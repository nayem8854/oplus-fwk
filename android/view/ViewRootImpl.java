package android.view;

import android.animation.LayoutTransition;
import android.app.ActivityManager;
import android.app.ActivityThread;
import android.app.IActivityManager;
import android.app.ResourcesManager;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.pm.OplusPackageManager;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.BLASTBufferQueue;
import android.graphics.Canvas;
import android.graphics.HardwareRenderer;
import android.graphics.Insets;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.RecordingCanvas;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.RenderNode;
import android.graphics.drawable.Drawable;
import android.hardware.display.DisplayManager;
import android.hardware.input.InputManager;
import android.media.AudioManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.Trace;
import android.os.UserHandle;
import android.util.AndroidRuntimeException;
import android.util.BoostFramework;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LongArray;
import android.util.MergedConfiguration;
import android.util.Slog;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeIdManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.accessibility.IAccessibilityEmbeddedConnection;
import android.view.accessibility.IAccessibilityInteractionConnection;
import android.view.accessibility.IAccessibilityInteractionConnectionCallback;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillManager;
import android.view.contentcapture.ContentCaptureManager;
import android.view.contentcapture.ContentCaptureSession;
import android.view.contentcapture.MainContentCaptureSession;
import android.view.inputmethod.InputMethodManager;
import android.widget.Scroller;
import com.android.internal.R;
import com.android.internal.os.IResultReceiver;
import com.android.internal.os.SomeArgs;
import com.android.internal.policy.DecorView;
import com.android.internal.policy.PhoneFallbackEventHandler;
import com.android.internal.util.Preconditions;
import com.android.internal.view.BaseSurfaceHolder;
import com.android.internal.view.RootViewSurfaceTaker;
import com.oplus.darkmode.IOplusDarkModeManager;
import com.oplus.debug.InputLog;
import com.oplus.screenmode.IOplusScreenModeFeature;
import com.oplus.screenshot.OplusLongshotViewRoot;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;

public final class ViewRootImpl implements ViewParent, View.AttachInfo.Callbacks, ThreadedRenderer.DrawCallbacks, IOplusBaseViewRoot {
  private static final int CONTENT_CAPTURE_ENABLED_FALSE = 2;
  
  private static final int CONTENT_CAPTURE_ENABLED_NOT_CHECKED = 0;
  
  private static final int CONTENT_CAPTURE_ENABLED_TRUE = 1;
  
  private static final boolean DBG = false;
  
  private static boolean DEBUG_CONFIGURATION = false;
  
  private static final boolean DEBUG_CONTENT_CAPTURE = false;
  
  private static boolean DEBUG_DIALOG = false;
  
  private static boolean DEBUG_DRAW = false;
  
  private static final boolean DEBUG_FPS = false;
  
  private static boolean DEBUG_IMF = false;
  
  private static final boolean DEBUG_INPUT_RESIZE = false;
  
  private static final boolean DEBUG_INPUT_STAGES = false;
  
  private static boolean DEBUG_INPUT_TRACING = false;
  
  private static final boolean DEBUG_KEEP_SCREEN_ON = false;
  
  private static boolean DEBUG_LAYOUT = false;
  
  private static final boolean DEBUG_ORIENTATION = false;
  
  private static boolean DEBUG_PANIC = false;
  
  private static final boolean DEBUG_SCROLL_CAPTURE = false;
  
  private static final boolean DEBUG_TRACKBALL = false;
  
  private static final boolean LOCAL_LOGV = false;
  
  private static final int MAX_QUEUED_INPUT_EVENT_POOL_SIZE = 10;
  
  static final int MAX_TRACKBALL_DELAY = 250;
  
  private static final int MSG_CHECK_FOCUS = 13;
  
  private static final int MSG_CLEAR_ACCESSIBILITY_FOCUS_HOST = 21;
  
  private static final int MSG_CLOSE_SYSTEM_DIALOGS = 14;
  
  private static final int MSG_DIE = 3;
  
  private static final int MSG_DISPATCH_APP_VISIBILITY = 8;
  
  private static final int MSG_DISPATCH_DRAG_EVENT = 15;
  
  private static final int MSG_DISPATCH_DRAG_LOCATION_EVENT = 16;
  
  private static final int MSG_DISPATCH_GET_NEW_SURFACE = 9;
  
  private static final int MSG_DISPATCH_INPUT_EVENT = 7;
  
  private static final int MSG_DISPATCH_KEY_FROM_AUTOFILL = 12;
  
  private static final int MSG_DISPATCH_KEY_FROM_IME = 11;
  
  private static final int MSG_DISPATCH_SYSTEM_UI_VISIBILITY = 17;
  
  private static final int MSG_DISPATCH_WINDOW_SHOWN = 25;
  
  private static final int MSG_DRAW_FINISHED = 29;
  
  private static final int MSG_HIDE_INSETS = 35;
  
  private static final int MSG_INSETS_CHANGED = 30;
  
  private static final int MSG_INSETS_CONTROL_CHANGED = 31;
  
  private static final int MSG_INVALIDATE = 1;
  
  private static final int MSG_INVALIDATE_RECT = 2;
  
  private static final int MSG_INVALIDATE_WORLD = 22;
  
  private static final int MSG_LOCATION_IN_PARENT_DISPLAY_CHANGED = 33;
  
  private static final int MSG_POINTER_CAPTURE_CHANGED = 28;
  
  private static final int MSG_PROCESS_INPUT_EVENTS = 19;
  
  private static final int MSG_REQUEST_KEYBOARD_SHORTCUTS = 26;
  
  private static final int MSG_REQUEST_SCROLL_CAPTURE = 36;
  
  private static final int MSG_RESIZED = 4;
  
  private static final int MSG_RESIZED_REPORT = 5;
  
  private static final int MSG_SHOW_INSETS = 34;
  
  private static final int MSG_SYNTHESIZE_INPUT_EVENT = 24;
  
  private static final int MSG_SYSTEM_GESTURE_EXCLUSION_CHANGED = 32;
  
  private static final int MSG_UPDATE_CONFIGURATION = 18;
  
  private static final int MSG_UPDATE_POINTER_ICON = 27;
  
  private static final int MSG_WINDOW_FOCUS_CHANGED = 6;
  
  private static final int MSG_WINDOW_MOVED = 23;
  
  private static final boolean MT_RENDERER_AVAILABLE = true;
  
  public static final int NEW_INSETS_MODE_FULL = 2;
  
  public static final int NEW_INSETS_MODE_IME = 1;
  
  public static final int NEW_INSETS_MODE_NONE = 0;
  
  private static final String PROPERTY_PROFILE_RENDERING = "viewroot.profile_rendering";
  
  private static final String TAG = "ViewRootImpl";
  
  private static final String USE_NEW_INSETS_PROPERTY = "persist.debug.new_insets";
  
  static final Interpolator mResizeInterpolator;
  
  private static boolean sAlwaysAssignFocus;
  
  private static boolean sCompatibilityDone;
  
  private static final ArrayList<ConfigChangedCallback> sConfigCallbacks;
  
  static boolean sFirstDrawComplete;
  
  static final ArrayList<Runnable> sFirstDrawHandlers;
  
  public static int sNewInsetsMode;
  
  static final ThreadLocal<HandlerActionQueue> sRunQueues;
  
  private boolean FRAME_ONT;
  
  private int doFrameIndex;
  
  private boolean isOptApp;
  
  private IAccessibilityEmbeddedConnection mAccessibilityEmbeddedConnection;
  
  View mAccessibilityFocusedHost;
  
  AccessibilityNodeInfo mAccessibilityFocusedVirtualView;
  
  final AccessibilityInteractionConnectionManager mAccessibilityInteractionConnectionManager;
  
  AccessibilityInteractionController mAccessibilityInteractionController;
  
  final AccessibilityManager mAccessibilityManager;
  
  private ActivityConfigCallback mActivityConfigCallback;
  
  private boolean mActivityRelaunched;
  
  boolean mAdded;
  
  boolean mAddedTouchMode;
  
  private boolean mAppVisibilityChanged;
  
  boolean mAppVisible;
  
  boolean mApplyInsetsRequested;
  
  final View.AttachInfo mAttachInfo;
  
  AudioManager mAudioManager;
  
  final String mBasePackageName;
  
  private BLASTBufferQueue mBlastBufferQueue;
  
  private SurfaceControl mBlastSurfaceControl;
  
  private SurfaceControl mBoundsLayer;
  
  private int mCanvasOffsetX;
  
  private int mCanvasOffsetY;
  
  final Choreographer mChoreographer;
  
  int mClientWindowLayoutFlags;
  
  IOplusViewRootUtil mColorViewRootUtil;
  
  final SystemUiVisibilityInfo mCompatibleVisibilityInfo;
  
  final ConsumeBatchedInputImmediatelyRunnable mConsumeBatchedInputImmediatelyRunnable;
  
  boolean mConsumeBatchedInputImmediatelyScheduled;
  
  boolean mConsumeBatchedInputScheduled;
  
  final ConsumeBatchedInputRunnable mConsumedBatchedInputRunnable;
  
  int mContentCaptureEnabled;
  
  public final Context mContext;
  
  int mCurScrollY;
  
  View mCurrentDragView;
  
  private PointerIcon mCustomPointerIcon;
  
  private final int mDensity;
  
  private final Object mDestroyLock;
  
  Rect mDirty;
  
  private boolean mDisableRelayout;
  
  int mDispatchedSystemUiVisibility;
  
  Display mDisplay;
  
  private final DisplayManager.DisplayListener mDisplayListener;
  
  final DisplayManager mDisplayManager;
  
  ClipDescription mDragDescription;
  
  final PointF mDragPoint;
  
  private boolean mDragResizing;
  
  boolean mDrawingAllowed;
  
  int mDrawsNeededToReport;
  
  private boolean mEnableTripleBuffering;
  
  FallbackEventHandler mFallbackEventHandler;
  
  boolean mFirst;
  
  private boolean mFirstFrameScheduled;
  
  InputStage mFirstInputStage;
  
  InputStage mFirstPostImeInputStage;
  
  private boolean mForceDecorViewVisibility;
  
  private boolean mForceDisableBLAST;
  
  private boolean mForceNextConfigUpdate;
  
  boolean mForceNextWindowRelayout;
  
  private int mFpsNumFrames;
  
  private long mFpsPrevTime;
  
  private long mFpsStartTime;
  
  boolean mFullRedrawNeeded;
  
  private final GestureExclusionTracker mGestureExclusionTracker;
  
  private final Object mGraphicLock;
  
  boolean mHadWindowFocus;
  
  final ViewRootHandler mHandler;
  
  boolean mHandlingLayoutInLayoutRequest;
  
  int mHardwareXOffset;
  
  int mHardwareYOffset;
  
  boolean mHaveMoveEvent;
  
  int mHeight;
  
  final HighContrastTextManager mHighContrastTextManager;
  
  private final ImeFocusController mImeFocusController;
  
  private boolean mInLayout;
  
  private final InputEventCompatProcessor mInputCompatProcessor;
  
  protected final InputEventConsistencyVerifier mInputEventConsistencyVerifier;
  
  WindowInputEventReceiver mInputEventReceiver;
  
  InputQueue mInputQueue;
  
  InputQueue.Callback mInputQueueCallback;
  
  private final InsetsController mInsetsController;
  
  final InvalidateOnAnimationRunnable mInvalidateOnAnimationRunnable;
  
  private boolean mInvalidateRootRequested;
  
  boolean mIsAmbientMode;
  
  public boolean mIsAnimating;
  
  boolean mIsCreating;
  
  boolean mIsDrawing;
  
  boolean mIsInTraversal;
  
  boolean mIsLuckyMoneyView;
  
  public boolean mIsWeixinLauncherUI;
  
  private final Configuration mLastConfigurationFromResources;
  
  final ViewTreeObserver.InternalInsetsInfo mLastGivenInsets;
  
  boolean mLastInCompatMode;
  
  private final MergedConfiguration mLastReportedMergedConfiguration;
  
  WeakReference<View> mLastScrolledFocus;
  
  int mLastSystemUiVisibility;
  
  final PointF mLastTouchPoint;
  
  int mLastTouchSource;
  
  private WindowInsets mLastWindowInsets;
  
  boolean mLayoutRequested;
  
  ArrayList<View> mLayoutRequesters;
  
  final IBinder mLeashToken;
  
  volatile Object mLocalDragState;
  
  final WindowLeaked mLocation;
  
  boolean mLostWindowFocus;
  
  public IOplusDarkModeManager mManager;
  
  private boolean mNeedsRendererSetup;
  
  boolean mNewSurfaceNeeded;
  
  private boolean mNextDrawUseBLASTSyncTransaction;
  
  private boolean mNextReportConsumeBLAST;
  
  private final int mNoncompatDensity;
  
  int mOrigWindowType;
  
  boolean mPausedForTransition;
  
  boolean mPendingAlwaysConsumeSystemBars;
  
  final Rect mPendingBackDropFrame;
  
  final DisplayCutout.ParcelableWrapper mPendingDisplayCutout;
  
  int mPendingInputEventCount;
  
  QueuedInputEvent mPendingInputEventHead;
  
  String mPendingInputEventQueueLengthCounterName;
  
  QueuedInputEvent mPendingInputEventTail;
  
  private final MergedConfiguration mPendingMergedConfiguration;
  
  private ArrayList<LayoutTransition> mPendingTransitions;
  
  boolean mPerformContentCapture;
  
  boolean mPointerCapture;
  
  private int mPointerIconType;
  
  final Region mPreviousTransparentRegion;
  
  boolean mProcessInputEventsScheduled;
  
  private boolean mProfile;
  
  private boolean mProfileRendering;
  
  private QueuedInputEvent mQueuedInputEventPool;
  
  private int mQueuedInputEventPoolSize;
  
  private boolean mRemoved;
  
  private Choreographer.FrameCallback mRenderProfiler;
  
  private boolean mRenderProfilingEnabled;
  
  boolean mReportNextDraw;
  
  private int mResizeMode;
  
  private HashSet<ScrollCaptureCallback> mRootScrollCaptureCallbacks;
  
  private SurfaceControl.Transaction mRtBLASTSyncTransaction;
  
  private ArrayList<String> mScreenModeViewList;
  
  private ScrollCaptureClient mScrollCaptureClient;
  
  boolean mScrollMayChange;
  
  int mScrollY;
  
  Scroller mScroller;
  
  private boolean mSendNextFrameToWm;
  
  SendWindowContentChangedAccessibilityEvent mSendWindowContentChangedAccessibilityEvent;
  
  int mSeq;
  
  int mSoftInputMode;
  
  boolean mStopped;
  
  public final Surface mSurface;
  
  private final ArrayList<SurfaceChangedCallback> mSurfaceChangedCallbacks;
  
  private final SurfaceControl.Transaction mSurfaceChangedTransaction;
  
  private final SurfaceControl mSurfaceControl;
  
  BaseSurfaceHolder mSurfaceHolder;
  
  SurfaceHolder.Callback2 mSurfaceHolderCallback;
  
  private final SurfaceSession mSurfaceSession;
  
  private final Point mSurfaceSize;
  
  InputStage mSyntheticInputStage;
  
  private String mTag;
  
  final int mTargetSdkVersion;
  
  private final Rect mTempBoundsRect;
  
  private final InsetsSourceControl[] mTempControls;
  
  HashSet<View> mTempHashSet;
  
  private final InsetsState mTempInsets;
  
  final Rect mTempRect;
  
  final Thread mThread;
  
  final Rect mTmpFrame;
  
  final int[] mTmpLocation;
  
  final Rect mTmpRect;
  
  final TypedValue mTmpValue;
  
  private final SurfaceControl.Transaction mTransaction;
  
  CompatibilityInfo.Translator mTranslator;
  
  final Region mTransparentRegion;
  
  int mTraversalBarrier;
  
  final TraversalRunnable mTraversalRunnable;
  
  public boolean mTraversalScheduled;
  
  private int mTypesHiddenByFlags;
  
  boolean mUnbufferedInputDispatch;
  
  int mUnbufferedInputSource;
  
  private final UnhandledKeyManager mUnhandledKeyManager;
  
  boolean mUpcomingInTouchMode;
  
  boolean mUpcomingWindowFocus;
  
  private boolean mUseBLASTAdapter;
  
  private boolean mUseMTRenderer;
  
  View mView;
  
  final ViewConfiguration mViewConfiguration;
  
  private int mViewLayoutDirectionInitial;
  
  public final OplusViewRootImplHooks mViewRootHooks;
  
  int mViewVisibility;
  
  final Rect mVisRect;
  
  int mWidth;
  
  boolean mWillDrawSoon;
  
  final Rect mWinFrame;
  
  final W mWindow;
  
  public final WindowManager.LayoutParams mWindowAttributes;
  
  boolean mWindowAttributesChanged;
  
  final ArrayList<WindowCallbacks> mWindowCallbacks;
  
  CountDownLatch mWindowDrawCountDown;
  
  boolean mWindowFocusChanged;
  
  final IWindowSession mWindowSession;
  
  static {
    DEBUG_DIALOG = false;
    DEBUG_IMF = false;
    DEBUG_CONFIGURATION = false;
    sNewInsetsMode = SystemProperties.getInt("persist.debug.new_insets", 2);
    sRunQueues = new ThreadLocal<>();
    sFirstDrawHandlers = new ArrayList<>();
    sFirstDrawComplete = false;
    sConfigCallbacks = new ArrayList<>();
    sCompatibilityDone = false;
    mResizeInterpolator = new AccelerateDecelerateInterpolator();
    DEBUG_PANIC = false;
    DEBUG_INPUT_TRACING = false;
  }
  
  public ImeFocusController getImeFocusController() {
    return this.mImeFocusController;
  }
  
  public ScrollCaptureClient getScrollCaptureClient() {
    return this.mScrollCaptureClient;
  }
  
  class SystemUiVisibilityInfo {
    int globalVisibility;
    
    int localChanges;
    
    int localValue;
    
    int seq;
  }
  
  public ViewRootImpl(Context paramContext, Display paramDisplay) {
    this(paramContext, paramDisplay, WindowManagerGlobal.getWindowSession(), false);
  }
  
  public ViewRootImpl(Context paramContext, Display paramDisplay, IWindowSession paramIWindowSession) {
    this(paramContext, paramDisplay, paramIWindowSession, false);
  }
  
  public ViewRootImpl(Context paramContext, Display paramDisplay, IWindowSession paramIWindowSession, boolean paramBoolean) {
    Choreographer choreographer;
    byte b;
    InputEventConsistencyVerifier inputEventConsistencyVerifier;
    this.mGraphicLock = new Object();
    this.mDestroyLock = new Object();
    this.mWindowCallbacks = new ArrayList<>();
    this.mTmpLocation = new int[2];
    this.mTmpValue = new TypedValue();
    this.mWindowAttributes = new WindowManager.LayoutParams();
    this.mAppVisible = true;
    this.mForceDecorViewVisibility = false;
    this.mOrigWindowType = -1;
    this.mStopped = false;
    this.mIsAmbientMode = false;
    this.mPausedForTransition = false;
    this.mLastInCompatMode = false;
    if (sNewInsetsMode == 2) {
      b = 0;
    } else {
      b = -1;
    } 
    this.mDispatchedSystemUiVisibility = b;
    this.mSurfaceSize = new Point();
    this.mTempBoundsRect = new Rect();
    this.mFirstFrameScheduled = false;
    this.doFrameIndex = 0;
    this.FRAME_ONT = true;
    this.isOptApp = false;
    this.mDisableRelayout = false;
    this.mContentCaptureEnabled = 0;
    this.mUnbufferedInputSource = 0;
    this.mPendingInputEventQueueLengthCounterName = "pq";
    this.mUnhandledKeyManager = new UnhandledKeyManager();
    this.mWindowAttributesChanged = false;
    this.mSurface = new Surface();
    this.mSurfaceControl = new SurfaceControl();
    this.mBlastSurfaceControl = new SurfaceControl();
    this.mSurfaceChangedTransaction = new SurfaceControl.Transaction();
    this.mSurfaceSession = new SurfaceSession();
    this.mTransaction = new SurfaceControl.Transaction();
    this.mTmpFrame = new Rect();
    this.mTmpRect = new Rect();
    this.mPendingBackDropFrame = new Rect();
    this.mPendingDisplayCutout = new DisplayCutout.ParcelableWrapper(DisplayCutout.NO_CUTOUT);
    this.mTempInsets = new InsetsState();
    this.mTempControls = new InsetsSourceControl[16];
    this.mLastGivenInsets = new ViewTreeObserver.InternalInsetsInfo();
    this.mTypesHiddenByFlags = 0;
    this.mLastConfigurationFromResources = new Configuration();
    this.mLastReportedMergedConfiguration = new MergedConfiguration();
    this.mPendingMergedConfiguration = new MergedConfiguration();
    this.mDragPoint = new PointF();
    this.mLastTouchPoint = new PointF();
    this.mFpsStartTime = -1L;
    this.mFpsPrevTime = -1L;
    this.mPointerIconType = 1;
    this.mCustomPointerIcon = null;
    this.mAccessibilityInteractionConnectionManager = new AccessibilityInteractionConnectionManager();
    this.mInLayout = false;
    this.mLayoutRequesters = new ArrayList<>();
    this.mHandlingLayoutInLayoutRequest = false;
    this.mColorViewRootUtil = null;
    this.mIsLuckyMoneyView = false;
    this.mIsWeixinLauncherUI = false;
    this.mManager = null;
    if (InputEventConsistencyVerifier.isInstrumentationEnabled()) {
      inputEventConsistencyVerifier = new InputEventConsistencyVerifier(this, 0);
    } else {
      inputEventConsistencyVerifier = null;
    } 
    this.mInputEventConsistencyVerifier = inputEventConsistencyVerifier;
    this.mGestureExclusionTracker = new GestureExclusionTracker();
    this.mRtBLASTSyncTransaction = new SurfaceControl.Transaction();
    this.mSendNextFrameToWm = false;
    this.mTag = "ViewRootImpl";
    this.mHaveMoveEvent = false;
    this.mProfile = false;
    this.mDisplayListener = new DisplayManager.DisplayListener() {
        final ViewRootImpl this$0;
        
        public void onDisplayChanged(int param1Int) {
          if (ViewRootImpl.this.mView != null && ViewRootImpl.this.mDisplay.getDisplayId() == param1Int) {
            param1Int = ViewRootImpl.this.mAttachInfo.mDisplayState;
            int i = ViewRootImpl.this.mDisplay.getState();
            if (param1Int != i) {
              ViewRootImpl.this.mAttachInfo.mDisplayState = i;
              ViewRootImpl.this.pokeDrawLockIfNeeded();
              if (param1Int != 0) {
                int j = toViewScreenState(param1Int);
                i = toViewScreenState(i);
                if (j != i)
                  ViewRootImpl.this.mView.dispatchScreenStateChanged(i); 
                if (param1Int == 1) {
                  ViewRootImpl.this.mFullRedrawNeeded = true;
                  ViewRootImpl.this.scheduleTraversals();
                } 
              } 
            } 
          } 
        }
        
        public void onDisplayRemoved(int param1Int) {}
        
        public void onDisplayAdded(int param1Int) {}
        
        private int toViewScreenState(int param1Int) {
          boolean bool = true;
          if (param1Int == 1) {
            param1Int = 0;
          } else {
            param1Int = bool;
          } 
          return param1Int;
        }
      };
    this.mSurfaceChangedCallbacks = new ArrayList<>();
    this.mDrawsNeededToReport = 0;
    this.mHandler = new ViewRootHandler();
    this.mTraversalRunnable = new TraversalRunnable();
    this.mConsumedBatchedInputRunnable = new ConsumeBatchedInputRunnable();
    this.mConsumeBatchedInputImmediatelyRunnable = new ConsumeBatchedInputImmediatelyRunnable();
    this.mInvalidateOnAnimationRunnable = new InvalidateOnAnimationRunnable();
    this.mScreenModeViewList = new ArrayList<>();
    this.mContext = paramContext;
    this.mWindowSession = paramIWindowSession;
    this.mViewRootHooks = new OplusViewRootImplHooks(this, paramContext);
    this.mDisplay = paramDisplay;
    this.mBasePackageName = paramContext.getBasePackageName();
    this.mThread = Thread.currentThread();
    WindowLeaked windowLeaked = new WindowLeaked(null);
    windowLeaked.fillInStackTrace();
    this.mWidth = -1;
    this.mHeight = -1;
    this.mDirty = new Rect();
    this.mTempRect = new Rect();
    this.mVisRect = new Rect();
    this.mWinFrame = new Rect();
    this.mWindow = this.mViewRootHooks.createWindowClient(this);
    this.mLeashToken = (IBinder)new Binder();
    this.mTargetSdkVersion = (paramContext.getApplicationInfo()).targetSdkVersion;
    this.mViewVisibility = 8;
    this.mTransparentRegion = new Region();
    this.mPreviousTransparentRegion = new Region();
    this.mFirst = true;
    this.mPerformContentCapture = true;
    this.mAdded = false;
    this.mAttachInfo = new View.AttachInfo(this.mWindowSession, this.mWindow, paramDisplay, this, this.mHandler, this, paramContext);
    this.mCompatibleVisibilityInfo = new SystemUiVisibilityInfo();
    AccessibilityManager accessibilityManager = AccessibilityManager.getInstance(paramContext);
    accessibilityManager.addAccessibilityStateChangeListener(this.mAccessibilityInteractionConnectionManager, this.mHandler);
    HighContrastTextManager highContrastTextManager = new HighContrastTextManager();
    this.mAccessibilityManager.addHighTextContrastStateChangeListener(highContrastTextManager, this.mHandler);
    this.mViewConfiguration = ViewConfiguration.get(paramContext);
    this.mDensity = (paramContext.getResources().getDisplayMetrics()).densityDpi;
    this.mNoncompatDensity = (paramContext.getResources().getDisplayMetrics()).noncompatDensityDpi;
    this.mFallbackEventHandler = (FallbackEventHandler)new PhoneFallbackEventHandler(paramContext);
    if (paramBoolean) {
      choreographer = Choreographer.getSfInstance();
    } else {
      choreographer = Choreographer.getInstance();
    } 
    this.mChoreographer = choreographer;
    this.mDisplayManager = (DisplayManager)paramContext.getSystemService("display");
    this.mInsetsController = new InsetsController(new ViewRootInsetsControllerHost(this));
    String str = paramContext.getResources().getString(17039921);
    if (str.isEmpty()) {
      this.mInputCompatProcessor = new InputEventCompatProcessor(paramContext);
    } else {
      try {
        Class<?> clazz = Class.forName(str);
        InputEventCompatProcessor inputEventCompatProcessor = clazz.getConstructor(new Class[] { Context.class }).newInstance(new Object[] { paramContext });
        this.mInputCompatProcessor = inputEventCompatProcessor;
      } catch (Exception exception) {
        Log.e("ViewRootImpl", "Unable to create the InputEventCompatProcessor. ", exception);
        this.mInputCompatProcessor = null;
      } finally {}
    } 
    if (!sCompatibilityDone) {
      if (this.mTargetSdkVersion < 28) {
        paramBoolean = true;
      } else {
        paramBoolean = false;
      } 
      sAlwaysAssignFocus = paramBoolean;
      sCompatibilityDone = true;
    } 
    loadSystemProperties();
    this.FRAME_ONT = paramBoolean = SystemProperties.getBoolean("persist.oppo.frameopts", true);
    if (paramBoolean && this.mBasePackageName != null) {
      OplusPackageManager oplusPackageManager = new OplusPackageManager();
      try {
        this.isOptApp = oplusPackageManager.inOplusStandardWhiteList("app_launch_opt_whitelist", 1, this.mBasePackageName);
        this.mDisableRelayout = oplusPackageManager.inCptWhiteList(736, this.mBasePackageName);
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("failed to query whitelist, package name: ");
        stringBuilder.append(this.mBasePackageName);
        Log.e("ViewRootImpl", stringBuilder.toString());
        this.isOptApp = false;
      } 
    } 
    try {
      IOplusViewRootUtil iOplusViewRootUtil = (IOplusViewRootUtil)OplusFeatureCache.getOrCreate(IOplusViewRootUtil.DEFAULT, new Object[0]);
      if (iOplusViewRootUtil != null) {
        paramBoolean = ((IOplusScreenModeFeature)OplusFeatureCache.get((IOplusCommonFeature)IOplusScreenModeFeature.DEFAULT)).supportDisplayCompat();
        this.mColorViewRootUtil.initSwipState(paramDisplay, this.mContext, paramBoolean);
      } 
    } catch (LinkageError linkageError) {
      Log.e("ViewRootImpl", "mColorViewRootUtil", linkageError);
    } 
    this.mImeFocusController = new ImeFocusController(this);
    this.mManager = ((IOplusDarkModeManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusDarkModeManager.DEFAULT, new Object[0])).newOplusDarkModeManager();
    DEBUG_PANIC = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    DEBUG_INPUT_TRACING = SystemProperties.getBoolean("persist.sys.tp_input.trace", false);
    DEBUG_IMF = DEBUG_PANIC;
    setDynamicalLogEnable(SystemProperties.get("debug.dynamicallog.view", "").equals(this.mBasePackageName));
  }
  
  public static void addFirstDrawHandler(Runnable paramRunnable) {
    synchronized (sFirstDrawHandlers) {
      if (!sFirstDrawComplete)
        sFirstDrawHandlers.add(paramRunnable); 
      return;
    } 
  }
  
  public static void addConfigCallback(ConfigChangedCallback paramConfigChangedCallback) {
    synchronized (sConfigCallbacks) {
      sConfigCallbacks.add(paramConfigChangedCallback);
      return;
    } 
  }
  
  public void setActivityConfigCallback(ActivityConfigCallback paramActivityConfigCallback) {
    this.mActivityConfigCallback = paramActivityConfigCallback;
  }
  
  public void setOnContentApplyWindowInsetsListener(Window.OnContentApplyWindowInsetsListener paramOnContentApplyWindowInsetsListener) {
    this.mAttachInfo.mContentOnApplyWindowInsetsListener = paramOnContentApplyWindowInsetsListener;
    if (!this.mFirst)
      requestFitSystemWindows(); 
  }
  
  public void addWindowCallbacks(WindowCallbacks paramWindowCallbacks) {
    synchronized (this.mWindowCallbacks) {
      this.mWindowCallbacks.add(paramWindowCallbacks);
      return;
    } 
  }
  
  public void removeWindowCallbacks(WindowCallbacks paramWindowCallbacks) {
    synchronized (this.mWindowCallbacks) {
      this.mWindowCallbacks.remove(paramWindowCallbacks);
      return;
    } 
  }
  
  public void reportDrawFinish() {
    CountDownLatch countDownLatch = this.mWindowDrawCountDown;
    if (countDownLatch != null)
      countDownLatch.countDown(); 
  }
  
  public void profile() {
    this.mProfile = true;
  }
  
  static boolean isInTouchMode() {
    IWindowSession iWindowSession = WindowManagerGlobal.peekWindowSession();
    if (iWindowSession != null)
      try {
        return iWindowSession.getInTouchMode();
      } catch (RemoteException remoteException) {} 
    return false;
  }
  
  public void notifyChildRebuilt() {
    if (this.mView instanceof RootViewSurfaceTaker) {
      SurfaceHolder.Callback2 callback22 = this.mSurfaceHolderCallback;
      if (callback22 != null)
        this.mSurfaceHolder.removeCallback(callback22); 
      RootViewSurfaceTaker rootViewSurfaceTaker2 = (RootViewSurfaceTaker)this.mView;
      SurfaceHolder.Callback2 callback21 = rootViewSurfaceTaker2.willYouTakeTheSurface();
      if (callback21 != null) {
        TakenSurfaceHolder takenSurfaceHolder = new TakenSurfaceHolder();
        takenSurfaceHolder.setFormat(0);
        this.mSurfaceHolder.addCallback(this.mSurfaceHolderCallback);
      } else {
        this.mSurfaceHolder = null;
      } 
      RootViewSurfaceTaker rootViewSurfaceTaker1 = (RootViewSurfaceTaker)this.mView;
      InputQueue.Callback callback = rootViewSurfaceTaker1.willYouTakeTheInputQueue();
      if (callback != null)
        callback.onInputQueueCreated(this.mInputQueue); 
    } 
  }
  
  public void setView(View paramView1, WindowManager.LayoutParams paramLayoutParams, View paramView2) {
    setView(paramView1, paramLayoutParams, paramView2, UserHandle.myUserId());
  }
  
  public void setView(View paramView1, WindowManager.LayoutParams paramLayoutParams, View paramView2, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mView : Landroid/view/View;
    //   6: ifnonnull -> 2167
    //   9: aload_0
    //   10: aload_1
    //   11: putfield mView : Landroid/view/View;
    //   14: aload_0
    //   15: getfield mViewRootHooks : Landroid/view/OplusViewRootImplHooks;
    //   18: aload_1
    //   19: invokevirtual setView : (Landroid/view/View;)V
    //   22: invokestatic getInstance : ()Landroid/common/ColorFrameworkFactory;
    //   25: getstatic android/view/IOplusBurmeseZgHooks.DEFAULT : Landroid/view/IOplusBurmeseZgHooks;
    //   28: iconst_0
    //   29: anewarray java/lang/Object
    //   32: invokevirtual getFeature : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   35: checkcast android/view/IOplusBurmeseZgHooks
    //   38: aload_0
    //   39: getfield mContext : Landroid/content/Context;
    //   42: invokeinterface initBurmeseZgFlag : (Landroid/content/Context;)V
    //   47: aload_0
    //   48: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   51: aload_0
    //   52: getfield mDisplay : Landroid/view/Display;
    //   55: invokevirtual getState : ()I
    //   58: putfield mDisplayState : I
    //   61: aload_0
    //   62: getfield mDisplayManager : Landroid/hardware/display/DisplayManager;
    //   65: aload_0
    //   66: getfield mDisplayListener : Landroid/hardware/display/DisplayManager$DisplayListener;
    //   69: aload_0
    //   70: getfield mHandler : Landroid/view/ViewRootImpl$ViewRootHandler;
    //   73: invokevirtual registerDisplayListener : (Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V
    //   76: aload_0
    //   77: aload_0
    //   78: getfield mView : Landroid/view/View;
    //   81: invokevirtual getRawLayoutDirection : ()I
    //   84: putfield mViewLayoutDirectionInitial : I
    //   87: aload_0
    //   88: getfield mFallbackEventHandler : Landroid/view/FallbackEventHandler;
    //   91: aload_1
    //   92: invokeinterface setView : (Landroid/view/View;)V
    //   97: aload_0
    //   98: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   101: astore #5
    //   103: aload #5
    //   105: aload_2
    //   106: invokevirtual copyFrom : (Landroid/view/WindowManager$LayoutParams;)I
    //   109: pop
    //   110: aload_0
    //   111: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   114: getfield packageName : Ljava/lang/String;
    //   117: ifnonnull -> 131
    //   120: aload_0
    //   121: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   124: aload_0
    //   125: getfield mBasePackageName : Ljava/lang/String;
    //   128: putfield packageName : Ljava/lang/String;
    //   131: aload_0
    //   132: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   135: astore_2
    //   136: aload_2
    //   137: aload_2
    //   138: getfield privateFlags : I
    //   141: ldc_w 33554432
    //   144: ior
    //   145: putfield privateFlags : I
    //   148: aload_0
    //   149: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   152: astore #5
    //   154: aload_0
    //   155: invokespecial setTag : ()V
    //   158: aload_0
    //   159: aload #5
    //   161: getfield flags : I
    //   164: putfield mClientWindowLayoutFlags : I
    //   167: aload_0
    //   168: aconst_null
    //   169: aconst_null
    //   170: invokevirtual setAccessibilityFocus : (Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    //   173: aload_1
    //   174: instanceof com/android/internal/view/RootViewSurfaceTaker
    //   177: ifeq -> 231
    //   180: aload_1
    //   181: checkcast com/android/internal/view/RootViewSurfaceTaker
    //   184: astore_2
    //   185: aload_2
    //   186: invokeinterface willYouTakeTheSurface : ()Landroid/view/SurfaceHolder$Callback2;
    //   191: astore_2
    //   192: aload_0
    //   193: aload_2
    //   194: putfield mSurfaceHolderCallback : Landroid/view/SurfaceHolder$Callback2;
    //   197: aload_2
    //   198: ifnull -> 231
    //   201: new android/view/ViewRootImpl$TakenSurfaceHolder
    //   204: astore_2
    //   205: aload_2
    //   206: aload_0
    //   207: invokespecial <init> : (Landroid/view/ViewRootImpl;)V
    //   210: aload_0
    //   211: aload_2
    //   212: putfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   215: aload_2
    //   216: iconst_0
    //   217: invokevirtual setFormat : (I)V
    //   220: aload_0
    //   221: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   224: aload_0
    //   225: getfield mSurfaceHolderCallback : Landroid/view/SurfaceHolder$Callback2;
    //   228: invokevirtual addCallback : (Landroid/view/SurfaceHolder$Callback;)V
    //   231: aload #5
    //   233: getfield hasManualSurfaceInsets : Z
    //   236: ifne -> 247
    //   239: aload #5
    //   241: aload_1
    //   242: iconst_0
    //   243: iconst_1
    //   244: invokevirtual setSurfaceInsets : (Landroid/view/View;ZZ)V
    //   247: aload_0
    //   248: getfield mDisplay : Landroid/view/Display;
    //   251: astore_2
    //   252: aload_2
    //   253: invokevirtual getDisplayAdjustments : ()Landroid/view/DisplayAdjustments;
    //   256: invokevirtual getCompatibilityInfo : ()Landroid/content/res/CompatibilityInfo;
    //   259: astore #6
    //   261: aload_0
    //   262: aload #6
    //   264: invokevirtual getTranslator : ()Landroid/content/res/CompatibilityInfo$Translator;
    //   267: putfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   270: aload_0
    //   271: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   274: ifnonnull -> 321
    //   277: aload_0
    //   278: aload #5
    //   280: invokespecial enableHardwareAcceleration : (Landroid/view/WindowManager$LayoutParams;)V
    //   283: aload_0
    //   284: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   287: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   290: ifnull -> 299
    //   293: iconst_1
    //   294: istore #7
    //   296: goto -> 302
    //   299: iconst_0
    //   300: istore #7
    //   302: aload_0
    //   303: getfield mUseMTRenderer : Z
    //   306: iload #7
    //   308: if_icmpeq -> 321
    //   311: aload_0
    //   312: invokespecial endDragResizing : ()V
    //   315: aload_0
    //   316: iload #7
    //   318: putfield mUseMTRenderer : Z
    //   321: aload_0
    //   322: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   325: ifnull -> 359
    //   328: aload_0
    //   329: getfield mSurface : Landroid/view/Surface;
    //   332: aload_0
    //   333: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   336: invokevirtual setCompatibilityTranslator : (Landroid/content/res/CompatibilityInfo$Translator;)V
    //   339: aload #5
    //   341: invokevirtual backup : ()V
    //   344: aload_0
    //   345: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   348: aload #5
    //   350: invokevirtual translateWindowLayout : (Landroid/view/WindowManager$LayoutParams;)V
    //   353: iconst_1
    //   354: istore #8
    //   356: goto -> 362
    //   359: iconst_0
    //   360: istore #8
    //   362: getstatic android/view/ViewRootImpl.DEBUG_LAYOUT : Z
    //   365: ifeq -> 410
    //   368: aload_0
    //   369: getfield mTag : Ljava/lang/String;
    //   372: astore_2
    //   373: new java/lang/StringBuilder
    //   376: astore #9
    //   378: aload #9
    //   380: invokespecial <init> : ()V
    //   383: aload #9
    //   385: ldc_w 'WindowLayout in setView:'
    //   388: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   391: pop
    //   392: aload #9
    //   394: aload #5
    //   396: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   399: pop
    //   400: aload_2
    //   401: aload #9
    //   403: invokevirtual toString : ()Ljava/lang/String;
    //   406: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   409: pop
    //   410: aload #6
    //   412: invokevirtual supportsScreen : ()Z
    //   415: ifne -> 437
    //   418: aload #5
    //   420: aload #5
    //   422: getfield privateFlags : I
    //   425: sipush #128
    //   428: ior
    //   429: putfield privateFlags : I
    //   432: aload_0
    //   433: iconst_1
    //   434: putfield mLastInCompatMode : Z
    //   437: aload_0
    //   438: aload #5
    //   440: getfield softInputMode : I
    //   443: putfield mSoftInputMode : I
    //   446: aload_0
    //   447: iconst_1
    //   448: putfield mWindowAttributesChanged : Z
    //   451: aload_0
    //   452: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   455: aload_1
    //   456: putfield mRootView : Landroid/view/View;
    //   459: aload_0
    //   460: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   463: astore_2
    //   464: aload_0
    //   465: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   468: ifnull -> 477
    //   471: iconst_1
    //   472: istore #7
    //   474: goto -> 480
    //   477: iconst_0
    //   478: istore #7
    //   480: aload_2
    //   481: iload #7
    //   483: putfield mScalingRequired : Z
    //   486: aload_0
    //   487: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   490: astore_2
    //   491: aload_0
    //   492: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   495: ifnonnull -> 504
    //   498: fconst_1
    //   499: fstore #10
    //   501: goto -> 513
    //   504: aload_0
    //   505: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   508: getfield applicationScale : F
    //   511: fstore #10
    //   513: aload_2
    //   514: fload #10
    //   516: putfield mApplicationScale : F
    //   519: aload_3
    //   520: ifnull -> 536
    //   523: aload_0
    //   524: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   527: astore_2
    //   528: aload_2
    //   529: aload_3
    //   530: invokevirtual getApplicationWindowToken : ()Landroid/os/IBinder;
    //   533: putfield mPanelParentWindowToken : Landroid/os/IBinder;
    //   536: aload_0
    //   537: iconst_1
    //   538: putfield mAdded : Z
    //   541: aload_0
    //   542: invokevirtual requestLayout : ()V
    //   545: aload_0
    //   546: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   549: getfield inputFeatures : I
    //   552: iconst_2
    //   553: iand
    //   554: ifne -> 568
    //   557: new android/view/InputChannel
    //   560: astore_2
    //   561: aload_2
    //   562: invokespecial <init> : ()V
    //   565: goto -> 570
    //   568: aconst_null
    //   569: astore_2
    //   570: aload_0
    //   571: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   574: getfield privateFlags : I
    //   577: sipush #16384
    //   580: iand
    //   581: ifeq -> 590
    //   584: iconst_1
    //   585: istore #7
    //   587: goto -> 593
    //   590: iconst_0
    //   591: istore #7
    //   593: aload_0
    //   594: iload #7
    //   596: putfield mForceDecorViewVisibility : Z
    //   599: aload_0
    //   600: getfield mViewRootHooks : Landroid/view/OplusViewRootImplHooks;
    //   603: aload_0
    //   604: getfield mView : Landroid/view/View;
    //   607: aload_0
    //   608: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   611: invokevirtual markUserDefinedToast : (Landroid/view/View;Landroid/view/WindowManager$LayoutParams;)V
    //   614: aload_0
    //   615: aload_0
    //   616: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   619: getfield type : I
    //   622: putfield mOrigWindowType : I
    //   625: aload_0
    //   626: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   629: iconst_1
    //   630: putfield mRecomputeGlobalAttributes : Z
    //   633: aload_0
    //   634: invokespecial collectViewAttributes : ()Z
    //   637: pop
    //   638: aload_0
    //   639: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   642: invokestatic adjustLayoutParamsForCompatibility : (Landroid/view/WindowManager$LayoutParams;)V
    //   645: aload_0
    //   646: getfield mWindowSession : Landroid/view/IWindowSession;
    //   649: astore #11
    //   651: aload_0
    //   652: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   655: astore #9
    //   657: aload_0
    //   658: getfield mSeq : I
    //   661: istore #12
    //   663: aload_0
    //   664: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   667: astore #13
    //   669: aload_0
    //   670: invokevirtual getHostVisibility : ()I
    //   673: istore #14
    //   675: aload_0
    //   676: getfield mDisplay : Landroid/view/Display;
    //   679: invokevirtual getDisplayId : ()I
    //   682: istore #15
    //   684: aload_0
    //   685: getfield mTmpFrame : Landroid/graphics/Rect;
    //   688: astore #16
    //   690: aload_0
    //   691: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   694: getfield mContentInsets : Landroid/graphics/Rect;
    //   697: astore #6
    //   699: aload_0
    //   700: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   703: getfield mStableInsets : Landroid/graphics/Rect;
    //   706: astore #17
    //   708: aload_0
    //   709: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   712: getfield mDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
    //   715: astore #18
    //   717: aload_0
    //   718: getfield mTempInsets : Landroid/view/InsetsState;
    //   721: astore_3
    //   722: aload_0
    //   723: getfield mTempControls : [Landroid/view/InsetsSourceControl;
    //   726: astore #19
    //   728: aload #11
    //   730: aload #9
    //   732: iload #12
    //   734: aload #13
    //   736: iload #14
    //   738: iload #15
    //   740: iload #4
    //   742: aload #16
    //   744: aload #6
    //   746: aload #17
    //   748: aload #18
    //   750: aload_2
    //   751: aload_3
    //   752: aload #19
    //   754: invokeinterface addToDisplayAsUser : (Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IIILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/DisplayCutout$ParcelableWrapper;Landroid/view/InputChannel;Landroid/view/InsetsState;[Landroid/view/InsetsSourceControl;)I
    //   759: istore #4
    //   761: aload_0
    //   762: aload_0
    //   763: getfield mTmpFrame : Landroid/graphics/Rect;
    //   766: invokespecial setFrame : (Landroid/graphics/Rect;)V
    //   769: iload #8
    //   771: ifeq -> 779
    //   774: aload #5
    //   776: invokevirtual restore : ()V
    //   779: aload_0
    //   780: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   783: ifnull -> 800
    //   786: aload_0
    //   787: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   790: aload_0
    //   791: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   794: getfield mContentInsets : Landroid/graphics/Rect;
    //   797: invokevirtual translateRectInScreenToAppWindow : (Landroid/graphics/Rect;)V
    //   800: aload_0
    //   801: getfield mPendingDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
    //   804: aload_0
    //   805: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   808: getfield mDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
    //   811: invokevirtual set : (Landroid/view/DisplayCutout$ParcelableWrapper;)V
    //   814: aload_0
    //   815: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   818: astore_3
    //   819: iload #4
    //   821: iconst_4
    //   822: iand
    //   823: ifeq -> 832
    //   826: iconst_1
    //   827: istore #7
    //   829: goto -> 835
    //   832: iconst_0
    //   833: istore #7
    //   835: aload_3
    //   836: iload #7
    //   838: putfield mAlwaysConsumeSystemBars : Z
    //   841: aload_0
    //   842: aload_0
    //   843: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   846: getfield mAlwaysConsumeSystemBars : Z
    //   849: putfield mPendingAlwaysConsumeSystemBars : Z
    //   852: aload_0
    //   853: getfield mInsetsController : Landroid/view/InsetsController;
    //   856: aload_0
    //   857: getfield mTempInsets : Landroid/view/InsetsState;
    //   860: invokevirtual onStateChanged : (Landroid/view/InsetsState;)Z
    //   863: pop
    //   864: aload_0
    //   865: getfield mInsetsController : Landroid/view/InsetsController;
    //   868: aload_0
    //   869: getfield mTempControls : [Landroid/view/InsetsSourceControl;
    //   872: invokevirtual onControlsChanged : ([Landroid/view/InsetsSourceControl;)V
    //   875: getstatic android/view/ViewRootImpl.DEBUG_LAYOUT : Z
    //   878: ifeq -> 922
    //   881: aload_0
    //   882: getfield mTag : Ljava/lang/String;
    //   885: astore #6
    //   887: new java/lang/StringBuilder
    //   890: astore_3
    //   891: aload_3
    //   892: invokespecial <init> : ()V
    //   895: aload_3
    //   896: ldc_w 'Added window '
    //   899: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   902: pop
    //   903: aload_3
    //   904: aload_0
    //   905: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   908: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   911: pop
    //   912: aload #6
    //   914: aload_3
    //   915: invokevirtual toString : ()Ljava/lang/String;
    //   918: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   921: pop
    //   922: iload #4
    //   924: ifge -> 1585
    //   927: aload_0
    //   928: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   931: aconst_null
    //   932: putfield mRootView : Landroid/view/View;
    //   935: aload_0
    //   936: iconst_0
    //   937: putfield mAdded : Z
    //   940: aload_0
    //   941: getfield mFallbackEventHandler : Landroid/view/FallbackEventHandler;
    //   944: aconst_null
    //   945: invokeinterface setView : (Landroid/view/View;)V
    //   950: aload_0
    //   951: invokevirtual unscheduleTraversals : ()V
    //   954: aload_0
    //   955: aconst_null
    //   956: aconst_null
    //   957: invokevirtual setAccessibilityFocus : (Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    //   960: aload_0
    //   961: getfield mAccessibilityInteractionConnectionManager : Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;
    //   964: invokevirtual ensureNoConnection : ()V
    //   967: aload_0
    //   968: getfield mAccessibilityManager : Landroid/view/accessibility/AccessibilityManager;
    //   971: aload_0
    //   972: getfield mAccessibilityInteractionConnectionManager : Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;
    //   975: invokevirtual removeAccessibilityStateChangeListener : (Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z
    //   978: pop
    //   979: aload_0
    //   980: getfield mAccessibilityManager : Landroid/view/accessibility/AccessibilityManager;
    //   983: aload_0
    //   984: getfield mHighContrastTextManager : Landroid/view/ViewRootImpl$HighContrastTextManager;
    //   987: invokevirtual removeHighTextContrastStateChangeListener : (Landroid/view/accessibility/AccessibilityManager$HighTextContrastChangeListener;)V
    //   990: aload_0
    //   991: invokespecial removeSendWindowContentChangedCallback : ()V
    //   994: aload_0
    //   995: getfield mDisplayManager : Landroid/hardware/display/DisplayManager;
    //   998: aload_0
    //   999: getfield mDisplayListener : Landroid/hardware/display/DisplayManager$DisplayListener;
    //   1002: invokevirtual unregisterDisplayListener : (Landroid/hardware/display/DisplayManager$DisplayListener;)V
    //   1005: iload #4
    //   1007: tableswitch default -> 1064, -11 -> 1505, -10 -> 1438, -9 -> 1391, -8 -> 1332, -7 -> 1265, -6 -> 1262, -5 -> 1215, -4 -> 1167, -3 -> 1119, -2 -> 1071, -1 -> 1071
    //   1064: new java/lang/RuntimeException
    //   1067: astore_1
    //   1068: goto -> 1552
    //   1071: new android/view/WindowManager$BadTokenException
    //   1074: astore_1
    //   1075: new java/lang/StringBuilder
    //   1078: astore_2
    //   1079: aload_2
    //   1080: invokespecial <init> : ()V
    //   1083: aload_2
    //   1084: ldc_w 'Unable to add window -- token '
    //   1087: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1090: pop
    //   1091: aload_2
    //   1092: aload #5
    //   1094: getfield token : Landroid/os/IBinder;
    //   1097: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1100: pop
    //   1101: aload_2
    //   1102: ldc_w ' is not valid; is your activity running?'
    //   1105: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1108: pop
    //   1109: aload_1
    //   1110: aload_2
    //   1111: invokevirtual toString : ()Ljava/lang/String;
    //   1114: invokespecial <init> : (Ljava/lang/String;)V
    //   1117: aload_1
    //   1118: athrow
    //   1119: new android/view/WindowManager$BadTokenException
    //   1122: astore_1
    //   1123: new java/lang/StringBuilder
    //   1126: astore_2
    //   1127: aload_2
    //   1128: invokespecial <init> : ()V
    //   1131: aload_2
    //   1132: ldc_w 'Unable to add window -- token '
    //   1135: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1138: pop
    //   1139: aload_2
    //   1140: aload #5
    //   1142: getfield token : Landroid/os/IBinder;
    //   1145: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1148: pop
    //   1149: aload_2
    //   1150: ldc_w ' is not for an application'
    //   1153: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1156: pop
    //   1157: aload_1
    //   1158: aload_2
    //   1159: invokevirtual toString : ()Ljava/lang/String;
    //   1162: invokespecial <init> : (Ljava/lang/String;)V
    //   1165: aload_1
    //   1166: athrow
    //   1167: new android/view/WindowManager$BadTokenException
    //   1170: astore_1
    //   1171: new java/lang/StringBuilder
    //   1174: astore_2
    //   1175: aload_2
    //   1176: invokespecial <init> : ()V
    //   1179: aload_2
    //   1180: ldc_w 'Unable to add window -- app for token '
    //   1183: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1186: pop
    //   1187: aload_2
    //   1188: aload #5
    //   1190: getfield token : Landroid/os/IBinder;
    //   1193: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1196: pop
    //   1197: aload_2
    //   1198: ldc_w ' is exiting'
    //   1201: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1204: pop
    //   1205: aload_1
    //   1206: aload_2
    //   1207: invokevirtual toString : ()Ljava/lang/String;
    //   1210: invokespecial <init> : (Ljava/lang/String;)V
    //   1213: aload_1
    //   1214: athrow
    //   1215: new android/view/WindowManager$BadTokenException
    //   1218: astore_1
    //   1219: new java/lang/StringBuilder
    //   1222: astore_2
    //   1223: aload_2
    //   1224: invokespecial <init> : ()V
    //   1227: aload_2
    //   1228: ldc_w 'Unable to add window -- window '
    //   1231: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1234: pop
    //   1235: aload_2
    //   1236: aload_0
    //   1237: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   1240: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1243: pop
    //   1244: aload_2
    //   1245: ldc_w ' has already been added'
    //   1248: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1251: pop
    //   1252: aload_1
    //   1253: aload_2
    //   1254: invokevirtual toString : ()Ljava/lang/String;
    //   1257: invokespecial <init> : (Ljava/lang/String;)V
    //   1260: aload_1
    //   1261: athrow
    //   1262: aload_0
    //   1263: monitorexit
    //   1264: return
    //   1265: new android/view/WindowManager$BadTokenException
    //   1268: astore_1
    //   1269: new java/lang/StringBuilder
    //   1272: astore_2
    //   1273: aload_2
    //   1274: invokespecial <init> : ()V
    //   1277: aload_2
    //   1278: ldc_w 'Unable to add window '
    //   1281: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1284: pop
    //   1285: aload_2
    //   1286: aload_0
    //   1287: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   1290: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1293: pop
    //   1294: aload_2
    //   1295: ldc_w ' -- another window of type '
    //   1298: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1301: pop
    //   1302: aload_2
    //   1303: aload_0
    //   1304: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   1307: getfield type : I
    //   1310: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1313: pop
    //   1314: aload_2
    //   1315: ldc_w ' already exists'
    //   1318: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1321: pop
    //   1322: aload_1
    //   1323: aload_2
    //   1324: invokevirtual toString : ()Ljava/lang/String;
    //   1327: invokespecial <init> : (Ljava/lang/String;)V
    //   1330: aload_1
    //   1331: athrow
    //   1332: new android/view/WindowManager$BadTokenException
    //   1335: astore_2
    //   1336: new java/lang/StringBuilder
    //   1339: astore_1
    //   1340: aload_1
    //   1341: invokespecial <init> : ()V
    //   1344: aload_1
    //   1345: ldc_w 'Unable to add window '
    //   1348: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1351: pop
    //   1352: aload_1
    //   1353: aload_0
    //   1354: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   1357: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1360: pop
    //   1361: aload_1
    //   1362: ldc_w ' -- permission denied for window type '
    //   1365: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1368: pop
    //   1369: aload_1
    //   1370: aload_0
    //   1371: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   1374: getfield type : I
    //   1377: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1380: pop
    //   1381: aload_2
    //   1382: aload_1
    //   1383: invokevirtual toString : ()Ljava/lang/String;
    //   1386: invokespecial <init> : (Ljava/lang/String;)V
    //   1389: aload_2
    //   1390: athrow
    //   1391: new android/view/WindowManager$InvalidDisplayException
    //   1394: astore_1
    //   1395: new java/lang/StringBuilder
    //   1398: astore_2
    //   1399: aload_2
    //   1400: invokespecial <init> : ()V
    //   1403: aload_2
    //   1404: ldc_w 'Unable to add window '
    //   1407: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1410: pop
    //   1411: aload_2
    //   1412: aload_0
    //   1413: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   1416: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1419: pop
    //   1420: aload_2
    //   1421: ldc_w ' -- the specified display can not be found'
    //   1424: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1427: pop
    //   1428: aload_1
    //   1429: aload_2
    //   1430: invokevirtual toString : ()Ljava/lang/String;
    //   1433: invokespecial <init> : (Ljava/lang/String;)V
    //   1436: aload_1
    //   1437: athrow
    //   1438: new android/view/WindowManager$InvalidDisplayException
    //   1441: astore_1
    //   1442: new java/lang/StringBuilder
    //   1445: astore_2
    //   1446: aload_2
    //   1447: invokespecial <init> : ()V
    //   1450: aload_2
    //   1451: ldc_w 'Unable to add window '
    //   1454: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1457: pop
    //   1458: aload_2
    //   1459: aload_0
    //   1460: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   1463: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1466: pop
    //   1467: aload_2
    //   1468: ldc_w ' -- the specified window type '
    //   1471: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1474: pop
    //   1475: aload_2
    //   1476: aload_0
    //   1477: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   1480: getfield type : I
    //   1483: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1486: pop
    //   1487: aload_2
    //   1488: ldc_w ' is not valid'
    //   1491: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1494: pop
    //   1495: aload_1
    //   1496: aload_2
    //   1497: invokevirtual toString : ()Ljava/lang/String;
    //   1500: invokespecial <init> : (Ljava/lang/String;)V
    //   1503: aload_1
    //   1504: athrow
    //   1505: new android/view/WindowManager$BadTokenException
    //   1508: astore_2
    //   1509: new java/lang/StringBuilder
    //   1512: astore_1
    //   1513: aload_1
    //   1514: invokespecial <init> : ()V
    //   1517: aload_1
    //   1518: ldc_w 'Unable to add Window '
    //   1521: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1524: pop
    //   1525: aload_1
    //   1526: aload_0
    //   1527: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   1530: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1533: pop
    //   1534: aload_1
    //   1535: ldc_w ' -- requested userId is not valid'
    //   1538: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1541: pop
    //   1542: aload_2
    //   1543: aload_1
    //   1544: invokevirtual toString : ()Ljava/lang/String;
    //   1547: invokespecial <init> : (Ljava/lang/String;)V
    //   1550: aload_2
    //   1551: athrow
    //   1552: new java/lang/StringBuilder
    //   1555: astore_2
    //   1556: aload_2
    //   1557: invokespecial <init> : ()V
    //   1560: aload_2
    //   1561: ldc_w 'Unable to add window -- unknown error code '
    //   1564: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1567: pop
    //   1568: aload_2
    //   1569: iload #4
    //   1571: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1574: pop
    //   1575: aload_1
    //   1576: aload_2
    //   1577: invokevirtual toString : ()Ljava/lang/String;
    //   1580: invokespecial <init> : (Ljava/lang/String;)V
    //   1583: aload_1
    //   1584: athrow
    //   1585: iload #4
    //   1587: bipush #8
    //   1589: iand
    //   1590: ifeq -> 1598
    //   1593: aload_0
    //   1594: iconst_1
    //   1595: putfield mUseBLASTAdapter : Z
    //   1598: iload #4
    //   1600: iconst_4
    //   1601: iand
    //   1602: ifeq -> 1610
    //   1605: aload_0
    //   1606: iconst_1
    //   1607: putfield mEnableTripleBuffering : Z
    //   1610: aload_1
    //   1611: instanceof com/android/internal/view/RootViewSurfaceTaker
    //   1614: ifeq -> 1632
    //   1617: aload_1
    //   1618: checkcast com/android/internal/view/RootViewSurfaceTaker
    //   1621: astore_3
    //   1622: aload_0
    //   1623: aload_3
    //   1624: invokeinterface willYouTakeTheInputQueue : ()Landroid/view/InputQueue$Callback;
    //   1629: putfield mInputQueueCallback : Landroid/view/InputQueue$Callback;
    //   1632: aload_2
    //   1633: ifnull -> 1684
    //   1636: aload_0
    //   1637: getfield mInputQueueCallback : Landroid/view/InputQueue$Callback;
    //   1640: ifnull -> 1666
    //   1643: new android/view/InputQueue
    //   1646: astore_3
    //   1647: aload_3
    //   1648: invokespecial <init> : ()V
    //   1651: aload_0
    //   1652: aload_3
    //   1653: putfield mInputQueue : Landroid/view/InputQueue;
    //   1656: aload_0
    //   1657: getfield mInputQueueCallback : Landroid/view/InputQueue$Callback;
    //   1660: aload_3
    //   1661: invokeinterface onInputQueueCreated : (Landroid/view/InputQueue;)V
    //   1666: new android/view/ViewRootImpl$WindowInputEventReceiver
    //   1669: astore_3
    //   1670: aload_3
    //   1671: aload_0
    //   1672: aload_2
    //   1673: invokestatic myLooper : ()Landroid/os/Looper;
    //   1676: invokespecial <init> : (Landroid/view/ViewRootImpl;Landroid/view/InputChannel;Landroid/os/Looper;)V
    //   1679: aload_0
    //   1680: aload_3
    //   1681: putfield mInputEventReceiver : Landroid/view/ViewRootImpl$WindowInputEventReceiver;
    //   1684: aload_1
    //   1685: aload_0
    //   1686: invokevirtual assignParent : (Landroid/view/ViewParent;)V
    //   1689: iload #4
    //   1691: iconst_1
    //   1692: iand
    //   1693: ifeq -> 1702
    //   1696: iconst_1
    //   1697: istore #7
    //   1699: goto -> 1705
    //   1702: iconst_0
    //   1703: istore #7
    //   1705: aload_0
    //   1706: iload #7
    //   1708: putfield mAddedTouchMode : Z
    //   1711: iload #4
    //   1713: iconst_2
    //   1714: iand
    //   1715: ifeq -> 1724
    //   1718: iconst_1
    //   1719: istore #7
    //   1721: goto -> 1727
    //   1724: iconst_0
    //   1725: istore #7
    //   1727: aload_0
    //   1728: iload #7
    //   1730: putfield mAppVisible : Z
    //   1733: aload_0
    //   1734: getfield mAccessibilityManager : Landroid/view/accessibility/AccessibilityManager;
    //   1737: invokevirtual isEnabled : ()Z
    //   1740: ifeq -> 1750
    //   1743: aload_0
    //   1744: getfield mAccessibilityInteractionConnectionManager : Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;
    //   1747: invokevirtual ensureConnection : ()V
    //   1750: aload_1
    //   1751: invokevirtual getImportantForAccessibility : ()I
    //   1754: ifne -> 1762
    //   1757: aload_1
    //   1758: iconst_1
    //   1759: invokevirtual setImportantForAccessibility : (I)V
    //   1762: aload #5
    //   1764: invokevirtual getTitle : ()Ljava/lang/CharSequence;
    //   1767: astore_1
    //   1768: new android/view/ViewRootImpl$SyntheticInputStage
    //   1771: astore_3
    //   1772: aload_3
    //   1773: aload_0
    //   1774: invokespecial <init> : (Landroid/view/ViewRootImpl;)V
    //   1777: aload_0
    //   1778: aload_3
    //   1779: putfield mSyntheticInputStage : Landroid/view/ViewRootImpl$InputStage;
    //   1782: new android/view/ViewRootImpl$ViewPostImeInputStage
    //   1785: astore_2
    //   1786: aload_2
    //   1787: aload_0
    //   1788: aload_3
    //   1789: invokespecial <init> : (Landroid/view/ViewRootImpl;Landroid/view/ViewRootImpl$InputStage;)V
    //   1792: new android/view/ViewRootImpl$NativePostImeInputStage
    //   1795: astore_3
    //   1796: new java/lang/StringBuilder
    //   1799: astore #5
    //   1801: aload #5
    //   1803: invokespecial <init> : ()V
    //   1806: aload #5
    //   1808: ldc_w 'aq:native-post-ime:'
    //   1811: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1814: pop
    //   1815: aload #5
    //   1817: aload_1
    //   1818: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1821: pop
    //   1822: aload_3
    //   1823: aload_0
    //   1824: aload_2
    //   1825: aload #5
    //   1827: invokevirtual toString : ()Ljava/lang/String;
    //   1830: invokespecial <init> : (Landroid/view/ViewRootImpl;Landroid/view/ViewRootImpl$InputStage;Ljava/lang/String;)V
    //   1833: new android/view/ViewRootImpl$EarlyPostImeInputStage
    //   1836: astore_2
    //   1837: aload_2
    //   1838: aload_0
    //   1839: aload_3
    //   1840: invokespecial <init> : (Landroid/view/ViewRootImpl;Landroid/view/ViewRootImpl$InputStage;)V
    //   1843: new android/view/ViewRootImpl$ImeInputStage
    //   1846: astore #5
    //   1848: new java/lang/StringBuilder
    //   1851: astore_3
    //   1852: aload_3
    //   1853: invokespecial <init> : ()V
    //   1856: aload_3
    //   1857: ldc_w 'aq:ime:'
    //   1860: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1863: pop
    //   1864: aload_3
    //   1865: aload_1
    //   1866: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1869: pop
    //   1870: aload #5
    //   1872: aload_0
    //   1873: aload_2
    //   1874: aload_3
    //   1875: invokevirtual toString : ()Ljava/lang/String;
    //   1878: invokespecial <init> : (Landroid/view/ViewRootImpl;Landroid/view/ViewRootImpl$InputStage;Ljava/lang/String;)V
    //   1881: new android/view/ViewRootImpl$ViewPreImeInputStage
    //   1884: astore_3
    //   1885: aload_3
    //   1886: aload_0
    //   1887: aload #5
    //   1889: invokespecial <init> : (Landroid/view/ViewRootImpl;Landroid/view/ViewRootImpl$InputStage;)V
    //   1892: new android/view/ViewRootImpl$NativePreImeInputStage
    //   1895: astore #5
    //   1897: new java/lang/StringBuilder
    //   1900: astore #6
    //   1902: aload #6
    //   1904: invokespecial <init> : ()V
    //   1907: aload #6
    //   1909: ldc_w 'aq:native-pre-ime:'
    //   1912: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1915: pop
    //   1916: aload #6
    //   1918: aload_1
    //   1919: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1922: pop
    //   1923: aload #5
    //   1925: aload_0
    //   1926: aload_3
    //   1927: aload #6
    //   1929: invokevirtual toString : ()Ljava/lang/String;
    //   1932: invokespecial <init> : (Landroid/view/ViewRootImpl;Landroid/view/ViewRootImpl$InputStage;Ljava/lang/String;)V
    //   1935: aload_0
    //   1936: aload #5
    //   1938: putfield mFirstInputStage : Landroid/view/ViewRootImpl$InputStage;
    //   1941: aload_0
    //   1942: aload_2
    //   1943: putfield mFirstPostImeInputStage : Landroid/view/ViewRootImpl$InputStage;
    //   1946: new java/lang/StringBuilder
    //   1949: astore_2
    //   1950: aload_2
    //   1951: invokespecial <init> : ()V
    //   1954: aload_2
    //   1955: ldc_w 'aq:pending:'
    //   1958: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1961: pop
    //   1962: aload_2
    //   1963: aload_1
    //   1964: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1967: pop
    //   1968: aload_0
    //   1969: aload_2
    //   1970: invokevirtual toString : ()Ljava/lang/String;
    //   1973: putfield mPendingInputEventQueueLengthCounterName : Ljava/lang/String;
    //   1976: aload_0
    //   1977: getfield mView : Landroid/view/View;
    //   1980: instanceof com/android/internal/view/RootViewSurfaceTaker
    //   1983: ifeq -> 2167
    //   1986: aload_0
    //   1987: getfield mView : Landroid/view/View;
    //   1990: checkcast com/android/internal/view/RootViewSurfaceTaker
    //   1993: astore_1
    //   1994: aload_1
    //   1995: invokeinterface providePendingInsetsController : ()Landroid/view/PendingInsetsController;
    //   2000: astore_1
    //   2001: aload_1
    //   2002: ifnull -> 2167
    //   2005: aload_1
    //   2006: aload_0
    //   2007: getfield mInsetsController : Landroid/view/InsetsController;
    //   2010: invokevirtual replayAndAttach : (Landroid/view/InsetsController;)V
    //   2013: goto -> 2167
    //   2016: astore_1
    //   2017: goto -> 2171
    //   2020: astore_1
    //   2021: goto -> 2151
    //   2024: astore_1
    //   2025: goto -> 2049
    //   2028: astore_1
    //   2029: goto -> 2037
    //   2032: astore_1
    //   2033: goto -> 2041
    //   2036: astore_1
    //   2037: goto -> 2151
    //   2040: astore_1
    //   2041: goto -> 2049
    //   2044: astore_1
    //   2045: goto -> 2151
    //   2048: astore_1
    //   2049: aload_0
    //   2050: iconst_0
    //   2051: putfield mAdded : Z
    //   2054: aload_0
    //   2055: aconst_null
    //   2056: putfield mView : Landroid/view/View;
    //   2059: aload_0
    //   2060: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   2063: aconst_null
    //   2064: putfield mRootView : Landroid/view/View;
    //   2067: aload_0
    //   2068: getfield mFallbackEventHandler : Landroid/view/FallbackEventHandler;
    //   2071: aconst_null
    //   2072: invokeinterface setView : (Landroid/view/View;)V
    //   2077: aload_0
    //   2078: invokevirtual unscheduleTraversals : ()V
    //   2081: aload_0
    //   2082: aconst_null
    //   2083: aconst_null
    //   2084: invokevirtual setAccessibilityFocus : (Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    //   2087: aload_0
    //   2088: getfield mAccessibilityInteractionConnectionManager : Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;
    //   2091: invokevirtual ensureNoConnection : ()V
    //   2094: aload_0
    //   2095: getfield mAccessibilityManager : Landroid/view/accessibility/AccessibilityManager;
    //   2098: aload_0
    //   2099: getfield mAccessibilityInteractionConnectionManager : Landroid/view/ViewRootImpl$AccessibilityInteractionConnectionManager;
    //   2102: invokevirtual removeAccessibilityStateChangeListener : (Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z
    //   2105: pop
    //   2106: aload_0
    //   2107: getfield mAccessibilityManager : Landroid/view/accessibility/AccessibilityManager;
    //   2110: aload_0
    //   2111: getfield mHighContrastTextManager : Landroid/view/ViewRootImpl$HighContrastTextManager;
    //   2114: invokevirtual removeHighTextContrastStateChangeListener : (Landroid/view/accessibility/AccessibilityManager$HighTextContrastChangeListener;)V
    //   2117: aload_0
    //   2118: invokespecial removeSendWindowContentChangedCallback : ()V
    //   2121: aload_0
    //   2122: getfield mDisplayManager : Landroid/hardware/display/DisplayManager;
    //   2125: aload_0
    //   2126: getfield mDisplayListener : Landroid/hardware/display/DisplayManager$DisplayListener;
    //   2129: invokevirtual unregisterDisplayListener : (Landroid/hardware/display/DisplayManager$DisplayListener;)V
    //   2132: new java/lang/RuntimeException
    //   2135: astore_2
    //   2136: aload_2
    //   2137: ldc_w 'Adding window failed'
    //   2140: aload_1
    //   2141: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   2144: aload_2
    //   2145: athrow
    //   2146: astore_1
    //   2147: goto -> 2151
    //   2150: astore_1
    //   2151: iload #8
    //   2153: ifeq -> 2161
    //   2156: aload #5
    //   2158: invokevirtual restore : ()V
    //   2161: aload_1
    //   2162: athrow
    //   2163: astore_1
    //   2164: goto -> 2171
    //   2167: aload_0
    //   2168: monitorexit
    //   2169: return
    //   2170: astore_1
    //   2171: aload_0
    //   2172: monitorexit
    //   2173: aload_1
    //   2174: athrow
    //   2175: astore_1
    //   2176: goto -> 2171
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1058	-> 0
    //   #1059	-> 2
    //   #1060	-> 9
    //   #1063	-> 14
    //   #1068	-> 22
    //   #1071	-> 47
    //   #1072	-> 61
    //   #1074	-> 76
    //   #1075	-> 87
    //   #1076	-> 97
    //   #1077	-> 110
    //   #1078	-> 120
    //   #1080	-> 131
    //   #1083	-> 148
    //   #1084	-> 154
    //   #1092	-> 158
    //   #1094	-> 167
    //   #1096	-> 173
    //   #1097	-> 180
    //   #1098	-> 185
    //   #1099	-> 197
    //   #1100	-> 201
    //   #1101	-> 215
    //   #1102	-> 220
    //   #1108	-> 231
    //   #1109	-> 239
    //   #1112	-> 247
    //   #1113	-> 252
    //   #1114	-> 261
    //   #1117	-> 270
    //   #1120	-> 277
    //   #1121	-> 283
    //   #1123	-> 302
    //   #1126	-> 311
    //   #1127	-> 315
    //   #1131	-> 321
    //   #1132	-> 321
    //   #1133	-> 328
    //   #1134	-> 339
    //   #1135	-> 339
    //   #1136	-> 344
    //   #1132	-> 359
    //   #1138	-> 362
    //   #1140	-> 410
    //   #1141	-> 418
    //   #1142	-> 432
    //   #1145	-> 437
    //   #1146	-> 446
    //   #1147	-> 451
    //   #1148	-> 459
    //   #1149	-> 486
    //   #1150	-> 491
    //   #1151	-> 519
    //   #1152	-> 523
    //   #1153	-> 528
    //   #1155	-> 536
    //   #1161	-> 541
    //   #1162	-> 545
    //   #1163	-> 545
    //   #1165	-> 557
    //   #1163	-> 568
    //   #1167	-> 570
    //   #1171	-> 599
    //   #1174	-> 614
    //   #1175	-> 625
    //   #1176	-> 633
    //   #1177	-> 638
    //   #1178	-> 645
    //   #1179	-> 669
    //   #1178	-> 728
    //   #1183	-> 761
    //   #1204	-> 769
    //   #1205	-> 774
    //   #1209	-> 779
    //   #1210	-> 786
    //   #1212	-> 800
    //   #1213	-> 814
    //   #1215	-> 841
    //   #1216	-> 852
    //   #1217	-> 864
    //   #1218	-> 875
    //   #1219	-> 922
    //   #1220	-> 927
    //   #1221	-> 935
    //   #1222	-> 940
    //   #1223	-> 950
    //   #1224	-> 954
    //   #1227	-> 960
    //   #1228	-> 967
    //   #1230	-> 979
    //   #1232	-> 990
    //   #1233	-> 994
    //   #1235	-> 1005
    //   #1276	-> 1064
    //   #1238	-> 1071
    //   #1242	-> 1119
    //   #1246	-> 1167
    //   #1250	-> 1215
    //   #1256	-> 1262
    //   #1258	-> 1265
    //   #1262	-> 1332
    //   #1266	-> 1391
    //   #1269	-> 1438
    //   #1273	-> 1505
    //   #1276	-> 1552
    //   #1280	-> 1585
    //   #1281	-> 1593
    //   #1283	-> 1598
    //   #1284	-> 1605
    //   #1287	-> 1610
    //   #1288	-> 1617
    //   #1289	-> 1622
    //   #1291	-> 1632
    //   #1292	-> 1636
    //   #1293	-> 1643
    //   #1294	-> 1656
    //   #1296	-> 1666
    //   #1297	-> 1670
    //   #1300	-> 1684
    //   #1301	-> 1689
    //   #1302	-> 1711
    //   #1304	-> 1733
    //   #1305	-> 1743
    //   #1308	-> 1750
    //   #1309	-> 1757
    //   #1313	-> 1762
    //   #1314	-> 1768
    //   #1315	-> 1782
    //   #1316	-> 1792
    //   #1318	-> 1833
    //   #1319	-> 1843
    //   #1321	-> 1881
    //   #1322	-> 1892
    //   #1325	-> 1935
    //   #1326	-> 1941
    //   #1327	-> 1946
    //   #1329	-> 1976
    //   #1330	-> 1986
    //   #1331	-> 1994
    //   #1332	-> 2001
    //   #1333	-> 2005
    //   #1337	-> 2016
    //   #1204	-> 2020
    //   #1184	-> 2024
    //   #1204	-> 2028
    //   #1184	-> 2032
    //   #1204	-> 2036
    //   #1184	-> 2040
    //   #1204	-> 2044
    //   #1184	-> 2048
    //   #1185	-> 2049
    //   #1186	-> 2054
    //   #1187	-> 2059
    //   #1188	-> 2067
    //   #1189	-> 2067
    //   #1190	-> 2077
    //   #1191	-> 2081
    //   #1194	-> 2087
    //   #1195	-> 2094
    //   #1197	-> 2106
    //   #1199	-> 2117
    //   #1200	-> 2121
    //   #1202	-> 2132
    //   #1204	-> 2146
    //   #1205	-> 2156
    //   #1207	-> 2161
    //   #1337	-> 2163
    //   #1059	-> 2167
    //   #1337	-> 2167
    //   #1338	-> 2169
    //   #1337	-> 2170
    // Exception table:
    //   from	to	target	type
    //   2	9	2170	finally
    //   9	14	2170	finally
    //   14	22	2170	finally
    //   22	47	2170	finally
    //   47	61	2170	finally
    //   61	76	2170	finally
    //   76	87	2170	finally
    //   87	97	2170	finally
    //   97	103	2170	finally
    //   103	110	2163	finally
    //   110	120	2163	finally
    //   120	131	2163	finally
    //   131	148	2163	finally
    //   148	154	2163	finally
    //   154	158	2175	finally
    //   158	167	2175	finally
    //   167	173	2175	finally
    //   173	180	2175	finally
    //   180	185	2175	finally
    //   185	197	2175	finally
    //   201	215	2175	finally
    //   215	220	2175	finally
    //   220	231	2175	finally
    //   231	239	2175	finally
    //   239	247	2175	finally
    //   247	252	2175	finally
    //   252	261	2175	finally
    //   261	270	2175	finally
    //   270	277	2175	finally
    //   277	283	2175	finally
    //   283	293	2175	finally
    //   302	311	2175	finally
    //   311	315	2175	finally
    //   315	321	2175	finally
    //   321	328	2175	finally
    //   328	339	2175	finally
    //   339	344	2175	finally
    //   344	353	2175	finally
    //   362	410	2175	finally
    //   410	418	2175	finally
    //   418	432	2175	finally
    //   432	437	2175	finally
    //   437	446	2175	finally
    //   446	451	2175	finally
    //   451	459	2175	finally
    //   459	471	2175	finally
    //   480	486	2175	finally
    //   486	491	2175	finally
    //   491	498	2175	finally
    //   504	513	2175	finally
    //   513	519	2175	finally
    //   523	528	2175	finally
    //   528	536	2175	finally
    //   536	541	2175	finally
    //   541	545	2175	finally
    //   545	557	2175	finally
    //   557	565	2175	finally
    //   570	584	2175	finally
    //   593	599	2175	finally
    //   599	614	2175	finally
    //   614	625	2048	android/os/RemoteException
    //   614	625	2044	finally
    //   625	633	2048	android/os/RemoteException
    //   625	633	2044	finally
    //   633	638	2048	android/os/RemoteException
    //   633	638	2044	finally
    //   638	645	2048	android/os/RemoteException
    //   638	645	2044	finally
    //   645	669	2048	android/os/RemoteException
    //   645	669	2044	finally
    //   669	717	2048	android/os/RemoteException
    //   669	717	2044	finally
    //   717	722	2040	android/os/RemoteException
    //   717	722	2036	finally
    //   722	728	2032	android/os/RemoteException
    //   722	728	2028	finally
    //   728	761	2024	android/os/RemoteException
    //   728	761	2020	finally
    //   761	769	2024	android/os/RemoteException
    //   761	769	2020	finally
    //   774	779	2016	finally
    //   779	786	2016	finally
    //   786	800	2016	finally
    //   800	814	2016	finally
    //   814	819	2016	finally
    //   835	841	2016	finally
    //   841	852	2016	finally
    //   852	864	2016	finally
    //   864	875	2016	finally
    //   875	922	2016	finally
    //   927	935	2016	finally
    //   935	940	2016	finally
    //   940	950	2016	finally
    //   950	954	2016	finally
    //   954	960	2016	finally
    //   960	967	2016	finally
    //   967	979	2016	finally
    //   979	990	2016	finally
    //   990	994	2016	finally
    //   994	1005	2016	finally
    //   1064	1068	2016	finally
    //   1071	1119	2016	finally
    //   1119	1167	2016	finally
    //   1167	1215	2016	finally
    //   1215	1262	2016	finally
    //   1262	1264	2016	finally
    //   1265	1332	2016	finally
    //   1332	1391	2016	finally
    //   1391	1438	2016	finally
    //   1438	1505	2016	finally
    //   1505	1552	2016	finally
    //   1552	1585	2016	finally
    //   1593	1598	2016	finally
    //   1605	1610	2016	finally
    //   1610	1617	2175	finally
    //   1617	1622	2175	finally
    //   1622	1632	2175	finally
    //   1636	1643	2175	finally
    //   1643	1656	2175	finally
    //   1656	1666	2175	finally
    //   1666	1670	2175	finally
    //   1670	1684	2175	finally
    //   1684	1689	2175	finally
    //   1705	1711	2175	finally
    //   1727	1733	2175	finally
    //   1733	1743	2175	finally
    //   1743	1750	2175	finally
    //   1750	1757	2175	finally
    //   1757	1762	2175	finally
    //   1762	1768	2175	finally
    //   1768	1782	2175	finally
    //   1782	1792	2175	finally
    //   1792	1833	2175	finally
    //   1833	1843	2175	finally
    //   1843	1881	2175	finally
    //   1881	1892	2175	finally
    //   1892	1935	2175	finally
    //   1935	1941	2175	finally
    //   1941	1946	2175	finally
    //   1946	1976	2175	finally
    //   1976	1986	2175	finally
    //   1986	1994	2175	finally
    //   1994	2001	2175	finally
    //   2005	2013	2175	finally
    //   2049	2054	2150	finally
    //   2054	2059	2150	finally
    //   2059	2067	2150	finally
    //   2067	2077	2146	finally
    //   2077	2081	2146	finally
    //   2081	2087	2146	finally
    //   2087	2094	2146	finally
    //   2094	2106	2146	finally
    //   2106	2117	2146	finally
    //   2117	2121	2146	finally
    //   2121	2132	2146	finally
    //   2132	2146	2146	finally
    //   2156	2161	2175	finally
    //   2161	2163	2175	finally
    //   2167	2169	2175	finally
    //   2171	2173	2175	finally
  }
  
  private void setTag() {
    String[] arrayOfString = this.mWindowAttributes.getTitle().toString().split("\\.");
    if (arrayOfString.length > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ViewRootImpl[");
      stringBuilder.append(arrayOfString[arrayOfString.length - 1]);
      stringBuilder.append("]");
      this.mTag = stringBuilder.toString();
    } 
  }
  
  public int getWindowFlags() {
    return this.mWindowAttributes.flags;
  }
  
  public int getDisplayId() {
    return this.mDisplay.getDisplayId();
  }
  
  public CharSequence getTitle() {
    return this.mWindowAttributes.getTitle();
  }
  
  public int getWidth() {
    return this.mWidth;
  }
  
  public int getHeight() {
    return this.mHeight;
  }
  
  void destroyHardwareResources() {
    synchronized (this.mDestroyLock) {
      ThreadedRenderer threadedRenderer = this.mAttachInfo.mThreadedRenderer;
      if (threadedRenderer != null) {
        _$$Lambda$dj1hfDQd0iEp_uBDBPEUMMYJJwk _$$Lambda$dj1hfDQd0iEp_uBDBPEUMMYJJwk;
        if (Looper.myLooper() != this.mAttachInfo.mHandler.getLooper()) {
          Handler handler = this.mAttachInfo.mHandler;
          _$$Lambda$dj1hfDQd0iEp_uBDBPEUMMYJJwk = new _$$Lambda$dj1hfDQd0iEp_uBDBPEUMMYJJwk();
          this(this);
          handler.postAtFrontOfQueue(_$$Lambda$dj1hfDQd0iEp_uBDBPEUMMYJJwk);
          return;
        } 
        _$$Lambda$dj1hfDQd0iEp_uBDBPEUMMYJJwk.destroyHardwareResources(this.mView);
        _$$Lambda$dj1hfDQd0iEp_uBDBPEUMMYJJwk.destroy();
      } 
      return;
    } 
  }
  
  public void detachFunctor(long paramLong) {
    if (this.mAttachInfo.mThreadedRenderer != null)
      this.mAttachInfo.mThreadedRenderer.stopDrawing(); 
  }
  
  public static void invokeFunctor(long paramLong, boolean paramBoolean) {
    ThreadedRenderer.invokeFunctor(paramLong, paramBoolean);
  }
  
  public void registerAnimatingRenderNode(RenderNode paramRenderNode) {
    if (this.mAttachInfo.mThreadedRenderer != null) {
      this.mAttachInfo.mThreadedRenderer.registerAnimatingRenderNode(paramRenderNode);
    } else {
      if (this.mAttachInfo.mPendingAnimatingRenderNodes == null)
        this.mAttachInfo.mPendingAnimatingRenderNodes = new ArrayList<>(); 
      this.mAttachInfo.mPendingAnimatingRenderNodes.add(paramRenderNode);
    } 
  }
  
  public void registerVectorDrawableAnimator(NativeVectorDrawableAnimator paramNativeVectorDrawableAnimator) {
    if (this.mAttachInfo.mThreadedRenderer != null)
      this.mAttachInfo.mThreadedRenderer.registerVectorDrawableAnimator(paramNativeVectorDrawableAnimator); 
  }
  
  public void registerRtFrameCallback(HardwareRenderer.FrameDrawingCallback paramFrameDrawingCallback) {
    if (this.mAttachInfo.mThreadedRenderer != null)
      this.mAttachInfo.mThreadedRenderer.registerRtFrameCallback(new _$$Lambda$ViewRootImpl$IReiNMSbDakZSGbIZuL_ifaFWn8(paramFrameDrawingCallback)); 
  }
  
  private void enableHardwareAcceleration(WindowManager.LayoutParams paramLayoutParams) {
    boolean bool2;
    View.AttachInfo attachInfo = this.mAttachInfo;
    boolean bool1 = false;
    attachInfo.mHardwareAccelerated = false;
    this.mAttachInfo.mHardwareAccelerationRequested = false;
    if (this.mTranslator != null)
      return; 
    if ((paramLayoutParams.flags & 0x1000000) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (bool2) {
      boolean bool;
      if (!ThreadedRenderer.isAvailable())
        return; 
      if ((paramLayoutParams.privateFlags & 0x1) != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if ((paramLayoutParams.privateFlags & 0x2) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool2) {
        this.mAttachInfo.mHardwareAccelerationRequested = true;
      } else if (!ThreadedRenderer.sRendererDisabled || (ThreadedRenderer.sSystemRendererDisabled && bool)) {
        boolean bool3;
        if (this.mAttachInfo.mThreadedRenderer != null)
          this.mAttachInfo.mThreadedRenderer.destroy(); 
        Rect rect = paramLayoutParams.surfaceInsets;
        if (rect.left != 0 || rect.right != 0 || rect.top != 0 || rect.bottom != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        if (paramLayoutParams.format != -1 || bool2) {
          bool3 = true;
        } else {
          bool3 = false;
        } 
        Context context1 = this.mContext;
        if (context1.getResources().getConfiguration().isScreenWideColorGamut() && paramLayoutParams.getColorMode() == 1)
          bool1 = true; 
        View.AttachInfo attachInfo1 = this.mAttachInfo;
        Context context2 = this.mContext;
        String str = paramLayoutParams.getTitle().toString();
        attachInfo1.mThreadedRenderer = ThreadedRenderer.create(context2, bool3, str);
        this.mAttachInfo.mThreadedRenderer.setWideGamut(bool1);
        updateForceDarkMode();
        if (this.mAttachInfo.mThreadedRenderer != null) {
          View.AttachInfo attachInfo2 = this.mAttachInfo;
          attachInfo2.mHardwareAccelerationRequested = true;
          attachInfo2.mHardwareAccelerated = true;
        } 
      } 
    } 
  }
  
  private int getNightMode() {
    return (this.mContext.getResources().getConfiguration()).uiMode & 0x30;
  }
  
  private void updateForceDarkMode() {
    boolean bool1;
    if (this.mAttachInfo.mThreadedRenderer == null)
      return; 
    int i = getNightMode();
    boolean bool = true;
    if (i == 32) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    this.mManager.logConfigurationNightError(this.mContext, bool1);
    boolean bool2 = bool1;
    if (bool1) {
      bool2 = SystemProperties.getBoolean("debug.hwui.force_dark", false);
      TypedArray typedArray = this.mContext.obtainStyledAttributes(R.styleable.Theme);
      if (typedArray.getBoolean(279, true) && typedArray.getBoolean(278, bool2)) {
        bool1 = bool;
      } else {
        bool1 = false;
      } 
      typedArray.recycle();
      this.mManager.logForceDarkAllowedStatus(this.mContext, bool2);
      bool2 = bool1;
    } 
    this.mManager.forceDarkWithoutTheme(this.mContext, this.mView, bool2);
    if (this.mAttachInfo.mThreadedRenderer.setForceDark(bool2))
      invalidateWorld(this.mView); 
  }
  
  public View getView() {
    return this.mView;
  }
  
  final WindowLeaked getLocation() {
    return this.mLocation;
  }
  
  void setLayoutParams(WindowManager.LayoutParams paramLayoutParams, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   6: getfield surfaceInsets : Landroid/graphics/Rect;
    //   9: getfield left : I
    //   12: istore_3
    //   13: aload_0
    //   14: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   17: getfield surfaceInsets : Landroid/graphics/Rect;
    //   20: getfield top : I
    //   23: istore #4
    //   25: aload_0
    //   26: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   29: getfield surfaceInsets : Landroid/graphics/Rect;
    //   32: getfield right : I
    //   35: istore #5
    //   37: aload_0
    //   38: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   41: getfield surfaceInsets : Landroid/graphics/Rect;
    //   44: getfield bottom : I
    //   47: istore #6
    //   49: aload_0
    //   50: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   53: getfield softInputMode : I
    //   56: istore #7
    //   58: aload_0
    //   59: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   62: getfield hasManualSurfaceInsets : Z
    //   65: istore #8
    //   67: aload_0
    //   68: aload_1
    //   69: getfield flags : I
    //   72: putfield mClientWindowLayoutFlags : I
    //   75: aload_0
    //   76: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   79: getfield privateFlags : I
    //   82: istore #9
    //   84: aload_1
    //   85: aload_0
    //   86: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   89: getfield systemUiVisibility : I
    //   92: putfield systemUiVisibility : I
    //   95: aload_1
    //   96: aload_0
    //   97: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   100: getfield subtreeSystemUiVisibility : I
    //   103: putfield subtreeSystemUiVisibility : I
    //   106: aload_0
    //   107: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   110: aload_1
    //   111: invokevirtual copyFrom : (Landroid/view/WindowManager$LayoutParams;)I
    //   114: istore #10
    //   116: ldc_w 524288
    //   119: iload #10
    //   121: iand
    //   122: ifeq -> 133
    //   125: aload_0
    //   126: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   129: iconst_1
    //   130: putfield mRecomputeGlobalAttributes : Z
    //   133: iload #10
    //   135: iconst_1
    //   136: iand
    //   137: ifeq -> 148
    //   140: aload_0
    //   141: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   144: iconst_1
    //   145: putfield mNeedsUpdateLightCenter : Z
    //   148: aload_0
    //   149: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   152: getfield packageName : Ljava/lang/String;
    //   155: ifnonnull -> 169
    //   158: aload_0
    //   159: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   162: aload_0
    //   163: getfield mBasePackageName : Ljava/lang/String;
    //   166: putfield packageName : Ljava/lang/String;
    //   169: aload_0
    //   170: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   173: astore #11
    //   175: aload #11
    //   177: aload #11
    //   179: getfield privateFlags : I
    //   182: iload #9
    //   184: sipush #128
    //   187: iand
    //   188: ior
    //   189: putfield privateFlags : I
    //   192: aload_0
    //   193: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   196: astore #11
    //   198: aload #11
    //   200: aload #11
    //   202: getfield privateFlags : I
    //   205: ldc_w 33554432
    //   208: ior
    //   209: putfield privateFlags : I
    //   212: aload_0
    //   213: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   216: getfield preservePreviousSurfaceInsets : Z
    //   219: ifeq -> 251
    //   222: aload_0
    //   223: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   226: getfield surfaceInsets : Landroid/graphics/Rect;
    //   229: iload_3
    //   230: iload #4
    //   232: iload #5
    //   234: iload #6
    //   236: invokevirtual set : (IIII)V
    //   239: aload_0
    //   240: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   243: iload #8
    //   245: putfield hasManualSurfaceInsets : Z
    //   248: goto -> 315
    //   251: aload_0
    //   252: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   255: getfield surfaceInsets : Landroid/graphics/Rect;
    //   258: getfield left : I
    //   261: iload_3
    //   262: if_icmpne -> 310
    //   265: aload_0
    //   266: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   269: getfield surfaceInsets : Landroid/graphics/Rect;
    //   272: getfield top : I
    //   275: iload #4
    //   277: if_icmpne -> 310
    //   280: aload_0
    //   281: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   284: getfield surfaceInsets : Landroid/graphics/Rect;
    //   287: getfield right : I
    //   290: iload #5
    //   292: if_icmpne -> 310
    //   295: aload_0
    //   296: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   299: getfield surfaceInsets : Landroid/graphics/Rect;
    //   302: getfield bottom : I
    //   305: iload #6
    //   307: if_icmpeq -> 315
    //   310: aload_0
    //   311: iconst_1
    //   312: putfield mNeedsRendererSetup : Z
    //   315: aload_0
    //   316: aload_0
    //   317: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   320: invokespecial applyKeepScreenOnFlag : (Landroid/view/WindowManager$LayoutParams;)V
    //   323: iload_2
    //   324: ifeq -> 339
    //   327: aload_0
    //   328: aload_1
    //   329: getfield softInputMode : I
    //   332: putfield mSoftInputMode : I
    //   335: aload_0
    //   336: invokevirtual requestLayout : ()V
    //   339: aload_1
    //   340: getfield softInputMode : I
    //   343: sipush #240
    //   346: iand
    //   347: ifne -> 375
    //   350: aload_0
    //   351: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   354: aload_0
    //   355: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   358: getfield softInputMode : I
    //   361: sipush #-241
    //   364: iand
    //   365: iload #7
    //   367: sipush #240
    //   370: iand
    //   371: ior
    //   372: putfield softInputMode : I
    //   375: iload #10
    //   377: sipush #512
    //   380: iand
    //   381: ifeq -> 388
    //   384: aload_0
    //   385: invokevirtual requestFitSystemWindows : ()V
    //   388: aload_0
    //   389: iconst_1
    //   390: putfield mWindowAttributesChanged : Z
    //   393: aload_0
    //   394: invokevirtual scheduleTraversals : ()V
    //   397: aload_0
    //   398: monitorexit
    //   399: return
    //   400: astore_1
    //   401: aload_0
    //   402: monitorexit
    //   403: aload_1
    //   404: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1581	-> 0
    //   #1582	-> 2
    //   #1583	-> 13
    //   #1584	-> 25
    //   #1585	-> 37
    //   #1586	-> 49
    //   #1587	-> 58
    //   #1596	-> 67
    //   #1599	-> 75
    //   #1603	-> 84
    //   #1604	-> 95
    //   #1606	-> 106
    //   #1607	-> 116
    //   #1609	-> 125
    //   #1611	-> 133
    //   #1613	-> 140
    //   #1615	-> 148
    //   #1616	-> 158
    //   #1618	-> 169
    //   #1620	-> 192
    //   #1623	-> 212
    //   #1625	-> 222
    //   #1627	-> 239
    //   #1628	-> 251
    //   #1632	-> 310
    //   #1635	-> 315
    //   #1637	-> 323
    //   #1638	-> 327
    //   #1639	-> 335
    //   #1643	-> 339
    //   #1645	-> 350
    //   #1650	-> 375
    //   #1651	-> 384
    //   #1654	-> 388
    //   #1655	-> 393
    //   #1656	-> 397
    //   #1657	-> 399
    //   #1656	-> 400
    // Exception table:
    //   from	to	target	type
    //   2	13	400	finally
    //   13	25	400	finally
    //   25	37	400	finally
    //   37	49	400	finally
    //   49	58	400	finally
    //   58	67	400	finally
    //   67	75	400	finally
    //   75	84	400	finally
    //   84	95	400	finally
    //   95	106	400	finally
    //   106	116	400	finally
    //   125	133	400	finally
    //   140	148	400	finally
    //   148	158	400	finally
    //   158	169	400	finally
    //   169	192	400	finally
    //   192	212	400	finally
    //   212	222	400	finally
    //   222	239	400	finally
    //   239	248	400	finally
    //   251	310	400	finally
    //   310	315	400	finally
    //   315	323	400	finally
    //   327	335	400	finally
    //   335	339	400	finally
    //   339	350	400	finally
    //   350	375	400	finally
    //   384	388	400	finally
    //   388	393	400	finally
    //   393	397	400	finally
    //   397	399	400	finally
    //   401	403	400	finally
  }
  
  void handleAppVisibility(boolean paramBoolean) {
    if (this.mAppVisible != paramBoolean) {
      this.mAppVisible = paramBoolean;
      this.mAppVisibilityChanged = true;
      scheduleTraversals();
      if (!this.mAppVisible)
        WindowManagerGlobal.trimForeground(); 
    } 
  }
  
  void handleGetNewSurface() {
    this.mNewSurfaceNeeded = true;
    this.mFullRedrawNeeded = true;
    scheduleTraversals();
  }
  
  public void onMovedToDisplay(int paramInt, Configuration paramConfiguration) {
    if (this.mDisplay.getDisplayId() == paramInt)
      return; 
    updateInternalDisplay(paramInt, this.mView.getResources());
    this.mImeFocusController.onMovedToDisplay();
    this.mAttachInfo.mDisplayState = this.mDisplay.getState();
    this.mView.dispatchMovedToDisplay(this.mDisplay, paramConfiguration);
  }
  
  private void updateInternalDisplay(int paramInt, Resources paramResources) {
    ResourcesManager resourcesManager;
    Display display = ResourcesManager.getInstance().getAdjustedDisplay(paramInt, paramResources);
    if (display == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Cannot get desired display with Id: ");
      stringBuilder.append(paramInt);
      Slog.w("ViewRootImpl", stringBuilder.toString());
      resourcesManager = ResourcesManager.getInstance();
      this.mDisplay = resourcesManager.getAdjustedDisplay(0, paramResources);
    } else {
      this.mDisplay = (Display)resourcesManager;
    } 
    this.mContext.updateDisplay(this.mDisplay.getDisplayId());
  }
  
  void pokeDrawLockIfNeeded() {
    int i = this.mAttachInfo.mDisplayState;
    if (this.mView != null && this.mAdded && this.mTraversalScheduled && i == 3)
      try {
        this.mWindowSession.pokeDrawLock((IBinder)this.mWindow);
      } catch (RemoteException remoteException) {} 
  }
  
  public void requestFitSystemWindows() {
    checkThread();
    this.mApplyInsetsRequested = true;
    scheduleTraversals();
  }
  
  void notifyInsetsChanged() {
    if (sNewInsetsMode == 0)
      return; 
    this.mApplyInsetsRequested = true;
    requestLayout();
    if (View.sForceLayoutWhenInsetsChanged) {
      View view = this.mView;
      if (view != null && !this.mDisableRelayout)
        forceLayout(view); 
    } 
    if (!this.mIsInTraversal)
      scheduleTraversals(); 
  }
  
  public void requestLayout() {
    if (!this.mHandlingLayoutInLayoutRequest) {
      checkThread();
      this.mLayoutRequested = true;
      scheduleTraversals();
    } 
  }
  
  public boolean isLayoutRequested() {
    return this.mLayoutRequested;
  }
  
  public void onDescendantInvalidated(View paramView1, View paramView2) {
    if ((paramView2.mPrivateFlags & 0x40) != 0)
      this.mIsAnimating = true; 
    invalidate();
  }
  
  void invalidate() {
    this.mDirty.set(0, 0, this.mWidth, this.mHeight);
    if (!this.mWillDrawSoon)
      scheduleTraversals(); 
  }
  
  void invalidateWorld(View paramView) {
    paramView.invalidate();
    if (paramView instanceof ViewGroup) {
      paramView = paramView;
      for (byte b = 0; b < paramView.getChildCount(); b++)
        invalidateWorld(paramView.getChildAt(b)); 
    } 
  }
  
  public void invalidateChild(View paramView, Rect paramRect) {
    invalidateChildInParent(null, paramRect);
  }
  
  public ViewParent invalidateChildInParent(int[] paramArrayOfint, Rect paramRect) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual checkThread : ()V
    //   4: getstatic android/view/ViewRootImpl.DEBUG_DRAW : Z
    //   7: ifeq -> 46
    //   10: aload_0
    //   11: getfield mTag : Ljava/lang/String;
    //   14: astore_3
    //   15: new java/lang/StringBuilder
    //   18: dup
    //   19: invokespecial <init> : ()V
    //   22: astore_1
    //   23: aload_1
    //   24: ldc_w 'Invalidate child: '
    //   27: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   30: pop
    //   31: aload_1
    //   32: aload_2
    //   33: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   36: pop
    //   37: aload_3
    //   38: aload_1
    //   39: invokevirtual toString : ()Ljava/lang/String;
    //   42: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   45: pop
    //   46: aload_2
    //   47: ifnonnull -> 56
    //   50: aload_0
    //   51: invokevirtual invalidate : ()V
    //   54: aconst_null
    //   55: areturn
    //   56: aload_2
    //   57: invokevirtual isEmpty : ()Z
    //   60: ifeq -> 72
    //   63: aload_0
    //   64: getfield mIsAnimating : Z
    //   67: ifne -> 72
    //   70: aconst_null
    //   71: areturn
    //   72: aload_0
    //   73: getfield mCurScrollY : I
    //   76: ifne -> 88
    //   79: aload_2
    //   80: astore_1
    //   81: aload_0
    //   82: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   85: ifnull -> 154
    //   88: aload_0
    //   89: getfield mTempRect : Landroid/graphics/Rect;
    //   92: aload_2
    //   93: invokevirtual set : (Landroid/graphics/Rect;)V
    //   96: aload_0
    //   97: getfield mTempRect : Landroid/graphics/Rect;
    //   100: astore_2
    //   101: aload_0
    //   102: getfield mCurScrollY : I
    //   105: istore #4
    //   107: iload #4
    //   109: ifeq -> 120
    //   112: aload_2
    //   113: iconst_0
    //   114: iload #4
    //   116: ineg
    //   117: invokevirtual offset : (II)V
    //   120: aload_0
    //   121: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   124: astore_1
    //   125: aload_1
    //   126: ifnull -> 134
    //   129: aload_1
    //   130: aload_2
    //   131: invokevirtual translateRectInAppWindowToScreen : (Landroid/graphics/Rect;)V
    //   134: aload_2
    //   135: astore_1
    //   136: aload_0
    //   137: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   140: getfield mScalingRequired : Z
    //   143: ifeq -> 154
    //   146: aload_2
    //   147: iconst_m1
    //   148: iconst_m1
    //   149: invokevirtual inset : (II)V
    //   152: aload_2
    //   153: astore_1
    //   154: aload_0
    //   155: aload_1
    //   156: invokespecial invalidateRectOnScreen : (Landroid/graphics/Rect;)V
    //   159: aconst_null
    //   160: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1855	-> 0
    //   #1856	-> 4
    //   #1858	-> 46
    //   #1859	-> 50
    //   #1860	-> 54
    //   #1861	-> 56
    //   #1862	-> 70
    //   #1865	-> 72
    //   #1866	-> 88
    //   #1867	-> 96
    //   #1868	-> 101
    //   #1869	-> 112
    //   #1871	-> 120
    //   #1872	-> 129
    //   #1874	-> 134
    //   #1875	-> 146
    //   #1879	-> 154
    //   #1881	-> 159
  }
  
  private void invalidateRectOnScreen(Rect paramRect) {
    Rect rect = this.mDirty;
    rect.union(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
    float f = this.mAttachInfo.mApplicationScale;
    boolean bool = rect.intersect(0, 0, (int)(this.mWidth * f + 0.5F), (int)(this.mHeight * f + 0.5F));
    if (!bool)
      rect.setEmpty(); 
    if (!this.mWillDrawSoon && (bool || this.mIsAnimating))
      scheduleTraversals(); 
  }
  
  public void setIsAmbientMode(boolean paramBoolean) {
    this.mIsAmbientMode = paramBoolean;
  }
  
  void setWindowStopped(boolean paramBoolean) {
    if (DEBUG_PANIC) {
      String str1 = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setWindowStopped, stopped:");
      stringBuilder.append(paramBoolean);
      stringBuilder.append(" mStopped ");
      stringBuilder.append(this.mStopped);
      stringBuilder.append(" this ");
      stringBuilder.append(this);
      stringBuilder.append(" ");
      stringBuilder.append(Debug.getCallers(6));
      String str2 = stringBuilder.toString();
      Log.d(str1, str2);
    } 
    checkThread();
    if (this.mStopped != paramBoolean) {
      this.mStopped = paramBoolean;
      ThreadedRenderer threadedRenderer = this.mAttachInfo.mThreadedRenderer;
      if (threadedRenderer != null) {
        if (DEBUG_DRAW) {
          String str = this.mTag;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("WindowStopped on ");
          stringBuilder.append(getTitle());
          stringBuilder.append(" set to ");
          stringBuilder.append(this.mStopped);
          Log.d(str, stringBuilder.toString());
        } 
        threadedRenderer.setStopped(this.mStopped);
      } 
      if (!this.mStopped) {
        this.mNewSurfaceNeeded = true;
        scheduleTraversals();
      } else {
        if (threadedRenderer != null)
          threadedRenderer.destroyHardwareResources(this.mView); 
        if (this.mSurface.isValid()) {
          if (this.mSurfaceHolder != null)
            notifyHolderSurfaceDestroyed(); 
          notifySurfaceDestroyed();
        } 
        destroySurface();
      } 
    } 
    scheduleConsumeBatchedInputImmediately();
  }
  
  void addSurfaceChangedCallback(SurfaceChangedCallback paramSurfaceChangedCallback) {
    this.mSurfaceChangedCallbacks.add(paramSurfaceChangedCallback);
  }
  
  void removeSurfaceChangedCallback(SurfaceChangedCallback paramSurfaceChangedCallback) {
    this.mSurfaceChangedCallbacks.remove(paramSurfaceChangedCallback);
  }
  
  private void notifySurfaceCreated() {
    for (byte b = 0; b < this.mSurfaceChangedCallbacks.size(); b++)
      ((SurfaceChangedCallback)this.mSurfaceChangedCallbacks.get(b)).surfaceCreated(this.mSurfaceChangedTransaction); 
  }
  
  private void notifySurfaceReplaced() {
    for (byte b = 0; b < this.mSurfaceChangedCallbacks.size(); b++)
      ((SurfaceChangedCallback)this.mSurfaceChangedCallbacks.get(b)).surfaceReplaced(this.mSurfaceChangedTransaction); 
  }
  
  private void notifySurfaceDestroyed() {
    for (byte b = 0; b < this.mSurfaceChangedCallbacks.size(); b++)
      ((SurfaceChangedCallback)this.mSurfaceChangedCallbacks.get(b)).surfaceDestroyed(); 
  }
  
  public SurfaceControl getBoundsLayer() {
    if (this.mBoundsLayer == null) {
      SurfaceControl.Builder builder = new SurfaceControl.Builder(this.mSurfaceSession);
      builder = builder.setContainerLayer();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Bounds for - ");
      stringBuilder.append(getTitle().toString());
      builder = builder.setName(stringBuilder.toString());
      builder = builder.setParent(getRenderSurfaceControl());
      builder = builder.setCallsite("ViewRootImpl.getBoundsLayer");
      this.mBoundsLayer = builder.build();
      setBoundsLayerCrop();
      this.mTransaction.show(this.mBoundsLayer).apply();
    } 
    return this.mBoundsLayer;
  }
  
  Surface getOrCreateBLASTSurface(int paramInt1, int paramInt2) {
    SurfaceControl surfaceControl = this.mSurfaceControl;
    if (surfaceControl != null && surfaceControl.isValid()) {
      surfaceControl = this.mBlastSurfaceControl;
      if (surfaceControl != null)
        if (surfaceControl.isValid()) {
          Surface surface;
          surfaceControl = null;
          BLASTBufferQueue bLASTBufferQueue = this.mBlastBufferQueue;
          if (bLASTBufferQueue == null) {
            BLASTBufferQueue bLASTBufferQueue1 = new BLASTBufferQueue(this.mBlastSurfaceControl, paramInt1, paramInt2, this.mEnableTripleBuffering);
            surface = bLASTBufferQueue1.getSurface();
          } else {
            bLASTBufferQueue.update(this.mBlastSurfaceControl, paramInt1, paramInt2);
          } 
          return surface;
        }  
    } 
    return null;
  }
  
  private void setBoundsLayerCrop() {
    this.mTempBoundsRect.set(this.mWinFrame);
    this.mTempBoundsRect.offsetTo(this.mWindowAttributes.surfaceInsets.left, this.mWindowAttributes.surfaceInsets.top);
    this.mTransaction.setWindowCrop(this.mBoundsLayer, this.mTempBoundsRect);
  }
  
  private void updateBoundsLayer() {
    if (this.mBoundsLayer != null) {
      setBoundsLayerCrop();
      SurfaceControl.Transaction transaction = this.mTransaction;
      SurfaceControl surfaceControl1 = this.mBoundsLayer;
      SurfaceControl surfaceControl2 = getRenderSurfaceControl();
      long l = this.mSurface.getNextFrameNumber();
      transaction = transaction.deferTransactionUntil(surfaceControl1, surfaceControl2, l);
      transaction.apply();
    } 
  }
  
  private void destroySurface() {
    SurfaceControl surfaceControl = this.mBoundsLayer;
    if (surfaceControl != null) {
      surfaceControl.release();
      this.mBoundsLayer = null;
    } 
    this.mSurface.release();
    this.mSurfaceControl.release();
    this.mBlastSurfaceControl.release();
    this.mBlastBufferQueue = null;
  }
  
  public void setPausedForTransition(boolean paramBoolean) {
    this.mPausedForTransition = paramBoolean;
  }
  
  public ViewParent getParent() {
    return null;
  }
  
  public boolean getChildVisibleRect(View paramView, Rect paramRect, Point paramPoint) {
    if (paramView == this.mView)
      return paramRect.intersect(0, 0, this.mWidth, this.mHeight); 
    throw new RuntimeException("child is not mine, honest!");
  }
  
  public void bringChildToFront(View paramView) {}
  
  int getHostVisibility() {
    if (this.mAppVisible || this.mForceDecorViewVisibility) {
      View view = this.mView;
      if (view != null)
        return view.getVisibility(); 
    } 
    return 8;
  }
  
  public void requestTransitionStart(LayoutTransition paramLayoutTransition) {
    ArrayList<LayoutTransition> arrayList = this.mPendingTransitions;
    if (arrayList == null || !arrayList.contains(paramLayoutTransition)) {
      if (this.mPendingTransitions == null)
        this.mPendingTransitions = new ArrayList<>(); 
      this.mPendingTransitions.add(paramLayoutTransition);
    } 
  }
  
  void notifyRendererOfFramePending() {
    if (this.mAttachInfo.mThreadedRenderer != null)
      this.mAttachInfo.mThreadedRenderer.notifyFramePending(); 
  }
  
  void scheduleTraversals() {
    Trace.traceBegin(8L, "scheduleTraversals");
    if (this.mFirst && !this.mFirstFrameScheduled && scheduleTraversalsImmediately()) {
      this.mFirstFrameScheduled = true;
    } else if (!this.mTraversalScheduled) {
      this.mTraversalScheduled = true;
      this.mTraversalBarrier = this.mHandler.getLooper().getQueue().postSyncBarrier();
      this.mChoreographer.postCallback(3, this.mTraversalRunnable, null);
      if (!this.mUnbufferedInputDispatch)
        scheduleConsumeBatchedInput(); 
      notifyRendererOfFramePending();
      pokeDrawLockIfNeeded();
    } 
    Trace.traceEnd(8L);
  }
  
  private boolean scheduleTraversalsImmediately() {
    if (!this.FRAME_ONT || !ActivityThread.sDoFrameOptEnabled)
      return false; 
    int i = this.doFrameIndex;
    if (i <= 1 && this.isOptApp) {
      if (i == 1 && ActivityThread.sDoFrameOptEnabled)
        ActivityThread.sDoFrameOptEnabled = false; 
      this.doFrameIndex++;
      Trace.traceBegin(8L, "scheduleTraversalsImmediately");
      this.mTraversalScheduled = true;
      try {
        this.mHandler.getLooper().getQueue().removeSyncBarrier(this.mTraversalBarrier);
      } catch (IllegalStateException illegalStateException) {
        Log.v("ViewRootImpl", "The specified message queue synchronization  barrier token has not been posted or has already been removed");
      } 
      this.mTraversalBarrier = this.mHandler.getLooper().getQueue().postSyncBarrier();
      this.mChoreographer.postCallbackImmediately(3, this.mTraversalRunnable, null, 0L);
      this.mChoreographer.doFrameImmediately();
      if (!this.mUnbufferedInputDispatch)
        scheduleConsumeBatchedInput(); 
      notifyRendererOfFramePending();
      pokeDrawLockIfNeeded();
      Trace.traceEnd(8L);
      return true;
    } 
    return false;
  }
  
  void unscheduleTraversals() {
    if (this.mTraversalScheduled) {
      this.mTraversalScheduled = false;
      try {
        this.mHandler.getLooper().getQueue().removeSyncBarrier(this.mTraversalBarrier);
      } catch (IllegalStateException illegalStateException) {
        Log.v("ViewRootImpl", "The specified message queue synchronization  barrier token has not been posted or has already been removed");
      } 
      this.mChoreographer.removeCallbacks(3, this.mTraversalRunnable, null);
    } 
  }
  
  void doTraversal() {
    if (this.mTraversalScheduled) {
      this.mTraversalScheduled = false;
      try {
        this.mHandler.getLooper().getQueue().removeSyncBarrier(this.mTraversalBarrier);
      } catch (IllegalStateException illegalStateException) {
        Log.v("ViewRootImpl", "The specified message queue synchronization  barrier token has not been posted or has already been removed");
      } 
      if (this.mProfile)
        Debug.startMethodTracing("ViewAncestor"); 
      performTraversals();
      if (this.mProfile) {
        Debug.stopMethodTracing();
        this.mProfile = false;
      } 
    } 
  }
  
  private void applyKeepScreenOnFlag(WindowManager.LayoutParams paramLayoutParams) {
    if (this.mAttachInfo.mKeepScreenOn) {
      paramLayoutParams.flags |= 0x80;
    } else {
      paramLayoutParams.flags = paramLayoutParams.flags & 0xFFFFFF7F | this.mClientWindowLayoutFlags & 0x80;
    } 
  }
  
  private boolean collectViewAttributes() {
    if (this.mAttachInfo.mRecomputeGlobalAttributes) {
      this.mAttachInfo.mRecomputeGlobalAttributes = false;
      boolean bool = this.mAttachInfo.mKeepScreenOn;
      this.mAttachInfo.mKeepScreenOn = false;
      this.mAttachInfo.mSystemUiVisibility = 0;
      this.mAttachInfo.mHasSystemUiListeners = false;
      this.mView.dispatchCollectViewAttributes(this.mAttachInfo, 0);
      View.AttachInfo attachInfo1 = this.mAttachInfo;
      attachInfo1.mSystemUiVisibility = this.mManager.changeSystemUiVisibility(attachInfo1.mSystemUiVisibility);
      attachInfo1 = this.mAttachInfo;
      attachInfo1.mSystemUiVisibility &= this.mAttachInfo.mDisabledSystemUiVisibility ^ 0xFFFFFFFF;
      WindowManager.LayoutParams layoutParams = this.mWindowAttributes;
      View.AttachInfo attachInfo2 = this.mAttachInfo;
      attachInfo2.mSystemUiVisibility |= getImpliedSystemUiVisibility(layoutParams);
      SystemUiVisibilityInfo systemUiVisibilityInfo = this.mCompatibleVisibilityInfo;
      systemUiVisibilityInfo.globalVisibility = systemUiVisibilityInfo.globalVisibility & 0xFFFFFFFE | this.mAttachInfo.mSystemUiVisibility & 0x1;
      if (this.mDispatchedSystemUiVisibility != this.mCompatibleVisibilityInfo.globalVisibility) {
        this.mHandler.removeMessages(17);
        ViewRootHandler viewRootHandler = this.mHandler;
        viewRootHandler.sendMessage(viewRootHandler.obtainMessage(17, this.mCompatibleVisibilityInfo));
      } 
      if (this.mAttachInfo.mKeepScreenOn != bool || this.mAttachInfo.mSystemUiVisibility != layoutParams.subtreeSystemUiVisibility || this.mAttachInfo.mHasSystemUiListeners != layoutParams.hasSystemUiListeners) {
        applyKeepScreenOnFlag(layoutParams);
        layoutParams.subtreeSystemUiVisibility = this.mAttachInfo.mSystemUiVisibility;
        layoutParams.hasSystemUiListeners = this.mAttachInfo.mHasSystemUiListeners;
        this.mView.dispatchWindowSystemUiVisiblityChanged(this.mAttachInfo.mSystemUiVisibility);
        return true;
      } 
    } 
    return false;
  }
  
  private int getImpliedSystemUiVisibility(WindowManager.LayoutParams paramLayoutParams) {
    int i = 0;
    if ((paramLayoutParams.flags & 0x4000000) != 0)
      i = Character.MIN_VALUE | 0x500; 
    int j = i;
    if ((paramLayoutParams.flags & 0x8000000) != 0)
      j = i | 0x300; 
    return j;
  }
  
  void updateCompatSysUiVisibility(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    boolean bool = true;
    if (paramInt == 0 || paramInt == 1) {
      int i = sNewInsetsMode;
      byte b = 2;
      if (i == 2) {
        SystemUiVisibilityInfo systemUiVisibilityInfo = this.mCompatibleVisibilityInfo;
        if (paramInt == 0) {
          paramInt = 4;
        } else {
          paramInt = b;
        } 
        if ((systemUiVisibilityInfo.globalVisibility & paramInt) != 0)
          bool = false; 
        if (paramBoolean1) {
          systemUiVisibilityInfo.globalVisibility &= paramInt ^ 0xFFFFFFFF;
          if (!bool && paramBoolean2)
            systemUiVisibilityInfo.localChanges |= paramInt; 
        } else {
          systemUiVisibilityInfo.globalVisibility |= paramInt;
          systemUiVisibilityInfo.localChanges &= paramInt ^ 0xFFFFFFFF;
        } 
        if (this.mDispatchedSystemUiVisibility != systemUiVisibilityInfo.globalVisibility) {
          this.mHandler.removeMessages(17);
          ViewRootHandler viewRootHandler = this.mHandler;
          viewRootHandler.sendMessage(viewRootHandler.obtainMessage(17, systemUiVisibilityInfo));
        } 
        return;
      } 
    } 
  }
  
  private void handleDispatchSystemUiVisibilityChanged(SystemUiVisibilityInfo paramSystemUiVisibilityInfo) {
    if (this.mSeq != paramSystemUiVisibilityInfo.seq && sNewInsetsMode != 2) {
      this.mSeq = paramSystemUiVisibilityInfo.seq;
      this.mAttachInfo.mForceReportNewAttributes = true;
      scheduleTraversals();
    } 
    if (this.mView == null)
      return; 
    if (paramSystemUiVisibilityInfo.localChanges != 0) {
      this.mView.updateLocalSystemUiVisibility(paramSystemUiVisibilityInfo.localValue, paramSystemUiVisibilityInfo.localChanges);
      paramSystemUiVisibilityInfo.localChanges = 0;
    } 
    int i = paramSystemUiVisibilityInfo.globalVisibility & 0x7;
    if (this.mDispatchedSystemUiVisibility != i) {
      this.mDispatchedSystemUiVisibility = i;
      this.mView.dispatchSystemUiVisibilityChanged(i);
    } 
  }
  
  public static void adjustLayoutParamsForCompatibility(WindowManager.LayoutParams paramLayoutParams) {
    // Byte code:
    //   0: getstatic android/view/ViewRootImpl.sNewInsetsMode : I
    //   3: iconst_2
    //   4: if_icmpeq -> 8
    //   7: return
    //   8: aload_0
    //   9: getfield systemUiVisibility : I
    //   12: aload_0
    //   13: getfield subtreeSystemUiVisibility : I
    //   16: ior
    //   17: istore_1
    //   18: aload_0
    //   19: getfield flags : I
    //   22: istore_2
    //   23: aload_0
    //   24: getfield type : I
    //   27: istore_3
    //   28: aload_0
    //   29: getfield privateFlags : I
    //   32: ldc_w 67108864
    //   35: iand
    //   36: ifne -> 124
    //   39: aload_0
    //   40: getfield insetsFlags : Landroid/view/InsetsFlags;
    //   43: iconst_0
    //   44: putfield appearance : I
    //   47: iload_1
    //   48: iconst_1
    //   49: iand
    //   50: ifeq -> 71
    //   53: aload_0
    //   54: getfield insetsFlags : Landroid/view/InsetsFlags;
    //   57: astore #4
    //   59: aload #4
    //   61: aload #4
    //   63: getfield appearance : I
    //   66: iconst_4
    //   67: ior
    //   68: putfield appearance : I
    //   71: iload_1
    //   72: sipush #8192
    //   75: iand
    //   76: ifeq -> 98
    //   79: aload_0
    //   80: getfield insetsFlags : Landroid/view/InsetsFlags;
    //   83: astore #4
    //   85: aload #4
    //   87: aload #4
    //   89: getfield appearance : I
    //   92: bipush #8
    //   94: ior
    //   95: putfield appearance : I
    //   98: iload_1
    //   99: bipush #16
    //   101: iand
    //   102: ifeq -> 124
    //   105: aload_0
    //   106: getfield insetsFlags : Landroid/view/InsetsFlags;
    //   109: astore #4
    //   111: aload #4
    //   113: aload #4
    //   115: getfield appearance : I
    //   118: bipush #16
    //   120: ior
    //   121: putfield appearance : I
    //   124: aload_0
    //   125: getfield privateFlags : I
    //   128: ldc_w 134217728
    //   131: iand
    //   132: ifne -> 192
    //   135: iload_1
    //   136: sipush #4096
    //   139: iand
    //   140: ifne -> 184
    //   143: iload_2
    //   144: sipush #1024
    //   147: iand
    //   148: ifeq -> 154
    //   151: goto -> 184
    //   154: iload_1
    //   155: sipush #2048
    //   158: iand
    //   159: ifeq -> 173
    //   162: aload_0
    //   163: getfield insetsFlags : Landroid/view/InsetsFlags;
    //   166: iconst_1
    //   167: putfield behavior : I
    //   170: goto -> 192
    //   173: aload_0
    //   174: getfield insetsFlags : Landroid/view/InsetsFlags;
    //   177: iconst_0
    //   178: putfield behavior : I
    //   181: goto -> 192
    //   184: aload_0
    //   185: getfield insetsFlags : Landroid/view/InsetsFlags;
    //   188: iconst_2
    //   189: putfield behavior : I
    //   192: aload_0
    //   193: getfield privateFlags : I
    //   196: ldc_w 268435456
    //   199: iand
    //   200: ifeq -> 204
    //   203: return
    //   204: aload_0
    //   205: invokevirtual getFitInsetsTypes : ()I
    //   208: istore #5
    //   210: aload_0
    //   211: invokevirtual getFitInsetsSides : ()I
    //   214: istore #6
    //   216: aload_0
    //   217: invokevirtual isFitInsetsIgnoringVisibility : ()Z
    //   220: istore #7
    //   222: iload_1
    //   223: sipush #1024
    //   226: iand
    //   227: ifne -> 250
    //   230: iload_2
    //   231: sipush #256
    //   234: iand
    //   235: ifne -> 250
    //   238: iload #5
    //   240: istore #8
    //   242: ldc_w 67108864
    //   245: iload_2
    //   246: iand
    //   247: ifeq -> 260
    //   250: iload #5
    //   252: invokestatic statusBars : ()I
    //   255: iconst_m1
    //   256: ixor
    //   257: iand
    //   258: istore #8
    //   260: iload_1
    //   261: sipush #512
    //   264: iand
    //   265: ifne -> 280
    //   268: iload #8
    //   270: istore #5
    //   272: iload_2
    //   273: ldc_w 134217728
    //   276: iand
    //   277: ifeq -> 290
    //   280: iload #8
    //   282: invokestatic systemBars : ()I
    //   285: iconst_m1
    //   286: ixor
    //   287: iand
    //   288: istore #5
    //   290: iload_3
    //   291: sipush #2005
    //   294: if_icmpeq -> 342
    //   297: iload_3
    //   298: sipush #2003
    //   301: if_icmpne -> 307
    //   304: goto -> 342
    //   307: iload #5
    //   309: istore #8
    //   311: iload #7
    //   313: istore #9
    //   315: invokestatic systemBars : ()I
    //   318: iload #5
    //   320: iand
    //   321: invokestatic systemBars : ()I
    //   324: if_icmpne -> 349
    //   327: iload #5
    //   329: invokestatic ime : ()I
    //   332: ior
    //   333: istore #8
    //   335: iload #7
    //   337: istore #9
    //   339: goto -> 349
    //   342: iconst_1
    //   343: istore #9
    //   345: iload #5
    //   347: istore #8
    //   349: aload_0
    //   350: iload #8
    //   352: invokevirtual setFitInsetsTypes : (I)V
    //   355: aload_0
    //   356: iload #6
    //   358: invokevirtual setFitInsetsSides : (I)V
    //   361: aload_0
    //   362: iload #9
    //   364: invokevirtual setFitInsetsIgnoringVisibility : (Z)V
    //   367: aload_0
    //   368: aload_0
    //   369: getfield privateFlags : I
    //   372: ldc_w -268435457
    //   375: iand
    //   376: putfield privateFlags : I
    //   379: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2380	-> 0
    //   #2381	-> 7
    //   #2383	-> 8
    //   #2384	-> 18
    //   #2385	-> 23
    //   #2387	-> 28
    //   #2388	-> 39
    //   #2389	-> 47
    //   #2390	-> 53
    //   #2392	-> 71
    //   #2393	-> 79
    //   #2395	-> 98
    //   #2396	-> 105
    //   #2400	-> 124
    //   #2401	-> 135
    //   #2404	-> 154
    //   #2405	-> 162
    //   #2407	-> 173
    //   #2403	-> 184
    //   #2411	-> 192
    //   #2412	-> 203
    //   #2415	-> 204
    //   #2416	-> 210
    //   #2417	-> 216
    //   #2419	-> 222
    //   #2422	-> 250
    //   #2424	-> 260
    //   #2426	-> 280
    //   #2428	-> 290
    //   #2430	-> 307
    //   #2431	-> 327
    //   #2429	-> 342
    //   #2433	-> 349
    //   #2434	-> 355
    //   #2435	-> 361
    //   #2438	-> 367
    //   #2439	-> 379
  }
  
  private void controlInsetsForCompatibility(WindowManager.LayoutParams paramLayoutParams) {
    int m, n, i1;
    if (sNewInsetsMode != 2)
      return; 
    int i = paramLayoutParams.systemUiVisibility | paramLayoutParams.subtreeSystemUiVisibility;
    int j = paramLayoutParams.flags;
    int k = paramLayoutParams.width;
    boolean bool1 = false;
    if (k == -1 && paramLayoutParams.height == -1) {
      k = 1;
    } else {
      k = 0;
    } 
    if (paramLayoutParams.type >= 1 && paramLayoutParams.type <= 99) {
      m = 1;
    } else {
      m = 0;
    } 
    if ((this.mTypesHiddenByFlags & WindowInsets.Type.statusBars()) != 0) {
      n = 1;
    } else {
      n = 0;
    } 
    if ((i & 0x4) != 0 || ((j & 0x400) != 0 && k != 0 && m)) {
      i1 = 1;
    } else {
      i1 = 0;
    } 
    if ((this.mTypesHiddenByFlags & WindowInsets.Type.navigationBars()) != 0) {
      j = 1;
    } else {
      j = 0;
    } 
    if ((i & 0x2) != 0)
      bool1 = true; 
    boolean bool2 = false;
    i = 0;
    if (i1 && !n) {
      m = 0x0 | WindowInsets.Type.statusBars();
      k = i;
    } else {
      m = bool2;
      k = i;
      if (!i1) {
        m = bool2;
        k = i;
        if (n) {
          k = 0x0 | WindowInsets.Type.statusBars();
          m = bool2;
        } 
      } 
    } 
    if (bool1 && j == 0) {
      i1 = m | WindowInsets.Type.navigationBars();
      n = k;
    } else {
      i1 = m;
      n = k;
      if (!bool1) {
        i1 = m;
        n = k;
        if (j != 0) {
          n = k | WindowInsets.Type.navigationBars();
          i1 = m;
        } 
      } 
    } 
    if (i1 != 0)
      getInsetsController().hide(i1); 
    if (n != 0)
      getInsetsController().show(n); 
    this.mTypesHiddenByFlags = k = this.mTypesHiddenByFlags | i1;
    this.mTypesHiddenByFlags = k & (n ^ 0xFFFFFFFF);
  }
  
  private boolean measureHierarchy(View paramView, WindowManager.LayoutParams paramLayoutParams, Resources paramResources, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: getstatic android/view/ViewRootImpl.DEBUG_LAYOUT : Z
    //   3: ifeq -> 94
    //   6: aload_0
    //   7: getfield mTag : Ljava/lang/String;
    //   10: astore #6
    //   12: new java/lang/StringBuilder
    //   15: dup
    //   16: invokespecial <init> : ()V
    //   19: astore #7
    //   21: aload #7
    //   23: ldc_w 'Measuring '
    //   26: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   29: pop
    //   30: aload #7
    //   32: aload_1
    //   33: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   36: pop
    //   37: aload #7
    //   39: ldc_w ' in display '
    //   42: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: pop
    //   46: aload #7
    //   48: iload #4
    //   50: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   53: pop
    //   54: aload #7
    //   56: ldc_w 'x'
    //   59: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   62: pop
    //   63: aload #7
    //   65: iload #5
    //   67: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   70: pop
    //   71: aload #7
    //   73: ldc_w '...'
    //   76: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   79: pop
    //   80: aload #6
    //   82: aload #7
    //   84: invokevirtual toString : ()Ljava/lang/String;
    //   87: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   90: pop
    //   91: goto -> 94
    //   94: aload_2
    //   95: getfield width : I
    //   98: bipush #-2
    //   100: if_icmpne -> 648
    //   103: aload_3
    //   104: invokevirtual getDisplayMetrics : ()Landroid/util/DisplayMetrics;
    //   107: astore #6
    //   109: aload_3
    //   110: ldc_w 17105073
    //   113: aload_0
    //   114: getfield mTmpValue : Landroid/util/TypedValue;
    //   117: iconst_1
    //   118: invokevirtual getValue : (ILandroid/util/TypedValue;Z)V
    //   121: iconst_0
    //   122: istore #8
    //   124: aload_0
    //   125: getfield mTmpValue : Landroid/util/TypedValue;
    //   128: getfield type : I
    //   131: iconst_5
    //   132: if_icmpne -> 147
    //   135: aload_0
    //   136: getfield mTmpValue : Landroid/util/TypedValue;
    //   139: aload #6
    //   141: invokevirtual getDimension : (Landroid/util/DisplayMetrics;)F
    //   144: f2i
    //   145: istore #8
    //   147: getstatic android/view/ViewRootImpl.DEBUG_DIALOG : Z
    //   150: ifeq -> 224
    //   153: aload_0
    //   154: getfield mTag : Ljava/lang/String;
    //   157: astore #6
    //   159: new java/lang/StringBuilder
    //   162: dup
    //   163: invokespecial <init> : ()V
    //   166: astore_3
    //   167: aload_3
    //   168: ldc_w 'Window '
    //   171: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   174: pop
    //   175: aload_3
    //   176: aload_0
    //   177: getfield mView : Landroid/view/View;
    //   180: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   183: pop
    //   184: aload_3
    //   185: ldc_w ': baseSize='
    //   188: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   191: pop
    //   192: aload_3
    //   193: iload #8
    //   195: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   198: pop
    //   199: aload_3
    //   200: ldc_w ', desiredWindowWidth='
    //   203: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   206: pop
    //   207: aload_3
    //   208: iload #4
    //   210: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   213: pop
    //   214: aload #6
    //   216: aload_3
    //   217: invokevirtual toString : ()Ljava/lang/String;
    //   220: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   223: pop
    //   224: iload #8
    //   226: ifeq -> 645
    //   229: iload #4
    //   231: iload #8
    //   233: if_icmple -> 645
    //   236: iload #8
    //   238: aload_2
    //   239: getfield width : I
    //   242: invokestatic getRootMeasureSpec : (II)I
    //   245: istore #9
    //   247: iload #5
    //   249: aload_2
    //   250: getfield height : I
    //   253: invokestatic getRootMeasureSpec : (II)I
    //   256: istore #10
    //   258: aload_0
    //   259: iload #9
    //   261: iload #10
    //   263: invokespecial performMeasure : (II)V
    //   266: getstatic android/view/ViewRootImpl.DEBUG_DIALOG : Z
    //   269: ifeq -> 400
    //   272: aload_0
    //   273: getfield mTag : Ljava/lang/String;
    //   276: astore_3
    //   277: new java/lang/StringBuilder
    //   280: dup
    //   281: invokespecial <init> : ()V
    //   284: astore #6
    //   286: aload #6
    //   288: ldc_w 'Window '
    //   291: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   294: pop
    //   295: aload #6
    //   297: aload_0
    //   298: getfield mView : Landroid/view/View;
    //   301: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   304: pop
    //   305: aload #6
    //   307: ldc_w ': measured ('
    //   310: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   313: pop
    //   314: aload #6
    //   316: aload_1
    //   317: invokevirtual getMeasuredWidth : ()I
    //   320: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   323: pop
    //   324: aload #6
    //   326: ldc_w ','
    //   329: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   332: pop
    //   333: aload #6
    //   335: aload_1
    //   336: invokevirtual getMeasuredHeight : ()I
    //   339: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   342: pop
    //   343: aload #6
    //   345: ldc_w ') from width spec: '
    //   348: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   351: pop
    //   352: aload #6
    //   354: iload #9
    //   356: invokestatic toString : (I)Ljava/lang/String;
    //   359: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   362: pop
    //   363: aload #6
    //   365: ldc_w ' and height spec: '
    //   368: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   371: pop
    //   372: aload #6
    //   374: iload #10
    //   376: invokestatic toString : (I)Ljava/lang/String;
    //   379: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   382: pop
    //   383: aload #6
    //   385: invokevirtual toString : ()Ljava/lang/String;
    //   388: astore #6
    //   390: aload_3
    //   391: aload #6
    //   393: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   396: pop
    //   397: goto -> 400
    //   400: aload_1
    //   401: invokevirtual getMeasuredWidthAndState : ()I
    //   404: ldc_w 16777216
    //   407: iand
    //   408: ifne -> 417
    //   411: iconst_1
    //   412: istore #8
    //   414: goto -> 651
    //   417: iload #8
    //   419: iload #4
    //   421: iadd
    //   422: iconst_2
    //   423: idiv
    //   424: istore #8
    //   426: getstatic android/view/ViewRootImpl.DEBUG_DIALOG : Z
    //   429: ifeq -> 492
    //   432: aload_0
    //   433: getfield mTag : Ljava/lang/String;
    //   436: astore_3
    //   437: new java/lang/StringBuilder
    //   440: dup
    //   441: invokespecial <init> : ()V
    //   444: astore #6
    //   446: aload #6
    //   448: ldc_w 'Window '
    //   451: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   454: pop
    //   455: aload #6
    //   457: aload_0
    //   458: getfield mView : Landroid/view/View;
    //   461: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   464: pop
    //   465: aload #6
    //   467: ldc_w ': next baseSize='
    //   470: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   473: pop
    //   474: aload #6
    //   476: iload #8
    //   478: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   481: pop
    //   482: aload_3
    //   483: aload #6
    //   485: invokevirtual toString : ()Ljava/lang/String;
    //   488: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   491: pop
    //   492: iload #8
    //   494: aload_2
    //   495: getfield width : I
    //   498: invokestatic getRootMeasureSpec : (II)I
    //   501: istore #8
    //   503: aload_0
    //   504: iload #8
    //   506: iload #10
    //   508: invokespecial performMeasure : (II)V
    //   511: getstatic android/view/ViewRootImpl.DEBUG_DIALOG : Z
    //   514: ifeq -> 611
    //   517: aload_0
    //   518: getfield mTag : Ljava/lang/String;
    //   521: astore_3
    //   522: new java/lang/StringBuilder
    //   525: dup
    //   526: invokespecial <init> : ()V
    //   529: astore #6
    //   531: aload #6
    //   533: ldc_w 'Window '
    //   536: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   539: pop
    //   540: aload #6
    //   542: aload_0
    //   543: getfield mView : Landroid/view/View;
    //   546: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   549: pop
    //   550: aload #6
    //   552: ldc_w ': measured ('
    //   555: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   558: pop
    //   559: aload #6
    //   561: aload_1
    //   562: invokevirtual getMeasuredWidth : ()I
    //   565: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   568: pop
    //   569: aload #6
    //   571: ldc_w ','
    //   574: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   577: pop
    //   578: aload #6
    //   580: aload_1
    //   581: invokevirtual getMeasuredHeight : ()I
    //   584: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   587: pop
    //   588: aload #6
    //   590: ldc_w ')'
    //   593: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   596: pop
    //   597: aload #6
    //   599: invokevirtual toString : ()Ljava/lang/String;
    //   602: astore #6
    //   604: aload_3
    //   605: aload #6
    //   607: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   610: pop
    //   611: aload_1
    //   612: invokevirtual getMeasuredWidthAndState : ()I
    //   615: ldc_w 16777216
    //   618: iand
    //   619: ifne -> 648
    //   622: getstatic android/view/ViewRootImpl.DEBUG_DIALOG : Z
    //   625: ifeq -> 639
    //   628: aload_0
    //   629: getfield mTag : Ljava/lang/String;
    //   632: ldc_w 'Good!'
    //   635: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   638: pop
    //   639: iconst_1
    //   640: istore #8
    //   642: goto -> 651
    //   645: goto -> 648
    //   648: iconst_0
    //   649: istore #8
    //   651: iload #8
    //   653: ifne -> 714
    //   656: iload #4
    //   658: aload_2
    //   659: getfield width : I
    //   662: invokestatic getRootMeasureSpec : (II)I
    //   665: istore #4
    //   667: iload #5
    //   669: aload_2
    //   670: getfield height : I
    //   673: invokestatic getRootMeasureSpec : (II)I
    //   676: istore #5
    //   678: aload_0
    //   679: iload #4
    //   681: iload #5
    //   683: invokespecial performMeasure : (II)V
    //   686: aload_0
    //   687: getfield mWidth : I
    //   690: aload_1
    //   691: invokevirtual getMeasuredWidth : ()I
    //   694: if_icmpne -> 708
    //   697: aload_0
    //   698: getfield mHeight : I
    //   701: aload_1
    //   702: invokevirtual getMeasuredHeight : ()I
    //   705: if_icmpeq -> 714
    //   708: iconst_1
    //   709: istore #11
    //   711: goto -> 717
    //   714: iconst_0
    //   715: istore #11
    //   717: iload #11
    //   719: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2482	-> 0
    //   #2484	-> 0
    //   #2488	-> 94
    //   #2489	-> 94
    //   #2494	-> 103
    //   #2495	-> 109
    //   #2496	-> 121
    //   #2497	-> 124
    //   #2498	-> 135
    //   #2500	-> 147
    //   #2502	-> 224
    //   #2503	-> 236
    //   #2504	-> 247
    //   #2505	-> 258
    //   #2506	-> 266
    //   #2507	-> 314
    //   #2508	-> 352
    //   #2509	-> 372
    //   #2506	-> 390
    //   #2510	-> 400
    //   #2511	-> 411
    //   #2514	-> 417
    //   #2515	-> 426
    //   #2517	-> 492
    //   #2518	-> 503
    //   #2519	-> 511
    //   #2520	-> 559
    //   #2519	-> 604
    //   #2521	-> 611
    //   #2522	-> 622
    //   #2523	-> 639
    //   #2502	-> 645
    //   #2489	-> 648
    //   #2529	-> 648
    //   #2530	-> 656
    //   #2531	-> 667
    //   #2532	-> 678
    //   #2533	-> 686
    //   #2534	-> 708
    //   #2544	-> 714
  }
  
  void transformMatrixToGlobal(Matrix paramMatrix) {
    paramMatrix.preTranslate(this.mAttachInfo.mWindowLeft, this.mAttachInfo.mWindowTop);
  }
  
  void transformMatrixToLocal(Matrix paramMatrix) {
    paramMatrix.postTranslate(-this.mAttachInfo.mWindowLeft, -this.mAttachInfo.mWindowTop);
  }
  
  WindowInsets getWindowInsets(boolean paramBoolean) {
    if (this.mLastWindowInsets == null || paramBoolean) {
      InsetsController insetsController = this.mInsetsController;
      Context context = this.mContext;
      boolean bool = context.getResources().getConfiguration().isScreenRound();
      paramBoolean = this.mAttachInfo.mAlwaysConsumeSystemBars;
      DisplayCutout.ParcelableWrapper parcelableWrapper = this.mPendingDisplayCutout;
      DisplayCutout displayCutout = parcelableWrapper.get();
      int i = this.mWindowAttributes.softInputMode, j = this.mWindowAttributes.flags, k = this.mWindowAttributes.systemUiVisibility, m = this.mWindowAttributes.subtreeSystemUiVisibility;
      this.mLastWindowInsets = insetsController.calculateInsets(bool, paramBoolean, displayCutout, i, j, m | k);
      Rect rect = this.mInsetsController.calculateVisibleInsets(this.mWindowAttributes.softInputMode);
      this.mAttachInfo.mVisibleInsets.set(rect);
      this.mAttachInfo.mContentInsets.set(this.mLastWindowInsets.getSystemWindowInsets().toRect());
      this.mAttachInfo.mStableInsets.set(this.mLastWindowInsets.getStableInsets().toRect());
    } 
    return this.mLastWindowInsets;
  }
  
  public void dispatchApplyInsets(View paramView) {
    Trace.traceBegin(8L, "dispatchApplyInsets");
    this.mApplyInsetsRequested = false;
    WindowInsets windowInsets1 = getWindowInsets(true);
    WindowInsets windowInsets2 = windowInsets1;
    if (!shouldDispatchCutout())
      windowInsets2 = windowInsets1.consumeDisplayCutout(); 
    paramView.dispatchApplyWindowInsets(windowInsets2);
    this.mAttachInfo.delayNotifyContentCaptureInsetsEvent(windowInsets2.getInsets(WindowInsets.Type.all()));
    Trace.traceEnd(8L);
  }
  
  private boolean updateCaptionInsets() {
    View view = this.mView;
    if (!(view instanceof DecorView))
      return false; 
    int i = ((DecorView)view).getCaptionInsetsHeight();
    Rect rect = new Rect();
    if (i != 0)
      rect.set(this.mWinFrame.left, this.mWinFrame.top, this.mWinFrame.right, this.mWinFrame.top + i); 
    if (this.mAttachInfo.mCaptionInsets.equals(rect))
      return false; 
    this.mAttachInfo.mCaptionInsets.set(rect);
    return true;
  }
  
  private boolean shouldDispatchCutout() {
    int i = this.mWindowAttributes.layoutInDisplayCutoutMode;
    boolean bool1 = true, bool2 = bool1;
    if (i != 3)
      if (this.mWindowAttributes.layoutInDisplayCutoutMode == 1) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    return bool2;
  }
  
  public InsetsController getInsetsController() {
    return this.mInsetsController;
  }
  
  private static boolean shouldUseDisplaySize(WindowManager.LayoutParams paramLayoutParams) {
    return (paramLayoutParams.type == 2041 || paramLayoutParams.type == 2011 || paramLayoutParams.type == 2020);
  }
  
  int dipToPx(int paramInt) {
    DisplayMetrics displayMetrics = this.mContext.getResources().getDisplayMetrics();
    return (int)(displayMetrics.density * paramInt + 0.5F);
  }
  
  private void performTraversals() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mView : Landroid/view/View;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnull -> 7007
    //   9: aload_0
    //   10: getfield mAdded : Z
    //   13: ifne -> 19
    //   16: goto -> 7007
    //   19: aload_0
    //   20: iconst_1
    //   21: putfield mIsInTraversal : Z
    //   24: aload_0
    //   25: iconst_1
    //   26: putfield mWillDrawSoon : Z
    //   29: iconst_0
    //   30: istore_2
    //   31: aload_0
    //   32: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   35: astore_3
    //   36: aload_0
    //   37: invokevirtual getHostVisibility : ()I
    //   40: istore #4
    //   42: aload_0
    //   43: getfield mFirst : Z
    //   46: ifne -> 78
    //   49: aload_0
    //   50: getfield mViewVisibility : I
    //   53: iload #4
    //   55: if_icmpne -> 72
    //   58: aload_0
    //   59: getfield mNewSurfaceNeeded : Z
    //   62: ifne -> 72
    //   65: aload_0
    //   66: getfield mAppVisibilityChanged : Z
    //   69: ifeq -> 78
    //   72: iconst_1
    //   73: istore #5
    //   75: goto -> 81
    //   78: iconst_0
    //   79: istore #5
    //   81: aload_0
    //   82: iconst_0
    //   83: putfield mAppVisibilityChanged : Z
    //   86: aload_0
    //   87: getfield mFirst : Z
    //   90: ifne -> 136
    //   93: aload_0
    //   94: getfield mViewVisibility : I
    //   97: ifne -> 106
    //   100: iconst_1
    //   101: istore #6
    //   103: goto -> 109
    //   106: iconst_0
    //   107: istore #6
    //   109: iload #4
    //   111: ifne -> 120
    //   114: iconst_1
    //   115: istore #7
    //   117: goto -> 123
    //   120: iconst_0
    //   121: istore #7
    //   123: iload #6
    //   125: iload #7
    //   127: if_icmpeq -> 136
    //   130: iconst_1
    //   131: istore #8
    //   133: goto -> 139
    //   136: iconst_0
    //   137: istore #8
    //   139: aload_0
    //   140: getfield mDisplay : Landroid/view/Display;
    //   143: astore #9
    //   145: aload #9
    //   147: invokevirtual getDisplayAdjustments : ()Landroid/view/DisplayAdjustments;
    //   150: invokevirtual getCompatibilityInfo : ()Landroid/content/res/CompatibilityInfo;
    //   153: astore #9
    //   155: aload #9
    //   157: invokevirtual supportsScreen : ()Z
    //   160: istore #10
    //   162: aload_0
    //   163: getfield mLastInCompatMode : Z
    //   166: istore #11
    //   168: iload #10
    //   170: iload #11
    //   172: if_icmpne -> 233
    //   175: aload_0
    //   176: iconst_1
    //   177: putfield mFullRedrawNeeded : Z
    //   180: aload_0
    //   181: iconst_1
    //   182: putfield mLayoutRequested : Z
    //   185: iload #11
    //   187: ifeq -> 210
    //   190: aload_3
    //   191: aload_3
    //   192: getfield privateFlags : I
    //   195: sipush #-129
    //   198: iand
    //   199: putfield privateFlags : I
    //   202: aload_0
    //   203: iconst_0
    //   204: putfield mLastInCompatMode : Z
    //   207: goto -> 227
    //   210: aload_3
    //   211: aload_3
    //   212: getfield privateFlags : I
    //   215: sipush #128
    //   218: ior
    //   219: putfield privateFlags : I
    //   222: aload_0
    //   223: iconst_1
    //   224: putfield mLastInCompatMode : Z
    //   227: aload_3
    //   228: astore #9
    //   230: goto -> 236
    //   233: aconst_null
    //   234: astore #9
    //   236: aload_0
    //   237: getfield mWinFrame : Landroid/graphics/Rect;
    //   240: astore #12
    //   242: aload_0
    //   243: getfield mFirst : Z
    //   246: ifeq -> 463
    //   249: aload_0
    //   250: iconst_1
    //   251: putfield mFullRedrawNeeded : Z
    //   254: aload_0
    //   255: iconst_1
    //   256: putfield mLayoutRequested : Z
    //   259: aload_0
    //   260: getfield mContext : Landroid/content/Context;
    //   263: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   266: invokevirtual getConfiguration : ()Landroid/content/res/Configuration;
    //   269: astore #13
    //   271: aload_3
    //   272: invokestatic shouldUseDisplaySize : (Landroid/view/WindowManager$LayoutParams;)Z
    //   275: ifeq -> 313
    //   278: new android/graphics/Point
    //   281: dup
    //   282: invokespecial <init> : ()V
    //   285: astore #14
    //   287: aload_0
    //   288: getfield mDisplay : Landroid/view/Display;
    //   291: aload #14
    //   293: invokevirtual getRealSize : (Landroid/graphics/Point;)V
    //   296: aload #14
    //   298: getfield x : I
    //   301: istore #6
    //   303: aload #14
    //   305: getfield y : I
    //   308: istore #7
    //   310: goto -> 373
    //   313: aload_3
    //   314: getfield width : I
    //   317: bipush #-2
    //   319: if_icmpeq -> 351
    //   322: aload_3
    //   323: getfield height : I
    //   326: bipush #-2
    //   328: if_icmpne -> 334
    //   331: goto -> 351
    //   334: aload #12
    //   336: invokevirtual width : ()I
    //   339: istore #6
    //   341: aload #12
    //   343: invokevirtual height : ()I
    //   346: istore #7
    //   348: goto -> 373
    //   351: aload_0
    //   352: aload #13
    //   354: getfield screenWidthDp : I
    //   357: invokevirtual dipToPx : (I)I
    //   360: istore #6
    //   362: aload_0
    //   363: aload #13
    //   365: getfield screenHeightDp : I
    //   368: invokevirtual dipToPx : (I)I
    //   371: istore #7
    //   373: aload_0
    //   374: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   377: iconst_1
    //   378: putfield mUse32BitDrawingCache : Z
    //   381: aload_0
    //   382: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   385: iload #4
    //   387: putfield mWindowVisibility : I
    //   390: aload_0
    //   391: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   394: iconst_0
    //   395: putfield mRecomputeGlobalAttributes : Z
    //   398: aload_0
    //   399: getfield mLastConfigurationFromResources : Landroid/content/res/Configuration;
    //   402: aload #13
    //   404: invokevirtual setTo : (Landroid/content/res/Configuration;)V
    //   407: aload_0
    //   408: aload_0
    //   409: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   412: getfield mSystemUiVisibility : I
    //   415: putfield mLastSystemUiVisibility : I
    //   418: aload_0
    //   419: getfield mViewLayoutDirectionInitial : I
    //   422: iconst_2
    //   423: if_icmpne -> 435
    //   426: aload_1
    //   427: aload #13
    //   429: invokevirtual getLayoutDirection : ()I
    //   432: invokevirtual setLayoutDirection : (I)V
    //   435: aload_1
    //   436: aload_0
    //   437: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   440: iconst_0
    //   441: invokevirtual dispatchAttachedToWindow : (Landroid/view/View$AttachInfo;I)V
    //   444: aload_0
    //   445: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   448: getfield mTreeObserver : Landroid/view/ViewTreeObserver;
    //   451: iconst_1
    //   452: invokevirtual dispatchOnWindowAttachedChange : (Z)V
    //   455: aload_0
    //   456: aload_1
    //   457: invokevirtual dispatchApplyInsets : (Landroid/view/View;)V
    //   460: goto -> 523
    //   463: aload #12
    //   465: invokevirtual width : ()I
    //   468: istore #15
    //   470: aload #12
    //   472: invokevirtual height : ()I
    //   475: istore #16
    //   477: iload #15
    //   479: aload_0
    //   480: getfield mWidth : I
    //   483: if_icmpne -> 503
    //   486: iload #16
    //   488: istore #7
    //   490: iload #15
    //   492: istore #6
    //   494: iload #16
    //   496: aload_0
    //   497: getfield mHeight : I
    //   500: if_icmpeq -> 523
    //   503: aload_0
    //   504: iconst_1
    //   505: putfield mFullRedrawNeeded : Z
    //   508: aload_0
    //   509: iconst_1
    //   510: putfield mLayoutRequested : Z
    //   513: iconst_1
    //   514: istore_2
    //   515: iload #15
    //   517: istore #6
    //   519: iload #16
    //   521: istore #7
    //   523: iload #5
    //   525: ifeq -> 589
    //   528: aload_0
    //   529: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   532: iload #4
    //   534: putfield mWindowVisibility : I
    //   537: aload_1
    //   538: iload #4
    //   540: invokevirtual dispatchWindowVisibilityChanged : (I)V
    //   543: iload #8
    //   545: ifeq -> 569
    //   548: iload #4
    //   550: ifne -> 559
    //   553: iconst_1
    //   554: istore #10
    //   556: goto -> 562
    //   559: iconst_0
    //   560: istore #10
    //   562: aload_1
    //   563: iload #10
    //   565: invokevirtual dispatchVisibilityAggregated : (Z)Z
    //   568: pop
    //   569: iload #4
    //   571: ifne -> 581
    //   574: aload_0
    //   575: getfield mNewSurfaceNeeded : Z
    //   578: ifeq -> 589
    //   581: aload_0
    //   582: invokespecial endDragResizing : ()V
    //   585: aload_0
    //   586: invokevirtual destroyHardwareResources : ()V
    //   589: aload_0
    //   590: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   593: getfield mWindowVisibility : I
    //   596: ifeq -> 603
    //   599: aload_1
    //   600: invokevirtual clearAccessibilityFocus : ()V
    //   603: invokestatic getRunQueue : ()Landroid/view/HandlerActionQueue;
    //   606: aload_0
    //   607: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   610: getfield mHandler : Landroid/os/Handler;
    //   613: invokevirtual executeActions : (Landroid/os/Handler;)V
    //   616: iconst_0
    //   617: istore #8
    //   619: aload_0
    //   620: getfield mLayoutRequested : Z
    //   623: ifeq -> 646
    //   626: aload_0
    //   627: getfield mStopped : Z
    //   630: ifeq -> 640
    //   633: aload_0
    //   634: getfield mReportNextDraw : Z
    //   637: ifeq -> 646
    //   640: iconst_1
    //   641: istore #17
    //   643: goto -> 649
    //   646: iconst_0
    //   647: istore #17
    //   649: iload #17
    //   651: ifeq -> 884
    //   654: aload_0
    //   655: getfield mView : Landroid/view/View;
    //   658: invokevirtual getContext : ()Landroid/content/Context;
    //   661: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   664: astore #13
    //   666: aload_0
    //   667: getfield mFirst : Z
    //   670: ifeq -> 709
    //   673: aload_0
    //   674: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   677: aload_0
    //   678: getfield mAddedTouchMode : Z
    //   681: iconst_1
    //   682: ixor
    //   683: putfield mInTouchMode : Z
    //   686: aload_0
    //   687: aload_0
    //   688: getfield mAddedTouchMode : Z
    //   691: invokespecial ensureTouchModeLocally : (Z)Z
    //   694: pop
    //   695: iconst_0
    //   696: istore #15
    //   698: iload #6
    //   700: istore #8
    //   702: iload #15
    //   704: istore #6
    //   706: goto -> 854
    //   709: aload_0
    //   710: getfield mPendingDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
    //   713: aload_0
    //   714: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   717: getfield mDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
    //   720: invokevirtual equals : (Ljava/lang/Object;)Z
    //   723: ifne -> 729
    //   726: iconst_1
    //   727: istore #8
    //   729: aload_3
    //   730: getfield width : I
    //   733: bipush #-2
    //   735: if_icmpeq -> 765
    //   738: aload_3
    //   739: getfield height : I
    //   742: bipush #-2
    //   744: if_icmpne -> 750
    //   747: goto -> 765
    //   750: iload #6
    //   752: istore #15
    //   754: iload #8
    //   756: istore #6
    //   758: iload #15
    //   760: istore #8
    //   762: goto -> 854
    //   765: iconst_1
    //   766: istore_2
    //   767: aload_3
    //   768: invokestatic shouldUseDisplaySize : (Landroid/view/WindowManager$LayoutParams;)Z
    //   771: ifeq -> 817
    //   774: new android/graphics/Point
    //   777: dup
    //   778: invokespecial <init> : ()V
    //   781: astore #14
    //   783: aload_0
    //   784: getfield mDisplay : Landroid/view/Display;
    //   787: aload #14
    //   789: invokevirtual getRealSize : (Landroid/graphics/Point;)V
    //   792: aload #14
    //   794: getfield x : I
    //   797: istore #15
    //   799: aload #14
    //   801: getfield y : I
    //   804: istore #7
    //   806: iload #8
    //   808: istore #6
    //   810: iload #15
    //   812: istore #8
    //   814: goto -> 854
    //   817: aload #13
    //   819: invokevirtual getConfiguration : ()Landroid/content/res/Configuration;
    //   822: astore #14
    //   824: aload_0
    //   825: aload #14
    //   827: getfield screenWidthDp : I
    //   830: invokevirtual dipToPx : (I)I
    //   833: istore #15
    //   835: aload_0
    //   836: aload #14
    //   838: getfield screenHeightDp : I
    //   841: invokevirtual dipToPx : (I)I
    //   844: istore #7
    //   846: iload #8
    //   848: istore #6
    //   850: iload #15
    //   852: istore #8
    //   854: iload_2
    //   855: aload_0
    //   856: aload_1
    //   857: aload_3
    //   858: aload #13
    //   860: iload #8
    //   862: iload #7
    //   864: invokespecial measureHierarchy : (Landroid/view/View;Landroid/view/WindowManager$LayoutParams;Landroid/content/res/Resources;II)Z
    //   867: ior
    //   868: istore_2
    //   869: iload #8
    //   871: istore #15
    //   873: iload #7
    //   875: istore #8
    //   877: iload #6
    //   879: istore #7
    //   881: goto -> 899
    //   884: iconst_0
    //   885: istore #15
    //   887: iload #7
    //   889: istore #8
    //   891: iload #15
    //   893: istore #7
    //   895: iload #6
    //   897: istore #15
    //   899: aload_0
    //   900: invokespecial collectViewAttributes : ()Z
    //   903: ifeq -> 909
    //   906: aload_3
    //   907: astore #9
    //   909: aload #9
    //   911: astore #13
    //   913: aload_0
    //   914: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   917: getfield mForceReportNewAttributes : Z
    //   920: ifeq -> 934
    //   923: aload_0
    //   924: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   927: iconst_0
    //   928: putfield mForceReportNewAttributes : Z
    //   931: aload_3
    //   932: astore #13
    //   934: aload_0
    //   935: getfield mFirst : Z
    //   938: ifne -> 955
    //   941: aload #13
    //   943: astore #9
    //   945: aload_0
    //   946: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   949: getfield mViewVisibilityChanged : Z
    //   952: ifeq -> 1083
    //   955: aload_0
    //   956: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   959: iconst_0
    //   960: putfield mViewVisibilityChanged : Z
    //   963: aload_0
    //   964: getfield mSoftInputMode : I
    //   967: sipush #240
    //   970: iand
    //   971: istore #6
    //   973: aload #13
    //   975: astore #9
    //   977: iload #6
    //   979: ifne -> 1083
    //   982: aload_0
    //   983: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   986: getfield mScrollContainers : Ljava/util/ArrayList;
    //   989: invokevirtual size : ()I
    //   992: istore #18
    //   994: iconst_0
    //   995: istore #16
    //   997: iload #16
    //   999: iload #18
    //   1001: if_icmpge -> 1035
    //   1004: aload_0
    //   1005: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   1008: getfield mScrollContainers : Ljava/util/ArrayList;
    //   1011: iload #16
    //   1013: invokevirtual get : (I)Ljava/lang/Object;
    //   1016: checkcast android/view/View
    //   1019: invokevirtual isShown : ()Z
    //   1022: ifeq -> 1029
    //   1025: bipush #16
    //   1027: istore #6
    //   1029: iinc #16, 1
    //   1032: goto -> 997
    //   1035: iload #6
    //   1037: istore #16
    //   1039: iload #6
    //   1041: ifne -> 1048
    //   1044: bipush #32
    //   1046: istore #16
    //   1048: aload #13
    //   1050: astore #9
    //   1052: aload_3
    //   1053: getfield softInputMode : I
    //   1056: sipush #240
    //   1059: iand
    //   1060: iload #16
    //   1062: if_icmpeq -> 1083
    //   1065: aload_3
    //   1066: aload_3
    //   1067: getfield softInputMode : I
    //   1070: sipush #-241
    //   1073: iand
    //   1074: iload #16
    //   1076: ior
    //   1077: putfield softInputMode : I
    //   1080: aload_3
    //   1081: astore #9
    //   1083: aload_0
    //   1084: getfield mApplyInsetsRequested : Z
    //   1087: ifeq -> 1140
    //   1090: aload_0
    //   1091: aload_1
    //   1092: invokevirtual dispatchApplyInsets : (Landroid/view/View;)V
    //   1095: aload_0
    //   1096: getfield mLayoutRequested : Z
    //   1099: ifeq -> 1137
    //   1102: aload_0
    //   1103: getfield mView : Landroid/view/View;
    //   1106: astore #13
    //   1108: aload #13
    //   1110: invokevirtual getContext : ()Landroid/content/Context;
    //   1113: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   1116: astore #13
    //   1118: iload_2
    //   1119: aload_0
    //   1120: aload_1
    //   1121: aload_3
    //   1122: aload #13
    //   1124: iload #15
    //   1126: iload #8
    //   1128: invokespecial measureHierarchy : (Landroid/view/View;Landroid/view/WindowManager$LayoutParams;Landroid/content/res/Resources;II)Z
    //   1131: ior
    //   1132: istore #6
    //   1134: goto -> 1143
    //   1137: goto -> 1140
    //   1140: iload_2
    //   1141: istore #6
    //   1143: iload #17
    //   1145: ifeq -> 1153
    //   1148: aload_0
    //   1149: iconst_0
    //   1150: putfield mLayoutRequested : Z
    //   1153: iload #17
    //   1155: ifeq -> 1269
    //   1158: iload #6
    //   1160: ifeq -> 1269
    //   1163: aload_0
    //   1164: getfield mWidth : I
    //   1167: istore #6
    //   1169: iload #6
    //   1171: aload_1
    //   1172: invokevirtual getMeasuredWidth : ()I
    //   1175: if_icmpne -> 1263
    //   1178: aload_0
    //   1179: getfield mHeight : I
    //   1182: aload_1
    //   1183: invokevirtual getMeasuredHeight : ()I
    //   1186: if_icmpne -> 1263
    //   1189: aload_3
    //   1190: getfield width : I
    //   1193: bipush #-2
    //   1195: if_icmpne -> 1226
    //   1198: aload #12
    //   1200: invokevirtual width : ()I
    //   1203: iload #15
    //   1205: if_icmpge -> 1226
    //   1208: aload #12
    //   1210: invokevirtual width : ()I
    //   1213: aload_0
    //   1214: getfield mWidth : I
    //   1217: if_icmpne -> 1223
    //   1220: goto -> 1226
    //   1223: goto -> 1263
    //   1226: aload_3
    //   1227: getfield height : I
    //   1230: bipush #-2
    //   1232: if_icmpne -> 1260
    //   1235: aload #12
    //   1237: invokevirtual height : ()I
    //   1240: iload #8
    //   1242: if_icmpge -> 1269
    //   1245: aload #12
    //   1247: invokevirtual height : ()I
    //   1250: aload_0
    //   1251: getfield mHeight : I
    //   1254: if_icmpeq -> 1269
    //   1257: goto -> 1263
    //   1260: goto -> 1269
    //   1263: iconst_1
    //   1264: istore #6
    //   1266: goto -> 1272
    //   1269: iconst_0
    //   1270: istore #6
    //   1272: aload_0
    //   1273: getfield mDragResizing : Z
    //   1276: ifeq -> 1292
    //   1279: aload_0
    //   1280: getfield mResizeMode : I
    //   1283: ifne -> 1292
    //   1286: iconst_1
    //   1287: istore #8
    //   1289: goto -> 1295
    //   1292: iconst_0
    //   1293: istore #8
    //   1295: aload_0
    //   1296: getfield mActivityRelaunched : Z
    //   1299: istore #11
    //   1301: aload_0
    //   1302: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   1305: getfield mTreeObserver : Landroid/view/ViewTreeObserver;
    //   1308: astore #13
    //   1310: aload #13
    //   1312: invokevirtual hasComputeInternalInsetsListeners : ()Z
    //   1315: ifne -> 1337
    //   1318: aload_0
    //   1319: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   1322: getfield mHasNonEmptyGivenInternalInsets : Z
    //   1325: ifeq -> 1331
    //   1328: goto -> 1337
    //   1331: iconst_0
    //   1332: istore #19
    //   1334: goto -> 1340
    //   1337: iconst_1
    //   1338: istore #19
    //   1340: iconst_0
    //   1341: istore #20
    //   1343: aload_0
    //   1344: getfield mSurface : Landroid/view/Surface;
    //   1347: invokevirtual getGenerationId : ()I
    //   1350: istore #21
    //   1352: iload #4
    //   1354: ifne -> 1363
    //   1357: iconst_1
    //   1358: istore #22
    //   1360: goto -> 1366
    //   1363: iconst_0
    //   1364: istore #22
    //   1366: aload_0
    //   1367: getfield mForceNextWindowRelayout : Z
    //   1370: istore #23
    //   1372: iconst_0
    //   1373: istore #24
    //   1375: iconst_0
    //   1376: istore #25
    //   1378: iconst_0
    //   1379: istore_2
    //   1380: iconst_0
    //   1381: istore #26
    //   1383: iconst_0
    //   1384: istore #27
    //   1386: iconst_0
    //   1387: istore #28
    //   1389: iconst_0
    //   1390: istore #15
    //   1392: iconst_0
    //   1393: istore #29
    //   1395: iconst_0
    //   1396: istore #30
    //   1398: iconst_0
    //   1399: istore #31
    //   1401: iconst_0
    //   1402: istore #16
    //   1404: iconst_0
    //   1405: istore #18
    //   1407: iconst_0
    //   1408: istore #32
    //   1410: aload_0
    //   1411: getfield mWindowAttributesChanged : Z
    //   1414: istore #33
    //   1416: iload #33
    //   1418: ifeq -> 1432
    //   1421: aload_0
    //   1422: iconst_0
    //   1423: putfield mWindowAttributesChanged : Z
    //   1426: aload_3
    //   1427: astore #9
    //   1429: goto -> 1432
    //   1432: aload #9
    //   1434: ifnull -> 1484
    //   1437: aload_1
    //   1438: getfield mPrivateFlags : I
    //   1441: sipush #512
    //   1444: iand
    //   1445: ifeq -> 1470
    //   1448: aload #9
    //   1450: getfield format : I
    //   1453: istore #34
    //   1455: iload #34
    //   1457: invokestatic formatHasAlpha : (I)Z
    //   1460: ifne -> 1470
    //   1463: aload #9
    //   1465: bipush #-3
    //   1467: putfield format : I
    //   1470: aload #9
    //   1472: invokestatic adjustLayoutParamsForCompatibility : (Landroid/view/WindowManager$LayoutParams;)V
    //   1475: aload_0
    //   1476: aload #9
    //   1478: invokespecial controlInsetsForCompatibility : (Landroid/view/WindowManager$LayoutParams;)V
    //   1481: goto -> 1484
    //   1484: iconst_0
    //   1485: istore #10
    //   1487: aload_0
    //   1488: getfield mFirst : Z
    //   1491: ifne -> 1561
    //   1494: iload #6
    //   1496: iload #8
    //   1498: ior
    //   1499: iload #11
    //   1501: ior
    //   1502: ifne -> 1561
    //   1505: iload #5
    //   1507: ifne -> 1561
    //   1510: iload #7
    //   1512: ifne -> 1561
    //   1515: aload #9
    //   1517: ifnonnull -> 1561
    //   1520: aload_0
    //   1521: getfield mForceNextWindowRelayout : Z
    //   1524: ifeq -> 1530
    //   1527: goto -> 1561
    //   1530: aload_0
    //   1531: aload #12
    //   1533: invokespecial maybeHandleWindowMove : (Landroid/graphics/Rect;)V
    //   1536: iconst_0
    //   1537: istore #7
    //   1539: iload #20
    //   1541: istore #6
    //   1543: iload #24
    //   1545: istore #8
    //   1547: iload #26
    //   1549: istore_2
    //   1550: iload #29
    //   1552: istore #15
    //   1554: iload #18
    //   1556: istore #16
    //   1558: goto -> 5785
    //   1561: iconst_0
    //   1562: istore #6
    //   1564: iconst_0
    //   1565: istore #18
    //   1567: aload_0
    //   1568: iconst_0
    //   1569: putfield mForceNextWindowRelayout : Z
    //   1572: iload #22
    //   1574: ifeq -> 1606
    //   1577: iload #19
    //   1579: ifeq -> 1600
    //   1582: aload_0
    //   1583: getfield mFirst : Z
    //   1586: ifne -> 1594
    //   1589: iload #5
    //   1591: ifeq -> 1600
    //   1594: iconst_1
    //   1595: istore #10
    //   1597: goto -> 1603
    //   1600: iconst_0
    //   1601: istore #10
    //   1603: goto -> 1609
    //   1606: iconst_0
    //   1607: istore #10
    //   1609: aload_0
    //   1610: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   1613: astore #13
    //   1615: aload #13
    //   1617: ifnull -> 1636
    //   1620: aload #13
    //   1622: getfield mSurfaceLock : Ljava/util/concurrent/locks/ReentrantLock;
    //   1625: invokevirtual lock : ()V
    //   1628: aload_0
    //   1629: iconst_1
    //   1630: putfield mDrawingAllowed : Z
    //   1633: goto -> 1636
    //   1636: iconst_0
    //   1637: istore #35
    //   1639: iconst_0
    //   1640: istore #36
    //   1642: iconst_0
    //   1643: istore #37
    //   1645: iconst_0
    //   1646: istore #38
    //   1648: aload_0
    //   1649: getfield mSurface : Landroid/view/Surface;
    //   1652: invokevirtual isValid : ()Z
    //   1655: istore #39
    //   1657: getstatic android/view/ViewRootImpl.DEBUG_LAYOUT : Z
    //   1660: istore #11
    //   1662: iload #11
    //   1664: ifeq -> 1804
    //   1667: aload_0
    //   1668: getfield mTag : Ljava/lang/String;
    //   1671: astore #13
    //   1673: iconst_0
    //   1674: istore #11
    //   1676: new java/lang/StringBuilder
    //   1679: astore #14
    //   1681: aload #14
    //   1683: invokespecial <init> : ()V
    //   1686: aload #14
    //   1688: ldc_w 'host=w:'
    //   1691: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1694: pop
    //   1695: aload #14
    //   1697: aload_1
    //   1698: invokevirtual getMeasuredWidth : ()I
    //   1701: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1704: pop
    //   1705: aload #14
    //   1707: ldc_w ', h:'
    //   1710: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1713: pop
    //   1714: aload #14
    //   1716: aload_1
    //   1717: invokevirtual getMeasuredHeight : ()I
    //   1720: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1723: pop
    //   1724: aload #14
    //   1726: ldc_w ', params='
    //   1729: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1732: pop
    //   1733: aload #14
    //   1735: aload #9
    //   1737: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1740: pop
    //   1741: aload #14
    //   1743: invokevirtual toString : ()Ljava/lang/String;
    //   1746: astore #14
    //   1748: aload #13
    //   1750: aload #14
    //   1752: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   1755: pop
    //   1756: goto -> 1804
    //   1759: astore #9
    //   1761: iconst_0
    //   1762: istore #8
    //   1764: iconst_0
    //   1765: istore #7
    //   1767: iconst_0
    //   1768: istore #6
    //   1770: goto -> 4829
    //   1773: astore #9
    //   1775: iconst_0
    //   1776: istore #8
    //   1778: iconst_0
    //   1779: istore #7
    //   1781: iconst_0
    //   1782: istore #6
    //   1784: goto -> 4829
    //   1787: astore #9
    //   1789: iconst_0
    //   1790: istore #11
    //   1792: iconst_0
    //   1793: istore #8
    //   1795: iconst_0
    //   1796: istore #7
    //   1798: iconst_0
    //   1799: istore #6
    //   1801: goto -> 4829
    //   1804: iconst_0
    //   1805: istore #40
    //   1807: iconst_0
    //   1808: istore #41
    //   1810: iconst_0
    //   1811: istore #42
    //   1813: iconst_0
    //   1814: istore #43
    //   1816: iconst_0
    //   1817: istore #44
    //   1819: iconst_0
    //   1820: istore #45
    //   1822: aload_0
    //   1823: getfield mGraphicLock : Ljava/lang/Object;
    //   1826: astore #13
    //   1828: aload #13
    //   1830: monitorenter
    //   1831: aload_0
    //   1832: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   1835: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   1838: astore #14
    //   1840: aload #14
    //   1842: ifnull -> 1919
    //   1845: aload_0
    //   1846: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   1849: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   1852: invokevirtual pause : ()Z
    //   1855: ifeq -> 1895
    //   1858: aload_0
    //   1859: getfield mDirty : Landroid/graphics/Rect;
    //   1862: astore #14
    //   1864: aload_0
    //   1865: getfield mWidth : I
    //   1868: istore #7
    //   1870: aload_0
    //   1871: getfield mHeight : I
    //   1874: istore #8
    //   1876: aload #14
    //   1878: iconst_0
    //   1879: iconst_0
    //   1880: iload #7
    //   1882: iload #8
    //   1884: invokevirtual set : (IIII)V
    //   1887: goto -> 1895
    //   1890: astore #9
    //   1892: goto -> 4760
    //   1895: aload_0
    //   1896: getfield mChoreographer : Landroid/view/Choreographer;
    //   1899: getfield mFrameInfo : Landroid/graphics/FrameInfo;
    //   1902: lconst_1
    //   1903: invokevirtual addFlags : (J)V
    //   1906: goto -> 1919
    //   1909: astore #9
    //   1911: goto -> 4760
    //   1914: astore #9
    //   1916: goto -> 4760
    //   1919: aload #13
    //   1921: monitorexit
    //   1922: aload_0
    //   1923: aload #9
    //   1925: iload #4
    //   1927: iload #10
    //   1929: invokespecial relayoutWindow : (Landroid/view/WindowManager$LayoutParams;IZ)I
    //   1932: istore #7
    //   1934: iload #6
    //   1936: istore #8
    //   1938: getstatic android/view/ViewRootImpl.DEBUG_LAYOUT : Z
    //   1941: istore #11
    //   1943: iload #11
    //   1945: ifeq -> 2069
    //   1948: aload_0
    //   1949: getfield mTag : Ljava/lang/String;
    //   1952: astore #13
    //   1954: new java/lang/StringBuilder
    //   1957: astore #46
    //   1959: aload #46
    //   1961: invokespecial <init> : ()V
    //   1964: aload #46
    //   1966: ldc_w 'relayout: frame='
    //   1969: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1972: pop
    //   1973: aload #46
    //   1975: aload #12
    //   1977: invokevirtual toShortString : ()Ljava/lang/String;
    //   1980: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1983: pop
    //   1984: aload #46
    //   1986: ldc_w ' cutout='
    //   1989: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1992: pop
    //   1993: aload_0
    //   1994: getfield mPendingDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
    //   1997: astore #14
    //   1999: aload #46
    //   2001: aload #14
    //   2003: invokevirtual get : ()Landroid/view/DisplayCutout;
    //   2006: invokevirtual toString : ()Ljava/lang/String;
    //   2009: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2012: pop
    //   2013: aload #46
    //   2015: ldc_w ' surface='
    //   2018: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2021: pop
    //   2022: aload #46
    //   2024: aload_0
    //   2025: getfield mSurface : Landroid/view/Surface;
    //   2028: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   2031: pop
    //   2032: aload #46
    //   2034: invokevirtual toString : ()Ljava/lang/String;
    //   2037: astore #14
    //   2039: aload #13
    //   2041: aload #14
    //   2043: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   2046: pop
    //   2047: goto -> 2069
    //   2050: astore #9
    //   2052: iload #7
    //   2054: istore #6
    //   2056: iconst_0
    //   2057: istore #8
    //   2059: iconst_0
    //   2060: istore #7
    //   2062: iload #44
    //   2064: istore #11
    //   2066: goto -> 4829
    //   2069: iload #6
    //   2071: istore #8
    //   2073: aload_0
    //   2074: getfield mPendingMergedConfiguration : Landroid/util/MergedConfiguration;
    //   2077: aload_0
    //   2078: getfield mLastReportedMergedConfiguration : Landroid/util/MergedConfiguration;
    //   2081: invokevirtual equals : (Ljava/lang/Object;)Z
    //   2084: istore #11
    //   2086: iload #18
    //   2088: istore #6
    //   2090: iload #11
    //   2092: ifne -> 2192
    //   2095: getstatic android/view/ViewRootImpl.DEBUG_CONFIGURATION : Z
    //   2098: ifeq -> 2158
    //   2101: aload_0
    //   2102: getfield mTag : Ljava/lang/String;
    //   2105: astore #13
    //   2107: new java/lang/StringBuilder
    //   2110: astore #46
    //   2112: aload #46
    //   2114: invokespecial <init> : ()V
    //   2117: aload #46
    //   2119: ldc_w 'Visible with new config: '
    //   2122: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2125: pop
    //   2126: aload_0
    //   2127: getfield mPendingMergedConfiguration : Landroid/util/MergedConfiguration;
    //   2130: astore #14
    //   2132: aload #46
    //   2134: aload #14
    //   2136: invokevirtual getMergedConfiguration : ()Landroid/content/res/Configuration;
    //   2139: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   2142: pop
    //   2143: aload #46
    //   2145: invokevirtual toString : ()Ljava/lang/String;
    //   2148: astore #14
    //   2150: aload #13
    //   2152: aload #14
    //   2154: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   2157: pop
    //   2158: aload_0
    //   2159: getfield mPendingMergedConfiguration : Landroid/util/MergedConfiguration;
    //   2162: astore #13
    //   2164: aload_0
    //   2165: getfield mFirst : Z
    //   2168: ifne -> 2177
    //   2171: iconst_1
    //   2172: istore #11
    //   2174: goto -> 2180
    //   2177: iconst_0
    //   2178: istore #11
    //   2180: aload_0
    //   2181: aload #13
    //   2183: iload #11
    //   2185: iconst_m1
    //   2186: invokespecial performConfigurationChange : (Landroid/util/MergedConfiguration;ZI)V
    //   2189: iconst_1
    //   2190: istore #6
    //   2192: iload #6
    //   2194: istore #8
    //   2196: aload_0
    //   2197: getfield mPendingDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
    //   2200: aload_0
    //   2201: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   2204: getfield mDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
    //   2207: invokevirtual equals : (Ljava/lang/Object;)Z
    //   2210: istore #11
    //   2212: iload #7
    //   2214: bipush #32
    //   2216: iand
    //   2217: ifeq -> 2226
    //   2220: iconst_1
    //   2221: istore #8
    //   2223: goto -> 2229
    //   2226: iconst_0
    //   2227: istore #8
    //   2229: iload #25
    //   2231: istore #26
    //   2233: iload #28
    //   2235: istore #29
    //   2237: iload #31
    //   2239: istore #18
    //   2241: iload #36
    //   2243: istore #37
    //   2245: iload #43
    //   2247: istore #44
    //   2249: aload_0
    //   2250: getfield mPendingAlwaysConsumeSystemBars : Z
    //   2253: aload_0
    //   2254: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   2257: getfield mAlwaysConsumeSystemBars : Z
    //   2260: if_icmpeq -> 2269
    //   2263: iconst_1
    //   2264: istore #20
    //   2266: goto -> 2272
    //   2269: iconst_0
    //   2270: istore #20
    //   2272: iload #25
    //   2274: istore #26
    //   2276: iload #28
    //   2278: istore #29
    //   2280: iload #31
    //   2282: istore #18
    //   2284: iload #36
    //   2286: istore #37
    //   2288: iload #43
    //   2290: istore #44
    //   2292: aload_0
    //   2293: aload_3
    //   2294: invokevirtual getColorMode : ()I
    //   2297: invokespecial hasColorModeChanged : (I)Z
    //   2300: istore #47
    //   2302: iload #39
    //   2304: ifne -> 2372
    //   2307: iload #27
    //   2309: istore #26
    //   2311: iload #30
    //   2313: istore #29
    //   2315: iload #32
    //   2317: istore #18
    //   2319: iload #38
    //   2321: istore #37
    //   2323: iload #45
    //   2325: istore #44
    //   2327: aload_0
    //   2328: getfield mSurface : Landroid/view/Surface;
    //   2331: invokevirtual isValid : ()Z
    //   2334: ifeq -> 2372
    //   2337: iconst_1
    //   2338: istore_2
    //   2339: goto -> 2374
    //   2342: astore #9
    //   2344: iload #6
    //   2346: istore_2
    //   2347: iload #7
    //   2349: istore #6
    //   2351: iload_2
    //   2352: istore #7
    //   2354: iload #26
    //   2356: istore_2
    //   2357: iload #29
    //   2359: istore #15
    //   2361: iload #18
    //   2363: istore #16
    //   2365: iload #44
    //   2367: istore #11
    //   2369: goto -> 4829
    //   2372: iconst_0
    //   2373: istore_2
    //   2374: iload #39
    //   2376: ifeq -> 2418
    //   2379: iload_2
    //   2380: istore #26
    //   2382: iload #30
    //   2384: istore #29
    //   2386: iload #32
    //   2388: istore #18
    //   2390: iload #38
    //   2392: istore #37
    //   2394: iload #45
    //   2396: istore #44
    //   2398: aload_0
    //   2399: getfield mSurface : Landroid/view/Surface;
    //   2402: invokevirtual isValid : ()Z
    //   2405: istore #39
    //   2407: iload #39
    //   2409: ifne -> 2418
    //   2412: iconst_1
    //   2413: istore #15
    //   2415: goto -> 2421
    //   2418: iconst_0
    //   2419: istore #15
    //   2421: iload_2
    //   2422: istore #26
    //   2424: iload #15
    //   2426: istore #29
    //   2428: iload #31
    //   2430: istore #18
    //   2432: iload #36
    //   2434: istore #37
    //   2436: iload #43
    //   2438: istore #44
    //   2440: aload_0
    //   2441: getfield mSurface : Landroid/view/Surface;
    //   2444: invokevirtual getGenerationId : ()I
    //   2447: istore #16
    //   2449: iload #21
    //   2451: iload #16
    //   2453: if_icmpeq -> 2514
    //   2456: iload_2
    //   2457: istore #26
    //   2459: iload #15
    //   2461: istore #29
    //   2463: iload #32
    //   2465: istore #18
    //   2467: iload #38
    //   2469: istore #37
    //   2471: iload #45
    //   2473: istore #44
    //   2475: aload_0
    //   2476: getfield mSurface : Landroid/view/Surface;
    //   2479: astore #13
    //   2481: iload_2
    //   2482: istore #26
    //   2484: iload #15
    //   2486: istore #29
    //   2488: iload #32
    //   2490: istore #18
    //   2492: iload #38
    //   2494: istore #37
    //   2496: iload #45
    //   2498: istore #44
    //   2500: aload #13
    //   2502: invokevirtual isValid : ()Z
    //   2505: ifeq -> 2514
    //   2508: iconst_1
    //   2509: istore #16
    //   2511: goto -> 2517
    //   2514: iconst_0
    //   2515: istore #16
    //   2517: iload #35
    //   2519: istore #37
    //   2521: iload #11
    //   2523: iconst_1
    //   2524: ixor
    //   2525: ifeq -> 2752
    //   2528: iload_2
    //   2529: istore #26
    //   2531: iload #15
    //   2533: istore #29
    //   2535: iload #16
    //   2537: istore #18
    //   2539: iload #38
    //   2541: istore #37
    //   2543: iload #45
    //   2545: istore #44
    //   2547: aload_0
    //   2548: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   2551: getfield mDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
    //   2554: aload_0
    //   2555: getfield mPendingDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
    //   2558: invokevirtual set : (Landroid/view/DisplayCutout$ParcelableWrapper;)V
    //   2561: iload_2
    //   2562: istore #26
    //   2564: iload #15
    //   2566: istore #29
    //   2568: iload #16
    //   2570: istore #18
    //   2572: iload #38
    //   2574: istore #37
    //   2576: iload #45
    //   2578: istore #44
    //   2580: getstatic android/view/ViewRootImpl.DEBUG_LAYOUT : Z
    //   2583: ifeq -> 2749
    //   2586: iload_2
    //   2587: istore #26
    //   2589: iload #15
    //   2591: istore #29
    //   2593: iload #16
    //   2595: istore #18
    //   2597: iload #38
    //   2599: istore #37
    //   2601: iload #45
    //   2603: istore #44
    //   2605: aload_0
    //   2606: getfield mTag : Ljava/lang/String;
    //   2609: astore #14
    //   2611: iload_2
    //   2612: istore #26
    //   2614: iload #15
    //   2616: istore #29
    //   2618: iload #16
    //   2620: istore #18
    //   2622: iload #38
    //   2624: istore #37
    //   2626: iload #45
    //   2628: istore #44
    //   2630: new java/lang/StringBuilder
    //   2633: astore #13
    //   2635: iload_2
    //   2636: istore #26
    //   2638: iload #15
    //   2640: istore #29
    //   2642: iload #16
    //   2644: istore #18
    //   2646: iload #38
    //   2648: istore #37
    //   2650: iload #45
    //   2652: istore #44
    //   2654: aload #13
    //   2656: invokespecial <init> : ()V
    //   2659: iload_2
    //   2660: istore #26
    //   2662: iload #15
    //   2664: istore #29
    //   2666: iload #16
    //   2668: istore #18
    //   2670: iload #38
    //   2672: istore #37
    //   2674: iload #45
    //   2676: istore #44
    //   2678: aload #13
    //   2680: ldc_w 'DisplayCutout changing to: '
    //   2683: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2686: pop
    //   2687: iload_2
    //   2688: istore #26
    //   2690: iload #15
    //   2692: istore #29
    //   2694: iload #16
    //   2696: istore #18
    //   2698: iload #38
    //   2700: istore #37
    //   2702: iload #45
    //   2704: istore #44
    //   2706: aload #13
    //   2708: aload_0
    //   2709: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   2712: getfield mDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
    //   2715: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   2718: pop
    //   2719: iload_2
    //   2720: istore #26
    //   2722: iload #15
    //   2724: istore #29
    //   2726: iload #16
    //   2728: istore #18
    //   2730: iload #38
    //   2732: istore #37
    //   2734: iload #45
    //   2736: istore #44
    //   2738: aload #14
    //   2740: aload #13
    //   2742: invokevirtual toString : ()Ljava/lang/String;
    //   2745: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   2748: pop
    //   2749: iconst_1
    //   2750: istore #37
    //   2752: iload #37
    //   2754: istore #38
    //   2756: iload #20
    //   2758: ifeq -> 2790
    //   2761: iload_2
    //   2762: istore #26
    //   2764: iload #15
    //   2766: istore #29
    //   2768: iload #16
    //   2770: istore #18
    //   2772: iload #45
    //   2774: istore #44
    //   2776: aload_0
    //   2777: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   2780: aload_0
    //   2781: getfield mPendingAlwaysConsumeSystemBars : Z
    //   2784: putfield mAlwaysConsumeSystemBars : Z
    //   2787: iconst_1
    //   2788: istore #38
    //   2790: iload_2
    //   2791: istore #26
    //   2793: iload #15
    //   2795: istore #29
    //   2797: iload #16
    //   2799: istore #18
    //   2801: iload #38
    //   2803: istore #37
    //   2805: iload #43
    //   2807: istore #44
    //   2809: aload_0
    //   2810: invokespecial updateCaptionInsets : ()Z
    //   2813: istore #11
    //   2815: iload #11
    //   2817: ifeq -> 2823
    //   2820: iconst_1
    //   2821: istore #38
    //   2823: iload #38
    //   2825: ifne -> 2895
    //   2828: iload_2
    //   2829: istore #26
    //   2831: iload #15
    //   2833: istore #29
    //   2835: iload #16
    //   2837: istore #18
    //   2839: iload #38
    //   2841: istore #37
    //   2843: iload #45
    //   2845: istore #44
    //   2847: aload_0
    //   2848: getfield mLastSystemUiVisibility : I
    //   2851: aload_0
    //   2852: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   2855: getfield mSystemUiVisibility : I
    //   2858: if_icmpne -> 2895
    //   2861: iload_2
    //   2862: istore #26
    //   2864: iload #15
    //   2866: istore #29
    //   2868: iload #16
    //   2870: istore #18
    //   2872: iload #38
    //   2874: istore #37
    //   2876: iload #45
    //   2878: istore #44
    //   2880: aload_0
    //   2881: getfield mApplyInsetsRequested : Z
    //   2884: istore #35
    //   2886: iload #38
    //   2888: istore #11
    //   2890: iload #35
    //   2892: ifeq -> 2952
    //   2895: iload_2
    //   2896: istore #26
    //   2898: iload #15
    //   2900: istore #29
    //   2902: iload #16
    //   2904: istore #18
    //   2906: iload #38
    //   2908: istore #37
    //   2910: iload #43
    //   2912: istore #44
    //   2914: aload_0
    //   2915: aload_0
    //   2916: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   2919: getfield mSystemUiVisibility : I
    //   2922: putfield mLastSystemUiVisibility : I
    //   2925: iload_2
    //   2926: istore #26
    //   2928: iload #15
    //   2930: istore #29
    //   2932: iload #16
    //   2934: istore #18
    //   2936: iload #38
    //   2938: istore #37
    //   2940: iload #43
    //   2942: istore #44
    //   2944: aload_0
    //   2945: aload_1
    //   2946: invokevirtual dispatchApplyInsets : (Landroid/view/View;)V
    //   2949: iconst_1
    //   2950: istore #11
    //   2952: iload #47
    //   2954: ifeq -> 3076
    //   2957: iload_2
    //   2958: istore #26
    //   2960: iload #15
    //   2962: istore #29
    //   2964: iload #16
    //   2966: istore #18
    //   2968: iload #11
    //   2970: istore #37
    //   2972: iload #45
    //   2974: istore #44
    //   2976: aload_0
    //   2977: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   2980: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   2983: ifnull -> 3076
    //   2986: iload_2
    //   2987: istore #26
    //   2989: iload #15
    //   2991: istore #29
    //   2993: iload #16
    //   2995: istore #18
    //   2997: iload #11
    //   2999: istore #37
    //   3001: iload #45
    //   3003: istore #44
    //   3005: aload_0
    //   3006: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   3009: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   3012: astore #13
    //   3014: iload_2
    //   3015: istore #26
    //   3017: iload #15
    //   3019: istore #29
    //   3021: iload #16
    //   3023: istore #18
    //   3025: iload #11
    //   3027: istore #37
    //   3029: iload #45
    //   3031: istore #44
    //   3033: aload_3
    //   3034: invokevirtual getColorMode : ()I
    //   3037: iconst_1
    //   3038: if_icmpne -> 3047
    //   3041: iconst_1
    //   3042: istore #38
    //   3044: goto -> 3050
    //   3047: iconst_0
    //   3048: istore #38
    //   3050: iload_2
    //   3051: istore #26
    //   3053: iload #15
    //   3055: istore #29
    //   3057: iload #16
    //   3059: istore #18
    //   3061: iload #11
    //   3063: istore #37
    //   3065: iload #45
    //   3067: istore #44
    //   3069: aload #13
    //   3071: iload #38
    //   3073: invokevirtual setWideGamut : (Z)V
    //   3076: iload_2
    //   3077: ifeq -> 3500
    //   3080: iload_2
    //   3081: istore #26
    //   3083: iload #15
    //   3085: istore #29
    //   3087: iload #16
    //   3089: istore #18
    //   3091: iload #11
    //   3093: istore #37
    //   3095: iload #45
    //   3097: istore #44
    //   3099: aload_0
    //   3100: iconst_1
    //   3101: putfield mFullRedrawNeeded : Z
    //   3104: iload_2
    //   3105: istore #26
    //   3107: iload #15
    //   3109: istore #29
    //   3111: iload #16
    //   3113: istore #18
    //   3115: iload #11
    //   3117: istore #37
    //   3119: iload #45
    //   3121: istore #44
    //   3123: aload_0
    //   3124: getfield mPreviousTransparentRegion : Landroid/graphics/Region;
    //   3127: invokevirtual setEmpty : ()V
    //   3130: iload_2
    //   3131: istore #26
    //   3133: iload #15
    //   3135: istore #29
    //   3137: iload #16
    //   3139: istore #18
    //   3141: iload #11
    //   3143: istore #37
    //   3145: iload #45
    //   3147: istore #44
    //   3149: aload_0
    //   3150: getfield mGraphicLock : Ljava/lang/Object;
    //   3153: astore #46
    //   3155: iload_2
    //   3156: istore #26
    //   3158: iload #15
    //   3160: istore #29
    //   3162: iload #16
    //   3164: istore #18
    //   3166: iload #11
    //   3168: istore #37
    //   3170: iload #45
    //   3172: istore #44
    //   3174: aload #46
    //   3176: monitorenter
    //   3177: iload #41
    //   3179: istore #37
    //   3181: aload_0
    //   3182: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   3185: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   3188: astore #13
    //   3190: iload #40
    //   3192: istore #38
    //   3194: aload #13
    //   3196: ifnull -> 3381
    //   3199: iload #41
    //   3201: istore #37
    //   3203: aload_0
    //   3204: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   3207: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   3210: aload_0
    //   3211: getfield mSurface : Landroid/view/Surface;
    //   3214: invokevirtual initialize : (Landroid/view/Surface;)Z
    //   3217: istore #38
    //   3219: iload #38
    //   3221: ifeq -> 3323
    //   3224: aload #9
    //   3226: astore #13
    //   3228: iload #10
    //   3230: istore #44
    //   3232: iload #38
    //   3234: istore #37
    //   3236: iload #6
    //   3238: istore #18
    //   3240: iload #11
    //   3242: istore #41
    //   3244: aload_1
    //   3245: getfield mPrivateFlags : I
    //   3248: sipush #512
    //   3251: iand
    //   3252: ifne -> 3323
    //   3255: aload #9
    //   3257: astore #13
    //   3259: iload #10
    //   3261: istore #44
    //   3263: iload #38
    //   3265: istore #37
    //   3267: iload #6
    //   3269: istore #18
    //   3271: iload #11
    //   3273: istore #41
    //   3275: aload_0
    //   3276: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   3279: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   3282: ifnull -> 3323
    //   3285: aload #9
    //   3287: astore #13
    //   3289: iload #10
    //   3291: istore #44
    //   3293: iload #38
    //   3295: istore #37
    //   3297: iload #6
    //   3299: istore #18
    //   3301: iload #11
    //   3303: istore #41
    //   3305: aload_0
    //   3306: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   3309: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   3312: invokevirtual allocateBuffers : ()V
    //   3315: goto -> 3323
    //   3318: astore #14
    //   3320: goto -> 3331
    //   3323: goto -> 3381
    //   3326: astore #14
    //   3328: iconst_0
    //   3329: istore #38
    //   3331: aload #9
    //   3333: astore #13
    //   3335: iload #10
    //   3337: istore #44
    //   3339: iload #38
    //   3341: istore #37
    //   3343: iload #6
    //   3345: istore #18
    //   3347: iload #11
    //   3349: istore #41
    //   3351: aload_0
    //   3352: aload #14
    //   3354: invokespecial handleOutOfResourcesException : (Landroid/view/Surface$OutOfResourcesException;)V
    //   3357: aload #9
    //   3359: astore #13
    //   3361: iload #10
    //   3363: istore #44
    //   3365: iload #38
    //   3367: istore #37
    //   3369: iload #6
    //   3371: istore #18
    //   3373: iload #11
    //   3375: istore #41
    //   3377: aload #46
    //   3379: monitorexit
    //   3380: return
    //   3381: iload #38
    //   3383: istore #37
    //   3385: aload #46
    //   3387: monitorexit
    //   3388: iload_2
    //   3389: istore #26
    //   3391: iload #15
    //   3393: istore #29
    //   3395: iload #16
    //   3397: istore #18
    //   3399: iload #11
    //   3401: istore #37
    //   3403: iload #38
    //   3405: istore #44
    //   3407: aload_0
    //   3408: invokespecial notifySurfaceCreated : ()V
    //   3411: goto -> 4085
    //   3414: astore #13
    //   3416: iload #37
    //   3418: istore #38
    //   3420: aload #13
    //   3422: astore #14
    //   3424: aload #9
    //   3426: astore #13
    //   3428: iload #10
    //   3430: istore #44
    //   3432: iload #38
    //   3434: istore #37
    //   3436: iload #6
    //   3438: istore #18
    //   3440: iload #11
    //   3442: istore #41
    //   3444: aload #46
    //   3446: monitorexit
    //   3447: aload #14
    //   3449: athrow
    //   3450: astore #9
    //   3452: iload #6
    //   3454: istore #18
    //   3456: iload #7
    //   3458: istore #6
    //   3460: iload #18
    //   3462: istore #7
    //   3464: iload #11
    //   3466: istore #37
    //   3468: iload #38
    //   3470: istore #11
    //   3472: goto -> 4829
    //   3475: astore #14
    //   3477: aload #13
    //   3479: astore #9
    //   3481: iload #44
    //   3483: istore #10
    //   3485: iload #37
    //   3487: istore #38
    //   3489: iload #18
    //   3491: istore #6
    //   3493: iload #41
    //   3495: istore #11
    //   3497: goto -> 3424
    //   3500: iload #15
    //   3502: ifeq -> 3846
    //   3505: iload_2
    //   3506: istore #26
    //   3508: iload #15
    //   3510: istore #29
    //   3512: iload #16
    //   3514: istore #18
    //   3516: iload #11
    //   3518: istore #37
    //   3520: iload #45
    //   3522: istore #44
    //   3524: aload_0
    //   3525: getfield mLastScrolledFocus : Ljava/lang/ref/WeakReference;
    //   3528: ifnull -> 3557
    //   3531: iload_2
    //   3532: istore #26
    //   3534: iload #15
    //   3536: istore #29
    //   3538: iload #16
    //   3540: istore #18
    //   3542: iload #11
    //   3544: istore #37
    //   3546: iload #45
    //   3548: istore #44
    //   3550: aload_0
    //   3551: getfield mLastScrolledFocus : Ljava/lang/ref/WeakReference;
    //   3554: invokevirtual clear : ()V
    //   3557: iload_2
    //   3558: istore #26
    //   3560: iload #15
    //   3562: istore #29
    //   3564: iload #16
    //   3566: istore #18
    //   3568: iload #11
    //   3570: istore #37
    //   3572: iload #45
    //   3574: istore #44
    //   3576: aload_0
    //   3577: iconst_0
    //   3578: putfield mCurScrollY : I
    //   3581: iload_2
    //   3582: istore #26
    //   3584: iload #15
    //   3586: istore #29
    //   3588: iload #16
    //   3590: istore #18
    //   3592: iload #11
    //   3594: istore #37
    //   3596: iload #45
    //   3598: istore #44
    //   3600: aload_0
    //   3601: iconst_0
    //   3602: putfield mScrollY : I
    //   3605: iload_2
    //   3606: istore #26
    //   3608: iload #15
    //   3610: istore #29
    //   3612: iload #16
    //   3614: istore #18
    //   3616: iload #11
    //   3618: istore #37
    //   3620: iload #45
    //   3622: istore #44
    //   3624: aload_0
    //   3625: getfield mView : Landroid/view/View;
    //   3628: instanceof com/android/internal/view/RootViewSurfaceTaker
    //   3631: ifeq -> 3666
    //   3634: iload_2
    //   3635: istore #26
    //   3637: iload #15
    //   3639: istore #29
    //   3641: iload #16
    //   3643: istore #18
    //   3645: iload #11
    //   3647: istore #37
    //   3649: iload #45
    //   3651: istore #44
    //   3653: aload_0
    //   3654: getfield mView : Landroid/view/View;
    //   3657: checkcast com/android/internal/view/RootViewSurfaceTaker
    //   3660: iconst_0
    //   3661: invokeinterface onRootViewScrollYChanged : (I)V
    //   3666: iload_2
    //   3667: istore #26
    //   3669: iload #15
    //   3671: istore #29
    //   3673: iload #16
    //   3675: istore #18
    //   3677: iload #11
    //   3679: istore #37
    //   3681: iload #45
    //   3683: istore #44
    //   3685: aload_0
    //   3686: getfield mScroller : Landroid/widget/Scroller;
    //   3689: ifnull -> 3718
    //   3692: iload_2
    //   3693: istore #26
    //   3695: iload #15
    //   3697: istore #29
    //   3699: iload #16
    //   3701: istore #18
    //   3703: iload #11
    //   3705: istore #37
    //   3707: iload #45
    //   3709: istore #44
    //   3711: aload_0
    //   3712: getfield mScroller : Landroid/widget/Scroller;
    //   3715: invokevirtual abortAnimation : ()V
    //   3718: iload_2
    //   3719: istore #26
    //   3721: iload #15
    //   3723: istore #29
    //   3725: iload #16
    //   3727: istore #18
    //   3729: iload #11
    //   3731: istore #37
    //   3733: iload #45
    //   3735: istore #44
    //   3737: iload #42
    //   3739: istore #38
    //   3741: aload_0
    //   3742: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   3745: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   3748: ifnull -> 4085
    //   3751: iload_2
    //   3752: istore #26
    //   3754: iload #15
    //   3756: istore #29
    //   3758: iload #16
    //   3760: istore #18
    //   3762: iload #11
    //   3764: istore #37
    //   3766: iload #45
    //   3768: istore #44
    //   3770: aload_0
    //   3771: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   3774: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   3777: astore #9
    //   3779: iload_2
    //   3780: istore #26
    //   3782: iload #15
    //   3784: istore #29
    //   3786: iload #16
    //   3788: istore #18
    //   3790: iload #11
    //   3792: istore #37
    //   3794: iload #45
    //   3796: istore #44
    //   3798: iload #42
    //   3800: istore #38
    //   3802: aload #9
    //   3804: invokevirtual isEnabled : ()Z
    //   3807: ifeq -> 4085
    //   3810: iload_2
    //   3811: istore #26
    //   3813: iload #15
    //   3815: istore #29
    //   3817: iload #16
    //   3819: istore #18
    //   3821: iload #11
    //   3823: istore #37
    //   3825: iload #45
    //   3827: istore #44
    //   3829: aload_0
    //   3830: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   3833: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   3836: invokevirtual destroy : ()V
    //   3839: iload #42
    //   3841: istore #38
    //   3843: goto -> 4085
    //   3846: iload #16
    //   3848: ifne -> 3870
    //   3851: iload #8
    //   3853: ifne -> 3870
    //   3856: iload #23
    //   3858: ifne -> 3870
    //   3861: iload #42
    //   3863: istore #38
    //   3865: iload #47
    //   3867: ifeq -> 4085
    //   3870: iload_2
    //   3871: istore #26
    //   3873: iload #15
    //   3875: istore #29
    //   3877: iload #16
    //   3879: istore #18
    //   3881: iload #11
    //   3883: istore #37
    //   3885: iload #43
    //   3887: istore #44
    //   3889: aload_0
    //   3890: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   3893: astore #9
    //   3895: iload #42
    //   3897: istore #38
    //   3899: aload #9
    //   3901: ifnonnull -> 4085
    //   3904: iload_2
    //   3905: istore #26
    //   3907: iload #15
    //   3909: istore #29
    //   3911: iload #16
    //   3913: istore #18
    //   3915: iload #11
    //   3917: istore #37
    //   3919: iload #45
    //   3921: istore #44
    //   3923: iload #42
    //   3925: istore #38
    //   3927: aload_0
    //   3928: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   3931: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   3934: ifnull -> 4085
    //   3937: iload_2
    //   3938: istore #26
    //   3940: iload #15
    //   3942: istore #29
    //   3944: iload #16
    //   3946: istore #18
    //   3948: iload #11
    //   3950: istore #37
    //   3952: iload #45
    //   3954: istore #44
    //   3956: aload_0
    //   3957: getfield mSurface : Landroid/view/Surface;
    //   3960: astore #9
    //   3962: iload_2
    //   3963: istore #26
    //   3965: iload #15
    //   3967: istore #29
    //   3969: iload #16
    //   3971: istore #18
    //   3973: iload #11
    //   3975: istore #37
    //   3977: iload #45
    //   3979: istore #44
    //   3981: iload #42
    //   3983: istore #38
    //   3985: aload #9
    //   3987: invokevirtual isValid : ()Z
    //   3990: ifeq -> 4085
    //   3993: iload_2
    //   3994: istore #26
    //   3996: iload #15
    //   3998: istore #29
    //   4000: iload #16
    //   4002: istore #18
    //   4004: iload #11
    //   4006: istore #37
    //   4008: iload #45
    //   4010: istore #44
    //   4012: aload_0
    //   4013: iconst_1
    //   4014: putfield mFullRedrawNeeded : Z
    //   4017: iload_2
    //   4018: istore #26
    //   4020: iload #15
    //   4022: istore #29
    //   4024: iload #16
    //   4026: istore #18
    //   4028: iload #11
    //   4030: istore #37
    //   4032: iload #45
    //   4034: istore #44
    //   4036: aload_0
    //   4037: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   4040: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   4043: aload_0
    //   4044: getfield mSurface : Landroid/view/Surface;
    //   4047: invokevirtual updateSurface : (Landroid/view/Surface;)V
    //   4050: iload #42
    //   4052: istore #38
    //   4054: goto -> 4085
    //   4057: astore #9
    //   4059: iload_2
    //   4060: istore #26
    //   4062: iload #15
    //   4064: istore #29
    //   4066: iload #16
    //   4068: istore #18
    //   4070: iload #11
    //   4072: istore #37
    //   4074: iload #45
    //   4076: istore #44
    //   4078: aload_0
    //   4079: aload #9
    //   4081: invokespecial handleOutOfResourcesException : (Landroid/view/Surface$OutOfResourcesException;)V
    //   4084: return
    //   4085: iload_2
    //   4086: ifne -> 4117
    //   4089: iload #16
    //   4091: ifeq -> 4117
    //   4094: iload_2
    //   4095: istore #26
    //   4097: iload #15
    //   4099: istore #29
    //   4101: iload #16
    //   4103: istore #18
    //   4105: iload #11
    //   4107: istore #37
    //   4109: iload #38
    //   4111: istore #44
    //   4113: aload_0
    //   4114: invokespecial notifySurfaceReplaced : ()V
    //   4117: iload #7
    //   4119: bipush #16
    //   4121: iand
    //   4122: ifeq -> 4131
    //   4125: iconst_1
    //   4126: istore #20
    //   4128: goto -> 4134
    //   4131: iconst_0
    //   4132: istore #20
    //   4134: iload #7
    //   4136: bipush #8
    //   4138: iand
    //   4139: ifeq -> 4148
    //   4142: iconst_1
    //   4143: istore #18
    //   4145: goto -> 4151
    //   4148: iconst_0
    //   4149: istore #18
    //   4151: iload #20
    //   4153: ifne -> 4170
    //   4156: iload #18
    //   4158: ifeq -> 4164
    //   4161: goto -> 4170
    //   4164: iconst_0
    //   4165: istore #41
    //   4167: goto -> 4173
    //   4170: iconst_1
    //   4171: istore #41
    //   4173: iload_2
    //   4174: istore #26
    //   4176: iload #15
    //   4178: istore #29
    //   4180: iload #16
    //   4182: istore #18
    //   4184: iload #11
    //   4186: istore #37
    //   4188: iload #38
    //   4190: istore #44
    //   4192: aload_0
    //   4193: getfield mDragResizing : Z
    //   4196: iload #41
    //   4198: if_icmpeq -> 4590
    //   4201: iload #41
    //   4203: ifeq -> 4583
    //   4206: iload #20
    //   4208: ifeq -> 4217
    //   4211: iconst_0
    //   4212: istore #20
    //   4214: goto -> 4220
    //   4217: iconst_1
    //   4218: istore #20
    //   4220: iload_2
    //   4221: istore #26
    //   4223: iload #15
    //   4225: istore #29
    //   4227: iload #16
    //   4229: istore #18
    //   4231: iload #11
    //   4233: istore #37
    //   4235: iload #38
    //   4237: istore #44
    //   4239: aload_0
    //   4240: iload #20
    //   4242: putfield mResizeMode : I
    //   4245: iload_2
    //   4246: istore #26
    //   4248: iload #15
    //   4250: istore #29
    //   4252: iload #16
    //   4254: istore #18
    //   4256: iload #11
    //   4258: istore #37
    //   4260: iload #38
    //   4262: istore #44
    //   4264: aload_0
    //   4265: getfield mWinFrame : Landroid/graphics/Rect;
    //   4268: astore #9
    //   4270: iload_2
    //   4271: istore #26
    //   4273: iload #15
    //   4275: istore #29
    //   4277: iload #16
    //   4279: istore #18
    //   4281: iload #11
    //   4283: istore #37
    //   4285: iload #38
    //   4287: istore #44
    //   4289: aload #9
    //   4291: invokevirtual width : ()I
    //   4294: istore #32
    //   4296: iload_2
    //   4297: istore #26
    //   4299: iload #15
    //   4301: istore #29
    //   4303: iload #16
    //   4305: istore #18
    //   4307: iload #11
    //   4309: istore #37
    //   4311: iload #38
    //   4313: istore #44
    //   4315: aload_0
    //   4316: getfield mPendingBackDropFrame : Landroid/graphics/Rect;
    //   4319: invokevirtual width : ()I
    //   4322: istore #20
    //   4324: iload #32
    //   4326: iload #20
    //   4328: if_icmpne -> 4423
    //   4331: iload_2
    //   4332: istore #26
    //   4334: iload #15
    //   4336: istore #29
    //   4338: iload #16
    //   4340: istore #18
    //   4342: iload #11
    //   4344: istore #37
    //   4346: iload #38
    //   4348: istore #44
    //   4350: aload_0
    //   4351: getfield mWinFrame : Landroid/graphics/Rect;
    //   4354: astore #9
    //   4356: iload_2
    //   4357: istore #26
    //   4359: iload #15
    //   4361: istore #29
    //   4363: iload #16
    //   4365: istore #18
    //   4367: iload #11
    //   4369: istore #37
    //   4371: iload #38
    //   4373: istore #44
    //   4375: aload #9
    //   4377: invokevirtual height : ()I
    //   4380: istore #32
    //   4382: iload_2
    //   4383: istore #26
    //   4385: iload #15
    //   4387: istore #29
    //   4389: iload #16
    //   4391: istore #18
    //   4393: iload #11
    //   4395: istore #37
    //   4397: iload #38
    //   4399: istore #44
    //   4401: aload_0
    //   4402: getfield mPendingBackDropFrame : Landroid/graphics/Rect;
    //   4405: invokevirtual height : ()I
    //   4408: istore #20
    //   4410: iload #32
    //   4412: iload #20
    //   4414: if_icmpne -> 4423
    //   4417: iconst_1
    //   4418: istore #20
    //   4420: goto -> 4426
    //   4423: iconst_0
    //   4424: istore #20
    //   4426: iload_2
    //   4427: istore #26
    //   4429: iload #15
    //   4431: istore #29
    //   4433: iload #16
    //   4435: istore #18
    //   4437: iload #11
    //   4439: istore #37
    //   4441: iload #38
    //   4443: istore #44
    //   4445: aload_0
    //   4446: getfield mPendingBackDropFrame : Landroid/graphics/Rect;
    //   4449: astore #9
    //   4451: iload #20
    //   4453: ifne -> 4462
    //   4456: iconst_1
    //   4457: istore #40
    //   4459: goto -> 4465
    //   4462: iconst_0
    //   4463: istore #40
    //   4465: iload_2
    //   4466: istore #26
    //   4468: iload #15
    //   4470: istore #29
    //   4472: iload #16
    //   4474: istore #18
    //   4476: iload #11
    //   4478: istore #37
    //   4480: iload #38
    //   4482: istore #44
    //   4484: aload_0
    //   4485: getfield mLastWindowInsets : Landroid/view/WindowInsets;
    //   4488: astore #13
    //   4490: iload_2
    //   4491: istore #26
    //   4493: iload #15
    //   4495: istore #29
    //   4497: iload #16
    //   4499: istore #18
    //   4501: iload #11
    //   4503: istore #37
    //   4505: iload #38
    //   4507: istore #44
    //   4509: aload #13
    //   4511: invokevirtual getSystemWindowInsets : ()Landroid/graphics/Insets;
    //   4514: invokevirtual toRect : ()Landroid/graphics/Rect;
    //   4517: astore #13
    //   4519: aload_0
    //   4520: getfield mLastWindowInsets : Landroid/view/WindowInsets;
    //   4523: astore #14
    //   4525: aload #14
    //   4527: invokevirtual getStableInsets : ()Landroid/graphics/Insets;
    //   4530: invokevirtual toRect : ()Landroid/graphics/Rect;
    //   4533: astore #14
    //   4535: aload_0
    //   4536: getfield mResizeMode : I
    //   4539: istore #18
    //   4541: aload_0
    //   4542: aload #9
    //   4544: iload #40
    //   4546: aload #13
    //   4548: aload #14
    //   4550: iload #18
    //   4552: invokespecial startDragResizing : (Landroid/graphics/Rect;ZLandroid/graphics/Rect;Landroid/graphics/Rect;I)V
    //   4555: goto -> 4590
    //   4558: astore #9
    //   4560: iload #6
    //   4562: istore #18
    //   4564: iload #7
    //   4566: istore #6
    //   4568: iload #18
    //   4570: istore #7
    //   4572: iload #11
    //   4574: istore #37
    //   4576: iload #38
    //   4578: istore #11
    //   4580: goto -> 4829
    //   4583: aload_0
    //   4584: invokespecial endDragResizing : ()V
    //   4587: goto -> 4590
    //   4590: iload #10
    //   4592: istore #37
    //   4594: aload_0
    //   4595: getfield mUseMTRenderer : Z
    //   4598: ifne -> 4641
    //   4601: iload #41
    //   4603: ifeq -> 4631
    //   4606: aload_0
    //   4607: aload_0
    //   4608: getfield mWinFrame : Landroid/graphics/Rect;
    //   4611: getfield left : I
    //   4614: putfield mCanvasOffsetX : I
    //   4617: aload_0
    //   4618: aload_0
    //   4619: getfield mWinFrame : Landroid/graphics/Rect;
    //   4622: getfield top : I
    //   4625: putfield mCanvasOffsetY : I
    //   4628: goto -> 4641
    //   4631: aload_0
    //   4632: iconst_0
    //   4633: putfield mCanvasOffsetY : I
    //   4636: aload_0
    //   4637: iconst_0
    //   4638: putfield mCanvasOffsetX : I
    //   4641: iload #7
    //   4643: istore #18
    //   4645: iload #6
    //   4647: istore #7
    //   4649: iload #37
    //   4651: istore #10
    //   4653: goto -> 4841
    //   4656: astore #9
    //   4658: iload #6
    //   4660: istore #18
    //   4662: iload #7
    //   4664: istore #6
    //   4666: iload #18
    //   4668: istore #7
    //   4670: iload #11
    //   4672: istore #37
    //   4674: iload #38
    //   4676: istore #11
    //   4678: goto -> 4829
    //   4681: astore #9
    //   4683: iload #6
    //   4685: istore_2
    //   4686: iload #7
    //   4688: istore #6
    //   4690: iload_2
    //   4691: istore #7
    //   4693: iload #26
    //   4695: istore_2
    //   4696: iload #29
    //   4698: istore #15
    //   4700: iload #18
    //   4702: istore #16
    //   4704: iload #44
    //   4706: istore #11
    //   4708: goto -> 4829
    //   4711: astore #9
    //   4713: iload #7
    //   4715: istore #6
    //   4717: iconst_0
    //   4718: istore #18
    //   4720: iload #8
    //   4722: istore #7
    //   4724: iload #18
    //   4726: istore #8
    //   4728: iload #44
    //   4730: istore #11
    //   4732: goto -> 4829
    //   4735: astore #9
    //   4737: iconst_0
    //   4738: istore #8
    //   4740: iconst_0
    //   4741: istore #7
    //   4743: iconst_0
    //   4744: istore #6
    //   4746: iload #44
    //   4748: istore #11
    //   4750: goto -> 4829
    //   4753: astore #9
    //   4755: goto -> 4760
    //   4758: astore #9
    //   4760: iload #10
    //   4762: istore #38
    //   4764: aload #13
    //   4766: monitorexit
    //   4767: aload #9
    //   4769: athrow
    //   4770: astore #9
    //   4772: iconst_0
    //   4773: istore #8
    //   4775: iconst_0
    //   4776: istore #7
    //   4778: iconst_0
    //   4779: istore #6
    //   4781: iload #44
    //   4783: istore #11
    //   4785: iload #38
    //   4787: istore #10
    //   4789: goto -> 4829
    //   4792: astore #9
    //   4794: goto -> 4760
    //   4797: astore #9
    //   4799: iconst_0
    //   4800: istore #8
    //   4802: iconst_0
    //   4803: istore #7
    //   4805: iconst_0
    //   4806: istore #6
    //   4808: iload #44
    //   4810: istore #11
    //   4812: goto -> 4829
    //   4815: astore #9
    //   4817: iconst_0
    //   4818: istore #11
    //   4820: iconst_0
    //   4821: istore #8
    //   4823: iconst_0
    //   4824: istore #7
    //   4826: iconst_0
    //   4827: istore #6
    //   4829: iload #11
    //   4831: istore #38
    //   4833: iload #37
    //   4835: istore #11
    //   4837: iload #6
    //   4839: istore #18
    //   4841: aload_0
    //   4842: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   4845: aload #12
    //   4847: getfield left : I
    //   4850: putfield mWindowLeft : I
    //   4853: aload_0
    //   4854: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   4857: aload #12
    //   4859: getfield top : I
    //   4862: putfield mWindowTop : I
    //   4865: aload_0
    //   4866: getfield mWidth : I
    //   4869: aload #12
    //   4871: invokevirtual width : ()I
    //   4874: if_icmpne -> 4889
    //   4877: aload_0
    //   4878: getfield mHeight : I
    //   4881: aload #12
    //   4883: invokevirtual height : ()I
    //   4886: if_icmpeq -> 4907
    //   4889: aload_0
    //   4890: aload #12
    //   4892: invokevirtual width : ()I
    //   4895: putfield mWidth : I
    //   4898: aload_0
    //   4899: aload #12
    //   4901: invokevirtual height : ()I
    //   4904: putfield mHeight : I
    //   4907: aload_0
    //   4908: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   4911: ifnull -> 5238
    //   4914: aload_0
    //   4915: getfield mSurface : Landroid/view/Surface;
    //   4918: invokevirtual isValid : ()Z
    //   4921: ifeq -> 4935
    //   4924: aload_0
    //   4925: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   4928: aload_0
    //   4929: getfield mSurface : Landroid/view/Surface;
    //   4932: putfield mSurface : Landroid/view/Surface;
    //   4935: aload_0
    //   4936: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   4939: aload_0
    //   4940: getfield mWidth : I
    //   4943: aload_0
    //   4944: getfield mHeight : I
    //   4947: invokevirtual setSurfaceFrameSize : (II)V
    //   4950: aload_0
    //   4951: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   4954: getfield mSurfaceLock : Ljava/util/concurrent/locks/ReentrantLock;
    //   4957: invokevirtual unlock : ()V
    //   4960: iload_2
    //   4961: ifeq -> 5029
    //   4964: aload_0
    //   4965: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   4968: invokevirtual ungetCallbacks : ()V
    //   4971: aload_0
    //   4972: iconst_1
    //   4973: putfield mIsCreating : Z
    //   4976: aload_0
    //   4977: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   4980: invokevirtual getCallbacks : ()[Landroid/view/SurfaceHolder$Callback;
    //   4983: astore #9
    //   4985: aload #9
    //   4987: ifnull -> 5029
    //   4990: aload #9
    //   4992: arraylength
    //   4993: istore #29
    //   4995: iconst_0
    //   4996: istore #6
    //   4998: iload #6
    //   5000: iload #29
    //   5002: if_icmpge -> 5029
    //   5005: aload #9
    //   5007: iload #6
    //   5009: aaload
    //   5010: astore #13
    //   5012: aload #13
    //   5014: aload_0
    //   5015: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   5018: invokeinterface surfaceCreated : (Landroid/view/SurfaceHolder;)V
    //   5023: iinc #6, 1
    //   5026: goto -> 4998
    //   5029: iload_2
    //   5030: ifne -> 5058
    //   5033: iload #16
    //   5035: ifne -> 5058
    //   5038: iload #8
    //   5040: ifne -> 5058
    //   5043: iload #33
    //   5045: ifeq -> 5051
    //   5048: goto -> 5058
    //   5051: iload #8
    //   5053: istore #6
    //   5055: goto -> 5160
    //   5058: aload_0
    //   5059: getfield mSurface : Landroid/view/Surface;
    //   5062: astore #9
    //   5064: aload #9
    //   5066: invokevirtual isValid : ()Z
    //   5069: ifeq -> 5156
    //   5072: aload_0
    //   5073: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   5076: invokevirtual getCallbacks : ()[Landroid/view/SurfaceHolder$Callback;
    //   5079: astore #9
    //   5081: aload #9
    //   5083: ifnull -> 5144
    //   5086: aload #9
    //   5088: arraylength
    //   5089: istore #6
    //   5091: iconst_0
    //   5092: istore #29
    //   5094: iload #29
    //   5096: iload #6
    //   5098: if_icmpge -> 5137
    //   5101: aload #9
    //   5103: iload #29
    //   5105: aaload
    //   5106: astore #13
    //   5108: aload #13
    //   5110: aload_0
    //   5111: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   5114: aload_3
    //   5115: getfield format : I
    //   5118: aload_0
    //   5119: getfield mWidth : I
    //   5122: aload_0
    //   5123: getfield mHeight : I
    //   5126: invokeinterface surfaceChanged : (Landroid/view/SurfaceHolder;III)V
    //   5131: iinc #29, 1
    //   5134: goto -> 5094
    //   5137: iload #8
    //   5139: istore #6
    //   5141: goto -> 5148
    //   5144: iload #8
    //   5146: istore #6
    //   5148: aload_0
    //   5149: iconst_0
    //   5150: putfield mIsCreating : Z
    //   5153: goto -> 5160
    //   5156: iload #8
    //   5158: istore #6
    //   5160: iload #6
    //   5162: istore #8
    //   5164: iload #15
    //   5166: ifeq -> 5238
    //   5169: aload_0
    //   5170: invokespecial notifyHolderSurfaceDestroyed : ()V
    //   5173: aload_0
    //   5174: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   5177: getfield mSurfaceLock : Ljava/util/concurrent/locks/ReentrantLock;
    //   5180: invokevirtual lock : ()V
    //   5183: aload_0
    //   5184: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   5187: astore #13
    //   5189: new android/view/Surface
    //   5192: astore #9
    //   5194: aload #9
    //   5196: invokespecial <init> : ()V
    //   5199: aload #13
    //   5201: aload #9
    //   5203: putfield mSurface : Landroid/view/Surface;
    //   5206: aload_0
    //   5207: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   5210: getfield mSurfaceLock : Ljava/util/concurrent/locks/ReentrantLock;
    //   5213: invokevirtual unlock : ()V
    //   5216: iload #6
    //   5218: istore #8
    //   5220: goto -> 5238
    //   5223: astore #9
    //   5225: aload_0
    //   5226: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   5229: getfield mSurfaceLock : Ljava/util/concurrent/locks/ReentrantLock;
    //   5232: invokevirtual unlock : ()V
    //   5235: aload #9
    //   5237: athrow
    //   5238: aload_0
    //   5239: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   5242: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   5245: astore #9
    //   5247: aload #9
    //   5249: ifnull -> 5333
    //   5252: aload #9
    //   5254: invokevirtual isEnabled : ()Z
    //   5257: ifeq -> 5333
    //   5260: iload #38
    //   5262: ifne -> 5304
    //   5265: aload_0
    //   5266: getfield mWidth : I
    //   5269: istore #6
    //   5271: iload #6
    //   5273: aload #9
    //   5275: invokevirtual getWidth : ()I
    //   5278: if_icmpne -> 5304
    //   5281: aload_0
    //   5282: getfield mHeight : I
    //   5285: istore #6
    //   5287: iload #6
    //   5289: aload #9
    //   5291: invokevirtual getHeight : ()I
    //   5294: if_icmpne -> 5304
    //   5297: aload_0
    //   5298: getfield mNeedsRendererSetup : Z
    //   5301: ifeq -> 5333
    //   5304: aload #9
    //   5306: aload_0
    //   5307: getfield mWidth : I
    //   5310: aload_0
    //   5311: getfield mHeight : I
    //   5314: aload_0
    //   5315: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   5318: aload_0
    //   5319: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   5322: getfield surfaceInsets : Landroid/graphics/Rect;
    //   5325: invokevirtual setup : (IILandroid/view/View$AttachInfo;Landroid/graphics/Rect;)V
    //   5328: aload_0
    //   5329: iconst_0
    //   5330: putfield mNeedsRendererSetup : Z
    //   5333: aload_0
    //   5334: getfield mStopped : Z
    //   5337: ifeq -> 5347
    //   5340: aload_0
    //   5341: getfield mReportNextDraw : Z
    //   5344: ifeq -> 5415
    //   5347: iload #18
    //   5349: iconst_1
    //   5350: iand
    //   5351: ifeq -> 5360
    //   5354: iconst_1
    //   5355: istore #38
    //   5357: goto -> 5363
    //   5360: iconst_0
    //   5361: istore #38
    //   5363: aload_0
    //   5364: iload #38
    //   5366: invokespecial ensureTouchModeLocally : (Z)Z
    //   5369: istore #38
    //   5371: iload #38
    //   5373: ifne -> 5418
    //   5376: aload_0
    //   5377: getfield mWidth : I
    //   5380: aload_1
    //   5381: invokevirtual getMeasuredWidth : ()I
    //   5384: if_icmpne -> 5418
    //   5387: aload_0
    //   5388: getfield mHeight : I
    //   5391: istore #6
    //   5393: iload #6
    //   5395: aload_1
    //   5396: invokevirtual getMeasuredHeight : ()I
    //   5399: if_icmpne -> 5418
    //   5402: iload #11
    //   5404: ifne -> 5418
    //   5407: iload #7
    //   5409: ifeq -> 5415
    //   5412: goto -> 5418
    //   5415: goto -> 5777
    //   5418: aload_0
    //   5419: getfield mWidth : I
    //   5422: aload_3
    //   5423: getfield width : I
    //   5426: invokestatic getRootMeasureSpec : (II)I
    //   5429: istore #17
    //   5431: aload_0
    //   5432: getfield mHeight : I
    //   5435: aload_3
    //   5436: getfield height : I
    //   5439: invokestatic getRootMeasureSpec : (II)I
    //   5442: istore #20
    //   5444: getstatic android/view/ViewRootImpl.DEBUG_LAYOUT : Z
    //   5447: ifeq -> 5576
    //   5450: aload_0
    //   5451: getfield mTag : Ljava/lang/String;
    //   5454: astore #9
    //   5456: new java/lang/StringBuilder
    //   5459: dup
    //   5460: invokespecial <init> : ()V
    //   5463: astore #13
    //   5465: aload #13
    //   5467: ldc_w 'Ooops, something changed!  mWidth='
    //   5470: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5473: pop
    //   5474: aload #13
    //   5476: aload_0
    //   5477: getfield mWidth : I
    //   5480: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   5483: pop
    //   5484: aload #13
    //   5486: ldc_w ' measuredWidth='
    //   5489: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5492: pop
    //   5493: aload #13
    //   5495: aload_1
    //   5496: invokevirtual getMeasuredWidth : ()I
    //   5499: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   5502: pop
    //   5503: aload #13
    //   5505: ldc_w ' mHeight='
    //   5508: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5511: pop
    //   5512: aload #13
    //   5514: aload_0
    //   5515: getfield mHeight : I
    //   5518: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   5521: pop
    //   5522: aload #13
    //   5524: ldc_w ' measuredHeight='
    //   5527: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5530: pop
    //   5531: aload #13
    //   5533: aload_1
    //   5534: invokevirtual getMeasuredHeight : ()I
    //   5537: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   5540: pop
    //   5541: aload #13
    //   5543: ldc_w ' dispatchApplyInsets='
    //   5546: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5549: pop
    //   5550: aload #13
    //   5552: iload #11
    //   5554: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   5557: pop
    //   5558: aload #13
    //   5560: invokevirtual toString : ()Ljava/lang/String;
    //   5563: astore #13
    //   5565: aload #9
    //   5567: aload #13
    //   5569: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   5572: pop
    //   5573: goto -> 5576
    //   5576: aload_0
    //   5577: iload #17
    //   5579: iload #20
    //   5581: invokespecial performMeasure : (II)V
    //   5584: aload_1
    //   5585: invokevirtual getMeasuredWidth : ()I
    //   5588: istore #26
    //   5590: aload_1
    //   5591: invokevirtual getMeasuredHeight : ()I
    //   5594: istore #32
    //   5596: iconst_0
    //   5597: istore #6
    //   5599: iload #26
    //   5601: istore #29
    //   5603: aload_3
    //   5604: getfield horizontalWeight : F
    //   5607: fconst_0
    //   5608: fcmpl
    //   5609: ifle -> 5644
    //   5612: iload #26
    //   5614: aload_0
    //   5615: getfield mWidth : I
    //   5618: iload #26
    //   5620: isub
    //   5621: i2f
    //   5622: aload_3
    //   5623: getfield horizontalWeight : F
    //   5626: fmul
    //   5627: f2i
    //   5628: iadd
    //   5629: istore #29
    //   5631: iload #29
    //   5633: ldc_w 1073741824
    //   5636: invokestatic makeMeasureSpec : (II)I
    //   5639: istore #17
    //   5641: iconst_1
    //   5642: istore #6
    //   5644: iload #32
    //   5646: istore #26
    //   5648: aload_3
    //   5649: getfield verticalWeight : F
    //   5652: fconst_0
    //   5653: fcmpl
    //   5654: ifle -> 5689
    //   5657: iload #32
    //   5659: aload_0
    //   5660: getfield mHeight : I
    //   5663: iload #32
    //   5665: isub
    //   5666: i2f
    //   5667: aload_3
    //   5668: getfield verticalWeight : F
    //   5671: fmul
    //   5672: f2i
    //   5673: iadd
    //   5674: istore #26
    //   5676: iload #26
    //   5678: ldc_w 1073741824
    //   5681: invokestatic makeMeasureSpec : (II)I
    //   5684: istore #20
    //   5686: iconst_1
    //   5687: istore #6
    //   5689: iload #6
    //   5691: ifeq -> 5774
    //   5694: getstatic android/view/ViewRootImpl.DEBUG_LAYOUT : Z
    //   5697: ifeq -> 5763
    //   5700: aload_0
    //   5701: getfield mTag : Ljava/lang/String;
    //   5704: astore #9
    //   5706: new java/lang/StringBuilder
    //   5709: dup
    //   5710: invokespecial <init> : ()V
    //   5713: astore #13
    //   5715: aload #13
    //   5717: ldc_w 'And hey let's measure once more: width='
    //   5720: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5723: pop
    //   5724: aload #13
    //   5726: iload #29
    //   5728: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   5731: pop
    //   5732: aload #13
    //   5734: ldc_w ' height='
    //   5737: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   5740: pop
    //   5741: aload #13
    //   5743: iload #26
    //   5745: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   5748: pop
    //   5749: aload #9
    //   5751: aload #13
    //   5753: invokevirtual toString : ()Ljava/lang/String;
    //   5756: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   5759: pop
    //   5760: goto -> 5763
    //   5763: aload_0
    //   5764: iload #17
    //   5766: iload #20
    //   5768: invokespecial performMeasure : (II)V
    //   5771: goto -> 5774
    //   5774: iconst_1
    //   5775: istore #17
    //   5777: iload #7
    //   5779: istore #6
    //   5781: iload #18
    //   5783: istore #7
    //   5785: iload #8
    //   5787: ifne -> 5804
    //   5790: iload #16
    //   5792: ifne -> 5804
    //   5795: iload_2
    //   5796: ifne -> 5804
    //   5799: iload #33
    //   5801: ifeq -> 5808
    //   5804: aload_0
    //   5805: invokespecial updateBoundsLayer : ()V
    //   5808: iload #17
    //   5810: ifeq -> 5833
    //   5813: aload_0
    //   5814: getfield mStopped : Z
    //   5817: ifeq -> 5827
    //   5820: aload_0
    //   5821: getfield mReportNextDraw : Z
    //   5824: ifeq -> 5833
    //   5827: iconst_1
    //   5828: istore #6
    //   5830: goto -> 5836
    //   5833: iconst_0
    //   5834: istore #6
    //   5836: iload #6
    //   5838: ifne -> 5860
    //   5841: aload_0
    //   5842: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   5845: getfield mRecomputeGlobalAttributes : Z
    //   5848: ifeq -> 5854
    //   5851: goto -> 5860
    //   5854: iconst_0
    //   5855: istore #8
    //   5857: goto -> 5863
    //   5860: iconst_1
    //   5861: istore #8
    //   5863: iload #6
    //   5865: ifeq -> 6099
    //   5868: aload_0
    //   5869: aload_3
    //   5870: aload_0
    //   5871: getfield mWidth : I
    //   5874: aload_0
    //   5875: getfield mHeight : I
    //   5878: invokespecial performLayout : (Landroid/view/WindowManager$LayoutParams;II)V
    //   5881: aload_0
    //   5882: getfield mView : Landroid/view/View;
    //   5885: instanceof android/view/ViewGroup
    //   5888: ifeq -> 5938
    //   5891: ldc2_w 8
    //   5894: ldc_w 'setRefreshRateIfNeed'
    //   5897: invokestatic traceBegin : (JLjava/lang/String;)V
    //   5900: getstatic com/oplus/screenmode/IOplusScreenModeFeature.DEFAULT : Lcom/oplus/screenmode/IOplusScreenModeFeature;
    //   5903: invokestatic get : (Landroid/common/IOplusCommonFeature;)Landroid/common/IOplusCommonFeature;
    //   5906: checkcast com/oplus/screenmode/IOplusScreenModeFeature
    //   5909: aload_0
    //   5910: getfield mContext : Landroid/content/Context;
    //   5913: aload_0
    //   5914: getfield mView : Landroid/view/View;
    //   5917: checkcast android/view/ViewGroup
    //   5920: aload_0
    //   5921: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   5924: getfield token : Landroid/os/IBinder;
    //   5927: invokeinterface setRefreshRateIfNeed : (Landroid/content/Context;Landroid/view/ViewGroup;Landroid/os/IBinder;)V
    //   5932: ldc2_w 8
    //   5935: invokestatic traceEnd : (J)V
    //   5938: aload_1
    //   5939: getfield mPrivateFlags : I
    //   5942: sipush #512
    //   5945: iand
    //   5946: ifeq -> 6096
    //   5949: aload_1
    //   5950: aload_0
    //   5951: getfield mTmpLocation : [I
    //   5954: invokevirtual getLocationInWindow : ([I)V
    //   5957: aload_0
    //   5958: getfield mTransparentRegion : Landroid/graphics/Region;
    //   5961: astore_3
    //   5962: aload_0
    //   5963: getfield mTmpLocation : [I
    //   5966: astore #9
    //   5968: aload_3
    //   5969: aload #9
    //   5971: iconst_0
    //   5972: iaload
    //   5973: aload #9
    //   5975: iconst_1
    //   5976: iaload
    //   5977: aload #9
    //   5979: iconst_0
    //   5980: iaload
    //   5981: aload_1
    //   5982: getfield mRight : I
    //   5985: iadd
    //   5986: aload_1
    //   5987: getfield mLeft : I
    //   5990: isub
    //   5991: aload_0
    //   5992: getfield mTmpLocation : [I
    //   5995: iconst_1
    //   5996: iaload
    //   5997: aload_1
    //   5998: getfield mBottom : I
    //   6001: iadd
    //   6002: aload_1
    //   6003: getfield mTop : I
    //   6006: isub
    //   6007: invokevirtual set : (IIII)Z
    //   6010: pop
    //   6011: aload_1
    //   6012: aload_0
    //   6013: getfield mTransparentRegion : Landroid/graphics/Region;
    //   6016: invokevirtual gatherTransparentRegion : (Landroid/graphics/Region;)Z
    //   6019: pop
    //   6020: aload_0
    //   6021: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   6024: astore #9
    //   6026: aload #9
    //   6028: ifnull -> 6040
    //   6031: aload #9
    //   6033: aload_0
    //   6034: getfield mTransparentRegion : Landroid/graphics/Region;
    //   6037: invokevirtual translateRegionInWindowToScreen : (Landroid/graphics/Region;)V
    //   6040: aload_0
    //   6041: getfield mTransparentRegion : Landroid/graphics/Region;
    //   6044: aload_0
    //   6045: getfield mPreviousTransparentRegion : Landroid/graphics/Region;
    //   6048: invokevirtual equals : (Ljava/lang/Object;)Z
    //   6051: ifne -> 6099
    //   6054: aload_0
    //   6055: getfield mPreviousTransparentRegion : Landroid/graphics/Region;
    //   6058: aload_0
    //   6059: getfield mTransparentRegion : Landroid/graphics/Region;
    //   6062: invokevirtual set : (Landroid/graphics/Region;)Z
    //   6065: pop
    //   6066: aload_0
    //   6067: iconst_1
    //   6068: putfield mFullRedrawNeeded : Z
    //   6071: aload_0
    //   6072: getfield mWindowSession : Landroid/view/IWindowSession;
    //   6075: aload_0
    //   6076: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   6079: aload_0
    //   6080: getfield mTransparentRegion : Landroid/graphics/Region;
    //   6083: invokeinterface setTransparentRegion : (Landroid/view/IWindow;Landroid/graphics/Region;)V
    //   6088: goto -> 6099
    //   6091: astore #9
    //   6093: goto -> 6099
    //   6096: goto -> 6099
    //   6099: iload #15
    //   6101: ifeq -> 6108
    //   6104: aload_0
    //   6105: invokespecial notifySurfaceDestroyed : ()V
    //   6108: iload #8
    //   6110: ifeq -> 6131
    //   6113: aload_0
    //   6114: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   6117: iconst_0
    //   6118: putfield mRecomputeGlobalAttributes : Z
    //   6121: aload_0
    //   6122: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   6125: getfield mTreeObserver : Landroid/view/ViewTreeObserver;
    //   6128: invokevirtual dispatchOnGlobalLayout : ()V
    //   6131: iload #19
    //   6133: ifeq -> 6325
    //   6136: aload_0
    //   6137: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   6140: getfield mGivenInternalInsets : Landroid/view/ViewTreeObserver$InternalInsetsInfo;
    //   6143: astore #14
    //   6145: aload #14
    //   6147: invokevirtual reset : ()V
    //   6150: aload_0
    //   6151: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   6154: getfield mTreeObserver : Landroid/view/ViewTreeObserver;
    //   6157: aload #14
    //   6159: invokevirtual dispatchOnComputeInternalInsets : (Landroid/view/ViewTreeObserver$InternalInsetsInfo;)V
    //   6162: aload_0
    //   6163: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   6166: aload #14
    //   6168: invokevirtual isEmpty : ()Z
    //   6171: iconst_1
    //   6172: ixor
    //   6173: putfield mHasNonEmptyGivenInternalInsets : Z
    //   6176: iload #10
    //   6178: ifne -> 6199
    //   6181: aload_0
    //   6182: getfield mLastGivenInsets : Landroid/view/ViewTreeObserver$InternalInsetsInfo;
    //   6185: aload #14
    //   6187: invokevirtual equals : (Ljava/lang/Object;)Z
    //   6190: ifne -> 6196
    //   6193: goto -> 6199
    //   6196: goto -> 6325
    //   6199: aload_0
    //   6200: getfield mLastGivenInsets : Landroid/view/ViewTreeObserver$InternalInsetsInfo;
    //   6203: aload #14
    //   6205: invokevirtual set : (Landroid/view/ViewTreeObserver$InternalInsetsInfo;)V
    //   6208: aload_0
    //   6209: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   6212: astore #9
    //   6214: aload #9
    //   6216: ifnull -> 6261
    //   6219: aload #9
    //   6221: aload #14
    //   6223: getfield contentInsets : Landroid/graphics/Rect;
    //   6226: invokevirtual getTranslatedContentInsets : (Landroid/graphics/Rect;)Landroid/graphics/Rect;
    //   6229: astore_3
    //   6230: aload_0
    //   6231: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   6234: aload #14
    //   6236: getfield visibleInsets : Landroid/graphics/Rect;
    //   6239: invokevirtual getTranslatedVisibleInsets : (Landroid/graphics/Rect;)Landroid/graphics/Rect;
    //   6242: astore #9
    //   6244: aload_0
    //   6245: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   6248: aload #14
    //   6250: getfield touchableRegion : Landroid/graphics/Region;
    //   6253: invokevirtual getTranslatedTouchableArea : (Landroid/graphics/Region;)Landroid/graphics/Region;
    //   6256: astore #13
    //   6258: goto -> 6281
    //   6261: aload #14
    //   6263: getfield contentInsets : Landroid/graphics/Rect;
    //   6266: astore_3
    //   6267: aload #14
    //   6269: getfield visibleInsets : Landroid/graphics/Rect;
    //   6272: astore #9
    //   6274: aload #14
    //   6276: getfield touchableRegion : Landroid/graphics/Region;
    //   6279: astore #13
    //   6281: aload_0
    //   6282: getfield mWindowSession : Landroid/view/IWindowSession;
    //   6285: astore #12
    //   6287: aload_0
    //   6288: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   6291: astore #46
    //   6293: aload #12
    //   6295: aload #46
    //   6297: aload #14
    //   6299: getfield mTouchableInsets : I
    //   6302: aload_3
    //   6303: aload #9
    //   6305: aload #13
    //   6307: invokeinterface setInsets : (Landroid/view/IWindow;ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Region;)V
    //   6312: goto -> 6325
    //   6315: astore #9
    //   6317: goto -> 6325
    //   6320: astore #9
    //   6322: goto -> 6325
    //   6325: aload_0
    //   6326: getfield mFirst : Z
    //   6329: ifeq -> 6531
    //   6332: aload_0
    //   6333: getfield mView : Landroid/view/View;
    //   6336: ifnull -> 6531
    //   6339: getstatic android/view/ViewRootImpl.sAlwaysAssignFocus : Z
    //   6342: ifne -> 6394
    //   6345: invokestatic isInTouchMode : ()Z
    //   6348: ifne -> 6354
    //   6351: goto -> 6394
    //   6354: aload_0
    //   6355: getfield mView : Landroid/view/View;
    //   6358: invokevirtual findFocus : ()Landroid/view/View;
    //   6361: astore_3
    //   6362: aload_3
    //   6363: instanceof android/view/ViewGroup
    //   6366: ifeq -> 6421
    //   6369: aload_3
    //   6370: checkcast android/view/ViewGroup
    //   6373: astore #9
    //   6375: aload #9
    //   6377: invokevirtual getDescendantFocusability : ()I
    //   6380: ldc_w 262144
    //   6383: if_icmpne -> 6421
    //   6386: aload_3
    //   6387: invokevirtual restoreDefaultFocus : ()Z
    //   6390: pop
    //   6391: goto -> 6421
    //   6394: aload_0
    //   6395: getfield mView : Landroid/view/View;
    //   6398: astore #9
    //   6400: aload #9
    //   6402: ifnull -> 6421
    //   6405: aload #9
    //   6407: invokevirtual hasFocus : ()Z
    //   6410: ifne -> 6421
    //   6413: aload_0
    //   6414: getfield mView : Landroid/view/View;
    //   6417: invokevirtual restoreDefaultFocus : ()Z
    //   6420: pop
    //   6421: aload_0
    //   6422: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   6425: astore #9
    //   6427: aload #9
    //   6429: ifnull -> 6482
    //   6432: aload #9
    //   6434: invokevirtual getTitle : ()Ljava/lang/CharSequence;
    //   6437: ifnull -> 6482
    //   6440: getstatic com/oplus/rp/bridge/IOplusRedPacketManager.DEFAULT : Lcom/oplus/rp/bridge/IOplusRedPacketManager;
    //   6443: astore #9
    //   6445: aload #9
    //   6447: iconst_0
    //   6448: anewarray java/lang/Object
    //   6451: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   6454: checkcast com/oplus/rp/bridge/IOplusRedPacketManager
    //   6457: aload_0
    //   6458: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   6461: invokevirtual getTitle : ()Ljava/lang/CharSequence;
    //   6464: invokeinterface toString : ()Ljava/lang/String;
    //   6469: invokeinterface isRPActivities : (Ljava/lang/String;)Z
    //   6474: ifeq -> 6482
    //   6477: aload_0
    //   6478: iconst_1
    //   6479: putfield mIsLuckyMoneyView : Z
    //   6482: aload_0
    //   6483: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   6486: astore #9
    //   6488: aload #9
    //   6490: ifnull -> 6531
    //   6493: aload #9
    //   6495: invokevirtual getTitle : ()Ljava/lang/CharSequence;
    //   6498: ifnull -> 6531
    //   6501: aload_0
    //   6502: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   6505: astore #9
    //   6507: aload #9
    //   6509: invokevirtual getTitle : ()Ljava/lang/CharSequence;
    //   6512: invokeinterface toString : ()Ljava/lang/String;
    //   6517: ldc_w 'com.tencent.mm.ui.LauncherUI'
    //   6520: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   6523: ifeq -> 6531
    //   6526: aload_0
    //   6527: iconst_1
    //   6528: putfield mIsWeixinLauncherUI : Z
    //   6531: iload #5
    //   6533: ifne -> 6543
    //   6536: aload_0
    //   6537: getfield mFirst : Z
    //   6540: ifeq -> 6554
    //   6543: iload #22
    //   6545: ifeq -> 6554
    //   6548: iconst_1
    //   6549: istore #6
    //   6551: goto -> 6557
    //   6554: iconst_0
    //   6555: istore #6
    //   6557: aload_0
    //   6558: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   6561: getfield mHasWindowFocus : Z
    //   6564: ifeq -> 6578
    //   6567: iload #22
    //   6569: ifeq -> 6578
    //   6572: iconst_1
    //   6573: istore #10
    //   6575: goto -> 6581
    //   6578: iconst_0
    //   6579: istore #10
    //   6581: iload #10
    //   6583: ifeq -> 6599
    //   6586: aload_0
    //   6587: getfield mLostWindowFocus : Z
    //   6590: ifeq -> 6599
    //   6593: iconst_1
    //   6594: istore #8
    //   6596: goto -> 6602
    //   6599: iconst_0
    //   6600: istore #8
    //   6602: iload #8
    //   6604: ifeq -> 6615
    //   6607: aload_0
    //   6608: iconst_0
    //   6609: putfield mLostWindowFocus : Z
    //   6612: goto -> 6632
    //   6615: iload #10
    //   6617: ifne -> 6632
    //   6620: aload_0
    //   6621: getfield mHadWindowFocus : Z
    //   6624: ifeq -> 6632
    //   6627: aload_0
    //   6628: iconst_1
    //   6629: putfield mLostWindowFocus : Z
    //   6632: iload #6
    //   6634: ifne -> 6642
    //   6637: iload #8
    //   6639: ifeq -> 6690
    //   6642: aload_0
    //   6643: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   6646: astore #9
    //   6648: aload #9
    //   6650: ifnonnull -> 6659
    //   6653: iconst_0
    //   6654: istore #6
    //   6656: goto -> 6679
    //   6659: aload #9
    //   6661: getfield type : I
    //   6664: sipush #2005
    //   6667: if_icmpne -> 6676
    //   6670: iconst_1
    //   6671: istore #6
    //   6673: goto -> 6679
    //   6676: iconst_0
    //   6677: istore #6
    //   6679: iload #6
    //   6681: ifne -> 6690
    //   6684: aload_1
    //   6685: bipush #32
    //   6687: invokevirtual sendAccessibilityEvent : (I)V
    //   6690: aload_0
    //   6691: iconst_0
    //   6692: putfield mWillDrawSoon : Z
    //   6695: aload_0
    //   6696: iconst_0
    //   6697: putfield mNewSurfaceNeeded : Z
    //   6700: aload_0
    //   6701: iconst_0
    //   6702: putfield mActivityRelaunched : Z
    //   6705: aload_0
    //   6706: iload #4
    //   6708: putfield mViewVisibility : I
    //   6711: aload_0
    //   6712: iload #10
    //   6714: putfield mHadWindowFocus : Z
    //   6717: aload_0
    //   6718: getfield mImeFocusController : Landroid/view/ImeFocusController;
    //   6721: iload #10
    //   6723: aload_0
    //   6724: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   6727: invokevirtual onTraversal : (ZLandroid/view/WindowManager$LayoutParams;)V
    //   6730: iload #7
    //   6732: iconst_2
    //   6733: iand
    //   6734: ifeq -> 6741
    //   6737: aload_0
    //   6738: invokespecial reportNextDraw : ()V
    //   6741: iload #7
    //   6743: sipush #128
    //   6746: iand
    //   6747: ifeq -> 6766
    //   6750: aload_0
    //   6751: invokespecial reportNextDraw : ()V
    //   6754: aload_0
    //   6755: invokevirtual setUseBLASTSyncTransaction : ()V
    //   6758: aload_0
    //   6759: iconst_1
    //   6760: putfield mSendNextFrameToWm : Z
    //   6763: goto -> 6766
    //   6766: iconst_1
    //   6767: istore #7
    //   6769: iload #7
    //   6771: istore #6
    //   6773: aload_0
    //   6774: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   6777: getfield mTreeObserver : Landroid/view/ViewTreeObserver;
    //   6780: invokevirtual dispatchOnPreDraw : ()Z
    //   6783: ifne -> 6801
    //   6786: iload #22
    //   6788: ifne -> 6798
    //   6791: iload #7
    //   6793: istore #6
    //   6795: goto -> 6801
    //   6798: iconst_0
    //   6799: istore #6
    //   6801: iload #6
    //   6803: ifne -> 6875
    //   6806: aload_0
    //   6807: getfield mPendingTransitions : Ljava/util/ArrayList;
    //   6810: astore #9
    //   6812: aload #9
    //   6814: ifnull -> 6868
    //   6817: aload #9
    //   6819: invokevirtual size : ()I
    //   6822: ifle -> 6868
    //   6825: iconst_0
    //   6826: istore #6
    //   6828: iload #6
    //   6830: aload_0
    //   6831: getfield mPendingTransitions : Ljava/util/ArrayList;
    //   6834: invokevirtual size : ()I
    //   6837: if_icmpge -> 6861
    //   6840: aload_0
    //   6841: getfield mPendingTransitions : Ljava/util/ArrayList;
    //   6844: iload #6
    //   6846: invokevirtual get : (I)Ljava/lang/Object;
    //   6849: checkcast android/animation/LayoutTransition
    //   6852: invokevirtual startChangingAnimations : ()V
    //   6855: iinc #6, 1
    //   6858: goto -> 6828
    //   6861: aload_0
    //   6862: getfield mPendingTransitions : Ljava/util/ArrayList;
    //   6865: invokevirtual clear : ()V
    //   6868: aload_0
    //   6869: invokespecial performDraw : ()V
    //   6872: goto -> 6982
    //   6875: iload #22
    //   6877: ifeq -> 6920
    //   6880: aload_0
    //   6881: getfield mFirst : Z
    //   6884: ifeq -> 6913
    //   6887: aload_0
    //   6888: getfield mIsInTraversal : Z
    //   6891: ifeq -> 6913
    //   6894: aload_0
    //   6895: invokespecial scheduleTraversalsImmediately : ()Z
    //   6898: ifeq -> 6913
    //   6901: ldc 'ViewRootImpl'
    //   6903: ldc_w 'schedule traversals immediately'
    //   6906: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   6909: pop
    //   6910: goto -> 6982
    //   6913: aload_0
    //   6914: invokevirtual scheduleTraversals : ()V
    //   6917: goto -> 6982
    //   6920: aload_0
    //   6921: getfield mPendingTransitions : Ljava/util/ArrayList;
    //   6924: astore #9
    //   6926: aload #9
    //   6928: ifnull -> 6982
    //   6931: aload #9
    //   6933: invokevirtual size : ()I
    //   6936: ifle -> 6982
    //   6939: iconst_0
    //   6940: istore #6
    //   6942: iload #6
    //   6944: aload_0
    //   6945: getfield mPendingTransitions : Ljava/util/ArrayList;
    //   6948: invokevirtual size : ()I
    //   6951: if_icmpge -> 6975
    //   6954: aload_0
    //   6955: getfield mPendingTransitions : Ljava/util/ArrayList;
    //   6958: iload #6
    //   6960: invokevirtual get : (I)Ljava/lang/Object;
    //   6963: checkcast android/animation/LayoutTransition
    //   6966: invokevirtual endChangingAnimations : ()V
    //   6969: iinc #6, 1
    //   6972: goto -> 6942
    //   6975: aload_0
    //   6976: getfield mPendingTransitions : Ljava/util/ArrayList;
    //   6979: invokevirtual clear : ()V
    //   6982: aload_0
    //   6983: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   6986: getfield mContentCaptureEvents : Landroid/util/SparseArray;
    //   6989: ifnull -> 6996
    //   6992: aload_0
    //   6993: invokespecial notifyContentCatpureEvents : ()V
    //   6996: aload_0
    //   6997: iconst_0
    //   6998: putfield mFirst : Z
    //   7001: aload_0
    //   7002: iconst_0
    //   7003: putfield mIsInTraversal : Z
    //   7006: return
    //   7007: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2638	-> 0
    //   #2646	-> 5
    //   #2653	-> 19
    //   #2654	-> 24
    //   #2655	-> 29
    //   #2656	-> 31
    //   #2661	-> 36
    //   #2662	-> 42
    //   #2667	-> 81
    //   #2668	-> 86
    //   #2671	-> 139
    //   #2672	-> 139
    //   #2673	-> 145
    //   #2674	-> 155
    //   #2675	-> 175
    //   #2676	-> 175
    //   #2677	-> 180
    //   #2678	-> 185
    //   #2679	-> 190
    //   #2680	-> 202
    //   #2682	-> 210
    //   #2683	-> 222
    //   #2687	-> 227
    //   #2674	-> 233
    //   #2687	-> 236
    //   #2688	-> 242
    //   #2689	-> 249
    //   #2690	-> 254
    //   #2692	-> 259
    //   #2693	-> 271
    //   #2695	-> 278
    //   #2696	-> 287
    //   #2697	-> 296
    //   #2698	-> 303
    //   #2699	-> 310
    //   #2709	-> 334
    //   #2710	-> 341
    //   #2703	-> 351
    //   #2704	-> 362
    //   #2716	-> 373
    //   #2717	-> 381
    //   #2718	-> 390
    //   #2719	-> 398
    //   #2720	-> 407
    //   #2722	-> 418
    //   #2723	-> 426
    //   #2725	-> 435
    //   #2726	-> 444
    //   #2727	-> 455
    //   #2728	-> 460
    //   #2729	-> 463
    //   #2730	-> 470
    //   #2731	-> 477
    //   #2733	-> 503
    //   #2734	-> 508
    //   #2735	-> 513
    //   #2739	-> 523
    //   #2740	-> 528
    //   #2741	-> 537
    //   #2742	-> 543
    //   #2743	-> 548
    //   #2745	-> 569
    //   #2746	-> 581
    //   #2747	-> 585
    //   #2752	-> 589
    //   #2753	-> 599
    //   #2757	-> 603
    //   #2759	-> 616
    //   #2761	-> 619
    //   #2762	-> 649
    //   #2764	-> 654
    //   #2766	-> 666
    //   #2769	-> 673
    //   #2770	-> 686
    //   #2772	-> 709
    //   #2773	-> 726
    //   #2775	-> 729
    //   #2777	-> 765
    //   #2779	-> 767
    //   #2781	-> 774
    //   #2782	-> 783
    //   #2783	-> 792
    //   #2784	-> 799
    //   #2785	-> 806
    //   #2786	-> 817
    //   #2787	-> 824
    //   #2788	-> 835
    //   #2794	-> 854
    //   #2762	-> 884
    //   #2798	-> 899
    //   #2799	-> 906
    //   #2801	-> 909
    //   #2802	-> 923
    //   #2803	-> 931
    //   #2806	-> 934
    //   #2807	-> 955
    //   #2808	-> 963
    //   #2812	-> 973
    //   #2813	-> 982
    //   #2814	-> 994
    //   #2815	-> 1004
    //   #2816	-> 1025
    //   #2814	-> 1029
    //   #2819	-> 1035
    //   #2820	-> 1044
    //   #2822	-> 1048
    //   #2824	-> 1065
    //   #2827	-> 1080
    //   #2832	-> 1083
    //   #2833	-> 1090
    //   #2834	-> 1095
    //   #2838	-> 1102
    //   #2839	-> 1108
    //   #2838	-> 1118
    //   #2834	-> 1137
    //   #2832	-> 1140
    //   #2844	-> 1140
    //   #2848	-> 1148
    //   #2851	-> 1153
    //   #2852	-> 1169
    //   #2854	-> 1198
    //   #2852	-> 1226
    //   #2854	-> 1226
    //   #2856	-> 1235
    //   #2854	-> 1260
    //   #2852	-> 1263
    //   #2856	-> 1263
    //   #2851	-> 1269
    //   #2856	-> 1269
    //   #2857	-> 1272
    //   #2862	-> 1295
    //   #2867	-> 1301
    //   #2868	-> 1310
    //   #2871	-> 1340
    //   #2872	-> 1340
    //   #2873	-> 1340
    //   #2875	-> 1343
    //   #2877	-> 1352
    //   #2878	-> 1366
    //   #2879	-> 1372
    //   #2880	-> 1375
    //   #2881	-> 1386
    //   #2883	-> 1398
    //   #2885	-> 1410
    //   #2886	-> 1416
    //   #2887	-> 1421
    //   #2888	-> 1426
    //   #2886	-> 1432
    //   #2891	-> 1432
    //   #2892	-> 1437
    //   #2893	-> 1455
    //   #2894	-> 1463
    //   #2896	-> 1470
    //   #2897	-> 1475
    //   #2891	-> 1484
    //   #2900	-> 1484
    //   #3247	-> 1530
    //   #2900	-> 1561
    //   #2902	-> 1561
    //   #2904	-> 1572
    //   #2914	-> 1577
    //   #2904	-> 1606
    //   #2917	-> 1609
    //   #2918	-> 1620
    //   #2919	-> 1628
    //   #2917	-> 1636
    //   #2922	-> 1636
    //   #2923	-> 1636
    //   #2924	-> 1648
    //   #2927	-> 1657
    //   #2928	-> 1667
    //   #2929	-> 1714
    //   #2928	-> 1748
    //   #3120	-> 1759
    //   #2927	-> 1804
    //   #2934	-> 1804
    //   #2936	-> 1831
    //   #2940	-> 1845
    //   #2943	-> 1858
    //   #2949	-> 1890
    //   #2940	-> 1895
    //   #2945	-> 1895
    //   #2949	-> 1909
    //   #2936	-> 1919
    //   #2949	-> 1919
    //   #2951	-> 1922
    //   #2953	-> 1934
    //   #2954	-> 1999
    //   #2953	-> 2039
    //   #3120	-> 2050
    //   #2962	-> 2069
    //   #2963	-> 2095
    //   #2964	-> 2132
    //   #2963	-> 2150
    //   #2965	-> 2158
    //   #2967	-> 2189
    //   #2970	-> 2192
    //   #2971	-> 2212
    //   #2973	-> 2229
    //   #2975	-> 2272
    //   #2976	-> 2302
    //   #3120	-> 2342
    //   #2976	-> 2372
    //   #2977	-> 2374
    //   #2978	-> 2421
    //   #2979	-> 2481
    //   #2981	-> 2517
    //   #2982	-> 2528
    //   #2983	-> 2561
    //   #2984	-> 2586
    //   #2987	-> 2749
    //   #2989	-> 2752
    //   #2990	-> 2761
    //   #2991	-> 2787
    //   #2993	-> 2790
    //   #2994	-> 2820
    //   #2996	-> 2823
    //   #2998	-> 2895
    //   #2999	-> 2925
    //   #3002	-> 2949
    //   #3004	-> 2952
    //   #3005	-> 2986
    //   #3006	-> 3014
    //   #3005	-> 3050
    //   #3009	-> 3076
    //   #3012	-> 3080
    //   #3013	-> 3104
    //   #3020	-> 3130
    //   #3022	-> 3177
    //   #3024	-> 3199
    //   #3025	-> 3219
    //   #3034	-> 3285
    //   #3036	-> 3318
    //   #3039	-> 3323
    //   #3036	-> 3326
    //   #3037	-> 3331
    //   #3038	-> 3357
    //   #3043	-> 3381
    //   #3045	-> 3388
    //   #3043	-> 3414
    //   #3120	-> 3450
    //   #3043	-> 3475
    //   #3046	-> 3500
    //   #3049	-> 3505
    //   #3050	-> 3531
    //   #3052	-> 3557
    //   #3053	-> 3605
    //   #3054	-> 3634
    //   #3056	-> 3666
    //   #3057	-> 3692
    //   #3060	-> 3718
    //   #3061	-> 3779
    //   #3062	-> 3810
    //   #3064	-> 3846
    //   #3068	-> 3962
    //   #3069	-> 3993
    //   #3079	-> 4017
    //   #3083	-> 4050
    //   #3080	-> 4057
    //   #3081	-> 4059
    //   #3082	-> 4084
    //   #3086	-> 4085
    //   #3087	-> 4094
    //   #3090	-> 4117
    //   #3092	-> 4134
    //   #3094	-> 4151
    //   #3095	-> 4173
    //   #3096	-> 4201
    //   #3097	-> 4206
    //   #3098	-> 4211
    //   #3099	-> 4217
    //   #3100	-> 4245
    //   #3101	-> 4270
    //   #3102	-> 4356
    //   #3104	-> 4426
    //   #3105	-> 4490
    //   #3106	-> 4525
    //   #3104	-> 4541
    //   #3107	-> 4555
    //   #3120	-> 4558
    //   #3109	-> 4583
    //   #3095	-> 4590
    //   #3112	-> 4590
    //   #3113	-> 4601
    //   #3114	-> 4606
    //   #3115	-> 4617
    //   #3117	-> 4631
    //   #3121	-> 4641
    //   #3120	-> 4656
    //   #2949	-> 4753
    //   #3120	-> 4770
    //   #2949	-> 4792
    //   #3120	-> 4797
    //   #3126	-> 4841
    //   #3127	-> 4853
    //   #3132	-> 4865
    //   #3133	-> 4889
    //   #3134	-> 4898
    //   #3137	-> 4907
    //   #3139	-> 4914
    //   #3142	-> 4924
    //   #3144	-> 4935
    //   #3145	-> 4950
    //   #3146	-> 4960
    //   #3147	-> 4964
    //   #3149	-> 4971
    //   #3150	-> 4976
    //   #3151	-> 4985
    //   #3152	-> 4990
    //   #3153	-> 5012
    //   #3152	-> 5023
    //   #3158	-> 5029
    //   #3159	-> 5064
    //   #3160	-> 5072
    //   #3161	-> 5081
    //   #3162	-> 5086
    //   #3163	-> 5108
    //   #3162	-> 5131
    //   #3161	-> 5144
    //   #3167	-> 5148
    //   #3159	-> 5156
    //   #3170	-> 5160
    //   #3171	-> 5169
    //   #3172	-> 5173
    //   #3174	-> 5183
    //   #3176	-> 5206
    //   #3177	-> 5216
    //   #3176	-> 5223
    //   #3177	-> 5235
    //   #3137	-> 5238
    //   #3181	-> 5238
    //   #3182	-> 5247
    //   #3183	-> 5260
    //   #3184	-> 5271
    //   #3185	-> 5287
    //   #3187	-> 5304
    //   #3189	-> 5328
    //   #3193	-> 5333
    //   #3194	-> 5347
    //   #3196	-> 5371
    //   #3197	-> 5393
    //   #3241	-> 5415
    //   #3199	-> 5418
    //   #3200	-> 5431
    //   #3202	-> 5444
    //   #3203	-> 5493
    //   #3205	-> 5531
    //   #3202	-> 5565
    //   #3209	-> 5576
    //   #3214	-> 5584
    //   #3215	-> 5590
    //   #3216	-> 5596
    //   #3218	-> 5599
    //   #3219	-> 5612
    //   #3220	-> 5631
    //   #3222	-> 5641
    //   #3224	-> 5644
    //   #3225	-> 5657
    //   #3226	-> 5676
    //   #3228	-> 5686
    //   #3231	-> 5689
    //   #3232	-> 5694
    //   #3235	-> 5763
    //   #3231	-> 5774
    //   #3238	-> 5774
    //   #3241	-> 5777
    //   #3250	-> 5785
    //   #3251	-> 5804
    //   #3254	-> 5808
    //   #3255	-> 5836
    //   #3257	-> 5863
    //   #3258	-> 5868
    //   #3262	-> 5881
    //   #3263	-> 5891
    //   #3264	-> 5900
    //   #3266	-> 5932
    //   #3273	-> 5938
    //   #3276	-> 5949
    //   #3277	-> 5957
    //   #3281	-> 6011
    //   #3282	-> 6020
    //   #3283	-> 6031
    //   #3286	-> 6040
    //   #3287	-> 6054
    //   #3288	-> 6066
    //   #3291	-> 6071
    //   #3293	-> 6088
    //   #3292	-> 6091
    //   #3273	-> 6096
    //   #3257	-> 6099
    //   #3304	-> 6099
    //   #3305	-> 6104
    //   #3308	-> 6108
    //   #3309	-> 6113
    //   #3310	-> 6121
    //   #3313	-> 6131
    //   #3315	-> 6136
    //   #3316	-> 6145
    //   #3319	-> 6150
    //   #3320	-> 6162
    //   #3323	-> 6176
    //   #3324	-> 6199
    //   #3330	-> 6208
    //   #3331	-> 6219
    //   #3332	-> 6230
    //   #3333	-> 6244
    //   #3335	-> 6261
    //   #3336	-> 6267
    //   #3337	-> 6274
    //   #3341	-> 6281
    //   #3344	-> 6312
    //   #3343	-> 6315
    //   #3313	-> 6325
    //   #3353	-> 6325
    //   #3355	-> 6339
    //   #3379	-> 6354
    //   #3380	-> 6362
    //   #3381	-> 6375
    //   #3383	-> 6386
    //   #3360	-> 6394
    //   #3361	-> 6405
    //   #3362	-> 6413
    //   #3389	-> 6421
    //   #3390	-> 6445
    //   #3391	-> 6477
    //   #3397	-> 6482
    //   #3398	-> 6493
    //   #3399	-> 6507
    //   #3400	-> 6526
    //   #3405	-> 6531
    //   #3406	-> 6557
    //   #3407	-> 6581
    //   #3408	-> 6602
    //   #3409	-> 6607
    //   #3410	-> 6615
    //   #3411	-> 6627
    //   #3414	-> 6632
    //   #3416	-> 6642
    //   #3417	-> 6659
    //   #3418	-> 6679
    //   #3419	-> 6684
    //   #3428	-> 6690
    //   #3429	-> 6695
    //   #3430	-> 6700
    //   #3431	-> 6705
    //   #3432	-> 6711
    //   #3434	-> 6717
    //   #3437	-> 6730
    //   #3438	-> 6737
    //   #3440	-> 6741
    //   #3441	-> 6750
    //   #3442	-> 6754
    //   #3443	-> 6758
    //   #3440	-> 6766
    //   #3446	-> 6766
    //   #3448	-> 6801
    //   #3449	-> 6806
    //   #3450	-> 6825
    //   #3451	-> 6840
    //   #3450	-> 6855
    //   #3453	-> 6861
    //   #3456	-> 6868
    //   #3458	-> 6875
    //   #3464	-> 6880
    //   #3465	-> 6901
    //   #3467	-> 6913
    //   #3470	-> 6920
    //   #3471	-> 6939
    //   #3472	-> 6954
    //   #3471	-> 6969
    //   #3474	-> 6975
    //   #3478	-> 6982
    //   #3479	-> 6992
    //   #3483	-> 6996
    //   #3486	-> 7001
    //   #3487	-> 7006
    //   #2647	-> 7007
    // Exception table:
    //   from	to	target	type
    //   1657	1662	4815	android/os/RemoteException
    //   1667	1673	1787	android/os/RemoteException
    //   1676	1686	1773	android/os/RemoteException
    //   1686	1714	1759	android/os/RemoteException
    //   1714	1748	1759	android/os/RemoteException
    //   1748	1756	1759	android/os/RemoteException
    //   1822	1831	4797	android/os/RemoteException
    //   1831	1840	4758	finally
    //   1845	1858	1914	finally
    //   1858	1870	1914	finally
    //   1870	1876	1890	finally
    //   1876	1887	1909	finally
    //   1895	1906	1909	finally
    //   1919	1922	4753	finally
    //   1922	1934	4735	android/os/RemoteException
    //   1938	1943	4711	android/os/RemoteException
    //   1948	1999	2050	android/os/RemoteException
    //   1999	2039	2050	android/os/RemoteException
    //   2039	2047	2050	android/os/RemoteException
    //   2073	2086	4711	android/os/RemoteException
    //   2095	2132	2050	android/os/RemoteException
    //   2132	2150	2050	android/os/RemoteException
    //   2150	2158	2050	android/os/RemoteException
    //   2158	2171	2050	android/os/RemoteException
    //   2180	2189	2050	android/os/RemoteException
    //   2196	2212	4711	android/os/RemoteException
    //   2249	2263	4681	android/os/RemoteException
    //   2292	2302	4681	android/os/RemoteException
    //   2327	2337	2342	android/os/RemoteException
    //   2398	2407	2342	android/os/RemoteException
    //   2440	2449	4681	android/os/RemoteException
    //   2475	2481	2342	android/os/RemoteException
    //   2500	2508	2342	android/os/RemoteException
    //   2547	2561	2342	android/os/RemoteException
    //   2580	2586	2342	android/os/RemoteException
    //   2605	2611	2342	android/os/RemoteException
    //   2630	2635	2342	android/os/RemoteException
    //   2654	2659	2342	android/os/RemoteException
    //   2678	2687	2342	android/os/RemoteException
    //   2706	2719	2342	android/os/RemoteException
    //   2738	2749	2342	android/os/RemoteException
    //   2776	2787	2342	android/os/RemoteException
    //   2809	2815	4681	android/os/RemoteException
    //   2847	2861	2342	android/os/RemoteException
    //   2880	2886	2342	android/os/RemoteException
    //   2914	2925	4681	android/os/RemoteException
    //   2944	2949	4681	android/os/RemoteException
    //   2976	2986	2342	android/os/RemoteException
    //   3005	3014	2342	android/os/RemoteException
    //   3033	3041	2342	android/os/RemoteException
    //   3069	3076	2342	android/os/RemoteException
    //   3099	3104	2342	android/os/RemoteException
    //   3123	3130	2342	android/os/RemoteException
    //   3149	3155	2342	android/os/RemoteException
    //   3174	3177	2342	android/os/RemoteException
    //   3181	3190	3414	finally
    //   3203	3219	3326	android/view/Surface$OutOfResourcesException
    //   3203	3219	3414	finally
    //   3244	3255	3318	android/view/Surface$OutOfResourcesException
    //   3244	3255	3475	finally
    //   3275	3285	3318	android/view/Surface$OutOfResourcesException
    //   3275	3285	3475	finally
    //   3305	3315	3318	android/view/Surface$OutOfResourcesException
    //   3305	3315	3475	finally
    //   3351	3357	3475	finally
    //   3377	3380	3475	finally
    //   3385	3388	3414	finally
    //   3407	3411	2342	android/os/RemoteException
    //   3444	3447	3475	finally
    //   3447	3450	3450	android/os/RemoteException
    //   3524	3531	2342	android/os/RemoteException
    //   3550	3557	2342	android/os/RemoteException
    //   3576	3581	2342	android/os/RemoteException
    //   3600	3605	2342	android/os/RemoteException
    //   3624	3634	2342	android/os/RemoteException
    //   3653	3666	2342	android/os/RemoteException
    //   3685	3692	2342	android/os/RemoteException
    //   3711	3718	2342	android/os/RemoteException
    //   3741	3751	2342	android/os/RemoteException
    //   3770	3779	2342	android/os/RemoteException
    //   3802	3810	2342	android/os/RemoteException
    //   3829	3839	2342	android/os/RemoteException
    //   3889	3895	4681	android/os/RemoteException
    //   3927	3937	2342	android/os/RemoteException
    //   3956	3962	2342	android/os/RemoteException
    //   3985	3993	2342	android/os/RemoteException
    //   4012	4017	2342	android/os/RemoteException
    //   4036	4050	4057	android/view/Surface$OutOfResourcesException
    //   4036	4050	2342	android/os/RemoteException
    //   4078	4084	2342	android/os/RemoteException
    //   4113	4117	2342	android/os/RemoteException
    //   4192	4201	4681	android/os/RemoteException
    //   4239	4245	4681	android/os/RemoteException
    //   4264	4270	4681	android/os/RemoteException
    //   4289	4296	4681	android/os/RemoteException
    //   4315	4324	4681	android/os/RemoteException
    //   4350	4356	2342	android/os/RemoteException
    //   4375	4382	2342	android/os/RemoteException
    //   4401	4410	2342	android/os/RemoteException
    //   4445	4451	4681	android/os/RemoteException
    //   4484	4490	4681	android/os/RemoteException
    //   4509	4519	4681	android/os/RemoteException
    //   4519	4525	4558	android/os/RemoteException
    //   4525	4541	4558	android/os/RemoteException
    //   4541	4555	4656	android/os/RemoteException
    //   4583	4587	4656	android/os/RemoteException
    //   4594	4601	4656	android/os/RemoteException
    //   4606	4617	4656	android/os/RemoteException
    //   4617	4628	4656	android/os/RemoteException
    //   4631	4641	4656	android/os/RemoteException
    //   4764	4767	4792	finally
    //   4767	4770	4770	android/os/RemoteException
    //   5183	5206	5223	finally
    //   6071	6088	6091	android/os/RemoteException
    //   6281	6293	6320	android/os/RemoteException
    //   6293	6312	6315	android/os/RemoteException
  }
  
  private void notifyContentCatpureEvents() {
    Trace.traceBegin(8L, "notifyContentCaptureEvents");
    try {
      ContentCaptureManager contentCaptureManager = this.mAttachInfo.mContentCaptureManager;
      MainContentCaptureSession mainContentCaptureSession = contentCaptureManager.getMainContentCaptureSession();
      for (byte b = 0; b < this.mAttachInfo.mContentCaptureEvents.size(); b++) {
        int i = this.mAttachInfo.mContentCaptureEvents.keyAt(b);
        mainContentCaptureSession.notifyViewTreeEvent(i, true);
        SparseArray<ArrayList<Object>> sparseArray = this.mAttachInfo.mContentCaptureEvents;
        ArrayList<Object> arrayList = sparseArray.valueAt(b);
        for (byte b1 = 0; b1 < arrayList.size(); b1++) {
          AutofillId autofillId = (AutofillId)arrayList.get(b1);
          if (autofillId instanceof AutofillId) {
            mainContentCaptureSession.notifyViewDisappeared(i, autofillId);
          } else {
            ViewStructure viewStructure;
            if (autofillId instanceof View) {
              StringBuilder stringBuilder;
              View view = (View)autofillId;
              ContentCaptureSession contentCaptureSession = view.getContentCaptureSession();
              if (contentCaptureSession == null) {
                String str = this.mTag;
                stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("no content capture session on view: ");
                stringBuilder.append(view);
                Log.w(str, stringBuilder.toString());
              } else {
                String str;
                int j = stringBuilder.getId();
                if (j != i) {
                  str = this.mTag;
                  StringBuilder stringBuilder1 = new StringBuilder();
                  this();
                  stringBuilder1.append("content capture session mismatch for view (");
                  stringBuilder1.append(view);
                  stringBuilder1.append("): was ");
                  stringBuilder1.append(i);
                  stringBuilder1.append(" before, it's ");
                  stringBuilder1.append(j);
                  stringBuilder1.append(" now");
                  Log.w(str, stringBuilder1.toString());
                } else {
                  viewStructure = str.newViewStructure(view);
                  view.onProvideContentCaptureStructure(viewStructure, 0);
                  str.notifyViewAppeared(viewStructure);
                } 
              } 
            } else if (viewStructure instanceof Insets) {
              mainContentCaptureSession.notifyViewInsetsChanged(i, (Insets)viewStructure);
            } else {
              String str = this.mTag;
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("invalid content capture event: ");
              stringBuilder.append(viewStructure);
              Log.w(str, stringBuilder.toString());
            } 
          } 
        } 
        mainContentCaptureSession.notifyViewTreeEvent(i, false);
      } 
      this.mAttachInfo.mContentCaptureEvents = null;
      return;
    } finally {
      Trace.traceEnd(8L);
    } 
  }
  
  private void notifyHolderSurfaceDestroyed() {
    this.mSurfaceHolder.ungetCallbacks();
    SurfaceHolder.Callback[] arrayOfCallback = this.mSurfaceHolder.getCallbacks();
    if (arrayOfCallback != null) {
      int i;
      byte b;
      for (i = arrayOfCallback.length, b = 0; b < i; ) {
        SurfaceHolder.Callback callback = arrayOfCallback[b];
        callback.surfaceDestroyed((SurfaceHolder)this.mSurfaceHolder);
        b++;
      } 
    } 
  }
  
  private void maybeHandleWindowMove(Rect paramRect) {
    boolean bool;
    if (this.mAttachInfo.mWindowLeft != paramRect.left || this.mAttachInfo.mWindowTop != paramRect.top) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      CompatibilityInfo.Translator translator = this.mTranslator;
      if (translator != null)
        translator.translateRectInScreenToAppWinFrame(paramRect); 
      this.mAttachInfo.mWindowLeft = paramRect.left;
      this.mAttachInfo.mWindowTop = paramRect.top;
    } 
    if (bool || this.mAttachInfo.mNeedsUpdateLightCenter) {
      if (this.mAttachInfo.mThreadedRenderer != null)
        this.mAttachInfo.mThreadedRenderer.setLightCenter(this.mAttachInfo); 
      this.mAttachInfo.mNeedsUpdateLightCenter = false;
    } 
  }
  
  private void handleWindowFocusChanged() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mWindowFocusChanged : Z
    //   6: ifne -> 12
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: aload_0
    //   13: iconst_0
    //   14: putfield mWindowFocusChanged : Z
    //   17: aload_0
    //   18: getfield mUpcomingWindowFocus : Z
    //   21: istore_1
    //   22: aload_0
    //   23: getfield mUpcomingInTouchMode : Z
    //   26: istore_2
    //   27: aload_0
    //   28: monitorexit
    //   29: getstatic android/view/ViewRootImpl.sNewInsetsMode : I
    //   32: ifeq -> 56
    //   35: iload_1
    //   36: ifeq -> 49
    //   39: aload_0
    //   40: getfield mInsetsController : Landroid/view/InsetsController;
    //   43: invokevirtual onWindowFocusGained : ()V
    //   46: goto -> 56
    //   49: aload_0
    //   50: getfield mInsetsController : Landroid/view/InsetsController;
    //   53: invokevirtual onWindowFocusLost : ()V
    //   56: aload_0
    //   57: getfield mAdded : Z
    //   60: ifeq -> 434
    //   63: aload_0
    //   64: iload_1
    //   65: invokespecial profileRendering : (Z)V
    //   68: aconst_null
    //   69: astore_3
    //   70: iload_1
    //   71: ifeq -> 237
    //   74: aload_0
    //   75: iload_2
    //   76: invokespecial ensureTouchModeLocally : (Z)Z
    //   79: pop
    //   80: aload_0
    //   81: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   84: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   87: ifnull -> 237
    //   90: aload_0
    //   91: getfield mSurface : Landroid/view/Surface;
    //   94: invokevirtual isValid : ()Z
    //   97: ifeq -> 237
    //   100: aload_0
    //   101: iconst_1
    //   102: putfield mFullRedrawNeeded : Z
    //   105: aload_0
    //   106: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   109: astore #4
    //   111: aload #4
    //   113: ifnull -> 126
    //   116: aload #4
    //   118: getfield surfaceInsets : Landroid/graphics/Rect;
    //   121: astore #4
    //   123: goto -> 129
    //   126: aconst_null
    //   127: astore #4
    //   129: aload_0
    //   130: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   133: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   136: aload_0
    //   137: getfield mWidth : I
    //   140: aload_0
    //   141: getfield mHeight : I
    //   144: aload_0
    //   145: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   148: aload_0
    //   149: getfield mSurface : Landroid/view/Surface;
    //   152: aload #4
    //   154: invokevirtual initializeIfNeeded : (IILandroid/view/View$AttachInfo;Landroid/view/Surface;Landroid/graphics/Rect;)Z
    //   157: pop
    //   158: goto -> 237
    //   161: astore #4
    //   163: aload_0
    //   164: getfield mTag : Ljava/lang/String;
    //   167: ldc_w 'OutOfResourcesException locking surface'
    //   170: aload #4
    //   172: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   175: pop
    //   176: aload_0
    //   177: getfield mWindowSession : Landroid/view/IWindowSession;
    //   180: aload_0
    //   181: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   184: invokeinterface outOfMemory : (Landroid/view/IWindow;)Z
    //   189: ifne -> 209
    //   192: aload_0
    //   193: getfield mTag : Ljava/lang/String;
    //   196: ldc_w 'No processes killed for memory; killing self'
    //   199: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   202: pop
    //   203: invokestatic myPid : ()I
    //   206: invokestatic killProcess : (I)V
    //   209: goto -> 214
    //   212: astore #4
    //   214: aload_0
    //   215: getfield mHandler : Landroid/view/ViewRootImpl$ViewRootHandler;
    //   218: astore #4
    //   220: aload #4
    //   222: aload #4
    //   224: bipush #6
    //   226: invokevirtual obtainMessage : (I)Landroid/os/Message;
    //   229: ldc2_w 500
    //   232: invokevirtual sendMessageDelayed : (Landroid/os/Message;J)Z
    //   235: pop
    //   236: return
    //   237: aload_0
    //   238: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   241: iload_1
    //   242: putfield mHasWindowFocus : Z
    //   245: aload_0
    //   246: getfield mImeFocusController : Landroid/view/ImeFocusController;
    //   249: aload_0
    //   250: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   253: iconst_1
    //   254: invokevirtual updateImeFocusable : (Landroid/view/WindowManager$LayoutParams;Z)Z
    //   257: pop
    //   258: aload_0
    //   259: getfield mImeFocusController : Landroid/view/ImeFocusController;
    //   262: iload_1
    //   263: aload_0
    //   264: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   267: invokevirtual onPreWindowFocus : (ZLandroid/view/WindowManager$LayoutParams;)V
    //   270: aload_0
    //   271: getfield mView : Landroid/view/View;
    //   274: ifnull -> 326
    //   277: aload_0
    //   278: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   281: getfield mKeyDispatchState : Landroid/view/KeyEvent$DispatcherState;
    //   284: invokevirtual reset : ()V
    //   287: aload_0
    //   288: getfield mView : Landroid/view/View;
    //   291: iload_1
    //   292: invokevirtual dispatchWindowFocusChanged : (Z)V
    //   295: aload_0
    //   296: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   299: getfield mTreeObserver : Landroid/view/ViewTreeObserver;
    //   302: iload_1
    //   303: invokevirtual dispatchOnWindowFocusChange : (Z)V
    //   306: aload_0
    //   307: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   310: getfield mTooltipHost : Landroid/view/View;
    //   313: ifnull -> 326
    //   316: aload_0
    //   317: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   320: getfield mTooltipHost : Landroid/view/View;
    //   323: invokevirtual hideTooltip : ()V
    //   326: aload_0
    //   327: getfield mImeFocusController : Landroid/view/ImeFocusController;
    //   330: astore #5
    //   332: aload_0
    //   333: getfield mView : Landroid/view/View;
    //   336: astore #6
    //   338: aload_3
    //   339: astore #4
    //   341: aload #6
    //   343: ifnull -> 353
    //   346: aload #6
    //   348: invokevirtual findFocus : ()Landroid/view/View;
    //   351: astore #4
    //   353: aload #5
    //   355: aload #4
    //   357: iload_1
    //   358: aload_0
    //   359: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   362: invokevirtual onPostWindowFocus : (Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V
    //   365: iload_1
    //   366: ifeq -> 422
    //   369: aload_0
    //   370: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   373: astore #4
    //   375: aload #4
    //   377: aload #4
    //   379: getfield softInputMode : I
    //   382: sipush #-257
    //   385: iand
    //   386: putfield softInputMode : I
    //   389: aload_0
    //   390: getfield mView : Landroid/view/View;
    //   393: invokevirtual getLayoutParams : ()Landroid/view/ViewGroup$LayoutParams;
    //   396: checkcast android/view/WindowManager$LayoutParams
    //   399: astore #4
    //   401: aload #4
    //   403: aload #4
    //   405: getfield softInputMode : I
    //   408: sipush #-257
    //   411: iand
    //   412: putfield softInputMode : I
    //   415: aload_0
    //   416: invokespecial fireAccessibilityFocusEventIfHasFocusedNode : ()V
    //   419: goto -> 434
    //   422: aload_0
    //   423: getfield mPointerCapture : Z
    //   426: ifeq -> 434
    //   429: aload_0
    //   430: iconst_0
    //   431: invokespecial handlePointerCaptureChanged : (Z)V
    //   434: aload_0
    //   435: getfield mFirstInputStage : Landroid/view/ViewRootImpl$InputStage;
    //   438: iload_1
    //   439: invokevirtual onWindowFocusChanged : (Z)V
    //   442: iload_1
    //   443: ifeq -> 450
    //   446: aload_0
    //   447: invokespecial handleContentCaptureFlush : ()V
    //   450: return
    //   451: astore #4
    //   453: aload_0
    //   454: monitorexit
    //   455: aload #4
    //   457: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3573	-> 0
    //   #3574	-> 2
    //   #3575	-> 9
    //   #3577	-> 12
    //   #3578	-> 17
    //   #3579	-> 22
    //   #3580	-> 27
    //   #3581	-> 29
    //   #3584	-> 35
    //   #3585	-> 39
    //   #3587	-> 49
    //   #3591	-> 56
    //   #3592	-> 63
    //   #3594	-> 68
    //   #3595	-> 74
    //   #3596	-> 80
    //   #3597	-> 100
    //   #3599	-> 105
    //   #3600	-> 111
    //   #3601	-> 129
    //   #3617	-> 158
    //   #3603	-> 161
    //   #3604	-> 163
    //   #3606	-> 176
    //   #3607	-> 192
    //   #3609	-> 203
    //   #3612	-> 209
    //   #3611	-> 212
    //   #3614	-> 214
    //   #3616	-> 236
    //   #3621	-> 237
    //   #3622	-> 245
    //   #3623	-> 258
    //   #3625	-> 270
    //   #3626	-> 277
    //   #3627	-> 287
    //   #3628	-> 295
    //   #3629	-> 306
    //   #3630	-> 316
    //   #3637	-> 326
    //   #3646	-> 365
    //   #3649	-> 369
    //   #3651	-> 389
    //   #3658	-> 415
    //   #3660	-> 422
    //   #3661	-> 429
    //   #3665	-> 434
    //   #3670	-> 442
    //   #3671	-> 446
    //   #3673	-> 450
    //   #3580	-> 451
    // Exception table:
    //   from	to	target	type
    //   2	9	451	finally
    //   9	11	451	finally
    //   12	17	451	finally
    //   17	22	451	finally
    //   22	27	451	finally
    //   27	29	451	finally
    //   105	111	161	android/view/Surface$OutOfResourcesException
    //   116	123	161	android/view/Surface$OutOfResourcesException
    //   129	158	161	android/view/Surface$OutOfResourcesException
    //   176	192	212	android/os/RemoteException
    //   192	203	212	android/os/RemoteException
    //   203	209	212	android/os/RemoteException
    //   453	455	451	finally
  }
  
  private void fireAccessibilityFocusEventIfHasFocusedNode() {
    if (!AccessibilityManager.getInstance(this.mContext).isEnabled())
      return; 
    View view = this.mView.findFocus();
    if (view == null)
      return; 
    AccessibilityNodeProvider accessibilityNodeProvider = view.getAccessibilityNodeProvider();
    if (accessibilityNodeProvider == null) {
      view.sendAccessibilityEvent(8);
    } else {
      AccessibilityNodeInfo accessibilityNodeInfo = findFocusedVirtualNode(accessibilityNodeProvider);
      if (accessibilityNodeInfo != null) {
        long l = accessibilityNodeInfo.getSourceNodeId();
        int i = AccessibilityNodeInfo.getVirtualDescendantId(l);
        AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain(8);
        accessibilityEvent.setSource(view, i);
        accessibilityEvent.setPackageName(accessibilityNodeInfo.getPackageName());
        accessibilityEvent.setChecked(accessibilityNodeInfo.isChecked());
        accessibilityEvent.setContentDescription(accessibilityNodeInfo.getContentDescription());
        accessibilityEvent.setPassword(accessibilityNodeInfo.isPassword());
        accessibilityEvent.getText().add(accessibilityNodeInfo.getText());
        accessibilityEvent.setEnabled(accessibilityNodeInfo.isEnabled());
        view.getParent().requestSendAccessibilityEvent(view, accessibilityEvent);
        accessibilityNodeInfo.recycle();
      } 
    } 
  }
  
  private AccessibilityNodeInfo findFocusedVirtualNode(AccessibilityNodeProvider paramAccessibilityNodeProvider) {
    AccessibilityNodeInfo accessibilityNodeInfo1 = paramAccessibilityNodeProvider.findFocus(1);
    if (accessibilityNodeInfo1 != null)
      return accessibilityNodeInfo1; 
    if (!this.mContext.isAutofillCompatibilityEnabled())
      return null; 
    AccessibilityNodeInfo accessibilityNodeInfo2 = paramAccessibilityNodeProvider.createAccessibilityNodeInfo(-1);
    if (accessibilityNodeInfo2.isFocused())
      return accessibilityNodeInfo2; 
    LinkedList<AccessibilityNodeInfo> linkedList = new LinkedList();
    linkedList.offer(accessibilityNodeInfo2);
    while (!linkedList.isEmpty()) {
      accessibilityNodeInfo2 = linkedList.poll();
      LongArray longArray = accessibilityNodeInfo2.getChildNodeIds();
      if (longArray == null || longArray.size() <= 0)
        continue; 
      int i = longArray.size();
      for (byte b = 0; b < i; b++) {
        long l = longArray.get(b);
        int j = AccessibilityNodeInfo.getVirtualDescendantId(l);
        AccessibilityNodeInfo accessibilityNodeInfo = paramAccessibilityNodeProvider.createAccessibilityNodeInfo(j);
        if (accessibilityNodeInfo != null) {
          if (accessibilityNodeInfo.isFocused())
            return accessibilityNodeInfo; 
          linkedList.offer(accessibilityNodeInfo);
        } 
      } 
      accessibilityNodeInfo2.recycle();
    } 
    return null;
  }
  
  private void handleOutOfResourcesException(Surface.OutOfResourcesException paramOutOfResourcesException) {
    Log.e(this.mTag, "OutOfResourcesException initializing HW surface", paramOutOfResourcesException);
    try {
      if (!this.mWindowSession.outOfMemory(this.mWindow) && Process.myUid() != 1000) {
        Slog.w(this.mTag, "No processes killed for memory; killing self");
        Process.killProcess(Process.myPid());
      } 
    } catch (RemoteException remoteException) {}
    this.mLayoutRequested = true;
  }
  
  private void performMeasure(int paramInt1, int paramInt2) {
    if (this.mView == null)
      return; 
    Trace.traceBegin(8L, "measure");
    try {
      this.mView.measure(paramInt1, paramInt2);
      return;
    } finally {
      Trace.traceEnd(8L);
    } 
  }
  
  boolean isInLayout() {
    return this.mInLayout;
  }
  
  boolean requestLayoutDuringLayout(View paramView) {
    if (paramView.mParent == null || paramView.mAttachInfo == null)
      return true; 
    if (!this.mLayoutRequesters.contains(paramView))
      this.mLayoutRequesters.add(paramView); 
    if (!this.mHandlingLayoutInLayoutRequest)
      return true; 
    return false;
  }
  
  private void performLayout(WindowManager.LayoutParams paramLayoutParams, int paramInt1, int paramInt2) {
    this.mScrollMayChange = true;
    this.mInLayout = true;
    View view = this.mView;
    if (view == null)
      return; 
    if (DEBUG_LAYOUT) {
      String str1 = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Laying out ");
      stringBuilder.append(view);
      stringBuilder.append(" to (");
      stringBuilder.append(view.getMeasuredWidth());
      stringBuilder.append(", ");
      stringBuilder.append(view.getMeasuredHeight());
      stringBuilder.append(")");
      String str2 = stringBuilder.toString();
      Log.v(str1, str2);
    } 
    Trace.traceBegin(8L, "layout");
    try {
      view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
      this.mInLayout = false;
      int i = this.mLayoutRequesters.size();
      if (i > 0) {
        ArrayList<View> arrayList = getValidLayoutRequesters(this.mLayoutRequesters, false);
        if (arrayList != null) {
          this.mHandlingLayoutInLayoutRequest = true;
          int j = arrayList.size();
          for (i = 0; i < j; i++) {
            View view1 = arrayList.get(i);
            if (DEBUG_LAYOUT) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("requestLayout() improperly called by ");
              stringBuilder.append(view1);
              stringBuilder.append(" during layout: running second layout pass");
              Log.w("View", stringBuilder.toString());
            } 
            view1.requestLayout();
          } 
          measureHierarchy(view, paramLayoutParams, this.mView.getContext().getResources(), paramInt1, paramInt2);
          this.mInLayout = true;
          view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
          this.mHandlingLayoutInLayoutRequest = false;
          ArrayList<View> arrayList1 = getValidLayoutRequesters(this.mLayoutRequesters, true);
          if (arrayList1 != null) {
            HandlerActionQueue handlerActionQueue = getRunQueue();
            Object object = new Object();
            super(this, arrayList1);
            handlerActionQueue.post((Runnable)object);
          } 
        } 
      } 
      Trace.traceEnd(8L);
      return;
    } finally {
      Trace.traceEnd(8L);
    } 
  }
  
  private ArrayList<View> getValidLayoutRequesters(ArrayList<View> paramArrayList, boolean paramBoolean) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual size : ()I
    //   4: istore_3
    //   5: aconst_null
    //   6: astore #4
    //   8: iconst_0
    //   9: istore #5
    //   11: iload #5
    //   13: iload_3
    //   14: if_icmpge -> 194
    //   17: aload_1
    //   18: iload #5
    //   20: invokevirtual get : (I)Ljava/lang/Object;
    //   23: checkcast android/view/View
    //   26: astore #6
    //   28: aload #4
    //   30: astore #7
    //   32: aload #6
    //   34: ifnull -> 184
    //   37: aload #4
    //   39: astore #7
    //   41: aload #6
    //   43: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   46: ifnull -> 184
    //   49: aload #4
    //   51: astore #7
    //   53: aload #6
    //   55: getfield mParent : Landroid/view/ViewParent;
    //   58: ifnull -> 184
    //   61: iload_2
    //   62: ifne -> 84
    //   65: aload #4
    //   67: astore #7
    //   69: aload #6
    //   71: getfield mPrivateFlags : I
    //   74: sipush #4096
    //   77: iand
    //   78: sipush #4096
    //   81: if_icmpne -> 184
    //   84: iconst_0
    //   85: istore #8
    //   87: aload #6
    //   89: astore #7
    //   91: iload #8
    //   93: istore #9
    //   95: aload #7
    //   97: ifnull -> 149
    //   100: aload #7
    //   102: getfield mViewFlags : I
    //   105: bipush #12
    //   107: iand
    //   108: bipush #8
    //   110: if_icmpne -> 119
    //   113: iconst_1
    //   114: istore #9
    //   116: goto -> 149
    //   119: aload #7
    //   121: getfield mParent : Landroid/view/ViewParent;
    //   124: instanceof android/view/View
    //   127: ifeq -> 143
    //   130: aload #7
    //   132: getfield mParent : Landroid/view/ViewParent;
    //   135: checkcast android/view/View
    //   138: astore #7
    //   140: goto -> 91
    //   143: aconst_null
    //   144: astore #7
    //   146: goto -> 91
    //   149: aload #4
    //   151: astore #7
    //   153: iload #9
    //   155: ifne -> 184
    //   158: aload #4
    //   160: astore #7
    //   162: aload #4
    //   164: ifnonnull -> 176
    //   167: new java/util/ArrayList
    //   170: dup
    //   171: invokespecial <init> : ()V
    //   174: astore #7
    //   176: aload #7
    //   178: aload #6
    //   180: invokevirtual add : (Ljava/lang/Object;)Z
    //   183: pop
    //   184: iinc #5, 1
    //   187: aload #7
    //   189: astore #4
    //   191: goto -> 11
    //   194: iload_2
    //   195: ifne -> 285
    //   198: iconst_0
    //   199: istore #5
    //   201: iload #5
    //   203: iload_3
    //   204: if_icmpge -> 285
    //   207: aload_1
    //   208: iload #5
    //   210: invokevirtual get : (I)Ljava/lang/Object;
    //   213: checkcast android/view/View
    //   216: astore #7
    //   218: aload #7
    //   220: ifnull -> 279
    //   223: aload #7
    //   225: getfield mPrivateFlags : I
    //   228: sipush #4096
    //   231: iand
    //   232: ifeq -> 279
    //   235: aload #7
    //   237: aload #7
    //   239: getfield mPrivateFlags : I
    //   242: sipush #-4097
    //   245: iand
    //   246: putfield mPrivateFlags : I
    //   249: aload #7
    //   251: getfield mParent : Landroid/view/ViewParent;
    //   254: instanceof android/view/View
    //   257: ifeq -> 273
    //   260: aload #7
    //   262: getfield mParent : Landroid/view/ViewParent;
    //   265: checkcast android/view/View
    //   268: astore #7
    //   270: goto -> 218
    //   273: aconst_null
    //   274: astore #7
    //   276: goto -> 218
    //   279: iinc #5, 1
    //   282: goto -> 201
    //   285: aload_1
    //   286: invokevirtual clear : ()V
    //   289: aload #4
    //   291: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3941	-> 0
    //   #3942	-> 5
    //   #3943	-> 8
    //   #3944	-> 17
    //   #3945	-> 28
    //   #3948	-> 84
    //   #3949	-> 87
    //   #3951	-> 91
    //   #3952	-> 100
    //   #3953	-> 113
    //   #3954	-> 116
    //   #3956	-> 119
    //   #3957	-> 130
    //   #3959	-> 143
    //   #3962	-> 149
    //   #3963	-> 158
    //   #3964	-> 167
    //   #3966	-> 176
    //   #3943	-> 184
    //   #3970	-> 194
    //   #3972	-> 198
    //   #3973	-> 207
    //   #3974	-> 218
    //   #3976	-> 235
    //   #3977	-> 249
    //   #3978	-> 260
    //   #3980	-> 273
    //   #3972	-> 279
    //   #3985	-> 285
    //   #3986	-> 289
  }
  
  public void requestTransparentRegion(View paramView) {
    checkThread();
    View view = this.mView;
    if (view != paramView)
      return; 
    if ((view.mPrivateFlags & 0x200) == 0) {
      paramView = this.mView;
      paramView.mPrivateFlags |= 0x200;
      this.mWindowAttributesChanged = true;
    } 
    requestLayout();
  }
  
  private static int getRootMeasureSpec(int paramInt1, int paramInt2) {
    if (paramInt2 != -2) {
      if (paramInt2 != -1) {
        paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
      } else {
        paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
      } 
    } else {
      paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt1, -2147483648);
    } 
    return paramInt1;
  }
  
  public void onPreDraw(RecordingCanvas paramRecordingCanvas) {
    if (this.mCurScrollY != 0 && this.mHardwareYOffset != 0 && this.mAttachInfo.mThreadedRenderer.isOpaque())
      paramRecordingCanvas.drawColor(-16777216); 
    paramRecordingCanvas.translate(-this.mHardwareXOffset, -this.mHardwareYOffset);
  }
  
  public void onPostDraw(RecordingCanvas paramRecordingCanvas) {
    drawAccessibilityFocusedDrawableIfNeeded((Canvas)paramRecordingCanvas);
    if (this.mUseMTRenderer)
      for (int i = this.mWindowCallbacks.size() - 1; i >= 0; i--)
        ((WindowCallbacks)this.mWindowCallbacks.get(i)).onPostDraw(paramRecordingCanvas);  
  }
  
  void outputDisplayList(View paramView) {
    paramView.mRenderNode.output();
  }
  
  private void profileRendering(boolean paramBoolean) {
    if (this.mProfileRendering) {
      this.mRenderProfilingEnabled = paramBoolean;
      Choreographer.FrameCallback frameCallback = this.mRenderProfiler;
      if (frameCallback != null)
        this.mChoreographer.removeFrameCallback(frameCallback); 
      if (this.mRenderProfilingEnabled) {
        if (this.mRenderProfiler == null)
          this.mRenderProfiler = new Choreographer.FrameCallback() {
              final ViewRootImpl this$0;
              
              public void doFrame(long param1Long) {
                ViewRootImpl.this.mDirty.set(0, 0, ViewRootImpl.this.mWidth, ViewRootImpl.this.mHeight);
                ViewRootImpl.this.scheduleTraversals();
                if (ViewRootImpl.this.mRenderProfilingEnabled)
                  ViewRootImpl.this.mChoreographer.postFrameCallback(ViewRootImpl.this.mRenderProfiler); 
              }
            }; 
        this.mChoreographer.postFrameCallback(this.mRenderProfiler);
      } else {
        this.mRenderProfiler = null;
      } 
    } 
  }
  
  private void trackFPS() {
    long l = System.currentTimeMillis();
    if (this.mFpsStartTime < 0L) {
      this.mFpsPrevTime = l;
      this.mFpsStartTime = l;
      this.mFpsNumFrames = 0;
    } else {
      this.mFpsNumFrames++;
      String str1 = Integer.toHexString(System.identityHashCode(this));
      long l1 = this.mFpsPrevTime;
      long l2 = l - this.mFpsStartTime;
      String str2 = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(str1);
      stringBuilder.append("\tFrame time:\t");
      stringBuilder.append(l - l1);
      Log.v(str2, stringBuilder.toString());
      this.mFpsPrevTime = l;
      if (l2 > 1000L) {
        float f = this.mFpsNumFrames * 1000.0F / (float)l2;
        String str = this.mTag;
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("0x");
        stringBuilder1.append(str1);
        stringBuilder1.append("\tFPS:\t");
        stringBuilder1.append(f);
        Log.v(str, stringBuilder1.toString());
        this.mFpsStartTime = l;
        this.mFpsNumFrames = 0;
      } 
    } 
  }
  
  void drawPending() {
    this.mDrawsNeededToReport++;
  }
  
  void pendingDrawFinished() {
    int i = this.mDrawsNeededToReport;
    if (i != 0) {
      this.mDrawsNeededToReport = --i;
      if (i == 0)
        reportDrawFinished(); 
      return;
    } 
    throw new RuntimeException("Unbalanced drawPending/pendingDrawFinished calls");
  }
  
  private void postDrawFinished() {
    this.mHandler.sendEmptyMessage(29);
  }
  
  private void reportDrawFinished() {
    try {
      this.mDrawsNeededToReport = 0;
      this.mWindowSession.finishDrawing(this.mWindow, this.mSurfaceChangedTransaction);
    } catch (RemoteException remoteException) {}
  }
  
  private void performDraw() {
    boolean bool1;
    if (this.mAttachInfo.mDisplayState == 1 && !this.mReportNextDraw)
      return; 
    if (this.mView == null)
      return; 
    if (this.mFullRedrawNeeded || this.mReportNextDraw) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    this.mFullRedrawNeeded = false;
    this.mIsDrawing = true;
    Trace.traceBegin(8L, "draw");
    boolean bool2 = false;
    boolean bool3 = this.mReportNextDraw;
    boolean bool4 = bool2;
    if (this.mAttachInfo.mThreadedRenderer != null) {
      bool4 = bool2;
      if (this.mAttachInfo.mThreadedRenderer.isEnabled()) {
        boolean bool;
        ViewTreeObserver viewTreeObserver = this.mAttachInfo.mTreeObserver;
        ArrayList<Runnable> arrayList = viewTreeObserver.captureFrameCommitCallbacks();
        if (this.mNextDrawUseBLASTSyncTransaction || (arrayList != null && arrayList.size() > 0) || this.mReportNextDraw) {
          bool = true;
        } else {
          bool = false;
        } 
        bool2 = this.mReportNextDraw;
        bool4 = bool2;
        if (bool) {
          Handler handler = this.mAttachInfo.mHandler;
          this.mAttachInfo.mThreadedRenderer.setFrameCompleteCallback(new _$$Lambda$ViewRootImpl$vBfxngTfPtkwcFoa96FB0CWn5ZI(this, handler, bool3, arrayList));
          bool4 = bool2;
        } 
      } 
    } 
    try {
      if (this.mNextDrawUseBLASTSyncTransaction) {
        if (this.mAttachInfo.mThreadedRenderer != null)
          this.mAttachInfo.mThreadedRenderer.pause(); 
        this.mNextReportConsumeBLAST = true;
        this.mNextDrawUseBLASTSyncTransaction = false;
        if (this.mBlastBufferQueue != null)
          this.mBlastBufferQueue.setNextTransaction(this.mRtBLASTSyncTransaction); 
      } 
      bool2 = draw(bool1);
      bool1 = bool4;
      if (bool4) {
        bool1 = bool4;
        if (!bool2) {
          if (this.mAttachInfo.mThreadedRenderer != null)
            this.mAttachInfo.mThreadedRenderer.setFrameCompleteCallback(null); 
          bool1 = false;
          finishBLASTSync(true);
        } 
      } 
      this.mIsDrawing = false;
      Trace.traceEnd(8L);
      if (this.mAttachInfo.mPendingAnimatingRenderNodes != null) {
        int i = this.mAttachInfo.mPendingAnimatingRenderNodes.size();
        for (byte b = 0; b < i; b++)
          ((RenderNode)this.mAttachInfo.mPendingAnimatingRenderNodes.get(b)).endAllAnimators(); 
        this.mAttachInfo.mPendingAnimatingRenderNodes.clear();
      } 
      return;
    } finally {
      this.mIsDrawing = false;
      Trace.traceEnd(8L);
    } 
  }
  
  private boolean isContentCaptureEnabled() {
    int i = this.mContentCaptureEnabled;
    byte b = 2;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("isContentCaptureEnabled(): invalid state ");
          stringBuilder.append(this.mContentCaptureEnabled);
          Log.w("ViewRootImpl", stringBuilder.toString());
          return false;
        } 
        return false;
      } 
      return true;
    } 
    boolean bool = isContentCaptureReallyEnabled();
    if (bool)
      b = 1; 
    this.mContentCaptureEnabled = b;
    return bool;
  }
  
  private boolean isContentCaptureReallyEnabled() {
    if (this.mContext.getContentCaptureOptions() == null)
      return false; 
    ContentCaptureManager contentCaptureManager = this.mAttachInfo.getContentCaptureManager(this.mContext);
    if (contentCaptureManager == null || !contentCaptureManager.isContentCaptureEnabled())
      return false; 
    return true;
  }
  
  private void performContentCaptureInitialReport() {
    this.mPerformContentCapture = false;
    null = this.mView;
    if (Trace.isTagEnabled(8L)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("dispatchContentCapture() for ");
      stringBuilder.append(getClass().getSimpleName());
      String str = stringBuilder.toString();
      Trace.traceBegin(8L, str);
    } 
    try {
      boolean bool = isContentCaptureEnabled();
      if (!bool)
        return; 
      null.dispatchInitialProvideContentCaptureStructure();
      return;
    } finally {
      Trace.traceEnd(8L);
    } 
  }
  
  private void handleContentCaptureFlush() {
    if (Trace.isTagEnabled(8L)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("flushContentCapture for ");
      stringBuilder.append(getClass().getSimpleName());
      String str = stringBuilder.toString();
      Trace.traceBegin(8L, str);
    } 
    try {
      boolean bool = isContentCaptureEnabled();
      if (!bool)
        return; 
      ContentCaptureManager contentCaptureManager = this.mAttachInfo.mContentCaptureManager;
      if (contentCaptureManager == null) {
        Log.w("ViewRootImpl", "No ContentCapture on AttachInfo");
        return;
      } 
      contentCaptureManager.flush(2);
      return;
    } finally {
      Trace.traceEnd(8L);
    } 
  }
  
  private boolean draw(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mSurface : Landroid/view/Surface;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual isValid : ()Z
    //   9: ifne -> 69
    //   12: getstatic android/view/ViewRootImpl.DEBUG_PANIC : Z
    //   15: ifeq -> 67
    //   18: new java/lang/StringBuilder
    //   21: dup
    //   22: invokespecial <init> : ()V
    //   25: astore_3
    //   26: aload_3
    //   27: ldc_w ' draw  --mSurface release,nothing draw. this='
    //   30: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   33: pop
    //   34: aload_3
    //   35: aload_0
    //   36: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   39: pop
    //   40: aload_3
    //   41: ldc_w ' mNativeObject '
    //   44: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: aload_3
    //   49: aload_2
    //   50: getfield mNativeObject : J
    //   53: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   56: pop
    //   57: ldc 'ViewRootImpl'
    //   59: aload_3
    //   60: invokevirtual toString : ()Ljava/lang/String;
    //   63: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   66: pop
    //   67: iconst_0
    //   68: ireturn
    //   69: getstatic android/view/ViewRootImpl.sFirstDrawComplete : Z
    //   72: ifne -> 138
    //   75: getstatic android/view/ViewRootImpl.sFirstDrawHandlers : Ljava/util/ArrayList;
    //   78: astore_3
    //   79: aload_3
    //   80: monitorenter
    //   81: iconst_1
    //   82: putstatic android/view/ViewRootImpl.sFirstDrawComplete : Z
    //   85: getstatic android/view/ViewRootImpl.sFirstDrawHandlers : Ljava/util/ArrayList;
    //   88: invokevirtual size : ()I
    //   91: istore #4
    //   93: iconst_0
    //   94: istore #5
    //   96: iload #5
    //   98: iload #4
    //   100: if_icmpge -> 128
    //   103: aload_0
    //   104: getfield mHandler : Landroid/view/ViewRootImpl$ViewRootHandler;
    //   107: getstatic android/view/ViewRootImpl.sFirstDrawHandlers : Ljava/util/ArrayList;
    //   110: iload #5
    //   112: invokevirtual get : (I)Ljava/lang/Object;
    //   115: checkcast java/lang/Runnable
    //   118: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   121: pop
    //   122: iinc #5, 1
    //   125: goto -> 96
    //   128: aload_3
    //   129: monitorexit
    //   130: goto -> 138
    //   133: astore_2
    //   134: aload_3
    //   135: monitorexit
    //   136: aload_2
    //   137: athrow
    //   138: aconst_null
    //   139: astore_3
    //   140: aload_0
    //   141: aconst_null
    //   142: iconst_0
    //   143: invokevirtual scrollToRectOrFocus : (Landroid/graphics/Rect;Z)Z
    //   146: pop
    //   147: aload_0
    //   148: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   151: getfield mViewScrollChanged : Z
    //   154: ifeq -> 175
    //   157: aload_0
    //   158: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   161: iconst_0
    //   162: putfield mViewScrollChanged : Z
    //   165: aload_0
    //   166: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   169: getfield mTreeObserver : Landroid/view/ViewTreeObserver;
    //   172: invokevirtual dispatchOnScrollChanged : ()V
    //   175: aload_0
    //   176: getfield mScroller : Landroid/widget/Scroller;
    //   179: astore #6
    //   181: aload #6
    //   183: ifnull -> 200
    //   186: aload #6
    //   188: invokevirtual computeScrollOffset : ()Z
    //   191: ifeq -> 200
    //   194: iconst_1
    //   195: istore #4
    //   197: goto -> 203
    //   200: iconst_0
    //   201: istore #4
    //   203: iload #4
    //   205: ifeq -> 220
    //   208: aload_0
    //   209: getfield mScroller : Landroid/widget/Scroller;
    //   212: invokevirtual getCurrY : ()I
    //   215: istore #5
    //   217: goto -> 226
    //   220: aload_0
    //   221: getfield mScrollY : I
    //   224: istore #5
    //   226: aload_0
    //   227: getfield mCurScrollY : I
    //   230: iload #5
    //   232: if_icmpeq -> 272
    //   235: aload_0
    //   236: iload #5
    //   238: putfield mCurScrollY : I
    //   241: aload_0
    //   242: getfield mView : Landroid/view/View;
    //   245: astore #6
    //   247: aload #6
    //   249: instanceof com/android/internal/view/RootViewSurfaceTaker
    //   252: ifeq -> 267
    //   255: aload #6
    //   257: checkcast com/android/internal/view/RootViewSurfaceTaker
    //   260: iload #5
    //   262: invokeinterface onRootViewScrollYChanged : (I)V
    //   267: iconst_1
    //   268: istore_1
    //   269: goto -> 272
    //   272: aload_0
    //   273: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   276: getfield mApplicationScale : F
    //   279: fstore #7
    //   281: aload_0
    //   282: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   285: getfield mScalingRequired : Z
    //   288: istore #8
    //   290: aload_0
    //   291: getfield mDirty : Landroid/graphics/Rect;
    //   294: astore #6
    //   296: aload_0
    //   297: getfield mSurfaceHolder : Lcom/android/internal/view/BaseSurfaceHolder;
    //   300: ifnull -> 328
    //   303: aload #6
    //   305: invokevirtual setEmpty : ()V
    //   308: iload #4
    //   310: ifeq -> 326
    //   313: aload_0
    //   314: getfield mScroller : Landroid/widget/Scroller;
    //   317: astore_3
    //   318: aload_3
    //   319: ifnull -> 326
    //   322: aload_3
    //   323: invokevirtual abortAnimation : ()V
    //   326: iconst_0
    //   327: ireturn
    //   328: iload_1
    //   329: ifeq -> 365
    //   332: aload #6
    //   334: iconst_0
    //   335: iconst_0
    //   336: aload_0
    //   337: getfield mWidth : I
    //   340: i2f
    //   341: fload #7
    //   343: fmul
    //   344: ldc_w 0.5
    //   347: fadd
    //   348: f2i
    //   349: aload_0
    //   350: getfield mHeight : I
    //   353: i2f
    //   354: fload #7
    //   356: fmul
    //   357: ldc_w 0.5
    //   360: fadd
    //   361: f2i
    //   362: invokevirtual set : (IIII)V
    //   365: getstatic android/view/ViewRootImpl.DEBUG_DRAW : Z
    //   368: ifeq -> 616
    //   371: aload_0
    //   372: getfield mTag : Ljava/lang/String;
    //   375: astore #9
    //   377: new java/lang/StringBuilder
    //   380: dup
    //   381: invokespecial <init> : ()V
    //   384: astore #10
    //   386: aload #10
    //   388: ldc_w 'Draw '
    //   391: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   394: pop
    //   395: aload #10
    //   397: aload_0
    //   398: getfield mView : Landroid/view/View;
    //   401: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   404: pop
    //   405: aload #10
    //   407: ldc_w '/'
    //   410: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   413: pop
    //   414: aload_0
    //   415: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   418: astore #11
    //   420: aload #10
    //   422: aload #11
    //   424: invokevirtual getTitle : ()Ljava/lang/CharSequence;
    //   427: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   430: pop
    //   431: aload #10
    //   433: ldc_w ': dirty={'
    //   436: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   439: pop
    //   440: aload #10
    //   442: aload #6
    //   444: getfield left : I
    //   447: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   450: pop
    //   451: aload #10
    //   453: ldc_w ','
    //   456: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   459: pop
    //   460: aload #10
    //   462: aload #6
    //   464: getfield top : I
    //   467: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   470: pop
    //   471: aload #10
    //   473: ldc_w ','
    //   476: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   479: pop
    //   480: aload #10
    //   482: aload #6
    //   484: getfield right : I
    //   487: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   490: pop
    //   491: aload #10
    //   493: ldc_w ','
    //   496: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   499: pop
    //   500: aload #10
    //   502: aload #6
    //   504: getfield bottom : I
    //   507: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   510: pop
    //   511: aload #10
    //   513: ldc_w '} surface='
    //   516: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   519: pop
    //   520: aload #10
    //   522: aload_2
    //   523: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   526: pop
    //   527: aload #10
    //   529: ldc_w ' surface.isValid()='
    //   532: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   535: pop
    //   536: aload #10
    //   538: aload_2
    //   539: invokevirtual isValid : ()Z
    //   542: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   545: pop
    //   546: aload #10
    //   548: ldc_w ', appScale:'
    //   551: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   554: pop
    //   555: aload #10
    //   557: fload #7
    //   559: invokevirtual append : (F)Ljava/lang/StringBuilder;
    //   562: pop
    //   563: aload #10
    //   565: ldc_w ', width='
    //   568: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   571: pop
    //   572: aload #10
    //   574: aload_0
    //   575: getfield mWidth : I
    //   578: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   581: pop
    //   582: aload #10
    //   584: ldc_w ', height='
    //   587: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   590: pop
    //   591: aload #10
    //   593: aload_0
    //   594: getfield mHeight : I
    //   597: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   600: pop
    //   601: aload #10
    //   603: invokevirtual toString : ()Ljava/lang/String;
    //   606: astore #11
    //   608: aload #9
    //   610: aload #11
    //   612: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   615: pop
    //   616: aload_0
    //   617: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   620: getfield mTreeObserver : Landroid/view/ViewTreeObserver;
    //   623: invokevirtual dispatchOnDraw : ()V
    //   626: aload_0
    //   627: getfield mCanvasOffsetX : I
    //   630: ineg
    //   631: istore #12
    //   633: aload_0
    //   634: getfield mCanvasOffsetY : I
    //   637: ineg
    //   638: iload #5
    //   640: iadd
    //   641: istore #13
    //   643: aload_0
    //   644: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   647: astore #9
    //   649: aload #9
    //   651: ifnull -> 660
    //   654: aload #9
    //   656: getfield surfaceInsets : Landroid/graphics/Rect;
    //   659: astore_3
    //   660: aload_3
    //   661: ifnull -> 702
    //   664: aload_3
    //   665: getfield left : I
    //   668: istore #5
    //   670: iload #13
    //   672: aload_3
    //   673: getfield top : I
    //   676: isub
    //   677: istore #13
    //   679: aload #6
    //   681: aload_3
    //   682: getfield left : I
    //   685: aload_3
    //   686: getfield right : I
    //   689: invokevirtual offset : (II)V
    //   692: iload #12
    //   694: iload #5
    //   696: isub
    //   697: istore #12
    //   699: goto -> 702
    //   702: aload_0
    //   703: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   706: getfield mAccessibilityFocusDrawable : Landroid/graphics/drawable/Drawable;
    //   709: astore #9
    //   711: aload #9
    //   713: ifnull -> 760
    //   716: aload_0
    //   717: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   720: getfield mTmpInvalRect : Landroid/graphics/Rect;
    //   723: astore #11
    //   725: aload_0
    //   726: aload #11
    //   728: invokespecial getAccessibilityFocusedRect : (Landroid/graphics/Rect;)Z
    //   731: istore_1
    //   732: iload_1
    //   733: ifne -> 741
    //   736: aload #11
    //   738: invokevirtual setEmpty : ()V
    //   741: aload #11
    //   743: aload #9
    //   745: invokevirtual getBounds : ()Landroid/graphics/Rect;
    //   748: invokevirtual equals : (Ljava/lang/Object;)Z
    //   751: ifne -> 760
    //   754: iconst_1
    //   755: istore #5
    //   757: goto -> 763
    //   760: iconst_0
    //   761: istore #5
    //   763: aload_0
    //   764: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   767: astore #11
    //   769: aload_0
    //   770: getfield mChoreographer : Landroid/view/Choreographer;
    //   773: astore #9
    //   775: aload #11
    //   777: aload #9
    //   779: invokevirtual getFrameTimeNanos : ()J
    //   782: ldc2_w 1000000
    //   785: ldiv
    //   786: putfield mDrawingTime : J
    //   789: iconst_0
    //   790: istore_1
    //   791: aload #6
    //   793: invokevirtual isEmpty : ()Z
    //   796: ifeq -> 817
    //   799: aload_0
    //   800: getfield mIsAnimating : Z
    //   803: ifne -> 817
    //   806: iload #5
    //   808: ifeq -> 814
    //   811: goto -> 817
    //   814: goto -> 1140
    //   817: aload_0
    //   818: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   821: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   824: ifnull -> 985
    //   827: aload_0
    //   828: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   831: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   834: invokevirtual isEnabled : ()Z
    //   837: ifeq -> 985
    //   840: iload #5
    //   842: ifne -> 861
    //   845: aload_0
    //   846: getfield mInvalidateRootRequested : Z
    //   849: ifeq -> 855
    //   852: goto -> 861
    //   855: iconst_0
    //   856: istore #5
    //   858: goto -> 864
    //   861: iconst_1
    //   862: istore #5
    //   864: aload_0
    //   865: iconst_0
    //   866: putfield mInvalidateRootRequested : Z
    //   869: aload_0
    //   870: iconst_0
    //   871: putfield mIsAnimating : Z
    //   874: aload_0
    //   875: getfield mHardwareYOffset : I
    //   878: iload #13
    //   880: if_icmpne -> 892
    //   883: aload_0
    //   884: getfield mHardwareXOffset : I
    //   887: iload #12
    //   889: if_icmpeq -> 907
    //   892: aload_0
    //   893: iload #13
    //   895: putfield mHardwareYOffset : I
    //   898: aload_0
    //   899: iload #12
    //   901: putfield mHardwareXOffset : I
    //   904: iconst_1
    //   905: istore #5
    //   907: iload #5
    //   909: ifeq -> 922
    //   912: aload_0
    //   913: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   916: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   919: invokevirtual invalidateRoot : ()V
    //   922: aload #6
    //   924: invokevirtual setEmpty : ()V
    //   927: aload_0
    //   928: invokespecial updateContentDrawBounds : ()Z
    //   931: istore_1
    //   932: aload_0
    //   933: getfield mReportNextDraw : Z
    //   936: ifeq -> 953
    //   939: aload_0
    //   940: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   943: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   946: iconst_0
    //   947: invokevirtual setStopped : (Z)V
    //   950: goto -> 953
    //   953: iload_1
    //   954: ifeq -> 961
    //   957: aload_0
    //   958: invokespecial requestDrawWindow : ()V
    //   961: iconst_1
    //   962: istore_1
    //   963: aload_0
    //   964: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   967: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   970: aload_0
    //   971: getfield mView : Landroid/view/View;
    //   974: aload_0
    //   975: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   978: aload_0
    //   979: invokevirtual draw : (Landroid/view/View;Landroid/view/View$AttachInfo;Landroid/view/ThreadedRenderer$DrawCallbacks;)V
    //   982: goto -> 1140
    //   985: aload_0
    //   986: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   989: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   992: ifnull -> 1117
    //   995: aload_0
    //   996: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   999: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   1002: astore #9
    //   1004: aload #9
    //   1006: invokevirtual isEnabled : ()Z
    //   1009: ifne -> 1114
    //   1012: aload_0
    //   1013: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   1016: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   1019: astore #9
    //   1021: aload #9
    //   1023: invokevirtual isRequested : ()Z
    //   1026: ifeq -> 1111
    //   1029: aload_0
    //   1030: getfield mSurface : Landroid/view/Surface;
    //   1033: astore #9
    //   1035: aload #9
    //   1037: invokevirtual isValid : ()Z
    //   1040: ifeq -> 1108
    //   1043: aload_0
    //   1044: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   1047: getfield mThreadedRenderer : Landroid/view/ThreadedRenderer;
    //   1050: astore #6
    //   1052: aload_0
    //   1053: getfield mWidth : I
    //   1056: istore #5
    //   1058: aload_0
    //   1059: getfield mHeight : I
    //   1062: istore #4
    //   1064: aload_0
    //   1065: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   1068: astore_2
    //   1069: aload #6
    //   1071: iload #5
    //   1073: iload #4
    //   1075: aload_2
    //   1076: aload_0
    //   1077: getfield mSurface : Landroid/view/Surface;
    //   1080: aload_3
    //   1081: invokevirtual initializeIfNeeded : (IILandroid/view/View$AttachInfo;Landroid/view/Surface;Landroid/graphics/Rect;)Z
    //   1084: pop
    //   1085: aload_0
    //   1086: iconst_1
    //   1087: putfield mFullRedrawNeeded : Z
    //   1090: aload_0
    //   1091: invokevirtual scheduleTraversals : ()V
    //   1094: iconst_0
    //   1095: ireturn
    //   1096: astore_3
    //   1097: goto -> 1101
    //   1100: astore_3
    //   1101: aload_0
    //   1102: aload_3
    //   1103: invokespecial handleOutOfResourcesException : (Landroid/view/Surface$OutOfResourcesException;)V
    //   1106: iconst_0
    //   1107: ireturn
    //   1108: goto -> 1117
    //   1111: goto -> 1117
    //   1114: goto -> 1117
    //   1117: aload_0
    //   1118: aload_2
    //   1119: aload_0
    //   1120: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   1123: iload #12
    //   1125: iload #13
    //   1127: iload #8
    //   1129: aload #6
    //   1131: aload_3
    //   1132: invokespecial drawSoftware : (Landroid/view/Surface;Landroid/view/View$AttachInfo;IIZLandroid/graphics/Rect;Landroid/graphics/Rect;)Z
    //   1135: ifne -> 1140
    //   1138: iconst_0
    //   1139: ireturn
    //   1140: iload #4
    //   1142: ifeq -> 1154
    //   1145: aload_0
    //   1146: iconst_1
    //   1147: putfield mFullRedrawNeeded : Z
    //   1150: aload_0
    //   1151: invokevirtual scheduleTraversals : ()V
    //   1154: iload_1
    //   1155: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4370	-> 0
    //   #4371	-> 5
    //   #4374	-> 12
    //   #4376	-> 67
    //   #4383	-> 69
    //   #4384	-> 75
    //   #4385	-> 81
    //   #4386	-> 85
    //   #4387	-> 93
    //   #4388	-> 103
    //   #4387	-> 122
    //   #4390	-> 128
    //   #4393	-> 138
    //   #4395	-> 147
    //   #4396	-> 157
    //   #4397	-> 165
    //   #4400	-> 175
    //   #4402	-> 203
    //   #4403	-> 208
    //   #4405	-> 220
    //   #4407	-> 226
    //   #4408	-> 235
    //   #4409	-> 241
    //   #4410	-> 241
    //   #4411	-> 255
    //   #4415	-> 267
    //   #4407	-> 272
    //   #4415	-> 272
    //   #4416	-> 281
    //   #4418	-> 290
    //   #4419	-> 296
    //   #4421	-> 303
    //   #4422	-> 308
    //   #4423	-> 322
    //   #4425	-> 326
    //   #4428	-> 328
    //   #4429	-> 332
    //   #4432	-> 365
    //   #4433	-> 371
    //   #4434	-> 420
    //   #4437	-> 536
    //   #4433	-> 608
    //   #4441	-> 616
    //   #4443	-> 626
    //   #4444	-> 633
    //   #4445	-> 643
    //   #4446	-> 649
    //   #4447	-> 660
    //   #4448	-> 664
    //   #4449	-> 670
    //   #4452	-> 679
    //   #4447	-> 702
    //   #4455	-> 702
    //   #4456	-> 702
    //   #4457	-> 711
    //   #4458	-> 716
    //   #4459	-> 725
    //   #4460	-> 732
    //   #4461	-> 736
    //   #4463	-> 741
    //   #4464	-> 754
    //   #4468	-> 760
    //   #4469	-> 775
    //   #4471	-> 789
    //   #4472	-> 791
    //   #4473	-> 817
    //   #4475	-> 840
    //   #4476	-> 864
    //   #4479	-> 869
    //   #4481	-> 874
    //   #4482	-> 892
    //   #4483	-> 898
    //   #4484	-> 904
    //   #4487	-> 907
    //   #4488	-> 912
    //   #4491	-> 922
    //   #4495	-> 927
    //   #4497	-> 932
    //   #4501	-> 939
    //   #4497	-> 953
    //   #4504	-> 953
    //   #4505	-> 957
    //   #4508	-> 961
    //   #4510	-> 963
    //   #4511	-> 982
    //   #4473	-> 985
    //   #4520	-> 985
    //   #4521	-> 1004
    //   #4522	-> 1021
    //   #4523	-> 1035
    //   #4526	-> 1043
    //   #4531	-> 1085
    //   #4533	-> 1085
    //   #4534	-> 1090
    //   #4535	-> 1094
    //   #4528	-> 1096
    //   #4529	-> 1101
    //   #4530	-> 1106
    //   #4523	-> 1108
    //   #4522	-> 1111
    //   #4521	-> 1114
    //   #4520	-> 1117
    //   #4538	-> 1117
    //   #4540	-> 1138
    //   #4545	-> 1140
    //   #4546	-> 1145
    //   #4547	-> 1150
    //   #4549	-> 1154
    // Exception table:
    //   from	to	target	type
    //   81	85	133	finally
    //   85	93	133	finally
    //   103	122	133	finally
    //   128	130	133	finally
    //   134	136	133	finally
    //   1043	1069	1100	android/view/Surface$OutOfResourcesException
    //   1069	1085	1096	android/view/Surface$OutOfResourcesException
  }
  
  private boolean drawSoftware(Surface paramSurface, View.AttachInfo paramAttachInfo, int paramInt1, int paramInt2, boolean paramBoolean, Rect paramRect1, Rect paramRect2) {
    int i, j;
    if (paramRect2 != null) {
      i = paramRect2.left;
      j = paramInt2 + paramRect2.top;
      i = paramInt1 + i;
    } else {
      j = paramInt2;
      i = paramInt1;
    } 
    int k = -i, m = -j;
    try {
      paramRect1.offset(k, m);
      k = paramRect1.left;
      k = paramRect1.top;
      k = paramRect1.right;
      k = paramRect1.bottom;
      Canvas canvas = this.mSurface.lockCanvas(paramRect1);
      canvas.setDensity(this.mDensity);
      paramRect1.offset(i, j);
      try {
        if (DEBUG_DRAW) {
          String str1 = this.mTag;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Surface ");
          stringBuilder.append(paramSurface);
          stringBuilder.append(" drawing to bitmap w=");
          stringBuilder.append(canvas.getWidth());
          stringBuilder.append(", h=");
          stringBuilder.append(canvas.getHeight());
          String str2 = stringBuilder.toString();
          Log.v(str1, str2);
        } 
        if (!canvas.isOpaque() || paramInt2 != 0 || paramInt1 != 0)
          canvas.drawColor(0, PorterDuff.Mode.CLEAR); 
        paramRect1.setEmpty();
        this.mIsAnimating = false;
        View view = this.mView;
        view.mPrivateFlags |= 0x20;
        if (DEBUG_DRAW) {
          Context context = this.mView.getContext();
          String str1 = this.mTag;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Drawing: package:");
          stringBuilder.append(context.getPackageName());
          stringBuilder.append(", metrics=");
          stringBuilder.append(context.getResources().getDisplayMetrics());
          stringBuilder.append(", compatibilityInfo=");
          stringBuilder.append(context.getResources().getCompatibilityInfo());
          String str2 = stringBuilder.toString();
          Log.i(str1, str2);
        } 
        canvas.translate(-paramInt1, -paramInt2);
        if (this.mTranslator != null)
          this.mTranslator.translateCanvas(canvas); 
        if (paramBoolean) {
          paramInt1 = this.mNoncompatDensity;
        } else {
          paramInt1 = 0;
        } 
        canvas.setScreenDensity(paramInt1);
        this.mView.draw(canvas);
        drawAccessibilityFocusedDrawableIfNeeded(canvas);
        try {
          paramSurface.unlockCanvasAndPost(canvas);
          return true;
        } catch (IllegalArgumentException illegalArgumentException) {
          Log.e(this.mTag, "Could not unlock surface", illegalArgumentException);
          this.mLayoutRequested = true;
        } 
      } finally {}
      return false;
    } catch (OutOfResourcesException outOfResourcesException) {
      handleOutOfResourcesException(outOfResourcesException);
      paramRect1.offset(i, j);
      return false;
    } catch (IllegalArgumentException illegalArgumentException) {
      Log.e(this.mTag, "Could not lock surface", illegalArgumentException);
      this.mLayoutRequested = true;
      paramRect1.offset(i, j);
      return false;
    } finally {}
    paramRect1.offset(i, j);
    throw paramSurface;
  }
  
  private void drawAccessibilityFocusedDrawableIfNeeded(Canvas paramCanvas) {
    Rect rect = this.mAttachInfo.mTmpInvalRect;
    if (getAccessibilityFocusedRect(rect)) {
      Drawable drawable = getAccessibilityFocusedDrawable();
      if (drawable != null) {
        drawable.setBounds(rect);
        drawable.draw(paramCanvas);
      } 
    } else if (this.mAttachInfo.mAccessibilityFocusDrawable != null) {
      this.mAttachInfo.mAccessibilityFocusDrawable.setBounds(0, 0, 0, 0);
    } 
  }
  
  private boolean getAccessibilityFocusedRect(Rect paramRect) {
    AccessibilityManager accessibilityManager = AccessibilityManager.getInstance(this.mView.mContext);
    if (!accessibilityManager.isEnabled() || !accessibilityManager.isTouchExplorationEnabled())
      return false; 
    View view = this.mAccessibilityFocusedHost;
    if (view == null || view.mAttachInfo == null)
      return false; 
    AccessibilityNodeProvider accessibilityNodeProvider = view.getAccessibilityNodeProvider();
    if (accessibilityNodeProvider == null) {
      view.getBoundsOnScreen(paramRect, true);
    } else {
      AccessibilityNodeInfo accessibilityNodeInfo = this.mAccessibilityFocusedVirtualView;
      if (accessibilityNodeInfo != null) {
        accessibilityNodeInfo.getBoundsInScreen(paramRect);
      } else {
        return false;
      } 
    } 
    View.AttachInfo attachInfo = this.mAttachInfo;
    paramRect.offset(0, attachInfo.mViewRootImpl.mScrollY);
    paramRect.offset(-attachInfo.mWindowLeft, -attachInfo.mWindowTop);
    if (!paramRect.intersect(0, 0, attachInfo.mViewRootImpl.mWidth, attachInfo.mViewRootImpl.mHeight))
      paramRect.setEmpty(); 
    return paramRect.isEmpty() ^ true;
  }
  
  private Drawable getAccessibilityFocusedDrawable() {
    if (this.mAttachInfo.mAccessibilityFocusDrawable == null) {
      TypedValue typedValue = new TypedValue();
      boolean bool = this.mView.mContext.getTheme().resolveAttribute(17956871, typedValue, true);
      if (bool) {
        View.AttachInfo attachInfo = this.mAttachInfo;
        Context context = this.mView.mContext;
        int i = typedValue.resourceId;
        attachInfo.mAccessibilityFocusDrawable = context.getDrawable(i);
      } 
    } 
    return this.mAttachInfo.mAccessibilityFocusDrawable;
  }
  
  void updateSystemGestureExclusionRectsForView(View paramView) {
    this.mGestureExclusionTracker.updateRectsForView(paramView);
    this.mHandler.sendEmptyMessage(32);
  }
  
  void systemGestureExclusionChanged() {
    List<Rect> list = this.mGestureExclusionTracker.computeChangedRects();
    if (list != null && this.mView != null)
      try {
        this.mWindowSession.reportSystemGestureExclusionChanged(this.mWindow, list);
        ViewTreeObserver viewTreeObserver = this.mAttachInfo.mTreeObserver;
        viewTreeObserver.dispatchOnSystemGestureExclusionRectsChanged(list);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
  }
  
  void updateLocationInParentDisplay(int paramInt1, int paramInt2) {
    View.AttachInfo attachInfo = this.mAttachInfo;
    if (attachInfo != null) {
      Point point = attachInfo.mLocationInParentDisplay;
      if (!point.equals(paramInt1, paramInt2))
        this.mAttachInfo.mLocationInParentDisplay.set(paramInt1, paramInt2); 
    } 
  }
  
  public void setRootSystemGestureExclusionRects(List<Rect> paramList) {
    this.mGestureExclusionTracker.setRootSystemGestureExclusionRects(paramList);
    this.mHandler.sendEmptyMessage(32);
  }
  
  public List<Rect> getRootSystemGestureExclusionRects() {
    return this.mGestureExclusionTracker.getRootSystemGestureExclusionRects();
  }
  
  public void requestInvalidateRootRenderNode() {
    this.mInvalidateRootRequested = true;
  }
  
  boolean scrollToRectOrFocus(Rect paramRect, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: iconst_0
    //   2: invokevirtual getWindowInsets : (Z)Landroid/view/WindowInsets;
    //   5: invokevirtual getSystemWindowInsetsAsRect : ()Landroid/graphics/Rect;
    //   8: astore_3
    //   9: aload_0
    //   10: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   13: getfield mVisibleInsets : Landroid/graphics/Rect;
    //   16: astore #4
    //   18: iconst_0
    //   19: istore #5
    //   21: iconst_0
    //   22: istore #6
    //   24: aload #4
    //   26: getfield left : I
    //   29: aload_3
    //   30: getfield left : I
    //   33: if_icmpgt -> 76
    //   36: aload #4
    //   38: getfield top : I
    //   41: aload_3
    //   42: getfield top : I
    //   45: if_icmpgt -> 76
    //   48: aload #4
    //   50: getfield right : I
    //   53: aload_3
    //   54: getfield right : I
    //   57: if_icmpgt -> 76
    //   60: iload #6
    //   62: istore #7
    //   64: aload #4
    //   66: getfield bottom : I
    //   69: aload_3
    //   70: getfield bottom : I
    //   73: if_icmple -> 396
    //   76: aload_0
    //   77: getfield mScrollY : I
    //   80: istore #8
    //   82: aload_0
    //   83: getfield mView : Landroid/view/View;
    //   86: invokevirtual findFocus : ()Landroid/view/View;
    //   89: astore #9
    //   91: aload #9
    //   93: ifnonnull -> 98
    //   96: iconst_0
    //   97: ireturn
    //   98: aload_0
    //   99: getfield mLastScrolledFocus : Ljava/lang/ref/WeakReference;
    //   102: astore_3
    //   103: aload_3
    //   104: ifnull -> 118
    //   107: aload_3
    //   108: invokevirtual get : ()Ljava/lang/Object;
    //   111: checkcast android/view/View
    //   114: astore_3
    //   115: goto -> 120
    //   118: aconst_null
    //   119: astore_3
    //   120: aload #9
    //   122: aload_3
    //   123: if_acmpeq -> 128
    //   126: aconst_null
    //   127: astore_1
    //   128: aload #9
    //   130: aload_3
    //   131: if_acmpne -> 156
    //   134: aload_0
    //   135: getfield mScrollMayChange : Z
    //   138: ifne -> 156
    //   141: aload_1
    //   142: ifnonnull -> 156
    //   145: iload #8
    //   147: istore #5
    //   149: iload #6
    //   151: istore #7
    //   153: goto -> 396
    //   156: aload_0
    //   157: new java/lang/ref/WeakReference
    //   160: dup
    //   161: aload #9
    //   163: invokespecial <init> : (Ljava/lang/Object;)V
    //   166: putfield mLastScrolledFocus : Ljava/lang/ref/WeakReference;
    //   169: aload_0
    //   170: iconst_0
    //   171: putfield mScrollMayChange : Z
    //   174: iload #8
    //   176: istore #5
    //   178: iload #6
    //   180: istore #7
    //   182: aload #9
    //   184: aload_0
    //   185: getfield mVisRect : Landroid/graphics/Rect;
    //   188: aconst_null
    //   189: invokevirtual getGlobalVisibleRect : (Landroid/graphics/Rect;Landroid/graphics/Point;)Z
    //   192: ifeq -> 396
    //   195: aload_1
    //   196: ifnonnull -> 236
    //   199: aload #9
    //   201: aload_0
    //   202: getfield mTempRect : Landroid/graphics/Rect;
    //   205: invokevirtual getFocusedRect : (Landroid/graphics/Rect;)V
    //   208: aload_0
    //   209: getfield mView : Landroid/view/View;
    //   212: astore_1
    //   213: aload_1
    //   214: instanceof android/view/ViewGroup
    //   217: ifeq -> 244
    //   220: aload_1
    //   221: checkcast android/view/ViewGroup
    //   224: aload #9
    //   226: aload_0
    //   227: getfield mTempRect : Landroid/graphics/Rect;
    //   230: invokevirtual offsetDescendantRectToMyCoords : (Landroid/view/View;Landroid/graphics/Rect;)V
    //   233: goto -> 244
    //   236: aload_0
    //   237: getfield mTempRect : Landroid/graphics/Rect;
    //   240: aload_1
    //   241: invokevirtual set : (Landroid/graphics/Rect;)V
    //   244: iload #8
    //   246: istore #5
    //   248: iload #6
    //   250: istore #7
    //   252: aload_0
    //   253: getfield mTempRect : Landroid/graphics/Rect;
    //   256: aload_0
    //   257: getfield mVisRect : Landroid/graphics/Rect;
    //   260: invokevirtual intersect : (Landroid/graphics/Rect;)Z
    //   263: ifeq -> 396
    //   266: aload_0
    //   267: getfield mTempRect : Landroid/graphics/Rect;
    //   270: invokevirtual height : ()I
    //   273: istore #5
    //   275: aload_0
    //   276: getfield mView : Landroid/view/View;
    //   279: astore_1
    //   280: iload #5
    //   282: aload_1
    //   283: invokevirtual getHeight : ()I
    //   286: aload #4
    //   288: getfield top : I
    //   291: isub
    //   292: aload #4
    //   294: getfield bottom : I
    //   297: isub
    //   298: if_icmple -> 308
    //   301: iload #8
    //   303: istore #5
    //   305: goto -> 393
    //   308: aload_0
    //   309: getfield mTempRect : Landroid/graphics/Rect;
    //   312: getfield top : I
    //   315: aload #4
    //   317: getfield top : I
    //   320: if_icmpge -> 341
    //   323: aload_0
    //   324: getfield mTempRect : Landroid/graphics/Rect;
    //   327: getfield top : I
    //   330: aload #4
    //   332: getfield top : I
    //   335: isub
    //   336: istore #5
    //   338: goto -> 393
    //   341: aload_0
    //   342: getfield mTempRect : Landroid/graphics/Rect;
    //   345: getfield bottom : I
    //   348: aload_0
    //   349: getfield mView : Landroid/view/View;
    //   352: invokevirtual getHeight : ()I
    //   355: aload #4
    //   357: getfield bottom : I
    //   360: isub
    //   361: if_icmple -> 390
    //   364: aload_0
    //   365: getfield mTempRect : Landroid/graphics/Rect;
    //   368: getfield bottom : I
    //   371: aload_0
    //   372: getfield mView : Landroid/view/View;
    //   375: invokevirtual getHeight : ()I
    //   378: aload #4
    //   380: getfield bottom : I
    //   383: isub
    //   384: isub
    //   385: istore #5
    //   387: goto -> 393
    //   390: iconst_0
    //   391: istore #5
    //   393: iconst_1
    //   394: istore #7
    //   396: iload #5
    //   398: aload_0
    //   399: getfield mScrollY : I
    //   402: if_icmpeq -> 480
    //   405: iload_2
    //   406: ifne -> 461
    //   409: aload_0
    //   410: getfield mScroller : Landroid/widget/Scroller;
    //   413: ifnonnull -> 434
    //   416: aload_0
    //   417: new android/widget/Scroller
    //   420: dup
    //   421: aload_0
    //   422: getfield mView : Landroid/view/View;
    //   425: invokevirtual getContext : ()Landroid/content/Context;
    //   428: invokespecial <init> : (Landroid/content/Context;)V
    //   431: putfield mScroller : Landroid/widget/Scroller;
    //   434: aload_0
    //   435: getfield mScroller : Landroid/widget/Scroller;
    //   438: astore_1
    //   439: aload_0
    //   440: getfield mScrollY : I
    //   443: istore #8
    //   445: aload_1
    //   446: iconst_0
    //   447: iload #8
    //   449: iconst_0
    //   450: iload #5
    //   452: iload #8
    //   454: isub
    //   455: invokevirtual startScroll : (IIII)V
    //   458: goto -> 474
    //   461: aload_0
    //   462: getfield mScroller : Landroid/widget/Scroller;
    //   465: astore_1
    //   466: aload_1
    //   467: ifnull -> 474
    //   470: aload_1
    //   471: invokevirtual abortAnimation : ()V
    //   474: aload_0
    //   475: iload #5
    //   477: putfield mScrollY : I
    //   480: iload #7
    //   482: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4771	-> 0
    //   #4772	-> 9
    //   #4773	-> 18
    //   #4774	-> 21
    //   #4776	-> 24
    //   #4782	-> 76
    //   #4789	-> 82
    //   #4790	-> 91
    //   #4791	-> 96
    //   #4793	-> 98
    //   #4794	-> 120
    //   #4798	-> 126
    //   #4803	-> 128
    //   #4813	-> 156
    //   #4814	-> 169
    //   #4817	-> 174
    //   #4822	-> 195
    //   #4823	-> 199
    //   #4826	-> 208
    //   #4827	-> 220
    //   #4835	-> 236
    //   #4841	-> 244
    //   #4845	-> 266
    //   #4846	-> 280
    //   #4857	-> 308
    //   #4858	-> 323
    //   #4861	-> 341
    //   #4862	-> 364
    //   #4866	-> 390
    //   #4868	-> 393
    //   #4874	-> 396
    //   #4877	-> 405
    //   #4878	-> 409
    //   #4879	-> 416
    //   #4881	-> 434
    //   #4882	-> 461
    //   #4883	-> 470
    //   #4885	-> 474
    //   #4888	-> 480
  }
  
  public View getAccessibilityFocusedHost() {
    return this.mAccessibilityFocusedHost;
  }
  
  public AccessibilityNodeInfo getAccessibilityFocusedVirtualView() {
    return this.mAccessibilityFocusedVirtualView;
  }
  
  void setAccessibilityFocus(View paramView, AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    if (this.mAccessibilityFocusedVirtualView != null) {
      AccessibilityNodeInfo accessibilityNodeInfo = this.mAccessibilityFocusedVirtualView;
      View view1 = this.mAccessibilityFocusedHost;
      this.mAccessibilityFocusedHost = null;
      this.mAccessibilityFocusedVirtualView = null;
      view1.clearAccessibilityFocusNoCallbacks(64);
      AccessibilityNodeProvider accessibilityNodeProvider = view1.getAccessibilityNodeProvider();
      if (accessibilityNodeProvider != null) {
        accessibilityNodeInfo.getBoundsInParent(this.mTempRect);
        view1.invalidate(this.mTempRect);
        long l = accessibilityNodeInfo.getSourceNodeId();
        int i = AccessibilityNodeInfo.getVirtualDescendantId(l);
        accessibilityNodeProvider.performAction(i, 128, null);
      } 
      accessibilityNodeInfo.recycle();
    } 
    View view = this.mAccessibilityFocusedHost;
    if (view != null && view != paramView)
      view.clearAccessibilityFocusNoCallbacks(64); 
    this.mAccessibilityFocusedHost = paramView;
    this.mAccessibilityFocusedVirtualView = paramAccessibilityNodeInfo;
    if (this.mAttachInfo.mThreadedRenderer != null)
      this.mAttachInfo.mThreadedRenderer.invalidateRoot(); 
  }
  
  boolean hasPointerCapture() {
    return this.mPointerCapture;
  }
  
  void requestPointerCapture(boolean paramBoolean) {
    if (this.mPointerCapture == paramBoolean)
      return; 
    InputManager.getInstance().requestPointerCapture(this.mAttachInfo.mWindowToken, paramBoolean);
  }
  
  private void handlePointerCaptureChanged(boolean paramBoolean) {
    if (this.mPointerCapture == paramBoolean)
      return; 
    this.mPointerCapture = paramBoolean;
    View view = this.mView;
    if (view != null)
      view.dispatchPointerCaptureChanged(paramBoolean); 
  }
  
  private boolean hasColorModeChanged(int paramInt) {
    boolean bool;
    if (this.mAttachInfo.mThreadedRenderer == null)
      return false; 
    if (paramInt == 1) {
      bool = true;
    } else {
      bool = false;
    } 
    if (this.mAttachInfo.mThreadedRenderer.isWideGamut() == bool)
      return false; 
    if (bool && !this.mContext.getResources().getConfiguration().isScreenWideColorGamut())
      return false; 
    return true;
  }
  
  public void requestChildFocus(View paramView1, View paramView2) {
    checkThread();
    scheduleTraversals();
  }
  
  public void clearChildFocus(View paramView) {
    checkThread();
    scheduleTraversals();
  }
  
  public ViewParent getParentForAccessibility() {
    return null;
  }
  
  public void focusableViewAvailable(View paramView) {
    checkThread();
    View view = this.mView;
    if (view != null)
      if (!view.hasFocus()) {
        if (sAlwaysAssignFocus || !this.mAttachInfo.mInTouchMode)
          paramView.requestFocus(); 
      } else {
        View view1 = this.mView.findFocus();
        if (view1 instanceof ViewGroup) {
          view = view1;
          if (view.getDescendantFocusability() == 262144 && isViewDescendantOf(paramView, view1))
            paramView.requestFocus(); 
        } 
      }  
  }
  
  public void recomputeViewAttributes(View paramView) {
    checkThread();
    if (this.mView == paramView) {
      this.mAttachInfo.mRecomputeGlobalAttributes = true;
      if (!this.mWillDrawSoon)
        scheduleTraversals(); 
    } 
  }
  
  void dispatchDetachedFromWindow() {
    this.mInsetsController.onWindowFocusLost();
    InputStage inputStage = this.mFirstInputStage;
    if (inputStage != null)
      inputStage.onDetachedFromWindow(); 
    View view = this.mView;
    if (view != null && view.mAttachInfo != null) {
      this.mAttachInfo.mTreeObserver.dispatchOnWindowAttachedChange(false);
      this.mView.dispatchDetachedFromWindow();
      this.mViewRootHooks.dispatchDetachedFromWindow(this.mView);
    } 
    this.mAccessibilityInteractionConnectionManager.ensureNoConnection();
    this.mAccessibilityManager.removeAccessibilityStateChangeListener(this.mAccessibilityInteractionConnectionManager);
    this.mAccessibilityManager.removeHighTextContrastStateChangeListener(this.mHighContrastTextManager);
    removeSendWindowContentChangedCallback();
    destroyHardwareRenderer();
    setAccessibilityFocus(null, null);
    this.mInsetsController.cancelExistingAnimations();
    this.mView.assignParent(null);
    this.mView = null;
    this.mAttachInfo.mRootView = null;
    destroySurface();
    InputQueue.Callback callback = this.mInputQueueCallback;
    if (callback != null) {
      InputQueue inputQueue = this.mInputQueue;
      if (inputQueue != null) {
        callback.onInputQueueDestroyed(inputQueue);
        this.mInputQueue.dispose();
        this.mInputQueueCallback = null;
        this.mInputQueue = null;
      } 
    } 
    try {
      this.mWindowSession.remove(this.mWindow);
    } catch (RemoteException remoteException) {}
    WindowInputEventReceiver windowInputEventReceiver = this.mInputEventReceiver;
    if (windowInputEventReceiver != null) {
      windowInputEventReceiver.dispose();
      this.mInputEventReceiver = null;
    } 
    this.mDisplayManager.unregisterDisplayListener(this.mDisplayListener);
    unscheduleTraversals();
  }
  
  private void performConfigurationChange(MergedConfiguration paramMergedConfiguration, boolean paramBoolean, int paramInt) {
    if (paramMergedConfiguration != null) {
      Configuration configuration1 = paramMergedConfiguration.getGlobalConfiguration();
      Configuration configuration2 = paramMergedConfiguration.getOverrideConfiguration();
      if (DEBUG_CONFIGURATION) {
        String str1 = this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Applying new config to window ");
        WindowManager.LayoutParams layoutParams = this.mWindowAttributes;
        stringBuilder.append(layoutParams.getTitle());
        stringBuilder.append(", globalConfig: ");
        stringBuilder.append(configuration1);
        stringBuilder.append(", overrideConfig: ");
        stringBuilder.append(configuration2);
        String str2 = stringBuilder.toString();
        Log.v(str1, str2);
      } 
      CompatibilityInfo compatibilityInfo = this.mDisplay.getDisplayAdjustments().getCompatibilityInfo();
      null = configuration1;
      if (!compatibilityInfo.equals(CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO)) {
        null = new Configuration(configuration1);
        compatibilityInfo.applyToConfiguration(this.mNoncompatDensity, null);
      } 
      synchronized (sConfigCallbacks) {
        for (int i = sConfigCallbacks.size() - 1; i >= 0; i--)
          ((ConfigChangedCallback)sConfigCallbacks.get(i)).onConfigurationChanged(null); 
        this.mManager.setDarkModeProgress(this.mView, null);
        this.mLastReportedMergedConfiguration.setConfiguration(null, configuration2);
        View view = this.mView;
        if (view != null && view instanceof DecorView)
          ((DecorView)view).setLastReportedMergedConfiguration(this.mLastReportedMergedConfiguration.getMergedConfiguration()); 
        this.mForceNextConfigUpdate = paramBoolean;
        ActivityConfigCallback activityConfigCallback = this.mActivityConfigCallback;
        if (activityConfigCallback != null) {
          activityConfigCallback.onConfigurationChanged(configuration2, paramInt);
        } else {
          updateConfiguration(paramInt);
        } 
        this.mForceNextConfigUpdate = false;
        return;
      } 
    } 
    throw new IllegalArgumentException("No merged config provided.");
  }
  
  public void updateConfiguration(int paramInt) {
    View view = this.mView;
    if (view == null)
      return; 
    Resources resources = view.getResources();
    Configuration configuration = resources.getConfiguration();
    if (paramInt != -1)
      onMovedToDisplay(paramInt, configuration); 
    if (this.mForceNextConfigUpdate || this.mLastConfigurationFromResources.diff(configuration) != 0) {
      updateInternalDisplay(this.mDisplay.getDisplayId(), resources);
      int i = this.mLastConfigurationFromResources.getLayoutDirection();
      paramInt = configuration.getLayoutDirection();
      this.mLastConfigurationFromResources.setTo(configuration);
      if (i != paramInt && this.mViewLayoutDirectionInitial == 2)
        this.mView.setLayoutDirection(paramInt); 
      this.mView.dispatchConfigurationChanged(configuration);
      this.mForceNextWindowRelayout = true;
      requestLayout();
    } 
    updateForceDarkMode();
  }
  
  public static boolean isViewDescendantOf(View paramView1, View paramView2) {
    boolean bool = true;
    if (paramView1 == paramView2)
      return true; 
    ViewParent viewParent = paramView1.getParent();
    if (!(viewParent instanceof ViewGroup) || !isViewDescendantOf((View)viewParent, paramView2))
      bool = false; 
    return bool;
  }
  
  private static void forceLayout(View paramView) {
    paramView.forceLayout();
    if (paramView instanceof ViewGroup) {
      paramView = paramView;
      int i = paramView.getChildCount();
      for (byte b = 0; b < i; b++) {
        if (paramView.getChildAt(b) != null)
          forceLayout(paramView.getChildAt(b)); 
      } 
    } 
  }
  
  final class ViewRootHandler extends Handler {
    final ViewRootImpl this$0;
    
    public String getMessageName(Message param1Message) {
      switch (param1Message.what) {
        default:
          return super.getMessageName(param1Message);
        case 35:
          return "MSG_HIDE_INSETS";
        case 34:
          return "MSG_SHOW_INSETS";
        case 33:
          return "MSG_LOCATION_IN_PARENT_DISPLAY_CHANGED";
        case 32:
          return "MSG_SYSTEM_GESTURE_EXCLUSION_CHANGED";
        case 31:
          return "MSG_INSETS_CONTROL_CHANGED";
        case 30:
          return "MSG_INSETS_CHANGED";
        case 29:
          return "MSG_DRAW_FINISHED";
        case 28:
          return "MSG_POINTER_CAPTURE_CHANGED";
        case 27:
          return "MSG_UPDATE_POINTER_ICON";
        case 25:
          return "MSG_DISPATCH_WINDOW_SHOWN";
        case 24:
          return "MSG_SYNTHESIZE_INPUT_EVENT";
        case 23:
          return "MSG_WINDOW_MOVED";
        case 21:
          return "MSG_CLEAR_ACCESSIBILITY_FOCUS_HOST";
        case 19:
          return "MSG_PROCESS_INPUT_EVENTS";
        case 18:
          return "MSG_UPDATE_CONFIGURATION";
        case 17:
          return "MSG_DISPATCH_SYSTEM_UI_VISIBILITY";
        case 16:
          return "MSG_DISPATCH_DRAG_LOCATION_EVENT";
        case 15:
          return "MSG_DISPATCH_DRAG_EVENT";
        case 14:
          return "MSG_CLOSE_SYSTEM_DIALOGS";
        case 13:
          return "MSG_CHECK_FOCUS";
        case 12:
          return "MSG_DISPATCH_KEY_FROM_AUTOFILL";
        case 11:
          return "MSG_DISPATCH_KEY_FROM_IME";
        case 9:
          return "MSG_DISPATCH_GET_NEW_SURFACE";
        case 8:
          return "MSG_DISPATCH_APP_VISIBILITY";
        case 7:
          return "MSG_DISPATCH_INPUT_EVENT";
        case 6:
          return "MSG_WINDOW_FOCUS_CHANGED";
        case 5:
          return "MSG_RESIZED_REPORT";
        case 4:
          return "MSG_RESIZED";
        case 3:
          return "MSG_DIE";
        case 2:
          return "MSG_INVALIDATE_RECT";
        case 1:
          break;
      } 
      return "MSG_INVALIDATE";
    }
    
    public boolean sendMessageAtTime(Message param1Message, long param1Long) {
      if (param1Message.what != 26 || param1Message.obj != null)
        return super.sendMessageAtTime(param1Message, param1Long); 
      throw new NullPointerException("Attempted to call MSG_REQUEST_KEYBOARD_SHORTCUTS with null receiver:");
    }
    
    public void handleMessage(Message param1Message) {
      // Byte code:
      //   0: aload_1
      //   1: getfield what : I
      //   4: istore_2
      //   5: iconst_m1
      //   6: istore_3
      //   7: iconst_0
      //   8: istore #4
      //   10: iconst_0
      //   11: istore #5
      //   13: iconst_0
      //   14: istore #6
      //   16: iconst_0
      //   17: istore #7
      //   19: iconst_0
      //   20: istore #8
      //   22: iload_2
      //   23: tableswitch default -> 180, 1 -> 1723, 2 -> 1685, 3 -> 1675, 4 -> 1188, 5 -> 1321, 6 -> 1131, 7 -> 1083, 8 -> 1053, 9 -> 1043, 10 -> 180, 11 -> 990, 12 -> 968, 13 -> 952, 14 -> 922, 15 -> 892, 16 -> 892, 17 -> 875, 18 -> 777, 19 -> 759, 20 -> 180, 21 -> 747, 22 -> 721, 23 -> 581, 24 -> 558, 25 -> 548, 26 -> 521, 27 -> 502, 28 -> 476, 29 -> 466, 30 -> 416, 31 -> 370, 32 -> 360, 33 -> 342, 34 -> 236, 35 -> 200, 36 -> 183
      //   180: goto -> 1733
      //   183: aload_0
      //   184: getfield this$0 : Landroid/view/ViewRootImpl;
      //   187: aload_1
      //   188: getfield obj : Ljava/lang/Object;
      //   191: checkcast android/view/IScrollCaptureController
      //   194: invokestatic access$1800 : (Landroid/view/ViewRootImpl;Landroid/view/IScrollCaptureController;)V
      //   197: goto -> 1733
      //   200: aload_0
      //   201: getfield this$0 : Landroid/view/ViewRootImpl;
      //   204: invokestatic access$1000 : (Landroid/view/ViewRootImpl;)Landroid/view/InsetsController;
      //   207: astore #9
      //   209: aload_1
      //   210: getfield arg1 : I
      //   213: istore_3
      //   214: aload_1
      //   215: getfield arg2 : I
      //   218: iconst_1
      //   219: if_icmpne -> 225
      //   222: iconst_1
      //   223: istore #8
      //   225: aload #9
      //   227: iload_3
      //   228: iload #8
      //   230: invokevirtual hide : (IZ)V
      //   233: goto -> 1733
      //   236: aload_0
      //   237: getfield this$0 : Landroid/view/ViewRootImpl;
      //   240: getfield mView : Landroid/view/View;
      //   243: ifnonnull -> 302
      //   246: aload_1
      //   247: getfield arg1 : I
      //   250: istore_3
      //   251: aload_1
      //   252: getfield arg2 : I
      //   255: iconst_1
      //   256: if_icmpne -> 265
      //   259: iconst_1
      //   260: istore #8
      //   262: goto -> 268
      //   265: iconst_0
      //   266: istore #8
      //   268: ldc 'Calling showInsets(%d,%b) on window that no longer has views.'
      //   270: iconst_2
      //   271: anewarray java/lang/Object
      //   274: dup
      //   275: iconst_0
      //   276: iload_3
      //   277: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   280: aastore
      //   281: dup
      //   282: iconst_1
      //   283: iload #8
      //   285: invokestatic valueOf : (Z)Ljava/lang/Boolean;
      //   288: aastore
      //   289: invokestatic format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
      //   292: astore #9
      //   294: ldc 'ViewRootImpl'
      //   296: aload #9
      //   298: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   301: pop
      //   302: aload_0
      //   303: getfield this$0 : Landroid/view/ViewRootImpl;
      //   306: invokestatic access$1000 : (Landroid/view/ViewRootImpl;)Landroid/view/InsetsController;
      //   309: astore #9
      //   311: aload_1
      //   312: getfield arg1 : I
      //   315: istore_3
      //   316: iload #4
      //   318: istore #8
      //   320: aload_1
      //   321: getfield arg2 : I
      //   324: iconst_1
      //   325: if_icmpne -> 331
      //   328: iconst_1
      //   329: istore #8
      //   331: aload #9
      //   333: iload_3
      //   334: iload #8
      //   336: invokevirtual show : (IZ)V
      //   339: goto -> 1733
      //   342: aload_0
      //   343: getfield this$0 : Landroid/view/ViewRootImpl;
      //   346: aload_1
      //   347: getfield arg1 : I
      //   350: aload_1
      //   351: getfield arg2 : I
      //   354: invokevirtual updateLocationInParentDisplay : (II)V
      //   357: goto -> 1733
      //   360: aload_0
      //   361: getfield this$0 : Landroid/view/ViewRootImpl;
      //   364: invokevirtual systemGestureExclusionChanged : ()V
      //   367: goto -> 1733
      //   370: aload_1
      //   371: getfield obj : Ljava/lang/Object;
      //   374: checkcast com/android/internal/os/SomeArgs
      //   377: astore_1
      //   378: aload_0
      //   379: getfield this$0 : Landroid/view/ViewRootImpl;
      //   382: invokestatic access$1000 : (Landroid/view/ViewRootImpl;)Landroid/view/InsetsController;
      //   385: aload_1
      //   386: getfield arg1 : Ljava/lang/Object;
      //   389: checkcast android/view/InsetsState
      //   392: invokevirtual onStateChanged : (Landroid/view/InsetsState;)Z
      //   395: pop
      //   396: aload_0
      //   397: getfield this$0 : Landroid/view/ViewRootImpl;
      //   400: invokestatic access$1000 : (Landroid/view/ViewRootImpl;)Landroid/view/InsetsController;
      //   403: aload_1
      //   404: getfield arg2 : Ljava/lang/Object;
      //   407: checkcast [Landroid/view/InsetsSourceControl;
      //   410: invokevirtual onControlsChanged : ([Landroid/view/InsetsSourceControl;)V
      //   413: goto -> 1733
      //   416: aload_0
      //   417: getfield this$0 : Landroid/view/ViewRootImpl;
      //   420: invokestatic access$1000 : (Landroid/view/ViewRootImpl;)Landroid/view/InsetsController;
      //   423: aload_1
      //   424: getfield obj : Ljava/lang/Object;
      //   427: checkcast android/view/InsetsState
      //   430: invokevirtual onStateChanged : (Landroid/view/InsetsState;)Z
      //   433: pop
      //   434: aload_0
      //   435: getfield this$0 : Landroid/view/ViewRootImpl;
      //   438: getfield mColorViewRootUtil : Landroid/view/IOplusViewRootUtil;
      //   441: ifnull -> 1733
      //   444: aload_0
      //   445: getfield this$0 : Landroid/view/ViewRootImpl;
      //   448: getfield mColorViewRootUtil : Landroid/view/IOplusViewRootUtil;
      //   451: aload_0
      //   452: getfield this$0 : Landroid/view/ViewRootImpl;
      //   455: getfield mContext : Landroid/content/Context;
      //   458: invokeinterface checkGestureConfig : (Landroid/content/Context;)V
      //   463: goto -> 1733
      //   466: aload_0
      //   467: getfield this$0 : Landroid/view/ViewRootImpl;
      //   470: invokevirtual pendingDrawFinished : ()V
      //   473: goto -> 1733
      //   476: iload #5
      //   478: istore #8
      //   480: aload_1
      //   481: getfield arg1 : I
      //   484: ifeq -> 490
      //   487: iconst_1
      //   488: istore #8
      //   490: aload_0
      //   491: getfield this$0 : Landroid/view/ViewRootImpl;
      //   494: iload #8
      //   496: invokestatic access$1700 : (Landroid/view/ViewRootImpl;Z)V
      //   499: goto -> 1733
      //   502: aload_1
      //   503: getfield obj : Ljava/lang/Object;
      //   506: checkcast android/view/MotionEvent
      //   509: astore_1
      //   510: aload_0
      //   511: getfield this$0 : Landroid/view/ViewRootImpl;
      //   514: aload_1
      //   515: invokestatic access$1600 : (Landroid/view/ViewRootImpl;Landroid/view/MotionEvent;)V
      //   518: goto -> 1733
      //   521: aload_1
      //   522: getfield obj : Ljava/lang/Object;
      //   525: checkcast com/android/internal/os/IResultReceiver
      //   528: astore #9
      //   530: aload_1
      //   531: getfield arg1 : I
      //   534: istore_3
      //   535: aload_0
      //   536: getfield this$0 : Landroid/view/ViewRootImpl;
      //   539: aload #9
      //   541: iload_3
      //   542: invokevirtual handleRequestKeyboardShortcuts : (Lcom/android/internal/os/IResultReceiver;I)V
      //   545: goto -> 1733
      //   548: aload_0
      //   549: getfield this$0 : Landroid/view/ViewRootImpl;
      //   552: invokevirtual handleDispatchWindowShown : ()V
      //   555: goto -> 1733
      //   558: aload_1
      //   559: getfield obj : Ljava/lang/Object;
      //   562: checkcast android/view/InputEvent
      //   565: astore_1
      //   566: aload_0
      //   567: getfield this$0 : Landroid/view/ViewRootImpl;
      //   570: aload_1
      //   571: aconst_null
      //   572: bipush #32
      //   574: iconst_1
      //   575: invokevirtual enqueueInputEvent : (Landroid/view/InputEvent;Landroid/view/InputEventReceiver;IZ)V
      //   578: goto -> 1733
      //   581: aload_0
      //   582: getfield this$0 : Landroid/view/ViewRootImpl;
      //   585: getfield mAdded : Z
      //   588: ifeq -> 1733
      //   591: aload_0
      //   592: getfield this$0 : Landroid/view/ViewRootImpl;
      //   595: getfield mWinFrame : Landroid/graphics/Rect;
      //   598: invokevirtual width : ()I
      //   601: istore #10
      //   603: aload_0
      //   604: getfield this$0 : Landroid/view/ViewRootImpl;
      //   607: getfield mWinFrame : Landroid/graphics/Rect;
      //   610: invokevirtual height : ()I
      //   613: istore #11
      //   615: aload_1
      //   616: getfield arg1 : I
      //   619: istore_3
      //   620: aload_1
      //   621: getfield arg2 : I
      //   624: istore_2
      //   625: aload_0
      //   626: getfield this$0 : Landroid/view/ViewRootImpl;
      //   629: getfield mTmpFrame : Landroid/graphics/Rect;
      //   632: iload_3
      //   633: putfield left : I
      //   636: aload_0
      //   637: getfield this$0 : Landroid/view/ViewRootImpl;
      //   640: getfield mTmpFrame : Landroid/graphics/Rect;
      //   643: iload_3
      //   644: iload #10
      //   646: iadd
      //   647: putfield right : I
      //   650: aload_0
      //   651: getfield this$0 : Landroid/view/ViewRootImpl;
      //   654: getfield mTmpFrame : Landroid/graphics/Rect;
      //   657: iload_2
      //   658: putfield top : I
      //   661: aload_0
      //   662: getfield this$0 : Landroid/view/ViewRootImpl;
      //   665: getfield mTmpFrame : Landroid/graphics/Rect;
      //   668: iload_2
      //   669: iload #11
      //   671: iadd
      //   672: putfield bottom : I
      //   675: aload_0
      //   676: getfield this$0 : Landroid/view/ViewRootImpl;
      //   679: astore_1
      //   680: aload_1
      //   681: aload_1
      //   682: getfield mTmpFrame : Landroid/graphics/Rect;
      //   685: invokestatic access$700 : (Landroid/view/ViewRootImpl;Landroid/graphics/Rect;)V
      //   688: aload_0
      //   689: getfield this$0 : Landroid/view/ViewRootImpl;
      //   692: getfield mPendingBackDropFrame : Landroid/graphics/Rect;
      //   695: aload_0
      //   696: getfield this$0 : Landroid/view/ViewRootImpl;
      //   699: getfield mWinFrame : Landroid/graphics/Rect;
      //   702: invokevirtual set : (Landroid/graphics/Rect;)V
      //   705: aload_0
      //   706: getfield this$0 : Landroid/view/ViewRootImpl;
      //   709: astore_1
      //   710: aload_1
      //   711: aload_1
      //   712: getfield mWinFrame : Landroid/graphics/Rect;
      //   715: invokestatic access$1100 : (Landroid/view/ViewRootImpl;Landroid/graphics/Rect;)V
      //   718: goto -> 1733
      //   721: aload_0
      //   722: getfield this$0 : Landroid/view/ViewRootImpl;
      //   725: getfield mView : Landroid/view/View;
      //   728: ifnull -> 1733
      //   731: aload_0
      //   732: getfield this$0 : Landroid/view/ViewRootImpl;
      //   735: astore_1
      //   736: aload_1
      //   737: aload_1
      //   738: getfield mView : Landroid/view/View;
      //   741: invokevirtual invalidateWorld : (Landroid/view/View;)V
      //   744: goto -> 1733
      //   747: aload_0
      //   748: getfield this$0 : Landroid/view/ViewRootImpl;
      //   751: aconst_null
      //   752: aconst_null
      //   753: invokevirtual setAccessibilityFocus : (Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
      //   756: goto -> 1733
      //   759: aload_0
      //   760: getfield this$0 : Landroid/view/ViewRootImpl;
      //   763: iconst_0
      //   764: putfield mProcessInputEventsScheduled : Z
      //   767: aload_0
      //   768: getfield this$0 : Landroid/view/ViewRootImpl;
      //   771: invokevirtual doProcessInputEvents : ()V
      //   774: goto -> 1733
      //   777: aload_1
      //   778: getfield obj : Ljava/lang/Object;
      //   781: checkcast android/content/res/Configuration
      //   784: astore #9
      //   786: aload_0
      //   787: getfield this$0 : Landroid/view/ViewRootImpl;
      //   790: astore_1
      //   791: aload_1
      //   792: invokestatic access$400 : (Landroid/view/ViewRootImpl;)Landroid/util/MergedConfiguration;
      //   795: invokevirtual getMergedConfiguration : ()Landroid/content/res/Configuration;
      //   798: astore #12
      //   800: aload #9
      //   802: astore_1
      //   803: aload #9
      //   805: aload #12
      //   807: invokevirtual isOtherSeqNewer : (Landroid/content/res/Configuration;)Z
      //   810: ifeq -> 824
      //   813: aload_0
      //   814: getfield this$0 : Landroid/view/ViewRootImpl;
      //   817: invokestatic access$400 : (Landroid/view/ViewRootImpl;)Landroid/util/MergedConfiguration;
      //   820: invokevirtual getGlobalConfiguration : ()Landroid/content/res/Configuration;
      //   823: astore_1
      //   824: aload_0
      //   825: getfield this$0 : Landroid/view/ViewRootImpl;
      //   828: invokestatic access$1500 : (Landroid/view/ViewRootImpl;)Landroid/util/MergedConfiguration;
      //   831: astore #9
      //   833: aload_0
      //   834: getfield this$0 : Landroid/view/ViewRootImpl;
      //   837: astore #12
      //   839: aload #12
      //   841: invokestatic access$400 : (Landroid/view/ViewRootImpl;)Landroid/util/MergedConfiguration;
      //   844: invokevirtual getOverrideConfiguration : ()Landroid/content/res/Configuration;
      //   847: astore #12
      //   849: aload #9
      //   851: aload_1
      //   852: aload #12
      //   854: invokevirtual setConfiguration : (Landroid/content/res/Configuration;Landroid/content/res/Configuration;)V
      //   857: aload_0
      //   858: getfield this$0 : Landroid/view/ViewRootImpl;
      //   861: astore_1
      //   862: aload_1
      //   863: aload_1
      //   864: invokestatic access$1500 : (Landroid/view/ViewRootImpl;)Landroid/util/MergedConfiguration;
      //   867: iconst_0
      //   868: iconst_m1
      //   869: invokestatic access$500 : (Landroid/view/ViewRootImpl;Landroid/util/MergedConfiguration;ZI)V
      //   872: goto -> 1733
      //   875: aload_0
      //   876: getfield this$0 : Landroid/view/ViewRootImpl;
      //   879: aload_1
      //   880: getfield obj : Ljava/lang/Object;
      //   883: checkcast android/view/ViewRootImpl$SystemUiVisibilityInfo
      //   886: invokestatic access$1400 : (Landroid/view/ViewRootImpl;Landroid/view/ViewRootImpl$SystemUiVisibilityInfo;)V
      //   889: goto -> 1733
      //   892: aload_1
      //   893: getfield obj : Ljava/lang/Object;
      //   896: checkcast android/view/DragEvent
      //   899: astore_1
      //   900: aload_1
      //   901: aload_0
      //   902: getfield this$0 : Landroid/view/ViewRootImpl;
      //   905: getfield mLocalDragState : Ljava/lang/Object;
      //   908: putfield mLocalState : Ljava/lang/Object;
      //   911: aload_0
      //   912: getfield this$0 : Landroid/view/ViewRootImpl;
      //   915: aload_1
      //   916: invokestatic access$1300 : (Landroid/view/ViewRootImpl;Landroid/view/DragEvent;)V
      //   919: goto -> 1733
      //   922: aload_0
      //   923: getfield this$0 : Landroid/view/ViewRootImpl;
      //   926: getfield mView : Landroid/view/View;
      //   929: ifnull -> 1733
      //   932: aload_0
      //   933: getfield this$0 : Landroid/view/ViewRootImpl;
      //   936: getfield mView : Landroid/view/View;
      //   939: aload_1
      //   940: getfield obj : Ljava/lang/Object;
      //   943: checkcast java/lang/String
      //   946: invokevirtual onCloseSystemDialogs : (Ljava/lang/String;)V
      //   949: goto -> 1733
      //   952: aload_0
      //   953: getfield this$0 : Landroid/view/ViewRootImpl;
      //   956: invokevirtual getImeFocusController : ()Landroid/view/ImeFocusController;
      //   959: iconst_0
      //   960: iconst_1
      //   961: invokevirtual checkFocus : (ZZ)Z
      //   964: pop
      //   965: goto -> 1733
      //   968: aload_1
      //   969: getfield obj : Ljava/lang/Object;
      //   972: checkcast android/view/KeyEvent
      //   975: astore_1
      //   976: aload_0
      //   977: getfield this$0 : Landroid/view/ViewRootImpl;
      //   980: aload_1
      //   981: aconst_null
      //   982: iconst_0
      //   983: iconst_1
      //   984: invokevirtual enqueueInputEvent : (Landroid/view/InputEvent;Landroid/view/InputEventReceiver;IZ)V
      //   987: goto -> 1733
      //   990: aload_1
      //   991: getfield obj : Ljava/lang/Object;
      //   994: checkcast android/view/KeyEvent
      //   997: astore #9
      //   999: aload #9
      //   1001: astore_1
      //   1002: aload #9
      //   1004: invokevirtual getFlags : ()I
      //   1007: bipush #8
      //   1009: iand
      //   1010: ifeq -> 1029
      //   1013: aload #9
      //   1015: invokevirtual getFlags : ()I
      //   1018: istore_3
      //   1019: aload #9
      //   1021: iload_3
      //   1022: bipush #-9
      //   1024: iand
      //   1025: invokestatic changeFlags : (Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;
      //   1028: astore_1
      //   1029: aload_0
      //   1030: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1033: aload_1
      //   1034: aconst_null
      //   1035: iconst_1
      //   1036: iconst_1
      //   1037: invokevirtual enqueueInputEvent : (Landroid/view/InputEvent;Landroid/view/InputEventReceiver;IZ)V
      //   1040: goto -> 1733
      //   1043: aload_0
      //   1044: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1047: invokevirtual handleGetNewSurface : ()V
      //   1050: goto -> 1733
      //   1053: aload_0
      //   1054: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1057: astore #9
      //   1059: iload #6
      //   1061: istore #8
      //   1063: aload_1
      //   1064: getfield arg1 : I
      //   1067: ifeq -> 1073
      //   1070: iconst_1
      //   1071: istore #8
      //   1073: aload #9
      //   1075: iload #8
      //   1077: invokevirtual handleAppVisibility : (Z)V
      //   1080: goto -> 1733
      //   1083: aload_1
      //   1084: getfield obj : Ljava/lang/Object;
      //   1087: checkcast com/android/internal/os/SomeArgs
      //   1090: astore #12
      //   1092: aload #12
      //   1094: getfield arg1 : Ljava/lang/Object;
      //   1097: checkcast android/view/InputEvent
      //   1100: astore #9
      //   1102: aload #12
      //   1104: getfield arg2 : Ljava/lang/Object;
      //   1107: checkcast android/view/InputEventReceiver
      //   1110: astore_1
      //   1111: aload_0
      //   1112: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1115: aload #9
      //   1117: aload_1
      //   1118: iconst_0
      //   1119: iconst_1
      //   1120: invokevirtual enqueueInputEvent : (Landroid/view/InputEvent;Landroid/view/InputEventReceiver;IZ)V
      //   1123: aload #12
      //   1125: invokevirtual recycle : ()V
      //   1128: goto -> 1733
      //   1131: invokestatic access$100 : ()Z
      //   1134: ifeq -> 1178
      //   1137: new java/lang/StringBuilder
      //   1140: dup
      //   1141: invokespecial <init> : ()V
      //   1144: astore_1
      //   1145: aload_1
      //   1146: ldc_w 'MSG_WINDOW_FOCUS_CHANGED, hasWindowFocus:'
      //   1149: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1152: pop
      //   1153: aload_1
      //   1154: aload_0
      //   1155: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1158: getfield mAttachInfo : Landroid/view/View$AttachInfo;
      //   1161: getfield mHasWindowFocus : Z
      //   1164: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   1167: pop
      //   1168: ldc 'ViewRootImpl'
      //   1170: aload_1
      //   1171: invokevirtual toString : ()Ljava/lang/String;
      //   1174: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   1177: pop
      //   1178: aload_0
      //   1179: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1182: invokestatic access$1200 : (Landroid/view/ViewRootImpl;)V
      //   1185: goto -> 1733
      //   1188: aload_1
      //   1189: getfield obj : Ljava/lang/Object;
      //   1192: checkcast com/android/internal/os/SomeArgs
      //   1195: astore #9
      //   1197: aload_0
      //   1198: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1201: getfield mWinFrame : Landroid/graphics/Rect;
      //   1204: aload #9
      //   1206: getfield arg1 : Ljava/lang/Object;
      //   1209: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1212: ifeq -> 1321
      //   1215: aload_0
      //   1216: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1219: getfield mPendingDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
      //   1222: astore #12
      //   1224: aload #12
      //   1226: invokevirtual get : ()Landroid/view/DisplayCutout;
      //   1229: aload #9
      //   1231: getfield arg9 : Ljava/lang/Object;
      //   1234: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1237: ifeq -> 1321
      //   1240: aload_0
      //   1241: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1244: getfield mPendingBackDropFrame : Landroid/graphics/Rect;
      //   1247: astore #12
      //   1249: aload #9
      //   1251: getfield arg8 : Ljava/lang/Object;
      //   1254: astore #13
      //   1256: aload #12
      //   1258: aload #13
      //   1260: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1263: ifeq -> 1321
      //   1266: aload_0
      //   1267: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1270: astore #12
      //   1272: aload #12
      //   1274: invokestatic access$400 : (Landroid/view/ViewRootImpl;)Landroid/util/MergedConfiguration;
      //   1277: aload #9
      //   1279: getfield arg4 : Ljava/lang/Object;
      //   1282: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1285: ifeq -> 1321
      //   1288: aload #9
      //   1290: getfield argi1 : I
      //   1293: ifne -> 1321
      //   1296: aload_0
      //   1297: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1300: getfield mDisplay : Landroid/view/Display;
      //   1303: astore #12
      //   1305: aload #12
      //   1307: invokevirtual getDisplayId : ()I
      //   1310: aload #9
      //   1312: getfield argi3 : I
      //   1315: if_icmpne -> 1321
      //   1318: goto -> 1733
      //   1321: aload_0
      //   1322: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1325: getfield mAdded : Z
      //   1328: ifeq -> 1733
      //   1331: aload_1
      //   1332: getfield obj : Ljava/lang/Object;
      //   1335: checkcast com/android/internal/os/SomeArgs
      //   1338: astore #9
      //   1340: aload #9
      //   1342: getfield argi3 : I
      //   1345: istore #10
      //   1347: aload #9
      //   1349: getfield arg4 : Ljava/lang/Object;
      //   1352: checkcast android/util/MergedConfiguration
      //   1355: astore #13
      //   1357: aload_0
      //   1358: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1361: getfield mDisplay : Landroid/view/Display;
      //   1364: invokevirtual getDisplayId : ()I
      //   1367: iload #10
      //   1369: if_icmpeq -> 1377
      //   1372: iconst_1
      //   1373: istore_2
      //   1374: goto -> 1379
      //   1377: iconst_0
      //   1378: istore_2
      //   1379: iconst_0
      //   1380: istore #11
      //   1382: aload_0
      //   1383: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1386: invokestatic access$400 : (Landroid/view/ViewRootImpl;)Landroid/util/MergedConfiguration;
      //   1389: aload #13
      //   1391: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1394: ifne -> 1427
      //   1397: aload_0
      //   1398: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1401: astore #12
      //   1403: iload_2
      //   1404: ifeq -> 1413
      //   1407: iload #10
      //   1409: istore_3
      //   1410: goto -> 1413
      //   1413: aload #12
      //   1415: aload #13
      //   1417: iconst_0
      //   1418: iload_3
      //   1419: invokestatic access$500 : (Landroid/view/ViewRootImpl;Landroid/util/MergedConfiguration;ZI)V
      //   1422: iconst_1
      //   1423: istore_3
      //   1424: goto -> 1455
      //   1427: iload #11
      //   1429: istore_3
      //   1430: iload_2
      //   1431: ifeq -> 1455
      //   1434: aload_0
      //   1435: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1438: astore #12
      //   1440: aload #12
      //   1442: iload #10
      //   1444: aload #12
      //   1446: invokestatic access$600 : (Landroid/view/ViewRootImpl;)Landroid/content/res/Configuration;
      //   1449: invokevirtual onMovedToDisplay : (ILandroid/content/res/Configuration;)V
      //   1452: iload #11
      //   1454: istore_3
      //   1455: aload_0
      //   1456: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1459: getfield mWinFrame : Landroid/graphics/Rect;
      //   1462: aload #9
      //   1464: getfield arg1 : Ljava/lang/Object;
      //   1467: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1470: ifeq -> 1506
      //   1473: aload_0
      //   1474: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1477: getfield mPendingDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
      //   1480: astore #12
      //   1482: aload #12
      //   1484: invokevirtual get : ()Landroid/view/DisplayCutout;
      //   1487: aload #9
      //   1489: getfield arg9 : Ljava/lang/Object;
      //   1492: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1495: ifne -> 1501
      //   1498: goto -> 1506
      //   1501: iconst_0
      //   1502: istore_2
      //   1503: goto -> 1508
      //   1506: iconst_1
      //   1507: istore_2
      //   1508: aload_0
      //   1509: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1512: aload #9
      //   1514: getfield arg1 : Ljava/lang/Object;
      //   1517: checkcast android/graphics/Rect
      //   1520: invokestatic access$700 : (Landroid/view/ViewRootImpl;Landroid/graphics/Rect;)V
      //   1523: aload_0
      //   1524: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1527: getfield mPendingDisplayCutout : Landroid/view/DisplayCutout$ParcelableWrapper;
      //   1530: aload #9
      //   1532: getfield arg9 : Ljava/lang/Object;
      //   1535: checkcast android/view/DisplayCutout
      //   1538: invokevirtual set : (Landroid/view/DisplayCutout;)V
      //   1541: aload_0
      //   1542: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1545: getfield mPendingBackDropFrame : Landroid/graphics/Rect;
      //   1548: aload #9
      //   1550: getfield arg8 : Ljava/lang/Object;
      //   1553: checkcast android/graphics/Rect
      //   1556: invokevirtual set : (Landroid/graphics/Rect;)V
      //   1559: aload_0
      //   1560: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1563: astore #12
      //   1565: aload #9
      //   1567: getfield argi1 : I
      //   1570: ifeq -> 1579
      //   1573: iconst_1
      //   1574: istore #8
      //   1576: goto -> 1582
      //   1579: iconst_0
      //   1580: istore #8
      //   1582: aload #12
      //   1584: iload #8
      //   1586: putfield mForceNextWindowRelayout : Z
      //   1589: aload_0
      //   1590: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1593: astore #12
      //   1595: iload #7
      //   1597: istore #8
      //   1599: aload #9
      //   1601: getfield argi2 : I
      //   1604: ifeq -> 1610
      //   1607: iconst_1
      //   1608: istore #8
      //   1610: aload #12
      //   1612: iload #8
      //   1614: putfield mPendingAlwaysConsumeSystemBars : Z
      //   1617: aload #9
      //   1619: invokevirtual recycle : ()V
      //   1622: aload_1
      //   1623: getfield what : I
      //   1626: iconst_5
      //   1627: if_icmpne -> 1637
      //   1630: aload_0
      //   1631: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1634: invokestatic access$800 : (Landroid/view/ViewRootImpl;)V
      //   1637: aload_0
      //   1638: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1641: getfield mView : Landroid/view/View;
      //   1644: ifnull -> 1665
      //   1647: iload_2
      //   1648: ifne -> 1655
      //   1651: iload_3
      //   1652: ifeq -> 1665
      //   1655: aload_0
      //   1656: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1659: getfield mView : Landroid/view/View;
      //   1662: invokestatic access$900 : (Landroid/view/View;)V
      //   1665: aload_0
      //   1666: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1669: invokevirtual requestLayout : ()V
      //   1672: goto -> 1733
      //   1675: aload_0
      //   1676: getfield this$0 : Landroid/view/ViewRootImpl;
      //   1679: invokevirtual doDie : ()V
      //   1682: goto -> 1733
      //   1685: aload_1
      //   1686: getfield obj : Ljava/lang/Object;
      //   1689: checkcast android/view/View$AttachInfo$InvalidateInfo
      //   1692: astore_1
      //   1693: aload_1
      //   1694: getfield target : Landroid/view/View;
      //   1697: aload_1
      //   1698: getfield left : I
      //   1701: aload_1
      //   1702: getfield top : I
      //   1705: aload_1
      //   1706: getfield right : I
      //   1709: aload_1
      //   1710: getfield bottom : I
      //   1713: invokevirtual invalidate : (IIII)V
      //   1716: aload_1
      //   1717: invokevirtual recycle : ()V
      //   1720: goto -> 1733
      //   1723: aload_1
      //   1724: getfield obj : Ljava/lang/Object;
      //   1727: checkcast android/view/View
      //   1730: invokevirtual invalidate : ()V
      //   1733: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5362	-> 0
      //   #5603	-> 183
      //   #5468	-> 200
      //   #5469	-> 233
      //   #5459	-> 236
      //   #5460	-> 246
      //   #5462	-> 251
      //   #5461	-> 268
      //   #5460	-> 294
      //   #5464	-> 302
      //   #5465	-> 339
      //   #5600	-> 342
      //   #5601	-> 357
      //   #5597	-> 360
      //   #5598	-> 367
      //   #5447	-> 370
      //   #5454	-> 378
      //   #5455	-> 396
      //   #5456	-> 413
      //   #5437	-> 416
      //   #5441	-> 434
      //   #5442	-> 444
      //   #5594	-> 466
      //   #5595	-> 473
      //   #5590	-> 476
      //   #5591	-> 490
      //   #5592	-> 499
      //   #5586	-> 502
      //   #5587	-> 510
      //   #5588	-> 518
      //   #5581	-> 521
      //   #5582	-> 530
      //   #5583	-> 535
      //   #5584	-> 545
      //   #5578	-> 548
      //   #5579	-> 555
      //   #5507	-> 558
      //   #5508	-> 566
      //   #5509	-> 578
      //   #5472	-> 581
      //   #5473	-> 591
      //   #5474	-> 603
      //   #5475	-> 615
      //   #5476	-> 620
      //   #5477	-> 625
      //   #5478	-> 636
      //   #5479	-> 650
      //   #5480	-> 661
      //   #5481	-> 675
      //   #5483	-> 688
      //   #5484	-> 705
      //   #5485	-> 718
      //   #5573	-> 721
      //   #5574	-> 731
      //   #5570	-> 747
      //   #5571	-> 756
      //   #5373	-> 759
      //   #5374	-> 767
      //   #5375	-> 774
      //   #5551	-> 777
      //   #5552	-> 786
      //   #5553	-> 791
      //   #5552	-> 800
      //   #5555	-> 813
      //   #5559	-> 824
      //   #5560	-> 839
      //   #5559	-> 849
      //   #5566	-> 857
      //   #5568	-> 872
      //   #5548	-> 875
      //   #5549	-> 889
      //   #5542	-> 892
      //   #5544	-> 900
      //   #5545	-> 911
      //   #5546	-> 919
      //   #5535	-> 922
      //   #5536	-> 932
      //   #5532	-> 952
      //   #5533	-> 965
      //   #5528	-> 968
      //   #5529	-> 976
      //   #5530	-> 987
      //   #5514	-> 990
      //   #5515	-> 999
      //   #5519	-> 1013
      //   #5520	-> 1013
      //   #5519	-> 1019
      //   #5522	-> 1029
      //   #5523	-> 1040
      //   #5380	-> 1043
      //   #5381	-> 1050
      //   #5377	-> 1053
      //   #5378	-> 1080
      //   #5500	-> 1083
      //   #5501	-> 1092
      //   #5502	-> 1102
      //   #5503	-> 1111
      //   #5504	-> 1123
      //   #5505	-> 1128
      //   #5490	-> 1131
      //   #5491	-> 1137
      //   #5494	-> 1178
      //   #5495	-> 1185
      //   #5384	-> 1188
      //   #5385	-> 1197
      //   #5386	-> 1224
      //   #5387	-> 1256
      //   #5388	-> 1272
      //   #5390	-> 1305
      //   #5391	-> 1318
      //   #5395	-> 1321
      //   #5396	-> 1331
      //   #5398	-> 1340
      //   #5399	-> 1347
      //   #5400	-> 1357
      //   #5401	-> 1379
      //   #5403	-> 1382
      //   #5406	-> 1397
      //   #5407	-> 1403
      //   #5408	-> 1407
      //   #5406	-> 1413
      //   #5409	-> 1422
      //   #5410	-> 1427
      //   #5412	-> 1434
      //   #5415	-> 1455
      //   #5416	-> 1482
      //   #5418	-> 1508
      //   #5419	-> 1523
      //   #5420	-> 1541
      //   #5421	-> 1559
      //   #5422	-> 1589
      //   #5424	-> 1617
      //   #5426	-> 1622
      //   #5427	-> 1630
      //   #5430	-> 1637
      //   #5431	-> 1655
      //   #5433	-> 1665
      //   #5434	-> 1672
      //   #5497	-> 1675
      //   #5498	-> 1682
      //   #5367	-> 1685
      //   #5369	-> 1693
      //   #5370	-> 1716
      //   #5371	-> 1720
      //   #5364	-> 1723
      //   #5365	-> 1733
      //   #5606	-> 1733
    }
  }
  
  boolean ensureTouchMode(boolean paramBoolean) {
    if (this.mAttachInfo.mInTouchMode == paramBoolean)
      return false; 
    try {
      this.mWindowSession.setInTouchMode(paramBoolean);
      return ensureTouchModeLocally(paramBoolean);
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException);
    } 
  }
  
  private boolean ensureTouchModeLocally(boolean paramBoolean) {
    if (this.mAttachInfo.mInTouchMode == paramBoolean)
      return false; 
    this.mAttachInfo.mInTouchMode = paramBoolean;
    this.mAttachInfo.mTreeObserver.dispatchOnTouchModeChanged(paramBoolean);
    if (paramBoolean) {
      paramBoolean = enterTouchMode();
    } else {
      paramBoolean = leaveTouchMode();
    } 
    return paramBoolean;
  }
  
  private boolean enterTouchMode() {
    View view = this.mView;
    if (view != null && view.hasFocus()) {
      view = this.mView.findFocus();
      if (view != null && !view.isFocusableInTouchMode()) {
        ViewGroup viewGroup = findAncestorToTakeFocusInTouchMode(view);
        if (viewGroup != null)
          return viewGroup.requestFocus(); 
        view.clearFocusInternal(null, true, false);
        return true;
      } 
    } 
    return false;
  }
  
  private static ViewGroup findAncestorToTakeFocusInTouchMode(View paramView) {
    ViewParent viewParent = paramView.getParent();
    while (viewParent instanceof ViewGroup) {
      viewParent = viewParent;
      if (viewParent.getDescendantFocusability() == 262144 && viewParent.isFocusableInTouchMode())
        return (ViewGroup)viewParent; 
      if (viewParent.isRootNamespace())
        return null; 
      viewParent = viewParent.getParent();
    } 
    return null;
  }
  
  private boolean leaveTouchMode() {
    View view = this.mView;
    if (view != null) {
      if (view.hasFocus()) {
        view = this.mView.findFocus();
        if (!(view instanceof ViewGroup))
          return false; 
        if (((ViewGroup)view).getDescendantFocusability() != 262144)
          return false; 
      } 
      return this.mView.restoreDefaultFocus();
    } 
    return false;
  }
  
  class InputStage {
    protected static final String DEBUG_TAG = "ANR_LOG";
    
    protected static final int FINISH_HANDLED = 1;
    
    protected static final int FINISH_NOT_HANDLED = 2;
    
    protected static final int FORWARD = 0;
    
    protected static final long SPENT_TOO_MUCH_TIME = 500L;
    
    protected long mDeliverTime = 0L;
    
    private final InputStage mNext;
    
    private String mTracePrefix;
    
    final ViewRootImpl this$0;
    
    public InputStage(InputStage param1InputStage) {
      this.mNext = param1InputStage;
    }
    
    public final void deliver(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      this.mDeliverTime = System.currentTimeMillis();
      if ((param1QueuedInputEvent.mFlags & 0x4) != 0) {
        forward(param1QueuedInputEvent);
      } else if (shouldDropInputEvent(param1QueuedInputEvent)) {
        finish(param1QueuedInputEvent, false);
      } else {
        traceEvent(param1QueuedInputEvent, 8L);
        try {
          int i = onProcess(param1QueuedInputEvent);
          Trace.traceEnd(8L);
          return;
        } finally {
          Trace.traceEnd(8L);
        } 
      } 
    }
    
    protected void finish(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent, boolean param1Boolean) {
      param1QueuedInputEvent.mFlags |= 0x4;
      if (param1Boolean)
        param1QueuedInputEvent.mFlags |= 0x8; 
      forward(param1QueuedInputEvent);
    }
    
    protected void forward(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      onDeliverToNext(param1QueuedInputEvent);
    }
    
    protected void apply(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent, int param1Int) {
      if (param1Int == 0) {
        forward(param1QueuedInputEvent);
      } else if (param1Int == 1) {
        finish(param1QueuedInputEvent, true);
      } else {
        if (param1Int == 2) {
          finish(param1QueuedInputEvent, false);
          return;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid result: ");
        stringBuilder.append(param1Int);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    }
    
    protected int onProcess(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      return 0;
    }
    
    protected void onDeliverToNext(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      if (SystemClock.uptimeMillis() - this.mDeliverTime > 500L) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Spent too much time, ");
        stringBuilder.append(Long.toString(SystemClock.uptimeMillis() - this.mDeliverTime));
        stringBuilder.append(" ms in ");
        stringBuilder.append(getClass().getSimpleName());
        Log.v("ANR_LOG", stringBuilder.toString());
      } 
      if (InputLog.DEBUG) {
        String str = ViewRootImpl.this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Done with ");
        stringBuilder.append(getClass().getSimpleName());
        stringBuilder.append(". ");
        stringBuilder.append(param1QueuedInputEvent);
        Log.v(str, stringBuilder.toString());
      } 
      InputStage inputStage = this.mNext;
      if (inputStage != null) {
        inputStage.deliver(param1QueuedInputEvent);
      } else {
        ViewRootImpl.this.finishInputEvent(param1QueuedInputEvent);
      } 
    }
    
    protected void onWindowFocusChanged(boolean param1Boolean) {
      boolean bool;
      if ((ViewRootImpl.this.mLastReportedMergedConfiguration.getMergedConfiguration()).windowConfiguration.getWindowingMode() == 100) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool && ViewRootImpl.this.mView != null && ViewRootImpl.this.mView instanceof DecorView)
        ((DecorView)ViewRootImpl.this.mView).onWindowFocusChangedByRoot(param1Boolean); 
      InputStage inputStage = this.mNext;
      if (inputStage != null)
        inputStage.onWindowFocusChanged(param1Boolean); 
    }
    
    protected void onDetachedFromWindow() {
      InputStage inputStage = this.mNext;
      if (inputStage != null)
        inputStage.onDetachedFromWindow(); 
    }
    
    protected boolean shouldDropInputEvent(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$0 : Landroid/view/ViewRootImpl;
      //   4: getfield mView : Landroid/view/View;
      //   7: ifnull -> 365
      //   10: aload_0
      //   11: getfield this$0 : Landroid/view/ViewRootImpl;
      //   14: getfield mAdded : Z
      //   17: ifne -> 23
      //   20: goto -> 365
      //   23: aload_0
      //   24: getfield this$0 : Landroid/view/ViewRootImpl;
      //   27: getfield mAttachInfo : Landroid/view/View$AttachInfo;
      //   30: getfield mHasWindowFocus : Z
      //   33: ifne -> 61
      //   36: aload_1
      //   37: getfield mEvent : Landroid/view/InputEvent;
      //   40: astore_2
      //   41: aload_2
      //   42: iconst_2
      //   43: invokevirtual isFromSource : (I)Z
      //   46: ifne -> 61
      //   49: aload_0
      //   50: getfield this$0 : Landroid/view/ViewRootImpl;
      //   53: astore_2
      //   54: aload_2
      //   55: invokestatic access$2100 : (Landroid/view/ViewRootImpl;)Z
      //   58: ifeq -> 122
      //   61: aload_0
      //   62: getfield this$0 : Landroid/view/ViewRootImpl;
      //   65: getfield mStopped : Z
      //   68: ifne -> 122
      //   71: aload_0
      //   72: getfield this$0 : Landroid/view/ViewRootImpl;
      //   75: getfield mIsAmbientMode : Z
      //   78: ifeq -> 94
      //   81: aload_1
      //   82: getfield mEvent : Landroid/view/InputEvent;
      //   85: astore_2
      //   86: aload_2
      //   87: iconst_1
      //   88: invokevirtual isFromSource : (I)Z
      //   91: ifeq -> 122
      //   94: aload_0
      //   95: getfield this$0 : Landroid/view/ViewRootImpl;
      //   98: getfield mPausedForTransition : Z
      //   101: ifeq -> 120
      //   104: aload_1
      //   105: getfield mEvent : Landroid/view/InputEvent;
      //   108: astore_2
      //   109: aload_0
      //   110: aload_2
      //   111: invokespecial isBack : (Landroid/view/InputEvent;)Z
      //   114: ifne -> 120
      //   117: goto -> 122
      //   120: iconst_0
      //   121: ireturn
      //   122: aload_1
      //   123: getfield mEvent : Landroid/view/InputEvent;
      //   126: invokestatic isTerminalInputEvent : (Landroid/view/InputEvent;)Z
      //   129: ifeq -> 252
      //   132: aload_1
      //   133: getfield mEvent : Landroid/view/InputEvent;
      //   136: invokevirtual cancel : ()V
      //   139: invokestatic access$100 : ()Z
      //   142: ifeq -> 250
      //   145: aload_0
      //   146: getfield this$0 : Landroid/view/ViewRootImpl;
      //   149: invokestatic access$1900 : (Landroid/view/ViewRootImpl;)Ljava/lang/String;
      //   152: astore_3
      //   153: new java/lang/StringBuilder
      //   156: dup
      //   157: invokespecial <init> : ()V
      //   160: astore_2
      //   161: aload_2
      //   162: ldc_w 'Cancelling event due to no window focus: '
      //   165: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   168: pop
      //   169: aload_2
      //   170: aload_1
      //   171: getfield mEvent : Landroid/view/InputEvent;
      //   174: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   177: pop
      //   178: aload_2
      //   179: ldc_w ', hasFocus:'
      //   182: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   185: pop
      //   186: aload_2
      //   187: aload_0
      //   188: getfield this$0 : Landroid/view/ViewRootImpl;
      //   191: getfield mAttachInfo : Landroid/view/View$AttachInfo;
      //   194: getfield mHasWindowFocus : Z
      //   197: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   200: pop
      //   201: aload_2
      //   202: ldc_w ', mStopped:'
      //   205: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   208: pop
      //   209: aload_2
      //   210: aload_0
      //   211: getfield this$0 : Landroid/view/ViewRootImpl;
      //   214: getfield mStopped : Z
      //   217: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   220: pop
      //   221: aload_2
      //   222: ldc_w ', mPausedForTransition:'
      //   225: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   228: pop
      //   229: aload_2
      //   230: aload_0
      //   231: getfield this$0 : Landroid/view/ViewRootImpl;
      //   234: getfield mPausedForTransition : Z
      //   237: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   240: pop
      //   241: aload_3
      //   242: aload_2
      //   243: invokevirtual toString : ()Ljava/lang/String;
      //   246: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   249: pop
      //   250: iconst_0
      //   251: ireturn
      //   252: invokestatic access$100 : ()Z
      //   255: ifeq -> 363
      //   258: aload_0
      //   259: getfield this$0 : Landroid/view/ViewRootImpl;
      //   262: invokestatic access$1900 : (Landroid/view/ViewRootImpl;)Ljava/lang/String;
      //   265: astore_3
      //   266: new java/lang/StringBuilder
      //   269: dup
      //   270: invokespecial <init> : ()V
      //   273: astore_2
      //   274: aload_2
      //   275: ldc_w 'Dropping event due to no window focus: '
      //   278: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   281: pop
      //   282: aload_2
      //   283: aload_1
      //   284: getfield mEvent : Landroid/view/InputEvent;
      //   287: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   290: pop
      //   291: aload_2
      //   292: ldc_w ', hasFocus:'
      //   295: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   298: pop
      //   299: aload_2
      //   300: aload_0
      //   301: getfield this$0 : Landroid/view/ViewRootImpl;
      //   304: getfield mAttachInfo : Landroid/view/View$AttachInfo;
      //   307: getfield mHasWindowFocus : Z
      //   310: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   313: pop
      //   314: aload_2
      //   315: ldc_w ', mStopped:'
      //   318: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   321: pop
      //   322: aload_2
      //   323: aload_0
      //   324: getfield this$0 : Landroid/view/ViewRootImpl;
      //   327: getfield mStopped : Z
      //   330: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   333: pop
      //   334: aload_2
      //   335: ldc_w ', mPausedForTransition:'
      //   338: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   341: pop
      //   342: aload_2
      //   343: aload_0
      //   344: getfield this$0 : Landroid/view/ViewRootImpl;
      //   347: getfield mPausedForTransition : Z
      //   350: invokevirtual append : (Z)Ljava/lang/StringBuilder;
      //   353: pop
      //   354: aload_3
      //   355: aload_2
      //   356: invokevirtual toString : ()Ljava/lang/String;
      //   359: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   362: pop
      //   363: iconst_1
      //   364: ireturn
      //   365: aload_0
      //   366: getfield this$0 : Landroid/view/ViewRootImpl;
      //   369: invokestatic access$1900 : (Landroid/view/ViewRootImpl;)Ljava/lang/String;
      //   372: astore_3
      //   373: new java/lang/StringBuilder
      //   376: dup
      //   377: invokespecial <init> : ()V
      //   380: astore_2
      //   381: aload_2
      //   382: ldc_w 'Dropping event due to root view being removed: '
      //   385: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   388: pop
      //   389: aload_2
      //   390: aload_1
      //   391: getfield mEvent : Landroid/view/InputEvent;
      //   394: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   397: pop
      //   398: aload_3
      //   399: aload_2
      //   400: invokevirtual toString : ()Ljava/lang/String;
      //   403: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   406: pop
      //   407: iconst_1
      //   408: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #5864	-> 0
      //   #5867	-> 23
      //   #5868	-> 41
      //   #5869	-> 54
      //   #5870	-> 86
      //   #5871	-> 109
      //   #5904	-> 120
      //   #5875	-> 122
      //   #5877	-> 132
      //   #5882	-> 139
      //   #5883	-> 145
      //   #5888	-> 250
      //   #5896	-> 252
      //   #5897	-> 258
      //   #5902	-> 363
      //   #5865	-> 365
      //   #5866	-> 407
    }
    
    void dump(String param1String, PrintWriter param1PrintWriter) {
      InputStage inputStage = this.mNext;
      if (inputStage != null)
        inputStage.dump(param1String, param1PrintWriter); 
    }
    
    private boolean isBack(InputEvent param1InputEvent) {
      boolean bool = param1InputEvent instanceof KeyEvent;
      boolean bool1 = false;
      if (bool) {
        if (((KeyEvent)param1InputEvent).getKeyCode() == 4)
          bool1 = true; 
        return bool1;
      } 
      return false;
    }
    
    private void traceEvent(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent, long param1Long) {
      if (!Trace.isTagEnabled(param1Long))
        return; 
      if (this.mTracePrefix == null)
        this.mTracePrefix = getClass().getSimpleName(); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.mTracePrefix);
      stringBuilder.append(" id=0x");
      InputEvent inputEvent = param1QueuedInputEvent.mEvent;
      stringBuilder.append(Integer.toHexString(inputEvent.getId()));
      String str = stringBuilder.toString();
      Trace.traceBegin(param1Long, str);
    }
  }
  
  abstract class AsyncInputStage extends InputStage {
    protected static final int DEFER = 3;
    
    private ViewRootImpl.QueuedInputEvent mQueueHead;
    
    private int mQueueLength;
    
    private ViewRootImpl.QueuedInputEvent mQueueTail;
    
    private final String mTraceCounter;
    
    final ViewRootImpl this$0;
    
    public AsyncInputStage(ViewRootImpl.InputStage param1InputStage, String param1String) {
      super(param1InputStage);
      this.mTraceCounter = param1String;
    }
    
    protected void defer(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      param1QueuedInputEvent.mFlags |= 0x2;
      enqueue(param1QueuedInputEvent);
    }
    
    protected void forward(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      param1QueuedInputEvent.mFlags &= 0xFFFFFFFD;
      ViewRootImpl.QueuedInputEvent queuedInputEvent1 = this.mQueueHead;
      if (queuedInputEvent1 == null) {
        super.forward(param1QueuedInputEvent);
        return;
      } 
      int i = param1QueuedInputEvent.mEvent.getDeviceId();
      ViewRootImpl.QueuedInputEvent queuedInputEvent2 = null;
      boolean bool = false;
      while (queuedInputEvent1 != null && queuedInputEvent1 != param1QueuedInputEvent) {
        boolean bool1 = bool;
        if (!bool) {
          bool1 = bool;
          if (i == queuedInputEvent1.mEvent.getDeviceId())
            bool1 = true; 
        } 
        queuedInputEvent2 = queuedInputEvent1;
        queuedInputEvent1 = queuedInputEvent1.mNext;
        bool = bool1;
      } 
      if (bool) {
        if (queuedInputEvent1 == null)
          enqueue(param1QueuedInputEvent); 
        return;
      } 
      ViewRootImpl.QueuedInputEvent queuedInputEvent3 = queuedInputEvent1;
      if (queuedInputEvent1 != null) {
        queuedInputEvent3 = queuedInputEvent1.mNext;
        dequeue(param1QueuedInputEvent, queuedInputEvent2);
      } 
      super.forward(param1QueuedInputEvent);
      param1QueuedInputEvent = queuedInputEvent3;
      while (param1QueuedInputEvent != null) {
        if (i == param1QueuedInputEvent.mEvent.getDeviceId()) {
          if ((param1QueuedInputEvent.mFlags & 0x2) != 0)
            break; 
          queuedInputEvent1 = param1QueuedInputEvent.mNext;
          dequeue(param1QueuedInputEvent, queuedInputEvent2);
          super.forward(param1QueuedInputEvent);
          param1QueuedInputEvent = queuedInputEvent1;
          continue;
        } 
        queuedInputEvent2 = param1QueuedInputEvent;
        param1QueuedInputEvent = param1QueuedInputEvent.mNext;
      } 
    }
    
    protected void apply(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent, int param1Int) {
      if (param1Int == 3) {
        defer(param1QueuedInputEvent);
      } else {
        super.apply(param1QueuedInputEvent, param1Int);
      } 
    }
    
    private void enqueue(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      ViewRootImpl.QueuedInputEvent queuedInputEvent = this.mQueueTail;
      if (queuedInputEvent == null) {
        this.mQueueHead = param1QueuedInputEvent;
        this.mQueueTail = param1QueuedInputEvent;
      } else {
        queuedInputEvent.mNext = param1QueuedInputEvent;
        this.mQueueTail = param1QueuedInputEvent;
      } 
      int i = this.mQueueLength + 1;
      Trace.traceCounter(4L, this.mTraceCounter, i);
    }
    
    private void dequeue(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent1, ViewRootImpl.QueuedInputEvent param1QueuedInputEvent2) {
      if (param1QueuedInputEvent2 == null) {
        this.mQueueHead = param1QueuedInputEvent1.mNext;
      } else {
        param1QueuedInputEvent2.mNext = param1QueuedInputEvent1.mNext;
      } 
      if (this.mQueueTail == param1QueuedInputEvent1)
        this.mQueueTail = param1QueuedInputEvent2; 
      param1QueuedInputEvent1.mNext = null;
      int i = this.mQueueLength - 1;
      Trace.traceCounter(4L, this.mTraceCounter, i);
    }
    
    void dump(String param1String, PrintWriter param1PrintWriter) {
      param1PrintWriter.print(param1String);
      param1PrintWriter.print(getClass().getName());
      param1PrintWriter.print(": mQueueLength=");
      param1PrintWriter.println(this.mQueueLength);
      super.dump(param1String, param1PrintWriter);
    }
  }
  
  class NativePreImeInputStage extends AsyncInputStage implements InputQueue.FinishedInputEventCallback {
    final ViewRootImpl this$0;
    
    public NativePreImeInputStage(ViewRootImpl.InputStage param1InputStage, String param1String) {
      super(param1InputStage, param1String);
    }
    
    protected int onProcess(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      if (ViewRootImpl.this.mInputQueue != null && param1QueuedInputEvent.mEvent instanceof KeyEvent) {
        ViewRootImpl.this.mInputQueue.sendInputEvent(param1QueuedInputEvent.mEvent, param1QueuedInputEvent, true, this);
        return 3;
      } 
      return 0;
    }
    
    public void onFinishedInputEvent(Object param1Object, boolean param1Boolean) {
      param1Object = param1Object;
      if (param1Boolean) {
        finish((ViewRootImpl.QueuedInputEvent)param1Object, true);
        return;
      } 
      forward((ViewRootImpl.QueuedInputEvent)param1Object);
    }
  }
  
  final class ViewPreImeInputStage extends InputStage {
    final ViewRootImpl this$0;
    
    public ViewPreImeInputStage(ViewRootImpl.InputStage param1InputStage) {
      super(param1InputStage);
    }
    
    protected int onProcess(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      if (param1QueuedInputEvent.mEvent instanceof KeyEvent)
        return processKeyEvent(param1QueuedInputEvent); 
      return 0;
    }
    
    private int processKeyEvent(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      KeyEvent keyEvent = (KeyEvent)param1QueuedInputEvent.mEvent;
      if (ViewRootImpl.this.mView.dispatchKeyEventPreIme(keyEvent))
        return 1; 
      return 0;
    }
  }
  
  class ImeInputStage extends AsyncInputStage implements InputMethodManager.FinishedInputEventCallback {
    final ViewRootImpl this$0;
    
    public ImeInputStage(ViewRootImpl.InputStage param1InputStage, String param1String) {
      super(param1InputStage, param1String);
    }
    
    protected int onProcess(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      int i = ViewRootImpl.this.mImeFocusController.onProcessImeInputStage(param1QueuedInputEvent, param1QueuedInputEvent.mEvent, ViewRootImpl.this.mWindowAttributes, this);
      if (i != -1) {
        if (i != 0) {
          if (i == 1)
            return 1; 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unexpected result=");
          stringBuilder.append(i);
          throw new IllegalStateException(stringBuilder.toString());
        } 
        return 0;
      } 
      return 3;
    }
    
    public void onFinishedInputEvent(Object param1Object, boolean param1Boolean) {
      ViewRootImpl.QueuedInputEvent queuedInputEvent = (ViewRootImpl.QueuedInputEvent)param1Object;
      if (ViewRootImpl.DEBUG_IMF) {
        String str = ViewRootImpl.this.mTag;
        param1Object = new StringBuilder();
        param1Object.append("IME finishedEvent: handled = ");
        param1Object.append(param1Boolean);
        param1Object.append(", event = ");
        param1Object.append(queuedInputEvent);
        param1Object.append(", viewAncestor = ");
        param1Object.append(this);
        Log.d(str, param1Object.toString());
      } 
      if (param1Boolean) {
        finish(queuedInputEvent, true);
        return;
      } 
      forward(queuedInputEvent);
    }
  }
  
  final class EarlyPostImeInputStage extends InputStage {
    final ViewRootImpl this$0;
    
    public EarlyPostImeInputStage(ViewRootImpl.InputStage param1InputStage) {
      super(param1InputStage);
    }
    
    protected int onProcess(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      if (param1QueuedInputEvent.mEvent instanceof KeyEvent)
        return processKeyEvent(param1QueuedInputEvent); 
      if (param1QueuedInputEvent.mEvent instanceof MotionEvent)
        return processMotionEvent(param1QueuedInputEvent); 
      return 0;
    }
    
    private int processKeyEvent(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      KeyEvent keyEvent = (KeyEvent)param1QueuedInputEvent.mEvent;
      if (ViewRootImpl.this.mAttachInfo.mTooltipHost != null)
        ViewRootImpl.this.mAttachInfo.mTooltipHost.handleTooltipKey(keyEvent); 
      if (ViewRootImpl.this.checkForLeavingTouchModeAndConsume(keyEvent))
        return 1; 
      ViewRootImpl.this.mFallbackEventHandler.preDispatchKeyEvent(keyEvent);
      return 0;
    }
    
    private int processMotionEvent(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      MotionEvent motionEvent = (MotionEvent)param1QueuedInputEvent.mEvent;
      if (motionEvent != null && (motionEvent.getAction() == 0 || motionEvent.getAction() == 1 || motionEvent.getAction() == 3)) {
        String str = ViewRootImpl.this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("processMotionEvent ");
        stringBuilder.append(motionEvent);
        Log.d(str, stringBuilder.toString());
      } 
      if (motionEvent.isFromSource(2))
        return processPointerEvent(param1QueuedInputEvent); 
      int i = motionEvent.getActionMasked();
      if ((i == 0 || i == 8) && motionEvent.isFromSource(8))
        ViewRootImpl.this.ensureTouchMode(false); 
      return 0;
    }
    
    private int processPointerEvent(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      MotionEvent motionEvent = (MotionEvent)param1QueuedInputEvent.mEvent;
      if (ViewRootImpl.this.mTranslator != null)
        ViewRootImpl.this.mTranslator.translateEventInScreenToAppWindow(motionEvent); 
      int i = motionEvent.getAction();
      if (i == 0 || i == 8)
        ViewRootImpl.this.ensureTouchMode(true); 
      if (i == 0) {
        AutofillManager autofillManager = ViewRootImpl.this.getAutofillManager();
        if (autofillManager != null)
          autofillManager.requestHideFillUi(); 
      } 
      if (i == 0 && ViewRootImpl.this.mAttachInfo.mTooltipHost != null)
        ViewRootImpl.this.mAttachInfo.mTooltipHost.hideTooltip(); 
      if (ViewRootImpl.this.mCurScrollY != 0)
        motionEvent.offsetLocation(0.0F, ViewRootImpl.this.mCurScrollY); 
      if (motionEvent.isTouchEvent()) {
        ViewRootImpl.this.mLastTouchPoint.x = motionEvent.getRawX();
        ViewRootImpl.this.mLastTouchPoint.y = motionEvent.getRawY();
        ViewRootImpl.this.mLastTouchSource = motionEvent.getSource();
      } 
      return 0;
    }
  }
  
  class NativePostImeInputStage extends AsyncInputStage implements InputQueue.FinishedInputEventCallback {
    final ViewRootImpl this$0;
    
    public NativePostImeInputStage(ViewRootImpl.InputStage param1InputStage, String param1String) {
      super(param1InputStage, param1String);
    }
    
    protected int onProcess(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      if (ViewRootImpl.this.mInputQueue != null) {
        ViewRootImpl.this.mInputQueue.sendInputEvent(param1QueuedInputEvent.mEvent, param1QueuedInputEvent, false, this);
        return 3;
      } 
      return 0;
    }
    
    public void onFinishedInputEvent(Object param1Object, boolean param1Boolean) {
      param1Object = param1Object;
      if (param1Boolean) {
        finish((ViewRootImpl.QueuedInputEvent)param1Object, true);
        return;
      } 
      forward((ViewRootImpl.QueuedInputEvent)param1Object);
    }
  }
  
  final class ViewPostImeInputStage extends InputStage {
    final ViewRootImpl this$0;
    
    public ViewPostImeInputStage(ViewRootImpl.InputStage param1InputStage) {
      super(param1InputStage);
    }
    
    protected int onProcess(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      if (param1QueuedInputEvent.mEvent instanceof KeyEvent)
        return processKeyEvent(param1QueuedInputEvent); 
      int i = param1QueuedInputEvent.mEvent.getSource();
      if ((i & 0x2) != 0)
        return processPointerEvent(param1QueuedInputEvent); 
      if ((i & 0x4) != 0)
        return processTrackballEvent(param1QueuedInputEvent); 
      return processGenericMotionEvent(param1QueuedInputEvent);
    }
    
    protected void onDeliverToNext(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      if (ViewRootImpl.this.mUnbufferedInputDispatch && param1QueuedInputEvent.mEvent instanceof MotionEvent) {
        MotionEvent motionEvent = (MotionEvent)param1QueuedInputEvent.mEvent;
        if (motionEvent.isTouchEvent()) {
          InputEvent inputEvent = param1QueuedInputEvent.mEvent;
          if (ViewRootImpl.isTerminalInputEvent(inputEvent)) {
            ViewRootImpl.this.mUnbufferedInputDispatch = false;
            ViewRootImpl.this.scheduleConsumeBatchedInput();
          } 
        } 
      } 
      super.onDeliverToNext(param1QueuedInputEvent);
    }
    
    private boolean performFocusNavigation(KeyEvent param1KeyEvent) {
      int i = 0;
      int j = param1KeyEvent.getKeyCode();
      if (j != 61) {
        switch (j) {
          case 22:
            if (param1KeyEvent.hasNoModifiers())
              i = 66; 
            break;
          case 21:
            if (param1KeyEvent.hasNoModifiers())
              i = 17; 
            break;
          case 20:
            if (param1KeyEvent.hasNoModifiers())
              i = 130; 
            break;
          case 19:
            if (param1KeyEvent.hasNoModifiers())
              i = 33; 
            break;
        } 
      } else if (param1KeyEvent.hasNoModifiers()) {
        i = 2;
      } else if (param1KeyEvent.hasModifiers(1)) {
        i = 1;
      } 
      if (i != 0) {
        View view = ViewRootImpl.this.mView.findFocus();
        if (view != null) {
          ViewRootImpl viewRootImpl;
          View view1 = view.focusSearch(i);
          if (view1 != null && view1 != view) {
            view.getFocusedRect(ViewRootImpl.this.mTempRect);
            if (ViewRootImpl.this.mView instanceof ViewGroup) {
              ((ViewGroup)ViewRootImpl.this.mView).offsetDescendantRectToMyCoords(view, ViewRootImpl.this.mTempRect);
              ((ViewGroup)ViewRootImpl.this.mView).offsetRectIntoDescendantCoords(view1, ViewRootImpl.this.mTempRect);
            } 
            if (view1.requestFocus(i, ViewRootImpl.this.mTempRect)) {
              viewRootImpl = ViewRootImpl.this;
              i = SoundEffectConstants.getContantForFocusDirection(i);
              viewRootImpl.playSoundEffect(i);
              return true;
            } 
          } 
          if (ViewRootImpl.this.mView.dispatchUnhandledMove((View)viewRootImpl, i))
            return true; 
        } else if (ViewRootImpl.this.mView.restoreDefaultFocus()) {
          return true;
        } 
      } 
      return false;
    }
    
    private boolean performKeyboardGroupNavigation(int param1Int) {
      View view1 = ViewRootImpl.this.mView.findFocus();
      if (view1 == null && ViewRootImpl.this.mView.restoreDefaultFocus())
        return true; 
      if (view1 == null) {
        view1 = ViewRootImpl.this.keyboardNavigationClusterSearch(null, param1Int);
      } else {
        view1 = view1.keyboardNavigationClusterSearch(null, param1Int);
      } 
      int i = param1Int;
      if (param1Int == 2 || param1Int == 1)
        i = 130; 
      View view2 = view1;
      if (view1 != null) {
        view2 = view1;
        if (view1.isRootNamespace()) {
          if (view1.restoreFocusNotInCluster()) {
            ViewRootImpl.this.playSoundEffect(SoundEffectConstants.getContantForFocusDirection(param1Int));
            return true;
          } 
          view2 = ViewRootImpl.this.keyboardNavigationClusterSearch(null, param1Int);
        } 
      } 
      if (view2 != null && view2.restoreFocusInCluster(i)) {
        ViewRootImpl.this.playSoundEffect(SoundEffectConstants.getContantForFocusDirection(param1Int));
        return true;
      } 
      return false;
    }
    
    private int processKeyEvent(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      String str;
      KeyEvent keyEvent = (KeyEvent)param1QueuedInputEvent.mEvent;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("processKeyEvent,");
      stringBuilder.append(keyEvent);
      Slog.d("ViewRootImpl", stringBuilder.toString());
      if (ViewRootImpl.this.mUnhandledKeyManager.preViewDispatch(keyEvent))
        return 1; 
      if (ViewRootImpl.this.mView.dispatchKeyEvent(keyEvent)) {
        if (InputLog.DEBUG) {
          str = ViewRootImpl.this.mTag;
          stringBuilder = new StringBuilder();
          stringBuilder.append(keyEvent.getKeyCode());
          stringBuilder.append(" was FINISH_HANDLED by  mView ");
          InputLog.d(str, stringBuilder.toString());
        } 
        return 1;
      } 
      if (shouldDropInputEvent((ViewRootImpl.QueuedInputEvent)str))
        return 2; 
      if (ViewRootImpl.this.mUnhandledKeyManager.dispatch(ViewRootImpl.this.mView, keyEvent))
        return 1; 
      byte b1 = 0;
      byte b2 = b1;
      if (keyEvent.getAction() == 0) {
        b2 = b1;
        if (keyEvent.getKeyCode() == 61)
          if (KeyEvent.metaStateHasModifiers(keyEvent.getMetaState(), 65536)) {
            b2 = 2;
          } else {
            b2 = b1;
            if (KeyEvent.metaStateHasModifiers(keyEvent.getMetaState(), 65537))
              b2 = 1; 
          }  
      } 
      if (keyEvent.getAction() == 0 && !KeyEvent.metaStateHasNoModifiers(keyEvent.getMetaState()) && keyEvent.getRepeatCount() == 0 && !KeyEvent.isModifierKey(keyEvent.getKeyCode()) && b2 == 0) {
        if (ViewRootImpl.this.mView.dispatchKeyShortcutEvent(keyEvent))
          return 1; 
        if (shouldDropInputEvent((ViewRootImpl.QueuedInputEvent)str))
          return 2; 
      } 
      if (ViewRootImpl.this.mFallbackEventHandler.dispatchKeyEvent(keyEvent))
        return 1; 
      if (shouldDropInputEvent((ViewRootImpl.QueuedInputEvent)str))
        return 2; 
      if (keyEvent.getAction() == 0)
        if (b2 != 0) {
          if (performKeyboardGroupNavigation(b2))
            return 1; 
        } else if (performFocusNavigation(keyEvent)) {
          return 1;
        }  
      return 0;
    }
    
    private int processPointerEvent(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      MotionEvent motionEvent = (MotionEvent)param1QueuedInputEvent.mEvent;
      boolean bool1 = OplusScreenShotUtil.checkPauseDeliverPointer();
      if (bool1) {
        OplusScreenShotUtil.dealPausedDeliverPointer((MotionEvent)param1QueuedInputEvent.mEvent, ViewRootImpl.this.mView);
        return 1;
      } 
      ViewRootImpl.this.mAttachInfo.mUnbufferedDispatchRequested = false;
      ViewRootImpl.this.mAttachInfo.mHandlingPointerEvent = true;
      if (ViewRootImpl.this.mView == null)
        return 1; 
      if (ViewRootImpl.DEBUG_INPUT_TRACING) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ViewPostImeInputStage,x:");
        stringBuilder.append(motionEvent.getRawX());
        stringBuilder.append("y:");
        stringBuilder.append(motionEvent.getRawY());
        Trace.traceBegin(8L, stringBuilder.toString());
      } 
      boolean bool2 = ViewRootImpl.this.mView.dispatchPointerEvent(motionEvent);
      int i = motionEvent.getActionMasked();
      if (i == 2) {
        ViewRootImpl.this.mHaveMoveEvent = true;
      } else if (i == 1) {
        ViewRootImpl.this.mHaveMoveEvent = false;
      } 
      maybeUpdatePointerIcon(motionEvent);
      ViewRootImpl.this.maybeUpdateTooltip(motionEvent);
      ViewRootImpl.this.mAttachInfo.mHandlingPointerEvent = false;
      if (ViewRootImpl.this.mAttachInfo.mUnbufferedDispatchRequested && !ViewRootImpl.this.mUnbufferedInputDispatch) {
        ViewRootImpl.this.mUnbufferedInputDispatch = true;
        if (ViewRootImpl.this.mConsumeBatchedInputScheduled)
          ViewRootImpl.this.scheduleConsumeBatchedInputImmediately(); 
      } 
      if (i == 0 || i == 1 || i == 3) {
        String str = ViewRootImpl.this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("dispatchPointerEvent handled=");
        stringBuilder.append(bool2);
        stringBuilder.append(", event=");
        stringBuilder.append(motionEvent);
        Log.d(str, stringBuilder.toString());
      } 
      if (ViewRootImpl.DEBUG_INPUT_TRACING)
        Trace.traceEnd(8L); 
      return bool2;
    }
    
    private void maybeUpdatePointerIcon(MotionEvent param1MotionEvent) {
      if (param1MotionEvent.getPointerCount() == 1 && param1MotionEvent.isFromSource(8194)) {
        if (param1MotionEvent.getActionMasked() == 9 || param1MotionEvent.getActionMasked() == 10)
          ViewRootImpl.access$2902(ViewRootImpl.this, 1); 
        if (param1MotionEvent.getActionMasked() != 10 && !ViewRootImpl.this.updatePointerIcon(param1MotionEvent) && param1MotionEvent.getActionMasked() == 7)
          ViewRootImpl.access$2902(ViewRootImpl.this, 1); 
      } 
    }
    
    private int processTrackballEvent(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      MotionEvent motionEvent = (MotionEvent)param1QueuedInputEvent.mEvent;
      if (motionEvent.isFromSource(131076) && (!ViewRootImpl.this.hasPointerCapture() || ViewRootImpl.this.mView.dispatchCapturedPointerEvent(motionEvent)))
        return 1; 
      if (ViewRootImpl.this.mView.dispatchTrackballEvent(motionEvent))
        return 1; 
      return 0;
    }
    
    private int processGenericMotionEvent(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      MotionEvent motionEvent = (MotionEvent)param1QueuedInputEvent.mEvent;
      if (motionEvent.isFromSource(1048584) && ViewRootImpl.this.hasPointerCapture() && ViewRootImpl.this.mView.dispatchCapturedPointerEvent(motionEvent))
        return 1; 
      if (ViewRootImpl.this.mView.dispatchGenericMotionEvent(motionEvent))
        return 1; 
      return 0;
    }
  }
  
  private void resetPointerIcon(MotionEvent paramMotionEvent) {
    this.mPointerIconType = 1;
    updatePointerIcon(paramMotionEvent);
  }
  
  private boolean updatePointerIcon(MotionEvent paramMotionEvent) {
    char c;
    float f1 = paramMotionEvent.getX(0);
    float f2 = paramMotionEvent.getY(0);
    View view = this.mView;
    if (view == null) {
      Slog.d(this.mTag, "updatePointerIcon called after view was removed");
      return false;
    } 
    if (f1 < 0.0F || f1 >= view.getWidth() || f2 < 0.0F || f2 >= this.mView.getHeight()) {
      Slog.d(this.mTag, "updatePointerIcon called with position out of bounds");
      return false;
    } 
    PointerIcon pointerIcon = this.mView.onResolvePointerIcon(paramMotionEvent, 0);
    if (pointerIcon != null) {
      c = pointerIcon.getType();
    } else {
      c = 'Ϩ';
    } 
    if (this.mPointerIconType != c) {
      this.mPointerIconType = c;
      this.mCustomPointerIcon = null;
      if (c != -1) {
        InputManager.getInstance().setPointerIconType(c);
        return true;
      } 
    } 
    if (this.mPointerIconType == -1) {
      PointerIcon pointerIcon1 = this.mCustomPointerIcon;
      if (!pointerIcon.equals(pointerIcon1)) {
        this.mCustomPointerIcon = pointerIcon;
        InputManager.getInstance().setCustomPointerIcon(this.mCustomPointerIcon);
      } 
    } 
    return true;
  }
  
  private void maybeUpdateTooltip(MotionEvent paramMotionEvent) {
    if (paramMotionEvent.getPointerCount() != 1)
      return; 
    int i = paramMotionEvent.getActionMasked();
    if (i != 9 && i != 7 && i != 10)
      return; 
    AccessibilityManager accessibilityManager = AccessibilityManager.getInstance(this.mContext);
    if (accessibilityManager.isEnabled() && accessibilityManager.isTouchExplorationEnabled())
      return; 
    View view = this.mView;
    if (view == null) {
      Slog.d(this.mTag, "maybeUpdateTooltip called after view was removed");
      return;
    } 
    view.dispatchTooltipHoverEvent(paramMotionEvent);
  }
  
  final class SyntheticInputStage extends InputStage {
    private final ViewRootImpl.SyntheticTrackballHandler mTrackball = new ViewRootImpl.SyntheticTrackballHandler();
    
    private final ViewRootImpl.SyntheticJoystickHandler mJoystick = new ViewRootImpl.SyntheticJoystickHandler();
    
    private final ViewRootImpl.SyntheticTouchNavigationHandler mTouchNavigation = new ViewRootImpl.SyntheticTouchNavigationHandler();
    
    private final ViewRootImpl.SyntheticKeyboardHandler mKeyboard = new ViewRootImpl.SyntheticKeyboardHandler();
    
    final ViewRootImpl this$0;
    
    public SyntheticInputStage() {
      super(null);
    }
    
    protected int onProcess(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      MotionEvent motionEvent;
      param1QueuedInputEvent.mFlags |= 0x10;
      if (param1QueuedInputEvent.mEvent instanceof MotionEvent) {
        motionEvent = (MotionEvent)param1QueuedInputEvent.mEvent;
        int i = motionEvent.getSource();
        if ((i & 0x4) != 0) {
          this.mTrackball.process(motionEvent);
          return 1;
        } 
        if ((i & 0x10) != 0) {
          this.mJoystick.process(motionEvent);
          return 1;
        } 
        if ((i & 0x200000) == 2097152) {
          this.mTouchNavigation.process(motionEvent);
          return 1;
        } 
      } else if ((((ViewRootImpl.QueuedInputEvent)motionEvent).mFlags & 0x20) != 0) {
        this.mKeyboard.process((KeyEvent)((ViewRootImpl.QueuedInputEvent)motionEvent).mEvent);
        return 1;
      } 
      return 0;
    }
    
    protected void onDeliverToNext(ViewRootImpl.QueuedInputEvent param1QueuedInputEvent) {
      if ((param1QueuedInputEvent.mFlags & 0x10) == 0)
        if (param1QueuedInputEvent.mEvent instanceof MotionEvent) {
          MotionEvent motionEvent = (MotionEvent)param1QueuedInputEvent.mEvent;
          int i = motionEvent.getSource();
          if ((i & 0x4) != 0) {
            this.mTrackball.cancel();
          } else if ((i & 0x10) != 0) {
            this.mJoystick.cancel();
          } else if ((i & 0x200000) == 2097152) {
            this.mTouchNavigation.cancel(motionEvent);
          } 
        }  
      super.onDeliverToNext(param1QueuedInputEvent);
    }
    
    protected void onWindowFocusChanged(boolean param1Boolean) {
      if (!param1Boolean)
        this.mJoystick.cancel(); 
    }
    
    protected void onDetachedFromWindow() {
      this.mJoystick.cancel();
    }
  }
  
  class SyntheticTrackballHandler {
    private long mLastTime;
    
    private final ViewRootImpl.TrackballAxis mX = new ViewRootImpl.TrackballAxis();
    
    private final ViewRootImpl.TrackballAxis mY = new ViewRootImpl.TrackballAxis();
    
    final ViewRootImpl this$0;
    
    public void process(MotionEvent param1MotionEvent) {
      long l = SystemClock.uptimeMillis();
      if (this.mLastTime + 250L < l) {
        this.mX.reset(0);
        this.mY.reset(0);
        this.mLastTime = l;
      } 
      int i = param1MotionEvent.getAction();
      int j = param1MotionEvent.getMetaState();
      if (i != 0) {
        if (i == 1) {
          this.mX.reset(2);
          this.mY.reset(2);
          ViewRootImpl.this.enqueueInputEvent(new KeyEvent(l, l, 1, 23, 0, j, -1, 0, 1024, 257));
        } 
      } else {
        this.mX.reset(2);
        this.mY.reset(2);
        ViewRootImpl.this.enqueueInputEvent(new KeyEvent(l, l, 0, 23, 0, j, -1, 0, 1024, 257));
      } 
      float f1 = this.mX.collect(param1MotionEvent.getX(), param1MotionEvent.getEventTime(), "X");
      float f2 = this.mY.collect(param1MotionEvent.getY(), param1MotionEvent.getEventTime(), "Y");
      int k = 0;
      if (f1 > f2) {
        k = this.mX.generate();
        if (k != 0) {
          if (k > 0) {
            i = 22;
          } else {
            i = 21;
          } 
          f2 = this.mX.acceleration;
          this.mY.reset(2);
        } else {
          i = 0;
          f2 = 1.0F;
        } 
      } else if (f2 > 0.0F) {
        k = this.mY.generate();
        if (k != 0) {
          if (k > 0) {
            i = 20;
          } else {
            i = 19;
          } 
          f2 = this.mY.acceleration;
          this.mX.reset(2);
        } else {
          i = 0;
          f2 = 1.0F;
        } 
      } else {
        i = 0;
        f2 = 1.0F;
      } 
      if (i != 0) {
        int m = k;
        if (k < 0)
          m = -k; 
        k = (int)(m * f2);
        if (k > m) {
          m--;
          ViewRootImpl.this.enqueueInputEvent(new KeyEvent(l, l, 2, i, k - m, j, -1, 0, 1024, 257));
        } 
        while (m > 0) {
          m--;
          l = SystemClock.uptimeMillis();
          ViewRootImpl.this.enqueueInputEvent(new KeyEvent(l, l, 0, i, 0, j, -1, 0, 1024, 257));
          ViewRootImpl.this.enqueueInputEvent(new KeyEvent(l, l, 1, i, 0, j, -1, 0, 1024, 257));
        } 
        this.mLastTime = l;
      } 
    }
    
    public void cancel() {
      this.mLastTime = -2147483648L;
      if (ViewRootImpl.this.mView != null && ViewRootImpl.this.mAdded)
        ViewRootImpl.this.ensureTouchMode(false); 
    }
  }
  
  class TrackballAxis {
    static final float ACCEL_MOVE_SCALING_FACTOR = 0.025F;
    
    static final long FAST_MOVE_TIME = 150L;
    
    static final float FIRST_MOVEMENT_THRESHOLD = 0.5F;
    
    static final float MAX_ACCELERATION = 20.0F;
    
    static final float SECOND_CUMULATIVE_MOVEMENT_THRESHOLD = 2.0F;
    
    static final float SUBSEQUENT_INCREMENTAL_MOVEMENT_THRESHOLD = 1.0F;
    
    float acceleration = 1.0F;
    
    int dir;
    
    long lastMoveTime = 0L;
    
    int nonAccelMovement;
    
    float position;
    
    int step;
    
    void reset(int param1Int) {
      this.position = 0.0F;
      this.acceleration = 1.0F;
      this.lastMoveTime = 0L;
      this.step = param1Int;
      this.dir = 0;
    }
    
    float collect(float param1Float, long param1Long, String param1String) {
      long l;
      float f = 1.0F;
      if (param1Float > 0.0F) {
        l = (long)(150.0F * param1Float);
        if (this.dir < 0) {
          this.position = 0.0F;
          this.step = 0;
          this.acceleration = 1.0F;
          this.lastMoveTime = 0L;
        } 
        this.dir = 1;
      } else if (param1Float < 0.0F) {
        l = (long)(-param1Float * 150.0F);
        if (this.dir > 0) {
          this.position = 0.0F;
          this.step = 0;
          this.acceleration = 1.0F;
          this.lastMoveTime = 0L;
        } 
        this.dir = -1;
      } else {
        l = 0L;
      } 
      if (l > 0L) {
        long l1 = param1Long - this.lastMoveTime;
        this.lastMoveTime = param1Long;
        float f1 = this.acceleration;
        if (l1 < l) {
          f = (float)(l - l1) * 0.025F;
          float f2 = f1;
          if (f > 1.0F)
            f2 = f1 * f; 
          f1 = 20.0F;
          if (f2 < 20.0F)
            f1 = f2; 
          this.acceleration = f1;
        } else {
          float f3 = (float)(l1 - l) * 0.025F;
          float f2 = f1;
          if (f3 > 1.0F)
            f2 = f1 / f3; 
          f1 = f;
          if (f2 > 1.0F)
            f1 = f2; 
          this.acceleration = f1;
        } 
      } 
      this.position = param1Float = this.position + param1Float;
      return Math.abs(param1Float);
    }
    
    int generate() {
      int i = 0;
      this.nonAccelMovement = 0;
      while (true) {
        int j;
        if (this.position >= 0.0F) {
          j = 1;
        } else {
          j = -1;
        } 
        int k = this.step;
        if (k != 0) {
          if (k != 1) {
            if (Math.abs(this.position) < 1.0F)
              return i; 
            i += j;
            this.position -= j * 1.0F;
            float f = this.acceleration;
            f *= 1.1F;
            if (f >= 20.0F)
              f = this.acceleration; 
            this.acceleration = f;
            j = i;
          } else {
            if (Math.abs(this.position) < 2.0F)
              return i; 
            i += j;
            this.nonAccelMovement += j;
            this.position -= j * 2.0F;
            this.step = 2;
            j = i;
          } 
        } else {
          if (Math.abs(this.position) < 0.5F)
            return i; 
          i += j;
          this.nonAccelMovement += j;
          this.step = 1;
          j = i;
        } 
        i = j;
      } 
    }
  }
  
  final class SyntheticJoystickHandler extends Handler {
    final ViewRootImpl this$0;
    
    private final JoystickAxesState mJoystickAxesState = new JoystickAxesState();
    
    private final SparseArray<KeyEvent> mDeviceKeyEvents = new SparseArray<>();
    
    private static final int MSG_ENQUEUE_Y_AXIS_KEY_REPEAT = 2;
    
    private static final int MSG_ENQUEUE_X_AXIS_KEY_REPEAT = 1;
    
    public SyntheticJoystickHandler() {
      super(true);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i == 1 || i == 2)
        if (ViewRootImpl.this.mAttachInfo.mHasWindowFocus) {
          KeyEvent keyEvent = (KeyEvent)param1Message.obj;
          long l = SystemClock.uptimeMillis();
          i = keyEvent.getRepeatCount();
          keyEvent = KeyEvent.changeTimeRepeat(keyEvent, l, i + 1);
          ViewRootImpl.this.enqueueInputEvent(keyEvent);
          param1Message = obtainMessage(param1Message.what, keyEvent);
          param1Message.setAsynchronous(true);
          sendMessageDelayed(param1Message, ViewConfiguration.getKeyRepeatDelay());
        }  
    }
    
    public void process(MotionEvent param1MotionEvent) {
      int i = param1MotionEvent.getActionMasked();
      if (i != 2) {
        if (i != 3) {
          String str = ViewRootImpl.this.mTag;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unexpected action: ");
          stringBuilder.append(param1MotionEvent.getActionMasked());
          Log.w(str, stringBuilder.toString());
        } else {
          cancel();
        } 
      } else {
        update(param1MotionEvent);
      } 
    }
    
    private void cancel() {
      removeMessages(1);
      removeMessages(2);
      for (byte b = 0; b < this.mDeviceKeyEvents.size(); b++) {
        KeyEvent keyEvent = this.mDeviceKeyEvents.valueAt(b);
        if (keyEvent != null) {
          ViewRootImpl viewRootImpl = ViewRootImpl.this;
          long l = SystemClock.uptimeMillis();
          viewRootImpl.enqueueInputEvent(KeyEvent.changeTimeRepeat(keyEvent, l, 0));
        } 
      } 
      this.mDeviceKeyEvents.clear();
      this.mJoystickAxesState.resetState();
    }
    
    private void update(MotionEvent param1MotionEvent) {
      int i = param1MotionEvent.getHistorySize();
      for (byte b = 0; b < i; b++) {
        long l1 = param1MotionEvent.getHistoricalEventTime(b);
        JoystickAxesState joystickAxesState1 = this.mJoystickAxesState;
        float f1 = param1MotionEvent.getHistoricalAxisValue(0, 0, b);
        joystickAxesState1.updateStateForAxis(param1MotionEvent, l1, 0, f1);
        joystickAxesState1 = this.mJoystickAxesState;
        f1 = param1MotionEvent.getHistoricalAxisValue(1, 0, b);
        joystickAxesState1.updateStateForAxis(param1MotionEvent, l1, 1, f1);
        joystickAxesState1 = this.mJoystickAxesState;
        f1 = param1MotionEvent.getHistoricalAxisValue(15, 0, b);
        joystickAxesState1.updateStateForAxis(param1MotionEvent, l1, 15, f1);
        joystickAxesState1 = this.mJoystickAxesState;
        f1 = param1MotionEvent.getHistoricalAxisValue(16, 0, b);
        joystickAxesState1.updateStateForAxis(param1MotionEvent, l1, 16, f1);
      } 
      long l = param1MotionEvent.getEventTime();
      JoystickAxesState joystickAxesState = this.mJoystickAxesState;
      float f = param1MotionEvent.getAxisValue(0);
      joystickAxesState.updateStateForAxis(param1MotionEvent, l, 0, f);
      joystickAxesState = this.mJoystickAxesState;
      f = param1MotionEvent.getAxisValue(1);
      joystickAxesState.updateStateForAxis(param1MotionEvent, l, 1, f);
      joystickAxesState = this.mJoystickAxesState;
      f = param1MotionEvent.getAxisValue(15);
      joystickAxesState.updateStateForAxis(param1MotionEvent, l, 15, f);
      joystickAxesState = this.mJoystickAxesState;
      f = param1MotionEvent.getAxisValue(16);
      joystickAxesState.updateStateForAxis(param1MotionEvent, l, 16, f);
    }
    
    class JoystickAxesState {
      private static final int STATE_DOWN_OR_RIGHT = 1;
      
      private static final int STATE_NEUTRAL = 0;
      
      private static final int STATE_UP_OR_LEFT = -1;
      
      final int[] mAxisStatesHat;
      
      final int[] mAxisStatesStick;
      
      final ViewRootImpl.SyntheticJoystickHandler this$1;
      
      JoystickAxesState() {
        this.mAxisStatesHat = new int[] { 0, 0 };
        this.mAxisStatesStick = new int[] { 0, 0 };
      }
      
      void resetState() {
        int[] arrayOfInt = this.mAxisStatesHat;
        arrayOfInt[0] = 0;
        arrayOfInt[1] = 0;
        arrayOfInt = this.mAxisStatesStick;
        arrayOfInt[0] = 0;
        arrayOfInt[1] = 0;
      }
      
      void updateStateForAxis(MotionEvent param2MotionEvent, long param2Long, int param2Int, float param2Float) {
        StringBuilder stringBuilder;
        boolean bool;
        byte b;
        int j;
        if (isXAxis(param2Int)) {
          bool = false;
          b = 1;
        } else if (isYAxis(param2Int)) {
          bool = true;
          b = 2;
        } else {
          String str = ViewRootImpl.this.mTag;
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unexpected axis ");
          stringBuilder.append(param2Int);
          stringBuilder.append(" in updateStateForAxis!");
          Log.e(str, stringBuilder.toString());
          return;
        } 
        int i = joystickAxisValueToState(param2Float);
        if (param2Int == 0 || param2Int == 1) {
          j = this.mAxisStatesStick[bool];
        } else {
          j = this.mAxisStatesHat[bool];
        } 
        if (j == i)
          return; 
        int k = stringBuilder.getMetaState();
        int m = stringBuilder.getDeviceId();
        int n = stringBuilder.getSource();
        if (j == 1 || j == -1) {
          j = joystickAxisAndStateToKeycode(param2Int, j);
          if (j != 0) {
            ViewRootImpl.this.enqueueInputEvent(new KeyEvent(param2Long, param2Long, 1, j, 0, k, m, 0, 1024, n));
            ViewRootImpl.SyntheticJoystickHandler.this.mDeviceKeyEvents.put(m, null);
          } 
          ViewRootImpl.SyntheticJoystickHandler.this.removeMessages(b);
        } 
        if (i == 1 || i == -1) {
          j = joystickAxisAndStateToKeycode(param2Int, i);
          if (j != 0) {
            KeyEvent keyEvent = new KeyEvent(param2Long, param2Long, 0, j, 0, k, m, 0, 1024, n);
            ViewRootImpl.this.enqueueInputEvent(keyEvent);
            Message message = ViewRootImpl.SyntheticJoystickHandler.this.obtainMessage(b, keyEvent);
            message.setAsynchronous(true);
            ViewRootImpl.SyntheticJoystickHandler.this.sendMessageDelayed(message, ViewConfiguration.getKeyRepeatTimeout());
            ViewRootImpl.SyntheticJoystickHandler.this.mDeviceKeyEvents.put(m, new KeyEvent(param2Long, param2Long, 1, j, 0, k, m, 0, 1056, n));
          } 
        } 
        if (param2Int == 0 || param2Int == 1) {
          this.mAxisStatesStick[bool] = i;
          return;
        } 
        this.mAxisStatesHat[bool] = i;
      }
      
      private boolean isXAxis(int param2Int) {
        return (param2Int == 0 || param2Int == 15);
      }
      
      private boolean isYAxis(int param2Int) {
        boolean bool1 = true, bool2 = bool1;
        if (param2Int != 1)
          if (param2Int == 16) {
            bool2 = bool1;
          } else {
            bool2 = false;
          }  
        return bool2;
      }
      
      private int joystickAxisAndStateToKeycode(int param2Int1, int param2Int2) {
        if (isXAxis(param2Int1) && param2Int2 == -1)
          return 21; 
        if (isXAxis(param2Int1) && param2Int2 == 1)
          return 22; 
        if (isYAxis(param2Int1) && param2Int2 == -1)
          return 19; 
        if (isYAxis(param2Int1) && param2Int2 == 1)
          return 20; 
        String str = ViewRootImpl.this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown axis ");
        stringBuilder.append(param2Int1);
        stringBuilder.append(" or direction ");
        stringBuilder.append(param2Int2);
        Log.e(str, stringBuilder.toString());
        return 0;
      }
      
      private int joystickAxisValueToState(float param2Float) {
        if (param2Float >= 0.5F)
          return 1; 
        if (param2Float <= -0.5F)
          return -1; 
        return 0;
      }
    }
  }
  
  class JoystickAxesState {
    private static final int STATE_DOWN_OR_RIGHT = 1;
    
    private static final int STATE_NEUTRAL = 0;
    
    private static final int STATE_UP_OR_LEFT = -1;
    
    final int[] mAxisStatesHat;
    
    final int[] mAxisStatesStick;
    
    final ViewRootImpl.SyntheticJoystickHandler this$1;
    
    JoystickAxesState() {
      this.mAxisStatesHat = new int[] { 0, 0 };
      this.mAxisStatesStick = new int[] { 0, 0 };
    }
    
    void resetState() {
      int[] arrayOfInt = this.mAxisStatesHat;
      arrayOfInt[0] = 0;
      arrayOfInt[1] = 0;
      arrayOfInt = this.mAxisStatesStick;
      arrayOfInt[0] = 0;
      arrayOfInt[1] = 0;
    }
    
    void updateStateForAxis(MotionEvent param1MotionEvent, long param1Long, int param1Int, float param1Float) {
      StringBuilder stringBuilder;
      boolean bool;
      byte b;
      int j;
      if (isXAxis(param1Int)) {
        bool = false;
        b = 1;
      } else if (isYAxis(param1Int)) {
        bool = true;
        b = 2;
      } else {
        String str = ViewRootImpl.this.mTag;
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected axis ");
        stringBuilder.append(param1Int);
        stringBuilder.append(" in updateStateForAxis!");
        Log.e(str, stringBuilder.toString());
        return;
      } 
      int i = joystickAxisValueToState(param1Float);
      if (param1Int == 0 || param1Int == 1) {
        j = this.mAxisStatesStick[bool];
      } else {
        j = this.mAxisStatesHat[bool];
      } 
      if (j == i)
        return; 
      int k = stringBuilder.getMetaState();
      int m = stringBuilder.getDeviceId();
      int n = stringBuilder.getSource();
      if (j == 1 || j == -1) {
        j = joystickAxisAndStateToKeycode(param1Int, j);
        if (j != 0) {
          ViewRootImpl.this.enqueueInputEvent(new KeyEvent(param1Long, param1Long, 1, j, 0, k, m, 0, 1024, n));
          this.this$1.mDeviceKeyEvents.put(m, null);
        } 
        this.this$1.removeMessages(b);
      } 
      if (i == 1 || i == -1) {
        j = joystickAxisAndStateToKeycode(param1Int, i);
        if (j != 0) {
          KeyEvent keyEvent = new KeyEvent(param1Long, param1Long, 0, j, 0, k, m, 0, 1024, n);
          ViewRootImpl.this.enqueueInputEvent(keyEvent);
          Message message = this.this$1.obtainMessage(b, keyEvent);
          message.setAsynchronous(true);
          this.this$1.sendMessageDelayed(message, ViewConfiguration.getKeyRepeatTimeout());
          this.this$1.mDeviceKeyEvents.put(m, new KeyEvent(param1Long, param1Long, 1, j, 0, k, m, 0, 1056, n));
        } 
      } 
      if (param1Int == 0 || param1Int == 1) {
        this.mAxisStatesStick[bool] = i;
        return;
      } 
      this.mAxisStatesHat[bool] = i;
    }
    
    private boolean isXAxis(int param1Int) {
      return (param1Int == 0 || param1Int == 15);
    }
    
    private boolean isYAxis(int param1Int) {
      boolean bool1 = true, bool2 = bool1;
      if (param1Int != 1)
        if (param1Int == 16) {
          bool2 = bool1;
        } else {
          bool2 = false;
        }  
      return bool2;
    }
    
    private int joystickAxisAndStateToKeycode(int param1Int1, int param1Int2) {
      if (isXAxis(param1Int1) && param1Int2 == -1)
        return 21; 
      if (isXAxis(param1Int1) && param1Int2 == 1)
        return 22; 
      if (isYAxis(param1Int1) && param1Int2 == -1)
        return 19; 
      if (isYAxis(param1Int1) && param1Int2 == 1)
        return 20; 
      String str = ViewRootImpl.this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown axis ");
      stringBuilder.append(param1Int1);
      stringBuilder.append(" or direction ");
      stringBuilder.append(param1Int2);
      Log.e(str, stringBuilder.toString());
      return 0;
    }
    
    private int joystickAxisValueToState(float param1Float) {
      if (param1Float >= 0.5F)
        return 1; 
      if (param1Float <= -0.5F)
        return -1; 
      return 0;
    }
  }
  
  final class SyntheticTouchNavigationHandler extends Handler {
    private int mCurrentDeviceId = -1;
    
    private int mActivePointerId = -1;
    
    private int mPendingKeyCode = 0;
    
    private static final float DEFAULT_HEIGHT_MILLIMETERS = 48.0F;
    
    private static final float DEFAULT_WIDTH_MILLIMETERS = 48.0F;
    
    private static final float FLING_TICK_DECAY = 0.8F;
    
    private static final boolean LOCAL_DEBUG = false;
    
    private static final String LOCAL_TAG = "SyntheticTouchNavigationHandler";
    
    private static final float MAX_FLING_VELOCITY_TICKS_PER_SECOND = 20.0F;
    
    private static final float MIN_FLING_VELOCITY_TICKS_PER_SECOND = 6.0F;
    
    private static final int TICK_DISTANCE_MILLIMETERS = 12;
    
    private float mAccumulatedX;
    
    private float mAccumulatedY;
    
    private float mConfigMaxFlingVelocity;
    
    private float mConfigMinFlingVelocity;
    
    private float mConfigTickDistance;
    
    private boolean mConsumedMovement;
    
    private boolean mCurrentDeviceSupported;
    
    private int mCurrentSource;
    
    private final Runnable mFlingRunnable;
    
    private float mFlingVelocity;
    
    private boolean mFlinging;
    
    private float mLastX;
    
    private float mLastY;
    
    private long mPendingKeyDownTime;
    
    private int mPendingKeyMetaState;
    
    private int mPendingKeyRepeatCount;
    
    private float mStartX;
    
    private float mStartY;
    
    private VelocityTracker mVelocityTracker;
    
    final ViewRootImpl this$0;
    
    public SyntheticTouchNavigationHandler() {
      super(true);
      this.mFlingRunnable = (Runnable)new Object(this);
    }
    
    public void process(MotionEvent param1MotionEvent) {
      long l = param1MotionEvent.getEventTime();
      int i = param1MotionEvent.getDeviceId();
      int j = param1MotionEvent.getSource();
      if (this.mCurrentDeviceId != i || this.mCurrentSource != j) {
        finishKeys(l);
        finishTracking(l);
        this.mCurrentDeviceId = i;
        this.mCurrentSource = j;
        this.mCurrentDeviceSupported = false;
        InputDevice inputDevice = param1MotionEvent.getDevice();
        if (inputDevice != null) {
          InputDevice.MotionRange motionRange2 = inputDevice.getMotionRange(0);
          InputDevice.MotionRange motionRange1 = inputDevice.getMotionRange(1);
          if (motionRange2 != null && motionRange1 != null) {
            this.mCurrentDeviceSupported = true;
            float f1 = motionRange2.getResolution();
            float f2 = f1;
            if (f1 <= 0.0F)
              f2 = motionRange2.getRange() / 48.0F; 
            float f3 = motionRange1.getResolution();
            f1 = f3;
            if (f3 <= 0.0F)
              f1 = motionRange1.getRange() / 48.0F; 
            this.mConfigTickDistance = f2 = 12.0F * (f2 + f1) * 0.5F;
            this.mConfigMinFlingVelocity = f2 * 6.0F;
            this.mConfigMaxFlingVelocity = f2 * 20.0F;
          } 
        } 
      } 
      if (!this.mCurrentDeviceSupported)
        return; 
      j = param1MotionEvent.getActionMasked();
      if (j != 0) {
        if (j != 1 && j != 2) {
          if (j == 3) {
            finishKeys(l);
            finishTracking(l);
          } 
        } else {
          i = this.mActivePointerId;
          if (i >= 0) {
            i = param1MotionEvent.findPointerIndex(i);
            if (i < 0) {
              finishKeys(l);
              finishTracking(l);
            } else {
              this.mVelocityTracker.addMovement(param1MotionEvent);
              float f1 = param1MotionEvent.getX(i);
              float f2 = param1MotionEvent.getY(i);
              this.mAccumulatedX += f1 - this.mLastX;
              this.mAccumulatedY += f2 - this.mLastY;
              this.mLastX = f1;
              this.mLastY = f2;
              i = param1MotionEvent.getMetaState();
              consumeAccumulatedMovement(l, i);
              if (j == 1) {
                if (this.mConsumedMovement && this.mPendingKeyCode != 0) {
                  this.mVelocityTracker.computeCurrentVelocity(1000, this.mConfigMaxFlingVelocity);
                  f1 = this.mVelocityTracker.getXVelocity(this.mActivePointerId);
                  f2 = this.mVelocityTracker.getYVelocity(this.mActivePointerId);
                  if (!startFling(l, f1, f2))
                    finishKeys(l); 
                } 
                finishTracking(l);
              } 
            } 
          } 
        } 
      } else {
        boolean bool = this.mFlinging;
        finishKeys(l);
        finishTracking(l);
        this.mActivePointerId = param1MotionEvent.getPointerId(0);
        VelocityTracker velocityTracker = VelocityTracker.obtain();
        velocityTracker.addMovement(param1MotionEvent);
        this.mStartX = param1MotionEvent.getX();
        float f = param1MotionEvent.getY();
        this.mLastX = this.mStartX;
        this.mLastY = f;
        this.mAccumulatedX = 0.0F;
        this.mAccumulatedY = 0.0F;
        this.mConsumedMovement = bool;
      } 
    }
    
    public void cancel(MotionEvent param1MotionEvent) {
      if (this.mCurrentDeviceId == param1MotionEvent.getDeviceId()) {
        int i = this.mCurrentSource;
        if (i == param1MotionEvent.getSource()) {
          long l = param1MotionEvent.getEventTime();
          finishKeys(l);
          finishTracking(l);
        } 
      } 
    }
    
    private void finishKeys(long param1Long) {
      cancelFling();
      sendKeyUp(param1Long);
    }
    
    private void finishTracking(long param1Long) {
      if (this.mActivePointerId >= 0) {
        this.mActivePointerId = -1;
        this.mVelocityTracker.recycle();
        this.mVelocityTracker = null;
      } 
    }
    
    private void consumeAccumulatedMovement(long param1Long, int param1Int) {
      float f1 = Math.abs(this.mAccumulatedX);
      float f2 = Math.abs(this.mAccumulatedY);
      if (f1 >= f2) {
        if (f1 >= this.mConfigTickDistance) {
          this.mAccumulatedX = consumeAccumulatedMovement(param1Long, param1Int, this.mAccumulatedX, 21, 22);
          this.mAccumulatedY = 0.0F;
          this.mConsumedMovement = true;
        } 
      } else if (f2 >= this.mConfigTickDistance) {
        this.mAccumulatedY = consumeAccumulatedMovement(param1Long, param1Int, this.mAccumulatedY, 19, 20);
        this.mAccumulatedX = 0.0F;
        this.mConsumedMovement = true;
      } 
    }
    
    private float consumeAccumulatedMovement(long param1Long, int param1Int1, float param1Float, int param1Int2, int param1Int3) {
      float f;
      while (true) {
        f = param1Float;
        if (param1Float <= -this.mConfigTickDistance) {
          sendKeyDownOrRepeat(param1Long, param1Int2, param1Int1);
          param1Float += this.mConfigTickDistance;
          continue;
        } 
        break;
      } 
      while (f >= this.mConfigTickDistance) {
        sendKeyDownOrRepeat(param1Long, param1Int3, param1Int1);
        f -= this.mConfigTickDistance;
      } 
      return f;
    }
    
    private void sendKeyDownOrRepeat(long param1Long, int param1Int1, int param1Int2) {
      if (this.mPendingKeyCode != param1Int1) {
        sendKeyUp(param1Long);
        this.mPendingKeyDownTime = param1Long;
        this.mPendingKeyCode = param1Int1;
        this.mPendingKeyRepeatCount = 0;
      } else {
        this.mPendingKeyRepeatCount++;
      } 
      this.mPendingKeyMetaState = param1Int2;
      ViewRootImpl.this.enqueueInputEvent(new KeyEvent(this.mPendingKeyDownTime, param1Long, 0, this.mPendingKeyCode, this.mPendingKeyRepeatCount, this.mPendingKeyMetaState, this.mCurrentDeviceId, 1024, this.mCurrentSource));
    }
    
    private void sendKeyUp(long param1Long) {
      if (this.mPendingKeyCode != 0) {
        ViewRootImpl.this.enqueueInputEvent(new KeyEvent(this.mPendingKeyDownTime, param1Long, 1, this.mPendingKeyCode, 0, this.mPendingKeyMetaState, this.mCurrentDeviceId, 0, 1024, this.mCurrentSource));
        this.mPendingKeyCode = 0;
      } 
    }
    
    private boolean startFling(long param1Long, float param1Float1, float param1Float2) {
      boolean bool;
      switch (this.mPendingKeyCode) {
        default:
          this.mFlinging = bool = postFling(param1Long);
          return bool;
        case 22:
          if (param1Float1 >= this.mConfigMinFlingVelocity && Math.abs(param1Float2) < this.mConfigMinFlingVelocity) {
            this.mFlingVelocity = param1Float1;
          } else {
            return false;
          } 
        case 21:
          if (-param1Float1 >= this.mConfigMinFlingVelocity && Math.abs(param1Float2) < this.mConfigMinFlingVelocity) {
            this.mFlingVelocity = -param1Float1;
          } else {
            return false;
          } 
        case 20:
          if (param1Float2 >= this.mConfigMinFlingVelocity && Math.abs(param1Float1) < this.mConfigMinFlingVelocity) {
            this.mFlingVelocity = param1Float2;
          } else {
            return false;
          } 
        case 19:
          break;
      } 
      if (-param1Float2 >= this.mConfigMinFlingVelocity && Math.abs(param1Float1) < this.mConfigMinFlingVelocity)
        this.mFlingVelocity = -param1Float2; 
      return false;
    }
    
    private boolean postFling(long param1Long) {
      float f = this.mFlingVelocity;
      if (f >= this.mConfigMinFlingVelocity) {
        long l = (long)(this.mConfigTickDistance / f * 1000.0F);
        postAtTime(this.mFlingRunnable, param1Long + l);
        return true;
      } 
      return false;
    }
    
    private void cancelFling() {
      if (this.mFlinging) {
        removeCallbacks(this.mFlingRunnable);
        this.mFlinging = false;
      } 
    }
  }
  
  class SyntheticKeyboardHandler {
    final ViewRootImpl this$0;
    
    public void process(KeyEvent param1KeyEvent) {
      if ((param1KeyEvent.getFlags() & 0x400) != 0)
        return; 
      KeyCharacterMap keyCharacterMap = param1KeyEvent.getKeyCharacterMap();
      int i = param1KeyEvent.getKeyCode();
      int j = param1KeyEvent.getMetaState();
      KeyCharacterMap.FallbackAction fallbackAction = keyCharacterMap.getFallbackAction(i, j);
      if (fallbackAction != null) {
        int k = param1KeyEvent.getFlags();
        long l1 = param1KeyEvent.getDownTime(), l2 = param1KeyEvent.getEventTime();
        int m = param1KeyEvent.getAction(), n = fallbackAction.keyCode;
        int i1 = param1KeyEvent.getRepeatCount(), i2 = fallbackAction.metaState;
        i = param1KeyEvent.getDeviceId();
        int i3 = param1KeyEvent.getScanCode();
        j = param1KeyEvent.getSource();
        param1KeyEvent = KeyEvent.obtain(l1, l2, m, n, i1, i2, i, i3, k | 0x400, j, null);
        fallbackAction.recycle();
        ViewRootImpl.this.enqueueInputEvent(param1KeyEvent);
      } 
    }
  }
  
  private static boolean isNavigationKey(KeyEvent paramKeyEvent) {
    int i = paramKeyEvent.getKeyCode();
    if (i != 61 && i != 62)
      if (i != 66) {
        if (i != 92 && i != 93 && i != 122 && i != 123)
          switch (i) {
            default:
              return false;
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
              break;
          }  
      } else {
        if (ActivityThread.currentPackageName() != null && !ActivityThread.currentPackageName().toLowerCase().contains("camera"))
          return true; 
        return true;
      }  
    return true;
  }
  
  private static boolean isTypingKey(KeyEvent paramKeyEvent) {
    boolean bool;
    if (paramKeyEvent.getUnicodeChar() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean checkForLeavingTouchModeAndConsume(KeyEvent paramKeyEvent) {
    if (!this.mAttachInfo.mInTouchMode)
      return false; 
    int i = paramKeyEvent.getAction();
    if (i != 0 && i != 2)
      return false; 
    if ((paramKeyEvent.getFlags() & 0x4) != 0)
      return false; 
    if (isNavigationKey(paramKeyEvent))
      return ensureTouchMode(false); 
    if (isTypingKey(paramKeyEvent)) {
      ensureTouchMode(false);
      return false;
    } 
    return false;
  }
  
  void setLocalDragState(Object paramObject) {
    this.mLocalDragState = paramObject;
  }
  
  private void handleDragEvent(DragEvent paramDragEvent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mView : Landroid/view/View;
    //   4: ifnull -> 414
    //   7: aload_0
    //   8: getfield mAdded : Z
    //   11: ifeq -> 414
    //   14: aload_1
    //   15: getfield mAction : I
    //   18: istore_2
    //   19: iload_2
    //   20: iconst_1
    //   21: if_icmpne -> 40
    //   24: aload_0
    //   25: aconst_null
    //   26: putfield mCurrentDragView : Landroid/view/View;
    //   29: aload_0
    //   30: aload_1
    //   31: getfield mClipDescription : Landroid/content/ClipDescription;
    //   34: putfield mDragDescription : Landroid/content/ClipDescription;
    //   37: goto -> 58
    //   40: iload_2
    //   41: iconst_4
    //   42: if_icmpne -> 50
    //   45: aload_0
    //   46: aconst_null
    //   47: putfield mDragDescription : Landroid/content/ClipDescription;
    //   50: aload_1
    //   51: aload_0
    //   52: getfield mDragDescription : Landroid/content/ClipDescription;
    //   55: putfield mClipDescription : Landroid/content/ClipDescription;
    //   58: iload_2
    //   59: bipush #6
    //   61: if_icmpne -> 88
    //   64: getstatic android/view/View.sCascadedDragDrop : Z
    //   67: ifeq -> 79
    //   70: aload_0
    //   71: getfield mView : Landroid/view/View;
    //   74: aload_1
    //   75: invokevirtual dispatchDragEnterExitInPreN : (Landroid/view/DragEvent;)Z
    //   78: pop
    //   79: aload_0
    //   80: aconst_null
    //   81: aload_1
    //   82: invokevirtual setDragFocus : (Landroid/view/View;Landroid/view/DragEvent;)V
    //   85: goto -> 414
    //   88: iload_2
    //   89: iconst_2
    //   90: if_icmpeq -> 98
    //   93: iload_2
    //   94: iconst_3
    //   95: if_icmpne -> 174
    //   98: aload_0
    //   99: getfield mDragPoint : Landroid/graphics/PointF;
    //   102: aload_1
    //   103: getfield mX : F
    //   106: aload_1
    //   107: getfield mY : F
    //   110: invokevirtual set : (FF)V
    //   113: aload_0
    //   114: getfield mTranslator : Landroid/content/res/CompatibilityInfo$Translator;
    //   117: astore_3
    //   118: aload_3
    //   119: ifnull -> 130
    //   122: aload_3
    //   123: aload_0
    //   124: getfield mDragPoint : Landroid/graphics/PointF;
    //   127: invokevirtual translatePointInScreenToAppWindow : (Landroid/graphics/PointF;)V
    //   130: aload_0
    //   131: getfield mCurScrollY : I
    //   134: istore #4
    //   136: iload #4
    //   138: ifeq -> 152
    //   141: aload_0
    //   142: getfield mDragPoint : Landroid/graphics/PointF;
    //   145: fconst_0
    //   146: iload #4
    //   148: i2f
    //   149: invokevirtual offset : (FF)V
    //   152: aload_1
    //   153: aload_0
    //   154: getfield mDragPoint : Landroid/graphics/PointF;
    //   157: getfield x : F
    //   160: putfield mX : F
    //   163: aload_1
    //   164: aload_0
    //   165: getfield mDragPoint : Landroid/graphics/PointF;
    //   168: getfield y : F
    //   171: putfield mY : F
    //   174: aload_0
    //   175: getfield mCurrentDragView : Landroid/view/View;
    //   178: astore_3
    //   179: iload_2
    //   180: iconst_3
    //   181: if_icmpne -> 198
    //   184: aload_1
    //   185: getfield mClipData : Landroid/content/ClipData;
    //   188: ifnull -> 198
    //   191: aload_1
    //   192: getfield mClipData : Landroid/content/ClipData;
    //   195: invokevirtual prepareToEnterProcess : ()V
    //   198: aload_0
    //   199: getfield mView : Landroid/view/View;
    //   202: aload_1
    //   203: invokevirtual dispatchDragEvent : (Landroid/view/DragEvent;)Z
    //   206: istore #5
    //   208: iload_2
    //   209: iconst_2
    //   210: if_icmpne -> 226
    //   213: aload_1
    //   214: getfield mEventHandlerWasCalled : Z
    //   217: ifne -> 226
    //   220: aload_0
    //   221: aconst_null
    //   222: aload_1
    //   223: invokevirtual setDragFocus : (Landroid/view/View;Landroid/view/DragEvent;)V
    //   226: aload_3
    //   227: aload_0
    //   228: getfield mCurrentDragView : Landroid/view/View;
    //   231: if_acmpeq -> 286
    //   234: aload_3
    //   235: ifnull -> 251
    //   238: aload_0
    //   239: getfield mWindowSession : Landroid/view/IWindowSession;
    //   242: aload_0
    //   243: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   246: invokeinterface dragRecipientExited : (Landroid/view/IWindow;)V
    //   251: aload_0
    //   252: getfield mCurrentDragView : Landroid/view/View;
    //   255: ifnull -> 271
    //   258: aload_0
    //   259: getfield mWindowSession : Landroid/view/IWindowSession;
    //   262: aload_0
    //   263: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   266: invokeinterface dragRecipientEntered : (Landroid/view/IWindow;)V
    //   271: goto -> 286
    //   274: astore_3
    //   275: aload_0
    //   276: getfield mTag : Ljava/lang/String;
    //   279: ldc_w 'Unable to note drag target change'
    //   282: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   285: pop
    //   286: iload_2
    //   287: iconst_3
    //   288: if_icmpne -> 363
    //   291: aload_0
    //   292: getfield mTag : Ljava/lang/String;
    //   295: astore_3
    //   296: new java/lang/StringBuilder
    //   299: astore #6
    //   301: aload #6
    //   303: invokespecial <init> : ()V
    //   306: aload #6
    //   308: ldc_w 'Reporting drop result: '
    //   311: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   314: pop
    //   315: aload #6
    //   317: iload #5
    //   319: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   322: pop
    //   323: aload_3
    //   324: aload #6
    //   326: invokevirtual toString : ()Ljava/lang/String;
    //   329: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   332: pop
    //   333: aload_0
    //   334: getfield mWindowSession : Landroid/view/IWindowSession;
    //   337: aload_0
    //   338: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   341: iload #5
    //   343: invokeinterface reportDropResult : (Landroid/view/IWindow;Z)V
    //   348: goto -> 363
    //   351: astore_3
    //   352: aload_0
    //   353: getfield mTag : Ljava/lang/String;
    //   356: ldc_w 'Unable to report drop result'
    //   359: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   362: pop
    //   363: iload_2
    //   364: iconst_4
    //   365: if_icmpne -> 414
    //   368: aload_0
    //   369: aconst_null
    //   370: putfield mCurrentDragView : Landroid/view/View;
    //   373: aload_0
    //   374: aconst_null
    //   375: invokevirtual setLocalDragState : (Ljava/lang/Object;)V
    //   378: aload_0
    //   379: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   382: aconst_null
    //   383: putfield mDragToken : Landroid/os/IBinder;
    //   386: aload_0
    //   387: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   390: getfield mDragSurface : Landroid/view/Surface;
    //   393: ifnull -> 414
    //   396: aload_0
    //   397: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   400: getfield mDragSurface : Landroid/view/Surface;
    //   403: invokevirtual release : ()V
    //   406: aload_0
    //   407: getfield mAttachInfo : Landroid/view/View$AttachInfo;
    //   410: aconst_null
    //   411: putfield mDragSurface : Landroid/view/Surface;
    //   414: aload_1
    //   415: invokevirtual recycle : ()V
    //   418: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #7790	-> 0
    //   #7791	-> 14
    //   #7795	-> 19
    //   #7796	-> 24
    //   #7797	-> 29
    //   #7799	-> 40
    //   #7800	-> 45
    //   #7802	-> 50
    //   #7805	-> 58
    //   #7809	-> 64
    //   #7810	-> 70
    //   #7812	-> 79
    //   #7815	-> 88
    //   #7816	-> 98
    //   #7817	-> 113
    //   #7818	-> 122
    //   #7821	-> 130
    //   #7822	-> 141
    //   #7825	-> 152
    //   #7826	-> 163
    //   #7830	-> 174
    //   #7832	-> 179
    //   #7833	-> 191
    //   #7836	-> 198
    //   #7838	-> 208
    //   #7841	-> 220
    //   #7845	-> 226
    //   #7847	-> 234
    //   #7848	-> 238
    //   #7850	-> 251
    //   #7851	-> 258
    //   #7855	-> 271
    //   #7853	-> 274
    //   #7854	-> 275
    //   #7859	-> 286
    //   #7861	-> 291
    //   #7862	-> 333
    //   #7865	-> 348
    //   #7863	-> 351
    //   #7864	-> 352
    //   #7869	-> 363
    //   #7870	-> 368
    //   #7871	-> 373
    //   #7872	-> 378
    //   #7873	-> 386
    //   #7874	-> 396
    //   #7875	-> 406
    //   #7880	-> 414
    //   #7881	-> 418
    // Exception table:
    //   from	to	target	type
    //   238	251	274	android/os/RemoteException
    //   251	258	274	android/os/RemoteException
    //   258	271	274	android/os/RemoteException
    //   291	333	351	android/os/RemoteException
    //   333	348	351	android/os/RemoteException
  }
  
  public void onWindowTitleChanged() {
    this.mAttachInfo.mForceReportNewAttributes = true;
  }
  
  public void handleDispatchWindowShown() {
    this.mAttachInfo.mTreeObserver.dispatchOnWindowShown();
  }
  
  public void handleRequestKeyboardShortcuts(IResultReceiver paramIResultReceiver, int paramInt) {
    Bundle bundle = new Bundle();
    ArrayList<KeyboardShortcutGroup> arrayList = new ArrayList();
    View view = this.mView;
    if (view != null)
      view.requestKeyboardShortcuts(arrayList, paramInt); 
    bundle.putParcelableArrayList("shortcuts_array", arrayList);
    try {
      paramIResultReceiver.send(0, bundle);
    } catch (RemoteException remoteException) {}
  }
  
  public void getLastTouchPoint(Point paramPoint) {
    paramPoint.x = (int)this.mLastTouchPoint.x;
    paramPoint.y = (int)this.mLastTouchPoint.y;
  }
  
  public int getLastTouchSource() {
    return this.mLastTouchSource;
  }
  
  public void setDragFocus(View paramView, DragEvent paramDragEvent) {
    if (this.mCurrentDragView != paramView && !View.sCascadedDragDrop) {
      float f1 = paramDragEvent.mX;
      float f2 = paramDragEvent.mY;
      int i = paramDragEvent.mAction;
      ClipData clipData = paramDragEvent.mClipData;
      paramDragEvent.mX = 0.0F;
      paramDragEvent.mY = 0.0F;
      paramDragEvent.mClipData = null;
      if (this.mCurrentDragView != null) {
        paramDragEvent.mAction = 6;
        this.mCurrentDragView.callDragEventHandler(paramDragEvent);
      } 
      if (paramView != null) {
        paramDragEvent.mAction = 5;
        paramView.callDragEventHandler(paramDragEvent);
      } 
      paramDragEvent.mAction = i;
      paramDragEvent.mX = f1;
      paramDragEvent.mY = f2;
      paramDragEvent.mClipData = clipData;
    } 
    this.mCurrentDragView = paramView;
  }
  
  private AudioManager getAudioManager() {
    View view = this.mView;
    if (view != null) {
      if (this.mAudioManager == null)
        this.mAudioManager = (AudioManager)view.getContext().getSystemService("audio"); 
      return this.mAudioManager;
    } 
    throw new IllegalStateException("getAudioManager called when there is no mView");
  }
  
  private AutofillManager getAutofillManager() {
    View view = this.mView;
    if (view instanceof ViewGroup) {
      view = view;
      if (view.getChildCount() > 0) {
        Context context = view.getChildAt(0).getContext();
        return (AutofillManager)context.getSystemService(AutofillManager.class);
      } 
    } 
    return null;
  }
  
  private boolean isAutofillUiShowing() {
    AutofillManager autofillManager = getAutofillManager();
    if (autofillManager == null)
      return false; 
    return autofillManager.isAutofillUiShowing();
  }
  
  public AccessibilityInteractionController getAccessibilityInteractionController() {
    if (this.mView != null) {
      if (this.mAccessibilityInteractionController == null)
        this.mAccessibilityInteractionController = new AccessibilityInteractionController(this); 
      return this.mAccessibilityInteractionController;
    } 
    throw new IllegalStateException("getAccessibilityInteractionController called when there is no mView");
  }
  
  private int relayoutWindow(WindowManager.LayoutParams paramLayoutParams, int paramInt, boolean paramBoolean) throws RemoteException {
    boolean bool1;
    long l;
    boolean bool2;
    float f = this.mAttachInfo.mApplicationScale;
    if (paramLayoutParams != null && this.mTranslator != null) {
      paramLayoutParams.backup();
      this.mTranslator.translateWindowLayout(paramLayoutParams);
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (paramLayoutParams != null)
      if (this.mOrigWindowType != paramLayoutParams.type)
        if (this.mTargetSdkVersion < 14) {
          String str = this.mTag;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Window type can not be changed after the window is added; ignoring change of ");
          stringBuilder.append(this.mView);
          Slog.w(str, stringBuilder.toString());
          paramLayoutParams.type = this.mOrigWindowType;
        }   
    if (this.mSurface.isValid()) {
      l = this.mSurface.getNextFrameNumber();
    } else {
      l = -1L;
    } 
    IWindowSession iWindowSession = this.mWindowSession;
    W w = this.mWindow;
    int i = this.mSeq;
    View view = this.mView;
    int j = (int)(view.getMeasuredWidth() * f + 0.5F);
    view = this.mView;
    int k = (int)(view.getMeasuredHeight() * f + 0.5F);
    Rect rect1 = this.mTmpFrame, rect2 = this.mTmpRect, rect3 = this.mPendingBackDropFrame;
    DisplayCutout.ParcelableWrapper parcelableWrapper = this.mPendingDisplayCutout;
    MergedConfiguration mergedConfiguration = this.mPendingMergedConfiguration;
    SurfaceControl surfaceControl1 = this.mSurfaceControl;
    InsetsState insetsState = this.mTempInsets;
    InsetsSourceControl[] arrayOfInsetsSourceControl = this.mTempControls;
    Point point = this.mSurfaceSize;
    SurfaceControl surfaceControl2 = this.mBlastSurfaceControl;
    paramInt = iWindowSession.relayout(w, i, paramLayoutParams, j, k, paramInt, paramBoolean, l, rect1, rect2, rect2, rect2, rect3, parcelableWrapper, mergedConfiguration, surfaceControl1, insetsState, arrayOfInsetsSourceControl, point, surfaceControl2);
    this.mManager.refreshForceDark(this.mView, this.mPendingMergedConfiguration.mOplusDarkModeData);
    if (this.mSurfaceControl.isValid()) {
      if (!useBLAST()) {
        this.mSurface.copyFrom(this.mSurfaceControl);
      } else {
        Surface surface = getOrCreateBLASTSurface(this.mSurfaceSize.x, this.mSurfaceSize.y);
        if (surface != null)
          this.mSurface.transferFrom(surface); 
      } 
    } else {
      destroySurface();
    } 
    if ((paramInt & 0x40) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mPendingAlwaysConsumeSystemBars = bool2;
    if (bool1)
      paramLayoutParams.restore(); 
    CompatibilityInfo.Translator translator = this.mTranslator;
    if (translator != null)
      translator.translateRectInScreenToAppWinFrame(this.mTmpFrame); 
    setFrame(this.mTmpFrame);
    this.mInsetsController.onStateChanged(this.mTempInsets);
    this.mInsetsController.onControlsChanged(this.mTempControls);
    return paramInt;
  }
  
  private void setFrame(Rect paramRect) {
    this.mWinFrame.set(paramRect);
    this.mInsetsController.onFrameChanged(paramRect);
  }
  
  public void playSoundEffect(int paramInt) {
    checkThread();
    try {
      StringBuilder stringBuilder;
      AudioManager audioManager = getAudioManager();
      if (paramInt != 0) {
        if (paramInt != 1) {
          if (paramInt != 2) {
            if (paramInt != 3) {
              if (paramInt == 4) {
                audioManager.playSoundEffect(2);
                return;
              } 
              IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
              stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("unknown effect id ");
              stringBuilder.append(paramInt);
              stringBuilder.append(" not defined in ");
              stringBuilder.append(SoundEffectConstants.class.getCanonicalName());
              this(stringBuilder.toString());
              throw illegalArgumentException;
            } 
            stringBuilder.playSoundEffect(4);
            return;
          } 
          stringBuilder.playSoundEffect(1);
          return;
        } 
        stringBuilder.playSoundEffect(3);
        return;
      } 
      stringBuilder.playSoundEffect(0);
      return;
    } catch (IllegalStateException illegalStateException) {
      String str = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("FATAL EXCEPTION when attempting to play sound effect: ");
      stringBuilder.append(illegalStateException);
      Log.e(str, stringBuilder.toString());
      illegalStateException.printStackTrace();
      return;
    } 
  }
  
  public boolean performHapticFeedback(int paramInt, boolean paramBoolean) {
    try {
      return this.mWindowSession.performHapticFeedback(paramInt, paramBoolean);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public View focusSearch(View paramView, int paramInt) {
    checkThread();
    if (!(this.mView instanceof ViewGroup))
      return null; 
    return FocusFinder.getInstance().findNextFocus((ViewGroup)this.mView, paramView, paramInt);
  }
  
  public View keyboardNavigationClusterSearch(View paramView, int paramInt) {
    checkThread();
    return FocusFinder.getInstance().findNextKeyboardNavigationCluster(this.mView, paramView, paramInt);
  }
  
  public void debug() {
    this.mView.debug();
  }
  
  public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("  ");
    String str = stringBuilder.toString();
    paramPrintWriter.print(paramString);
    paramPrintWriter.println("ViewRoot:");
    paramPrintWriter.print(str);
    paramPrintWriter.print("mAdded=");
    paramPrintWriter.print(this.mAdded);
    paramPrintWriter.print(" mRemoved=");
    paramPrintWriter.println(this.mRemoved);
    paramPrintWriter.print(str);
    paramPrintWriter.print("mConsumeBatchedInputScheduled=");
    paramPrintWriter.println(this.mConsumeBatchedInputScheduled);
    paramPrintWriter.print(str);
    paramPrintWriter.print("mConsumeBatchedInputImmediatelyScheduled=");
    paramPrintWriter.println(this.mConsumeBatchedInputImmediatelyScheduled);
    paramPrintWriter.print(str);
    paramPrintWriter.print("mPendingInputEventCount=");
    paramPrintWriter.println(this.mPendingInputEventCount);
    paramPrintWriter.print(str);
    paramPrintWriter.print("mProcessInputEventsScheduled=");
    paramPrintWriter.println(this.mProcessInputEventsScheduled);
    paramPrintWriter.print(str);
    paramPrintWriter.print("mTraversalScheduled=");
    paramPrintWriter.print(this.mTraversalScheduled);
    paramPrintWriter.print(str);
    paramPrintWriter.print("mIsAmbientMode=");
    paramPrintWriter.print(this.mIsAmbientMode);
    paramPrintWriter.print(str);
    paramPrintWriter.print("mUnbufferedInputSource=");
    paramPrintWriter.print(Integer.toHexString(this.mUnbufferedInputSource));
    if (this.mTraversalScheduled) {
      paramPrintWriter.print(" (barrier=");
      paramPrintWriter.print(this.mTraversalBarrier);
      paramPrintWriter.println(")");
    } else {
      paramPrintWriter.println();
    } 
    this.mFirstInputStage.dump(str, paramPrintWriter);
    this.mChoreographer.dump(paramString, paramPrintWriter);
    this.mInsetsController.dump(paramString, paramPrintWriter);
    paramPrintWriter.print(paramString);
    paramPrintWriter.println("View Hierarchy:");
    dumpViewHierarchy(str, paramPrintWriter, this.mView);
  }
  
  private void dumpViewHierarchy(String paramString, PrintWriter paramPrintWriter, View paramView) {
    paramPrintWriter.print(paramString);
    if (paramView == null) {
      paramPrintWriter.println("null");
      return;
    } 
    paramPrintWriter.println(paramView.toString());
    if (!(paramView instanceof ViewGroup))
      return; 
    paramView = paramView;
    int i = paramView.getChildCount();
    if (i <= 0)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("  ");
    paramString = stringBuilder.toString();
    for (byte b = 0; b < i; b++)
      dumpViewHierarchy(paramString, paramPrintWriter, paramView.getChildAt(b)); 
  }
  
  class GfxInfo {
    public long renderNodeMemoryAllocated;
    
    public long renderNodeMemoryUsage;
    
    public int viewCount;
    
    void add(GfxInfo param1GfxInfo) {
      this.viewCount += param1GfxInfo.viewCount;
      this.renderNodeMemoryUsage += param1GfxInfo.renderNodeMemoryUsage;
      this.renderNodeMemoryAllocated += param1GfxInfo.renderNodeMemoryAllocated;
    }
  }
  
  GfxInfo getGfxInfo() {
    GfxInfo gfxInfo = new GfxInfo();
    View view = this.mView;
    if (view != null)
      appendGfxInfo(view, gfxInfo); 
    return gfxInfo;
  }
  
  private static void computeRenderNodeUsage(RenderNode paramRenderNode, GfxInfo paramGfxInfo) {
    if (paramRenderNode == null)
      return; 
    paramGfxInfo.renderNodeMemoryUsage += paramRenderNode.computeApproximateMemoryUsage();
    paramGfxInfo.renderNodeMemoryAllocated += paramRenderNode.computeApproximateMemoryAllocated();
  }
  
  private static void appendGfxInfo(View paramView, GfxInfo paramGfxInfo) {
    paramGfxInfo.viewCount++;
    computeRenderNodeUsage(paramView.mRenderNode, paramGfxInfo);
    computeRenderNodeUsage(paramView.mBackgroundRenderNode, paramGfxInfo);
    if (paramView instanceof ViewGroup) {
      paramView = paramView;
      int i = paramView.getChildCount();
      for (byte b = 0; b < i; b++)
        appendGfxInfo(paramView.getChildAt(b), paramGfxInfo); 
    } 
  }
  
  boolean die(boolean paramBoolean) {
    if (DEBUG_PANIC) {
      String str = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("die ");
      stringBuilder.append(this);
      stringBuilder.append(", caller=");
      stringBuilder.append(Debug.getCallers(5));
      Log.d(str, stringBuilder.toString());
    } 
    if (paramBoolean && !this.mIsInTraversal) {
      doDie();
      return false;
    } 
    if (!this.mIsDrawing) {
      destroyHardwareRenderer();
    } else {
      String str1 = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Attempting to destroy the window while drawing!\n  window=");
      stringBuilder.append(this);
      stringBuilder.append(", title=");
      WindowManager.LayoutParams layoutParams = this.mWindowAttributes;
      stringBuilder.append(layoutParams.getTitle());
      String str2 = stringBuilder.toString();
      Log.e(str1, str2);
    } 
    this.mHandler.sendEmptyMessage(3);
    return true;
  }
  
  void doDie() {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual checkThread : ()V
    //   4: aload_0
    //   5: getfield mGraphicLock : Ljava/lang/Object;
    //   8: astore_1
    //   9: aload_1
    //   10: monitorenter
    //   11: aload_0
    //   12: monitorenter
    //   13: aload_0
    //   14: getfield mRemoved : Z
    //   17: ifeq -> 25
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_1
    //   23: monitorexit
    //   24: return
    //   25: iconst_1
    //   26: istore_2
    //   27: aload_0
    //   28: iconst_1
    //   29: putfield mRemoved : Z
    //   32: aload_0
    //   33: getfield mAdded : Z
    //   36: ifeq -> 43
    //   39: aload_0
    //   40: invokevirtual dispatchDetachedFromWindow : ()V
    //   43: aload_0
    //   44: getfield mAdded : Z
    //   47: ifeq -> 142
    //   50: aload_0
    //   51: getfield mFirst : Z
    //   54: ifne -> 142
    //   57: aload_0
    //   58: invokespecial destroyHardwareRenderer : ()V
    //   61: aload_0
    //   62: getfield mView : Landroid/view/View;
    //   65: ifnull -> 142
    //   68: aload_0
    //   69: getfield mView : Landroid/view/View;
    //   72: invokevirtual getVisibility : ()I
    //   75: istore_3
    //   76: aload_0
    //   77: getfield mViewVisibility : I
    //   80: iload_3
    //   81: if_icmpeq -> 87
    //   84: goto -> 89
    //   87: iconst_0
    //   88: istore_2
    //   89: aload_0
    //   90: getfield mWindowAttributesChanged : Z
    //   93: istore #4
    //   95: iload #4
    //   97: ifne -> 104
    //   100: iload_2
    //   101: ifeq -> 138
    //   104: aload_0
    //   105: aload_0
    //   106: getfield mWindowAttributes : Landroid/view/WindowManager$LayoutParams;
    //   109: iload_3
    //   110: iconst_0
    //   111: invokespecial relayoutWindow : (Landroid/view/WindowManager$LayoutParams;IZ)I
    //   114: iconst_2
    //   115: iand
    //   116: ifeq -> 133
    //   119: aload_0
    //   120: getfield mWindowSession : Landroid/view/IWindowSession;
    //   123: aload_0
    //   124: getfield mWindow : Landroid/view/ViewRootImpl$W;
    //   127: aconst_null
    //   128: invokeinterface finishDrawing : (Landroid/view/IWindow;Landroid/view/SurfaceControl$Transaction;)V
    //   133: goto -> 138
    //   136: astore #5
    //   138: aload_0
    //   139: invokespecial destroySurface : ()V
    //   142: aload_0
    //   143: iconst_0
    //   144: putfield mAdded : Z
    //   147: aload_0
    //   148: monitorexit
    //   149: aload_1
    //   150: monitorexit
    //   151: invokestatic getInstance : ()Landroid/view/WindowManagerGlobal;
    //   154: aload_0
    //   155: invokevirtual doRemoveView : (Landroid/view/ViewRootImpl;)V
    //   158: return
    //   159: astore #5
    //   161: aload_0
    //   162: monitorexit
    //   163: aload #5
    //   165: athrow
    //   166: astore #5
    //   168: aload_1
    //   169: monitorexit
    //   170: aload #5
    //   172: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #8272	-> 0
    //   #8276	-> 4
    //   #8278	-> 11
    //   #8279	-> 13
    //   #8280	-> 20
    //   #8282	-> 25
    //   #8283	-> 32
    //   #8284	-> 39
    //   #8287	-> 43
    //   #8288	-> 57
    //   #8290	-> 61
    //   #8291	-> 68
    //   #8292	-> 76
    //   #8293	-> 89
    //   #8298	-> 104
    //   #8300	-> 119
    //   #8304	-> 133
    //   #8303	-> 136
    //   #8307	-> 138
    //   #8311	-> 142
    //   #8312	-> 147
    //   #8315	-> 149
    //   #8317	-> 151
    //   #8318	-> 158
    //   #8312	-> 159
    //   #8315	-> 166
    // Exception table:
    //   from	to	target	type
    //   11	13	166	finally
    //   13	20	159	finally
    //   20	22	159	finally
    //   22	24	166	finally
    //   27	32	159	finally
    //   32	39	159	finally
    //   39	43	159	finally
    //   43	57	159	finally
    //   57	61	159	finally
    //   61	68	159	finally
    //   68	76	159	finally
    //   76	84	159	finally
    //   89	95	159	finally
    //   104	119	136	android/os/RemoteException
    //   104	119	159	finally
    //   119	133	136	android/os/RemoteException
    //   119	133	159	finally
    //   138	142	159	finally
    //   142	147	159	finally
    //   147	149	159	finally
    //   149	151	166	finally
    //   161	163	159	finally
    //   163	166	166	finally
    //   168	170	166	finally
  }
  
  public void requestUpdateConfiguration(Configuration paramConfiguration) {
    Message message = this.mHandler.obtainMessage(18, paramConfiguration);
    this.mHandler.sendMessage(message);
  }
  
  public void loadSystemProperties() {
    this.mHandler.post((Runnable)new Object(this));
  }
  
  private void destroyHardwareRenderer() {
    synchronized (this.mDestroyLock) {
      ThreadedRenderer threadedRenderer = this.mAttachInfo.mThreadedRenderer;
      if (threadedRenderer != null) {
        if (this.mView != null)
          threadedRenderer.destroyHardwareResources(this.mView); 
        threadedRenderer.destroy();
        threadedRenderer.setRequested(false);
        this.mAttachInfo.mThreadedRenderer = null;
        this.mAttachInfo.mHardwareAccelerated = false;
      } 
      return;
    } 
  }
  
  private void dispatchResized(Rect paramRect1, Rect paramRect2, Rect paramRect3, Rect paramRect4, boolean paramBoolean1, MergedConfiguration paramMergedConfiguration, Rect paramRect5, boolean paramBoolean2, boolean paramBoolean3, int paramInt, DisplayCutout.ParcelableWrapper paramParcelableWrapper) {
    MergedConfiguration mergedConfiguration;
    Rect rect;
    byte b;
    if (DEBUG_LAYOUT) {
      String str1 = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Resizing ");
      stringBuilder.append(this);
      stringBuilder.append(": frame=");
      stringBuilder.append(paramRect1.toShortString());
      stringBuilder.append(" contentInsets=");
      stringBuilder.append(paramRect2.toShortString());
      stringBuilder.append(" visibleInsets=");
      stringBuilder.append(paramRect3.toShortString());
      stringBuilder.append(" reportDraw=");
      stringBuilder.append(paramBoolean1);
      stringBuilder.append(" backDropFrame=");
      stringBuilder.append(paramRect5);
      String str2 = stringBuilder.toString();
      Log.v(str1, str2);
    } 
    boolean bool = this.mDragResizing;
    boolean bool1 = true;
    if (bool && this.mUseMTRenderer) {
      bool = paramRect1.equals(paramRect5);
      synchronized (this.mWindowCallbacks) {
        for (b = this.mWindowCallbacks.size() - 1; b >= 0; b--)
          ((WindowCallbacks)this.mWindowCallbacks.get(b)).onWindowSizeIsChanging(paramRect5, bool, paramRect3, paramRect4); 
      } 
    } 
    ViewRootHandler viewRootHandler = this.mHandler;
    if (paramBoolean1) {
      b = 5;
    } else {
      b = 4;
    } 
    Message message = viewRootHandler.obtainMessage(b);
    CompatibilityInfo.Translator translator = this.mTranslator;
    if (translator != null) {
      translator.translateRectInScreenToAppWindow(paramRect1);
      this.mTranslator.translateRectInScreenToAppWindow(paramRect2);
      this.mTranslator.translateRectInScreenToAppWindow(paramRect3);
    } 
    SomeArgs someArgs = SomeArgs.obtain();
    if (Binder.getCallingPid() == Process.myPid()) {
      b = bool1;
    } else {
      b = 0;
    } 
    if (b != 0)
      paramRect1 = new Rect(paramRect1); 
    someArgs.arg1 = paramRect1;
    if (b != 0)
      paramRect2 = new Rect(paramRect2); 
    someArgs.arg2 = paramRect2;
    if (b != 0)
      paramRect3 = new Rect(paramRect3); 
    someArgs.arg3 = paramRect3;
    if (b != 0 && paramMergedConfiguration != null) {
      mergedConfiguration = new MergedConfiguration(paramMergedConfiguration);
    } else {
      mergedConfiguration = paramMergedConfiguration;
    } 
    someArgs.arg4 = mergedConfiguration;
    if (b != 0) {
      rect = new Rect(paramRect4);
    } else {
      rect = paramRect4;
    } 
    someArgs.arg6 = rect;
    if (b != 0)
      paramRect5 = new Rect(paramRect5); 
    someArgs.arg8 = paramRect5;
    someArgs.arg9 = paramParcelableWrapper.get();
    someArgs.argi1 = paramBoolean2;
    someArgs.argi2 = paramBoolean3;
    someArgs.argi3 = paramInt;
    message.obj = someArgs;
    this.mHandler.sendMessage(message);
  }
  
  private void dispatchInsetsChanged(InsetsState paramInsetsState) {
    InsetsState insetsState = paramInsetsState;
    if (Binder.getCallingPid() == Process.myPid())
      insetsState = new InsetsState(paramInsetsState, true); 
    this.mHandler.obtainMessage(30, insetsState).sendToTarget();
  }
  
  private void dispatchInsetsControlChanged(InsetsState paramInsetsState, InsetsSourceControl[] paramArrayOfInsetsSourceControl) {
    InsetsState insetsState = paramInsetsState;
    if (Binder.getCallingPid() == Process.myPid()) {
      paramInsetsState = new InsetsState(paramInsetsState, true);
      insetsState = paramInsetsState;
      if (paramArrayOfInsetsSourceControl != null) {
        int i = paramArrayOfInsetsSourceControl.length - 1;
        while (true) {
          insetsState = paramInsetsState;
          if (i >= 0) {
            paramArrayOfInsetsSourceControl[i] = new InsetsSourceControl(paramArrayOfInsetsSourceControl[i]);
            i--;
            continue;
          } 
          break;
        } 
      } 
    } 
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = insetsState;
    someArgs.arg2 = paramArrayOfInsetsSourceControl;
    this.mHandler.obtainMessage(31, someArgs).sendToTarget();
  }
  
  private void showInsets(int paramInt, boolean paramBoolean) {
    this.mHandler.obtainMessage(34, paramInt, paramBoolean).sendToTarget();
  }
  
  private void hideInsets(int paramInt, boolean paramBoolean) {
    this.mHandler.obtainMessage(35, paramInt, paramBoolean).sendToTarget();
  }
  
  public void dispatchMoved(int paramInt1, int paramInt2) {
    if (DEBUG_LAYOUT) {
      String str = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Window moved ");
      stringBuilder.append(this);
      stringBuilder.append(": newX=");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" newY=");
      stringBuilder.append(paramInt2);
      Log.v(str, stringBuilder.toString());
    } 
    int i = paramInt1, j = paramInt2;
    if (this.mTranslator != null) {
      PointF pointF = new PointF(paramInt1, paramInt2);
      this.mTranslator.translatePointInScreenToAppWindow(pointF);
      i = (int)(pointF.x + 0.5D);
      j = (int)(pointF.y + 0.5D);
    } 
    Message message = this.mHandler.obtainMessage(23, i, j);
    this.mHandler.sendMessage(message);
  }
  
  class QueuedInputEvent {
    public static final int FLAG_DEFERRED = 2;
    
    public static final int FLAG_DELIVER_POST_IME = 1;
    
    public static final int FLAG_FINISHED = 4;
    
    public static final int FLAG_FINISHED_HANDLED = 8;
    
    public static final int FLAG_MODIFIED_FOR_COMPATIBILITY = 64;
    
    public static final int FLAG_RESYNTHESIZED = 16;
    
    public static final int FLAG_UNHANDLED = 32;
    
    public InputEvent mEvent;
    
    public int mFlags;
    
    public QueuedInputEvent mNext;
    
    public InputEventReceiver mReceiver;
    
    private QueuedInputEvent() {}
    
    public boolean shouldSkipIme() {
      int i = this.mFlags;
      null = true;
      if ((i & 0x1) != 0)
        return true; 
      InputEvent inputEvent = this.mEvent;
      if (inputEvent instanceof MotionEvent)
        if (!inputEvent.isFromSource(2)) {
          inputEvent = this.mEvent;
          if (inputEvent.isFromSource(4194304))
            return null; 
        } else {
          return null;
        }  
      return false;
    }
    
    public boolean shouldSendToSynthesizer() {
      if ((this.mFlags & 0x20) != 0)
        return true; 
      return false;
    }
    
    public String toString() {
      String str1;
      StringBuilder stringBuilder1 = new StringBuilder("QueuedInputEvent{flags=");
      boolean bool = flagToString("DELIVER_POST_IME", 1, false, stringBuilder1);
      bool = flagToString("DEFERRED", 2, bool, stringBuilder1);
      bool = flagToString("FINISHED", 4, bool, stringBuilder1);
      bool = flagToString("FINISHED_HANDLED", 8, bool, stringBuilder1);
      bool = flagToString("RESYNTHESIZED", 16, bool, stringBuilder1);
      bool = flagToString("UNHANDLED", 32, bool, stringBuilder1);
      if (!bool)
        stringBuilder1.append("0"); 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(", hasNextQueuedEvent=");
      InputEvent inputEvent = this.mEvent;
      String str2 = "true";
      if (inputEvent != null) {
        str1 = "true";
      } else {
        str1 = "false";
      } 
      stringBuilder2.append(str1);
      stringBuilder1.append(stringBuilder2.toString());
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(", hasInputEventReceiver=");
      if (this.mReceiver != null) {
        str1 = str2;
      } else {
        str1 = "false";
      } 
      stringBuilder2.append(str1);
      stringBuilder1.append(stringBuilder2.toString());
      StringBuilder stringBuilder3 = new StringBuilder();
      stringBuilder3.append(", mEvent=");
      stringBuilder3.append(this.mEvent);
      stringBuilder3.append("}");
      stringBuilder1.append(stringBuilder3.toString());
      return stringBuilder1.toString();
    }
    
    private boolean flagToString(String param1String, int param1Int, boolean param1Boolean, StringBuilder param1StringBuilder) {
      if ((this.mFlags & param1Int) != 0) {
        if (param1Boolean)
          param1StringBuilder.append("|"); 
        param1StringBuilder.append(param1String);
        return true;
      } 
      return param1Boolean;
    }
  }
  
  private QueuedInputEvent obtainQueuedInputEvent(InputEvent paramInputEvent, InputEventReceiver paramInputEventReceiver, int paramInt) {
    QueuedInputEvent queuedInputEvent = this.mQueuedInputEventPool;
    if (queuedInputEvent != null) {
      this.mQueuedInputEventPoolSize--;
      this.mQueuedInputEventPool = queuedInputEvent.mNext;
      queuedInputEvent.mNext = null;
    } else {
      queuedInputEvent = new QueuedInputEvent();
    } 
    queuedInputEvent.mEvent = paramInputEvent;
    queuedInputEvent.mReceiver = paramInputEventReceiver;
    queuedInputEvent.mFlags = paramInt;
    return queuedInputEvent;
  }
  
  private void recycleQueuedInputEvent(QueuedInputEvent paramQueuedInputEvent) {
    paramQueuedInputEvent.mEvent = null;
    paramQueuedInputEvent.mReceiver = null;
    int i = this.mQueuedInputEventPoolSize;
    if (i < 10) {
      this.mQueuedInputEventPoolSize = i + 1;
      paramQueuedInputEvent.mNext = this.mQueuedInputEventPool;
      this.mQueuedInputEventPool = paramQueuedInputEvent;
    } 
  }
  
  void enqueueInputEvent(InputEvent paramInputEvent) {
    enqueueInputEvent(paramInputEvent, null, 0, false);
  }
  
  void enqueueInputEvent(InputEvent paramInputEvent, InputEventReceiver paramInputEventReceiver, int paramInt, boolean paramBoolean) {
    QueuedInputEvent queuedInputEvent1 = obtainQueuedInputEvent(paramInputEvent, paramInputEventReceiver, paramInt);
    QueuedInputEvent queuedInputEvent2 = this.mPendingInputEventTail;
    if (queuedInputEvent2 == null) {
      this.mPendingInputEventHead = queuedInputEvent1;
      this.mPendingInputEventTail = queuedInputEvent1;
    } else {
      queuedInputEvent2.mNext = queuedInputEvent1;
      this.mPendingInputEventTail = queuedInputEvent1;
    } 
    this.mPendingInputEventCount = paramInt = this.mPendingInputEventCount + 1;
    Trace.traceCounter(4L, this.mPendingInputEventQueueLengthCounterName, paramInt);
    if (InputLog.DEBUG) {
      String str = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(" enqueueInputEvent,");
      stringBuilder.append(paramInputEvent);
      stringBuilder.append(",");
      stringBuilder.append(Debug.getCallers(3));
      InputLog.d(str, stringBuilder.toString());
    } 
    if (paramInputEvent instanceof MotionEvent) {
      MotionEvent motionEvent = (MotionEvent)paramInputEvent;
      if (motionEvent != null && motionEvent.getActionMasked() == 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("enqueueInputEvent");
        stringBuilder.append(paramInputEvent);
        Slog.d("ViewRootImpl", stringBuilder.toString());
      } 
    } 
    if (paramBoolean) {
      doProcessInputEvents();
    } else {
      scheduleProcessInputEvents();
    } 
  }
  
  private void scheduleProcessInputEvents() {
    if (!this.mProcessInputEventsScheduled) {
      this.mProcessInputEventsScheduled = true;
      Message message = this.mHandler.obtainMessage(19);
      message.setAsynchronous(true);
      this.mHandler.sendMessage(message);
    } 
  }
  
  void doProcessInputEvents() {
    while (this.mPendingInputEventHead != null) {
      QueuedInputEvent queuedInputEvent1 = this.mPendingInputEventHead;
      QueuedInputEvent queuedInputEvent2 = queuedInputEvent1.mNext;
      if (queuedInputEvent2 == null)
        this.mPendingInputEventTail = null; 
      queuedInputEvent1.mNext = null;
      int i = this.mPendingInputEventCount - 1;
      Trace.traceCounter(4L, this.mPendingInputEventQueueLengthCounterName, i);
      long l1 = queuedInputEvent1.mEvent.getEventTimeNano();
      long l2 = l1;
      long l3 = l2;
      if (queuedInputEvent1.mEvent instanceof MotionEvent) {
        BoostFramework.ScrollOptimizer.setSurface(this.mSurface);
        MotionEvent motionEvent = (MotionEvent)queuedInputEvent1.mEvent;
        l3 = l2;
        if (motionEvent.getHistorySize() > 0)
          l3 = motionEvent.getHistoricalEventTimeNano(0); 
      } 
      this.mChoreographer.mFrameInfo.updateInputEventTime(l1, l3);
      deliverInputEvent(queuedInputEvent1);
    } 
    if (this.mProcessInputEventsScheduled) {
      this.mProcessInputEventsScheduled = false;
      this.mHandler.removeMessages(19);
    } 
  }
  
  private void deliverInputEvent(QueuedInputEvent paramQueuedInputEvent) {
    InputEvent inputEvent = paramQueuedInputEvent.mEvent;
    int i = inputEvent.getId();
    Trace.asyncTraceBegin(8L, "deliverInputEvent", i);
    if (Trace.isTagEnabled(8L)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("deliverInputEvent src=0x");
      InputEvent inputEvent1 = paramQueuedInputEvent.mEvent;
      stringBuilder.append(Integer.toHexString(inputEvent1.getSource()));
      stringBuilder.append(" eventTimeNano=");
      inputEvent1 = paramQueuedInputEvent.mEvent;
      stringBuilder.append(inputEvent1.getEventTimeNano());
      stringBuilder.append(" id=0x");
      inputEvent1 = paramQueuedInputEvent.mEvent;
      stringBuilder.append(Integer.toHexString(inputEvent1.getId()));
      String str = stringBuilder.toString();
      Trace.traceBegin(8L, str);
    } 
    try {
      InputStage inputStage;
      if (this.mInputEventConsistencyVerifier != null) {
        Trace.traceBegin(8L, "verifyEventConsistency");
        try {
          this.mInputEventConsistencyVerifier.onInputEvent(paramQueuedInputEvent.mEvent, 0);
        } finally {
          Trace.traceEnd(8L);
        } 
      } 
      if (paramQueuedInputEvent.shouldSendToSynthesizer()) {
        inputStage = this.mSyntheticInputStage;
      } else if (paramQueuedInputEvent.shouldSkipIme()) {
        inputStage = this.mFirstPostImeInputStage;
      } else {
        inputStage = this.mFirstInputStage;
      } 
      if (paramQueuedInputEvent.mEvent instanceof KeyEvent) {
        Trace.traceBegin(8L, "preDispatchToUnhandledKeyManager");
        try {
          this.mUnhandledKeyManager.preDispatch((KeyEvent)paramQueuedInputEvent.mEvent);
        } finally {
          Trace.traceEnd(8L);
        } 
      } 
      if (inputStage != null) {
        handleWindowFocusChanged();
        inputStage.deliver(paramQueuedInputEvent);
      } else {
        finishInputEvent(paramQueuedInputEvent);
      } 
      return;
    } finally {
      Trace.traceEnd(8L);
    } 
  }
  
  private void finishInputEvent(QueuedInputEvent paramQueuedInputEvent) {
    InputEvent inputEvent = paramQueuedInputEvent.mEvent;
    int i = inputEvent.getId();
    Trace.asyncTraceEnd(8L, "deliverInputEvent", i);
    if (paramQueuedInputEvent.mReceiver != null) {
      boolean bool;
      int j = paramQueuedInputEvent.mFlags;
      i = 1;
      if ((j & 0x8) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      if ((paramQueuedInputEvent.mFlags & 0x40) == 0)
        i = 0; 
      if (i != 0) {
        Trace.traceBegin(8L, "processInputEventBeforeFinish");
        try {
          InputEventCompatProcessor inputEventCompatProcessor = this.mInputCompatProcessor;
          InputEvent inputEvent2 = paramQueuedInputEvent.mEvent;
          InputEvent inputEvent1 = inputEventCompatProcessor.processInputEventBeforeFinish(inputEvent2);
          Trace.traceEnd(8L);
        } finally {
          Trace.traceEnd(8L);
        } 
      } else {
        paramQueuedInputEvent.mReceiver.finishInputEvent(paramQueuedInputEvent.mEvent, bool);
      } 
    } else {
      paramQueuedInputEvent.mEvent.recycleIfNeededAfterDispatch();
    } 
    recycleQueuedInputEvent(paramQueuedInputEvent);
  }
  
  static boolean isTerminalInputEvent(InputEvent paramInputEvent) {
    boolean bool = paramInputEvent instanceof KeyEvent;
    boolean bool1 = false, bool2 = false;
    if (bool) {
      paramInputEvent = paramInputEvent;
      bool1 = bool2;
      if (paramInputEvent.getAction() == 1)
        bool1 = true; 
      return bool1;
    } 
    paramInputEvent = paramInputEvent;
    int i = paramInputEvent.getAction();
    if (i == 1 || i == 3 || i == 10)
      bool1 = true; 
    return bool1;
  }
  
  void scheduleConsumeBatchedInput() {
    if (!this.mConsumeBatchedInputScheduled && !this.mConsumeBatchedInputImmediatelyScheduled) {
      this.mConsumeBatchedInputScheduled = true;
      this.mChoreographer.postCallback(0, this.mConsumedBatchedInputRunnable, null);
    } 
  }
  
  void unscheduleConsumeBatchedInput() {
    if (this.mConsumeBatchedInputScheduled) {
      this.mConsumeBatchedInputScheduled = false;
      this.mChoreographer.removeCallbacks(0, this.mConsumedBatchedInputRunnable, null);
    } 
  }
  
  void scheduleConsumeBatchedInputImmediately() {
    if (!this.mConsumeBatchedInputImmediatelyScheduled) {
      unscheduleConsumeBatchedInput();
      this.mConsumeBatchedInputImmediatelyScheduled = true;
      this.mHandler.post(this.mConsumeBatchedInputImmediatelyRunnable);
    } 
  }
  
  boolean doConsumeBatchedInput(long paramLong) {
    boolean bool;
    WindowInputEventReceiver windowInputEventReceiver = this.mInputEventReceiver;
    if (windowInputEventReceiver != null) {
      bool = windowInputEventReceiver.consumeBatchedInputEvents(paramLong);
    } else {
      bool = false;
    } 
    doProcessInputEvents();
    return bool;
  }
  
  class TraversalRunnable implements Runnable {
    final ViewRootImpl this$0;
    
    public void run() {
      ViewRootImpl.this.doTraversal();
    }
  }
  
  final class WindowInputEventReceiver extends InputEventReceiver {
    final ViewRootImpl this$0;
    
    public WindowInputEventReceiver(InputChannel param1InputChannel, Looper param1Looper) {
      super(param1InputChannel, param1Looper);
    }
    
    public void onInputEvent(InputEvent param1InputEvent) {
      Trace.traceBegin(8L, "processInputEventForCompatibility");
      try {
        byte b;
        if ((ViewRootImpl.this.mLastReportedMergedConfiguration.getMergedConfiguration()).windowConfiguration.getWindowingMode() == 100) {
          b = 1;
        } else {
          b = 0;
        } 
        if (b && ViewRootImpl.this.mView != null && ViewRootImpl.this.mView instanceof DecorView && param1InputEvent instanceof MotionEvent)
          ((DecorView)ViewRootImpl.this.mView).dispatchTouchEventToZoomDecorView((MotionEvent)param1InputEvent); 
        ViewRootImpl viewRootImpl2 = ViewRootImpl.this;
        List<InputEvent> list = viewRootImpl2.mInputCompatProcessor.processInputEventForCompatibility(param1InputEvent);
        Trace.traceEnd(8L);
        return;
      } finally {
        Trace.traceEnd(8L);
      } 
    }
    
    public void onBatchedInputEventPending(int param1Int) {
      if (ViewRootImpl.this.mUnbufferedInputDispatch || (ViewRootImpl.this.mUnbufferedInputSource & param1Int) != 0 || ViewRootImpl.this.mStopped) {
        param1Int = 1;
      } else {
        param1Int = 0;
      } 
      if (param1Int != 0) {
        if (ViewRootImpl.this.mConsumeBatchedInputScheduled)
          ViewRootImpl.this.unscheduleConsumeBatchedInput(); 
        consumeBatchedInputEvents(-1L);
        return;
      } 
      ViewRootImpl.this.scheduleConsumeBatchedInput();
    }
    
    public void onFocusEvent(boolean param1Boolean1, boolean param1Boolean2) {
      ViewRootImpl.this.windowFocusChanged(param1Boolean1, param1Boolean2);
    }
    
    public void dispose() {
      ViewRootImpl.this.unscheduleConsumeBatchedInput();
      super.dispose();
    }
  }
  
  class ConsumeBatchedInputRunnable implements Runnable {
    final ViewRootImpl this$0;
    
    public void run() {
      ViewRootImpl.this.mConsumeBatchedInputScheduled = false;
      ViewRootImpl viewRootImpl = ViewRootImpl.this;
      if (viewRootImpl.doConsumeBatchedInput(viewRootImpl.mChoreographer.getFrameTimeNanos()))
        ViewRootImpl.this.scheduleConsumeBatchedInput(); 
    }
  }
  
  class ConsumeBatchedInputImmediatelyRunnable implements Runnable {
    final ViewRootImpl this$0;
    
    public void run() {
      ViewRootImpl.this.mConsumeBatchedInputImmediatelyScheduled = false;
      ViewRootImpl.this.doConsumeBatchedInput(-1L);
    }
  }
  
  class InvalidateOnAnimationRunnable implements Runnable {
    private boolean mPosted;
    
    private View.AttachInfo.InvalidateInfo[] mTempViewRects;
    
    private View[] mTempViews;
    
    private final ArrayList<View.AttachInfo.InvalidateInfo> mViewRects;
    
    private final ArrayList<View> mViews;
    
    final ViewRootImpl this$0;
    
    InvalidateOnAnimationRunnable() {
      this.mViews = new ArrayList<>();
      this.mViewRects = new ArrayList<>();
    }
    
    public void addView(View param1View) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mViews : Ljava/util/ArrayList;
      //   6: aload_1
      //   7: invokevirtual add : (Ljava/lang/Object;)Z
      //   10: pop
      //   11: aload_0
      //   12: invokespecial postIfNeededLocked : ()V
      //   15: aload_0
      //   16: monitorexit
      //   17: return
      //   18: astore_1
      //   19: aload_0
      //   20: monitorexit
      //   21: aload_1
      //   22: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #8903	-> 0
      //   #8904	-> 2
      //   #8905	-> 11
      //   #8906	-> 15
      //   #8907	-> 17
      //   #8906	-> 18
      // Exception table:
      //   from	to	target	type
      //   2	11	18	finally
      //   11	15	18	finally
      //   15	17	18	finally
      //   19	21	18	finally
    }
    
    public void addViewRect(View.AttachInfo.InvalidateInfo param1InvalidateInfo) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mViewRects : Ljava/util/ArrayList;
      //   6: aload_1
      //   7: invokevirtual add : (Ljava/lang/Object;)Z
      //   10: pop
      //   11: aload_0
      //   12: invokespecial postIfNeededLocked : ()V
      //   15: aload_0
      //   16: monitorexit
      //   17: return
      //   18: astore_1
      //   19: aload_0
      //   20: monitorexit
      //   21: aload_1
      //   22: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #8910	-> 0
      //   #8911	-> 2
      //   #8912	-> 11
      //   #8913	-> 15
      //   #8914	-> 17
      //   #8913	-> 18
      // Exception table:
      //   from	to	target	type
      //   2	11	18	finally
      //   11	15	18	finally
      //   15	17	18	finally
      //   19	21	18	finally
    }
    
    public void removeView(View param1View) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mViews : Ljava/util/ArrayList;
      //   6: aload_1
      //   7: invokevirtual remove : (Ljava/lang/Object;)Z
      //   10: pop
      //   11: aload_0
      //   12: getfield mViewRects : Ljava/util/ArrayList;
      //   15: invokevirtual size : ()I
      //   18: istore_2
      //   19: iload_2
      //   20: iconst_1
      //   21: isub
      //   22: istore_3
      //   23: iload_2
      //   24: ifle -> 68
      //   27: aload_0
      //   28: getfield mViewRects : Ljava/util/ArrayList;
      //   31: iload_3
      //   32: invokevirtual get : (I)Ljava/lang/Object;
      //   35: checkcast android/view/View$AttachInfo$InvalidateInfo
      //   38: astore #4
      //   40: aload #4
      //   42: getfield target : Landroid/view/View;
      //   45: aload_1
      //   46: if_acmpne -> 63
      //   49: aload_0
      //   50: getfield mViewRects : Ljava/util/ArrayList;
      //   53: iload_3
      //   54: invokevirtual remove : (I)Ljava/lang/Object;
      //   57: pop
      //   58: aload #4
      //   60: invokevirtual recycle : ()V
      //   63: iload_3
      //   64: istore_2
      //   65: goto -> 19
      //   68: aload_0
      //   69: getfield mPosted : Z
      //   72: ifeq -> 113
      //   75: aload_0
      //   76: getfield mViews : Ljava/util/ArrayList;
      //   79: invokevirtual isEmpty : ()Z
      //   82: ifeq -> 113
      //   85: aload_0
      //   86: getfield mViewRects : Ljava/util/ArrayList;
      //   89: invokevirtual isEmpty : ()Z
      //   92: ifeq -> 113
      //   95: aload_0
      //   96: getfield this$0 : Landroid/view/ViewRootImpl;
      //   99: getfield mChoreographer : Landroid/view/Choreographer;
      //   102: iconst_1
      //   103: aload_0
      //   104: aconst_null
      //   105: invokevirtual removeCallbacks : (ILjava/lang/Runnable;Ljava/lang/Object;)V
      //   108: aload_0
      //   109: iconst_0
      //   110: putfield mPosted : Z
      //   113: aload_0
      //   114: monitorexit
      //   115: return
      //   116: astore_1
      //   117: aload_0
      //   118: monitorexit
      //   119: aload_1
      //   120: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #8917	-> 0
      //   #8918	-> 2
      //   #8920	-> 11
      //   #8921	-> 27
      //   #8922	-> 40
      //   #8923	-> 49
      //   #8924	-> 58
      //   #8926	-> 63
      //   #8928	-> 68
      //   #8929	-> 95
      //   #8930	-> 108
      //   #8932	-> 113
      //   #8933	-> 115
      //   #8932	-> 116
      // Exception table:
      //   from	to	target	type
      //   2	11	116	finally
      //   11	19	116	finally
      //   27	40	116	finally
      //   40	49	116	finally
      //   49	58	116	finally
      //   58	63	116	finally
      //   68	95	116	finally
      //   95	108	116	finally
      //   108	113	116	finally
      //   113	115	116	finally
      //   117	119	116	finally
    }
    
    public void run() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: iconst_0
      //   4: putfield mPosted : Z
      //   7: aload_0
      //   8: getfield mViews : Ljava/util/ArrayList;
      //   11: invokevirtual size : ()I
      //   14: istore_1
      //   15: iload_1
      //   16: ifeq -> 63
      //   19: aload_0
      //   20: getfield mViews : Ljava/util/ArrayList;
      //   23: astore_2
      //   24: aload_0
      //   25: getfield mTempViews : [Landroid/view/View;
      //   28: ifnull -> 39
      //   31: aload_0
      //   32: getfield mTempViews : [Landroid/view/View;
      //   35: astore_3
      //   36: goto -> 44
      //   39: iload_1
      //   40: anewarray android/view/View
      //   43: astore_3
      //   44: aload_0
      //   45: aload_2
      //   46: aload_3
      //   47: invokevirtual toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
      //   50: checkcast [Landroid/view/View;
      //   53: putfield mTempViews : [Landroid/view/View;
      //   56: aload_0
      //   57: getfield mViews : Ljava/util/ArrayList;
      //   60: invokevirtual clear : ()V
      //   63: aload_0
      //   64: getfield mViewRects : Ljava/util/ArrayList;
      //   67: invokevirtual size : ()I
      //   70: istore #4
      //   72: iload #4
      //   74: ifeq -> 122
      //   77: aload_0
      //   78: getfield mViewRects : Ljava/util/ArrayList;
      //   81: astore_2
      //   82: aload_0
      //   83: getfield mTempViewRects : [Landroid/view/View$AttachInfo$InvalidateInfo;
      //   86: ifnull -> 97
      //   89: aload_0
      //   90: getfield mTempViewRects : [Landroid/view/View$AttachInfo$InvalidateInfo;
      //   93: astore_3
      //   94: goto -> 103
      //   97: iload #4
      //   99: anewarray android/view/View$AttachInfo$InvalidateInfo
      //   102: astore_3
      //   103: aload_0
      //   104: aload_2
      //   105: aload_3
      //   106: invokevirtual toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
      //   109: checkcast [Landroid/view/View$AttachInfo$InvalidateInfo;
      //   112: putfield mTempViewRects : [Landroid/view/View$AttachInfo$InvalidateInfo;
      //   115: aload_0
      //   116: getfield mViewRects : Ljava/util/ArrayList;
      //   119: invokevirtual clear : ()V
      //   122: aload_0
      //   123: monitorexit
      //   124: iconst_0
      //   125: istore #5
      //   127: iload #5
      //   129: iload_1
      //   130: if_icmpge -> 157
      //   133: aload_0
      //   134: getfield mTempViews : [Landroid/view/View;
      //   137: iload #5
      //   139: aaload
      //   140: invokevirtual invalidate : ()V
      //   143: aload_0
      //   144: getfield mTempViews : [Landroid/view/View;
      //   147: iload #5
      //   149: aconst_null
      //   150: aastore
      //   151: iinc #5, 1
      //   154: goto -> 127
      //   157: iconst_0
      //   158: istore #5
      //   160: iload #5
      //   162: iload #4
      //   164: if_icmpge -> 208
      //   167: aload_0
      //   168: getfield mTempViewRects : [Landroid/view/View$AttachInfo$InvalidateInfo;
      //   171: iload #5
      //   173: aaload
      //   174: astore_3
      //   175: aload_3
      //   176: getfield target : Landroid/view/View;
      //   179: aload_3
      //   180: getfield left : I
      //   183: aload_3
      //   184: getfield top : I
      //   187: aload_3
      //   188: getfield right : I
      //   191: aload_3
      //   192: getfield bottom : I
      //   195: invokevirtual invalidate : (IIII)V
      //   198: aload_3
      //   199: invokevirtual recycle : ()V
      //   202: iinc #5, 1
      //   205: goto -> 160
      //   208: return
      //   209: astore_3
      //   210: aload_0
      //   211: monitorexit
      //   212: aload_3
      //   213: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #8939	-> 0
      //   #8940	-> 2
      //   #8942	-> 7
      //   #8943	-> 15
      //   #8944	-> 19
      //   #8945	-> 31
      //   #8944	-> 44
      //   #8946	-> 56
      //   #8949	-> 63
      //   #8950	-> 72
      //   #8951	-> 77
      //   #8952	-> 89
      //   #8951	-> 103
      //   #8953	-> 115
      //   #8955	-> 122
      //   #8957	-> 124
      //   #8958	-> 133
      //   #8959	-> 143
      //   #8957	-> 151
      //   #8962	-> 157
      //   #8963	-> 167
      //   #8964	-> 175
      //   #8965	-> 198
      //   #8962	-> 202
      //   #8967	-> 208
      //   #8955	-> 209
      // Exception table:
      //   from	to	target	type
      //   2	7	209	finally
      //   7	15	209	finally
      //   19	31	209	finally
      //   31	36	209	finally
      //   39	44	209	finally
      //   44	56	209	finally
      //   56	63	209	finally
      //   63	72	209	finally
      //   77	89	209	finally
      //   89	94	209	finally
      //   97	103	209	finally
      //   103	115	209	finally
      //   115	122	209	finally
      //   122	124	209	finally
      //   210	212	209	finally
    }
    
    private void postIfNeededLocked() {
      if (!this.mPosted) {
        ViewRootImpl.this.mChoreographer.postCallback(1, this, null);
        this.mPosted = true;
      } 
    }
  }
  
  public void dispatchInvalidateDelayed(View paramView, long paramLong) {
    Message message = this.mHandler.obtainMessage(1, paramView);
    this.mHandler.sendMessageDelayed(message, paramLong);
  }
  
  public void dispatchInvalidateRectDelayed(View.AttachInfo.InvalidateInfo paramInvalidateInfo, long paramLong) {
    Message message = this.mHandler.obtainMessage(2, paramInvalidateInfo);
    this.mHandler.sendMessageDelayed(message, paramLong);
  }
  
  public void dispatchInvalidateOnAnimation(View paramView) {
    this.mInvalidateOnAnimationRunnable.addView(paramView);
  }
  
  public void dispatchInvalidateRectOnAnimation(View.AttachInfo.InvalidateInfo paramInvalidateInfo) {
    this.mInvalidateOnAnimationRunnable.addViewRect(paramInvalidateInfo);
  }
  
  public void cancelInvalidate(View paramView) {
    this.mHandler.removeMessages(1, paramView);
    this.mHandler.removeMessages(2, paramView);
    this.mInvalidateOnAnimationRunnable.removeView(paramView);
  }
  
  public void dispatchInputEvent(InputEvent paramInputEvent) {
    dispatchInputEvent(paramInputEvent, null);
  }
  
  public void dispatchInputEvent(InputEvent paramInputEvent, InputEventReceiver paramInputEventReceiver) {
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramInputEvent;
    someArgs.arg2 = paramInputEventReceiver;
    Message message = this.mHandler.obtainMessage(7, someArgs);
    message.setAsynchronous(true);
    this.mHandler.sendMessage(message);
  }
  
  public void synthesizeInputEvent(InputEvent paramInputEvent) {
    Message message = this.mHandler.obtainMessage(24, paramInputEvent);
    message.setAsynchronous(true);
    this.mHandler.sendMessage(message);
  }
  
  public void dispatchKeyFromIme(KeyEvent paramKeyEvent) {
    Message message = this.mHandler.obtainMessage(11, paramKeyEvent);
    message.setAsynchronous(true);
    this.mHandler.sendMessage(message);
  }
  
  public void dispatchKeyFromAutofill(KeyEvent paramKeyEvent) {
    Message message = this.mHandler.obtainMessage(12, paramKeyEvent);
    message.setAsynchronous(true);
    this.mHandler.sendMessage(message);
  }
  
  public void dispatchUnhandledInputEvent(InputEvent paramInputEvent) {
    InputEvent inputEvent = paramInputEvent;
    if (paramInputEvent instanceof MotionEvent)
      inputEvent = MotionEvent.obtain((MotionEvent)paramInputEvent); 
    synthesizeInputEvent(inputEvent);
  }
  
  public void dispatchAppVisibility(boolean paramBoolean) {
    Message message = this.mHandler.obtainMessage(8);
    message.arg1 = paramBoolean;
    this.mHandler.sendMessage(message);
  }
  
  public void dispatchGetNewSurface() {
    Message message = this.mHandler.obtainMessage(9);
    this.mHandler.sendMessage(message);
  }
  
  public void dispatchLocationInParentDisplayChanged(Point paramPoint) {
    ViewRootHandler viewRootHandler = this.mHandler;
    int i = paramPoint.x, j = paramPoint.y;
    Message message = viewRootHandler.obtainMessage(33, i, j);
    this.mHandler.sendMessage(message);
  }
  
  public void windowFocusChanged(boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_1
    //   4: putfield mWindowFocusChanged : Z
    //   7: aload_0
    //   8: iload_1
    //   9: putfield mUpcomingWindowFocus : Z
    //   12: aload_0
    //   13: iload_2
    //   14: putfield mUpcomingInTouchMode : Z
    //   17: aload_0
    //   18: monitorexit
    //   19: invokestatic obtain : ()Landroid/os/Message;
    //   22: astore_3
    //   23: aload_3
    //   24: bipush #6
    //   26: putfield what : I
    //   29: aload_0
    //   30: getfield mHandler : Landroid/view/ViewRootImpl$ViewRootHandler;
    //   33: aload_3
    //   34: invokevirtual sendMessage : (Landroid/os/Message;)Z
    //   37: pop
    //   38: return
    //   39: astore_3
    //   40: aload_0
    //   41: monitorexit
    //   42: aload_3
    //   43: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #9078	-> 0
    //   #9079	-> 2
    //   #9080	-> 7
    //   #9081	-> 12
    //   #9082	-> 17
    //   #9083	-> 19
    //   #9084	-> 23
    //   #9085	-> 29
    //   #9086	-> 38
    //   #9082	-> 39
    // Exception table:
    //   from	to	target	type
    //   2	7	39	finally
    //   7	12	39	finally
    //   12	17	39	finally
    //   17	19	39	finally
    //   40	42	39	finally
  }
  
  public void dispatchWindowShown() {
    this.mHandler.sendEmptyMessage(25);
  }
  
  public void dispatchCloseSystemDialogs(String paramString) {
    Message message = Message.obtain();
    message.what = 14;
    message.obj = paramString;
    this.mHandler.sendMessage(message);
  }
  
  public void dispatchDragEvent(DragEvent paramDragEvent) {
    byte b;
    if (paramDragEvent.getAction() == 2) {
      b = 16;
      this.mHandler.removeMessages(16);
    } else {
      b = 15;
    } 
    Message message = this.mHandler.obtainMessage(b, paramDragEvent);
    this.mHandler.sendMessage(message);
  }
  
  public void updatePointerIcon(float paramFloat1, float paramFloat2) {
    this.mHandler.removeMessages(27);
    long l = SystemClock.uptimeMillis();
    MotionEvent motionEvent = MotionEvent.obtain(0L, l, 7, paramFloat1, paramFloat2, 0);
    Message message = this.mHandler.obtainMessage(27, motionEvent);
    this.mHandler.sendMessage(message);
  }
  
  public void dispatchSystemUiVisibilityChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    SystemUiVisibilityInfo systemUiVisibilityInfo = new SystemUiVisibilityInfo();
    systemUiVisibilityInfo.seq = paramInt1;
    systemUiVisibilityInfo.globalVisibility = paramInt2;
    systemUiVisibilityInfo.localValue = paramInt3;
    systemUiVisibilityInfo.localChanges = paramInt4;
    ViewRootHandler viewRootHandler = this.mHandler;
    viewRootHandler.sendMessage(viewRootHandler.obtainMessage(17, systemUiVisibilityInfo));
  }
  
  public void dispatchCheckFocus() {
    if (!this.mHandler.hasMessages(13))
      this.mHandler.sendEmptyMessage(13); 
  }
  
  public void dispatchRequestKeyboardShortcuts(IResultReceiver paramIResultReceiver, int paramInt) {
    Message message = this.mHandler.obtainMessage(26, paramInt, 0, paramIResultReceiver);
    message.sendToTarget();
  }
  
  public void dispatchPointerCaptureChanged(boolean paramBoolean) {
    this.mHandler.removeMessages(28);
    Message message = this.mHandler.obtainMessage(28);
    message.arg1 = paramBoolean;
    this.mHandler.sendMessage(message);
  }
  
  private void postSendWindowContentChangedCallback(View paramView, int paramInt) {
    if (this.mSendWindowContentChangedAccessibilityEvent == null)
      this.mSendWindowContentChangedAccessibilityEvent = new SendWindowContentChangedAccessibilityEvent(); 
    this.mSendWindowContentChangedAccessibilityEvent.runOrPost(paramView, paramInt);
  }
  
  private void removeSendWindowContentChangedCallback() {
    SendWindowContentChangedAccessibilityEvent sendWindowContentChangedAccessibilityEvent = this.mSendWindowContentChangedAccessibilityEvent;
    if (sendWindowContentChangedAccessibilityEvent != null)
      this.mHandler.removeCallbacks(sendWindowContentChangedAccessibilityEvent); 
  }
  
  public boolean showContextMenuForChild(View paramView) {
    return false;
  }
  
  public boolean showContextMenuForChild(View paramView, float paramFloat1, float paramFloat2) {
    return false;
  }
  
  public ActionMode startActionModeForChild(View paramView, ActionMode.Callback paramCallback) {
    return null;
  }
  
  public ActionMode startActionModeForChild(View paramView, ActionMode.Callback paramCallback, int paramInt) {
    return null;
  }
  
  public void createContextMenu(ContextMenu paramContextMenu) {}
  
  public void childDrawableStateChanged(View paramView) {}
  
  public boolean requestSendAccessibilityEvent(View paramView, AccessibilityEvent paramAccessibilityEvent) {
    if (this.mView == null || this.mStopped || this.mPausedForTransition)
      return false; 
    if (paramAccessibilityEvent.getEventType() != 2048) {
      SendWindowContentChangedAccessibilityEvent sendWindowContentChangedAccessibilityEvent = this.mSendWindowContentChangedAccessibilityEvent;
      if (sendWindowContentChangedAccessibilityEvent != null && sendWindowContentChangedAccessibilityEvent.mSource != null)
        this.mSendWindowContentChangedAccessibilityEvent.removeCallbacksAndRun(); 
    } 
    int i = paramAccessibilityEvent.getEventType();
    paramView = getSourceForAccessibilityEvent(paramAccessibilityEvent);
    if (i != 2048) {
      if (i != 32768) {
        if (i == 65536)
          if (paramView != null && paramView.getAccessibilityNodeProvider() != null)
            setAccessibilityFocus(null, null);  
      } else if (paramView != null) {
        AccessibilityNodeProvider accessibilityNodeProvider = paramView.getAccessibilityNodeProvider();
        if (accessibilityNodeProvider != null) {
          long l = paramAccessibilityEvent.getSourceNodeId();
          i = AccessibilityNodeInfo.getVirtualDescendantId(l);
          AccessibilityNodeInfo accessibilityNodeInfo = accessibilityNodeProvider.createAccessibilityNodeInfo(i);
          setAccessibilityFocus(paramView, accessibilityNodeInfo);
        } 
      } 
    } else {
      handleWindowContentChangedEvent(paramAccessibilityEvent);
    } 
    this.mAccessibilityManager.sendAccessibilityEvent(paramAccessibilityEvent);
    return true;
  }
  
  private View getSourceForAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    long l = paramAccessibilityEvent.getSourceNodeId();
    int i = AccessibilityNodeInfo.getAccessibilityViewId(l);
    return AccessibilityNodeIdManager.getInstance().findView(i);
  }
  
  private void handleWindowContentChangedEvent(AccessibilityEvent paramAccessibilityEvent) {
    View view2 = this.mAccessibilityFocusedHost;
    if (view2 == null || this.mAccessibilityFocusedVirtualView == null)
      return; 
    AccessibilityNodeProvider accessibilityNodeProvider = view2.getAccessibilityNodeProvider();
    if (accessibilityNodeProvider == null) {
      this.mAccessibilityFocusedHost = null;
      this.mAccessibilityFocusedVirtualView = null;
      view2.clearAccessibilityFocusNoCallbacks(0);
      return;
    } 
    int i = paramAccessibilityEvent.getContentChangeTypes();
    if ((i & 0x1) == 0 && i != 0)
      return; 
    long l = paramAccessibilityEvent.getSourceNodeId();
    int j = AccessibilityNodeInfo.getAccessibilityViewId(l);
    i = 0;
    View view1 = this.mAccessibilityFocusedHost;
    while (view1 != null && i == 0) {
      if (j == view1.getAccessibilityViewId()) {
        i = 1;
        continue;
      } 
      ViewParent viewParent = view1.getParent();
      if (viewParent instanceof View) {
        View view = (View)viewParent;
        continue;
      } 
      viewParent = null;
    } 
    if (i == 0)
      return; 
    l = this.mAccessibilityFocusedVirtualView.getSourceNodeId();
    j = AccessibilityNodeInfo.getVirtualDescendantId(l);
    Rect rect = this.mTempRect;
    this.mAccessibilityFocusedVirtualView.getBoundsInScreen(rect);
    AccessibilityNodeInfo accessibilityNodeInfo = accessibilityNodeProvider.createAccessibilityNodeInfo(j);
    if (accessibilityNodeInfo == null) {
      this.mAccessibilityFocusedHost = null;
      view2.clearAccessibilityFocusNoCallbacks(0);
      AccessibilityNodeInfo.AccessibilityAction accessibilityAction = AccessibilityNodeInfo.AccessibilityAction.ACTION_CLEAR_ACCESSIBILITY_FOCUS;
      i = accessibilityAction.getId();
      accessibilityNodeProvider.performAction(j, i, null);
      invalidateRectOnScreen(rect);
    } else {
      Rect rect1 = accessibilityNodeInfo.getBoundsInScreen();
      if (!rect.equals(rect1)) {
        rect.union(rect1);
        invalidateRectOnScreen(rect);
      } 
    } 
  }
  
  public void notifySubtreeAccessibilityStateChanged(View paramView1, View paramView2, int paramInt) {
    postSendWindowContentChangedCallback((View)Preconditions.checkNotNull(paramView2), paramInt);
  }
  
  public boolean canResolveLayoutDirection() {
    return true;
  }
  
  public boolean isLayoutDirectionResolved() {
    return true;
  }
  
  public int getLayoutDirection() {
    return 0;
  }
  
  public boolean canResolveTextDirection() {
    return true;
  }
  
  public boolean isTextDirectionResolved() {
    return true;
  }
  
  public int getTextDirection() {
    return 1;
  }
  
  public boolean canResolveTextAlignment() {
    return true;
  }
  
  public boolean isTextAlignmentResolved() {
    return true;
  }
  
  public int getTextAlignment() {
    return 1;
  }
  
  private View getCommonPredecessor(View paramView1, View paramView2) {
    if (this.mTempHashSet == null)
      this.mTempHashSet = new HashSet<>(); 
    HashSet<View> hashSet = this.mTempHashSet;
    hashSet.clear();
    while (paramView1 != null) {
      hashSet.add(paramView1);
      ViewParent viewParent = paramView1.mParent;
      if (viewParent instanceof View) {
        View view = (View)viewParent;
        continue;
      } 
      viewParent = null;
    } 
    paramView1 = paramView2;
    while (paramView1 != null) {
      if (hashSet.contains(paramView1)) {
        hashSet.clear();
        return paramView1;
      } 
      ViewParent viewParent = paramView1.mParent;
      if (viewParent instanceof View) {
        View view = (View)viewParent;
        continue;
      } 
      viewParent = null;
    } 
    hashSet.clear();
    return null;
  }
  
  void checkThread() {
    if (this.mThread == Thread.currentThread())
      return; 
    throw new CalledFromWrongThreadException("Only the original thread that created a view hierarchy can touch its views.");
  }
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean) {}
  
  public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean) {
    if (paramRect == null)
      return scrollToRectOrFocus(null, paramBoolean); 
    int i = paramView.getLeft(), j = paramView.getScrollX();
    int k = paramView.getTop(), m = paramView.getScrollY();
    paramRect.offset(i - j, k - m);
    paramBoolean = scrollToRectOrFocus(paramRect, paramBoolean);
    this.mTempRect.set(paramRect);
    this.mTempRect.offset(0, -this.mCurScrollY);
    this.mTempRect.offset(this.mAttachInfo.mWindowLeft, this.mAttachInfo.mWindowTop);
    try {
      this.mWindowSession.onRectangleOnScreenRequested((IBinder)this.mWindow, this.mTempRect);
    } catch (RemoteException remoteException) {}
    return paramBoolean;
  }
  
  public void childHasTransientStateChanged(View paramView, boolean paramBoolean) {}
  
  public boolean onStartNestedScroll(View paramView1, View paramView2, int paramInt) {
    return false;
  }
  
  public void onStopNestedScroll(View paramView) {}
  
  public void onNestedScrollAccepted(View paramView1, View paramView2, int paramInt) {}
  
  public void onNestedScroll(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {}
  
  public void onNestedPreScroll(View paramView, int paramInt1, int paramInt2, int[] paramArrayOfint) {}
  
  public boolean onNestedFling(View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean) {
    return false;
  }
  
  public boolean onNestedPreFling(View paramView, float paramFloat1, float paramFloat2) {
    return false;
  }
  
  public boolean onNestedPrePerformAccessibilityAction(View paramView, int paramInt, Bundle paramBundle) {
    return false;
  }
  
  public void addScrollCaptureCallback(ScrollCaptureCallback paramScrollCaptureCallback) {
    if (this.mRootScrollCaptureCallbacks == null)
      this.mRootScrollCaptureCallbacks = new HashSet<>(); 
    this.mRootScrollCaptureCallbacks.add(paramScrollCaptureCallback);
  }
  
  public void removeScrollCaptureCallback(ScrollCaptureCallback paramScrollCaptureCallback) {
    HashSet<ScrollCaptureCallback> hashSet = this.mRootScrollCaptureCallbacks;
    if (hashSet != null) {
      hashSet.remove(paramScrollCaptureCallback);
      if (this.mRootScrollCaptureCallbacks.isEmpty())
        this.mRootScrollCaptureCallbacks = null; 
    } 
  }
  
  public void dispatchScrollCaptureRequest(IScrollCaptureController paramIScrollCaptureController) {
    this.mHandler.obtainMessage(36, paramIScrollCaptureController).sendToTarget();
  }
  
  private void collectRootScrollCaptureTargets(Queue<ScrollCaptureTarget> paramQueue) {
    for (ScrollCaptureCallback scrollCaptureCallback : this.mRootScrollCaptureCallbacks) {
      Point point = new Point(this.mView.getLeft(), this.mView.getTop());
      Rect rect = new Rect(0, 0, this.mView.getWidth(), this.mView.getHeight());
      paramQueue.add(new ScrollCaptureTarget(this.mView, rect, point, scrollCaptureCallback));
    } 
  }
  
  private void handleScrollCaptureRequest(IScrollCaptureController paramIScrollCaptureController) {
    LinkedList<ScrollCaptureTarget> linkedList = new LinkedList();
    collectRootScrollCaptureTargets(linkedList);
    View view = getView();
    Point point = new Point();
    Rect rect = new Rect(0, 0, view.getWidth(), view.getHeight());
    getChildVisibleRect(view, rect, point);
    view.dispatchScrollCaptureSearch(rect, point, linkedList);
    if (linkedList.isEmpty()) {
      dispatchScrollCaptureSearchResult(paramIScrollCaptureController, null);
      return;
    } 
    ScrollCaptureTargetResolver scrollCaptureTargetResolver = new ScrollCaptureTargetResolver(linkedList);
    scrollCaptureTargetResolver.start(this.mHandler, 1000L, new _$$Lambda$ViewRootImpl$q8OEhO4Gpuq2GiPXjd5gNL6TlLg(this, paramIScrollCaptureController));
  }
  
  private void dispatchScrollCaptureSearchResult(IScrollCaptureController paramIScrollCaptureController, ScrollCaptureTarget paramScrollCaptureTarget) {
    if (paramScrollCaptureTarget == null) {
      try {
        paramIScrollCaptureController.onClientUnavailable();
      } catch (RemoteException remoteException) {}
      return;
    } 
    ScrollCaptureClient scrollCaptureClient = new ScrollCaptureClient(paramScrollCaptureTarget, (IScrollCaptureController)remoteException);
    try {
      Rect rect = paramScrollCaptureTarget.getScrollBounds();
      Point point = paramScrollCaptureTarget.getPositionInWindow();
      remoteException.onClientConnected(scrollCaptureClient, rect, point);
    } catch (RemoteException remoteException1) {
      this.mScrollCaptureClient.disconnect();
      this.mScrollCaptureClient = null;
    } 
  }
  
  private void reportNextDraw() {
    if (!this.mReportNextDraw)
      drawPending(); 
    this.mReportNextDraw = true;
  }
  
  public void setReportNextDraw() {
    reportNextDraw();
    invalidate();
  }
  
  void changeCanvasOpacity(boolean paramBoolean) {
    boolean bool;
    String str = this.mTag;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("changeCanvasOpacity: opaque=");
    stringBuilder.append(paramBoolean);
    Log.d(str, stringBuilder.toString());
    if ((this.mView.mPrivateFlags & 0x200) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (this.mAttachInfo.mThreadedRenderer != null)
      this.mAttachInfo.mThreadedRenderer.setOpaque(paramBoolean & bool); 
  }
  
  public boolean dispatchUnhandledKeyEvent(KeyEvent paramKeyEvent) {
    return this.mUnhandledKeyManager.dispatch(this.mView, paramKeyEvent);
  }
  
  class TakenSurfaceHolder extends BaseSurfaceHolder {
    final ViewRootImpl this$0;
    
    public boolean onAllowLockCanvas() {
      return ViewRootImpl.this.mDrawingAllowed;
    }
    
    public void onRelayoutContainer() {}
    
    public void setFormat(int param1Int) {
      ((RootViewSurfaceTaker)ViewRootImpl.this.mView).setSurfaceFormat(param1Int);
    }
    
    public void setType(int param1Int) {
      ((RootViewSurfaceTaker)ViewRootImpl.this.mView).setSurfaceType(param1Int);
    }
    
    public void onUpdateSurface() {
      throw new IllegalStateException("Shouldn't be here");
    }
    
    public boolean isCreating() {
      return ViewRootImpl.this.mIsCreating;
    }
    
    public void setFixedSize(int param1Int1, int param1Int2) {
      throw new UnsupportedOperationException("Currently only support sizing from layout");
    }
    
    public void setKeepScreenOn(boolean param1Boolean) {
      ((RootViewSurfaceTaker)ViewRootImpl.this.mView).setSurfaceKeepScreenOn(param1Boolean);
    }
  }
  
  class W extends IWindow.Stub {
    final WeakReference<ViewRootImpl> mViewAncestor;
    
    private final IWindowSession mWindowSession;
    
    W(ViewRootImpl this$0) {
      this.mViewAncestor = new WeakReference<>(this$0);
      this.mWindowSession = this$0.mWindowSession;
    }
    
    public void resized(Rect param1Rect1, Rect param1Rect2, Rect param1Rect3, Rect param1Rect4, boolean param1Boolean1, MergedConfiguration param1MergedConfiguration, Rect param1Rect5, boolean param1Boolean2, boolean param1Boolean3, int param1Int, DisplayCutout.ParcelableWrapper param1ParcelableWrapper) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchResized(param1Rect1, param1Rect2, param1Rect3, param1Rect4, param1Boolean1, param1MergedConfiguration, param1Rect5, param1Boolean2, param1Boolean3, param1Int, param1ParcelableWrapper); 
    }
    
    public void locationInParentDisplayChanged(Point param1Point) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchLocationInParentDisplayChanged(param1Point); 
    }
    
    public void insetsChanged(InsetsState param1InsetsState) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchInsetsChanged(param1InsetsState); 
    }
    
    public void insetsControlChanged(InsetsState param1InsetsState, InsetsSourceControl[] param1ArrayOfInsetsSourceControl) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchInsetsControlChanged(param1InsetsState, param1ArrayOfInsetsSourceControl); 
    }
    
    public void showInsets(int param1Int, boolean param1Boolean) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.showInsets(param1Int, param1Boolean); 
    }
    
    public void hideInsets(int param1Int, boolean param1Boolean) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.hideInsets(param1Int, param1Boolean); 
    }
    
    public void moved(int param1Int1, int param1Int2) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchMoved(param1Int1, param1Int2); 
    }
    
    public void dispatchAppVisibility(boolean param1Boolean) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchAppVisibility(param1Boolean); 
    }
    
    public void dispatchGetNewSurface() {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchGetNewSurface(); 
    }
    
    public void windowFocusChanged(boolean param1Boolean1, boolean param1Boolean2) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.windowFocusChanged(param1Boolean1, param1Boolean2); 
    }
    
    private static int checkCallingPermission(String param1String) {
      try {
        IActivityManager iActivityManager = ActivityManager.getService();
        null = Binder.getCallingPid();
        int i = Binder.getCallingUid();
        return iActivityManager.checkPermission(param1String, null, i);
      } catch (RemoteException remoteException) {
        return -1;
      } 
    }
    
    public void executeCommand(String param1String1, String param1String2, ParcelFileDescriptor param1ParcelFileDescriptor) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null) {
        View view = viewRootImpl.mView;
        if (view != null)
          if (checkCallingPermission("android.permission.DUMP") == 0) {
            ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream1 = null;
            ViewRootImpl viewRootImpl1 = null;
            viewRootImpl = viewRootImpl1;
            ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream2 = autoCloseOutputStream1;
            try {
              ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream5 = new ParcelFileDescriptor.AutoCloseOutputStream();
              viewRootImpl = viewRootImpl1;
              autoCloseOutputStream2 = autoCloseOutputStream1;
              this(param1ParcelFileDescriptor);
              ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream3 = autoCloseOutputStream5;
              ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream4 = autoCloseOutputStream3;
              autoCloseOutputStream2 = autoCloseOutputStream3;
              ViewDebug.dispatchCommand(view, param1String1, param1String2, (OutputStream)autoCloseOutputStream3);
              try {
                autoCloseOutputStream3.close();
              } catch (IOException iOException) {
                iOException.printStackTrace();
              } 
            } catch (IOException iOException) {
              ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream = autoCloseOutputStream2;
              iOException.printStackTrace();
              if (autoCloseOutputStream2 != null)
                autoCloseOutputStream2.close(); 
            } finally {}
          } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Insufficient permissions to invoke executeCommand() from pid=");
            stringBuilder.append(Binder.getCallingPid());
            stringBuilder.append(", uid=");
            stringBuilder.append(Binder.getCallingUid());
            throw new SecurityException(stringBuilder.toString());
          }  
      } 
    }
    
    public void closeSystemDialogs(String param1String) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchCloseSystemDialogs(param1String); 
    }
    
    public void dispatchWallpaperOffsets(float param1Float1, float param1Float2, float param1Float3, float param1Float4, float param1Float5, boolean param1Boolean) {
      if (param1Boolean)
        try {
          this.mWindowSession.wallpaperOffsetsComplete(asBinder());
        } catch (RemoteException remoteException) {} 
    }
    
    public void dispatchWallpaperCommand(String param1String, int param1Int1, int param1Int2, int param1Int3, Bundle param1Bundle, boolean param1Boolean) {
      if (param1Boolean)
        try {
          this.mWindowSession.wallpaperCommandComplete(asBinder(), null);
        } catch (RemoteException remoteException) {} 
    }
    
    public void dispatchDragEvent(DragEvent param1DragEvent) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchDragEvent(param1DragEvent); 
    }
    
    public void updatePointerIcon(float param1Float1, float param1Float2) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.updatePointerIcon(param1Float1, param1Float2); 
    }
    
    public void dispatchSystemUiVisibilityChanged(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchSystemUiVisibilityChanged(param1Int1, param1Int2, param1Int3, param1Int4); 
    }
    
    public void dispatchWindowShown() {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchWindowShown(); 
    }
    
    public void requestAppKeyboardShortcuts(IResultReceiver param1IResultReceiver, int param1Int) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchRequestKeyboardShortcuts(param1IResultReceiver, param1Int); 
    }
    
    public void dispatchPointerCaptureChanged(boolean param1Boolean) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchPointerCaptureChanged(param1Boolean); 
    }
    
    public void requestScrollCapture(IScrollCaptureController param1IScrollCaptureController) {
      ViewRootImpl viewRootImpl = this.mViewAncestor.get();
      if (viewRootImpl != null)
        viewRootImpl.dispatchScrollCaptureRequest(param1IScrollCaptureController); 
    }
  }
  
  public static final class CalledFromWrongThreadException extends AndroidRuntimeException {
    public CalledFromWrongThreadException(String param1String) {
      super(param1String);
    }
  }
  
  static HandlerActionQueue getRunQueue() {
    HandlerActionQueue handlerActionQueue = sRunQueues.get();
    if (handlerActionQueue != null)
      return handlerActionQueue; 
    handlerActionQueue = new HandlerActionQueue();
    sRunQueues.set(handlerActionQueue);
    return handlerActionQueue;
  }
  
  private void startDragResizing(Rect paramRect1, boolean paramBoolean, Rect paramRect2, Rect paramRect3, int paramInt) {
    if (!this.mDragResizing) {
      this.mDragResizing = true;
      if (this.mUseMTRenderer)
        for (int i = this.mWindowCallbacks.size() - 1; i >= 0; i--)
          ((WindowCallbacks)this.mWindowCallbacks.get(i)).onWindowDragResizeStart(paramRect1, paramBoolean, paramRect2, paramRect3, paramInt);  
      this.mFullRedrawNeeded = true;
    } 
  }
  
  private void endDragResizing() {
    if (this.mDragResizing) {
      this.mDragResizing = false;
      if (this.mUseMTRenderer)
        for (int i = this.mWindowCallbacks.size() - 1; i >= 0; i--)
          ((WindowCallbacks)this.mWindowCallbacks.get(i)).onWindowDragResizeEnd();  
      this.mFullRedrawNeeded = true;
    } 
  }
  
  private boolean updateContentDrawBounds() {
    boolean bool1 = false, bool2 = false;
    boolean bool3 = this.mUseMTRenderer;
    boolean bool = true;
    if (bool3) {
      int i = this.mWindowCallbacks.size() - 1;
      while (true) {
        bool1 = bool2;
        if (i >= 0) {
          ArrayList<WindowCallbacks> arrayList = this.mWindowCallbacks;
          bool2 |= ((WindowCallbacks)arrayList.get(i)).onContentDrawn(this.mWindowAttributes.surfaceInsets.left, this.mWindowAttributes.surfaceInsets.top, this.mWidth, this.mHeight);
          i--;
          continue;
        } 
        break;
      } 
    } 
    if (this.mDragResizing && this.mReportNextDraw) {
      bool2 = bool;
    } else {
      bool2 = false;
    } 
    return bool1 | bool2;
  }
  
  private void requestDrawWindow() {
    if (!this.mUseMTRenderer)
      return; 
    this.mWindowDrawCountDown = new CountDownLatch(this.mWindowCallbacks.size());
    for (int i = this.mWindowCallbacks.size() - 1; i >= 0; i--) {
      boolean bool;
      WindowCallbacks windowCallbacks = this.mWindowCallbacks.get(i);
      if (this.mReportNextDraw || (this.mResizeMode == 1 && this.mDragResizing)) {
        bool = true;
      } else {
        bool = false;
      } 
      windowCallbacks.onRequestDraw(bool);
    } 
  }
  
  public void reportActivityRelaunched() {
    this.mActivityRelaunched = true;
  }
  
  public SurfaceControl getSurfaceControl() {
    return this.mSurfaceControl;
  }
  
  public IBinder getInputToken() {
    WindowInputEventReceiver windowInputEventReceiver = this.mInputEventReceiver;
    if (windowInputEventReceiver == null)
      return null; 
    return windowInputEventReceiver.getToken();
  }
  
  public IBinder getWindowToken() {
    return this.mAttachInfo.mWindowToken;
  }
  
  final class AccessibilityInteractionConnectionManager implements AccessibilityManager.AccessibilityStateChangeListener {
    final ViewRootImpl this$0;
    
    public void onAccessibilityStateChanged(boolean param1Boolean) {
      if (param1Boolean) {
        ensureConnection();
        try {
          if (ViewRootImpl.this.mAttachInfo.mHasWindowFocus && ViewRootImpl.this.mView != null) {
            ViewRootImpl.this.mView.sendAccessibilityEvent(32);
            View view = ViewRootImpl.this.mView.findFocus();
            if (view != null && view != ViewRootImpl.this.mView)
              view.sendAccessibilityEvent(8); 
          } 
        } catch (NullPointerException nullPointerException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("onAccessibilityStateChanged catch NullPointerException ");
          stringBuilder.append(nullPointerException);
          Slog.w("ViewRootImpl", stringBuilder.toString());
        } 
      } else {
        ensureNoConnection();
        ViewRootImpl.this.mHandler.obtainMessage(21).sendToTarget();
      } 
    }
    
    public void ensureConnection() {
      boolean bool;
      if (ViewRootImpl.this.mAttachInfo.mAccessibilityWindowId != -1) {
        bool = true;
      } else {
        bool = false;
      } 
      if (!bool) {
        View.AttachInfo attachInfo = ViewRootImpl.this.mAttachInfo;
        AccessibilityManager accessibilityManager = ViewRootImpl.this.mAccessibilityManager;
        ViewRootImpl.W w = ViewRootImpl.this.mWindow;
        IBinder iBinder = ViewRootImpl.this.mLeashToken;
        Context context = ViewRootImpl.this.mContext;
        String str = context.getPackageName();
        ViewRootImpl.AccessibilityInteractionConnection accessibilityInteractionConnection = new ViewRootImpl.AccessibilityInteractionConnection(ViewRootImpl.this);
        attachInfo.mAccessibilityWindowId = accessibilityManager.addAccessibilityInteractionConnection(w, iBinder, str, accessibilityInteractionConnection);
      } 
    }
    
    public void ensureNoConnection() {
      boolean bool;
      if (ViewRootImpl.this.mAttachInfo.mAccessibilityWindowId != -1) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool) {
        ViewRootImpl.this.mAttachInfo.mAccessibilityWindowId = -1;
        ViewRootImpl.this.mAccessibilityManager.removeAccessibilityInteractionConnection(ViewRootImpl.this.mWindow);
      } 
    }
  }
  
  final class HighContrastTextManager implements AccessibilityManager.HighTextContrastChangeListener {
    final ViewRootImpl this$0;
    
    HighContrastTextManager() {
      ThreadedRenderer.setHighContrastText(ViewRootImpl.this.mAccessibilityManager.isHighTextContrastEnabled());
    }
    
    public void onHighTextContrastStateChanged(boolean param1Boolean) {
      ThreadedRenderer.setHighContrastText(param1Boolean);
      ViewRootImpl.this.destroyHardwareResources();
      ViewRootImpl.this.invalidate();
    }
  }
  
  class AccessibilityInteractionConnection extends IAccessibilityInteractionConnection.Stub {
    private final WeakReference<ViewRootImpl> mViewRootImpl;
    
    AccessibilityInteractionConnection(ViewRootImpl this$0) {
      this.mViewRootImpl = new WeakReference<>(this$0);
    }
    
    public void findAccessibilityNodeInfoByAccessibilityId(long param1Long1, Region param1Region, int param1Int1, IAccessibilityInteractionConnectionCallback param1IAccessibilityInteractionConnectionCallback, int param1Int2, int param1Int3, long param1Long2, MagnificationSpec param1MagnificationSpec, Bundle param1Bundle) {
      ViewRootImpl viewRootImpl = this.mViewRootImpl.get();
      if (viewRootImpl != null && viewRootImpl.mView != null) {
        AccessibilityInteractionController accessibilityInteractionController = viewRootImpl.getAccessibilityInteractionController();
        accessibilityInteractionController.findAccessibilityNodeInfoByAccessibilityIdClientThread(param1Long1, param1Region, param1Int1, param1IAccessibilityInteractionConnectionCallback, param1Int2, param1Int3, param1Long2, param1MagnificationSpec, param1Bundle);
      } else {
        try {
          param1IAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfosResult(null, param1Int1);
        } catch (RemoteException remoteException) {}
      } 
    }
    
    public void performAccessibilityAction(long param1Long1, int param1Int1, Bundle param1Bundle, int param1Int2, IAccessibilityInteractionConnectionCallback param1IAccessibilityInteractionConnectionCallback, int param1Int3, int param1Int4, long param1Long2) {
      ViewRootImpl viewRootImpl = this.mViewRootImpl.get();
      if (viewRootImpl != null && viewRootImpl.mView != null) {
        AccessibilityInteractionController accessibilityInteractionController = viewRootImpl.getAccessibilityInteractionController();
        accessibilityInteractionController.performAccessibilityActionClientThread(param1Long1, param1Int1, param1Bundle, param1Int2, param1IAccessibilityInteractionConnectionCallback, param1Int3, param1Int4, param1Long2);
      } else {
        try {
          param1IAccessibilityInteractionConnectionCallback.setPerformAccessibilityActionResult(false, param1Int2);
        } catch (RemoteException remoteException) {}
      } 
    }
    
    public void findAccessibilityNodeInfosByViewId(long param1Long1, String param1String, Region param1Region, int param1Int1, IAccessibilityInteractionConnectionCallback param1IAccessibilityInteractionConnectionCallback, int param1Int2, int param1Int3, long param1Long2, MagnificationSpec param1MagnificationSpec) {
      ViewRootImpl viewRootImpl = this.mViewRootImpl.get();
      if (viewRootImpl != null && viewRootImpl.mView != null) {
        AccessibilityInteractionController accessibilityInteractionController = viewRootImpl.getAccessibilityInteractionController();
        accessibilityInteractionController.findAccessibilityNodeInfosByViewIdClientThread(param1Long1, param1String, param1Region, param1Int1, param1IAccessibilityInteractionConnectionCallback, param1Int2, param1Int3, param1Long2, param1MagnificationSpec);
      } else {
        try {
          param1IAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfoResult(null, param1Int1);
        } catch (RemoteException remoteException) {}
      } 
    }
    
    public void findAccessibilityNodeInfosByText(long param1Long1, String param1String, Region param1Region, int param1Int1, IAccessibilityInteractionConnectionCallback param1IAccessibilityInteractionConnectionCallback, int param1Int2, int param1Int3, long param1Long2, MagnificationSpec param1MagnificationSpec) {
      ViewRootImpl viewRootImpl = this.mViewRootImpl.get();
      if (viewRootImpl != null && viewRootImpl.mView != null) {
        AccessibilityInteractionController accessibilityInteractionController = viewRootImpl.getAccessibilityInteractionController();
        accessibilityInteractionController.findAccessibilityNodeInfosByTextClientThread(param1Long1, param1String, param1Region, param1Int1, param1IAccessibilityInteractionConnectionCallback, param1Int2, param1Int3, param1Long2, param1MagnificationSpec);
      } else {
        try {
          param1IAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfosResult(null, param1Int1);
        } catch (RemoteException remoteException) {}
      } 
    }
    
    public void findFocus(long param1Long1, int param1Int1, Region param1Region, int param1Int2, IAccessibilityInteractionConnectionCallback param1IAccessibilityInteractionConnectionCallback, int param1Int3, int param1Int4, long param1Long2, MagnificationSpec param1MagnificationSpec) {
      ViewRootImpl viewRootImpl = this.mViewRootImpl.get();
      if (viewRootImpl != null && viewRootImpl.mView != null) {
        AccessibilityInteractionController accessibilityInteractionController = viewRootImpl.getAccessibilityInteractionController();
        accessibilityInteractionController.findFocusClientThread(param1Long1, param1Int1, param1Region, param1Int2, param1IAccessibilityInteractionConnectionCallback, param1Int3, param1Int4, param1Long2, param1MagnificationSpec);
      } else {
        try {
          param1IAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfoResult(null, param1Int2);
        } catch (RemoteException remoteException) {}
      } 
    }
    
    public void focusSearch(long param1Long1, int param1Int1, Region param1Region, int param1Int2, IAccessibilityInteractionConnectionCallback param1IAccessibilityInteractionConnectionCallback, int param1Int3, int param1Int4, long param1Long2, MagnificationSpec param1MagnificationSpec) {
      ViewRootImpl viewRootImpl = this.mViewRootImpl.get();
      if (viewRootImpl != null && viewRootImpl.mView != null) {
        AccessibilityInteractionController accessibilityInteractionController = viewRootImpl.getAccessibilityInteractionController();
        accessibilityInteractionController.focusSearchClientThread(param1Long1, param1Int1, param1Region, param1Int2, param1IAccessibilityInteractionConnectionCallback, param1Int3, param1Int4, param1Long2, param1MagnificationSpec);
      } else {
        try {
          param1IAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfoResult(null, param1Int2);
        } catch (RemoteException remoteException) {}
      } 
    }
    
    public void clearAccessibilityFocus() {
      ViewRootImpl viewRootImpl = this.mViewRootImpl.get();
      if (viewRootImpl != null && viewRootImpl.mView != null) {
        AccessibilityInteractionController accessibilityInteractionController = viewRootImpl.getAccessibilityInteractionController();
        accessibilityInteractionController.clearAccessibilityFocusClientThread();
      } 
    }
    
    public void notifyOutsideTouch() {
      ViewRootImpl viewRootImpl = this.mViewRootImpl.get();
      if (viewRootImpl != null && viewRootImpl.mView != null) {
        AccessibilityInteractionController accessibilityInteractionController = viewRootImpl.getAccessibilityInteractionController();
        accessibilityInteractionController.notifyOutsideTouchClientThread();
      } 
    }
  }
  
  public IAccessibilityEmbeddedConnection getAccessibilityEmbeddedConnection() {
    if (this.mAccessibilityEmbeddedConnection == null)
      this.mAccessibilityEmbeddedConnection = new AccessibilityEmbeddedConnection(this); 
    return this.mAccessibilityEmbeddedConnection;
  }
  
  class SendWindowContentChangedAccessibilityEvent implements Runnable {
    private int mChangeTypes;
    
    public long mLastEventTimeMillis;
    
    public StackTraceElement[] mOrigin;
    
    public View mSource;
    
    final ViewRootImpl this$0;
    
    private SendWindowContentChangedAccessibilityEvent() {
      this.mChangeTypes = 0;
    }
    
    public void run() {
      View view = this.mSource;
      this.mSource = null;
      if (view == null) {
        Log.e("ViewRootImpl", "Accessibility content change has no source");
        return;
      } 
      if (AccessibilityManager.getInstance(ViewRootImpl.this.mContext).isEnabled()) {
        this.mLastEventTimeMillis = SystemClock.uptimeMillis();
        AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain();
        accessibilityEvent.setEventType(2048);
        accessibilityEvent.setContentChangeTypes(this.mChangeTypes);
        view.sendAccessibilityEventUnchecked(accessibilityEvent);
      } else {
        this.mLastEventTimeMillis = 0L;
      } 
      view.resetSubtreeAccessibilityStateChanged();
      this.mChangeTypes = 0;
    }
    
    public void runOrPost(View param1View, int param1Int) {
      if (ViewRootImpl.this.mHandler.getLooper() != Looper.myLooper()) {
        ViewRootImpl.CalledFromWrongThreadException calledFromWrongThreadException = new ViewRootImpl.CalledFromWrongThreadException("Only the original thread that created a view hierarchy can touch its views.");
        Log.e("ViewRootImpl", "Accessibility content change on non-UI thread. Future Android versions will throw an exception.", calledFromWrongThreadException);
        ViewRootImpl.this.mHandler.removeCallbacks(this);
        if (this.mSource != null)
          run(); 
      } 
      View view = this.mSource;
      if (view != null) {
        View view1 = ViewRootImpl.this.getCommonPredecessor(view, param1View);
        view = view1;
        if (view1 != null)
          view = view1.getSelfOrParentImportantForA11y(); 
        if (view != null)
          param1View = view; 
        this.mSource = param1View;
        this.mChangeTypes |= param1Int;
        return;
      } 
      this.mSource = param1View;
      this.mChangeTypes = param1Int;
      long l1 = SystemClock.uptimeMillis() - this.mLastEventTimeMillis;
      long l2 = ViewConfiguration.getSendRecurringAccessibilityEventsInterval();
      if (l1 >= l2) {
        removeCallbacksAndRun();
      } else {
        ViewRootImpl.this.mHandler.postDelayed(this, l2 - l1);
      } 
    }
    
    public void removeCallbacksAndRun() {
      ViewRootImpl.this.mHandler.removeCallbacks(this);
      run();
    }
  }
  
  class UnhandledKeyManager {
    private final SparseArray<WeakReference<View>> mCapturedKeys;
    
    private WeakReference<View> mCurrentReceiver;
    
    private boolean mDispatched;
    
    private UnhandledKeyManager() {
      this.mDispatched = true;
      this.mCapturedKeys = new SparseArray<>();
      this.mCurrentReceiver = null;
    }
    
    boolean dispatch(View param1View, KeyEvent param1KeyEvent) {
      boolean bool = this.mDispatched;
      boolean bool1 = false;
      if (bool)
        return false; 
      try {
        Trace.traceBegin(8L, "UnhandledKeyEvent dispatch");
        this.mDispatched = true;
        param1View = param1View.dispatchUnhandledKeyEvent(param1KeyEvent);
        if (param1KeyEvent.getAction() == 0) {
          int i = param1KeyEvent.getKeyCode();
          if (param1View != null && !KeyEvent.isModifierKey(i)) {
            SparseArray<WeakReference<View>> sparseArray = this.mCapturedKeys;
            WeakReference<View> weakReference = new WeakReference();
            this((T)param1View);
            sparseArray.put(i, weakReference);
          } 
        } 
        Trace.traceEnd(8L);
        return bool1;
      } finally {
        Trace.traceEnd(8L);
      } 
    }
    
    void preDispatch(KeyEvent param1KeyEvent) {
      this.mCurrentReceiver = null;
      if (param1KeyEvent.getAction() == 1) {
        int i = this.mCapturedKeys.indexOfKey(param1KeyEvent.getKeyCode());
        if (i >= 0) {
          this.mCurrentReceiver = this.mCapturedKeys.valueAt(i);
          this.mCapturedKeys.removeAt(i);
        } 
      } 
    }
    
    boolean preViewDispatch(KeyEvent param1KeyEvent) {
      this.mDispatched = false;
      if (this.mCurrentReceiver == null)
        this.mCurrentReceiver = this.mCapturedKeys.get(param1KeyEvent.getKeyCode()); 
      WeakReference<View> weakReference = this.mCurrentReceiver;
      if (weakReference != null) {
        View view = weakReference.get();
        if (param1KeyEvent.getAction() == 1)
          this.mCurrentReceiver = null; 
        if (view != null && view.isAttachedToWindow())
          view.onUnhandledKeyEvent(param1KeyEvent); 
        return true;
      } 
      return false;
    }
  }
  
  public OplusViewRootImplHooks getOplusViewRootImplHooks() {
    return this.mViewRootHooks;
  }
  
  public OplusLongshotViewRoot getLongshotViewRoot() {
    OplusViewRootImplHooks oplusViewRootImplHooks = this.mViewRootHooks;
    if (oplusViewRootImplHooks != null)
      return oplusViewRootImplHooks.getLongshotViewRoot(); 
    return null;
  }
  
  void setUseBLASTSyncTransaction() {
    this.mNextDrawUseBLASTSyncTransaction = true;
  }
  
  private void finishBLASTSync(boolean paramBoolean) {
    this.mSendNextFrameToWm = false;
    if (this.mNextReportConsumeBLAST) {
      this.mNextReportConsumeBLAST = false;
      if (paramBoolean) {
        this.mRtBLASTSyncTransaction.apply();
      } else {
        this.mSurfaceChangedTransaction.merge(this.mRtBLASTSyncTransaction);
      } 
    } 
  }
  
  SurfaceControl.Transaction getBLASTSyncTransaction() {
    return this.mRtBLASTSyncTransaction;
  }
  
  public SurfaceControl getRenderSurfaceControl() {
    if (useBLAST())
      return this.mBlastSurfaceControl; 
    return this.mSurfaceControl;
  }
  
  public void onDescendantUnbufferedRequested() {
    this.mUnbufferedInputSource = this.mView.mUnbufferedInputSource;
  }
  
  void forceDisableBLAST() {
    this.mForceDisableBLAST = true;
  }
  
  boolean useBLAST() {
    boolean bool;
    if (this.mUseBLASTAdapter && !this.mForceDisableBLAST) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean isDrawingToBLASTTransaction() {
    return this.mNextReportConsumeBLAST;
  }
  
  public ArrayList<String> getScreenModeViewList() {
    return this.mScreenModeViewList;
  }
  
  public boolean isScreenModeViewListEmpty() {
    return this.mScreenModeViewList.isEmpty();
  }
  
  public boolean containInScreenModeViewList(String paramString) {
    if (paramString == null || paramString.isEmpty())
      return false; 
    return this.mScreenModeViewList.contains(paramString);
  }
  
  public void addViewToScreenModeViewList(String paramString) {
    if (paramString == null || paramString.isEmpty())
      return; 
    this.mScreenModeViewList.add(paramString);
  }
  
  public void removeViewFromScreenModeViewList(String paramString) {
    if (paramString == null || paramString.isEmpty())
      return; 
    this.mScreenModeViewList.remove(paramString);
  }
  
  private static void setDynamicalLogEnable(boolean paramBoolean) {
    DEBUG_CONFIGURATION = paramBoolean;
    DEBUG_LAYOUT = paramBoolean;
    DEBUG_DIALOG = paramBoolean;
    DEBUG_DRAW = paramBoolean;
  }
  
  class ActivityConfigCallback {
    public abstract void onConfigurationChanged(Configuration param1Configuration, int param1Int);
  }
  
  class ConfigChangedCallback {
    public abstract void onConfigurationChanged(Configuration param1Configuration);
  }
  
  class SurfaceChangedCallback {
    public abstract void surfaceCreated(SurfaceControl.Transaction param1Transaction);
    
    public abstract void surfaceDestroyed();
    
    public abstract void surfaceReplaced(SurfaceControl.Transaction param1Transaction);
  }
}
