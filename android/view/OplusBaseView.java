package android.view;

import android.graphics.Bitmap;
import android.graphics.OplusBaseRenderNode;
import android.graphics.Rect;
import android.text.ITextJustificationHooks;
import android.text.Layout;
import com.oplus.screenshot.OplusLongshotViewInfo;
import com.oplus.screenshot.OplusLongshotViewInt;

public abstract class OplusBaseView implements OplusLongshotViewInt {
  public static final int CRUDE_STATE_BACKGROUND = 1;
  
  public static final int TYPE_FORCE_DARK_ALGORITHM_GOOGLE = 2;
  
  public static final int TYPE_FORCE_DARK_ALGORITHM_OPLUS = 1;
  
  public void setScrollXForColor(int paramInt) {
    if (getScrollX() != paramInt) {
      int i = getScrollX();
      setValueScrollX(paramInt);
      invalidateParentCaches();
      onScrollChanged(getScrollX(), getScrollY(), i, getScrollY());
      if (!awakenScrollBars())
        postInvalidateOnAnimation(); 
    } 
  }
  
  public void setScrollYForColor(int paramInt) {
    if (getScrollY() != paramInt) {
      int i = getScrollY();
      setValueScrollY(paramInt);
      invalidateParentCaches();
      onScrollChanged(getScrollX(), getScrollY(), getScrollX(), i);
      if (!awakenScrollBars())
        postInvalidateOnAnimation(); 
    } 
  }
  
  public boolean isOplusStyle() {
    boolean bool;
    IOplusViewHooks iOplusViewHooks = this.mViewHooks;
    if (iOplusViewHooks != null && iOplusViewHooks.isOplusStyle()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isOplusOSStyle() {
    boolean bool;
    IOplusViewHooks iOplusViewHooks = this.mViewHooks;
    if (iOplusViewHooks != null && iOplusViewHooks.isOplusOSStyle()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void hookPerformClick() {
    IOplusViewHooks iOplusViewHooks = this.mViewHooks;
    if (iOplusViewHooks != null)
      iOplusViewHooks.performClick(); 
  }
  
  public int computeLongScrollRange() {
    return computeVerticalScrollRange();
  }
  
  public int computeLongScrollOffset() {
    return computeVerticalScrollOffset();
  }
  
  public int computeLongScrollExtent() {
    return computeVerticalScrollExtent();
  }
  
  public boolean canLongScroll() {
    return canScrollVertically(1);
  }
  
  public boolean isLongshotVisibleToUser() {
    if (getVisibility() != 0)
      return false; 
    return isVisibleToUser();
  }
  
  public boolean findViewsLongshotInfo(OplusLongshotViewInfo paramOplusLongshotViewInfo) {
    return this.mViewHooks.findViewsLongshotInfo(paramOplusLongshotViewInfo);
  }
  
  public void onLongshotOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    onOverScrolled(paramInt1, paramInt2, paramBoolean1, paramBoolean2);
  }
  
  protected boolean hookOverScrollBy(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean) {
    boolean bool1 = this.mViewHooks.isLongshotConnected();
    if (bool1)
      paramInt8 = 0; 
    boolean bool2 = overScrollBy(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramBoolean);
    boolean bool3 = bool2;
    if (bool1) {
      IOplusViewHooks iOplusViewHooks = this.mViewHooks;
      int i = getScrollY();
      bool3 = iOplusViewHooks.overScrollBy(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramBoolean, i, bool2);
    } 
    return bool3;
  }
  
  private int mCrudeState = 0;
  
  protected Layout mLayout;
  
  private int mOriginWebSettingForceDark = -1;
  
  protected ITextJustificationHooks mTextJustificationHooksImpl;
  
  protected IOplusViewHooks mViewHooks;
  
  private int mViewType = 0;
  
  public int getOplusViewType() {
    return this.mViewType;
  }
  
  public void setOplusViewTypeLocked(int paramInt) {
    this.mViewType = paramInt;
  }
  
  public void setOriginWebSettingForceDark(int paramInt) {
    this.mOriginWebSettingForceDark = paramInt;
  }
  
  public int getOriginWebSettingForceDark() {
    return this.mOriginWebSettingForceDark;
  }
  
  public void setCrudeState(int paramInt) {
    this.mCrudeState = paramInt;
  }
  
  public int getCrudeState() {
    return this.mCrudeState;
  }
  
  public void setUsageForceDarkAlgorithmType(int paramInt) {
    OplusBaseRenderNode oplusBaseRenderNode = getRenderNode();
    if (oplusBaseRenderNode != null) {
      oplusBaseRenderNode.setUsageForceDarkAlgorithmType(paramInt);
      invalidate();
    } 
  }
  
  public void setParaSpacing(float paramFloat) {
    ITextJustificationHooks iTextJustificationHooks = this.mTextJustificationHooksImpl;
    if (iTextJustificationHooks != null)
      iTextJustificationHooks.setTextViewParaSpacing(this, paramFloat, this.mLayout); 
  }
  
  public float getParaSpacing() {
    ITextJustificationHooks iTextJustificationHooks = this.mTextJustificationHooksImpl;
    if (iTextJustificationHooks != null)
      return iTextJustificationHooks.getTextViewParaSpacing(this); 
    return 0.0F;
  }
  
  public Bitmap getColorCustomDrawingCache(Rect paramRect, int paramInt) {
    return null;
  }
  
  public void updateColorNavigationGuardColor(int paramInt) {}
  
  protected abstract boolean awakenScrollBars();
  
  protected abstract boolean canScrollVertically(int paramInt);
  
  protected abstract int computeVerticalScrollExtent();
  
  protected abstract int computeVerticalScrollOffset();
  
  protected abstract int computeVerticalScrollRange();
  
  protected abstract OplusBaseRenderNode getRenderNode();
  
  public abstract int getScrollX();
  
  public abstract int getScrollY();
  
  public abstract IOplusBaseViewRoot getViewRootImpl();
  
  protected abstract int getVisibility();
  
  protected abstract void invalidate();
  
  protected abstract void invalidateParentCaches();
  
  protected abstract boolean isVisibleToUser();
  
  protected abstract void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2);
  
  protected abstract void onScrollChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  protected abstract boolean overScrollBy(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean);
  
  public abstract void postInvalidateOnAnimation();
  
  protected abstract void setValueScrollX(int paramInt);
  
  protected abstract void setValueScrollY(int paramInt);
}
