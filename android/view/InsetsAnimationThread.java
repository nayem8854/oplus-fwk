package android.view;

import android.os.Handler;
import android.os.HandlerThread;

public class InsetsAnimationThread extends HandlerThread {
  private static Handler sHandler;
  
  private static InsetsAnimationThread sInstance;
  
  private InsetsAnimationThread() {
    super("InsetsAnimations");
  }
  
  private static void ensureThreadLocked() {
    if (sInstance == null) {
      InsetsAnimationThread insetsAnimationThread = new InsetsAnimationThread();
      insetsAnimationThread.start();
      sInstance.getLooper().setTraceTag(8L);
      sHandler = new Handler(sInstance.getLooper());
    } 
  }
  
  public static void release() {
    // Byte code:
    //   0: ldc android/view/InsetsAnimationThread
    //   2: monitorenter
    //   3: getstatic android/view/InsetsAnimationThread.sInstance : Landroid/view/InsetsAnimationThread;
    //   6: ifnonnull -> 13
    //   9: ldc android/view/InsetsAnimationThread
    //   11: monitorexit
    //   12: return
    //   13: getstatic android/view/InsetsAnimationThread.sInstance : Landroid/view/InsetsAnimationThread;
    //   16: invokevirtual getLooper : ()Landroid/os/Looper;
    //   19: invokevirtual quitSafely : ()V
    //   22: aconst_null
    //   23: putstatic android/view/InsetsAnimationThread.sInstance : Landroid/view/InsetsAnimationThread;
    //   26: aconst_null
    //   27: putstatic android/view/InsetsAnimationThread.sHandler : Landroid/os/Handler;
    //   30: ldc android/view/InsetsAnimationThread
    //   32: monitorexit
    //   33: return
    //   34: astore_0
    //   35: ldc android/view/InsetsAnimationThread
    //   37: monitorexit
    //   38: aload_0
    //   39: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #47	-> 0
    //   #48	-> 3
    //   #49	-> 9
    //   #51	-> 13
    //   #52	-> 22
    //   #53	-> 26
    //   #54	-> 30
    //   #55	-> 33
    //   #54	-> 34
    // Exception table:
    //   from	to	target	type
    //   3	9	34	finally
    //   9	12	34	finally
    //   13	22	34	finally
    //   22	26	34	finally
    //   26	30	34	finally
    //   30	33	34	finally
    //   35	38	34	finally
  }
  
  public static InsetsAnimationThread get() {
    // Byte code:
    //   0: ldc android/view/InsetsAnimationThread
    //   2: monitorenter
    //   3: invokestatic ensureThreadLocked : ()V
    //   6: getstatic android/view/InsetsAnimationThread.sInstance : Landroid/view/InsetsAnimationThread;
    //   9: astore_0
    //   10: ldc android/view/InsetsAnimationThread
    //   12: monitorexit
    //   13: aload_0
    //   14: areturn
    //   15: astore_0
    //   16: ldc android/view/InsetsAnimationThread
    //   18: monitorexit
    //   19: aload_0
    //   20: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #58	-> 0
    //   #59	-> 3
    //   #60	-> 6
    //   #61	-> 15
    // Exception table:
    //   from	to	target	type
    //   3	6	15	finally
    //   6	13	15	finally
    //   16	19	15	finally
  }
  
  public static Handler getHandler() {
    // Byte code:
    //   0: ldc android/view/InsetsAnimationThread
    //   2: monitorenter
    //   3: invokestatic ensureThreadLocked : ()V
    //   6: getstatic android/view/InsetsAnimationThread.sHandler : Landroid/os/Handler;
    //   9: astore_0
    //   10: ldc android/view/InsetsAnimationThread
    //   12: monitorexit
    //   13: aload_0
    //   14: areturn
    //   15: astore_0
    //   16: ldc android/view/InsetsAnimationThread
    //   18: monitorexit
    //   19: aload_0
    //   20: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #65	-> 0
    //   #66	-> 3
    //   #67	-> 6
    //   #68	-> 15
    // Exception table:
    //   from	to	target	type
    //   3	6	15	finally
    //   6	13	15	finally
    //   16	19	15	finally
  }
}
