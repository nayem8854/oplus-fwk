package android.view;

import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.DisplayMetrics;
import java.util.Objects;

public class DisplayAdjustments {
  public static final DisplayAdjustments DEFAULT_DISPLAY_ADJUSTMENTS = new DisplayAdjustments();
  
  private volatile CompatibilityInfo mCompatInfo = CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO;
  
  private final Configuration mConfiguration;
  
  private FixedRotationAdjustments mFixedRotationAdjustments;
  
  public DisplayAdjustments() {
    this.mConfiguration = new Configuration(Configuration.EMPTY);
  }
  
  public DisplayAdjustments(Configuration paramConfiguration) {
    Configuration configuration = new Configuration(Configuration.EMPTY);
    if (paramConfiguration != null)
      configuration.setTo(paramConfiguration); 
  }
  
  public DisplayAdjustments(DisplayAdjustments paramDisplayAdjustments) {
    this.mConfiguration = new Configuration(Configuration.EMPTY);
    setCompatibilityInfo(paramDisplayAdjustments.mCompatInfo);
    this.mConfiguration.setTo(paramDisplayAdjustments.getConfiguration());
    this.mFixedRotationAdjustments = paramDisplayAdjustments.mFixedRotationAdjustments;
  }
  
  public void setCompatibilityInfo(CompatibilityInfo paramCompatibilityInfo) {
    if (this != DEFAULT_DISPLAY_ADJUSTMENTS) {
      if (paramCompatibilityInfo != null && (paramCompatibilityInfo.isScalingRequired() || 
        !paramCompatibilityInfo.supportsScreen())) {
        this.mCompatInfo = paramCompatibilityInfo;
      } else {
        this.mCompatInfo = CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO;
      } 
      return;
    } 
    throw new IllegalArgumentException("setCompatbilityInfo: Cannot modify DEFAULT_DISPLAY_ADJUSTMENTS");
  }
  
  public CompatibilityInfo getCompatibilityInfo() {
    return this.mCompatInfo;
  }
  
  public void setConfiguration(Configuration paramConfiguration) {
    if (this != DEFAULT_DISPLAY_ADJUSTMENTS) {
      Configuration configuration = this.mConfiguration;
      if (paramConfiguration == null)
        paramConfiguration = Configuration.EMPTY; 
      configuration.setTo(paramConfiguration);
      return;
    } 
    throw new IllegalArgumentException("setConfiguration: Cannot modify DEFAULT_DISPLAY_ADJUSTMENTS");
  }
  
  public Configuration getConfiguration() {
    return this.mConfiguration;
  }
  
  public void setFixedRotationAdjustments(FixedRotationAdjustments paramFixedRotationAdjustments) {
    this.mFixedRotationAdjustments = paramFixedRotationAdjustments;
  }
  
  public FixedRotationAdjustments getFixedRotationAdjustments() {
    return this.mFixedRotationAdjustments;
  }
  
  private boolean noFlip(int paramInt) {
    FixedRotationAdjustments fixedRotationAdjustments = this.mFixedRotationAdjustments;
    boolean bool = true;
    if (fixedRotationAdjustments == null)
      return true; 
    if ((paramInt - fixedRotationAdjustments.mRotation + 4) % 2 != 0)
      bool = false; 
    return bool;
  }
  
  public void adjustSize(Point paramPoint, int paramInt) {
    if (noFlip(paramInt))
      return; 
    paramInt = paramPoint.x;
    paramPoint.x = paramPoint.y;
    paramPoint.y = paramInt;
  }
  
  public void adjustMetrics(DisplayMetrics paramDisplayMetrics, int paramInt) {
    if (noFlip(paramInt))
      return; 
    paramInt = paramDisplayMetrics.widthPixels;
    paramDisplayMetrics.widthPixels = paramDisplayMetrics.heightPixels;
    paramDisplayMetrics.heightPixels = paramInt;
    paramInt = paramDisplayMetrics.noncompatWidthPixels;
    paramDisplayMetrics.noncompatWidthPixels = paramDisplayMetrics.noncompatHeightPixels;
    paramDisplayMetrics.noncompatHeightPixels = paramInt;
    float f = paramDisplayMetrics.xdpi;
    paramDisplayMetrics.xdpi = paramDisplayMetrics.ydpi;
    paramDisplayMetrics.ydpi = f;
    f = paramDisplayMetrics.noncompatXdpi;
    paramDisplayMetrics.noncompatXdpi = paramDisplayMetrics.noncompatYdpi;
    paramDisplayMetrics.noncompatYdpi = f;
  }
  
  public DisplayCutout getDisplayCutout(DisplayCutout paramDisplayCutout) {
    FixedRotationAdjustments fixedRotationAdjustments = this.mFixedRotationAdjustments;
    if (fixedRotationAdjustments != null && fixedRotationAdjustments.mRotatedDisplayCutout != null)
      paramDisplayCutout = fixedRotationAdjustments.mRotatedDisplayCutout; 
    return paramDisplayCutout;
  }
  
  public int getRotation(int paramInt) {
    FixedRotationAdjustments fixedRotationAdjustments = this.mFixedRotationAdjustments;
    if (fixedRotationAdjustments != null)
      paramInt = fixedRotationAdjustments.mRotation; 
    return paramInt;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mCompatInfo);
    int j = Objects.hashCode(this.mConfiguration);
    int k = Objects.hashCode(this.mFixedRotationAdjustments);
    return ((17 * 31 + i) * 31 + j) * 31 + k;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof DisplayAdjustments;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (Objects.equals(((DisplayAdjustments)paramObject).mCompatInfo, this.mCompatInfo)) {
      Configuration configuration1 = ((DisplayAdjustments)paramObject).mConfiguration, configuration2 = this.mConfiguration;
      if (Objects.equals(configuration1, configuration2)) {
        paramObject = ((DisplayAdjustments)paramObject).mFixedRotationAdjustments;
        FixedRotationAdjustments fixedRotationAdjustments = this.mFixedRotationAdjustments;
        if (Objects.equals(paramObject, fixedRotationAdjustments))
          bool1 = true; 
      } 
    } 
    return bool1;
  }
  
  class FixedRotationAdjustments implements Parcelable {
    public FixedRotationAdjustments(DisplayAdjustments this$0, DisplayCutout param1DisplayCutout) {
      this.mRotation = this$0;
      this.mRotatedDisplayCutout = param1DisplayCutout;
    }
    
    public int hashCode() {
      int i = this.mRotation;
      int j = Objects.hashCode(this.mRotatedDisplayCutout);
      return (17 * 31 + i) * 31 + j;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof FixedRotationAdjustments;
      boolean bool1 = false;
      if (!bool)
        return false; 
      FixedRotationAdjustments fixedRotationAdjustments = (FixedRotationAdjustments)param1Object;
      if (this.mRotation == fixedRotationAdjustments.mRotation) {
        param1Object = this.mRotatedDisplayCutout;
        DisplayCutout displayCutout = fixedRotationAdjustments.mRotatedDisplayCutout;
        if (Objects.equals(param1Object, displayCutout))
          bool1 = true; 
      } 
      return bool1;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("FixedRotationAdjustments{rotation=");
      stringBuilder.append(Surface.rotationToString(this.mRotation));
      stringBuilder.append(" cutout=");
      stringBuilder.append(this.mRotatedDisplayCutout);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mRotation);
      param1Parcel.writeTypedObject(new DisplayCutout.ParcelableWrapper(this.mRotatedDisplayCutout), param1Int);
    }
    
    private FixedRotationAdjustments(DisplayAdjustments this$0) {
      this.mRotation = this$0.readInt();
      Parcelable.Creator<DisplayCutout.ParcelableWrapper> creator = DisplayCutout.ParcelableWrapper.CREATOR;
      DisplayCutout.ParcelableWrapper parcelableWrapper = (DisplayCutout.ParcelableWrapper)this$0.readTypedObject(creator);
      if (parcelableWrapper != null) {
        DisplayCutout displayCutout = parcelableWrapper.get();
      } else {
        parcelableWrapper = null;
      } 
      this.mRotatedDisplayCutout = (DisplayCutout)parcelableWrapper;
    }
    
    public static final Parcelable.Creator<FixedRotationAdjustments> CREATOR = new Parcelable.Creator<FixedRotationAdjustments>() {
        public DisplayAdjustments.FixedRotationAdjustments createFromParcel(Parcel param2Parcel) {
          return new DisplayAdjustments.FixedRotationAdjustments();
        }
        
        public DisplayAdjustments.FixedRotationAdjustments[] newArray(int param2Int) {
          return new DisplayAdjustments.FixedRotationAdjustments[param2Int];
        }
      };
    
    final DisplayCutout mRotatedDisplayCutout;
    
    final int mRotation;
  }
}
