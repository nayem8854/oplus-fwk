package android.view;

import android.content.Context;
import android.util.Log;
import com.oplus.util.OplusContextUtil;

public class OplusViewConfigHelper implements IOplusViewConfigHelper {
  private int mColorOverDist;
  
  private boolean mIsColorStyle;
  
  public OplusViewConfigHelper(Context paramContext) {
    this.mColorOverDist = (paramContext.getResources().getDisplayMetrics()).heightPixels;
    this.mIsColorStyle = OplusContextUtil.isOplusOSStyle(paramContext);
  }
  
  public int getScaledOverscrollDistance(int paramInt) {
    if (this.mIsColorStyle) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getScaledOverscrollDistance: a mColorOverDist: ");
      stringBuilder.append(this.mColorOverDist);
      Log.d("TestOverScroll", stringBuilder.toString());
      return this.mColorOverDist;
    } 
    Log.d("TestOverScroll", "getScaledOverscrollDistance: b");
    return paramInt;
  }
  
  public int getScaledOverflingDistance(int paramInt) {
    if (this.mIsColorStyle)
      return this.mColorOverDist; 
    return paramInt;
  }
  
  public int calcRealOverScrollDist(int paramInt1, int paramInt2) {
    if (this.mIsColorStyle) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("calcRealOverScrollDist: a-scrollY: ");
      stringBuilder.append(paramInt2);
      Log.d("TestOverScroll", stringBuilder.toString());
      return (int)(paramInt1 * (1.0F - Math.abs(paramInt2) * 1.0F / this.mColorOverDist) / 3.0F);
    } 
    return paramInt1;
  }
  
  public int calcRealOverScrollDist(int paramInt1, int paramInt2, int paramInt3) {
    if (this.mIsColorStyle && (paramInt2 < 0 || paramInt2 > paramInt3)) {
      int i = paramInt2;
      if (paramInt2 > paramInt3)
        i = paramInt2 - paramInt3; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("calcRealOverScrollDist: b-scrollY: ");
      stringBuilder.append(paramInt2);
      Log.d("TestOverScroll", stringBuilder.toString());
      return (int)(paramInt1 * (1.0F - Math.abs(i) * 1.0F / this.mColorOverDist) / 3.0F);
    } 
    return paramInt1;
  }
}
