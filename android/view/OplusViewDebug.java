package android.view;

import android.util.Log;

public class OplusViewDebug {
  private static final String LOG_TAG = "View";
  
  private static boolean sAppCancelDraw = false;
  
  private static int sAppCancelDrawCount = 0;
  
  public static boolean firstCancelDrawListener() {
    return sAppCancelDraw;
  }
  
  public static void dispatchOnPreDraw(boolean paramBoolean, ViewTreeObserver.OnPreDrawListener paramOnPreDrawListener) {
    if (!sAppCancelDraw && paramBoolean) {
      int i = sAppCancelDrawCount;
      if (i > 5 && i % 100 == 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("dispatchOnPreDraw cancelDraw listener = ");
        stringBuilder.append(paramOnPreDrawListener);
        stringBuilder.append(",cancelDraw times =");
        stringBuilder.append(sAppCancelDrawCount);
        Log.d("View", stringBuilder.toString());
      } 
      sAppCancelDraw = paramBoolean;
    } 
  }
  
  public static void performTraversals(boolean paramBoolean) {
    if (paramBoolean) {
      sAppCancelDrawCount++;
    } else {
      if (sAppCancelDrawCount > 5) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("cancelDraw times  = ");
        stringBuilder.append(sAppCancelDrawCount);
        Log.d("View", stringBuilder.toString());
      } 
      sAppCancelDrawCount = 0;
    } 
    sAppCancelDraw = false;
  }
}
