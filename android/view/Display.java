package android.view;

import android.content.res.Resources;
import android.graphics.ColorSpace;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.display.DisplayManagerGlobal;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;

public final class Display {
  private static final int CACHED_APP_SIZE_DURATION_MILLIS = 20;
  
  public static final int COLOR_MODE_ADOBE_RGB = 8;
  
  public static final int COLOR_MODE_BT601_525 = 3;
  
  public static final int COLOR_MODE_BT601_525_UNADJUSTED = 4;
  
  public static final int COLOR_MODE_BT601_625 = 1;
  
  public static final int COLOR_MODE_BT601_625_UNADJUSTED = 2;
  
  public static final int COLOR_MODE_BT709 = 5;
  
  public static final int COLOR_MODE_DCI_P3 = 6;
  
  public static final int COLOR_MODE_DEFAULT = 0;
  
  public static final int COLOR_MODE_DISPLAY_P3 = 9;
  
  public static final int COLOR_MODE_INVALID = -1;
  
  public static final int COLOR_MODE_SRGB = 7;
  
  private static final boolean DEBUG = false;
  
  public static final int DEFAULT_DISPLAY = 0;
  
  public static final int FLAG_CAN_SHOW_WITH_INSECURE_KEYGUARD = 32;
  
  public static final int FLAG_PRESENTATION = 8;
  
  public static final int FLAG_PRIVATE = 4;
  
  public static final int FLAG_ROUND = 16;
  
  public static final int FLAG_SCALING_DISABLED = 1073741824;
  
  public static final int FLAG_SECURE = 2;
  
  public static final int FLAG_SHOULD_SHOW_SYSTEM_DECORATIONS = 64;
  
  public static final int FLAG_SUPPORTS_PROTECTED_BUFFERS = 1;
  
  public static final int FLAG_TRUSTED = 128;
  
  public static final int INVALID_DISPLAY = -1;
  
  public static final int REMOVE_MODE_DESTROY_CONTENT = 1;
  
  public static final int REMOVE_MODE_MOVE_CONTENT_TO_PRIMARY = 0;
  
  public static final int STATE_DOZE = 3;
  
  public static final int STATE_DOZE_SUSPEND = 4;
  
  public static final int STATE_OFF = 1;
  
  public static final int STATE_ON = 2;
  
  public static final int STATE_ON_SUSPEND = 6;
  
  public static final int STATE_UNKNOWN = 0;
  
  public static final int STATE_VR = 5;
  
  private static final String TAG = "Display";
  
  public static final int TYPE_EXTERNAL = 2;
  
  public static final int TYPE_INTERNAL = 1;
  
  public static final int TYPE_OVERLAY = 4;
  
  public static final int TYPE_UNKNOWN = 0;
  
  public static final int TYPE_VIRTUAL = 5;
  
  public static final int TYPE_WIFI = 3;
  
  private final DisplayAddress mAddress;
  
  private int mCachedAppHeightCompat;
  
  private int mCachedAppWidthCompat;
  
  private DisplayAdjustments mDisplayAdjustments;
  
  private final int mDisplayId;
  
  private DisplayInfo mDisplayInfo;
  
  private final int mFlags;
  
  private final DisplayManagerGlobal mGlobal;
  
  private boolean mIsValid;
  
  private long mLastCachedAppSizeUpdate;
  
  private final int mLayerStack;
  
  private boolean mMayAdjustByFixedRotation;
  
  private final String mOwnerPackageName;
  
  private final int mOwnerUid;
  
  private final Resources mResources;
  
  private final DisplayMetrics mTempMetrics;
  
  private final int mType;
  
  public Display(DisplayManagerGlobal paramDisplayManagerGlobal, int paramInt, DisplayInfo paramDisplayInfo, DisplayAdjustments paramDisplayAdjustments) {
    this(paramDisplayManagerGlobal, paramInt, paramDisplayInfo, paramDisplayAdjustments, null);
  }
  
  public Display(DisplayManagerGlobal paramDisplayManagerGlobal, int paramInt, DisplayInfo paramDisplayInfo, Resources paramResources) {
    this(paramDisplayManagerGlobal, paramInt, paramDisplayInfo, null, paramResources);
  }
  
  private Display(DisplayManagerGlobal paramDisplayManagerGlobal, int paramInt, DisplayInfo paramDisplayInfo, DisplayAdjustments paramDisplayAdjustments, Resources paramResources) {
    DisplayAdjustments displayAdjustments;
    this.mTempMetrics = new DisplayMetrics();
    this.mGlobal = paramDisplayManagerGlobal;
    this.mDisplayId = paramInt;
    this.mDisplayInfo = paramDisplayInfo;
    this.mResources = paramResources;
    if (paramResources != null) {
      displayAdjustments = new DisplayAdjustments(this.mResources.getConfiguration());
    } else {
      displayAdjustments = new DisplayAdjustments();
      if (paramDisplayAdjustments != null) {
        this(paramDisplayAdjustments);
      } else {
        this();
      } 
    } 
    this.mDisplayAdjustments = displayAdjustments;
    this.mIsValid = true;
    this.mLayerStack = paramDisplayInfo.layerStack;
    this.mFlags = paramDisplayInfo.flags;
    this.mType = paramDisplayInfo.type;
    this.mAddress = paramDisplayInfo.address;
    this.mOwnerUid = paramDisplayInfo.ownerUid;
    this.mOwnerPackageName = paramDisplayInfo.ownerPackageName;
  }
  
  public int getDisplayId() {
    return this.mDisplayId;
  }
  
  public String getUniqueId() {
    return this.mDisplayInfo.uniqueId;
  }
  
  public boolean isValid() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mIsValid : Z
    //   10: istore_1
    //   11: aload_0
    //   12: monitorexit
    //   13: iload_1
    //   14: ireturn
    //   15: astore_2
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_2
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #543	-> 0
    //   #544	-> 2
    //   #545	-> 6
    //   #546	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	6	15	finally
    //   6	13	15	finally
    //   16	18	15	finally
  }
  
  public boolean getDisplayInfo(DisplayInfo paramDisplayInfo) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_1
    //   7: aload_0
    //   8: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   11: invokevirtual copyFrom : (Landroid/view/DisplayInfo;)V
    //   14: aload_0
    //   15: getfield mIsValid : Z
    //   18: istore_2
    //   19: aload_0
    //   20: monitorexit
    //   21: iload_2
    //   22: ireturn
    //   23: astore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #558	-> 0
    //   #559	-> 2
    //   #560	-> 6
    //   #561	-> 14
    //   #562	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	6	23	finally
    //   6	14	23	finally
    //   14	21	23	finally
    //   24	26	23	finally
  }
  
  public int getLayerStack() {
    return this.mLayerStack;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public DisplayAddress getAddress() {
    return this.mAddress;
  }
  
  public int getOwnerUid() {
    return this.mOwnerUid;
  }
  
  public String getOwnerPackageName() {
    return this.mOwnerPackageName;
  }
  
  public DisplayAdjustments getDisplayAdjustments() {
    Resources resources = this.mResources;
    if (resources != null) {
      DisplayAdjustments displayAdjustments = resources.getDisplayAdjustments();
      if (!this.mDisplayAdjustments.equals(displayAdjustments))
        this.mDisplayAdjustments = new DisplayAdjustments(displayAdjustments); 
    } 
    return this.mDisplayAdjustments;
  }
  
  public String getName() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: getfield name : Ljava/lang/String;
    //   13: astore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_1
    //   17: areturn
    //   18: astore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_1
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #676	-> 0
    //   #677	-> 2
    //   #678	-> 6
    //   #679	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	16	18	finally
    //   19	21	18	finally
  }
  
  @Deprecated
  public void getSize(Point paramPoint) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: aload_0
    //   11: getfield mTempMetrics : Landroid/util/DisplayMetrics;
    //   14: aload_0
    //   15: invokevirtual getDisplayAdjustments : ()Landroid/view/DisplayAdjustments;
    //   18: invokevirtual getAppMetrics : (Landroid/util/DisplayMetrics;Landroid/view/DisplayAdjustments;)V
    //   21: aload_1
    //   22: aload_0
    //   23: getfield mTempMetrics : Landroid/util/DisplayMetrics;
    //   26: getfield widthPixels : I
    //   29: putfield x : I
    //   32: aload_1
    //   33: aload_0
    //   34: getfield mTempMetrics : Landroid/util/DisplayMetrics;
    //   37: getfield heightPixels : I
    //   40: putfield y : I
    //   43: aload_0
    //   44: monitorexit
    //   45: return
    //   46: astore_1
    //   47: aload_0
    //   48: monitorexit
    //   49: aload_1
    //   50: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #715	-> 0
    //   #716	-> 2
    //   #717	-> 6
    //   #718	-> 21
    //   #719	-> 32
    //   #720	-> 43
    //   #721	-> 45
    //   #720	-> 46
    // Exception table:
    //   from	to	target	type
    //   2	6	46	finally
    //   6	21	46	finally
    //   21	32	46	finally
    //   32	43	46	finally
    //   43	45	46	finally
    //   47	49	46	finally
  }
  
  @Deprecated
  public void getRectSize(Rect paramRect) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: aload_0
    //   11: getfield mTempMetrics : Landroid/util/DisplayMetrics;
    //   14: aload_0
    //   15: invokevirtual getDisplayAdjustments : ()Landroid/view/DisplayAdjustments;
    //   18: invokevirtual getAppMetrics : (Landroid/util/DisplayMetrics;Landroid/view/DisplayAdjustments;)V
    //   21: aload_1
    //   22: iconst_0
    //   23: iconst_0
    //   24: aload_0
    //   25: getfield mTempMetrics : Landroid/util/DisplayMetrics;
    //   28: getfield widthPixels : I
    //   31: aload_0
    //   32: getfield mTempMetrics : Landroid/util/DisplayMetrics;
    //   35: getfield heightPixels : I
    //   38: invokevirtual set : (IIII)V
    //   41: aload_0
    //   42: monitorexit
    //   43: return
    //   44: astore_1
    //   45: aload_0
    //   46: monitorexit
    //   47: aload_1
    //   48: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #732	-> 0
    //   #733	-> 2
    //   #734	-> 6
    //   #735	-> 21
    //   #736	-> 41
    //   #737	-> 43
    //   #736	-> 44
    // Exception table:
    //   from	to	target	type
    //   2	6	44	finally
    //   6	21	44	finally
    //   21	41	44	finally
    //   41	43	44	finally
    //   45	47	44	finally
  }
  
  public void getCurrentSizeRange(Point paramPoint1, Point paramPoint2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_1
    //   7: aload_0
    //   8: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   11: getfield smallestNominalAppWidth : I
    //   14: putfield x : I
    //   17: aload_1
    //   18: aload_0
    //   19: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   22: getfield smallestNominalAppHeight : I
    //   25: putfield y : I
    //   28: aload_2
    //   29: aload_0
    //   30: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   33: getfield largestNominalAppWidth : I
    //   36: putfield x : I
    //   39: aload_2
    //   40: aload_0
    //   41: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   44: getfield largestNominalAppHeight : I
    //   47: putfield y : I
    //   50: aload_0
    //   51: monitorexit
    //   52: return
    //   53: astore_1
    //   54: aload_0
    //   55: monitorexit
    //   56: aload_1
    //   57: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #770	-> 0
    //   #771	-> 2
    //   #772	-> 6
    //   #773	-> 17
    //   #774	-> 28
    //   #775	-> 39
    //   #776	-> 50
    //   #777	-> 52
    //   #776	-> 53
    // Exception table:
    //   from	to	target	type
    //   2	6	53	finally
    //   6	17	53	finally
    //   17	28	53	finally
    //   28	39	53	finally
    //   39	50	53	finally
    //   50	52	53	finally
    //   54	56	53	finally
  }
  
  public int getMaximumSizeDimension() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: getfield logicalWidth : I
    //   13: aload_0
    //   14: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   17: getfield logicalHeight : I
    //   20: invokestatic max : (II)I
    //   23: istore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: iload_1
    //   27: ireturn
    //   28: astore_2
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_2
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #786	-> 0
    //   #787	-> 2
    //   #788	-> 6
    //   #789	-> 28
    // Exception table:
    //   from	to	target	type
    //   2	6	28	finally
    //   6	26	28	finally
    //   29	31	28	finally
  }
  
  @Deprecated
  public int getWidth() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateCachedAppSizeIfNeededLocked : ()V
    //   6: aload_0
    //   7: getfield mCachedAppWidthCompat : I
    //   10: istore_1
    //   11: aload_0
    //   12: monitorexit
    //   13: iload_1
    //   14: ireturn
    //   15: astore_2
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_2
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #797	-> 0
    //   #798	-> 2
    //   #799	-> 6
    //   #800	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	6	15	finally
    //   6	13	15	finally
    //   16	18	15	finally
  }
  
  @Deprecated
  public int getHeight() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateCachedAppSizeIfNeededLocked : ()V
    //   6: aload_0
    //   7: getfield mCachedAppHeightCompat : I
    //   10: istore_1
    //   11: aload_0
    //   12: monitorexit
    //   13: iload_1
    //   14: ireturn
    //   15: astore_2
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_2
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #808	-> 0
    //   #809	-> 2
    //   #810	-> 6
    //   #811	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	6	15	finally
    //   6	13	15	finally
    //   16	18	15	finally
  }
  
  public int getRotation() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mMayAdjustByFixedRotation : Z
    //   10: ifeq -> 31
    //   13: aload_0
    //   14: invokevirtual getDisplayAdjustments : ()Landroid/view/DisplayAdjustments;
    //   17: aload_0
    //   18: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   21: getfield rotation : I
    //   24: invokevirtual getRotation : (I)I
    //   27: istore_1
    //   28: goto -> 39
    //   31: aload_0
    //   32: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   35: getfield rotation : I
    //   38: istore_1
    //   39: aload_0
    //   40: monitorexit
    //   41: iload_1
    //   42: ireturn
    //   43: astore_2
    //   44: aload_0
    //   45: monitorexit
    //   46: aload_2
    //   47: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #833	-> 0
    //   #834	-> 2
    //   #835	-> 6
    //   #836	-> 13
    //   #837	-> 31
    //   #835	-> 41
    //   #838	-> 43
    // Exception table:
    //   from	to	target	type
    //   2	6	43	finally
    //   6	13	43	finally
    //   13	28	43	finally
    //   31	39	43	finally
    //   39	41	43	finally
    //   44	46	43	finally
  }
  
  @Deprecated
  public int getOrientation() {
    return getRotation();
  }
  
  public DisplayCutout getCutout() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mMayAdjustByFixedRotation : Z
    //   10: ifeq -> 31
    //   13: aload_0
    //   14: invokevirtual getDisplayAdjustments : ()Landroid/view/DisplayAdjustments;
    //   17: aload_0
    //   18: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   21: getfield displayCutout : Landroid/view/DisplayCutout;
    //   24: invokevirtual getDisplayCutout : (Landroid/view/DisplayCutout;)Landroid/view/DisplayCutout;
    //   27: astore_1
    //   28: goto -> 39
    //   31: aload_0
    //   32: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   35: getfield displayCutout : Landroid/view/DisplayCutout;
    //   38: astore_1
    //   39: aload_0
    //   40: monitorexit
    //   41: aload_1
    //   42: areturn
    //   43: astore_1
    //   44: aload_0
    //   45: monitorexit
    //   46: aload_1
    //   47: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #859	-> 0
    //   #860	-> 2
    //   #861	-> 6
    //   #862	-> 13
    //   #863	-> 31
    //   #861	-> 41
    //   #864	-> 43
    // Exception table:
    //   from	to	target	type
    //   2	6	43	finally
    //   6	13	43	finally
    //   13	28	43	finally
    //   31	39	43	finally
    //   39	41	43	finally
    //   44	46	43	finally
  }
  
  @Deprecated
  public int getPixelFormat() {
    return 1;
  }
  
  public float getRefreshRate() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: invokevirtual getMode : ()Landroid/view/Display$Mode;
    //   13: invokevirtual getRefreshRate : ()F
    //   16: fstore_1
    //   17: aload_0
    //   18: monitorexit
    //   19: fload_1
    //   20: freturn
    //   21: astore_2
    //   22: aload_0
    //   23: monitorexit
    //   24: aload_2
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #883	-> 0
    //   #884	-> 2
    //   #885	-> 6
    //   #886	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	19	21	finally
    //   22	24	21	finally
  }
  
  @Deprecated
  public float[] getSupportedRefreshRates() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: invokevirtual getDefaultRefreshRates : ()[F
    //   13: astore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_1
    //   17: areturn
    //   18: astore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_1
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #899	-> 0
    //   #900	-> 2
    //   #901	-> 6
    //   #902	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	16	18	finally
    //   19	21	18	finally
  }
  
  public Mode getMode() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: invokevirtual getMode : ()Landroid/view/Display$Mode;
    //   13: astore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_1
    //   17: areturn
    //   18: astore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_1
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #909	-> 0
    //   #910	-> 2
    //   #911	-> 6
    //   #912	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	16	18	finally
    //   19	21	18	finally
  }
  
  public Mode[] getSupportedModes() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: getfield supportedModes : [Landroid/view/Display$Mode;
    //   13: astore_1
    //   14: aload_1
    //   15: aload_1
    //   16: arraylength
    //   17: invokestatic copyOf : ([Ljava/lang/Object;I)[Ljava/lang/Object;
    //   20: checkcast [Landroid/view/Display$Mode;
    //   23: astore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: areturn
    //   28: astore_1
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_1
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #919	-> 0
    //   #920	-> 2
    //   #921	-> 6
    //   #922	-> 14
    //   #923	-> 28
    // Exception table:
    //   from	to	target	type
    //   2	6	28	finally
    //   6	14	28	finally
    //   14	26	28	finally
    //   29	31	28	finally
  }
  
  public boolean isMinimalPostProcessingSupported() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: getfield minimalPostProcessingSupported : Z
    //   13: istore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: iload_1
    //   17: ireturn
    //   18: astore_2
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_2
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #945	-> 0
    //   #946	-> 2
    //   #947	-> 6
    //   #948	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	16	18	finally
    //   19	21	18	finally
  }
  
  public void requestColorMode(int paramInt) {
    this.mGlobal.requestColorMode(this.mDisplayId, paramInt);
  }
  
  public int getColorMode() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: getfield colorMode : I
    //   13: istore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: iload_1
    //   17: ireturn
    //   18: astore_2
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_2
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #965	-> 0
    //   #966	-> 2
    //   #967	-> 6
    //   #968	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	16	18	finally
    //   19	21	18	finally
  }
  
  public int getRemoveMode() {
    return this.mDisplayInfo.removeMode;
  }
  
  public HdrCapabilities getHdrCapabilities() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: getfield hdrCapabilities : Landroid/view/Display$HdrCapabilities;
    //   13: astore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_1
    //   17: areturn
    //   18: astore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_1
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #992	-> 0
    //   #993	-> 2
    //   #994	-> 6
    //   #995	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	16	18	finally
    //   19	21	18	finally
  }
  
  public boolean isHdr() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: invokevirtual isHdr : ()Z
    //   13: istore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: iload_1
    //   17: ireturn
    //   18: astore_2
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_2
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1005	-> 0
    //   #1006	-> 2
    //   #1007	-> 6
    //   #1008	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	16	18	finally
    //   19	21	18	finally
  }
  
  public boolean isWideColorGamut() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: invokevirtual isWideColorGamut : ()Z
    //   13: istore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: iload_1
    //   17: ireturn
    //   18: astore_2
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_2
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1018	-> 0
    //   #1019	-> 2
    //   #1020	-> 6
    //   #1021	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	16	18	finally
    //   19	21	18	finally
  }
  
  public ColorSpace getPreferredWideGamutColorSpace() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: invokevirtual isWideColorGamut : ()Z
    //   13: ifeq -> 28
    //   16: aload_0
    //   17: getfield mGlobal : Landroid/hardware/display/DisplayManagerGlobal;
    //   20: invokevirtual getPreferredWideGamutColorSpace : ()Landroid/graphics/ColorSpace;
    //   23: astore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: areturn
    //   28: aload_0
    //   29: monitorexit
    //   30: aconst_null
    //   31: areturn
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1033	-> 0
    //   #1034	-> 2
    //   #1035	-> 6
    //   #1036	-> 16
    //   #1038	-> 28
    //   #1039	-> 32
    // Exception table:
    //   from	to	target	type
    //   2	6	32	finally
    //   6	16	32	finally
    //   16	26	32	finally
    //   28	30	32	finally
    //   33	35	32	finally
  }
  
  public int[] getSupportedColorModes() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: getfield supportedColorModes : [I
    //   13: astore_1
    //   14: aload_1
    //   15: aload_1
    //   16: arraylength
    //   17: invokestatic copyOf : ([II)[I
    //   20: astore_1
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_1
    //   24: areturn
    //   25: astore_1
    //   26: aload_0
    //   27: monitorexit
    //   28: aload_1
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1047	-> 0
    //   #1048	-> 2
    //   #1049	-> 6
    //   #1050	-> 14
    //   #1051	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	6	25	finally
    //   6	14	25	finally
    //   14	23	25	finally
    //   26	28	25	finally
  }
  
  public ColorSpace[] getSupportedWideColorGamut() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iconst_0
    //   3: istore_1
    //   4: iconst_0
    //   5: anewarray android/graphics/ColorSpace
    //   8: astore_2
    //   9: aload_0
    //   10: invokespecial updateDisplayInfoLocked : ()V
    //   13: aload_0
    //   14: invokevirtual isWideColorGamut : ()Z
    //   17: ifne -> 24
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_2
    //   23: areturn
    //   24: aload_0
    //   25: invokevirtual getSupportedColorModes : ()[I
    //   28: astore_3
    //   29: new java/util/ArrayList
    //   32: astore #4
    //   34: aload #4
    //   36: invokespecial <init> : ()V
    //   39: aload_3
    //   40: arraylength
    //   41: istore #5
    //   43: iload_1
    //   44: iload #5
    //   46: if_icmpge -> 108
    //   49: aload_3
    //   50: iload_1
    //   51: iaload
    //   52: istore #6
    //   54: iload #6
    //   56: bipush #6
    //   58: if_icmpeq -> 88
    //   61: iload #6
    //   63: bipush #9
    //   65: if_icmpeq -> 71
    //   68: goto -> 102
    //   71: aload #4
    //   73: getstatic android/graphics/ColorSpace$Named.DISPLAY_P3 : Landroid/graphics/ColorSpace$Named;
    //   76: invokestatic get : (Landroid/graphics/ColorSpace$Named;)Landroid/graphics/ColorSpace;
    //   79: invokeinterface add : (Ljava/lang/Object;)Z
    //   84: pop
    //   85: goto -> 102
    //   88: aload #4
    //   90: getstatic android/graphics/ColorSpace$Named.DCI_P3 : Landroid/graphics/ColorSpace$Named;
    //   93: invokestatic get : (Landroid/graphics/ColorSpace$Named;)Landroid/graphics/ColorSpace;
    //   96: invokeinterface add : (Ljava/lang/Object;)Z
    //   101: pop
    //   102: iinc #1, 1
    //   105: goto -> 43
    //   108: aload #4
    //   110: aload_2
    //   111: invokeinterface toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   116: checkcast [Landroid/graphics/ColorSpace;
    //   119: astore_3
    //   120: aload_0
    //   121: monitorexit
    //   122: aload_3
    //   123: areturn
    //   124: astore_3
    //   125: aload_0
    //   126: monitorexit
    //   127: aload_3
    //   128: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1064	-> 0
    //   #1065	-> 2
    //   #1066	-> 9
    //   #1067	-> 13
    //   #1068	-> 20
    //   #1071	-> 24
    //   #1072	-> 29
    //   #1073	-> 39
    //   #1075	-> 54
    //   #1080	-> 71
    //   #1077	-> 88
    //   #1078	-> 102
    //   #1073	-> 102
    //   #1084	-> 108
    //   #1085	-> 124
    // Exception table:
    //   from	to	target	type
    //   4	9	124	finally
    //   9	13	124	finally
    //   13	20	124	finally
    //   20	22	124	finally
    //   24	29	124	finally
    //   29	39	124	finally
    //   39	43	124	finally
    //   71	85	124	finally
    //   88	102	124	finally
    //   108	122	124	finally
    //   125	127	124	finally
  }
  
  public long getAppVsyncOffsetNanos() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: getfield appVsyncOffsetNanos : J
    //   13: lstore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: lload_1
    //   17: lreturn
    //   18: astore_3
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_3
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1098	-> 0
    //   #1099	-> 2
    //   #1100	-> 6
    //   #1101	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	16	18	finally
    //   19	21	18	finally
  }
  
  public long getPresentationDeadlineNanos() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: getfield presentationDeadlineNanos : J
    //   13: lstore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: lload_1
    //   17: lreturn
    //   18: astore_3
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_3
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1116	-> 0
    //   #1117	-> 2
    //   #1118	-> 6
    //   #1119	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	16	18	finally
    //   19	21	18	finally
  }
  
  @Deprecated
  public void getMetrics(DisplayMetrics paramDisplayMetrics) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: aload_1
    //   11: aload_0
    //   12: invokevirtual getDisplayAdjustments : ()Landroid/view/DisplayAdjustments;
    //   15: invokevirtual getAppMetrics : (Landroid/util/DisplayMetrics;Landroid/view/DisplayAdjustments;)V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore_1
    //   22: aload_0
    //   23: monitorexit
    //   24: aload_1
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1152	-> 0
    //   #1153	-> 2
    //   #1154	-> 6
    //   #1155	-> 18
    //   #1156	-> 20
    //   #1155	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	6	21	finally
    //   6	18	21	finally
    //   18	20	21	finally
    //   22	24	21	finally
  }
  
  public void getRealSize(Point paramPoint) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_1
    //   7: aload_0
    //   8: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   11: getfield logicalWidth : I
    //   14: putfield x : I
    //   17: aload_1
    //   18: aload_0
    //   19: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   22: getfield logicalHeight : I
    //   25: putfield y : I
    //   28: aload_0
    //   29: getfield mMayAdjustByFixedRotation : Z
    //   32: ifeq -> 50
    //   35: aload_0
    //   36: invokevirtual getDisplayAdjustments : ()Landroid/view/DisplayAdjustments;
    //   39: aload_1
    //   40: aload_0
    //   41: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   44: getfield rotation : I
    //   47: invokevirtual adjustSize : (Landroid/graphics/Point;I)V
    //   50: getstatic com/oplus/screenmode/IOplusScreenModeFeature.DEFAULT : Lcom/oplus/screenmode/IOplusScreenModeFeature;
    //   53: invokestatic get : (Landroid/common/IOplusCommonFeature;)Landroid/common/IOplusCommonFeature;
    //   56: checkcast com/oplus/screenmode/IOplusScreenModeFeature
    //   59: aload_0
    //   60: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   63: aload_1
    //   64: invokeinterface updateCompatRealSize : (Landroid/view/DisplayInfo;Landroid/graphics/Point;)V
    //   69: aload_0
    //   70: monitorexit
    //   71: return
    //   72: astore_1
    //   73: aload_0
    //   74: monitorexit
    //   75: aload_1
    //   76: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1171	-> 0
    //   #1172	-> 2
    //   #1173	-> 6
    //   #1174	-> 17
    //   #1175	-> 28
    //   #1176	-> 35
    //   #1180	-> 50
    //   #1182	-> 69
    //   #1183	-> 71
    //   #1182	-> 72
    // Exception table:
    //   from	to	target	type
    //   2	6	72	finally
    //   6	17	72	finally
    //   17	28	72	finally
    //   28	35	72	finally
    //   35	50	72	finally
    //   50	69	72	finally
    //   69	71	72	finally
    //   73	75	72	finally
  }
  
  public void getRealMetrics(DisplayMetrics paramDisplayMetrics) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   10: aload_1
    //   11: getstatic android/content/res/CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO : Landroid/content/res/CompatibilityInfo;
    //   14: aconst_null
    //   15: invokevirtual getLogicalMetrics : (Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;Landroid/content/res/Configuration;)V
    //   18: aload_0
    //   19: getfield mMayAdjustByFixedRotation : Z
    //   22: ifeq -> 40
    //   25: aload_0
    //   26: invokevirtual getDisplayAdjustments : ()Landroid/view/DisplayAdjustments;
    //   29: aload_1
    //   30: aload_0
    //   31: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   34: getfield rotation : I
    //   37: invokevirtual adjustMetrics : (Landroid/util/DisplayMetrics;I)V
    //   40: aload_0
    //   41: monitorexit
    //   42: return
    //   43: astore_1
    //   44: aload_0
    //   45: monitorexit
    //   46: aload_1
    //   47: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1197	-> 0
    //   #1198	-> 2
    //   #1199	-> 6
    //   #1201	-> 18
    //   #1202	-> 25
    //   #1204	-> 40
    //   #1205	-> 42
    //   #1204	-> 43
    // Exception table:
    //   from	to	target	type
    //   2	6	43	finally
    //   6	18	43	finally
    //   18	25	43	finally
    //   25	40	43	finally
    //   40	42	43	finally
    //   44	46	43	finally
  }
  
  public int getState() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: getfield mIsValid : Z
    //   10: ifeq -> 24
    //   13: aload_0
    //   14: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   17: getfield state : I
    //   20: istore_1
    //   21: goto -> 26
    //   24: iconst_0
    //   25: istore_1
    //   26: aload_0
    //   27: monitorexit
    //   28: iload_1
    //   29: ireturn
    //   30: astore_2
    //   31: aload_0
    //   32: monitorexit
    //   33: aload_2
    //   34: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1215	-> 0
    //   #1216	-> 2
    //   #1217	-> 6
    //   #1218	-> 30
    // Exception table:
    //   from	to	target	type
    //   2	6	30	finally
    //   6	21	30	finally
    //   26	28	30	finally
    //   31	33	30	finally
  }
  
  public boolean hasAccess(int paramInt) {
    return hasAccess(paramInt, this.mFlags, this.mOwnerUid, this.mDisplayId);
  }
  
  public static boolean hasAccess(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    return ((paramInt2 & 0x4) != 0 && paramInt1 != paramInt3 && paramInt1 != 1000 && paramInt1 != 0) ? (



      
      DisplayManagerGlobal.getInstance().isUidPresentOnDisplay(paramInt1, paramInt4)) : true;
  }
  
  public boolean isPublicPresentation() {
    boolean bool;
    if ((this.mFlags & 0xC) == 8) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isTrusted() {
    boolean bool;
    if ((this.mFlags & 0x80) == 128) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void updateDisplayInfoLocked() {
    DisplayInfo displayInfo = this.mGlobal.getDisplayInfo(this.mDisplayId);
    boolean bool = false;
    if (displayInfo == null) {
      if (this.mIsValid)
        this.mIsValid = false; 
    } else {
      this.mDisplayInfo = displayInfo;
      if (!this.mIsValid)
        this.mIsValid = true; 
    } 
    Resources resources = this.mResources;
    if (resources != null && 
      resources.hasOverrideDisplayAdjustments())
      bool = true; 
    this.mMayAdjustByFixedRotation = bool;
  }
  
  private void updateCachedAppSizeIfNeededLocked() {
    long l = SystemClock.uptimeMillis();
    if (l > this.mLastCachedAppSizeUpdate + 20L) {
      updateDisplayInfoLocked();
      this.mDisplayInfo.getAppMetrics(this.mTempMetrics, getDisplayAdjustments());
      this.mCachedAppWidthCompat = this.mTempMetrics.widthPixels;
      this.mCachedAppHeightCompat = this.mTempMetrics.heightPixels;
      this.mLastCachedAppSizeUpdate = l;
    } 
  }
  
  public String toString() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial updateDisplayInfoLocked : ()V
    //   6: aload_0
    //   7: invokevirtual getDisplayAdjustments : ()Landroid/view/DisplayAdjustments;
    //   10: astore_1
    //   11: aload_0
    //   12: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   15: aload_0
    //   16: getfield mTempMetrics : Landroid/util/DisplayMetrics;
    //   19: aload_1
    //   20: invokevirtual getAppMetrics : (Landroid/util/DisplayMetrics;Landroid/view/DisplayAdjustments;)V
    //   23: new java/lang/StringBuilder
    //   26: astore_2
    //   27: aload_2
    //   28: invokespecial <init> : ()V
    //   31: aload_2
    //   32: ldc_w 'Display id '
    //   35: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   38: pop
    //   39: aload_2
    //   40: aload_0
    //   41: getfield mDisplayId : I
    //   44: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: aload_2
    //   49: ldc_w ': '
    //   52: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: aload_2
    //   57: aload_0
    //   58: getfield mDisplayInfo : Landroid/view/DisplayInfo;
    //   61: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   64: pop
    //   65: aload_0
    //   66: getfield mMayAdjustByFixedRotation : Z
    //   69: ifeq -> 113
    //   72: new java/lang/StringBuilder
    //   75: astore_3
    //   76: aload_3
    //   77: invokespecial <init> : ()V
    //   80: aload_3
    //   81: ldc_w ', '
    //   84: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   87: pop
    //   88: aload_3
    //   89: aload_1
    //   90: invokevirtual getFixedRotationAdjustments : ()Landroid/view/DisplayAdjustments$FixedRotationAdjustments;
    //   93: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   96: pop
    //   97: aload_3
    //   98: ldc_w ', '
    //   101: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   104: pop
    //   105: aload_3
    //   106: invokevirtual toString : ()Ljava/lang/String;
    //   109: astore_3
    //   110: goto -> 117
    //   113: ldc_w ', '
    //   116: astore_3
    //   117: aload_2
    //   118: aload_3
    //   119: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   122: pop
    //   123: aload_2
    //   124: aload_0
    //   125: getfield mTempMetrics : Landroid/util/DisplayMetrics;
    //   128: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   131: pop
    //   132: aload_2
    //   133: ldc_w ', isValid='
    //   136: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   139: pop
    //   140: aload_2
    //   141: aload_0
    //   142: getfield mIsValid : Z
    //   145: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   148: pop
    //   149: aload_2
    //   150: invokevirtual toString : ()Ljava/lang/String;
    //   153: astore_3
    //   154: aload_0
    //   155: monitorexit
    //   156: aload_3
    //   157: areturn
    //   158: astore_3
    //   159: aload_0
    //   160: monitorexit
    //   161: aload_3
    //   162: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1299	-> 0
    //   #1300	-> 2
    //   #1301	-> 6
    //   #1302	-> 11
    //   #1303	-> 23
    //   #1304	-> 65
    //   #1305	-> 72
    //   #1303	-> 156
    //   #1307	-> 158
    // Exception table:
    //   from	to	target	type
    //   2	6	158	finally
    //   6	11	158	finally
    //   11	23	158	finally
    //   23	65	158	finally
    //   65	72	158	finally
    //   72	110	158	finally
    //   117	156	158	finally
    //   159	161	158	finally
  }
  
  public static String typeToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4) {
              if (paramInt != 5)
                return Integer.toString(paramInt); 
              return "VIRTUAL";
            } 
            return "OVERLAY";
          } 
          return "WIFI";
        } 
        return "EXTERNAL";
      } 
      return "INTERNAL";
    } 
    return "UNKNOWN";
  }
  
  public static String stateToString(int paramInt) {
    switch (paramInt) {
      default:
        return Integer.toString(paramInt);
      case 6:
        return "ON_SUSPEND";
      case 5:
        return "VR";
      case 4:
        return "DOZE_SUSPEND";
      case 3:
        return "DOZE";
      case 2:
        return "ON";
      case 1:
        return "OFF";
      case 0:
        break;
    } 
    return "UNKNOWN";
  }
  
  public static boolean isSuspendedState(int paramInt) {
    boolean bool1 = true, bool2 = bool1;
    if (paramInt != 1) {
      bool2 = bool1;
      if (paramInt != 4)
        if (paramInt == 6) {
          bool2 = bool1;
        } else {
          bool2 = false;
        }  
    } 
    return bool2;
  }
  
  public static boolean isDozeState(int paramInt) {
    return (paramInt == 3 || paramInt == 4);
  }
  
  public static boolean isActiveState(int paramInt) {
    return (paramInt == 2 || paramInt == 5);
  }
  
  class Mode implements Parcelable {
    public static final Parcelable.Creator<Mode> CREATOR = new Parcelable.Creator<Mode>() {
        public Display.Mode createFromParcel(Parcel param2Parcel) {
          return new Display.Mode();
        }
        
        public Display.Mode[] newArray(int param2Int) {
          return new Display.Mode[param2Int];
        }
      };
    
    public static final Mode[] EMPTY_ARRAY = new Mode[0];
    
    private final int mHeight;
    
    private final int mModeId;
    
    private final float mRefreshRate;
    
    private final int mWidth;
    
    public Mode(Display this$0, int param1Int1, int param1Int2, float param1Float) {
      this.mModeId = this$0;
      this.mWidth = param1Int1;
      this.mHeight = param1Int2;
      this.mRefreshRate = param1Float;
    }
    
    public int getModeId() {
      return this.mModeId;
    }
    
    public int getPhysicalWidth() {
      return this.mWidth;
    }
    
    public int getPhysicalHeight() {
      return this.mHeight;
    }
    
    public float getRefreshRate() {
      return this.mRefreshRate;
    }
    
    public boolean matches(int param1Int1, int param1Int2, float param1Float) {
      if (this.mWidth == param1Int1 && this.mHeight == param1Int2) {
        float f = this.mRefreshRate;
        if (Float.floatToIntBits(f) == Float.floatToIntBits(param1Float))
          return true; 
      } 
      return false;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof Mode))
        return false; 
      param1Object = param1Object;
      if (this.mModeId != ((Mode)param1Object).mModeId || !matches(((Mode)param1Object).mWidth, ((Mode)param1Object).mHeight, ((Mode)param1Object).mRefreshRate))
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      int i = this.mModeId;
      int j = this.mWidth;
      int k = this.mHeight;
      int m = Float.floatToIntBits(this.mRefreshRate);
      return (((1 * 17 + i) * 17 + j) * 17 + k) * 17 + m;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder("{");
      stringBuilder.append("id=");
      stringBuilder.append(this.mModeId);
      stringBuilder.append(", width=");
      stringBuilder.append(this.mWidth);
      stringBuilder.append(", height=");
      stringBuilder.append(this.mHeight);
      stringBuilder.append(", fps=");
      stringBuilder.append(this.mRefreshRate);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public int describeContents() {
      return 0;
    }
    
    private Mode() {
      this(this$0.readInt(), this$0.readInt(), this$0.readInt(), this$0.readFloat());
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mModeId);
      param1Parcel.writeInt(this.mWidth);
      param1Parcel.writeInt(this.mHeight);
      param1Parcel.writeFloat(this.mRefreshRate);
    }
    
    static {
    
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface HdrType {}
  
  class HdrCapabilities implements Parcelable {
    private int[] mSupportedHdrTypes = new int[0];
    
    private float mMaxLuminance = -1.0F;
    
    private float mMaxAverageLuminance = -1.0F;
    
    private float mMinLuminance = -1.0F;
    
    public HdrCapabilities(float param1Float1, float param1Float2, float param1Float3) {
      this.mSupportedHdrTypes = (int[])this$0;
      Arrays.sort((int[])this$0);
      this.mMaxLuminance = param1Float1;
      this.mMaxAverageLuminance = param1Float2;
      this.mMinLuminance = param1Float3;
    }
    
    public int[] getSupportedHdrTypes() {
      return this.mSupportedHdrTypes;
    }
    
    public float getDesiredMaxLuminance() {
      return this.mMaxLuminance;
    }
    
    public float getDesiredMaxAverageLuminance() {
      return this.mMaxAverageLuminance;
    }
    
    public float getDesiredMinLuminance() {
      return this.mMinLuminance;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof HdrCapabilities))
        return false; 
      param1Object = param1Object;
      if (!Arrays.equals(this.mSupportedHdrTypes, ((HdrCapabilities)param1Object).mSupportedHdrTypes) || this.mMaxLuminance != ((HdrCapabilities)param1Object).mMaxLuminance || this.mMaxAverageLuminance != ((HdrCapabilities)param1Object).mMaxAverageLuminance || this.mMinLuminance != ((HdrCapabilities)param1Object).mMinLuminance)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      int i = Arrays.hashCode(this.mSupportedHdrTypes);
      int j = Float.floatToIntBits(this.mMaxLuminance);
      int k = Float.floatToIntBits(this.mMaxAverageLuminance);
      int m = Float.floatToIntBits(this.mMinLuminance);
      return (((23 * 17 + i) * 17 + j) * 17 + k) * 17 + m;
    }
    
    public static final Parcelable.Creator<HdrCapabilities> CREATOR = new Parcelable.Creator<HdrCapabilities>() {
        public Display.HdrCapabilities createFromParcel(Parcel param2Parcel) {
          return new Display.HdrCapabilities();
        }
        
        public Display.HdrCapabilities[] newArray(int param2Int) {
          return new Display.HdrCapabilities[param2Int];
        }
      };
    
    public static final int HDR_TYPE_DOLBY_VISION = 1;
    
    public static final int HDR_TYPE_HDR10 = 2;
    
    public static final int HDR_TYPE_HDR10_PLUS = 4;
    
    public static final int HDR_TYPE_HLG = 3;
    
    public static final float INVALID_LUMINANCE = -1.0F;
    
    private HdrCapabilities(Display this$0) {
      readFromParcel((Parcel)this$0);
    }
    
    public void readFromParcel(Parcel param1Parcel) {
      int i = param1Parcel.readInt();
      this.mSupportedHdrTypes = new int[i];
      for (byte b = 0; b < i; b++)
        this.mSupportedHdrTypes[b] = param1Parcel.readInt(); 
      this.mMaxLuminance = param1Parcel.readFloat();
      this.mMaxAverageLuminance = param1Parcel.readFloat();
      this.mMinLuminance = param1Parcel.readFloat();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mSupportedHdrTypes.length);
      param1Int = 0;
      while (true) {
        int[] arrayOfInt = this.mSupportedHdrTypes;
        if (param1Int < arrayOfInt.length) {
          param1Parcel.writeInt(arrayOfInt[param1Int]);
          param1Int++;
          continue;
        } 
        break;
      } 
      param1Parcel.writeFloat(this.mMaxLuminance);
      param1Parcel.writeFloat(this.mMaxAverageLuminance);
      param1Parcel.writeFloat(this.mMinLuminance);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("HdrCapabilities{mSupportedHdrTypes=");
      int[] arrayOfInt = this.mSupportedHdrTypes;
      stringBuilder.append(Arrays.toString(arrayOfInt));
      stringBuilder.append(", mMaxLuminance=");
      stringBuilder.append(this.mMaxLuminance);
      stringBuilder.append(", mMaxAverageLuminance=");
      stringBuilder.append(this.mMaxAverageLuminance);
      stringBuilder.append(", mMinLuminance=");
      stringBuilder.append(this.mMinLuminance);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    public HdrCapabilities() {}
    
    @Retention(RetentionPolicy.SOURCE)
    class HdrType implements Annotation {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ColorMode {}
}
