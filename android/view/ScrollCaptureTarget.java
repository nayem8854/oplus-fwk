package android.view;

import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import com.android.internal.util.FastMath;

public final class ScrollCaptureTarget {
  private final float[] mTmpFloatArr = new float[2];
  
  private final Matrix mMatrixViewLocalToWindow = new Matrix();
  
  private final Rect mTmpRect = new Rect();
  
  private final ScrollCaptureCallback mCallback;
  
  private final View mContainingView;
  
  private final int mHint;
  
  private final Rect mLocalVisibleRect;
  
  private final Point mPositionInWindow;
  
  private Rect mScrollBounds;
  
  public ScrollCaptureTarget(View paramView, Rect paramRect, Point paramPoint, ScrollCaptureCallback paramScrollCaptureCallback) {
    this.mContainingView = paramView;
    this.mHint = paramView.getScrollCaptureHint();
    this.mCallback = paramScrollCaptureCallback;
    this.mLocalVisibleRect = paramRect;
    this.mPositionInWindow = paramPoint;
  }
  
  public int getHint() {
    return this.mHint;
  }
  
  public ScrollCaptureCallback getCallback() {
    return this.mCallback;
  }
  
  public View getContainingView() {
    return this.mContainingView;
  }
  
  public Rect getLocalVisibleRect() {
    return this.mLocalVisibleRect;
  }
  
  public Point getPositionInWindow() {
    return this.mPositionInWindow;
  }
  
  public Rect getScrollBounds() {
    return this.mScrollBounds;
  }
  
  public void setScrollBounds(Rect paramRect) {
    this.mScrollBounds = paramRect = Rect.copyOrNull(paramRect);
    if (paramRect == null)
      return; 
    View view = this.mContainingView;
    int i = view.getWidth(), j = this.mContainingView.getHeight();
    if (!paramRect.intersect(0, 0, i, j))
      this.mScrollBounds.setEmpty(); 
  }
  
  private static void zero(float[] paramArrayOffloat) {
    paramArrayOffloat[0] = 0.0F;
    paramArrayOffloat[1] = 0.0F;
  }
  
  private static void roundIntoPoint(Point paramPoint, float[] paramArrayOffloat) {
    paramPoint.x = FastMath.round(paramArrayOffloat[0]);
    paramPoint.y = FastMath.round(paramArrayOffloat[1]);
  }
  
  public void updatePositionInWindow() {
    this.mMatrixViewLocalToWindow.reset();
    this.mContainingView.transformMatrixToGlobal(this.mMatrixViewLocalToWindow);
    zero(this.mTmpFloatArr);
    this.mMatrixViewLocalToWindow.mapPoints(this.mTmpFloatArr);
    roundIntoPoint(this.mPositionInWindow, this.mTmpFloatArr);
  }
}
