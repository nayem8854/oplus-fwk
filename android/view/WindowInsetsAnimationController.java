package android.view;

import android.graphics.Insets;

public interface WindowInsetsAnimationController {
  void finish(boolean paramBoolean);
  
  float getCurrentAlpha();
  
  float getCurrentFraction();
  
  Insets getCurrentInsets();
  
  Insets getHiddenStateInsets();
  
  Insets getShownStateInsets();
  
  int getTypes();
  
  boolean hasZeroInsetsIme();
  
  boolean isCancelled();
  
  boolean isFinished();
  
  default boolean isReady() {
    boolean bool;
    if (!isFinished() && !isCancelled()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  void setInsetsAndAlpha(Insets paramInsets, float paramFloat1, float paramFloat2);
}
