package android.view;

import android.graphics.Region;
import android.os.IBinder;
import java.lang.ref.WeakReference;

public final class InputWindowHandle {
  public final Region touchableRegion = new Region();
  
  public int portalToDisplayId = -1;
  
  public WeakReference<SurfaceControl> touchableRegionSurfaceControl = new WeakReference<>(null);
  
  public boolean canReceiveKeys;
  
  public long dispatchingTimeoutNanos;
  
  public int displayId;
  
  public int frameBottom;
  
  public int frameLeft;
  
  public int frameRight;
  
  public int frameTop;
  
  public boolean hasFocus;
  
  public boolean hasWallpaper;
  
  public InputApplicationHandle inputApplicationHandle;
  
  public int inputFeatures;
  
  public int layoutParamsFlags;
  
  public int layoutParamsType;
  
  public String name;
  
  public int ownerPid;
  
  public int ownerUid;
  
  public boolean paused;
  
  private long ptr;
  
  public boolean replaceTouchableRegionWithCrop;
  
  public float scaleFactor;
  
  public int surfaceInset;
  
  public IBinder token;
  
  public boolean visible;
  
  public InputWindowHandle(InputApplicationHandle paramInputApplicationHandle, int paramInt) {
    this.inputApplicationHandle = paramInputApplicationHandle;
    this.displayId = paramInt;
  }
  
  public String toString() {
    String str = this.name;
    if (str == null)
      str = ""; 
    StringBuilder stringBuilder = new StringBuilder(str);
    stringBuilder.append(", frame=[");
    stringBuilder.append(this.frameLeft);
    stringBuilder.append(",");
    stringBuilder.append(this.frameTop);
    stringBuilder.append(",");
    int i = this.frameRight;
    stringBuilder.append(i);
    stringBuilder.append(",");
    stringBuilder.append(this.frameBottom);
    stringBuilder.append("]");
    stringBuilder.append(", touchableRegion=");
    stringBuilder.append(this.touchableRegion);
    stringBuilder.append(", visible=");
    stringBuilder.append(this.visible);
    return stringBuilder.toString();
  }
  
  protected void finalize() throws Throwable {
    try {
      nativeDispose();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public void replaceTouchableRegionWithCrop(SurfaceControl paramSurfaceControl) {
    setTouchableRegionCrop(paramSurfaceControl);
    this.replaceTouchableRegionWithCrop = true;
  }
  
  public void setTouchableRegionCrop(SurfaceControl paramSurfaceControl) {
    this.touchableRegionSurfaceControl = new WeakReference<>(paramSurfaceControl);
  }
  
  private native void nativeDispose();
}
