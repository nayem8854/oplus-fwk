package android.view;

import android.app.ActivityManager;
import android.graphics.Rect;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRecentsAnimationController extends IInterface {
  void cleanupScreenshot() throws RemoteException;
  
  void finish(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void finishZoom(boolean paramBoolean1, boolean paramBoolean2, int paramInt1, int paramInt2, Rect paramRect, int paramInt3, Bundle paramBundle) throws RemoteException;
  
  void hideCurrentInputMethod() throws RemoteException;
  
  boolean removeTask(int paramInt) throws RemoteException;
  
  ActivityManager.TaskSnapshot screenshotTask(int paramInt) throws RemoteException;
  
  void setAnimationTargetsBehindSystemBars(boolean paramBoolean) throws RemoteException;
  
  void setDeferCancelUntilNextTransition(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void setInputConsumerEnabled(boolean paramBoolean) throws RemoteException;
  
  void setWillFinishToHome(boolean paramBoolean) throws RemoteException;
  
  class Default implements IRecentsAnimationController {
    public ActivityManager.TaskSnapshot screenshotTask(int param1Int) throws RemoteException {
      return null;
    }
    
    public void finish(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void setInputConsumerEnabled(boolean param1Boolean) throws RemoteException {}
    
    public void setAnimationTargetsBehindSystemBars(boolean param1Boolean) throws RemoteException {}
    
    public void hideCurrentInputMethod() throws RemoteException {}
    
    public void cleanupScreenshot() throws RemoteException {}
    
    public void setDeferCancelUntilNextTransition(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void setWillFinishToHome(boolean param1Boolean) throws RemoteException {}
    
    public boolean removeTask(int param1Int) throws RemoteException {
      return false;
    }
    
    public void finishZoom(boolean param1Boolean1, boolean param1Boolean2, int param1Int1, int param1Int2, Rect param1Rect, int param1Int3, Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRecentsAnimationController {
    private static final String DESCRIPTOR = "android.view.IRecentsAnimationController";
    
    static final int TRANSACTION_cleanupScreenshot = 6;
    
    static final int TRANSACTION_finish = 2;
    
    static final int TRANSACTION_finishZoom = 10;
    
    static final int TRANSACTION_hideCurrentInputMethod = 5;
    
    static final int TRANSACTION_removeTask = 9;
    
    static final int TRANSACTION_screenshotTask = 1;
    
    static final int TRANSACTION_setAnimationTargetsBehindSystemBars = 4;
    
    static final int TRANSACTION_setDeferCancelUntilNextTransition = 7;
    
    static final int TRANSACTION_setInputConsumerEnabled = 3;
    
    static final int TRANSACTION_setWillFinishToHome = 8;
    
    public Stub() {
      attachInterface(this, "android.view.IRecentsAnimationController");
    }
    
    public static IRecentsAnimationController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IRecentsAnimationController");
      if (iInterface != null && iInterface instanceof IRecentsAnimationController)
        return (IRecentsAnimationController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "finishZoom";
        case 9:
          return "removeTask";
        case 8:
          return "setWillFinishToHome";
        case 7:
          return "setDeferCancelUntilNextTransition";
        case 6:
          return "cleanupScreenshot";
        case 5:
          return "hideCurrentInputMethod";
        case 4:
          return "setAnimationTargetsBehindSystemBars";
        case 3:
          return "setInputConsumerEnabled";
        case 2:
          return "finish";
        case 1:
          break;
      } 
      return "screenshotTask";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        Rect rect;
        int j;
        boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.view.IRecentsAnimationController");
            if (param1Parcel1.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            if (param1Parcel1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              rect = (Rect)Rect.CREATOR.createFromParcel(param1Parcel1);
            } else {
              rect = null;
            } 
            j = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            finishZoom(bool3, bool4, param1Int2, param1Int1, rect, j, (Bundle)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.view.IRecentsAnimationController");
            param1Int1 = param1Parcel1.readInt();
            bool = removeTask(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.view.IRecentsAnimationController");
            bool3 = bool5;
            if (param1Parcel1.readInt() != 0)
              bool3 = true; 
            setWillFinishToHome(bool3);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.view.IRecentsAnimationController");
            if (param1Parcel1.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            bool4 = bool1;
            if (param1Parcel1.readInt() != 0)
              bool4 = true; 
            setDeferCancelUntilNextTransition(bool3, bool4);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.view.IRecentsAnimationController");
            cleanupScreenshot();
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.view.IRecentsAnimationController");
            hideCurrentInputMethod();
            param1Parcel2.writeNoException();
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.view.IRecentsAnimationController");
            bool3 = bool2;
            if (param1Parcel1.readInt() != 0)
              bool3 = true; 
            setAnimationTargetsBehindSystemBars(bool3);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.view.IRecentsAnimationController");
            if (param1Parcel1.readInt() != 0)
              bool3 = true; 
            setInputConsumerEnabled(bool3);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.view.IRecentsAnimationController");
            if (param1Parcel1.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            if (param1Parcel1.readInt() != 0)
              bool4 = true; 
            finish(bool3, bool4);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.view.IRecentsAnimationController");
        int i = param1Parcel1.readInt();
        ActivityManager.TaskSnapshot taskSnapshot = screenshotTask(i);
        param1Parcel2.writeNoException();
        if (taskSnapshot != null) {
          param1Parcel2.writeInt(1);
          taskSnapshot.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("android.view.IRecentsAnimationController");
      return true;
    }
    
    private static class Proxy implements IRecentsAnimationController {
      public static IRecentsAnimationController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IRecentsAnimationController";
      }
      
      public ActivityManager.TaskSnapshot screenshotTask(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ActivityManager.TaskSnapshot taskSnapshot;
          parcel1.writeInterfaceToken("android.view.IRecentsAnimationController");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IRecentsAnimationController.Stub.getDefaultImpl() != null) {
            taskSnapshot = IRecentsAnimationController.Stub.getDefaultImpl().screenshotTask(param2Int);
            return taskSnapshot;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            taskSnapshot = (ActivityManager.TaskSnapshot)ActivityManager.TaskSnapshot.CREATOR.createFromParcel(parcel2);
          } else {
            taskSnapshot = null;
          } 
          return taskSnapshot;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finish(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.view.IRecentsAnimationController");
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IRecentsAnimationController.Stub.getDefaultImpl() != null) {
            IRecentsAnimationController.Stub.getDefaultImpl().finish(param2Boolean1, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInputConsumerEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IRecentsAnimationController");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && IRecentsAnimationController.Stub.getDefaultImpl() != null) {
            IRecentsAnimationController.Stub.getDefaultImpl().setInputConsumerEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAnimationTargetsBehindSystemBars(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IRecentsAnimationController");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool1 && IRecentsAnimationController.Stub.getDefaultImpl() != null) {
            IRecentsAnimationController.Stub.getDefaultImpl().setAnimationTargetsBehindSystemBars(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void hideCurrentInputMethod() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IRecentsAnimationController");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IRecentsAnimationController.Stub.getDefaultImpl() != null) {
            IRecentsAnimationController.Stub.getDefaultImpl().hideCurrentInputMethod();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cleanupScreenshot() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IRecentsAnimationController");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IRecentsAnimationController.Stub.getDefaultImpl() != null) {
            IRecentsAnimationController.Stub.getDefaultImpl().cleanupScreenshot();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDeferCancelUntilNextTransition(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.view.IRecentsAnimationController");
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IRecentsAnimationController.Stub.getDefaultImpl() != null) {
            IRecentsAnimationController.Stub.getDefaultImpl().setDeferCancelUntilNextTransition(param2Boolean1, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setWillFinishToHome(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IRecentsAnimationController");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool1 && IRecentsAnimationController.Stub.getDefaultImpl() != null) {
            IRecentsAnimationController.Stub.getDefaultImpl().setWillFinishToHome(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeTask(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IRecentsAnimationController");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IRecentsAnimationController.Stub.getDefaultImpl() != null) {
            bool1 = IRecentsAnimationController.Stub.getDefaultImpl().removeTask(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finishZoom(boolean param2Boolean1, boolean param2Boolean2, int param2Int1, int param2Int2, Rect param2Rect, int param2Int3, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.view.IRecentsAnimationController");
          if (param2Boolean1) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          if (param2Boolean2) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              if (param2Rect != null) {
                parcel1.writeInt(1);
                param2Rect.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeInt(param2Int3);
                if (param2Bundle != null) {
                  parcel1.writeInt(1);
                  param2Bundle.writeToParcel(parcel1, 0);
                } else {
                  parcel1.writeInt(0);
                } 
                boolean bool1 = this.mRemote.transact(10, parcel1, parcel2, 0);
                if (!bool1 && IRecentsAnimationController.Stub.getDefaultImpl() != null) {
                  IRecentsAnimationController.Stub.getDefaultImpl().finishZoom(param2Boolean1, param2Boolean2, param2Int1, param2Int2, param2Rect, param2Int3, param2Bundle);
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } 
                parcel2.readException();
                parcel2.recycle();
                parcel1.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2Rect;
      }
    }
    
    public static boolean setDefaultImpl(IRecentsAnimationController param1IRecentsAnimationController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRecentsAnimationController != null) {
          Proxy.sDefaultImpl = param1IRecentsAnimationController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRecentsAnimationController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
