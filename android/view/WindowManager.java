package android.view;

import android.annotation.SystemApi;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.util.proto.ProtoOutputStream;
import android.view.accessibility.AccessibilityNodeInfo;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public interface WindowManager extends ViewManager {
  public static final int DOCKED_BOTTOM = 4;
  
  public static final int DOCKED_INVALID = -1;
  
  public static final int DOCKED_LEFT = 1;
  
  public static final int DOCKED_RIGHT = 3;
  
  public static final int DOCKED_TOP = 2;
  
  public static final String INPUT_CONSUMER_NAVIGATION = "nav_input_consumer";
  
  public static final String INPUT_CONSUMER_PIP = "pip_input_consumer";
  
  public static final String INPUT_CONSUMER_RECENTS_ANIMATION = "recents_animation_input_consumer";
  
  public static final String INPUT_CONSUMER_WALLPAPER = "wallpaper_input_consumer";
  
  public static final String PARCEL_KEY_SHORTCUTS_ARRAY = "shortcuts_array";
  
  public static final int REMOVE_CONTENT_MODE_DESTROY = 2;
  
  public static final int REMOVE_CONTENT_MODE_MOVE_TO_PRIMARY = 1;
  
  public static final int REMOVE_CONTENT_MODE_UNDEFINED = 0;
  
  public static final int TAKE_SCREENSHOT_FULLSCREEN = 1;
  
  public static final int TAKE_SCREENSHOT_PROVIDED_IMAGE = 3;
  
  public static final int TAKE_SCREENSHOT_SELECTED_REGION = 2;
  
  public static final int TRANSIT_ACTIVITY_CLOSE = 7;
  
  public static final int TRANSIT_ACTIVITY_OPEN = 6;
  
  public static final int TRANSIT_ACTIVITY_RELAUNCH = 18;
  
  public static final int TRANSIT_CRASHING_ACTIVITY_CLOSE = 26;
  
  public static final int TRANSIT_DOCK_TASK_FROM_RECENTS = 19;
  
  public static final int TRANSIT_FLAG_KEYGUARD_GOING_AWAY_NO_ANIMATION = 2;
  
  public static final int TRANSIT_FLAG_KEYGUARD_GOING_AWAY_SUBTLE_ANIMATION = 8;
  
  public static final int TRANSIT_FLAG_KEYGUARD_GOING_AWAY_TO_SHADE = 1;
  
  public static final int TRANSIT_FLAG_KEYGUARD_GOING_AWAY_WITH_WALLPAPER = 4;
  
  public static final int TRANSIT_KEYGUARD_GOING_AWAY = 20;
  
  public static final int TRANSIT_KEYGUARD_GOING_AWAY_ON_WALLPAPER = 21;
  
  public static final int TRANSIT_KEYGUARD_OCCLUDE = 22;
  
  public static final int TRANSIT_KEYGUARD_UNOCCLUDE = 23;
  
  public static final int TRANSIT_NONE = 0;
  
  public static final int TRANSIT_SHOW_SINGLE_TASK_DISPLAY = 28;
  
  public static final int TRANSIT_TASK_CHANGE_WINDOWING_MODE = 27;
  
  public static final int TRANSIT_TASK_CLOSE = 9;
  
  public static final int TRANSIT_TASK_OPEN = 8;
  
  public static final int TRANSIT_TASK_OPEN_BEHIND = 16;
  
  public static final int TRANSIT_TASK_TO_BACK = 11;
  
  public static final int TRANSIT_TASK_TO_FRONT = 10;
  
  public static final int TRANSIT_TRANSLUCENT_ACTIVITY_CLOSE = 25;
  
  public static final int TRANSIT_TRANSLUCENT_ACTIVITY_OPEN = 24;
  
  public static final int TRANSIT_UNSET = -1;
  
  public static final int TRANSIT_WALLPAPER_CLOSE = 12;
  
  public static final int TRANSIT_WALLPAPER_INTRA_CLOSE = 15;
  
  public static final int TRANSIT_WALLPAPER_INTRA_OPEN = 14;
  
  public static final int TRANSIT_WALLPAPER_OPEN = 13;
  
  class BadTokenException extends RuntimeException {
    public BadTokenException() {}
    
    public BadTokenException(WindowManager this$0) {
      super((String)this$0);
    }
  }
  
  class InvalidDisplayException extends RuntimeException {
    public InvalidDisplayException() {}
    
    public InvalidDisplayException(WindowManager this$0) {
      super((String)this$0);
    }
  }
  
  default WindowMetrics getCurrentWindowMetrics() {
    throw new UnsupportedOperationException();
  }
  
  default WindowMetrics getMaximumWindowMetrics() {
    throw new UnsupportedOperationException();
  }
  
  default void setShouldShowWithInsecureKeyguard(int paramInt, boolean paramBoolean) {}
  
  default void setShouldShowSystemDecors(int paramInt, boolean paramBoolean) {}
  
  default boolean shouldShowSystemDecors(int paramInt) {
    return false;
  }
  
  default void setShouldShowIme(int paramInt, boolean paramBoolean) {}
  
  default boolean shouldShowIme(int paramInt) {
    return false;
  }
  
  @SystemApi
  Region getCurrentImeTouchRegion();
  
  @Deprecated
  Display getDefaultDisplay();
  
  void removeViewImmediate(View paramView);
  
  void requestAppKeyboardShortcuts(KeyboardShortcutsReceiver paramKeyboardShortcutsReceiver, int paramInt);
  
  class KeyboardShortcutsReceiver {
    public abstract void onKeyboardShortcutsReceived(List<KeyboardShortcutGroup> param1List);
  }
  
  class LayoutParams extends OplusBaseLayoutParams implements Parcelable {
    public static final int ACCESSIBILITY_ANCHOR_CHANGED = 16777216;
    
    public static final int ACCESSIBILITY_TITLE_CHANGED = 33554432;
    
    public static final int ALPHA_CHANGED = 128;
    
    public static final int ANIMATION_CHANGED = 16;
    
    public static final float BRIGHTNESS_OVERRIDE_FULL = 1.0F;
    
    public static final float BRIGHTNESS_OVERRIDE_NONE = -1.0F;
    
    public static final float BRIGHTNESS_OVERRIDE_OFF = 0.0F;
    
    public static final int BUTTON_BRIGHTNESS_CHANGED = 8192;
    
    public static final int COLOR_MODE_CHANGED = 67108864;
    
    public static boolean isSystemAlertWindowType(int param1Int) {
      if (param1Int != 2002 && param1Int != 2003 && param1Int != 2006 && param1Int != 2007 && param1Int != 2010 && param1Int != 2038)
        return false; 
      return true;
    }
    
    public static boolean mayUseInputMethod(int param1Int) {
      boolean bool;
      if ((param1Int & 0x8) != 8 && (param1Int & 0x20000) != 131072) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void setFitInsetsTypes(int param1Int) {
      this.mFitInsetsTypes = param1Int;
      this.privateFlags |= 0x10000000;
    }
    
    public void setFitInsetsSides(int param1Int) {
      this.mFitInsetsSides = param1Int;
      this.privateFlags |= 0x10000000;
    }
    
    public void setFitInsetsIgnoringVisibility(boolean param1Boolean) {
      this.mFitInsetsIgnoringVisibility = param1Boolean;
      this.privateFlags |= 0x10000000;
    }
    
    public int getFitInsetsTypes() {
      return this.mFitInsetsTypes;
    }
    
    public int getFitInsetsSides() {
      return this.mFitInsetsSides;
    }
    
    public boolean isFitInsetsIgnoringVisibility() {
      return this.mFitInsetsIgnoringVisibility;
    }
    
    public LayoutParams() {
      super(-1, -1);
      this.surfaceInsets = new Rect();
      this.preservePreviousSurfaceInsets = true;
      this.alpha = 1.0F;
      this.dimAmount = 1.0F;
      this.screenBrightness = -1.0F;
      this.buttonBrightness = -1.0F;
      this.rotationAnimation = 0;
      this.token = null;
      this.packageName = null;
      this.screenOrientation = -1;
      this.layoutInDisplayCutoutMode = 0;
      this.userActivityTimeout = -1L;
      this.accessibilityIdOfAnchor = AccessibilityNodeInfo.UNDEFINED_NODE_ID;
      this.hideTimeoutMilliseconds = -1L;
      this.preferMinimalPostProcessing = false;
      this.mColorMode = 0;
      this.insetsFlags = new InsetsFlags();
      this.mFitInsetsTypes = WindowInsets.Type.systemBars();
      this.mFitInsetsSides = WindowInsets.Side.all();
      this.mFitInsetsIgnoringVisibility = false;
      this.mCompatibilityParamsBackup = null;
      this.mTitle = null;
      this.type = 2;
      this.format = -1;
    }
    
    public LayoutParams(WindowManager this$0) {
      super(-1, -1);
      this.surfaceInsets = new Rect();
      this.preservePreviousSurfaceInsets = true;
      this.alpha = 1.0F;
      this.dimAmount = 1.0F;
      this.screenBrightness = -1.0F;
      this.buttonBrightness = -1.0F;
      this.rotationAnimation = 0;
      this.token = null;
      this.packageName = null;
      this.screenOrientation = -1;
      this.layoutInDisplayCutoutMode = 0;
      this.userActivityTimeout = -1L;
      this.accessibilityIdOfAnchor = AccessibilityNodeInfo.UNDEFINED_NODE_ID;
      this.hideTimeoutMilliseconds = -1L;
      this.preferMinimalPostProcessing = false;
      this.mColorMode = 0;
      this.insetsFlags = new InsetsFlags();
      this.mFitInsetsTypes = WindowInsets.Type.systemBars();
      this.mFitInsetsSides = WindowInsets.Side.all();
      this.mFitInsetsIgnoringVisibility = false;
      this.mCompatibilityParamsBackup = null;
      this.mTitle = null;
      this.type = this$0;
      this.format = -1;
    }
    
    public LayoutParams(WindowManager this$0, int param1Int1) {
      super(-1, -1);
      this.surfaceInsets = new Rect();
      this.preservePreviousSurfaceInsets = true;
      this.alpha = 1.0F;
      this.dimAmount = 1.0F;
      this.screenBrightness = -1.0F;
      this.buttonBrightness = -1.0F;
      this.rotationAnimation = 0;
      this.token = null;
      this.packageName = null;
      this.screenOrientation = -1;
      this.layoutInDisplayCutoutMode = 0;
      this.userActivityTimeout = -1L;
      this.accessibilityIdOfAnchor = AccessibilityNodeInfo.UNDEFINED_NODE_ID;
      this.hideTimeoutMilliseconds = -1L;
      this.preferMinimalPostProcessing = false;
      this.mColorMode = 0;
      this.insetsFlags = new InsetsFlags();
      this.mFitInsetsTypes = WindowInsets.Type.systemBars();
      this.mFitInsetsSides = WindowInsets.Side.all();
      this.mFitInsetsIgnoringVisibility = false;
      this.mCompatibilityParamsBackup = null;
      this.mTitle = null;
      this.type = this$0;
      this.flags = param1Int1;
      this.format = -1;
    }
    
    public LayoutParams(WindowManager this$0, int param1Int1, int param1Int2) {
      super(-1, -1);
      this.surfaceInsets = new Rect();
      this.preservePreviousSurfaceInsets = true;
      this.alpha = 1.0F;
      this.dimAmount = 1.0F;
      this.screenBrightness = -1.0F;
      this.buttonBrightness = -1.0F;
      this.rotationAnimation = 0;
      this.token = null;
      this.packageName = null;
      this.screenOrientation = -1;
      this.layoutInDisplayCutoutMode = 0;
      this.userActivityTimeout = -1L;
      this.accessibilityIdOfAnchor = AccessibilityNodeInfo.UNDEFINED_NODE_ID;
      this.hideTimeoutMilliseconds = -1L;
      this.preferMinimalPostProcessing = false;
      this.mColorMode = 0;
      this.insetsFlags = new InsetsFlags();
      this.mFitInsetsTypes = WindowInsets.Type.systemBars();
      this.mFitInsetsSides = WindowInsets.Side.all();
      this.mFitInsetsIgnoringVisibility = false;
      this.mCompatibilityParamsBackup = null;
      this.mTitle = null;
      this.type = this$0;
      this.flags = param1Int1;
      this.format = param1Int2;
    }
    
    public LayoutParams(WindowManager this$0, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      super(this$0, param1Int1);
      this.surfaceInsets = new Rect();
      this.preservePreviousSurfaceInsets = true;
      this.alpha = 1.0F;
      this.dimAmount = 1.0F;
      this.screenBrightness = -1.0F;
      this.buttonBrightness = -1.0F;
      this.rotationAnimation = 0;
      this.token = null;
      this.packageName = null;
      this.screenOrientation = -1;
      this.layoutInDisplayCutoutMode = 0;
      this.userActivityTimeout = -1L;
      this.accessibilityIdOfAnchor = AccessibilityNodeInfo.UNDEFINED_NODE_ID;
      this.hideTimeoutMilliseconds = -1L;
      this.preferMinimalPostProcessing = false;
      this.mColorMode = 0;
      this.insetsFlags = new InsetsFlags();
      this.mFitInsetsTypes = WindowInsets.Type.systemBars();
      this.mFitInsetsSides = WindowInsets.Side.all();
      this.mFitInsetsIgnoringVisibility = false;
      this.mCompatibilityParamsBackup = null;
      this.mTitle = null;
      this.type = param1Int2;
      this.flags = param1Int3;
      this.format = param1Int4;
    }
    
    public LayoutParams(WindowManager this$0, int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6) {
      super(this$0, param1Int1);
      this.surfaceInsets = new Rect();
      this.preservePreviousSurfaceInsets = true;
      this.alpha = 1.0F;
      this.dimAmount = 1.0F;
      this.screenBrightness = -1.0F;
      this.buttonBrightness = -1.0F;
      this.rotationAnimation = 0;
      this.token = null;
      this.packageName = null;
      this.screenOrientation = -1;
      this.layoutInDisplayCutoutMode = 0;
      this.userActivityTimeout = -1L;
      this.accessibilityIdOfAnchor = AccessibilityNodeInfo.UNDEFINED_NODE_ID;
      this.hideTimeoutMilliseconds = -1L;
      this.preferMinimalPostProcessing = false;
      this.mColorMode = 0;
      this.insetsFlags = new InsetsFlags();
      this.mFitInsetsTypes = WindowInsets.Type.systemBars();
      this.mFitInsetsSides = WindowInsets.Side.all();
      this.mFitInsetsIgnoringVisibility = false;
      this.mCompatibilityParamsBackup = null;
      this.mTitle = null;
      this.x = param1Int2;
      this.y = param1Int3;
      this.type = param1Int4;
      this.flags = param1Int5;
      this.format = param1Int6;
    }
    
    public final void setTitle(CharSequence param1CharSequence) {
      CharSequence charSequence = param1CharSequence;
      if (param1CharSequence == null)
        charSequence = ""; 
      this.mTitle = TextUtils.stringOrSpannedString(charSequence);
    }
    
    public final CharSequence getTitle() {
      CharSequence charSequence = this.mTitle;
      if (charSequence == null)
        charSequence = ""; 
      return charSequence;
    }
    
    public final void setSurfaceInsets(View param1View, boolean param1Boolean1, boolean param1Boolean2) {
      int i = (int)Math.ceil((param1View.getZ() * 2.0F));
      if (i == 0) {
        this.surfaceInsets.set(0, 0, 0, 0);
      } else {
        Rect rect = this.surfaceInsets;
        int j = rect.left;
        j = Math.max(i, j);
        int k = this.surfaceInsets.top;
        k = Math.max(i, k);
        int m = this.surfaceInsets.right;
        m = Math.max(i, m);
        int n = this.surfaceInsets.bottom;
        i = Math.max(i, n);
        rect.set(j, k, m, i);
      } 
      this.hasManualSurfaceInsets = param1Boolean1;
      this.preservePreviousSurfaceInsets = param1Boolean2;
    }
    
    public void setColorMode(int param1Int) {
      this.mColorMode = param1Int;
    }
    
    public int getColorMode() {
      return this.mColorMode;
    }
    
    @SystemApi
    public final void setUserActivityTimeout(long param1Long) {
      this.userActivityTimeout = param1Long;
    }
    
    @SystemApi
    public final long getUserActivityTimeout() {
      return this.userActivityTimeout;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.width);
      param1Parcel.writeInt(this.height);
      param1Parcel.writeInt(this.x);
      param1Parcel.writeInt(this.y);
      param1Parcel.writeInt(this.type);
      param1Parcel.writeInt(this.flags);
      param1Parcel.writeInt(this.privateFlags);
      param1Parcel.writeInt(this.softInputMode);
      param1Parcel.writeInt(this.layoutInDisplayCutoutMode);
      param1Parcel.writeInt(this.gravity);
      param1Parcel.writeFloat(this.horizontalMargin);
      param1Parcel.writeFloat(this.verticalMargin);
      param1Parcel.writeInt(this.format);
      param1Parcel.writeInt(this.windowAnimations);
      param1Parcel.writeFloat(this.alpha);
      param1Parcel.writeFloat(this.dimAmount);
      param1Parcel.writeFloat(this.screenBrightness);
      param1Parcel.writeFloat(this.buttonBrightness);
      param1Parcel.writeInt(this.rotationAnimation);
      param1Parcel.writeStrongBinder(this.token);
      param1Parcel.writeString(this.packageName);
      TextUtils.writeToParcel(this.mTitle, param1Parcel, param1Int);
      param1Parcel.writeInt(this.screenOrientation);
      param1Parcel.writeFloat(this.preferredRefreshRate);
      param1Parcel.writeInt(this.preferredDisplayModeId);
      param1Parcel.writeInt(this.systemUiVisibility);
      param1Parcel.writeInt(this.subtreeSystemUiVisibility);
      param1Parcel.writeInt(this.hasSystemUiListeners);
      param1Parcel.writeInt(this.inputFeatures);
      param1Parcel.writeLong(this.userActivityTimeout);
      param1Parcel.writeInt(this.surfaceInsets.left);
      param1Parcel.writeInt(this.surfaceInsets.top);
      param1Parcel.writeInt(this.surfaceInsets.right);
      param1Parcel.writeInt(this.surfaceInsets.bottom);
      param1Parcel.writeInt(this.hasManualSurfaceInsets);
      param1Parcel.writeInt(this.preservePreviousSurfaceInsets);
      param1Parcel.writeLong(this.accessibilityIdOfAnchor);
      TextUtils.writeToParcel(this.accessibilityTitle, param1Parcel, param1Int);
      param1Parcel.writeInt(this.mColorMode);
      param1Parcel.writeLong(this.hideTimeoutMilliseconds);
      param1Parcel.writeInt(this.insetsFlags.appearance);
      param1Parcel.writeInt(this.insetsFlags.behavior);
      param1Parcel.writeInt(this.mFitInsetsTypes);
      param1Parcel.writeInt(this.mFitInsetsSides);
      param1Parcel.writeBoolean(this.mFitInsetsIgnoringVisibility);
      param1Parcel.writeBoolean(this.preferMinimalPostProcessing);
      int[] arrayOfInt = this.providesInsetsTypes;
      if (arrayOfInt != null) {
        param1Parcel.writeInt(arrayOfInt.length);
        param1Parcel.writeIntArray(this.providesInsetsTypes);
      } else {
        param1Parcel.writeInt(0);
      } 
      super.writeToParcel(param1Parcel, this.flags);
    }
    
    public static final Parcelable.Creator<LayoutParams> CREATOR = new Parcelable.Creator<LayoutParams>() {
        public WindowManager.LayoutParams createFromParcel(Parcel param1Parcel) {
          return new WindowManager.LayoutParams(param1Parcel);
        }
        
        public WindowManager.LayoutParams[] newArray(int param1Int) {
          return new WindowManager.LayoutParams[param1Int];
        }
      };
    
    public static final int DIM_AMOUNT_CHANGED = 32;
    
    public static final int FIRST_APPLICATION_WINDOW = 1;
    
    public static final int FIRST_SUB_WINDOW = 1000;
    
    public static final int FIRST_SYSTEM_WINDOW = 2000;
    
    public static final int FLAGS_CHANGED = 4;
    
    public static final int FLAG_ALLOW_LOCK_WHILE_SCREEN_ON = 1;
    
    public static final int FLAG_ALT_FOCUSABLE_IM = 131072;
    
    @Deprecated
    public static final int FLAG_BLUR_BEHIND = 4;
    
    public static final int FLAG_DIM_BEHIND = 2;
    
    @Deprecated
    public static final int FLAG_DISMISS_KEYGUARD = 4194304;
    
    @Deprecated
    public static final int FLAG_DITHER = 4096;
    
    public static final int FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS = -2147483648;
    
    @Deprecated
    public static final int FLAG_FORCE_NOT_FULLSCREEN = 2048;
    
    @Deprecated
    public static final int FLAG_FULLSCREEN = 1024;
    
    public static final int FLAG_HARDWARE_ACCELERATED = 16777216;
    
    public static final int FLAG_IGNORE_CHEEK_PRESSES = 32768;
    
    public static final int FLAG_KEEP_SCREEN_ON = 128;
    
    @Deprecated
    public static final int FLAG_LAYOUT_ATTACHED_IN_DECOR = 1073741824;
    
    @Deprecated
    public static final int FLAG_LAYOUT_INSET_DECOR = 65536;
    
    @Deprecated
    public static final int FLAG_LAYOUT_IN_OVERSCAN = 33554432;
    
    public static final int FLAG_LAYOUT_IN_SCREEN = 256;
    
    public static final int FLAG_LAYOUT_NO_LIMITS = 512;
    
    public static final int FLAG_LOCAL_FOCUS_MODE = 268435456;
    
    public static final int FLAG_NOT_FOCUSABLE = 8;
    
    public static final int FLAG_NOT_TOUCHABLE = 16;
    
    public static final int FLAG_NOT_TOUCH_MODAL = 32;
    
    public static final int FLAG_SCALED = 16384;
    
    public static final int FLAG_SECURE = 8192;
    
    public static final int FLAG_SHOW_WALLPAPER = 1048576;
    
    @Deprecated
    public static final int FLAG_SHOW_WHEN_LOCKED = 524288;
    
    public static final int FLAG_SLIPPERY = 536870912;
    
    public static final int FLAG_SPLIT_TOUCH = 8388608;
    
    @Deprecated
    public static final int FLAG_TOUCHABLE_WHEN_WAKING = 64;
    
    @Deprecated
    public static final int FLAG_TRANSLUCENT_NAVIGATION = 134217728;
    
    @Deprecated
    public static final int FLAG_TRANSLUCENT_STATUS = 67108864;
    
    @Deprecated
    public static final int FLAG_TURN_SCREEN_ON = 2097152;
    
    public static final int FLAG_WATCH_OUTSIDE_TOUCH = 262144;
    
    public static final int FORMAT_CHANGED = 8;
    
    public static final int INPUT_FEATURES_CHANGED = 65536;
    
    public static final int INPUT_FEATURE_DISABLE_POINTER_GESTURES = 1;
    
    public static final int INPUT_FEATURE_DISABLE_USER_ACTIVITY = 4;
    
    public static final int INPUT_FEATURE_NO_INPUT_CHANNEL = 2;
    
    public static final int INSET_FLAGS_CHANGED = 134217728;
    
    public static final int INVALID_WINDOW_TYPE = -1;
    
    public static final int LAST_APPLICATION_WINDOW = 99;
    
    public static final int LAST_SUB_WINDOW = 1999;
    
    public static final int LAST_SYSTEM_WINDOW = 2999;
    
    public static final int LAYOUT_CHANGED = 1;
    
    public static final int LAYOUT_IN_DISPLAY_CUTOUT_MODE_ALWAYS = 3;
    
    public static final int LAYOUT_IN_DISPLAY_CUTOUT_MODE_DEFAULT = 0;
    
    public static final int LAYOUT_IN_DISPLAY_CUTOUT_MODE_NEVER = 2;
    
    public static final int LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES = 1;
    
    public static final int MEMORY_TYPE_CHANGED = 256;
    
    @Deprecated
    public static final int MEMORY_TYPE_GPU = 2;
    
    @Deprecated
    public static final int MEMORY_TYPE_HARDWARE = 1;
    
    @Deprecated
    public static final int MEMORY_TYPE_NORMAL = 0;
    
    @Deprecated
    public static final int MEMORY_TYPE_PUSH_BUFFERS = 3;
    
    public static final int MINIMAL_POST_PROCESSING_PREFERENCE_CHANGED = 268435456;
    
    public static final int PREFERRED_DISPLAY_MODE_ID = 8388608;
    
    public static final int PREFERRED_REFRESH_RATE_CHANGED = 2097152;
    
    public static final int PRIVATE_FLAGS_CHANGED = 131072;
    
    public static final int PRIVATE_FLAG_APPEARANCE_CONTROLLED = 67108864;
    
    public static final int PRIVATE_FLAG_BEHAVIOR_CONTROLLED = 134217728;
    
    public static final int PRIVATE_FLAG_COLOR_SPACE_AGNOSTIC = 16777216;
    
    public static final int PRIVATE_FLAG_COMPATIBLE_WINDOW = 128;
    
    public static final int PRIVATE_FLAG_DISABLE_WALLPAPER_TOUCH_EVENTS = 2048;
    
    public static final int PRIVATE_FLAG_FAKE_HARDWARE_ACCELERATED = 1;
    
    public static final int PRIVATE_FLAG_FIT_INSETS_CONTROLLED = 268435456;
    
    public static final int PRIVATE_FLAG_FORCE_DECOR_VIEW_VISIBILITY = 16384;
    
    public static final int PRIVATE_FLAG_FORCE_DRAW_BAR_BACKGROUNDS = 131072;
    
    public static final int PRIVATE_FLAG_FORCE_HARDWARE_ACCELERATED = 2;
    
    public static final int PRIVATE_FLAG_FORCE_SHOW_STATUS_BAR = 4096;
    
    public static final int PRIVATE_FLAG_IS_ROUNDED_CORNERS_OVERLAY = 1048576;
    
    public static final int PRIVATE_FLAG_IS_SCREEN_DECOR = 4194304;
    
    public static final int PRIVATE_FLAG_LAYOUT_CHILD_WINDOW_IN_PARENT_FRAME = 65536;
    
    public static final int PRIVATE_FLAG_NO_MOVE_ANIMATION = 64;
    
    public static final int PRIVATE_FLAG_PRESERVE_GEOMETRY = 8192;
    
    public static final int PRIVATE_FLAG_STATUS_FORCE_SHOW_NAVIGATION = 8388608;
    
    public static final int PRIVATE_FLAG_SUSTAINED_PERFORMANCE_MODE = 262144;
    
    public static final int PRIVATE_FLAG_SYSTEM_ERROR = 256;
    
    public static final int PRIVATE_FLAG_USE_BLAST = 33554432;
    
    public static final int PRIVATE_FLAG_WANTS_OFFSET_NOTIFICATIONS = 4;
    
    public static final int PRIVATE_FLAG_WILL_NOT_REPLACE_ON_RELAUNCH = 32768;
    
    public static final int ROTATION_ANIMATION_CHANGED = 4096;
    
    public static final int ROTATION_ANIMATION_CROSSFADE = 1;
    
    public static final int ROTATION_ANIMATION_JUMPCUT = 2;
    
    public static final int ROTATION_ANIMATION_ROTATE = 0;
    
    public static final int ROTATION_ANIMATION_SEAMLESS = 3;
    
    public static final int ROTATION_ANIMATION_UNSPECIFIED = -1;
    
    public static final int SCREEN_BRIGHTNESS_CHANGED = 2048;
    
    public static final int SCREEN_ORIENTATION_CHANGED = 1024;
    
    public static final int SOFT_INPUT_ADJUST_NOTHING = 48;
    
    public static final int SOFT_INPUT_ADJUST_PAN = 32;
    
    @Deprecated
    public static final int SOFT_INPUT_ADJUST_RESIZE = 16;
    
    public static final int SOFT_INPUT_ADJUST_UNSPECIFIED = 0;
    
    public static final int SOFT_INPUT_IS_FORWARD_NAVIGATION = 256;
    
    public static final int SOFT_INPUT_MASK_ADJUST = 240;
    
    public static final int SOFT_INPUT_MASK_STATE = 15;
    
    public static final int SOFT_INPUT_MODE_CHANGED = 512;
    
    public static final int SOFT_INPUT_STATE_ALWAYS_HIDDEN = 3;
    
    public static final int SOFT_INPUT_STATE_ALWAYS_VISIBLE = 5;
    
    public static final int SOFT_INPUT_STATE_HIDDEN = 2;
    
    public static final int SOFT_INPUT_STATE_UNCHANGED = 1;
    
    public static final int SOFT_INPUT_STATE_UNSPECIFIED = 0;
    
    public static final int SOFT_INPUT_STATE_VISIBLE = 4;
    
    public static final int SURFACE_INSETS_CHANGED = 1048576;
    
    @SystemApi
    public static final int SYSTEM_FLAG_HIDE_NON_SYSTEM_OVERLAY_WINDOWS = 524288;
    
    @SystemApi
    public static final int SYSTEM_FLAG_SHOW_FOR_ALL_USERS = 16;
    
    public static final int SYSTEM_UI_LISTENER_CHANGED = 32768;
    
    public static final int SYSTEM_UI_VISIBILITY_CHANGED = 16384;
    
    public static final int TITLE_CHANGED = 64;
    
    public static final int TRANSLUCENT_FLAGS_CHANGED = 524288;
    
    public static final int TYPE_ACCESSIBILITY_MAGNIFICATION_OVERLAY = 2039;
    
    public static final int TYPE_ACCESSIBILITY_OVERLAY = 2032;
    
    public static final int TYPE_APPLICATION = 2;
    
    public static final int TYPE_APPLICATION_ABOVE_SUB_PANEL = 1005;
    
    public static final int TYPE_APPLICATION_ATTACHED_DIALOG = 1003;
    
    public static final int TYPE_APPLICATION_MEDIA = 1001;
    
    public static final int TYPE_APPLICATION_MEDIA_OVERLAY = 1004;
    
    public static final int TYPE_APPLICATION_OVERLAY = 2038;
    
    public static final int TYPE_APPLICATION_PANEL = 1000;
    
    public static final int TYPE_APPLICATION_STARTING = 3;
    
    public static final int TYPE_APPLICATION_SUB_PANEL = 1002;
    
    public static final int TYPE_BASE_APPLICATION = 1;
    
    public static final int TYPE_BOOT_PROGRESS = 2021;
    
    public static final int TYPE_CHANGED = 2;
    
    public static final int TYPE_DISPLAY_OVERLAY = 2026;
    
    public static final int TYPE_DOCK_DIVIDER = 2034;
    
    public static final int TYPE_DRAG = 2016;
    
    public static final int TYPE_DRAWN_APPLICATION = 4;
    
    public static final int TYPE_INPUT_CONSUMER = 2022;
    
    public static final int TYPE_INPUT_METHOD = 2011;
    
    public static final int TYPE_INPUT_METHOD_DIALOG = 2012;
    
    public static final int TYPE_KEYGUARD = 2004;
    
    public static final int TYPE_KEYGUARD_DIALOG = 2009;
    
    public static final int TYPE_MAGNIFICATION_OVERLAY = 2027;
    
    public static final int TYPE_NAVIGATION_BAR = 2019;
    
    public static final int TYPE_NAVIGATION_BAR_PANEL = 2024;
    
    public static final int TYPE_NOTIFICATION_SHADE = 2040;
    
    @Deprecated
    public static final int TYPE_PHONE = 2002;
    
    public static final int TYPE_POINTER = 2018;
    
    public static final int TYPE_PRESENTATION = 2037;
    
    @Deprecated
    public static final int TYPE_PRIORITY_PHONE = 2007;
    
    public static final int TYPE_PRIVATE_PRESENTATION = 2030;
    
    public static final int TYPE_QS_DIALOG = 2035;
    
    public static final int TYPE_SCREENSHOT = 2036;
    
    public static final int TYPE_SEARCH_BAR = 2001;
    
    public static final int TYPE_SECURE_SYSTEM_OVERLAY = 2015;
    
    public static final int TYPE_STATUS_BAR = 2000;
    
    public static final int TYPE_STATUS_BAR_ADDITIONAL = 2041;
    
    @Deprecated
    public static final int TYPE_STATUS_BAR_PANEL = 2014;
    
    public static final int TYPE_STATUS_BAR_SUB_PANEL = 2017;
    
    @Deprecated
    public static final int TYPE_SYSTEM_ALERT = 2003;
    
    public static final int TYPE_SYSTEM_DIALOG = 2008;
    
    @Deprecated
    public static final int TYPE_SYSTEM_ERROR = 2010;
    
    @Deprecated
    public static final int TYPE_SYSTEM_OVERLAY = 2006;
    
    @Deprecated
    public static final int TYPE_TOAST = 2005;
    
    public static final int TYPE_TRUSTED_APPLICATION_OVERLAY = 2042;
    
    public static final int TYPE_VOICE_INTERACTION = 2031;
    
    public static final int TYPE_VOICE_INTERACTION_STARTING = 2033;
    
    public static final int TYPE_VOLUME_OVERLAY = 2020;
    
    public static final int TYPE_WALLPAPER = 2013;
    
    public static final int USER_ACTIVITY_TIMEOUT_CHANGED = 262144;
    
    public long accessibilityIdOfAnchor;
    
    public CharSequence accessibilityTitle;
    
    public float alpha;
    
    public float buttonBrightness;
    
    public float dimAmount;
    
    @ExportedProperty(flagMapping = {@FlagToString(equals = 1, mask = 1, name = "ALLOW_LOCK_WHILE_SCREEN_ON"), @FlagToString(equals = 2, mask = 2, name = "DIM_BEHIND"), @FlagToString(equals = 4, mask = 4, name = "BLUR_BEHIND"), @FlagToString(equals = 8, mask = 8, name = "NOT_FOCUSABLE"), @FlagToString(equals = 16, mask = 16, name = "NOT_TOUCHABLE"), @FlagToString(equals = 32, mask = 32, name = "NOT_TOUCH_MODAL"), @FlagToString(equals = 64, mask = 64, name = "TOUCHABLE_WHEN_WAKING"), @FlagToString(equals = 128, mask = 128, name = "KEEP_SCREEN_ON"), @FlagToString(equals = 256, mask = 256, name = "LAYOUT_IN_SCREEN"), @FlagToString(equals = 512, mask = 512, name = "LAYOUT_NO_LIMITS"), @FlagToString(equals = 1024, mask = 1024, name = "FULLSCREEN"), @FlagToString(equals = 2048, mask = 2048, name = "FORCE_NOT_FULLSCREEN"), @FlagToString(equals = 4096, mask = 4096, name = "DITHER"), @FlagToString(equals = 8192, mask = 8192, name = "SECURE"), @FlagToString(equals = 16384, mask = 16384, name = "SCALED"), @FlagToString(equals = 32768, mask = 32768, name = "IGNORE_CHEEK_PRESSES"), @FlagToString(equals = 65536, mask = 65536, name = "LAYOUT_INSET_DECOR"), @FlagToString(equals = 131072, mask = 131072, name = "ALT_FOCUSABLE_IM"), @FlagToString(equals = 262144, mask = 262144, name = "WATCH_OUTSIDE_TOUCH"), @FlagToString(equals = 524288, mask = 524288, name = "SHOW_WHEN_LOCKED"), @FlagToString(equals = 1048576, mask = 1048576, name = "SHOW_WALLPAPER"), @FlagToString(equals = 2097152, mask = 2097152, name = "TURN_SCREEN_ON"), @FlagToString(equals = 4194304, mask = 4194304, name = "DISMISS_KEYGUARD"), @FlagToString(equals = 8388608, mask = 8388608, name = "SPLIT_TOUCH"), @FlagToString(equals = 16777216, mask = 16777216, name = "HARDWARE_ACCELERATED"), @FlagToString(equals = 33554432, mask = 33554432, name = "LOCAL_FOCUS_MODE"), @FlagToString(equals = 67108864, mask = 67108864, name = "TRANSLUCENT_STATUS"), @FlagToString(equals = 134217728, mask = 134217728, name = "TRANSLUCENT_NAVIGATION"), @FlagToString(equals = 268435456, mask = 268435456, name = "LOCAL_FOCUS_MODE"), @FlagToString(equals = 536870912, mask = 536870912, name = "FLAG_SLIPPERY"), @FlagToString(equals = 1073741824, mask = 1073741824, name = "FLAG_LAYOUT_ATTACHED_IN_DECOR"), @FlagToString(equals = -2147483648, mask = -2147483648, name = "DRAWS_SYSTEM_BAR_BACKGROUNDS")}, formatToHexString = true)
    public int flags;
    
    public int format;
    
    public int gravity;
    
    public boolean hasManualSurfaceInsets;
    
    public boolean hasSystemUiListeners;
    
    public long hideTimeoutMilliseconds;
    
    public float horizontalMargin;
    
    @ExportedProperty
    public float horizontalWeight;
    
    public int inputFeatures;
    
    public final InsetsFlags insetsFlags;
    
    public int layoutInDisplayCutoutMode;
    
    private int mColorMode;
    
    private int[] mCompatibilityParamsBackup;
    
    private boolean mFitInsetsIgnoringVisibility;
    
    @ExportedProperty(flagMapping = {@FlagToString(equals = 1, mask = 1, name = "LEFT"), @FlagToString(equals = 2, mask = 2, name = "TOP"), @FlagToString(equals = 4, mask = 4, name = "RIGHT"), @FlagToString(equals = 8, mask = 8, name = "BOTTOM")})
    private int mFitInsetsSides;
    
    @ExportedProperty(flagMapping = {@FlagToString(equals = 1, mask = 1, name = "STATUS_BARS"), @FlagToString(equals = 2, mask = 2, name = "NAVIGATION_BARS"), @FlagToString(equals = 4, mask = 4, name = "CAPTION_BAR"), @FlagToString(equals = 8, mask = 8, name = "IME"), @FlagToString(equals = 16, mask = 16, name = "SYSTEM_GESTURES"), @FlagToString(equals = 32, mask = 32, name = "MANDATORY_SYSTEM_GESTURES"), @FlagToString(equals = 64, mask = 64, name = "TAPPABLE_ELEMENT"), @FlagToString(equals = 256, mask = 256, name = "WINDOW_DECOR")})
    private int mFitInsetsTypes;
    
    private CharSequence mTitle;
    
    @Deprecated
    public int memoryType;
    
    public String packageName;
    
    public boolean preferMinimalPostProcessing;
    
    public int preferredDisplayModeId;
    
    @Deprecated
    public float preferredRefreshRate;
    
    public boolean preservePreviousSurfaceInsets;
    
    @ExportedProperty(flagMapping = {@FlagToString(equals = 1, mask = 1, name = "FAKE_HARDWARE_ACCELERATED"), @FlagToString(equals = 2, mask = 2, name = "FORCE_HARDWARE_ACCELERATED"), @FlagToString(equals = 4, mask = 4, name = "WANTS_OFFSET_NOTIFICATIONS"), @FlagToString(equals = 16, mask = 16, name = "SHOW_FOR_ALL_USERS"), @FlagToString(equals = 64, mask = 64, name = "NO_MOVE_ANIMATION"), @FlagToString(equals = 128, mask = 128, name = "COMPATIBLE_WINDOW"), @FlagToString(equals = 256, mask = 256, name = "SYSTEM_ERROR"), @FlagToString(equals = 2048, mask = 2048, name = "DISABLE_WALLPAPER_TOUCH_EVENTS"), @FlagToString(equals = 4096, mask = 4096, name = "FORCE_STATUS_BAR_VISIBLE"), @FlagToString(equals = 8192, mask = 8192, name = "PRESERVE_GEOMETRY"), @FlagToString(equals = 16384, mask = 16384, name = "FORCE_DECOR_VIEW_VISIBILITY"), @FlagToString(equals = 32768, mask = 32768, name = "WILL_NOT_REPLACE_ON_RELAUNCH"), @FlagToString(equals = 65536, mask = 65536, name = "LAYOUT_CHILD_WINDOW_IN_PARENT_FRAME"), @FlagToString(equals = 131072, mask = 131072, name = "FORCE_DRAW_STATUS_BAR_BACKGROUND"), @FlagToString(equals = 262144, mask = 262144, name = "SUSTAINED_PERFORMANCE_MODE"), @FlagToString(equals = 524288, mask = 524288, name = "HIDE_NON_SYSTEM_OVERLAY_WINDOWS"), @FlagToString(equals = 1048576, mask = 1048576, name = "IS_ROUNDED_CORNERS_OVERLAY"), @FlagToString(equals = 4194304, mask = 4194304, name = "IS_SCREEN_DECOR"), @FlagToString(equals = 8388608, mask = 8388608, name = "STATUS_FORCE_SHOW_NAVIGATION"), @FlagToString(equals = 16777216, mask = 16777216, name = "COLOR_SPACE_AGNOSTIC"), @FlagToString(equals = 67108864, mask = 67108864, name = "APPEARANCE_CONTROLLED"), @FlagToString(equals = 134217728, mask = 134217728, name = "BEHAVIOR_CONTROLLED"), @FlagToString(equals = 268435456, mask = 268435456, name = "FIT_INSETS_CONTROLLED")})
    public int privateFlags;
    
    public int[] providesInsetsTypes;
    
    public int rotationAnimation;
    
    public float screenBrightness;
    
    public int screenOrientation;
    
    public int softInputMode;
    
    public int subtreeSystemUiVisibility;
    
    public final Rect surfaceInsets;
    
    @Deprecated
    public int systemUiVisibility;
    
    public IBinder token;
    
    @ExportedProperty(mapping = {@IntToString(from = 1, to = "BASE_APPLICATION"), @IntToString(from = 2, to = "APPLICATION"), @IntToString(from = 3, to = "APPLICATION_STARTING"), @IntToString(from = 4, to = "DRAWN_APPLICATION"), @IntToString(from = 1000, to = "APPLICATION_PANEL"), @IntToString(from = 1001, to = "APPLICATION_MEDIA"), @IntToString(from = 1002, to = "APPLICATION_SUB_PANEL"), @IntToString(from = 1005, to = "APPLICATION_ABOVE_SUB_PANEL"), @IntToString(from = 1003, to = "APPLICATION_ATTACHED_DIALOG"), @IntToString(from = 1004, to = "APPLICATION_MEDIA_OVERLAY"), @IntToString(from = 2000, to = "STATUS_BAR"), @IntToString(from = 2001, to = "SEARCH_BAR"), @IntToString(from = 2002, to = "PHONE"), @IntToString(from = 2003, to = "SYSTEM_ALERT"), @IntToString(from = 2005, to = "TOAST"), @IntToString(from = 2006, to = "SYSTEM_OVERLAY"), @IntToString(from = 2007, to = "PRIORITY_PHONE"), @IntToString(from = 2008, to = "SYSTEM_DIALOG"), @IntToString(from = 2009, to = "KEYGUARD_DIALOG"), @IntToString(from = 2010, to = "SYSTEM_ERROR"), @IntToString(from = 2011, to = "INPUT_METHOD"), @IntToString(from = 2012, to = "INPUT_METHOD_DIALOG"), @IntToString(from = 2013, to = "WALLPAPER"), @IntToString(from = 2014, to = "STATUS_BAR_PANEL"), @IntToString(from = 2015, to = "SECURE_SYSTEM_OVERLAY"), @IntToString(from = 2016, to = "DRAG"), @IntToString(from = 2017, to = "STATUS_BAR_SUB_PANEL"), @IntToString(from = 2018, to = "POINTER"), @IntToString(from = 2019, to = "NAVIGATION_BAR"), @IntToString(from = 2020, to = "VOLUME_OVERLAY"), @IntToString(from = 2021, to = "BOOT_PROGRESS"), @IntToString(from = 2022, to = "INPUT_CONSUMER"), @IntToString(from = 2024, to = "NAVIGATION_BAR_PANEL"), @IntToString(from = 2026, to = "DISPLAY_OVERLAY"), @IntToString(from = 2027, to = "MAGNIFICATION_OVERLAY"), @IntToString(from = 2037, to = "PRESENTATION"), @IntToString(from = 2030, to = "PRIVATE_PRESENTATION"), @IntToString(from = 2031, to = "VOICE_INTERACTION"), @IntToString(from = 2033, to = "VOICE_INTERACTION_STARTING"), @IntToString(from = 2034, to = "DOCK_DIVIDER"), @IntToString(from = 2035, to = "QS_DIALOG"), @IntToString(from = 2036, to = "SCREENSHOT"), @IntToString(from = 2038, to = "APPLICATION_OVERLAY")})
    public int type;
    
    public long userActivityTimeout;
    
    public float verticalMargin;
    
    @ExportedProperty
    public float verticalWeight;
    
    public int windowAnimations;
    
    @ExportedProperty
    public int x;
    
    @ExportedProperty
    public int y;
    
    public LayoutParams(WindowManager this$0) {
      boolean bool2;
      this.surfaceInsets = new Rect();
      boolean bool1 = true;
      this.preservePreviousSurfaceInsets = true;
      this.alpha = 1.0F;
      this.dimAmount = 1.0F;
      this.screenBrightness = -1.0F;
      this.buttonBrightness = -1.0F;
      this.rotationAnimation = 0;
      this.token = null;
      this.packageName = null;
      this.screenOrientation = -1;
      this.layoutInDisplayCutoutMode = 0;
      this.userActivityTimeout = -1L;
      this.accessibilityIdOfAnchor = AccessibilityNodeInfo.UNDEFINED_NODE_ID;
      this.hideTimeoutMilliseconds = -1L;
      this.preferMinimalPostProcessing = false;
      this.mColorMode = 0;
      this.insetsFlags = new InsetsFlags();
      this.mFitInsetsTypes = WindowInsets.Type.systemBars();
      this.mFitInsetsSides = WindowInsets.Side.all();
      this.mFitInsetsIgnoringVisibility = false;
      this.mCompatibilityParamsBackup = null;
      this.mTitle = null;
      this.width = this$0.readInt();
      this.height = this$0.readInt();
      this.x = this$0.readInt();
      this.y = this$0.readInt();
      this.type = this$0.readInt();
      this.flags = this$0.readInt();
      this.privateFlags = this$0.readInt();
      this.softInputMode = this$0.readInt();
      this.layoutInDisplayCutoutMode = this$0.readInt();
      this.gravity = this$0.readInt();
      this.horizontalMargin = this$0.readFloat();
      this.verticalMargin = this$0.readFloat();
      this.format = this$0.readInt();
      this.windowAnimations = this$0.readInt();
      this.alpha = this$0.readFloat();
      this.dimAmount = this$0.readFloat();
      this.screenBrightness = this$0.readFloat();
      this.buttonBrightness = this$0.readFloat();
      this.rotationAnimation = this$0.readInt();
      this.token = this$0.readStrongBinder();
      this.packageName = this$0.readString();
      this.mTitle = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)this$0);
      this.screenOrientation = this$0.readInt();
      this.preferredRefreshRate = this$0.readFloat();
      this.preferredDisplayModeId = this$0.readInt();
      this.systemUiVisibility = this$0.readInt();
      this.subtreeSystemUiVisibility = this$0.readInt();
      if (this$0.readInt() != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.hasSystemUiListeners = bool2;
      this.inputFeatures = this$0.readInt();
      this.userActivityTimeout = this$0.readLong();
      this.surfaceInsets.left = this$0.readInt();
      this.surfaceInsets.top = this$0.readInt();
      this.surfaceInsets.right = this$0.readInt();
      this.surfaceInsets.bottom = this$0.readInt();
      if (this$0.readInt() != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.hasManualSurfaceInsets = bool2;
      if (this$0.readInt() != 0) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      this.preservePreviousSurfaceInsets = bool2;
      this.accessibilityIdOfAnchor = this$0.readLong();
      this.accessibilityTitle = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)this$0);
      this.mColorMode = this$0.readInt();
      this.hideTimeoutMilliseconds = this$0.readLong();
      this.insetsFlags.appearance = this$0.readInt();
      this.insetsFlags.behavior = this$0.readInt();
      this.mFitInsetsTypes = this$0.readInt();
      this.mFitInsetsSides = this$0.readInt();
      this.mFitInsetsIgnoringVisibility = this$0.readBoolean();
      this.preferMinimalPostProcessing = this$0.readBoolean();
      int i = this$0.readInt();
      if (i > 0) {
        int[] arrayOfInt = new int[i];
        this$0.readIntArray(arrayOfInt);
      } 
      readFromParcel((Parcel)this$0);
    }
    
    public final int copyFrom(LayoutParams param1LayoutParams) {
      // Byte code:
      //   0: iconst_0
      //   1: istore_2
      //   2: aload_0
      //   3: getfield width : I
      //   6: aload_1
      //   7: getfield width : I
      //   10: if_icmpeq -> 25
      //   13: aload_0
      //   14: aload_1
      //   15: getfield width : I
      //   18: putfield width : I
      //   21: iconst_0
      //   22: iconst_1
      //   23: ior
      //   24: istore_2
      //   25: iload_2
      //   26: istore_3
      //   27: aload_0
      //   28: getfield height : I
      //   31: aload_1
      //   32: getfield height : I
      //   35: if_icmpeq -> 50
      //   38: aload_0
      //   39: aload_1
      //   40: getfield height : I
      //   43: putfield height : I
      //   46: iload_2
      //   47: iconst_1
      //   48: ior
      //   49: istore_3
      //   50: aload_0
      //   51: getfield x : I
      //   54: istore #4
      //   56: aload_1
      //   57: getfield x : I
      //   60: istore #5
      //   62: iload_3
      //   63: istore_2
      //   64: iload #4
      //   66: iload #5
      //   68: if_icmpeq -> 81
      //   71: aload_0
      //   72: iload #5
      //   74: putfield x : I
      //   77: iload_3
      //   78: iconst_1
      //   79: ior
      //   80: istore_2
      //   81: aload_0
      //   82: getfield y : I
      //   85: istore #4
      //   87: aload_1
      //   88: getfield y : I
      //   91: istore #5
      //   93: iload_2
      //   94: istore_3
      //   95: iload #4
      //   97: iload #5
      //   99: if_icmpeq -> 112
      //   102: aload_0
      //   103: iload #5
      //   105: putfield y : I
      //   108: iload_2
      //   109: iconst_1
      //   110: ior
      //   111: istore_3
      //   112: aload_0
      //   113: getfield horizontalWeight : F
      //   116: fstore #6
      //   118: aload_1
      //   119: getfield horizontalWeight : F
      //   122: fstore #7
      //   124: iload_3
      //   125: istore #4
      //   127: fload #6
      //   129: fload #7
      //   131: fcmpl
      //   132: ifeq -> 146
      //   135: aload_0
      //   136: fload #7
      //   138: putfield horizontalWeight : F
      //   141: iload_3
      //   142: iconst_1
      //   143: ior
      //   144: istore #4
      //   146: aload_0
      //   147: getfield verticalWeight : F
      //   150: fstore #6
      //   152: aload_1
      //   153: getfield verticalWeight : F
      //   156: fstore #7
      //   158: iload #4
      //   160: istore_2
      //   161: fload #6
      //   163: fload #7
      //   165: fcmpl
      //   166: ifeq -> 180
      //   169: aload_0
      //   170: fload #7
      //   172: putfield verticalWeight : F
      //   175: iload #4
      //   177: iconst_1
      //   178: ior
      //   179: istore_2
      //   180: aload_0
      //   181: getfield horizontalMargin : F
      //   184: fstore #7
      //   186: aload_1
      //   187: getfield horizontalMargin : F
      //   190: fstore #6
      //   192: iload_2
      //   193: istore #4
      //   195: fload #7
      //   197: fload #6
      //   199: fcmpl
      //   200: ifeq -> 214
      //   203: aload_0
      //   204: fload #6
      //   206: putfield horizontalMargin : F
      //   209: iload_2
      //   210: iconst_1
      //   211: ior
      //   212: istore #4
      //   214: aload_0
      //   215: getfield verticalMargin : F
      //   218: fstore #7
      //   220: aload_1
      //   221: getfield verticalMargin : F
      //   224: fstore #6
      //   226: iload #4
      //   228: istore_3
      //   229: fload #7
      //   231: fload #6
      //   233: fcmpl
      //   234: ifeq -> 248
      //   237: aload_0
      //   238: fload #6
      //   240: putfield verticalMargin : F
      //   243: iload #4
      //   245: iconst_1
      //   246: ior
      //   247: istore_3
      //   248: aload_0
      //   249: getfield type : I
      //   252: istore #4
      //   254: aload_1
      //   255: getfield type : I
      //   258: istore #5
      //   260: iload_3
      //   261: istore_2
      //   262: iload #4
      //   264: iload #5
      //   266: if_icmpeq -> 279
      //   269: aload_0
      //   270: iload #5
      //   272: putfield type : I
      //   275: iload_3
      //   276: iconst_2
      //   277: ior
      //   278: istore_2
      //   279: aload_0
      //   280: getfield flags : I
      //   283: istore #4
      //   285: aload_1
      //   286: getfield flags : I
      //   289: istore #5
      //   291: iload_2
      //   292: istore_3
      //   293: iload #4
      //   295: iload #5
      //   297: if_icmpeq -> 331
      //   300: iload_2
      //   301: istore_3
      //   302: ldc_w 201326592
      //   305: iload #4
      //   307: iload #5
      //   309: ixor
      //   310: iand
      //   311: ifeq -> 319
      //   314: iload_2
      //   315: ldc 524288
      //   317: ior
      //   318: istore_3
      //   319: aload_0
      //   320: aload_1
      //   321: getfield flags : I
      //   324: putfield flags : I
      //   327: iload_3
      //   328: iconst_4
      //   329: ior
      //   330: istore_3
      //   331: aload_0
      //   332: getfield privateFlags : I
      //   335: istore_2
      //   336: aload_1
      //   337: getfield privateFlags : I
      //   340: istore #5
      //   342: iload_3
      //   343: istore #4
      //   345: iload_2
      //   346: iload #5
      //   348: if_icmpeq -> 363
      //   351: aload_0
      //   352: iload #5
      //   354: putfield privateFlags : I
      //   357: iload_3
      //   358: ldc 131072
      //   360: ior
      //   361: istore #4
      //   363: aload_0
      //   364: getfield softInputMode : I
      //   367: istore #5
      //   369: aload_1
      //   370: getfield softInputMode : I
      //   373: istore_3
      //   374: iload #4
      //   376: istore_2
      //   377: iload #5
      //   379: iload_3
      //   380: if_icmpeq -> 395
      //   383: aload_0
      //   384: iload_3
      //   385: putfield softInputMode : I
      //   388: iload #4
      //   390: sipush #512
      //   393: ior
      //   394: istore_2
      //   395: aload_0
      //   396: getfield layoutInDisplayCutoutMode : I
      //   399: istore #4
      //   401: aload_1
      //   402: getfield layoutInDisplayCutoutMode : I
      //   405: istore #5
      //   407: iload_2
      //   408: istore_3
      //   409: iload #4
      //   411: iload #5
      //   413: if_icmpeq -> 426
      //   416: aload_0
      //   417: iload #5
      //   419: putfield layoutInDisplayCutoutMode : I
      //   422: iload_2
      //   423: iconst_1
      //   424: ior
      //   425: istore_3
      //   426: aload_0
      //   427: getfield gravity : I
      //   430: istore_2
      //   431: aload_1
      //   432: getfield gravity : I
      //   435: istore #5
      //   437: iload_3
      //   438: istore #4
      //   440: iload_2
      //   441: iload #5
      //   443: if_icmpeq -> 457
      //   446: aload_0
      //   447: iload #5
      //   449: putfield gravity : I
      //   452: iload_3
      //   453: iconst_1
      //   454: ior
      //   455: istore #4
      //   457: aload_0
      //   458: getfield format : I
      //   461: istore_3
      //   462: aload_1
      //   463: getfield format : I
      //   466: istore #5
      //   468: iload #4
      //   470: istore_2
      //   471: iload_3
      //   472: iload #5
      //   474: if_icmpeq -> 489
      //   477: aload_0
      //   478: iload #5
      //   480: putfield format : I
      //   483: iload #4
      //   485: bipush #8
      //   487: ior
      //   488: istore_2
      //   489: aload_0
      //   490: getfield windowAnimations : I
      //   493: istore #4
      //   495: aload_1
      //   496: getfield windowAnimations : I
      //   499: istore #5
      //   501: iload_2
      //   502: istore_3
      //   503: iload #4
      //   505: iload #5
      //   507: if_icmpeq -> 521
      //   510: aload_0
      //   511: iload #5
      //   513: putfield windowAnimations : I
      //   516: iload_2
      //   517: bipush #16
      //   519: ior
      //   520: istore_3
      //   521: aload_0
      //   522: getfield token : Landroid/os/IBinder;
      //   525: ifnonnull -> 536
      //   528: aload_0
      //   529: aload_1
      //   530: getfield token : Landroid/os/IBinder;
      //   533: putfield token : Landroid/os/IBinder;
      //   536: aload_0
      //   537: getfield packageName : Ljava/lang/String;
      //   540: ifnonnull -> 551
      //   543: aload_0
      //   544: aload_1
      //   545: getfield packageName : Ljava/lang/String;
      //   548: putfield packageName : Ljava/lang/String;
      //   551: iload_3
      //   552: istore_2
      //   553: aload_0
      //   554: getfield mTitle : Ljava/lang/CharSequence;
      //   557: aload_1
      //   558: getfield mTitle : Ljava/lang/CharSequence;
      //   561: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
      //   564: ifne -> 591
      //   567: aload_1
      //   568: getfield mTitle : Ljava/lang/CharSequence;
      //   571: astore #8
      //   573: iload_3
      //   574: istore_2
      //   575: aload #8
      //   577: ifnull -> 591
      //   580: aload_0
      //   581: aload #8
      //   583: putfield mTitle : Ljava/lang/CharSequence;
      //   586: iload_3
      //   587: bipush #64
      //   589: ior
      //   590: istore_2
      //   591: aload_0
      //   592: getfield alpha : F
      //   595: fstore #7
      //   597: aload_1
      //   598: getfield alpha : F
      //   601: fstore #6
      //   603: iload_2
      //   604: istore_3
      //   605: fload #7
      //   607: fload #6
      //   609: fcmpl
      //   610: ifeq -> 625
      //   613: aload_0
      //   614: fload #6
      //   616: putfield alpha : F
      //   619: iload_2
      //   620: sipush #128
      //   623: ior
      //   624: istore_3
      //   625: aload_0
      //   626: getfield dimAmount : F
      //   629: fstore #6
      //   631: aload_1
      //   632: getfield dimAmount : F
      //   635: fstore #7
      //   637: iload_3
      //   638: istore_2
      //   639: fload #6
      //   641: fload #7
      //   643: fcmpl
      //   644: ifeq -> 658
      //   647: aload_0
      //   648: fload #7
      //   650: putfield dimAmount : F
      //   653: iload_3
      //   654: bipush #32
      //   656: ior
      //   657: istore_2
      //   658: aload_0
      //   659: getfield screenBrightness : F
      //   662: fstore #7
      //   664: aload_1
      //   665: getfield screenBrightness : F
      //   668: fstore #6
      //   670: iload_2
      //   671: istore #4
      //   673: fload #7
      //   675: fload #6
      //   677: fcmpl
      //   678: ifeq -> 694
      //   681: aload_0
      //   682: fload #6
      //   684: putfield screenBrightness : F
      //   687: iload_2
      //   688: sipush #2048
      //   691: ior
      //   692: istore #4
      //   694: aload_0
      //   695: getfield buttonBrightness : F
      //   698: fstore #6
      //   700: aload_1
      //   701: getfield buttonBrightness : F
      //   704: fstore #7
      //   706: iload #4
      //   708: istore_3
      //   709: fload #6
      //   711: fload #7
      //   713: fcmpl
      //   714: ifeq -> 730
      //   717: aload_0
      //   718: fload #7
      //   720: putfield buttonBrightness : F
      //   723: iload #4
      //   725: sipush #8192
      //   728: ior
      //   729: istore_3
      //   730: aload_0
      //   731: getfield rotationAnimation : I
      //   734: istore #5
      //   736: aload_1
      //   737: getfield rotationAnimation : I
      //   740: istore #4
      //   742: iload_3
      //   743: istore_2
      //   744: iload #5
      //   746: iload #4
      //   748: if_icmpeq -> 763
      //   751: aload_0
      //   752: iload #4
      //   754: putfield rotationAnimation : I
      //   757: iload_3
      //   758: sipush #4096
      //   761: ior
      //   762: istore_2
      //   763: aload_0
      //   764: getfield screenOrientation : I
      //   767: istore #4
      //   769: aload_1
      //   770: getfield screenOrientation : I
      //   773: istore #5
      //   775: iload_2
      //   776: istore_3
      //   777: iload #4
      //   779: iload #5
      //   781: if_icmpeq -> 796
      //   784: aload_0
      //   785: iload #5
      //   787: putfield screenOrientation : I
      //   790: iload_2
      //   791: sipush #1024
      //   794: ior
      //   795: istore_3
      //   796: aload_0
      //   797: getfield preferredRefreshRate : F
      //   800: fstore #7
      //   802: aload_1
      //   803: getfield preferredRefreshRate : F
      //   806: fstore #6
      //   808: iload_3
      //   809: istore_2
      //   810: fload #7
      //   812: fload #6
      //   814: fcmpl
      //   815: ifeq -> 829
      //   818: aload_0
      //   819: fload #6
      //   821: putfield preferredRefreshRate : F
      //   824: iload_3
      //   825: ldc 2097152
      //   827: ior
      //   828: istore_2
      //   829: aload_0
      //   830: getfield preferredDisplayModeId : I
      //   833: istore #5
      //   835: aload_1
      //   836: getfield preferredDisplayModeId : I
      //   839: istore #4
      //   841: iload_2
      //   842: istore_3
      //   843: iload #5
      //   845: iload #4
      //   847: if_icmpeq -> 861
      //   850: aload_0
      //   851: iload #4
      //   853: putfield preferredDisplayModeId : I
      //   856: iload_2
      //   857: ldc 8388608
      //   859: ior
      //   860: istore_3
      //   861: aload_0
      //   862: getfield systemUiVisibility : I
      //   865: aload_1
      //   866: getfield systemUiVisibility : I
      //   869: if_icmpne -> 886
      //   872: iload_3
      //   873: istore #4
      //   875: aload_0
      //   876: getfield subtreeSystemUiVisibility : I
      //   879: aload_1
      //   880: getfield subtreeSystemUiVisibility : I
      //   883: if_icmpeq -> 909
      //   886: aload_0
      //   887: aload_1
      //   888: getfield systemUiVisibility : I
      //   891: putfield systemUiVisibility : I
      //   894: aload_0
      //   895: aload_1
      //   896: getfield subtreeSystemUiVisibility : I
      //   899: putfield subtreeSystemUiVisibility : I
      //   902: iload_3
      //   903: sipush #16384
      //   906: ior
      //   907: istore #4
      //   909: aload_0
      //   910: getfield hasSystemUiListeners : Z
      //   913: istore #9
      //   915: aload_1
      //   916: getfield hasSystemUiListeners : Z
      //   919: istore #10
      //   921: iload #4
      //   923: istore_2
      //   924: iload #9
      //   926: iload #10
      //   928: if_icmpeq -> 943
      //   931: aload_0
      //   932: iload #10
      //   934: putfield hasSystemUiListeners : Z
      //   937: iload #4
      //   939: ldc 32768
      //   941: ior
      //   942: istore_2
      //   943: aload_0
      //   944: getfield inputFeatures : I
      //   947: istore #4
      //   949: aload_1
      //   950: getfield inputFeatures : I
      //   953: istore #5
      //   955: iload_2
      //   956: istore_3
      //   957: iload #4
      //   959: iload #5
      //   961: if_icmpeq -> 975
      //   964: aload_0
      //   965: iload #5
      //   967: putfield inputFeatures : I
      //   970: iload_2
      //   971: ldc 65536
      //   973: ior
      //   974: istore_3
      //   975: aload_0
      //   976: getfield userActivityTimeout : J
      //   979: lstore #11
      //   981: aload_1
      //   982: getfield userActivityTimeout : J
      //   985: lstore #13
      //   987: iload_3
      //   988: istore_2
      //   989: lload #11
      //   991: lload #13
      //   993: lcmp
      //   994: ifeq -> 1008
      //   997: aload_0
      //   998: lload #13
      //   1000: putfield userActivityTimeout : J
      //   1003: iload_3
      //   1004: ldc 262144
      //   1006: ior
      //   1007: istore_2
      //   1008: iload_2
      //   1009: istore_3
      //   1010: aload_0
      //   1011: getfield surfaceInsets : Landroid/graphics/Rect;
      //   1014: aload_1
      //   1015: getfield surfaceInsets : Landroid/graphics/Rect;
      //   1018: invokevirtual equals : (Ljava/lang/Object;)Z
      //   1021: ifne -> 1040
      //   1024: aload_0
      //   1025: getfield surfaceInsets : Landroid/graphics/Rect;
      //   1028: aload_1
      //   1029: getfield surfaceInsets : Landroid/graphics/Rect;
      //   1032: invokevirtual set : (Landroid/graphics/Rect;)V
      //   1035: iload_2
      //   1036: ldc 1048576
      //   1038: ior
      //   1039: istore_3
      //   1040: aload_0
      //   1041: getfield hasManualSurfaceInsets : Z
      //   1044: istore #10
      //   1046: aload_1
      //   1047: getfield hasManualSurfaceInsets : Z
      //   1050: istore #9
      //   1052: iload_3
      //   1053: istore_2
      //   1054: iload #10
      //   1056: iload #9
      //   1058: if_icmpeq -> 1072
      //   1061: aload_0
      //   1062: iload #9
      //   1064: putfield hasManualSurfaceInsets : Z
      //   1067: iload_3
      //   1068: ldc 1048576
      //   1070: ior
      //   1071: istore_2
      //   1072: aload_0
      //   1073: getfield preservePreviousSurfaceInsets : Z
      //   1076: istore #9
      //   1078: aload_1
      //   1079: getfield preservePreviousSurfaceInsets : Z
      //   1082: istore #10
      //   1084: iload_2
      //   1085: istore_3
      //   1086: iload #9
      //   1088: iload #10
      //   1090: if_icmpeq -> 1104
      //   1093: aload_0
      //   1094: iload #10
      //   1096: putfield preservePreviousSurfaceInsets : Z
      //   1099: iload_2
      //   1100: ldc 1048576
      //   1102: ior
      //   1103: istore_3
      //   1104: aload_0
      //   1105: getfield accessibilityIdOfAnchor : J
      //   1108: lstore #13
      //   1110: aload_1
      //   1111: getfield accessibilityIdOfAnchor : J
      //   1114: lstore #11
      //   1116: iload_3
      //   1117: istore_2
      //   1118: lload #13
      //   1120: lload #11
      //   1122: lcmp
      //   1123: ifeq -> 1137
      //   1126: aload_0
      //   1127: lload #11
      //   1129: putfield accessibilityIdOfAnchor : J
      //   1132: iload_3
      //   1133: ldc 16777216
      //   1135: ior
      //   1136: istore_2
      //   1137: iload_2
      //   1138: istore_3
      //   1139: aload_0
      //   1140: getfield accessibilityTitle : Ljava/lang/CharSequence;
      //   1143: aload_1
      //   1144: getfield accessibilityTitle : Ljava/lang/CharSequence;
      //   1147: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
      //   1150: ifne -> 1177
      //   1153: aload_1
      //   1154: getfield accessibilityTitle : Ljava/lang/CharSequence;
      //   1157: astore #8
      //   1159: iload_2
      //   1160: istore_3
      //   1161: aload #8
      //   1163: ifnull -> 1177
      //   1166: aload_0
      //   1167: aload #8
      //   1169: putfield accessibilityTitle : Ljava/lang/CharSequence;
      //   1172: iload_2
      //   1173: ldc 33554432
      //   1175: ior
      //   1176: istore_3
      //   1177: aload_0
      //   1178: getfield mColorMode : I
      //   1181: istore #4
      //   1183: aload_1
      //   1184: getfield mColorMode : I
      //   1187: istore #5
      //   1189: iload_3
      //   1190: istore_2
      //   1191: iload #4
      //   1193: iload #5
      //   1195: if_icmpeq -> 1209
      //   1198: aload_0
      //   1199: iload #5
      //   1201: putfield mColorMode : I
      //   1204: iload_3
      //   1205: ldc 67108864
      //   1207: ior
      //   1208: istore_2
      //   1209: aload_0
      //   1210: getfield preferMinimalPostProcessing : Z
      //   1213: istore #9
      //   1215: aload_1
      //   1216: getfield preferMinimalPostProcessing : Z
      //   1219: istore #10
      //   1221: iload_2
      //   1222: istore_3
      //   1223: iload #9
      //   1225: iload #10
      //   1227: if_icmpeq -> 1241
      //   1230: aload_0
      //   1231: iload #10
      //   1233: putfield preferMinimalPostProcessing : Z
      //   1236: iload_2
      //   1237: ldc 268435456
      //   1239: ior
      //   1240: istore_3
      //   1241: aload_0
      //   1242: aload_1
      //   1243: getfield hideTimeoutMilliseconds : J
      //   1246: putfield hideTimeoutMilliseconds : J
      //   1249: iload_3
      //   1250: istore_2
      //   1251: aload_0
      //   1252: getfield insetsFlags : Landroid/view/InsetsFlags;
      //   1255: getfield appearance : I
      //   1258: aload_1
      //   1259: getfield insetsFlags : Landroid/view/InsetsFlags;
      //   1262: getfield appearance : I
      //   1265: if_icmpeq -> 1287
      //   1268: aload_0
      //   1269: getfield insetsFlags : Landroid/view/InsetsFlags;
      //   1272: aload_1
      //   1273: getfield insetsFlags : Landroid/view/InsetsFlags;
      //   1276: getfield appearance : I
      //   1279: putfield appearance : I
      //   1282: iload_3
      //   1283: ldc 134217728
      //   1285: ior
      //   1286: istore_2
      //   1287: iload_2
      //   1288: istore_3
      //   1289: aload_0
      //   1290: getfield insetsFlags : Landroid/view/InsetsFlags;
      //   1293: getfield behavior : I
      //   1296: aload_1
      //   1297: getfield insetsFlags : Landroid/view/InsetsFlags;
      //   1300: getfield behavior : I
      //   1303: if_icmpeq -> 1325
      //   1306: aload_0
      //   1307: getfield insetsFlags : Landroid/view/InsetsFlags;
      //   1310: aload_1
      //   1311: getfield insetsFlags : Landroid/view/InsetsFlags;
      //   1314: getfield behavior : I
      //   1317: putfield behavior : I
      //   1320: iload_2
      //   1321: ldc 134217728
      //   1323: ior
      //   1324: istore_3
      //   1325: aload_0
      //   1326: getfield mFitInsetsTypes : I
      //   1329: istore #5
      //   1331: aload_1
      //   1332: getfield mFitInsetsTypes : I
      //   1335: istore #4
      //   1337: iload_3
      //   1338: istore_2
      //   1339: iload #5
      //   1341: iload #4
      //   1343: if_icmpeq -> 1356
      //   1346: aload_0
      //   1347: iload #4
      //   1349: putfield mFitInsetsTypes : I
      //   1352: iload_3
      //   1353: iconst_1
      //   1354: ior
      //   1355: istore_2
      //   1356: aload_0
      //   1357: getfield mFitInsetsSides : I
      //   1360: istore #5
      //   1362: aload_1
      //   1363: getfield mFitInsetsSides : I
      //   1366: istore #4
      //   1368: iload_2
      //   1369: istore_3
      //   1370: iload #5
      //   1372: iload #4
      //   1374: if_icmpeq -> 1387
      //   1377: aload_0
      //   1378: iload #4
      //   1380: putfield mFitInsetsSides : I
      //   1383: iload_2
      //   1384: iconst_1
      //   1385: ior
      //   1386: istore_3
      //   1387: aload_0
      //   1388: getfield mFitInsetsIgnoringVisibility : Z
      //   1391: istore #10
      //   1393: aload_1
      //   1394: getfield mFitInsetsIgnoringVisibility : Z
      //   1397: istore #9
      //   1399: iload_3
      //   1400: istore_2
      //   1401: iload #10
      //   1403: iload #9
      //   1405: if_icmpeq -> 1418
      //   1408: aload_0
      //   1409: iload #9
      //   1411: putfield mFitInsetsIgnoringVisibility : Z
      //   1414: iload_3
      //   1415: iconst_1
      //   1416: ior
      //   1417: istore_2
      //   1418: iload_2
      //   1419: istore_3
      //   1420: aload_0
      //   1421: getfield providesInsetsTypes : [I
      //   1424: aload_1
      //   1425: getfield providesInsetsTypes : [I
      //   1428: invokestatic equals : ([I[I)Z
      //   1431: ifne -> 1446
      //   1434: aload_0
      //   1435: aload_1
      //   1436: getfield providesInsetsTypes : [I
      //   1439: putfield providesInsetsTypes : [I
      //   1442: iload_2
      //   1443: iconst_1
      //   1444: ior
      //   1445: istore_3
      //   1446: aload_0
      //   1447: aload_1
      //   1448: invokespecial copyFrom : (Landroid/view/OplusBaseLayoutParams;)I
      //   1451: istore_2
      //   1452: iload_3
      //   1453: iload_2
      //   1454: ior
      //   1455: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #3270	-> 0
      //   #3272	-> 2
      //   #3273	-> 13
      //   #3274	-> 21
      //   #3276	-> 25
      //   #3277	-> 38
      //   #3278	-> 46
      //   #3280	-> 50
      //   #3281	-> 71
      //   #3282	-> 77
      //   #3284	-> 81
      //   #3285	-> 102
      //   #3286	-> 108
      //   #3288	-> 112
      //   #3289	-> 135
      //   #3290	-> 141
      //   #3292	-> 146
      //   #3293	-> 169
      //   #3294	-> 175
      //   #3296	-> 180
      //   #3297	-> 203
      //   #3298	-> 209
      //   #3300	-> 214
      //   #3301	-> 237
      //   #3302	-> 243
      //   #3304	-> 248
      //   #3305	-> 269
      //   #3306	-> 275
      //   #3308	-> 279
      //   #3309	-> 300
      //   #3310	-> 300
      //   #3311	-> 314
      //   #3313	-> 319
      //   #3314	-> 327
      //   #3316	-> 331
      //   #3317	-> 351
      //   #3318	-> 357
      //   #3320	-> 363
      //   #3321	-> 383
      //   #3322	-> 388
      //   #3324	-> 395
      //   #3325	-> 416
      //   #3326	-> 422
      //   #3328	-> 426
      //   #3329	-> 446
      //   #3330	-> 452
      //   #3332	-> 457
      //   #3333	-> 477
      //   #3334	-> 483
      //   #3336	-> 489
      //   #3337	-> 510
      //   #3338	-> 516
      //   #3340	-> 521
      //   #3343	-> 528
      //   #3345	-> 536
      //   #3348	-> 543
      //   #3350	-> 551
      //   #3352	-> 580
      //   #3353	-> 586
      //   #3355	-> 591
      //   #3356	-> 613
      //   #3357	-> 619
      //   #3359	-> 625
      //   #3360	-> 647
      //   #3361	-> 653
      //   #3363	-> 658
      //   #3364	-> 681
      //   #3365	-> 687
      //   #3367	-> 694
      //   #3368	-> 717
      //   #3369	-> 723
      //   #3371	-> 730
      //   #3372	-> 751
      //   #3373	-> 757
      //   #3376	-> 763
      //   #3377	-> 784
      //   #3378	-> 790
      //   #3381	-> 796
      //   #3382	-> 818
      //   #3383	-> 824
      //   #3386	-> 829
      //   #3387	-> 850
      //   #3388	-> 856
      //   #3391	-> 861
      //   #3393	-> 886
      //   #3394	-> 894
      //   #3395	-> 902
      //   #3398	-> 909
      //   #3399	-> 931
      //   #3400	-> 937
      //   #3403	-> 943
      //   #3404	-> 964
      //   #3405	-> 970
      //   #3408	-> 975
      //   #3409	-> 997
      //   #3410	-> 1003
      //   #3413	-> 1008
      //   #3414	-> 1024
      //   #3415	-> 1035
      //   #3418	-> 1040
      //   #3419	-> 1061
      //   #3420	-> 1067
      //   #3423	-> 1072
      //   #3424	-> 1093
      //   #3425	-> 1099
      //   #3428	-> 1104
      //   #3429	-> 1126
      //   #3430	-> 1132
      //   #3433	-> 1137
      //   #3436	-> 1166
      //   #3437	-> 1172
      //   #3440	-> 1177
      //   #3441	-> 1198
      //   #3442	-> 1204
      //   #3445	-> 1209
      //   #3446	-> 1230
      //   #3447	-> 1236
      //   #3451	-> 1241
      //   #3453	-> 1249
      //   #3454	-> 1268
      //   #3455	-> 1282
      //   #3458	-> 1287
      //   #3459	-> 1306
      //   #3460	-> 1320
      //   #3463	-> 1325
      //   #3464	-> 1346
      //   #3465	-> 1352
      //   #3468	-> 1356
      //   #3469	-> 1377
      //   #3470	-> 1383
      //   #3473	-> 1387
      //   #3474	-> 1408
      //   #3475	-> 1414
      //   #3478	-> 1418
      //   #3479	-> 1434
      //   #3480	-> 1442
      //   #3484	-> 1446
      //   #3488	-> 1452
    }
    
    public String debug(String param1String) {
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(param1String);
      stringBuilder2.append("Contents of ");
      stringBuilder2.append(this);
      stringBuilder2.append(":");
      param1String = stringBuilder2.toString();
      Log.d("Debug", param1String);
      param1String = super.debug("");
      Log.d("Debug", param1String);
      Log.d("Debug", "");
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("WindowManager.LayoutParams={title=");
      stringBuilder1.append(this.mTitle);
      stringBuilder1.append("}");
      Log.d("Debug", stringBuilder1.toString());
      return "";
    }
    
    public String toString() {
      return toString("");
    }
    
    public void dumpDimensions(StringBuilder param1StringBuilder) {
      String str2;
      param1StringBuilder.append('(');
      param1StringBuilder.append(this.x);
      param1StringBuilder.append(',');
      param1StringBuilder.append(this.y);
      param1StringBuilder.append(")(");
      int i = this.width;
      String str1 = "wrap";
      if (i == -1) {
        str2 = "fill";
      } else if (this.width == -2) {
        str2 = "wrap";
      } else {
        str2 = String.valueOf(this.width);
      } 
      param1StringBuilder.append(str2);
      param1StringBuilder.append('x');
      if (this.height == -1) {
        str2 = "fill";
      } else if (this.height == -2) {
        str2 = str1;
      } else {
        str2 = String.valueOf(this.height);
      } 
      param1StringBuilder.append(str2);
      param1StringBuilder.append(")");
    }
    
    public String toString(String param1String) {
      StringBuilder stringBuilder = new StringBuilder(256);
      stringBuilder.append('{');
      dumpDimensions(stringBuilder);
      if (this.horizontalMargin != 0.0F) {
        stringBuilder.append(" hm=");
        stringBuilder.append(this.horizontalMargin);
      } 
      if (this.verticalMargin != 0.0F) {
        stringBuilder.append(" vm=");
        stringBuilder.append(this.verticalMargin);
      } 
      if (this.gravity != 0) {
        stringBuilder.append(" gr=");
        stringBuilder.append(Gravity.toString(this.gravity));
      } 
      if (this.softInputMode != 0) {
        stringBuilder.append(" sim={");
        stringBuilder.append(softInputModeToString(this.softInputMode));
        stringBuilder.append('}');
      } 
      if (this.layoutInDisplayCutoutMode != 0) {
        stringBuilder.append(" layoutInDisplayCutoutMode=");
        stringBuilder.append(layoutInDisplayCutoutModeToString(this.layoutInDisplayCutoutMode));
      } 
      stringBuilder.append(" ty=");
      stringBuilder.append(ViewDebug.intToString(LayoutParams.class, "type", this.type));
      if (this.format != -1) {
        stringBuilder.append(" fmt=");
        stringBuilder.append(PixelFormat.formatToString(this.format));
      } 
      if (this.windowAnimations != 0) {
        stringBuilder.append(" wanim=0x");
        stringBuilder.append(Integer.toHexString(this.windowAnimations));
      } 
      if (this.screenOrientation != -1) {
        stringBuilder.append(" or=");
        stringBuilder.append(ActivityInfo.screenOrientationToString(this.screenOrientation));
      } 
      if (this.alpha != 1.0F) {
        stringBuilder.append(" alpha=");
        stringBuilder.append(this.alpha);
      } 
      if (this.screenBrightness != -1.0F) {
        stringBuilder.append(" sbrt=");
        stringBuilder.append(this.screenBrightness);
      } 
      if (this.buttonBrightness != -1.0F) {
        stringBuilder.append(" bbrt=");
        stringBuilder.append(this.buttonBrightness);
      } 
      if (this.rotationAnimation != 0) {
        stringBuilder.append(" rotAnim=");
        stringBuilder.append(rotationAnimationToString(this.rotationAnimation));
      } 
      if (this.preferredRefreshRate != 0.0F) {
        stringBuilder.append(" preferredRefreshRate=");
        stringBuilder.append(this.preferredRefreshRate);
      } 
      if (this.preferredDisplayModeId != 0) {
        stringBuilder.append(" preferredDisplayMode=");
        stringBuilder.append(this.preferredDisplayModeId);
      } 
      if (this.hasSystemUiListeners) {
        stringBuilder.append(" sysuil=");
        stringBuilder.append(this.hasSystemUiListeners);
      } 
      if (this.inputFeatures != 0) {
        stringBuilder.append(" if=");
        stringBuilder.append(inputFeatureToString(this.inputFeatures));
      } 
      if (this.userActivityTimeout >= 0L) {
        stringBuilder.append(" userActivityTimeout=");
        stringBuilder.append(this.userActivityTimeout);
      } 
      if (this.surfaceInsets.left != 0 || this.surfaceInsets.top != 0 || this.surfaceInsets.right != 0 || this.surfaceInsets.bottom != 0 || this.hasManualSurfaceInsets || !this.preservePreviousSurfaceInsets) {
        stringBuilder.append(" surfaceInsets=");
        stringBuilder.append(this.surfaceInsets);
        if (this.hasManualSurfaceInsets)
          stringBuilder.append(" (manual)"); 
        if (!this.preservePreviousSurfaceInsets)
          stringBuilder.append(" (!preservePreviousSurfaceInsets)"); 
      } 
      if (this.mColorMode != 0) {
        stringBuilder.append(" colorMode=");
        stringBuilder.append(ActivityInfo.colorModeToString(this.mColorMode));
      } 
      if (this.preferMinimalPostProcessing) {
        stringBuilder.append(" preferMinimalPostProcessing=");
        stringBuilder.append(this.preferMinimalPostProcessing);
      } 
      stringBuilder.append(System.lineSeparator());
      stringBuilder.append(param1String);
      stringBuilder.append("  fl=");
      int i = this.flags;
      String str = ViewDebug.flagsToString(LayoutParams.class, "flags", i);
      stringBuilder.append(str);
      if (this.privateFlags != 0) {
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(param1String);
        stringBuilder.append("  pfl=");
        stringBuilder.append(ViewDebug.flagsToString(LayoutParams.class, "privateFlags", this.privateFlags));
      } 
      if (this.systemUiVisibility != 0) {
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(param1String);
        stringBuilder.append("  sysui=");
        stringBuilder.append(ViewDebug.flagsToString(View.class, "mSystemUiVisibility", this.systemUiVisibility));
      } 
      if (this.subtreeSystemUiVisibility != 0) {
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(param1String);
        stringBuilder.append("  vsysui=");
        stringBuilder.append(ViewDebug.flagsToString(View.class, "mSystemUiVisibility", this.subtreeSystemUiVisibility));
      } 
      if (this.insetsFlags.appearance != 0) {
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(param1String);
        stringBuilder.append("  apr=");
        stringBuilder.append(ViewDebug.flagsToString(InsetsFlags.class, "appearance", this.insetsFlags.appearance));
      } 
      if (this.insetsFlags.behavior != 0) {
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(param1String);
        stringBuilder.append("  bhv=");
        stringBuilder.append(ViewDebug.flagsToString(InsetsFlags.class, "behavior", this.insetsFlags.behavior));
      } 
      if (this.mFitInsetsTypes != 0) {
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(param1String);
        stringBuilder.append("  fitTypes=");
        stringBuilder.append(ViewDebug.flagsToString(LayoutParams.class, "mFitInsetsTypes", this.mFitInsetsTypes));
      } 
      if (this.mFitInsetsSides != WindowInsets.Side.all()) {
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(param1String);
        stringBuilder.append("  fitSides=");
        stringBuilder.append(ViewDebug.flagsToString(LayoutParams.class, "mFitInsetsSides", this.mFitInsetsSides));
      } 
      if (this.mFitInsetsIgnoringVisibility) {
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(param1String);
        stringBuilder.append("  fitIgnoreVis");
      } 
      if (this.providesInsetsTypes != null) {
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(param1String);
        stringBuilder.append("  insetsTypes=");
        for (i = 0; i < this.providesInsetsTypes.length; i++) {
          if (i > 0)
            stringBuilder.append(' '); 
          stringBuilder.append(InsetsState.typeToString(this.providesInsetsTypes[i]));
        } 
      } 
      stringBuilder.append(super.toString(param1String));
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    public void dumpDebug(ProtoOutputStream param1ProtoOutputStream, long param1Long) {
      param1Long = param1ProtoOutputStream.start(param1Long);
      param1ProtoOutputStream.write(1120986464257L, this.type);
      param1ProtoOutputStream.write(1120986464258L, this.x);
      param1ProtoOutputStream.write(1120986464259L, this.y);
      param1ProtoOutputStream.write(1120986464260L, this.width);
      param1ProtoOutputStream.write(1120986464261L, this.height);
      param1ProtoOutputStream.write(1108101562374L, this.horizontalMargin);
      param1ProtoOutputStream.write(1108101562375L, this.verticalMargin);
      param1ProtoOutputStream.write(1120986464264L, this.gravity);
      param1ProtoOutputStream.write(1120986464265L, this.softInputMode);
      param1ProtoOutputStream.write(1159641169930L, this.format);
      param1ProtoOutputStream.write(1120986464267L, this.windowAnimations);
      param1ProtoOutputStream.write(1108101562380L, this.alpha);
      param1ProtoOutputStream.write(1108101562381L, this.screenBrightness);
      param1ProtoOutputStream.write(1108101562382L, this.buttonBrightness);
      param1ProtoOutputStream.write(1159641169935L, this.rotationAnimation);
      param1ProtoOutputStream.write(1108101562384L, this.preferredRefreshRate);
      param1ProtoOutputStream.write(1120986464273L, this.preferredDisplayModeId);
      param1ProtoOutputStream.write(1133871366162L, this.hasSystemUiListeners);
      param1ProtoOutputStream.write(1155346202643L, this.inputFeatures);
      param1ProtoOutputStream.write(1112396529684L, this.userActivityTimeout);
      param1ProtoOutputStream.write(1159641169943L, this.mColorMode);
      param1ProtoOutputStream.write(1155346202648L, this.flags);
      param1ProtoOutputStream.write(1155346202650L, this.privateFlags);
      param1ProtoOutputStream.write(1155346202651L, this.systemUiVisibility);
      param1ProtoOutputStream.write(1155346202652L, this.subtreeSystemUiVisibility);
      param1ProtoOutputStream.write(1155346202653L, this.insetsFlags.appearance);
      param1ProtoOutputStream.write(1155346202654L, this.insetsFlags.behavior);
      param1ProtoOutputStream.write(1155346202655L, this.mFitInsetsTypes);
      param1ProtoOutputStream.write(1155346202656L, this.mFitInsetsSides);
      param1ProtoOutputStream.write(1133871366177L, this.mFitInsetsIgnoringVisibility);
      param1ProtoOutputStream.end(param1Long);
    }
    
    public void scale(float param1Float) {
      this.x = (int)(this.x * param1Float + 0.5F);
      this.y = (int)(this.y * param1Float + 0.5F);
      if (this.width > 0)
        this.width = (int)(this.width * param1Float + 0.5F); 
      if (this.height > 0)
        this.height = (int)(this.height * param1Float + 0.5F); 
    }
    
    void backup() {
      int[] arrayOfInt1 = this.mCompatibilityParamsBackup;
      int[] arrayOfInt2 = arrayOfInt1;
      if (arrayOfInt1 == null)
        this.mCompatibilityParamsBackup = arrayOfInt2 = new int[4]; 
      arrayOfInt2[0] = this.x;
      arrayOfInt2[1] = this.y;
      arrayOfInt2[2] = this.width;
      arrayOfInt2[3] = this.height;
    }
    
    void restore() {
      int[] arrayOfInt = this.mCompatibilityParamsBackup;
      if (arrayOfInt != null) {
        this.x = arrayOfInt[0];
        this.y = arrayOfInt[1];
        this.width = arrayOfInt[2];
        this.height = arrayOfInt[3];
      } 
    }
    
    protected void encodeProperties(ViewHierarchyEncoder param1ViewHierarchyEncoder) {
      super.encodeProperties(param1ViewHierarchyEncoder);
      param1ViewHierarchyEncoder.addProperty("x", this.x);
      param1ViewHierarchyEncoder.addProperty("y", this.y);
      param1ViewHierarchyEncoder.addProperty("horizontalWeight", this.horizontalWeight);
      param1ViewHierarchyEncoder.addProperty("verticalWeight", this.verticalWeight);
      param1ViewHierarchyEncoder.addProperty("type", this.type);
      param1ViewHierarchyEncoder.addProperty("flags", this.flags);
    }
    
    public boolean isFullscreen() {
      boolean bool;
      if (this.x == 0 && this.y == 0 && this.width == -1 && this.height == -1) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private static String layoutInDisplayCutoutModeToString(int param1Int) {
      if (param1Int != 0) {
        if (param1Int != 1) {
          if (param1Int != 2) {
            if (param1Int != 3) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("unknown(");
              stringBuilder.append(param1Int);
              stringBuilder.append(")");
              return stringBuilder.toString();
            } 
            return "always";
          } 
          return "never";
        } 
        return "shortEdges";
      } 
      return "default";
    }
    
    private static String softInputModeToString(int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      int i = param1Int & 0xF;
      if (i != 0) {
        stringBuilder.append("state=");
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                if (i != 5) {
                  stringBuilder.append(i);
                } else {
                  stringBuilder.append("always_visible");
                } 
              } else {
                stringBuilder.append("visible");
              } 
            } else {
              stringBuilder.append("always_hidden");
            } 
          } else {
            stringBuilder.append("hidden");
          } 
        } else {
          stringBuilder.append("unchanged");
        } 
        stringBuilder.append(' ');
      } 
      i = param1Int & 0xF0;
      if (i != 0) {
        stringBuilder.append("adjust=");
        if (i != 16) {
          if (i != 32) {
            if (i != 48) {
              stringBuilder.append(i);
            } else {
              stringBuilder.append("nothing");
            } 
          } else {
            stringBuilder.append("pan");
          } 
        } else {
          stringBuilder.append("resize");
        } 
        stringBuilder.append(' ');
      } 
      if ((param1Int & 0x100) != 0) {
        stringBuilder.append("forwardNavigation");
        stringBuilder.append(' ');
      } 
      stringBuilder.deleteCharAt(stringBuilder.length() - 1);
      return stringBuilder.toString();
    }
    
    private static String rotationAnimationToString(int param1Int) {
      if (param1Int != -1) {
        if (param1Int != 0) {
          if (param1Int != 1) {
            if (param1Int != 2) {
              if (param1Int != 3)
                return Integer.toString(param1Int); 
              return "SEAMLESS";
            } 
            return "JUMPCUT";
          } 
          return "CROSSFADE";
        } 
        return "ROTATE";
      } 
      return "UNSPECIFIED";
    }
    
    private static String inputFeatureToString(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 4)
            return Integer.toString(param1Int); 
          return "DISABLE_USER_ACTIVITY";
        } 
        return "NO_INPUT_CHANNEL";
      } 
      return "DISABLE_POINTER_GESTURES";
    }
    
    @Retention(RetentionPolicy.SOURCE)
    class LayoutInDisplayCutoutMode implements Annotation {}
    
    @Retention(RetentionPolicy.SOURCE)
    class SoftInputModeFlags implements Annotation {}
    
    @SystemApi
    @Retention(RetentionPolicy.SOURCE)
    class SystemFlags implements Annotation {}
    
    @Retention(RetentionPolicy.SOURCE)
    class WindowType implements Annotation {}
  }
  
  class null implements Parcelable.Creator<LayoutParams> {
    public WindowManager.LayoutParams createFromParcel(Parcel param1Parcel) {
      return new WindowManager.LayoutParams(param1Parcel);
    }
    
    public WindowManager.LayoutParams[] newArray(int param1Int) {
      return new WindowManager.LayoutParams[param1Int];
    }
  }
  
  class RemoveContentMode implements Annotation {}
  
  class ScreenshotSource implements Annotation {
    public static final int SCREENSHOT_ACCESSIBILITY_ACTIONS = 4;
    
    public static final int SCREENSHOT_GLOBAL_ACTIONS = 0;
    
    public static final int SCREENSHOT_KEY_CHORD = 1;
    
    public static final int SCREENSHOT_KEY_OTHER = 2;
    
    public static final int SCREENSHOT_OTHER = 5;
    
    public static final int SCREENSHOT_OVERVIEW = 3;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class TransitionFlags implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class TransitionType implements Annotation {}
}
