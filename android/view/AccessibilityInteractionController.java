package android.view;

import android.content.res.Resources;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.style.ClickableSpan;
import android.util.LongSparseArray;
import android.util.Slog;
import android.view.accessibility.AccessibilityInteractionClient;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeIdManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.accessibility.AccessibilityRequestPreparer;
import android.view.accessibility.IAccessibilityInteractionConnectionCallback;
import com.android.internal.os.SomeArgs;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

public final class AccessibilityInteractionController {
  private final ArrayList<AccessibilityNodeInfo> mTempAccessibilityNodeInfoList = new ArrayList<>();
  
  private final Object mLock = new Object();
  
  private final ArrayList<View> mTempArrayList = new ArrayList<>();
  
  private final Point mTempPoint = new Point();
  
  private final Rect mTempRect = new Rect();
  
  private final Rect mTempRect1 = new Rect();
  
  private final Rect mTempRect2 = new Rect();
  
  private final RectF mTempRectF = new RectF();
  
  private static final boolean CONSIDER_REQUEST_PREPARERS = false;
  
  private static final boolean ENFORCE_NODE_TREE_CONSISTENT = false;
  
  private static final boolean IGNORE_REQUEST_PREPARERS = true;
  
  private static final String LOG_TAG = "AccessibilityInteractionController";
  
  private static final long REQUEST_PREPARER_TIMEOUT_MS = 500L;
  
  private final AccessibilityManager mA11yManager;
  
  private int mActiveRequestPreparerId;
  
  private AddNodeInfosForViewId mAddNodeInfosForViewId;
  
  private final PrivateHandler mHandler;
  
  private List<MessageHolder> mMessagesWaitingForRequestPreparer;
  
  private final long mMyLooperThreadId;
  
  private final int mMyProcessId;
  
  private int mNumActiveRequestPreparers;
  
  private final AccessibilityNodePrefetcher mPrefetcher;
  
  private final ViewRootImpl mViewRootImpl;
  
  public AccessibilityInteractionController(ViewRootImpl paramViewRootImpl) {
    Looper looper = paramViewRootImpl.mHandler.getLooper();
    this.mMyLooperThreadId = looper.getThread().getId();
    this.mMyProcessId = Process.myPid();
    this.mHandler = new PrivateHandler(looper);
    this.mViewRootImpl = paramViewRootImpl;
    this.mPrefetcher = new AccessibilityNodePrefetcher();
    this.mA11yManager = (AccessibilityManager)this.mViewRootImpl.mContext.getSystemService(AccessibilityManager.class);
  }
  
  private void scheduleMessage(Message paramMessage, int paramInt, long paramLong, boolean paramBoolean) {
    if (paramBoolean || 
      !holdOffMessageIfNeeded(paramMessage, paramInt, paramLong)) {
      if (paramInt == this.mMyProcessId && paramLong == this.mMyLooperThreadId) {
        PrivateHandler privateHandler = this.mHandler;
        if (privateHandler.hasAccessibilityCallback(paramMessage)) {
          AccessibilityInteractionClient accessibilityInteractionClient = AccessibilityInteractionClient.getInstanceForThread(paramLong);
          accessibilityInteractionClient.setSameThreadMessage(paramMessage);
          return;
        } 
      } 
      if (!this.mHandler.hasAccessibilityCallback(paramMessage) && 
        Thread.currentThread().getId() == this.mMyLooperThreadId) {
        this.mHandler.handleMessage(paramMessage);
      } else {
        this.mHandler.sendMessage(paramMessage);
      } 
    } 
  }
  
  private boolean isShown(View paramView) {
    boolean bool;
    if (paramView != null && paramView.getWindowVisibility() == 0 && paramView.isShown()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void findAccessibilityNodeInfoByAccessibilityIdClientThread(long paramLong1, Region paramRegion, int paramInt1, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt2, int paramInt3, long paramLong2, MagnificationSpec paramMagnificationSpec, Bundle paramBundle) {
    Message message = this.mHandler.obtainMessage();
    message.what = 2;
    message.arg1 = paramInt2;
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.argi1 = AccessibilityNodeInfo.getAccessibilityViewId(paramLong1);
    someArgs.argi2 = AccessibilityNodeInfo.getVirtualDescendantId(paramLong1);
    someArgs.argi3 = paramInt1;
    someArgs.arg1 = paramIAccessibilityInteractionConnectionCallback;
    someArgs.arg2 = paramMagnificationSpec;
    someArgs.arg3 = paramRegion;
    someArgs.arg4 = paramBundle;
    message.obj = someArgs;
    scheduleMessage(message, paramInt3, paramLong2, false);
  }
  
  private boolean holdOffMessageIfNeeded(Message paramMessage, int paramInt, long paramLong) {
    synchronized (this.mLock) {
      if (this.mNumActiveRequestPreparers != 0) {
        queueMessageToHandleOncePrepared(paramMessage, paramInt, paramLong);
        return true;
      } 
      if (paramMessage.what != 2)
        return false; 
      SomeArgs someArgs = (SomeArgs)paramMessage.obj;
      Bundle bundle = (Bundle)someArgs.arg4;
      if (bundle == null)
        return false; 
      int i = someArgs.argi1;
      AccessibilityManager accessibilityManager = this.mA11yManager;
      List<AccessibilityRequestPreparer> list = accessibilityManager.getRequestPreparersForAccessibilityId(i);
      if (list == null)
        return false; 
      String str = bundle.getString("android.view.accessibility.AccessibilityNodeInfo.extra_data_requested");
      if (str == null)
        return false; 
      this.mNumActiveRequestPreparers = list.size();
      for (i = 0; i < list.size(); i++) {
        Message message1 = this.mHandler.obtainMessage(7);
        SomeArgs someArgs1 = SomeArgs.obtain();
        if (someArgs.argi2 == Integer.MAX_VALUE) {
          j = -1;
        } else {
          j = someArgs.argi2;
        } 
        someArgs1.argi1 = j;
        someArgs1.arg1 = list.get(i);
        someArgs1.arg2 = str;
        someArgs1.arg3 = bundle;
        Message message2 = this.mHandler.obtainMessage(8);
        int j = this.mActiveRequestPreparerId + 1;
        message2.arg1 = j;
        someArgs1.arg4 = message2;
        message1.obj = someArgs1;
        scheduleMessage(message1, paramInt, paramLong, true);
        this.mHandler.obtainMessage(9);
        this.mHandler.sendEmptyMessageDelayed(9, 500L);
      } 
      queueMessageToHandleOncePrepared(paramMessage, paramInt, paramLong);
      return true;
    } 
  }
  
  private void prepareForExtraDataRequestUiThread(Message paramMessage) {
    SomeArgs someArgs = (SomeArgs)paramMessage.obj;
    int i = someArgs.argi1;
    AccessibilityRequestPreparer accessibilityRequestPreparer = (AccessibilityRequestPreparer)someArgs.arg1;
    String str = (String)someArgs.arg2;
    Bundle bundle = (Bundle)someArgs.arg3;
    Message message = (Message)someArgs.arg4;
    accessibilityRequestPreparer.onPrepareExtraData(i, str, bundle, message);
  }
  
  private void queueMessageToHandleOncePrepared(Message paramMessage, int paramInt, long paramLong) {
    if (this.mMessagesWaitingForRequestPreparer == null)
      this.mMessagesWaitingForRequestPreparer = new ArrayList<>(1); 
    MessageHolder messageHolder = new MessageHolder(paramMessage, paramInt, paramLong);
    this.mMessagesWaitingForRequestPreparer.add(messageHolder);
  }
  
  private void requestPreparerDoneUiThread(Message paramMessage) {
    synchronized (this.mLock) {
      if (paramMessage.arg1 != this.mActiveRequestPreparerId) {
        Slog.e("AccessibilityInteractionController", "Surprising AccessibilityRequestPreparer callback (likely late)");
        return;
      } 
      int i = this.mNumActiveRequestPreparers - 1;
      if (i <= 0) {
        this.mHandler.removeMessages(9);
        scheduleAllMessagesWaitingForRequestPreparerLocked();
      } 
      return;
    } 
  }
  
  private void requestPreparerTimeoutUiThread() {
    synchronized (this.mLock) {
      Slog.e("AccessibilityInteractionController", "AccessibilityRequestPreparer timed out");
      scheduleAllMessagesWaitingForRequestPreparerLocked();
      return;
    } 
  }
  
  private void scheduleAllMessagesWaitingForRequestPreparerLocked() {
    int i = this.mMessagesWaitingForRequestPreparer.size();
    byte b = 0;
    while (true) {
      boolean bool = false;
      if (b < i) {
        MessageHolder messageHolder = this.mMessagesWaitingForRequestPreparer.get(b);
        Message message = messageHolder.mMessage;
        int j = messageHolder.mInterrogatingPid;
        long l = messageHolder.mInterrogatingTid;
        if (b == 0)
          bool = true; 
        scheduleMessage(message, j, l, bool);
        b++;
        continue;
      } 
      break;
    } 
    this.mMessagesWaitingForRequestPreparer.clear();
    this.mNumActiveRequestPreparers = 0;
    this.mActiveRequestPreparerId = -1;
  }
  
  private void findAccessibilityNodeInfoByAccessibilityIdUiThread(Message paramMessage) {
    int i = paramMessage.arg1;
    SomeArgs someArgs = (SomeArgs)paramMessage.obj;
    int j = someArgs.argi1;
    int k = someArgs.argi2;
    int m = someArgs.argi3;
    IAccessibilityInteractionConnectionCallback iAccessibilityInteractionConnectionCallback = (IAccessibilityInteractionConnectionCallback)someArgs.arg1;
    MagnificationSpec magnificationSpec = (MagnificationSpec)someArgs.arg2;
    Region region = (Region)someArgs.arg3;
    null = (Bundle)someArgs.arg4;
    someArgs.recycle();
    ArrayList<AccessibilityNodeInfo> arrayList = this.mTempAccessibilityNodeInfoList;
    arrayList.clear();
    try {
      if (this.mViewRootImpl.mView == null || this.mViewRootImpl.mAttachInfo == null)
        return; 
      this.mViewRootImpl.mAttachInfo.mAccessibilityFetchFlags = i;
      View view = findViewByAccessibilityId(j);
      if (view != null && isShown(view)) {
        AccessibilityNodePrefetcher accessibilityNodePrefetcher = this.mPrefetcher;
        try {
          accessibilityNodePrefetcher.prefetchAccessibilityNodeInfos(view, k, i, arrayList, null);
        } finally {}
      } 
      return;
    } finally {
      updateInfosForViewportAndReturnFindNodeResult(arrayList, iAccessibilityInteractionConnectionCallback, m, magnificationSpec, region);
    } 
  }
  
  public void findAccessibilityNodeInfosByViewIdClientThread(long paramLong1, String paramString, Region paramRegion, int paramInt1, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt2, int paramInt3, long paramLong2, MagnificationSpec paramMagnificationSpec) {
    Message message = this.mHandler.obtainMessage();
    message.what = 3;
    message.arg1 = paramInt2;
    message.arg2 = AccessibilityNodeInfo.getAccessibilityViewId(paramLong1);
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.argi1 = paramInt1;
    someArgs.arg1 = paramIAccessibilityInteractionConnectionCallback;
    someArgs.arg2 = paramMagnificationSpec;
    someArgs.arg3 = paramString;
    someArgs.arg4 = paramRegion;
    message.obj = someArgs;
    scheduleMessage(message, paramInt3, paramLong2, false);
  }
  
  private void findAccessibilityNodeInfosByViewIdUiThread(Message paramMessage) {
    int i = paramMessage.arg1;
    int j = paramMessage.arg2;
    SomeArgs someArgs = (SomeArgs)paramMessage.obj;
    int k = someArgs.argi1;
    IAccessibilityInteractionConnectionCallback iAccessibilityInteractionConnectionCallback = (IAccessibilityInteractionConnectionCallback)someArgs.arg1;
    MagnificationSpec magnificationSpec = (MagnificationSpec)someArgs.arg2;
    null = (String)someArgs.arg3;
    Region region = (Region)someArgs.arg4;
    someArgs.recycle();
    ArrayList<AccessibilityNodeInfo> arrayList = this.mTempAccessibilityNodeInfoList;
    arrayList.clear();
    try {
      if (this.mViewRootImpl.mView == null || this.mViewRootImpl.mAttachInfo == null)
        return; 
      this.mViewRootImpl.mAttachInfo.mAccessibilityFetchFlags = i;
      View view = findViewByAccessibilityId(j);
      if (view != null) {
        Resources resources = view.getContext().getResources();
        j = resources.getIdentifier(null, null, null);
        if (j <= 0)
          return; 
        try {
          if (this.mAddNodeInfosForViewId == null) {
            AddNodeInfosForViewId addNodeInfosForViewId = new AddNodeInfosForViewId();
            this(this);
            this.mAddNodeInfosForViewId = addNodeInfosForViewId;
          } 
          this.mAddNodeInfosForViewId.init(j, arrayList);
          view.findViewByPredicate(this.mAddNodeInfosForViewId);
          this.mAddNodeInfosForViewId.reset();
        } finally {}
      } 
      return;
    } finally {
      updateInfosForViewportAndReturnFindNodeResult(arrayList, iAccessibilityInteractionConnectionCallback, k, magnificationSpec, region);
    } 
  }
  
  public void findAccessibilityNodeInfosByTextClientThread(long paramLong1, String paramString, Region paramRegion, int paramInt1, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt2, int paramInt3, long paramLong2, MagnificationSpec paramMagnificationSpec) {
    Message message = this.mHandler.obtainMessage();
    message.what = 4;
    message.arg1 = paramInt2;
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramString;
    someArgs.arg2 = paramIAccessibilityInteractionConnectionCallback;
    someArgs.arg3 = paramMagnificationSpec;
    someArgs.argi1 = AccessibilityNodeInfo.getAccessibilityViewId(paramLong1);
    someArgs.argi2 = AccessibilityNodeInfo.getVirtualDescendantId(paramLong1);
    someArgs.argi3 = paramInt1;
    someArgs.arg4 = paramRegion;
    message.obj = someArgs;
    scheduleMessage(message, paramInt3, paramLong2, false);
  }
  
  private void findAccessibilityNodeInfosByTextUiThread(Message paramMessage) {
    ArrayList<AccessibilityNodeInfo> arrayList;
    int i = paramMessage.arg1;
    null = (SomeArgs)paramMessage.obj;
    String str = (String)null.arg1;
    IAccessibilityInteractionConnectionCallback iAccessibilityInteractionConnectionCallback = (IAccessibilityInteractionConnectionCallback)null.arg2;
    MagnificationSpec magnificationSpec = (MagnificationSpec)null.arg3;
    int j = null.argi1;
    int k = null.argi2;
    int m = null.argi3;
    Region region = (Region)null.arg4;
    null.recycle();
    View view = null;
    null = null;
    try {
      if (this.mViewRootImpl.mView == null || this.mViewRootImpl.mAttachInfo == null)
        return; 
      this.mViewRootImpl.mAttachInfo.mAccessibilityFetchFlags = i;
      View view1 = findViewByAccessibilityId(j);
      if (view1 != null)
        try {
          if (isShown(view1)) {
            AccessibilityNodeProvider accessibilityNodeProvider = view1.getAccessibilityNodeProvider();
            if (accessibilityNodeProvider != null) {
              List<AccessibilityNodeInfo> list = accessibilityNodeProvider.findAccessibilityNodeInfosByText(str, k);
            } else if (k == -1) {
              ArrayList<View> arrayList1 = this.mTempArrayList;
              arrayList1.clear();
              view1.findViewsWithText(arrayList1, str, 7);
              if (!arrayList1.isEmpty()) {
                ArrayList<AccessibilityNodeInfo> arrayList2 = this.mTempAccessibilityNodeInfoList;
                try {
                
                } finally {
                  view = null;
                  ArrayList<AccessibilityNodeInfo> arrayList3 = arrayList2;
                  View view2 = view;
                } 
              } 
            } 
          } 
        } finally {} 
      return;
    } finally {
      updateInfosForViewportAndReturnFindNodeResult(arrayList, iAccessibilityInteractionConnectionCallback, m, magnificationSpec, region);
    } 
  }
  
  public void findFocusClientThread(long paramLong1, int paramInt1, Region paramRegion, int paramInt2, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt3, int paramInt4, long paramLong2, MagnificationSpec paramMagnificationSpec) {
    Message message = this.mHandler.obtainMessage();
    message.what = 5;
    message.arg1 = paramInt3;
    message.arg2 = paramInt1;
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.argi1 = paramInt2;
    someArgs.argi2 = AccessibilityNodeInfo.getAccessibilityViewId(paramLong1);
    someArgs.argi3 = AccessibilityNodeInfo.getVirtualDescendantId(paramLong1);
    someArgs.arg1 = paramIAccessibilityInteractionConnectionCallback;
    someArgs.arg2 = paramMagnificationSpec;
    someArgs.arg3 = paramRegion;
    message.obj = someArgs;
    scheduleMessage(message, paramInt4, paramLong2, false);
  }
  
  private void findFocusUiThread(Message paramMessage) {
    AccessibilityNodeInfo accessibilityNodeInfo;
    int i = paramMessage.arg1;
    int j = paramMessage.arg2;
    null = (SomeArgs)paramMessage.obj;
    int k = null.argi1;
    int m = null.argi2;
    int n = null.argi3;
    IAccessibilityInteractionConnectionCallback iAccessibilityInteractionConnectionCallback = (IAccessibilityInteractionConnectionCallback)null.arg1;
    MagnificationSpec magnificationSpec = (MagnificationSpec)null.arg2;
    Region region = (Region)null.arg3;
    null.recycle();
    View view1 = null;
    SomeArgs someArgs = null;
    View view2 = null;
    AccessibilityNodeProvider accessibilityNodeProvider = null;
    View view3 = view2;
    try {
      if (this.mViewRootImpl.mView != null) {
        view3 = view2;
        if (this.mViewRootImpl.mAttachInfo != null) {
          AccessibilityNodeInfo accessibilityNodeInfo1;
          view3 = view2;
          this.mViewRootImpl.mAttachInfo.mAccessibilityFetchFlags = i;
          view3 = view2;
          View view = findViewByAccessibilityId(m);
          null = someArgs;
          if (view != null) {
            null = someArgs;
            view3 = view2;
            if (isShown(view))
              if (j != 1) {
                if (j == 2) {
                  view3 = view2;
                  view1 = this.mViewRootImpl.mAccessibilityFocusedHost;
                  null = someArgs;
                  if (view1 != null) {
                    view3 = view2;
                    if (!ViewRootImpl.isViewDescendantOf(view1, view)) {
                      null = someArgs;
                    } else {
                      view3 = view2;
                      if (!isShown(view1)) {
                        null = someArgs;
                      } else {
                        view3 = view2;
                        AccessibilityNodeProvider accessibilityNodeProvider1 = view1.getAccessibilityNodeProvider();
                        if (accessibilityNodeProvider1 != null) {
                          accessibilityNodeProvider1 = accessibilityNodeProvider;
                          view3 = view2;
                          if (this.mViewRootImpl.mAccessibilityFocusedVirtualView != null) {
                            view3 = view2;
                            accessibilityNodeInfo1 = AccessibilityNodeInfo.obtain(this.mViewRootImpl.mAccessibilityFocusedVirtualView);
                          } 
                        } else {
                          accessibilityNodeProvider1 = accessibilityNodeProvider;
                          if (n == -1) {
                            view3 = view2;
                            accessibilityNodeInfo1 = view1.createAccessibilityNodeInfo();
                          } 
                        } 
                      } 
                    } 
                  } 
                } else {
                  view3 = view2;
                  IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
                  view3 = view2;
                  StringBuilder stringBuilder = new StringBuilder();
                  view3 = view2;
                  this();
                  view3 = view2;
                  stringBuilder.append("Unknown focus type: ");
                  view3 = view2;
                  stringBuilder.append(j);
                  view3 = view2;
                  this(stringBuilder.toString());
                  view3 = view2;
                  throw illegalArgumentException;
                } 
              } else {
                view3 = view2;
                View view4 = view.findFocus();
                view3 = view2;
                if (!isShown(view4)) {
                  null = someArgs;
                } else {
                  view3 = view2;
                  AccessibilityNodeProvider accessibilityNodeProvider1 = view4.getAccessibilityNodeProvider();
                  view3 = view1;
                  if (accessibilityNodeProvider1 != null) {
                    view3 = view2;
                    AccessibilityNodeInfo accessibilityNodeInfo2 = accessibilityNodeProvider1.findFocus(j);
                  } 
                  accessibilityNodeInfo1 = accessibilityNodeInfo;
                  if (accessibilityNodeInfo == null)
                    accessibilityNodeInfo1 = view4.createAccessibilityNodeInfo(); 
                } 
              }  
          } 
          return;
        } 
      } 
      return;
    } finally {
      updateInfoForViewportAndReturnFindNodeResult(accessibilityNodeInfo, iAccessibilityInteractionConnectionCallback, k, magnificationSpec, region);
    } 
  }
  
  public void focusSearchClientThread(long paramLong1, int paramInt1, Region paramRegion, int paramInt2, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt3, int paramInt4, long paramLong2, MagnificationSpec paramMagnificationSpec) {
    Message message = this.mHandler.obtainMessage();
    message.what = 6;
    message.arg1 = paramInt3;
    message.arg2 = AccessibilityNodeInfo.getAccessibilityViewId(paramLong1);
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.argi2 = paramInt1;
    someArgs.argi3 = paramInt2;
    someArgs.arg1 = paramIAccessibilityInteractionConnectionCallback;
    someArgs.arg2 = paramMagnificationSpec;
    someArgs.arg3 = paramRegion;
    message.obj = someArgs;
    scheduleMessage(message, paramInt4, paramLong2, false);
  }
  
  private void focusSearchUiThread(Message paramMessage) {
    int i = paramMessage.arg1;
    int j = paramMessage.arg2;
    null = (SomeArgs)paramMessage.obj;
    int k = null.argi2;
    int m = null.argi3;
    IAccessibilityInteractionConnectionCallback iAccessibilityInteractionConnectionCallback = (IAccessibilityInteractionConnectionCallback)null.arg1;
    MagnificationSpec magnificationSpec = (MagnificationSpec)null.arg2;
    Region region = (Region)null.arg3;
    null.recycle();
    SomeArgs someArgs = null;
    try {
      AccessibilityNodeInfo accessibilityNodeInfo;
      if (this.mViewRootImpl.mView == null || this.mViewRootImpl.mAttachInfo == null)
        return; 
      this.mViewRootImpl.mAttachInfo.mAccessibilityFetchFlags = i;
      View view = findViewByAccessibilityId(j);
      null = someArgs;
      if (view != null) {
        null = someArgs;
        if (isShown(view)) {
          view = view.focusSearch(k);
          null = someArgs;
          if (view != null)
            accessibilityNodeInfo = view.createAccessibilityNodeInfo(); 
        } 
      } 
      return;
    } finally {
      updateInfoForViewportAndReturnFindNodeResult(null, iAccessibilityInteractionConnectionCallback, m, magnificationSpec, region);
    } 
  }
  
  public void performAccessibilityActionClientThread(long paramLong1, int paramInt1, Bundle paramBundle, int paramInt2, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt3, int paramInt4, long paramLong2) {
    Message message = this.mHandler.obtainMessage();
    message.what = 1;
    message.arg1 = paramInt3;
    message.arg2 = AccessibilityNodeInfo.getAccessibilityViewId(paramLong1);
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.argi1 = AccessibilityNodeInfo.getVirtualDescendantId(paramLong1);
    someArgs.argi2 = paramInt1;
    someArgs.argi3 = paramInt2;
    someArgs.arg1 = paramIAccessibilityInteractionConnectionCallback;
    someArgs.arg2 = paramBundle;
    message.obj = someArgs;
    scheduleMessage(message, paramInt4, paramLong2, false);
  }
  
  private void performAccessibilityActionUiThread(Message paramMessage) {
    int i = paramMessage.arg1;
    int j = paramMessage.arg2;
    SomeArgs someArgs = (SomeArgs)paramMessage.obj;
    int k = someArgs.argi1;
    int m = someArgs.argi2;
    int n = someArgs.argi3;
    IAccessibilityInteractionConnectionCallback iAccessibilityInteractionConnectionCallback = (IAccessibilityInteractionConnectionCallback)someArgs.arg1;
    null = (Bundle)someArgs.arg2;
    someArgs.recycle();
    boolean bool = false;
    try {
      if (this.mViewRootImpl.mView == null || this.mViewRootImpl.mAttachInfo == null || this.mViewRootImpl.mStopped || this.mViewRootImpl.mPausedForTransition)
        return; 
      this.mViewRootImpl.mAttachInfo.mAccessibilityFetchFlags = i;
      View view = findViewByAccessibilityId(j);
      boolean bool1 = bool;
      if (view != null) {
        bool1 = bool;
        if (isShown(view))
          if (m == 16908669) {
            bool1 = handleClickableSpanActionUiThread(view, k, null);
          } else {
            AccessibilityNodeProvider accessibilityNodeProvider = view.getAccessibilityNodeProvider();
            if (accessibilityNodeProvider != null) {
              bool1 = accessibilityNodeProvider.performAction(k, m, null);
            } else {
              bool1 = bool;
              if (k == -1)
                bool1 = view.performAccessibilityAction(m, null); 
            } 
          }  
      } 
      return;
    } finally {
      try {
        this.mViewRootImpl.mAttachInfo.mAccessibilityFetchFlags = 0;
        remoteException.setPerformAccessibilityActionResult(false, n);
      } catch (RemoteException remoteException1) {}
    } 
  }
  
  public void clearAccessibilityFocusClientThread() {
    Message message = this.mHandler.obtainMessage();
    message.what = 101;
    scheduleMessage(message, 0, 0L, false);
  }
  
  private void clearAccessibilityFocusUiThread() {
    if (this.mViewRootImpl.mView == null || this.mViewRootImpl.mAttachInfo == null)
      return; 
    try {
      this.mViewRootImpl.mAttachInfo.mAccessibilityFetchFlags = 8;
      View view = this.mViewRootImpl.mView;
      if (view != null && isShown(view)) {
        AccessibilityNodeInfo.AccessibilityAction accessibilityAction;
        View view1 = this.mViewRootImpl.mAccessibilityFocusedHost;
        if (view1 == null || !ViewRootImpl.isViewDescendantOf(view1, view))
          return; 
        AccessibilityNodeProvider accessibilityNodeProvider = view1.getAccessibilityNodeProvider();
        AccessibilityNodeInfo accessibilityNodeInfo = this.mViewRootImpl.mAccessibilityFocusedVirtualView;
        if (accessibilityNodeProvider != null && accessibilityNodeInfo != null) {
          long l = accessibilityNodeInfo.getSourceNodeId();
          int i = AccessibilityNodeInfo.getVirtualDescendantId(l);
          accessibilityAction = AccessibilityNodeInfo.AccessibilityAction.ACTION_CLEAR_ACCESSIBILITY_FOCUS;
          int j = accessibilityAction.getId();
          accessibilityNodeProvider.performAction(i, j, null);
        } else {
          AccessibilityNodeInfo.AccessibilityAction accessibilityAction1 = AccessibilityNodeInfo.AccessibilityAction.ACTION_CLEAR_ACCESSIBILITY_FOCUS;
          int i = accessibilityAction1.getId();
          accessibilityAction.performAccessibilityAction(i, null);
        } 
      } 
      return;
    } finally {
      this.mViewRootImpl.mAttachInfo.mAccessibilityFetchFlags = 0;
    } 
  }
  
  public void notifyOutsideTouchClientThread() {
    Message message = this.mHandler.obtainMessage();
    message.what = 102;
    scheduleMessage(message, 0, 0L, false);
  }
  
  private void notifyOutsideTouchUiThread() {
    if (this.mViewRootImpl.mView == null || this.mViewRootImpl.mAttachInfo == null || this.mViewRootImpl.mStopped || this.mViewRootImpl.mPausedForTransition)
      return; 
    View view = this.mViewRootImpl.mView;
    if (view != null && isShown(view)) {
      long l = SystemClock.uptimeMillis();
      MotionEvent motionEvent = MotionEvent.obtain(l, l, 4, 0.0F, 0.0F, 0);
      motionEvent.setSource(4098);
      this.mViewRootImpl.dispatchInputEvent(motionEvent);
    } 
  }
  
  private View findViewByAccessibilityId(int paramInt) {
    if (paramInt == 2147483646)
      return this.mViewRootImpl.mView; 
    return AccessibilityNodeIdManager.getInstance().findView(paramInt);
  }
  
  private void applyAppScaleAndMagnificationSpecIfNeeded(List<AccessibilityNodeInfo> paramList, MagnificationSpec paramMagnificationSpec) {
    if (paramList == null)
      return; 
    float f = this.mViewRootImpl.mAttachInfo.mApplicationScale;
    if (shouldApplyAppScaleAndMagnificationSpec(f, paramMagnificationSpec)) {
      int i = paramList.size();
      for (byte b = 0; b < i; b++) {
        AccessibilityNodeInfo accessibilityNodeInfo = paramList.get(b);
        applyAppScaleAndMagnificationSpecIfNeeded(accessibilityNodeInfo, paramMagnificationSpec);
      } 
    } 
  }
  
  private void adjustIsVisibleToUserIfNeeded(List<AccessibilityNodeInfo> paramList, Region paramRegion) {
    if (paramRegion == null || paramList == null)
      return; 
    int i = paramList.size();
    for (byte b = 0; b < i; b++) {
      AccessibilityNodeInfo accessibilityNodeInfo = paramList.get(b);
      adjustIsVisibleToUserIfNeeded(accessibilityNodeInfo, paramRegion);
    } 
  }
  
  private void adjustIsVisibleToUserIfNeeded(AccessibilityNodeInfo paramAccessibilityNodeInfo, Region paramRegion) {
    if (paramRegion == null || paramAccessibilityNodeInfo == null)
      return; 
    Rect rect = this.mTempRect;
    paramAccessibilityNodeInfo.getBoundsInScreen(rect);
    if (paramRegion.quickReject(rect) && !shouldBypassAdjustIsVisible())
      paramAccessibilityNodeInfo.setVisibleToUser(false); 
  }
  
  private boolean shouldBypassAdjustIsVisible() {
    int i = this.mViewRootImpl.mOrigWindowType;
    if (i == 2011)
      return true; 
    return false;
  }
  
  private void adjustBoundsInScreenIfNeeded(List<AccessibilityNodeInfo> paramList) {
    if (paramList == null || shouldBypassAdjustBoundsInScreen())
      return; 
    int i = paramList.size();
    for (byte b = 0; b < i; b++) {
      AccessibilityNodeInfo accessibilityNodeInfo = paramList.get(b);
      adjustBoundsInScreenIfNeeded(accessibilityNodeInfo);
    } 
  }
  
  private void adjustBoundsInScreenIfNeeded(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    if (paramAccessibilityNodeInfo == null || shouldBypassAdjustBoundsInScreen())
      return; 
    Rect rect = this.mTempRect;
    paramAccessibilityNodeInfo.getBoundsInScreen(rect);
    rect.offset(this.mViewRootImpl.mAttachInfo.mLocationInParentDisplay.x, this.mViewRootImpl.mAttachInfo.mLocationInParentDisplay.y);
    paramAccessibilityNodeInfo.setBoundsInScreen(rect);
  }
  
  private boolean shouldBypassAdjustBoundsInScreen() {
    return this.mViewRootImpl.mAttachInfo.mLocationInParentDisplay.equals(0, 0);
  }
  
  private void applyScreenMatrixIfNeeded(List<AccessibilityNodeInfo> paramList) {
    if (paramList == null || shouldBypassApplyScreenMatrix())
      return; 
    int i = paramList.size();
    for (byte b = 0; b < i; b++) {
      AccessibilityNodeInfo accessibilityNodeInfo = paramList.get(b);
      applyScreenMatrixIfNeeded(accessibilityNodeInfo);
    } 
  }
  
  private void applyScreenMatrixIfNeeded(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    if (paramAccessibilityNodeInfo == null || shouldBypassApplyScreenMatrix())
      return; 
    Rect rect = this.mTempRect;
    RectF rectF = this.mTempRectF;
    Matrix matrix = this.mViewRootImpl.mAttachInfo.mScreenMatrixInEmbeddedHierarchy;
    paramAccessibilityNodeInfo.getBoundsInScreen(rect);
    rectF.set(rect);
    matrix.mapRect(rectF);
    rect.set((int)rectF.left, (int)rectF.top, (int)rectF.right, (int)rectF.bottom);
    paramAccessibilityNodeInfo.setBoundsInScreen(rect);
  }
  
  private boolean shouldBypassApplyScreenMatrix() {
    Matrix matrix = this.mViewRootImpl.mAttachInfo.mScreenMatrixInEmbeddedHierarchy;
    return (matrix == null || matrix.isIdentity());
  }
  
  private void associateLeashedParentIfNeeded(List<AccessibilityNodeInfo> paramList) {
    if (paramList == null || shouldBypassAssociateLeashedParent())
      return; 
    int i = paramList.size();
    for (byte b = 0; b < i; b++) {
      AccessibilityNodeInfo accessibilityNodeInfo = paramList.get(b);
      associateLeashedParentIfNeeded(accessibilityNodeInfo);
    } 
  }
  
  private void associateLeashedParentIfNeeded(AccessibilityNodeInfo paramAccessibilityNodeInfo) {
    if (paramAccessibilityNodeInfo == null || shouldBypassAssociateLeashedParent())
      return; 
    int i = this.mViewRootImpl.mView.getAccessibilityViewId();
    if (i != AccessibilityNodeInfo.getAccessibilityViewId(paramAccessibilityNodeInfo.getSourceNodeId()))
      return; 
    paramAccessibilityNodeInfo.setLeashedParent(this.mViewRootImpl.mAttachInfo.mLeashedParentToken, this.mViewRootImpl.mAttachInfo.mLeashedParentAccessibilityViewId);
  }
  
  private boolean shouldBypassAssociateLeashedParent() {
    boolean bool;
    if (this.mViewRootImpl.mAttachInfo.mLeashedParentToken == null && this.mViewRootImpl.mAttachInfo.mLeashedParentAccessibilityViewId == -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void applyAppScaleAndMagnificationSpecIfNeeded(AccessibilityNodeInfo paramAccessibilityNodeInfo, MagnificationSpec paramMagnificationSpec) {
    if (paramAccessibilityNodeInfo == null)
      return; 
    float f = this.mViewRootImpl.mAttachInfo.mApplicationScale;
    if (!shouldApplyAppScaleAndMagnificationSpec(f, paramMagnificationSpec))
      return; 
    Rect rect1 = this.mTempRect;
    Rect rect2 = this.mTempRect1;
    paramAccessibilityNodeInfo.getBoundsInParent(rect1);
    paramAccessibilityNodeInfo.getBoundsInScreen(rect2);
    if (f != 1.0F) {
      rect1.scale(f);
      rect2.scale(f);
    } 
    if (paramMagnificationSpec != null) {
      rect1.scale(paramMagnificationSpec.scale);
      rect2.scale(paramMagnificationSpec.scale);
      rect2.offset((int)paramMagnificationSpec.offsetX, (int)paramMagnificationSpec.offsetY);
    } 
    paramAccessibilityNodeInfo.setBoundsInParent(rect1);
    paramAccessibilityNodeInfo.setBoundsInScreen(rect2);
    if (paramAccessibilityNodeInfo.hasExtras()) {
      Bundle bundle = paramAccessibilityNodeInfo.getExtras();
      Parcelable[] arrayOfParcelable = bundle.getParcelableArray("android.view.accessibility.extra.DATA_TEXT_CHARACTER_LOCATION_KEY");
      if (arrayOfParcelable != null)
        for (byte b = 0; b < arrayOfParcelable.length; b++) {
          RectF rectF = (RectF)arrayOfParcelable[b];
          rectF.scale(f);
          if (paramMagnificationSpec != null) {
            rectF.scale(paramMagnificationSpec.scale);
            rectF.offset(paramMagnificationSpec.offsetX, paramMagnificationSpec.offsetY);
          } 
        }  
    } 
  }
  
  private boolean shouldApplyAppScaleAndMagnificationSpec(float paramFloat, MagnificationSpec paramMagnificationSpec) {
    return (paramFloat != 1.0F || (paramMagnificationSpec != null && !paramMagnificationSpec.isNop()));
  }
  
  private void updateInfosForViewportAndReturnFindNodeResult(List<AccessibilityNodeInfo> paramList, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt, MagnificationSpec paramMagnificationSpec, Region paramRegion) {
    try {
      this.mViewRootImpl.mAttachInfo.mAccessibilityFetchFlags = 0;
      associateLeashedParentIfNeeded(paramList);
      applyScreenMatrixIfNeeded(paramList);
      adjustBoundsInScreenIfNeeded(paramList);
      adjustIsVisibleToUserIfNeeded(paramList, paramRegion);
      applyAppScaleAndMagnificationSpecIfNeeded(paramList, paramMagnificationSpec);
      paramIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfosResult(paramList, paramInt);
      if (paramList != null)
        paramList.clear(); 
    } catch (RemoteException remoteException) {
    
    } finally {
      recycleMagnificationSpecAndRegionIfNeeded(paramMagnificationSpec, paramRegion);
    } 
  }
  
  private void updateInfoForViewportAndReturnFindNodeResult(AccessibilityNodeInfo paramAccessibilityNodeInfo, IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt, MagnificationSpec paramMagnificationSpec, Region paramRegion) {
    try {
      this.mViewRootImpl.mAttachInfo.mAccessibilityFetchFlags = 0;
      associateLeashedParentIfNeeded(paramAccessibilityNodeInfo);
      applyScreenMatrixIfNeeded(paramAccessibilityNodeInfo);
      adjustBoundsInScreenIfNeeded(paramAccessibilityNodeInfo);
      adjustIsVisibleToUserIfNeeded(paramAccessibilityNodeInfo, paramRegion);
      applyAppScaleAndMagnificationSpecIfNeeded(paramAccessibilityNodeInfo, paramMagnificationSpec);
      paramIAccessibilityInteractionConnectionCallback.setFindAccessibilityNodeInfoResult(paramAccessibilityNodeInfo, paramInt);
    } catch (RemoteException remoteException) {
    
    } finally {
      recycleMagnificationSpecAndRegionIfNeeded(paramMagnificationSpec, paramRegion);
    } 
  }
  
  private void recycleMagnificationSpecAndRegionIfNeeded(MagnificationSpec paramMagnificationSpec, Region paramRegion) {
    if (Process.myPid() != Binder.getCallingPid()) {
      if (paramMagnificationSpec != null)
        paramMagnificationSpec.recycle(); 
    } else if (paramRegion != null) {
      paramRegion.recycle();
    } 
  }
  
  private boolean handleClickableSpanActionUiThread(View paramView, int paramInt, Bundle paramBundle) {
    AccessibilityNodeInfo accessibilityNodeInfo;
    Parcelable parcelable = paramBundle.getParcelable("android.view.accessibility.action.ACTION_ARGUMENT_ACCESSIBLE_CLICKABLE_SPAN");
    if (!(parcelable instanceof android.text.style.AccessibilityClickableSpan))
      return false; 
    paramBundle = null;
    AccessibilityNodeProvider accessibilityNodeProvider = paramView.getAccessibilityNodeProvider();
    if (accessibilityNodeProvider != null) {
      accessibilityNodeInfo = accessibilityNodeProvider.createAccessibilityNodeInfo(paramInt);
    } else if (paramInt == -1) {
      accessibilityNodeInfo = paramView.createAccessibilityNodeInfo();
    } 
    if (accessibilityNodeInfo == null)
      return false; 
    parcelable = parcelable;
    CharSequence charSequence = accessibilityNodeInfo.getOriginalText();
    ClickableSpan clickableSpan = parcelable.findClickableSpan(charSequence);
    if (clickableSpan != null) {
      clickableSpan.onClick(paramView);
      return true;
    } 
    return false;
  }
  
  private class AccessibilityNodePrefetcher {
    private static final int MAX_ACCESSIBILITY_NODE_INFO_BATCH_SIZE = 50;
    
    private final ArrayList<View> mTempViewList = new ArrayList<>();
    
    final AccessibilityInteractionController this$0;
    
    public void prefetchAccessibilityNodeInfos(View param1View, int param1Int1, int param1Int2, List<AccessibilityNodeInfo> param1List, Bundle param1Bundle) {
      AccessibilityNodeInfo accessibilityNodeInfo;
      String str;
      AccessibilityNodeProvider accessibilityNodeProvider = param1View.getAccessibilityNodeProvider();
      if (param1Bundle == null) {
        str = null;
      } else {
        str = param1Bundle.getString("android.view.accessibility.AccessibilityNodeInfo.extra_data_requested");
      } 
      if (accessibilityNodeProvider == null) {
        accessibilityNodeInfo = param1View.createAccessibilityNodeInfo();
        if (accessibilityNodeInfo != null) {
          if (str != null)
            param1View.addExtraDataToAccessibilityNodeInfo(accessibilityNodeInfo, str, param1Bundle); 
          param1List.add(accessibilityNodeInfo);
          if ((param1Int2 & 0x1) != 0)
            prefetchPredecessorsOfRealNode(param1View, param1List); 
          if ((param1Int2 & 0x2) != 0)
            prefetchSiblingsOfRealNode(param1View, param1List); 
          if ((param1Int2 & 0x4) != 0)
            prefetchDescendantsOfRealNode(param1View, param1List); 
        } 
      } else {
        AccessibilityNodeInfo accessibilityNodeInfo1 = accessibilityNodeInfo.createAccessibilityNodeInfo(param1Int1);
        if (accessibilityNodeInfo1 != null) {
          if (str != null)
            accessibilityNodeInfo.addExtraDataToAccessibilityNodeInfo(param1Int1, accessibilityNodeInfo1, str, param1Bundle); 
          param1List.add(accessibilityNodeInfo1);
          if ((param1Int2 & 0x1) != 0)
            prefetchPredecessorsOfVirtualNode(accessibilityNodeInfo1, param1View, (AccessibilityNodeProvider)accessibilityNodeInfo, param1List); 
          if ((param1Int2 & 0x2) != 0)
            prefetchSiblingsOfVirtualNode(accessibilityNodeInfo1, param1View, (AccessibilityNodeProvider)accessibilityNodeInfo, param1List); 
          if ((param1Int2 & 0x4) != 0)
            prefetchDescendantsOfVirtualNode(accessibilityNodeInfo1, (AccessibilityNodeProvider)accessibilityNodeInfo, param1List); 
        } 
      } 
    }
    
    private void enforceNodeTreeConsistent(List<AccessibilityNodeInfo> param1List) {
      LongSparseArray<AccessibilityNodeInfo> longSparseArray = new LongSparseArray();
      int i = param1List.size();
      int j;
      for (j = 0; j < i; j++) {
        AccessibilityNodeInfo accessibilityNodeInfo = param1List.get(j);
        longSparseArray.put(accessibilityNodeInfo.getSourceNodeId(), accessibilityNodeInfo);
      } 
      AccessibilityNodeInfo accessibilityNodeInfo2 = longSparseArray.valueAt(0);
      AccessibilityNodeInfo accessibilityNodeInfo1 = accessibilityNodeInfo2;
      while (accessibilityNodeInfo1 != null) {
        accessibilityNodeInfo2 = accessibilityNodeInfo1;
        accessibilityNodeInfo1 = longSparseArray.get(accessibilityNodeInfo1.getParentNodeId());
      } 
      AccessibilityNodeInfo accessibilityNodeInfo3 = null;
      accessibilityNodeInfo1 = null;
      HashSet<AccessibilityNodeInfo> hashSet = new HashSet();
      LinkedList<AccessibilityNodeInfo> linkedList = new LinkedList();
      linkedList.add(accessibilityNodeInfo2);
      accessibilityNodeInfo2 = accessibilityNodeInfo1;
      while (!linkedList.isEmpty()) {
        accessibilityNodeInfo1 = linkedList.poll();
        if (hashSet.add(accessibilityNodeInfo1)) {
          StringBuilder stringBuilder1;
          AccessibilityInteractionController accessibilityInteractionController2;
          AccessibilityNodeInfo accessibilityNodeInfo = accessibilityNodeInfo3;
          if (accessibilityNodeInfo1.isAccessibilityFocused())
            if (accessibilityNodeInfo3 == null) {
              accessibilityNodeInfo = accessibilityNodeInfo1;
            } else {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Duplicate accessibility focus:");
              stringBuilder1.append(accessibilityNodeInfo1);
              stringBuilder1.append(" in window:");
              accessibilityInteractionController = AccessibilityInteractionController.this;
              stringBuilder1.append(accessibilityInteractionController.mViewRootImpl.mAttachInfo.mAccessibilityWindowId);
              throw new IllegalStateException(stringBuilder1.toString());
            }  
          StringBuilder stringBuilder2 = stringBuilder1;
          if (accessibilityInteractionController.isFocused())
            if (stringBuilder1 == null) {
              accessibilityInteractionController2 = accessibilityInteractionController;
            } else {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Duplicate input focus: ");
              stringBuilder1.append(accessibilityInteractionController);
              stringBuilder1.append(" in window:");
              accessibilityInteractionController = AccessibilityInteractionController.this;
              stringBuilder1.append(accessibilityInteractionController.mViewRootImpl.mAttachInfo.mAccessibilityWindowId);
              throw new IllegalStateException(stringBuilder1.toString());
            }  
          i = accessibilityInteractionController.getChildCount();
          for (j = 0; j < i; j++) {
            long l = accessibilityInteractionController.getChildId(j);
            AccessibilityNodeInfo accessibilityNodeInfo4 = longSparseArray.get(l);
            if (accessibilityNodeInfo4 != null)
              linkedList.add(accessibilityNodeInfo4); 
          } 
          accessibilityNodeInfo3 = accessibilityNodeInfo;
          AccessibilityInteractionController accessibilityInteractionController1 = accessibilityInteractionController2;
          continue;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Duplicate node: ");
        stringBuilder.append(accessibilityInteractionController);
        stringBuilder.append(" in window:");
        AccessibilityInteractionController accessibilityInteractionController = AccessibilityInteractionController.this;
        stringBuilder.append(accessibilityInteractionController.mViewRootImpl.mAttachInfo.mAccessibilityWindowId);
        throw new IllegalStateException(stringBuilder.toString());
      } 
      for (j = longSparseArray.size() - 1; j >= 0; ) {
        accessibilityNodeInfo1 = longSparseArray.valueAt(j);
        if (hashSet.contains(accessibilityNodeInfo1)) {
          j--;
          continue;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Disconnected node: ");
        stringBuilder.append(accessibilityNodeInfo1);
        throw new IllegalStateException(stringBuilder.toString());
      } 
    }
    
    private void prefetchPredecessorsOfRealNode(View param1View, List<AccessibilityNodeInfo> param1List) {
      ViewParent viewParent = param1View.getParentForAccessibility();
      while (viewParent instanceof View && 
        param1List.size() < 50) {
        View view = (View)viewParent;
        AccessibilityNodeInfo accessibilityNodeInfo = view.createAccessibilityNodeInfo();
        if (accessibilityNodeInfo != null)
          param1List.add(accessibilityNodeInfo); 
        viewParent = viewParent.getParentForAccessibility();
      } 
    }
    
    private void prefetchSiblingsOfRealNode(View param1View, List<AccessibilityNodeInfo> param1List) {
      ViewParent viewParent = param1View.getParentForAccessibility();
      if (viewParent instanceof ViewGroup) {
        viewParent = viewParent;
        ArrayList<View> arrayList = this.mTempViewList;
        arrayList.clear();
        try {
          viewParent.addChildrenForAccessibility(arrayList);
          int i = arrayList.size();
          for (byte b = 0; b < i; b++) {
            int j = param1List.size();
            if (j >= 50)
              return; 
            View view = arrayList.get(b);
            if (view.getAccessibilityViewId() != param1View.getAccessibilityViewId()) {
              AccessibilityInteractionController accessibilityInteractionController = AccessibilityInteractionController.this;
              if (accessibilityInteractionController.isShown(view)) {
                AccessibilityNodeInfo accessibilityNodeInfo;
                AccessibilityNodeProvider accessibilityNodeProvider = view.getAccessibilityNodeProvider();
                if (accessibilityNodeProvider == null) {
                  accessibilityNodeInfo = view.createAccessibilityNodeInfo();
                } else {
                  accessibilityNodeInfo = accessibilityNodeProvider.createAccessibilityNodeInfo(-1);
                } 
                if (accessibilityNodeInfo != null)
                  param1List.add(accessibilityNodeInfo); 
              } 
            } 
          } 
        } finally {
          arrayList.clear();
        } 
      } 
    }
    
    private void prefetchDescendantsOfRealNode(View param1View, List<AccessibilityNodeInfo> param1List) {
      AccessibilityNodeInfo accessibilityNodeInfo;
      if (!(param1View instanceof ViewGroup))
        return; 
      HashMap<Object, Object> hashMap = new HashMap<>();
      ArrayList<View> arrayList = this.mTempViewList;
      arrayList.clear();
      try {
        param1View.addChildrenForAccessibility(arrayList);
        int i = arrayList.size();
        for (byte b = 0; b < i; b++) {
          int j = param1List.size();
          if (j >= 50)
            return; 
          param1View = arrayList.get(b);
          if (AccessibilityInteractionController.this.isShown(param1View)) {
            AccessibilityNodeInfo accessibilityNodeInfo1;
            AccessibilityNodeProvider accessibilityNodeProvider = param1View.getAccessibilityNodeProvider();
            if (accessibilityNodeProvider == null) {
              accessibilityNodeInfo1 = param1View.createAccessibilityNodeInfo();
              if (accessibilityNodeInfo1 != null) {
                param1List.add(accessibilityNodeInfo1);
                hashMap.put(param1View, null);
              } 
            } else {
              accessibilityNodeInfo1 = accessibilityNodeInfo1.createAccessibilityNodeInfo(-1);
              if (accessibilityNodeInfo1 != null) {
                param1List.add(accessibilityNodeInfo1);
                hashMap.put(param1View, accessibilityNodeInfo1);
              } 
            } 
          } 
        } 
        arrayList.clear();
        return;
      } finally {
        accessibilityNodeInfo.clear();
      } 
    }
    
    private void prefetchPredecessorsOfVirtualNode(AccessibilityNodeInfo param1AccessibilityNodeInfo, View param1View, AccessibilityNodeProvider param1AccessibilityNodeProvider, List<AccessibilityNodeInfo> param1List) {
      int i = param1List.size();
      long l = param1AccessibilityNodeInfo.getParentNodeId();
      int j = AccessibilityNodeInfo.getAccessibilityViewId(l);
      while (j != Integer.MAX_VALUE) {
        if (param1List.size() >= 50)
          return; 
        int k = AccessibilityNodeInfo.getVirtualDescendantId(l);
        if (k != -1 || 
          j == param1View.getAccessibilityViewId()) {
          param1AccessibilityNodeInfo = param1AccessibilityNodeProvider.createAccessibilityNodeInfo(k);
          if (param1AccessibilityNodeInfo == null) {
            j = param1List.size();
            for (; --j >= i; j--)
              param1List.remove(j); 
            return;
          } 
          param1List.add(param1AccessibilityNodeInfo);
          l = param1AccessibilityNodeInfo.getParentNodeId();
          j = AccessibilityNodeInfo.getAccessibilityViewId(l);
          continue;
        } 
        prefetchPredecessorsOfRealNode(param1View, param1List);
        return;
      } 
    }
    
    private void prefetchSiblingsOfVirtualNode(AccessibilityNodeInfo param1AccessibilityNodeInfo, View param1View, AccessibilityNodeProvider param1AccessibilityNodeProvider, List<AccessibilityNodeInfo> param1List) {
      AccessibilityNodeInfo accessibilityNodeInfo;
      long l = param1AccessibilityNodeInfo.getParentNodeId();
      int i = AccessibilityNodeInfo.getAccessibilityViewId(l);
      int j = AccessibilityNodeInfo.getVirtualDescendantId(l);
      if (j != -1 || 
        i == param1View.getAccessibilityViewId()) {
        AccessibilityNodeInfo accessibilityNodeInfo1 = param1AccessibilityNodeProvider.createAccessibilityNodeInfo(j);
        if (accessibilityNodeInfo1 != null) {
          j = accessibilityNodeInfo1.getChildCount();
          for (i = 0; i < j; i++) {
            if (param1List.size() >= 50)
              return; 
            l = accessibilityNodeInfo1.getChildId(i);
            if (l != param1AccessibilityNodeInfo.getSourceNodeId()) {
              int k = AccessibilityNodeInfo.getVirtualDescendantId(l);
              accessibilityNodeInfo = param1AccessibilityNodeProvider.createAccessibilityNodeInfo(k);
              if (accessibilityNodeInfo != null)
                param1List.add(accessibilityNodeInfo); 
            } 
          } 
        } 
        return;
      } 
      prefetchSiblingsOfRealNode((View)accessibilityNodeInfo, param1List);
    }
    
    private void prefetchDescendantsOfVirtualNode(AccessibilityNodeInfo param1AccessibilityNodeInfo, AccessibilityNodeProvider param1AccessibilityNodeProvider, List<AccessibilityNodeInfo> param1List) {
      int i = param1List.size();
      int j = param1AccessibilityNodeInfo.getChildCount();
      byte b;
      for (b = 0; b < j; b++) {
        if (param1List.size() >= 50)
          return; 
        long l = param1AccessibilityNodeInfo.getChildId(b);
        int k = AccessibilityNodeInfo.getVirtualDescendantId(l);
        AccessibilityNodeInfo accessibilityNodeInfo = param1AccessibilityNodeProvider.createAccessibilityNodeInfo(k);
        if (accessibilityNodeInfo != null)
          param1List.add(accessibilityNodeInfo); 
      } 
      if (param1List.size() < 50) {
        j = param1List.size();
        for (b = 0; b < j - i; b++) {
          param1AccessibilityNodeInfo = param1List.get(i + b);
          prefetchDescendantsOfVirtualNode(param1AccessibilityNodeInfo, param1AccessibilityNodeProvider, param1List);
        } 
      } 
    }
    
    private AccessibilityNodePrefetcher() {}
  }
  
  class PrivateHandler extends Handler {
    private static final int FIRST_NO_ACCESSIBILITY_CALLBACK_MSG = 100;
    
    private static final int MSG_APP_PREPARATION_FINISHED = 8;
    
    private static final int MSG_APP_PREPARATION_TIMEOUT = 9;
    
    private static final int MSG_CLEAR_ACCESSIBILITY_FOCUS = 101;
    
    private static final int MSG_FIND_ACCESSIBILITY_NODE_INFOS_BY_VIEW_ID = 3;
    
    private static final int MSG_FIND_ACCESSIBILITY_NODE_INFO_BY_ACCESSIBILITY_ID = 2;
    
    private static final int MSG_FIND_ACCESSIBILITY_NODE_INFO_BY_TEXT = 4;
    
    private static final int MSG_FIND_FOCUS = 5;
    
    private static final int MSG_FOCUS_SEARCH = 6;
    
    private static final int MSG_NOTIFY_OUTSIDE_TOUCH = 102;
    
    private static final int MSG_PERFORM_ACCESSIBILITY_ACTION = 1;
    
    private static final int MSG_PREPARE_FOR_EXTRA_DATA_REQUEST = 7;
    
    final AccessibilityInteractionController this$0;
    
    public PrivateHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public String getMessageName(Message param1Message) {
      int i = param1Message.what;
      if (i != 101) {
        if (i != 102) {
          StringBuilder stringBuilder;
          switch (i) {
            default:
              stringBuilder = new StringBuilder();
              stringBuilder.append("Unknown message type: ");
              stringBuilder.append(i);
              throw new IllegalArgumentException(stringBuilder.toString());
            case 9:
              return "MSG_APP_PREPARATION_TIMEOUT";
            case 8:
              return "MSG_APP_PREPARATION_FINISHED";
            case 7:
              return "MSG_PREPARE_FOR_EXTRA_DATA_REQUEST";
            case 6:
              return "MSG_FOCUS_SEARCH";
            case 5:
              return "MSG_FIND_FOCUS";
            case 4:
              return "MSG_FIND_ACCESSIBILITY_NODE_INFO_BY_TEXT";
            case 3:
              return "MSG_FIND_ACCESSIBILITY_NODE_INFOS_BY_VIEW_ID";
            case 2:
              return "MSG_FIND_ACCESSIBILITY_NODE_INFO_BY_ACCESSIBILITY_ID";
            case 1:
              break;
          } 
          return "MSG_PERFORM_ACCESSIBILITY_ACTION";
        } 
        return "MSG_NOTIFY_OUTSIDE_TOUCH";
      } 
      return "MSG_CLEAR_ACCESSIBILITY_FOCUS";
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 101) {
        if (i != 102) {
          StringBuilder stringBuilder;
          switch (i) {
            default:
              stringBuilder = new StringBuilder();
              stringBuilder.append("Unknown message type: ");
              stringBuilder.append(i);
              throw new IllegalArgumentException(stringBuilder.toString());
            case 9:
              AccessibilityInteractionController.this.requestPreparerTimeoutUiThread();
              return;
            case 8:
              AccessibilityInteractionController.this.requestPreparerDoneUiThread((Message)stringBuilder);
              return;
            case 7:
              AccessibilityInteractionController.this.prepareForExtraDataRequestUiThread((Message)stringBuilder);
              return;
            case 6:
              AccessibilityInteractionController.this.focusSearchUiThread((Message)stringBuilder);
              return;
            case 5:
              AccessibilityInteractionController.this.findFocusUiThread((Message)stringBuilder);
              return;
            case 4:
              AccessibilityInteractionController.this.findAccessibilityNodeInfosByTextUiThread((Message)stringBuilder);
              return;
            case 3:
              AccessibilityInteractionController.this.findAccessibilityNodeInfosByViewIdUiThread((Message)stringBuilder);
              return;
            case 2:
              AccessibilityInteractionController.this.findAccessibilityNodeInfoByAccessibilityIdUiThread((Message)stringBuilder);
              return;
            case 1:
              break;
          } 
          AccessibilityInteractionController.this.performAccessibilityActionUiThread((Message)stringBuilder);
        } else {
          AccessibilityInteractionController.this.notifyOutsideTouchUiThread();
        } 
      } else {
        AccessibilityInteractionController.this.clearAccessibilityFocusUiThread();
      } 
    }
    
    boolean hasAccessibilityCallback(Message param1Message) {
      boolean bool;
      if (param1Message.what < 100) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  private final class AddNodeInfosForViewId implements Predicate<View> {
    private List<AccessibilityNodeInfo> mInfos;
    
    private int mViewId;
    
    final AccessibilityInteractionController this$0;
    
    private AddNodeInfosForViewId() {
      this.mViewId = -1;
    }
    
    public void init(int param1Int, List<AccessibilityNodeInfo> param1List) {
      this.mViewId = param1Int;
      this.mInfos = param1List;
    }
    
    public void reset() {
      this.mViewId = -1;
      this.mInfos = null;
    }
    
    public boolean test(View param1View) {
      if (param1View.getId() == this.mViewId && AccessibilityInteractionController.this.isShown(param1View))
        this.mInfos.add(param1View.createAccessibilityNodeInfo()); 
      return false;
    }
  }
  
  private static final class MessageHolder {
    final int mInterrogatingPid;
    
    final long mInterrogatingTid;
    
    final Message mMessage;
    
    MessageHolder(Message param1Message, int param1Int, long param1Long) {
      this.mMessage = param1Message;
      this.mInterrogatingPid = param1Int;
      this.mInterrogatingTid = param1Long;
    }
  }
}
