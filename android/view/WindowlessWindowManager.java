package android.view;

import android.content.ClipData;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.util.MergedConfiguration;
import java.util.HashMap;
import java.util.List;

public class WindowlessWindowManager implements IWindowSession {
  class ResizeCompleteCallback {
    public abstract void finished(SurfaceControl.Transaction param1Transaction);
  }
  
  class State {
    int mDisplayId;
    
    IBinder mInputChannelToken;
    
    Region mInputRegion;
    
    WindowManager.LayoutParams mParams;
    
    SurfaceControl mSurfaceControl;
    
    final WindowlessWindowManager this$0;
    
    State(SurfaceControl param1SurfaceControl, WindowManager.LayoutParams param1LayoutParams, int param1Int, IBinder param1IBinder) {
      WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
      this.mSurfaceControl = param1SurfaceControl;
      layoutParams.copyFrom(param1LayoutParams);
      this.mDisplayId = param1Int;
      this.mInputChannelToken = param1IBinder;
    }
  }
  
  final HashMap<IBinder, State> mStateForWindow = new HashMap<>();
  
  final HashMap<IBinder, ResizeCompleteCallback> mResizeCompletionForWindow = new HashMap<>();
  
  private final SurfaceSession mSurfaceSession = new SurfaceSession();
  
  private int mForceHeight = -1;
  
  private int mForceWidth = -1;
  
  private static final String TAG = "WindowlessWindowManager";
  
  private final Configuration mConfiguration;
  
  private final IBinder mHostInputToken;
  
  private final IWindowSession mRealWm;
  
  private final SurfaceControl mRootSurface;
  
  public WindowlessWindowManager(Configuration paramConfiguration, SurfaceControl paramSurfaceControl, IBinder paramIBinder) {
    this.mRootSurface = paramSurfaceControl;
    this.mConfiguration = new Configuration(paramConfiguration);
    this.mRealWm = WindowManagerGlobal.getWindowSession();
    this.mHostInputToken = paramIBinder;
  }
  
  protected void setConfiguration(Configuration paramConfiguration) {
    this.mConfiguration.setTo(paramConfiguration);
  }
  
  void setCompletionCallback(IBinder paramIBinder, ResizeCompleteCallback paramResizeCompleteCallback) {
    if (this.mResizeCompletionForWindow.get(paramIBinder) != null)
      Log.w("WindowlessWindowManager", "Unsupported overlapping resizes"); 
    this.mResizeCompletionForWindow.put(paramIBinder, paramResizeCompleteCallback);
  }
  
  protected void setTouchRegion(IBinder paramIBinder, Region paramRegion) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mStateForWindow : Ljava/util/HashMap;
    //   6: aload_1
    //   7: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   10: checkcast android/view/WindowlessWindowManager$State
    //   13: astore_3
    //   14: aload_3
    //   15: ifnonnull -> 21
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: aload_2
    //   22: aload_3
    //   23: getfield mInputRegion : Landroid/graphics/Region;
    //   26: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   29: ifeq -> 35
    //   32: aload_0
    //   33: monitorexit
    //   34: return
    //   35: aload_2
    //   36: ifnull -> 51
    //   39: new android/graphics/Region
    //   42: astore_1
    //   43: aload_1
    //   44: aload_2
    //   45: invokespecial <init> : (Landroid/graphics/Region;)V
    //   48: goto -> 53
    //   51: aconst_null
    //   52: astore_1
    //   53: aload_3
    //   54: aload_1
    //   55: putfield mInputRegion : Landroid/graphics/Region;
    //   58: aload_3
    //   59: getfield mInputChannelToken : Landroid/os/IBinder;
    //   62: astore_1
    //   63: aload_1
    //   64: ifnull -> 113
    //   67: aload_0
    //   68: getfield mRealWm : Landroid/view/IWindowSession;
    //   71: aload_3
    //   72: getfield mInputChannelToken : Landroid/os/IBinder;
    //   75: aload_3
    //   76: getfield mDisplayId : I
    //   79: aload_3
    //   80: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   83: aload_3
    //   84: getfield mParams : Landroid/view/WindowManager$LayoutParams;
    //   87: getfield flags : I
    //   90: aload_3
    //   91: getfield mInputRegion : Landroid/graphics/Region;
    //   94: invokeinterface updateInputChannel : (Landroid/os/IBinder;ILandroid/view/SurfaceControl;ILandroid/graphics/Region;)V
    //   99: goto -> 113
    //   102: astore_1
    //   103: ldc 'WindowlessWindowManager'
    //   105: ldc_w 'Failed to update surface input channel: '
    //   108: aload_1
    //   109: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   112: pop
    //   113: aload_0
    //   114: monitorexit
    //   115: return
    //   116: astore_1
    //   117: aload_0
    //   118: monitorexit
    //   119: aload_1
    //   120: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #104	-> 0
    //   #107	-> 2
    //   #108	-> 14
    //   #109	-> 18
    //   #111	-> 21
    //   #112	-> 32
    //   #114	-> 35
    //   #115	-> 58
    //   #117	-> 67
    //   #121	-> 99
    //   #119	-> 102
    //   #120	-> 103
    //   #123	-> 113
    //   #124	-> 115
    //   #123	-> 116
    // Exception table:
    //   from	to	target	type
    //   2	14	116	finally
    //   18	20	116	finally
    //   21	32	116	finally
    //   32	34	116	finally
    //   39	48	116	finally
    //   53	58	116	finally
    //   58	63	116	finally
    //   67	99	102	android/os/RemoteException
    //   67	99	116	finally
    //   103	113	116	finally
    //   113	115	116	finally
    //   117	119	116	finally
  }
  
  public int addToDisplay(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, int paramInt3, Rect paramRect1, Rect paramRect2, Rect paramRect3, DisplayCutout.ParcelableWrapper paramParcelableWrapper, InputChannel paramInputChannel, InsetsState paramInsetsState, InsetsSourceControl[] paramArrayOfInsetsSourceControl) {
    // Byte code:
    //   0: new android/view/SurfaceControl$Builder
    //   3: dup
    //   4: aload_0
    //   5: getfield mSurfaceSession : Landroid/view/SurfaceSession;
    //   8: invokespecial <init> : (Landroid/view/SurfaceSession;)V
    //   11: astore #6
    //   13: aload_0
    //   14: getfield mRootSurface : Landroid/view/SurfaceControl;
    //   17: astore #7
    //   19: aload #6
    //   21: aload #7
    //   23: invokevirtual setParent : (Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Builder;
    //   26: astore #6
    //   28: aload_3
    //   29: getfield format : I
    //   32: istore_2
    //   33: aload #6
    //   35: iload_2
    //   36: invokevirtual setFormat : (I)Landroid/view/SurfaceControl$Builder;
    //   39: astore #6
    //   41: aload #6
    //   43: aload_0
    //   44: aload_3
    //   45: invokespecial getSurfaceWidth : (Landroid/view/WindowManager$LayoutParams;)I
    //   48: aload_0
    //   49: aload_3
    //   50: invokespecial getSurfaceHeight : (Landroid/view/WindowManager$LayoutParams;)I
    //   53: invokevirtual setBufferSize : (II)Landroid/view/SurfaceControl$Builder;
    //   56: astore #6
    //   58: aload #6
    //   60: aload_3
    //   61: invokevirtual getTitle : ()Ljava/lang/CharSequence;
    //   64: invokeinterface toString : ()Ljava/lang/String;
    //   69: invokevirtual setName : (Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
    //   72: astore #6
    //   74: aload #6
    //   76: ldc 'WindowlessWindowManager.addToDisplay'
    //   78: invokevirtual setCallsite : (Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
    //   81: astore #6
    //   83: aload #6
    //   85: invokevirtual build : ()Landroid/view/SurfaceControl;
    //   88: astore #7
    //   90: aload_3
    //   91: getfield inputFeatures : I
    //   94: iconst_2
    //   95: iand
    //   96: ifne -> 142
    //   99: aload_0
    //   100: getfield mRealWm : Landroid/view/IWindowSession;
    //   103: iload #5
    //   105: aload #7
    //   107: aload_1
    //   108: aload_0
    //   109: getfield mHostInputToken : Landroid/os/IBinder;
    //   112: aload_3
    //   113: getfield flags : I
    //   116: aload_3
    //   117: getfield type : I
    //   120: aload #10
    //   122: invokeinterface grantInputChannel : (ILandroid/view/SurfaceControl;Landroid/view/IWindow;Landroid/os/IBinder;IILandroid/view/InputChannel;)V
    //   127: goto -> 142
    //   130: astore #6
    //   132: ldc 'WindowlessWindowManager'
    //   134: ldc 'Failed to grant input to surface: '
    //   136: aload #6
    //   138: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   141: pop
    //   142: aload #10
    //   144: ifnull -> 157
    //   147: aload #10
    //   149: invokevirtual getToken : ()Landroid/os/IBinder;
    //   152: astore #6
    //   154: goto -> 160
    //   157: aconst_null
    //   158: astore #6
    //   160: new android/view/WindowlessWindowManager$State
    //   163: dup
    //   164: aload_0
    //   165: aload #7
    //   167: aload_3
    //   168: iload #5
    //   170: aload #6
    //   172: invokespecial <init> : (Landroid/view/WindowlessWindowManager;Landroid/view/SurfaceControl;Landroid/view/WindowManager$LayoutParams;ILandroid/os/IBinder;)V
    //   175: astore_3
    //   176: aload_0
    //   177: monitorenter
    //   178: aload_0
    //   179: getfield mStateForWindow : Ljava/util/HashMap;
    //   182: aload_1
    //   183: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   188: aload_3
    //   189: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   192: pop
    //   193: aload_0
    //   194: monitorexit
    //   195: iconst_2
    //   196: ireturn
    //   197: astore_1
    //   198: aload_0
    //   199: monitorexit
    //   200: aload_1
    //   201: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #135	-> 0
    //   #136	-> 19
    //   #137	-> 33
    //   #138	-> 41
    //   #139	-> 58
    //   #140	-> 74
    //   #141	-> 83
    //   #143	-> 90
    //   #146	-> 99
    //   #150	-> 127
    //   #148	-> 130
    //   #149	-> 132
    //   #153	-> 142
    //   #154	-> 142
    //   #155	-> 176
    //   #156	-> 178
    //   #157	-> 193
    //   #159	-> 195
    //   #157	-> 197
    // Exception table:
    //   from	to	target	type
    //   99	127	130	android/os/RemoteException
    //   178	193	197	finally
    //   193	195	197	finally
    //   198	200	197	finally
  }
  
  public int addToDisplayAsUser(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, int paramInt3, int paramInt4, Rect paramRect1, Rect paramRect2, Rect paramRect3, DisplayCutout.ParcelableWrapper paramParcelableWrapper, InputChannel paramInputChannel, InsetsState paramInsetsState, InsetsSourceControl[] paramArrayOfInsetsSourceControl) {
    return addToDisplay(paramIWindow, paramInt1, paramLayoutParams, paramInt2, paramInt3, paramRect1, paramRect2, paramRect3, paramParcelableWrapper, paramInputChannel, paramInsetsState, paramArrayOfInsetsSourceControl);
  }
  
  public int addToDisplayWithoutInputChannel(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, int paramInt3, Rect paramRect1, Rect paramRect2, InsetsState paramInsetsState) {
    return 0;
  }
  
  public void remove(IWindow paramIWindow) throws RemoteException {
    // Byte code:
    //   0: aload_0
    //   1: getfield mRealWm : Landroid/view/IWindowSession;
    //   4: aload_1
    //   5: invokeinterface remove : (Landroid/view/IWindow;)V
    //   10: aload_0
    //   11: monitorenter
    //   12: aload_0
    //   13: getfield mStateForWindow : Ljava/util/HashMap;
    //   16: aload_1
    //   17: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   22: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   25: checkcast android/view/WindowlessWindowManager$State
    //   28: astore_1
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_1
    //   32: ifnull -> 75
    //   35: new android/view/SurfaceControl$Transaction
    //   38: dup
    //   39: invokespecial <init> : ()V
    //   42: astore_2
    //   43: aload_2
    //   44: aload_1
    //   45: getfield mSurfaceControl : Landroid/view/SurfaceControl;
    //   48: invokevirtual remove : (Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
    //   51: invokevirtual apply : ()V
    //   54: aload_2
    //   55: invokevirtual close : ()V
    //   58: return
    //   59: astore_1
    //   60: aload_2
    //   61: invokevirtual close : ()V
    //   64: goto -> 73
    //   67: astore_2
    //   68: aload_1
    //   69: aload_2
    //   70: invokevirtual addSuppressed : (Ljava/lang/Throwable;)V
    //   73: aload_1
    //   74: athrow
    //   75: new java/lang/IllegalArgumentException
    //   78: dup
    //   79: ldc_w 'Invalid window token (never added or removed already)'
    //   82: invokespecial <init> : (Ljava/lang/String;)V
    //   85: athrow
    //   86: astore_1
    //   87: aload_0
    //   88: monitorexit
    //   89: aload_1
    //   90: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #186	-> 0
    //   #188	-> 10
    //   #189	-> 12
    //   #190	-> 29
    //   #191	-> 31
    //   #196	-> 35
    //   #197	-> 43
    //   #198	-> 54
    //   #199	-> 58
    //   #196	-> 59
    //   #192	-> 75
    //   #190	-> 86
    // Exception table:
    //   from	to	target	type
    //   12	29	86	finally
    //   29	31	86	finally
    //   43	54	59	finally
    //   60	64	67	finally
    //   87	89	86	finally
  }
  
  private boolean isOpaque(WindowManager.LayoutParams paramLayoutParams) {
    if ((paramLayoutParams.surfaceInsets != null && paramLayoutParams.surfaceInsets.left != 0) || paramLayoutParams.surfaceInsets.top != 0 || paramLayoutParams.surfaceInsets.right != 0 || paramLayoutParams.surfaceInsets.bottom != 0)
      return false; 
    return PixelFormat.formatHasAlpha(paramLayoutParams.format) ^ true;
  }
  
  protected SurfaceControl getSurfaceControl(View paramView) {
    ViewRootImpl viewRootImpl = paramView.getViewRootImpl();
    if (viewRootImpl == null)
      return null; 
    State state = this.mStateForWindow.get(viewRootImpl.mWindow.asBinder());
    if (state == null)
      return null; 
    return state.mSurfaceControl;
  }
  
  public int relayout(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, int paramInt3, int paramInt4, int paramInt5, long paramLong, Rect paramRect1, Rect paramRect2, Rect paramRect3, Rect paramRect4, Rect paramRect5, DisplayCutout.ParcelableWrapper paramParcelableWrapper, MergedConfiguration paramMergedConfiguration, SurfaceControl paramSurfaceControl1, InsetsState paramInsetsState, InsetsSourceControl[] paramArrayOfInsetsSourceControl, Point paramPoint, SurfaceControl paramSurfaceControl2) {
    /* monitor enter ThisExpression{ObjectType{android/view/WindowlessWindowManager}} */
    try {
      State state = this.mStateForWindow.get(paramIWindow.asBinder());
      /* monitor exit ThisExpression{ObjectType{android/view/WindowlessWindowManager}} */
      if (state != null) {
        SurfaceControl surfaceControl = state.mSurfaceControl;
        SurfaceControl.Transaction transaction = new SurfaceControl.Transaction();
        if (paramLayoutParams != null) {
          paramInt1 = state.mParams.copyFrom(paramLayoutParams);
        } else {
          paramInt1 = 0;
        } 
        paramLayoutParams = state.mParams;
        if (paramInt4 == 0) {
          transaction = transaction.setBufferSize(surfaceControl, getSurfaceWidth(paramLayoutParams), getSurfaceHeight(paramLayoutParams));
          transaction.setOpaque(surfaceControl, isOpaque(paramLayoutParams)).show(surfaceControl).apply();
          paramSurfaceControl1.copyFrom(surfaceControl, "WindowlessWindowManager.relayout");
        } else {
          transaction.hide(surfaceControl).apply();
          paramSurfaceControl1.release();
        } 
        paramRect1.set(0, 0, paramLayoutParams.width, paramLayoutParams.height);
        Configuration configuration = this.mConfiguration;
        paramMergedConfiguration.setConfiguration(configuration, configuration);
        return 0;
      } 
      throw new IllegalArgumentException("Invalid window token (never added or removed already)");
    } finally {
      paramIWindow = null;
    } 
  }
  
  public void prepareToReplaceWindows(IBinder paramIBinder, boolean paramBoolean) {}
  
  public boolean outOfMemory(IWindow paramIWindow) {
    return false;
  }
  
  public void setTransparentRegion(IWindow paramIWindow, Region paramRegion) {}
  
  public void setInsets(IWindow paramIWindow, int paramInt, Rect paramRect1, Rect paramRect2, Region paramRegion) {}
  
  public void getDisplayFrame(IWindow paramIWindow, Rect paramRect) {}
  
  public void getVisibleDisplayFrame(IWindow paramIWindow, Rect paramRect) {}
  
  public void finishDrawing(IWindow paramIWindow, SurfaceControl.Transaction paramTransaction) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mResizeCompletionForWindow : Ljava/util/HashMap;
    //   6: astore_3
    //   7: aload_3
    //   8: aload_1
    //   9: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   14: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   17: checkcast android/view/WindowlessWindowManager$ResizeCompleteCallback
    //   20: astore_3
    //   21: aload_3
    //   22: ifnonnull -> 32
    //   25: aload_2
    //   26: invokevirtual apply : ()V
    //   29: aload_0
    //   30: monitorexit
    //   31: return
    //   32: aload_3
    //   33: aload_2
    //   34: invokeinterface finished : (Landroid/view/SurfaceControl$Transaction;)V
    //   39: aload_0
    //   40: getfield mResizeCompletionForWindow : Ljava/util/HashMap;
    //   43: aload_1
    //   44: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   49: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   52: pop
    //   53: aload_0
    //   54: monitorexit
    //   55: return
    //   56: astore_1
    //   57: aload_0
    //   58: monitorexit
    //   59: aload_1
    //   60: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #309	-> 0
    //   #310	-> 2
    //   #311	-> 7
    //   #312	-> 21
    //   #314	-> 25
    //   #315	-> 29
    //   #317	-> 32
    //   #318	-> 39
    //   #319	-> 53
    //   #320	-> 55
    //   #319	-> 56
    // Exception table:
    //   from	to	target	type
    //   2	7	56	finally
    //   7	21	56	finally
    //   25	29	56	finally
    //   29	31	56	finally
    //   32	39	56	finally
    //   39	53	56	finally
    //   53	55	56	finally
    //   57	59	56	finally
  }
  
  public void setInTouchMode(boolean paramBoolean) {}
  
  public boolean getInTouchMode() {
    return false;
  }
  
  public boolean performHapticFeedback(int paramInt, boolean paramBoolean) {
    return false;
  }
  
  public IBinder performDrag(IWindow paramIWindow, int paramInt1, SurfaceControl paramSurfaceControl, int paramInt2, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, ClipData paramClipData) {
    return null;
  }
  
  public void reportDropResult(IWindow paramIWindow, boolean paramBoolean) {}
  
  public void cancelDragAndDrop(IBinder paramIBinder, boolean paramBoolean) {}
  
  public void dragRecipientEntered(IWindow paramIWindow) {}
  
  public void dragRecipientExited(IWindow paramIWindow) {}
  
  public void setWallpaperPosition(IBinder paramIBinder, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {}
  
  public void setWallpaperZoomOut(IBinder paramIBinder, float paramFloat) {}
  
  public void setShouldZoomOutWallpaper(IBinder paramIBinder, boolean paramBoolean) {}
  
  public void wallpaperOffsetsComplete(IBinder paramIBinder) {}
  
  public void setWallpaperDisplayOffset(IBinder paramIBinder, int paramInt1, int paramInt2) {}
  
  public Bundle sendWallpaperCommand(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle, boolean paramBoolean) {
    return null;
  }
  
  public void wallpaperCommandComplete(IBinder paramIBinder, Bundle paramBundle) {}
  
  public void onRectangleOnScreenRequested(IBinder paramIBinder, Rect paramRect) {}
  
  public IWindowId getWindowId(IBinder paramIBinder) {
    return null;
  }
  
  public void pokeDrawLock(IBinder paramIBinder) {}
  
  public boolean startMovingTask(IWindow paramIWindow, float paramFloat1, float paramFloat2) {
    return false;
  }
  
  public void finishMovingTask(IWindow paramIWindow) {}
  
  public void updatePointerIcon(IWindow paramIWindow) {}
  
  public void reparentDisplayContent(IWindow paramIWindow, SurfaceControl paramSurfaceControl, int paramInt) {}
  
  public void updateDisplayContentLocation(IWindow paramIWindow, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void updateTapExcludeRegion(IWindow paramIWindow, Region paramRegion) {}
  
  public void insetsModified(IWindow paramIWindow, InsetsState paramInsetsState) {}
  
  public void reportSystemGestureExclusionChanged(IWindow paramIWindow, List<Rect> paramList) {}
  
  public void grantInputChannel(int paramInt1, SurfaceControl paramSurfaceControl, IWindow paramIWindow, IBinder paramIBinder, int paramInt2, int paramInt3, InputChannel paramInputChannel) {}
  
  public void updateInputChannel(IBinder paramIBinder, int paramInt1, SurfaceControl paramSurfaceControl, int paramInt2, Region paramRegion) {}
  
  public IBinder asBinder() {
    return null;
  }
  
  private int getSurfaceWidth(WindowManager.LayoutParams paramLayoutParams) {
    int i;
    Rect rect = paramLayoutParams.surfaceInsets;
    if (rect != null) {
      i = paramLayoutParams.width + rect.left + rect.right;
    } else {
      i = paramLayoutParams.width;
    } 
    return i;
  }
  
  private int getSurfaceHeight(WindowManager.LayoutParams paramLayoutParams) {
    int i;
    Rect rect = paramLayoutParams.surfaceInsets;
    if (rect != null) {
      i = paramLayoutParams.height + rect.top + rect.bottom;
    } else {
      i = paramLayoutParams.height;
    } 
    return i;
  }
}
