package android.view;

public class InflateException extends RuntimeException {
  public InflateException() {}
  
  public InflateException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
  
  public InflateException(String paramString) {
    super(paramString);
  }
  
  public InflateException(Throwable paramThrowable) {
    super(paramThrowable);
  }
}
