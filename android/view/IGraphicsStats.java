package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public interface IGraphicsStats extends IInterface {
  ParcelFileDescriptor requestBufferForProcess(String paramString, IGraphicsStatsCallback paramIGraphicsStatsCallback) throws RemoteException;
  
  class Default implements IGraphicsStats {
    public ParcelFileDescriptor requestBufferForProcess(String param1String, IGraphicsStatsCallback param1IGraphicsStatsCallback) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IGraphicsStats {
    private static final String DESCRIPTOR = "android.view.IGraphicsStats";
    
    static final int TRANSACTION_requestBufferForProcess = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IGraphicsStats");
    }
    
    public static IGraphicsStats asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IGraphicsStats");
      if (iInterface != null && iInterface instanceof IGraphicsStats)
        return (IGraphicsStats)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "requestBufferForProcess";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.IGraphicsStats");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IGraphicsStats");
      String str = param1Parcel1.readString();
      IGraphicsStatsCallback iGraphicsStatsCallback = IGraphicsStatsCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      ParcelFileDescriptor parcelFileDescriptor = requestBufferForProcess(str, iGraphicsStatsCallback);
      param1Parcel2.writeNoException();
      if (parcelFileDescriptor != null) {
        param1Parcel2.writeInt(1);
        parcelFileDescriptor.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements IGraphicsStats {
      public static IGraphicsStats sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IGraphicsStats";
      }
      
      public ParcelFileDescriptor requestBufferForProcess(String param2String, IGraphicsStatsCallback param2IGraphicsStatsCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.view.IGraphicsStats");
          parcel1.writeString(param2String);
          if (param2IGraphicsStatsCallback != null) {
            iBinder = param2IGraphicsStatsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IGraphicsStats.Stub.getDefaultImpl() != null)
            return IGraphicsStats.Stub.getDefaultImpl().requestBufferForProcess(param2String, param2IGraphicsStatsCallback); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParcelFileDescriptor)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IGraphicsStats param1IGraphicsStats) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IGraphicsStats != null) {
          Proxy.sDefaultImpl = param1IGraphicsStats;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IGraphicsStats getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
