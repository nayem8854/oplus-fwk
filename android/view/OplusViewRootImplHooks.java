package android.view;

import android.common.ColorFrameworkFactory;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;
import com.oplus.favorite.IOplusFavoriteManager;
import com.oplus.screenshot.OplusLongshotViewRoot;
import com.oplus.util.OplusTypeCastingHelper;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;

public class OplusViewRootImplHooks {
  private final String TAG = "ColorViewRootImplHooks";
  
  private final OplusLongshotViewRoot mLongshotViewRoot = new OplusLongshotViewRoot();
  
  private final ViewRootImpl mViewRootImpl;
  
  public OplusViewRootImplHooks(ViewRootImpl paramViewRootImpl, Context paramContext) {
    this.mViewRootImpl = paramViewRootImpl;
  }
  
  public OplusLongshotViewRoot getLongshotViewRoot() {
    return this.mLongshotViewRoot;
  }
  
  public void setView(View paramView) {
    if (paramView == null)
      return; 
    IOplusFavoriteManager iOplusFavoriteManager = (IOplusFavoriteManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFavoriteManager.DEFAULT, new Object[0]);
    iOplusFavoriteManager.init(paramView.getContext());
  }
  
  public void markUserDefinedToast(View paramView, WindowManager.LayoutParams paramLayoutParams) {
    if (paramView == null || paramLayoutParams == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("markUserDefinedToast invalid args, view=");
      stringBuilder.append(paramView);
      stringBuilder.append(", attrs=");
      stringBuilder.append(paramLayoutParams);
      Log.w("ColorViewRootImplHooks", stringBuilder.toString());
      return;
    } 
    if (paramLayoutParams.type == 2005 && paramView.mID != 201457771) {
      OplusBaseLayoutParams oplusBaseLayoutParams = (OplusBaseLayoutParams)OplusTypeCastingHelper.typeCasting(OplusBaseLayoutParams.class, paramLayoutParams);
      if (oplusBaseLayoutParams != null)
        oplusBaseLayoutParams.oplusFlags |= 0x1; 
    } 
  }
  
  public void dispatchDetachedFromWindow(View paramView) {
    if (paramView == null)
      return; 
    IOplusFavoriteManager iOplusFavoriteManager = (IOplusFavoriteManager)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusFavoriteManager.DEFAULT, new Object[0]);
    iOplusFavoriteManager.release();
  }
  
  ViewRootImpl.W createWindowClient(ViewRootImpl paramViewRootImpl) {
    return new ColorW(paramViewRootImpl);
  }
  
  public MotionEvent updatePointerEvent(MotionEvent paramMotionEvent, View paramView, Configuration paramConfiguration) {
    return ((IOplusAccidentallyTouchHelper)OplusFeatureCache.getOrCreate(IOplusAccidentallyTouchHelper.DEFAULT, new Object[0])).updatePointerEvent(paramMotionEvent, paramView, paramConfiguration);
  }
  
  class ColorW extends ViewRootImpl.W {
    private IOplusDirectViewHelper mDirectHelper;
    
    private IOplusLongshotViewHelper mLongshotHelper;
    
    ColorW(OplusViewRootImplHooks this$0) {
      super((ViewRootImpl)this$0);
      this$0 = null;
      NoSuchFieldException noSuchFieldException2 = null;
      try {
        Field field = ColorW.class.getSuperclass().getDeclaredField("mViewAncestor");
        field.setAccessible(true);
        WeakReference weakReference2 = (WeakReference)field.get(this), weakReference1 = weakReference2;
      } catch (NoSuchFieldException noSuchFieldException1) {
        Log.w("ColorViewRootImplHooks", "NoSuchFieldException reflect to get mViewAncestor from ViewRootImpl");
        noSuchFieldException1 = noSuchFieldException2;
      } catch (IllegalAccessException illegalAccessException) {
        Log.w("ColorViewRootImplHooks", "IllegalAccessException reflect to get mViewAncestor from ViewRootImpl");
      } 
      if (noSuchFieldException1 != null) {
        this.mLongshotHelper = ((IOplusViewRootUtil)OplusFeatureCache.getOrCreate(IOplusViewRootUtil.DEFAULT, new Object[0])).getOplusLongshotViewHelper((WeakReference<ViewRootImpl>)noSuchFieldException1);
        this.mDirectHelper = (IOplusDirectViewHelper)ColorFrameworkFactory.getInstance().getFeature(IOplusDirectViewHelper.DEFAULT, new Object[] { noSuchFieldException1 });
      } 
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IOplusLongshotViewHelper iOplusLongshotViewHelper = this.mLongshotHelper;
      if (iOplusLongshotViewHelper != null && iOplusLongshotViewHelper.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2))
        return true; 
      IOplusDirectViewHelper iOplusDirectViewHelper = this.mDirectHelper;
      if (iOplusDirectViewHelper != null && iOplusDirectViewHelper.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2))
        return true; 
      return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
    }
  }
}
