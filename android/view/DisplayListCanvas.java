package android.view;

import android.graphics.BaseRecordingCanvas;
import android.graphics.CanvasProperty;
import android.graphics.Paint;

public abstract class DisplayListCanvas extends BaseRecordingCanvas {
  protected DisplayListCanvas(long paramLong) {
    super(paramLong);
  }
  
  public abstract void drawCircle(CanvasProperty<Float> paramCanvasProperty1, CanvasProperty<Float> paramCanvasProperty2, CanvasProperty<Float> paramCanvasProperty3, CanvasProperty<Paint> paramCanvasProperty);
  
  public abstract void drawRoundRect(CanvasProperty<Float> paramCanvasProperty1, CanvasProperty<Float> paramCanvasProperty2, CanvasProperty<Float> paramCanvasProperty3, CanvasProperty<Float> paramCanvasProperty4, CanvasProperty<Float> paramCanvasProperty5, CanvasProperty<Float> paramCanvasProperty6, CanvasProperty<Paint> paramCanvasProperty);
}
