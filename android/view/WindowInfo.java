package android.view;

import android.graphics.Region;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pools;
import android.view.accessibility.AccessibilityNodeInfo;
import java.util.ArrayList;
import java.util.List;

public class WindowInfo implements Parcelable {
  private static final Pools.SynchronizedPool<WindowInfo> sPool = new Pools.SynchronizedPool<>(10);
  
  public Region regionInScreen = new Region();
  
  public long accessibilityIdOfAnchor = AccessibilityNodeInfo.UNDEFINED_NODE_ID;
  
  public int displayId = -1;
  
  public static final Parcelable.Creator<WindowInfo> CREATOR;
  
  private static final int MAX_POOL_SIZE = 10;
  
  public IBinder activityToken;
  
  public List<IBinder> childTokens;
  
  public boolean focused;
  
  public boolean hasFlagWatchOutsideTouch;
  
  public boolean inPictureInPicture;
  
  public int layer;
  
  public IBinder parentToken;
  
  public CharSequence title;
  
  public IBinder token;
  
  public int type;
  
  public static WindowInfo obtain() {
    WindowInfo windowInfo1 = sPool.acquire();
    WindowInfo windowInfo2 = windowInfo1;
    if (windowInfo1 == null)
      windowInfo2 = new WindowInfo(); 
    return windowInfo2;
  }
  
  public static WindowInfo obtain(WindowInfo paramWindowInfo) {
    WindowInfo windowInfo = obtain();
    windowInfo.displayId = paramWindowInfo.displayId;
    windowInfo.type = paramWindowInfo.type;
    windowInfo.layer = paramWindowInfo.layer;
    windowInfo.token = paramWindowInfo.token;
    windowInfo.parentToken = paramWindowInfo.parentToken;
    windowInfo.activityToken = paramWindowInfo.activityToken;
    windowInfo.focused = paramWindowInfo.focused;
    windowInfo.regionInScreen.set(paramWindowInfo.regionInScreen);
    windowInfo.title = paramWindowInfo.title;
    windowInfo.accessibilityIdOfAnchor = paramWindowInfo.accessibilityIdOfAnchor;
    windowInfo.inPictureInPicture = paramWindowInfo.inPictureInPicture;
    windowInfo.hasFlagWatchOutsideTouch = paramWindowInfo.hasFlagWatchOutsideTouch;
    List<IBinder> list = paramWindowInfo.childTokens;
    if (list != null && !list.isEmpty()) {
      list = windowInfo.childTokens;
      if (list == null) {
        windowInfo.childTokens = new ArrayList<>(paramWindowInfo.childTokens);
      } else {
        list.addAll(paramWindowInfo.childTokens);
      } 
    } 
    return windowInfo;
  }
  
  public void recycle() {
    clear();
    sPool.release(this);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.displayId);
    paramParcel.writeInt(this.type);
    paramParcel.writeInt(this.layer);
    paramParcel.writeStrongBinder(this.token);
    paramParcel.writeStrongBinder(this.parentToken);
    paramParcel.writeStrongBinder(this.activityToken);
    paramParcel.writeInt(this.focused);
    this.regionInScreen.writeToParcel(paramParcel, paramInt);
    paramParcel.writeCharSequence(this.title);
    paramParcel.writeLong(this.accessibilityIdOfAnchor);
    paramParcel.writeInt(this.inPictureInPicture);
    paramParcel.writeInt(this.hasFlagWatchOutsideTouch);
    List<IBinder> list = this.childTokens;
    if (list != null && !list.isEmpty()) {
      paramParcel.writeInt(1);
      paramParcel.writeBinderList(this.childTokens);
    } else {
      paramParcel.writeInt(0);
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("WindowInfo[");
    stringBuilder.append("title=");
    stringBuilder.append(this.title);
    stringBuilder.append(", displayId=");
    stringBuilder.append(this.displayId);
    stringBuilder.append(", type=");
    stringBuilder.append(this.type);
    stringBuilder.append(", layer=");
    stringBuilder.append(this.layer);
    stringBuilder.append(", token=");
    stringBuilder.append(this.token);
    stringBuilder.append(", region=");
    stringBuilder.append(this.regionInScreen);
    stringBuilder.append(", bounds=");
    stringBuilder.append(this.regionInScreen.getBounds());
    stringBuilder.append(", parent=");
    stringBuilder.append(this.parentToken);
    stringBuilder.append(", focused=");
    stringBuilder.append(this.focused);
    stringBuilder.append(", children=");
    stringBuilder.append(this.childTokens);
    stringBuilder.append(", accessibility anchor=");
    stringBuilder.append(this.accessibilityIdOfAnchor);
    stringBuilder.append(", pictureInPicture=");
    stringBuilder.append(this.inPictureInPicture);
    stringBuilder.append(", watchOutsideTouch=");
    stringBuilder.append(this.hasFlagWatchOutsideTouch);
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  private void initFromParcel(Parcel paramParcel) {
    boolean bool2;
    this.displayId = paramParcel.readInt();
    this.type = paramParcel.readInt();
    this.layer = paramParcel.readInt();
    this.token = paramParcel.readStrongBinder();
    this.parentToken = paramParcel.readStrongBinder();
    this.activityToken = paramParcel.readStrongBinder();
    int i = paramParcel.readInt();
    boolean bool1 = false;
    if (i == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.focused = bool2;
    this.regionInScreen = (Region)Region.CREATOR.createFromParcel(paramParcel);
    this.title = paramParcel.readCharSequence();
    this.accessibilityIdOfAnchor = paramParcel.readLong();
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.inPictureInPicture = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.hasFlagWatchOutsideTouch = bool2;
    if (paramParcel.readInt() == 1)
      bool1 = true; 
    if (bool1) {
      if (this.childTokens == null)
        this.childTokens = new ArrayList<>(); 
      paramParcel.readBinderList(this.childTokens);
    } 
  }
  
  private void clear() {
    this.displayId = -1;
    this.type = 0;
    this.layer = 0;
    this.token = null;
    this.parentToken = null;
    this.activityToken = null;
    this.focused = false;
    this.regionInScreen.setEmpty();
    List<IBinder> list = this.childTokens;
    if (list != null)
      list.clear(); 
    this.inPictureInPicture = false;
    this.hasFlagWatchOutsideTouch = false;
  }
  
  static {
    CREATOR = new Parcelable.Creator<WindowInfo>() {
        public WindowInfo createFromParcel(Parcel param1Parcel) {
          WindowInfo windowInfo = WindowInfo.obtain();
          windowInfo.initFromParcel(param1Parcel);
          return windowInfo;
        }
        
        public WindowInfo[] newArray(int param1Int) {
          return new WindowInfo[param1Int];
        }
      };
  }
}
