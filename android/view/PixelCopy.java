package android.view;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Handler;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class PixelCopy {
  public static final int ERROR_DESTINATION_INVALID = 5;
  
  public static final int ERROR_SOURCE_INVALID = 4;
  
  public static final int ERROR_SOURCE_NO_DATA = 3;
  
  public static final int ERROR_TIMEOUT = 2;
  
  public static final int ERROR_UNKNOWN = 1;
  
  public static final int SUCCESS = 0;
  
  public static void request(SurfaceView paramSurfaceView, Bitmap paramBitmap, OnPixelCopyFinishedListener paramOnPixelCopyFinishedListener, Handler paramHandler) {
    request(paramSurfaceView.getHolder().getSurface(), paramBitmap, paramOnPixelCopyFinishedListener, paramHandler);
  }
  
  public static void request(SurfaceView paramSurfaceView, Rect paramRect, Bitmap paramBitmap, OnPixelCopyFinishedListener paramOnPixelCopyFinishedListener, Handler paramHandler) {
    request(paramSurfaceView.getHolder().getSurface(), paramRect, paramBitmap, paramOnPixelCopyFinishedListener, paramHandler);
  }
  
  public static void request(Surface paramSurface, Bitmap paramBitmap, OnPixelCopyFinishedListener paramOnPixelCopyFinishedListener, Handler paramHandler) {
    request(paramSurface, (Rect)null, paramBitmap, paramOnPixelCopyFinishedListener, paramHandler);
  }
  
  public static void request(Surface paramSurface, Rect paramRect, Bitmap paramBitmap, final OnPixelCopyFinishedListener listener, Handler paramHandler) {
    validateBitmapDest(paramBitmap);
    if (paramSurface.isValid()) {
      if (paramRect == null || !paramRect.isEmpty()) {
        final int result = ThreadedRenderer.copySurfaceInto(paramSurface, paramRect, paramBitmap);
        paramHandler.post(new Runnable() {
              final PixelCopy.OnPixelCopyFinishedListener val$listener;
              
              final int val$result;
              
              public void run() {
                listener.onPixelCopyFinished(result);
              }
            });
        return;
      } 
      throw new IllegalArgumentException("sourceRect is empty");
    } 
    throw new IllegalArgumentException("Surface isn't valid, source.isValid() == false");
  }
  
  public static void request(Window paramWindow, Bitmap paramBitmap, OnPixelCopyFinishedListener paramOnPixelCopyFinishedListener, Handler paramHandler) {
    request(paramWindow, (Rect)null, paramBitmap, paramOnPixelCopyFinishedListener, paramHandler);
  }
  
  public static void request(Window paramWindow, Rect paramRect, Bitmap paramBitmap, OnPixelCopyFinishedListener paramOnPixelCopyFinishedListener, Handler paramHandler) {
    validateBitmapDest(paramBitmap);
    if (paramWindow != null) {
      if (paramWindow.peekDecorView() != null) {
        Surface surface;
        Window window = null;
        ViewRootImpl viewRootImpl = paramWindow.peekDecorView().getViewRootImpl();
        paramWindow = window;
        Rect rect = paramRect;
        if (viewRootImpl != null) {
          surface = viewRootImpl.mSurface;
          rect = viewRootImpl.mWindowAttributes.surfaceInsets;
          if (paramRect == null) {
            rect = new Rect(rect.left, rect.top, viewRootImpl.mWidth + rect.left, viewRootImpl.mHeight + rect.top);
          } else {
            paramRect.offset(rect.left, rect.top);
            rect = paramRect;
          } 
        } 
        if (surface != null && surface.isValid()) {
          request(surface, rect, paramBitmap, paramOnPixelCopyFinishedListener, paramHandler);
          return;
        } 
        throw new IllegalArgumentException("Window doesn't have a backing surface!");
      } 
      throw new IllegalArgumentException("Only able to copy windows with decor views");
    } 
    throw new IllegalArgumentException("source is null");
  }
  
  private static void validateBitmapDest(Bitmap paramBitmap) {
    if (paramBitmap != null) {
      if (!paramBitmap.isRecycled()) {
        if (paramBitmap.isMutable())
          return; 
        throw new IllegalArgumentException("Bitmap is immutable");
      } 
      throw new IllegalArgumentException("Bitmap is recycled");
    } 
    throw new IllegalArgumentException("Bitmap cannot be null");
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CopyResultStatus {}
  
  public static interface OnPixelCopyFinishedListener {
    void onPixelCopyFinished(int param1Int);
  }
}
