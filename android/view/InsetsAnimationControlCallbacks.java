package android.view;

public interface InsetsAnimationControlCallbacks {
  void applySurfaceParams(SyncRtSurfaceTransactionApplier.SurfaceParams... paramVarArgs);
  
  void notifyFinished(InsetsAnimationControlRunner paramInsetsAnimationControlRunner, boolean paramBoolean);
  
  void releaseSurfaceControlFromRt(SurfaceControl paramSurfaceControl);
  
  void reportPerceptible(int paramInt, boolean paramBoolean);
  
  void scheduleApplyChangeInsets(InsetsAnimationControlRunner paramInsetsAnimationControlRunner);
  
  void startAnimation(InsetsAnimationControlImpl paramInsetsAnimationControlImpl, WindowInsetsAnimationControlListener paramWindowInsetsAnimationControlListener, int paramInt, WindowInsetsAnimation paramWindowInsetsAnimation, WindowInsetsAnimation.Bounds paramBounds);
}
