package android.view;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class VerifiedInputEvent implements Parcelable {
  protected VerifiedInputEvent(int paramInt1, int paramInt2, long paramLong, int paramInt3, int paramInt4) {
    this.mType = paramInt1;
    this.mDeviceId = paramInt2;
    this.mEventTimeNanos = paramLong;
    this.mSource = paramInt3;
    this.mDisplayId = paramInt4;
  }
  
  protected VerifiedInputEvent(Parcel paramParcel, int paramInt) {
    int i = paramParcel.readInt();
    if (i == paramInt) {
      this.mDeviceId = paramParcel.readInt();
      this.mEventTimeNanos = paramParcel.readLong();
      this.mSource = paramParcel.readInt();
      this.mDisplayId = paramParcel.readInt();
      return;
    } 
    throw new IllegalArgumentException("Unexpected input event type token in parcel.");
  }
  
  public int getDeviceId() {
    return this.mDeviceId;
  }
  
  public long getEventTimeNanos() {
    return this.mEventTimeNanos;
  }
  
  public int getSource() {
    return this.mSource;
  }
  
  public int getDisplayId() {
    return this.mDisplayId;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    paramParcel.writeInt(this.mDeviceId);
    paramParcel.writeLong(this.mEventTimeNanos);
    paramParcel.writeInt(this.mSource);
    paramParcel.writeInt(this.mDisplayId);
  }
  
  public int describeContents() {
    return 0;
  }
  
  private static int peekInt(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    paramParcel.setDataPosition(i);
    return j;
  }
  
  public static final Parcelable.Creator<VerifiedInputEvent> CREATOR = new Parcelable.Creator<VerifiedInputEvent>() {
      public VerifiedInputEvent[] newArray(int param1Int) {
        return new VerifiedInputEvent[param1Int];
      }
      
      public VerifiedInputEvent createFromParcel(Parcel param1Parcel) {
        int i = VerifiedInputEvent.peekInt(param1Parcel);
        if (i == 1)
          return (VerifiedInputEvent)VerifiedKeyEvent.CREATOR.createFromParcel(param1Parcel); 
        if (i == 2)
          return (VerifiedInputEvent)VerifiedMotionEvent.CREATOR.createFromParcel(param1Parcel); 
        throw new IllegalArgumentException("Unexpected input event type in parcel.");
      }
    };
  
  private static final String TAG = "VerifiedInputEvent";
  
  protected static final int VERIFIED_KEY = 1;
  
  protected static final int VERIFIED_MOTION = 2;
  
  private int mDeviceId;
  
  private int mDisplayId;
  
  private long mEventTimeNanos;
  
  private int mSource;
  
  private int mType;
  
  @Retention(RetentionPolicy.SOURCE)
  class VerifiedInputEventType implements Annotation {}
}
