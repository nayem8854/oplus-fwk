package android.view;

import android.graphics.HardwareRendererObserver;
import android.os.Handler;
import java.lang.ref.WeakReference;

public class FrameMetricsObserver implements HardwareRendererObserver.OnFrameMetricsAvailableListener {
  private final FrameMetrics mFrameMetrics;
  
  final Window.OnFrameMetricsAvailableListener mListener;
  
  private final HardwareRendererObserver mObserver;
  
  private final WeakReference<Window> mWindow;
  
  FrameMetricsObserver(Window paramWindow, Handler paramHandler, Window.OnFrameMetricsAvailableListener paramOnFrameMetricsAvailableListener) {
    this.mWindow = new WeakReference<>(paramWindow);
    this.mListener = paramOnFrameMetricsAvailableListener;
    FrameMetrics frameMetrics = new FrameMetrics();
    this.mObserver = new HardwareRendererObserver(this, frameMetrics.mTimingData, paramHandler);
  }
  
  public void onFrameMetricsAvailable(int paramInt) {
    if (this.mWindow.get() != null)
      this.mListener.onFrameMetricsAvailable(this.mWindow.get(), this.mFrameMetrics, paramInt); 
  }
  
  HardwareRendererObserver getRendererObserver() {
    return this.mObserver;
  }
}
