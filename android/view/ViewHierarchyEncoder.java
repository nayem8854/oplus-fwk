package android.view;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class ViewHierarchyEncoder {
  private final Map<String, Short> mPropertyNames = new HashMap<>(200);
  
  private short mPropertyId = 1;
  
  private Charset mCharset = Charset.forName("utf-8");
  
  private boolean mUserPropertiesEnabled = true;
  
  private static final byte SIG_BOOLEAN = 90;
  
  private static final byte SIG_BYTE = 66;
  
  private static final byte SIG_DOUBLE = 68;
  
  private static final short SIG_END_MAP = 0;
  
  private static final byte SIG_FLOAT = 70;
  
  private static final byte SIG_INT = 73;
  
  private static final byte SIG_LONG = 74;
  
  private static final byte SIG_MAP = 77;
  
  private static final byte SIG_SHORT = 83;
  
  private static final byte SIG_STRING = 82;
  
  private final DataOutputStream mStream;
  
  public ViewHierarchyEncoder(ByteArrayOutputStream paramByteArrayOutputStream) {
    this.mStream = new DataOutputStream(paramByteArrayOutputStream);
  }
  
  public void setUserPropertiesEnabled(boolean paramBoolean) {
    this.mUserPropertiesEnabled = paramBoolean;
  }
  
  public void beginObject(Object paramObject) {
    startPropertyMap();
    addProperty("meta:__name__", paramObject.getClass().getName());
    addProperty("meta:__hash__", paramObject.hashCode());
  }
  
  public void endObject() {
    endPropertyMap();
  }
  
  public void endStream() {
    startPropertyMap();
    addProperty("__name__", "propertyIndex");
    for (Map.Entry<String, Short> entry : this.mPropertyNames.entrySet()) {
      writeShort(((Short)entry.getValue()).shortValue());
      writeString((String)entry.getKey());
    } 
    endPropertyMap();
  }
  
  public void addProperty(String paramString, boolean paramBoolean) {
    writeShort(createPropertyIndex(paramString));
    writeBoolean(paramBoolean);
  }
  
  public void addProperty(String paramString, short paramShort) {
    writeShort(createPropertyIndex(paramString));
    writeShort(paramShort);
  }
  
  public void addProperty(String paramString, int paramInt) {
    writeShort(createPropertyIndex(paramString));
    writeInt(paramInt);
  }
  
  public void addProperty(String paramString, float paramFloat) {
    writeShort(createPropertyIndex(paramString));
    writeFloat(paramFloat);
  }
  
  public void addProperty(String paramString1, String paramString2) {
    writeShort(createPropertyIndex(paramString1));
    writeString(paramString2);
  }
  
  public void addUserProperty(String paramString1, String paramString2) {
    if (this.mUserPropertiesEnabled)
      addProperty(paramString1, paramString2); 
  }
  
  public void addPropertyKey(String paramString) {
    writeShort(createPropertyIndex(paramString));
  }
  
  private short createPropertyIndex(String paramString) {
    Short short_1 = this.mPropertyNames.get(paramString);
    Short short_2 = short_1;
    if (short_1 == null) {
      short s = this.mPropertyId;
      this.mPropertyId = (short)(s + 1);
      short_2 = Short.valueOf(s);
      this.mPropertyNames.put(paramString, short_2);
    } 
    return short_2.shortValue();
  }
  
  private void startPropertyMap() {
    try {
      this.mStream.write(77);
    } catch (IOException iOException) {}
  }
  
  private void endPropertyMap() {
    writeShort((short)0);
  }
  
  private void writeBoolean(boolean paramBoolean) {
    try {
      boolean bool;
      this.mStream.write(90);
      DataOutputStream dataOutputStream = this.mStream;
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      dataOutputStream.write(bool);
    } catch (IOException iOException) {}
  }
  
  private void writeShort(short paramShort) {
    try {
      this.mStream.write(83);
      this.mStream.writeShort(paramShort);
    } catch (IOException iOException) {}
  }
  
  private void writeInt(int paramInt) {
    try {
      this.mStream.write(73);
      this.mStream.writeInt(paramInt);
    } catch (IOException iOException) {}
  }
  
  private void writeFloat(float paramFloat) {
    try {
      this.mStream.write(70);
      this.mStream.writeFloat(paramFloat);
    } catch (IOException iOException) {}
  }
  
  private void writeString(String paramString) {
    String str = paramString;
    if (paramString == null)
      str = ""; 
    try {
      this.mStream.write(82);
      byte[] arrayOfByte = str.getBytes(this.mCharset);
      short s = (short)Math.min(arrayOfByte.length, 32767);
      this.mStream.writeShort(s);
      this.mStream.write(arrayOfByte, 0, s);
    } catch (IOException iOException) {}
  }
}
