package android.view;

import android.content.Context;
import android.content.res.Configuration;
import android.provider.Settings;
import android.system.Os;
import android.util.Log;
import com.oplus.util.OplusFontUtils;
import java.io.File;
import java.util.Locale;

public class OplusBurmeseZgFlagHooksImpl implements IOplusBurmeseZgHooks {
  private static final String BURMESE_FONT_LINK_ON_DATA = "/data/format_unclear/font/OplusOSUI-Myanmar.ttf";
  
  private static final String CURRENT_FONT_BURMESE = "current_typeface_burmese";
  
  private static final String CURRENT_FONT_BURMESE_OLD = "current_typeface";
  
  private static final int FLIP_FONT_FLAG_FOR_BURMESE_UNICODE = 10003;
  
  private static final int FLIP_FONT_FLAG_FOR_BURMESE_ZG = 10002;
  
  private static final String SYSTEM_BURMESE_UNICODE_REAL_FONT_FILE = "/system/fonts/OplusOSUI-XThin.ttf";
  
  private static final String SYSTEM_BURMESE_ZG_REAL_FONT_FILE = "/system/fonts/MyanmarZg.ttf";
  
  private static final String SYSTEM_DEFAULT_FONT = "system.default.font";
  
  private static final String TAG = "OplusZGSupport";
  
  private boolean mIsZgFlagInited;
  
  private boolean mIsZgFlagOn;
  
  public void initBurmeseZgFlag(Context paramContext) {
    if (!this.mIsZgFlagInited) {
      this.mIsZgFlagOn = isCurrentUseZgEncoding(paramContext, null);
      this.mIsZgFlagInited = true;
    } 
    OplusFontUtils.setNeedReplaceAllTypefaceApp(this.mIsZgFlagOn);
  }
  
  public void updateBurmeseZgFlag(Context paramContext) {
    boolean bool = isCurrentUseZgEncoding(paramContext, null);
    OplusFontUtils.setNeedReplaceAllTypefaceApp(bool);
  }
  
  public boolean getZgFlag() {
    return OplusFontUtils.getNeedReplaceAllTypefaceApp();
  }
  
  public void updateBurmeseEncodingForUser(Context paramContext, Configuration paramConfiguration, int paramInt) {
    if (paramContext != null) {
      flipBurmeseEncoding(paramContext, paramConfiguration);
    } else {
      Log.d("OplusZGSupport", "updateBurmeseEncodingForUser : WARNING context == null");
    } 
  }
  
  private boolean flipBurmeseEncoding(Context paramContext, Configuration paramConfiguration) {
    String str;
    paramContext.getPackageManager();
    if (isCurrentUseZgEncoding(paramContext, paramConfiguration)) {
      str = "/system/fonts/MyanmarZg.ttf";
    } else {
      str = "/system/fonts/OplusOSUI-XThin.ttf";
    } 
    return relinkFontFile("/data/format_unclear/font/OplusOSUI-Myanmar.ttf", str);
  }
  
  private boolean relinkFontFile(String paramString1, String paramString2) {
    try {
      if (!Os.readlink(paramString1).equals(paramString2)) {
        File file = new File();
        this(paramString1);
        file.delete();
        Os.symlink(paramString2, paramString1);
        return true;
      } 
    } catch (IllegalArgumentException illegalArgumentException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("SELinux policy update malformed: ");
      stringBuilder.append(illegalArgumentException.getMessage());
      Log.d("OplusZGSupport", stringBuilder.toString());
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Could not update selinux policy: ");
      stringBuilder.append(exception.getMessage());
      Log.d("OplusZGSupport", stringBuilder.toString());
    } 
    return false;
  }
  
  private boolean isCurrentUseZgEncoding(Context paramContext, Configuration paramConfiguration) {
    if (paramConfiguration != null) {
      if (!paramConfiguration.getLocales().isEmpty() && paramConfiguration.getLocales().get(0) != null && paramConfiguration.getLocales().get(0).getCountry().equals("ZG"))
        return true; 
      if (paramConfiguration.getLocales().isEmpty()) {
        Locale locale = Locale.getDefault();
        if (locale != null && locale.getCountry().equals("ZG"))
          return true; 
      } 
    } 
    if (paramContext != null) {
      String str2 = Settings.System.getString(paramContext.getContentResolver(), "current_typeface_burmese");
      String str1 = Settings.System.getString(paramContext.getContentResolver(), "current_typeface");
      if (str2 == null || str2 == "") {
        if ("/system/fonts/MyanmarZg.ttf".equals(str1))
          return true; 
        return false;
      } 
      if ("/system/fonts/MyanmarZg.ttf".equals(str2))
        return true; 
    } 
    return false;
  }
}
