package android.view;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;

public abstract class InputFilter extends IInputFilter.Stub {
  private static final int MSG_INPUT_EVENT = 3;
  
  private static final int MSG_INSTALL = 1;
  
  private static final int MSG_UNINSTALL = 2;
  
  private final H mH;
  
  private IInputFilterHost mHost;
  
  private final InputEventConsistencyVerifier mInboundInputEventConsistencyVerifier;
  
  private final InputEventConsistencyVerifier mOutboundInputEventConsistencyVerifier;
  
  public InputFilter(Looper paramLooper) {
    InputEventConsistencyVerifier inputEventConsistencyVerifier2;
    boolean bool = InputEventConsistencyVerifier.isInstrumentationEnabled();
    InputEventConsistencyVerifier inputEventConsistencyVerifier1 = null;
    if (bool) {
      inputEventConsistencyVerifier2 = new InputEventConsistencyVerifier(this, 1, "InputFilter#InboundInputEventConsistencyVerifier");
    } else {
      inputEventConsistencyVerifier2 = null;
    } 
    this.mInboundInputEventConsistencyVerifier = inputEventConsistencyVerifier2;
    if (InputEventConsistencyVerifier.isInstrumentationEnabled()) {
      inputEventConsistencyVerifier2 = new InputEventConsistencyVerifier(this, 1, "InputFilter#OutboundInputEventConsistencyVerifier");
    } else {
      inputEventConsistencyVerifier2 = inputEventConsistencyVerifier1;
    } 
    this.mOutboundInputEventConsistencyVerifier = inputEventConsistencyVerifier2;
    this.mH = new H(paramLooper);
  }
  
  public final void install(IInputFilterHost paramIInputFilterHost) {
    this.mH.obtainMessage(1, paramIInputFilterHost).sendToTarget();
  }
  
  public final void uninstall() {
    this.mH.obtainMessage(2).sendToTarget();
  }
  
  public final void filterInputEvent(InputEvent paramInputEvent, int paramInt) {
    this.mH.obtainMessage(3, paramInt, 0, paramInputEvent).sendToTarget();
  }
  
  public void sendInputEvent(InputEvent paramInputEvent, int paramInt) {
    if (paramInputEvent != null) {
      if (this.mHost != null) {
        InputEventConsistencyVerifier inputEventConsistencyVerifier = this.mOutboundInputEventConsistencyVerifier;
        if (inputEventConsistencyVerifier != null)
          inputEventConsistencyVerifier.onInputEvent(paramInputEvent, 0); 
        try {
          this.mHost.sendInputEvent(paramInputEvent, paramInt);
        } catch (RemoteException remoteException) {}
        return;
      } 
      throw new IllegalStateException("Cannot send input event because the input filter is not installed.");
    } 
    throw new IllegalArgumentException("event must not be null");
  }
  
  public void onInputEvent(InputEvent paramInputEvent, int paramInt) {
    sendInputEvent(paramInputEvent, paramInt);
  }
  
  public void onInstalled() {}
  
  public void onUninstalled() {}
  
  class H extends Handler {
    final InputFilter this$0;
    
    public H(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 1) {
        if (i != 2) {
          if (i == 3) {
            InputEvent inputEvent = (InputEvent)param1Message.obj;
            try {
              if (InputFilter.this.mInboundInputEventConsistencyVerifier != null)
                InputFilter.this.mInboundInputEventConsistencyVerifier.onInputEvent(inputEvent, 0); 
              InputFilter.this.onInputEvent(inputEvent, param1Message.arg1);
            } finally {
              inputEvent.recycle();
            } 
          } 
        } else {
          try {
            InputFilter.this.onUninstalled();
          } finally {
            InputFilter.access$002(InputFilter.this, null);
          } 
        } 
      } else {
        InputFilter.access$002(InputFilter.this, (IInputFilterHost)param1Message.obj);
        if (InputFilter.this.mInboundInputEventConsistencyVerifier != null)
          InputFilter.this.mInboundInputEventConsistencyVerifier.reset(); 
        if (InputFilter.this.mOutboundInputEventConsistencyVerifier != null)
          InputFilter.this.mOutboundInputEventConsistencyVerifier.reset(); 
        InputFilter.this.onInstalled();
      } 
    }
  }
}
