package android.view;

import android.os.Parcel;
import com.oplus.direct.OplusDirectFindCmd;
import com.oplus.direct.OplusDirectUtils;
import java.lang.ref.WeakReference;

public class OplusDirectViewHelper extends OplusDummyDirectViewHelper {
  private static final boolean DBG = OplusDirectUtils.DBG;
  
  private static final String TAG = "DirectService";
  
  private final OplusDirectViewDump mDump = new OplusDirectViewDump();
  
  private final WeakReference<ViewRootImpl> mViewAncestor;
  
  public OplusDirectViewHelper(WeakReference<ViewRootImpl> paramWeakReference) {
    super(paramWeakReference);
    this.mViewAncestor = paramWeakReference;
  }
  
  public void directFindCmd(OplusDirectFindCmd paramOplusDirectFindCmd) {
    if (paramOplusDirectFindCmd == null)
      return; 
    ViewRootImpl viewRootImpl = this.mViewAncestor.get();
    if (viewRootImpl != null && viewRootImpl.mView != null) {
      this.mDump.findCmd(viewRootImpl, paramOplusDirectFindCmd);
    } else {
      OplusDirectUtils.onFindFailed(paramOplusDirectFindCmd.getCallback(), "no_view");
    } 
  }
  
  public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2) {
    if (paramInt1 != 10008)
      return false; 
    paramParcel1.enforceInterface("android.view.IWindow");
    if (paramParcel1.readInt() != 0) {
      OplusDirectFindCmd oplusDirectFindCmd = (OplusDirectFindCmd)OplusDirectFindCmd.CREATOR.createFromParcel(paramParcel1);
    } else {
      paramParcel1 = null;
    } 
    directFindCmd((OplusDirectFindCmd)paramParcel1);
    return true;
  }
}
