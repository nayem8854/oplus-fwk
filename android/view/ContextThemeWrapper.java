package android.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;

public class ContextThemeWrapper extends ContextWrapper {
  private LayoutInflater mInflater;
  
  private Configuration mOverrideConfiguration;
  
  private Resources mResources;
  
  private Resources.Theme mTheme;
  
  private int mThemeResource;
  
  public ContextThemeWrapper() {
    super(null);
  }
  
  public ContextThemeWrapper(Context paramContext, int paramInt) {
    super(paramContext);
    this.mThemeResource = paramInt;
  }
  
  public ContextThemeWrapper(Context paramContext, Resources.Theme paramTheme) {
    super(paramContext);
    this.mTheme = paramTheme;
  }
  
  protected void attachBaseContext(Context paramContext) {
    super.attachBaseContext(paramContext);
  }
  
  public void applyOverrideConfiguration(Configuration paramConfiguration) {
    if (this.mResources == null) {
      if (this.mOverrideConfiguration == null) {
        this.mOverrideConfiguration = new Configuration(paramConfiguration);
        return;
      } 
      throw new IllegalStateException("Override configuration has already been set");
    } 
    throw new IllegalStateException("getResources() or getAssets() has already been called");
  }
  
  public Configuration getOverrideConfiguration() {
    return this.mOverrideConfiguration;
  }
  
  public AssetManager getAssets() {
    return getResourcesInternal().getAssets();
  }
  
  public Resources getResources() {
    return getResourcesInternal();
  }
  
  private Resources getResourcesInternal() {
    if (this.mResources == null) {
      Configuration configuration = this.mOverrideConfiguration;
      if (configuration == null) {
        this.mResources = super.getResources();
      } else {
        Context context = createConfigurationContext(configuration);
        this.mResources = context.getResources();
      } 
    } 
    return this.mResources;
  }
  
  public void setTheme(int paramInt) {
    if (this.mThemeResource != paramInt) {
      this.mThemeResource = paramInt;
      initializeTheme();
    } 
  }
  
  public void setTheme(Resources.Theme paramTheme) {
    this.mTheme = paramTheme;
  }
  
  public int getThemeResId() {
    return this.mThemeResource;
  }
  
  public Resources.Theme getTheme() {
    Resources.Theme theme = this.mTheme;
    if (theme != null)
      return theme; 
    int i = this.mThemeResource;
    int j = (getApplicationInfo()).targetSdkVersion;
    this.mThemeResource = Resources.selectDefaultTheme(i, j);
    initializeTheme();
    return this.mTheme;
  }
  
  public Object getSystemService(String paramString) {
    if ("layout_inflater".equals(paramString)) {
      if (this.mInflater == null)
        this.mInflater = LayoutInflater.from(getBaseContext()).cloneInContext((Context)this); 
      return this.mInflater;
    } 
    return getBaseContext().getSystemService(paramString);
  }
  
  protected void onApplyThemeResource(Resources.Theme paramTheme, int paramInt, boolean paramBoolean) {
    paramTheme.applyStyle(paramInt, true);
  }
  
  private void initializeTheme() {
    boolean bool;
    if (this.mTheme == null) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      this.mTheme = getResources().newTheme();
      Resources.Theme theme = getBaseContext().getTheme();
      if (theme != null)
        this.mTheme.setTo(theme); 
    } 
    onApplyThemeResource(this.mTheme, this.mThemeResource, bool);
  }
}
