package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDockedStackListener extends IInterface {
  void onAdjustedForImeChanged(boolean paramBoolean, long paramLong) throws RemoteException;
  
  void onDividerVisibilityChanged(boolean paramBoolean) throws RemoteException;
  
  void onDockSideChanged(int paramInt) throws RemoteException;
  
  void onDockedStackExistsChanged(boolean paramBoolean) throws RemoteException;
  
  void onDockedStackMinimizedChanged(boolean paramBoolean1, long paramLong, boolean paramBoolean2) throws RemoteException;
  
  class Default implements IDockedStackListener {
    public void onDividerVisibilityChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onDockedStackExistsChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onDockedStackMinimizedChanged(boolean param1Boolean1, long param1Long, boolean param1Boolean2) throws RemoteException {}
    
    public void onAdjustedForImeChanged(boolean param1Boolean, long param1Long) throws RemoteException {}
    
    public void onDockSideChanged(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDockedStackListener {
    private static final String DESCRIPTOR = "android.view.IDockedStackListener";
    
    static final int TRANSACTION_onAdjustedForImeChanged = 4;
    
    static final int TRANSACTION_onDividerVisibilityChanged = 1;
    
    static final int TRANSACTION_onDockSideChanged = 5;
    
    static final int TRANSACTION_onDockedStackExistsChanged = 2;
    
    static final int TRANSACTION_onDockedStackMinimizedChanged = 3;
    
    public Stub() {
      attachInterface(this, "android.view.IDockedStackListener");
    }
    
    public static IDockedStackListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IDockedStackListener");
      if (iInterface != null && iInterface instanceof IDockedStackListener)
        return (IDockedStackListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "onDockSideChanged";
            } 
            return "onAdjustedForImeChanged";
          } 
          return "onDockedStackMinimizedChanged";
        } 
        return "onDockedStackExistsChanged";
      } 
      return "onDividerVisibilityChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.view.IDockedStackListener");
                return true;
              } 
              param1Parcel1.enforceInterface("android.view.IDockedStackListener");
              param1Int1 = param1Parcel1.readInt();
              onDockSideChanged(param1Int1);
              return true;
            } 
            param1Parcel1.enforceInterface("android.view.IDockedStackListener");
            if (param1Parcel1.readInt() != 0)
              bool4 = true; 
            long l1 = param1Parcel1.readLong();
            onAdjustedForImeChanged(bool4, l1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.view.IDockedStackListener");
          if (param1Parcel1.readInt() != 0) {
            bool4 = true;
          } else {
            bool4 = false;
          } 
          long l = param1Parcel1.readLong();
          if (param1Parcel1.readInt() != 0)
            bool1 = true; 
          onDockedStackMinimizedChanged(bool4, l, bool1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.view.IDockedStackListener");
        bool4 = bool2;
        if (param1Parcel1.readInt() != 0)
          bool4 = true; 
        onDockedStackExistsChanged(bool4);
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IDockedStackListener");
      bool4 = bool3;
      if (param1Parcel1.readInt() != 0)
        bool4 = true; 
      onDividerVisibilityChanged(bool4);
      return true;
    }
    
    private static class Proxy implements IDockedStackListener {
      public static IDockedStackListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IDockedStackListener";
      }
      
      public void onDividerVisibilityChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IDockedStackListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IDockedStackListener.Stub.getDefaultImpl() != null) {
            IDockedStackListener.Stub.getDefaultImpl().onDividerVisibilityChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDockedStackExistsChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IDockedStackListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel, null, 1);
          if (!bool1 && IDockedStackListener.Stub.getDefaultImpl() != null) {
            IDockedStackListener.Stub.getDefaultImpl().onDockedStackExistsChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDockedStackMinimizedChanged(boolean param2Boolean1, long param2Long, boolean param2Boolean2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IDockedStackListener");
          boolean bool1 = false;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          parcel.writeLong(param2Long);
          boolean bool2 = bool1;
          if (param2Boolean2)
            bool2 = true; 
          parcel.writeInt(bool2);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IDockedStackListener.Stub.getDefaultImpl() != null) {
            IDockedStackListener.Stub.getDefaultImpl().onDockedStackMinimizedChanged(param2Boolean1, param2Long, param2Boolean2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAdjustedForImeChanged(boolean param2Boolean, long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.view.IDockedStackListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeLong(param2Long);
          boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
          if (!bool1 && IDockedStackListener.Stub.getDefaultImpl() != null) {
            IDockedStackListener.Stub.getDefaultImpl().onAdjustedForImeChanged(param2Boolean, param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDockSideChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.view.IDockedStackListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IDockedStackListener.Stub.getDefaultImpl() != null) {
            IDockedStackListener.Stub.getDefaultImpl().onDockSideChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDockedStackListener param1IDockedStackListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDockedStackListener != null) {
          Proxy.sDefaultImpl = param1IDockedStackListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDockedStackListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
