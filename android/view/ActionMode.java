package android.view;

import android.graphics.Rect;

public abstract class ActionMode {
  public static final int DEFAULT_HIDE_DURATION = -1;
  
  public static final int TYPE_FLOATING = 1;
  
  public static final int TYPE_PRIMARY = 0;
  
  private Object mTag;
  
  private boolean mTitleOptionalHint;
  
  private int mType = 0;
  
  public void setTag(Object paramObject) {
    this.mTag = paramObject;
  }
  
  public Object getTag() {
    return this.mTag;
  }
  
  public void setTitleOptionalHint(boolean paramBoolean) {
    this.mTitleOptionalHint = paramBoolean;
  }
  
  public boolean getTitleOptionalHint() {
    return this.mTitleOptionalHint;
  }
  
  public boolean isTitleOptional() {
    return false;
  }
  
  public void setType(int paramInt) {
    this.mType = paramInt;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public void invalidateContentRect() {}
  
  public void hide(long paramLong) {}
  
  public void onWindowFocusChanged(boolean paramBoolean) {}
  
  public boolean isUiFocusable() {
    return true;
  }
  
  public abstract void finish();
  
  public abstract View getCustomView();
  
  public abstract Menu getMenu();
  
  public abstract MenuInflater getMenuInflater();
  
  public abstract CharSequence getSubtitle();
  
  public abstract CharSequence getTitle();
  
  public abstract void invalidate();
  
  public abstract void setCustomView(View paramView);
  
  public abstract void setSubtitle(int paramInt);
  
  public abstract void setSubtitle(CharSequence paramCharSequence);
  
  public abstract void setTitle(int paramInt);
  
  public abstract void setTitle(CharSequence paramCharSequence);
  
  public static interface Callback {
    boolean onActionItemClicked(ActionMode param1ActionMode, MenuItem param1MenuItem);
    
    boolean onCreateActionMode(ActionMode param1ActionMode, Menu param1Menu);
    
    void onDestroyActionMode(ActionMode param1ActionMode);
    
    boolean onPrepareActionMode(ActionMode param1ActionMode, Menu param1Menu);
  }
  
  class Callback2 implements Callback {
    public void onGetContentRect(ActionMode param1ActionMode, View param1View, Rect param1Rect) {
      if (param1View != null) {
        param1Rect.set(0, 0, param1View.getWidth(), param1View.getHeight());
      } else {
        param1Rect.set(0, 0, 0, 0);
      } 
    }
  }
}
