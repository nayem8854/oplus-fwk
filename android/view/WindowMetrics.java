package android.view;

import android.graphics.Rect;

public final class WindowMetrics {
  private final Rect mBounds;
  
  private final WindowInsets mWindowInsets;
  
  public WindowMetrics(Rect paramRect, WindowInsets paramWindowInsets) {
    this.mBounds = paramRect;
    this.mWindowInsets = paramWindowInsets;
  }
  
  public Rect getBounds() {
    return this.mBounds;
  }
  
  public WindowInsets getWindowInsets() {
    return this.mWindowInsets;
  }
}
