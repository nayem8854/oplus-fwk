package android.view;

import android.graphics.Insets;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import java.io.PrintWriter;
import java.util.Objects;

public class InsetsSource implements Parcelable {
  private boolean mIsFormInsetsAnimation = false;
  
  private final Rect mTmpFrame = new Rect();
  
  public InsetsSource(int paramInt) {
    this.mType = paramInt;
    this.mFrame = new Rect();
    this.mVisible = InsetsState.getDefaultVisibility(paramInt);
  }
  
  public InsetsSource(InsetsSource paramInsetsSource) {
    this.mType = paramInsetsSource.mType;
    this.mFrame = new Rect(paramInsetsSource.mFrame);
    this.mVisible = paramInsetsSource.mVisible;
    if (paramInsetsSource.mVisibleFrame != null) {
      Rect rect = new Rect(paramInsetsSource.mVisibleFrame);
    } else {
      paramInsetsSource = null;
    } 
    this.mVisibleFrame = (Rect)paramInsetsSource;
  }
  
  public InsetsSource(InsetsSource paramInsetsSource, boolean paramBoolean) {
    this.mIsFormInsetsAnimation = paramBoolean;
    this.mType = paramInsetsSource.mType;
    this.mFrame = new Rect(paramInsetsSource.mFrame);
    this.mVisible = paramInsetsSource.mVisible;
    if (paramInsetsSource.mVisibleFrame != null) {
      Rect rect = new Rect(paramInsetsSource.mVisibleFrame);
    } else {
      paramInsetsSource = null;
    } 
    this.mVisibleFrame = (Rect)paramInsetsSource;
  }
  
  public void setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mFrame.set(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void setFrame(Rect paramRect) {
    this.mFrame.set(paramRect);
  }
  
  public void setVisibleFrame(Rect paramRect) {
    if (paramRect != null)
      paramRect = new Rect(paramRect); 
    this.mVisibleFrame = paramRect;
  }
  
  public void setVisible(boolean paramBoolean) {
    this.mVisible = paramBoolean;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public Rect getFrame() {
    return this.mFrame;
  }
  
  public Rect getVisibleFrame() {
    return this.mVisibleFrame;
  }
  
  public boolean isVisible() {
    return this.mVisible;
  }
  
  boolean isUserControllable() {
    Rect rect = this.mVisibleFrame;
    return (rect == null || !rect.isEmpty());
  }
  
  public Insets calculateInsets(Rect paramRect, boolean paramBoolean) {
    return calculateInsets(paramRect, this.mFrame, paramBoolean);
  }
  
  public Insets calculateVisibleInsets(Rect paramRect) {
    Rect rect = this.mVisibleFrame;
    if (rect == null)
      rect = this.mFrame; 
    return calculateInsets(paramRect, rect, false);
  }
  
  private Insets calculateInsets(Rect paramRect1, Rect paramRect2, boolean paramBoolean) {
    if (!paramBoolean && !this.mVisible)
      return Insets.NONE; 
    if (getType() == 2)
      return Insets.of(0, paramRect2.height(), 0, 0); 
    if (!getIntersection(paramRect2, paramRect1, this.mTmpFrame))
      return Insets.NONE; 
    if (getType() == 13)
      return Insets.of(0, 0, 0, this.mTmpFrame.height()); 
    if (this.mTmpFrame.width() == paramRect1.width()) {
      if (this.mTmpFrame.top == paramRect1.top)
        return Insets.of(0, this.mTmpFrame.height(), 0, 0); 
      if (this.mTmpFrame.bottom == paramRect1.bottom) {
        if (getType() == 1 && 
          this.mIsFormInsetsAnimation && this.mTmpFrame.height() == 0)
          return Insets.of(0, 0, 0, 54); 
        return Insets.of(0, 0, 0, this.mTmpFrame.height());
      } 
      if (this.mTmpFrame.top == 0)
        return Insets.of(0, this.mTmpFrame.height(), 0, 0); 
    } else if (this.mTmpFrame.height() == paramRect1.height()) {
      if (this.mTmpFrame.left == paramRect1.left)
        return Insets.of(this.mTmpFrame.width(), 0, 0, 0); 
      if (this.mTmpFrame.right == paramRect1.right)
        return Insets.of(0, 0, this.mTmpFrame.width(), 0); 
    } 
    return Insets.NONE;
  }
  
  private static boolean getIntersection(Rect paramRect1, Rect paramRect2, Rect paramRect3) {
    if (paramRect1.left <= paramRect2.right && paramRect2.left <= paramRect1.right && paramRect1.top <= paramRect2.bottom && paramRect2.top <= paramRect1.bottom) {
      paramRect3.left = Math.max(paramRect1.left, paramRect2.left);
      paramRect3.top = Math.max(paramRect1.top, paramRect2.top);
      paramRect3.right = Math.min(paramRect1.right, paramRect2.right);
      paramRect3.bottom = Math.min(paramRect1.bottom, paramRect2.bottom);
      return true;
    } 
    paramRect3.setEmpty();
    return false;
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("InsetsSource type=");
    paramPrintWriter.print(InsetsState.typeToString(this.mType));
    paramPrintWriter.print(" frame=");
    paramPrintWriter.print(this.mFrame.toShortString());
    if (this.mVisibleFrame != null) {
      paramPrintWriter.print(" visibleFrame=");
      paramPrintWriter.print(this.mVisibleFrame.toShortString());
    } 
    paramPrintWriter.print(" visible=");
    paramPrintWriter.print(this.mVisible);
    paramPrintWriter.println();
  }
  
  public boolean equals(Object paramObject) {
    return equals(paramObject, false);
  }
  
  public boolean equals(Object paramObject, boolean paramBoolean) {
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    int i = this.mType;
    if (i != ((InsetsSource)paramObject).mType)
      return false; 
    boolean bool = this.mVisible;
    if (bool != ((InsetsSource)paramObject).mVisible)
      return false; 
    if (paramBoolean && !bool && i == 13)
      return true; 
    if (!Objects.equals(this.mVisibleFrame, ((InsetsSource)paramObject).mVisibleFrame))
      return false; 
    return this.mFrame.equals(((InsetsSource)paramObject).mFrame);
  }
  
  public int hashCode() {
    byte b;
    int i = this.mType;
    int j = this.mFrame.hashCode();
    Rect rect = this.mVisibleFrame;
    if (rect != null) {
      b = rect.hashCode();
    } else {
      b = 0;
    } 
    boolean bool = this.mVisible;
    return ((i * 31 + j) * 31 + b) * 31 + bool;
  }
  
  public InsetsSource(Parcel paramParcel) {
    this.mType = paramParcel.readInt();
    if (paramParcel.readInt() != 0) {
      this.mFrame = (Rect)Rect.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mFrame = null;
    } 
    if (paramParcel.readInt() != 0) {
      this.mVisibleFrame = (Rect)Rect.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mVisibleFrame = null;
    } 
    this.mVisible = paramParcel.readBoolean();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    if (this.mFrame != null) {
      paramParcel.writeInt(1);
      this.mFrame.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mVisibleFrame != null) {
      paramParcel.writeInt(1);
      this.mVisibleFrame.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeBoolean(this.mVisible);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("InsetsSource: {mType=");
    int i = this.mType;
    stringBuilder.append(InsetsState.typeToString(i));
    stringBuilder.append(", mFrame=");
    Rect rect = this.mFrame;
    stringBuilder.append(rect.toShortString());
    stringBuilder.append(", mVisible=");
    stringBuilder.append(this.mVisible);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void scaleFrame(float paramFloat) {
    Rect rect = this.mFrame;
    if (rect != null)
      rect.scale(paramFloat); 
    rect = this.mVisibleFrame;
    if (rect != null)
      rect.scale(paramFloat); 
  }
  
  public static final Parcelable.Creator<InsetsSource> CREATOR = new Parcelable.Creator<InsetsSource>() {
      public InsetsSource createFromParcel(Parcel param1Parcel) {
        return new InsetsSource(param1Parcel);
      }
      
      public InsetsSource[] newArray(int param1Int) {
        return new InsetsSource[param1Int];
      }
    };
  
  private static final int OPLUS_NAV_GESTURE_BAR_HEIGHT = 54;
  
  private final Rect mFrame;
  
  private final int mType;
  
  private boolean mVisible;
  
  private Rect mVisibleFrame;
}
