package android.view;

import android.content.Context;
import android.util.Log;

public abstract class ActionProvider {
  private static final String TAG = "ActionProvider";
  
  private SubUiVisibilityListener mSubUiVisibilityListener;
  
  private VisibilityListener mVisibilityListener;
  
  public ActionProvider(Context paramContext) {}
  
  public View onCreateActionView(MenuItem paramMenuItem) {
    return onCreateActionView();
  }
  
  public boolean overridesItemVisibility() {
    return false;
  }
  
  public boolean isVisible() {
    return true;
  }
  
  public void refreshVisibility() {
    if (this.mVisibilityListener != null && overridesItemVisibility())
      this.mVisibilityListener.onActionProviderVisibilityChanged(isVisible()); 
  }
  
  public boolean onPerformDefaultAction() {
    return false;
  }
  
  public boolean hasSubMenu() {
    return false;
  }
  
  public void onPrepareSubMenu(SubMenu paramSubMenu) {}
  
  public void subUiVisibilityChanged(boolean paramBoolean) {
    SubUiVisibilityListener subUiVisibilityListener = this.mSubUiVisibilityListener;
    if (subUiVisibilityListener != null)
      subUiVisibilityListener.onSubUiVisibilityChanged(paramBoolean); 
  }
  
  public void setSubUiVisibilityListener(SubUiVisibilityListener paramSubUiVisibilityListener) {
    this.mSubUiVisibilityListener = paramSubUiVisibilityListener;
  }
  
  public void setVisibilityListener(VisibilityListener paramVisibilityListener) {
    if (this.mVisibilityListener != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this ");
      stringBuilder.append(getClass().getSimpleName());
      stringBuilder.append(" instance while it is still in use somewhere else?");
      String str = stringBuilder.toString();
      Log.w("ActionProvider", str);
    } 
    this.mVisibilityListener = paramVisibilityListener;
  }
  
  public void reset() {
    this.mVisibilityListener = null;
    this.mSubUiVisibilityListener = null;
  }
  
  @Deprecated
  public abstract View onCreateActionView();
  
  public static interface SubUiVisibilityListener {
    void onSubUiVisibilityChanged(boolean param1Boolean);
  }
  
  public static interface VisibilityListener {
    void onActionProviderVisibilityChanged(boolean param1Boolean);
  }
}
