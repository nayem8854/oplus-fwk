package android.view;

import android.os.Build;
import android.util.Log;

public final class InputEventConsistencyVerifier {
  private static final String EVENT_TYPE_GENERIC_MOTION = "GenericMotionEvent";
  
  private static final String EVENT_TYPE_KEY = "KeyEvent";
  
  private static final String EVENT_TYPE_TOUCH = "TouchEvent";
  
  private static final String EVENT_TYPE_TRACKBALL = "TrackballEvent";
  
  public static final int FLAG_RAW_DEVICE_INPUT = 1;
  
  private static final boolean IS_ENG_BUILD = Build.IS_ENG;
  
  private static final int RECENT_EVENTS_TO_LOG = 5;
  
  private int mButtonsPressed;
  
  private final Object mCaller;
  
  private InputEvent mCurrentEvent;
  
  private String mCurrentEventType;
  
  private final int mFlags;
  
  private boolean mHoverEntered;
  
  private KeyState mKeyStateList;
  
  private int mLastEventSeq;
  
  private String mLastEventType;
  
  private int mLastNestingLevel;
  
  private final String mLogTag;
  
  private int mMostRecentEventIndex;
  
  private InputEvent[] mRecentEvents;
  
  private boolean[] mRecentEventsUnhandled;
  
  private int mTouchEventStreamDeviceId = -1;
  
  private boolean mTouchEventStreamIsTainted;
  
  private int mTouchEventStreamPointers;
  
  private int mTouchEventStreamSource;
  
  private boolean mTouchEventStreamUnhandled;
  
  private boolean mTrackballDown;
  
  private boolean mTrackballUnhandled;
  
  private StringBuilder mViolationMessage;
  
  public InputEventConsistencyVerifier(Object paramObject, int paramInt) {
    this(paramObject, paramInt, null);
  }
  
  public InputEventConsistencyVerifier(Object paramObject, int paramInt, String paramString) {
    this.mCaller = paramObject;
    this.mFlags = paramInt;
    if (paramString == null)
      paramString = "InputEventConsistencyVerifier"; 
    this.mLogTag = paramString;
  }
  
  public static boolean isInstrumentationEnabled() {
    return IS_ENG_BUILD;
  }
  
  public void reset() {
    this.mLastEventSeq = -1;
    this.mLastNestingLevel = 0;
    this.mTrackballDown = false;
    this.mTrackballUnhandled = false;
    this.mTouchEventStreamPointers = 0;
    this.mTouchEventStreamIsTainted = false;
    this.mTouchEventStreamUnhandled = false;
    this.mHoverEntered = false;
    this.mButtonsPressed = 0;
    while (this.mKeyStateList != null) {
      KeyState keyState = this.mKeyStateList;
      this.mKeyStateList = keyState.next;
      keyState.recycle();
    } 
  }
  
  public void onInputEvent(InputEvent paramInputEvent, int paramInt) {
    if (paramInputEvent instanceof KeyEvent) {
      paramInputEvent = paramInputEvent;
      onKeyEvent((KeyEvent)paramInputEvent, paramInt);
    } else {
      paramInputEvent = paramInputEvent;
      if (paramInputEvent.isTouchEvent()) {
        onTouchEvent((MotionEvent)paramInputEvent, paramInt);
      } else if ((paramInputEvent.getSource() & 0x4) != 0) {
        onTrackballEvent((MotionEvent)paramInputEvent, paramInt);
      } else {
        onGenericMotionEvent((MotionEvent)paramInputEvent, paramInt);
      } 
    } 
  }
  
  public void onKeyEvent(KeyEvent paramKeyEvent, int paramInt) {
    if (!startEvent(paramKeyEvent, paramInt, "KeyEvent"))
      return; 
    try {
      KeyState keyState;
      ensureMetaStateIsNormalized(paramKeyEvent.getMetaState());
      paramInt = paramKeyEvent.getAction();
      int i = paramKeyEvent.getDeviceId();
      int j = paramKeyEvent.getSource();
      int k = paramKeyEvent.getKeyCode();
      if (paramInt != 0) {
        if (paramInt != 1) {
          if (paramInt != 2) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Invalid action ");
            stringBuilder.append(KeyEvent.actionToString(paramInt));
            stringBuilder.append(" for key event.");
            problem(stringBuilder.toString());
          } 
        } else {
          keyState = findKeyState(i, j, k, true);
          if (keyState == null) {
            problem("ACTION_UP but key was not down.");
          } else {
            keyState.recycle();
          } 
        } 
      } else {
        KeyState keyState1 = findKeyState(i, j, k, false);
        if (keyState1 != null) {
          if (keyState1.unhandled) {
            keyState1.unhandled = false;
          } else if ((0x1 & this.mFlags) == 0 && keyState.getRepeatCount() == 0) {
            problem("ACTION_DOWN but key is already down and this event is not a key repeat.");
          } 
        } else {
          addKeyState(i, j, k);
        } 
      } 
      return;
    } finally {
      finishEvent();
    } 
  }
  
  public void onTrackballEvent(MotionEvent paramMotionEvent, int paramInt) {
    if (!startEvent(paramMotionEvent, paramInt, "TrackballEvent"))
      return; 
    try {
      ensureMetaStateIsNormalized(paramMotionEvent.getMetaState());
      int i = paramMotionEvent.getAction();
      paramInt = paramMotionEvent.getSource();
      if ((paramInt & 0x4) != 0) {
        if (i != 0) {
          if (i != 1) {
            if (i != 2) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Invalid action ");
              stringBuilder.append(MotionEvent.actionToString(i));
              stringBuilder.append(" for trackball event.");
              problem(stringBuilder.toString());
            } else {
              ensurePointerCountIsOneForThisAction(paramMotionEvent);
            } 
          } else {
            if (!this.mTrackballDown) {
              problem("ACTION_UP but trackball is not down.");
            } else {
              this.mTrackballDown = false;
              this.mTrackballUnhandled = false;
            } 
            ensureHistorySizeIsZeroForThisAction(paramMotionEvent);
            ensurePointerCountIsOneForThisAction(paramMotionEvent);
          } 
        } else {
          if (this.mTrackballDown && !this.mTrackballUnhandled) {
            problem("ACTION_DOWN but trackball is already down.");
          } else {
            this.mTrackballDown = true;
            this.mTrackballUnhandled = false;
          } 
          ensureHistorySizeIsZeroForThisAction(paramMotionEvent);
          ensurePointerCountIsOneForThisAction(paramMotionEvent);
        } 
        if (this.mTrackballDown && paramMotionEvent.getPressure() <= 0.0F) {
          problem("Trackball is down but pressure is not greater than 0.");
        } else if (!this.mTrackballDown && paramMotionEvent.getPressure() != 0.0F) {
          problem("Trackball is up but pressure is not equal to 0.");
        } 
      } else {
        problem("Source was not SOURCE_CLASS_TRACKBALL.");
      } 
      return;
    } finally {
      finishEvent();
    } 
  }
  
  public void onTouchEvent(MotionEvent paramMotionEvent, int paramInt) {
    if (!startEvent(paramMotionEvent, paramInt, "TouchEvent"))
      return; 
    int i = paramMotionEvent.getAction();
    if (i == 0 || i == 3 || i == 4) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    if (paramInt != 0 && (this.mTouchEventStreamIsTainted || this.mTouchEventStreamUnhandled)) {
      this.mTouchEventStreamIsTainted = false;
      this.mTouchEventStreamUnhandled = false;
      this.mTouchEventStreamPointers = 0;
    } 
    if (this.mTouchEventStreamIsTainted)
      paramMotionEvent.setTainted(true); 
    try {
      ensureMetaStateIsNormalized(paramMotionEvent.getMetaState());
      int j = paramMotionEvent.getDeviceId();
      int k = paramMotionEvent.getSource();
      if (paramInt == 0 && this.mTouchEventStreamDeviceId != -1 && (this.mTouchEventStreamDeviceId != j || this.mTouchEventStreamSource != k)) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Touch event stream contains events from multiple sources: previous device id ");
        stringBuilder.append(this.mTouchEventStreamDeviceId);
        stringBuilder.append(", previous source ");
        paramInt = this.mTouchEventStreamSource;
        stringBuilder.append(Integer.toHexString(paramInt));
        stringBuilder.append(", new device id ");
        stringBuilder.append(j);
        stringBuilder.append(", new source ");
        stringBuilder.append(Integer.toHexString(k));
        String str = stringBuilder.toString();
        problem(str);
      } 
      this.mTouchEventStreamDeviceId = j;
      this.mTouchEventStreamSource = k;
      paramInt = paramMotionEvent.getPointerCount();
      if ((k & 0x2) != 0) {
        StringBuilder stringBuilder;
        if (i != 0) {
          if (i != 1) {
            if (i != 2) {
              if (i != 3) {
                if (i != 4) {
                  k = paramMotionEvent.getActionMasked();
                  j = paramMotionEvent.getActionIndex();
                  if (k == 5) {
                    if (this.mTouchEventStreamPointers == 0) {
                      problem("ACTION_POINTER_DOWN but no other pointers were down.");
                      this.mTouchEventStreamIsTainted = true;
                    } 
                    if (j < 0 || j >= paramInt) {
                      StringBuilder stringBuilder1 = new StringBuilder();
                      this();
                      stringBuilder1.append("ACTION_POINTER_DOWN index is ");
                      stringBuilder1.append(j);
                      stringBuilder1.append(" but the pointer count is ");
                      stringBuilder1.append(paramInt);
                      stringBuilder1.append(".");
                      problem(stringBuilder1.toString());
                      this.mTouchEventStreamIsTainted = true;
                    } else {
                      paramInt = paramMotionEvent.getPointerId(j);
                      i = 1 << paramInt;
                      if ((this.mTouchEventStreamPointers & i) != 0) {
                        StringBuilder stringBuilder1 = new StringBuilder();
                        this();
                        stringBuilder1.append("ACTION_POINTER_DOWN specified pointer id ");
                        stringBuilder1.append(paramInt);
                        stringBuilder1.append(" which is already down.");
                        problem(stringBuilder1.toString());
                        this.mTouchEventStreamIsTainted = true;
                      } else {
                        this.mTouchEventStreamPointers |= i;
                      } 
                    } 
                    ensureHistorySizeIsZeroForThisAction(paramMotionEvent);
                  } else if (k == 6) {
                    if (j < 0 || j >= paramInt) {
                      StringBuilder stringBuilder1 = new StringBuilder();
                      this();
                      stringBuilder1.append("ACTION_POINTER_UP index is ");
                      stringBuilder1.append(j);
                      stringBuilder1.append(" but the pointer count is ");
                      stringBuilder1.append(paramInt);
                      stringBuilder1.append(".");
                      problem(stringBuilder1.toString());
                      this.mTouchEventStreamIsTainted = true;
                    } else {
                      i = paramMotionEvent.getPointerId(j);
                      paramInt = 1 << i;
                      if ((this.mTouchEventStreamPointers & paramInt) == 0) {
                        StringBuilder stringBuilder1 = new StringBuilder();
                        this();
                        stringBuilder1.append("ACTION_POINTER_UP specified pointer id ");
                        stringBuilder1.append(i);
                        stringBuilder1.append(" which is not currently down.");
                        problem(stringBuilder1.toString());
                        this.mTouchEventStreamIsTainted = true;
                      } else {
                        this.mTouchEventStreamPointers &= paramInt ^ 0xFFFFFFFF;
                      } 
                    } 
                    ensureHistorySizeIsZeroForThisAction(paramMotionEvent);
                  } else {
                    stringBuilder = new StringBuilder();
                    this();
                    stringBuilder.append("Invalid action ");
                    stringBuilder.append(MotionEvent.actionToString(i));
                    stringBuilder.append(" for touch event.");
                    problem(stringBuilder.toString());
                  } 
                } else {
                  if (this.mTouchEventStreamPointers != 0)
                    problem("ACTION_OUTSIDE but pointers are still down."); 
                  ensureHistorySizeIsZeroForThisAction((MotionEvent)stringBuilder);
                  ensurePointerCountIsOneForThisAction((MotionEvent)stringBuilder);
                  this.mTouchEventStreamIsTainted = false;
                } 
              } else {
                this.mTouchEventStreamPointers = 0;
                this.mTouchEventStreamIsTainted = false;
              } 
            } else {
              i = this.mTouchEventStreamPointers;
              i = Integer.bitCount(i);
              if (paramInt != i) {
                stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("ACTION_MOVE contained ");
                stringBuilder.append(paramInt);
                stringBuilder.append(" pointers but there are currently ");
                stringBuilder.append(i);
                stringBuilder.append(" pointers down.");
                problem(stringBuilder.toString());
                this.mTouchEventStreamIsTainted = true;
              } 
            } 
          } else {
            ensureHistorySizeIsZeroForThisAction((MotionEvent)stringBuilder);
            ensurePointerCountIsOneForThisAction((MotionEvent)stringBuilder);
            this.mTouchEventStreamPointers = 0;
            this.mTouchEventStreamIsTainted = false;
          } 
        } else {
          if (this.mTouchEventStreamPointers != 0)
            problem("ACTION_DOWN but pointers are already down.  Probably missing ACTION_UP from previous gesture."); 
          ensureHistorySizeIsZeroForThisAction((MotionEvent)stringBuilder);
          ensurePointerCountIsOneForThisAction((MotionEvent)stringBuilder);
          this.mTouchEventStreamPointers = 1 << stringBuilder.getPointerId(0);
        } 
      } else {
        problem("Source was not SOURCE_CLASS_POINTER.");
      } 
      return;
    } finally {
      finishEvent();
    } 
  }
  
  public void onGenericMotionEvent(MotionEvent paramMotionEvent, int paramInt) {
    if (!startEvent(paramMotionEvent, paramInt, "GenericMotionEvent"))
      return; 
    try {
      StringBuilder stringBuilder;
      ensureMetaStateIsNormalized(paramMotionEvent.getMetaState());
      int i = paramMotionEvent.getAction();
      int j = paramMotionEvent.getSource();
      paramInt = paramMotionEvent.getButtonState();
      int k = paramMotionEvent.getActionButton();
      if ((j & 0x2) != 0) {
        switch (i) {
          default:
            problem("Invalid action for generic pointer event.");
            return;
          case 12:
            ensureActionButtonIsNonZeroForThisAction(paramMotionEvent);
            if ((this.mButtonsPressed & k) != k) {
              stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Action button for ACTION_BUTTON_RELEASE event is ");
              stringBuilder.append(k);
              stringBuilder.append(", but it was either never pressed or has already been released.");
              problem(stringBuilder.toString());
            } 
            this.mButtonsPressed = j = this.mButtonsPressed & (k ^ 0xFFFFFFFF);
            if (k == 32 && (paramInt & 0x2) == 0) {
              this.mButtonsPressed = j & 0xFFFFFFFD;
            } else if (k == 64 && (paramInt & 0x4) == 0) {
              this.mButtonsPressed &= 0xFFFFFFFB;
            } 
            if (this.mButtonsPressed != paramInt) {
              k = this.mButtonsPressed;
              problem(String.format("Reported button state differs from expected button state based on press and release events. Is 0x%08x but expected 0x%08x.", new Object[] { Integer.valueOf(paramInt), Integer.valueOf(k) }));
            } 
            break;
          case 11:
            ensureActionButtonIsNonZeroForThisAction((MotionEvent)stringBuilder);
            if ((this.mButtonsPressed & k) != 0) {
              stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Action button for ACTION_BUTTON_PRESS event is ");
              stringBuilder.append(k);
              stringBuilder.append(", but it has already been pressed and has yet to be released.");
              problem(stringBuilder.toString());
            } 
            this.mButtonsPressed = j = this.mButtonsPressed | k;
            if (k == 32 && (paramInt & 0x2) != 0) {
              this.mButtonsPressed = j | 0x2;
            } else if (k == 64 && (paramInt & 0x4) != 0) {
              this.mButtonsPressed |= 0x4;
            } 
            if (this.mButtonsPressed != paramInt) {
              k = this.mButtonsPressed;
              problem(String.format("Reported button state differs from expected button state based on press and release events. Is 0x%08x but expected 0x%08x.", new Object[] { Integer.valueOf(paramInt), Integer.valueOf(k) }));
            } 
            break;
          case 10:
            ensurePointerCountIsOneForThisAction((MotionEvent)stringBuilder);
            if (!this.mHoverEntered)
              problem("ACTION_HOVER_EXIT without prior ACTION_HOVER_ENTER"); 
            this.mHoverEntered = false;
            break;
          case 9:
            ensurePointerCountIsOneForThisAction((MotionEvent)stringBuilder);
            this.mHoverEntered = true;
            break;
          case 8:
            ensureHistorySizeIsZeroForThisAction((MotionEvent)stringBuilder);
            ensurePointerCountIsOneForThisAction((MotionEvent)stringBuilder);
            break;
          case 7:
            ensurePointerCountIsOneForThisAction((MotionEvent)stringBuilder);
            break;
        } 
      } else if ((j & 0x10) != 0) {
        if (i != 2) {
          problem("Invalid action for generic joystick event.");
        } else {
          ensurePointerCountIsOneForThisAction((MotionEvent)stringBuilder);
        } 
      } 
      return;
    } finally {
      finishEvent();
    } 
  }
  
  public void onUnhandledEvent(InputEvent paramInputEvent, int paramInt) {
    KeyState keyState;
    if (paramInt != this.mLastNestingLevel)
      return; 
    boolean[] arrayOfBoolean = this.mRecentEventsUnhandled;
    if (arrayOfBoolean != null)
      arrayOfBoolean[this.mMostRecentEventIndex] = true; 
    if (paramInputEvent instanceof KeyEvent) {
      paramInputEvent = paramInputEvent;
      paramInt = paramInputEvent.getDeviceId();
      int i = paramInputEvent.getSource();
      int j = paramInputEvent.getKeyCode();
      keyState = findKeyState(paramInt, i, j, false);
      if (keyState != null)
        keyState.unhandled = true; 
    } else {
      MotionEvent motionEvent = (MotionEvent)keyState;
      if (motionEvent.isTouchEvent()) {
        this.mTouchEventStreamUnhandled = true;
      } else if ((motionEvent.getSource() & 0x4) != 0 && 
        this.mTrackballDown) {
        this.mTrackballUnhandled = true;
      } 
    } 
  }
  
  private void ensureMetaStateIsNormalized(int paramInt) {
    int i = KeyEvent.normalizeMetaState(paramInt);
    if (i != paramInt)
      problem(String.format("Metastate not normalized.  Was 0x%08x but expected 0x%08x.", new Object[] { Integer.valueOf(paramInt), Integer.valueOf(i) })); 
  }
  
  private void ensurePointerCountIsOneForThisAction(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getPointerCount();
    if (i != 1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Pointer count is ");
      stringBuilder.append(i);
      stringBuilder.append(" but it should always be 1 for ");
      stringBuilder.append(MotionEvent.actionToString(paramMotionEvent.getAction()));
      String str = stringBuilder.toString();
      problem(str);
    } 
  }
  
  private void ensureActionButtonIsNonZeroForThisAction(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getActionButton();
    if (i == 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No action button set. Action button should always be non-zero for ");
      stringBuilder.append(MotionEvent.actionToString(paramMotionEvent.getAction()));
      String str = stringBuilder.toString();
      problem(str);
    } 
  }
  
  private void ensureHistorySizeIsZeroForThisAction(MotionEvent paramMotionEvent) {
    int i = paramMotionEvent.getHistorySize();
    if (i != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("History size is ");
      stringBuilder.append(i);
      stringBuilder.append(" but it should always be 0 for ");
      stringBuilder.append(MotionEvent.actionToString(paramMotionEvent.getAction()));
      String str = stringBuilder.toString();
      problem(str);
    } 
  }
  
  private boolean startEvent(InputEvent paramInputEvent, int paramInt, String paramString) {
    int i = paramInputEvent.getSequenceNumber();
    if (i == this.mLastEventSeq && paramInt < this.mLastNestingLevel && paramString == this.mLastEventType)
      return false; 
    if (paramInt > 0) {
      this.mLastEventSeq = i;
      this.mLastEventType = paramString;
      this.mLastNestingLevel = paramInt;
    } else {
      this.mLastEventSeq = -1;
      this.mLastEventType = null;
      this.mLastNestingLevel = 0;
    } 
    this.mCurrentEvent = paramInputEvent;
    this.mCurrentEventType = paramString;
    return true;
  }
  
  private void finishEvent() {
    StringBuilder stringBuilder = this.mViolationMessage;
    if (stringBuilder != null && stringBuilder.length() != 0) {
      if (!this.mCurrentEvent.isTainted()) {
        stringBuilder = this.mViolationMessage;
        stringBuilder.append("\n  in ");
        stringBuilder.append(this.mCaller);
        this.mViolationMessage.append("\n  ");
        appendEvent(this.mViolationMessage, 0, this.mCurrentEvent, false);
        if (this.mRecentEvents != null) {
          this.mViolationMessage.append("\n  -- recent events --");
          for (byte b = 0; b < 5; b++) {
            int j = (this.mMostRecentEventIndex + 5 - b) % 5;
            InputEvent inputEvent = this.mRecentEvents[j];
            if (inputEvent == null)
              break; 
            this.mViolationMessage.append("\n  ");
            appendEvent(this.mViolationMessage, b + 1, inputEvent, this.mRecentEventsUnhandled[j]);
          } 
        } 
        Log.d(this.mLogTag, this.mViolationMessage.toString());
        this.mCurrentEvent.setTainted(true);
      } 
      this.mViolationMessage.setLength(0);
    } 
    if (this.mRecentEvents == null) {
      this.mRecentEvents = new InputEvent[5];
      this.mRecentEventsUnhandled = new boolean[5];
    } 
    int i = (this.mMostRecentEventIndex + 1) % 5;
    this.mMostRecentEventIndex = i;
    InputEvent[] arrayOfInputEvent = this.mRecentEvents;
    if (arrayOfInputEvent[i] != null)
      arrayOfInputEvent[i].recycle(); 
    this.mRecentEvents[i] = this.mCurrentEvent.copy();
    this.mRecentEventsUnhandled[i] = false;
    this.mCurrentEvent = null;
    this.mCurrentEventType = null;
  }
  
  private static void appendEvent(StringBuilder paramStringBuilder, int paramInt, InputEvent paramInputEvent, boolean paramBoolean) {
    paramStringBuilder.append(paramInt);
    paramStringBuilder.append(": sent at ");
    paramStringBuilder.append(paramInputEvent.getEventTimeNano());
    paramStringBuilder.append(", ");
    if (paramBoolean)
      paramStringBuilder.append("(unhandled) "); 
    paramStringBuilder.append(paramInputEvent);
  }
  
  private void problem(String paramString) {
    if (this.mViolationMessage == null)
      this.mViolationMessage = new StringBuilder(); 
    if (this.mViolationMessage.length() == 0) {
      StringBuilder stringBuilder = this.mViolationMessage;
      stringBuilder.append(this.mCurrentEventType);
      stringBuilder.append(": ");
    } else {
      this.mViolationMessage.append("\n  ");
    } 
    this.mViolationMessage.append(paramString);
  }
  
  private KeyState findKeyState(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) {
    KeyState keyState1 = null;
    KeyState keyState2 = this.mKeyStateList;
    while (keyState2 != null) {
      if (keyState2.deviceId == paramInt1 && keyState2.source == paramInt2 && keyState2.keyCode == paramInt3) {
        if (paramBoolean) {
          if (keyState1 != null) {
            keyState1.next = keyState2.next;
          } else {
            this.mKeyStateList = keyState2.next;
          } 
          keyState2.next = null;
        } 
        return keyState2;
      } 
      keyState1 = keyState2;
      keyState2 = keyState2.next;
    } 
    return null;
  }
  
  private void addKeyState(int paramInt1, int paramInt2, int paramInt3) {
    KeyState keyState = KeyState.obtain(paramInt1, paramInt2, paramInt3);
    keyState.next = this.mKeyStateList;
    this.mKeyStateList = keyState;
  }
  
  private static final class KeyState {
    private static KeyState mRecycledList;
    
    private static Object mRecycledListLock = new Object();
    
    public int deviceId;
    
    public int keyCode;
    
    public KeyState next;
    
    public int source;
    
    public boolean unhandled;
    
    public static KeyState obtain(int param1Int1, int param1Int2, int param1Int3) {
      synchronized (mRecycledListLock) {
        KeyState keyState = mRecycledList;
        if (keyState != null) {
          mRecycledList = keyState.next;
        } else {
          keyState = new KeyState();
        } 
        keyState.deviceId = param1Int1;
        keyState.source = param1Int2;
        keyState.keyCode = param1Int3;
        keyState.unhandled = false;
        return keyState;
      } 
    }
    
    public void recycle() {
      synchronized (mRecycledListLock) {
        KeyState keyState = mRecycledList;
        mRecycledList = keyState;
        return;
      } 
    }
  }
}
