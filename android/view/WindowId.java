package android.view;

import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import java.util.HashMap;

public class WindowId implements Parcelable {
  class H extends Handler {
    final WindowId.FocusObserver this$0;
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 1) {
        if (i != 2) {
          super.handleMessage(param1Message);
        } else {
          this.this$0.onFocusLost((WindowId)param1Message.obj);
        } 
      } else {
        this.this$0.onFocusGained((WindowId)param1Message.obj);
      } 
    }
  }
  
  class FocusObserver {
    final Handler mHandler;
    
    final IWindowFocusObserver.Stub mIObserver = (IWindowFocusObserver.Stub)new Object(this);
    
    final HashMap<IBinder, WindowId> mRegistrations = new HashMap<>();
    
    class H extends Handler {
      final WindowId.FocusObserver this$0;
      
      public void handleMessage(Message param2Message) {
        int i = param2Message.what;
        if (i != 1) {
          if (i != 2) {
            super.handleMessage(param2Message);
          } else {
            WindowId.FocusObserver.this.onFocusLost((WindowId)param2Message.obj);
          } 
        } else {
          WindowId.FocusObserver.this.onFocusGained((WindowId)param2Message.obj);
        } 
      }
    }
    
    public FocusObserver() {
      this.mHandler = new H();
    }
    
    public abstract void onFocusGained(WindowId param1WindowId);
    
    public abstract void onFocusLost(WindowId param1WindowId);
  }
  
  public boolean isFocused() {
    try {
      return this.mToken.isFocused();
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public void registerFocusObserver(FocusObserver paramFocusObserver) {
    synchronized (paramFocusObserver.mRegistrations) {
      if (!paramFocusObserver.mRegistrations.containsKey(this.mToken.asBinder())) {
        paramFocusObserver.mRegistrations.put(this.mToken.asBinder(), this);
        try {
          this.mToken.registerFocusObserver(paramFocusObserver.mIObserver);
        } catch (RemoteException remoteException) {}
        return;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Focus observer already registered with input token");
      throw illegalStateException;
    } 
  }
  
  public void unregisterFocusObserver(FocusObserver paramFocusObserver) {
    synchronized (paramFocusObserver.mRegistrations) {
      Object object = paramFocusObserver.mRegistrations.remove(this.mToken.asBinder());
      if (object != null) {
        try {
          this.mToken.unregisterFocusObserver(paramFocusObserver.mIObserver);
        } catch (RemoteException remoteException) {}
        return;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Focus observer not registered with input token");
      throw illegalStateException;
    } 
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject instanceof WindowId)
      return this.mToken.asBinder().equals(((WindowId)paramObject).mToken.asBinder()); 
    return false;
  }
  
  public int hashCode() {
    return this.mToken.asBinder().hashCode();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("IntentSender{");
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    stringBuilder.append(": ");
    stringBuilder.append(this.mToken.asBinder());
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStrongBinder(this.mToken.asBinder());
  }
  
  public static final Parcelable.Creator<WindowId> CREATOR = new Parcelable.Creator<WindowId>() {
      public WindowId createFromParcel(Parcel param1Parcel) {
        IBinder iBinder = param1Parcel.readStrongBinder();
        if (iBinder != null) {
          WindowId windowId = new WindowId(iBinder);
        } else {
          iBinder = null;
        } 
        return (WindowId)iBinder;
      }
      
      public WindowId[] newArray(int param1Int) {
        return new WindowId[param1Int];
      }
    };
  
  private final IWindowId mToken;
  
  public IWindowId getTarget() {
    return this.mToken;
  }
  
  public WindowId(IWindowId paramIWindowId) {
    this.mToken = paramIWindowId;
  }
  
  public WindowId(IBinder paramIBinder) {
    this.mToken = IWindowId.Stub.asInterface(paramIBinder);
  }
}
