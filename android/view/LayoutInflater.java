package android.view;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Message;
import android.os.Trace;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.util.Xml;
import android.widget.FrameLayout;
import com.android.internal.R;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public abstract class LayoutInflater {
  private static final int[] ATTRS_THEME;
  
  private static final String ATTR_LAYOUT = "layout";
  
  private static final ClassLoader BOOT_CLASS_LOADER;
  
  private static final String COMPILED_VIEW_DEX_FILE_NAME = "/compiled_view.dex";
  
  private static final boolean DEBUG = false;
  
  private static final StackTraceElement[] EMPTY_STACK_TRACE;
  
  private static final String TAG = LayoutInflater.class.getSimpleName();
  
  private static final String TAG_1995 = "blink";
  
  private static final String TAG_INCLUDE = "include";
  
  private static final String TAG_MERGE = "merge";
  
  private static final String TAG_REQUEST_FOCUS = "requestFocus";
  
  private static final String TAG_TAG = "tag";
  
  private static final String USE_PRECOMPILED_LAYOUT = "view.precompiled_layout_enabled";
  
  static final Class<?>[] mConstructorSignature;
  
  private static final HashMap<String, Constructor<? extends View>> sConstructorMap;
  
  static {
    EMPTY_STACK_TRACE = new StackTraceElement[0];
    mConstructorSignature = new Class[] { Context.class, AttributeSet.class };
    sConstructorMap = new HashMap<>();
    ATTRS_THEME = new int[] { 16842752 };
    BOOT_CLASS_LOADER = LayoutInflater.class.getClassLoader();
  }
  
  final Object[] mConstructorArgs = new Object[2];
  
  protected final Context mContext;
  
  private Factory mFactory;
  
  private Factory2 mFactory2;
  
  private boolean mFactorySet;
  
  private Filter mFilter;
  
  private HashMap<String, Boolean> mFilterMap;
  
  private ClassLoader mPrecompiledClassLoader;
  
  private Factory2 mPrivateFactory;
  
  private TypedValue mTempValue;
  
  private boolean mUseCompiledView;
  
  public static interface Filter {
    boolean onLoadClass(Class param1Class);
  }
  
  class FactoryMerger implements Factory2 {
    private final LayoutInflater.Factory mF1;
    
    private final LayoutInflater.Factory2 mF12;
    
    private final LayoutInflater.Factory mF2;
    
    private final LayoutInflater.Factory2 mF22;
    
    FactoryMerger(LayoutInflater this$0, LayoutInflater.Factory2 param1Factory21, LayoutInflater.Factory param1Factory1, LayoutInflater.Factory2 param1Factory22) {
      this.mF1 = (LayoutInflater.Factory)this$0;
      this.mF2 = param1Factory1;
      this.mF12 = param1Factory21;
      this.mF22 = param1Factory22;
    }
    
    public View onCreateView(String param1String, Context param1Context, AttributeSet param1AttributeSet) {
      View view = this.mF1.onCreateView(param1String, param1Context, param1AttributeSet);
      if (view != null)
        return view; 
      return this.mF2.onCreateView(param1String, param1Context, param1AttributeSet);
    }
    
    public View onCreateView(View param1View, String param1String, Context param1Context, AttributeSet param1AttributeSet) {
      View view;
      LayoutInflater.Factory2 factory22 = this.mF12;
      if (factory22 != null) {
        view = factory22.onCreateView(param1View, param1String, param1Context, param1AttributeSet);
      } else {
        view = this.mF1.onCreateView(param1String, param1Context, param1AttributeSet);
      } 
      if (view != null)
        return view; 
      LayoutInflater.Factory2 factory21 = this.mF22;
      if (factory21 != null) {
        param1View = factory21.onCreateView(param1View, param1String, param1Context, param1AttributeSet);
      } else {
        param1View = this.mF2.onCreateView(param1String, param1Context, param1AttributeSet);
      } 
      return param1View;
    }
  }
  
  protected LayoutInflater(Context paramContext) {
    this.mContext = paramContext;
    initPrecompiledViews();
  }
  
  protected LayoutInflater(LayoutInflater paramLayoutInflater, Context paramContext) {
    this.mContext = paramContext;
    this.mFactory = paramLayoutInflater.mFactory;
    this.mFactory2 = paramLayoutInflater.mFactory2;
    this.mPrivateFactory = paramLayoutInflater.mPrivateFactory;
    setFilter(paramLayoutInflater.mFilter);
    initPrecompiledViews();
  }
  
  public static LayoutInflater from(Context paramContext) {
    LayoutInflater layoutInflater = (LayoutInflater)paramContext.getSystemService("layout_inflater");
    if (layoutInflater != null)
      return layoutInflater; 
    throw new AssertionError("LayoutInflater not found.");
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  public final Factory getFactory() {
    return this.mFactory;
  }
  
  public final Factory2 getFactory2() {
    return this.mFactory2;
  }
  
  public void setFactory(Factory paramFactory) {
    if (!this.mFactorySet) {
      if (paramFactory != null) {
        this.mFactorySet = true;
        Factory factory = this.mFactory;
        if (factory == null) {
          this.mFactory = paramFactory;
        } else {
          this.mFactory = new FactoryMerger(paramFactory, null, factory, this.mFactory2);
        } 
        return;
      } 
      throw new NullPointerException("Given factory can not be null");
    } 
    throw new IllegalStateException("A factory has already been set on this LayoutInflater");
  }
  
  public void setFactory2(Factory2 paramFactory2) {
    if (!this.mFactorySet) {
      if (paramFactory2 != null) {
        this.mFactorySet = true;
        Factory factory = this.mFactory;
        if (factory == null) {
          this.mFactory2 = paramFactory2;
          this.mFactory = paramFactory2;
        } else {
          this.mFactory2 = paramFactory2 = new FactoryMerger(paramFactory2, paramFactory2, factory, this.mFactory2);
          this.mFactory = paramFactory2;
        } 
        return;
      } 
      throw new NullPointerException("Given factory can not be null");
    } 
    throw new IllegalStateException("A factory has already been set on this LayoutInflater");
  }
  
  public void setPrivateFactory(Factory2 paramFactory2) {
    Factory2 factory2 = this.mPrivateFactory;
    if (factory2 == null) {
      this.mPrivateFactory = paramFactory2;
    } else {
      this.mPrivateFactory = new FactoryMerger(paramFactory2, paramFactory2, factory2, factory2);
    } 
  }
  
  public Filter getFilter() {
    return this.mFilter;
  }
  
  public void setFilter(Filter paramFilter) {
    this.mFilter = paramFilter;
    if (paramFilter != null)
      this.mFilterMap = new HashMap<>(); 
  }
  
  private void initPrecompiledViews() {
    initPrecompiledViews(false);
  }
  
  private void initPrecompiledViews(boolean paramBoolean) {
    this.mUseCompiledView = paramBoolean;
    if (!paramBoolean) {
      this.mPrecompiledClassLoader = null;
      return;
    } 
    ApplicationInfo applicationInfo = this.mContext.getApplicationInfo();
    if (applicationInfo.isEmbeddedDexUsed() || applicationInfo.isPrivilegedApp()) {
      this.mUseCompiledView = false;
      return;
    } 
    try {
      this.mPrecompiledClassLoader = this.mContext.getClassLoader();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(this.mContext.getCodeCacheDir());
      stringBuilder.append("/compiled_view.dex");
      String str = stringBuilder.toString();
      File file = new File();
      this(str);
    } finally {
      applicationInfo = null;
    } 
    if (!this.mUseCompiledView)
      this.mPrecompiledClassLoader = null; 
  }
  
  public void setPrecompiledLayoutsEnabledForTesting(boolean paramBoolean) {
    initPrecompiledViews(paramBoolean);
  }
  
  public View inflate(int paramInt, ViewGroup paramViewGroup) {
    boolean bool;
    if (paramViewGroup != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return inflate(paramInt, paramViewGroup, bool);
  }
  
  public View inflate(XmlPullParser paramXmlPullParser, ViewGroup paramViewGroup) {
    boolean bool;
    if (paramViewGroup != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return inflate(paramXmlPullParser, paramViewGroup, bool);
  }
  
  public View inflate(int paramInt, ViewGroup paramViewGroup, boolean paramBoolean) {
    Resources resources = getContext().getResources();
    View view = tryInflatePrecompiled(paramInt, resources, paramViewGroup, paramBoolean);
    if (view != null)
      return view; 
    XmlResourceParser xmlResourceParser = resources.getLayout(paramInt);
    try {
      return inflate((XmlPullParser)xmlResourceParser, paramViewGroup, paramBoolean);
    } finally {
      xmlResourceParser.close();
    } 
  }
  
  private View tryInflatePrecompiled(int paramInt, Resources paramResources, ViewGroup paramViewGroup, boolean paramBoolean) {
    if (!this.mUseCompiledView)
      return null; 
    Trace.traceBegin(8L, "inflate (precompiled)");
    String str1 = paramResources.getResourcePackageName(paramInt);
    String str2 = paramResources.getResourceEntryName(paramInt);
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("");
      stringBuilder.append(str1);
      stringBuilder.append(".CompiledView");
      Class<?> clazz = Class.forName(stringBuilder.toString(), false, this.mPrecompiledClassLoader);
      Method method = clazz.getMethod(str2, new Class[] { Context.class, int.class });
      View view = (View)method.invoke(null, new Object[] { this.mContext, Integer.valueOf(paramInt) });
      return view;
    } finally {
      paramResources = null;
      Trace.traceEnd(8L);
    } 
  }
  
  private void advanceToRootNode(XmlPullParser paramXmlPullParser) throws InflateException, IOException, XmlPullParserException {
    int i;
    while (true) {
      i = paramXmlPullParser.next();
      if (i != 2 && i != 1)
        continue; 
      break;
    } 
    if (i == 2)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramXmlPullParser.getPositionDescription());
    stringBuilder.append(": No start tag found!");
    throw new InflateException(stringBuilder.toString());
  }
  
  public View inflate(XmlPullParser paramXmlPullParser, ViewGroup paramViewGroup, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mConstructorArgs : [Ljava/lang/Object;
    //   4: astore #4
    //   6: aload #4
    //   8: monitorenter
    //   9: ldc2_w 8
    //   12: ldc_w 'inflate'
    //   15: invokestatic traceBegin : (JLjava/lang/String;)V
    //   18: aload_0
    //   19: getfield mContext : Landroid/content/Context;
    //   22: astore #5
    //   24: aload_1
    //   25: invokestatic asAttributeSet : (Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
    //   28: astore #6
    //   30: aload_0
    //   31: getfield mConstructorArgs : [Ljava/lang/Object;
    //   34: iconst_0
    //   35: aaload
    //   36: checkcast android/content/Context
    //   39: astore #7
    //   41: aload_0
    //   42: getfield mConstructorArgs : [Ljava/lang/Object;
    //   45: iconst_0
    //   46: aload #5
    //   48: aastore
    //   49: aload_2
    //   50: astore #8
    //   52: aload_0
    //   53: aload_1
    //   54: invokespecial advanceToRootNode : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   57: aload_1
    //   58: invokeinterface getName : ()Ljava/lang/String;
    //   63: astore #9
    //   65: ldc 'merge'
    //   67: aload #9
    //   69: invokevirtual equals : (Ljava/lang/Object;)Z
    //   72: istore #10
    //   74: iload #10
    //   76: ifeq -> 114
    //   79: aload_2
    //   80: ifnull -> 101
    //   83: iload_3
    //   84: ifeq -> 101
    //   87: aload_0
    //   88: aload_1
    //   89: aload_2
    //   90: aload #5
    //   92: aload #6
    //   94: iconst_0
    //   95: invokevirtual rInflate : (Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;Z)V
    //   98: goto -> 199
    //   101: new android/view/InflateException
    //   104: astore_1
    //   105: aload_1
    //   106: ldc_w '<merge /> can be used only with a valid ViewGroup root and attachToRoot=true'
    //   109: invokespecial <init> : (Ljava/lang/String;)V
    //   112: aload_1
    //   113: athrow
    //   114: aload_0
    //   115: aload_2
    //   116: aload #9
    //   118: aload #5
    //   120: aload #6
    //   122: invokespecial createViewFromTag : (Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    //   125: astore #11
    //   127: aconst_null
    //   128: astore #9
    //   130: aload_2
    //   131: ifnull -> 161
    //   134: aload_2
    //   135: aload #6
    //   137: invokevirtual generateLayoutParams : (Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    //   140: astore #12
    //   142: aload #12
    //   144: astore #9
    //   146: iload_3
    //   147: ifne -> 161
    //   150: aload #11
    //   152: aload #12
    //   154: invokevirtual setLayoutParams : (Landroid/view/ViewGroup$LayoutParams;)V
    //   157: aload #12
    //   159: astore #9
    //   161: aload_0
    //   162: aload_1
    //   163: aload #11
    //   165: aload #6
    //   167: iconst_1
    //   168: invokevirtual rInflateChildren : (Lorg/xmlpull/v1/XmlPullParser;Landroid/view/View;Landroid/util/AttributeSet;Z)V
    //   171: aload_2
    //   172: ifnull -> 187
    //   175: iload_3
    //   176: ifeq -> 187
    //   179: aload_2
    //   180: aload #11
    //   182: aload #9
    //   184: invokevirtual addView : (Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    //   187: aload_2
    //   188: ifnull -> 195
    //   191: iload_3
    //   192: ifne -> 199
    //   195: aload #11
    //   197: astore #8
    //   199: aload_0
    //   200: getfield mConstructorArgs : [Ljava/lang/Object;
    //   203: iconst_0
    //   204: aload #7
    //   206: aastore
    //   207: aload_0
    //   208: getfield mConstructorArgs : [Ljava/lang/Object;
    //   211: iconst_1
    //   212: aconst_null
    //   213: aastore
    //   214: ldc2_w 8
    //   217: invokestatic traceEnd : (J)V
    //   220: aload #4
    //   222: monitorexit
    //   223: aload #8
    //   225: areturn
    //   226: astore_1
    //   227: goto -> 251
    //   230: astore_1
    //   231: goto -> 315
    //   234: astore_1
    //   235: goto -> 338
    //   238: astore_1
    //   239: goto -> 251
    //   242: astore_1
    //   243: goto -> 315
    //   246: astore_1
    //   247: goto -> 338
    //   250: astore_1
    //   251: new android/view/InflateException
    //   254: astore #8
    //   256: new java/lang/StringBuilder
    //   259: astore_2
    //   260: aload_2
    //   261: invokespecial <init> : ()V
    //   264: aload_2
    //   265: aload #5
    //   267: aload #6
    //   269: invokestatic getParserStateDescription : (Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
    //   272: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   275: pop
    //   276: aload_2
    //   277: ldc_w ': '
    //   280: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   283: pop
    //   284: aload_2
    //   285: aload_1
    //   286: invokevirtual getMessage : ()Ljava/lang/String;
    //   289: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   292: pop
    //   293: aload #8
    //   295: aload_2
    //   296: invokevirtual toString : ()Ljava/lang/String;
    //   299: aload_1
    //   300: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   303: aload #8
    //   305: getstatic android/view/LayoutInflater.EMPTY_STACK_TRACE : [Ljava/lang/StackTraceElement;
    //   308: invokevirtual setStackTrace : ([Ljava/lang/StackTraceElement;)V
    //   311: aload #8
    //   313: athrow
    //   314: astore_1
    //   315: new android/view/InflateException
    //   318: astore_2
    //   319: aload_2
    //   320: aload_1
    //   321: invokevirtual getMessage : ()Ljava/lang/String;
    //   324: aload_1
    //   325: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   328: aload_2
    //   329: getstatic android/view/LayoutInflater.EMPTY_STACK_TRACE : [Ljava/lang/StackTraceElement;
    //   332: invokevirtual setStackTrace : ([Ljava/lang/StackTraceElement;)V
    //   335: aload_2
    //   336: athrow
    //   337: astore_1
    //   338: aload_0
    //   339: getfield mConstructorArgs : [Ljava/lang/Object;
    //   342: iconst_0
    //   343: aload #7
    //   345: aastore
    //   346: aload_0
    //   347: getfield mConstructorArgs : [Ljava/lang/Object;
    //   350: iconst_1
    //   351: aconst_null
    //   352: aastore
    //   353: ldc2_w 8
    //   356: invokestatic traceEnd : (J)V
    //   359: aload_1
    //   360: athrow
    //   361: astore_1
    //   362: aload #4
    //   364: monitorexit
    //   365: aload_1
    //   366: athrow
    //   367: astore_1
    //   368: goto -> 362
    // Line number table:
    //   Java source line number -> byte code offset
    //   #628	-> 0
    //   #629	-> 9
    //   #631	-> 18
    //   #632	-> 24
    //   #633	-> 30
    //   #634	-> 41
    //   #635	-> 49
    //   #638	-> 52
    //   #639	-> 57
    //   #648	-> 65
    //   #649	-> 79
    //   #654	-> 87
    //   #649	-> 101
    //   #650	-> 101
    //   #657	-> 114
    //   #659	-> 127
    //   #661	-> 130
    //   #667	-> 134
    //   #668	-> 142
    //   #671	-> 150
    //   #680	-> 161
    //   #688	-> 171
    //   #689	-> 179
    //   #694	-> 187
    //   #695	-> 195
    //   #711	-> 199
    //   #712	-> 207
    //   #714	-> 214
    //   #715	-> 220
    //   #717	-> 220
    //   #703	-> 226
    //   #699	-> 230
    //   #711	-> 234
    //   #703	-> 238
    //   #699	-> 242
    //   #711	-> 246
    //   #703	-> 250
    //   #704	-> 251
    //   #705	-> 264
    //   #706	-> 284
    //   #707	-> 303
    //   #708	-> 311
    //   #699	-> 314
    //   #700	-> 315
    //   #701	-> 328
    //   #702	-> 335
    //   #711	-> 337
    //   #712	-> 346
    //   #714	-> 353
    //   #715	-> 359
    //   #718	-> 361
    // Exception table:
    //   from	to	target	type
    //   9	18	361	finally
    //   18	24	361	finally
    //   24	30	361	finally
    //   30	41	361	finally
    //   41	49	361	finally
    //   52	57	314	org/xmlpull/v1/XmlPullParserException
    //   52	57	250	java/lang/Exception
    //   52	57	246	finally
    //   57	65	314	org/xmlpull/v1/XmlPullParserException
    //   57	65	250	java/lang/Exception
    //   57	65	246	finally
    //   65	74	314	org/xmlpull/v1/XmlPullParserException
    //   65	74	250	java/lang/Exception
    //   65	74	246	finally
    //   87	98	242	org/xmlpull/v1/XmlPullParserException
    //   87	98	238	java/lang/Exception
    //   87	98	234	finally
    //   101	114	242	org/xmlpull/v1/XmlPullParserException
    //   101	114	238	java/lang/Exception
    //   101	114	234	finally
    //   114	127	242	org/xmlpull/v1/XmlPullParserException
    //   114	127	238	java/lang/Exception
    //   114	127	234	finally
    //   134	142	242	org/xmlpull/v1/XmlPullParserException
    //   134	142	238	java/lang/Exception
    //   134	142	234	finally
    //   150	157	242	org/xmlpull/v1/XmlPullParserException
    //   150	157	238	java/lang/Exception
    //   150	157	234	finally
    //   161	171	230	org/xmlpull/v1/XmlPullParserException
    //   161	171	226	java/lang/Exception
    //   161	171	337	finally
    //   179	187	230	org/xmlpull/v1/XmlPullParserException
    //   179	187	226	java/lang/Exception
    //   179	187	337	finally
    //   199	207	367	finally
    //   207	214	367	finally
    //   214	220	367	finally
    //   220	223	367	finally
    //   251	264	337	finally
    //   264	284	337	finally
    //   284	303	337	finally
    //   303	311	337	finally
    //   311	314	337	finally
    //   315	328	337	finally
    //   328	335	337	finally
    //   335	337	337	finally
    //   338	346	367	finally
    //   346	353	367	finally
    //   353	359	367	finally
    //   359	361	367	finally
    //   362	365	367	finally
  }
  
  private static String getParserStateDescription(Context paramContext, AttributeSet paramAttributeSet) {
    int i = Resources.getAttributeSetSourceResId(paramAttributeSet);
    if (i == 0)
      return paramAttributeSet.getPositionDescription(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramAttributeSet.getPositionDescription());
    stringBuilder.append(" in ");
    stringBuilder.append(paramContext.getResources().getResourceName(i));
    return stringBuilder.toString();
  }
  
  private final boolean verifyClassLoader(Constructor<? extends View> paramConstructor) {
    ClassLoader classLoader2 = paramConstructor.getDeclaringClass().getClassLoader();
    if (classLoader2 == BOOT_CLASS_LOADER)
      return true; 
    ClassLoader classLoader1 = this.mContext.getClassLoader();
    while (true) {
      if (classLoader2 == classLoader1)
        return true; 
      classLoader1 = classLoader1.getParent();
      if (classLoader1 == null)
        return false; 
    } 
  }
  
  public final View createView(String paramString1, String paramString2, AttributeSet paramAttributeSet) throws ClassNotFoundException, InflateException {
    Context context1 = (Context)this.mConstructorArgs[0];
    Context context2 = context1;
    if (context1 == null)
      context2 = this.mContext; 
    return createView(context2, paramString1, paramString2, paramAttributeSet);
  }
  
  public final View createView(Context paramContext, String paramString1, String paramString2, AttributeSet paramAttributeSet) throws ClassNotFoundException, InflateException {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic requireNonNull : (Ljava/lang/Object;)Ljava/lang/Object;
    //   4: pop
    //   5: aload_2
    //   6: invokestatic requireNonNull : (Ljava/lang/Object;)Ljava/lang/Object;
    //   9: pop
    //   10: getstatic android/view/LayoutInflater.sConstructorMap : Ljava/util/HashMap;
    //   13: aload_2
    //   14: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   17: checkcast java/lang/reflect/Constructor
    //   20: astore #5
    //   22: aload #5
    //   24: astore #6
    //   26: aload #5
    //   28: ifnull -> 55
    //   31: aload #5
    //   33: astore #6
    //   35: aload_0
    //   36: aload #5
    //   38: invokespecial verifyClassLoader : (Ljava/lang/reflect/Constructor;)Z
    //   41: ifne -> 55
    //   44: aconst_null
    //   45: astore #6
    //   47: getstatic android/view/LayoutInflater.sConstructorMap : Ljava/util/HashMap;
    //   50: aload_2
    //   51: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   54: pop
    //   55: aconst_null
    //   56: astore #7
    //   58: aconst_null
    //   59: astore #8
    //   61: aconst_null
    //   62: astore #9
    //   64: aload #8
    //   66: astore #5
    //   68: ldc2_w 8
    //   71: aload_2
    //   72: invokestatic traceBegin : (JLjava/lang/String;)V
    //   75: aload #6
    //   77: ifnonnull -> 282
    //   80: aload_3
    //   81: ifnull -> 138
    //   84: aload #8
    //   86: astore #5
    //   88: new java/lang/StringBuilder
    //   91: astore #10
    //   93: aload #8
    //   95: astore #5
    //   97: aload #10
    //   99: invokespecial <init> : ()V
    //   102: aload #8
    //   104: astore #5
    //   106: aload #10
    //   108: aload_3
    //   109: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   112: pop
    //   113: aload #8
    //   115: astore #5
    //   117: aload #10
    //   119: aload_2
    //   120: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   123: pop
    //   124: aload #8
    //   126: astore #5
    //   128: aload #10
    //   130: invokevirtual toString : ()Ljava/lang/String;
    //   133: astore #10
    //   135: goto -> 141
    //   138: aload_2
    //   139: astore #10
    //   141: aload #8
    //   143: astore #5
    //   145: aload_0
    //   146: getfield mContext : Landroid/content/Context;
    //   149: astore #6
    //   151: aload #8
    //   153: astore #5
    //   155: aload #6
    //   157: invokevirtual getClassLoader : ()Ljava/lang/ClassLoader;
    //   160: astore #6
    //   162: aload #8
    //   164: astore #5
    //   166: aload #10
    //   168: iconst_0
    //   169: aload #6
    //   171: invokestatic forName : (Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    //   174: astore #10
    //   176: aload #8
    //   178: astore #5
    //   180: aload #10
    //   182: ldc_w android/view/View
    //   185: invokevirtual asSubclass : (Ljava/lang/Class;)Ljava/lang/Class;
    //   188: astore #10
    //   190: aload #10
    //   192: astore #5
    //   194: aload_0
    //   195: getfield mFilter : Landroid/view/LayoutInflater$Filter;
    //   198: ifnull -> 241
    //   201: aload #10
    //   203: ifnull -> 241
    //   206: aload #10
    //   208: astore #5
    //   210: aload_0
    //   211: getfield mFilter : Landroid/view/LayoutInflater$Filter;
    //   214: aload #10
    //   216: invokeinterface onLoadClass : (Ljava/lang/Class;)Z
    //   221: istore #11
    //   223: iload #11
    //   225: ifne -> 241
    //   228: aload #10
    //   230: astore #5
    //   232: aload_0
    //   233: aload_2
    //   234: aload_3
    //   235: aload_1
    //   236: aload #4
    //   238: invokespecial failNotAllowed : (Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)V
    //   241: aload #10
    //   243: astore #5
    //   245: aload #10
    //   247: getstatic android/view/LayoutInflater.mConstructorSignature : [Ljava/lang/Class;
    //   250: invokevirtual getConstructor : ([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    //   253: astore #12
    //   255: aload #10
    //   257: astore #5
    //   259: aload #12
    //   261: iconst_1
    //   262: invokevirtual setAccessible : (Z)V
    //   265: aload #10
    //   267: astore #5
    //   269: getstatic android/view/LayoutInflater.sConstructorMap : Ljava/util/HashMap;
    //   272: aload_2
    //   273: aload #12
    //   275: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   278: pop
    //   279: goto -> 556
    //   282: aload #6
    //   284: astore #12
    //   286: aload #7
    //   288: astore #10
    //   290: aload #8
    //   292: astore #5
    //   294: aload_0
    //   295: getfield mFilter : Landroid/view/LayoutInflater$Filter;
    //   298: ifnull -> 556
    //   301: aload #8
    //   303: astore #5
    //   305: aload_0
    //   306: getfield mFilterMap : Ljava/util/HashMap;
    //   309: aload_2
    //   310: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   313: checkcast java/lang/Boolean
    //   316: astore #12
    //   318: aload #12
    //   320: ifnonnull -> 516
    //   323: aload_3
    //   324: ifnull -> 381
    //   327: aload #8
    //   329: astore #5
    //   331: new java/lang/StringBuilder
    //   334: astore #10
    //   336: aload #8
    //   338: astore #5
    //   340: aload #10
    //   342: invokespecial <init> : ()V
    //   345: aload #8
    //   347: astore #5
    //   349: aload #10
    //   351: aload_3
    //   352: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   355: pop
    //   356: aload #8
    //   358: astore #5
    //   360: aload #10
    //   362: aload_2
    //   363: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   366: pop
    //   367: aload #8
    //   369: astore #5
    //   371: aload #10
    //   373: invokevirtual toString : ()Ljava/lang/String;
    //   376: astore #10
    //   378: goto -> 384
    //   381: aload_2
    //   382: astore #10
    //   384: aload #8
    //   386: astore #5
    //   388: aload_0
    //   389: getfield mContext : Landroid/content/Context;
    //   392: astore #12
    //   394: aload #8
    //   396: astore #5
    //   398: aload #12
    //   400: invokevirtual getClassLoader : ()Ljava/lang/ClassLoader;
    //   403: astore #12
    //   405: aload #8
    //   407: astore #5
    //   409: aload #10
    //   411: iconst_0
    //   412: aload #12
    //   414: invokestatic forName : (Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    //   417: astore #10
    //   419: aload #8
    //   421: astore #5
    //   423: aload #10
    //   425: ldc_w android/view/View
    //   428: invokevirtual asSubclass : (Ljava/lang/Class;)Ljava/lang/Class;
    //   431: astore #12
    //   433: aload #12
    //   435: ifnull -> 462
    //   438: aload #12
    //   440: astore #5
    //   442: aload_0
    //   443: getfield mFilter : Landroid/view/LayoutInflater$Filter;
    //   446: aload #12
    //   448: invokeinterface onLoadClass : (Ljava/lang/Class;)Z
    //   453: ifeq -> 462
    //   456: iconst_1
    //   457: istore #11
    //   459: goto -> 465
    //   462: iconst_0
    //   463: istore #11
    //   465: aload #12
    //   467: astore #5
    //   469: aload_0
    //   470: getfield mFilterMap : Ljava/util/HashMap;
    //   473: aload_2
    //   474: iload #11
    //   476: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   479: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   482: pop
    //   483: aload #12
    //   485: astore #10
    //   487: iload #11
    //   489: ifne -> 509
    //   492: aload #12
    //   494: astore #5
    //   496: aload_0
    //   497: aload_2
    //   498: aload_3
    //   499: aload_1
    //   500: aload #4
    //   502: invokespecial failNotAllowed : (Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)V
    //   505: aload #12
    //   507: astore #10
    //   509: aload #6
    //   511: astore #12
    //   513: goto -> 556
    //   516: aload #9
    //   518: astore #10
    //   520: aload #8
    //   522: astore #5
    //   524: aload #12
    //   526: getstatic java/lang/Boolean.FALSE : Ljava/lang/Boolean;
    //   529: invokevirtual equals : (Ljava/lang/Object;)Z
    //   532: ifeq -> 509
    //   535: aload #8
    //   537: astore #5
    //   539: aload_0
    //   540: aload_2
    //   541: aload_3
    //   542: aload_1
    //   543: aload #4
    //   545: invokespecial failNotAllowed : (Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)V
    //   548: aload #7
    //   550: astore #10
    //   552: aload #6
    //   554: astore #12
    //   556: aload #10
    //   558: astore #5
    //   560: aload_0
    //   561: getfield mConstructorArgs : [Ljava/lang/Object;
    //   564: iconst_0
    //   565: aaload
    //   566: astore #6
    //   568: aload #10
    //   570: astore #5
    //   572: aload_0
    //   573: getfield mConstructorArgs : [Ljava/lang/Object;
    //   576: iconst_0
    //   577: aload_1
    //   578: aastore
    //   579: aload #10
    //   581: astore #5
    //   583: aload_0
    //   584: getfield mConstructorArgs : [Ljava/lang/Object;
    //   587: astore #8
    //   589: aload #8
    //   591: iconst_1
    //   592: aload #4
    //   594: aastore
    //   595: aload #12
    //   597: aload #8
    //   599: invokevirtual newInstance : ([Ljava/lang/Object;)Ljava/lang/Object;
    //   602: checkcast android/view/View
    //   605: astore #12
    //   607: aload #12
    //   609: instanceof android/view/ViewStub
    //   612: ifeq -> 638
    //   615: aload #12
    //   617: checkcast android/view/ViewStub
    //   620: astore #5
    //   622: aload #5
    //   624: aload_0
    //   625: aload #8
    //   627: iconst_0
    //   628: aaload
    //   629: checkcast android/content/Context
    //   632: invokevirtual cloneInContext : (Landroid/content/Context;)Landroid/view/LayoutInflater;
    //   635: invokevirtual setLayoutInflater : (Landroid/view/LayoutInflater;)V
    //   638: aload #10
    //   640: astore #5
    //   642: aload_0
    //   643: getfield mConstructorArgs : [Ljava/lang/Object;
    //   646: iconst_0
    //   647: aload #6
    //   649: aastore
    //   650: ldc2_w 8
    //   653: invokestatic traceEnd : (J)V
    //   656: aload #12
    //   658: areturn
    //   659: astore #12
    //   661: aload #10
    //   663: astore #5
    //   665: aload_0
    //   666: getfield mConstructorArgs : [Ljava/lang/Object;
    //   669: iconst_0
    //   670: aload #6
    //   672: aastore
    //   673: aload #10
    //   675: astore #5
    //   677: aload #12
    //   679: athrow
    //   680: astore_1
    //   681: goto -> 967
    //   684: astore_2
    //   685: new android/view/InflateException
    //   688: astore #10
    //   690: new java/lang/StringBuilder
    //   693: astore_3
    //   694: aload_3
    //   695: invokespecial <init> : ()V
    //   698: aload_3
    //   699: aload_1
    //   700: aload #4
    //   702: invokestatic getParserStateDescription : (Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
    //   705: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   708: pop
    //   709: aload_3
    //   710: ldc_w ': Error inflating class '
    //   713: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   716: pop
    //   717: aload #5
    //   719: ifnonnull -> 729
    //   722: ldc_w '<unknown>'
    //   725: astore_1
    //   726: goto -> 735
    //   729: aload #5
    //   731: invokevirtual getName : ()Ljava/lang/String;
    //   734: astore_1
    //   735: aload_3
    //   736: aload_1
    //   737: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   740: pop
    //   741: aload #10
    //   743: aload_3
    //   744: invokevirtual toString : ()Ljava/lang/String;
    //   747: aload_2
    //   748: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   751: aload #10
    //   753: getstatic android/view/LayoutInflater.EMPTY_STACK_TRACE : [Ljava/lang/StackTraceElement;
    //   756: invokevirtual setStackTrace : ([Ljava/lang/StackTraceElement;)V
    //   759: aload #10
    //   761: athrow
    //   762: astore_1
    //   763: aload_1
    //   764: athrow
    //   765: astore #6
    //   767: new android/view/InflateException
    //   770: astore #10
    //   772: new java/lang/StringBuilder
    //   775: astore #5
    //   777: aload #5
    //   779: invokespecial <init> : ()V
    //   782: aload #5
    //   784: aload_1
    //   785: aload #4
    //   787: invokestatic getParserStateDescription : (Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
    //   790: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   793: pop
    //   794: aload #5
    //   796: ldc_w ': Class is not a View '
    //   799: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   802: pop
    //   803: aload_3
    //   804: ifnull -> 835
    //   807: new java/lang/StringBuilder
    //   810: astore_1
    //   811: aload_1
    //   812: invokespecial <init> : ()V
    //   815: aload_1
    //   816: aload_3
    //   817: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   820: pop
    //   821: aload_1
    //   822: aload_2
    //   823: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   826: pop
    //   827: aload_1
    //   828: invokevirtual toString : ()Ljava/lang/String;
    //   831: astore_2
    //   832: goto -> 835
    //   835: aload #5
    //   837: aload_2
    //   838: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   841: pop
    //   842: aload #10
    //   844: aload #5
    //   846: invokevirtual toString : ()Ljava/lang/String;
    //   849: aload #6
    //   851: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   854: aload #10
    //   856: getstatic android/view/LayoutInflater.EMPTY_STACK_TRACE : [Ljava/lang/StackTraceElement;
    //   859: invokevirtual setStackTrace : ([Ljava/lang/StackTraceElement;)V
    //   862: aload #10
    //   864: athrow
    //   865: astore #5
    //   867: new android/view/InflateException
    //   870: astore #10
    //   872: new java/lang/StringBuilder
    //   875: astore #6
    //   877: aload #6
    //   879: invokespecial <init> : ()V
    //   882: aload #6
    //   884: aload_1
    //   885: aload #4
    //   887: invokestatic getParserStateDescription : (Landroid/content/Context;Landroid/util/AttributeSet;)Ljava/lang/String;
    //   890: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   893: pop
    //   894: aload #6
    //   896: ldc_w ': Error inflating class '
    //   899: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   902: pop
    //   903: aload_3
    //   904: ifnull -> 935
    //   907: new java/lang/StringBuilder
    //   910: astore_1
    //   911: aload_1
    //   912: invokespecial <init> : ()V
    //   915: aload_1
    //   916: aload_3
    //   917: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   920: pop
    //   921: aload_1
    //   922: aload_2
    //   923: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   926: pop
    //   927: aload_1
    //   928: invokevirtual toString : ()Ljava/lang/String;
    //   931: astore_1
    //   932: goto -> 937
    //   935: aload_2
    //   936: astore_1
    //   937: aload #6
    //   939: aload_1
    //   940: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   943: pop
    //   944: aload #10
    //   946: aload #6
    //   948: invokevirtual toString : ()Ljava/lang/String;
    //   951: aload #5
    //   953: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   956: aload #10
    //   958: getstatic android/view/LayoutInflater.EMPTY_STACK_TRACE : [Ljava/lang/StackTraceElement;
    //   961: invokevirtual setStackTrace : ([Ljava/lang/StackTraceElement;)V
    //   964: aload #10
    //   966: athrow
    //   967: ldc2_w 8
    //   970: invokestatic traceEnd : (J)V
    //   973: aload_1
    //   974: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #799	-> 0
    //   #800	-> 5
    //   #801	-> 10
    //   #802	-> 22
    //   #803	-> 44
    //   #804	-> 47
    //   #806	-> 55
    //   #809	-> 64
    //   #811	-> 75
    //   #813	-> 80
    //   #814	-> 151
    //   #813	-> 162
    //   #814	-> 176
    //   #816	-> 190
    //   #817	-> 206
    //   #818	-> 223
    //   #819	-> 228
    //   #822	-> 241
    //   #823	-> 255
    //   #824	-> 265
    //   #827	-> 282
    //   #829	-> 301
    //   #830	-> 318
    //   #832	-> 323
    //   #833	-> 394
    //   #832	-> 405
    //   #833	-> 419
    //   #835	-> 433
    //   #836	-> 465
    //   #837	-> 483
    //   #838	-> 492
    //   #840	-> 509
    //   #841	-> 535
    //   #846	-> 556
    //   #847	-> 568
    //   #848	-> 579
    //   #849	-> 589
    //   #852	-> 595
    //   #853	-> 607
    //   #855	-> 615
    //   #856	-> 622
    //   #858	-> 638
    //   #860	-> 638
    //   #886	-> 650
    //   #858	-> 656
    //   #860	-> 659
    //   #861	-> 673
    //   #886	-> 680
    //   #879	-> 684
    //   #880	-> 685
    //   #881	-> 698
    //   #882	-> 717
    //   #883	-> 751
    //   #884	-> 759
    //   #876	-> 762
    //   #878	-> 763
    //   #869	-> 765
    //   #871	-> 767
    //   #872	-> 782
    //   #873	-> 803
    //   #874	-> 854
    //   #875	-> 862
    //   #862	-> 865
    //   #863	-> 867
    //   #864	-> 882
    //   #865	-> 903
    //   #866	-> 956
    //   #867	-> 964
    //   #886	-> 967
    //   #887	-> 973
    // Exception table:
    //   from	to	target	type
    //   68	75	865	java/lang/NoSuchMethodException
    //   68	75	765	java/lang/ClassCastException
    //   68	75	762	java/lang/ClassNotFoundException
    //   68	75	684	java/lang/Exception
    //   68	75	680	finally
    //   88	93	865	java/lang/NoSuchMethodException
    //   88	93	765	java/lang/ClassCastException
    //   88	93	762	java/lang/ClassNotFoundException
    //   88	93	684	java/lang/Exception
    //   88	93	680	finally
    //   97	102	865	java/lang/NoSuchMethodException
    //   97	102	765	java/lang/ClassCastException
    //   97	102	762	java/lang/ClassNotFoundException
    //   97	102	684	java/lang/Exception
    //   97	102	680	finally
    //   106	113	865	java/lang/NoSuchMethodException
    //   106	113	765	java/lang/ClassCastException
    //   106	113	762	java/lang/ClassNotFoundException
    //   106	113	684	java/lang/Exception
    //   106	113	680	finally
    //   117	124	865	java/lang/NoSuchMethodException
    //   117	124	765	java/lang/ClassCastException
    //   117	124	762	java/lang/ClassNotFoundException
    //   117	124	684	java/lang/Exception
    //   117	124	680	finally
    //   128	135	865	java/lang/NoSuchMethodException
    //   128	135	765	java/lang/ClassCastException
    //   128	135	762	java/lang/ClassNotFoundException
    //   128	135	684	java/lang/Exception
    //   128	135	680	finally
    //   145	151	865	java/lang/NoSuchMethodException
    //   145	151	765	java/lang/ClassCastException
    //   145	151	762	java/lang/ClassNotFoundException
    //   145	151	684	java/lang/Exception
    //   145	151	680	finally
    //   155	162	865	java/lang/NoSuchMethodException
    //   155	162	765	java/lang/ClassCastException
    //   155	162	762	java/lang/ClassNotFoundException
    //   155	162	684	java/lang/Exception
    //   155	162	680	finally
    //   166	176	865	java/lang/NoSuchMethodException
    //   166	176	765	java/lang/ClassCastException
    //   166	176	762	java/lang/ClassNotFoundException
    //   166	176	684	java/lang/Exception
    //   166	176	680	finally
    //   180	190	865	java/lang/NoSuchMethodException
    //   180	190	765	java/lang/ClassCastException
    //   180	190	762	java/lang/ClassNotFoundException
    //   180	190	684	java/lang/Exception
    //   180	190	680	finally
    //   194	201	865	java/lang/NoSuchMethodException
    //   194	201	765	java/lang/ClassCastException
    //   194	201	762	java/lang/ClassNotFoundException
    //   194	201	684	java/lang/Exception
    //   194	201	680	finally
    //   210	223	865	java/lang/NoSuchMethodException
    //   210	223	765	java/lang/ClassCastException
    //   210	223	762	java/lang/ClassNotFoundException
    //   210	223	684	java/lang/Exception
    //   210	223	680	finally
    //   232	241	865	java/lang/NoSuchMethodException
    //   232	241	765	java/lang/ClassCastException
    //   232	241	762	java/lang/ClassNotFoundException
    //   232	241	684	java/lang/Exception
    //   232	241	680	finally
    //   245	255	865	java/lang/NoSuchMethodException
    //   245	255	765	java/lang/ClassCastException
    //   245	255	762	java/lang/ClassNotFoundException
    //   245	255	684	java/lang/Exception
    //   245	255	680	finally
    //   259	265	865	java/lang/NoSuchMethodException
    //   259	265	765	java/lang/ClassCastException
    //   259	265	762	java/lang/ClassNotFoundException
    //   259	265	684	java/lang/Exception
    //   259	265	680	finally
    //   269	279	865	java/lang/NoSuchMethodException
    //   269	279	765	java/lang/ClassCastException
    //   269	279	762	java/lang/ClassNotFoundException
    //   269	279	684	java/lang/Exception
    //   269	279	680	finally
    //   294	301	865	java/lang/NoSuchMethodException
    //   294	301	765	java/lang/ClassCastException
    //   294	301	762	java/lang/ClassNotFoundException
    //   294	301	684	java/lang/Exception
    //   294	301	680	finally
    //   305	318	865	java/lang/NoSuchMethodException
    //   305	318	765	java/lang/ClassCastException
    //   305	318	762	java/lang/ClassNotFoundException
    //   305	318	684	java/lang/Exception
    //   305	318	680	finally
    //   331	336	865	java/lang/NoSuchMethodException
    //   331	336	765	java/lang/ClassCastException
    //   331	336	762	java/lang/ClassNotFoundException
    //   331	336	684	java/lang/Exception
    //   331	336	680	finally
    //   340	345	865	java/lang/NoSuchMethodException
    //   340	345	765	java/lang/ClassCastException
    //   340	345	762	java/lang/ClassNotFoundException
    //   340	345	684	java/lang/Exception
    //   340	345	680	finally
    //   349	356	865	java/lang/NoSuchMethodException
    //   349	356	765	java/lang/ClassCastException
    //   349	356	762	java/lang/ClassNotFoundException
    //   349	356	684	java/lang/Exception
    //   349	356	680	finally
    //   360	367	865	java/lang/NoSuchMethodException
    //   360	367	765	java/lang/ClassCastException
    //   360	367	762	java/lang/ClassNotFoundException
    //   360	367	684	java/lang/Exception
    //   360	367	680	finally
    //   371	378	865	java/lang/NoSuchMethodException
    //   371	378	765	java/lang/ClassCastException
    //   371	378	762	java/lang/ClassNotFoundException
    //   371	378	684	java/lang/Exception
    //   371	378	680	finally
    //   388	394	865	java/lang/NoSuchMethodException
    //   388	394	765	java/lang/ClassCastException
    //   388	394	762	java/lang/ClassNotFoundException
    //   388	394	684	java/lang/Exception
    //   388	394	680	finally
    //   398	405	865	java/lang/NoSuchMethodException
    //   398	405	765	java/lang/ClassCastException
    //   398	405	762	java/lang/ClassNotFoundException
    //   398	405	684	java/lang/Exception
    //   398	405	680	finally
    //   409	419	865	java/lang/NoSuchMethodException
    //   409	419	765	java/lang/ClassCastException
    //   409	419	762	java/lang/ClassNotFoundException
    //   409	419	684	java/lang/Exception
    //   409	419	680	finally
    //   423	433	865	java/lang/NoSuchMethodException
    //   423	433	765	java/lang/ClassCastException
    //   423	433	762	java/lang/ClassNotFoundException
    //   423	433	684	java/lang/Exception
    //   423	433	680	finally
    //   442	456	865	java/lang/NoSuchMethodException
    //   442	456	765	java/lang/ClassCastException
    //   442	456	762	java/lang/ClassNotFoundException
    //   442	456	684	java/lang/Exception
    //   442	456	680	finally
    //   469	483	865	java/lang/NoSuchMethodException
    //   469	483	765	java/lang/ClassCastException
    //   469	483	762	java/lang/ClassNotFoundException
    //   469	483	684	java/lang/Exception
    //   469	483	680	finally
    //   496	505	865	java/lang/NoSuchMethodException
    //   496	505	765	java/lang/ClassCastException
    //   496	505	762	java/lang/ClassNotFoundException
    //   496	505	684	java/lang/Exception
    //   496	505	680	finally
    //   524	535	865	java/lang/NoSuchMethodException
    //   524	535	765	java/lang/ClassCastException
    //   524	535	762	java/lang/ClassNotFoundException
    //   524	535	684	java/lang/Exception
    //   524	535	680	finally
    //   539	548	865	java/lang/NoSuchMethodException
    //   539	548	765	java/lang/ClassCastException
    //   539	548	762	java/lang/ClassNotFoundException
    //   539	548	684	java/lang/Exception
    //   539	548	680	finally
    //   560	568	865	java/lang/NoSuchMethodException
    //   560	568	765	java/lang/ClassCastException
    //   560	568	762	java/lang/ClassNotFoundException
    //   560	568	684	java/lang/Exception
    //   560	568	680	finally
    //   572	579	865	java/lang/NoSuchMethodException
    //   572	579	765	java/lang/ClassCastException
    //   572	579	762	java/lang/ClassNotFoundException
    //   572	579	684	java/lang/Exception
    //   572	579	680	finally
    //   583	589	865	java/lang/NoSuchMethodException
    //   583	589	765	java/lang/ClassCastException
    //   583	589	762	java/lang/ClassNotFoundException
    //   583	589	684	java/lang/Exception
    //   583	589	680	finally
    //   595	607	659	finally
    //   607	615	659	finally
    //   615	622	659	finally
    //   622	638	659	finally
    //   642	650	865	java/lang/NoSuchMethodException
    //   642	650	765	java/lang/ClassCastException
    //   642	650	762	java/lang/ClassNotFoundException
    //   642	650	684	java/lang/Exception
    //   642	650	680	finally
    //   665	673	865	java/lang/NoSuchMethodException
    //   665	673	765	java/lang/ClassCastException
    //   665	673	762	java/lang/ClassNotFoundException
    //   665	673	684	java/lang/Exception
    //   665	673	680	finally
    //   677	680	865	java/lang/NoSuchMethodException
    //   677	680	765	java/lang/ClassCastException
    //   677	680	762	java/lang/ClassNotFoundException
    //   677	680	684	java/lang/Exception
    //   677	680	680	finally
    //   685	698	680	finally
    //   698	717	680	finally
    //   729	735	680	finally
    //   735	751	680	finally
    //   751	759	680	finally
    //   759	762	680	finally
    //   763	765	680	finally
    //   767	782	680	finally
    //   782	803	680	finally
    //   807	832	680	finally
    //   835	854	680	finally
    //   854	862	680	finally
    //   862	865	680	finally
    //   867	882	680	finally
    //   882	903	680	finally
    //   907	932	680	finally
    //   937	956	680	finally
    //   956	964	680	finally
    //   964	967	680	finally
  }
  
  private void failNotAllowed(String paramString1, String paramString2, Context paramContext, AttributeSet paramAttributeSet) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getParserStateDescription(paramContext, paramAttributeSet));
    stringBuilder.append(": Class not allowed to be inflated ");
    if (paramString2 != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString2);
      stringBuilder1.append(paramString1);
      paramString1 = stringBuilder1.toString();
    } 
    stringBuilder.append(paramString1);
    throw new InflateException(stringBuilder.toString());
  }
  
  protected View onCreateView(String paramString, AttributeSet paramAttributeSet) throws ClassNotFoundException {
    return createView(paramString, "android.view.", paramAttributeSet);
  }
  
  protected View onCreateView(View paramView, String paramString, AttributeSet paramAttributeSet) throws ClassNotFoundException {
    return onCreateView(paramString, paramAttributeSet);
  }
  
  public View onCreateView(Context paramContext, View paramView, String paramString, AttributeSet paramAttributeSet) throws ClassNotFoundException {
    return onCreateView(paramView, paramString, paramAttributeSet);
  }
  
  private View createViewFromTag(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet) {
    return createViewFromTag(paramView, paramString, paramContext, paramAttributeSet, false);
  }
  
  View createViewFromTag(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet, boolean paramBoolean) {
    ContextThemeWrapper contextThemeWrapper;
    String str = paramString;
    if (paramString.equals("view"))
      str = paramAttributeSet.getAttributeValue(null, "class"); 
    Context context = paramContext;
    if (!paramBoolean) {
      TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, ATTRS_THEME);
      int i = typedArray.getResourceId(0, 0);
      context = paramContext;
      if (i != 0)
        contextThemeWrapper = new ContextThemeWrapper(paramContext, i); 
      typedArray.recycle();
    } 
    try {
      View view = tryCreateView(paramView, str, (Context)contextThemeWrapper, paramAttributeSet);
      Object object = view;
      if (view == null) {
        object = this.mConstructorArgs[0];
        this.mConstructorArgs[0] = contextThemeWrapper;
        try {
          if (-1 == str.indexOf('.')) {
            paramView = onCreateView((Context)contextThemeWrapper, paramView, str, paramAttributeSet);
          } else {
            paramView = createView((Context)contextThemeWrapper, str, null, paramAttributeSet);
          } 
          this.mConstructorArgs[0] = object;
        } finally {
          this.mConstructorArgs[0] = object;
        } 
      } 
      return (View)object;
    } catch (InflateException inflateException) {
      throw inflateException;
    } catch (ClassNotFoundException classNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getParserStateDescription((Context)contextThemeWrapper, paramAttributeSet));
      stringBuilder.append(": Error inflating class ");
      stringBuilder.append(str);
      InflateException inflateException = new InflateException(stringBuilder.toString(), classNotFoundException);
      inflateException.setStackTrace(EMPTY_STACK_TRACE);
      throw inflateException;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getParserStateDescription((Context)contextThemeWrapper, paramAttributeSet));
      stringBuilder.append(": Error inflating class ");
      stringBuilder.append(str);
      InflateException inflateException = new InflateException(stringBuilder.toString(), exception);
      inflateException.setStackTrace(EMPTY_STACK_TRACE);
      throw inflateException;
    } 
  }
  
  public final View tryCreateView(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet) {
    View view;
    if (paramString.equals("blink"))
      return new BlinkLayout(paramContext, paramAttributeSet); 
    Factory factory1 = this.mFactory2;
    if (factory1 != null) {
      View view1 = factory1.onCreateView(paramView, paramString, paramContext, paramAttributeSet);
    } else {
      factory1 = this.mFactory;
      if (factory1 != null) {
        View view1 = factory1.onCreateView(paramString, paramContext, paramAttributeSet);
      } else {
        factory1 = null;
      } 
    } 
    Factory factory2 = factory1;
    if (factory1 == null) {
      Factory2 factory21 = this.mPrivateFactory;
      factory2 = factory1;
      if (factory21 != null)
        view = factory21.onCreateView(paramView, paramString, paramContext, paramAttributeSet); 
    } 
    return view;
  }
  
  final void rInflateChildren(XmlPullParser paramXmlPullParser, View paramView, AttributeSet paramAttributeSet, boolean paramBoolean) throws XmlPullParserException, IOException {
    rInflate(paramXmlPullParser, paramView, paramView.getContext(), paramAttributeSet, paramBoolean);
  }
  
  void rInflate(XmlPullParser paramXmlPullParser, View paramView, Context paramContext, AttributeSet paramAttributeSet, boolean paramBoolean) throws XmlPullParserException, IOException {
    int i = paramXmlPullParser.getDepth();
    boolean bool = false;
    while (true) {
      int j = paramXmlPullParser.next();
      if ((j != 3 || 
        paramXmlPullParser.getDepth() > i) && j != 1) {
        if (j != 2)
          continue; 
        String str = paramXmlPullParser.getName();
        if ("requestFocus".equals(str)) {
          bool = true;
          consumeChildElements(paramXmlPullParser);
          continue;
        } 
        if ("tag".equals(str)) {
          parseViewTag(paramXmlPullParser, paramView, paramAttributeSet);
          continue;
        } 
        if ("include".equals(str)) {
          if (paramXmlPullParser.getDepth() != 0) {
            parseInclude(paramXmlPullParser, paramContext, paramView, paramAttributeSet);
            continue;
          } 
          throw new InflateException("<include /> cannot be the root element");
        } 
        if (!"merge".equals(str)) {
          View view = createViewFromTag(paramView, str, paramContext, paramAttributeSet);
          ViewGroup viewGroup = (ViewGroup)paramView;
          ViewGroup.LayoutParams layoutParams = viewGroup.generateLayoutParams(paramAttributeSet);
          rInflateChildren(paramXmlPullParser, view, paramAttributeSet, true);
          viewGroup.addView(view, layoutParams);
          continue;
        } 
        throw new InflateException("<merge /> must be the root element");
      } 
      break;
    } 
    if (bool)
      paramView.restoreDefaultFocus(); 
    if (paramBoolean)
      paramView.onFinishInflate(); 
  }
  
  private void parseViewTag(XmlPullParser paramXmlPullParser, View paramView, AttributeSet paramAttributeSet) throws XmlPullParserException, IOException {
    Context context = paramView.getContext();
    TypedArray typedArray = context.obtainStyledAttributes(paramAttributeSet, R.styleable.ViewTag);
    int i = typedArray.getResourceId(1, 0);
    CharSequence charSequence = typedArray.getText(0);
    paramView.setTag(i, charSequence);
    typedArray.recycle();
    consumeChildElements(paramXmlPullParser);
  }
  
  private void parseInclude(XmlPullParser paramXmlPullParser, Context paramContext, View paramView, AttributeSet paramAttributeSet) throws XmlPullParserException, IOException {
    String str;
    if (paramView instanceof ViewGroup) {
      ContextThemeWrapper contextThemeWrapper;
      boolean bool;
      TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, ATTRS_THEME);
      int i = typedArray.getResourceId(0, 0);
      if (i != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool)
        contextThemeWrapper = new ContextThemeWrapper(paramContext, i); 
      typedArray.recycle();
      int j = paramAttributeSet.getAttributeResourceValue(null, "layout", 0);
      i = j;
      if (j == 0) {
        String str1 = paramAttributeSet.getAttributeValue(null, "layout");
        if (str1 != null && str1.length() > 0) {
          Resources resources = contextThemeWrapper.getResources();
          str1 = str1.substring(1);
          String str2 = contextThemeWrapper.getPackageName();
          i = resources.getIdentifier(str1, "attr", str2);
        } else {
          throw new InflateException("You must specify a layout in the include tag: <include layout=\"@layout/layoutID\" />");
        } 
      } 
      if (this.mTempValue == null)
        this.mTempValue = new TypedValue(); 
      if (i != 0 && contextThemeWrapper.getTheme().resolveAttribute(i, this.mTempValue, true))
        i = this.mTempValue.resourceId; 
      if (i != 0) {
        View view = tryInflatePrecompiled(i, contextThemeWrapper.getResources(), (ViewGroup)paramView, true);
        if (view == null) {
          XmlResourceParser xmlResourceParser = contextThemeWrapper.getResources().getLayout(i);
          try {
            ViewGroup.LayoutParams layoutParams;
            AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)xmlResourceParser);
            while (true) {
              i = xmlResourceParser.next();
              if (i != 2 && i != 1)
                continue; 
              break;
            } 
            if (i == 2) {
              String str1 = xmlResourceParser.getName();
              boolean bool1 = "merge".equals(str1);
              if (bool1) {
                try {
                  rInflate((XmlPullParser)xmlResourceParser, paramView, (Context)contextThemeWrapper, attributeSet, false);
                } finally {}
              } else {
                try {
                  View view1 = createViewFromTag(paramView, str1, (Context)contextThemeWrapper, attributeSet, bool);
                  ViewGroup viewGroup = (ViewGroup)paramView;
                  TypedArray typedArray1 = contextThemeWrapper.obtainStyledAttributes(paramAttributeSet, R.styleable.Include);
                  i = typedArray1.getResourceId(0, -1);
                  j = typedArray1.getInt(1, -1);
                  typedArray1.recycle();
                  typedArray1 = null;
                  try {
                    ViewGroup.LayoutParams layoutParams1 = viewGroup.generateLayoutParams(paramAttributeSet);
                  } catch (RuntimeException runtimeException) {
                  
                  } finally {}
                  if (layoutParams == null)
                    layoutParams = viewGroup.generateLayoutParams(attributeSet); 
                  view1.setLayoutParams(layoutParams);
                  try {
                    rInflateChildren((XmlPullParser)xmlResourceParser, view1, attributeSet, true);
                    if (i != -1)
                      view1.setId(i); 
                    if (j != 0) {
                      if (j != 1) {
                        if (j == 2)
                          view1.setVisibility(8); 
                      } else {
                        view1.setVisibility(4);
                      } 
                    } else {
                      view1.setVisibility(0);
                    } 
                    viewGroup.addView(view1);
                    xmlResourceParser.close();
                  } finally {}
                } finally {}
                xmlResourceParser.close();
                throw paramXmlPullParser;
              } 
            } else {
              InflateException inflateException = new InflateException();
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append(getParserStateDescription((Context)layoutParams, attributeSet));
              stringBuilder.append(": No start tag found!");
              this(stringBuilder.toString());
              throw inflateException;
            } 
            xmlResourceParser.close();
          } finally {}
          xmlResourceParser.close();
          throw paramXmlPullParser;
        } 
      } else {
        str = paramAttributeSet.getAttributeValue(null, "layout");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("You must specify a valid layout reference. The layout ID ");
        stringBuilder.append(str);
        stringBuilder.append(" is not valid.");
        throw new InflateException(stringBuilder.toString());
      } 
    } else {
      throw new InflateException("<include /> can only be used inside of a ViewGroup");
    } 
    consumeChildElements((XmlPullParser)str);
  }
  
  static final void consumeChildElements(XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    int i = paramXmlPullParser.getDepth();
    while (true) {
      int j = paramXmlPullParser.next();
      if ((j != 3 || 
        paramXmlPullParser.getDepth() > i) && j != 1)
        continue; 
      break;
    } 
  }
  
  public abstract LayoutInflater cloneInContext(Context paramContext);
  
  class Factory2 implements Factory {
    public abstract View onCreateView(View param1View, String param1String, Context param1Context, AttributeSet param1AttributeSet);
  }
  
  public static interface Factory {
    View onCreateView(String param1String, Context param1Context, AttributeSet param1AttributeSet);
  }
  
  class BlinkLayout extends FrameLayout {
    private static final int BLINK_DELAY = 500;
    
    private static final int MESSAGE_BLINK = 66;
    
    private boolean mBlink;
    
    private boolean mBlinkState;
    
    private final Handler mHandler;
    
    public BlinkLayout(LayoutInflater this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
      this.mHandler = new Handler((Handler.Callback)new Object(this));
    }
    
    private void makeBlink() {
      Message message = this.mHandler.obtainMessage(66);
      this.mHandler.sendMessageDelayed(message, 500L);
    }
    
    protected void onAttachedToWindow() {
      super.onAttachedToWindow();
      this.mBlink = true;
      this.mBlinkState = true;
      makeBlink();
    }
    
    protected void onDetachedFromWindow() {
      super.onDetachedFromWindow();
      this.mBlink = false;
      this.mBlinkState = true;
      this.mHandler.removeMessages(66);
    }
    
    protected void dispatchDraw(Canvas param1Canvas) {
      if (this.mBlinkState)
        super.dispatchDraw(param1Canvas); 
    }
  }
}
