package android.view;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;

public interface IOplusViewConfigHelper extends IOplusCommonFeature {
  public static final IOplusViewConfigHelper DEFAULT = (IOplusViewConfigHelper)new Object();
  
  default IOplusViewConfigHelper getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusViewConfigHelper;
  }
  
  default int getScaledOverscrollDistance(int paramInt) {
    return paramInt;
  }
  
  default int getScaledOverflingDistance(int paramInt) {
    return paramInt;
  }
  
  default int calcRealOverScrollDist(int paramInt1, int paramInt2) {
    return paramInt1;
  }
  
  default int calcRealOverScrollDist(int paramInt1, int paramInt2, int paramInt3) {
    return paramInt1;
  }
}
