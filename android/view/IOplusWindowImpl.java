package android.view;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.oplus.direct.OplusDirectFindCmd;
import com.oplus.view.analysis.OplusWindowNode;
import java.io.FileDescriptor;
import java.util.List;

public class IOplusWindowImpl implements IOplusWindow {
  private static final String TAG = "IOplusWindowImpl";
  
  private final IBinder mRemote;
  
  private IOplusWindowImpl(IWindow paramIWindow) {
    this(paramIWindow.asBinder());
  }
  
  private IOplusWindowImpl(IBinder paramIBinder) {
    this.mRemote = paramIBinder;
  }
  
  public static IOplusWindow asInterface(IWindow paramIWindow) {
    if (paramIWindow == null)
      return null; 
    return new IOplusWindowImpl(paramIWindow);
  }
  
  public static IOplusWindow asInterface(IBinder paramIBinder) {
    return new IOplusWindowImpl(paramIBinder);
  }
  
  public void longshotNotifyConnected(boolean paramBoolean) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      boolean bool;
      parcel.writeInterfaceToken("android.view.IWindow");
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      parcel.writeInt(bool);
      this.mRemote.transact(10002, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public void longshotDump(FileDescriptor paramFileDescriptor, List<OplusWindowNode> paramList1, List<OplusWindowNode> paramList2) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.view.IWindow");
      parcel.writeFileDescriptor(paramFileDescriptor);
      if (paramList1 != null) {
        parcel.writeInt(1);
        parcel.writeTypedList(paramList1);
      } else {
        parcel.writeInt(0);
      } 
      if (paramList2 != null) {
        parcel.writeInt(1);
        parcel.writeTypedList(paramList2);
      } else {
        parcel.writeInt(0);
      } 
      this.mRemote.transact(10003, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public OplusWindowNode longshotCollectWindow(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    null = null;
    try {
      boolean bool2;
      parcel1.writeInterfaceToken("android.view.IWindow");
      boolean bool1 = true;
      if (paramBoolean1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      parcel1.writeInt(bool2);
      if (paramBoolean2) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      parcel1.writeInt(bool2);
      this.mRemote.transact(10004, parcel1, parcel2, 0);
      parcel2.readException();
      if (parcel2.readInt() != 0)
        null = (OplusWindowNode)OplusWindowNode.CREATOR.createFromParcel(parcel2); 
      return null;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void longshotInjectInput(InputEvent paramInputEvent, int paramInt) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.view.IWindow");
      if (paramInputEvent != null) {
        parcel.writeInt(1);
        paramInputEvent.writeToParcel(parcel, 0);
      } else {
        parcel.writeInt(0);
      } 
      parcel.writeInt(paramInt);
      this.mRemote.transact(10005, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public void longshotInjectInputBegin() throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.view.IWindow");
      this.mRemote.transact(10006, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public void longshotInjectInputEnd() throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.view.IWindow");
      this.mRemote.transact(10007, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public void screenshotDump(FileDescriptor paramFileDescriptor) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.view.IWindow");
      parcel.writeFileDescriptor(paramFileDescriptor);
      this.mRemote.transact(10009, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public void directFindCmd(OplusDirectFindCmd paramOplusDirectFindCmd) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.view.IWindow");
      if (paramOplusDirectFindCmd != null) {
        parcel.writeInt(1);
        paramOplusDirectFindCmd.writeToParcel(parcel, 0);
      } else {
        parcel.writeInt(0);
      } 
      this.mRemote.transact(10008, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
}
