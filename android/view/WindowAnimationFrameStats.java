package android.view;

import android.os.Parcel;
import android.os.Parcelable;

public final class WindowAnimationFrameStats extends FrameStats implements Parcelable {
  public WindowAnimationFrameStats() {}
  
  public void init(long paramLong, long[] paramArrayOflong) {
    this.mRefreshPeriodNano = paramLong;
    this.mFramesPresentedTimeNano = paramArrayOflong;
  }
  
  private WindowAnimationFrameStats(Parcel paramParcel) {
    this.mRefreshPeriodNano = paramParcel.readLong();
    this.mFramesPresentedTimeNano = paramParcel.createLongArray();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mRefreshPeriodNano);
    paramParcel.writeLongArray(this.mFramesPresentedTimeNano);
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("WindowAnimationFrameStats[");
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("frameCount:");
    stringBuilder2.append(getFrameCount());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", fromTimeNano:");
    stringBuilder2.append(getStartTimeNano());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(", toTimeNano:");
    stringBuilder2.append(getEndTimeNano());
    stringBuilder1.append(stringBuilder2.toString());
    stringBuilder1.append(']');
    return stringBuilder1.toString();
  }
  
  public static final Parcelable.Creator<WindowAnimationFrameStats> CREATOR = new Parcelable.Creator<WindowAnimationFrameStats>() {
      public WindowAnimationFrameStats createFromParcel(Parcel param1Parcel) {
        return new WindowAnimationFrameStats(param1Parcel);
      }
      
      public WindowAnimationFrameStats[] newArray(int param1Int) {
        return new WindowAnimationFrameStats[param1Int];
      }
    };
}
