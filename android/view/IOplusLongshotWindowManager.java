package android.view;

import android.graphics.Rect;
import android.os.IBinder;
import android.os.RemoteException;

public interface IOplusLongshotWindowManager extends IOplusBaseWindowManager {
  public static final int GET_FOCUSED_WINDOW_FRAME = 10202;
  
  public static final int GET_LONGSHOT_SURFACE_LAYER = 10204;
  
  public static final int GET_LONGSHOT_SURFACE_LAYER_BY_TYPE = 10205;
  
  public static final int GET_LONGSHOT_WINDOW_BY_TYPE = 10212;
  
  public static final int GET_LONGSHOT_WINDOW_BY_TYPE_FOR_R = 10216;
  
  public static final int IOPLUSLONGSHOTWINDOWMANAGER_INDEX = 10201;
  
  public static final int IS_EDGE_PANEL_EXPAND = 10215;
  
  public static final int IS_FLOAT_ASSIST_EXPAND = 10214;
  
  public static final int IS_KEYGUARD_SHOWING_AND_NOT_OCCLUDED = 10208;
  
  public static final int IS_NAVIGATIONBAR_VISIBLE = 10207;
  
  public static final int IS_SHORTCUTS_PANEL_SHOW = 10209;
  
  public static final int IS_VOLUME_SHOW = 10213;
  
  public static final int LONGSHOT_INJECT_INPUT = 10203;
  
  public static final int LONGSHOT_INJECT_INPUT_BEGIN = 10210;
  
  public static final int LONGSHOT_INJECT_INPUT_END = 10211;
  
  public static final int LONGSHOT_NOTIFY_CONNECTED = 10206;
  
  public static final String LONGSHOT_SURFACECONTROL = "longshotSurfaceControl";
  
  void getFocusedWindowFrame(Rect paramRect) throws RemoteException;
  
  int getLongshotSurfaceLayer() throws RemoteException;
  
  int getLongshotSurfaceLayerByType(int paramInt) throws RemoteException;
  
  IBinder getLongshotWindowByType(int paramInt) throws RemoteException;
  
  SurfaceControl getLongshotWindowByTypeForR(int paramInt) throws RemoteException;
  
  boolean isEdgePanelExpand() throws RemoteException;
  
  boolean isFloatAssistExpand() throws RemoteException;
  
  boolean isKeyguardShowingAndNotOccluded() throws RemoteException;
  
  boolean isNavigationBarVisible() throws RemoteException;
  
  boolean isShortcutsPanelShow() throws RemoteException;
  
  boolean isVolumeShow() throws RemoteException;
  
  void longshotInjectInput(InputEvent paramInputEvent, int paramInt) throws RemoteException;
  
  void longshotInjectInputBegin() throws RemoteException;
  
  void longshotInjectInputEnd() throws RemoteException;
  
  void longshotNotifyConnected(boolean paramBoolean) throws RemoteException;
}
