package android.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.HardwareRenderer;
import android.graphics.Picture;
import android.graphics.RecordingCanvas;
import android.graphics.Rect;
import android.graphics.RenderNode;
import android.os.Debug;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.Stream;
import libcore.util.HexEncoding;

public class ViewDebug {
  private static final int CAPTURE_TIMEOUT = 6000;
  
  public static final boolean DEBUG_DRAG = false;
  
  public static final boolean DEBUG_POSITIONING = false;
  
  private static final String REMOTE_COMMAND_CAPTURE = "CAPTURE";
  
  private static final String REMOTE_COMMAND_CAPTURE_LAYERS = "CAPTURE_LAYERS";
  
  private static final String REMOTE_COMMAND_DUMP = "DUMP";
  
  public static final String REMOTE_COMMAND_DUMP_ENCODED = "DUMP_ENCODED";
  
  private static final String REMOTE_COMMAND_DUMP_THEME = "DUMP_THEME";
  
  private static final String REMOTE_COMMAND_INVALIDATE = "INVALIDATE";
  
  private static final String REMOTE_COMMAND_OUTPUT_DISPLAYLIST = "OUTPUT_DISPLAYLIST";
  
  private static final String REMOTE_COMMAND_REQUEST_LAYOUT = "REQUEST_LAYOUT";
  
  private static final String REMOTE_PROFILE = "PROFILE";
  
  @Deprecated
  public static final boolean TRACE_HIERARCHY = false;
  
  @Deprecated
  public static final boolean TRACE_RECYCLER = false;
  
  private static HashMap<Class<?>, PropertyInfo<CapturedViewProperty, ?>[]> sCapturedViewProperties;
  
  private static HashMap<Class<?>, PropertyInfo<ExportedProperty, ?>[]> sExportProperties;
  
  private static abstract class PropertyInfo<T extends Annotation, R extends AccessibleObject & Member> {
    public String entrySuffix = "";
    
    public final R member;
    
    public final String name;
    
    public final T property;
    
    public final Class<?> returnType;
    
    public String valueSuffix = "";
    
    PropertyInfo(Class<T> param1Class, R param1R, Class<?> param1Class1) {
      this.member = param1R;
      this.name = ((Member)param1R).getName();
      this.property = param1R.getAnnotation(param1Class);
      this.returnType = param1Class1;
    }
    
    static <T extends Annotation> PropertyInfo<T, ?> forMethod(Method param1Method, Class<T> param1Class) {
      try {
        if (param1Method.getReturnType() != Void.class) {
          int i = (param1Method.getParameterTypes()).length;
          if (i == 0) {
            if (!param1Method.isAnnotationPresent(param1Class))
              return null; 
            param1Method.setAccessible(true);
            ViewDebug.MethodPI<T> methodPI = new ViewDebug.MethodPI<>(param1Method, param1Class);
            methodPI.entrySuffix = "()";
            methodPI.valueSuffix = ";";
            return methodPI;
          } 
        } 
        return null;
      } catch (NoClassDefFoundError noClassDefFoundError) {
        return null;
      } 
    }
    
    static <T extends Annotation> PropertyInfo<T, ?> forField(Field param1Field, Class<T> param1Class) {
      if (!param1Field.isAnnotationPresent(param1Class))
        return null; 
      param1Field.setAccessible(true);
      return new ViewDebug.FieldPI<>(param1Field, param1Class);
    }
    
    public abstract Object invoke(Object param1Object) throws Exception;
  }
  
  class MethodPI<T extends Annotation> extends PropertyInfo<T, Method> {
    MethodPI(ViewDebug this$0, Class<T> param1Class) {
      super(param1Class, this$0, this$0.getReturnType());
    }
    
    public Object invoke(Object param1Object) throws Exception {
      return this.member.invoke(param1Object, new Object[0]);
    }
  }
  
  class FieldPI<T extends Annotation> extends PropertyInfo<T, Field> {
    FieldPI(ViewDebug this$0, Class<T> param1Class) {
      super(param1Class, this$0, this$0.getType());
    }
    
    public Object invoke(Object param1Object) throws Exception {
      return this.member.get(param1Object);
    }
  }
  
  @Deprecated
  public enum HierarchyTraceType {
    BUILD_CACHE, DRAW, INVALIDATE, INVALIDATE_CHILD, INVALIDATE_CHILD_IN_PARENT, ON_LAYOUT, ON_MEASURE, REQUEST_LAYOUT;
    
    private static final HierarchyTraceType[] $VALUES;
    
    static {
      DRAW = new HierarchyTraceType("DRAW", 6);
      HierarchyTraceType hierarchyTraceType = new HierarchyTraceType("BUILD_CACHE", 7);
      $VALUES = new HierarchyTraceType[] { INVALIDATE, INVALIDATE_CHILD, INVALIDATE_CHILD_IN_PARENT, REQUEST_LAYOUT, ON_LAYOUT, ON_MEASURE, DRAW, hierarchyTraceType };
    }
  }
  
  @Deprecated
  public enum RecyclerTraceType {
    NEW_VIEW, RECYCLE_FROM_ACTIVE_HEAP, RECYCLE_FROM_SCRAP_HEAP, BIND_VIEW, MOVE_FROM_ACTIVE_TO_SCRAP_HEAP, MOVE_TO_SCRAP_HEAP;
    
    private static final RecyclerTraceType[] $VALUES;
    
    static {
      MOVE_TO_SCRAP_HEAP = new RecyclerTraceType("MOVE_TO_SCRAP_HEAP", 4);
      RecyclerTraceType recyclerTraceType = new RecyclerTraceType("MOVE_FROM_ACTIVE_TO_SCRAP_HEAP", 5);
      $VALUES = new RecyclerTraceType[] { NEW_VIEW, BIND_VIEW, RECYCLE_FROM_ACTIVE_HEAP, RECYCLE_FROM_SCRAP_HEAP, MOVE_TO_SCRAP_HEAP, recyclerTraceType };
    }
  }
  
  public static long getViewInstanceCount() {
    return Debug.countInstancesOfClass(View.class);
  }
  
  public static long getViewRootImplCount() {
    return Debug.countInstancesOfClass(ViewRootImpl.class);
  }
  
  @Deprecated
  public static void trace(View paramView, RecyclerTraceType paramRecyclerTraceType, int... paramVarArgs) {}
  
  @Deprecated
  public static void startRecyclerTracing(String paramString, View paramView) {}
  
  @Deprecated
  public static void stopRecyclerTracing() {}
  
  @Deprecated
  public static void trace(View paramView, HierarchyTraceType paramHierarchyTraceType) {}
  
  @Deprecated
  public static void startHierarchyTracing(String paramString, View paramView) {}
  
  @Deprecated
  public static void stopHierarchyTracing() {}
  
  static void dispatchCommand(View paramView, String paramString1, String paramString2, OutputStream paramOutputStream) throws IOException {
    paramView = paramView.getRootView();
    if ("DUMP".equalsIgnoreCase(paramString1)) {
      dump(paramView, false, true, paramOutputStream);
    } else if ("DUMP_THEME".equalsIgnoreCase(paramString1)) {
      dumpTheme(paramView, paramOutputStream);
    } else if ("DUMP_ENCODED".equalsIgnoreCase(paramString1)) {
      dumpEncoded(paramView, paramOutputStream);
    } else if ("CAPTURE_LAYERS".equalsIgnoreCase(paramString1)) {
      captureLayers(paramView, new DataOutputStream(paramOutputStream));
    } else {
      String[] arrayOfString = paramString2.split(" ");
      if ("CAPTURE".equalsIgnoreCase(paramString1)) {
        capture(paramView, paramOutputStream, arrayOfString[0]);
      } else if ("OUTPUT_DISPLAYLIST".equalsIgnoreCase(paramString1)) {
        outputDisplayList(paramView, arrayOfString[0]);
      } else if ("INVALIDATE".equalsIgnoreCase(paramString1)) {
        invalidate(paramView, arrayOfString[0]);
      } else if ("REQUEST_LAYOUT".equalsIgnoreCase(paramString1)) {
        requestLayout(paramView, arrayOfString[0]);
      } else if ("PROFILE".equalsIgnoreCase(paramString1)) {
        profile(paramView, paramOutputStream, arrayOfString[0]);
      } 
    } 
  }
  
  public static View findView(View paramView, String paramString) {
    if (paramString.indexOf('@') != -1) {
      String[] arrayOfString = paramString.split("@");
      paramString = arrayOfString[0];
      int j = (int)Long.parseLong(arrayOfString[1], 16);
      paramView = paramView.getRootView();
      if (paramView instanceof ViewGroup)
        return findView((ViewGroup)paramView, paramString, j); 
      return null;
    } 
    int i = paramView.getResources().getIdentifier(paramString, null, null);
    return paramView.getRootView().findViewById(i);
  }
  
  private static void invalidate(View paramView, String paramString) {
    paramView = findView(paramView, paramString);
    if (paramView != null)
      paramView.postInvalidate(); 
  }
  
  private static void requestLayout(View paramView, String paramString) {
    final View view = findView(paramView, paramString);
    if (view != null)
      paramView.post(new Runnable() {
            final View val$view;
            
            public void run() {
              view.requestLayout();
            }
          }); 
  }
  
  private static void profile(View paramView, OutputStream paramOutputStream, String paramString) throws IOException {
    BufferedWriter bufferedWriter;
    View view1 = findView(paramView, paramString);
    View view2 = null;
    String str = null;
    paramString = str;
    paramView = view2;
    try {
      BufferedWriter bufferedWriter3 = new BufferedWriter();
      paramString = str;
      paramView = view2;
      OutputStreamWriter outputStreamWriter = new OutputStreamWriter();
      paramString = str;
      paramView = view2;
      this(paramOutputStream);
      paramString = str;
      paramView = view2;
      this(outputStreamWriter, 32768);
      BufferedWriter bufferedWriter1 = bufferedWriter3;
      if (view1 != null) {
        BufferedWriter bufferedWriter5 = bufferedWriter1, bufferedWriter4 = bufferedWriter1;
        profileViewAndChildren(view1, bufferedWriter1);
      } else {
        BufferedWriter bufferedWriter5 = bufferedWriter1, bufferedWriter4 = bufferedWriter1;
        bufferedWriter1.write("-1 -1 -1");
        bufferedWriter5 = bufferedWriter1;
        bufferedWriter4 = bufferedWriter1;
        bufferedWriter1.newLine();
      } 
      BufferedWriter bufferedWriter2 = bufferedWriter1;
      bufferedWriter = bufferedWriter1;
      bufferedWriter1.write("DONE.");
      bufferedWriter2 = bufferedWriter1;
      bufferedWriter = bufferedWriter1;
      bufferedWriter1.newLine();
      bufferedWriter = bufferedWriter1;
      bufferedWriter.close();
    } catch (Exception exception) {
      BufferedWriter bufferedWriter1 = bufferedWriter;
      Log.w("View", "Problem profiling the view:", exception);
      if (bufferedWriter != null)
        bufferedWriter.close(); 
    } finally {}
  }
  
  public static void profileViewAndChildren(View paramView, BufferedWriter paramBufferedWriter) throws IOException {
    RenderNode renderNode = RenderNode.create("ViewDebug", null);
    profileViewAndChildren(paramView, renderNode, paramBufferedWriter, true);
  }
  
  private static void profileViewAndChildren(View paramView, RenderNode paramRenderNode, BufferedWriter paramBufferedWriter, boolean paramBoolean) throws IOException {
    long l2, l3, l1 = 0L;
    if (paramBoolean || (paramView.mPrivateFlags & 0x800) != 0) {
      l2 = profileViewMeasure(paramView);
    } else {
      l2 = 0L;
    } 
    if (paramBoolean || (paramView.mPrivateFlags & 0x2000) != 0) {
      l3 = profileViewLayout(paramView);
    } else {
      l3 = 0L;
    } 
    if (paramBoolean || !paramView.willNotDraw() || (paramView.mPrivateFlags & 0x20) != 0)
      l1 = profileViewDraw(paramView, paramRenderNode); 
    paramBufferedWriter.write(String.valueOf(l2));
    paramBufferedWriter.write(32);
    paramBufferedWriter.write(String.valueOf(l3));
    paramBufferedWriter.write(32);
    paramBufferedWriter.write(String.valueOf(l1));
    paramBufferedWriter.newLine();
    if (paramView instanceof ViewGroup) {
      paramView = paramView;
      int i = paramView.getChildCount();
      for (byte b = 0; b < i; b++)
        profileViewAndChildren(paramView.getChildAt(b), paramRenderNode, paramBufferedWriter, false); 
    } 
  }
  
  private static long profileViewMeasure(View paramView) {
    return profileViewOperation(paramView, (ViewOperation)new Object(paramView));
  }
  
  private static long profileViewLayout(View paramView) {
    return profileViewOperation(paramView, new _$$Lambda$ViewDebug$inOytI2zZEgp1DJv8Cu4GjQVNiE(paramView));
  }
  
  private static long profileViewDraw(View paramView, RenderNode paramRenderNode) {
    _$$Lambda$ViewDebug$flFXZc7_CjFXx7_tYT59WSbUNjI _$$Lambda$ViewDebug$flFXZc7_CjFXx7_tYT59WSbUNjI;
    DisplayMetrics displayMetrics = paramView.getResources().getDisplayMetrics();
    if (displayMetrics == null)
      return 0L; 
    if (paramView.isHardwareAccelerated()) {
      RecordingCanvas recordingCanvas = paramRenderNode.beginRecording(displayMetrics.widthPixels, displayMetrics.heightPixels);
      try {
        _$$Lambda$ViewDebug$flFXZc7_CjFXx7_tYT59WSbUNjI = new _$$Lambda$ViewDebug$flFXZc7_CjFXx7_tYT59WSbUNjI();
        this(paramView, recordingCanvas);
        return profileViewOperation(paramView, _$$Lambda$ViewDebug$flFXZc7_CjFXx7_tYT59WSbUNjI);
      } finally {
        paramRenderNode.endRecording();
      } 
    } 
    Bitmap bitmap = Bitmap.createBitmap((DisplayMetrics)_$$Lambda$ViewDebug$flFXZc7_CjFXx7_tYT59WSbUNjI, ((DisplayMetrics)_$$Lambda$ViewDebug$flFXZc7_CjFXx7_tYT59WSbUNjI).widthPixels, ((DisplayMetrics)_$$Lambda$ViewDebug$flFXZc7_CjFXx7_tYT59WSbUNjI).heightPixels, Bitmap.Config.RGB_565);
    Canvas canvas = new Canvas(bitmap);
    try {
      _$$Lambda$ViewDebug$w986pBwzwNi77yEgLa3IWusjPNw _$$Lambda$ViewDebug$w986pBwzwNi77yEgLa3IWusjPNw = new _$$Lambda$ViewDebug$w986pBwzwNi77yEgLa3IWusjPNw();
      this(paramView, canvas);
      return profileViewOperation(paramView, _$$Lambda$ViewDebug$w986pBwzwNi77yEgLa3IWusjPNw);
    } finally {
      canvas.setBitmap(null);
      bitmap.recycle();
    } 
  }
  
  static interface ViewOperation {
    default void pre() {}
    
    void run();
  }
  
  private static long profileViewOperation(View paramView, ViewOperation paramViewOperation) {
    CountDownLatch countDownLatch = new CountDownLatch(1);
    long[] arrayOfLong = new long[1];
    paramView.post(new _$$Lambda$ViewDebug$5rTN0pemwbr3I3IL2E_xDBeDTDg(paramViewOperation, arrayOfLong, countDownLatch));
    try {
      if (!countDownLatch.await(6000L, TimeUnit.MILLISECONDS)) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Could not complete the profiling of the view ");
        stringBuilder.append(paramView);
        Log.w("View", stringBuilder.toString());
        return -1L;
      } 
      return arrayOfLong[0];
    } catch (InterruptedException interruptedException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Could not complete the profiling of the view ");
      stringBuilder.append(paramView);
      Log.w("View", stringBuilder.toString());
      Thread.currentThread().interrupt();
      return -1L;
    } 
  }
  
  public static void captureLayers(View paramView, DataOutputStream paramDataOutputStream) throws IOException {
    try {
      Rect rect = new Rect();
      this();
      try {
        paramView.mAttachInfo.mSession.getDisplayFrame(paramView.mAttachInfo.mWindow, rect);
      } catch (RemoteException remoteException) {}
      paramDataOutputStream.writeInt(rect.width());
      paramDataOutputStream.writeInt(rect.height());
      captureViewLayer(paramView, paramDataOutputStream, true);
      paramDataOutputStream.write(2);
      return;
    } finally {
      paramDataOutputStream.close();
    } 
  }
  
  private static void captureViewLayer(View paramView, DataOutputStream paramDataOutputStream, boolean paramBoolean) throws IOException {
    if (paramView.getVisibility() == 0 && paramBoolean) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    } 
    if ((paramView.mPrivateFlags & 0x80) != 128) {
      int i = paramView.getId();
      String str = paramView.getClass().getSimpleName();
      if (i != -1)
        str = resolveId(paramView.getContext(), i).toString(); 
      paramDataOutputStream.write(1);
      paramDataOutputStream.writeUTF(str);
      if (paramBoolean) {
        i = 1;
      } else {
        i = 0;
      } 
      paramDataOutputStream.writeByte(i);
      int[] arrayOfInt = new int[2];
      paramView.getLocationInWindow(arrayOfInt);
      paramDataOutputStream.writeInt(arrayOfInt[0]);
      paramDataOutputStream.writeInt(arrayOfInt[1]);
      paramDataOutputStream.flush();
      Bitmap bitmap = performViewCapture(paramView, true);
      if (bitmap != null) {
        i = bitmap.getWidth();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(i * bitmap.getHeight() * 2);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        paramDataOutputStream.writeInt(byteArrayOutputStream.size());
        byteArrayOutputStream.writeTo(paramDataOutputStream);
      } 
      paramDataOutputStream.flush();
    } 
    if (paramView instanceof ViewGroup) {
      ViewGroup viewGroup = (ViewGroup)paramView;
      int i = viewGroup.getChildCount();
      for (byte b = 0; b < i; b++)
        captureViewLayer(viewGroup.getChildAt(b), paramDataOutputStream, paramBoolean); 
    } 
    if (paramView.mOverlay != null) {
      paramView = (paramView.getOverlay()).mOverlayViewGroup;
      captureViewLayer(paramView, paramDataOutputStream, paramBoolean);
    } 
  }
  
  private static void outputDisplayList(View paramView, String paramString) throws IOException {
    paramView = findView(paramView, paramString);
    paramView.getViewRootImpl().outputDisplayList(paramView);
  }
  
  public static void outputDisplayList(View paramView1, View paramView2) {
    paramView1.getViewRootImpl().outputDisplayList(paramView2);
  }
  
  class PictureCallbackHandler implements AutoCloseable, HardwareRenderer.PictureCapturedCallback, Runnable {
    private final Function<Picture, Boolean> mCallback;
    
    private final Executor mExecutor;
    
    private final ReentrantLock mLock = new ReentrantLock(false);
    
    private final ArrayDeque<Picture> mQueue = new ArrayDeque<>(3);
    
    private Thread mRenderThread;
    
    private final HardwareRenderer mRenderer;
    
    private boolean mStopListening;
    
    private PictureCallbackHandler(ViewDebug this$0, Function<Picture, Boolean> param1Function, Executor param1Executor) {
      this.mRenderer = (HardwareRenderer)this$0;
      this.mCallback = param1Function;
      this.mExecutor = param1Executor;
      this$0.setPictureCaptureCallback(this);
    }
    
    public void close() {
      this.mLock.lock();
      this.mStopListening = true;
      this.mLock.unlock();
      this.mRenderer.setPictureCaptureCallback(null);
    }
    
    public void onPictureCaptured(Picture param1Picture) {
      this.mLock.lock();
      if (this.mStopListening) {
        this.mLock.unlock();
        this.mRenderer.setPictureCaptureCallback(null);
        return;
      } 
      if (this.mRenderThread == null)
        this.mRenderThread = Thread.currentThread(); 
      Picture picture = null;
      if (this.mQueue.size() == 3)
        picture = this.mQueue.removeLast(); 
      this.mQueue.add(param1Picture);
      this.mLock.unlock();
      if (picture == null) {
        this.mExecutor.execute(this);
      } else {
        picture.close();
      } 
    }
    
    public void run() {
      this.mLock.lock();
      Picture picture = this.mQueue.poll();
      boolean bool = this.mStopListening;
      this.mLock.unlock();
      if (Thread.currentThread() != this.mRenderThread) {
        if (bool) {
          picture.close();
          return;
        } 
        bool = ((Boolean)this.mCallback.apply(picture)).booleanValue();
        if (!bool)
          close(); 
        return;
      } 
      close();
      throw new IllegalStateException("ViewDebug#startRenderingCommandsCapture must be given an executor that invokes asynchronously");
    }
  }
  
  @Deprecated
  public static AutoCloseable startRenderingCommandsCapture(View paramView, Executor paramExecutor, Function<Picture, Boolean> paramFunction) {
    View.AttachInfo attachInfo = paramView.mAttachInfo;
    if (attachInfo != null) {
      if (attachInfo.mHandler.getLooper() == Looper.myLooper()) {
        ThreadedRenderer threadedRenderer = attachInfo.mThreadedRenderer;
        if (threadedRenderer != null)
          return new PictureCallbackHandler(paramFunction, paramExecutor); 
        return null;
      } 
      throw new IllegalStateException("Called on the wrong thread. Must be called on the thread that owns the given View");
    } 
    throw new IllegalArgumentException("Given view isn't attached");
  }
  
  class StreamingPictureCallbackHandler implements AutoCloseable, HardwareRenderer.PictureCapturedCallback, Runnable {
    private final ReentrantLock mLock = new ReentrantLock(false);
    
    private final ArrayDeque<byte[]> mQueue = (ArrayDeque)new ArrayDeque<>(3);
    
    private final ByteArrayOutputStream mByteStream = new ByteArrayOutputStream();
    
    private final Callable<OutputStream> mCallback;
    
    private final Executor mExecutor;
    
    private Thread mRenderThread;
    
    private final HardwareRenderer mRenderer;
    
    private boolean mStopListening;
    
    private StreamingPictureCallbackHandler(ViewDebug this$0, Callable<OutputStream> param1Callable, Executor param1Executor) {
      this.mRenderer = (HardwareRenderer)this$0;
      this.mCallback = param1Callable;
      this.mExecutor = param1Executor;
      this$0.setPictureCaptureCallback(this);
    }
    
    public void close() {
      this.mLock.lock();
      this.mStopListening = true;
      this.mLock.unlock();
      this.mRenderer.setPictureCaptureCallback(null);
    }
    
    public void onPictureCaptured(Picture param1Picture) {
      this.mLock.lock();
      if (this.mStopListening) {
        this.mLock.unlock();
        this.mRenderer.setPictureCaptureCallback(null);
        return;
      } 
      if (this.mRenderThread == null)
        this.mRenderThread = Thread.currentThread(); 
      boolean bool = true;
      if (this.mQueue.size() == 3) {
        this.mQueue.removeLast();
        bool = false;
      } 
      param1Picture.writeToStream(this.mByteStream);
      this.mQueue.add(this.mByteStream.toByteArray());
      this.mByteStream.reset();
      this.mLock.unlock();
      if (bool)
        this.mExecutor.execute(this); 
    }
    
    public void run() {
      this.mLock.lock();
      byte[] arrayOfByte = this.mQueue.poll();
      boolean bool = this.mStopListening;
      this.mLock.unlock();
      if (Thread.currentThread() != this.mRenderThread) {
        if (bool)
          return; 
        OutputStream outputStream = null;
        try {
          OutputStream outputStream1 = this.mCallback.call();
        } catch (Exception exception) {
          Log.w("ViewDebug", "Aborting rendering commands capture because callback threw exception", exception);
        } 
        if (outputStream != null) {
          try {
            outputStream.write(arrayOfByte);
          } catch (IOException iOException) {
            Log.w("ViewDebug", "Aborting rendering commands capture due to IOException writing to output stream", iOException);
          } 
        } else {
          close();
        } 
        return;
      } 
      close();
      throw new IllegalStateException("ViewDebug#startRenderingCommandsCapture must be given an executor that invokes asynchronously");
    }
  }
  
  public static AutoCloseable startRenderingCommandsCapture(View paramView, Executor paramExecutor, Callable<OutputStream> paramCallable) {
    View.AttachInfo attachInfo = paramView.mAttachInfo;
    if (attachInfo != null) {
      if (attachInfo.mHandler.getLooper() == Looper.myLooper()) {
        ThreadedRenderer threadedRenderer = attachInfo.mThreadedRenderer;
        if (threadedRenderer != null)
          return new StreamingPictureCallbackHandler(paramCallable, paramExecutor); 
        return null;
      } 
      throw new IllegalStateException("Called on the wrong thread. Must be called on the thread that owns the given View");
    } 
    throw new IllegalArgumentException("Given view isn't attached");
  }
  
  private static void capture(View paramView, OutputStream paramOutputStream, String paramString) throws IOException {
    View view = findView(paramView, paramString);
    capture(paramView, paramOutputStream, view);
  }
  
  public static void capture(View paramView1, OutputStream paramOutputStream, View paramView2) throws IOException {
    OutputStream outputStream;
    Bitmap bitmap3 = performViewCapture(paramView2, false);
    Bitmap bitmap2 = bitmap3;
    if (bitmap3 == null) {
      Log.w("View", "Failed to create capture bitmap!");
      bitmap2 = Bitmap.createBitmap(paramView1.getResources().getDisplayMetrics(), 1, 1, Bitmap.Config.ARGB_8888);
    } 
    bitmap3 = null;
    Bitmap bitmap1 = bitmap3;
    try {
      BufferedOutputStream bufferedOutputStream = new BufferedOutputStream();
      bitmap1 = bitmap3;
      this(paramOutputStream, 32768);
      paramOutputStream = bufferedOutputStream;
      outputStream = paramOutputStream;
      bitmap2.compress(Bitmap.CompressFormat.PNG, 100, paramOutputStream);
      outputStream = paramOutputStream;
      paramOutputStream.flush();
      return;
    } finally {
      if (outputStream != null)
        outputStream.close(); 
      bitmap2.recycle();
    } 
  }
  
  private static Bitmap performViewCapture(View paramView, boolean paramBoolean) {
    if (paramView != null) {
      CountDownLatch countDownLatch = new CountDownLatch(1);
      Bitmap[] arrayOfBitmap = new Bitmap[1];
      paramView.post(new _$$Lambda$ViewDebug$SYbJuwHeGrHQLha0YsHp4VI9JLg(paramView, arrayOfBitmap, paramBoolean, countDownLatch));
      try {
        countDownLatch.await(6000L, TimeUnit.MILLISECONDS);
        return arrayOfBitmap[0];
      } catch (InterruptedException interruptedException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Could not complete the capture of the view ");
        stringBuilder.append(paramView);
        Log.w("View", stringBuilder.toString());
        Thread.currentThread().interrupt();
      } 
    } 
    return null;
  }
  
  @Deprecated
  public static void dump(View paramView, boolean paramBoolean1, boolean paramBoolean2, OutputStream paramOutputStream) throws IOException {
    BufferedWriter bufferedWriter1 = null, bufferedWriter2 = null;
    BufferedWriter bufferedWriter3 = bufferedWriter2, bufferedWriter4 = bufferedWriter1;
    try {
      BufferedWriter bufferedWriter6 = new BufferedWriter();
      bufferedWriter3 = bufferedWriter2;
      bufferedWriter4 = bufferedWriter1;
      OutputStreamWriter outputStreamWriter = new OutputStreamWriter();
      bufferedWriter3 = bufferedWriter2;
      bufferedWriter4 = bufferedWriter1;
      this(paramOutputStream, "utf-8");
      bufferedWriter3 = bufferedWriter2;
      bufferedWriter4 = bufferedWriter1;
      this(outputStreamWriter, 32768);
      BufferedWriter bufferedWriter5 = bufferedWriter6;
      bufferedWriter3 = bufferedWriter5;
      bufferedWriter4 = bufferedWriter5;
      paramView = paramView.getRootView();
      bufferedWriter3 = bufferedWriter5;
      bufferedWriter4 = bufferedWriter5;
      if (paramView instanceof ViewGroup) {
        bufferedWriter3 = bufferedWriter5;
        bufferedWriter4 = bufferedWriter5;
        paramView = paramView;
        bufferedWriter3 = bufferedWriter5;
        bufferedWriter4 = bufferedWriter5;
        dumpViewHierarchy(paramView.getContext(), (ViewGroup)paramView, bufferedWriter5, 0, paramBoolean1, paramBoolean2);
      } 
      bufferedWriter3 = bufferedWriter5;
      bufferedWriter4 = bufferedWriter5;
      bufferedWriter5.write("DONE.");
      bufferedWriter3 = bufferedWriter5;
      bufferedWriter4 = bufferedWriter5;
      bufferedWriter5.newLine();
      bufferedWriter5.close();
    } catch (Exception exception) {
      BufferedWriter bufferedWriter;
      bufferedWriter3 = bufferedWriter4;
      Log.w("View", "Problem dumping the view:", exception);
      if (bufferedWriter4 != null) {
        bufferedWriter = bufferedWriter4;
      } else {
        return;
      } 
      bufferedWriter.close();
    } finally {}
  }
  
  public static void dumpv2(final View view, ByteArrayOutputStream paramByteArrayOutputStream) throws InterruptedException {
    final ViewHierarchyEncoder encoder = new ViewHierarchyEncoder(paramByteArrayOutputStream);
    final CountDownLatch latch = new CountDownLatch(1);
    view.post(new Runnable() {
          final ViewHierarchyEncoder val$encoder;
          
          final CountDownLatch val$latch;
          
          final View val$view;
          
          public void run() {
            encoder.addProperty("window:left", view.mAttachInfo.mWindowLeft);
            encoder.addProperty("window:top", view.mAttachInfo.mWindowTop);
            view.encode(encoder);
            latch.countDown();
          }
        });
    countDownLatch.await(2L, TimeUnit.SECONDS);
    viewHierarchyEncoder.endStream();
  }
  
  private static void dumpEncoded(View paramView, OutputStream paramOutputStream) throws IOException {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    ViewHierarchyEncoder viewHierarchyEncoder = new ViewHierarchyEncoder(byteArrayOutputStream);
    viewHierarchyEncoder.setUserPropertiesEnabled(false);
    viewHierarchyEncoder.addProperty("window:left", paramView.mAttachInfo.mWindowLeft);
    viewHierarchyEncoder.addProperty("window:top", paramView.mAttachInfo.mWindowTop);
    paramView.encode(viewHierarchyEncoder);
    viewHierarchyEncoder.endStream();
    paramOutputStream.write(byteArrayOutputStream.toByteArray());
  }
  
  public static void dumpTheme(View paramView, OutputStream paramOutputStream) throws IOException {
    BufferedWriter bufferedWriter1 = null, bufferedWriter2 = null;
    BufferedWriter bufferedWriter3 = bufferedWriter2, bufferedWriter4 = bufferedWriter1;
    try {
      BufferedWriter bufferedWriter6 = new BufferedWriter();
      bufferedWriter3 = bufferedWriter2;
      bufferedWriter4 = bufferedWriter1;
      OutputStreamWriter outputStreamWriter = new OutputStreamWriter();
      bufferedWriter3 = bufferedWriter2;
      bufferedWriter4 = bufferedWriter1;
      this(paramOutputStream, "utf-8");
      bufferedWriter3 = bufferedWriter2;
      bufferedWriter4 = bufferedWriter1;
      this(outputStreamWriter, 32768);
      BufferedWriter bufferedWriter5 = bufferedWriter6;
      bufferedWriter3 = bufferedWriter5;
      bufferedWriter4 = bufferedWriter5;
      Resources resources = paramView.getContext().getResources();
      bufferedWriter3 = bufferedWriter5;
      bufferedWriter4 = bufferedWriter5;
      Resources.Theme theme = paramView.getContext().getTheme();
      bufferedWriter3 = bufferedWriter5;
      bufferedWriter4 = bufferedWriter5;
      String[] arrayOfString = getStyleAttributesDump(resources, theme);
      if (arrayOfString != null) {
        byte b = 0;
        while (true) {
          bufferedWriter3 = bufferedWriter5;
          bufferedWriter4 = bufferedWriter5;
          if (b < arrayOfString.length) {
            if (arrayOfString[b] != null) {
              bufferedWriter3 = bufferedWriter5;
              bufferedWriter4 = bufferedWriter5;
              StringBuilder stringBuilder = new StringBuilder();
              bufferedWriter3 = bufferedWriter5;
              bufferedWriter4 = bufferedWriter5;
              this();
              bufferedWriter3 = bufferedWriter5;
              bufferedWriter4 = bufferedWriter5;
              stringBuilder.append(arrayOfString[b]);
              bufferedWriter3 = bufferedWriter5;
              bufferedWriter4 = bufferedWriter5;
              stringBuilder.append("\n");
              bufferedWriter3 = bufferedWriter5;
              bufferedWriter4 = bufferedWriter5;
              bufferedWriter5.write(stringBuilder.toString());
              bufferedWriter3 = bufferedWriter5;
              bufferedWriter4 = bufferedWriter5;
              stringBuilder = new StringBuilder();
              bufferedWriter3 = bufferedWriter5;
              bufferedWriter4 = bufferedWriter5;
              this();
              bufferedWriter3 = bufferedWriter5;
              bufferedWriter4 = bufferedWriter5;
              stringBuilder.append(arrayOfString[b + 1]);
              bufferedWriter3 = bufferedWriter5;
              bufferedWriter4 = bufferedWriter5;
              stringBuilder.append("\n");
              bufferedWriter3 = bufferedWriter5;
              bufferedWriter4 = bufferedWriter5;
              bufferedWriter5.write(stringBuilder.toString());
            } 
            b += 2;
            continue;
          } 
          break;
        } 
      } 
      bufferedWriter3 = bufferedWriter5;
      bufferedWriter4 = bufferedWriter5;
      bufferedWriter5.write("DONE.");
      bufferedWriter3 = bufferedWriter5;
      bufferedWriter4 = bufferedWriter5;
      bufferedWriter5.newLine();
      bufferedWriter5.close();
    } catch (Exception exception) {
      BufferedWriter bufferedWriter;
      bufferedWriter3 = bufferedWriter4;
      Log.w("View", "Problem dumping View Theme:", exception);
      if (bufferedWriter4 != null) {
        bufferedWriter = bufferedWriter4;
      } else {
        return;
      } 
      bufferedWriter.close();
    } finally {}
  }
  
  private static String[] getStyleAttributesDump(Resources paramResources, Resources.Theme paramTheme) {
    TypedValue typedValue = new TypedValue();
    byte b1 = 0;
    int[] arrayOfInt = paramTheme.getAllAttributes();
    String[] arrayOfString = new String[arrayOfInt.length * 2];
    int i;
    byte b2;
    for (i = arrayOfInt.length, b2 = 0; b2 < i; ) {
      int j = arrayOfInt[b2];
      byte b = b1;
      try {
        String str;
        arrayOfString[b1] = paramResources.getResourceName(j);
        b = b1;
        if (paramTheme.resolveAttribute(j, typedValue, true)) {
          b = b1;
          str = typedValue.coerceToString().toString();
        } else {
          str = "null";
        } 
        arrayOfString[b1 + 1] = str;
        b1 += 2;
        b = b1;
        if (typedValue.type == 1) {
          b = b1;
          arrayOfString[b1 - 1] = paramResources.getResourceName(typedValue.resourceId);
        } 
        b = b1;
      } catch (android.content.res.Resources.NotFoundException notFoundException) {}
      b2++;
      b1 = b;
    } 
    return arrayOfString;
  }
  
  private static View findView(ViewGroup paramViewGroup, String paramString, int paramInt) {
    if (isRequestedView(paramViewGroup, paramString, paramInt))
      return paramViewGroup; 
    int i = paramViewGroup.getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = paramViewGroup.getChildAt(b);
      if (view instanceof ViewGroup) {
        View view1 = findView((ViewGroup)view, paramString, paramInt);
        if (view1 != null)
          return view1; 
      } else if (isRequestedView(view, paramString, paramInt)) {
        return view;
      } 
      if (view.mOverlay != null) {
        View view1 = findView(view.mOverlay.mOverlayViewGroup, paramString, paramInt);
        if (view1 != null)
          return view1; 
      } 
      if (view instanceof HierarchyHandler) {
        HierarchyHandler hierarchyHandler = (HierarchyHandler)view;
        View view1 = hierarchyHandler.findHierarchyView(paramString, paramInt);
        if (view1 != null)
          return view1; 
      } 
    } 
    return null;
  }
  
  private static boolean isRequestedView(View paramView, String paramString, int paramInt) {
    if (paramView.hashCode() == paramInt) {
      String str = paramView.getClass().getName();
      if (paramString.equals("ViewOverlay"))
        return str.equals("android.view.ViewOverlay$OverlayViewGroup"); 
      return paramString.equals(str);
    } 
    return false;
  }
  
  private static void dumpViewHierarchy(Context paramContext, ViewGroup paramViewGroup, BufferedWriter paramBufferedWriter, int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    cacheExportedProperties(paramViewGroup.getClass());
    if (!paramBoolean1)
      cacheExportedPropertiesForChildren(paramViewGroup); 
    Handler handler1 = paramViewGroup.getHandler();
    Handler handler2 = handler1;
    if (handler1 == null)
      handler2 = new Handler(Looper.getMainLooper()); 
    if (handler2.getLooper() == Looper.myLooper()) {
      dumpViewHierarchyOnUIThread(paramContext, paramViewGroup, paramBufferedWriter, paramInt, paramBoolean1, paramBoolean2);
      return;
    } 
    FutureTask futureTask = new FutureTask(new _$$Lambda$ViewDebug$bTxz__vo_V1ixs_gVm1TKHAK8HQ(paramContext, paramViewGroup, paramBufferedWriter, paramInt, paramBoolean1, paramBoolean2), null);
    Message message = Message.obtain(handler2, futureTask);
    message.setAsynchronous(true);
    handler2.sendMessage(message);
    while (true) {
      try {
        futureTask.get(6000L, TimeUnit.MILLISECONDS);
        return;
      } catch (InterruptedException interruptedException) {
      
      } catch (ExecutionException|java.util.concurrent.TimeoutException executionException) {
        throw new RuntimeException(executionException);
      } 
    } 
  }
  
  private static void cacheExportedPropertiesForChildren(ViewGroup paramViewGroup) {
    int i = paramViewGroup.getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = paramViewGroup.getChildAt(b);
      cacheExportedProperties(view.getClass());
      if (view instanceof ViewGroup)
        cacheExportedPropertiesForChildren((ViewGroup)view); 
    } 
  }
  
  private static void cacheExportedProperties(Class<?> paramClass) {
    HashMap<Class<?>, PropertyInfo<ExportedProperty, ?>[]> hashMap = sExportProperties;
    Class<?> clazz = paramClass;
    if (hashMap != null) {
      clazz = paramClass;
      if (hashMap.containsKey(paramClass))
        return; 
    } 
    do {
      for (PropertyInfo<ExportedProperty, ?> propertyInfo : getExportedProperties(clazz)) {
        if (!propertyInfo.returnType.isPrimitive() && ((ExportedProperty)propertyInfo.property).deepExport())
          cacheExportedProperties(propertyInfo.returnType); 
      } 
      clazz = clazz.getSuperclass();
    } while (clazz != Object.class);
  }
  
  private static void dumpViewHierarchyOnUIThread(Context paramContext, ViewGroup paramViewGroup, BufferedWriter paramBufferedWriter, int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    if (!dumpView(paramContext, paramViewGroup, paramBufferedWriter, paramInt, paramBoolean2))
      return; 
    if (paramBoolean1)
      return; 
    int i = paramViewGroup.getChildCount();
    for (byte b = 0; b < i; b++) {
      View view = paramViewGroup.getChildAt(b);
      if (view instanceof ViewGroup) {
        dumpViewHierarchyOnUIThread(paramContext, (ViewGroup)view, paramBufferedWriter, paramInt + 1, paramBoolean1, paramBoolean2);
      } else {
        dumpView(paramContext, view, paramBufferedWriter, paramInt + 1, paramBoolean2);
      } 
      if (view.mOverlay != null) {
        ViewOverlay viewOverlay = view.getOverlay();
        ViewOverlay.OverlayViewGroup overlayViewGroup = viewOverlay.mOverlayViewGroup;
        dumpViewHierarchyOnUIThread(paramContext, overlayViewGroup, paramBufferedWriter, paramInt + 2, paramBoolean1, paramBoolean2);
      } 
    } 
    if (paramViewGroup instanceof HierarchyHandler)
      ((HierarchyHandler)paramViewGroup).dumpViewHierarchyWithProperties(paramBufferedWriter, paramInt + 1); 
  }
  
  private static boolean dumpView(Context paramContext, View paramView, BufferedWriter paramBufferedWriter, int paramInt, boolean paramBoolean) {
    byte b = 0;
    while (true) {
      if (b < paramInt) {
        try {
          paramBufferedWriter.write(32);
          b++;
        } catch (IOException iOException) {
          Log.w("View", "Error while dumping hierarchy tree");
          return false;
        } 
        continue;
      } 
      String str1 = paramView.getClass().getName();
      String str2 = str1;
      if (str1.equals("android.view.ViewOverlay$OverlayViewGroup"))
        str2 = "ViewOverlay"; 
      paramBufferedWriter.write(str2);
      paramBufferedWriter.write(64);
      paramBufferedWriter.write(Integer.toHexString(paramView.hashCode()));
      paramBufferedWriter.write(32);
      if (paramBoolean)
        dumpViewProperties((Context)iOException, paramView, paramBufferedWriter); 
      paramBufferedWriter.newLine();
      return true;
    } 
  }
  
  private static <T extends Annotation> PropertyInfo<T, ?>[] convertToPropertyInfos(Method[] paramArrayOfMethod, Field[] paramArrayOfField, Class<T> paramClass) {
    Stream<Stream> stream1 = Arrays.<Method>stream(paramArrayOfMethod).map(new _$$Lambda$ViewDebug$oTD19EOl5NKuILAfG60mCu8jfBc(paramClass));
    Stream stream = Arrays.<Field>stream(paramArrayOfField).map(new _$$Lambda$ViewDebug$IfqgQtwO6dHcRW0fkYD_gr6jHow(paramClass));
    stream1 = Stream.of(new Stream[] { stream1, stream });
    stream1 = stream1.flatMap((Function)Function.identity());
    -$.Lambda.ViewDebug.nWDBN_NTdi-tDKEGiWZSUZui5DE nWDBN_NTdi-tDKEGiWZSUZui5DE = _$$Lambda$ViewDebug$nWDBN_NTdi_tDKEGiWZSUZui5DE.INSTANCE;
    Stream<Stream> stream2 = stream1.filter((Predicate<? super Stream>)nWDBN_NTdi-tDKEGiWZSUZui5DE);
    -$.Lambda.ViewDebug.zYEykESfEZyxNAHOdqQtZB4owCQ zYEykESfEZyxNAHOdqQtZB4owCQ = _$$Lambda$ViewDebug$zYEykESfEZyxNAHOdqQtZB4owCQ.INSTANCE;
    return (PropertyInfo<T, ?>[])stream2.<PropertyInfo>toArray((IntFunction<PropertyInfo[]>)zYEykESfEZyxNAHOdqQtZB4owCQ);
  }
  
  private static PropertyInfo<ExportedProperty, ?>[] getExportedProperties(Class<?> paramClass) {
    if (sExportProperties == null)
      sExportProperties = (HashMap)new HashMap<>(); 
    HashMap<Class<?>, PropertyInfo<ExportedProperty, ?>[]> hashMap = sExportProperties;
    PropertyInfo[] arrayOfPropertyInfo1 = (PropertyInfo[])sExportProperties.get(paramClass);
    PropertyInfo[] arrayOfPropertyInfo2 = arrayOfPropertyInfo1;
    if (arrayOfPropertyInfo1 == null) {
      Method[] arrayOfMethod = paramClass.getDeclaredMethodsUnchecked(false);
      Field[] arrayOfField = paramClass.getDeclaredFieldsUnchecked(false);
      arrayOfPropertyInfo2 = convertToPropertyInfos(arrayOfMethod, arrayOfField, ExportedProperty.class);
      hashMap.put(paramClass, arrayOfPropertyInfo2);
    } 
    return (PropertyInfo<ExportedProperty, ?>[])arrayOfPropertyInfo2;
  }
  
  private static void dumpViewProperties(Context paramContext, Object paramObject, BufferedWriter paramBufferedWriter) throws IOException {
    dumpViewProperties(paramContext, paramObject, paramBufferedWriter, "");
  }
  
  private static void dumpViewProperties(Context paramContext, Object paramObject, BufferedWriter paramBufferedWriter, String paramString) throws IOException {
    StringBuilder stringBuilder;
    if (paramObject == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append("=4,null ");
      paramBufferedWriter.write(stringBuilder.toString());
      return;
    } 
    Class<?> clazz = paramObject.getClass();
    do {
      writeExportedProperties((Context)stringBuilder, paramObject, paramBufferedWriter, clazz, paramString);
      clazz = clazz.getSuperclass();
    } while (clazz != Object.class);
  }
  
  private static String formatIntToHexString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt).toUpperCase());
    return stringBuilder.toString();
  }
  
  private static void writeExportedProperties(Context paramContext, Object paramObject, BufferedWriter paramBufferedWriter, Class<?> paramClass, String paramString) throws IOException {
    for (PropertyInfo<ExportedProperty, ?> propertyInfo : getExportedProperties(paramClass)) {
      try {
        Object object1;
        String str;
        Object object2 = propertyInfo.invoke(paramObject);
        if (((ExportedProperty)propertyInfo.property).category().length() != 0) {
          object1 = new StringBuilder();
          object1.append(((ExportedProperty)propertyInfo.property).category());
          object1.append(":");
          str = object1.toString();
        } else {
          str = "";
        } 
        if (propertyInfo.returnType == int.class || propertyInfo.returnType == byte.class) {
          if (((ExportedProperty)propertyInfo.property).resolveId() && paramContext != null) {
            int i = ((Integer)object2).intValue();
            object1 = resolveId(paramContext, i);
          } else if (((ExportedProperty)propertyInfo.property).formatToHexString()) {
            if (propertyInfo.returnType == int.class) {
              object1 = formatIntToHexString(((Integer)object2).intValue());
            } else {
              object1 = object2;
              if (propertyInfo.returnType == byte.class) {
                object1 = new StringBuilder();
                object1.append("0x");
                object2 = object2;
                object1.append(HexEncoding.encodeToString(object2.byteValue(), true));
                object1 = object1.toString();
              } 
            } 
          } else {
            FlagToString[] arrayOfFlagToString = ((ExportedProperty)propertyInfo.property).flagMapping();
            if (arrayOfFlagToString.length > 0) {
              int i = ((Integer)object2).intValue();
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(str);
              stringBuilder.append(paramString);
              stringBuilder.append(propertyInfo.name);
              stringBuilder.append('_');
              String str1 = stringBuilder.toString();
              exportUnrolledFlags(paramBufferedWriter, arrayOfFlagToString, i, str1);
            } 
            object1 = ((ExportedProperty)propertyInfo.property).mapping();
            if (object1.length > 0) {
              int i = ((Integer)object2).intValue();
              boolean bool = false;
              int j = object1.length;
              byte b = 0;
              while (true) {
                if (b < j) {
                  IntToString intToString = object1[b];
                  if (intToString.from() == i) {
                    String str1 = intToString.to();
                    b = 1;
                    break;
                  } 
                  b++;
                  continue;
                } 
                b = bool;
                Object object = object2;
                break;
              } 
              if (b == 0)
                Integer integer = Integer.valueOf(i); 
            } else {
              Object object = object2;
            } 
          } 
        } else {
          if (propertyInfo.returnType == int[].class) {
            int[] arrayOfInt = (int[])object2;
            object2 = new StringBuilder();
            object2.append(str);
            object2.append(paramString);
            object2.append(propertyInfo.name);
            object2.append('_');
            object2 = object2.toString();
            exportUnrolledArray(paramContext, paramBufferedWriter, (ExportedProperty)propertyInfo.property, arrayOfInt, (String)object2, propertyInfo.entrySuffix);
            continue;
          } 
          if (propertyInfo.returnType == String[].class) {
            object2 = object2;
            if (((ExportedProperty)propertyInfo.property).hasAdjacentMapping() && object2 != null)
              for (byte b = 0; b < object2.length; b += 2) {
                if (object2[b] != null) {
                  Object object3 = new StringBuilder();
                  object3.append(str);
                  object3.append(paramString);
                  String str1 = object3.toString();
                  Object object4 = object2[b];
                  String str2 = propertyInfo.entrySuffix;
                  if (object2[b + 1] == null) {
                    String str3 = "null";
                  } else {
                    object3 = object2[b + 1];
                  } 
                  writeEntry(paramBufferedWriter, str1, (String)object4, str2, object3);
                } 
              }  
            continue;
          } 
          object1 = object2;
          if (!propertyInfo.returnType.isPrimitive()) {
            object1 = object2;
            if (((ExportedProperty)propertyInfo.property).deepExport()) {
              object1 = new StringBuilder();
              object1.append(paramString);
              object1.append(((ExportedProperty)propertyInfo.property).prefix());
              dumpViewProperties(paramContext, object2, paramBufferedWriter, object1.toString());
              continue;
            } 
          } 
        } 
        object2 = new StringBuilder();
        object2.append(str);
        object2.append(paramString);
        writeEntry(paramBufferedWriter, object2.toString(), propertyInfo.name, propertyInfo.entrySuffix, object1);
      } catch (Exception exception) {}
      continue;
    } 
  }
  
  private static void writeEntry(BufferedWriter paramBufferedWriter, String paramString1, String paramString2, String paramString3, Object paramObject) throws IOException {
    paramBufferedWriter.write(paramString1);
    paramBufferedWriter.write(paramString2);
    paramBufferedWriter.write(paramString3);
    paramBufferedWriter.write("=");
    writeValue(paramBufferedWriter, paramObject);
    paramBufferedWriter.write(32);
  }
  
  private static void exportUnrolledFlags(BufferedWriter paramBufferedWriter, FlagToString[] paramArrayOfFlagToString, int paramInt, String paramString) throws IOException {
    int i = paramArrayOfFlagToString.length;
    for (byte b = 0; b < i; b++) {
      boolean bool1;
      FlagToString flagToString = paramArrayOfFlagToString[b];
      boolean bool = flagToString.outputIf();
      int j = flagToString.mask() & paramInt;
      if (j == flagToString.equals()) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if ((bool1 && bool) || (!bool1 && !bool)) {
        String str1 = flagToString.name();
        String str2 = formatIntToHexString(j);
        writeEntry(paramBufferedWriter, paramString, str1, "", str2);
      } 
    } 
  }
  
  public static String intToString(Class<?> paramClass, String paramString, int paramInt) {
    IntToString[] arrayOfIntToString = getMapping(paramClass, paramString);
    if (arrayOfIntToString == null)
      return Integer.toString(paramInt); 
    int i = arrayOfIntToString.length;
    for (byte b = 0; b < i; b++) {
      IntToString intToString = arrayOfIntToString[b];
      if (intToString.from() == paramInt)
        return intToString.to(); 
    } 
    return Integer.toString(paramInt);
  }
  
  public static String flagsToString(Class<?> paramClass, String paramString, int paramInt) {
    FlagToString[] arrayOfFlagToString = getFlagMapping(paramClass, paramString);
    if (arrayOfFlagToString == null)
      return Integer.toHexString(paramInt); 
    StringBuilder stringBuilder = new StringBuilder();
    int i = arrayOfFlagToString.length;
    byte b = 0;
    while (true) {
      boolean bool = true;
      if (b < i) {
        FlagToString flagToString = arrayOfFlagToString[b];
        boolean bool1 = flagToString.outputIf();
        int j = flagToString.mask();
        if ((j & paramInt) != flagToString.equals())
          bool = false; 
        if (bool && bool1) {
          String str = flagToString.name();
          stringBuilder.append(str);
          stringBuilder.append(' ');
        } 
        b++;
        continue;
      } 
      break;
    } 
    if (stringBuilder.length() > 0)
      stringBuilder.deleteCharAt(stringBuilder.length() - 1); 
    return stringBuilder.toString();
  }
  
  private static FlagToString[] getFlagMapping(Class<?> paramClass, String paramString) {
    try {
      ExportedProperty exportedProperty = paramClass.getDeclaredField(paramString).<ExportedProperty>getAnnotation(ExportedProperty.class);
      return 
        exportedProperty.flagMapping();
    } catch (NoSuchFieldException noSuchFieldException) {
      return null;
    } 
  }
  
  private static IntToString[] getMapping(Class<?> paramClass, String paramString) {
    try {
      return ((ExportedProperty)paramClass.getDeclaredField(paramString).<ExportedProperty>getAnnotation(ExportedProperty.class)).mapping();
    } catch (NoSuchFieldException noSuchFieldException) {
      return null;
    } 
  }
  
  private static void exportUnrolledArray(Context paramContext, BufferedWriter paramBufferedWriter, ExportedProperty paramExportedProperty, int[] paramArrayOfint, String paramString1, String paramString2) throws IOException {
    boolean bool;
    IntToString[] arrayOfIntToString1 = paramExportedProperty.indexMapping();
    int i = arrayOfIntToString1.length;
    byte b1 = 0;
    if (i > 0) {
      i = 1;
    } else {
      i = 0;
    } 
    IntToString[] arrayOfIntToString2 = paramExportedProperty.mapping();
    if (arrayOfIntToString2.length > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    byte b2 = b1;
    if (paramExportedProperty.resolveId()) {
      b2 = b1;
      if (paramContext != null)
        b2 = 1; 
    } 
    int j = paramArrayOfint.length;
    for (b1 = 0; b1 < j; b1++) {
      String str2 = null;
      int k = paramArrayOfint[b1];
      String str1 = String.valueOf(b1);
      String str3 = str1;
      if (i != 0) {
        int m = arrayOfIntToString1.length;
        byte b = 0;
        while (true) {
          str3 = str1;
          if (b < m) {
            IntToString intToString = arrayOfIntToString1[b];
            if (intToString.from() == b1) {
              str3 = intToString.to();
              break;
            } 
            b++;
            continue;
          } 
          break;
        } 
      } 
      str1 = str2;
      if (bool) {
        int m = arrayOfIntToString2.length;
        byte b = 0;
        while (true) {
          str1 = str2;
          if (b < m) {
            IntToString intToString = arrayOfIntToString2[b];
            if (intToString.from() == k) {
              str1 = intToString.to();
              break;
            } 
            b++;
            continue;
          } 
          break;
        } 
      } 
      if (b2 != 0) {
        str2 = str1;
        if (str1 == null)
          str2 = (String)resolveId(paramContext, k); 
      } else {
        str2 = String.valueOf(k);
      } 
      writeEntry(paramBufferedWriter, paramString1, str3, paramString2, str2);
    } 
  }
  
  static Object resolveId(Context paramContext, int paramInt) {
    String str;
    Resources resources = paramContext.getResources();
    if (paramInt >= 0) {
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(resources.getResourceTypeName(paramInt));
        stringBuilder.append('/');
        stringBuilder.append(resources.getResourceEntryName(paramInt));
        str = stringBuilder.toString();
      } catch (android.content.res.Resources.NotFoundException notFoundException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("id/");
        stringBuilder.append(formatIntToHexString(paramInt));
        str = stringBuilder.toString();
      } 
    } else {
      str = "NO_ID";
    } 
    return str;
  }
  
  private static void writeValue(BufferedWriter paramBufferedWriter, Object paramObject) throws IOException {
    if (paramObject != null) {
      try {
        paramObject = paramObject.toString().replace("\n", "\\n");
      } finally {
        paramBufferedWriter.write(String.valueOf("[EXCEPTION]".length()));
        paramBufferedWriter.write(",");
        paramBufferedWriter.write("[EXCEPTION]");
      } 
    } else {
      paramBufferedWriter.write("4,null");
    } 
  }
  
  private static PropertyInfo<CapturedViewProperty, ?>[] getCapturedViewProperties(Class<?> paramClass) {
    if (sCapturedViewProperties == null)
      sCapturedViewProperties = (HashMap)new HashMap<>(); 
    HashMap<Class<?>, PropertyInfo<CapturedViewProperty, ?>[]> hashMap = sCapturedViewProperties;
    PropertyInfo[] arrayOfPropertyInfo1 = (PropertyInfo[])hashMap.get(paramClass);
    PropertyInfo[] arrayOfPropertyInfo2 = arrayOfPropertyInfo1;
    if (arrayOfPropertyInfo1 == null) {
      arrayOfPropertyInfo2 = convertToPropertyInfos(paramClass.getMethods(), paramClass.getFields(), CapturedViewProperty.class);
      hashMap.put(paramClass, arrayOfPropertyInfo2);
    } 
    return (PropertyInfo<CapturedViewProperty, ?>[])arrayOfPropertyInfo2;
  }
  
  private static String exportCapturedViewProperties(Object paramObject, Class<?> paramClass, String paramString) {
    if (paramObject == null)
      return "null"; 
    StringBuilder stringBuilder = new StringBuilder();
    for (PropertyInfo<CapturedViewProperty, ?> propertyInfo : getCapturedViewProperties(paramClass)) {
      try {
        Object object = propertyInfo.invoke(paramObject);
        if (((CapturedViewProperty)propertyInfo.property).retrieveReturn()) {
          Class<?> clazz = propertyInfo.returnType;
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append(propertyInfo.name);
          stringBuilder1.append("#");
          stringBuilder.append(exportCapturedViewProperties(object, clazz, stringBuilder1.toString()));
        } else {
          stringBuilder.append(paramString);
          stringBuilder.append(propertyInfo.name);
          stringBuilder.append(propertyInfo.entrySuffix);
          stringBuilder.append("=");
          if (object != null) {
            object = object.toString().replace("\n", "\\n");
            stringBuilder.append((String)object);
          } else {
            stringBuilder.append("null");
          } 
          stringBuilder.append(propertyInfo.valueSuffix);
          stringBuilder.append(" ");
        } 
      } catch (Exception exception) {}
    } 
    return stringBuilder.toString();
  }
  
  public static void dumpCapturedView(String paramString, Object paramObject) {
    Class<?> clazz = paramObject.getClass();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(clazz.getName());
    stringBuilder.append(": ");
    stringBuilder = new StringBuilder(stringBuilder.toString());
    stringBuilder.append(exportCapturedViewProperties(paramObject, clazz, ""));
    Log.d(paramString, stringBuilder.toString());
  }
  
  public static Object invokeViewMethod(final View view, final Method method, final Object[] args) {
    final CountDownLatch latch = new CountDownLatch(1);
    final AtomicReference result = new AtomicReference();
    final AtomicReference exception = new AtomicReference();
    view.post(new Runnable() {
          final Object[] val$args;
          
          final AtomicReference val$exception;
          
          final CountDownLatch val$latch;
          
          final Method val$method;
          
          final AtomicReference val$result;
          
          final View val$view;
          
          public void run() {
            try {
              result.set(method.invoke(view, args));
            } catch (InvocationTargetException invocationTargetException) {
              exception.set(invocationTargetException.getCause());
            } catch (Exception exception) {
              exception.set(exception);
            } 
            latch.countDown();
          }
        });
    try {
      countDownLatch.await();
      if (atomicReference2.get() == null)
        return atomicReference1.get(); 
      throw new RuntimeException((Throwable)atomicReference2.get());
    } catch (InterruptedException interruptedException) {
      throw new RuntimeException(interruptedException);
    } 
  }
  
  public static void setLayoutParameter(final View view, String paramString, int paramInt) throws NoSuchFieldException, IllegalAccessException {
    final ViewGroup.LayoutParams p = view.getLayoutParams();
    Field field = layoutParams.getClass().getField(paramString);
    if (field.getType() == int.class) {
      field.set(layoutParams, Integer.valueOf(paramInt));
      view.post(new Runnable() {
            final ViewGroup.LayoutParams val$p;
            
            final View val$view;
            
            public void run() {
              view.setLayoutParams(p);
            }
          });
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Only integer layout parameters can be set. Field ");
    stringBuilder.append(paramString);
    stringBuilder.append(" is of type ");
    stringBuilder.append(field.getType().getSimpleName());
    throw new RuntimeException(stringBuilder.toString());
  }
  
  class SoftwareCanvasProvider implements CanvasProvider {
    private Bitmap mBitmap;
    
    private Canvas mCanvas;
    
    private boolean mEnabledHwBitmapsInSwMode;
    
    public Canvas getCanvas(View param1View, int param1Int1, int param1Int2) {
      Bitmap bitmap = Bitmap.createBitmap(param1View.getResources().getDisplayMetrics(), param1Int1, param1Int2, Bitmap.Config.ARGB_8888);
      if (bitmap != null) {
        bitmap.setDensity((param1View.getResources().getDisplayMetrics()).densityDpi);
        if (param1View.mAttachInfo != null)
          this.mCanvas = param1View.mAttachInfo.mCanvas; 
        if (this.mCanvas == null)
          this.mCanvas = new Canvas(); 
        this.mEnabledHwBitmapsInSwMode = this.mCanvas.isHwBitmapsInSwModeEnabled();
        this.mCanvas.setBitmap(this.mBitmap);
        return this.mCanvas;
      } 
      throw new OutOfMemoryError();
    }
    
    public Bitmap createBitmap() {
      this.mCanvas.setBitmap(null);
      this.mCanvas.setHwBitmapsInSwModeEnabled(this.mEnabledHwBitmapsInSwMode);
      return this.mBitmap;
    }
  }
  
  class HardwareCanvasProvider implements CanvasProvider {
    private Picture mPicture;
    
    public Canvas getCanvas(View param1View, int param1Int1, int param1Int2) {
      Picture picture = new Picture();
      return picture.beginRecording(param1Int1, param1Int2);
    }
    
    public Bitmap createBitmap() {
      this.mPicture.endRecording();
      return Bitmap.createBitmap(this.mPicture);
    }
  }
  
  public static interface CanvasProvider {
    Bitmap createBitmap();
    
    Canvas getCanvas(View param1View, int param1Int1, int param1Int2);
  }
  
  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.FIELD, ElementType.METHOD})
  public static @interface CapturedViewProperty {
    boolean retrieveReturn() default false;
  }
  
  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.FIELD, ElementType.METHOD})
  public static @interface ExportedProperty {
    String category() default "";
    
    boolean deepExport() default false;
    
    ViewDebug.FlagToString[] flagMapping() default {};
    
    boolean formatToHexString() default false;
    
    boolean hasAdjacentMapping() default false;
    
    ViewDebug.IntToString[] indexMapping() default {};
    
    ViewDebug.IntToString[] mapping() default {};
    
    String prefix() default "";
    
    boolean resolveId() default false;
  }
  
  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.TYPE})
  public static @interface FlagToString {
    int equals();
    
    int mask();
    
    String name();
    
    boolean outputIf() default true;
  }
  
  public static interface HierarchyHandler {
    void dumpViewHierarchyWithProperties(BufferedWriter param1BufferedWriter, int param1Int);
    
    View findHierarchyView(String param1String, int param1Int);
  }
  
  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.TYPE})
  public static @interface IntToString {
    int from();
    
    String to();
  }
}
