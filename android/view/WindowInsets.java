package android.view;

import android.graphics.Insets;
import android.graphics.Rect;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.Objects;

public final class WindowInsets {
  public static final WindowInsets CONSUMED = new WindowInsets((Rect)null, null, false, false, null);
  
  private final boolean mAlwaysConsumeSystemBars;
  
  private final boolean mCompatIgnoreVisibility;
  
  private final int mCompatInsetsTypes;
  
  private final DisplayCutout mDisplayCutout;
  
  private final boolean mDisplayCutoutConsumed;
  
  private final boolean mIsRound;
  
  private final boolean mStableInsetsConsumed;
  
  private final boolean mSystemWindowInsetsConsumed;
  
  private Rect mTempRect;
  
  private final Insets[] mTypeInsetsMap;
  
  private final Insets[] mTypeMaxInsetsMap;
  
  private final boolean[] mTypeVisibilityMap;
  
  public WindowInsets(Rect paramRect1, Rect paramRect2, boolean paramBoolean1, boolean paramBoolean2, DisplayCutout paramDisplayCutout) {
    this(arrayOfInsets2, arrayOfInsets1, arrayOfBoolean, paramBoolean1, paramBoolean2, paramDisplayCutout, i, false);
  }
  
  public WindowInsets(Insets[] paramArrayOfInsets1, Insets[] paramArrayOfInsets2, boolean[] paramArrayOfboolean, boolean paramBoolean1, boolean paramBoolean2, DisplayCutout paramDisplayCutout, int paramInt, boolean paramBoolean3) {
    boolean bool2, bool1 = true;
    if (paramArrayOfInsets1 == null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mSystemWindowInsetsConsumed = bool2;
    if (bool2) {
      paramArrayOfInsets1 = new Insets[9];
    } else {
      paramArrayOfInsets1 = (Insets[])paramArrayOfInsets1.clone();
    } 
    this.mTypeInsetsMap = paramArrayOfInsets1;
    if (paramArrayOfInsets2 == null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mStableInsetsConsumed = bool2;
    if (bool2) {
      paramArrayOfInsets1 = new Insets[9];
    } else {
      paramArrayOfInsets1 = (Insets[])paramArrayOfInsets2.clone();
    } 
    this.mTypeMaxInsetsMap = paramArrayOfInsets1;
    this.mTypeVisibilityMap = paramArrayOfboolean;
    this.mIsRound = paramBoolean1;
    this.mAlwaysConsumeSystemBars = paramBoolean2;
    this.mCompatInsetsTypes = paramInt;
    this.mCompatIgnoreVisibility = paramBoolean3;
    if (paramDisplayCutout == null) {
      paramBoolean1 = bool1;
    } else {
      paramBoolean1 = false;
    } 
    this.mDisplayCutoutConsumed = paramBoolean1;
    if (paramBoolean1 || paramDisplayCutout.isEmpty())
      paramDisplayCutout = null; 
    this.mDisplayCutout = paramDisplayCutout;
  }
  
  public WindowInsets(WindowInsets paramWindowInsets) {
    this(arrayOfInsets2, arrayOfInsets1, arrayOfBoolean, bool2, bool1, displayCutout, i, bool3);
  }
  
  private static DisplayCutout displayCutoutCopyConstructorArgument(WindowInsets paramWindowInsets) {
    if (paramWindowInsets.mDisplayCutoutConsumed)
      return null; 
    DisplayCutout displayCutout = paramWindowInsets.mDisplayCutout;
    if (displayCutout == null)
      return DisplayCutout.NO_CUTOUT; 
    return displayCutout;
  }
  
  static Insets getInsets(Insets[] paramArrayOfInsets, int paramInt) {
    Insets insets1, insets2 = null;
    for (int i = 1; i <= 256; i <<= 1) {
      if ((paramInt & i) != 0) {
        Insets insets = paramArrayOfInsets[Type.indexOf(i)];
        if (insets != null)
          if (insets2 == null) {
            insets2 = insets;
          } else {
            insets2 = Insets.max(insets2, insets);
          }  
      } 
    } 
    if (insets2 == null) {
      insets1 = Insets.NONE;
    } else {
      insets1 = insets2;
    } 
    return insets1;
  }
  
  private static void setInsets(Insets[] paramArrayOfInsets, int paramInt, Insets paramInsets) {
    for (int i = 1; i <= 256; i <<= 1) {
      if ((paramInt & i) != 0)
        paramArrayOfInsets[Type.indexOf(i)] = paramInsets; 
    } 
  }
  
  public WindowInsets(Rect paramRect) {
    this(arrayOfInsets, null, arrayOfBoolean, false, false, null, i, false);
  }
  
  private static Insets[] createCompatTypeMap(Rect paramRect) {
    if (paramRect == null)
      return null; 
    Insets[] arrayOfInsets = new Insets[9];
    assignCompatInsets(arrayOfInsets, paramRect);
    return arrayOfInsets;
  }
  
  public static void assignCompatInsets(Insets[] paramArrayOfInsets, Rect paramRect) {
    paramArrayOfInsets[Type.indexOf(1)] = Insets.of(0, paramRect.top, 0, 0);
    int i = Type.indexOf(2), j = paramRect.left, k = paramRect.right, m = paramRect.bottom;
    paramArrayOfInsets[i] = Insets.of(j, 0, k, m);
  }
  
  private static boolean[] createCompatVisibilityMap(Insets[] paramArrayOfInsets) {
    boolean[] arrayOfBoolean = new boolean[9];
    if (paramArrayOfInsets == null)
      return arrayOfBoolean; 
    for (int i = 1; i <= 256; i <<= 1) {
      int j = Type.indexOf(i);
      if (!Insets.NONE.equals(paramArrayOfInsets[j]))
        arrayOfBoolean[j] = true; 
    } 
    return arrayOfBoolean;
  }
  
  @Deprecated
  public Rect getSystemWindowInsetsAsRect() {
    if (this.mTempRect == null)
      this.mTempRect = new Rect(); 
    Insets insets = getSystemWindowInsets();
    this.mTempRect.set(insets.left, insets.top, insets.right, insets.bottom);
    return this.mTempRect;
  }
  
  @Deprecated
  public Insets getSystemWindowInsets() {
    Insets insets1;
    if (this.mCompatIgnoreVisibility) {
      insets1 = getInsetsIgnoringVisibility(this.mCompatInsetsTypes & (Type.ime() ^ 0xFFFFFFFF));
    } else {
      insets1 = getInsets(this.mCompatInsetsTypes);
    } 
    Insets insets2 = insets1;
    if ((this.mCompatInsetsTypes & Type.ime()) != 0) {
      insets2 = insets1;
      if (this.mCompatIgnoreVisibility)
        insets2 = Insets.max(insets1, getInsets(Type.ime())); 
    } 
    return insets2;
  }
  
  public Insets getInsets(int paramInt) {
    return getInsets(this.mTypeInsetsMap, paramInt);
  }
  
  public Insets getInsetsIgnoringVisibility(int paramInt) {
    if ((paramInt & 0x8) == 0)
      return getInsets(this.mTypeMaxInsetsMap, paramInt); 
    throw new IllegalArgumentException("Unable to query the maximum insets for IME");
  }
  
  public boolean isVisible(int paramInt) {
    for (int i = 1; i <= 256; i <<= 1) {
      if ((paramInt & i) != 0)
        if (!this.mTypeVisibilityMap[Type.indexOf(i)])
          return false;  
    } 
    return true;
  }
  
  @Deprecated
  public int getSystemWindowInsetLeft() {
    return (getSystemWindowInsets()).left;
  }
  
  @Deprecated
  public int getSystemWindowInsetTop() {
    return (getSystemWindowInsets()).top;
  }
  
  @Deprecated
  public int getSystemWindowInsetRight() {
    return (getSystemWindowInsets()).right;
  }
  
  @Deprecated
  public int getSystemWindowInsetBottom() {
    return (getSystemWindowInsets()).bottom;
  }
  
  @Deprecated
  public boolean hasSystemWindowInsets() {
    return getSystemWindowInsets().equals(Insets.NONE) ^ true;
  }
  
  public boolean hasInsets() {
    if (getInsets(this.mTypeInsetsMap, Type.all()).equals(Insets.NONE)) {
      Insets[] arrayOfInsets = this.mTypeMaxInsetsMap;
      return 
        (!getInsets(arrayOfInsets, Type.all()).equals(Insets.NONE) || this.mDisplayCutout != null);
    } 
    return true;
  }
  
  public DisplayCutout getDisplayCutout() {
    return this.mDisplayCutout;
  }
  
  @Deprecated
  public WindowInsets consumeDisplayCutout() {
    Insets[] arrayOfInsets1;
    Insets[] arrayOfInsets2;
    if (this.mSystemWindowInsetsConsumed) {
      arrayOfInsets1 = null;
    } else {
      arrayOfInsets1 = this.mTypeInsetsMap;
    } 
    if (this.mStableInsetsConsumed) {
      arrayOfInsets2 = null;
    } else {
      arrayOfInsets2 = this.mTypeMaxInsetsMap;
    } 
    return new WindowInsets(arrayOfInsets1, arrayOfInsets2, this.mTypeVisibilityMap, this.mIsRound, this.mAlwaysConsumeSystemBars, null, this.mCompatInsetsTypes, this.mCompatIgnoreVisibility);
  }
  
  public boolean isConsumed() {
    boolean bool;
    if (this.mSystemWindowInsetsConsumed && this.mStableInsetsConsumed && this.mDisplayCutoutConsumed) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isRound() {
    return this.mIsRound;
  }
  
  @Deprecated
  public WindowInsets consumeSystemWindowInsets() {
    boolean arrayOfBoolean[] = this.mTypeVisibilityMap, bool1 = this.mIsRound, bool2 = this.mAlwaysConsumeSystemBars;
    return 

      
      new WindowInsets(null, null, arrayOfBoolean, bool1, bool2, displayCutoutCopyConstructorArgument(this), this.mCompatInsetsTypes, this.mCompatIgnoreVisibility);
  }
  
  @Deprecated
  public WindowInsets replaceSystemWindowInsets(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.mSystemWindowInsetsConsumed)
      return this; 
    return (new Builder(this)).setSystemWindowInsets(Insets.of(paramInt1, paramInt2, paramInt3, paramInt4)).build();
  }
  
  @Deprecated
  public WindowInsets replaceSystemWindowInsets(Rect paramRect) {
    return replaceSystemWindowInsets(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
  }
  
  @Deprecated
  public Insets getStableInsets() {
    return getInsets(this.mTypeMaxInsetsMap, this.mCompatInsetsTypes);
  }
  
  @Deprecated
  public int getStableInsetTop() {
    return (getStableInsets()).top;
  }
  
  @Deprecated
  public int getStableInsetLeft() {
    return (getStableInsets()).left;
  }
  
  @Deprecated
  public int getStableInsetRight() {
    return (getStableInsets()).right;
  }
  
  @Deprecated
  public int getStableInsetBottom() {
    return (getStableInsets()).bottom;
  }
  
  @Deprecated
  public boolean hasStableInsets() {
    return getStableInsets().equals(Insets.NONE) ^ true;
  }
  
  @Deprecated
  public Insets getSystemGestureInsets() {
    return getInsets(this.mTypeInsetsMap, 16);
  }
  
  @Deprecated
  public Insets getMandatorySystemGestureInsets() {
    return getInsets(this.mTypeInsetsMap, 32);
  }
  
  @Deprecated
  public Insets getTappableElementInsets() {
    return getInsets(this.mTypeInsetsMap, 64);
  }
  
  @Deprecated
  public WindowInsets consumeStableInsets() {
    return this;
  }
  
  public boolean shouldAlwaysConsumeSystemBars() {
    return this.mAlwaysConsumeSystemBars;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("WindowInsets{\n    ");
    for (byte b = 0; b < 9; b++) {
      Insets insets1 = this.mTypeInsetsMap[b];
      Insets insets2 = this.mTypeMaxInsetsMap[b];
      boolean bool = this.mTypeVisibilityMap[b];
      if (!Insets.NONE.equals(insets1) || !Insets.NONE.equals(insets2) || bool) {
        stringBuilder.append(Type.toString(1 << b));
        stringBuilder.append("=");
        stringBuilder.append(insets1);
        stringBuilder.append(" max=");
        stringBuilder.append(insets2);
        stringBuilder.append(" vis=");
        stringBuilder.append(bool);
        stringBuilder.append("\n    ");
      } 
    } 
    DisplayCutout displayCutout = this.mDisplayCutout;
    String str1 = "";
    if (displayCutout != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("cutout=");
      stringBuilder1.append(this.mDisplayCutout);
      str2 = stringBuilder1.toString();
    } else {
      str2 = "";
    } 
    stringBuilder.append(str2);
    stringBuilder.append("\n    ");
    String str2 = str1;
    if (isRound())
      str2 = "round"; 
    stringBuilder.append(str2);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  @Deprecated
  public WindowInsets inset(Rect paramRect) {
    return inset(paramRect.left, paramRect.top, paramRect.right, paramRect.bottom);
  }
  
  public WindowInsets inset(Insets paramInsets) {
    Objects.requireNonNull(paramInsets);
    return inset(paramInsets.left, paramInsets.top, paramInsets.right, paramInsets.bottom);
  }
  
  public WindowInsets inset(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    Insets[] arrayOfInsets1, arrayOfInsets2;
    DisplayCutout displayCutout;
    Preconditions.checkArgumentNonnegative(paramInt1);
    Preconditions.checkArgumentNonnegative(paramInt2);
    Preconditions.checkArgumentNonnegative(paramInt3);
    Preconditions.checkArgumentNonnegative(paramInt4);
    if (this.mSystemWindowInsetsConsumed) {
      arrayOfInsets1 = null;
    } else {
      arrayOfInsets1 = insetInsets(this.mTypeInsetsMap, paramInt1, paramInt2, paramInt3, paramInt4);
    } 
    if (this.mStableInsetsConsumed) {
      arrayOfInsets2 = null;
    } else {
      arrayOfInsets2 = insetInsets(this.mTypeMaxInsetsMap, paramInt1, paramInt2, paramInt3, paramInt4);
    } 
    boolean arrayOfBoolean[] = this.mTypeVisibilityMap, bool1 = this.mIsRound, bool2 = this.mAlwaysConsumeSystemBars;
    if (this.mDisplayCutoutConsumed) {
      displayCutout = null;
    } else {
      displayCutout = this.mDisplayCutout;
      if (displayCutout == null) {
        displayCutout = DisplayCutout.NO_CUTOUT;
      } else {
        displayCutout = displayCutout.inset(paramInt1, paramInt2, paramInt3, paramInt4);
      } 
    } 
    return new WindowInsets(arrayOfInsets1, arrayOfInsets2, arrayOfBoolean, bool1, bool2, displayCutout, this.mCompatInsetsTypes, this.mCompatIgnoreVisibility);
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || !(paramObject instanceof WindowInsets))
      return false; 
    paramObject = paramObject;
    if (this.mIsRound == ((WindowInsets)paramObject).mIsRound && this.mAlwaysConsumeSystemBars == ((WindowInsets)paramObject).mAlwaysConsumeSystemBars && this.mSystemWindowInsetsConsumed == ((WindowInsets)paramObject).mSystemWindowInsetsConsumed && this.mStableInsetsConsumed == ((WindowInsets)paramObject).mStableInsetsConsumed && this.mDisplayCutoutConsumed == ((WindowInsets)paramObject).mDisplayCutoutConsumed) {
      Insets[] arrayOfInsets1 = this.mTypeInsetsMap, arrayOfInsets2 = ((WindowInsets)paramObject).mTypeInsetsMap;
      if (Arrays.equals((Object[])arrayOfInsets1, (Object[])arrayOfInsets2)) {
        arrayOfInsets2 = this.mTypeMaxInsetsMap;
        arrayOfInsets1 = ((WindowInsets)paramObject).mTypeMaxInsetsMap;
        if (Arrays.equals((Object[])arrayOfInsets2, (Object[])arrayOfInsets1)) {
          boolean[] arrayOfBoolean1 = this.mTypeVisibilityMap, arrayOfBoolean2 = ((WindowInsets)paramObject).mTypeVisibilityMap;
          if (Arrays.equals(arrayOfBoolean1, arrayOfBoolean2)) {
            DisplayCutout displayCutout = this.mDisplayCutout;
            paramObject = ((WindowInsets)paramObject).mDisplayCutout;
            if (Objects.equals(displayCutout, paramObject))
              return null; 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Arrays.hashCode((Object[])this.mTypeInsetsMap), j = Arrays.hashCode((Object[])this.mTypeMaxInsetsMap);
    boolean[] arrayOfBoolean = this.mTypeVisibilityMap;
    int k = Arrays.hashCode(arrayOfBoolean);
    boolean bool1 = this.mIsRound;
    DisplayCutout displayCutout = this.mDisplayCutout;
    boolean bool2 = this.mAlwaysConsumeSystemBars;
    boolean bool3 = this.mSystemWindowInsetsConsumed, bool4 = this.mStableInsetsConsumed, bool5 = this.mDisplayCutoutConsumed;
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Boolean.valueOf(bool1), displayCutout, Boolean.valueOf(bool2), Boolean.valueOf(bool3), Boolean.valueOf(bool4), Boolean.valueOf(bool5) });
  }
  
  private static Insets[] insetInsets(Insets[] paramArrayOfInsets, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    boolean bool = false;
    for (byte b = 0; b < 9; b++, bool = bool1, paramArrayOfInsets = arrayOfInsets) {
      boolean bool1;
      Insets arrayOfInsets[], insets = paramArrayOfInsets[b];
      if (insets == null) {
        bool1 = bool;
        arrayOfInsets = paramArrayOfInsets;
      } else {
        Insets insets1 = insetInsets(insets, paramInt1, paramInt2, paramInt3, paramInt4);
        bool1 = bool;
        arrayOfInsets = paramArrayOfInsets;
        if (insets1 != insets) {
          bool1 = bool;
          arrayOfInsets = paramArrayOfInsets;
          if (!bool) {
            arrayOfInsets = (Insets[])paramArrayOfInsets.clone();
            bool1 = true;
          } 
          arrayOfInsets[b] = insets1;
        } 
      } 
    } 
    return paramArrayOfInsets;
  }
  
  static Insets insetInsets(Insets paramInsets, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    int i = Math.max(0, paramInsets.left - paramInt1);
    int j = Math.max(0, paramInsets.top - paramInt2);
    int k = Math.max(0, paramInsets.right - paramInt3);
    int m = Math.max(0, paramInsets.bottom - paramInt4);
    if (i == paramInt1 && j == paramInt2 && k == paramInt3 && m == paramInt4)
      return paramInsets; 
    return Insets.of(i, j, k, m);
  }
  
  boolean isSystemWindowInsetsConsumed() {
    return this.mSystemWindowInsetsConsumed;
  }
  
  public static final class Builder {
    private final boolean[] mTypeVisibilityMap;
    
    private final Insets[] mTypeMaxInsetsMap;
    
    private final Insets[] mTypeInsetsMap;
    
    private boolean mSystemInsetsConsumed = true;
    
    private boolean mStableInsetsConsumed = true;
    
    private boolean mIsRound;
    
    private DisplayCutout mDisplayCutout;
    
    private boolean mAlwaysConsumeSystemBars;
    
    public Builder() {
      this.mTypeInsetsMap = new Insets[9];
      this.mTypeMaxInsetsMap = new Insets[9];
      this.mTypeVisibilityMap = new boolean[9];
    }
    
    public Builder(WindowInsets param1WindowInsets) {
      this.mTypeInsetsMap = (Insets[])param1WindowInsets.mTypeInsetsMap.clone();
      this.mTypeMaxInsetsMap = (Insets[])param1WindowInsets.mTypeMaxInsetsMap.clone();
      this.mTypeVisibilityMap = (boolean[])param1WindowInsets.mTypeVisibilityMap.clone();
      this.mSystemInsetsConsumed = param1WindowInsets.mSystemWindowInsetsConsumed;
      this.mStableInsetsConsumed = param1WindowInsets.mStableInsetsConsumed;
      this.mDisplayCutout = WindowInsets.displayCutoutCopyConstructorArgument(param1WindowInsets);
      this.mIsRound = param1WindowInsets.mIsRound;
      this.mAlwaysConsumeSystemBars = param1WindowInsets.mAlwaysConsumeSystemBars;
    }
    
    @Deprecated
    public Builder setSystemWindowInsets(Insets param1Insets) {
      Preconditions.checkNotNull(param1Insets);
      WindowInsets.assignCompatInsets(this.mTypeInsetsMap, param1Insets.toRect());
      this.mSystemInsetsConsumed = false;
      return this;
    }
    
    @Deprecated
    public Builder setSystemGestureInsets(Insets param1Insets) {
      WindowInsets.setInsets(this.mTypeInsetsMap, 16, param1Insets);
      return this;
    }
    
    @Deprecated
    public Builder setMandatorySystemGestureInsets(Insets param1Insets) {
      WindowInsets.setInsets(this.mTypeInsetsMap, 32, param1Insets);
      return this;
    }
    
    @Deprecated
    public Builder setTappableElementInsets(Insets param1Insets) {
      WindowInsets.setInsets(this.mTypeInsetsMap, 64, param1Insets);
      return this;
    }
    
    public Builder setInsets(int param1Int, Insets param1Insets) {
      Preconditions.checkNotNull(param1Insets);
      WindowInsets.setInsets(this.mTypeInsetsMap, param1Int, param1Insets);
      this.mSystemInsetsConsumed = false;
      return this;
    }
    
    public Builder setInsetsIgnoringVisibility(int param1Int, Insets param1Insets) throws IllegalArgumentException {
      if (param1Int != 8) {
        Preconditions.checkNotNull(param1Insets);
        WindowInsets.setInsets(this.mTypeMaxInsetsMap, param1Int, param1Insets);
        this.mStableInsetsConsumed = false;
        return this;
      } 
      throw new IllegalArgumentException("Maximum inset not available for IME");
    }
    
    public Builder setVisible(int param1Int, boolean param1Boolean) {
      for (int i = 1; i <= 256; i <<= 1) {
        if ((param1Int & i) != 0)
          this.mTypeVisibilityMap[WindowInsets.Type.indexOf(i)] = param1Boolean; 
      } 
      return this;
    }
    
    @Deprecated
    public Builder setStableInsets(Insets param1Insets) {
      Preconditions.checkNotNull(param1Insets);
      WindowInsets.assignCompatInsets(this.mTypeMaxInsetsMap, param1Insets.toRect());
      this.mStableInsetsConsumed = false;
      return this;
    }
    
    public Builder setDisplayCutout(DisplayCutout param1DisplayCutout) {
      if (param1DisplayCutout == null)
        param1DisplayCutout = DisplayCutout.NO_CUTOUT; 
      this.mDisplayCutout = param1DisplayCutout;
      if (!param1DisplayCutout.isEmpty()) {
        Insets insets = Insets.of(this.mDisplayCutout.getSafeInsets());
        int i = WindowInsets.Type.indexOf(128);
        this.mTypeInsetsMap[i] = insets;
        this.mTypeMaxInsetsMap[i] = insets;
        this.mTypeVisibilityMap[i] = true;
      } 
      return this;
    }
    
    public Builder setRound(boolean param1Boolean) {
      this.mIsRound = param1Boolean;
      return this;
    }
    
    public Builder setAlwaysConsumeSystemBars(boolean param1Boolean) {
      this.mAlwaysConsumeSystemBars = param1Boolean;
      return this;
    }
    
    public WindowInsets build() {
      Insets[] arrayOfInsets1, arrayOfInsets2;
      if (this.mSystemInsetsConsumed) {
        arrayOfInsets1 = null;
      } else {
        arrayOfInsets1 = this.mTypeInsetsMap;
      } 
      if (this.mStableInsetsConsumed) {
        arrayOfInsets2 = null;
      } else {
        arrayOfInsets2 = this.mTypeMaxInsetsMap;
      } 
      boolean arrayOfBoolean[] = this.mTypeVisibilityMap, bool1 = this.mIsRound, bool2 = this.mAlwaysConsumeSystemBars;
      DisplayCutout displayCutout = this.mDisplayCutout;
      return new WindowInsets(arrayOfInsets1, arrayOfInsets2, arrayOfBoolean, bool1, bool2, displayCutout, WindowInsets.Type.systemBars(), false);
    }
  }
  
  public static final class Type {
    static final int CAPTION_BAR = 4;
    
    static final int DISPLAY_CUTOUT = 128;
    
    static final int FIRST = 1;
    
    static final int IME = 8;
    
    static final int LAST = 256;
    
    static final int MANDATORY_SYSTEM_GESTURES = 32;
    
    static final int NAVIGATION_BARS = 2;
    
    static final int SIZE = 9;
    
    static final int STATUS_BARS = 1;
    
    static final int SYSTEM_GESTURES = 16;
    
    static final int TAPPABLE_ELEMENT = 64;
    
    static final int WINDOW_DECOR = 256;
    
    static int indexOf(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 4) {
            if (param1Int != 8) {
              if (param1Int != 16) {
                if (param1Int != 32) {
                  if (param1Int != 64) {
                    if (param1Int != 128) {
                      if (param1Int == 256)
                        return 8; 
                      StringBuilder stringBuilder = new StringBuilder();
                      stringBuilder.append("type needs to be >= FIRST and <= LAST, type=");
                      stringBuilder.append(param1Int);
                      throw new IllegalArgumentException(stringBuilder.toString());
                    } 
                    return 7;
                  } 
                  return 6;
                } 
                return 5;
              } 
              return 4;
            } 
            return 3;
          } 
          return 2;
        } 
        return 1;
      } 
      return 0;
    }
    
    static String toString(int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      if ((param1Int & 0x1) != 0)
        stringBuilder.append("statusBars |"); 
      if ((param1Int & 0x2) != 0)
        stringBuilder.append("navigationBars |"); 
      if ((param1Int & 0x8) != 0)
        stringBuilder.append("ime |"); 
      if ((param1Int & 0x10) != 0)
        stringBuilder.append("systemGestures |"); 
      if ((param1Int & 0x20) != 0)
        stringBuilder.append("mandatorySystemGestures |"); 
      if ((param1Int & 0x40) != 0)
        stringBuilder.append("tappableElement |"); 
      if ((param1Int & 0x80) != 0)
        stringBuilder.append("displayCutout |"); 
      if ((param1Int & 0x100) != 0)
        stringBuilder.append("windowDecor |"); 
      if (stringBuilder.length() > 0)
        stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length()); 
      return stringBuilder.toString();
    }
    
    public static int statusBars() {
      return 1;
    }
    
    public static int navigationBars() {
      return 2;
    }
    
    public static int captionBar() {
      return 4;
    }
    
    public static int ime() {
      return 8;
    }
    
    public static int systemGestures() {
      return 16;
    }
    
    public static int mandatorySystemGestures() {
      return 32;
    }
    
    public static int tappableElement() {
      return 64;
    }
    
    public static int displayCutout() {
      return 128;
    }
    
    public static int systemBars() {
      return 7;
    }
    
    public static int all() {
      return -1;
    }
    
    public static boolean isVisibleInsetsType(int param1Int1, int param1Int2) {
      return ((systemBars() & param1Int1) != 0 || ((param1Int2 & 0xF0) != 48 && (
        ime() & param1Int1) != 0));
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface InsetsType {}
  }
  
  public static final class Side {
    public static final int BOTTOM = 8;
    
    public static final int LEFT = 1;
    
    public static final int RIGHT = 4;
    
    public static final int TOP = 2;
    
    public static int all() {
      return 15;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface InsetsSide {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface InsetsSide {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface InsetsType {}
}
