package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public interface IAppTransitionAnimationSpecsFuture extends IInterface {
  AppTransitionAnimationSpec[] get() throws RemoteException;
  
  class Default implements IAppTransitionAnimationSpecsFuture {
    public AppTransitionAnimationSpec[] get() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAppTransitionAnimationSpecsFuture {
    private static final String DESCRIPTOR = "android.view.IAppTransitionAnimationSpecsFuture";
    
    static final int TRANSACTION_get = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IAppTransitionAnimationSpecsFuture");
    }
    
    public static IAppTransitionAnimationSpecsFuture asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IAppTransitionAnimationSpecsFuture");
      if (iInterface != null && iInterface instanceof IAppTransitionAnimationSpecsFuture)
        return (IAppTransitionAnimationSpecsFuture)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "get";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.IAppTransitionAnimationSpecsFuture");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IAppTransitionAnimationSpecsFuture");
      AppTransitionAnimationSpec[] arrayOfAppTransitionAnimationSpec = get();
      param1Parcel2.writeNoException();
      param1Parcel2.writeTypedArray((Parcelable[])arrayOfAppTransitionAnimationSpec, 1);
      return true;
    }
    
    private static class Proxy implements IAppTransitionAnimationSpecsFuture {
      public static IAppTransitionAnimationSpecsFuture sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IAppTransitionAnimationSpecsFuture";
      }
      
      public AppTransitionAnimationSpec[] get() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IAppTransitionAnimationSpecsFuture");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAppTransitionAnimationSpecsFuture.Stub.getDefaultImpl() != null)
            return IAppTransitionAnimationSpecsFuture.Stub.getDefaultImpl().get(); 
          parcel2.readException();
          return (AppTransitionAnimationSpec[])parcel2.createTypedArray(AppTransitionAnimationSpec.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAppTransitionAnimationSpecsFuture param1IAppTransitionAnimationSpecsFuture) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAppTransitionAnimationSpecsFuture != null) {
          Proxy.sDefaultImpl = param1IAppTransitionAnimationSpecsFuture;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAppTransitionAnimationSpecsFuture getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
