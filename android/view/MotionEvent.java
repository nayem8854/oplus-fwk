package android.view;

import android.graphics.Matrix;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.util.SparseArray;
import dalvik.annotation.optimization.CriticalNative;
import dalvik.annotation.optimization.FastNative;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class MotionEvent extends InputEvent implements Parcelable {
  public static final int ACTION_BUTTON_PRESS = 11;
  
  public static final int ACTION_BUTTON_RELEASE = 12;
  
  public static final int ACTION_CANCEL = 3;
  
  public static final int ACTION_DOWN = 0;
  
  public static final int ACTION_HOVER_ENTER = 9;
  
  public static final int ACTION_HOVER_EXIT = 10;
  
  public static final int ACTION_HOVER_MOVE = 7;
  
  public static final int ACTION_MASK = 255;
  
  public static final int ACTION_MOVE = 2;
  
  public static final int ACTION_OUTSIDE = 4;
  
  @Deprecated
  public static final int ACTION_POINTER_1_DOWN = 5;
  
  @Deprecated
  public static final int ACTION_POINTER_1_UP = 6;
  
  @Deprecated
  public static final int ACTION_POINTER_2_DOWN = 261;
  
  @Deprecated
  public static final int ACTION_POINTER_2_UP = 262;
  
  @Deprecated
  public static final int ACTION_POINTER_3_DOWN = 517;
  
  @Deprecated
  public static final int ACTION_POINTER_3_UP = 518;
  
  public static final int ACTION_POINTER_DOWN = 5;
  
  @Deprecated
  public static final int ACTION_POINTER_ID_MASK = 65280;
  
  @Deprecated
  public static final int ACTION_POINTER_ID_SHIFT = 8;
  
  public static final int ACTION_POINTER_INDEX_MASK = 65280;
  
  public static final int ACTION_POINTER_INDEX_SHIFT = 8;
  
  public static final int ACTION_POINTER_UP = 6;
  
  public static final int ACTION_SCROLL = 8;
  
  public static final int ACTION_UP = 1;
  
  public static final int AXIS_BRAKE = 23;
  
  public static final int AXIS_DISTANCE = 24;
  
  public static final int AXIS_GAS = 22;
  
  public static final int AXIS_GENERIC_1 = 32;
  
  public static final int AXIS_GENERIC_10 = 41;
  
  public static final int AXIS_GENERIC_11 = 42;
  
  public static final int AXIS_GENERIC_12 = 43;
  
  public static final int AXIS_GENERIC_13 = 44;
  
  public static final int AXIS_GENERIC_14 = 45;
  
  public static final int AXIS_GENERIC_15 = 46;
  
  public static final int AXIS_GENERIC_16 = 47;
  
  public static final int AXIS_GENERIC_2 = 33;
  
  public static final int AXIS_GENERIC_3 = 34;
  
  public static final int AXIS_GENERIC_4 = 35;
  
  public static final int AXIS_GENERIC_5 = 36;
  
  public static final int AXIS_GENERIC_6 = 37;
  
  public static final int AXIS_GENERIC_7 = 38;
  
  public static final int AXIS_GENERIC_8 = 39;
  
  public static final int AXIS_GENERIC_9 = 40;
  
  public static final int AXIS_HAT_X = 15;
  
  public static final int AXIS_HAT_Y = 16;
  
  public static final int AXIS_HSCROLL = 10;
  
  public static final int AXIS_LTRIGGER = 17;
  
  public static final int AXIS_ORIENTATION = 8;
  
  public static final int AXIS_PRESSURE = 2;
  
  public static final int AXIS_RELATIVE_X = 27;
  
  public static final int AXIS_RELATIVE_Y = 28;
  
  public static final int AXIS_RTRIGGER = 18;
  
  public static final int AXIS_RUDDER = 20;
  
  public static final int AXIS_RX = 12;
  
  public static final int AXIS_RY = 13;
  
  public static final int AXIS_RZ = 14;
  
  public static final int AXIS_SCROLL = 26;
  
  public static final int AXIS_SIZE = 3;
  
  private static final SparseArray<String> AXIS_SYMBOLIC_NAMES = new SparseArray<>();
  
  public static final int AXIS_THROTTLE = 19;
  
  public static final int AXIS_TILT = 25;
  
  public static final int AXIS_TOOL_MAJOR = 6;
  
  public static final int AXIS_TOOL_MINOR = 7;
  
  public static final int AXIS_TOUCH_MAJOR = 4;
  
  public static final int AXIS_TOUCH_MINOR = 5;
  
  public static final int AXIS_VSCROLL = 9;
  
  public static final int AXIS_WHEEL = 21;
  
  public static final int AXIS_X = 0;
  
  public static final int AXIS_Y = 1;
  
  public static final int AXIS_Z = 11;
  
  public static final int BUTTON_BACK = 8;
  
  public static final int BUTTON_FORWARD = 16;
  
  public static final int BUTTON_PRIMARY = 1;
  
  public static final int BUTTON_SECONDARY = 2;
  
  public static final int BUTTON_STYLUS_PRIMARY = 32;
  
  public static final int BUTTON_STYLUS_SECONDARY = 64;
  
  static {
    SparseArray<String> sparseArray = AXIS_SYMBOLIC_NAMES;
    sparseArray.append(0, "AXIS_X");
    sparseArray.append(1, "AXIS_Y");
    sparseArray.append(2, "AXIS_PRESSURE");
    sparseArray.append(3, "AXIS_SIZE");
    sparseArray.append(4, "AXIS_TOUCH_MAJOR");
    sparseArray.append(5, "AXIS_TOUCH_MINOR");
    sparseArray.append(6, "AXIS_TOOL_MAJOR");
    sparseArray.append(7, "AXIS_TOOL_MINOR");
    sparseArray.append(8, "AXIS_ORIENTATION");
    sparseArray.append(9, "AXIS_VSCROLL");
    sparseArray.append(10, "AXIS_HSCROLL");
    sparseArray.append(11, "AXIS_Z");
    sparseArray.append(12, "AXIS_RX");
    sparseArray.append(13, "AXIS_RY");
    sparseArray.append(14, "AXIS_RZ");
    sparseArray.append(15, "AXIS_HAT_X");
    sparseArray.append(16, "AXIS_HAT_Y");
    sparseArray.append(17, "AXIS_LTRIGGER");
    sparseArray.append(18, "AXIS_RTRIGGER");
    sparseArray.append(19, "AXIS_THROTTLE");
    sparseArray.append(20, "AXIS_RUDDER");
    sparseArray.append(21, "AXIS_WHEEL");
    sparseArray.append(22, "AXIS_GAS");
    sparseArray.append(23, "AXIS_BRAKE");
    sparseArray.append(24, "AXIS_DISTANCE");
    sparseArray.append(25, "AXIS_TILT");
    sparseArray.append(26, "AXIS_SCROLL");
    sparseArray.append(27, "AXIS_REALTIVE_X");
    sparseArray.append(28, "AXIS_REALTIVE_Y");
    sparseArray.append(32, "AXIS_GENERIC_1");
    sparseArray.append(33, "AXIS_GENERIC_2");
    sparseArray.append(34, "AXIS_GENERIC_3");
    sparseArray.append(35, "AXIS_GENERIC_4");
    sparseArray.append(36, "AXIS_GENERIC_5");
    sparseArray.append(37, "AXIS_GENERIC_6");
    sparseArray.append(38, "AXIS_GENERIC_7");
    sparseArray.append(39, "AXIS_GENERIC_8");
    sparseArray.append(40, "AXIS_GENERIC_9");
    sparseArray.append(41, "AXIS_GENERIC_10");
    sparseArray.append(42, "AXIS_GENERIC_11");
    sparseArray.append(43, "AXIS_GENERIC_12");
    sparseArray.append(44, "AXIS_GENERIC_13");
    sparseArray.append(45, "AXIS_GENERIC_14");
    sparseArray.append(46, "AXIS_GENERIC_15");
    sparseArray.append(47, "AXIS_GENERIC_16");
  }
  
  private static final String[] BUTTON_SYMBOLIC_NAMES = new String[] { 
      "BUTTON_PRIMARY", "BUTTON_SECONDARY", "BUTTON_TERTIARY", "BUTTON_BACK", "BUTTON_FORWARD", "BUTTON_STYLUS_PRIMARY", "BUTTON_STYLUS_SECONDARY", "0x00000080", "0x00000100", "0x00000200", 
      "0x00000400", "0x00000800", "0x00001000", "0x00002000", "0x00004000", "0x00008000", "0x00010000", "0x00020000", "0x00040000", "0x00080000", 
      "0x00100000", "0x00200000", "0x00400000", "0x00800000", "0x01000000", "0x02000000", "0x04000000", "0x08000000", "0x10000000", "0x20000000", 
      "0x40000000", "0x80000000" };
  
  public static final int BUTTON_TERTIARY = 4;
  
  public static final int CLASSIFICATION_AMBIGUOUS_GESTURE = 1;
  
  public static final int CLASSIFICATION_DEEP_PRESS = 2;
  
  public static final int CLASSIFICATION_NONE = 0;
  
  public static final Parcelable.Creator<MotionEvent> CREATOR;
  
  private static final boolean DEBUG_CONCISE_TOSTRING = false;
  
  public static final int EDGE_BOTTOM = 2;
  
  public static final int EDGE_LEFT = 4;
  
  public static final int EDGE_RIGHT = 8;
  
  public static final int EDGE_TOP = 1;
  
  public static final int FLAG_HOVER_EXIT_PENDING = 4;
  
  public static final int FLAG_IS_GENERATED_GESTURE = 8;
  
  public static final int FLAG_TAINTED = -2147483648;
  
  public static final int FLAG_TARGET_ACCESSIBILITY_FOCUS = 1073741824;
  
  public static final int FLAG_WINDOW_IS_OBSCURED = 1;
  
  public static final int FLAG_WINDOW_IS_PARTIALLY_OBSCURED = 2;
  
  private static final int HISTORY_CURRENT = -2147483648;
  
  private static final float INVALID_CURSOR_POSITION = NaNF;
  
  public static final int INVALID_POINTER_ID = -1;
  
  private static final String LABEL_PREFIX = "AXIS_";
  
  private static final int MAX_RECYCLED = 10;
  
  private static final long NS_PER_MS = 1000000L;
  
  private static final String TAG = "MotionEvent";
  
  public static final int TOOL_TYPE_ERASER = 4;
  
  public static final int TOOL_TYPE_FINGER = 1;
  
  public static final int TOOL_TYPE_MOUSE = 3;
  
  public static final int TOOL_TYPE_STYLUS = 2;
  
  private static final SparseArray<String> TOOL_TYPE_SYMBOLIC_NAMES = new SparseArray<>();
  
  public static final int TOOL_TYPE_UNKNOWN = 0;
  
  static {
    sparseArray = TOOL_TYPE_SYMBOLIC_NAMES;
    sparseArray.append(0, "TOOL_TYPE_UNKNOWN");
    sparseArray.append(1, "TOOL_TYPE_FINGER");
    sparseArray.append(2, "TOOL_TYPE_STYLUS");
    sparseArray.append(3, "TOOL_TYPE_MOUSE");
    sparseArray.append(4, "TOOL_TYPE_ERASER");
  }
  
  private static final Object gRecyclerLock = new Object();
  
  private static MotionEvent gRecyclerTop;
  
  private static int gRecyclerUsed;
  
  private static final Object gSharedTempLock = new Object();
  
  private static PointerCoords[] gSharedTempPointerCoords;
  
  private static int[] gSharedTempPointerIndexMap;
  
  private static PointerProperties[] gSharedTempPointerProperties;
  
  private long mNativePtr;
  
  private MotionEvent mNext;
  
  private static final void ensureSharedTempPointerCapacity(int paramInt) {
    PointerCoords[] arrayOfPointerCoords = gSharedTempPointerCoords;
    if (arrayOfPointerCoords == null || arrayOfPointerCoords.length < paramInt) {
      int i;
      arrayOfPointerCoords = gSharedTempPointerCoords;
      if (arrayOfPointerCoords != null) {
        i = arrayOfPointerCoords.length;
      } else {
        i = 8;
      } 
      while (i < paramInt)
        i *= 2; 
      gSharedTempPointerCoords = PointerCoords.createArray(i);
      gSharedTempPointerProperties = PointerProperties.createArray(i);
      gSharedTempPointerIndexMap = new int[i];
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mNativePtr != 0L) {
        nativeDispose(this.mNativePtr);
        this.mNativePtr = 0L;
      } 
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private static MotionEvent obtain() {
    synchronized (gRecyclerLock) {
      MotionEvent motionEvent = gRecyclerTop;
      if (motionEvent == null) {
        motionEvent = new MotionEvent();
        this();
        return motionEvent;
      } 
      gRecyclerTop = motionEvent.mNext;
      gRecyclerUsed--;
      motionEvent.mNext = null;
      motionEvent.prepareForReuse();
      return motionEvent;
    } 
  }
  
  public static MotionEvent obtain(long paramLong1, long paramLong2, int paramInt1, int paramInt2, PointerProperties[] paramArrayOfPointerProperties, PointerCoords[] paramArrayOfPointerCoords, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9) {
    MotionEvent motionEvent = obtain();
    boolean bool = motionEvent.initialize(paramInt5, paramInt7, paramInt8, paramInt1, paramInt9, paramInt6, paramInt3, paramInt4, 0, 0.0F, 0.0F, paramFloat1, paramFloat2, paramLong1 * 1000000L, paramLong2 * 1000000L, paramInt2, paramArrayOfPointerProperties, paramArrayOfPointerCoords);
    if (!bool) {
      Log.e("MotionEvent", "Could not initialize MotionEvent");
      motionEvent.recycle();
      return null;
    } 
    return motionEvent;
  }
  
  public static MotionEvent obtain(long paramLong1, long paramLong2, int paramInt1, int paramInt2, PointerProperties[] paramArrayOfPointerProperties, PointerCoords[] paramArrayOfPointerCoords, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    return obtain(paramLong1, paramLong2, paramInt1, paramInt2, paramArrayOfPointerProperties, paramArrayOfPointerCoords, paramInt3, paramInt4, paramFloat1, paramFloat2, paramInt5, paramInt6, paramInt7, 0, paramInt8);
  }
  
  @Deprecated
  public static MotionEvent obtain(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int[] paramArrayOfint, PointerCoords[] paramArrayOfPointerCoords, int paramInt3, float paramFloat1, float paramFloat2, int paramInt4, int paramInt5, int paramInt6, int paramInt7) {
    synchronized (gSharedTempLock) {
      ensureSharedTempPointerCapacity(paramInt2);
      PointerProperties[] arrayOfPointerProperties = gSharedTempPointerProperties;
      for (byte b = 0; b < paramInt2; b++) {
        arrayOfPointerProperties[b].clear();
        (arrayOfPointerProperties[b]).id = paramArrayOfint[b];
      } 
      return obtain(paramLong1, paramLong2, paramInt1, paramInt2, arrayOfPointerProperties, paramArrayOfPointerCoords, paramInt3, 0, paramFloat1, paramFloat2, paramInt4, paramInt5, paramInt6, paramInt7);
    } 
  }
  
  public static MotionEvent obtain(long paramLong1, long paramLong2, int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2, float paramFloat5, float paramFloat6, int paramInt3, int paramInt4) {
    return obtain(paramLong1, paramLong2, paramInt1, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramInt2, paramFloat5, paramFloat6, paramInt3, paramInt4, 0, 0);
  }
  
  public static MotionEvent obtain(long paramLong1, long paramLong2, int paramInt1, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt2, float paramFloat5, float paramFloat6, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    MotionEvent motionEvent = obtain();
    synchronized (gSharedTempLock) {
      ensureSharedTempPointerCapacity(1);
      PointerProperties[] arrayOfPointerProperties = gSharedTempPointerProperties;
      arrayOfPointerProperties[0].clear();
      (arrayOfPointerProperties[0]).id = 0;
      PointerCoords[] arrayOfPointerCoords = gSharedTempPointerCoords;
      arrayOfPointerCoords[0].clear();
      (arrayOfPointerCoords[0]).x = paramFloat1;
      (arrayOfPointerCoords[0]).y = paramFloat2;
      (arrayOfPointerCoords[0]).pressure = paramFloat3;
      (arrayOfPointerCoords[0]).size = paramFloat4;
      motionEvent.initialize(paramInt3, paramInt5, paramInt6, paramInt1, 0, paramInt4, paramInt2, 0, 0, 0.0F, 0.0F, paramFloat5, paramFloat6, paramLong1 * 1000000L, paramLong2 * 1000000L, 1, arrayOfPointerProperties, arrayOfPointerCoords);
      return motionEvent;
    } 
  }
  
  @Deprecated
  public static MotionEvent obtain(long paramLong1, long paramLong2, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt3, float paramFloat5, float paramFloat6, int paramInt4, int paramInt5) {
    return obtain(paramLong1, paramLong2, paramInt1, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramInt3, paramFloat5, paramFloat6, paramInt4, paramInt5);
  }
  
  public static MotionEvent obtain(long paramLong1, long paramLong2, int paramInt1, float paramFloat1, float paramFloat2, int paramInt2) {
    return obtain(paramLong1, paramLong2, paramInt1, paramFloat1, paramFloat2, 1.0F, 1.0F, paramInt2, 1.0F, 1.0F, 0, 0);
  }
  
  public static MotionEvent obtain(MotionEvent paramMotionEvent) {
    if (paramMotionEvent != null) {
      MotionEvent motionEvent = obtain();
      motionEvent.mNativePtr = nativeCopy(motionEvent.mNativePtr, paramMotionEvent.mNativePtr, true);
      return motionEvent;
    } 
    throw new IllegalArgumentException("other motion event must not be null");
  }
  
  public static MotionEvent obtainNoHistory(MotionEvent paramMotionEvent) {
    if (paramMotionEvent != null) {
      MotionEvent motionEvent = obtain();
      motionEvent.mNativePtr = nativeCopy(motionEvent.mNativePtr, paramMotionEvent.mNativePtr, false);
      return motionEvent;
    } 
    throw new IllegalArgumentException("other motion event must not be null");
  }
  
  private boolean initialize(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, long paramLong1, long paramLong2, int paramInt10, PointerProperties[] paramArrayOfPointerProperties, PointerCoords[] paramArrayOfPointerCoords) {
    this.mNativePtr = paramLong1 = nativeInitialize(this.mNativePtr, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramInt9, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramLong1, paramLong2, paramInt10, paramArrayOfPointerProperties, paramArrayOfPointerCoords);
    if (paramLong1 == 0L)
      return false; 
    updateCursorPosition();
    return true;
  }
  
  public MotionEvent copy() {
    return obtain(this);
  }
  
  public final void recycle() {
    super.recycle();
    synchronized (gRecyclerLock) {
      if (gRecyclerUsed < 10) {
        gRecyclerUsed++;
        this.mNext = gRecyclerTop;
        gRecyclerTop = this;
      } 
      return;
    } 
  }
  
  public final void scale(float paramFloat) {
    if (paramFloat != 1.0F)
      nativeScale(this.mNativePtr, paramFloat); 
  }
  
  public int getId() {
    return nativeGetId(this.mNativePtr);
  }
  
  public final int getDeviceId() {
    return nativeGetDeviceId(this.mNativePtr);
  }
  
  public final int getSource() {
    return nativeGetSource(this.mNativePtr);
  }
  
  public final void setSource(int paramInt) {
    if (paramInt == getSource())
      return; 
    nativeSetSource(this.mNativePtr, paramInt);
    updateCursorPosition();
  }
  
  public int getDisplayId() {
    return nativeGetDisplayId(this.mNativePtr);
  }
  
  public void setDisplayId(int paramInt) {
    nativeSetDisplayId(this.mNativePtr, paramInt);
  }
  
  public final int getAction() {
    return nativeGetAction(this.mNativePtr);
  }
  
  public final int getActionMasked() {
    return nativeGetAction(this.mNativePtr) & 0xFF;
  }
  
  public final int getActionIndex() {
    return (nativeGetAction(this.mNativePtr) & 0xFF00) >> 8;
  }
  
  public final boolean isTouchEvent() {
    return nativeIsTouchEvent(this.mNativePtr);
  }
  
  public final int getFlags() {
    return nativeGetFlags(this.mNativePtr);
  }
  
  public final boolean isTainted() {
    boolean bool;
    int i = getFlags();
    if ((Integer.MIN_VALUE & i) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final void setTainted(boolean paramBoolean) {
    int i = getFlags();
    long l = this.mNativePtr;
    if (paramBoolean) {
      i = Integer.MIN_VALUE | i;
    } else {
      i = Integer.MAX_VALUE & i;
    } 
    nativeSetFlags(l, i);
  }
  
  public boolean isTargetAccessibilityFocus() {
    boolean bool;
    int i = getFlags();
    if ((0x40000000 & i) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setTargetAccessibilityFocus(boolean paramBoolean) {
    int i = getFlags();
    long l = this.mNativePtr;
    if (paramBoolean) {
      i = 0x40000000 | i;
    } else {
      i = 0xBFFFFFFF & i;
    } 
    nativeSetFlags(l, i);
  }
  
  public final boolean isHoverExitPending() {
    boolean bool;
    int i = getFlags();
    if ((i & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setHoverExitPending(boolean paramBoolean) {
    int i = getFlags();
    long l = this.mNativePtr;
    if (paramBoolean) {
      i |= 0x4;
    } else {
      i &= 0xFFFFFFFB;
    } 
    nativeSetFlags(l, i);
  }
  
  public final long getDownTime() {
    return nativeGetDownTimeNanos(this.mNativePtr) / 1000000L;
  }
  
  public final void setDownTime(long paramLong) {
    nativeSetDownTimeNanos(this.mNativePtr, 1000000L * paramLong);
  }
  
  public final long getEventTime() {
    return nativeGetEventTimeNanos(this.mNativePtr, -2147483648) / 1000000L;
  }
  
  public final long getEventTimeNano() {
    return nativeGetEventTimeNanos(this.mNativePtr, -2147483648);
  }
  
  public final float getX() {
    return nativeGetAxisValue(this.mNativePtr, 0, 0, -2147483648);
  }
  
  public final float getY() {
    return nativeGetAxisValue(this.mNativePtr, 1, 0, -2147483648);
  }
  
  public final float getPressure() {
    return nativeGetAxisValue(this.mNativePtr, 2, 0, -2147483648);
  }
  
  public final float getSize() {
    return nativeGetAxisValue(this.mNativePtr, 3, 0, -2147483648);
  }
  
  public final float getTouchMajor() {
    return nativeGetAxisValue(this.mNativePtr, 4, 0, -2147483648);
  }
  
  public final float getTouchMinor() {
    return nativeGetAxisValue(this.mNativePtr, 5, 0, -2147483648);
  }
  
  public final float getToolMajor() {
    return nativeGetAxisValue(this.mNativePtr, 6, 0, -2147483648);
  }
  
  public final float getToolMinor() {
    return nativeGetAxisValue(this.mNativePtr, 7, 0, -2147483648);
  }
  
  public final float getOrientation() {
    return nativeGetAxisValue(this.mNativePtr, 8, 0, -2147483648);
  }
  
  public final float getAxisValue(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, paramInt, 0, -2147483648);
  }
  
  public final int getPointerCount() {
    return nativeGetPointerCount(this.mNativePtr);
  }
  
  public final int getPointerId(int paramInt) {
    return nativeGetPointerId(this.mNativePtr, paramInt);
  }
  
  public final int getToolType(int paramInt) {
    return nativeGetToolType(this.mNativePtr, paramInt);
  }
  
  public final int findPointerIndex(int paramInt) {
    return nativeFindPointerIndex(this.mNativePtr, paramInt);
  }
  
  public final float getX(int paramInt) {
    try {
      return nativeGetAxisValue(this.mNativePtr, 0, paramInt, -2147483648);
    } catch (IllegalArgumentException illegalArgumentException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("MotionEvent is IllegalArgumentException ");
      stringBuilder.append(illegalArgumentException);
      Log.d("MotionEvent", stringBuilder.toString());
      return 0.0F;
    } 
  }
  
  public final float getY(int paramInt) {
    try {
      return nativeGetAxisValue(this.mNativePtr, 1, paramInt, -2147483648);
    } catch (IllegalArgumentException illegalArgumentException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("MotionEvent is IllegalArgumentException ");
      stringBuilder.append(illegalArgumentException);
      Log.d("MotionEvent", stringBuilder.toString());
      return 0.0F;
    } 
  }
  
  public final float getPressure(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 2, paramInt, -2147483648);
  }
  
  public final float getSize(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 3, paramInt, -2147483648);
  }
  
  public final float getTouchMajor(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 4, paramInt, -2147483648);
  }
  
  public final float getTouchMinor(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 5, paramInt, -2147483648);
  }
  
  public final float getToolMajor(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 6, paramInt, -2147483648);
  }
  
  public final float getToolMinor(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 7, paramInt, -2147483648);
  }
  
  public final float getOrientation(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 8, paramInt, -2147483648);
  }
  
  public final float getAxisValue(int paramInt1, int paramInt2) {
    return nativeGetAxisValue(this.mNativePtr, paramInt1, paramInt2, -2147483648);
  }
  
  public final void getPointerCoords(int paramInt, PointerCoords paramPointerCoords) {
    nativeGetPointerCoords(this.mNativePtr, paramInt, -2147483648, paramPointerCoords);
  }
  
  public final void getPointerProperties(int paramInt, PointerProperties paramPointerProperties) {
    nativeGetPointerProperties(this.mNativePtr, paramInt, paramPointerProperties);
  }
  
  public final int getMetaState() {
    return nativeGetMetaState(this.mNativePtr);
  }
  
  public final int getButtonState() {
    return nativeGetButtonState(this.mNativePtr);
  }
  
  public final void setButtonState(int paramInt) {
    nativeSetButtonState(this.mNativePtr, paramInt);
  }
  
  public int getClassification() {
    return nativeGetClassification(this.mNativePtr);
  }
  
  public final int getActionButton() {
    return nativeGetActionButton(this.mNativePtr);
  }
  
  public final void setActionButton(int paramInt) {
    nativeSetActionButton(this.mNativePtr, paramInt);
  }
  
  public final float getRawX() {
    if (OplusScreenDragUtil.isDragState())
      return OplusScreenDragUtil.getOffsetPosX(nativeGetRawAxisValue(this.mNativePtr, 0, 0, -2147483648)); 
    return nativeGetRawAxisValue(this.mNativePtr, 0, 0, -2147483648);
  }
  
  public final float getRawY() {
    if (OplusScreenDragUtil.isDragState())
      return OplusScreenDragUtil.getOffsetPosY(nativeGetRawAxisValue(this.mNativePtr, 1, 0, -2147483648)); 
    return nativeGetRawAxisValue(this.mNativePtr, 1, 0, -2147483648);
  }
  
  public float getRawX(int paramInt) {
    return nativeGetRawAxisValue(this.mNativePtr, 0, paramInt, -2147483648);
  }
  
  public float getRawY(int paramInt) {
    return nativeGetRawAxisValue(this.mNativePtr, 1, paramInt, -2147483648);
  }
  
  public final float getXPrecision() {
    return nativeGetXPrecision(this.mNativePtr);
  }
  
  public final float getYPrecision() {
    return nativeGetYPrecision(this.mNativePtr);
  }
  
  public float getXCursorPosition() {
    return nativeGetXCursorPosition(this.mNativePtr);
  }
  
  public float getYCursorPosition() {
    return nativeGetYCursorPosition(this.mNativePtr);
  }
  
  private void setCursorPosition(float paramFloat1, float paramFloat2) {
    nativeSetCursorPosition(this.mNativePtr, paramFloat1, paramFloat2);
  }
  
  public final int getHistorySize() {
    return nativeGetHistorySize(this.mNativePtr);
  }
  
  public final long getHistoricalEventTime(int paramInt) {
    return nativeGetEventTimeNanos(this.mNativePtr, paramInt) / 1000000L;
  }
  
  public final long getHistoricalEventTimeNano(int paramInt) {
    return nativeGetEventTimeNanos(this.mNativePtr, paramInt);
  }
  
  public final float getHistoricalX(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 0, 0, paramInt);
  }
  
  public final float getHistoricalY(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 1, 0, paramInt);
  }
  
  public final float getHistoricalPressure(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 2, 0, paramInt);
  }
  
  public final float getHistoricalSize(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 3, 0, paramInt);
  }
  
  public final float getHistoricalTouchMajor(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 4, 0, paramInt);
  }
  
  public final float getHistoricalTouchMinor(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 5, 0, paramInt);
  }
  
  public final float getHistoricalToolMajor(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 6, 0, paramInt);
  }
  
  public final float getHistoricalToolMinor(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 7, 0, paramInt);
  }
  
  public final float getHistoricalOrientation(int paramInt) {
    return nativeGetAxisValue(this.mNativePtr, 8, 0, paramInt);
  }
  
  public final float getHistoricalAxisValue(int paramInt1, int paramInt2) {
    return nativeGetAxisValue(this.mNativePtr, paramInt1, 0, paramInt2);
  }
  
  public final float getHistoricalX(int paramInt1, int paramInt2) {
    return nativeGetAxisValue(this.mNativePtr, 0, paramInt1, paramInt2);
  }
  
  public final float getHistoricalY(int paramInt1, int paramInt2) {
    return nativeGetAxisValue(this.mNativePtr, 1, paramInt1, paramInt2);
  }
  
  public final float getHistoricalPressure(int paramInt1, int paramInt2) {
    return nativeGetAxisValue(this.mNativePtr, 2, paramInt1, paramInt2);
  }
  
  public final float getHistoricalSize(int paramInt1, int paramInt2) {
    return nativeGetAxisValue(this.mNativePtr, 3, paramInt1, paramInt2);
  }
  
  public final float getHistoricalTouchMajor(int paramInt1, int paramInt2) {
    return nativeGetAxisValue(this.mNativePtr, 4, paramInt1, paramInt2);
  }
  
  public final float getHistoricalTouchMinor(int paramInt1, int paramInt2) {
    return nativeGetAxisValue(this.mNativePtr, 5, paramInt1, paramInt2);
  }
  
  public final float getHistoricalToolMajor(int paramInt1, int paramInt2) {
    return nativeGetAxisValue(this.mNativePtr, 6, paramInt1, paramInt2);
  }
  
  public final float getHistoricalToolMinor(int paramInt1, int paramInt2) {
    return nativeGetAxisValue(this.mNativePtr, 7, paramInt1, paramInt2);
  }
  
  public final float getHistoricalOrientation(int paramInt1, int paramInt2) {
    return nativeGetAxisValue(this.mNativePtr, 8, paramInt1, paramInt2);
  }
  
  public final float getHistoricalAxisValue(int paramInt1, int paramInt2, int paramInt3) {
    return nativeGetAxisValue(this.mNativePtr, paramInt1, paramInt2, paramInt3);
  }
  
  public final void getHistoricalPointerCoords(int paramInt1, int paramInt2, PointerCoords paramPointerCoords) {
    nativeGetPointerCoords(this.mNativePtr, paramInt1, paramInt2, paramPointerCoords);
  }
  
  public final int getEdgeFlags() {
    return nativeGetEdgeFlags(this.mNativePtr);
  }
  
  public final void setEdgeFlags(int paramInt) {
    nativeSetEdgeFlags(this.mNativePtr, paramInt);
  }
  
  public final void setAction(int paramInt) {
    nativeSetAction(this.mNativePtr, paramInt);
  }
  
  public final void offsetLocation(float paramFloat1, float paramFloat2) {
    if (paramFloat1 != 0.0F || paramFloat2 != 0.0F)
      nativeOffsetLocation(this.mNativePtr, paramFloat1, paramFloat2); 
  }
  
  public final void setLocation(float paramFloat1, float paramFloat2) {
    float f1 = getX();
    float f2 = getY();
    offsetLocation(paramFloat1 - f1, paramFloat2 - f2);
  }
  
  public final void transform(Matrix paramMatrix) {
    if (paramMatrix != null) {
      nativeTransform(this.mNativePtr, paramMatrix);
      return;
    } 
    throw new IllegalArgumentException("matrix must not be null");
  }
  
  public final void addBatch(long paramLong, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt) {
    synchronized (gSharedTempLock) {
      ensureSharedTempPointerCapacity(1);
      PointerCoords[] arrayOfPointerCoords = gSharedTempPointerCoords;
      arrayOfPointerCoords[0].clear();
      (arrayOfPointerCoords[0]).x = paramFloat1;
      (arrayOfPointerCoords[0]).y = paramFloat2;
      (arrayOfPointerCoords[0]).pressure = paramFloat3;
      (arrayOfPointerCoords[0]).size = paramFloat4;
      nativeAddBatch(this.mNativePtr, 1000000L * paramLong, arrayOfPointerCoords, paramInt);
      return;
    } 
  }
  
  public final void addBatch(long paramLong, PointerCoords[] paramArrayOfPointerCoords, int paramInt) {
    nativeAddBatch(this.mNativePtr, 1000000L * paramLong, paramArrayOfPointerCoords, paramInt);
  }
  
  public final boolean addBatch(MotionEvent paramMotionEvent) {
    int i = nativeGetAction(this.mNativePtr);
    if (i != 2 && i != 7)
      return false; 
    if (i != nativeGetAction(paramMotionEvent.mNativePtr))
      return false; 
    if (nativeGetDeviceId(this.mNativePtr) == nativeGetDeviceId(paramMotionEvent.mNativePtr)) {
      long l = this.mNativePtr;
      if (nativeGetSource(l) == nativeGetSource(paramMotionEvent.mNativePtr)) {
        l = this.mNativePtr;
        if (nativeGetDisplayId(l) == nativeGetDisplayId(paramMotionEvent.mNativePtr)) {
          l = this.mNativePtr;
          if (nativeGetFlags(l) == nativeGetFlags(paramMotionEvent.mNativePtr)) {
            l = this.mNativePtr;
            i = nativeGetClassification(l);
            l = paramMotionEvent.mNativePtr;
            if (i == nativeGetClassification(l)) {
              int j = nativeGetPointerCount(this.mNativePtr);
              if (j != nativeGetPointerCount(paramMotionEvent.mNativePtr))
                return false; 
              synchronized (gSharedTempLock) {
                ensureSharedTempPointerCapacity(Math.max(j, 2));
                PointerProperties[] arrayOfPointerProperties = gSharedTempPointerProperties;
                PointerCoords[] arrayOfPointerCoords = gSharedTempPointerCoords;
                for (i = 0; i < j; i++) {
                  nativeGetPointerProperties(this.mNativePtr, i, arrayOfPointerProperties[0]);
                  nativeGetPointerProperties(paramMotionEvent.mNativePtr, i, arrayOfPointerProperties[1]);
                  if (!arrayOfPointerProperties[0].equals(arrayOfPointerProperties[1]))
                    return false; 
                } 
                int k = nativeGetMetaState(paramMotionEvent.mNativePtr);
                int m = nativeGetHistorySize(paramMotionEvent.mNativePtr);
                for (i = 0; i <= m; i++) {
                  int n;
                  if (i == m) {
                    n = Integer.MIN_VALUE;
                  } else {
                    n = i;
                  } 
                  for (byte b = 0; b < j; b++)
                    nativeGetPointerCoords(paramMotionEvent.mNativePtr, b, n, arrayOfPointerCoords[b]); 
                  l = nativeGetEventTimeNanos(paramMotionEvent.mNativePtr, n);
                  nativeAddBatch(this.mNativePtr, l, arrayOfPointerCoords, k);
                } 
                return true;
              } 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public final boolean isWithinBoundsNoHistory(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    int i = nativeGetPointerCount(this.mNativePtr);
    for (byte b = 0; b < i; b++) {
      float f1 = nativeGetAxisValue(this.mNativePtr, 0, b, -2147483648);
      float f2 = nativeGetAxisValue(this.mNativePtr, 1, b, -2147483648);
      if (f1 < paramFloat1 || f1 > paramFloat3 || f2 < paramFloat2 || f2 > paramFloat4)
        return false; 
    } 
    return true;
  }
  
  private static final float clamp(float paramFloat1, float paramFloat2, float paramFloat3) {
    if (paramFloat1 < paramFloat2)
      return paramFloat2; 
    if (paramFloat1 > paramFloat3)
      return paramFloat3; 
    return paramFloat1;
  }
  
  public final MotionEvent clampNoHistory(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    Exception exception;
    MotionEvent motionEvent = obtain();
    Object object = gSharedTempLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      int i = nativeGetPointerCount(this.mNativePtr);
      ensureSharedTempPointerCapacity(i);
      PointerProperties[] arrayOfPointerProperties = gSharedTempPointerProperties;
      PointerCoords[] arrayOfPointerCoords = gSharedTempPointerCoords;
      int j = 0;
      while (true) {
        if (j < i) {
          try {
            nativeGetPointerProperties(this.mNativePtr, j, arrayOfPointerProperties[j]);
            nativeGetPointerCoords(this.mNativePtr, j, -2147483648, arrayOfPointerCoords[j]);
            PointerCoords pointerCoords = arrayOfPointerCoords[j];
            float f = (arrayOfPointerCoords[j]).x;
            try {
              pointerCoords.x = clamp(f, paramFloat1, paramFloat3);
              pointerCoords = arrayOfPointerCoords[j];
              f = (arrayOfPointerCoords[j]).y;
              pointerCoords.y = clamp(f, paramFloat2, paramFloat4);
              j++;
              continue;
            } finally {}
          } finally {}
        } else {
          int k = nativeGetDeviceId(this.mNativePtr), m = nativeGetSource(this.mNativePtr);
          long l1 = this.mNativePtr;
          j = nativeGetDisplayId(l1);
          l1 = this.mNativePtr;
          int n = nativeGetAction(l1), i1 = nativeGetFlags(this.mNativePtr);
          l1 = this.mNativePtr;
          int i2 = nativeGetEdgeFlags(l1), i3 = nativeGetMetaState(this.mNativePtr);
          l1 = this.mNativePtr;
          int i4 = nativeGetButtonState(l1), i5 = nativeGetClassification(this.mNativePtr);
          l1 = this.mNativePtr;
          paramFloat2 = nativeGetXOffset(l1);
          paramFloat3 = nativeGetYOffset(this.mNativePtr);
          l1 = this.mNativePtr;
          paramFloat1 = nativeGetXPrecision(l1);
          paramFloat4 = nativeGetYPrecision(this.mNativePtr);
          l1 = this.mNativePtr;
          l1 = nativeGetDownTimeNanos(l1);
          long l2 = this.mNativePtr;
          l2 = nativeGetEventTimeNanos(l2, -2147483648);
          motionEvent.initialize(k, m, j, n, i1, i2, i3, i4, i5, paramFloat2, paramFloat3, paramFloat1, paramFloat4, l1, l2, i, arrayOfPointerProperties, arrayOfPointerCoords);
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return motionEvent;
        } 
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        throw arrayOfPointerProperties;
      } 
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    throw exception;
  }
  
  public final int getPointerIdBits() {
    int i = 0;
    int j = nativeGetPointerCount(this.mNativePtr);
    for (byte b = 0; b < j; b++)
      i |= 1 << nativeGetPointerId(this.mNativePtr, b); 
    return i;
  }
  
  public final MotionEvent split(int paramInt) {
    // Byte code:
    //   0: invokestatic obtain : ()Landroid/view/MotionEvent;
    //   3: astore_2
    //   4: getstatic android/view/MotionEvent.gSharedTempLock : Ljava/lang/Object;
    //   7: astore_3
    //   8: aload_3
    //   9: monitorenter
    //   10: aload_2
    //   11: astore #4
    //   13: aload_0
    //   14: getfield mNativePtr : J
    //   17: invokestatic nativeGetPointerCount : (J)I
    //   20: istore #5
    //   22: aload_2
    //   23: astore #4
    //   25: iload #5
    //   27: invokestatic ensureSharedTempPointerCapacity : (I)V
    //   30: aload_2
    //   31: astore #4
    //   33: getstatic android/view/MotionEvent.gSharedTempPointerProperties : [Landroid/view/MotionEvent$PointerProperties;
    //   36: astore #6
    //   38: aload_2
    //   39: astore #4
    //   41: getstatic android/view/MotionEvent.gSharedTempPointerCoords : [Landroid/view/MotionEvent$PointerCoords;
    //   44: astore #7
    //   46: aload_2
    //   47: astore #4
    //   49: getstatic android/view/MotionEvent.gSharedTempPointerIndexMap : [I
    //   52: astore #8
    //   54: aload_2
    //   55: astore #4
    //   57: aload_0
    //   58: getfield mNativePtr : J
    //   61: invokestatic nativeGetAction : (J)I
    //   64: istore #9
    //   66: iload #9
    //   68: sipush #255
    //   71: iand
    //   72: istore #10
    //   74: ldc 65280
    //   76: iload #9
    //   78: iand
    //   79: bipush #8
    //   81: ishr
    //   82: istore #11
    //   84: iconst_0
    //   85: istore #12
    //   87: iconst_m1
    //   88: istore #13
    //   90: iconst_0
    //   91: istore #14
    //   93: iconst_1
    //   94: istore #15
    //   96: iload #12
    //   98: iload #5
    //   100: if_icmpge -> 196
    //   103: aload_2
    //   104: astore #4
    //   106: aload_0
    //   107: getfield mNativePtr : J
    //   110: iload #12
    //   112: aload #6
    //   114: iload #14
    //   116: aaload
    //   117: invokestatic nativeGetPointerProperties : (JILandroid/view/MotionEvent$PointerProperties;)V
    //   120: aload_2
    //   121: astore #4
    //   123: aload #6
    //   125: iload #14
    //   127: aaload
    //   128: getfield id : I
    //   131: istore #16
    //   133: iload #14
    //   135: istore #17
    //   137: iload #13
    //   139: istore #15
    //   141: iconst_1
    //   142: iload #16
    //   144: ishl
    //   145: iload_1
    //   146: iand
    //   147: ifeq -> 178
    //   150: iload #12
    //   152: iload #11
    //   154: if_icmpne -> 161
    //   157: iload #14
    //   159: istore #13
    //   161: aload #8
    //   163: iload #14
    //   165: iload #12
    //   167: iastore
    //   168: iload #14
    //   170: iconst_1
    //   171: iadd
    //   172: istore #17
    //   174: iload #13
    //   176: istore #15
    //   178: iinc #12, 1
    //   181: iload #17
    //   183: istore #14
    //   185: iload #15
    //   187: istore #13
    //   189: goto -> 93
    //   192: astore_2
    //   193: goto -> 666
    //   196: iload #14
    //   198: ifeq -> 652
    //   201: iload #10
    //   203: iconst_5
    //   204: if_icmpeq -> 223
    //   207: iload #10
    //   209: bipush #6
    //   211: if_icmpne -> 217
    //   214: goto -> 223
    //   217: iload #9
    //   219: istore_1
    //   220: goto -> 265
    //   223: iload #13
    //   225: ifge -> 233
    //   228: iconst_2
    //   229: istore_1
    //   230: goto -> 265
    //   233: iload #14
    //   235: iconst_1
    //   236: if_icmpne -> 256
    //   239: iload #10
    //   241: iconst_5
    //   242: if_icmpne -> 250
    //   245: iconst_0
    //   246: istore_1
    //   247: goto -> 253
    //   250: iload #15
    //   252: istore_1
    //   253: goto -> 265
    //   256: iload #13
    //   258: bipush #8
    //   260: ishl
    //   261: iload #10
    //   263: ior
    //   264: istore_1
    //   265: aload_2
    //   266: astore #4
    //   268: aload_0
    //   269: getfield mNativePtr : J
    //   272: invokestatic nativeGetHistorySize : (J)I
    //   275: istore #15
    //   277: iconst_0
    //   278: istore #5
    //   280: iload #9
    //   282: istore #12
    //   284: iload #10
    //   286: istore #17
    //   288: iload #11
    //   290: istore #9
    //   292: iload #14
    //   294: istore #13
    //   296: iload #5
    //   298: istore #14
    //   300: iload #14
    //   302: iload #15
    //   304: if_icmpgt -> 648
    //   307: iload #14
    //   309: iload #15
    //   311: if_icmpne -> 321
    //   314: ldc -2147483648
    //   316: istore #11
    //   318: goto -> 325
    //   321: iload #14
    //   323: istore #11
    //   325: iconst_0
    //   326: istore #10
    //   328: iload #10
    //   330: iload #13
    //   332: if_icmpge -> 363
    //   335: aload_2
    //   336: astore #4
    //   338: aload_0
    //   339: getfield mNativePtr : J
    //   342: aload #8
    //   344: iload #10
    //   346: iaload
    //   347: iload #11
    //   349: aload #7
    //   351: iload #10
    //   353: aaload
    //   354: invokestatic nativeGetPointerCoords : (JIILandroid/view/MotionEvent$PointerCoords;)V
    //   357: iinc #10, 1
    //   360: goto -> 328
    //   363: aload_2
    //   364: astore #4
    //   366: aload_0
    //   367: getfield mNativePtr : J
    //   370: iload #11
    //   372: invokestatic nativeGetEventTimeNanos : (JI)J
    //   375: lstore #18
    //   377: iload #14
    //   379: ifne -> 630
    //   382: aload_2
    //   383: astore #4
    //   385: aload_0
    //   386: getfield mNativePtr : J
    //   389: invokestatic nativeGetDeviceId : (J)I
    //   392: istore #5
    //   394: aload_2
    //   395: astore #4
    //   397: aload_0
    //   398: getfield mNativePtr : J
    //   401: invokestatic nativeGetSource : (J)I
    //   404: istore #20
    //   406: aload_2
    //   407: astore #4
    //   409: aload_0
    //   410: getfield mNativePtr : J
    //   413: lstore #21
    //   415: aload_2
    //   416: astore #4
    //   418: lload #21
    //   420: invokestatic nativeGetDisplayId : (J)I
    //   423: istore #10
    //   425: aload_2
    //   426: astore #4
    //   428: aload_0
    //   429: getfield mNativePtr : J
    //   432: lstore #21
    //   434: aload_2
    //   435: astore #4
    //   437: lload #21
    //   439: invokestatic nativeGetFlags : (J)I
    //   442: istore #23
    //   444: aload_2
    //   445: astore #4
    //   447: aload_0
    //   448: getfield mNativePtr : J
    //   451: lstore #21
    //   453: aload_2
    //   454: astore #4
    //   456: lload #21
    //   458: invokestatic nativeGetEdgeFlags : (J)I
    //   461: istore #24
    //   463: aload_2
    //   464: astore #4
    //   466: aload_0
    //   467: getfield mNativePtr : J
    //   470: invokestatic nativeGetMetaState : (J)I
    //   473: istore #25
    //   475: aload_2
    //   476: astore #4
    //   478: aload_0
    //   479: getfield mNativePtr : J
    //   482: lstore #21
    //   484: aload_2
    //   485: astore #4
    //   487: lload #21
    //   489: invokestatic nativeGetButtonState : (J)I
    //   492: istore #11
    //   494: aload_2
    //   495: astore #4
    //   497: aload_0
    //   498: getfield mNativePtr : J
    //   501: invokestatic nativeGetClassification : (J)I
    //   504: istore #16
    //   506: aload_2
    //   507: astore #4
    //   509: aload_0
    //   510: getfield mNativePtr : J
    //   513: lstore #21
    //   515: aload_2
    //   516: astore #4
    //   518: lload #21
    //   520: invokestatic nativeGetXOffset : (J)F
    //   523: fstore #26
    //   525: aload_2
    //   526: astore #4
    //   528: aload_0
    //   529: getfield mNativePtr : J
    //   532: invokestatic nativeGetYOffset : (J)F
    //   535: fstore #27
    //   537: aload_2
    //   538: astore #4
    //   540: aload_0
    //   541: getfield mNativePtr : J
    //   544: lstore #21
    //   546: aload_2
    //   547: astore #4
    //   549: lload #21
    //   551: invokestatic nativeGetXPrecision : (J)F
    //   554: fstore #28
    //   556: aload_2
    //   557: astore #4
    //   559: aload_0
    //   560: getfield mNativePtr : J
    //   563: invokestatic nativeGetYPrecision : (J)F
    //   566: fstore #29
    //   568: aload_2
    //   569: astore #4
    //   571: aload_0
    //   572: getfield mNativePtr : J
    //   575: lstore #21
    //   577: aload_2
    //   578: astore #4
    //   580: lload #21
    //   582: invokestatic nativeGetDownTimeNanos : (J)J
    //   585: lstore #21
    //   587: aload_2
    //   588: iload #5
    //   590: iload #20
    //   592: iload #10
    //   594: iload_1
    //   595: iload #23
    //   597: iload #24
    //   599: iload #25
    //   601: iload #11
    //   603: iload #16
    //   605: fload #26
    //   607: fload #27
    //   609: fload #28
    //   611: fload #29
    //   613: lload #21
    //   615: lload #18
    //   617: iload #13
    //   619: aload #6
    //   621: aload #7
    //   623: invokespecial initialize : (IIIIIIIIIFFFFJJI[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;)Z
    //   626: pop
    //   627: goto -> 642
    //   630: aload_2
    //   631: getfield mNativePtr : J
    //   634: lload #18
    //   636: aload #7
    //   638: iconst_0
    //   639: invokestatic nativeAddBatch : (JJ[Landroid/view/MotionEvent$PointerCoords;I)V
    //   642: iinc #14, 1
    //   645: goto -> 300
    //   648: aload_3
    //   649: monitorexit
    //   650: aload_2
    //   651: areturn
    //   652: new java/lang/IllegalArgumentException
    //   655: astore_2
    //   656: aload_2
    //   657: ldc_w 'idBits did not match any ids in the event'
    //   660: invokespecial <init> : (Ljava/lang/String;)V
    //   663: aload_2
    //   664: athrow
    //   665: astore_2
    //   666: aload_3
    //   667: monitorexit
    //   668: aload_2
    //   669: athrow
    //   670: astore_2
    //   671: goto -> 666
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3484	-> 0
    //   #3485	-> 4
    //   #3486	-> 10
    //   #3487	-> 22
    //   #3488	-> 30
    //   #3489	-> 38
    //   #3490	-> 46
    //   #3492	-> 54
    //   #3493	-> 66
    //   #3494	-> 74
    //   #3496	-> 84
    //   #3497	-> 84
    //   #3498	-> 84
    //   #3499	-> 103
    //   #3500	-> 120
    //   #3501	-> 133
    //   #3502	-> 150
    //   #3503	-> 157
    //   #3505	-> 161
    //   #3506	-> 168
    //   #3498	-> 178
    //   #3557	-> 192
    //   #3510	-> 196
    //   #3515	-> 201
    //   #3530	-> 217
    //   #3516	-> 223
    //   #3518	-> 228
    //   #3519	-> 233
    //   #3521	-> 239
    //   #3522	-> 245
    //   #3525	-> 256
    //   #3533	-> 265
    //   #3534	-> 277
    //   #3535	-> 307
    //   #3537	-> 325
    //   #3538	-> 335
    //   #3537	-> 357
    //   #3541	-> 363
    //   #3542	-> 377
    //   #3543	-> 382
    //   #3544	-> 415
    //   #3545	-> 434
    //   #3546	-> 453
    //   #3547	-> 484
    //   #3548	-> 515
    //   #3549	-> 546
    //   #3550	-> 577
    //   #3543	-> 587
    //   #3553	-> 630
    //   #3534	-> 642
    //   #3556	-> 648
    //   #3511	-> 652
    //   #3557	-> 665
    // Exception table:
    //   from	to	target	type
    //   13	22	665	finally
    //   25	30	665	finally
    //   33	38	665	finally
    //   41	46	665	finally
    //   49	54	665	finally
    //   57	66	665	finally
    //   106	120	192	finally
    //   123	133	192	finally
    //   268	277	665	finally
    //   338	357	192	finally
    //   366	377	665	finally
    //   385	394	665	finally
    //   397	406	665	finally
    //   409	415	665	finally
    //   418	425	665	finally
    //   428	434	665	finally
    //   437	444	665	finally
    //   447	453	665	finally
    //   456	463	665	finally
    //   466	475	665	finally
    //   478	484	665	finally
    //   487	494	665	finally
    //   497	506	665	finally
    //   509	515	665	finally
    //   518	525	665	finally
    //   528	537	665	finally
    //   540	546	665	finally
    //   549	556	665	finally
    //   559	568	665	finally
    //   571	577	665	finally
    //   580	587	665	finally
    //   587	627	670	finally
    //   630	642	670	finally
    //   648	650	670	finally
    //   652	665	670	finally
    //   666	668	670	finally
  }
  
  private void updateCursorPosition() {
    if (getSource() != 8194) {
      setCursorPosition(Float.NaN, Float.NaN);
      return;
    } 
    float f1 = 0.0F;
    float f2 = 0.0F;
    int i = getPointerCount();
    for (byte b = 0; b < i; b++) {
      f1 += getX(b);
      f2 += getY(b);
    } 
    f1 /= i;
    f2 /= i;
    setCursorPosition(f1, f2);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MotionEvent { action=");
    stringBuilder.append(actionToString(getAction()));
    appendUnless("0", stringBuilder, ", actionButton=", buttonStateToString(getActionButton()));
    int i = getPointerCount();
    for (byte b = 0; b < i; b++) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", id[");
      stringBuilder1.append(b);
      stringBuilder1.append("]=");
      appendUnless(Integer.valueOf(b), stringBuilder, stringBuilder1.toString(), Integer.valueOf(getPointerId(b)));
      float f1 = getX(b);
      float f2 = getY(b);
      stringBuilder.append(", x[");
      stringBuilder.append(b);
      stringBuilder.append("]=");
      stringBuilder.append(f1);
      stringBuilder.append(", y[");
      stringBuilder.append(b);
      stringBuilder.append("]=");
      stringBuilder.append(f2);
      String str3 = TOOL_TYPE_SYMBOLIC_NAMES.get(1);
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(", toolType[");
      stringBuilder2.append(b);
      stringBuilder2.append("]=");
      String str5 = stringBuilder2.toString();
      String str4 = toolTypeToString(getToolType(b));
      appendUnless(str3, stringBuilder, str5, str4);
    } 
    appendUnless("0", stringBuilder, ", buttonState=", buttonStateToString(getButtonState()));
    String str1 = classificationToString(0);
    String str2 = classificationToString(getClassification());
    appendUnless(str1, stringBuilder, ", classification=", str2);
    appendUnless("0", stringBuilder, ", metaState=", KeyEvent.metaStateToString(getMetaState()));
    appendUnless("0", stringBuilder, ", flags=0x", Integer.toHexString(getFlags()));
    appendUnless("0", stringBuilder, ", edgeFlags=0x", Integer.toHexString(getEdgeFlags()));
    appendUnless(Integer.valueOf(1), stringBuilder, ", pointerCount=", Integer.valueOf(i));
    appendUnless(Integer.valueOf(0), stringBuilder, ", historySize=", Integer.valueOf(getHistorySize()));
    stringBuilder.append(", eventTime=");
    stringBuilder.append(getEventTime());
    stringBuilder.append(", downTime=");
    stringBuilder.append(getDownTime());
    stringBuilder.append(", deviceId=");
    stringBuilder.append(getDeviceId());
    stringBuilder.append(", source=0x");
    stringBuilder.append(Integer.toHexString(getSource()));
    stringBuilder.append(", displayId=");
    stringBuilder.append(getDisplayId());
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  private static <T> void appendUnless(T paramT1, StringBuilder paramStringBuilder, String paramString, T paramT2) {
    paramStringBuilder.append(paramString);
    paramStringBuilder.append(paramT2);
  }
  
  public static String actionToString(int paramInt) {
    int i, j;
    switch (paramInt) {
      default:
        i = (0xFF00 & paramInt) >> 8;
        j = paramInt & 0xFF;
        break;
      case 12:
        return "ACTION_BUTTON_RELEASE";
      case 11:
        return "ACTION_BUTTON_PRESS";
      case 10:
        return "ACTION_HOVER_EXIT";
      case 9:
        return "ACTION_HOVER_ENTER";
      case 8:
        return "ACTION_SCROLL";
      case 7:
        return "ACTION_HOVER_MOVE";
      case 4:
        return "ACTION_OUTSIDE";
      case 3:
        return "ACTION_CANCEL";
      case 2:
        return "ACTION_MOVE";
      case 1:
        return "ACTION_UP";
      case 0:
        return "ACTION_DOWN";
    } 
    if (j != 5) {
      if (j != 6)
        return Integer.toString(paramInt); 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("ACTION_POINTER_UP(");
      stringBuilder1.append(i);
      stringBuilder1.append(")");
      return stringBuilder1.toString();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ACTION_POINTER_DOWN(");
    stringBuilder.append(i);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  public static String axisToString(int paramInt) {
    String str = nativeAxisToString(paramInt);
    if (str != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AXIS_");
      stringBuilder.append(str);
      str = stringBuilder.toString();
    } else {
      str = Integer.toString(paramInt);
    } 
    return str;
  }
  
  public static int axisFromString(String paramString) {
    String str = paramString;
    if (paramString.startsWith("AXIS_")) {
      str = paramString.substring("AXIS_".length());
      int i = nativeAxisFromString(str);
      if (i >= 0)
        return i; 
    } 
    try {
      return Integer.parseInt(str, 10);
    } catch (NumberFormatException numberFormatException) {
      return -1;
    } 
  }
  
  public static String buttonStateToString(int paramInt) {
    if (paramInt == 0)
      return "0"; 
    String str = null;
    boolean bool = false;
    int i = paramInt;
    paramInt = bool;
    while (i != 0) {
      if ((i & 0x1) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      i >>>= 1;
      String str1 = str;
      if (bool) {
        StringBuilder stringBuilder;
        str1 = BUTTON_SYMBOLIC_NAMES[paramInt];
        if (str == null) {
          if (i == 0)
            return str1; 
          stringBuilder = new StringBuilder(str1);
        } else {
          str.append('|');
          str.append((String)stringBuilder);
          str1 = str;
        } 
      } 
      paramInt++;
      str = str1;
    } 
    return str.toString();
  }
  
  public static String classificationToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2)
        return "NONE"; 
      return "DEEP_PRESS";
    } 
    return "AMBIGUOUS_GESTURE";
  }
  
  public static String toolTypeToString(int paramInt) {
    String str = TOOL_TYPE_SYMBOLIC_NAMES.get(paramInt);
    if (str == null)
      str = Integer.toString(paramInt); 
    return str;
  }
  
  public final boolean isButtonPressed(int paramInt) {
    boolean bool = false;
    if (paramInt == 0)
      return false; 
    if ((getButtonState() & paramInt) == paramInt)
      bool = true; 
    return bool;
  }
  
  static {
    CREATOR = (Parcelable.Creator<MotionEvent>)new Object();
  }
  
  public static MotionEvent createFromParcelBody(Parcel paramParcel) {
    MotionEvent motionEvent = obtain();
    motionEvent.mNativePtr = nativeReadFromParcel(motionEvent.mNativePtr, paramParcel);
    return motionEvent;
  }
  
  public final void cancel() {
    setAction(3);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(1);
    nativeWriteToParcel(this.mNativePtr, paramParcel);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Classification implements Annotation {}
  
  class PointerCoords {
    private static final int INITIAL_PACKED_AXIS_VALUES = 8;
    
    private long mPackedAxisBits;
    
    private float[] mPackedAxisValues;
    
    public float orientation;
    
    public float pressure;
    
    public float size;
    
    public float toolMajor;
    
    public float toolMinor;
    
    public float touchMajor;
    
    public float touchMinor;
    
    public float x;
    
    public float y;
    
    public PointerCoords() {}
    
    public PointerCoords(MotionEvent this$0) {
      copyFrom((PointerCoords)this$0);
    }
    
    public static PointerCoords[] createArray(int param1Int) {
      PointerCoords[] arrayOfPointerCoords = new PointerCoords[param1Int];
      for (byte b = 0; b < param1Int; b++)
        arrayOfPointerCoords[b] = new PointerCoords(); 
      return arrayOfPointerCoords;
    }
    
    public void clear() {
      this.mPackedAxisBits = 0L;
      this.x = 0.0F;
      this.y = 0.0F;
      this.pressure = 0.0F;
      this.size = 0.0F;
      this.touchMajor = 0.0F;
      this.touchMinor = 0.0F;
      this.toolMajor = 0.0F;
      this.toolMinor = 0.0F;
      this.orientation = 0.0F;
    }
    
    public void copyFrom(PointerCoords param1PointerCoords) {
      // Byte code:
      //   0: aload_1
      //   1: getfield mPackedAxisBits : J
      //   4: lstore_2
      //   5: aload_0
      //   6: lload_2
      //   7: putfield mPackedAxisBits : J
      //   10: lload_2
      //   11: lconst_0
      //   12: lcmp
      //   13: ifeq -> 75
      //   16: aload_1
      //   17: getfield mPackedAxisValues : [F
      //   20: astore #4
      //   22: lload_2
      //   23: invokestatic bitCount : (J)I
      //   26: istore #5
      //   28: aload_0
      //   29: getfield mPackedAxisValues : [F
      //   32: astore #6
      //   34: aload #6
      //   36: ifnull -> 51
      //   39: aload #6
      //   41: astore #7
      //   43: iload #5
      //   45: aload #6
      //   47: arraylength
      //   48: if_icmple -> 64
      //   51: aload #4
      //   53: arraylength
      //   54: newarray float
      //   56: astore #7
      //   58: aload_0
      //   59: aload #7
      //   61: putfield mPackedAxisValues : [F
      //   64: aload #4
      //   66: iconst_0
      //   67: aload #7
      //   69: iconst_0
      //   70: iload #5
      //   72: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
      //   75: aload_0
      //   76: aload_1
      //   77: getfield x : F
      //   80: putfield x : F
      //   83: aload_0
      //   84: aload_1
      //   85: getfield y : F
      //   88: putfield y : F
      //   91: aload_0
      //   92: aload_1
      //   93: getfield pressure : F
      //   96: putfield pressure : F
      //   99: aload_0
      //   100: aload_1
      //   101: getfield size : F
      //   104: putfield size : F
      //   107: aload_0
      //   108: aload_1
      //   109: getfield touchMajor : F
      //   112: putfield touchMajor : F
      //   115: aload_0
      //   116: aload_1
      //   117: getfield touchMinor : F
      //   120: putfield touchMinor : F
      //   123: aload_0
      //   124: aload_1
      //   125: getfield toolMajor : F
      //   128: putfield toolMajor : F
      //   131: aload_0
      //   132: aload_1
      //   133: getfield toolMinor : F
      //   136: putfield toolMinor : F
      //   139: aload_0
      //   140: aload_1
      //   141: getfield orientation : F
      //   144: putfield orientation : F
      //   147: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #3996	-> 0
      //   #3997	-> 5
      //   #3998	-> 10
      //   #3999	-> 16
      //   #4000	-> 22
      //   #4001	-> 28
      //   #4002	-> 34
      //   #4003	-> 51
      //   #4004	-> 58
      //   #4006	-> 64
      //   #4009	-> 75
      //   #4010	-> 83
      //   #4011	-> 91
      //   #4012	-> 99
      //   #4013	-> 107
      //   #4014	-> 115
      //   #4015	-> 123
      //   #4016	-> 131
      //   #4017	-> 139
      //   #4018	-> 147
    }
    
    public float getAxisValue(int param1Int) {
      switch (param1Int) {
        default:
          if (param1Int >= 0 && param1Int <= 63) {
            long l = this.mPackedAxisBits;
            if ((l & Long.MIN_VALUE >>> param1Int) == 0L)
              return 0.0F; 
            param1Int = Long.bitCount((-1L >>> param1Int ^ 0xFFFFFFFFFFFFFFFFL) & l);
            return this.mPackedAxisValues[param1Int];
          } 
          throw new IllegalArgumentException("Axis out of range.");
        case 8:
          return this.orientation;
        case 7:
          return this.toolMinor;
        case 6:
          return this.toolMajor;
        case 5:
          return this.touchMinor;
        case 4:
          return this.touchMajor;
        case 3:
          return this.size;
        case 2:
          return this.pressure;
        case 1:
          return this.y;
        case 0:
          break;
      } 
      return this.x;
    }
    
    public void setAxisValue(int param1Int, float param1Float) {
      int i;
      float[] arrayOfFloat;
      switch (param1Int) {
        default:
          if (param1Int >= 0 && param1Int <= 63) {
            long l1 = this.mPackedAxisBits;
            long l2 = Long.MIN_VALUE >>> param1Int;
            i = Long.bitCount((-1L >>> param1Int ^ 0xFFFFFFFFFFFFFFFFL) & l1);
            float[] arrayOfFloat1 = this.mPackedAxisValues;
            arrayOfFloat = arrayOfFloat1;
            if ((l1 & l2) == 0L) {
              if (arrayOfFloat1 == null) {
                arrayOfFloat = new float[8];
                this.mPackedAxisValues = arrayOfFloat;
              } else {
                param1Int = Long.bitCount(l1);
                if (param1Int < arrayOfFloat1.length) {
                  arrayOfFloat = arrayOfFloat1;
                  if (i != param1Int) {
                    System.arraycopy(arrayOfFloat1, i, arrayOfFloat1, i + 1, param1Int - i);
                    arrayOfFloat = arrayOfFloat1;
                  } 
                } else {
                  arrayOfFloat = new float[param1Int * 2];
                  System.arraycopy(arrayOfFloat1, 0, arrayOfFloat, 0, i);
                  System.arraycopy(arrayOfFloat1, i, arrayOfFloat, i + 1, param1Int - i);
                  this.mPackedAxisValues = arrayOfFloat;
                } 
              } 
              this.mPackedAxisBits = l1 | l2;
              arrayOfFloat[i] = param1Float;
              return;
            } 
            break;
          } 
          throw new IllegalArgumentException("Axis out of range.");
        case 8:
          this.orientation = param1Float;
          return;
        case 7:
          this.toolMinor = param1Float;
          return;
        case 6:
          this.toolMajor = param1Float;
          return;
        case 5:
          this.touchMinor = param1Float;
          return;
        case 4:
          this.touchMajor = param1Float;
          return;
        case 3:
          this.size = param1Float;
          return;
        case 2:
          this.pressure = param1Float;
          return;
        case 1:
          this.y = param1Float;
          return;
        case 0:
          this.x = param1Float;
          return;
      } 
      arrayOfFloat[i] = param1Float;
    }
  }
  
  class PointerProperties {
    public int id;
    
    public int toolType;
    
    public PointerProperties() {
      clear();
    }
    
    public PointerProperties(MotionEvent this$0) {
      copyFrom((PointerProperties)this$0);
    }
    
    public static PointerProperties[] createArray(int param1Int) {
      PointerProperties[] arrayOfPointerProperties = new PointerProperties[param1Int];
      for (byte b = 0; b < param1Int; b++)
        arrayOfPointerProperties[b] = new PointerProperties(); 
      return arrayOfPointerProperties;
    }
    
    public void clear() {
      this.id = -1;
      this.toolType = 0;
    }
    
    public void copyFrom(PointerProperties param1PointerProperties) {
      this.id = param1PointerProperties.id;
      this.toolType = param1PointerProperties.toolType;
    }
    
    public boolean equals(Object param1Object) {
      if (param1Object instanceof PointerProperties)
        return equals((PointerProperties)param1Object); 
      return false;
    }
    
    private boolean equals(PointerProperties param1PointerProperties) {
      boolean bool;
      if (param1PointerProperties != null && this.id == param1PointerProperties.id && this.toolType == param1PointerProperties.toolType) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int hashCode() {
      return this.id | this.toolType << 8;
    }
  }
  
  public float getXOffset() {
    return nativeGetXOffset(this.mNativePtr);
  }
  
  public float getYOffset() {
    return nativeGetYOffset(this.mNativePtr);
  }
  
  private static native void nativeAddBatch(long paramLong1, long paramLong2, PointerCoords[] paramArrayOfPointerCoords, int paramInt);
  
  private static native int nativeAxisFromString(String paramString);
  
  private static native String nativeAxisToString(int paramInt);
  
  @CriticalNative
  private static native long nativeCopy(long paramLong1, long paramLong2, boolean paramBoolean);
  
  private static native void nativeDispose(long paramLong);
  
  @CriticalNative
  private static native int nativeFindPointerIndex(long paramLong, int paramInt);
  
  @CriticalNative
  private static native int nativeGetAction(long paramLong);
  
  @CriticalNative
  private static native int nativeGetActionButton(long paramLong);
  
  @FastNative
  private static native float nativeGetAxisValue(long paramLong, int paramInt1, int paramInt2, int paramInt3);
  
  @CriticalNative
  private static native int nativeGetButtonState(long paramLong);
  
  @CriticalNative
  private static native int nativeGetClassification(long paramLong);
  
  @CriticalNative
  private static native int nativeGetDeviceId(long paramLong);
  
  @CriticalNative
  private static native int nativeGetDisplayId(long paramLong);
  
  @CriticalNative
  private static native long nativeGetDownTimeNanos(long paramLong);
  
  @CriticalNative
  private static native int nativeGetEdgeFlags(long paramLong);
  
  @FastNative
  private static native long nativeGetEventTimeNanos(long paramLong, int paramInt);
  
  @CriticalNative
  private static native int nativeGetFlags(long paramLong);
  
  @CriticalNative
  private static native int nativeGetHistorySize(long paramLong);
  
  @CriticalNative
  private static native int nativeGetId(long paramLong);
  
  @CriticalNative
  private static native int nativeGetMetaState(long paramLong);
  
  private static native void nativeGetPointerCoords(long paramLong, int paramInt1, int paramInt2, PointerCoords paramPointerCoords);
  
  @CriticalNative
  private static native int nativeGetPointerCount(long paramLong);
  
  @FastNative
  private static native int nativeGetPointerId(long paramLong, int paramInt);
  
  private static native void nativeGetPointerProperties(long paramLong, int paramInt, PointerProperties paramPointerProperties);
  
  @FastNative
  private static native float nativeGetRawAxisValue(long paramLong, int paramInt1, int paramInt2, int paramInt3);
  
  @CriticalNative
  private static native int nativeGetSource(long paramLong);
  
  @FastNative
  private static native int nativeGetToolType(long paramLong, int paramInt);
  
  @CriticalNative
  private static native float nativeGetXCursorPosition(long paramLong);
  
  @CriticalNative
  private static native float nativeGetXOffset(long paramLong);
  
  @CriticalNative
  private static native float nativeGetXPrecision(long paramLong);
  
  @CriticalNative
  private static native float nativeGetYCursorPosition(long paramLong);
  
  @CriticalNative
  private static native float nativeGetYOffset(long paramLong);
  
  @CriticalNative
  private static native float nativeGetYPrecision(long paramLong);
  
  private static native long nativeInitialize(long paramLong1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, long paramLong2, long paramLong3, int paramInt10, PointerProperties[] paramArrayOfPointerProperties, PointerCoords[] paramArrayOfPointerCoords);
  
  @CriticalNative
  private static native boolean nativeIsTouchEvent(long paramLong);
  
  @CriticalNative
  private static native void nativeOffsetLocation(long paramLong, float paramFloat1, float paramFloat2);
  
  private static native long nativeReadFromParcel(long paramLong, Parcel paramParcel);
  
  @CriticalNative
  private static native void nativeScale(long paramLong, float paramFloat);
  
  @CriticalNative
  private static native void nativeSetAction(long paramLong, int paramInt);
  
  @CriticalNative
  private static native void nativeSetActionButton(long paramLong, int paramInt);
  
  @CriticalNative
  private static native void nativeSetButtonState(long paramLong, int paramInt);
  
  @CriticalNative
  private static native void nativeSetCursorPosition(long paramLong, float paramFloat1, float paramFloat2);
  
  @CriticalNative
  private static native void nativeSetDisplayId(long paramLong, int paramInt);
  
  @CriticalNative
  private static native void nativeSetDownTimeNanos(long paramLong1, long paramLong2);
  
  @CriticalNative
  private static native void nativeSetEdgeFlags(long paramLong, int paramInt);
  
  @CriticalNative
  private static native void nativeSetFlags(long paramLong, int paramInt);
  
  @CriticalNative
  private static native void nativeSetSource(long paramLong, int paramInt);
  
  @FastNative
  private static native void nativeTransform(long paramLong, Matrix paramMatrix);
  
  private static native void nativeWriteToParcel(long paramLong, Parcel paramParcel);
}
