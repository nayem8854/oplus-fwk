package android.view;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IApplicationToken extends IInterface {
  String getName() throws RemoteException;
  
  class Default implements IApplicationToken {
    public String getName() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IApplicationToken {
    private static final String DESCRIPTOR = "android.view.IApplicationToken";
    
    static final int TRANSACTION_getName = 1;
    
    public Stub() {
      attachInterface(this, "android.view.IApplicationToken");
    }
    
    public static IApplicationToken asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.view.IApplicationToken");
      if (iInterface != null && iInterface instanceof IApplicationToken)
        return (IApplicationToken)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getName";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.view.IApplicationToken");
        return true;
      } 
      param1Parcel1.enforceInterface("android.view.IApplicationToken");
      String str = getName();
      param1Parcel2.writeNoException();
      param1Parcel2.writeString(str);
      return true;
    }
    
    private static class Proxy implements IApplicationToken {
      public static IApplicationToken sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.view.IApplicationToken";
      }
      
      public String getName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.view.IApplicationToken");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IApplicationToken.Stub.getDefaultImpl() != null)
            return IApplicationToken.Stub.getDefaultImpl().getName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IApplicationToken param1IApplicationToken) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IApplicationToken != null) {
          Proxy.sDefaultImpl = param1IApplicationToken;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IApplicationToken getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
