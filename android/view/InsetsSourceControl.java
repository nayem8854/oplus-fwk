package android.view;

import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import java.io.PrintWriter;
import java.util.function.Consumer;

public class InsetsSourceControl implements Parcelable {
  public InsetsSourceControl(int paramInt, SurfaceControl paramSurfaceControl, Point paramPoint) {
    this.mType = paramInt;
    this.mLeash = paramSurfaceControl;
    this.mSurfacePosition = paramPoint;
  }
  
  public InsetsSourceControl(InsetsSourceControl paramInsetsSourceControl) {
    this.mType = paramInsetsSourceControl.mType;
    if (paramInsetsSourceControl.mLeash != null) {
      this.mLeash = new SurfaceControl(paramInsetsSourceControl.mLeash, "InsetsSourceControl");
    } else {
      this.mLeash = null;
    } 
    this.mSurfacePosition = new Point(paramInsetsSourceControl.mSurfacePosition);
  }
  
  public int getType() {
    return this.mType;
  }
  
  public SurfaceControl getLeash() {
    return this.mLeash;
  }
  
  public InsetsSourceControl(Parcel paramParcel) {
    this.mType = paramParcel.readInt();
    this.mLeash = (SurfaceControl)paramParcel.readParcelable(null);
    this.mSurfacePosition = (Point)paramParcel.readParcelable(null);
  }
  
  public boolean setSurfacePosition(int paramInt1, int paramInt2) {
    if (this.mSurfacePosition.equals(paramInt1, paramInt2))
      return false; 
    this.mSurfacePosition.set(paramInt1, paramInt2);
    return true;
  }
  
  public Point getSurfacePosition() {
    return this.mSurfacePosition;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    paramParcel.writeParcelable(this.mLeash, 0);
    paramParcel.writeParcelable((Parcelable)this.mSurfacePosition, 0);
  }
  
  public void release(Consumer<SurfaceControl> paramConsumer) {
    SurfaceControl surfaceControl = this.mLeash;
    if (surfaceControl != null)
      paramConsumer.accept(surfaceControl); 
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("InsetsSourceControl type=");
    paramPrintWriter.print(InsetsState.typeToString(this.mType));
    paramPrintWriter.print(" mLeash=");
    paramPrintWriter.print(this.mLeash);
    paramPrintWriter.print(" mSurfacePosition=");
    paramPrintWriter.print(this.mSurfacePosition);
    paramPrintWriter.println();
  }
  
  public static final Parcelable.Creator<InsetsSourceControl> CREATOR = new Parcelable.Creator<InsetsSourceControl>() {
      public InsetsSourceControl createFromParcel(Parcel param1Parcel) {
        return new InsetsSourceControl(param1Parcel);
      }
      
      public InsetsSourceControl[] newArray(int param1Int) {
        return new InsetsSourceControl[param1Int];
      }
    };
  
  private final SurfaceControl mLeash;
  
  private final Point mSurfacePosition;
  
  private final int mType;
}
