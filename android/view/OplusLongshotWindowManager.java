package android.view;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public class OplusLongshotWindowManager extends OplusBaseWindowManager implements IOplusLongshotWindowManager {
  private static final String TAG = "OplusLongshotWindowManager";
  
  public void getFocusedWindowFrame(Rect paramRect) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      if (paramRect != null) {
        parcel1.writeInt(1);
        paramRect.writeToParcel(parcel1, 0);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(10202, parcel1, parcel2, 0);
      parcel2.readException();
      if (parcel2.readInt() != 0)
        paramRect.readFromParcel(parcel2); 
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public int getLongshotSurfaceLayer() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      this.mRemote.transact(10204, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public int getLongshotSurfaceLayerByType(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10205, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      return paramInt;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void longshotInjectInput(InputEvent paramInputEvent, int paramInt) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.view.IWindowManager");
      if (paramInputEvent != null) {
        parcel.writeInt(1);
        paramInputEvent.writeToParcel(parcel, 0);
      } else {
        parcel.writeInt(0);
      } 
      parcel.writeInt(paramInt);
      this.mRemote.transact(10203, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public void longshotNotifyConnected(boolean paramBoolean) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      boolean bool;
      parcel.writeInterfaceToken("android.view.IWindowManager");
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      parcel.writeInt(bool);
      this.mRemote.transact(10206, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public boolean isNavigationBarVisible() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10207, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isVolumeShow() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10213, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isShortcutsPanelShow() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10209, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void longshotInjectInputBegin() throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.view.IWindowManager");
      this.mRemote.transact(10210, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public boolean isKeyguardShowingAndNotOccluded() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10208, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void longshotInjectInputEnd() throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.view.IWindowManager");
      this.mRemote.transact(10211, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public IBinder getLongshotWindowByType(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10212, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readStrongBinder();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public SurfaceControl getLongshotWindowByTypeForR(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    null = null;
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10216, parcel1, parcel2, 0);
      parcel2.readException();
      Bundle bundle = parcel2.readBundle();
      if (bundle != null)
        null = (SurfaceControl)bundle.getParcelable("longshotSurfaceControl"); 
      return null;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isFloatAssistExpand() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10214, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isEdgePanelExpand() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.view.IWindowManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10215, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
}
