package android.view;

public abstract class OplusBaseWindow {
  public void setSystemBarColor(int paramInt) {}
  
  public ViewGroup getContentParent() {
    return null;
  }
  
  public CharSequence getWindowTitle() {
    return "";
  }
  
  public boolean isUseDefaultNavigationBarColor() {
    return true;
  }
}
