package android.view;

import android.graphics.Rect;
import android.os.Handler;
import android.os.Trace;
import android.util.Log;
import android.util.SparseArray;
import android.view.animation.Interpolator;
import java.util.Objects;
import java.util.function.Consumer;

public class InsetsAnimationThreadControlRunner implements InsetsAnimationControlRunner {
  private static final String TAG = "InsetsAnimThreadRunner";
  
  private final InsetsAnimationControlCallbacks mCallbacks;
  
  private final InsetsAnimationControlImpl mControl;
  
  private final Handler mMainThreadHandler;
  
  private final InsetsAnimationControlCallbacks mOuterCallbacks;
  
  private final InsetsState mState = new InsetsState();
  
  public InsetsAnimationThreadControlRunner(SparseArray<InsetsSourceControl> paramSparseArray, Rect paramRect, InsetsState paramInsetsState, WindowInsetsAnimationControlListener paramWindowInsetsAnimationControlListener, int paramInt1, InsetsAnimationControlCallbacks paramInsetsAnimationControlCallbacks, long paramLong, Interpolator paramInterpolator, int paramInt2, Handler paramHandler) {
    InsetsAnimationControlCallbacks insetsAnimationControlCallbacks = new InsetsAnimationControlCallbacks() {
        private final float[] mTmpFloat9 = new float[9];
        
        final InsetsAnimationThreadControlRunner this$0;
        
        public void startAnimation(InsetsAnimationControlImpl param1InsetsAnimationControlImpl, WindowInsetsAnimationControlListener param1WindowInsetsAnimationControlListener, int param1Int, WindowInsetsAnimation param1WindowInsetsAnimation, WindowInsetsAnimation.Bounds param1Bounds) {}
        
        public void scheduleApplyChangeInsets(InsetsAnimationControlRunner param1InsetsAnimationControlRunner) {
          InsetsAnimationThreadControlRunner.this.mControl.applyChangeInsets(InsetsAnimationThreadControlRunner.this.mState);
        }
        
        public void notifyFinished(InsetsAnimationControlRunner param1InsetsAnimationControlRunner, boolean param1Boolean) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("InsetsAsyncAnimation: ");
          stringBuilder.append(WindowInsets.Type.toString(param1InsetsAnimationControlRunner.getTypes()));
          String str = stringBuilder.toString();
          int i = param1InsetsAnimationControlRunner.getTypes();
          Trace.asyncTraceEnd(8L, str, i);
          param1InsetsAnimationControlRunner = InsetsAnimationThreadControlRunner.this;
          param1InsetsAnimationControlRunner.releaseControls(((InsetsAnimationThreadControlRunner)param1InsetsAnimationControlRunner).mControl.getControls());
          InsetsAnimationThreadControlRunner.this.mMainThreadHandler.post(new _$$Lambda$InsetsAnimationThreadControlRunner$1$HYaS_j4hzpYaXxnEg1yPA7mlZPo(this, param1Boolean));
        }
        
        public void applySurfaceParams(SyncRtSurfaceTransactionApplier.SurfaceParams... param1VarArgs) {
          if (InsetsController.DEBUG)
            Log.d("InsetsAnimThreadRunner", "applySurfaceParams"); 
          SurfaceControl.Transaction transaction = new SurfaceControl.Transaction();
          for (int i = param1VarArgs.length - 1; i >= 0; i--) {
            SyncRtSurfaceTransactionApplier.SurfaceParams surfaceParams = param1VarArgs[i];
            SyncRtSurfaceTransactionApplier.applyParams(transaction, surfaceParams, this.mTmpFloat9);
          } 
          transaction.apply();
          transaction.close();
        }
        
        public void releaseSurfaceControlFromRt(SurfaceControl param1SurfaceControl) {
          if (InsetsController.DEBUG)
            Log.d("InsetsAnimThreadRunner", "releaseSurfaceControlFromRt"); 
          param1SurfaceControl.release();
        }
        
        public void reportPerceptible(int param1Int, boolean param1Boolean) {
          InsetsAnimationThreadControlRunner.this.mMainThreadHandler.post(new _$$Lambda$InsetsAnimationThreadControlRunner$1$P1j8tXGxG0HNP_tlmNjLKchwSD0(this, param1Int, param1Boolean));
        }
      };
    this.mMainThreadHandler = paramHandler;
    this.mOuterCallbacks = paramInsetsAnimationControlCallbacks;
    this.mControl = new InsetsAnimationControlImpl(paramSparseArray, paramRect, paramInsetsState, paramWindowInsetsAnimationControlListener, paramInt1, insetsAnimationControlCallbacks, paramLong, paramInterpolator, paramInt2);
    InsetsAnimationThread.getHandler().post(new _$$Lambda$InsetsAnimationThreadControlRunner$QoTV7aDlMBWllgklmGJ_FE7p30o(this, paramInt1, paramWindowInsetsAnimationControlListener));
  }
  
  private void releaseControls(SparseArray<InsetsSourceControl> paramSparseArray) {
    for (int i = paramSparseArray.size() - 1; i >= 0; i--)
      ((InsetsSourceControl)paramSparseArray.valueAt(i)).release((Consumer<SurfaceControl>)_$$Lambda$Rl1VZmNJ0VZDLK0BAbaVGis0rrA.INSTANCE); 
  }
  
  public int getTypes() {
    return this.mControl.getTypes();
  }
  
  public void cancel() {
    Handler handler = InsetsAnimationThread.getHandler();
    InsetsAnimationControlImpl insetsAnimationControlImpl = this.mControl;
    Objects.requireNonNull(insetsAnimationControlImpl);
    handler.post(new _$$Lambda$kl_1KEyg7FwPQcg9XfREJM1iCiM(insetsAnimationControlImpl));
  }
  
  public WindowInsetsAnimation getAnimation() {
    return this.mControl.getAnimation();
  }
  
  public int getAnimationType() {
    return this.mControl.getAnimationType();
  }
}
