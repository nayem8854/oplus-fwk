package android.view;

import android.content.ComponentName;
import android.content.Intent;

public interface Menu {
  public static final int CATEGORY_ALTERNATIVE = 262144;
  
  public static final int CATEGORY_CONTAINER = 65536;
  
  public static final int CATEGORY_MASK = -65536;
  
  public static final int CATEGORY_SECONDARY = 196608;
  
  public static final int CATEGORY_SHIFT = 16;
  
  public static final int CATEGORY_SYSTEM = 131072;
  
  public static final int FIRST = 1;
  
  public static final int FLAG_ALWAYS_PERFORM_CLOSE = 2;
  
  public static final int FLAG_APPEND_TO_GROUP = 1;
  
  public static final int FLAG_PERFORM_NO_CLOSE = 1;
  
  public static final int NONE = 0;
  
  public static final int SUPPORTED_MODIFIERS_MASK = 69647;
  
  public static final int USER_MASK = 65535;
  
  public static final int USER_SHIFT = 0;
  
  MenuItem add(int paramInt);
  
  MenuItem add(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  MenuItem add(int paramInt1, int paramInt2, int paramInt3, CharSequence paramCharSequence);
  
  MenuItem add(CharSequence paramCharSequence);
  
  int addIntentOptions(int paramInt1, int paramInt2, int paramInt3, ComponentName paramComponentName, Intent[] paramArrayOfIntent, Intent paramIntent, int paramInt4, MenuItem[] paramArrayOfMenuItem);
  
  SubMenu addSubMenu(int paramInt);
  
  SubMenu addSubMenu(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  SubMenu addSubMenu(int paramInt1, int paramInt2, int paramInt3, CharSequence paramCharSequence);
  
  SubMenu addSubMenu(CharSequence paramCharSequence);
  
  void clear();
  
  void close();
  
  MenuItem findItem(int paramInt);
  
  MenuItem getItem(int paramInt);
  
  boolean hasVisibleItems();
  
  boolean isShortcutKey(int paramInt, KeyEvent paramKeyEvent);
  
  boolean performIdentifierAction(int paramInt1, int paramInt2);
  
  boolean performShortcut(int paramInt1, KeyEvent paramKeyEvent, int paramInt2);
  
  void removeGroup(int paramInt);
  
  void removeItem(int paramInt);
  
  void setGroupCheckable(int paramInt, boolean paramBoolean1, boolean paramBoolean2);
  
  default void setGroupDividerEnabled(boolean paramBoolean) {}
  
  void setGroupEnabled(int paramInt, boolean paramBoolean);
  
  void setGroupVisible(int paramInt, boolean paramBoolean);
  
  void setQwertyMode(boolean paramBoolean);
  
  int size();
}
