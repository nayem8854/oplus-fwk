package android.window;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IWindowContainerToken extends IInterface {
  class Default implements IWindowContainerToken {
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWindowContainerToken {
    private static final String DESCRIPTOR = "android.window.IWindowContainerToken";
    
    public Stub() {
      attachInterface(this, "android.window.IWindowContainerToken");
    }
    
    public static IWindowContainerToken asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.window.IWindowContainerToken");
      if (iInterface != null && iInterface instanceof IWindowContainerToken)
        return (IWindowContainerToken)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      return null;
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902)
        return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
      param1Parcel2.writeString("android.window.IWindowContainerToken");
      return true;
    }
    
    private static class Proxy implements IWindowContainerToken {
      public static IWindowContainerToken sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.window.IWindowContainerToken";
      }
    }
    
    public static boolean setDefaultImpl(IWindowContainerToken param1IWindowContainerToken) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWindowContainerToken != null) {
          Proxy.sDefaultImpl = param1IWindowContainerToken;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWindowContainerToken getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
