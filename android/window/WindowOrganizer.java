package android.window;

import android.os.RemoteException;
import android.util.Singleton;
import android.view.SurfaceControl;

public class WindowOrganizer {
  public static void applyTransaction(WindowContainerTransaction paramWindowContainerTransaction) {
    try {
      getWindowOrganizerController().applyTransaction(paramWindowContainerTransaction);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int applySyncTransaction(WindowContainerTransaction paramWindowContainerTransaction, WindowContainerTransactionCallback paramWindowContainerTransactionCallback) {
    try {
      return getWindowOrganizerController().applySyncTransaction(paramWindowContainerTransaction, paramWindowContainerTransactionCallback.mInterface);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static SurfaceControl takeScreenshot(WindowContainerToken paramWindowContainerToken) {
    try {
      SurfaceControl surfaceControl = new SurfaceControl();
      this();
      boolean bool = getWindowOrganizerController().takeScreenshot(paramWindowContainerToken, surfaceControl);
      if (bool)
        return surfaceControl; 
      return null;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  static IWindowOrganizerController getWindowOrganizerController() {
    return IWindowOrganizerControllerSingleton.get();
  }
  
  private static final Singleton<IWindowOrganizerController> IWindowOrganizerControllerSingleton = (Singleton<IWindowOrganizerController>)new Object();
}
