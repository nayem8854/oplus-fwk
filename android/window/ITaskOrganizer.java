package android.window;

import android.app.ActivityManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.SurfaceControl;

public interface ITaskOrganizer extends IInterface {
  void onBackPressedOnTaskRoot(ActivityManager.RunningTaskInfo paramRunningTaskInfo) throws RemoteException;
  
  void onTaskAppeared(ActivityManager.RunningTaskInfo paramRunningTaskInfo, SurfaceControl paramSurfaceControl) throws RemoteException;
  
  void onTaskInfoChanged(ActivityManager.RunningTaskInfo paramRunningTaskInfo) throws RemoteException;
  
  void onTaskVanished(ActivityManager.RunningTaskInfo paramRunningTaskInfo) throws RemoteException;
  
  class Default implements ITaskOrganizer {
    public void onTaskAppeared(ActivityManager.RunningTaskInfo param1RunningTaskInfo, SurfaceControl param1SurfaceControl) throws RemoteException {}
    
    public void onTaskVanished(ActivityManager.RunningTaskInfo param1RunningTaskInfo) throws RemoteException {}
    
    public void onTaskInfoChanged(ActivityManager.RunningTaskInfo param1RunningTaskInfo) throws RemoteException {}
    
    public void onBackPressedOnTaskRoot(ActivityManager.RunningTaskInfo param1RunningTaskInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITaskOrganizer {
    private static final String DESCRIPTOR = "android.window.ITaskOrganizer";
    
    static final int TRANSACTION_onBackPressedOnTaskRoot = 4;
    
    static final int TRANSACTION_onTaskAppeared = 1;
    
    static final int TRANSACTION_onTaskInfoChanged = 3;
    
    static final int TRANSACTION_onTaskVanished = 2;
    
    public Stub() {
      attachInterface(this, "android.window.ITaskOrganizer");
    }
    
    public static ITaskOrganizer asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.window.ITaskOrganizer");
      if (iInterface != null && iInterface instanceof ITaskOrganizer)
        return (ITaskOrganizer)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onBackPressedOnTaskRoot";
          } 
          return "onTaskInfoChanged";
        } 
        return "onTaskVanished";
      } 
      return "onTaskAppeared";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.window.ITaskOrganizer");
              return true;
            } 
            param1Parcel1.enforceInterface("android.window.ITaskOrganizer");
            if (param1Parcel1.readInt() != 0) {
              ActivityManager.RunningTaskInfo runningTaskInfo = (ActivityManager.RunningTaskInfo)ActivityManager.RunningTaskInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onBackPressedOnTaskRoot((ActivityManager.RunningTaskInfo)param1Parcel1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.window.ITaskOrganizer");
          if (param1Parcel1.readInt() != 0) {
            ActivityManager.RunningTaskInfo runningTaskInfo = (ActivityManager.RunningTaskInfo)ActivityManager.RunningTaskInfo.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onTaskInfoChanged((ActivityManager.RunningTaskInfo)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.window.ITaskOrganizer");
        if (param1Parcel1.readInt() != 0) {
          ActivityManager.RunningTaskInfo runningTaskInfo = (ActivityManager.RunningTaskInfo)ActivityManager.RunningTaskInfo.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onTaskVanished((ActivityManager.RunningTaskInfo)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.window.ITaskOrganizer");
      if (param1Parcel1.readInt() != 0) {
        ActivityManager.RunningTaskInfo runningTaskInfo = (ActivityManager.RunningTaskInfo)ActivityManager.RunningTaskInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        SurfaceControl surfaceControl = (SurfaceControl)SurfaceControl.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onTaskAppeared((ActivityManager.RunningTaskInfo)param1Parcel2, (SurfaceControl)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements ITaskOrganizer {
      public static ITaskOrganizer sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.window.ITaskOrganizer";
      }
      
      public void onTaskAppeared(ActivityManager.RunningTaskInfo param2RunningTaskInfo, SurfaceControl param2SurfaceControl) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.window.ITaskOrganizer");
          if (param2RunningTaskInfo != null) {
            parcel.writeInt(1);
            param2RunningTaskInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2SurfaceControl != null) {
            parcel.writeInt(1);
            param2SurfaceControl.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ITaskOrganizer.Stub.getDefaultImpl() != null) {
            ITaskOrganizer.Stub.getDefaultImpl().onTaskAppeared(param2RunningTaskInfo, param2SurfaceControl);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTaskVanished(ActivityManager.RunningTaskInfo param2RunningTaskInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.window.ITaskOrganizer");
          if (param2RunningTaskInfo != null) {
            parcel.writeInt(1);
            param2RunningTaskInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ITaskOrganizer.Stub.getDefaultImpl() != null) {
            ITaskOrganizer.Stub.getDefaultImpl().onTaskVanished(param2RunningTaskInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTaskInfoChanged(ActivityManager.RunningTaskInfo param2RunningTaskInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.window.ITaskOrganizer");
          if (param2RunningTaskInfo != null) {
            parcel.writeInt(1);
            param2RunningTaskInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && ITaskOrganizer.Stub.getDefaultImpl() != null) {
            ITaskOrganizer.Stub.getDefaultImpl().onTaskInfoChanged(param2RunningTaskInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onBackPressedOnTaskRoot(ActivityManager.RunningTaskInfo param2RunningTaskInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.window.ITaskOrganizer");
          if (param2RunningTaskInfo != null) {
            parcel.writeInt(1);
            param2RunningTaskInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && ITaskOrganizer.Stub.getDefaultImpl() != null) {
            ITaskOrganizer.Stub.getDefaultImpl().onBackPressedOnTaskRoot(param2RunningTaskInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITaskOrganizer param1ITaskOrganizer) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITaskOrganizer != null) {
          Proxy.sDefaultImpl = param1ITaskOrganizer;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITaskOrganizer getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
