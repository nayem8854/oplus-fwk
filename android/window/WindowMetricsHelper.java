package android.window;

import android.graphics.Insets;
import android.graphics.Rect;
import android.view.DisplayCutout;
import android.view.ViewRootImpl;
import android.view.WindowInsets;
import android.view.WindowMetrics;

public final class WindowMetricsHelper {
  public static Rect getBoundsExcludingNavigationBarAndCutout(WindowMetrics paramWindowMetrics) {
    Insets insets;
    WindowInsets windowInsets = paramWindowMetrics.getWindowInsets();
    if (ViewRootImpl.sNewInsetsMode == 2) {
      insets = windowInsets.getInsetsIgnoringVisibility(WindowInsets.Type.navigationBars() | WindowInsets.Type.displayCutout());
    } else {
      insets = windowInsets.getStableInsets();
      insets = Insets.of(insets.left, 0, insets.right, insets.bottom);
      DisplayCutout displayCutout = windowInsets.getDisplayCutout();
      if (displayCutout != null)
        insets = Insets.max(insets, Insets.of(displayCutout.getSafeInsets())); 
    } 
    Rect rect = new Rect(paramWindowMetrics.getBounds());
    rect.inset(insets);
    return rect;
  }
}
