package android.window;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.SurfaceControl;

public interface IDisplayAreaOrganizer extends IInterface {
  void onDisplayAreaAppeared(DisplayAreaInfo paramDisplayAreaInfo, SurfaceControl paramSurfaceControl) throws RemoteException;
  
  void onDisplayAreaInfoChanged(DisplayAreaInfo paramDisplayAreaInfo) throws RemoteException;
  
  void onDisplayAreaVanished(DisplayAreaInfo paramDisplayAreaInfo) throws RemoteException;
  
  class Default implements IDisplayAreaOrganizer {
    public void onDisplayAreaAppeared(DisplayAreaInfo param1DisplayAreaInfo, SurfaceControl param1SurfaceControl) throws RemoteException {}
    
    public void onDisplayAreaVanished(DisplayAreaInfo param1DisplayAreaInfo) throws RemoteException {}
    
    public void onDisplayAreaInfoChanged(DisplayAreaInfo param1DisplayAreaInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDisplayAreaOrganizer {
    private static final String DESCRIPTOR = "android.window.IDisplayAreaOrganizer";
    
    static final int TRANSACTION_onDisplayAreaAppeared = 1;
    
    static final int TRANSACTION_onDisplayAreaInfoChanged = 3;
    
    static final int TRANSACTION_onDisplayAreaVanished = 2;
    
    public Stub() {
      attachInterface(this, "android.window.IDisplayAreaOrganizer");
    }
    
    public static IDisplayAreaOrganizer asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.window.IDisplayAreaOrganizer");
      if (iInterface != null && iInterface instanceof IDisplayAreaOrganizer)
        return (IDisplayAreaOrganizer)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onDisplayAreaInfoChanged";
        } 
        return "onDisplayAreaVanished";
      } 
      return "onDisplayAreaAppeared";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.window.IDisplayAreaOrganizer");
            return true;
          } 
          param1Parcel1.enforceInterface("android.window.IDisplayAreaOrganizer");
          if (param1Parcel1.readInt() != 0) {
            DisplayAreaInfo displayAreaInfo = (DisplayAreaInfo)DisplayAreaInfo.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onDisplayAreaInfoChanged((DisplayAreaInfo)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.window.IDisplayAreaOrganizer");
        if (param1Parcel1.readInt() != 0) {
          DisplayAreaInfo displayAreaInfo = (DisplayAreaInfo)DisplayAreaInfo.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onDisplayAreaVanished((DisplayAreaInfo)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.window.IDisplayAreaOrganizer");
      if (param1Parcel1.readInt() != 0) {
        DisplayAreaInfo displayAreaInfo = (DisplayAreaInfo)DisplayAreaInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        SurfaceControl surfaceControl = (SurfaceControl)SurfaceControl.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onDisplayAreaAppeared((DisplayAreaInfo)param1Parcel2, (SurfaceControl)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IDisplayAreaOrganizer {
      public static IDisplayAreaOrganizer sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.window.IDisplayAreaOrganizer";
      }
      
      public void onDisplayAreaAppeared(DisplayAreaInfo param2DisplayAreaInfo, SurfaceControl param2SurfaceControl) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.window.IDisplayAreaOrganizer");
          if (param2DisplayAreaInfo != null) {
            parcel.writeInt(1);
            param2DisplayAreaInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2SurfaceControl != null) {
            parcel.writeInt(1);
            param2SurfaceControl.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IDisplayAreaOrganizer.Stub.getDefaultImpl() != null) {
            IDisplayAreaOrganizer.Stub.getDefaultImpl().onDisplayAreaAppeared(param2DisplayAreaInfo, param2SurfaceControl);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDisplayAreaVanished(DisplayAreaInfo param2DisplayAreaInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.window.IDisplayAreaOrganizer");
          if (param2DisplayAreaInfo != null) {
            parcel.writeInt(1);
            param2DisplayAreaInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IDisplayAreaOrganizer.Stub.getDefaultImpl() != null) {
            IDisplayAreaOrganizer.Stub.getDefaultImpl().onDisplayAreaVanished(param2DisplayAreaInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDisplayAreaInfoChanged(DisplayAreaInfo param2DisplayAreaInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.window.IDisplayAreaOrganizer");
          if (param2DisplayAreaInfo != null) {
            parcel.writeInt(1);
            param2DisplayAreaInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IDisplayAreaOrganizer.Stub.getDefaultImpl() != null) {
            IDisplayAreaOrganizer.Stub.getDefaultImpl().onDisplayAreaInfoChanged(param2DisplayAreaInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDisplayAreaOrganizer param1IDisplayAreaOrganizer) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDisplayAreaOrganizer != null) {
          Proxy.sDefaultImpl = param1IDisplayAreaOrganizer;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDisplayAreaOrganizer getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
