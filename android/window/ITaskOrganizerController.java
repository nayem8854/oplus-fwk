package android.window;

import android.app.ActivityManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface ITaskOrganizerController extends IInterface {
  ActivityManager.RunningTaskInfo createRootTask(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean deleteRootTask(WindowContainerToken paramWindowContainerToken) throws RemoteException;
  
  List<ActivityManager.RunningTaskInfo> getChildTasks(WindowContainerToken paramWindowContainerToken, int[] paramArrayOfint) throws RemoteException;
  
  WindowContainerToken getImeTarget(int paramInt) throws RemoteException;
  
  List<ActivityManager.RunningTaskInfo> getRootTasks(int paramInt, int[] paramArrayOfint) throws RemoteException;
  
  void registerTaskOrganizer(ITaskOrganizer paramITaskOrganizer, int paramInt) throws RemoteException;
  
  void setInterceptBackPressedOnTaskRoot(ITaskOrganizer paramITaskOrganizer, boolean paramBoolean) throws RemoteException;
  
  void setLaunchRoot(int paramInt, WindowContainerToken paramWindowContainerToken) throws RemoteException;
  
  void unregisterTaskOrganizer(ITaskOrganizer paramITaskOrganizer) throws RemoteException;
  
  class Default implements ITaskOrganizerController {
    public void registerTaskOrganizer(ITaskOrganizer param1ITaskOrganizer, int param1Int) throws RemoteException {}
    
    public void unregisterTaskOrganizer(ITaskOrganizer param1ITaskOrganizer) throws RemoteException {}
    
    public ActivityManager.RunningTaskInfo createRootTask(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public boolean deleteRootTask(WindowContainerToken param1WindowContainerToken) throws RemoteException {
      return false;
    }
    
    public List<ActivityManager.RunningTaskInfo> getChildTasks(WindowContainerToken param1WindowContainerToken, int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public List<ActivityManager.RunningTaskInfo> getRootTasks(int param1Int, int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public WindowContainerToken getImeTarget(int param1Int) throws RemoteException {
      return null;
    }
    
    public void setLaunchRoot(int param1Int, WindowContainerToken param1WindowContainerToken) throws RemoteException {}
    
    public void setInterceptBackPressedOnTaskRoot(ITaskOrganizer param1ITaskOrganizer, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITaskOrganizerController {
    private static final String DESCRIPTOR = "android.window.ITaskOrganizerController";
    
    static final int TRANSACTION_createRootTask = 3;
    
    static final int TRANSACTION_deleteRootTask = 4;
    
    static final int TRANSACTION_getChildTasks = 5;
    
    static final int TRANSACTION_getImeTarget = 7;
    
    static final int TRANSACTION_getRootTasks = 6;
    
    static final int TRANSACTION_registerTaskOrganizer = 1;
    
    static final int TRANSACTION_setInterceptBackPressedOnTaskRoot = 9;
    
    static final int TRANSACTION_setLaunchRoot = 8;
    
    static final int TRANSACTION_unregisterTaskOrganizer = 2;
    
    public Stub() {
      attachInterface(this, "android.window.ITaskOrganizerController");
    }
    
    public static ITaskOrganizerController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.window.ITaskOrganizerController");
      if (iInterface != null && iInterface instanceof ITaskOrganizerController)
        return (ITaskOrganizerController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "setInterceptBackPressedOnTaskRoot";
        case 8:
          return "setLaunchRoot";
        case 7:
          return "getImeTarget";
        case 6:
          return "getRootTasks";
        case 5:
          return "getChildTasks";
        case 4:
          return "deleteRootTask";
        case 3:
          return "createRootTask";
        case 2:
          return "unregisterTaskOrganizer";
        case 1:
          break;
      } 
      return "registerTaskOrganizer";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        WindowContainerToken windowContainerToken;
        int[] arrayOfInt2;
        List<ActivityManager.RunningTaskInfo> list2;
        int[] arrayOfInt1;
        List<ActivityManager.RunningTaskInfo> list1;
        ActivityManager.RunningTaskInfo runningTaskInfo;
        ITaskOrganizer iTaskOrganizer1;
        boolean bool1 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.window.ITaskOrganizerController");
            iTaskOrganizer2 = ITaskOrganizer.Stub.asInterface(param1Parcel1.readStrongBinder());
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            setInterceptBackPressedOnTaskRoot(iTaskOrganizer2, bool1);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.window.ITaskOrganizerController");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              WindowContainerToken windowContainerToken1 = (WindowContainerToken)WindowContainerToken.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setLaunchRoot(param1Int1, (WindowContainerToken)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.window.ITaskOrganizerController");
            param1Int1 = param1Parcel1.readInt();
            windowContainerToken = getImeTarget(param1Int1);
            param1Parcel2.writeNoException();
            if (windowContainerToken != null) {
              param1Parcel2.writeInt(1);
              windowContainerToken.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            windowContainerToken.enforceInterface("android.window.ITaskOrganizerController");
            param1Int1 = windowContainerToken.readInt();
            arrayOfInt2 = windowContainerToken.createIntArray();
            list2 = getRootTasks(param1Int1, arrayOfInt2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list2);
            return true;
          case 5:
            list2.enforceInterface("android.window.ITaskOrganizerController");
            if (list2.readInt() != 0) {
              WindowContainerToken windowContainerToken1 = (WindowContainerToken)WindowContainerToken.CREATOR.createFromParcel((Parcel)list2);
            } else {
              iTaskOrganizer2 = null;
            } 
            arrayOfInt1 = list2.createIntArray();
            list1 = getChildTasks((WindowContainerToken)iTaskOrganizer2, arrayOfInt1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 4:
            list1.enforceInterface("android.window.ITaskOrganizerController");
            if (list1.readInt() != 0) {
              WindowContainerToken windowContainerToken1 = (WindowContainerToken)WindowContainerToken.CREATOR.createFromParcel((Parcel)list1);
            } else {
              list1 = null;
            } 
            bool = deleteRootTask((WindowContainerToken)list1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 3:
            list1.enforceInterface("android.window.ITaskOrganizerController");
            i = list1.readInt();
            param1Int2 = list1.readInt();
            runningTaskInfo = createRootTask(i, param1Int2);
            param1Parcel2.writeNoException();
            if (runningTaskInfo != null) {
              param1Parcel2.writeInt(1);
              runningTaskInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            runningTaskInfo.enforceInterface("android.window.ITaskOrganizerController");
            iTaskOrganizer1 = ITaskOrganizer.Stub.asInterface(runningTaskInfo.readStrongBinder());
            unregisterTaskOrganizer(iTaskOrganizer1);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iTaskOrganizer1.enforceInterface("android.window.ITaskOrganizerController");
        ITaskOrganizer iTaskOrganizer2 = ITaskOrganizer.Stub.asInterface(iTaskOrganizer1.readStrongBinder());
        int i = iTaskOrganizer1.readInt();
        registerTaskOrganizer(iTaskOrganizer2, i);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.window.ITaskOrganizerController");
      return true;
    }
    
    private static class Proxy implements ITaskOrganizerController {
      public static ITaskOrganizerController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.window.ITaskOrganizerController";
      }
      
      public void registerTaskOrganizer(ITaskOrganizer param2ITaskOrganizer, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.window.ITaskOrganizerController");
          if (param2ITaskOrganizer != null) {
            iBinder = param2ITaskOrganizer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ITaskOrganizerController.Stub.getDefaultImpl() != null) {
            ITaskOrganizerController.Stub.getDefaultImpl().registerTaskOrganizer(param2ITaskOrganizer, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterTaskOrganizer(ITaskOrganizer param2ITaskOrganizer) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.window.ITaskOrganizerController");
          if (param2ITaskOrganizer != null) {
            iBinder = param2ITaskOrganizer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ITaskOrganizerController.Stub.getDefaultImpl() != null) {
            ITaskOrganizerController.Stub.getDefaultImpl().unregisterTaskOrganizer(param2ITaskOrganizer);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ActivityManager.RunningTaskInfo createRootTask(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ActivityManager.RunningTaskInfo runningTaskInfo;
          parcel1.writeInterfaceToken("android.window.ITaskOrganizerController");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ITaskOrganizerController.Stub.getDefaultImpl() != null) {
            runningTaskInfo = ITaskOrganizerController.Stub.getDefaultImpl().createRootTask(param2Int1, param2Int2);
            return runningTaskInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            runningTaskInfo = (ActivityManager.RunningTaskInfo)ActivityManager.RunningTaskInfo.CREATOR.createFromParcel(parcel2);
          } else {
            runningTaskInfo = null;
          } 
          return runningTaskInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean deleteRootTask(WindowContainerToken param2WindowContainerToken) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.window.ITaskOrganizerController");
          boolean bool1 = true;
          if (param2WindowContainerToken != null) {
            parcel1.writeInt(1);
            param2WindowContainerToken.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool2 && ITaskOrganizerController.Stub.getDefaultImpl() != null) {
            bool1 = ITaskOrganizerController.Stub.getDefaultImpl().deleteRootTask(param2WindowContainerToken);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ActivityManager.RunningTaskInfo> getChildTasks(WindowContainerToken param2WindowContainerToken, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.window.ITaskOrganizerController");
          if (param2WindowContainerToken != null) {
            parcel1.writeInt(1);
            param2WindowContainerToken.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ITaskOrganizerController.Stub.getDefaultImpl() != null)
            return ITaskOrganizerController.Stub.getDefaultImpl().getChildTasks(param2WindowContainerToken, param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ActivityManager.RunningTaskInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ActivityManager.RunningTaskInfo> getRootTasks(int param2Int, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.window.ITaskOrganizerController");
          parcel1.writeInt(param2Int);
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ITaskOrganizerController.Stub.getDefaultImpl() != null)
            return ITaskOrganizerController.Stub.getDefaultImpl().getRootTasks(param2Int, param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ActivityManager.RunningTaskInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public WindowContainerToken getImeTarget(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          WindowContainerToken windowContainerToken;
          parcel1.writeInterfaceToken("android.window.ITaskOrganizerController");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ITaskOrganizerController.Stub.getDefaultImpl() != null) {
            windowContainerToken = ITaskOrganizerController.Stub.getDefaultImpl().getImeTarget(param2Int);
            return windowContainerToken;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            windowContainerToken = (WindowContainerToken)WindowContainerToken.CREATOR.createFromParcel(parcel2);
          } else {
            windowContainerToken = null;
          } 
          return windowContainerToken;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setLaunchRoot(int param2Int, WindowContainerToken param2WindowContainerToken) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.window.ITaskOrganizerController");
          parcel1.writeInt(param2Int);
          if (param2WindowContainerToken != null) {
            parcel1.writeInt(1);
            param2WindowContainerToken.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ITaskOrganizerController.Stub.getDefaultImpl() != null) {
            ITaskOrganizerController.Stub.getDefaultImpl().setLaunchRoot(param2Int, param2WindowContainerToken);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInterceptBackPressedOnTaskRoot(ITaskOrganizer param2ITaskOrganizer, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel1.writeInterfaceToken("android.window.ITaskOrganizerController");
          if (param2ITaskOrganizer != null) {
            iBinder = param2ITaskOrganizer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool1 && ITaskOrganizerController.Stub.getDefaultImpl() != null) {
            ITaskOrganizerController.Stub.getDefaultImpl().setInterceptBackPressedOnTaskRoot(param2ITaskOrganizer, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITaskOrganizerController param1ITaskOrganizerController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITaskOrganizerController != null) {
          Proxy.sDefaultImpl = param1ITaskOrganizerController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITaskOrganizerController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
