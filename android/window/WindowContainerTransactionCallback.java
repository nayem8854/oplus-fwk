package android.window;

import android.view.SurfaceControl;

public abstract class WindowContainerTransactionCallback {
  final IWindowContainerTransactionCallback mInterface = (IWindowContainerTransactionCallback)new Object(this);
  
  public abstract void onTransactionReady(int paramInt, SurfaceControl.Transaction paramTransaction);
}
