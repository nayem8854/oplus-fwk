package android.window;

import android.app.ActivityOptions;
import android.app.ActivityTaskManager;
import android.app.IActivityTaskManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LauncherApps;
import android.content.pm.ShortcutInfo;
import android.graphics.Insets;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Region;
import android.hardware.display.VirtualDisplay;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.UserHandle;
import android.view.IWindow;
import android.view.IWindowSession;
import android.view.SurfaceControl;
import android.view.WindowManagerGlobal;
import dalvik.system.CloseGuard;

public abstract class TaskEmbedder {
  private static final String TAG = "TaskEmbedder";
  
  public static interface Host {
    boolean canReceivePointerEvents();
    
    int getHeight();
    
    Point getPositionInWindow();
    
    Rect getScreenBounds();
    
    Matrix getScreenToTaskMatrix();
    
    Region getTapExcludeRegion();
    
    int getWidth();
    
    IWindow getWindow();
    
    void onTaskBackgroundColorChanged(TaskEmbedder param1TaskEmbedder, int param1Int);
    
    boolean post(Runnable param1Runnable);
  }
  
  public static interface Listener {
    default void onInitialized() {}
    
    default void onReleased() {}
    
    default void onTaskCreated(int param1Int, ComponentName param1ComponentName) {}
    
    default void onTaskVisibilityChanged(int param1Int, boolean param1Boolean) {}
    
    default void onTaskMovedToFront(int param1Int) {}
    
    default void onTaskRemovalStarted(int param1Int) {}
    
    default void onBackPressedOnTaskRoot(int param1Int) {}
  }
  
  protected IActivityTaskManager mActivityTaskManager = ActivityTaskManager.getService();
  
  protected final Context mContext;
  
  private final CloseGuard mGuard = CloseGuard.get();
  
  protected Host mHost;
  
  protected Listener mListener;
  
  protected boolean mOpened;
  
  protected SurfaceControl mSurfaceControl;
  
  protected SurfaceControl.Transaction mTransaction;
  
  public TaskEmbedder(Context paramContext, Host paramHost) {
    this.mContext = paramContext;
    this.mHost = paramHost;
  }
  
  public boolean initialize(SurfaceControl paramSurfaceControl) {
    if (!isInitialized()) {
      this.mTransaction = new SurfaceControl.Transaction();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("TaskEmbedder - ");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      String str = stringBuilder.toString();
      SurfaceControl.Builder builder2 = new SurfaceControl.Builder();
      builder2 = builder2.setContainerLayer();
      SurfaceControl.Builder builder1 = builder2.setParent(paramSurfaceControl);
      builder1 = builder1.setName(str);
      builder1 = builder1.setCallsite("TaskEmbedder.initialize");
      this.mSurfaceControl = builder1.build();
      if (!onInitialize())
        return false; 
      if (this.mListener != null && isInitialized())
        this.mListener.onInitialized(); 
      this.mOpened = true;
      this.mGuard.open("release");
      this.mTransaction.show(getSurfaceControl()).apply();
      return true;
    } 
    throw new IllegalStateException("Trying to initialize for the second time.");
  }
  
  public boolean onInitialize() {
    updateLocationAndTapExcludeRegion();
    return true;
  }
  
  protected boolean onRelease() {
    clearTapExcludeRegion();
    return true;
  }
  
  public void start() {
    updateLocationAndTapExcludeRegion();
  }
  
  public void stop() {
    clearTapExcludeRegion();
  }
  
  public void notifyBoundsChanged() {
    updateLocationAndTapExcludeRegion();
  }
  
  public void resizeTask(int paramInt1, int paramInt2) {}
  
  public boolean gatherTransparentRegion(Region paramRegion) {
    return false;
  }
  
  public SurfaceControl getSurfaceControl() {
    return this.mSurfaceControl;
  }
  
  public int getDisplayId() {
    return -1;
  }
  
  public VirtualDisplay getVirtualDisplay() {
    return null;
  }
  
  public void setForwardedInsets(Insets paramInsets) {}
  
  protected void updateLocationAndTapExcludeRegion() {
    if (!isInitialized() || this.mHost.getWindow() == null)
      return; 
    applyTapExcludeRegion(this.mHost.getWindow(), this.mHost.getTapExcludeRegion());
  }
  
  private void applyTapExcludeRegion(IWindow paramIWindow, Region paramRegion) {
    try {
      IWindowSession iWindowSession = WindowManagerGlobal.getWindowSession();
      iWindowSession.updateTapExcludeRegion(paramIWindow, paramRegion);
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  private void clearTapExcludeRegion() {
    if (!isInitialized() || this.mHost.getWindow() == null)
      return; 
    applyTapExcludeRegion(this.mHost.getWindow(), null);
  }
  
  public void setListener(Listener paramListener) {
    this.mListener = paramListener;
    if (paramListener != null && isInitialized())
      this.mListener.onInitialized(); 
  }
  
  public void startActivity(Intent paramIntent) {
    ActivityOptions activityOptions = prepareActivityOptions(null);
    this.mContext.startActivity(paramIntent, activityOptions.toBundle());
  }
  
  public void startActivity(Intent paramIntent, UserHandle paramUserHandle) {
    ActivityOptions activityOptions = prepareActivityOptions(null);
    this.mContext.startActivityAsUser(paramIntent, activityOptions.toBundle(), paramUserHandle);
  }
  
  public void startActivity(PendingIntent paramPendingIntent) {
    ActivityOptions activityOptions = prepareActivityOptions(null);
    try {
      Bundle bundle = activityOptions.toBundle();
      paramPendingIntent.send(null, 0, null, null, null, null, bundle);
      return;
    } catch (android.app.PendingIntent.CanceledException canceledException) {
      throw new RuntimeException(canceledException);
    } 
  }
  
  public void startActivity(PendingIntent paramPendingIntent, Intent paramIntent, ActivityOptions paramActivityOptions) {
    prepareActivityOptions(paramActivityOptions);
    try {
      Context context = this.mContext;
      Bundle bundle = paramActivityOptions.toBundle();
      paramPendingIntent.send(context, 0, paramIntent, null, null, null, bundle);
      return;
    } catch (android.app.PendingIntent.CanceledException canceledException) {
      throw new RuntimeException(canceledException);
    } 
  }
  
  public void startShortcutActivity(ShortcutInfo paramShortcutInfo, ActivityOptions paramActivityOptions, Rect paramRect) {
    Context context = this.mContext;
    LauncherApps launcherApps = (LauncherApps)context.getSystemService("launcherapps");
    prepareActivityOptions(paramActivityOptions);
    launcherApps.startShortcut(paramShortcutInfo, paramRect, paramActivityOptions.toBundle());
  }
  
  protected ActivityOptions prepareActivityOptions(ActivityOptions paramActivityOptions) {
    if (isInitialized()) {
      ActivityOptions activityOptions = paramActivityOptions;
      if (paramActivityOptions == null)
        activityOptions = ActivityOptions.makeBasic(); 
      return activityOptions;
    } 
    throw new IllegalStateException("Trying to start activity before ActivityView is ready.");
  }
  
  public void release() {
    if (isInitialized()) {
      performRelease();
      return;
    } 
    throw new IllegalStateException("Trying to release container that is not initialized.");
  }
  
  private boolean performRelease() {
    if (!this.mOpened)
      return false; 
    this.mTransaction.reparent(this.mSurfaceControl, null).apply();
    this.mSurfaceControl.release();
    boolean bool = onRelease();
    Listener listener = this.mListener;
    if (listener != null && bool)
      listener.onReleased(); 
    this.mOpened = false;
    this.mGuard.close();
    return true;
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mGuard != null) {
        this.mGuard.warnIfOpen();
        performRelease();
      } 
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public abstract int getId();
  
  public abstract boolean isInitialized();
  
  public abstract void performBackPress();
}
