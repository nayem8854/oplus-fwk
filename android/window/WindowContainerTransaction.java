package android.window;

import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArrayMap;
import android.view.SurfaceControl;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class WindowContainerTransaction implements Parcelable {
  private final ArrayMap<IBinder, Change> mChanges = new ArrayMap<>();
  
  private final ArrayList<HierarchyOp> mHierarchyOps = new ArrayList<>();
  
  private WindowContainerTransaction(Parcel paramParcel) {
    paramParcel.readMap(this.mChanges, null);
    paramParcel.readList(this.mHierarchyOps, null);
  }
  
  private Change getOrCreateChange(IBinder paramIBinder) {
    Change change1 = this.mChanges.get(paramIBinder);
    Change change2 = change1;
    if (change1 == null) {
      change2 = new Change();
      this.mChanges.put(paramIBinder, change2);
    } 
    return change2;
  }
  
  public WindowContainerTransaction setBounds(WindowContainerToken paramWindowContainerToken, Rect paramRect) {
    Change change = getOrCreateChange(paramWindowContainerToken.asBinder());
    change.mConfiguration.windowConfiguration.setBounds(paramRect);
    Change.access$176(change, 536870912);
    Change.access$276(change, 1);
    return this;
  }
  
  public WindowContainerTransaction setAppBounds(WindowContainerToken paramWindowContainerToken, Rect paramRect) {
    Change change = getOrCreateChange(paramWindowContainerToken.asBinder());
    change.mConfiguration.windowConfiguration.setAppBounds(paramRect);
    Change.access$176(change, 536870912);
    Change.access$276(change, 2);
    return this;
  }
  
  public WindowContainerTransaction setScreenSizeDp(WindowContainerToken paramWindowContainerToken, int paramInt1, int paramInt2) {
    Change change = getOrCreateChange(paramWindowContainerToken.asBinder());
    change.mConfiguration.screenWidthDp = paramInt1;
    change.mConfiguration.screenHeightDp = paramInt2;
    Change.access$176(change, 1024);
    return this;
  }
  
  public WindowContainerTransaction scheduleFinishEnterPip(WindowContainerToken paramWindowContainerToken, Rect paramRect) {
    Change change = getOrCreateChange(paramWindowContainerToken.asBinder());
    Change.access$302(change, new Rect(paramRect));
    Change.access$476(change, 4);
    return this;
  }
  
  public WindowContainerTransaction setBoundsChangeTransaction(WindowContainerToken paramWindowContainerToken, SurfaceControl.Transaction paramTransaction) {
    Change change = getOrCreateChange(paramWindowContainerToken.asBinder());
    Change.access$502(change, paramTransaction);
    Change.access$476(change, 2);
    return this;
  }
  
  public WindowContainerTransaction setBoundsChangeTransaction(WindowContainerToken paramWindowContainerToken, Rect paramRect) {
    Change change = getOrCreateChange(paramWindowContainerToken.asBinder());
    if (change.mBoundsChangeSurfaceBounds == null)
      Change.access$602(change, new Rect()); 
    change.mBoundsChangeSurfaceBounds.set(paramRect);
    Change.access$476(change, 16);
    return this;
  }
  
  public WindowContainerTransaction setActivityWindowingMode(WindowContainerToken paramWindowContainerToken, int paramInt) {
    Change change = getOrCreateChange(paramWindowContainerToken.asBinder());
    Change.access$702(change, paramInt);
    return this;
  }
  
  public WindowContainerTransaction setWindowingMode(WindowContainerToken paramWindowContainerToken, int paramInt) {
    Change change = getOrCreateChange(paramWindowContainerToken.asBinder());
    Change.access$802(change, paramInt);
    return this;
  }
  
  public WindowContainerTransaction setFocusable(WindowContainerToken paramWindowContainerToken, boolean paramBoolean) {
    Change change = getOrCreateChange(paramWindowContainerToken.asBinder());
    Change.access$902(change, paramBoolean);
    Change.access$476(change, 1);
    return this;
  }
  
  public WindowContainerTransaction setHidden(WindowContainerToken paramWindowContainerToken, boolean paramBoolean) {
    Change change = getOrCreateChange(paramWindowContainerToken.asBinder());
    Change.access$1002(change, paramBoolean);
    Change.access$476(change, 8);
    return this;
  }
  
  public WindowContainerTransaction setSmallestScreenWidthDp(WindowContainerToken paramWindowContainerToken, int paramInt) {
    Change change = getOrCreateChange(paramWindowContainerToken.asBinder());
    change.mConfiguration.smallestScreenWidthDp = paramInt;
    Change.access$176(change, 2048);
    return this;
  }
  
  public WindowContainerTransaction reparent(WindowContainerToken paramWindowContainerToken1, WindowContainerToken paramWindowContainerToken2, boolean paramBoolean) {
    IBinder iBinder1;
    ArrayList<HierarchyOp> arrayList = this.mHierarchyOps;
    IBinder iBinder2 = paramWindowContainerToken1.asBinder();
    if (paramWindowContainerToken2 == null) {
      paramWindowContainerToken1 = null;
    } else {
      iBinder1 = paramWindowContainerToken2.asBinder();
    } 
    HierarchyOp hierarchyOp = new HierarchyOp(iBinder2, iBinder1, paramBoolean);
    arrayList.add(hierarchyOp);
    return this;
  }
  
  public WindowContainerTransaction reorder(WindowContainerToken paramWindowContainerToken, boolean paramBoolean) {
    this.mHierarchyOps.add(new HierarchyOp(paramWindowContainerToken.asBinder(), paramBoolean));
    return this;
  }
  
  public void merge(WindowContainerTransaction paramWindowContainerTransaction, boolean paramBoolean) {
    byte b;
    int i;
    for (b = 0, i = paramWindowContainerTransaction.mChanges.size(); b < i; b++) {
      IBinder iBinder = paramWindowContainerTransaction.mChanges.keyAt(b);
      Change change1 = this.mChanges.get(iBinder);
      Change change2 = change1;
      if (change1 == null) {
        change2 = new Change();
        this.mChanges.put(iBinder, change2);
      } 
      change2.merge(paramWindowContainerTransaction.mChanges.valueAt(b), paramBoolean);
    } 
    for (b = 0, i = paramWindowContainerTransaction.mHierarchyOps.size(); b < i; b++) {
      HierarchyOp hierarchyOp;
      ArrayList<HierarchyOp> arrayList = this.mHierarchyOps;
      if (paramBoolean) {
        hierarchyOp = paramWindowContainerTransaction.mHierarchyOps.get(b);
      } else {
        hierarchyOp = new HierarchyOp(paramWindowContainerTransaction.mHierarchyOps.get(b));
      } 
      arrayList.add(hierarchyOp);
    } 
  }
  
  public Map<IBinder, Change> getChanges() {
    return this.mChanges;
  }
  
  public List<HierarchyOp> getHierarchyOps() {
    return this.mHierarchyOps;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("WindowContainerTransaction { changes = ");
    stringBuilder.append(this.mChanges);
    stringBuilder.append(" hops = ");
    stringBuilder.append(this.mHierarchyOps);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeMap(this.mChanges);
    paramParcel.writeList(this.mHierarchyOps);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<WindowContainerTransaction> CREATOR = new Parcelable.Creator<WindowContainerTransaction>() {
      public WindowContainerTransaction createFromParcel(Parcel param1Parcel) {
        return new WindowContainerTransaction(param1Parcel);
      }
      
      public WindowContainerTransaction[] newArray(int param1Int) {
        return new WindowContainerTransaction[param1Int];
      }
    };
  
  public WindowContainerTransaction() {}
  
  public static class Change implements Parcelable {
    public static final int CHANGE_BOUNDS_TRANSACTION = 2;
    
    public static final int CHANGE_BOUNDS_TRANSACTION_RECT = 16;
    
    public static final int CHANGE_FOCUSABLE = 1;
    
    public static final int CHANGE_HIDDEN = 8;
    
    public static final int CHANGE_PIP_CALLBACK = 4;
    
    public Change() {
      this.mConfiguration = new Configuration();
      this.mFocusable = true;
      this.mHidden = false;
      this.mChangeMask = 0;
      this.mConfigSetMask = 0;
      this.mWindowSetMask = 0;
      this.mPinnedBounds = null;
      this.mBoundsChangeTransaction = null;
      this.mBoundsChangeSurfaceBounds = null;
      this.mActivityWindowingMode = -1;
      this.mWindowingMode = -1;
    }
    
    protected Change(Parcel param1Parcel) {
      Configuration configuration = new Configuration();
      this.mFocusable = true;
      this.mHidden = false;
      this.mChangeMask = 0;
      this.mConfigSetMask = 0;
      this.mWindowSetMask = 0;
      this.mPinnedBounds = null;
      this.mBoundsChangeTransaction = null;
      this.mBoundsChangeSurfaceBounds = null;
      this.mActivityWindowingMode = -1;
      this.mWindowingMode = -1;
      configuration.readFromParcel(param1Parcel);
      this.mFocusable = param1Parcel.readBoolean();
      this.mHidden = param1Parcel.readBoolean();
      this.mChangeMask = param1Parcel.readInt();
      this.mConfigSetMask = param1Parcel.readInt();
      this.mWindowSetMask = param1Parcel.readInt();
      if ((this.mChangeMask & 0x4) != 0) {
        Rect rect = new Rect();
        rect.readFromParcel(param1Parcel);
      } 
      if ((this.mChangeMask & 0x2) != 0) {
        Parcelable.Creator<SurfaceControl.Transaction> creator = SurfaceControl.Transaction.CREATOR;
        this.mBoundsChangeTransaction = (SurfaceControl.Transaction)creator.createFromParcel(param1Parcel);
      } 
      if ((this.mChangeMask & 0x10) != 0) {
        Rect rect = new Rect();
        rect.readFromParcel(param1Parcel);
      } 
      this.mWindowingMode = param1Parcel.readInt();
      this.mActivityWindowingMode = param1Parcel.readInt();
    }
    
    public void merge(Change param1Change, boolean param1Boolean) {
      this.mConfiguration.setTo(param1Change.mConfiguration, param1Change.mConfigSetMask, param1Change.mWindowSetMask);
      this.mConfigSetMask |= param1Change.mConfigSetMask;
      this.mWindowSetMask |= param1Change.mWindowSetMask;
      if ((param1Change.mChangeMask & 0x1) != 0)
        this.mFocusable = param1Change.mFocusable; 
      if (param1Boolean && (param1Change.mChangeMask & 0x2) != 0) {
        this.mBoundsChangeTransaction = param1Change.mBoundsChangeTransaction;
        param1Change.mBoundsChangeTransaction = null;
      } 
      if ((param1Change.mChangeMask & 0x4) != 0) {
        Rect rect1;
        if (param1Boolean) {
          rect1 = param1Change.mPinnedBounds;
        } else {
          rect1 = new Rect(param1Change.mPinnedBounds);
        } 
        this.mPinnedBounds = rect1;
      } 
      if ((param1Change.mChangeMask & 0x8) != 0)
        this.mHidden = param1Change.mHidden; 
      this.mChangeMask |= param1Change.mChangeMask;
      int i = param1Change.mActivityWindowingMode;
      if (i >= 0)
        this.mActivityWindowingMode = i; 
      i = param1Change.mWindowingMode;
      if (i >= 0)
        this.mWindowingMode = i; 
      Rect rect = param1Change.mBoundsChangeSurfaceBounds;
      if (rect != null) {
        Rect rect1;
        if (param1Boolean) {
          rect1 = rect;
        } else {
          rect1 = new Rect(((Change)rect1).mBoundsChangeSurfaceBounds);
        } 
        this.mBoundsChangeSurfaceBounds = rect1;
      } 
    }
    
    public int getWindowingMode() {
      return this.mWindowingMode;
    }
    
    public int getActivityWindowingMode() {
      return this.mActivityWindowingMode;
    }
    
    public Configuration getConfiguration() {
      return this.mConfiguration;
    }
    
    public boolean getFocusable() {
      if ((this.mChangeMask & 0x1) != 0)
        return this.mFocusable; 
      throw new RuntimeException("Focusable not set. check CHANGE_FOCUSABLE first");
    }
    
    public boolean getHidden() {
      if ((this.mChangeMask & 0x8) != 0)
        return this.mHidden; 
      throw new RuntimeException("Hidden not set. check CHANGE_HIDDEN first");
    }
    
    public int getChangeMask() {
      return this.mChangeMask;
    }
    
    public int getConfigSetMask() {
      return this.mConfigSetMask;
    }
    
    public int getWindowSetMask() {
      return this.mWindowSetMask;
    }
    
    public Rect getEnterPipBounds() {
      return this.mPinnedBounds;
    }
    
    public SurfaceControl.Transaction getBoundsChangeTransaction() {
      return this.mBoundsChangeTransaction;
    }
    
    public Rect getBoundsChangeSurfaceBounds() {
      return this.mBoundsChangeSurfaceBounds;
    }
    
    public String toString() {
      boolean bool2, bool3;
      int i = this.mConfigSetMask;
      boolean bool1 = false;
      if ((i & 0x20000000) != 0 && (this.mWindowSetMask & 0x1) != 0) {
        i = 1;
      } else {
        i = 0;
      } 
      if ((0x20000000 & this.mConfigSetMask) != 0 && (this.mWindowSetMask & 0x2) != 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if ((this.mConfigSetMask & 0x400) != 0) {
        bool3 = true;
      } else {
        bool3 = false;
      } 
      if ((this.mConfigSetMask & 0x800) != 0)
        bool1 = true; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append('{');
      if (i != 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("bounds:");
        stringBuilder1.append(this.mConfiguration.windowConfiguration.getBounds());
        stringBuilder1.append(",");
        stringBuilder.append(stringBuilder1.toString());
      } 
      if (bool2) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("appbounds:");
        stringBuilder1.append(this.mConfiguration.windowConfiguration.getAppBounds());
        stringBuilder1.append(",");
        stringBuilder.append(stringBuilder1.toString());
      } 
      if (bool1) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("ssw:");
        stringBuilder1.append(this.mConfiguration.smallestScreenWidthDp);
        stringBuilder1.append(",");
        stringBuilder.append(stringBuilder1.toString());
      } 
      if (bool3) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("sw/h:");
        stringBuilder1.append(this.mConfiguration.screenWidthDp);
        stringBuilder1.append("x");
        stringBuilder1.append(this.mConfiguration.screenHeightDp);
        stringBuilder1.append(",");
        stringBuilder.append(stringBuilder1.toString());
      } 
      if ((0x1 & this.mChangeMask) != 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("focusable:");
        stringBuilder1.append(this.mFocusable);
        stringBuilder1.append(",");
        stringBuilder.append(stringBuilder1.toString());
      } 
      if (this.mBoundsChangeTransaction != null)
        stringBuilder.append("hasBoundsTransaction,"); 
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      this.mConfiguration.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeBoolean(this.mFocusable);
      param1Parcel.writeBoolean(this.mHidden);
      param1Parcel.writeInt(this.mChangeMask);
      param1Parcel.writeInt(this.mConfigSetMask);
      param1Parcel.writeInt(this.mWindowSetMask);
      Rect rect2 = this.mPinnedBounds;
      if (rect2 != null)
        rect2.writeToParcel(param1Parcel, param1Int); 
      SurfaceControl.Transaction transaction = this.mBoundsChangeTransaction;
      if (transaction != null)
        transaction.writeToParcel(param1Parcel, param1Int); 
      Rect rect1 = this.mBoundsChangeSurfaceBounds;
      if (rect1 != null)
        rect1.writeToParcel(param1Parcel, param1Int); 
      param1Parcel.writeInt(this.mWindowingMode);
      param1Parcel.writeInt(this.mActivityWindowingMode);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public static final Parcelable.Creator<Change> CREATOR = new Parcelable.Creator<Change>() {
        public WindowContainerTransaction.Change createFromParcel(Parcel param2Parcel) {
          return new WindowContainerTransaction.Change(param2Parcel);
        }
        
        public WindowContainerTransaction.Change[] newArray(int param2Int) {
          return new WindowContainerTransaction.Change[param2Int];
        }
      };
    
    private int mActivityWindowingMode;
    
    private Rect mBoundsChangeSurfaceBounds;
    
    private SurfaceControl.Transaction mBoundsChangeTransaction;
    
    private int mChangeMask;
    
    private int mConfigSetMask;
    
    private final Configuration mConfiguration;
    
    private boolean mFocusable;
    
    private boolean mHidden;
    
    private Rect mPinnedBounds;
    
    private int mWindowSetMask;
    
    private int mWindowingMode;
  }
  
  class null implements Parcelable.Creator<Change> {
    public WindowContainerTransaction.Change createFromParcel(Parcel param1Parcel) {
      return new WindowContainerTransaction.Change(param1Parcel);
    }
    
    public WindowContainerTransaction.Change[] newArray(int param1Int) {
      return new WindowContainerTransaction.Change[param1Int];
    }
  }
  
  public static class HierarchyOp implements Parcelable {
    public HierarchyOp(IBinder param1IBinder1, IBinder param1IBinder2, boolean param1Boolean) {
      this.mContainer = param1IBinder1;
      this.mReparent = param1IBinder2;
      this.mToTop = param1Boolean;
    }
    
    public HierarchyOp(IBinder param1IBinder, boolean param1Boolean) {
      this.mContainer = param1IBinder;
      this.mReparent = param1IBinder;
      this.mToTop = param1Boolean;
    }
    
    public HierarchyOp(HierarchyOp param1HierarchyOp) {
      this.mContainer = param1HierarchyOp.mContainer;
      this.mReparent = param1HierarchyOp.mReparent;
      this.mToTop = param1HierarchyOp.mToTop;
    }
    
    protected HierarchyOp(Parcel param1Parcel) {
      this.mContainer = param1Parcel.readStrongBinder();
      this.mReparent = param1Parcel.readStrongBinder();
      this.mToTop = param1Parcel.readBoolean();
    }
    
    public boolean isReparent() {
      boolean bool;
      if (this.mContainer != this.mReparent) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public IBinder getNewParent() {
      return this.mReparent;
    }
    
    public IBinder getContainer() {
      return this.mContainer;
    }
    
    public boolean getToTop() {
      return this.mToTop;
    }
    
    public String toString() {
      String str;
      if (isReparent()) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("{reparent: ");
        stringBuilder1.append(this.mContainer);
        stringBuilder1.append(" to ");
        if (this.mToTop) {
          str = "top of ";
        } else {
          str = "bottom of ";
        } 
        stringBuilder1.append(str);
        stringBuilder1.append(this.mReparent);
        stringBuilder1.append("}");
        return stringBuilder1.toString();
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{reorder: ");
      stringBuilder.append(this.mContainer);
      stringBuilder.append(" to ");
      if (this.mToTop) {
        str = "top";
      } else {
        str = "bottom";
      } 
      stringBuilder.append(str);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeStrongBinder(this.mContainer);
      param1Parcel.writeStrongBinder(this.mReparent);
      param1Parcel.writeBoolean(this.mToTop);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public static final Parcelable.Creator<HierarchyOp> CREATOR = new Parcelable.Creator<HierarchyOp>() {
        public WindowContainerTransaction.HierarchyOp createFromParcel(Parcel param2Parcel) {
          return new WindowContainerTransaction.HierarchyOp(param2Parcel);
        }
        
        public WindowContainerTransaction.HierarchyOp[] newArray(int param2Int) {
          return new WindowContainerTransaction.HierarchyOp[param2Int];
        }
      };
    
    private final IBinder mContainer;
    
    private final IBinder mReparent;
    
    private final boolean mToTop;
  }
  
  class null implements Parcelable.Creator<HierarchyOp> {
    public WindowContainerTransaction.HierarchyOp createFromParcel(Parcel param1Parcel) {
      return new WindowContainerTransaction.HierarchyOp(param1Parcel);
    }
    
    public WindowContainerTransaction.HierarchyOp[] newArray(int param1Int) {
      return new WindowContainerTransaction.HierarchyOp[param1Int];
    }
  }
}
