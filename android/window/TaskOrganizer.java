package android.window;

import android.app.ActivityManager;
import android.os.RemoteException;
import android.util.Singleton;
import android.view.SurfaceControl;
import java.util.List;

public class TaskOrganizer extends WindowOrganizer {
  public final void registerOrganizer(int paramInt) {
    try {
      getController().registerTaskOrganizer(this.mInterface, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public final void unregisterOrganizer() {
    try {
      getController().unregisterTaskOrganizer(this.mInterface);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void onTaskAppeared(ActivityManager.RunningTaskInfo paramRunningTaskInfo, SurfaceControl paramSurfaceControl) {}
  
  public void onTaskVanished(ActivityManager.RunningTaskInfo paramRunningTaskInfo) {}
  
  public void onTaskInfoChanged(ActivityManager.RunningTaskInfo paramRunningTaskInfo) {}
  
  public void onBackPressedOnTaskRoot(ActivityManager.RunningTaskInfo paramRunningTaskInfo) {}
  
  public static ActivityManager.RunningTaskInfo createRootTask(int paramInt1, int paramInt2) {
    try {
      return getController().createRootTask(paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean deleteRootTask(WindowContainerToken paramWindowContainerToken) {
    try {
      return getController().deleteRootTask(paramWindowContainerToken);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static List<ActivityManager.RunningTaskInfo> getChildTasks(WindowContainerToken paramWindowContainerToken, int[] paramArrayOfint) {
    try {
      return getController().getChildTasks(paramWindowContainerToken, paramArrayOfint);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static List<ActivityManager.RunningTaskInfo> getRootTasks(int paramInt, int[] paramArrayOfint) {
    try {
      return getController().getRootTasks(paramInt, paramArrayOfint);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static WindowContainerToken getImeTarget(int paramInt) {
    try {
      return getController().getImeTarget(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static void setLaunchRoot(int paramInt, WindowContainerToken paramWindowContainerToken) {
    try {
      getController().setLaunchRoot(paramInt, paramWindowContainerToken);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setInterceptBackPressedOnTaskRoot(boolean paramBoolean) {
    try {
      getController().setInterceptBackPressedOnTaskRoot(this.mInterface, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private final ITaskOrganizer mInterface = (ITaskOrganizer)new Object(this);
  
  private static ITaskOrganizerController getController() {
    return ITaskOrganizerControllerSingleton.get();
  }
  
  private static final Singleton<ITaskOrganizerController> ITaskOrganizerControllerSingleton = new Singleton<ITaskOrganizerController>() {
      protected ITaskOrganizerController create() {
        try {
          return WindowOrganizer.getWindowOrganizerController().getTaskOrganizerController();
        } catch (RemoteException remoteException) {
          return null;
        } 
      }
    };
}
