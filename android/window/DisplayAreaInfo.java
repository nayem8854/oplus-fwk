package android.window;

import android.content.res.Configuration;
import android.os.Parcel;
import android.os.Parcelable;

public final class DisplayAreaInfo implements Parcelable {
  public final WindowContainerToken token;
  
  public final int featureId;
  
  public final int displayId;
  
  public final Configuration configuration = new Configuration();
  
  public DisplayAreaInfo(WindowContainerToken paramWindowContainerToken, int paramInt1, int paramInt2) {
    this.token = paramWindowContainerToken;
    this.displayId = paramInt1;
    this.featureId = paramInt2;
  }
  
  private DisplayAreaInfo(Parcel paramParcel) {
    this.token = (WindowContainerToken)WindowContainerToken.CREATOR.createFromParcel(paramParcel);
    this.configuration.readFromParcel(paramParcel);
    this.displayId = paramParcel.readInt();
    this.featureId = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.token.writeToParcel(paramParcel, paramInt);
    this.configuration.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.displayId);
    paramParcel.writeInt(this.featureId);
  }
  
  public static final Parcelable.Creator<DisplayAreaInfo> CREATOR = new Parcelable.Creator<DisplayAreaInfo>() {
      public DisplayAreaInfo createFromParcel(Parcel param1Parcel) {
        return new DisplayAreaInfo(param1Parcel);
      }
      
      public DisplayAreaInfo[] newArray(int param1Int) {
        return new DisplayAreaInfo[param1Int];
      }
    };
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DisplayAreaInfo{token=");
    stringBuilder.append(this.token);
    stringBuilder.append(" config=");
    stringBuilder.append(this.configuration);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
}
