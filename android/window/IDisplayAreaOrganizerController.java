package android.window;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDisplayAreaOrganizerController extends IInterface {
  void registerOrganizer(IDisplayAreaOrganizer paramIDisplayAreaOrganizer, int paramInt) throws RemoteException;
  
  void unregisterOrganizer(IDisplayAreaOrganizer paramIDisplayAreaOrganizer) throws RemoteException;
  
  class Default implements IDisplayAreaOrganizerController {
    public void registerOrganizer(IDisplayAreaOrganizer param1IDisplayAreaOrganizer, int param1Int) throws RemoteException {}
    
    public void unregisterOrganizer(IDisplayAreaOrganizer param1IDisplayAreaOrganizer) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDisplayAreaOrganizerController {
    private static final String DESCRIPTOR = "android.window.IDisplayAreaOrganizerController";
    
    static final int TRANSACTION_registerOrganizer = 1;
    
    static final int TRANSACTION_unregisterOrganizer = 2;
    
    public Stub() {
      attachInterface(this, "android.window.IDisplayAreaOrganizerController");
    }
    
    public static IDisplayAreaOrganizerController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.window.IDisplayAreaOrganizerController");
      if (iInterface != null && iInterface instanceof IDisplayAreaOrganizerController)
        return (IDisplayAreaOrganizerController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "unregisterOrganizer";
      } 
      return "registerOrganizer";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IDisplayAreaOrganizer iDisplayAreaOrganizer1;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.window.IDisplayAreaOrganizerController");
          return true;
        } 
        param1Parcel1.enforceInterface("android.window.IDisplayAreaOrganizerController");
        iDisplayAreaOrganizer1 = IDisplayAreaOrganizer.Stub.asInterface(param1Parcel1.readStrongBinder());
        unregisterOrganizer(iDisplayAreaOrganizer1);
        param1Parcel2.writeNoException();
        return true;
      } 
      iDisplayAreaOrganizer1.enforceInterface("android.window.IDisplayAreaOrganizerController");
      IDisplayAreaOrganizer iDisplayAreaOrganizer2 = IDisplayAreaOrganizer.Stub.asInterface(iDisplayAreaOrganizer1.readStrongBinder());
      param1Int1 = iDisplayAreaOrganizer1.readInt();
      registerOrganizer(iDisplayAreaOrganizer2, param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IDisplayAreaOrganizerController {
      public static IDisplayAreaOrganizerController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.window.IDisplayAreaOrganizerController";
      }
      
      public void registerOrganizer(IDisplayAreaOrganizer param2IDisplayAreaOrganizer, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.window.IDisplayAreaOrganizerController");
          if (param2IDisplayAreaOrganizer != null) {
            iBinder = param2IDisplayAreaOrganizer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IDisplayAreaOrganizerController.Stub.getDefaultImpl() != null) {
            IDisplayAreaOrganizerController.Stub.getDefaultImpl().registerOrganizer(param2IDisplayAreaOrganizer, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterOrganizer(IDisplayAreaOrganizer param2IDisplayAreaOrganizer) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.window.IDisplayAreaOrganizerController");
          if (param2IDisplayAreaOrganizer != null) {
            iBinder = param2IDisplayAreaOrganizer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IDisplayAreaOrganizerController.Stub.getDefaultImpl() != null) {
            IDisplayAreaOrganizerController.Stub.getDefaultImpl().unregisterOrganizer(param2IDisplayAreaOrganizer);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDisplayAreaOrganizerController param1IDisplayAreaOrganizerController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDisplayAreaOrganizerController != null) {
          Proxy.sDefaultImpl = param1IDisplayAreaOrganizerController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDisplayAreaOrganizerController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
