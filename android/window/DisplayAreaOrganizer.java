package android.window;

import android.os.RemoteException;
import android.util.Singleton;
import android.view.SurfaceControl;

public class DisplayAreaOrganizer extends WindowOrganizer {
  public void registerOrganizer(int paramInt) {
    try {
      getController().registerOrganizer(this.mInterface, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unregisterOrganizer() {
    try {
      getController().unregisterOrganizer(this.mInterface);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void onDisplayAreaAppeared(DisplayAreaInfo paramDisplayAreaInfo, SurfaceControl paramSurfaceControl) {}
  
  public void onDisplayAreaVanished(DisplayAreaInfo paramDisplayAreaInfo) {}
  
  public void onDisplayAreaInfoChanged(DisplayAreaInfo paramDisplayAreaInfo) {}
  
  private final IDisplayAreaOrganizer mInterface = (IDisplayAreaOrganizer)new Object(this);
  
  private static IDisplayAreaOrganizerController getController() {
    return IDisplayAreaOrganizerControllerSingleton.get();
  }
  
  private static final Singleton<IDisplayAreaOrganizerController> IDisplayAreaOrganizerControllerSingleton = new Singleton<IDisplayAreaOrganizerController>() {
      protected IDisplayAreaOrganizerController create() {
        try {
          IWindowOrganizerController iWindowOrganizerController = WindowOrganizer.getWindowOrganizerController();
          return 
            iWindowOrganizerController.getDisplayAreaOrganizerController();
        } catch (RemoteException remoteException) {
          return null;
        } 
      }
    };
  
  public static final int FEATURE_WINDOW_TOKENS = 2;
  
  public static final int FEATURE_VENDOR_FIRST = 10001;
  
  public static final int FEATURE_UNDEFINED = -1;
  
  public static final int FEATURE_SYSTEM_LAST = 10000;
  
  public static final int FEATURE_SYSTEM_FIRST = 0;
  
  public static final int FEATURE_ROOT = 0;
  
  public static final int FEATURE_DEFAULT_TASK_CONTAINER = 1;
}
