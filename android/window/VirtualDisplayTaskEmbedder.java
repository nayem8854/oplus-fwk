package android.window;

import android.app.ActivityManager;
import android.app.ActivityOptions;
import android.app.ActivityTaskManager;
import android.app.IActivityTaskManager;
import android.app.ITaskStackListener;
import android.app.TaskStackListener;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.Insets;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Region;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.hardware.input.InputManager;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.IWindow;
import android.view.IWindowManager;
import android.view.IWindowSession;
import android.view.KeyEvent;
import android.view.SurfaceControl;
import android.view.WindowManagerGlobal;
import android.view.inputmethod.InputMethodManager;
import java.util.List;

public class VirtualDisplayTaskEmbedder extends TaskEmbedder {
  private static final String DISPLAY_NAME = "TaskVirtualDisplay";
  
  private static final String TAG = "VirDispTaskEmbedder";
  
  private int mDisplayDensityDpi;
  
  private Insets mForwardedInsets;
  
  private final boolean mSingleTaskInstance;
  
  private TaskStackListener mTaskStackListener;
  
  private DisplayMetrics mTmpDisplayMetrics;
  
  private final boolean mUsePublicVirtualDisplay;
  
  private final boolean mUseTrustedDisplay;
  
  private VirtualDisplay mVirtualDisplay;
  
  public VirtualDisplayTaskEmbedder(Context paramContext, TaskEmbedder.Host paramHost, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    super(paramContext, paramHost);
    this.mSingleTaskInstance = paramBoolean1;
    this.mUsePublicVirtualDisplay = paramBoolean2;
    this.mUseTrustedDisplay = paramBoolean3;
  }
  
  public boolean isInitialized() {
    boolean bool;
    if (this.mVirtualDisplay != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean onInitialize() {
    DisplayManager displayManager = (DisplayManager)this.mContext.getSystemService(DisplayManager.class);
    this.mDisplayDensityDpi = getBaseDisplayDensity();
    int i = 264;
    if (this.mUsePublicVirtualDisplay)
      i = 0x108 | 0x1; 
    if (this.mUseTrustedDisplay)
      i |= 0x400; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("TaskVirtualDisplay@");
    stringBuilder.append(System.identityHashCode(this));
    String str = stringBuilder.toString();
    int j = this.mHost.getWidth();
    TaskEmbedder.Host host = this.mHost;
    int k = host.getHeight(), m = this.mDisplayDensityDpi;
    VirtualDisplay virtualDisplay = displayManager.createVirtualDisplay(str, j, k, m, null, i);
    if (virtualDisplay == null) {
      Log.e("VirDispTaskEmbedder", "Failed to initialize TaskEmbedder");
      return false;
    } 
    try {
      i = getDisplayId();
      IWindowManager iWindowManager = WindowManagerGlobal.getWindowManagerService();
      IWindowSession iWindowSession = WindowManagerGlobal.getWindowSession();
      TaskEmbedder.Host host1 = this.mHost;
      IWindow iWindow = host1.getWindow();
      SurfaceControl surfaceControl = this.mSurfaceControl;
      iWindowSession.reparentDisplayContent(iWindow, surfaceControl, i);
      iWindowManager.dontOverrideDisplayInfo(i);
      if (this.mSingleTaskInstance) {
        ActivityTaskManager activityTaskManager = (ActivityTaskManager)this.mContext.getSystemService(ActivityTaskManager.class);
        activityTaskManager.setDisplayToSingleTaskInstance(i);
      } 
      setForwardedInsets(this.mForwardedInsets);
      TaskStackListenerImpl taskStackListenerImpl = new TaskStackListenerImpl();
      this(this);
      this.mTaskStackListener = taskStackListenerImpl;
      this.mActivityTaskManager.registerTaskStackListener((ITaskStackListener)this.mTaskStackListener);
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
    return super.onInitialize();
  }
  
  protected boolean onRelease() {
    super.onRelease();
    clearActivityViewGeometryForIme();
    if (this.mTaskStackListener != null) {
      try {
        this.mActivityTaskManager.unregisterTaskStackListener((ITaskStackListener)this.mTaskStackListener);
      } catch (RemoteException remoteException) {
        Log.e("VirDispTaskEmbedder", "Failed to unregister task stack listener", (Throwable)remoteException);
      } 
      this.mTaskStackListener = null;
    } 
    if (isInitialized()) {
      this.mVirtualDisplay.release();
      this.mVirtualDisplay = null;
      return true;
    } 
    return false;
  }
  
  public void start() {
    super.start();
    if (isInitialized())
      this.mVirtualDisplay.setDisplayState(true); 
  }
  
  public void stop() {
    super.stop();
    if (isInitialized()) {
      this.mVirtualDisplay.setDisplayState(false);
      clearActivityViewGeometryForIme();
    } 
  }
  
  public void resizeTask(int paramInt1, int paramInt2) {
    this.mDisplayDensityDpi = getBaseDisplayDensity();
    if (isInitialized())
      this.mVirtualDisplay.resize(paramInt1, paramInt2, this.mDisplayDensityDpi); 
  }
  
  public void performBackPress() {
    if (!isInitialized())
      return; 
    int i = this.mVirtualDisplay.getDisplay().getDisplayId();
    InputManager inputManager = InputManager.getInstance();
    inputManager.injectInputEvent(createKeyEvent(0, 4, i), 0);
    inputManager.injectInputEvent(createKeyEvent(1, 4, i), 0);
  }
  
  public boolean gatherTransparentRegion(Region paramRegion) {
    notifyBoundsChanged();
    return super.gatherTransparentRegion(paramRegion);
  }
  
  public int getId() {
    return getDisplayId();
  }
  
  public int getDisplayId() {
    if (isInitialized())
      return this.mVirtualDisplay.getDisplay().getDisplayId(); 
    return -1;
  }
  
  public VirtualDisplay getVirtualDisplay() {
    if (isInitialized())
      return this.mVirtualDisplay; 
    return null;
  }
  
  protected ActivityOptions prepareActivityOptions(ActivityOptions paramActivityOptions) {
    paramActivityOptions = super.prepareActivityOptions(paramActivityOptions);
    paramActivityOptions.setLaunchDisplayId(getDisplayId());
    return paramActivityOptions;
  }
  
  public void setForwardedInsets(Insets paramInsets) {
    this.mForwardedInsets = paramInsets;
    if (!isInitialized())
      return; 
    try {
      IWindowManager iWindowManager = WindowManagerGlobal.getWindowManagerService();
      iWindowManager.setForwardedInsets(getDisplayId(), this.mForwardedInsets);
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  protected void updateLocationAndTapExcludeRegion() {
    super.updateLocationAndTapExcludeRegion();
    if (!isInitialized() || this.mHost.getWindow() == null)
      return; 
    reportLocation(this.mHost.getScreenToTaskMatrix(), this.mHost.getPositionInWindow());
  }
  
  private void reportLocation(Matrix paramMatrix, Point paramPoint) {
    try {
      int i = getDisplayId();
      InputMethodManager inputMethodManager = (InputMethodManager)this.mContext.getSystemService(InputMethodManager.class);
      inputMethodManager.reportActivityView(i, paramMatrix);
      IWindowSession iWindowSession = WindowManagerGlobal.getWindowSession();
      iWindowSession.updateDisplayContentLocation(this.mHost.getWindow(), paramPoint.x, paramPoint.y, i);
    } catch (RemoteException remoteException) {
      remoteException.rethrowAsRuntimeException();
    } 
  }
  
  private void clearActivityViewGeometryForIme() {
    int i = getDisplayId();
    ((InputMethodManager)this.mContext.getSystemService(InputMethodManager.class)).reportActivityView(i, null);
  }
  
  private static KeyEvent createKeyEvent(int paramInt1, int paramInt2, int paramInt3) {
    long l = SystemClock.uptimeMillis();
    KeyEvent keyEvent = new KeyEvent(l, l, paramInt1, paramInt2, 0, 0, -1, 0, 72, 257);
    keyEvent.setDisplayId(paramInt3);
    return keyEvent;
  }
  
  private int getBaseDisplayDensity() {
    return (this.mContext.getResources().getConfiguration()).densityDpi;
  }
  
  class TaskStackListenerImpl extends TaskStackListener {
    final VirtualDisplayTaskEmbedder this$0;
    
    private TaskStackListenerImpl() {}
    
    public void onTaskDescriptionChanged(ActivityManager.RunningTaskInfo param1RunningTaskInfo) throws RemoteException {
      if (!VirtualDisplayTaskEmbedder.this.isInitialized())
        return; 
      if (param1RunningTaskInfo.displayId != VirtualDisplayTaskEmbedder.this.getDisplayId())
        return; 
      ActivityManager.StackInfo stackInfo = getTopMostStackInfo();
      if (stackInfo == null)
        return; 
      if (param1RunningTaskInfo.taskId == stackInfo.taskIds[stackInfo.taskIds.length - 1])
        VirtualDisplayTaskEmbedder.this.mHost.post(new _$$Lambda$VirtualDisplayTaskEmbedder$TaskStackListenerImpl$T5XvoM0ShYGPCBOY7qLgIslsK9g(this, param1RunningTaskInfo)); 
    }
    
    public void onTaskMovedToFront(ActivityManager.RunningTaskInfo param1RunningTaskInfo) throws RemoteException {
      if (VirtualDisplayTaskEmbedder.this.isInitialized() && VirtualDisplayTaskEmbedder.this.mListener != null) {
        int i = param1RunningTaskInfo.displayId;
        VirtualDisplayTaskEmbedder virtualDisplayTaskEmbedder = VirtualDisplayTaskEmbedder.this;
        if (i == virtualDisplayTaskEmbedder.getDisplayId()) {
          ActivityManager.StackInfo stackInfo = getTopMostStackInfo();
          if (stackInfo != null && param1RunningTaskInfo.taskId == stackInfo.taskIds[stackInfo.taskIds.length - 1])
            VirtualDisplayTaskEmbedder.this.mListener.onTaskMovedToFront(param1RunningTaskInfo.taskId); 
          return;
        } 
      } 
    }
    
    public void onTaskCreated(int param1Int, ComponentName param1ComponentName) throws RemoteException {
      if (VirtualDisplayTaskEmbedder.this.mListener == null || !VirtualDisplayTaskEmbedder.this.isInitialized())
        return; 
      ActivityManager.StackInfo stackInfo = getTopMostStackInfo();
      if (stackInfo != null && param1Int == stackInfo.taskIds[stackInfo.taskIds.length - 1])
        VirtualDisplayTaskEmbedder.this.mListener.onTaskCreated(param1Int, param1ComponentName); 
    }
    
    public void onTaskRemovalStarted(ActivityManager.RunningTaskInfo param1RunningTaskInfo) throws RemoteException {
      if (VirtualDisplayTaskEmbedder.this.mListener != null && VirtualDisplayTaskEmbedder.this.isInitialized()) {
        int i = param1RunningTaskInfo.displayId;
        VirtualDisplayTaskEmbedder virtualDisplayTaskEmbedder = VirtualDisplayTaskEmbedder.this;
        if (i == virtualDisplayTaskEmbedder.getDisplayId()) {
          VirtualDisplayTaskEmbedder.this.mListener.onTaskRemovalStarted(param1RunningTaskInfo.taskId);
          return;
        } 
      } 
    }
    
    private ActivityManager.StackInfo getTopMostStackInfo() throws RemoteException {
      int i = VirtualDisplayTaskEmbedder.this.getDisplayId();
      IActivityTaskManager iActivityTaskManager = VirtualDisplayTaskEmbedder.this.mActivityTaskManager;
      List<ActivityManager.StackInfo> list = iActivityTaskManager.getAllStackInfosOnDisplay(i);
      if (list.isEmpty())
        return null; 
      return list.get(0);
    }
  }
}
