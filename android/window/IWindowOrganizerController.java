package android.window;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.SurfaceControl;

public interface IWindowOrganizerController extends IInterface {
  int applySyncTransaction(WindowContainerTransaction paramWindowContainerTransaction, IWindowContainerTransactionCallback paramIWindowContainerTransactionCallback) throws RemoteException;
  
  void applyTransaction(WindowContainerTransaction paramWindowContainerTransaction) throws RemoteException;
  
  IDisplayAreaOrganizerController getDisplayAreaOrganizerController() throws RemoteException;
  
  ITaskOrganizerController getTaskOrganizerController() throws RemoteException;
  
  boolean takeScreenshot(WindowContainerToken paramWindowContainerToken, SurfaceControl paramSurfaceControl) throws RemoteException;
  
  class Default implements IWindowOrganizerController {
    public void applyTransaction(WindowContainerTransaction param1WindowContainerTransaction) throws RemoteException {}
    
    public int applySyncTransaction(WindowContainerTransaction param1WindowContainerTransaction, IWindowContainerTransactionCallback param1IWindowContainerTransactionCallback) throws RemoteException {
      return 0;
    }
    
    public ITaskOrganizerController getTaskOrganizerController() throws RemoteException {
      return null;
    }
    
    public IDisplayAreaOrganizerController getDisplayAreaOrganizerController() throws RemoteException {
      return null;
    }
    
    public boolean takeScreenshot(WindowContainerToken param1WindowContainerToken, SurfaceControl param1SurfaceControl) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWindowOrganizerController {
    private static final String DESCRIPTOR = "android.window.IWindowOrganizerController";
    
    static final int TRANSACTION_applySyncTransaction = 2;
    
    static final int TRANSACTION_applyTransaction = 1;
    
    static final int TRANSACTION_getDisplayAreaOrganizerController = 4;
    
    static final int TRANSACTION_getTaskOrganizerController = 3;
    
    static final int TRANSACTION_takeScreenshot = 5;
    
    public Stub() {
      attachInterface(this, "android.window.IWindowOrganizerController");
    }
    
    public static IWindowOrganizerController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.window.IWindowOrganizerController");
      if (iInterface != null && iInterface instanceof IWindowOrganizerController)
        return (IWindowOrganizerController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "takeScreenshot";
            } 
            return "getDisplayAreaOrganizerController";
          } 
          return "getTaskOrganizerController";
        } 
        return "applySyncTransaction";
      } 
      return "applyTransaction";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IWindowContainerTransactionCallback iWindowContainerTransactionCallback;
      if (param1Int1 != 1) {
        IBinder iBinder;
        WindowContainerTransaction windowContainerTransaction;
        if (param1Int1 != 2) {
          IBinder iBinder1;
          SurfaceControl surfaceControl = null;
          Parcel parcel = null;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.window.IWindowOrganizerController");
                return true;
              } 
              param1Parcel1.enforceInterface("android.window.IWindowOrganizerController");
              if (param1Parcel1.readInt() != 0) {
                WindowContainerToken windowContainerToken = (WindowContainerToken)WindowContainerToken.CREATOR.createFromParcel(param1Parcel1);
              } else {
                param1Parcel1 = null;
              } 
              surfaceControl = new SurfaceControl();
              boolean bool = takeScreenshot((WindowContainerToken)param1Parcel1, surfaceControl);
              param1Parcel2.writeNoException();
              param1Parcel2.writeInt(bool);
              param1Parcel2.writeInt(1);
              surfaceControl.writeToParcel(param1Parcel2, 1);
              return true;
            } 
            param1Parcel1.enforceInterface("android.window.IWindowOrganizerController");
            windowContainerTransaction = (WindowContainerTransaction)getDisplayAreaOrganizerController();
            param1Parcel2.writeNoException();
            param1Parcel1 = parcel;
            if (windowContainerTransaction != null)
              iBinder1 = windowContainerTransaction.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder1);
            return true;
          } 
          iBinder1.enforceInterface("android.window.IWindowOrganizerController");
          ITaskOrganizerController iTaskOrganizerController = getTaskOrganizerController();
          param1Parcel2.writeNoException();
          IDisplayAreaOrganizerController iDisplayAreaOrganizerController = (IDisplayAreaOrganizerController)windowContainerTransaction;
          if (iTaskOrganizerController != null)
            iBinder = iTaskOrganizerController.asBinder(); 
          param1Parcel2.writeStrongBinder(iBinder);
          return true;
        } 
        iBinder.enforceInterface("android.window.IWindowOrganizerController");
        if (iBinder.readInt() != 0) {
          windowContainerTransaction = (WindowContainerTransaction)WindowContainerTransaction.CREATOR.createFromParcel((Parcel)iBinder);
        } else {
          windowContainerTransaction = null;
        } 
        iWindowContainerTransactionCallback = IWindowContainerTransactionCallback.Stub.asInterface(iBinder.readStrongBinder());
        param1Int1 = applySyncTransaction(windowContainerTransaction, iWindowContainerTransactionCallback);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(param1Int1);
        return true;
      } 
      iWindowContainerTransactionCallback.enforceInterface("android.window.IWindowOrganizerController");
      if (iWindowContainerTransactionCallback.readInt() != 0) {
        WindowContainerTransaction windowContainerTransaction = (WindowContainerTransaction)WindowContainerTransaction.CREATOR.createFromParcel((Parcel)iWindowContainerTransactionCallback);
      } else {
        iWindowContainerTransactionCallback = null;
      } 
      applyTransaction((WindowContainerTransaction)iWindowContainerTransactionCallback);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IWindowOrganizerController {
      public static IWindowOrganizerController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.window.IWindowOrganizerController";
      }
      
      public void applyTransaction(WindowContainerTransaction param2WindowContainerTransaction) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.window.IWindowOrganizerController");
          if (param2WindowContainerTransaction != null) {
            parcel1.writeInt(1);
            param2WindowContainerTransaction.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IWindowOrganizerController.Stub.getDefaultImpl() != null) {
            IWindowOrganizerController.Stub.getDefaultImpl().applyTransaction(param2WindowContainerTransaction);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int applySyncTransaction(WindowContainerTransaction param2WindowContainerTransaction, IWindowContainerTransactionCallback param2IWindowContainerTransactionCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.window.IWindowOrganizerController");
          if (param2WindowContainerTransaction != null) {
            parcel1.writeInt(1);
            param2WindowContainerTransaction.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IWindowContainerTransactionCallback != null) {
            iBinder = param2IWindowContainerTransactionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IWindowOrganizerController.Stub.getDefaultImpl() != null)
            return IWindowOrganizerController.Stub.getDefaultImpl().applySyncTransaction(param2WindowContainerTransaction, param2IWindowContainerTransactionCallback); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ITaskOrganizerController getTaskOrganizerController() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.window.IWindowOrganizerController");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IWindowOrganizerController.Stub.getDefaultImpl() != null)
            return IWindowOrganizerController.Stub.getDefaultImpl().getTaskOrganizerController(); 
          parcel2.readException();
          return ITaskOrganizerController.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IDisplayAreaOrganizerController getDisplayAreaOrganizerController() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.window.IWindowOrganizerController");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IWindowOrganizerController.Stub.getDefaultImpl() != null)
            return IWindowOrganizerController.Stub.getDefaultImpl().getDisplayAreaOrganizerController(); 
          parcel2.readException();
          return IDisplayAreaOrganizerController.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean takeScreenshot(WindowContainerToken param2WindowContainerToken, SurfaceControl param2SurfaceControl) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.window.IWindowOrganizerController");
          boolean bool1 = true;
          if (param2WindowContainerToken != null) {
            parcel1.writeInt(1);
            param2WindowContainerToken.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IWindowOrganizerController.Stub.getDefaultImpl() != null) {
            bool1 = IWindowOrganizerController.Stub.getDefaultImpl().takeScreenshot(param2WindowContainerToken, param2SurfaceControl);
            return bool1;
          } 
          parcel2.readException();
          if (parcel2.readInt() == 0)
            bool1 = false; 
          if (parcel2.readInt() != 0)
            param2SurfaceControl.readFromParcel(parcel2); 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWindowOrganizerController param1IWindowOrganizerController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWindowOrganizerController != null) {
          Proxy.sDefaultImpl = param1IWindowOrganizerController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWindowOrganizerController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
