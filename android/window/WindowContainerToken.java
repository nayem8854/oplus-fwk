package android.window;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public final class WindowContainerToken implements Parcelable {
  public WindowContainerToken(IWindowContainerToken paramIWindowContainerToken) {
    this.mRealToken = paramIWindowContainerToken;
  }
  
  private WindowContainerToken(Parcel paramParcel) {
    this.mRealToken = IWindowContainerToken.Stub.asInterface(paramParcel.readStrongBinder());
  }
  
  public IBinder asBinder() {
    return this.mRealToken.asBinder();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStrongBinder(this.mRealToken.asBinder());
  }
  
  public static final Parcelable.Creator<WindowContainerToken> CREATOR = new Parcelable.Creator<WindowContainerToken>() {
      public WindowContainerToken createFromParcel(Parcel param1Parcel) {
        return new WindowContainerToken(param1Parcel);
      }
      
      public WindowContainerToken[] newArray(int param1Int) {
        return new WindowContainerToken[param1Int];
      }
    };
  
  private final IWindowContainerToken mRealToken;
  
  public int describeContents() {
    return 0;
  }
  
  public int hashCode() {
    return this.mRealToken.asBinder().hashCode();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof WindowContainerToken;
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (this.mRealToken.asBinder() == ((WindowContainerToken)paramObject).asBinder())
      bool1 = true; 
    return bool1;
  }
}
