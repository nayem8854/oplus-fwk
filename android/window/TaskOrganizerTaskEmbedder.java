package android.window;

import android.app.ActivityManager;
import android.app.ActivityOptions;
import android.content.Context;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceControl;

public class TaskOrganizerTaskEmbedder extends TaskEmbedder {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "TaskOrgTaskEmbedder";
  
  private boolean mPendingNotifyBoundsChanged;
  
  private ActivityManager.RunningTaskInfo mTaskInfo;
  
  private SurfaceControl mTaskLeash;
  
  private TaskOrganizer mTaskOrganizer;
  
  private WindowContainerToken mTaskToken;
  
  public TaskOrganizerTaskEmbedder(Context paramContext, TaskEmbedder.Host paramHost) {
    super(paramContext, paramHost);
  }
  
  public boolean isInitialized() {
    boolean bool;
    if (this.mTaskOrganizer != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean onInitialize() {
    TaskOrganizerImpl taskOrganizerImpl = new TaskOrganizerImpl();
    taskOrganizerImpl.registerOrganizer(6);
    this.mTaskOrganizer.setInterceptBackPressedOnTaskRoot(true);
    return super.onInitialize();
  }
  
  protected boolean onRelease() {
    if (!isInitialized())
      return false; 
    this.mTaskOrganizer.unregisterOrganizer();
    resetTaskInfo();
    return true;
  }
  
  public void start() {
    super.start();
    if (!isInitialized())
      return; 
    if (this.mTaskToken == null)
      return; 
    WindowContainerTransaction windowContainerTransaction = new WindowContainerTransaction();
    windowContainerTransaction.setHidden(this.mTaskToken, false);
    WindowOrganizer.applyTransaction(windowContainerTransaction);
    if (this.mListener != null)
      this.mListener.onTaskVisibilityChanged(getTaskId(), true); 
  }
  
  public void stop() {
    super.stop();
    if (!isInitialized())
      return; 
    if (this.mTaskToken == null)
      return; 
    WindowContainerTransaction windowContainerTransaction = new WindowContainerTransaction();
    windowContainerTransaction.setHidden(this.mTaskToken, true);
    WindowOrganizer.applyTransaction(windowContainerTransaction);
    if (this.mListener != null)
      this.mListener.onTaskVisibilityChanged(getTaskId(), false); 
  }
  
  public void notifyBoundsChanged() {
    super.notifyBoundsChanged();
    if (this.mTaskToken == null) {
      this.mPendingNotifyBoundsChanged = true;
      return;
    } 
    this.mPendingNotifyBoundsChanged = false;
    Rect rect = this.mHost.getScreenBounds();
    if (rect.left < 0 || rect.top < 0)
      rect.offsetTo(0, 0); 
    WindowContainerTransaction windowContainerTransaction = new WindowContainerTransaction();
    windowContainerTransaction.setBounds(this.mTaskToken, rect);
    WindowOrganizer.applyTransaction(windowContainerTransaction);
  }
  
  public void performBackPress() {}
  
  public int getId() {
    return getTaskId();
  }
  
  protected ActivityOptions prepareActivityOptions(ActivityOptions paramActivityOptions) {
    paramActivityOptions = super.prepareActivityOptions(paramActivityOptions);
    paramActivityOptions.setLaunchWindowingMode(6);
    return paramActivityOptions;
  }
  
  private int getTaskId() {
    byte b;
    ActivityManager.RunningTaskInfo runningTaskInfo = this.mTaskInfo;
    if (runningTaskInfo != null) {
      b = runningTaskInfo.taskId;
    } else {
      b = -1;
    } 
    return b;
  }
  
  private void resetTaskInfo() {
    this.mTaskInfo = null;
    this.mTaskToken = null;
    this.mTaskLeash = null;
  }
  
  private void log(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    stringBuilder.append(System.identityHashCode(this));
    stringBuilder.append("] ");
    stringBuilder.append(paramString);
    Log.d("TaskOrgTaskEmbedder", stringBuilder.toString());
  }
  
  class TaskOrganizerImpl extends TaskOrganizer {
    final TaskOrganizerTaskEmbedder this$0;
    
    private TaskOrganizerImpl() {}
    
    public void onTaskAppeared(ActivityManager.RunningTaskInfo param1RunningTaskInfo, SurfaceControl param1SurfaceControl) {
      TaskOrganizerTaskEmbedder.access$102(TaskOrganizerTaskEmbedder.this, param1RunningTaskInfo);
      TaskOrganizerTaskEmbedder.access$202(TaskOrganizerTaskEmbedder.this, param1RunningTaskInfo.token);
      TaskOrganizerTaskEmbedder.access$302(TaskOrganizerTaskEmbedder.this, param1SurfaceControl);
      SurfaceControl.Transaction transaction = TaskOrganizerTaskEmbedder.this.mTransaction.reparent(TaskOrganizerTaskEmbedder.this.mTaskLeash, TaskOrganizerTaskEmbedder.this.mSurfaceControl);
      TaskOrganizerTaskEmbedder taskOrganizerTaskEmbedder = TaskOrganizerTaskEmbedder.this;
      transaction = transaction.show(taskOrganizerTaskEmbedder.mTaskLeash);
      SurfaceControl surfaceControl = TaskOrganizerTaskEmbedder.this.mSurfaceControl;
      transaction = transaction.show(surfaceControl);
      transaction.apply();
      if (TaskOrganizerTaskEmbedder.this.mPendingNotifyBoundsChanged)
        TaskOrganizerTaskEmbedder.this.notifyBoundsChanged(); 
      TaskOrganizerTaskEmbedder.this.mHost.post(new _$$Lambda$TaskOrganizerTaskEmbedder$TaskOrganizerImpl$18tjzvdxudjYJtLPS_3LO_6XhJU(this, param1RunningTaskInfo));
      if (TaskOrganizerTaskEmbedder.this.mListener != null)
        TaskOrganizerTaskEmbedder.this.mListener.onTaskCreated(param1RunningTaskInfo.taskId, param1RunningTaskInfo.baseActivity); 
    }
    
    public void onTaskInfoChanged(ActivityManager.RunningTaskInfo param1RunningTaskInfo) {
      TaskOrganizerTaskEmbedder.this.mTaskInfo.taskDescription = param1RunningTaskInfo.taskDescription;
      TaskOrganizerTaskEmbedder.this.mHost.post(new _$$Lambda$TaskOrganizerTaskEmbedder$TaskOrganizerImpl$lWy60hv4_xWQkR7DgDtDHy9IHHc(this, param1RunningTaskInfo));
    }
    
    public void onTaskVanished(ActivityManager.RunningTaskInfo param1RunningTaskInfo) {
      if (TaskOrganizerTaskEmbedder.this.mTaskToken != null) {
        if (param1RunningTaskInfo != null) {
          TaskOrganizerTaskEmbedder taskOrganizerTaskEmbedder = TaskOrganizerTaskEmbedder.this;
          if (taskOrganizerTaskEmbedder.mTaskToken.asBinder().equals(param1RunningTaskInfo.token.asBinder())) {
            if (TaskOrganizerTaskEmbedder.this.mListener != null)
              TaskOrganizerTaskEmbedder.this.mListener.onTaskRemovalStarted(param1RunningTaskInfo.taskId); 
            TaskOrganizerTaskEmbedder.this.resetTaskInfo();
            return;
          } 
          return;
        } 
      } else {
        return;
      } 
      if (TaskOrganizerTaskEmbedder.this.mListener != null)
        TaskOrganizerTaskEmbedder.this.mListener.onTaskRemovalStarted(param1RunningTaskInfo.taskId); 
      TaskOrganizerTaskEmbedder.this.resetTaskInfo();
    }
    
    public void onBackPressedOnTaskRoot(ActivityManager.RunningTaskInfo param1RunningTaskInfo) {
      if (TaskOrganizerTaskEmbedder.this.mListener != null)
        TaskOrganizerTaskEmbedder.this.mListener.onBackPressedOnTaskRoot(param1RunningTaskInfo.taskId); 
    }
  }
}
