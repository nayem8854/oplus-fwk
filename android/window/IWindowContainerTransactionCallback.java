package android.window;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.SurfaceControl;

public interface IWindowContainerTransactionCallback extends IInterface {
  void onTransactionReady(int paramInt, SurfaceControl.Transaction paramTransaction) throws RemoteException;
  
  class Default implements IWindowContainerTransactionCallback {
    public void onTransactionReady(int param1Int, SurfaceControl.Transaction param1Transaction) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWindowContainerTransactionCallback {
    private static final String DESCRIPTOR = "android.window.IWindowContainerTransactionCallback";
    
    static final int TRANSACTION_onTransactionReady = 1;
    
    public Stub() {
      attachInterface(this, "android.window.IWindowContainerTransactionCallback");
    }
    
    public static IWindowContainerTransactionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.window.IWindowContainerTransactionCallback");
      if (iInterface != null && iInterface instanceof IWindowContainerTransactionCallback)
        return (IWindowContainerTransactionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onTransactionReady";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.window.IWindowContainerTransactionCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.window.IWindowContainerTransactionCallback");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        SurfaceControl.Transaction transaction = (SurfaceControl.Transaction)SurfaceControl.Transaction.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onTransactionReady(param1Int1, (SurfaceControl.Transaction)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IWindowContainerTransactionCallback {
      public static IWindowContainerTransactionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.window.IWindowContainerTransactionCallback";
      }
      
      public void onTransactionReady(int param2Int, SurfaceControl.Transaction param2Transaction) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.window.IWindowContainerTransactionCallback");
          parcel.writeInt(param2Int);
          if (param2Transaction != null) {
            parcel.writeInt(1);
            param2Transaction.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IWindowContainerTransactionCallback.Stub.getDefaultImpl() != null) {
            IWindowContainerTransactionCallback.Stub.getDefaultImpl().onTransactionReady(param2Int, param2Transaction);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWindowContainerTransactionCallback param1IWindowContainerTransactionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWindowContainerTransactionCallback != null) {
          Proxy.sDefaultImpl = param1IWindowContainerTransactionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWindowContainerTransactionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
