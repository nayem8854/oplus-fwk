package android.opengl;

import javax.microedition.khronos.opengles.GL10;

public class GLU {
  public static String gluErrorString(int paramInt) {
    if (paramInt != 0) {
      switch (paramInt) {
        default:
          return null;
        case 1285:
          return "out of memory";
        case 1284:
          return "stack underflow";
        case 1283:
          return "stack overflow";
        case 1282:
          return "invalid operation";
        case 1281:
          return "invalid value";
        case 1280:
          break;
      } 
      return "invalid enum";
    } 
    return "no error";
  }
  
  public static void gluLookAt(GL10 paramGL10, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8, float paramFloat9) {
    float[] arrayOfFloat = sScratch;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{F, dimension=1}, name=null} */
    try {
      Matrix.setLookAtM(arrayOfFloat, 0, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, paramFloat7, paramFloat8, paramFloat9);
      try {
        paramGL10.glMultMatrixf(arrayOfFloat, 0);
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{F, dimension=1}, name=null} */
        return;
      } finally {}
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{F, dimension=1}, name=null} */
    throw paramGL10;
  }
  
  public static void gluOrtho2D(GL10 paramGL10, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    paramGL10.glOrthof(paramFloat1, paramFloat2, paramFloat3, paramFloat4, -1.0F, 1.0F);
  }
  
  public static void gluPerspective(GL10 paramGL10, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    paramFloat1 = (float)Math.tan(paramFloat1 * 0.008726646259971648D) * paramFloat3;
    float f = -paramFloat1;
    paramGL10.glFrustumf(f * paramFloat2, paramFloat1 * paramFloat2, f, paramFloat1, paramFloat3, paramFloat4);
  }
  
  public static int gluProject(float paramFloat1, float paramFloat2, float paramFloat3, float[] paramArrayOffloat1, int paramInt1, float[] paramArrayOffloat2, int paramInt2, int[] paramArrayOfint, int paramInt3, float[] paramArrayOffloat3, int paramInt4) {
    synchronized (sScratch) {
      Matrix.multiplyMM(null, 0, paramArrayOffloat2, paramInt2, paramArrayOffloat1, paramInt1);
      null[16] = paramFloat1;
      null[17] = paramFloat2;
      null[18] = paramFloat3;
      null[19] = 1.0F;
      Matrix.multiplyMV(null, 20, null, 0, null, 16);
      paramFloat1 = null[23];
      if (paramFloat1 == 0.0F)
        return 0; 
      paramFloat1 = 1.0F / paramFloat1;
      paramArrayOffloat3[paramInt4] = paramArrayOfint[paramInt3] + paramArrayOfint[paramInt3 + 2] * (null[20] * paramFloat1 + 1.0F) * 0.5F;
      paramArrayOffloat3[paramInt4 + 1] = paramArrayOfint[paramInt3 + 1] + paramArrayOfint[paramInt3 + 3] * (null[21] * paramFloat1 + 1.0F) * 0.5F;
      paramArrayOffloat3[paramInt4 + 2] = (null[22] * paramFloat1 + 1.0F) * 0.5F;
      return 1;
    } 
  }
  
  public static int gluUnProject(float paramFloat1, float paramFloat2, float paramFloat3, float[] paramArrayOffloat1, int paramInt1, float[] paramArrayOffloat2, int paramInt2, int[] paramArrayOfint, int paramInt3, float[] paramArrayOffloat3, int paramInt4) {
    synchronized (sScratch) {
      Matrix.multiplyMM(null, 0, paramArrayOffloat2, paramInt2, paramArrayOffloat1, paramInt1);
      if (!Matrix.invertM(null, 16, null, 0))
        return 0; 
      null[0] = (paramFloat1 - paramArrayOfint[paramInt3 + 0]) * 2.0F / paramArrayOfint[paramInt3 + 2] - 1.0F;
      null[1] = (paramFloat2 - paramArrayOfint[paramInt3 + 1]) * 2.0F / paramArrayOfint[paramInt3 + 3] - 1.0F;
      null[2] = paramFloat3 * 2.0F - 1.0F;
      null[3] = 1.0F;
      Matrix.multiplyMV(paramArrayOffloat3, paramInt4, null, 16, null, 0);
      return 1;
    } 
  }
  
  private static final float[] sScratch = new float[32];
}
