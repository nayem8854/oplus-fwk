package android.opengl;

import java.io.IOException;
import java.io.Writer;
import javax.microedition.khronos.egl.EGL;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGL11;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;

class EGLLogWrapper implements EGL11 {
  private int mArgCount;
  
  boolean mCheckError;
  
  private EGL10 mEgl10;
  
  Writer mLog;
  
  boolean mLogArgumentNames;
  
  public EGLLogWrapper(EGL paramEGL, int paramInt, Writer paramWriter) {
    this.mEgl10 = (EGL10)paramEGL;
    this.mLog = paramWriter;
    boolean bool1 = false;
    if ((paramInt & 0x4) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mLogArgumentNames = bool2;
    boolean bool2 = bool1;
    if ((paramInt & 0x1) != 0)
      bool2 = true; 
    this.mCheckError = bool2;
  }
  
  public boolean eglChooseConfig(EGLDisplay paramEGLDisplay, int[] paramArrayOfint1, EGLConfig[] paramArrayOfEGLConfig, int paramInt, int[] paramArrayOfint2) {
    begin("eglChooseConfig");
    arg("display", paramEGLDisplay);
    arg("attrib_list", paramArrayOfint1);
    arg("config_size", paramInt);
    end();
    boolean bool = this.mEgl10.eglChooseConfig(paramEGLDisplay, paramArrayOfint1, paramArrayOfEGLConfig, paramInt, paramArrayOfint2);
    arg("configs", (Object[])paramArrayOfEGLConfig);
    arg("num_config", paramArrayOfint2);
    returns(bool);
    checkError();
    return bool;
  }
  
  public boolean eglCopyBuffers(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface, Object paramObject) {
    begin("eglCopyBuffers");
    arg("display", paramEGLDisplay);
    arg("surface", paramEGLSurface);
    arg("native_pixmap", paramObject);
    end();
    boolean bool = this.mEgl10.eglCopyBuffers(paramEGLDisplay, paramEGLSurface, paramObject);
    returns(bool);
    checkError();
    return bool;
  }
  
  public EGLContext eglCreateContext(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, EGLContext paramEGLContext, int[] paramArrayOfint) {
    begin("eglCreateContext");
    arg("display", paramEGLDisplay);
    arg("config", paramEGLConfig);
    arg("share_context", paramEGLContext);
    arg("attrib_list", paramArrayOfint);
    end();
    EGLContext eGLContext = this.mEgl10.eglCreateContext(paramEGLDisplay, paramEGLConfig, paramEGLContext, paramArrayOfint);
    returns(eGLContext);
    checkError();
    return eGLContext;
  }
  
  public EGLSurface eglCreatePbufferSurface(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, int[] paramArrayOfint) {
    begin("eglCreatePbufferSurface");
    arg("display", paramEGLDisplay);
    arg("config", paramEGLConfig);
    arg("attrib_list", paramArrayOfint);
    end();
    EGLSurface eGLSurface = this.mEgl10.eglCreatePbufferSurface(paramEGLDisplay, paramEGLConfig, paramArrayOfint);
    returns(eGLSurface);
    checkError();
    return eGLSurface;
  }
  
  public EGLSurface eglCreatePixmapSurface(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject, int[] paramArrayOfint) {
    begin("eglCreatePixmapSurface");
    arg("display", paramEGLDisplay);
    arg("config", paramEGLConfig);
    arg("native_pixmap", paramObject);
    arg("attrib_list", paramArrayOfint);
    end();
    EGLSurface eGLSurface = this.mEgl10.eglCreatePixmapSurface(paramEGLDisplay, paramEGLConfig, paramObject, paramArrayOfint);
    returns(eGLSurface);
    checkError();
    return eGLSurface;
  }
  
  public EGLSurface eglCreateWindowSurface(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, Object paramObject, int[] paramArrayOfint) {
    begin("eglCreateWindowSurface");
    arg("display", paramEGLDisplay);
    arg("config", paramEGLConfig);
    arg("native_window", paramObject);
    arg("attrib_list", paramArrayOfint);
    end();
    EGLSurface eGLSurface = this.mEgl10.eglCreateWindowSurface(paramEGLDisplay, paramEGLConfig, paramObject, paramArrayOfint);
    returns(eGLSurface);
    checkError();
    return eGLSurface;
  }
  
  public boolean eglDestroyContext(EGLDisplay paramEGLDisplay, EGLContext paramEGLContext) {
    begin("eglDestroyContext");
    arg("display", paramEGLDisplay);
    arg("context", paramEGLContext);
    end();
    boolean bool = this.mEgl10.eglDestroyContext(paramEGLDisplay, paramEGLContext);
    returns(bool);
    checkError();
    return bool;
  }
  
  public boolean eglDestroySurface(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface) {
    begin("eglDestroySurface");
    arg("display", paramEGLDisplay);
    arg("surface", paramEGLSurface);
    end();
    boolean bool = this.mEgl10.eglDestroySurface(paramEGLDisplay, paramEGLSurface);
    returns(bool);
    checkError();
    return bool;
  }
  
  public boolean eglGetConfigAttrib(EGLDisplay paramEGLDisplay, EGLConfig paramEGLConfig, int paramInt, int[] paramArrayOfint) {
    begin("eglGetConfigAttrib");
    arg("display", paramEGLDisplay);
    arg("config", paramEGLConfig);
    arg("attribute", paramInt);
    end();
    boolean bool = this.mEgl10.eglGetConfigAttrib(paramEGLDisplay, paramEGLConfig, paramInt, paramArrayOfint);
    arg("value", paramArrayOfint);
    returns(bool);
    checkError();
    return false;
  }
  
  public boolean eglGetConfigs(EGLDisplay paramEGLDisplay, EGLConfig[] paramArrayOfEGLConfig, int paramInt, int[] paramArrayOfint) {
    begin("eglGetConfigs");
    arg("display", paramEGLDisplay);
    arg("config_size", paramInt);
    end();
    boolean bool = this.mEgl10.eglGetConfigs(paramEGLDisplay, paramArrayOfEGLConfig, paramInt, paramArrayOfint);
    arg("configs", (Object[])paramArrayOfEGLConfig);
    arg("num_config", paramArrayOfint);
    returns(bool);
    checkError();
    return bool;
  }
  
  public EGLContext eglGetCurrentContext() {
    begin("eglGetCurrentContext");
    end();
    EGLContext eGLContext = this.mEgl10.eglGetCurrentContext();
    returns(eGLContext);
    checkError();
    return eGLContext;
  }
  
  public EGLDisplay eglGetCurrentDisplay() {
    begin("eglGetCurrentDisplay");
    end();
    EGLDisplay eGLDisplay = this.mEgl10.eglGetCurrentDisplay();
    returns(eGLDisplay);
    checkError();
    return eGLDisplay;
  }
  
  public EGLSurface eglGetCurrentSurface(int paramInt) {
    begin("eglGetCurrentSurface");
    arg("readdraw", paramInt);
    end();
    EGLSurface eGLSurface = this.mEgl10.eglGetCurrentSurface(paramInt);
    returns(eGLSurface);
    checkError();
    return eGLSurface;
  }
  
  public EGLDisplay eglGetDisplay(Object paramObject) {
    begin("eglGetDisplay");
    arg("native_display", paramObject);
    end();
    paramObject = this.mEgl10.eglGetDisplay(paramObject);
    returns(paramObject);
    checkError();
    return (EGLDisplay)paramObject;
  }
  
  public int eglGetError() {
    begin("eglGetError");
    end();
    int i = this.mEgl10.eglGetError();
    returns(getErrorString(i));
    return i;
  }
  
  public boolean eglInitialize(EGLDisplay paramEGLDisplay, int[] paramArrayOfint) {
    begin("eglInitialize");
    arg("display", paramEGLDisplay);
    end();
    boolean bool = this.mEgl10.eglInitialize(paramEGLDisplay, paramArrayOfint);
    returns(bool);
    arg("major_minor", paramArrayOfint);
    checkError();
    return bool;
  }
  
  public boolean eglMakeCurrent(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface1, EGLSurface paramEGLSurface2, EGLContext paramEGLContext) {
    begin("eglMakeCurrent");
    arg("display", paramEGLDisplay);
    arg("draw", paramEGLSurface1);
    arg("read", paramEGLSurface2);
    arg("context", paramEGLContext);
    end();
    boolean bool = this.mEgl10.eglMakeCurrent(paramEGLDisplay, paramEGLSurface1, paramEGLSurface2, paramEGLContext);
    returns(bool);
    checkError();
    return bool;
  }
  
  public boolean eglQueryContext(EGLDisplay paramEGLDisplay, EGLContext paramEGLContext, int paramInt, int[] paramArrayOfint) {
    begin("eglQueryContext");
    arg("display", paramEGLDisplay);
    arg("context", paramEGLContext);
    arg("attribute", paramInt);
    end();
    boolean bool = this.mEgl10.eglQueryContext(paramEGLDisplay, paramEGLContext, paramInt, paramArrayOfint);
    returns(paramArrayOfint[0]);
    returns(bool);
    checkError();
    return bool;
  }
  
  public String eglQueryString(EGLDisplay paramEGLDisplay, int paramInt) {
    begin("eglQueryString");
    arg("display", paramEGLDisplay);
    arg("name", paramInt);
    end();
    String str = this.mEgl10.eglQueryString(paramEGLDisplay, paramInt);
    returns(str);
    checkError();
    return str;
  }
  
  public boolean eglQuerySurface(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface, int paramInt, int[] paramArrayOfint) {
    begin("eglQuerySurface");
    arg("display", paramEGLDisplay);
    arg("surface", paramEGLSurface);
    arg("attribute", paramInt);
    end();
    boolean bool = this.mEgl10.eglQuerySurface(paramEGLDisplay, paramEGLSurface, paramInt, paramArrayOfint);
    returns(paramArrayOfint[0]);
    returns(bool);
    checkError();
    return bool;
  }
  
  public boolean eglReleaseThread() {
    begin("eglReleaseThread");
    end();
    boolean bool = this.mEgl10.eglReleaseThread();
    returns(bool);
    checkError();
    return bool;
  }
  
  public boolean eglSwapBuffers(EGLDisplay paramEGLDisplay, EGLSurface paramEGLSurface) {
    begin("eglSwapBuffers");
    arg("display", paramEGLDisplay);
    arg("surface", paramEGLSurface);
    end();
    boolean bool = this.mEgl10.eglSwapBuffers(paramEGLDisplay, paramEGLSurface);
    returns(bool);
    checkError();
    return bool;
  }
  
  public boolean eglTerminate(EGLDisplay paramEGLDisplay) {
    begin("eglTerminate");
    arg("display", paramEGLDisplay);
    end();
    boolean bool = this.mEgl10.eglTerminate(paramEGLDisplay);
    returns(bool);
    checkError();
    return bool;
  }
  
  public boolean eglWaitGL() {
    begin("eglWaitGL");
    end();
    boolean bool = this.mEgl10.eglWaitGL();
    returns(bool);
    checkError();
    return bool;
  }
  
  public boolean eglWaitNative(int paramInt, Object paramObject) {
    begin("eglWaitNative");
    arg("engine", paramInt);
    arg("bindTarget", paramObject);
    end();
    boolean bool = this.mEgl10.eglWaitNative(paramInt, paramObject);
    returns(bool);
    checkError();
    return bool;
  }
  
  private void checkError() {
    int i = this.mEgl10.eglGetError();
    if (i != 12288) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("eglError: ");
      stringBuilder.append(getErrorString(i));
      String str = stringBuilder.toString();
      logLine(str);
      if (this.mCheckError)
        throw new GLException(i, str); 
    } 
  }
  
  private void logLine(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append('\n');
    log(stringBuilder.toString());
  }
  
  private void log(String paramString) {
    try {
      this.mLog.write(paramString);
    } catch (IOException iOException) {}
  }
  
  private void begin(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append('(');
    log(stringBuilder.toString());
    this.mArgCount = 0;
  }
  
  private void arg(String paramString1, String paramString2) {
    int i = this.mArgCount;
    this.mArgCount = i + 1;
    if (i > 0)
      log(", "); 
    if (this.mLogArgumentNames) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("=");
      log(stringBuilder.toString());
    } 
    log(paramString2);
  }
  
  private void end() {
    log(");\n");
    flush();
  }
  
  private void flush() {
    try {
      this.mLog.flush();
    } catch (IOException iOException) {
      this.mLog = null;
    } 
  }
  
  private void arg(String paramString, int paramInt) {
    arg(paramString, Integer.toString(paramInt));
  }
  
  private void arg(String paramString, Object paramObject) {
    arg(paramString, toString(paramObject));
  }
  
  private void arg(String paramString, EGLDisplay paramEGLDisplay) {
    if (paramEGLDisplay == EGL10.EGL_DEFAULT_DISPLAY) {
      arg(paramString, "EGL10.EGL_DEFAULT_DISPLAY");
    } else if (paramEGLDisplay == EGL_NO_DISPLAY) {
      arg(paramString, "EGL10.EGL_NO_DISPLAY");
    } else {
      arg(paramString, toString(paramEGLDisplay));
    } 
  }
  
  private void arg(String paramString, EGLContext paramEGLContext) {
    if (paramEGLContext == EGL10.EGL_NO_CONTEXT) {
      arg(paramString, "EGL10.EGL_NO_CONTEXT");
    } else {
      arg(paramString, toString(paramEGLContext));
    } 
  }
  
  private void arg(String paramString, EGLSurface paramEGLSurface) {
    if (paramEGLSurface == EGL10.EGL_NO_SURFACE) {
      arg(paramString, "EGL10.EGL_NO_SURFACE");
    } else {
      arg(paramString, toString(paramEGLSurface));
    } 
  }
  
  private void returns(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" returns ");
    stringBuilder.append(paramString);
    stringBuilder.append(";\n");
    log(stringBuilder.toString());
    flush();
  }
  
  private void returns(int paramInt) {
    returns(Integer.toString(paramInt));
  }
  
  private void returns(boolean paramBoolean) {
    returns(Boolean.toString(paramBoolean));
  }
  
  private void returns(Object paramObject) {
    returns(toString(paramObject));
  }
  
  private String toString(Object paramObject) {
    if (paramObject == null)
      return "null"; 
    return paramObject.toString();
  }
  
  private void arg(String paramString, int[] paramArrayOfint) {
    if (paramArrayOfint == null) {
      arg(paramString, "null");
    } else {
      arg(paramString, toString(paramArrayOfint.length, paramArrayOfint, 0));
    } 
  }
  
  private void arg(String paramString, Object[] paramArrayOfObject) {
    if (paramArrayOfObject == null) {
      arg(paramString, "null");
    } else {
      arg(paramString, toString(paramArrayOfObject.length, paramArrayOfObject, 0));
    } 
  }
  
  private String toString(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{\n");
    int i = paramArrayOfint.length;
    for (byte b = 0; b < paramInt1; b++) {
      int j = paramInt2 + b;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(" [");
      stringBuilder1.append(j);
      stringBuilder1.append("] = ");
      stringBuilder.append(stringBuilder1.toString());
      if (j < 0 || j >= i) {
        stringBuilder.append("out of bounds");
      } else {
        stringBuilder.append(paramArrayOfint[j]);
      } 
      stringBuilder.append('\n');
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private String toString(int paramInt1, Object[] paramArrayOfObject, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{\n");
    int i = paramArrayOfObject.length;
    for (byte b = 0; b < paramInt1; b++) {
      int j = paramInt2 + b;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(" [");
      stringBuilder1.append(j);
      stringBuilder1.append("] = ");
      stringBuilder.append(stringBuilder1.toString());
      if (j < 0 || j >= i) {
        stringBuilder.append("out of bounds");
      } else {
        stringBuilder.append(paramArrayOfObject[j]);
      } 
      stringBuilder.append('\n');
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private static String getHex(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static String getErrorString(int paramInt) {
    switch (paramInt) {
      default:
        return getHex(paramInt);
      case 12302:
        return "EGL_CONTEXT_LOST";
      case 12301:
        return "EGL_BAD_SURFACE";
      case 12300:
        return "EGL_BAD_PARAMETER";
      case 12299:
        return "EGL_BAD_NATIVE_WINDOW";
      case 12298:
        return "EGL_BAD_NATIVE_PIXMAP";
      case 12297:
        return "EGL_BAD_MATCH";
      case 12296:
        return "EGL_BAD_DISPLAY";
      case 12295:
        return "EGL_BAD_CURRENT_SURFACE";
      case 12294:
        return "EGL_BAD_CONTEXT";
      case 12293:
        return "EGL_BAD_CONFIG";
      case 12292:
        return "EGL_BAD_ATTRIBUTE";
      case 12291:
        return "EGL_BAD_ALLOC";
      case 12290:
        return "EGL_BAD_ACCESS";
      case 12289:
        return "EGL_NOT_INITIALIZED";
      case 12288:
        break;
    } 
    return "EGL_SUCCESS";
  }
}
