package android.opengl;

import java.io.Writer;
import javax.microedition.khronos.egl.EGL;
import javax.microedition.khronos.opengles.GL;

public class GLDebugHelper {
  public static final int CONFIG_CHECK_GL_ERROR = 1;
  
  public static final int CONFIG_CHECK_THREAD = 2;
  
  public static final int CONFIG_LOG_ARGUMENT_NAMES = 4;
  
  public static final int ERROR_WRONG_THREAD = 28672;
  
  public static GL wrap(GL paramGL, int paramInt, Writer paramWriter) {
    GL gL = paramGL;
    if (paramInt != 0)
      gL = new GLErrorWrapper(paramGL, paramInt); 
    paramGL = gL;
    if (paramWriter != null) {
      boolean bool;
      if ((paramInt & 0x4) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      paramGL = new GLLogWrapper(gL, paramWriter, bool);
    } 
    return paramGL;
  }
  
  public static EGL wrap(EGL paramEGL, int paramInt, Writer paramWriter) {
    EGLLogWrapper eGLLogWrapper;
    EGL eGL = paramEGL;
    if (paramWriter != null)
      eGLLogWrapper = new EGLLogWrapper(paramEGL, paramInt, paramWriter); 
    return (EGL)eGLLogWrapper;
  }
}
