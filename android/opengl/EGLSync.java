package android.opengl;

public class EGLSync extends EGLObjectHandle {
  private EGLSync(long paramLong) {
    super(paramLong);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof EGLSync))
      return false; 
    paramObject = paramObject;
    if (getNativeHandle() != paramObject.getNativeHandle())
      bool = false; 
    return bool;
  }
}
