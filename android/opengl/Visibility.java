package android.opengl;

public class Visibility {
  public static native void computeBoundingSphere(float[] paramArrayOffloat1, int paramInt1, int paramInt2, float[] paramArrayOffloat2, int paramInt3);
  
  public static native int frustumCullSpheres(float[] paramArrayOffloat1, int paramInt1, float[] paramArrayOffloat2, int paramInt2, int paramInt3, int[] paramArrayOfint, int paramInt4, int paramInt5);
  
  public static native int visibilityTest(float[] paramArrayOffloat1, int paramInt1, float[] paramArrayOffloat2, int paramInt2, char[] paramArrayOfchar, int paramInt3, int paramInt4);
}
