package android.opengl;

public abstract class EGLObjectHandle {
  private final long mHandle;
  
  @Deprecated
  protected EGLObjectHandle(int paramInt) {
    this.mHandle = paramInt;
  }
  
  protected EGLObjectHandle(long paramLong) {
    this.mHandle = paramLong;
  }
  
  @Deprecated
  public int getHandle() {
    long l = this.mHandle;
    if ((0xFFFFFFFFL & l) == l)
      return (int)l; 
    throw new UnsupportedOperationException();
  }
  
  public long getNativeHandle() {
    return this.mHandle;
  }
  
  public int hashCode() {
    long l = this.mHandle;
    int i = (int)(l ^ l >>> 32L);
    return 17 * 31 + i;
  }
}
