package android.opengl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ETC1Util {
  public static void loadTexture(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, InputStream paramInputStream) throws IOException {
    loadTexture(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, createTexture(paramInputStream));
  }
  
  public static void loadTexture(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, ETC1Texture paramETC1Texture) {
    if (paramInt4 == 6407) {
      if (paramInt5 == 33635 || paramInt5 == 5121) {
        int i = paramETC1Texture.getWidth();
        int j = paramETC1Texture.getHeight();
        ByteBuffer byteBuffer = paramETC1Texture.getData();
        if (isETC1Supported()) {
          paramInt4 = byteBuffer.remaining();
          GLES10.glCompressedTexImage2D(paramInt1, paramInt2, 36196, i, j, paramInt3, paramInt4, byteBuffer);
        } else {
          byte b;
          if (paramInt5 != 5121) {
            b = 1;
          } else {
            b = 0;
          } 
          if (b) {
            b = 2;
          } else {
            b = 3;
          } 
          int k = b * i;
          ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(k * j);
          byteBuffer1 = byteBuffer1.order(ByteOrder.nativeOrder());
          ETC1.decodeImage(byteBuffer, byteBuffer1, i, j, b, k);
          GLES10.glTexImage2D(paramInt1, paramInt2, paramInt4, i, j, paramInt3, paramInt4, paramInt5, byteBuffer1);
        } 
        return;
      } 
      throw new IllegalArgumentException("Unsupported fallbackType");
    } 
    throw new IllegalArgumentException("fallbackFormat must be GL_RGB");
  }
  
  public static boolean isETC1Supported() {
    int[] arrayOfInt1 = new int[20];
    GLES10.glGetIntegerv(34466, arrayOfInt1, 0);
    int i = arrayOfInt1[0];
    int[] arrayOfInt2 = arrayOfInt1;
    if (i > arrayOfInt1.length)
      arrayOfInt2 = new int[i]; 
    GLES10.glGetIntegerv(34467, arrayOfInt2, 0);
    for (byte b = 0; b < i; b++) {
      if (arrayOfInt2[b] == 36196)
        return true; 
    } 
    return false;
  }
  
  public static class ETC1Texture {
    private ByteBuffer mData;
    
    private int mHeight;
    
    private int mWidth;
    
    public ETC1Texture(int param1Int1, int param1Int2, ByteBuffer param1ByteBuffer) {
      this.mWidth = param1Int1;
      this.mHeight = param1Int2;
      this.mData = param1ByteBuffer;
    }
    
    public int getWidth() {
      return this.mWidth;
    }
    
    public int getHeight() {
      return this.mHeight;
    }
    
    public ByteBuffer getData() {
      return this.mData;
    }
  }
  
  public static ETC1Texture createTexture(InputStream paramInputStream) throws IOException {
    byte[] arrayOfByte = new byte[4096];
    if (paramInputStream.read(arrayOfByte, 0, 16) == 16) {
      ByteBuffer byteBuffer = ByteBuffer.allocateDirect(16);
      byteBuffer = byteBuffer.order(ByteOrder.nativeOrder());
      byteBuffer.put(arrayOfByte, 0, 16).position(0);
      if (ETC1.isValid(byteBuffer)) {
        int i = ETC1.getWidth(byteBuffer);
        int j = ETC1.getHeight(byteBuffer);
        int k = ETC1.getEncodedDataSize(i, j);
        byteBuffer = ByteBuffer.allocateDirect(k).order(ByteOrder.nativeOrder());
        for (int m = 0; m < k; ) {
          int n = Math.min(arrayOfByte.length, k - m);
          if (paramInputStream.read(arrayOfByte, 0, n) == n) {
            byteBuffer.put(arrayOfByte, 0, n);
            m += n;
            continue;
          } 
          throw new IOException("Unable to read PKM file data.");
        } 
        byteBuffer.position(0);
        return new ETC1Texture(i, j, byteBuffer);
      } 
      throw new IOException("Not a PKM file.");
    } 
    throw new IOException("Unable to read PKM file header.");
  }
  
  public static ETC1Texture compressTexture(Buffer paramBuffer, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    int i = ETC1.getEncodedDataSize(paramInt1, paramInt2);
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(i);
    byteBuffer = byteBuffer.order(ByteOrder.nativeOrder());
    ETC1.encodeImage(paramBuffer, paramInt1, paramInt2, paramInt3, paramInt4, byteBuffer);
    return new ETC1Texture(paramInt1, paramInt2, byteBuffer);
  }
  
  public static void writeTexture(ETC1Texture paramETC1Texture, OutputStream paramOutputStream) throws IOException {
    ByteBuffer byteBuffer = paramETC1Texture.getData();
    int i = byteBuffer.position();
    try {
      int j = paramETC1Texture.getWidth();
      int k = paramETC1Texture.getHeight();
      ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(16).order(ByteOrder.nativeOrder());
      ETC1.formatHeader(byteBuffer1, j, k);
      byte[] arrayOfByte = new byte[4096];
      byteBuffer1.get(arrayOfByte, 0, 16);
      paramOutputStream.write(arrayOfByte, 0, 16);
      j = ETC1.getEncodedDataSize(j, k);
      for (k = 0; k < j; ) {
        int m = Math.min(arrayOfByte.length, j - k);
        byteBuffer.get(arrayOfByte, 0, m);
        paramOutputStream.write(arrayOfByte, 0, m);
        k += m;
      } 
      return;
    } finally {
      byteBuffer.position(i);
    } 
  }
}
