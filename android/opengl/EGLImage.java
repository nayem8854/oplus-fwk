package android.opengl;

public class EGLImage extends EGLObjectHandle {
  private EGLImage(long paramLong) {
    super(paramLong);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof EGLImage))
      return false; 
    paramObject = paramObject;
    if (getNativeHandle() != paramObject.getNativeHandle())
      bool = false; 
    return bool;
  }
}
