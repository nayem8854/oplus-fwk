package android.opengl;

public class GLException extends RuntimeException {
  private final int mError;
  
  public GLException(int paramInt) {
    super(getErrorString(paramInt));
    this.mError = paramInt;
  }
  
  public GLException(int paramInt, String paramString) {
    super(paramString);
    this.mError = paramInt;
  }
  
  private static String getErrorString(int paramInt) {
    String str1 = GLU.gluErrorString(paramInt);
    String str2 = str1;
    if (str1 == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown error 0x");
      stringBuilder.append(Integer.toHexString(paramInt));
      str2 = stringBuilder.toString();
    } 
    return str2;
  }
  
  int getError() {
    return this.mError;
  }
}
