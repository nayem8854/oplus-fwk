package android.opengl;

import java.nio.IntBuffer;

public class GLES10Ext {
  static {
    _nativeClassInit();
  }
  
  private static native void _nativeClassInit();
  
  public static native int glQueryMatrixxOES(IntBuffer paramIntBuffer1, IntBuffer paramIntBuffer2);
  
  public static native int glQueryMatrixxOES(int[] paramArrayOfint1, int paramInt1, int[] paramArrayOfint2, int paramInt2);
}
