package android.opengl;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.io.Writer;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

public class GLSurfaceView extends SurfaceView implements SurfaceHolder.Callback2 {
  public static final int DEBUG_CHECK_GL_ERROR = 1;
  
  public static final int DEBUG_LOG_GL_CALLS = 2;
  
  private static final boolean LOG_ATTACH_DETACH = false;
  
  private static final boolean LOG_EGL = false;
  
  private static final boolean LOG_PAUSE_RESUME = false;
  
  private static final boolean LOG_RENDERER = false;
  
  private static final boolean LOG_RENDERER_DRAW_FRAME = false;
  
  private static final boolean LOG_SURFACE = false;
  
  private static final boolean LOG_THREADS = false;
  
  public static final int RENDERMODE_CONTINUOUSLY = 1;
  
  public static final int RENDERMODE_WHEN_DIRTY = 0;
  
  private static final String TAG = "GLSurfaceView";
  
  public GLSurfaceView(Context paramContext) {
    super(paramContext);
    this.mThisWeakRef = new WeakReference<>(this);
    init();
  }
  
  public GLSurfaceView(Context paramContext, AttributeSet paramAttributeSet) {
    super(paramContext, paramAttributeSet);
    this.mThisWeakRef = new WeakReference<>(this);
    init();
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mGLThread != null)
        this.mGLThread.requestExitAndWait(); 
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private void init() {
    SurfaceHolder surfaceHolder = getHolder();
    surfaceHolder.addCallback((SurfaceHolder.Callback)this);
  }
  
  public void setGLWrapper(GLWrapper paramGLWrapper) {
    this.mGLWrapper = paramGLWrapper;
  }
  
  public void setDebugFlags(int paramInt) {
    this.mDebugFlags = paramInt;
  }
  
  public int getDebugFlags() {
    return this.mDebugFlags;
  }
  
  public void setPreserveEGLContextOnPause(boolean paramBoolean) {
    this.mPreserveEGLContextOnPause = paramBoolean;
  }
  
  public boolean getPreserveEGLContextOnPause() {
    return this.mPreserveEGLContextOnPause;
  }
  
  public void setRenderer(Renderer paramRenderer) {
    checkRenderThreadState();
    if (this.mEGLConfigChooser == null)
      this.mEGLConfigChooser = new SimpleEGLConfigChooser(true); 
    if (this.mEGLContextFactory == null)
      this.mEGLContextFactory = new DefaultContextFactory(); 
    if (this.mEGLWindowSurfaceFactory == null)
      this.mEGLWindowSurfaceFactory = new DefaultWindowSurfaceFactory(); 
    this.mRenderer = paramRenderer;
    GLThread gLThread = new GLThread(this.mThisWeakRef);
    gLThread.start();
  }
  
  public void setEGLContextFactory(EGLContextFactory paramEGLContextFactory) {
    checkRenderThreadState();
    this.mEGLContextFactory = paramEGLContextFactory;
  }
  
  public void setEGLWindowSurfaceFactory(EGLWindowSurfaceFactory paramEGLWindowSurfaceFactory) {
    checkRenderThreadState();
    this.mEGLWindowSurfaceFactory = paramEGLWindowSurfaceFactory;
  }
  
  public void setEGLConfigChooser(EGLConfigChooser paramEGLConfigChooser) {
    checkRenderThreadState();
    this.mEGLConfigChooser = paramEGLConfigChooser;
  }
  
  public void setEGLConfigChooser(boolean paramBoolean) {
    setEGLConfigChooser(new SimpleEGLConfigChooser(paramBoolean));
  }
  
  public void setEGLConfigChooser(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    setEGLConfigChooser(new ComponentSizeChooser(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6));
  }
  
  public void setEGLContextClientVersion(int paramInt) {
    checkRenderThreadState();
    this.mEGLContextClientVersion = paramInt;
  }
  
  public void setRenderMode(int paramInt) {
    this.mGLThread.setRenderMode(paramInt);
  }
  
  public int getRenderMode() {
    return this.mGLThread.getRenderMode();
  }
  
  public void requestRender() {
    this.mGLThread.requestRender();
  }
  
  public void surfaceCreated(SurfaceHolder paramSurfaceHolder) {
    this.mGLThread.surfaceCreated();
  }
  
  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder) {
    this.mGLThread.surfaceDestroyed();
  }
  
  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3) {
    this.mGLThread.onWindowResize(paramInt2, paramInt3);
  }
  
  public void surfaceRedrawNeededAsync(SurfaceHolder paramSurfaceHolder, Runnable paramRunnable) {
    GLThread gLThread = this.mGLThread;
    if (gLThread != null)
      gLThread.requestRenderAndNotify(paramRunnable); 
  }
  
  @Deprecated
  public void surfaceRedrawNeeded(SurfaceHolder paramSurfaceHolder) {}
  
  public void onPause() {
    this.mGLThread.onPause();
  }
  
  public void onResume() {
    this.mGLThread.onResume();
  }
  
  public void queueEvent(Runnable paramRunnable) {
    this.mGLThread.queueEvent(paramRunnable);
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (this.mDetached && this.mRenderer != null) {
      int i = 1;
      GLThread gLThread = this.mGLThread;
      if (gLThread != null)
        i = gLThread.getRenderMode(); 
      this.mGLThread = gLThread = new GLThread(this.mThisWeakRef);
      if (i != 1)
        gLThread.setRenderMode(i); 
      this.mGLThread.start();
    } 
    this.mDetached = false;
  }
  
  protected void onDetachedFromWindow() {
    GLThread gLThread = this.mGLThread;
    if (gLThread != null)
      gLThread.requestExitAndWait(); 
    this.mDetached = true;
    super.onDetachedFromWindow();
  }
  
  class DefaultContextFactory implements EGLContextFactory {
    private DefaultContextFactory() {}
    
    private int EGL_CONTEXT_CLIENT_VERSION = 12440;
    
    final GLSurfaceView this$0;
    
    public EGLContext createContext(EGL10 param1EGL10, EGLDisplay param1EGLDisplay, EGLConfig param1EGLConfig) {
      int arrayOfInt[], i = this.EGL_CONTEXT_CLIENT_VERSION, j = GLSurfaceView.this.mEGLContextClientVersion;
      EGLContext eGLContext = EGL10.EGL_NO_CONTEXT;
      if (GLSurfaceView.this.mEGLContextClientVersion != 0) {
        arrayOfInt = new int[] { i, j, 12344 };
      } else {
        arrayOfInt = null;
      } 
      return param1EGL10.eglCreateContext(param1EGLDisplay, param1EGLConfig, eGLContext, arrayOfInt);
    }
    
    public void destroyContext(EGL10 param1EGL10, EGLDisplay param1EGLDisplay, EGLContext param1EGLContext) {
      if (!param1EGL10.eglDestroyContext(param1EGLDisplay, param1EGLContext)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("display:");
        stringBuilder.append(param1EGLDisplay);
        stringBuilder.append(" context: ");
        stringBuilder.append(param1EGLContext);
        Log.e("DefaultContextFactory", stringBuilder.toString());
        GLSurfaceView.EglHelper.throwEglException("eglDestroyContex", param1EGL10.eglGetError());
      } 
    }
  }
  
  class DefaultWindowSurfaceFactory implements EGLWindowSurfaceFactory {
    private DefaultWindowSurfaceFactory() {}
    
    public EGLSurface createWindowSurface(EGL10 param1EGL10, EGLDisplay param1EGLDisplay, EGLConfig param1EGLConfig, Object param1Object) {
      IllegalArgumentException illegalArgumentException2 = null;
      try {
        EGLSurface eGLSurface = param1EGL10.eglCreateWindowSurface(param1EGLDisplay, param1EGLConfig, param1Object, null);
      } catch (IllegalArgumentException illegalArgumentException1) {
        Log.e("GLSurfaceView", "eglCreateWindowSurface", illegalArgumentException1);
        illegalArgumentException1 = illegalArgumentException2;
      } 
      return (EGLSurface)illegalArgumentException1;
    }
    
    public void destroySurface(EGL10 param1EGL10, EGLDisplay param1EGLDisplay, EGLSurface param1EGLSurface) {
      param1EGL10.eglDestroySurface(param1EGLDisplay, param1EGLSurface);
    }
  }
  
  class BaseConfigChooser implements EGLConfigChooser {
    protected int[] mConfigSpec;
    
    final GLSurfaceView this$0;
    
    public BaseConfigChooser(int[] param1ArrayOfint) {
      this.mConfigSpec = filterConfigSpec(param1ArrayOfint);
    }
    
    public EGLConfig chooseConfig(EGL10 param1EGL10, EGLDisplay param1EGLDisplay) {
      int[] arrayOfInt = new int[1];
      if (param1EGL10.eglChooseConfig(param1EGLDisplay, this.mConfigSpec, null, 0, arrayOfInt)) {
        int i = arrayOfInt[0];
        if (i > 0) {
          EGLConfig[] arrayOfEGLConfig = new EGLConfig[i];
          if (param1EGL10.eglChooseConfig(param1EGLDisplay, this.mConfigSpec, arrayOfEGLConfig, i, arrayOfInt)) {
            EGLConfig eGLConfig = chooseConfig(param1EGL10, param1EGLDisplay, arrayOfEGLConfig);
            if (eGLConfig != null)
              return eGLConfig; 
            throw new IllegalArgumentException("No config chosen");
          } 
          throw new IllegalArgumentException("eglChooseConfig#2 failed");
        } 
        throw new IllegalArgumentException("No configs match configSpec");
      } 
      throw new IllegalArgumentException("eglChooseConfig failed");
    }
    
    private int[] filterConfigSpec(int[] param1ArrayOfint) {
      if (GLSurfaceView.this.mEGLContextClientVersion != 2 && GLSurfaceView.this.mEGLContextClientVersion != 3)
        return param1ArrayOfint; 
      int i = param1ArrayOfint.length;
      int[] arrayOfInt = new int[i + 2];
      System.arraycopy(param1ArrayOfint, 0, arrayOfInt, 0, i - 1);
      arrayOfInt[i - 1] = 12352;
      if (GLSurfaceView.this.mEGLContextClientVersion == 2) {
        arrayOfInt[i] = 4;
      } else {
        arrayOfInt[i] = 64;
      } 
      arrayOfInt[i + 1] = 12344;
      return arrayOfInt;
    }
    
    abstract EGLConfig chooseConfig(EGL10 param1EGL10, EGLDisplay param1EGLDisplay, EGLConfig[] param1ArrayOfEGLConfig);
  }
  
  class ComponentSizeChooser extends BaseConfigChooser {
    protected int mAlphaSize;
    
    protected int mBlueSize;
    
    protected int mDepthSize;
    
    protected int mGreenSize;
    
    protected int mRedSize;
    
    protected int mStencilSize;
    
    private int[] mValue;
    
    final GLSurfaceView this$0;
    
    public ComponentSizeChooser(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6) {
      super(new int[] { 
            12324, param1Int1, 12323, param1Int2, 12322, param1Int3, 12321, param1Int4, 12325, param1Int5, 
            12326, param1Int6, 12344 });
      this.mValue = new int[1];
      this.mRedSize = param1Int1;
      this.mGreenSize = param1Int2;
      this.mBlueSize = param1Int3;
      this.mAlphaSize = param1Int4;
      this.mDepthSize = param1Int5;
      this.mStencilSize = param1Int6;
    }
    
    public EGLConfig chooseConfig(EGL10 param1EGL10, EGLDisplay param1EGLDisplay, EGLConfig[] param1ArrayOfEGLConfig) {
      int i;
      byte b;
      for (i = param1ArrayOfEGLConfig.length, b = 0; b < i; ) {
        EGLConfig eGLConfig = param1ArrayOfEGLConfig[b];
        int j = findConfigAttrib(param1EGL10, param1EGLDisplay, eGLConfig, 12325, 0);
        int k = findConfigAttrib(param1EGL10, param1EGLDisplay, eGLConfig, 12326, 0);
        if (j >= this.mDepthSize && k >= this.mStencilSize) {
          j = findConfigAttrib(param1EGL10, param1EGLDisplay, eGLConfig, 12324, 0);
          int m = findConfigAttrib(param1EGL10, param1EGLDisplay, eGLConfig, 12323, 0);
          k = findConfigAttrib(param1EGL10, param1EGLDisplay, eGLConfig, 12322, 0);
          int n = findConfigAttrib(param1EGL10, param1EGLDisplay, eGLConfig, 12321, 0);
          if (j == this.mRedSize && m == this.mGreenSize && k == this.mBlueSize && n == this.mAlphaSize)
            return eGLConfig; 
        } 
        b++;
      } 
      return null;
    }
    
    private int findConfigAttrib(EGL10 param1EGL10, EGLDisplay param1EGLDisplay, EGLConfig param1EGLConfig, int param1Int1, int param1Int2) {
      if (param1EGL10.eglGetConfigAttrib(param1EGLDisplay, param1EGLConfig, param1Int1, this.mValue))
        return this.mValue[0]; 
      return param1Int2;
    }
  }
  
  class SimpleEGLConfigChooser extends ComponentSizeChooser {
    final GLSurfaceView this$0;
    
    public SimpleEGLConfigChooser(boolean param1Boolean) {
      super(8, 8, 8, 0, bool, 0);
    }
  }
  
  class EglHelper {
    EGL10 mEgl;
    
    EGLConfig mEglConfig;
    
    EGLContext mEglContext;
    
    EGLDisplay mEglDisplay;
    
    EGLSurface mEglSurface;
    
    private WeakReference<GLSurfaceView> mGLSurfaceViewWeakRef;
    
    public EglHelper(GLSurfaceView this$0) {
      this.mGLSurfaceViewWeakRef = (WeakReference<GLSurfaceView>)this$0;
    }
    
    public void start() {
      EGL10 eGL10 = (EGL10)EGLContext.getEGL();
      EGLDisplay eGLDisplay = eGL10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
      if (eGLDisplay != EGL10.EGL_NO_DISPLAY) {
        int[] arrayOfInt = new int[2];
        if (this.mEgl.eglInitialize(this.mEglDisplay, arrayOfInt)) {
          GLSurfaceView gLSurfaceView = this.mGLSurfaceViewWeakRef.get();
          if (gLSurfaceView == null) {
            this.mEglConfig = null;
            this.mEglContext = null;
          } else {
            this.mEglConfig = gLSurfaceView.mEGLConfigChooser.chooseConfig(this.mEgl, this.mEglDisplay);
            this.mEglContext = gLSurfaceView.mEGLContextFactory.createContext(this.mEgl, this.mEglDisplay, this.mEglConfig);
          } 
          EGLContext eGLContext = this.mEglContext;
          if (eGLContext == null || eGLContext == EGL10.EGL_NO_CONTEXT) {
            this.mEglContext = null;
            throwEglException("createContext");
          } 
          this.mEglSurface = null;
          return;
        } 
        throw new RuntimeException("eglInitialize failed");
      } 
      throw new RuntimeException("eglGetDisplay failed");
    }
    
    public boolean createSurface() {
      if (this.mEgl != null) {
        if (this.mEglDisplay != null) {
          if (this.mEglConfig != null) {
            destroySurfaceImp();
            GLSurfaceView gLSurfaceView = this.mGLSurfaceViewWeakRef.get();
            if (gLSurfaceView != null) {
              GLSurfaceView.EGLWindowSurfaceFactory eGLWindowSurfaceFactory = gLSurfaceView.mEGLWindowSurfaceFactory;
              EGL10 eGL101 = this.mEgl;
              EGLDisplay eGLDisplay1 = this.mEglDisplay;
              EGLConfig eGLConfig = this.mEglConfig;
              SurfaceHolder surfaceHolder = gLSurfaceView.getHolder();
              this.mEglSurface = eGLWindowSurfaceFactory.createWindowSurface(eGL101, eGLDisplay1, eGLConfig, surfaceHolder);
            } else {
              this.mEglSurface = null;
            } 
            EGLSurface eGLSurface2 = this.mEglSurface;
            if (eGLSurface2 == null || eGLSurface2 == EGL10.EGL_NO_SURFACE) {
              int i = this.mEgl.eglGetError();
              if (i == 12299)
                Log.e("EglHelper", "createWindowSurface returned EGL_BAD_NATIVE_WINDOW."); 
              return false;
            } 
            EGL10 eGL10 = this.mEgl;
            EGLDisplay eGLDisplay = this.mEglDisplay;
            EGLSurface eGLSurface1 = this.mEglSurface;
            if (!eGL10.eglMakeCurrent(eGLDisplay, eGLSurface1, eGLSurface1, this.mEglContext)) {
              logEglErrorAsWarning("EGLHelper", "eglMakeCurrent", this.mEgl.eglGetError());
              return false;
            } 
            return true;
          } 
          throw new RuntimeException("mEglConfig not initialized");
        } 
        throw new RuntimeException("eglDisplay not initialized");
      } 
      throw new RuntimeException("egl not initialized");
    }
    
    GL createGL() {
      GL gL1 = this.mEglContext.getGL();
      GLSurfaceView gLSurfaceView = this.mGLSurfaceViewWeakRef.get();
      GL gL2 = gL1;
      if (gLSurfaceView != null) {
        GL gL = gL1;
        if (gLSurfaceView.mGLWrapper != null)
          gL = gLSurfaceView.mGLWrapper.wrap(gL1); 
        gL2 = gL;
        if ((gLSurfaceView.mDebugFlags & 0x3) != 0) {
          GLSurfaceView.LogWriter logWriter;
          int i = 0;
          gL2 = null;
          if ((gLSurfaceView.mDebugFlags & 0x1) != 0)
            i = false | true; 
          if ((gLSurfaceView.mDebugFlags & 0x2) != 0)
            logWriter = new GLSurfaceView.LogWriter(); 
          gL2 = GLDebugHelper.wrap(gL, i, logWriter);
        } 
      } 
      return gL2;
    }
    
    public int swap() {
      if (!this.mEgl.eglSwapBuffers(this.mEglDisplay, this.mEglSurface))
        return this.mEgl.eglGetError(); 
      return 12288;
    }
    
    public void destroySurface() {
      destroySurfaceImp();
    }
    
    private void destroySurfaceImp() {
      EGLSurface eGLSurface = this.mEglSurface;
      if (eGLSurface != null && eGLSurface != EGL10.EGL_NO_SURFACE) {
        this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
        GLSurfaceView gLSurfaceView = this.mGLSurfaceViewWeakRef.get();
        if (gLSurfaceView != null)
          gLSurfaceView.mEGLWindowSurfaceFactory.destroySurface(this.mEgl, this.mEglDisplay, this.mEglSurface); 
        this.mEglSurface = null;
      } 
    }
    
    public void finish() {
      if (this.mEglContext != null) {
        GLSurfaceView gLSurfaceView = this.mGLSurfaceViewWeakRef.get();
        if (gLSurfaceView != null)
          gLSurfaceView.mEGLContextFactory.destroyContext(this.mEgl, this.mEglDisplay, this.mEglContext); 
        this.mEglContext = null;
      } 
      EGLDisplay eGLDisplay = this.mEglDisplay;
      if (eGLDisplay != null) {
        this.mEgl.eglTerminate(eGLDisplay);
        this.mEglDisplay = null;
      } 
    }
    
    private void throwEglException(String param1String) {
      throwEglException(param1String, this.mEgl.eglGetError());
    }
    
    public static void throwEglException(String param1String, int param1Int) {
      param1String = formatEglError(param1String, param1Int);
      throw new RuntimeException(param1String);
    }
    
    public static void logEglErrorAsWarning(String param1String1, String param1String2, int param1Int) {
      Log.w(param1String1, formatEglError(param1String2, param1Int));
    }
    
    public static String formatEglError(String param1String, int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1String);
      stringBuilder.append(" failed: ");
      stringBuilder.append(EGLLogWrapper.getErrorString(param1Int));
      return stringBuilder.toString();
    }
  }
  
  class GLThread extends Thread {
    private GLSurfaceView.EglHelper mEglHelper;
    
    private ArrayList<Runnable> mEventQueue;
    
    private boolean mExited;
    
    private Runnable mFinishDrawingRunnable;
    
    private boolean mFinishedCreatingEglSurface;
    
    private WeakReference<GLSurfaceView> mGLSurfaceViewWeakRef;
    
    private boolean mHasSurface;
    
    private boolean mHaveEglContext;
    
    private boolean mHaveEglSurface;
    
    private int mHeight;
    
    private boolean mPaused;
    
    private boolean mRenderComplete;
    
    private int mRenderMode;
    
    private boolean mRequestPaused;
    
    private boolean mRequestRender;
    
    private boolean mShouldExit;
    
    private boolean mShouldReleaseEglContext;
    
    private boolean mSizeChanged;
    
    private boolean mSurfaceIsBad;
    
    private boolean mWaitingForSurface;
    
    private boolean mWantRenderNotification;
    
    private int mWidth;
    
    GLThread(GLSurfaceView this$0) {
      this.mEventQueue = new ArrayList<>();
      this.mSizeChanged = true;
      this.mFinishDrawingRunnable = null;
      this.mWidth = 0;
      this.mHeight = 0;
      this.mRequestRender = true;
      this.mRenderMode = 1;
      this.mWantRenderNotification = false;
      this.mGLSurfaceViewWeakRef = (WeakReference<GLSurfaceView>)this$0;
    }
    
    public void run() {
      null = new StringBuilder();
      null.append("GLThread ");
      null.append(getId());
      setName(null.toString());
      try {
        guardedRun();
      } catch (InterruptedException interruptedException) {
      
      } finally {
        GLSurfaceView.sGLThreadManager.threadExiting(this);
      } 
    }
    
    private void stopEglSurfaceLocked() {
      if (this.mHaveEglSurface) {
        this.mHaveEglSurface = false;
        this.mEglHelper.destroySurface();
      } 
    }
    
    private void stopEglContextLocked() {
      if (this.mHaveEglContext) {
        this.mEglHelper.finish();
        this.mHaveEglContext = false;
        GLSurfaceView.sGLThreadManager.releaseEglContextLocked(this);
      } 
    }
    
    private void guardedRun() throws InterruptedException {
      this.mEglHelper = new GLSurfaceView.EglHelper(this.mGLSurfaceViewWeakRef);
      this.mHaveEglContext = false;
      this.mHaveEglSurface = false;
      this.mWantRenderNotification = false;
      Exception exception1 = null;
      boolean bool1 = false;
      boolean bool2 = false;
      boolean bool3 = false;
      boolean bool4 = false;
      boolean bool5 = false;
      boolean bool6 = false;
      boolean bool7 = false;
      boolean bool8 = false;
      boolean bool9 = false;
      boolean bool10 = false;
      Object object = null;
      Exception exception2 = null;
      try {
        while (true) {
          GLSurfaceView.GLThreadManager gLThreadManager = GLSurfaceView.sGLThreadManager;
          /* monitor enter ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/opengl/GLSurfaceView}.Landroid/opengl/GLSurfaceView$GLThreadManager;}, name=null} */
          boolean bool11 = bool7;
          bool7 = bool5;
          boolean bool12 = bool4;
          bool4 = bool3;
          bool5 = bool2;
          try {
            while (true) {
              boolean bool;
              return;
            } 
            break;
          } finally {}
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/opengl/GLSurfaceView}.Landroid/opengl/GLSurfaceView$GLThreadManager;}, name=null} */
          throw exception2;
        } 
      } finally {
        exception1 = null;
      } 
    }
    
    public boolean ableToDraw() {
      boolean bool;
      if (this.mHaveEglContext && this.mHaveEglSurface && readyToDraw()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private boolean readyToDraw() {
      boolean bool = this.mPaused;
      boolean bool1 = true;
      if (bool || !this.mHasSurface || this.mSurfaceIsBad || this.mWidth <= 0 || this.mHeight <= 0 || (!this.mRequestRender && this.mRenderMode != 1))
        bool1 = false; 
      return bool1;
    }
    
    public void setRenderMode(int param1Int) {
      if (param1Int >= 0 && param1Int <= 1)
        synchronized (GLSurfaceView.sGLThreadManager) {
          this.mRenderMode = param1Int;
          GLSurfaceView.sGLThreadManager.notifyAll();
          return;
        }  
      throw new IllegalArgumentException("renderMode");
    }
    
    public int getRenderMode() {
      synchronized (GLSurfaceView.sGLThreadManager) {
        return this.mRenderMode;
      } 
    }
    
    public void requestRender() {
      synchronized (GLSurfaceView.sGLThreadManager) {
        this.mRequestRender = true;
        GLSurfaceView.sGLThreadManager.notifyAll();
        return;
      } 
    }
    
    public void requestRenderAndNotify(Runnable param1Runnable) {
      synchronized (GLSurfaceView.sGLThreadManager) {
        if (Thread.currentThread() == this)
          return; 
        if (this.mFinishDrawingRunnable != null) {
          Runnable runnable = this.mFinishDrawingRunnable;
          if (runnable != param1Runnable)
            try {
              runnable = this.mFinishDrawingRunnable;
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("requestRenderAndNotify ==> run previous finish runnable mFinishDrawingRunnable =:");
              stringBuilder.append(this.mFinishDrawingRunnable);
              stringBuilder.append(",tmpRunnable =:");
              stringBuilder.append(runnable);
              Log.i("GLSurfaceView", stringBuilder.toString());
              if (runnable != null)
                runnable.run(); 
            } catch (Exception exception) {
              exception.printStackTrace();
            }  
        } 
        this.mWantRenderNotification = true;
        this.mRequestRender = true;
        this.mRenderComplete = false;
        this.mFinishDrawingRunnable = param1Runnable;
        GLSurfaceView.sGLThreadManager.notifyAll();
        return;
      } 
    }
    
    public void surfaceCreated() {
      synchronized (GLSurfaceView.sGLThreadManager) {
        this.mHasSurface = true;
        this.mFinishedCreatingEglSurface = false;
        GLSurfaceView.sGLThreadManager.notifyAll();
        while (this.mWaitingForSurface && !this.mFinishedCreatingEglSurface) {
          boolean bool = this.mExited;
          if (!bool)
            try {
              GLSurfaceView.sGLThreadManager.wait();
            } catch (InterruptedException interruptedException) {
              Thread.currentThread().interrupt();
            }  
        } 
        return;
      } 
    }
    
    public void surfaceDestroyed() {
      synchronized (GLSurfaceView.sGLThreadManager) {
        this.mHasSurface = false;
        GLSurfaceView.sGLThreadManager.notifyAll();
        while (!this.mWaitingForSurface) {
          boolean bool = this.mExited;
          if (!bool)
            try {
              GLSurfaceView.sGLThreadManager.wait();
            } catch (InterruptedException interruptedException) {
              Thread.currentThread().interrupt();
            }  
        } 
        return;
      } 
    }
    
    public void onPause() {
      synchronized (GLSurfaceView.sGLThreadManager) {
        this.mRequestPaused = true;
        GLSurfaceView.sGLThreadManager.notifyAll();
        while (!this.mExited) {
          boolean bool = this.mPaused;
          if (!bool)
            try {
              GLSurfaceView.sGLThreadManager.wait();
            } catch (InterruptedException interruptedException) {
              Thread.currentThread().interrupt();
            }  
        } 
        return;
      } 
    }
    
    public void onResume() {
      synchronized (GLSurfaceView.sGLThreadManager) {
        this.mRequestPaused = false;
        this.mRequestRender = true;
        this.mRenderComplete = false;
        GLSurfaceView.sGLThreadManager.notifyAll();
        while (!this.mExited && this.mPaused) {
          boolean bool = this.mRenderComplete;
          if (!bool)
            try {
              GLSurfaceView.sGLThreadManager.wait();
            } catch (InterruptedException interruptedException) {
              Thread.currentThread().interrupt();
            }  
        } 
        return;
      } 
    }
    
    public void onWindowResize(int param1Int1, int param1Int2) {
      synchronized (GLSurfaceView.sGLThreadManager) {
        this.mWidth = param1Int1;
        this.mHeight = param1Int2;
        this.mSizeChanged = true;
        this.mRequestRender = true;
        this.mRenderComplete = false;
        if (Thread.currentThread() == this)
          return; 
        GLSurfaceView.sGLThreadManager.notifyAll();
        while (!this.mExited && !this.mPaused && !this.mRenderComplete) {
          boolean bool = ableToDraw();
          if (bool)
            try {
              GLSurfaceView.sGLThreadManager.wait();
            } catch (InterruptedException interruptedException) {
              Thread.currentThread().interrupt();
            }  
        } 
        return;
      } 
    }
    
    public void requestExitAndWait() {
      synchronized (GLSurfaceView.sGLThreadManager) {
        this.mShouldExit = true;
        GLSurfaceView.sGLThreadManager.notifyAll();
        while (true) {
          boolean bool = this.mExited;
          if (!bool) {
            try {
              GLSurfaceView.sGLThreadManager.wait();
            } catch (InterruptedException interruptedException) {
              Thread.currentThread().interrupt();
            } 
            continue;
          } 
          break;
        } 
        return;
      } 
    }
    
    public void requestReleaseEglContextLocked() {
      this.mShouldReleaseEglContext = true;
      GLSurfaceView.sGLThreadManager.notifyAll();
    }
    
    public void queueEvent(Runnable param1Runnable) {
      if (param1Runnable != null)
        synchronized (GLSurfaceView.sGLThreadManager) {
          this.mEventQueue.add(param1Runnable);
          GLSurfaceView.sGLThreadManager.notifyAll();
          return;
        }  
      throw new IllegalArgumentException("r must not be null");
    }
  }
  
  class LogWriter extends Writer {
    public void close() {
      flushBuilder();
    }
    
    public void flush() {
      flushBuilder();
    }
    
    public void write(char[] param1ArrayOfchar, int param1Int1, int param1Int2) {
      for (byte b = 0; b < param1Int2; b++) {
        char c = param1ArrayOfchar[param1Int1 + b];
        if (c == '\n') {
          flushBuilder();
        } else {
          this.mBuilder.append(c);
        } 
      } 
    }
    
    private void flushBuilder() {
      if (this.mBuilder.length() > 0) {
        Log.v("GLSurfaceView", this.mBuilder.toString());
        StringBuilder stringBuilder = this.mBuilder;
        stringBuilder.delete(0, stringBuilder.length());
      } 
    }
    
    private StringBuilder mBuilder = new StringBuilder();
  }
  
  private void checkRenderThreadState() {
    if (this.mGLThread == null)
      return; 
    throw new IllegalStateException("setRenderer has already been called for this instance.");
  }
  
  class GLThreadManager {
    private GLThreadManager() {}
    
    private static String TAG = "GLThreadManager";
    
    public void threadExiting(GLSurfaceView.GLThread param1GLThread) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_1
      //   3: iconst_1
      //   4: invokestatic access$1102 : (Landroid/opengl/GLSurfaceView$GLThread;Z)Z
      //   7: pop
      //   8: aload_0
      //   9: invokevirtual notifyAll : ()V
      //   12: aload_0
      //   13: monitorexit
      //   14: return
      //   15: astore_1
      //   16: aload_0
      //   17: monitorexit
      //   18: aload_1
      //   19: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1942	-> 2
      //   #1943	-> 8
      //   #1944	-> 12
      //   #1941	-> 15
      // Exception table:
      //   from	to	target	type
      //   2	8	15	finally
      //   8	12	15	finally
    }
    
    public void releaseEglContextLocked(GLSurfaceView.GLThread param1GLThread) {
      notifyAll();
    }
  }
  
  private static final GLThreadManager sGLThreadManager = new GLThreadManager();
  
  private int mDebugFlags;
  
  private boolean mDetached;
  
  private EGLConfigChooser mEGLConfigChooser;
  
  private int mEGLContextClientVersion;
  
  private EGLContextFactory mEGLContextFactory;
  
  private EGLWindowSurfaceFactory mEGLWindowSurfaceFactory;
  
  private GLThread mGLThread;
  
  private GLWrapper mGLWrapper;
  
  private boolean mPreserveEGLContextOnPause;
  
  private Renderer mRenderer;
  
  private final WeakReference<GLSurfaceView> mThisWeakRef;
  
  class EGLConfigChooser {
    public abstract EGLConfig chooseConfig(EGL10 param1EGL10, EGLDisplay param1EGLDisplay);
  }
  
  class EGLContextFactory {
    public abstract EGLContext createContext(EGL10 param1EGL10, EGLDisplay param1EGLDisplay, EGLConfig param1EGLConfig);
    
    public abstract void destroyContext(EGL10 param1EGL10, EGLDisplay param1EGLDisplay, EGLContext param1EGLContext);
  }
  
  class EGLWindowSurfaceFactory {
    public abstract EGLSurface createWindowSurface(EGL10 param1EGL10, EGLDisplay param1EGLDisplay, EGLConfig param1EGLConfig, Object param1Object);
    
    public abstract void destroySurface(EGL10 param1EGL10, EGLDisplay param1EGLDisplay, EGLSurface param1EGLSurface);
  }
  
  class GLWrapper {
    public abstract GL wrap(GL param1GL);
  }
  
  class Renderer {
    public abstract void onDrawFrame(GL10 param1GL10);
    
    public abstract void onSurfaceChanged(GL10 param1GL10, int param1Int1, int param1Int2);
    
    public abstract void onSurfaceCreated(GL10 param1GL10, EGLConfig param1EGLConfig);
  }
}
