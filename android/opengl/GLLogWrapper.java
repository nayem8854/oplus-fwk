package android.opengl;

import java.io.IOException;
import java.io.Writer;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;
import javax.microedition.khronos.opengles.GL;

class GLLogWrapper extends GLWrapperBase {
  private static final int FORMAT_FIXED = 2;
  
  private static final int FORMAT_FLOAT = 1;
  
  private static final int FORMAT_INT = 0;
  
  private int mArgCount;
  
  boolean mColorArrayEnabled;
  
  private PointerInfo mColorPointer;
  
  private Writer mLog;
  
  private boolean mLogArgumentNames;
  
  boolean mNormalArrayEnabled;
  
  private PointerInfo mNormalPointer;
  
  StringBuilder mStringBuilder;
  
  private PointerInfo mTexCoordPointer;
  
  boolean mTextureCoordArrayEnabled;
  
  boolean mVertexArrayEnabled;
  
  private PointerInfo mVertexPointer;
  
  public GLLogWrapper(GL paramGL, Writer paramWriter, boolean paramBoolean) {
    super(paramGL);
    this.mColorPointer = new PointerInfo();
    this.mNormalPointer = new PointerInfo();
    this.mTexCoordPointer = new PointerInfo();
    this.mVertexPointer = new PointerInfo();
    this.mLog = paramWriter;
    this.mLogArgumentNames = paramBoolean;
  }
  
  private void checkError() {
    int i = this.mgl.glGetError();
    if (i != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("glError: ");
      stringBuilder.append(Integer.toString(i));
      String str = stringBuilder.toString();
      logLine(str);
    } 
  }
  
  private void logLine(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append('\n');
    log(stringBuilder.toString());
  }
  
  private void log(String paramString) {
    try {
      this.mLog.write(paramString);
    } catch (IOException iOException) {}
  }
  
  private void begin(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append('(');
    log(stringBuilder.toString());
    this.mArgCount = 0;
  }
  
  private void arg(String paramString1, String paramString2) {
    int i = this.mArgCount;
    this.mArgCount = i + 1;
    if (i > 0)
      log(", "); 
    if (this.mLogArgumentNames) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("=");
      log(stringBuilder.toString());
    } 
    log(paramString2);
  }
  
  private void end() {
    log(");\n");
    flush();
  }
  
  private void flush() {
    try {
      this.mLog.flush();
    } catch (IOException iOException) {
      this.mLog = null;
    } 
  }
  
  private void arg(String paramString, boolean paramBoolean) {
    arg(paramString, Boolean.toString(paramBoolean));
  }
  
  private void arg(String paramString, int paramInt) {
    arg(paramString, Integer.toString(paramInt));
  }
  
  private void arg(String paramString, float paramFloat) {
    arg(paramString, Float.toString(paramFloat));
  }
  
  private void returns(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(") returns ");
    stringBuilder.append(paramString);
    stringBuilder.append(";\n");
    log(stringBuilder.toString());
    flush();
  }
  
  private void returns(int paramInt) {
    returns(Integer.toString(paramInt));
  }
  
  private void arg(String paramString, int paramInt1, int[] paramArrayOfint, int paramInt2) {
    arg(paramString, toString(paramInt1, 0, paramArrayOfint, paramInt2));
  }
  
  private void arg(String paramString, int paramInt1, short[] paramArrayOfshort, int paramInt2) {
    arg(paramString, toString(paramInt1, paramArrayOfshort, paramInt2));
  }
  
  private void arg(String paramString, int paramInt1, float[] paramArrayOffloat, int paramInt2) {
    arg(paramString, toString(paramInt1, paramArrayOffloat, paramInt2));
  }
  
  private void formattedAppend(StringBuilder paramStringBuilder, int paramInt1, int paramInt2) {
    if (paramInt2 != 0) {
      if (paramInt2 != 1) {
        if (paramInt2 == 2)
          paramStringBuilder.append(paramInt1 / 65536.0F); 
      } else {
        paramStringBuilder.append(Float.intBitsToFloat(paramInt1));
      } 
    } else {
      paramStringBuilder.append(paramInt1);
    } 
  }
  
  private String toString(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{\n");
    int i = paramArrayOfint.length;
    for (byte b = 0; b < paramInt1; b++) {
      int j = paramInt3 + b;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(" [");
      stringBuilder1.append(j);
      stringBuilder1.append("] = ");
      stringBuilder.append(stringBuilder1.toString());
      if (j < 0 || j >= i) {
        stringBuilder.append("out of bounds");
      } else {
        formattedAppend(stringBuilder, paramArrayOfint[j], paramInt2);
      } 
      stringBuilder.append('\n');
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private String toString(int paramInt1, short[] paramArrayOfshort, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{\n");
    int i = paramArrayOfshort.length;
    for (byte b = 0; b < paramInt1; b++) {
      int j = paramInt2 + b;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(" [");
      stringBuilder1.append(j);
      stringBuilder1.append("] = ");
      stringBuilder.append(stringBuilder1.toString());
      if (j < 0 || j >= i) {
        stringBuilder.append("out of bounds");
      } else {
        stringBuilder.append(paramArrayOfshort[j]);
      } 
      stringBuilder.append('\n');
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private String toString(int paramInt1, float[] paramArrayOffloat, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{\n");
    int i = paramArrayOffloat.length;
    for (byte b = 0; b < paramInt1; b++) {
      int j = paramInt2 + b;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("[");
      stringBuilder1.append(j);
      stringBuilder1.append("] = ");
      stringBuilder.append(stringBuilder1.toString());
      if (j < 0 || j >= i) {
        stringBuilder.append("out of bounds");
      } else {
        stringBuilder.append(paramArrayOffloat[j]);
      } 
      stringBuilder.append('\n');
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private String toString(int paramInt, FloatBuffer paramFloatBuffer) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{\n");
    for (byte b = 0; b < paramInt; b++) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(" [");
      stringBuilder1.append(b);
      stringBuilder1.append("] = ");
      stringBuilder1.append(paramFloatBuffer.get(b));
      stringBuilder1.append('\n');
      stringBuilder.append(stringBuilder1.toString());
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private String toString(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{\n");
    for (byte b = 0; b < paramInt1; b++) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(" [");
      stringBuilder1.append(b);
      stringBuilder1.append("] = ");
      stringBuilder.append(stringBuilder1.toString());
      formattedAppend(stringBuilder, paramIntBuffer.get(b), paramInt2);
      stringBuilder.append('\n');
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private String toString(int paramInt, ShortBuffer paramShortBuffer) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{\n");
    for (byte b = 0; b < paramInt; b++) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(" [");
      stringBuilder1.append(b);
      stringBuilder1.append("] = ");
      stringBuilder1.append(paramShortBuffer.get(b));
      stringBuilder1.append('\n');
      stringBuilder.append(stringBuilder1.toString());
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private void arg(String paramString, int paramInt, FloatBuffer paramFloatBuffer) {
    arg(paramString, toString(paramInt, paramFloatBuffer));
  }
  
  private void arg(String paramString, int paramInt, IntBuffer paramIntBuffer) {
    arg(paramString, toString(paramInt, 0, paramIntBuffer));
  }
  
  private void arg(String paramString, int paramInt, ShortBuffer paramShortBuffer) {
    arg(paramString, toString(paramInt, paramShortBuffer));
  }
  
  private void argPointer(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer) {
    arg("size", paramInt1);
    arg("type", getPointerTypeName(paramInt2));
    arg("stride", paramInt3);
    arg("pointer", paramBuffer.toString());
  }
  
  private static String getHex(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static String getErrorString(int paramInt) {
    if (paramInt != 0) {
      switch (paramInt) {
        default:
          return getHex(paramInt);
        case 1285:
          return "GL_OUT_OF_MEMORY";
        case 1284:
          return "GL_STACK_UNDERFLOW";
        case 1283:
          return "GL_STACK_OVERFLOW";
        case 1282:
          return "GL_INVALID_OPERATION";
        case 1281:
          return "GL_INVALID_VALUE";
        case 1280:
          break;
      } 
      return "GL_INVALID_ENUM";
    } 
    return "GL_NO_ERROR";
  }
  
  private String getClearBufferMask(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    int i = paramInt;
    if ((paramInt & 0x100) != 0) {
      stringBuilder.append("GL_DEPTH_BUFFER_BIT");
      i = paramInt & 0xFFFFFEFF;
    } 
    paramInt = i;
    if ((i & 0x400) != 0) {
      if (stringBuilder.length() > 0)
        stringBuilder.append(" | "); 
      stringBuilder.append("GL_STENCIL_BUFFER_BIT");
      paramInt = i & 0xFFFFFBFF;
    } 
    i = paramInt;
    if ((paramInt & 0x4000) != 0) {
      if (stringBuilder.length() > 0)
        stringBuilder.append(" | "); 
      stringBuilder.append("GL_COLOR_BUFFER_BIT");
      i = paramInt & 0xFFFFBFFF;
    } 
    if (i != 0) {
      if (stringBuilder.length() > 0)
        stringBuilder.append(" | "); 
      stringBuilder.append(getHex(i));
    } 
    return stringBuilder.toString();
  }
  
  private String getFactor(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        switch (paramInt) {
          default:
            return getHex(paramInt);
          case 776:
            return "GL_SRC_ALPHA_SATURATE";
          case 775:
            return "GL_ONE_MINUS_DST_COLOR";
          case 774:
            return "GL_DST_COLOR";
          case 773:
            return "GL_ONE_MINUS_DST_ALPHA";
          case 772:
            return "GL_DST_ALPHA";
          case 771:
            return "GL_ONE_MINUS_SRC_ALPHA";
          case 770:
            return "GL_SRC_ALPHA";
          case 769:
            return "GL_ONE_MINUS_SRC_COLOR";
          case 768:
            break;
        } 
        return "GL_SRC_COLOR";
      } 
      return "GL_ONE";
    } 
    return "GL_ZERO";
  }
  
  private String getShadeModel(int paramInt) {
    if (paramInt != 7424) {
      if (paramInt != 7425)
        return getHex(paramInt); 
      return "GL_SMOOTH";
    } 
    return "GL_FLAT";
  }
  
  private String getTextureTarget(int paramInt) {
    if (paramInt != 3553)
      return getHex(paramInt); 
    return "GL_TEXTURE_2D";
  }
  
  private String getTextureEnvTarget(int paramInt) {
    if (paramInt != 8960)
      return getHex(paramInt); 
    return "GL_TEXTURE_ENV";
  }
  
  private String getTextureEnvPName(int paramInt) {
    if (paramInt != 8704) {
      if (paramInt != 8705)
        return getHex(paramInt); 
      return "GL_TEXTURE_ENV_COLOR";
    } 
    return "GL_TEXTURE_ENV_MODE";
  }
  
  private int getTextureEnvParamCount(int paramInt) {
    if (paramInt != 8704) {
      if (paramInt != 8705)
        return 0; 
      return 4;
    } 
    return 1;
  }
  
  private String getTextureEnvParamName(float paramFloat) {
    int i = (int)paramFloat;
    if (paramFloat == i) {
      if (i != 260) {
        if (i != 3042) {
          if (i != 7681) {
            if (i != 34160) {
              if (i != 8448) {
                if (i != 8449)
                  return getHex(i); 
                return "GL_DECAL";
              } 
              return "GL_MODULATE";
            } 
            return "GL_COMBINE";
          } 
          return "GL_REPLACE";
        } 
        return "GL_BLEND";
      } 
      return "GL_ADD";
    } 
    return Float.toString(paramFloat);
  }
  
  private String getMatrixMode(int paramInt) {
    switch (paramInt) {
      default:
        return getHex(paramInt);
      case 5890:
        return "GL_TEXTURE";
      case 5889:
        return "GL_PROJECTION";
      case 5888:
        break;
    } 
    return "GL_MODELVIEW";
  }
  
  private String getClientState(int paramInt) {
    switch (paramInt) {
      default:
        return getHex(paramInt);
      case 32888:
        return "GL_TEXTURE_COORD_ARRAY";
      case 32886:
        return "GL_COLOR_ARRAY";
      case 32885:
        return "GL_NORMAL_ARRAY";
      case 32884:
        break;
    } 
    return "GL_VERTEX_ARRAY";
  }
  
  private String getCap(int paramInt) {
    switch (paramInt) {
      default:
        switch (paramInt) {
          default:
            switch (paramInt) {
              default:
                switch (paramInt) {
                  default:
                    return getHex(paramInt);
                  case 32928:
                    return "GL_SAMPLE_COVERAGE";
                  case 32927:
                    return "GL_SAMPLE_ALPHA_TO_ONE";
                  case 32926:
                    return "GL_SAMPLE_ALPHA_TO_COVERAGE";
                  case 32925:
                    break;
                } 
                return "GL_MULTISAMPLE";
              case 32886:
                return "GL_COLOR_ARRAY";
              case 32885:
                return "GL_NORMAL_ARRAY";
              case 32884:
                break;
            } 
            return "GL_VERTEX_ARRAY";
          case 16391:
            return "GL_LIGHT7";
          case 16390:
            return "GL_LIGHT6";
          case 16389:
            return "GL_LIGHT5";
          case 16388:
            return "GL_LIGHT4";
          case 16387:
            return "GL_LIGHT3";
          case 16386:
            return "GL_LIGHT2";
          case 16385:
            return "GL_LIGHT1";
          case 16384:
            break;
        } 
        return "GL_LIGHT0";
      case 32888:
        return "GL_TEXTURE_COORD_ARRAY";
      case 32826:
        return "GL_RESCALE_NORMAL";
      case 3553:
        return "GL_TEXTURE_2D";
      case 3089:
        return "GL_SCISSOR_TEST";
      case 3058:
        return "GL_COLOR_LOGIC_OP";
      case 3042:
        return "GL_BLEND";
      case 3024:
        return "GL_DITHER";
      case 3008:
        return "GL_ALPHA_TEST";
      case 2977:
        return "GL_NORMALIZE";
      case 2960:
        return "GL_STENCIL_TEST";
      case 2929:
        return "GL_DEPTH_TEST";
      case 2912:
        return "GL_FOG";
      case 2903:
        return "GL_COLOR_MATERIAL";
      case 2896:
        return "GL_LIGHTING";
      case 2884:
        return "GL_CULL_FACE";
      case 2848:
        return "GL_LINE_SMOOTH";
      case 2832:
        break;
    } 
    return "GL_POINT_SMOOTH";
  }
  
  private String getTexturePName(int paramInt) {
    if (paramInt != 33169) {
      if (paramInt != 35741) {
        switch (paramInt) {
          default:
            return getHex(paramInt);
          case 10243:
            return "GL_TEXTURE_WRAP_T";
          case 10242:
            return "GL_TEXTURE_WRAP_S";
          case 10241:
            return "GL_TEXTURE_MIN_FILTER";
          case 10240:
            break;
        } 
        return "GL_TEXTURE_MAG_FILTER";
      } 
      return "GL_TEXTURE_CROP_RECT_OES";
    } 
    return "GL_GENERATE_MIPMAP";
  }
  
  private String getTextureParamName(float paramFloat) {
    int i = (int)paramFloat;
    if (paramFloat == i) {
      if (i != 9728) {
        if (i != 9729) {
          if (i != 10497) {
            if (i != 33071) {
              switch (i) {
                default:
                  return getHex(i);
                case 9987:
                  return "GL_LINEAR_MIPMAP_LINEAR";
                case 9986:
                  return "GL_NEAREST_MIPMAP_LINEAR";
                case 9985:
                  return "GL_LINEAR_MIPMAP_NEAREST";
                case 9984:
                  break;
              } 
              return "GL_NEAREST_MIPMAP_NEAREST";
            } 
            return "GL_CLAMP_TO_EDGE";
          } 
          return "GL_REPEAT";
        } 
        return "GL_LINEAR";
      } 
      return "GL_NEAREST";
    } 
    return Float.toString(paramFloat);
  }
  
  private String getFogPName(int paramInt) {
    switch (paramInt) {
      default:
        return getHex(paramInt);
      case 2918:
        return "GL_FOG_COLOR";
      case 2917:
        return "GL_FOG_MODE";
      case 2916:
        return "GL_FOG_END";
      case 2915:
        return "GL_FOG_START";
      case 2914:
        break;
    } 
    return "GL_FOG_DENSITY";
  }
  
  private int getFogParamCount(int paramInt) {
    switch (paramInt) {
      default:
        return 0;
      case 2918:
        return 4;
      case 2917:
        return 1;
      case 2916:
        return 1;
      case 2915:
        return 1;
      case 2914:
        break;
    } 
    return 1;
  }
  
  private String getBeginMode(int paramInt) {
    switch (paramInt) {
      default:
        return getHex(paramInt);
      case 6:
        return "GL_TRIANGLE_FAN";
      case 5:
        return "GL_TRIANGLE_STRIP";
      case 4:
        return "GL_TRIANGLES";
      case 3:
        return "GL_LINE_STRIP";
      case 2:
        return "GL_LINE_LOOP";
      case 1:
        return "GL_LINES";
      case 0:
        break;
    } 
    return "GL_POINTS";
  }
  
  private String getIndexType(int paramInt) {
    if (paramInt != 5121) {
      if (paramInt != 5123)
        return getHex(paramInt); 
      return "GL_UNSIGNED_SHORT";
    } 
    return "GL_UNSIGNED_BYTE";
  }
  
  private String getIntegerStateName(int paramInt) {
    switch (paramInt) {
      default:
        return getHex(paramInt);
      case 35215:
        return "GL_TEXTURE_MATRIX_FLOAT_AS_INT_BITS_OES";
      case 35214:
        return "GL_PROJECTION_MATRIX_FLOAT_AS_INT_BITS_OES";
      case 35213:
        return "GL_MODELVIEW_MATRIX_FLOAT_AS_INT_BITS_OES";
      case 34467:
        return "GL_COMPRESSED_TEXTURE_FORMATS";
      case 34466:
        return "GL_NUM_COMPRESSED_TEXTURE_FORMATS";
      case 34018:
        return "GL_MAX_TEXTURE_UNITS";
      case 33902:
        return "GL_ALIASED_LINE_WIDTH_RANGE";
      case 33901:
        return "GL_ALIASED_POINT_SIZE_RANGE";
      case 33001:
        return "GL_MAX_ELEMENTS_INDICES";
      case 33000:
        return "GL_MAX_ELEMENTS_VERTICES";
      case 3415:
        return "GL_STENCIL_BITS";
      case 3414:
        return "GL_DEPTH_BITS";
      case 3413:
        return "GL_ALPHA_BITS";
      case 3412:
        return "GL_BLUE_BITS";
      case 3411:
        return "GL_GREEN_BITS";
      case 3410:
        return "GL_RED_BITS";
      case 3408:
        return "GL_SUBPIXEL_BITS";
      case 3386:
        return "GL_MAX_VIEWPORT_DIMS";
      case 3385:
        return "GL_MAX_TEXTURE_STACK_DEPTH";
      case 3384:
        return "GL_MAX_PROJECTION_STACK_DEPTH";
      case 3382:
        return "GL_MAX_MODELVIEW_STACK_DEPTH";
      case 3379:
        return "GL_MAX_TEXTURE_SIZE";
      case 3377:
        return "GL_MAX_LIGHTS";
      case 2850:
        return "GL_SMOOTH_LINE_WIDTH_RANGE";
      case 2834:
        break;
    } 
    return "GL_SMOOTH_POINT_SIZE_RANGE";
  }
  
  private int getIntegerStateSize(int paramInt) {
    int[] arrayOfInt;
    switch (paramInt) {
      default:
        return 0;
      case 35213:
      case 35214:
      case 35215:
        return 16;
      case 34467:
        arrayOfInt = new int[1];
        this.mgl.glGetIntegerv(34466, arrayOfInt, 0);
        return arrayOfInt[0];
      case 34466:
        return 1;
      case 34018:
        return 1;
      case 33902:
        return 2;
      case 33901:
        return 2;
      case 33001:
        return 1;
      case 33000:
        return 1;
      case 3415:
        return 1;
      case 3414:
        return 1;
      case 3413:
        return 1;
      case 3412:
        return 1;
      case 3411:
        return 1;
      case 3410:
        return 1;
      case 3408:
        return 1;
      case 3386:
        return 2;
      case 3385:
        return 1;
      case 3384:
        return 1;
      case 3382:
        return 1;
      case 3379:
        return 1;
      case 3377:
        return 1;
      case 2850:
        return 2;
      case 2834:
        break;
    } 
    return 2;
  }
  
  private int getIntegerStateFormat(int paramInt) {
    switch (paramInt) {
      default:
        return 0;
      case 35213:
      case 35214:
      case 35215:
        break;
    } 
    return 1;
  }
  
  private String getHintTarget(int paramInt) {
    if (paramInt != 33170) {
      switch (paramInt) {
        default:
          return getHex(paramInt);
        case 3156:
          return "GL_FOG_HINT";
        case 3155:
          return "GL_POLYGON_SMOOTH_HINT";
        case 3154:
          return "GL_LINE_SMOOTH_HINT";
        case 3153:
          return "GL_POINT_SMOOTH_HINT";
        case 3152:
          break;
      } 
      return "GL_PERSPECTIVE_CORRECTION_HINT";
    } 
    return "GL_GENERATE_MIPMAP_HINT";
  }
  
  private String getHintMode(int paramInt) {
    switch (paramInt) {
      default:
        return getHex(paramInt);
      case 4354:
        return "GL_NICEST";
      case 4353:
        return "GL_FASTEST";
      case 4352:
        break;
    } 
    return "GL_DONT_CARE";
  }
  
  private String getFaceName(int paramInt) {
    if (paramInt != 1032)
      return getHex(paramInt); 
    return "GL_FRONT_AND_BACK";
  }
  
  private String getMaterialPName(int paramInt) {
    switch (paramInt) {
      default:
        switch (paramInt) {
          default:
            return getHex(paramInt);
          case 5634:
            return "GL_AMBIENT_AND_DIFFUSE";
          case 5633:
            return "GL_SHININESS";
          case 5632:
            break;
        } 
        return "GL_EMISSION";
      case 4610:
        return "GL_SPECULAR";
      case 4609:
        return "GL_DIFFUSE";
      case 4608:
        break;
    } 
    return "GL_AMBIENT";
  }
  
  private int getMaterialParamCount(int paramInt) {
    switch (paramInt) {
      default:
        switch (paramInt) {
          default:
            return 0;
          case 5634:
            return 4;
          case 5633:
            return 1;
          case 5632:
            break;
        } 
        return 4;
      case 4610:
        return 4;
      case 4609:
        return 4;
      case 4608:
        break;
    } 
    return 4;
  }
  
  private String getLightName(int paramInt) {
    if (paramInt >= 16384 && paramInt <= 16391) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("GL_LIGHT");
      stringBuilder.append(Integer.toString(paramInt));
      return stringBuilder.toString();
    } 
    return getHex(paramInt);
  }
  
  private String getLightPName(int paramInt) {
    switch (paramInt) {
      default:
        return getHex(paramInt);
      case 4617:
        return "GL_QUADRATIC_ATTENUATION";
      case 4616:
        return "GL_LINEAR_ATTENUATION";
      case 4615:
        return "GL_CONSTANT_ATTENUATION";
      case 4614:
        return "GL_SPOT_CUTOFF";
      case 4613:
        return "GL_SPOT_EXPONENT";
      case 4612:
        return "GL_SPOT_DIRECTION";
      case 4611:
        return "GL_POSITION";
      case 4610:
        return "GL_SPECULAR";
      case 4609:
        return "GL_DIFFUSE";
      case 4608:
        break;
    } 
    return "GL_AMBIENT";
  }
  
  private int getLightParamCount(int paramInt) {
    switch (paramInt) {
      default:
        return 0;
      case 4617:
        return 1;
      case 4616:
        return 1;
      case 4615:
        return 1;
      case 4614:
        return 1;
      case 4613:
        return 1;
      case 4612:
        return 3;
      case 4611:
        return 4;
      case 4610:
        return 4;
      case 4609:
        return 4;
      case 4608:
        break;
    } 
    return 4;
  }
  
  private String getLightModelPName(int paramInt) {
    if (paramInt != 2898) {
      if (paramInt != 2899)
        return getHex(paramInt); 
      return "GL_LIGHT_MODEL_AMBIENT";
    } 
    return "GL_LIGHT_MODEL_TWO_SIDE";
  }
  
  private int getLightModelParamCount(int paramInt) {
    if (paramInt != 2898) {
      if (paramInt != 2899)
        return 0; 
      return 4;
    } 
    return 1;
  }
  
  private String getPointerTypeName(int paramInt) {
    if (paramInt != 5126) {
      if (paramInt != 5132) {
        switch (paramInt) {
          default:
            return getHex(paramInt);
          case 5122:
            return "GL_SHORT";
          case 5121:
            return "GL_UNSIGNED_BYTE";
          case 5120:
            break;
        } 
        return "GL_BYTE";
      } 
      return "GL_FIXED";
    } 
    return "GL_FLOAT";
  }
  
  private ByteBuffer toByteBuffer(int paramInt, Buffer paramBuffer) {
    byte b;
    if (paramInt < 0) {
      b = 1;
    } else {
      b = 0;
    } 
    if (paramBuffer instanceof ByteBuffer) {
      ByteBuffer byteBuffer = (ByteBuffer)paramBuffer;
      int i = byteBuffer.position();
      if (b)
        paramInt = byteBuffer.limit() - i; 
      paramBuffer = ByteBuffer.allocate(paramInt).order(byteBuffer.order());
      for (b = 0; b < paramInt; b++)
        paramBuffer.put(byteBuffer.get()); 
      byteBuffer.position(i);
    } else if (paramBuffer instanceof CharBuffer) {
      CharBuffer charBuffer1 = (CharBuffer)paramBuffer;
      int i = charBuffer1.position();
      if (b != 0)
        paramInt = (charBuffer1.limit() - i) * 2; 
      paramBuffer = ByteBuffer.allocate(paramInt).order(charBuffer1.order());
      CharBuffer charBuffer2 = paramBuffer.asCharBuffer();
      for (b = 0; b < paramInt / 2; b++)
        charBuffer2.put(charBuffer1.get()); 
      charBuffer1.position(i);
    } else if (paramBuffer instanceof ShortBuffer) {
      ShortBuffer shortBuffer1 = (ShortBuffer)paramBuffer;
      int i = shortBuffer1.position();
      if (b != 0)
        paramInt = (shortBuffer1.limit() - i) * 2; 
      paramBuffer = ByteBuffer.allocate(paramInt).order(shortBuffer1.order());
      ShortBuffer shortBuffer2 = paramBuffer.asShortBuffer();
      for (b = 0; b < paramInt / 2; b++)
        shortBuffer2.put(shortBuffer1.get()); 
      shortBuffer1.position(i);
    } else if (paramBuffer instanceof IntBuffer) {
      IntBuffer intBuffer2 = (IntBuffer)paramBuffer;
      int i = intBuffer2.position();
      if (b != 0)
        paramInt = (intBuffer2.limit() - i) * 4; 
      paramBuffer = ByteBuffer.allocate(paramInt).order(intBuffer2.order());
      IntBuffer intBuffer1 = paramBuffer.asIntBuffer();
      for (b = 0; b < paramInt / 4; b++)
        intBuffer1.put(intBuffer2.get()); 
      intBuffer2.position(i);
    } else if (paramBuffer instanceof FloatBuffer) {
      FloatBuffer floatBuffer2 = (FloatBuffer)paramBuffer;
      int i = floatBuffer2.position();
      if (b != 0)
        paramInt = (floatBuffer2.limit() - i) * 4; 
      paramBuffer = ByteBuffer.allocate(paramInt).order(floatBuffer2.order());
      FloatBuffer floatBuffer1 = paramBuffer.asFloatBuffer();
      for (b = 0; b < paramInt / 4; b++)
        floatBuffer1.put(floatBuffer2.get()); 
      floatBuffer2.position(i);
    } else if (paramBuffer instanceof DoubleBuffer) {
      DoubleBuffer doubleBuffer1 = (DoubleBuffer)paramBuffer;
      int i = doubleBuffer1.position();
      if (b != 0)
        paramInt = (doubleBuffer1.limit() - i) * 8; 
      paramBuffer = ByteBuffer.allocate(paramInt).order(doubleBuffer1.order());
      DoubleBuffer doubleBuffer2 = paramBuffer.asDoubleBuffer();
      for (b = 0; b < paramInt / 8; b++)
        doubleBuffer2.put(doubleBuffer1.get()); 
      doubleBuffer1.position(i);
    } else {
      if (paramBuffer instanceof LongBuffer) {
        LongBuffer longBuffer1 = (LongBuffer)paramBuffer;
        int i = longBuffer1.position();
        if (b != 0)
          paramInt = (longBuffer1.limit() - i) * 8; 
        paramBuffer = ByteBuffer.allocate(paramInt).order(longBuffer1.order());
        LongBuffer longBuffer2 = paramBuffer.asLongBuffer();
        for (b = 0; b < paramInt / 8; b++)
          longBuffer2.put(longBuffer1.get()); 
        longBuffer1.position(i);
        paramBuffer.rewind();
        paramBuffer.order(ByteOrder.nativeOrder());
        return (ByteBuffer)paramBuffer;
      } 
      throw new RuntimeException("Unimplemented Buffer subclass.");
    } 
    paramBuffer.rewind();
    paramBuffer.order(ByteOrder.nativeOrder());
    return (ByteBuffer)paramBuffer;
  }
  
  private char[] toCharIndices(int paramInt1, int paramInt2, Buffer paramBuffer) {
    char[] arrayOfChar = new char[paramInt1];
    if (paramInt2 != 5121) {
      if (paramInt2 == 5123) {
        if (paramBuffer instanceof CharBuffer) {
          paramBuffer = paramBuffer;
        } else {
          paramBuffer = toByteBuffer(paramInt1 * 2, paramBuffer);
          paramBuffer = paramBuffer.asCharBuffer();
        } 
        paramInt1 = paramBuffer.position();
        paramBuffer.position(0);
        paramBuffer.get(arrayOfChar);
        paramBuffer.position(paramInt1);
      } 
    } else {
      ByteBuffer byteBuffer = toByteBuffer(paramInt1, paramBuffer);
      byte[] arrayOfByte = byteBuffer.array();
      int i = byteBuffer.arrayOffset();
      for (paramInt2 = 0; paramInt2 < paramInt1; paramInt2++)
        arrayOfChar[paramInt2] = (char)(arrayOfByte[i + paramInt2] & 0xFF); 
    } 
    return arrayOfChar;
  }
  
  private void doArrayElement(StringBuilder paramStringBuilder, boolean paramBoolean, String paramString, PointerInfo paramPointerInfo, int paramInt) {
    if (!paramBoolean)
      return; 
    paramStringBuilder.append(" ");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(":{");
    paramStringBuilder.append(stringBuilder.toString());
    if (paramPointerInfo == null || paramPointerInfo.mTempByteBuffer == null) {
      paramStringBuilder.append("undefined }");
      return;
    } 
    if (paramPointerInfo.mStride < 0) {
      paramStringBuilder.append("invalid stride");
      return;
    } 
    int i = paramPointerInfo.getStride();
    ByteBuffer byteBuffer = paramPointerInfo.mTempByteBuffer;
    int j = paramPointerInfo.mSize;
    int k = paramPointerInfo.mType;
    int m = paramPointerInfo.sizeof(k);
    i *= paramInt;
    for (paramInt = 0; paramInt < j; paramInt++) {
      if (paramInt > 0)
        paramStringBuilder.append(", "); 
      if (k != 5126) {
        if (k != 5132) {
          ShortBuffer shortBuffer;
          short s;
          switch (k) {
            default:
              paramStringBuilder.append("?");
              break;
            case 5122:
              shortBuffer = byteBuffer.asShortBuffer();
              s = shortBuffer.get(i / 2);
              paramStringBuilder.append(Integer.toString(s));
              break;
            case 5121:
              s = byteBuffer.get(i);
              paramStringBuilder.append(Integer.toString(s & 0xFF));
              break;
            case 5120:
              s = byteBuffer.get(i);
              paramStringBuilder.append(Integer.toString(s));
              break;
          } 
        } else {
          IntBuffer intBuffer = byteBuffer.asIntBuffer();
          int n = intBuffer.get(i / 4);
          paramStringBuilder.append(Integer.toString(n));
        } 
      } else {
        FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
        float f = floatBuffer.get(i / 4);
        paramStringBuilder.append(Float.toString(f));
      } 
      i += m;
    } 
    paramStringBuilder.append("}");
  }
  
  private void doElement(StringBuilder paramStringBuilder, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" [");
    stringBuilder.append(paramInt1);
    stringBuilder.append(" : ");
    stringBuilder.append(paramInt2);
    stringBuilder.append("] =");
    paramStringBuilder.append(stringBuilder.toString());
    doArrayElement(paramStringBuilder, this.mVertexArrayEnabled, "v", this.mVertexPointer, paramInt2);
    doArrayElement(paramStringBuilder, this.mNormalArrayEnabled, "n", this.mNormalPointer, paramInt2);
    doArrayElement(paramStringBuilder, this.mColorArrayEnabled, "c", this.mColorPointer, paramInt2);
    doArrayElement(paramStringBuilder, this.mTextureCoordArrayEnabled, "t", this.mTexCoordPointer, paramInt2);
    paramStringBuilder.append("\n");
  }
  
  private void bindArrays() {
    if (this.mColorArrayEnabled)
      this.mColorPointer.bindByteBuffer(); 
    if (this.mNormalArrayEnabled)
      this.mNormalPointer.bindByteBuffer(); 
    if (this.mTextureCoordArrayEnabled)
      this.mTexCoordPointer.bindByteBuffer(); 
    if (this.mVertexArrayEnabled)
      this.mVertexPointer.bindByteBuffer(); 
  }
  
  private void unbindArrays() {
    if (this.mColorArrayEnabled)
      this.mColorPointer.unbindByteBuffer(); 
    if (this.mNormalArrayEnabled)
      this.mNormalPointer.unbindByteBuffer(); 
    if (this.mTextureCoordArrayEnabled)
      this.mTexCoordPointer.unbindByteBuffer(); 
    if (this.mVertexArrayEnabled)
      this.mVertexPointer.unbindByteBuffer(); 
  }
  
  private void startLogIndices() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("\n");
    bindArrays();
  }
  
  private void endLogIndices() {
    log(this.mStringBuilder.toString());
    unbindArrays();
  }
  
  public void glActiveTexture(int paramInt) {
    begin("glActiveTexture");
    arg("texture", paramInt);
    end();
    this.mgl.glActiveTexture(paramInt);
    checkError();
  }
  
  public void glAlphaFunc(int paramInt, float paramFloat) {
    begin("glAlphaFunc");
    arg("func", paramInt);
    arg("ref", paramFloat);
    end();
    this.mgl.glAlphaFunc(paramInt, paramFloat);
    checkError();
  }
  
  public void glAlphaFuncx(int paramInt1, int paramInt2) {
    begin("glAlphaFuncx");
    arg("func", paramInt1);
    arg("ref", paramInt2);
    end();
    this.mgl.glAlphaFuncx(paramInt1, paramInt2);
    checkError();
  }
  
  public void glBindTexture(int paramInt1, int paramInt2) {
    begin("glBindTexture");
    arg("target", getTextureTarget(paramInt1));
    arg("texture", paramInt2);
    end();
    this.mgl.glBindTexture(paramInt1, paramInt2);
    checkError();
  }
  
  public void glBlendFunc(int paramInt1, int paramInt2) {
    begin("glBlendFunc");
    arg("sfactor", getFactor(paramInt1));
    arg("dfactor", getFactor(paramInt2));
    end();
    this.mgl.glBlendFunc(paramInt1, paramInt2);
    checkError();
  }
  
  public void glClear(int paramInt) {
    begin("glClear");
    arg("mask", getClearBufferMask(paramInt));
    end();
    this.mgl.glClear(paramInt);
    checkError();
  }
  
  public void glClearColor(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    begin("glClearColor");
    arg("red", paramFloat1);
    arg("green", paramFloat2);
    arg("blue", paramFloat3);
    arg("alpha", paramFloat4);
    end();
    this.mgl.glClearColor(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    checkError();
  }
  
  public void glClearColorx(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glClearColor");
    arg("red", paramInt1);
    arg("green", paramInt2);
    arg("blue", paramInt3);
    arg("alpha", paramInt4);
    end();
    this.mgl.glClearColorx(paramInt1, paramInt2, paramInt3, paramInt4);
    checkError();
  }
  
  public void glClearDepthf(float paramFloat) {
    begin("glClearDepthf");
    arg("depth", paramFloat);
    end();
    this.mgl.glClearDepthf(paramFloat);
    checkError();
  }
  
  public void glClearDepthx(int paramInt) {
    begin("glClearDepthx");
    arg("depth", paramInt);
    end();
    this.mgl.glClearDepthx(paramInt);
    checkError();
  }
  
  public void glClearStencil(int paramInt) {
    begin("glClearStencil");
    arg("s", paramInt);
    end();
    this.mgl.glClearStencil(paramInt);
    checkError();
  }
  
  public void glClientActiveTexture(int paramInt) {
    begin("glClientActiveTexture");
    arg("texture", paramInt);
    end();
    this.mgl.glClientActiveTexture(paramInt);
    checkError();
  }
  
  public void glColor4f(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    begin("glColor4f");
    arg("red", paramFloat1);
    arg("green", paramFloat2);
    arg("blue", paramFloat3);
    arg("alpha", paramFloat4);
    end();
    this.mgl.glColor4f(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    checkError();
  }
  
  public void glColor4x(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glColor4x");
    arg("red", paramInt1);
    arg("green", paramInt2);
    arg("blue", paramInt3);
    arg("alpha", paramInt4);
    end();
    this.mgl.glColor4x(paramInt1, paramInt2, paramInt3, paramInt4);
    checkError();
  }
  
  public void glColorMask(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4) {
    begin("glColorMask");
    arg("red", paramBoolean1);
    arg("green", paramBoolean2);
    arg("blue", paramBoolean3);
    arg("alpha", paramBoolean4);
    end();
    this.mgl.glColorMask(paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4);
    checkError();
  }
  
  public void glColorPointer(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer) {
    begin("glColorPointer");
    argPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
    end();
    this.mColorPointer = new PointerInfo(paramInt1, paramInt2, paramInt3, paramBuffer);
    this.mgl.glColorPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
    checkError();
  }
  
  public void glCompressedTexImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, Buffer paramBuffer) {
    begin("glCompressedTexImage2D");
    arg("target", getTextureTarget(paramInt1));
    arg("level", paramInt2);
    arg("internalformat", paramInt3);
    arg("width", paramInt4);
    arg("height", paramInt5);
    arg("border", paramInt6);
    arg("imageSize", paramInt7);
    arg("data", paramBuffer.toString());
    end();
    this.mgl.glCompressedTexImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramBuffer);
    checkError();
  }
  
  public void glCompressedTexSubImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, Buffer paramBuffer) {
    begin("glCompressedTexSubImage2D");
    arg("target", getTextureTarget(paramInt1));
    arg("level", paramInt2);
    arg("xoffset", paramInt3);
    arg("yoffset", paramInt4);
    arg("width", paramInt5);
    arg("height", paramInt6);
    arg("format", paramInt7);
    arg("imageSize", paramInt8);
    arg("data", paramBuffer.toString());
    end();
    this.mgl.glCompressedTexSubImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramBuffer);
    checkError();
  }
  
  public void glCopyTexImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    begin("glCopyTexImage2D");
    arg("target", getTextureTarget(paramInt1));
    arg("level", paramInt2);
    arg("internalformat", paramInt3);
    arg("x", paramInt4);
    arg("y", paramInt5);
    arg("width", paramInt6);
    arg("height", paramInt7);
    arg("border", paramInt8);
    end();
    this.mgl.glCopyTexImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8);
    checkError();
  }
  
  public void glCopyTexSubImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    begin("glCopyTexSubImage2D");
    arg("target", getTextureTarget(paramInt1));
    arg("level", paramInt2);
    arg("xoffset", paramInt3);
    arg("yoffset", paramInt4);
    arg("x", paramInt5);
    arg("y", paramInt6);
    arg("width", paramInt7);
    arg("height", paramInt8);
    end();
    this.mgl.glCopyTexSubImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8);
    checkError();
  }
  
  public void glCullFace(int paramInt) {
    begin("glCullFace");
    arg("mode", paramInt);
    end();
    this.mgl.glCullFace(paramInt);
    checkError();
  }
  
  public void glDeleteTextures(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glDeleteTextures");
    arg("n", paramInt1);
    arg("textures", paramInt1, paramArrayOfint, paramInt2);
    arg("offset", paramInt2);
    end();
    this.mgl.glDeleteTextures(paramInt1, paramArrayOfint, paramInt2);
    checkError();
  }
  
  public void glDeleteTextures(int paramInt, IntBuffer paramIntBuffer) {
    begin("glDeleteTextures");
    arg("n", paramInt);
    arg("textures", paramInt, paramIntBuffer);
    end();
    this.mgl.glDeleteTextures(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glDepthFunc(int paramInt) {
    begin("glDepthFunc");
    arg("func", paramInt);
    end();
    this.mgl.glDepthFunc(paramInt);
    checkError();
  }
  
  public void glDepthMask(boolean paramBoolean) {
    begin("glDepthMask");
    arg("flag", paramBoolean);
    end();
    this.mgl.glDepthMask(paramBoolean);
    checkError();
  }
  
  public void glDepthRangef(float paramFloat1, float paramFloat2) {
    begin("glDepthRangef");
    arg("near", paramFloat1);
    arg("far", paramFloat2);
    end();
    this.mgl.glDepthRangef(paramFloat1, paramFloat2);
    checkError();
  }
  
  public void glDepthRangex(int paramInt1, int paramInt2) {
    begin("glDepthRangex");
    arg("near", paramInt1);
    arg("far", paramInt2);
    end();
    this.mgl.glDepthRangex(paramInt1, paramInt2);
    checkError();
  }
  
  public void glDisable(int paramInt) {
    begin("glDisable");
    arg("cap", getCap(paramInt));
    end();
    this.mgl.glDisable(paramInt);
    checkError();
  }
  
  public void glDisableClientState(int paramInt) {
    begin("glDisableClientState");
    arg("array", getClientState(paramInt));
    end();
    switch (paramInt) {
      case 32888:
        this.mTextureCoordArrayEnabled = false;
        break;
      case 32886:
        this.mColorArrayEnabled = false;
        break;
      case 32885:
        this.mNormalArrayEnabled = false;
        break;
      case 32884:
        this.mVertexArrayEnabled = false;
        break;
    } 
    this.mgl.glDisableClientState(paramInt);
    checkError();
  }
  
  public void glDrawArrays(int paramInt1, int paramInt2, int paramInt3) {
    begin("glDrawArrays");
    arg("mode", paramInt1);
    arg("first", paramInt2);
    arg("count", paramInt3);
    startLogIndices();
    for (byte b = 0; b < paramInt3; b++)
      doElement(this.mStringBuilder, b, paramInt2 + b); 
    endLogIndices();
    end();
    this.mgl.glDrawArrays(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glDrawElements(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer) {
    begin("glDrawElements");
    arg("mode", getBeginMode(paramInt1));
    arg("count", paramInt2);
    arg("type", getIndexType(paramInt3));
    char[] arrayOfChar = toCharIndices(paramInt2, paramInt3, paramBuffer);
    int i = arrayOfChar.length;
    startLogIndices();
    for (byte b = 0; b < i; b++)
      doElement(this.mStringBuilder, b, arrayOfChar[b]); 
    endLogIndices();
    end();
    this.mgl.glDrawElements(paramInt1, paramInt2, paramInt3, paramBuffer);
    checkError();
  }
  
  public void glEnable(int paramInt) {
    begin("glEnable");
    arg("cap", getCap(paramInt));
    end();
    this.mgl.glEnable(paramInt);
    checkError();
  }
  
  public void glEnableClientState(int paramInt) {
    begin("glEnableClientState");
    arg("array", getClientState(paramInt));
    end();
    switch (paramInt) {
      case 32888:
        this.mTextureCoordArrayEnabled = true;
        break;
      case 32886:
        this.mColorArrayEnabled = true;
        break;
      case 32885:
        this.mNormalArrayEnabled = true;
        break;
      case 32884:
        this.mVertexArrayEnabled = true;
        break;
    } 
    this.mgl.glEnableClientState(paramInt);
    checkError();
  }
  
  public void glFinish() {
    begin("glFinish");
    end();
    this.mgl.glFinish();
    checkError();
  }
  
  public void glFlush() {
    begin("glFlush");
    end();
    this.mgl.glFlush();
    checkError();
  }
  
  public void glFogf(int paramInt, float paramFloat) {
    begin("glFogf");
    arg("pname", paramInt);
    arg("param", paramFloat);
    end();
    this.mgl.glFogf(paramInt, paramFloat);
    checkError();
  }
  
  public void glFogfv(int paramInt1, float[] paramArrayOffloat, int paramInt2) {
    begin("glFogfv");
    arg("pname", getFogPName(paramInt1));
    arg("params", getFogParamCount(paramInt1), paramArrayOffloat, paramInt2);
    arg("offset", paramInt2);
    end();
    this.mgl.glFogfv(paramInt1, paramArrayOffloat, paramInt2);
    checkError();
  }
  
  public void glFogfv(int paramInt, FloatBuffer paramFloatBuffer) {
    begin("glFogfv");
    arg("pname", getFogPName(paramInt));
    arg("params", getFogParamCount(paramInt), paramFloatBuffer);
    end();
    this.mgl.glFogfv(paramInt, paramFloatBuffer);
    checkError();
  }
  
  public void glFogx(int paramInt1, int paramInt2) {
    begin("glFogx");
    arg("pname", getFogPName(paramInt1));
    arg("param", paramInt2);
    end();
    this.mgl.glFogx(paramInt1, paramInt2);
    checkError();
  }
  
  public void glFogxv(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glFogxv");
    arg("pname", getFogPName(paramInt1));
    arg("params", getFogParamCount(paramInt1), paramArrayOfint, paramInt2);
    arg("offset", paramInt2);
    end();
    this.mgl.glFogxv(paramInt1, paramArrayOfint, paramInt2);
    checkError();
  }
  
  public void glFogxv(int paramInt, IntBuffer paramIntBuffer) {
    begin("glFogxv");
    arg("pname", getFogPName(paramInt));
    arg("params", getFogParamCount(paramInt), paramIntBuffer);
    end();
    this.mgl.glFogxv(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glFrontFace(int paramInt) {
    begin("glFrontFace");
    arg("mode", paramInt);
    end();
    this.mgl.glFrontFace(paramInt);
    checkError();
  }
  
  public void glFrustumf(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6) {
    begin("glFrustumf");
    arg("left", paramFloat1);
    arg("right", paramFloat2);
    arg("bottom", paramFloat3);
    arg("top", paramFloat4);
    arg("near", paramFloat5);
    arg("far", paramFloat6);
    end();
    this.mgl.glFrustumf(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6);
    checkError();
  }
  
  public void glFrustumx(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    begin("glFrustumx");
    arg("left", paramInt1);
    arg("right", paramInt2);
    arg("bottom", paramInt3);
    arg("top", paramInt4);
    arg("near", paramInt5);
    arg("far", paramInt6);
    end();
    this.mgl.glFrustumx(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
    checkError();
  }
  
  public void glGenTextures(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glGenTextures");
    arg("n", paramInt1);
    arg("textures", Arrays.toString(paramArrayOfint));
    arg("offset", paramInt2);
    this.mgl.glGenTextures(paramInt1, paramArrayOfint, paramInt2);
    returns(toString(paramInt1, 0, paramArrayOfint, paramInt2));
    checkError();
  }
  
  public void glGenTextures(int paramInt, IntBuffer paramIntBuffer) {
    begin("glGenTextures");
    arg("n", paramInt);
    arg("textures", paramIntBuffer.toString());
    this.mgl.glGenTextures(paramInt, paramIntBuffer);
    returns(toString(paramInt, 0, paramIntBuffer));
    checkError();
  }
  
  public int glGetError() {
    begin("glGetError");
    int i = this.mgl.glGetError();
    returns(i);
    return i;
  }
  
  public void glGetIntegerv(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glGetIntegerv");
    arg("pname", getIntegerStateName(paramInt1));
    arg("params", Arrays.toString(paramArrayOfint));
    arg("offset", paramInt2);
    this.mgl.glGetIntegerv(paramInt1, paramArrayOfint, paramInt2);
    int i = getIntegerStateSize(paramInt1);
    paramInt1 = getIntegerStateFormat(paramInt1);
    returns(toString(i, paramInt1, paramArrayOfint, paramInt2));
    checkError();
  }
  
  public void glGetIntegerv(int paramInt, IntBuffer paramIntBuffer) {
    begin("glGetIntegerv");
    arg("pname", getIntegerStateName(paramInt));
    arg("params", paramIntBuffer.toString());
    this.mgl.glGetIntegerv(paramInt, paramIntBuffer);
    int i = getIntegerStateSize(paramInt);
    paramInt = getIntegerStateFormat(paramInt);
    returns(toString(i, paramInt, paramIntBuffer));
    checkError();
  }
  
  public String glGetString(int paramInt) {
    begin("glGetString");
    arg("name", paramInt);
    String str = this.mgl.glGetString(paramInt);
    returns(str);
    checkError();
    return str;
  }
  
  public void glHint(int paramInt1, int paramInt2) {
    begin("glHint");
    arg("target", getHintTarget(paramInt1));
    arg("mode", getHintMode(paramInt2));
    end();
    this.mgl.glHint(paramInt1, paramInt2);
    checkError();
  }
  
  public void glLightModelf(int paramInt, float paramFloat) {
    begin("glLightModelf");
    arg("pname", getLightModelPName(paramInt));
    arg("param", paramFloat);
    end();
    this.mgl.glLightModelf(paramInt, paramFloat);
    checkError();
  }
  
  public void glLightModelfv(int paramInt1, float[] paramArrayOffloat, int paramInt2) {
    begin("glLightModelfv");
    arg("pname", getLightModelPName(paramInt1));
    arg("params", getLightModelParamCount(paramInt1), paramArrayOffloat, paramInt2);
    arg("offset", paramInt2);
    end();
    this.mgl.glLightModelfv(paramInt1, paramArrayOffloat, paramInt2);
    checkError();
  }
  
  public void glLightModelfv(int paramInt, FloatBuffer paramFloatBuffer) {
    begin("glLightModelfv");
    arg("pname", getLightModelPName(paramInt));
    arg("params", getLightModelParamCount(paramInt), paramFloatBuffer);
    end();
    this.mgl.glLightModelfv(paramInt, paramFloatBuffer);
    checkError();
  }
  
  public void glLightModelx(int paramInt1, int paramInt2) {
    begin("glLightModelx");
    arg("pname", getLightModelPName(paramInt1));
    arg("param", paramInt2);
    end();
    this.mgl.glLightModelx(paramInt1, paramInt2);
    checkError();
  }
  
  public void glLightModelxv(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glLightModelxv");
    arg("pname", getLightModelPName(paramInt1));
    arg("params", getLightModelParamCount(paramInt1), paramArrayOfint, paramInt2);
    arg("offset", paramInt2);
    end();
    this.mgl.glLightModelxv(paramInt1, paramArrayOfint, paramInt2);
    checkError();
  }
  
  public void glLightModelxv(int paramInt, IntBuffer paramIntBuffer) {
    begin("glLightModelfv");
    arg("pname", getLightModelPName(paramInt));
    arg("params", getLightModelParamCount(paramInt), paramIntBuffer);
    end();
    this.mgl.glLightModelxv(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glLightf(int paramInt1, int paramInt2, float paramFloat) {
    begin("glLightf");
    arg("light", getLightName(paramInt1));
    arg("pname", getLightPName(paramInt2));
    arg("param", paramFloat);
    end();
    this.mgl.glLightf(paramInt1, paramInt2, paramFloat);
    checkError();
  }
  
  public void glLightfv(int paramInt1, int paramInt2, float[] paramArrayOffloat, int paramInt3) {
    begin("glLightfv");
    arg("light", getLightName(paramInt1));
    arg("pname", getLightPName(paramInt2));
    arg("params", getLightParamCount(paramInt2), paramArrayOffloat, paramInt3);
    arg("offset", paramInt3);
    end();
    this.mgl.glLightfv(paramInt1, paramInt2, paramArrayOffloat, paramInt3);
    checkError();
  }
  
  public void glLightfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer) {
    begin("glLightfv");
    arg("light", getLightName(paramInt1));
    arg("pname", getLightPName(paramInt2));
    arg("params", getLightParamCount(paramInt2), paramFloatBuffer);
    end();
    this.mgl.glLightfv(paramInt1, paramInt2, paramFloatBuffer);
    checkError();
  }
  
  public void glLightx(int paramInt1, int paramInt2, int paramInt3) {
    begin("glLightx");
    arg("light", getLightName(paramInt1));
    arg("pname", getLightPName(paramInt2));
    arg("param", paramInt3);
    end();
    this.mgl.glLightx(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glLightxv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glLightxv");
    arg("light", getLightName(paramInt1));
    arg("pname", getLightPName(paramInt2));
    arg("params", getLightParamCount(paramInt2), paramArrayOfint, paramInt3);
    arg("offset", paramInt3);
    end();
    this.mgl.glLightxv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glLightxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glLightxv");
    arg("light", getLightName(paramInt1));
    arg("pname", getLightPName(paramInt2));
    arg("params", getLightParamCount(paramInt2), paramIntBuffer);
    end();
    this.mgl.glLightxv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glLineWidth(float paramFloat) {
    begin("glLineWidth");
    arg("width", paramFloat);
    end();
    this.mgl.glLineWidth(paramFloat);
    checkError();
  }
  
  public void glLineWidthx(int paramInt) {
    begin("glLineWidthx");
    arg("width", paramInt);
    end();
    this.mgl.glLineWidthx(paramInt);
    checkError();
  }
  
  public void glLoadIdentity() {
    begin("glLoadIdentity");
    end();
    this.mgl.glLoadIdentity();
    checkError();
  }
  
  public void glLoadMatrixf(float[] paramArrayOffloat, int paramInt) {
    begin("glLoadMatrixf");
    arg("m", 16, paramArrayOffloat, paramInt);
    arg("offset", paramInt);
    end();
    this.mgl.glLoadMatrixf(paramArrayOffloat, paramInt);
    checkError();
  }
  
  public void glLoadMatrixf(FloatBuffer paramFloatBuffer) {
    begin("glLoadMatrixf");
    arg("m", 16, paramFloatBuffer);
    end();
    this.mgl.glLoadMatrixf(paramFloatBuffer);
    checkError();
  }
  
  public void glLoadMatrixx(int[] paramArrayOfint, int paramInt) {
    begin("glLoadMatrixx");
    arg("m", 16, paramArrayOfint, paramInt);
    arg("offset", paramInt);
    end();
    this.mgl.glLoadMatrixx(paramArrayOfint, paramInt);
    checkError();
  }
  
  public void glLoadMatrixx(IntBuffer paramIntBuffer) {
    begin("glLoadMatrixx");
    arg("m", 16, paramIntBuffer);
    end();
    this.mgl.glLoadMatrixx(paramIntBuffer);
    checkError();
  }
  
  public void glLogicOp(int paramInt) {
    begin("glLogicOp");
    arg("opcode", paramInt);
    end();
    this.mgl.glLogicOp(paramInt);
    checkError();
  }
  
  public void glMaterialf(int paramInt1, int paramInt2, float paramFloat) {
    begin("glMaterialf");
    arg("face", getFaceName(paramInt1));
    arg("pname", getMaterialPName(paramInt2));
    arg("param", paramFloat);
    end();
    this.mgl.glMaterialf(paramInt1, paramInt2, paramFloat);
    checkError();
  }
  
  public void glMaterialfv(int paramInt1, int paramInt2, float[] paramArrayOffloat, int paramInt3) {
    begin("glMaterialfv");
    arg("face", getFaceName(paramInt1));
    arg("pname", getMaterialPName(paramInt2));
    arg("params", getMaterialParamCount(paramInt2), paramArrayOffloat, paramInt3);
    arg("offset", paramInt3);
    end();
    this.mgl.glMaterialfv(paramInt1, paramInt2, paramArrayOffloat, paramInt3);
    checkError();
  }
  
  public void glMaterialfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer) {
    begin("glMaterialfv");
    arg("face", getFaceName(paramInt1));
    arg("pname", getMaterialPName(paramInt2));
    arg("params", getMaterialParamCount(paramInt2), paramFloatBuffer);
    end();
    this.mgl.glMaterialfv(paramInt1, paramInt2, paramFloatBuffer);
    checkError();
  }
  
  public void glMaterialx(int paramInt1, int paramInt2, int paramInt3) {
    begin("glMaterialx");
    arg("face", getFaceName(paramInt1));
    arg("pname", getMaterialPName(paramInt2));
    arg("param", paramInt3);
    end();
    this.mgl.glMaterialx(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glMaterialxv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glMaterialxv");
    arg("face", getFaceName(paramInt1));
    arg("pname", getMaterialPName(paramInt2));
    arg("params", getMaterialParamCount(paramInt2), paramArrayOfint, paramInt3);
    arg("offset", paramInt3);
    end();
    this.mgl.glMaterialxv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glMaterialxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glMaterialxv");
    arg("face", getFaceName(paramInt1));
    arg("pname", getMaterialPName(paramInt2));
    arg("params", getMaterialParamCount(paramInt2), paramIntBuffer);
    end();
    this.mgl.glMaterialxv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glMatrixMode(int paramInt) {
    begin("glMatrixMode");
    arg("mode", getMatrixMode(paramInt));
    end();
    this.mgl.glMatrixMode(paramInt);
    checkError();
  }
  
  public void glMultMatrixf(float[] paramArrayOffloat, int paramInt) {
    begin("glMultMatrixf");
    arg("m", 16, paramArrayOffloat, paramInt);
    arg("offset", paramInt);
    end();
    this.mgl.glMultMatrixf(paramArrayOffloat, paramInt);
    checkError();
  }
  
  public void glMultMatrixf(FloatBuffer paramFloatBuffer) {
    begin("glMultMatrixf");
    arg("m", 16, paramFloatBuffer);
    end();
    this.mgl.glMultMatrixf(paramFloatBuffer);
    checkError();
  }
  
  public void glMultMatrixx(int[] paramArrayOfint, int paramInt) {
    begin("glMultMatrixx");
    arg("m", 16, paramArrayOfint, paramInt);
    arg("offset", paramInt);
    end();
    this.mgl.glMultMatrixx(paramArrayOfint, paramInt);
    checkError();
  }
  
  public void glMultMatrixx(IntBuffer paramIntBuffer) {
    begin("glMultMatrixx");
    arg("m", 16, paramIntBuffer);
    end();
    this.mgl.glMultMatrixx(paramIntBuffer);
    checkError();
  }
  
  public void glMultiTexCoord4f(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    begin("glMultiTexCoord4f");
    arg("target", paramInt);
    arg("s", paramFloat1);
    arg("t", paramFloat2);
    arg("r", paramFloat3);
    arg("q", paramFloat4);
    end();
    this.mgl.glMultiTexCoord4f(paramInt, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    checkError();
  }
  
  public void glMultiTexCoord4x(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    begin("glMultiTexCoord4x");
    arg("target", paramInt1);
    arg("s", paramInt2);
    arg("t", paramInt3);
    arg("r", paramInt4);
    arg("q", paramInt5);
    end();
    this.mgl.glMultiTexCoord4x(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    checkError();
  }
  
  public void glNormal3f(float paramFloat1, float paramFloat2, float paramFloat3) {
    begin("glNormal3f");
    arg("nx", paramFloat1);
    arg("ny", paramFloat2);
    arg("nz", paramFloat3);
    end();
    this.mgl.glNormal3f(paramFloat1, paramFloat2, paramFloat3);
    checkError();
  }
  
  public void glNormal3x(int paramInt1, int paramInt2, int paramInt3) {
    begin("glNormal3x");
    arg("nx", paramInt1);
    arg("ny", paramInt2);
    arg("nz", paramInt3);
    end();
    this.mgl.glNormal3x(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glNormalPointer(int paramInt1, int paramInt2, Buffer paramBuffer) {
    begin("glNormalPointer");
    arg("type", paramInt1);
    arg("stride", paramInt2);
    arg("pointer", paramBuffer.toString());
    end();
    this.mNormalPointer = new PointerInfo(3, paramInt1, paramInt2, paramBuffer);
    this.mgl.glNormalPointer(paramInt1, paramInt2, paramBuffer);
    checkError();
  }
  
  public void glOrthof(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6) {
    begin("glOrthof");
    arg("left", paramFloat1);
    arg("right", paramFloat2);
    arg("bottom", paramFloat3);
    arg("top", paramFloat4);
    arg("near", paramFloat5);
    arg("far", paramFloat6);
    end();
    this.mgl.glOrthof(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6);
    checkError();
  }
  
  public void glOrthox(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    begin("glOrthox");
    arg("left", paramInt1);
    arg("right", paramInt2);
    arg("bottom", paramInt3);
    arg("top", paramInt4);
    arg("near", paramInt5);
    arg("far", paramInt6);
    end();
    this.mgl.glOrthox(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
    checkError();
  }
  
  public void glPixelStorei(int paramInt1, int paramInt2) {
    begin("glPixelStorei");
    arg("pname", paramInt1);
    arg("param", paramInt2);
    end();
    this.mgl.glPixelStorei(paramInt1, paramInt2);
    checkError();
  }
  
  public void glPointSize(float paramFloat) {
    begin("glPointSize");
    arg("size", paramFloat);
    end();
    this.mgl.glPointSize(paramFloat);
    checkError();
  }
  
  public void glPointSizex(int paramInt) {
    begin("glPointSizex");
    arg("size", paramInt);
    end();
    this.mgl.glPointSizex(paramInt);
    checkError();
  }
  
  public void glPolygonOffset(float paramFloat1, float paramFloat2) {
    begin("glPolygonOffset");
    arg("factor", paramFloat1);
    arg("units", paramFloat2);
    end();
    this.mgl.glPolygonOffset(paramFloat1, paramFloat2);
    checkError();
  }
  
  public void glPolygonOffsetx(int paramInt1, int paramInt2) {
    begin("glPolygonOffsetx");
    arg("factor", paramInt1);
    arg("units", paramInt2);
    end();
    this.mgl.glPolygonOffsetx(paramInt1, paramInt2);
    checkError();
  }
  
  public void glPopMatrix() {
    begin("glPopMatrix");
    end();
    this.mgl.glPopMatrix();
    checkError();
  }
  
  public void glPushMatrix() {
    begin("glPushMatrix");
    end();
    this.mgl.glPushMatrix();
    checkError();
  }
  
  public void glReadPixels(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Buffer paramBuffer) {
    begin("glReadPixels");
    arg("x", paramInt1);
    arg("y", paramInt2);
    arg("width", paramInt3);
    arg("height", paramInt4);
    arg("format", paramInt5);
    arg("type", paramInt6);
    arg("pixels", paramBuffer.toString());
    end();
    this.mgl.glReadPixels(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramBuffer);
    checkError();
  }
  
  public void glRotatef(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4) {
    begin("glRotatef");
    arg("angle", paramFloat1);
    arg("x", paramFloat2);
    arg("y", paramFloat3);
    arg("z", paramFloat4);
    end();
    this.mgl.glRotatef(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    checkError();
  }
  
  public void glRotatex(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glRotatex");
    arg("angle", paramInt1);
    arg("x", paramInt2);
    arg("y", paramInt3);
    arg("z", paramInt4);
    end();
    this.mgl.glRotatex(paramInt1, paramInt2, paramInt3, paramInt4);
    checkError();
  }
  
  public void glSampleCoverage(float paramFloat, boolean paramBoolean) {
    begin("glSampleCoveragex");
    arg("value", paramFloat);
    arg("invert", paramBoolean);
    end();
    this.mgl.glSampleCoverage(paramFloat, paramBoolean);
    checkError();
  }
  
  public void glSampleCoveragex(int paramInt, boolean paramBoolean) {
    begin("glSampleCoveragex");
    arg("value", paramInt);
    arg("invert", paramBoolean);
    end();
    this.mgl.glSampleCoveragex(paramInt, paramBoolean);
    checkError();
  }
  
  public void glScalef(float paramFloat1, float paramFloat2, float paramFloat3) {
    begin("glScalef");
    arg("x", paramFloat1);
    arg("y", paramFloat2);
    arg("z", paramFloat3);
    end();
    this.mgl.glScalef(paramFloat1, paramFloat2, paramFloat3);
    checkError();
  }
  
  public void glScalex(int paramInt1, int paramInt2, int paramInt3) {
    begin("glScalex");
    arg("x", paramInt1);
    arg("y", paramInt2);
    arg("z", paramInt3);
    end();
    this.mgl.glScalex(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glScissor(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glScissor");
    arg("x", paramInt1);
    arg("y", paramInt2);
    arg("width", paramInt3);
    arg("height", paramInt4);
    end();
    this.mgl.glScissor(paramInt1, paramInt2, paramInt3, paramInt4);
    checkError();
  }
  
  public void glShadeModel(int paramInt) {
    begin("glShadeModel");
    arg("mode", getShadeModel(paramInt));
    end();
    this.mgl.glShadeModel(paramInt);
    checkError();
  }
  
  public void glStencilFunc(int paramInt1, int paramInt2, int paramInt3) {
    begin("glStencilFunc");
    arg("func", paramInt1);
    arg("ref", paramInt2);
    arg("mask", paramInt3);
    end();
    this.mgl.glStencilFunc(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glStencilMask(int paramInt) {
    begin("glStencilMask");
    arg("mask", paramInt);
    end();
    this.mgl.glStencilMask(paramInt);
    checkError();
  }
  
  public void glStencilOp(int paramInt1, int paramInt2, int paramInt3) {
    begin("glStencilOp");
    arg("fail", paramInt1);
    arg("zfail", paramInt2);
    arg("zpass", paramInt3);
    end();
    this.mgl.glStencilOp(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glTexCoordPointer(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer) {
    begin("glTexCoordPointer");
    argPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
    end();
    this.mTexCoordPointer = new PointerInfo(paramInt1, paramInt2, paramInt3, paramBuffer);
    this.mgl.glTexCoordPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
    checkError();
  }
  
  public void glTexEnvf(int paramInt1, int paramInt2, float paramFloat) {
    begin("glTexEnvf");
    arg("target", getTextureEnvTarget(paramInt1));
    arg("pname", getTextureEnvPName(paramInt2));
    arg("param", getTextureEnvParamName(paramFloat));
    end();
    this.mgl.glTexEnvf(paramInt1, paramInt2, paramFloat);
    checkError();
  }
  
  public void glTexEnvfv(int paramInt1, int paramInt2, float[] paramArrayOffloat, int paramInt3) {
    begin("glTexEnvfv");
    arg("target", getTextureEnvTarget(paramInt1));
    arg("pname", getTextureEnvPName(paramInt2));
    arg("params", getTextureEnvParamCount(paramInt2), paramArrayOffloat, paramInt3);
    arg("offset", paramInt3);
    end();
    this.mgl.glTexEnvfv(paramInt1, paramInt2, paramArrayOffloat, paramInt3);
    checkError();
  }
  
  public void glTexEnvfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer) {
    begin("glTexEnvfv");
    arg("target", getTextureEnvTarget(paramInt1));
    arg("pname", getTextureEnvPName(paramInt2));
    arg("params", getTextureEnvParamCount(paramInt2), paramFloatBuffer);
    end();
    this.mgl.glTexEnvfv(paramInt1, paramInt2, paramFloatBuffer);
    checkError();
  }
  
  public void glTexEnvx(int paramInt1, int paramInt2, int paramInt3) {
    begin("glTexEnvx");
    arg("target", getTextureEnvTarget(paramInt1));
    arg("pname", getTextureEnvPName(paramInt2));
    arg("param", paramInt3);
    end();
    this.mgl.glTexEnvx(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glTexEnvxv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glTexEnvxv");
    arg("target", getTextureEnvTarget(paramInt1));
    arg("pname", getTextureEnvPName(paramInt2));
    arg("params", getTextureEnvParamCount(paramInt2), paramArrayOfint, paramInt3);
    arg("offset", paramInt3);
    end();
    this.mgl.glTexEnvxv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glTexEnvxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glTexEnvxv");
    arg("target", getTextureEnvTarget(paramInt1));
    arg("pname", getTextureEnvPName(paramInt2));
    arg("params", getTextureEnvParamCount(paramInt2), paramIntBuffer);
    end();
    this.mgl.glTexEnvxv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glTexImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, Buffer paramBuffer) {
    begin("glTexImage2D");
    arg("target", paramInt1);
    arg("level", paramInt2);
    arg("internalformat", paramInt3);
    arg("width", paramInt4);
    arg("height", paramInt5);
    arg("border", paramInt6);
    arg("format", paramInt7);
    arg("type", paramInt8);
    arg("pixels", paramBuffer.toString());
    end();
    this.mgl.glTexImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramBuffer);
    checkError();
  }
  
  public void glTexParameterf(int paramInt1, int paramInt2, float paramFloat) {
    begin("glTexParameterf");
    arg("target", getTextureTarget(paramInt1));
    arg("pname", getTexturePName(paramInt2));
    arg("param", getTextureParamName(paramFloat));
    end();
    this.mgl.glTexParameterf(paramInt1, paramInt2, paramFloat);
    checkError();
  }
  
  public void glTexParameterx(int paramInt1, int paramInt2, int paramInt3) {
    begin("glTexParameterx");
    arg("target", getTextureTarget(paramInt1));
    arg("pname", getTexturePName(paramInt2));
    arg("param", paramInt3);
    end();
    this.mgl.glTexParameterx(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glTexParameteriv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glTexParameteriv");
    arg("target", getTextureTarget(paramInt1));
    arg("pname", getTexturePName(paramInt2));
    arg("params", 4, paramArrayOfint, paramInt3);
    end();
    this.mgl11.glTexParameteriv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glTexParameteriv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glTexParameteriv");
    arg("target", getTextureTarget(paramInt1));
    arg("pname", getTexturePName(paramInt2));
    arg("params", 4, paramIntBuffer);
    end();
    this.mgl11.glTexParameteriv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glTexSubImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, Buffer paramBuffer) {
    begin("glTexSubImage2D");
    arg("target", getTextureTarget(paramInt1));
    arg("level", paramInt2);
    arg("xoffset", paramInt3);
    arg("yoffset", paramInt4);
    arg("width", paramInt5);
    arg("height", paramInt6);
    arg("format", paramInt7);
    arg("type", paramInt8);
    arg("pixels", paramBuffer.toString());
    end();
    this.mgl.glTexSubImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramBuffer);
    checkError();
  }
  
  public void glTranslatef(float paramFloat1, float paramFloat2, float paramFloat3) {
    begin("glTranslatef");
    arg("x", paramFloat1);
    arg("y", paramFloat2);
    arg("z", paramFloat3);
    end();
    this.mgl.glTranslatef(paramFloat1, paramFloat2, paramFloat3);
    checkError();
  }
  
  public void glTranslatex(int paramInt1, int paramInt2, int paramInt3) {
    begin("glTranslatex");
    arg("x", paramInt1);
    arg("y", paramInt2);
    arg("z", paramInt3);
    end();
    this.mgl.glTranslatex(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glVertexPointer(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer) {
    begin("glVertexPointer");
    argPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
    end();
    this.mVertexPointer = new PointerInfo(paramInt1, paramInt2, paramInt3, paramBuffer);
    this.mgl.glVertexPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
    checkError();
  }
  
  public void glViewport(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glViewport");
    arg("x", paramInt1);
    arg("y", paramInt2);
    arg("width", paramInt3);
    arg("height", paramInt4);
    end();
    this.mgl.glViewport(paramInt1, paramInt2, paramInt3, paramInt4);
    checkError();
  }
  
  public void glClipPlanef(int paramInt1, float[] paramArrayOffloat, int paramInt2) {
    begin("glClipPlanef");
    arg("plane", paramInt1);
    arg("equation", 4, paramArrayOffloat, paramInt2);
    arg("offset", paramInt2);
    end();
    this.mgl11.glClipPlanef(paramInt1, paramArrayOffloat, paramInt2);
    checkError();
  }
  
  public void glClipPlanef(int paramInt, FloatBuffer paramFloatBuffer) {
    begin("glClipPlanef");
    arg("plane", paramInt);
    arg("equation", 4, paramFloatBuffer);
    end();
    this.mgl11.glClipPlanef(paramInt, paramFloatBuffer);
    checkError();
  }
  
  public void glClipPlanex(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glClipPlanex");
    arg("plane", paramInt1);
    arg("equation", 4, paramArrayOfint, paramInt2);
    arg("offset", paramInt2);
    end();
    this.mgl11.glClipPlanex(paramInt1, paramArrayOfint, paramInt2);
    checkError();
  }
  
  public void glClipPlanex(int paramInt, IntBuffer paramIntBuffer) {
    begin("glClipPlanef");
    arg("plane", paramInt);
    arg("equation", 4, paramIntBuffer);
    end();
    this.mgl11.glClipPlanex(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glDrawTexfOES(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5) {
    begin("glDrawTexfOES");
    arg("x", paramFloat1);
    arg("y", paramFloat2);
    arg("z", paramFloat3);
    arg("width", paramFloat4);
    arg("height", paramFloat5);
    end();
    this.mgl11Ext.glDrawTexfOES(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5);
    checkError();
  }
  
  public void glDrawTexfvOES(float[] paramArrayOffloat, int paramInt) {
    begin("glDrawTexfvOES");
    arg("coords", 5, paramArrayOffloat, paramInt);
    arg("offset", paramInt);
    end();
    this.mgl11Ext.glDrawTexfvOES(paramArrayOffloat, paramInt);
    checkError();
  }
  
  public void glDrawTexfvOES(FloatBuffer paramFloatBuffer) {
    begin("glDrawTexfvOES");
    arg("coords", 5, paramFloatBuffer);
    end();
    this.mgl11Ext.glDrawTexfvOES(paramFloatBuffer);
    checkError();
  }
  
  public void glDrawTexiOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    begin("glDrawTexiOES");
    arg("x", paramInt1);
    arg("y", paramInt2);
    arg("z", paramInt3);
    arg("width", paramInt4);
    arg("height", paramInt5);
    end();
    this.mgl11Ext.glDrawTexiOES(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    checkError();
  }
  
  public void glDrawTexivOES(int[] paramArrayOfint, int paramInt) {
    begin("glDrawTexivOES");
    arg("coords", 5, paramArrayOfint, paramInt);
    arg("offset", paramInt);
    end();
    this.mgl11Ext.glDrawTexivOES(paramArrayOfint, paramInt);
    checkError();
  }
  
  public void glDrawTexivOES(IntBuffer paramIntBuffer) {
    begin("glDrawTexivOES");
    arg("coords", 5, paramIntBuffer);
    end();
    this.mgl11Ext.glDrawTexivOES(paramIntBuffer);
    checkError();
  }
  
  public void glDrawTexsOES(short paramShort1, short paramShort2, short paramShort3, short paramShort4, short paramShort5) {
    begin("glDrawTexsOES");
    arg("x", paramShort1);
    arg("y", paramShort2);
    arg("z", paramShort3);
    arg("width", paramShort4);
    arg("height", paramShort5);
    end();
    this.mgl11Ext.glDrawTexsOES(paramShort1, paramShort2, paramShort3, paramShort4, paramShort5);
    checkError();
  }
  
  public void glDrawTexsvOES(short[] paramArrayOfshort, int paramInt) {
    begin("glDrawTexsvOES");
    arg("coords", 5, paramArrayOfshort, paramInt);
    arg("offset", paramInt);
    end();
    this.mgl11Ext.glDrawTexsvOES(paramArrayOfshort, paramInt);
    checkError();
  }
  
  public void glDrawTexsvOES(ShortBuffer paramShortBuffer) {
    begin("glDrawTexsvOES");
    arg("coords", 5, paramShortBuffer);
    end();
    this.mgl11Ext.glDrawTexsvOES(paramShortBuffer);
    checkError();
  }
  
  public void glDrawTexxOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    begin("glDrawTexxOES");
    arg("x", paramInt1);
    arg("y", paramInt2);
    arg("z", paramInt3);
    arg("width", paramInt4);
    arg("height", paramInt5);
    end();
    this.mgl11Ext.glDrawTexxOES(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    checkError();
  }
  
  public void glDrawTexxvOES(int[] paramArrayOfint, int paramInt) {
    begin("glDrawTexxvOES");
    arg("coords", 5, paramArrayOfint, paramInt);
    arg("offset", paramInt);
    end();
    this.mgl11Ext.glDrawTexxvOES(paramArrayOfint, paramInt);
    checkError();
  }
  
  public void glDrawTexxvOES(IntBuffer paramIntBuffer) {
    begin("glDrawTexxvOES");
    arg("coords", 5, paramIntBuffer);
    end();
    this.mgl11Ext.glDrawTexxvOES(paramIntBuffer);
    checkError();
  }
  
  public int glQueryMatrixxOES(int[] paramArrayOfint1, int paramInt1, int[] paramArrayOfint2, int paramInt2) {
    begin("glQueryMatrixxOES");
    arg("mantissa", Arrays.toString(paramArrayOfint1));
    arg("exponent", Arrays.toString(paramArrayOfint2));
    end();
    int i = this.mgl10Ext.glQueryMatrixxOES(paramArrayOfint1, paramInt1, paramArrayOfint2, paramInt2);
    returns(toString(16, 2, paramArrayOfint1, paramInt1));
    returns(toString(16, 0, paramArrayOfint2, paramInt2));
    checkError();
    return i;
  }
  
  public int glQueryMatrixxOES(IntBuffer paramIntBuffer1, IntBuffer paramIntBuffer2) {
    begin("glQueryMatrixxOES");
    arg("mantissa", paramIntBuffer1.toString());
    arg("exponent", paramIntBuffer2.toString());
    end();
    int i = this.mgl10Ext.glQueryMatrixxOES(paramIntBuffer1, paramIntBuffer2);
    returns(toString(16, 2, paramIntBuffer1));
    returns(toString(16, 0, paramIntBuffer2));
    checkError();
    return i;
  }
  
  public void glBindBuffer(int paramInt1, int paramInt2) {
    begin("glBindBuffer");
    arg("target", paramInt1);
    arg("buffer", paramInt2);
    end();
    this.mgl11.glBindBuffer(paramInt1, paramInt2);
    checkError();
  }
  
  public void glBufferData(int paramInt1, int paramInt2, Buffer paramBuffer, int paramInt3) {
    begin("glBufferData");
    arg("target", paramInt1);
    arg("size", paramInt2);
    arg("data", paramBuffer.toString());
    arg("usage", paramInt3);
    end();
    this.mgl11.glBufferData(paramInt1, paramInt2, paramBuffer, paramInt3);
    checkError();
  }
  
  public void glBufferSubData(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer) {
    begin("glBufferSubData");
    arg("target", paramInt1);
    arg("offset", paramInt2);
    arg("size", paramInt3);
    arg("data", paramBuffer.toString());
    end();
    this.mgl11.glBufferSubData(paramInt1, paramInt2, paramInt3, paramBuffer);
    checkError();
  }
  
  public void glColor4ub(byte paramByte1, byte paramByte2, byte paramByte3, byte paramByte4) {
    begin("glColor4ub");
    arg("red", paramByte1);
    arg("green", paramByte2);
    arg("blue", paramByte3);
    arg("alpha", paramByte4);
    end();
    this.mgl11.glColor4ub(paramByte1, paramByte2, paramByte3, paramByte4);
    checkError();
  }
  
  public void glDeleteBuffers(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glDeleteBuffers");
    arg("n", paramInt1);
    arg("buffers", paramArrayOfint.toString());
    arg("offset", paramInt2);
    end();
    this.mgl11.glDeleteBuffers(paramInt1, paramArrayOfint, paramInt2);
    checkError();
  }
  
  public void glDeleteBuffers(int paramInt, IntBuffer paramIntBuffer) {
    begin("glDeleteBuffers");
    arg("n", paramInt);
    arg("buffers", paramIntBuffer.toString());
    end();
    this.mgl11.glDeleteBuffers(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glGenBuffers(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glGenBuffers");
    arg("n", paramInt1);
    arg("buffers", paramArrayOfint.toString());
    arg("offset", paramInt2);
    end();
    this.mgl11.glGenBuffers(paramInt1, paramArrayOfint, paramInt2);
    checkError();
  }
  
  public void glGenBuffers(int paramInt, IntBuffer paramIntBuffer) {
    begin("glGenBuffers");
    arg("n", paramInt);
    arg("buffers", paramIntBuffer.toString());
    end();
    this.mgl11.glGenBuffers(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glGetBooleanv(int paramInt1, boolean[] paramArrayOfboolean, int paramInt2) {
    begin("glGetBooleanv");
    arg("pname", paramInt1);
    arg("params", paramArrayOfboolean.toString());
    arg("offset", paramInt2);
    end();
    this.mgl11.glGetBooleanv(paramInt1, paramArrayOfboolean, paramInt2);
    checkError();
  }
  
  public void glGetBooleanv(int paramInt, IntBuffer paramIntBuffer) {
    begin("glGetBooleanv");
    arg("pname", paramInt);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11.glGetBooleanv(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glGetBufferParameteriv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glGetBufferParameteriv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11.glGetBufferParameteriv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glGetBufferParameteriv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glGetBufferParameteriv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11.glGetBufferParameteriv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glGetClipPlanef(int paramInt1, float[] paramArrayOffloat, int paramInt2) {
    begin("glGetClipPlanef");
    arg("pname", paramInt1);
    arg("eqn", paramArrayOffloat.toString());
    arg("offset", paramInt2);
    end();
    this.mgl11.glGetClipPlanef(paramInt1, paramArrayOffloat, paramInt2);
    checkError();
  }
  
  public void glGetClipPlanef(int paramInt, FloatBuffer paramFloatBuffer) {
    begin("glGetClipPlanef");
    arg("pname", paramInt);
    arg("eqn", paramFloatBuffer.toString());
    end();
    this.mgl11.glGetClipPlanef(paramInt, paramFloatBuffer);
    checkError();
  }
  
  public void glGetClipPlanex(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glGetClipPlanex");
    arg("pname", paramInt1);
    arg("eqn", paramArrayOfint.toString());
    arg("offset", paramInt2);
    end();
    this.mgl11.glGetClipPlanex(paramInt1, paramArrayOfint, paramInt2);
  }
  
  public void glGetClipPlanex(int paramInt, IntBuffer paramIntBuffer) {
    begin("glGetClipPlanex");
    arg("pname", paramInt);
    arg("eqn", paramIntBuffer.toString());
    end();
    this.mgl11.glGetClipPlanex(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glGetFixedv(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glGetFixedv");
    arg("pname", paramInt1);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt2);
    end();
    this.mgl11.glGetFixedv(paramInt1, paramArrayOfint, paramInt2);
  }
  
  public void glGetFixedv(int paramInt, IntBuffer paramIntBuffer) {
    begin("glGetFixedv");
    arg("pname", paramInt);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11.glGetFixedv(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glGetFloatv(int paramInt1, float[] paramArrayOffloat, int paramInt2) {
    begin("glGetFloatv");
    arg("pname", paramInt1);
    arg("params", paramArrayOffloat.toString());
    arg("offset", paramInt2);
    end();
    this.mgl11.glGetFloatv(paramInt1, paramArrayOffloat, paramInt2);
  }
  
  public void glGetFloatv(int paramInt, FloatBuffer paramFloatBuffer) {
    begin("glGetFloatv");
    arg("pname", paramInt);
    arg("params", paramFloatBuffer.toString());
    end();
    this.mgl11.glGetFloatv(paramInt, paramFloatBuffer);
    checkError();
  }
  
  public void glGetLightfv(int paramInt1, int paramInt2, float[] paramArrayOffloat, int paramInt3) {
    begin("glGetLightfv");
    arg("light", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOffloat.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11.glGetLightfv(paramInt1, paramInt2, paramArrayOffloat, paramInt3);
    checkError();
  }
  
  public void glGetLightfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer) {
    begin("glGetLightfv");
    arg("light", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramFloatBuffer.toString());
    end();
    this.mgl11.glGetLightfv(paramInt1, paramInt2, paramFloatBuffer);
    checkError();
  }
  
  public void glGetLightxv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glGetLightxv");
    arg("light", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11.glGetLightxv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glGetLightxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glGetLightxv");
    arg("light", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11.glGetLightxv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glGetMaterialfv(int paramInt1, int paramInt2, float[] paramArrayOffloat, int paramInt3) {
    begin("glGetMaterialfv");
    arg("face", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOffloat.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11.glGetMaterialfv(paramInt1, paramInt2, paramArrayOffloat, paramInt3);
    checkError();
  }
  
  public void glGetMaterialfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer) {
    begin("glGetMaterialfv");
    arg("face", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramFloatBuffer.toString());
    end();
    this.mgl11.glGetMaterialfv(paramInt1, paramInt2, paramFloatBuffer);
    checkError();
  }
  
  public void glGetMaterialxv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glGetMaterialxv");
    arg("face", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11.glGetMaterialxv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glGetMaterialxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glGetMaterialxv");
    arg("face", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11.glGetMaterialxv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glGetTexEnviv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glGetTexEnviv");
    arg("env", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11.glGetTexEnviv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glGetTexEnviv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glGetTexEnviv");
    arg("env", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11.glGetTexEnviv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glGetTexEnvxv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glGetTexEnviv");
    arg("env", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11.glGetTexEnviv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glGetTexEnvxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glGetTexEnviv");
    arg("env", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11.glGetTexEnvxv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glGetTexParameterfv(int paramInt1, int paramInt2, float[] paramArrayOffloat, int paramInt3) {
    begin("glGetTexParameterfv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOffloat.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11.glGetTexParameterfv(paramInt1, paramInt2, paramArrayOffloat, paramInt3);
    checkError();
  }
  
  public void glGetTexParameterfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer) {
    begin("glGetTexParameterfv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramFloatBuffer.toString());
    end();
    this.mgl11.glGetTexParameterfv(paramInt1, paramInt2, paramFloatBuffer);
    checkError();
  }
  
  public void glGetTexParameteriv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glGetTexParameteriv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11.glGetTexEnviv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glGetTexParameteriv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glGetTexParameteriv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11.glGetTexParameteriv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glGetTexParameterxv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glGetTexParameterxv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11.glGetTexParameterxv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glGetTexParameterxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glGetTexParameterxv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11.glGetTexParameterxv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public boolean glIsBuffer(int paramInt) {
    begin("glIsBuffer");
    arg("buffer", paramInt);
    end();
    boolean bool = this.mgl11.glIsBuffer(paramInt);
    checkError();
    return bool;
  }
  
  public boolean glIsEnabled(int paramInt) {
    begin("glIsEnabled");
    arg("cap", paramInt);
    end();
    boolean bool = this.mgl11.glIsEnabled(paramInt);
    checkError();
    return bool;
  }
  
  public boolean glIsTexture(int paramInt) {
    begin("glIsTexture");
    arg("texture", paramInt);
    end();
    boolean bool = this.mgl11.glIsTexture(paramInt);
    checkError();
    return bool;
  }
  
  public void glPointParameterf(int paramInt, float paramFloat) {
    begin("glPointParameterf");
    arg("pname", paramInt);
    arg("param", paramFloat);
    end();
    this.mgl11.glPointParameterf(paramInt, paramFloat);
    checkError();
  }
  
  public void glPointParameterfv(int paramInt1, float[] paramArrayOffloat, int paramInt2) {
    begin("glPointParameterfv");
    arg("pname", paramInt1);
    arg("params", paramArrayOffloat.toString());
    arg("offset", paramInt2);
    end();
    this.mgl11.glPointParameterfv(paramInt1, paramArrayOffloat, paramInt2);
    checkError();
  }
  
  public void glPointParameterfv(int paramInt, FloatBuffer paramFloatBuffer) {
    begin("glPointParameterfv");
    arg("pname", paramInt);
    arg("params", paramFloatBuffer.toString());
    end();
    this.mgl11.glPointParameterfv(paramInt, paramFloatBuffer);
    checkError();
  }
  
  public void glPointParameterx(int paramInt1, int paramInt2) {
    begin("glPointParameterfv");
    arg("pname", paramInt1);
    arg("param", paramInt2);
    end();
    this.mgl11.glPointParameterx(paramInt1, paramInt2);
    checkError();
  }
  
  public void glPointParameterxv(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glPointParameterxv");
    arg("pname", paramInt1);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt2);
    end();
    this.mgl11.glPointParameterxv(paramInt1, paramArrayOfint, paramInt2);
    checkError();
  }
  
  public void glPointParameterxv(int paramInt, IntBuffer paramIntBuffer) {
    begin("glPointParameterxv");
    arg("pname", paramInt);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11.glPointParameterxv(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glPointSizePointerOES(int paramInt1, int paramInt2, Buffer paramBuffer) {
    begin("glPointSizePointerOES");
    arg("type", paramInt1);
    arg("stride", paramInt2);
    arg("params", paramBuffer.toString());
    end();
    this.mgl11.glPointSizePointerOES(paramInt1, paramInt2, paramBuffer);
    checkError();
  }
  
  public void glTexEnvi(int paramInt1, int paramInt2, int paramInt3) {
    begin("glTexEnvi");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("param", paramInt3);
    end();
    this.mgl11.glTexEnvi(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glTexEnviv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glTexEnviv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11.glTexEnviv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glTexEnviv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glTexEnviv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11.glTexEnviv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glTexParameterfv(int paramInt1, int paramInt2, float[] paramArrayOffloat, int paramInt3) {
    begin("glTexParameterfv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOffloat.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11.glTexParameterfv(paramInt1, paramInt2, paramArrayOffloat, paramInt3);
    checkError();
  }
  
  public void glTexParameterfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer) {
    begin("glTexParameterfv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramFloatBuffer.toString());
    end();
    this.mgl11.glTexParameterfv(paramInt1, paramInt2, paramFloatBuffer);
    checkError();
  }
  
  public void glTexParameteri(int paramInt1, int paramInt2, int paramInt3) {
    begin("glTexParameterxv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("param", paramInt3);
    end();
    this.mgl11.glTexParameteri(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glTexParameterxv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glTexParameterxv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11.glTexParameterxv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glTexParameterxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glTexParameterxv");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11.glTexParameterxv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glColorPointer(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glColorPointer");
    arg("size", paramInt1);
    arg("type", paramInt2);
    arg("stride", paramInt3);
    arg("offset", paramInt4);
    end();
    this.mgl11.glColorPointer(paramInt1, paramInt2, paramInt3, paramInt4);
    checkError();
  }
  
  public void glDrawElements(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glDrawElements");
    arg("mode", paramInt1);
    arg("count", paramInt2);
    arg("type", paramInt3);
    arg("offset", paramInt4);
    end();
    this.mgl11.glDrawElements(paramInt1, paramInt2, paramInt3, paramInt4);
    checkError();
  }
  
  public void glGetPointerv(int paramInt, Buffer[] paramArrayOfBuffer) {
    begin("glGetPointerv");
    arg("pname", paramInt);
    arg("params", paramArrayOfBuffer.toString());
    end();
    this.mgl11.glGetPointerv(paramInt, paramArrayOfBuffer);
    checkError();
  }
  
  public void glNormalPointer(int paramInt1, int paramInt2, int paramInt3) {
    begin("glNormalPointer");
    arg("type", paramInt1);
    arg("stride", paramInt2);
    arg("offset", paramInt3);
    end();
    this.mgl11.glNormalPointer(paramInt1, paramInt2, paramInt3);
  }
  
  public void glTexCoordPointer(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glTexCoordPointer");
    arg("size", paramInt1);
    arg("type", paramInt2);
    arg("stride", paramInt3);
    arg("offset", paramInt4);
    end();
    this.mgl11.glTexCoordPointer(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void glVertexPointer(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glVertexPointer");
    arg("size", paramInt1);
    arg("type", paramInt2);
    arg("stride", paramInt3);
    arg("offset", paramInt4);
    end();
    this.mgl11.glVertexPointer(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void glCurrentPaletteMatrixOES(int paramInt) {
    begin("glCurrentPaletteMatrixOES");
    arg("matrixpaletteindex", paramInt);
    end();
    this.mgl11Ext.glCurrentPaletteMatrixOES(paramInt);
    checkError();
  }
  
  public void glLoadPaletteFromModelViewMatrixOES() {
    begin("glLoadPaletteFromModelViewMatrixOES");
    end();
    this.mgl11Ext.glLoadPaletteFromModelViewMatrixOES();
    checkError();
  }
  
  public void glMatrixIndexPointerOES(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer) {
    begin("glMatrixIndexPointerOES");
    argPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
    end();
    this.mgl11Ext.glMatrixIndexPointerOES(paramInt1, paramInt2, paramInt3, paramBuffer);
    checkError();
  }
  
  public void glMatrixIndexPointerOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glMatrixIndexPointerOES");
    arg("size", paramInt1);
    arg("type", paramInt2);
    arg("stride", paramInt3);
    arg("offset", paramInt4);
    end();
    this.mgl11Ext.glMatrixIndexPointerOES(paramInt1, paramInt2, paramInt3, paramInt4);
    checkError();
  }
  
  public void glWeightPointerOES(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer) {
    begin("glWeightPointerOES");
    argPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
    end();
    this.mgl11Ext.glWeightPointerOES(paramInt1, paramInt2, paramInt3, paramBuffer);
    checkError();
  }
  
  public void glWeightPointerOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glWeightPointerOES");
    arg("size", paramInt1);
    arg("type", paramInt2);
    arg("stride", paramInt3);
    arg("offset", paramInt4);
    end();
    this.mgl11Ext.glWeightPointerOES(paramInt1, paramInt2, paramInt3, paramInt4);
    checkError();
  }
  
  public void glBindFramebufferOES(int paramInt1, int paramInt2) {
    begin("glBindFramebufferOES");
    arg("target", paramInt1);
    arg("framebuffer", paramInt2);
    end();
    this.mgl11ExtensionPack.glBindFramebufferOES(paramInt1, paramInt2);
    checkError();
  }
  
  public void glBindRenderbufferOES(int paramInt1, int paramInt2) {
    begin("glBindRenderbufferOES");
    arg("target", paramInt1);
    arg("renderbuffer", paramInt2);
    end();
    this.mgl11ExtensionPack.glBindRenderbufferOES(paramInt1, paramInt2);
    checkError();
  }
  
  public void glBlendEquation(int paramInt) {
    begin("glBlendEquation");
    arg("mode", paramInt);
    end();
    this.mgl11ExtensionPack.glBlendEquation(paramInt);
    checkError();
  }
  
  public void glBlendEquationSeparate(int paramInt1, int paramInt2) {
    begin("glBlendEquationSeparate");
    arg("modeRGB", paramInt1);
    arg("modeAlpha", paramInt2);
    end();
    this.mgl11ExtensionPack.glBlendEquationSeparate(paramInt1, paramInt2);
    checkError();
  }
  
  public void glBlendFuncSeparate(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glBlendFuncSeparate");
    arg("srcRGB", paramInt1);
    arg("dstRGB", paramInt2);
    arg("srcAlpha", paramInt3);
    arg("dstAlpha", paramInt4);
    end();
    this.mgl11ExtensionPack.glBlendFuncSeparate(paramInt1, paramInt2, paramInt3, paramInt4);
    checkError();
  }
  
  public int glCheckFramebufferStatusOES(int paramInt) {
    begin("glCheckFramebufferStatusOES");
    arg("target", paramInt);
    end();
    paramInt = this.mgl11ExtensionPack.glCheckFramebufferStatusOES(paramInt);
    checkError();
    return paramInt;
  }
  
  public void glDeleteFramebuffersOES(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glDeleteFramebuffersOES");
    arg("n", paramInt1);
    arg("framebuffers", paramArrayOfint.toString());
    arg("offset", paramInt2);
    end();
    this.mgl11ExtensionPack.glDeleteFramebuffersOES(paramInt1, paramArrayOfint, paramInt2);
    checkError();
  }
  
  public void glDeleteFramebuffersOES(int paramInt, IntBuffer paramIntBuffer) {
    begin("glDeleteFramebuffersOES");
    arg("n", paramInt);
    arg("framebuffers", paramIntBuffer.toString());
    end();
    this.mgl11ExtensionPack.glDeleteFramebuffersOES(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glDeleteRenderbuffersOES(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glDeleteRenderbuffersOES");
    arg("n", paramInt1);
    arg("renderbuffers", paramArrayOfint.toString());
    arg("offset", paramInt2);
    end();
    this.mgl11ExtensionPack.glDeleteRenderbuffersOES(paramInt1, paramArrayOfint, paramInt2);
    checkError();
  }
  
  public void glDeleteRenderbuffersOES(int paramInt, IntBuffer paramIntBuffer) {
    begin("glDeleteRenderbuffersOES");
    arg("n", paramInt);
    arg("renderbuffers", paramIntBuffer.toString());
    end();
    this.mgl11ExtensionPack.glDeleteRenderbuffersOES(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glFramebufferRenderbufferOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glFramebufferRenderbufferOES");
    arg("target", paramInt1);
    arg("attachment", paramInt2);
    arg("renderbuffertarget", paramInt3);
    arg("renderbuffer", paramInt4);
    end();
    this.mgl11ExtensionPack.glFramebufferRenderbufferOES(paramInt1, paramInt2, paramInt3, paramInt4);
    checkError();
  }
  
  public void glFramebufferTexture2DOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    begin("glFramebufferTexture2DOES");
    arg("target", paramInt1);
    arg("attachment", paramInt2);
    arg("textarget", paramInt3);
    arg("texture", paramInt4);
    arg("level", paramInt5);
    end();
    this.mgl11ExtensionPack.glFramebufferTexture2DOES(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    checkError();
  }
  
  public void glGenerateMipmapOES(int paramInt) {
    begin("glGenerateMipmapOES");
    arg("target", paramInt);
    end();
    this.mgl11ExtensionPack.glGenerateMipmapOES(paramInt);
    checkError();
  }
  
  public void glGenFramebuffersOES(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glGenFramebuffersOES");
    arg("n", paramInt1);
    arg("framebuffers", paramArrayOfint.toString());
    arg("offset", paramInt2);
    end();
    this.mgl11ExtensionPack.glGenFramebuffersOES(paramInt1, paramArrayOfint, paramInt2);
    checkError();
  }
  
  public void glGenFramebuffersOES(int paramInt, IntBuffer paramIntBuffer) {
    begin("glGenFramebuffersOES");
    arg("n", paramInt);
    arg("framebuffers", paramIntBuffer.toString());
    end();
    this.mgl11ExtensionPack.glGenFramebuffersOES(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glGenRenderbuffersOES(int paramInt1, int[] paramArrayOfint, int paramInt2) {
    begin("glGenRenderbuffersOES");
    arg("n", paramInt1);
    arg("renderbuffers", paramArrayOfint.toString());
    arg("offset", paramInt2);
    end();
    this.mgl11ExtensionPack.glGenRenderbuffersOES(paramInt1, paramArrayOfint, paramInt2);
    checkError();
  }
  
  public void glGenRenderbuffersOES(int paramInt, IntBuffer paramIntBuffer) {
    begin("glGenRenderbuffersOES");
    arg("n", paramInt);
    arg("renderbuffers", paramIntBuffer.toString());
    end();
    this.mgl11ExtensionPack.glGenRenderbuffersOES(paramInt, paramIntBuffer);
    checkError();
  }
  
  public void glGetFramebufferAttachmentParameterivOES(int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfint, int paramInt4) {
    begin("glGetFramebufferAttachmentParameterivOES");
    arg("target", paramInt1);
    arg("attachment", paramInt2);
    arg("pname", paramInt3);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt4);
    end();
    this.mgl11ExtensionPack.glGetFramebufferAttachmentParameterivOES(paramInt1, paramInt2, paramInt3, paramArrayOfint, paramInt4);
    checkError();
  }
  
  public void glGetFramebufferAttachmentParameterivOES(int paramInt1, int paramInt2, int paramInt3, IntBuffer paramIntBuffer) {
    begin("glGetFramebufferAttachmentParameterivOES");
    arg("target", paramInt1);
    arg("attachment", paramInt2);
    arg("pname", paramInt3);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11ExtensionPack.glGetFramebufferAttachmentParameterivOES(paramInt1, paramInt2, paramInt3, paramIntBuffer);
    checkError();
  }
  
  public void glGetRenderbufferParameterivOES(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glGetRenderbufferParameterivOES");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11ExtensionPack.glGetRenderbufferParameterivOES(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glGetRenderbufferParameterivOES(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glGetRenderbufferParameterivOES");
    arg("target", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11ExtensionPack.glGetRenderbufferParameterivOES(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glGetTexGenfv(int paramInt1, int paramInt2, float[] paramArrayOffloat, int paramInt3) {
    begin("glGetTexGenfv");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOffloat.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11ExtensionPack.glGetTexGenfv(paramInt1, paramInt2, paramArrayOffloat, paramInt3);
    checkError();
  }
  
  public void glGetTexGenfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer) {
    begin("glGetTexGenfv");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramFloatBuffer.toString());
    end();
    this.mgl11ExtensionPack.glGetTexGenfv(paramInt1, paramInt2, paramFloatBuffer);
    checkError();
  }
  
  public void glGetTexGeniv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glGetTexGeniv");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11ExtensionPack.glGetTexGeniv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glGetTexGeniv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glGetTexGeniv");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11ExtensionPack.glGetTexGeniv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glGetTexGenxv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glGetTexGenxv");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11ExtensionPack.glGetTexGenxv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glGetTexGenxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glGetTexGenxv");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11ExtensionPack.glGetTexGenxv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public boolean glIsFramebufferOES(int paramInt) {
    begin("glIsFramebufferOES");
    arg("framebuffer", paramInt);
    end();
    boolean bool = this.mgl11ExtensionPack.glIsFramebufferOES(paramInt);
    checkError();
    return bool;
  }
  
  public boolean glIsRenderbufferOES(int paramInt) {
    begin("glIsRenderbufferOES");
    arg("renderbuffer", paramInt);
    end();
    this.mgl11ExtensionPack.glIsRenderbufferOES(paramInt);
    checkError();
    return false;
  }
  
  public void glRenderbufferStorageOES(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    begin("glRenderbufferStorageOES");
    arg("target", paramInt1);
    arg("internalformat", paramInt2);
    arg("width", paramInt3);
    arg("height", paramInt4);
    end();
    this.mgl11ExtensionPack.glRenderbufferStorageOES(paramInt1, paramInt2, paramInt3, paramInt4);
    checkError();
  }
  
  public void glTexGenf(int paramInt1, int paramInt2, float paramFloat) {
    begin("glTexGenf");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("param", paramFloat);
    end();
    this.mgl11ExtensionPack.glTexGenf(paramInt1, paramInt2, paramFloat);
    checkError();
  }
  
  public void glTexGenfv(int paramInt1, int paramInt2, float[] paramArrayOffloat, int paramInt3) {
    begin("glTexGenfv");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOffloat.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11ExtensionPack.glTexGenfv(paramInt1, paramInt2, paramArrayOffloat, paramInt3);
    checkError();
  }
  
  public void glTexGenfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer) {
    begin("glTexGenfv");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramFloatBuffer.toString());
    end();
    this.mgl11ExtensionPack.glTexGenfv(paramInt1, paramInt2, paramFloatBuffer);
    checkError();
  }
  
  public void glTexGeni(int paramInt1, int paramInt2, int paramInt3) {
    begin("glTexGeni");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("param", paramInt3);
    end();
    this.mgl11ExtensionPack.glTexGeni(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glTexGeniv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glTexGeniv");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11ExtensionPack.glTexGeniv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glTexGeniv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glTexGeniv");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11ExtensionPack.glTexGeniv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  public void glTexGenx(int paramInt1, int paramInt2, int paramInt3) {
    begin("glTexGenx");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("param", paramInt3);
    end();
    this.mgl11ExtensionPack.glTexGenx(paramInt1, paramInt2, paramInt3);
    checkError();
  }
  
  public void glTexGenxv(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    begin("glTexGenxv");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramArrayOfint.toString());
    arg("offset", paramInt3);
    end();
    this.mgl11ExtensionPack.glTexGenxv(paramInt1, paramInt2, paramArrayOfint, paramInt3);
    checkError();
  }
  
  public void glTexGenxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer) {
    begin("glTexGenxv");
    arg("coord", paramInt1);
    arg("pname", paramInt2);
    arg("params", paramIntBuffer.toString());
    end();
    this.mgl11ExtensionPack.glTexGenxv(paramInt1, paramInt2, paramIntBuffer);
    checkError();
  }
  
  class PointerInfo {
    public Buffer mPointer;
    
    public int mSize;
    
    public int mStride;
    
    public ByteBuffer mTempByteBuffer;
    
    public int mType;
    
    final GLLogWrapper this$0;
    
    public PointerInfo() {}
    
    public PointerInfo(int param1Int1, int param1Int2, int param1Int3, Buffer param1Buffer) {
      this.mSize = param1Int1;
      this.mType = param1Int2;
      this.mStride = param1Int3;
      this.mPointer = param1Buffer;
    }
    
    public int sizeof(int param1Int) {
      if (param1Int != 5126) {
        if (param1Int != 5132) {
          switch (param1Int) {
            default:
              return 0;
            case 5122:
              return 2;
            case 5121:
              return 1;
            case 5120:
              break;
          } 
          return 1;
        } 
        return 4;
      } 
      return 4;
    }
    
    public int getStride() {
      int i = this.mStride;
      if (i <= 0)
        i = sizeof(this.mType) * this.mSize; 
      return i;
    }
    
    public void bindByteBuffer() {
      Buffer buffer = this.mPointer;
      if (buffer == null) {
        buffer = null;
      } else {
        buffer = GLLogWrapper.this.toByteBuffer(-1, buffer);
      } 
      this.mTempByteBuffer = (ByteBuffer)buffer;
    }
    
    public void unbindByteBuffer() {
      this.mTempByteBuffer = null;
    }
  }
}
