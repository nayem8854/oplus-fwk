package android.os;

import java.util.ArrayList;
import java.util.Iterator;

public class PerformanceCollector {
  public static final String METRIC_KEY_CPU_TIME = "cpu_time";
  
  public static final String METRIC_KEY_EXECUTION_TIME = "execution_time";
  
  public static final String METRIC_KEY_GC_INVOCATION_COUNT = "gc_invocation_count";
  
  public static final String METRIC_KEY_GLOBAL_ALLOC_COUNT = "global_alloc_count";
  
  public static final String METRIC_KEY_GLOBAL_ALLOC_SIZE = "global_alloc_size";
  
  public static final String METRIC_KEY_GLOBAL_FREED_COUNT = "global_freed_count";
  
  public static final String METRIC_KEY_GLOBAL_FREED_SIZE = "global_freed_size";
  
  public static final String METRIC_KEY_ITERATIONS = "iterations";
  
  public static final String METRIC_KEY_JAVA_ALLOCATED = "java_allocated";
  
  public static final String METRIC_KEY_JAVA_FREE = "java_free";
  
  public static final String METRIC_KEY_JAVA_PRIVATE_DIRTY = "java_private_dirty";
  
  public static final String METRIC_KEY_JAVA_PSS = "java_pss";
  
  public static final String METRIC_KEY_JAVA_SHARED_DIRTY = "java_shared_dirty";
  
  public static final String METRIC_KEY_JAVA_SIZE = "java_size";
  
  public static final String METRIC_KEY_LABEL = "label";
  
  public static final String METRIC_KEY_NATIVE_ALLOCATED = "native_allocated";
  
  public static final String METRIC_KEY_NATIVE_FREE = "native_free";
  
  public static final String METRIC_KEY_NATIVE_PRIVATE_DIRTY = "native_private_dirty";
  
  public static final String METRIC_KEY_NATIVE_PSS = "native_pss";
  
  public static final String METRIC_KEY_NATIVE_SHARED_DIRTY = "native_shared_dirty";
  
  public static final String METRIC_KEY_NATIVE_SIZE = "native_size";
  
  public static final String METRIC_KEY_OTHER_PRIVATE_DIRTY = "other_private_dirty";
  
  public static final String METRIC_KEY_OTHER_PSS = "other_pss";
  
  public static final String METRIC_KEY_OTHER_SHARED_DIRTY = "other_shared_dirty";
  
  public static final String METRIC_KEY_PRE_RECEIVED_TRANSACTIONS = "pre_received_transactions";
  
  public static final String METRIC_KEY_PRE_SENT_TRANSACTIONS = "pre_sent_transactions";
  
  public static final String METRIC_KEY_RECEIVED_TRANSACTIONS = "received_transactions";
  
  public static final String METRIC_KEY_SENT_TRANSACTIONS = "sent_transactions";
  
  private long mCpuTime;
  
  private long mExecTime;
  
  private Bundle mPerfMeasurement;
  
  private Bundle mPerfSnapshot;
  
  private PerformanceResultsWriter mPerfWriter;
  
  private long mSnapshotCpuTime;
  
  private long mSnapshotExecTime;
  
  public PerformanceCollector() {}
  
  public PerformanceCollector(PerformanceResultsWriter paramPerformanceResultsWriter) {
    setPerformanceResultsWriter(paramPerformanceResultsWriter);
  }
  
  public void setPerformanceResultsWriter(PerformanceResultsWriter paramPerformanceResultsWriter) {
    this.mPerfWriter = paramPerformanceResultsWriter;
  }
  
  public void beginSnapshot(String paramString) {
    PerformanceResultsWriter performanceResultsWriter = this.mPerfWriter;
    if (performanceResultsWriter != null)
      performanceResultsWriter.writeBeginSnapshot(paramString); 
    startPerformanceSnapshot();
  }
  
  public Bundle endSnapshot() {
    endPerformanceSnapshot();
    PerformanceResultsWriter performanceResultsWriter = this.mPerfWriter;
    if (performanceResultsWriter != null)
      performanceResultsWriter.writeEndSnapshot(this.mPerfSnapshot); 
    return this.mPerfSnapshot;
  }
  
  public void startTiming(String paramString) {
    PerformanceResultsWriter performanceResultsWriter = this.mPerfWriter;
    if (performanceResultsWriter != null)
      performanceResultsWriter.writeStartTiming(paramString); 
    Bundle bundle = new Bundle();
    bundle.putParcelableArrayList("iterations", new ArrayList<>());
    this.mExecTime = SystemClock.uptimeMillis();
    this.mCpuTime = Process.getElapsedCpuTime();
  }
  
  public Bundle addIteration(String paramString) {
    this.mCpuTime = Process.getElapsedCpuTime() - this.mCpuTime;
    this.mExecTime = SystemClock.uptimeMillis() - this.mExecTime;
    Bundle bundle = new Bundle();
    bundle.putString("label", paramString);
    bundle.putLong("execution_time", this.mExecTime);
    bundle.putLong("cpu_time", this.mCpuTime);
    this.mPerfMeasurement.<Bundle>getParcelableArrayList("iterations").add(bundle);
    this.mExecTime = SystemClock.uptimeMillis();
    this.mCpuTime = Process.getElapsedCpuTime();
    return bundle;
  }
  
  public Bundle stopTiming(String paramString) {
    addIteration(paramString);
    PerformanceResultsWriter performanceResultsWriter = this.mPerfWriter;
    if (performanceResultsWriter != null)
      performanceResultsWriter.writeStopTiming(this.mPerfMeasurement); 
    return this.mPerfMeasurement;
  }
  
  public void addMeasurement(String paramString, long paramLong) {
    PerformanceResultsWriter performanceResultsWriter = this.mPerfWriter;
    if (performanceResultsWriter != null)
      performanceResultsWriter.writeMeasurement(paramString, paramLong); 
  }
  
  public void addMeasurement(String paramString, float paramFloat) {
    PerformanceResultsWriter performanceResultsWriter = this.mPerfWriter;
    if (performanceResultsWriter != null)
      performanceResultsWriter.writeMeasurement(paramString, paramFloat); 
  }
  
  public void addMeasurement(String paramString1, String paramString2) {
    PerformanceResultsWriter performanceResultsWriter = this.mPerfWriter;
    if (performanceResultsWriter != null)
      performanceResultsWriter.writeMeasurement(paramString1, paramString2); 
  }
  
  private void startPerformanceSnapshot() {
    this.mPerfSnapshot = new Bundle();
    Bundle bundle = getBinderCounts();
    for (String str : bundle.keySet()) {
      Bundle bundle1 = this.mPerfSnapshot;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("pre_");
      stringBuilder.append(str);
      bundle1.putLong(stringBuilder.toString(), bundle.getLong(str));
    } 
    startAllocCounting();
    this.mSnapshotExecTime = SystemClock.uptimeMillis();
    this.mSnapshotCpuTime = Process.getElapsedCpuTime();
  }
  
  private void endPerformanceSnapshot() {
    this.mSnapshotCpuTime = Process.getElapsedCpuTime() - this.mSnapshotCpuTime;
    this.mSnapshotExecTime = SystemClock.uptimeMillis() - this.mSnapshotExecTime;
    stopAllocCounting();
    long l1 = Debug.getNativeHeapSize() / 1024L;
    long l2 = Debug.getNativeHeapAllocatedSize() / 1024L;
    long l3 = Debug.getNativeHeapFreeSize() / 1024L;
    Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
    Debug.getMemoryInfo(memoryInfo);
    Runtime runtime = Runtime.getRuntime();
    long l4 = runtime.totalMemory() / 1024L;
    long l5 = runtime.freeMemory() / 1024L;
    long l6 = l4 - l5;
    Bundle bundle2 = getBinderCounts();
    for (String str : bundle2.keySet())
      this.mPerfSnapshot.putLong(str, bundle2.getLong(str)); 
    Bundle bundle3 = getAllocCounts();
    Bundle bundle1;
    Iterator<String> iterator;
    for (iterator = bundle3.keySet().iterator(), bundle1 = bundle2; iterator.hasNext(); ) {
      String str = iterator.next();
      this.mPerfSnapshot.putLong(str, bundle3.getLong(str));
    } 
    this.mPerfSnapshot.putLong("execution_time", this.mSnapshotExecTime);
    this.mPerfSnapshot.putLong("cpu_time", this.mSnapshotCpuTime);
    this.mPerfSnapshot.putLong("native_size", l1);
    this.mPerfSnapshot.putLong("native_allocated", l2);
    this.mPerfSnapshot.putLong("native_free", l3);
    this.mPerfSnapshot.putLong("native_pss", memoryInfo.nativePss);
    this.mPerfSnapshot.putLong("native_private_dirty", memoryInfo.nativePrivateDirty);
    this.mPerfSnapshot.putLong("native_shared_dirty", memoryInfo.nativeSharedDirty);
    this.mPerfSnapshot.putLong("java_size", l4);
    this.mPerfSnapshot.putLong("java_allocated", l6);
    this.mPerfSnapshot.putLong("java_free", l5);
    this.mPerfSnapshot.putLong("java_pss", memoryInfo.dalvikPss);
    this.mPerfSnapshot.putLong("java_private_dirty", memoryInfo.dalvikPrivateDirty);
    this.mPerfSnapshot.putLong("java_shared_dirty", memoryInfo.dalvikSharedDirty);
    this.mPerfSnapshot.putLong("other_pss", memoryInfo.otherPss);
    this.mPerfSnapshot.putLong("other_private_dirty", memoryInfo.otherPrivateDirty);
    this.mPerfSnapshot.putLong("other_shared_dirty", memoryInfo.otherSharedDirty);
  }
  
  private static void startAllocCounting() {
    Runtime.getRuntime().gc();
    Runtime.getRuntime().runFinalization();
    Runtime.getRuntime().gc();
    Debug.resetAllCounts();
    Debug.startAllocCounting();
  }
  
  private static void stopAllocCounting() {
    Runtime.getRuntime().gc();
    Runtime.getRuntime().runFinalization();
    Runtime.getRuntime().gc();
    Debug.stopAllocCounting();
  }
  
  private static Bundle getAllocCounts() {
    Bundle bundle = new Bundle();
    bundle.putLong("global_alloc_count", Debug.getGlobalAllocCount());
    bundle.putLong("global_alloc_size", Debug.getGlobalAllocSize());
    bundle.putLong("global_freed_count", Debug.getGlobalFreedCount());
    bundle.putLong("global_freed_size", Debug.getGlobalFreedSize());
    bundle.putLong("gc_invocation_count", Debug.getGlobalGcInvocationCount());
    return bundle;
  }
  
  private static Bundle getBinderCounts() {
    Bundle bundle = new Bundle();
    bundle.putLong("sent_transactions", Debug.getBinderSentTransactions());
    bundle.putLong("received_transactions", Debug.getBinderReceivedTransactions());
    return bundle;
  }
  
  public static interface PerformanceResultsWriter {
    void writeBeginSnapshot(String param1String);
    
    void writeEndSnapshot(Bundle param1Bundle);
    
    void writeMeasurement(String param1String, float param1Float);
    
    void writeMeasurement(String param1String, long param1Long);
    
    void writeMeasurement(String param1String1, String param1String2);
    
    void writeStartTiming(String param1String);
    
    void writeStopTiming(Bundle param1Bundle);
  }
}
