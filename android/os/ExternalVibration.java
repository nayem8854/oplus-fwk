package android.os;

import android.media.AudioAttributes;
import android.util.Slog;
import com.android.internal.util.Preconditions;

public class ExternalVibration implements Parcelable {
  public ExternalVibration(int paramInt, String paramString, AudioAttributes paramAudioAttributes, IExternalVibrationController paramIExternalVibrationController) {
    this.mUid = paramInt;
    this.mPkg = (String)Preconditions.checkNotNull(paramString);
    this.mAttrs = (AudioAttributes)Preconditions.checkNotNull(paramAudioAttributes);
    this.mController = (IExternalVibrationController)Preconditions.checkNotNull(paramIExternalVibrationController);
    this.mToken = new Binder();
  }
  
  private ExternalVibration(Parcel paramParcel) {
    this.mUid = paramParcel.readInt();
    this.mPkg = paramParcel.readString();
    this.mAttrs = readAudioAttributes(paramParcel);
    this.mController = IExternalVibrationController.Stub.asInterface(paramParcel.readStrongBinder());
    this.mToken = paramParcel.readStrongBinder();
  }
  
  private AudioAttributes readAudioAttributes(Parcel paramParcel) {
    int i = paramParcel.readInt();
    int j = paramParcel.readInt();
    int k = paramParcel.readInt();
    int m = paramParcel.readInt();
    AudioAttributes.Builder builder = new AudioAttributes.Builder();
    builder = builder.setUsage(i);
    builder = builder.setContentType(j);
    builder = builder.setCapturePreset(k);
    builder = builder.setFlags(m);
    return builder.build();
  }
  
  public int getUid() {
    return this.mUid;
  }
  
  public String getPackage() {
    return this.mPkg;
  }
  
  public AudioAttributes getAudioAttributes() {
    return this.mAttrs;
  }
  
  public VibrationAttributes getVibrationAttributes() {
    return (new VibrationAttributes.Builder(this.mAttrs, null)).build();
  }
  
  public boolean mute() {
    try {
      this.mController.mute();
      return true;
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to mute vibration stream: ");
      stringBuilder.append(this);
      Slog.wtf("ExternalVibration", stringBuilder.toString(), (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean unmute() {
    try {
      this.mController.unmute();
      return true;
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to unmute vibration stream: ");
      stringBuilder.append(this);
      Slog.wtf("ExternalVibration", stringBuilder.toString(), (Throwable)remoteException);
      return false;
    } 
  }
  
  public void linkToDeath(IBinder.DeathRecipient paramDeathRecipient) {
    try {
      this.mToken.linkToDeath(paramDeathRecipient, 0);
      return;
    } catch (RemoteException remoteException) {
      return;
    } 
  }
  
  public void unlinkToDeath(IBinder.DeathRecipient paramDeathRecipient) {
    this.mToken.unlinkToDeath(paramDeathRecipient, 0);
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == null || !(paramObject instanceof ExternalVibration))
      return false; 
    paramObject = paramObject;
    return this.mToken.equals(((ExternalVibration)paramObject).mToken);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ExternalVibration{uid=");
    stringBuilder.append(this.mUid);
    stringBuilder.append(", pkg=");
    stringBuilder.append(this.mPkg);
    stringBuilder.append(", attrs=");
    stringBuilder.append(this.mAttrs);
    stringBuilder.append(", controller=");
    stringBuilder.append(this.mController);
    stringBuilder.append("token=");
    stringBuilder.append(this.mController);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mUid);
    paramParcel.writeString(this.mPkg);
    writeAudioAttributes(this.mAttrs, paramParcel, paramInt);
    paramParcel.writeStrongBinder(this.mController.asBinder());
    paramParcel.writeStrongBinder(this.mToken);
  }
  
  private static void writeAudioAttributes(AudioAttributes paramAudioAttributes, Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(paramAudioAttributes.getUsage());
    paramParcel.writeInt(paramAudioAttributes.getContentType());
    paramParcel.writeInt(paramAudioAttributes.getCapturePreset());
    paramParcel.writeInt(paramAudioAttributes.getAllFlags());
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<ExternalVibration> CREATOR = new Parcelable.Creator<ExternalVibration>() {
      public ExternalVibration createFromParcel(Parcel param1Parcel) {
        return new ExternalVibration(param1Parcel);
      }
      
      public ExternalVibration[] newArray(int param1Int) {
        return new ExternalVibration[param1Int];
      }
    };
  
  private static final String TAG = "ExternalVibration";
  
  private AudioAttributes mAttrs;
  
  private IExternalVibrationController mController;
  
  private String mPkg;
  
  private IBinder mToken;
  
  private int mUid;
}
