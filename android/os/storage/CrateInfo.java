package android.os.storage;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.UserHandle;
import android.text.TextUtils;
import com.android.internal.util.Preconditions;

public final class CrateInfo implements Parcelable {
  private CrateInfo() {
    this.mExpiration = 0L;
  }
  
  public CrateInfo(CharSequence paramCharSequence, long paramLong) {
    Preconditions.checkStringNotEmpty(paramCharSequence, "Label should not be either null or empty string");
    Preconditions.checkArgumentNonnegative(paramLong, "Expiration should be non negative number");
    this.mLabel = paramCharSequence;
    this.mExpiration = paramLong;
  }
  
  public CrateInfo(CharSequence paramCharSequence) {
    this(paramCharSequence, 0L);
  }
  
  public CharSequence getLabel() {
    if (TextUtils.isEmpty(this.mLabel))
      return this.mId; 
    return this.mLabel;
  }
  
  public long getExpirationMillis() {
    return this.mExpiration;
  }
  
  public void setExpiration(long paramLong) {
    Preconditions.checkArgumentNonnegative(paramLong);
    this.mExpiration = paramLong;
  }
  
  public int hashCode() {
    return super.hashCode();
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == null)
      return false; 
    if (paramObject instanceof CrateInfo) {
      CrateInfo crateInfo = (CrateInfo)paramObject;
      if (!TextUtils.isEmpty(this.mId)) {
        String str2 = this.mId, str1 = crateInfo.mId;
        if (TextUtils.equals(str2, str1))
          return true; 
      } 
    } 
    return super.equals(paramObject);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (paramParcel == null)
      return; 
    paramParcel.writeCharSequence(this.mLabel);
    paramParcel.writeLong(this.mExpiration);
    paramParcel.writeInt(this.mUid);
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeString(this.mId);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    if (paramParcel == null)
      return; 
    this.mLabel = paramParcel.readCharSequence();
    this.mExpiration = paramParcel.readLong();
    this.mUid = paramParcel.readInt();
    this.mPackageName = paramParcel.readString();
    this.mId = paramParcel.readString();
  }
  
  public static final Parcelable.Creator<CrateInfo> CREATOR = new Parcelable.Creator<CrateInfo>() {
      public CrateInfo createFromParcel(Parcel param1Parcel) {
        CrateInfo crateInfo = new CrateInfo();
        crateInfo.readFromParcel(param1Parcel);
        return crateInfo;
      }
      
      public CrateInfo[] newArray(int param1Int) {
        return new CrateInfo[param1Int];
      }
    };
  
  private static final String TAG = "CrateInfo";
  
  private long mExpiration;
  
  private String mId;
  
  private CharSequence mLabel;
  
  private String mPackageName;
  
  private int mUid;
  
  public static CrateInfo copyFrom(int paramInt, String paramString1, String paramString2) {
    if (!UserHandle.isApp(paramInt) || TextUtils.isEmpty(paramString1) || TextUtils.isEmpty(paramString2))
      return null; 
    CrateInfo crateInfo = new CrateInfo(paramString2, 0L);
    crateInfo.mUid = paramInt;
    crateInfo.mPackageName = paramString1;
    crateInfo.mId = paramString2;
    return crateInfo;
  }
}
