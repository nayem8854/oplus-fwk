package android.os.storage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IStorageEventListener extends IInterface {
  void onDiskDestroyed(DiskInfo paramDiskInfo) throws RemoteException;
  
  void onDiskScanned(DiskInfo paramDiskInfo, int paramInt) throws RemoteException;
  
  void onStorageStateChanged(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  void onUsbMassStorageConnectionChanged(boolean paramBoolean) throws RemoteException;
  
  void onVolumeForgotten(String paramString) throws RemoteException;
  
  void onVolumeRecordChanged(VolumeRecord paramVolumeRecord) throws RemoteException;
  
  void onVolumeStateChanged(VolumeInfo paramVolumeInfo, int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IStorageEventListener {
    public void onUsbMassStorageConnectionChanged(boolean param1Boolean) throws RemoteException {}
    
    public void onStorageStateChanged(String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public void onVolumeStateChanged(VolumeInfo param1VolumeInfo, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onVolumeRecordChanged(VolumeRecord param1VolumeRecord) throws RemoteException {}
    
    public void onVolumeForgotten(String param1String) throws RemoteException {}
    
    public void onDiskScanned(DiskInfo param1DiskInfo, int param1Int) throws RemoteException {}
    
    public void onDiskDestroyed(DiskInfo param1DiskInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IStorageEventListener {
    private static final String DESCRIPTOR = "android.os.storage.IStorageEventListener";
    
    static final int TRANSACTION_onDiskDestroyed = 7;
    
    static final int TRANSACTION_onDiskScanned = 6;
    
    static final int TRANSACTION_onStorageStateChanged = 2;
    
    static final int TRANSACTION_onUsbMassStorageConnectionChanged = 1;
    
    static final int TRANSACTION_onVolumeForgotten = 5;
    
    static final int TRANSACTION_onVolumeRecordChanged = 4;
    
    static final int TRANSACTION_onVolumeStateChanged = 3;
    
    public Stub() {
      attachInterface(this, "android.os.storage.IStorageEventListener");
    }
    
    public static IStorageEventListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.storage.IStorageEventListener");
      if (iInterface != null && iInterface instanceof IStorageEventListener)
        return (IStorageEventListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "onDiskDestroyed";
        case 6:
          return "onDiskScanned";
        case 5:
          return "onVolumeForgotten";
        case 4:
          return "onVolumeRecordChanged";
        case 3:
          return "onVolumeStateChanged";
        case 2:
          return "onStorageStateChanged";
        case 1:
          break;
      } 
      return "onUsbMassStorageConnectionChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str1, str2;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.os.storage.IStorageEventListener");
            if (param1Parcel1.readInt() != 0) {
              DiskInfo diskInfo = DiskInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onDiskDestroyed((DiskInfo)param1Parcel1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.os.storage.IStorageEventListener");
            if (param1Parcel1.readInt() != 0) {
              DiskInfo diskInfo = DiskInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            onDiskScanned((DiskInfo)param1Parcel2, param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.os.storage.IStorageEventListener");
            str1 = param1Parcel1.readString();
            onVolumeForgotten(str1);
            return true;
          case 4:
            str1.enforceInterface("android.os.storage.IStorageEventListener");
            if (str1.readInt() != 0) {
              VolumeRecord volumeRecord = VolumeRecord.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            onVolumeRecordChanged((VolumeRecord)str1);
            return true;
          case 3:
            str1.enforceInterface("android.os.storage.IStorageEventListener");
            if (str1.readInt() != 0) {
              VolumeInfo volumeInfo = VolumeInfo.CREATOR.createFromParcel((Parcel)str1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            onVolumeStateChanged((VolumeInfo)param1Parcel2, param1Int1, param1Int2);
            return true;
          case 2:
            str1.enforceInterface("android.os.storage.IStorageEventListener");
            str2 = str1.readString();
            str = str1.readString();
            str1 = str1.readString();
            onStorageStateChanged(str2, str, str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.os.storage.IStorageEventListener");
        if (str1.readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        onUsbMassStorageConnectionChanged(bool);
        return true;
      } 
      str.writeString("android.os.storage.IStorageEventListener");
      return true;
    }
    
    private static class Proxy implements IStorageEventListener {
      public static IStorageEventListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.storage.IStorageEventListener";
      }
      
      public void onUsbMassStorageConnectionChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.os.storage.IStorageEventListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && IStorageEventListener.Stub.getDefaultImpl() != null) {
            IStorageEventListener.Stub.getDefaultImpl().onUsbMassStorageConnectionChanged(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStorageStateChanged(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.storage.IStorageEventListener");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IStorageEventListener.Stub.getDefaultImpl() != null) {
            IStorageEventListener.Stub.getDefaultImpl().onStorageStateChanged(param2String1, param2String2, param2String3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVolumeStateChanged(VolumeInfo param2VolumeInfo, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.storage.IStorageEventListener");
          if (param2VolumeInfo != null) {
            parcel.writeInt(1);
            param2VolumeInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IStorageEventListener.Stub.getDefaultImpl() != null) {
            IStorageEventListener.Stub.getDefaultImpl().onVolumeStateChanged(param2VolumeInfo, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVolumeRecordChanged(VolumeRecord param2VolumeRecord) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.storage.IStorageEventListener");
          if (param2VolumeRecord != null) {
            parcel.writeInt(1);
            param2VolumeRecord.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IStorageEventListener.Stub.getDefaultImpl() != null) {
            IStorageEventListener.Stub.getDefaultImpl().onVolumeRecordChanged(param2VolumeRecord);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVolumeForgotten(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.storage.IStorageEventListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IStorageEventListener.Stub.getDefaultImpl() != null) {
            IStorageEventListener.Stub.getDefaultImpl().onVolumeForgotten(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDiskScanned(DiskInfo param2DiskInfo, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.storage.IStorageEventListener");
          if (param2DiskInfo != null) {
            parcel.writeInt(1);
            param2DiskInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IStorageEventListener.Stub.getDefaultImpl() != null) {
            IStorageEventListener.Stub.getDefaultImpl().onDiskScanned(param2DiskInfo, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDiskDestroyed(DiskInfo param2DiskInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.storage.IStorageEventListener");
          if (param2DiskInfo != null) {
            parcel.writeInt(1);
            param2DiskInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IStorageEventListener.Stub.getDefaultImpl() != null) {
            IStorageEventListener.Stub.getDefaultImpl().onDiskDestroyed(param2DiskInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IStorageEventListener param1IStorageEventListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IStorageEventListener != null) {
          Proxy.sDefaultImpl = param1IStorageEventListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IStorageEventListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
