package android.os.storage;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.UserHandle;
import android.provider.DocumentsContract;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.DebugUtils;
import android.util.SparseArray;
import android.util.SparseIntArray;
import com.android.internal.util.IndentingPrintWriter;
import com.android.internal.util.Preconditions;
import java.io.CharArrayWriter;
import java.io.File;
import java.util.Comparator;
import java.util.Locale;
import java.util.Objects;

public class VolumeInfo extends OplusBaseVolumeInfo {
  public static final String ACTION_VOLUME_STATE_CHANGED = "android.os.storage.action.VOLUME_STATE_CHANGED";
  
  public static final Parcelable.Creator<VolumeInfo> CREATOR;
  
  private static final String DOCUMENT_AUTHORITY = "com.android.externalstorage.documents";
  
  private static final String DOCUMENT_ROOT_PRIMARY_EMULATED = "primary";
  
  public static final String EXTRA_VOLUME_ID = "android.os.storage.extra.VOLUME_ID";
  
  public static final String EXTRA_VOLUME_STATE = "android.os.storage.extra.VOLUME_STATE";
  
  public static final String ID_EMULATED_INTERNAL = "emulated";
  
  public static final String ID_PRIVATE_INTERNAL = "private";
  
  public static final int MOUNT_FLAG_PRIMARY = 1;
  
  public static final int MOUNT_FLAG_VISIBLE = 2;
  
  public static final int STATE_BAD_REMOVAL = 8;
  
  public static final int STATE_CHECKING = 1;
  
  public static final int STATE_EJECTING = 5;
  
  public static final int STATE_FORMATTING = 4;
  
  public static final int STATE_MOUNTED = 2;
  
  public static final int STATE_MOUNTED_READ_ONLY = 3;
  
  public static final int STATE_REMOVED = 7;
  
  public static final int STATE_UNMOUNTABLE = 6;
  
  public static final int STATE_UNMOUNTED = 0;
  
  public static final int TYPE_ASEC = 3;
  
  public static final int TYPE_EMULATED = 2;
  
  public static final int TYPE_OBB = 4;
  
  public static final int TYPE_PRIVATE = 1;
  
  public static final int TYPE_PUBLIC = 0;
  
  public static final int TYPE_STUB = 5;
  
  private static final Comparator<VolumeInfo> sDescriptionComparator;
  
  private static ArrayMap<String, String> sEnvironmentToBroadcast;
  
  private static SparseIntArray sStateToDescrip;
  
  private static SparseArray<String> sStateToEnvironment = new SparseArray();
  
  public final DiskInfo disk;
  
  public String fsLabel;
  
  public String fsType;
  
  public String fsUuid;
  
  public final String id;
  
  public String internalPath;
  
  static {
    sEnvironmentToBroadcast = new ArrayMap();
    sStateToDescrip = new SparseIntArray();
    sDescriptionComparator = (Comparator<VolumeInfo>)new Object();
    sStateToEnvironment.put(0, "unmounted");
    sStateToEnvironment.put(1, "checking");
    sStateToEnvironment.put(2, "mounted");
    sStateToEnvironment.put(3, "mounted_ro");
    sStateToEnvironment.put(4, "unmounted");
    sStateToEnvironment.put(5, "ejecting");
    sStateToEnvironment.put(6, "unmountable");
    sStateToEnvironment.put(7, "removed");
    sStateToEnvironment.put(8, "bad_removal");
    sEnvironmentToBroadcast.put("unmounted", "android.intent.action.MEDIA_UNMOUNTED");
    sEnvironmentToBroadcast.put("checking", "android.intent.action.MEDIA_CHECKING");
    sEnvironmentToBroadcast.put("mounted", "android.intent.action.MEDIA_MOUNTED");
    sEnvironmentToBroadcast.put("mounted_ro", "android.intent.action.MEDIA_MOUNTED");
    sEnvironmentToBroadcast.put("ejecting", "android.intent.action.MEDIA_EJECT");
    sEnvironmentToBroadcast.put("unmountable", "android.intent.action.MEDIA_UNMOUNTABLE");
    sEnvironmentToBroadcast.put("removed", "android.intent.action.MEDIA_REMOVED");
    sEnvironmentToBroadcast.put("bad_removal", "android.intent.action.MEDIA_BAD_REMOVAL");
    sStateToDescrip.put(0, 17040158);
    sStateToDescrip.put(1, 17040150);
    sStateToDescrip.put(2, 17040154);
    sStateToDescrip.put(3, 17040155);
    sStateToDescrip.put(4, 17040152);
    sStateToDescrip.put(5, 17040151);
    sStateToDescrip.put(6, 17040157);
    sStateToDescrip.put(7, 17040156);
    sStateToDescrip.put(8, 17040149);
    CREATOR = (Parcelable.Creator<VolumeInfo>)new Object();
  }
  
  public int mountFlags = 0;
  
  public int mountUserId = -10000;
  
  public final String partGuid;
  
  public String path;
  
  public int state = 0;
  
  public final int type;
  
  public VolumeInfo(String paramString1, int paramInt, DiskInfo paramDiskInfo, String paramString2) {
    this.id = (String)Preconditions.checkNotNull(paramString1);
    this.type = paramInt;
    this.disk = paramDiskInfo;
    this.partGuid = paramString2;
  }
  
  public VolumeInfo(Parcel paramParcel) {
    this.id = paramParcel.readString8();
    this.type = paramParcel.readInt();
    if (paramParcel.readInt() != 0) {
      this.disk = DiskInfo.CREATOR.createFromParcel(paramParcel);
    } else {
      this.disk = null;
    } 
    this.partGuid = paramParcel.readString8();
    this.mountFlags = paramParcel.readInt();
    this.mountUserId = paramParcel.readInt();
    this.state = paramParcel.readInt();
    this.fsType = paramParcel.readString8();
    this.fsUuid = paramParcel.readString8();
    this.fsLabel = paramParcel.readString8();
    this.path = paramParcel.readString8();
    this.internalPath = paramParcel.readString8();
    initFromParcel(paramParcel);
  }
  
  public static String getEnvironmentForState(int paramInt) {
    String str = (String)sStateToEnvironment.get(paramInt);
    if (str != null)
      return str; 
    return "unknown";
  }
  
  public static String getBroadcastForEnvironment(String paramString) {
    return (String)sEnvironmentToBroadcast.get(paramString);
  }
  
  public static String getBroadcastForState(int paramInt) {
    return getBroadcastForEnvironment(getEnvironmentForState(paramInt));
  }
  
  public static Comparator<VolumeInfo> getDescriptionComparator() {
    return sDescriptionComparator;
  }
  
  public String getId() {
    return this.id;
  }
  
  public DiskInfo getDisk() {
    return this.disk;
  }
  
  public String getDiskId() {
    DiskInfo diskInfo = this.disk;
    if (diskInfo != null) {
      String str = diskInfo.id;
    } else {
      diskInfo = null;
    } 
    return (String)diskInfo;
  }
  
  public int getType() {
    return this.type;
  }
  
  public int getState() {
    return this.state;
  }
  
  public int getStateDescription() {
    return sStateToDescrip.get(this.state, 0);
  }
  
  public String getFsUuid() {
    return this.fsUuid;
  }
  
  public String getNormalizedFsUuid() {
    String str = this.fsUuid;
    if (str != null) {
      str = str.toLowerCase(Locale.US);
    } else {
      str = null;
    } 
    return str;
  }
  
  public int getMountUserId() {
    return this.mountUserId;
  }
  
  public String getDescription() {
    if ("private".equals(this.id) || this.id.startsWith("emulated;"))
      return Resources.getSystem().getString(17041371); 
    if (!TextUtils.isEmpty(this.fsLabel))
      return this.fsLabel; 
    return null;
  }
  
  public boolean isMountedReadable() {
    int i = this.state;
    return (i == 2 || i == 3);
  }
  
  public boolean isMountedWritable() {
    boolean bool;
    if (this.state == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isPrimary() {
    int i = this.mountFlags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public boolean isPrimaryPhysical() {
    boolean bool;
    if (isPrimary() && getType() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isVisible() {
    boolean bool;
    if ((this.mountFlags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isVisibleForUser(int paramInt) {
    int i = this.type;
    if ((i == 0 || i == 5 || i == 2) && this.mountUserId == paramInt)
      return isVisible(); 
    return false;
  }
  
  public boolean isPrimaryEmulatedForUser(int paramInt) {
    String str = this.id;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("emulated;");
    stringBuilder.append(paramInt);
    return str.equals(stringBuilder.toString());
  }
  
  public boolean isVisibleForRead(int paramInt) {
    return isVisibleForUser(paramInt);
  }
  
  public boolean isVisibleForWrite(int paramInt) {
    return isVisibleForUser(paramInt);
  }
  
  public File getPath() {
    File file;
    if (this.path != null) {
      file = new File(this.path);
    } else {
      file = null;
    } 
    return file;
  }
  
  public File getInternalPath() {
    File file;
    if (this.internalPath != null) {
      file = new File(this.internalPath);
    } else {
      file = null;
    } 
    return file;
  }
  
  public File getPathForUser(int paramInt) {
    if (this.path == null)
      return null; 
    int i = this.type;
    if (i == 0 || i == 5)
      return new File(this.path); 
    if (i == 2)
      return new File(this.path, Integer.toString(paramInt)); 
    return null;
  }
  
  public File getInternalPathForUser(int paramInt) {
    if (this.path == null)
      return null; 
    int i = this.type;
    if (i == 0 || i == 5)
      return new File(this.path.replace("/storage/", "/mnt/media_rw/")); 
    return getPathForUser(paramInt);
  }
  
  public StorageVolume buildStorageVolume(Context paramContext, int paramInt, boolean paramBoolean) {
    StringBuilder stringBuilder;
    String str1;
    long l;
    boolean bool;
    StorageManager storageManager = (StorageManager)paramContext.getSystemService(StorageManager.class);
    if (paramBoolean) {
      str1 = "unmounted";
    } else {
      str1 = getEnvironmentForState(this.state);
    } 
    File file1 = getPathForUser(paramInt);
    File file2 = file1;
    if (file1 == null)
      file2 = new File("/dev/null"); 
    File file3 = getInternalPathForUser(paramInt);
    if (file3 == null)
      file3 = new File("/dev/null"); 
    String str3 = null;
    String str2 = this.fsUuid;
    int i = this.type;
    if (i == 2) {
      VolumeInfo volumeInfo = storageManager.findPrivateForEmulated(this);
      if (volumeInfo != null) {
        str3 = storageManager.getBestVolumeDescription(volumeInfo);
        str2 = volumeInfo.fsUuid;
      } 
      if (isPrimaryEmulatedForUser(paramInt)) {
        paramBoolean = false;
      } else {
        paramBoolean = true;
      } 
      l = 0L;
      boolean bool1 = true;
      bool = paramBoolean;
      paramBoolean = bool1;
    } else if (i == 0 || i == 5) {
      str3 = storageManager.getBestVolumeDescription(this);
      if ("vfat".equals(this.fsType)) {
        l = 4294967295L;
        paramBoolean = false;
        bool = true;
      } else {
        l = 0L;
        paramBoolean = false;
        bool = true;
      } 
    } else {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected volume type ");
      stringBuilder.append(this.type);
      throw new IllegalStateException(stringBuilder.toString());
    } 
    if (str3 == null)
      str3 = stringBuilder.getString(17039374); 
    return new StorageVolume(this.id, file2, file3, str3, isPrimary(), bool, paramBoolean, false, l, new UserHandle(paramInt), str2, str1);
  }
  
  public static int buildStableMtpStorageId(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return 0; 
    int i = 0;
    int j;
    for (j = 0; j < paramString.length(); j++)
      i = i * 31 + paramString.charAt(j); 
    i = (i << 16 ^ i) & 0xFFFF0000;
    j = i;
    if (i == 0)
      j = 131072; 
    i = j;
    if (j == 65536)
      i = 131072; 
    j = i;
    if (i == -65536)
      j = -131072; 
    return j | 0x1;
  }
  
  public Intent buildBrowseIntent() {
    return buildBrowseIntentForUser(UserHandle.myUserId());
  }
  
  public Intent buildBrowseIntentForUser(int paramInt) {
    Uri uri;
    int i = this.type;
    if ((i == 0 || i == 5) && this.mountUserId == paramInt) {
      uri = DocumentsContract.buildRootUri("com.android.externalstorage.documents", this.fsUuid);
    } else {
      if (this.type == 2 && isPrimary()) {
        uri = DocumentsContract.buildRootUri("com.android.externalstorage.documents", "primary");
        Intent intent1 = new Intent("android.intent.action.VIEW");
        intent1.addCategory("android.intent.category.DEFAULT");
        intent1.setDataAndType(uri, "vnd.android.document/root");
        intent1.putExtra("android.provider.extra.SHOW_ADVANCED", isPrimary());
        return intent1;
      } 
      return null;
    } 
    Intent intent = new Intent("android.intent.action.VIEW");
    intent.addCategory("android.intent.category.DEFAULT");
    intent.setDataAndType(uri, "vnd.android.document/root");
    intent.putExtra("android.provider.extra.SHOW_ADVANCED", isPrimary());
    return intent;
  }
  
  public String toString() {
    CharArrayWriter charArrayWriter = new CharArrayWriter();
    dump(new IndentingPrintWriter(charArrayWriter, "    ", 80));
    return charArrayWriter.toString();
  }
  
  public void dump(IndentingPrintWriter paramIndentingPrintWriter) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("VolumeInfo{");
    stringBuilder.append(this.id);
    stringBuilder.append("}:");
    paramIndentingPrintWriter.println(stringBuilder.toString());
    paramIndentingPrintWriter.increaseIndent();
    paramIndentingPrintWriter.printPair("type", DebugUtils.valueToString(getClass(), "TYPE_", this.type));
    paramIndentingPrintWriter.printPair("diskId", getDiskId());
    paramIndentingPrintWriter.printPair("partGuid", this.partGuid);
    paramIndentingPrintWriter.printPair("mountFlags", DebugUtils.flagsToString(getClass(), "MOUNT_FLAG_", this.mountFlags));
    paramIndentingPrintWriter.printPair("mountUserId", Integer.valueOf(this.mountUserId));
    paramIndentingPrintWriter.printPair("state", DebugUtils.valueToString(getClass(), "STATE_", this.state));
    paramIndentingPrintWriter.println();
    paramIndentingPrintWriter.printPair("fsType", this.fsType);
    paramIndentingPrintWriter.printPair("fsUuid", this.fsUuid);
    paramIndentingPrintWriter.printPair("fsLabel", this.fsLabel);
    paramIndentingPrintWriter.println();
    paramIndentingPrintWriter.printPair("path", this.path);
    paramIndentingPrintWriter.printPair("internalPath", this.internalPath);
    paramIndentingPrintWriter.decreaseIndent();
    paramIndentingPrintWriter.println();
    super.dump(paramIndentingPrintWriter);
  }
  
  public VolumeInfo clone() {
    Parcel parcel = Parcel.obtain();
    try {
      writeToParcel(parcel, 0);
      parcel.setDataPosition(0);
      return CREATOR.createFromParcel(parcel);
    } finally {
      parcel.recycle();
    } 
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject instanceof VolumeInfo)
      return Objects.equals(this.id, ((VolumeInfo)paramObject).id); 
    return false;
  }
  
  public int hashCode() {
    return this.id.hashCode();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString8(this.id);
    paramParcel.writeInt(this.type);
    if (this.disk != null) {
      paramParcel.writeInt(1);
      this.disk.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeString8(this.partGuid);
    paramParcel.writeInt(this.mountFlags);
    paramParcel.writeInt(this.mountUserId);
    paramParcel.writeInt(this.state);
    paramParcel.writeString8(this.fsType);
    paramParcel.writeString8(this.fsUuid);
    paramParcel.writeString8(this.fsLabel);
    paramParcel.writeString8(this.path);
    paramParcel.writeString8(this.internalPath);
    super.writeToParcel(paramParcel, paramInt);
  }
}
