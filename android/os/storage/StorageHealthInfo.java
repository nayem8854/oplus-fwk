package android.os.storage;

import java.util.List;

public class StorageHealthInfo {
  private List<StorageHealthInfoItem> mHealthInfoItems;
  
  private String mHealthInfoName;
  
  public StorageHealthInfo(String paramString) {
    this.mHealthInfoName = paramString;
    this.mHealthInfoItems = null;
  }
  
  public String getHealthInfoName() {
    return this.mHealthInfoName;
  }
  
  public void setHealthInfoItems(List<StorageHealthInfoItem> paramList) {
    this.mHealthInfoItems = paramList;
  }
  
  public List<StorageHealthInfoItem> getHealthInfoItems() {
    return this.mHealthInfoItems;
  }
}
