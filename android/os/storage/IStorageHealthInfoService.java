package android.os.storage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IStorageHealthInfoService extends IInterface {
  String[] getStorageHealthInfoItem() throws RemoteException;
  
  byte[] getStorageOriginalInfo() throws RemoteException;
  
  String[] getstrStorageHealthInfo() throws RemoteException;
  
  class Default implements IStorageHealthInfoService {
    public String[] getstrStorageHealthInfo() throws RemoteException {
      return null;
    }
    
    public String[] getStorageHealthInfoItem() throws RemoteException {
      return null;
    }
    
    public byte[] getStorageOriginalInfo() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IStorageHealthInfoService {
    private static final String DESCRIPTOR = "android.os.storage.IStorageHealthInfoService";
    
    static final int TRANSACTION_getStorageHealthInfoItem = 2;
    
    static final int TRANSACTION_getStorageOriginalInfo = 3;
    
    static final int TRANSACTION_getstrStorageHealthInfo = 1;
    
    public Stub() {
      attachInterface(this, "android.os.storage.IStorageHealthInfoService");
    }
    
    public static IStorageHealthInfoService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.storage.IStorageHealthInfoService");
      if (iInterface != null && iInterface instanceof IStorageHealthInfoService)
        return (IStorageHealthInfoService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "getStorageOriginalInfo";
        } 
        return "getStorageHealthInfoItem";
      } 
      return "getstrStorageHealthInfo";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        byte[] arrayOfByte;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.os.storage.IStorageHealthInfoService");
            return true;
          } 
          param1Parcel1.enforceInterface("android.os.storage.IStorageHealthInfoService");
          arrayOfByte = getStorageOriginalInfo();
          param1Parcel2.writeNoException();
          param1Parcel2.writeByteArray(arrayOfByte);
          return true;
        } 
        arrayOfByte.enforceInterface("android.os.storage.IStorageHealthInfoService");
        arrayOfString = getStorageHealthInfoItem();
        param1Parcel2.writeNoException();
        param1Parcel2.writeStringArray(arrayOfString);
        return true;
      } 
      arrayOfString.enforceInterface("android.os.storage.IStorageHealthInfoService");
      String[] arrayOfString = getstrStorageHealthInfo();
      param1Parcel2.writeNoException();
      param1Parcel2.writeStringArray(arrayOfString);
      return true;
    }
    
    private static class Proxy implements IStorageHealthInfoService {
      public static IStorageHealthInfoService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.storage.IStorageHealthInfoService";
      }
      
      public String[] getstrStorageHealthInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageHealthInfoService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IStorageHealthInfoService.Stub.getDefaultImpl() != null)
            return IStorageHealthInfoService.Stub.getDefaultImpl().getstrStorageHealthInfo(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getStorageHealthInfoItem() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageHealthInfoService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IStorageHealthInfoService.Stub.getDefaultImpl() != null)
            return IStorageHealthInfoService.Stub.getDefaultImpl().getStorageHealthInfoItem(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getStorageOriginalInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageHealthInfoService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IStorageHealthInfoService.Stub.getDefaultImpl() != null)
            return IStorageHealthInfoService.Stub.getDefaultImpl().getStorageOriginalInfo(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IStorageHealthInfoService param1IStorageHealthInfoService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IStorageHealthInfoService != null) {
          Proxy.sDefaultImpl = param1IStorageHealthInfoService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IStorageHealthInfoService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
