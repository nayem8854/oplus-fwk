package android.os.storage;

import android.annotation.SystemApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.UserHandle;
import android.provider.DocumentsContract;
import com.android.internal.util.IndentingPrintWriter;
import com.android.internal.util.Preconditions;
import java.io.CharArrayWriter;
import java.io.File;
import java.util.Locale;

public final class StorageVolume extends OplusBaseStorageVolume {
  private static final String ACTION_OPEN_EXTERNAL_DIRECTORY = "android.os.storage.action.OPEN_EXTERNAL_DIRECTORY";
  
  public StorageVolume(String paramString1, File paramFile1, File paramFile2, String paramString2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, long paramLong, UserHandle paramUserHandle, String paramString3, String paramString4) {
    this.mId = (String)Preconditions.checkNotNull(paramString1);
    this.mPath = (File)Preconditions.checkNotNull(paramFile1);
    this.mInternalPath = (File)Preconditions.checkNotNull(paramFile2);
    this.mDescription = (String)Preconditions.checkNotNull(paramString2);
    this.mPrimary = paramBoolean1;
    this.mRemovable = paramBoolean2;
    this.mEmulated = paramBoolean3;
    this.mAllowMassStorage = paramBoolean4;
    this.mMaxFileSize = paramLong;
    this.mOwner = (UserHandle)Preconditions.checkNotNull(paramUserHandle);
    this.mFsUuid = paramString3;
    this.mState = (String)Preconditions.checkNotNull(paramString4);
  }
  
  private StorageVolume(Parcel paramParcel) {
    boolean bool2;
    this.mId = paramParcel.readString8();
    this.mPath = new File(paramParcel.readString8());
    this.mInternalPath = new File(paramParcel.readString8());
    this.mDescription = paramParcel.readString8();
    int i = paramParcel.readInt();
    boolean bool1 = true;
    if (i != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mPrimary = bool2;
    if (paramParcel.readInt() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mRemovable = bool2;
    if (paramParcel.readInt() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mEmulated = bool2;
    if (paramParcel.readInt() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mAllowMassStorage = bool2;
    this.mMaxFileSize = paramParcel.readLong();
    this.mOwner = paramParcel.<UserHandle>readParcelable(null);
    this.mFsUuid = paramParcel.readString8();
    this.mState = paramParcel.readString8();
    initFromParcel(paramParcel);
  }
  
  public StorageVolume(String paramString1, File paramFile1, File paramFile2, String paramString2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, long paramLong, UserHandle paramUserHandle, String paramString3, String paramString4, int paramInt) {
    this(paramString1, paramFile1, paramFile2, paramString2, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4, paramLong, paramUserHandle, paramString3, paramString4);
    setReadOnlyType(paramInt);
  }
  
  @SystemApi
  public String getId() {
    return this.mId;
  }
  
  public String getPath() {
    return this.mPath.toString();
  }
  
  public String getInternalPath() {
    return this.mInternalPath.toString();
  }
  
  public File getPathFile() {
    return this.mPath;
  }
  
  public File getDirectory() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mState : Ljava/lang/String;
    //   4: astore_1
    //   5: aload_1
    //   6: invokevirtual hashCode : ()I
    //   9: istore_2
    //   10: iload_2
    //   11: ldc 1242932856
    //   13: if_icmpeq -> 40
    //   16: iload_2
    //   17: ldc 1299749220
    //   19: if_icmpeq -> 25
    //   22: goto -> 55
    //   25: aload_1
    //   26: ldc_w 'mounted_ro'
    //   29: invokevirtual equals : (Ljava/lang/Object;)Z
    //   32: ifeq -> 22
    //   35: iconst_1
    //   36: istore_2
    //   37: goto -> 57
    //   40: aload_1
    //   41: ldc_w 'mounted'
    //   44: invokevirtual equals : (Ljava/lang/Object;)Z
    //   47: ifeq -> 22
    //   50: iconst_0
    //   51: istore_2
    //   52: goto -> 57
    //   55: iconst_m1
    //   56: istore_2
    //   57: iload_2
    //   58: ifeq -> 68
    //   61: iload_2
    //   62: iconst_1
    //   63: if_icmpeq -> 68
    //   66: aconst_null
    //   67: areturn
    //   68: aload_0
    //   69: getfield mPath : Ljava/io/File;
    //   72: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #239	-> 0
    //   #244	-> 66
    //   #242	-> 68
  }
  
  public String getDescription(Context paramContext) {
    return this.mDescription;
  }
  
  public boolean isPrimary() {
    return this.mPrimary;
  }
  
  public boolean isRemovable() {
    return this.mRemovable;
  }
  
  public boolean isEmulated() {
    return this.mEmulated;
  }
  
  public boolean allowMassStorage() {
    return this.mAllowMassStorage;
  }
  
  public long getMaxFileSize() {
    return this.mMaxFileSize;
  }
  
  public UserHandle getOwner() {
    return this.mOwner;
  }
  
  public String getUuid() {
    return this.mFsUuid;
  }
  
  public String getMediaStoreVolumeName() {
    if (isPrimary())
      return "external_primary"; 
    return getNormalizedUuid();
  }
  
  public static String normalizeUuid(String paramString) {
    if (paramString != null) {
      paramString = paramString.toLowerCase(Locale.US);
    } else {
      paramString = null;
    } 
    return paramString;
  }
  
  public String getNormalizedUuid() {
    return normalizeUuid(this.mFsUuid);
  }
  
  public int getFatVolumeId() {
    String str = this.mFsUuid;
    if (str == null || str.length() != 9)
      return -1; 
    try {
      long l = Long.parseLong(this.mFsUuid.replace("-", ""), 16);
      return (int)l;
    } catch (NumberFormatException numberFormatException) {
      return -1;
    } 
  }
  
  public String getUserLabel() {
    return this.mDescription;
  }
  
  public String getState() {
    return this.mState;
  }
  
  @Deprecated
  public Intent createAccessIntent(String paramString) {
    if ((isPrimary() && paramString == null) || (paramString != null && 
      !Environment.isStandardDirectory(paramString)))
      return null; 
    Intent intent = new Intent("android.os.storage.action.OPEN_EXTERNAL_DIRECTORY");
    intent.putExtra("android.os.storage.extra.STORAGE_VOLUME", this);
    intent.putExtra("android.os.storage.extra.DIRECTORY_NAME", paramString);
    return intent;
  }
  
  public Intent createOpenDocumentTreeIntent() {
    String str;
    if (isEmulated()) {
      str = "primary";
    } else {
      str = this.mFsUuid;
    } 
    Uri uri = DocumentsContract.buildRootUri("com.android.externalstorage.documents", str);
    Intent intent2 = new Intent("android.intent.action.OPEN_DOCUMENT_TREE");
    Intent intent1 = intent2.putExtra("android.provider.extra.INITIAL_URI", uri);
    intent1 = intent1.putExtra("android.provider.extra.SHOW_ADVANCED", true);
    return intent1;
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject instanceof StorageVolume) {
      File file = this.mPath;
      if (file != null) {
        paramObject = paramObject;
        return file.equals(((StorageVolume)paramObject).mPath);
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    return this.mPath.hashCode();
  }
  
  public String toString() {
    StringBuilder stringBuilder = (new StringBuilder("StorageVolume: ")).append(this.mDescription);
    if (this.mFsUuid != null) {
      stringBuilder.append(" (");
      stringBuilder.append(this.mFsUuid);
      stringBuilder.append(")");
    } 
    return stringBuilder.toString();
  }
  
  public String dump() {
    CharArrayWriter charArrayWriter = new CharArrayWriter();
    dump(new IndentingPrintWriter(charArrayWriter, "    ", 80));
    return charArrayWriter.toString();
  }
  
  public void dump(IndentingPrintWriter paramIndentingPrintWriter) {
    paramIndentingPrintWriter.println("StorageVolume:");
    paramIndentingPrintWriter.increaseIndent();
    paramIndentingPrintWriter.printPair("mId", this.mId);
    paramIndentingPrintWriter.printPair("mPath", this.mPath);
    paramIndentingPrintWriter.printPair("mInternalPath", this.mInternalPath);
    paramIndentingPrintWriter.printPair("mDescription", this.mDescription);
    paramIndentingPrintWriter.printPair("mPrimary", Boolean.valueOf(this.mPrimary));
    paramIndentingPrintWriter.printPair("mRemovable", Boolean.valueOf(this.mRemovable));
    paramIndentingPrintWriter.printPair("mEmulated", Boolean.valueOf(this.mEmulated));
    paramIndentingPrintWriter.printPair("mAllowMassStorage", Boolean.valueOf(this.mAllowMassStorage));
    paramIndentingPrintWriter.printPair("mMaxFileSize", Long.valueOf(this.mMaxFileSize));
    paramIndentingPrintWriter.printPair("mOwner", this.mOwner);
    paramIndentingPrintWriter.printPair("mFsUuid", this.mFsUuid);
    paramIndentingPrintWriter.printPair("mState", this.mState);
    super.dump(paramIndentingPrintWriter);
    paramIndentingPrintWriter.decreaseIndent();
  }
  
  public static final Parcelable.Creator<StorageVolume> CREATOR = (Parcelable.Creator<StorageVolume>)new Object();
  
  public static final String EXTRA_DIRECTORY_NAME = "android.os.storage.extra.DIRECTORY_NAME";
  
  public static final String EXTRA_STORAGE_VOLUME = "android.os.storage.extra.STORAGE_VOLUME";
  
  public static final int STORAGE_ID_INVALID = 0;
  
  public static final int STORAGE_ID_PRIMARY = 65537;
  
  private final boolean mAllowMassStorage;
  
  private final String mDescription;
  
  private final boolean mEmulated;
  
  private final String mFsUuid;
  
  private final String mId;
  
  private final File mInternalPath;
  
  private final long mMaxFileSize;
  
  private final UserHandle mOwner;
  
  private final File mPath;
  
  private final boolean mPrimary;
  
  private final boolean mRemovable;
  
  private final String mState;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString8(this.mId);
    paramParcel.writeString8(this.mPath.toString());
    paramParcel.writeString8(this.mInternalPath.toString());
    paramParcel.writeString8(this.mDescription);
    paramParcel.writeInt(this.mPrimary);
    paramParcel.writeInt(this.mRemovable);
    paramParcel.writeInt(this.mEmulated);
    paramParcel.writeInt(this.mAllowMassStorage);
    paramParcel.writeLong(this.mMaxFileSize);
    paramParcel.writeParcelable(this.mOwner, paramInt);
    paramParcel.writeString8(this.mFsUuid);
    paramParcel.writeString8(this.mState);
    super.writeToParcel(paramParcel, paramInt);
  }
}
