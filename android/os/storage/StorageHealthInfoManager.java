package android.os.storage;

import android.content.Context;
import android.os.RemoteException;
import java.util.HashMap;
import java.util.Map;

public class StorageHealthInfoManager {
  public static final double PE_CYCLE_LEVEL = 1800.0D;
  
  public static final int PE_CYCLE_MAX = 30;
  
  public static final int RR_THREHOLD = 30;
  
  public static final int RTBB_THREHOLD = 30;
  
  private static final String TAG = "StorageHealthInfoManager";
  
  public static final int WAF_THREHOLD = 5;
  
  IStorageHealthInfoService mService;
  
  public StorageHealthInfoManager(Context paramContext, IStorageHealthInfoService paramIStorageHealthInfoService) {
    this.mService = paramIStorageHealthInfoService;
  }
  
  public Map<String, String> getStorageHealthInfoMap() throws RemoteException {
    String[] arrayOfString1 = getstrStorageHealthInfo();
    String[] arrayOfString2 = getStorageHealthInfoItem();
    if (arrayOfString1 == null || arrayOfString2 == null || arrayOfString1.length != arrayOfString2.length)
      return null; 
    HashMap<Object, Object> hashMap = new HashMap<>();
    for (byte b = 0; b < arrayOfString2.length; b++)
      hashMap.put(arrayOfString2[b], arrayOfString1[b]); 
    return (Map)hashMap;
  }
  
  public boolean judgeStorageHealthInfo(Map<String, String> paramMap) throws RemoteException {
    if (paramMap == null)
      return false; 
    if (paramMap.get("badBlock_runtim") == null || paramMap.get("readReclaim") == null || paramMap.get("TBW") == null || paramMap.get("eraseXLCCntAvg") == null)
      return false; 
    Map<String, String> map = getStorageHealthInfoMap();
    if (map == null)
      return false; 
    String str = map.get("StorageSize");
    int i = Integer.parseInt(str.substring(0, str.length() - 1));
    int j = Integer.parseInt(map.get("badBlock_runtim")), k = Integer.parseInt(paramMap.get("badBlock_runtim"));
    int m = Integer.parseInt(map.get("readReclaim")), n = Integer.parseInt(paramMap.get("readReclaim"));
    int i1 = Integer.parseInt(map.get("TBW")) - Integer.parseInt(paramMap.get("TBW"));
    int i2 = Integer.parseInt(map.get("eraseXLCCntAvg")) - Integer.parseInt(paramMap.get("eraseXLCCntAvg"));
    float f1 = 0.0F;
    float f2 = f1;
    if (i1 > 0) {
      f2 = f1;
      if (i2 > 0)
        f2 = (i2 * i * 1024 / i1 * 100); 
    } 
    if (f2 > 0.0F && f2 <= 5.0F && m - n <= 30 && j - k <= 30)
      return true; 
    return false;
  }
  
  public boolean checkStorageHealthInfo() throws RemoteException {
    Map<String, String> map = getStorageHealthInfoMap();
    if (map == null)
      return false; 
    int i = Integer.parseInt(map.get("eraseXLCCntAvg"));
    if (i <= 30)
      return true; 
    return false;
  }
  
  public String[] getstrStorageHealthInfo() throws RemoteException {
    return this.mService.getstrStorageHealthInfo();
  }
  
  public String[] getStorageHealthInfoItem() throws RemoteException {
    return this.mService.getStorageHealthInfoItem();
  }
  
  public byte[] getStorageOriginalInfo() throws RemoteException {
    return this.mService.getStorageOriginalInfo();
  }
}
