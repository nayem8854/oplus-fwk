package android.os.storage;

import android.content.pm.IPackageMoveObserver;
import android.content.res.ObbInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.IVoldTaskListener;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import com.android.internal.os.AppFuseMount;

public interface IStorageManager extends IInterface {
  void abortChanges(String paramString, boolean paramBoolean) throws RemoteException;
  
  void abortIdleMaintenance() throws RemoteException;
  
  void addUserKeyAuth(int paramInt1, int paramInt2, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws RemoteException;
  
  void allocateBytes(String paramString1, long paramLong, int paramInt, String paramString2) throws RemoteException;
  
  void benchmark(String paramString, IVoldTaskListener paramIVoldTaskListener) throws RemoteException;
  
  int changeEncryptionPassword(int paramInt, String paramString) throws RemoteException;
  
  void clearPassword() throws RemoteException;
  
  void clearUserKeyAuth(int paramInt1, int paramInt2, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws RemoteException;
  
  void commitChanges() throws RemoteException;
  
  void createUserKey(int paramInt1, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  int decryptStorage(String paramString) throws RemoteException;
  
  void destroyUserKey(int paramInt) throws RemoteException;
  
  void destroyUserStorage(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  int encryptStorage(int paramInt, String paramString) throws RemoteException;
  
  void fixateNewestUserKeyAuth(int paramInt) throws RemoteException;
  
  void fixupAppDir(String paramString) throws RemoteException;
  
  void forgetAllVolumes() throws RemoteException;
  
  void forgetVolume(String paramString) throws RemoteException;
  
  void format(String paramString) throws RemoteException;
  
  void fstrim(int paramInt, IVoldTaskListener paramIVoldTaskListener) throws RemoteException;
  
  long getAllocatableBytes(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  long getCacheQuotaBytes(String paramString, int paramInt) throws RemoteException;
  
  long getCacheSizeBytes(String paramString, int paramInt) throws RemoteException;
  
  DiskInfo[] getDisks() throws RemoteException;
  
  int getEncryptionState() throws RemoteException;
  
  String getField(String paramString) throws RemoteException;
  
  String getMountedObbPath(String paramString) throws RemoteException;
  
  String getPassword() throws RemoteException;
  
  int getPasswordType() throws RemoteException;
  
  String getPrimaryStorageUuid() throws RemoteException;
  
  StorageVolume[] getVolumeList(int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  VolumeRecord[] getVolumeRecords(int paramInt) throws RemoteException;
  
  VolumeInfo[] getVolumes(int paramInt) throws RemoteException;
  
  boolean isConvertibleToFBE() throws RemoteException;
  
  boolean isObbMounted(String paramString) throws RemoteException;
  
  boolean isUserKeyUnlocked(int paramInt) throws RemoteException;
  
  long lastMaintenance() throws RemoteException;
  
  void lockUserKey(int paramInt) throws RemoteException;
  
  void mkdirs(String paramString1, String paramString2) throws RemoteException;
  
  void mount(String paramString) throws RemoteException;
  
  void mountObb(String paramString1, String paramString2, String paramString3, IObbActionListener paramIObbActionListener, int paramInt, ObbInfo paramObbInfo) throws RemoteException;
  
  AppFuseMount mountProxyFileDescriptorBridge() throws RemoteException;
  
  boolean needsCheckpoint() throws RemoteException;
  
  ParcelFileDescriptor openProxyFileDescriptor(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void partitionMixed(String paramString, int paramInt) throws RemoteException;
  
  void partitionPrivate(String paramString) throws RemoteException;
  
  void partitionPublic(String paramString) throws RemoteException;
  
  void prepareUserStorage(String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void registerListener(IStorageEventListener paramIStorageEventListener) throws RemoteException;
  
  void runIdleMaintenance() throws RemoteException;
  
  void runMaintenance() throws RemoteException;
  
  void setDebugFlags(int paramInt1, int paramInt2) throws RemoteException;
  
  void setField(String paramString1, String paramString2) throws RemoteException;
  
  void setPrimaryStorageUuid(String paramString, IPackageMoveObserver paramIPackageMoveObserver) throws RemoteException;
  
  void setVolumeNickname(String paramString1, String paramString2) throws RemoteException;
  
  void setVolumeUserFlags(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void shutdown(IStorageShutdownObserver paramIStorageShutdownObserver) throws RemoteException;
  
  void startCheckpoint(int paramInt) throws RemoteException;
  
  boolean supportsCheckpoint() throws RemoteException;
  
  void unlockUserKey(int paramInt1, int paramInt2, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws RemoteException;
  
  void unmount(String paramString) throws RemoteException;
  
  void unmountObb(String paramString, boolean paramBoolean, IObbActionListener paramIObbActionListener, int paramInt) throws RemoteException;
  
  void unregisterListener(IStorageEventListener paramIStorageEventListener) throws RemoteException;
  
  int verifyEncryptionPassword(String paramString) throws RemoteException;
  
  class Default implements IStorageManager {
    public void registerListener(IStorageEventListener param1IStorageEventListener) throws RemoteException {}
    
    public void unregisterListener(IStorageEventListener param1IStorageEventListener) throws RemoteException {}
    
    public void shutdown(IStorageShutdownObserver param1IStorageShutdownObserver) throws RemoteException {}
    
    public void mountObb(String param1String1, String param1String2, String param1String3, IObbActionListener param1IObbActionListener, int param1Int, ObbInfo param1ObbInfo) throws RemoteException {}
    
    public void unmountObb(String param1String, boolean param1Boolean, IObbActionListener param1IObbActionListener, int param1Int) throws RemoteException {}
    
    public boolean isObbMounted(String param1String) throws RemoteException {
      return false;
    }
    
    public String getMountedObbPath(String param1String) throws RemoteException {
      return null;
    }
    
    public int decryptStorage(String param1String) throws RemoteException {
      return 0;
    }
    
    public int encryptStorage(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public int changeEncryptionPassword(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public StorageVolume[] getVolumeList(int param1Int1, String param1String, int param1Int2) throws RemoteException {
      return null;
    }
    
    public int getEncryptionState() throws RemoteException {
      return 0;
    }
    
    public int verifyEncryptionPassword(String param1String) throws RemoteException {
      return 0;
    }
    
    public void mkdirs(String param1String1, String param1String2) throws RemoteException {}
    
    public int getPasswordType() throws RemoteException {
      return 0;
    }
    
    public String getPassword() throws RemoteException {
      return null;
    }
    
    public void clearPassword() throws RemoteException {}
    
    public void setField(String param1String1, String param1String2) throws RemoteException {}
    
    public String getField(String param1String) throws RemoteException {
      return null;
    }
    
    public long lastMaintenance() throws RemoteException {
      return 0L;
    }
    
    public void runMaintenance() throws RemoteException {}
    
    public DiskInfo[] getDisks() throws RemoteException {
      return null;
    }
    
    public VolumeInfo[] getVolumes(int param1Int) throws RemoteException {
      return null;
    }
    
    public VolumeRecord[] getVolumeRecords(int param1Int) throws RemoteException {
      return null;
    }
    
    public void mount(String param1String) throws RemoteException {}
    
    public void unmount(String param1String) throws RemoteException {}
    
    public void format(String param1String) throws RemoteException {}
    
    public void partitionPublic(String param1String) throws RemoteException {}
    
    public void partitionPrivate(String param1String) throws RemoteException {}
    
    public void partitionMixed(String param1String, int param1Int) throws RemoteException {}
    
    public void setVolumeNickname(String param1String1, String param1String2) throws RemoteException {}
    
    public void setVolumeUserFlags(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void forgetVolume(String param1String) throws RemoteException {}
    
    public void forgetAllVolumes() throws RemoteException {}
    
    public String getPrimaryStorageUuid() throws RemoteException {
      return null;
    }
    
    public void setPrimaryStorageUuid(String param1String, IPackageMoveObserver param1IPackageMoveObserver) throws RemoteException {}
    
    public void benchmark(String param1String, IVoldTaskListener param1IVoldTaskListener) throws RemoteException {}
    
    public void setDebugFlags(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void createUserKey(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public void destroyUserKey(int param1Int) throws RemoteException {}
    
    public void unlockUserKey(int param1Int1, int param1Int2, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) throws RemoteException {}
    
    public void lockUserKey(int param1Int) throws RemoteException {}
    
    public boolean isUserKeyUnlocked(int param1Int) throws RemoteException {
      return false;
    }
    
    public void prepareUserStorage(String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void destroyUserStorage(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public boolean isConvertibleToFBE() throws RemoteException {
      return false;
    }
    
    public void addUserKeyAuth(int param1Int1, int param1Int2, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) throws RemoteException {}
    
    public void fixateNewestUserKeyAuth(int param1Int) throws RemoteException {}
    
    public void fstrim(int param1Int, IVoldTaskListener param1IVoldTaskListener) throws RemoteException {}
    
    public AppFuseMount mountProxyFileDescriptorBridge() throws RemoteException {
      return null;
    }
    
    public ParcelFileDescriptor openProxyFileDescriptor(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return null;
    }
    
    public long getCacheQuotaBytes(String param1String, int param1Int) throws RemoteException {
      return 0L;
    }
    
    public long getCacheSizeBytes(String param1String, int param1Int) throws RemoteException {
      return 0L;
    }
    
    public long getAllocatableBytes(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return 0L;
    }
    
    public void allocateBytes(String param1String1, long param1Long, int param1Int, String param1String2) throws RemoteException {}
    
    public void runIdleMaintenance() throws RemoteException {}
    
    public void abortIdleMaintenance() throws RemoteException {}
    
    public void commitChanges() throws RemoteException {}
    
    public boolean supportsCheckpoint() throws RemoteException {
      return false;
    }
    
    public void startCheckpoint(int param1Int) throws RemoteException {}
    
    public boolean needsCheckpoint() throws RemoteException {
      return false;
    }
    
    public void abortChanges(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void clearUserKeyAuth(int param1Int1, int param1Int2, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) throws RemoteException {}
    
    public void fixupAppDir(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IStorageManager {
    private static final String DESCRIPTOR = "android.os.storage.IStorageManager";
    
    static final int TRANSACTION_abortChanges = 88;
    
    static final int TRANSACTION_abortIdleMaintenance = 81;
    
    static final int TRANSACTION_addUserKeyAuth = 71;
    
    static final int TRANSACTION_allocateBytes = 79;
    
    static final int TRANSACTION_benchmark = 60;
    
    static final int TRANSACTION_changeEncryptionPassword = 29;
    
    static final int TRANSACTION_clearPassword = 38;
    
    static final int TRANSACTION_clearUserKeyAuth = 89;
    
    static final int TRANSACTION_commitChanges = 84;
    
    static final int TRANSACTION_createUserKey = 62;
    
    static final int TRANSACTION_decryptStorage = 27;
    
    static final int TRANSACTION_destroyUserKey = 63;
    
    static final int TRANSACTION_destroyUserStorage = 68;
    
    static final int TRANSACTION_encryptStorage = 28;
    
    static final int TRANSACTION_fixateNewestUserKeyAuth = 72;
    
    static final int TRANSACTION_fixupAppDir = 90;
    
    static final int TRANSACTION_forgetAllVolumes = 57;
    
    static final int TRANSACTION_forgetVolume = 56;
    
    static final int TRANSACTION_format = 50;
    
    static final int TRANSACTION_fstrim = 73;
    
    static final int TRANSACTION_getAllocatableBytes = 78;
    
    static final int TRANSACTION_getCacheQuotaBytes = 76;
    
    static final int TRANSACTION_getCacheSizeBytes = 77;
    
    static final int TRANSACTION_getDisks = 45;
    
    static final int TRANSACTION_getEncryptionState = 32;
    
    static final int TRANSACTION_getField = 40;
    
    static final int TRANSACTION_getMountedObbPath = 25;
    
    static final int TRANSACTION_getPassword = 37;
    
    static final int TRANSACTION_getPasswordType = 36;
    
    static final int TRANSACTION_getPrimaryStorageUuid = 58;
    
    static final int TRANSACTION_getVolumeList = 30;
    
    static final int TRANSACTION_getVolumeRecords = 47;
    
    static final int TRANSACTION_getVolumes = 46;
    
    static final int TRANSACTION_isConvertibleToFBE = 69;
    
    static final int TRANSACTION_isObbMounted = 24;
    
    static final int TRANSACTION_isUserKeyUnlocked = 66;
    
    static final int TRANSACTION_lastMaintenance = 42;
    
    static final int TRANSACTION_lockUserKey = 65;
    
    static final int TRANSACTION_mkdirs = 35;
    
    static final int TRANSACTION_mount = 48;
    
    static final int TRANSACTION_mountObb = 22;
    
    static final int TRANSACTION_mountProxyFileDescriptorBridge = 74;
    
    static final int TRANSACTION_needsCheckpoint = 87;
    
    static final int TRANSACTION_openProxyFileDescriptor = 75;
    
    static final int TRANSACTION_partitionMixed = 53;
    
    static final int TRANSACTION_partitionPrivate = 52;
    
    static final int TRANSACTION_partitionPublic = 51;
    
    static final int TRANSACTION_prepareUserStorage = 67;
    
    static final int TRANSACTION_registerListener = 1;
    
    static final int TRANSACTION_runIdleMaintenance = 80;
    
    static final int TRANSACTION_runMaintenance = 43;
    
    static final int TRANSACTION_setDebugFlags = 61;
    
    static final int TRANSACTION_setField = 39;
    
    static final int TRANSACTION_setPrimaryStorageUuid = 59;
    
    static final int TRANSACTION_setVolumeNickname = 54;
    
    static final int TRANSACTION_setVolumeUserFlags = 55;
    
    static final int TRANSACTION_shutdown = 20;
    
    static final int TRANSACTION_startCheckpoint = 86;
    
    static final int TRANSACTION_supportsCheckpoint = 85;
    
    static final int TRANSACTION_unlockUserKey = 64;
    
    static final int TRANSACTION_unmount = 49;
    
    static final int TRANSACTION_unmountObb = 23;
    
    static final int TRANSACTION_unregisterListener = 2;
    
    static final int TRANSACTION_verifyEncryptionPassword = 33;
    
    public Stub() {
      attachInterface(this, "android.os.storage.IStorageManager");
    }
    
    public static IStorageManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.storage.IStorageManager");
      if (iInterface != null && iInterface instanceof IStorageManager)
        return (IStorageManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 20) {
            if (param1Int != 32) {
              if (param1Int != 33) {
                if (param1Int != 42) {
                  if (param1Int != 43) {
                    switch (param1Int) {
                      default:
                        switch (param1Int) {
                          default:
                            switch (param1Int) {
                              default:
                                switch (param1Int) {
                                  default:
                                    switch (param1Int) {
                                      default:
                                        switch (param1Int) {
                                          default:
                                            return null;
                                          case 90:
                                            return "fixupAppDir";
                                          case 89:
                                            return "clearUserKeyAuth";
                                          case 88:
                                            return "abortChanges";
                                          case 87:
                                            return "needsCheckpoint";
                                          case 86:
                                            return "startCheckpoint";
                                          case 85:
                                            return "supportsCheckpoint";
                                          case 84:
                                            break;
                                        } 
                                        return "commitChanges";
                                      case 81:
                                        return "abortIdleMaintenance";
                                      case 80:
                                        return "runIdleMaintenance";
                                      case 79:
                                        return "allocateBytes";
                                      case 78:
                                        return "getAllocatableBytes";
                                      case 77:
                                        return "getCacheSizeBytes";
                                      case 76:
                                        return "getCacheQuotaBytes";
                                      case 75:
                                        return "openProxyFileDescriptor";
                                      case 74:
                                        return "mountProxyFileDescriptorBridge";
                                      case 73:
                                        return "fstrim";
                                      case 72:
                                        return "fixateNewestUserKeyAuth";
                                      case 71:
                                        break;
                                    } 
                                    return "addUserKeyAuth";
                                  case 69:
                                    return "isConvertibleToFBE";
                                  case 68:
                                    return "destroyUserStorage";
                                  case 67:
                                    return "prepareUserStorage";
                                  case 66:
                                    return "isUserKeyUnlocked";
                                  case 65:
                                    return "lockUserKey";
                                  case 64:
                                    return "unlockUserKey";
                                  case 63:
                                    return "destroyUserKey";
                                  case 62:
                                    return "createUserKey";
                                  case 61:
                                    return "setDebugFlags";
                                  case 60:
                                    return "benchmark";
                                  case 59:
                                    return "setPrimaryStorageUuid";
                                  case 58:
                                    return "getPrimaryStorageUuid";
                                  case 57:
                                    return "forgetAllVolumes";
                                  case 56:
                                    return "forgetVolume";
                                  case 55:
                                    return "setVolumeUserFlags";
                                  case 54:
                                    return "setVolumeNickname";
                                  case 53:
                                    return "partitionMixed";
                                  case 52:
                                    return "partitionPrivate";
                                  case 51:
                                    return "partitionPublic";
                                  case 50:
                                    return "format";
                                  case 49:
                                    return "unmount";
                                  case 48:
                                    return "mount";
                                  case 47:
                                    return "getVolumeRecords";
                                  case 46:
                                    return "getVolumes";
                                  case 45:
                                    break;
                                } 
                                return "getDisks";
                              case 40:
                                return "getField";
                              case 39:
                                return "setField";
                              case 38:
                                return "clearPassword";
                              case 37:
                                return "getPassword";
                              case 36:
                                return "getPasswordType";
                              case 35:
                                break;
                            } 
                            return "mkdirs";
                          case 30:
                            return "getVolumeList";
                          case 29:
                            return "changeEncryptionPassword";
                          case 28:
                            return "encryptStorage";
                          case 27:
                            break;
                        } 
                        return "decryptStorage";
                      case 25:
                        return "getMountedObbPath";
                      case 24:
                        return "isObbMounted";
                      case 23:
                        return "unmountObb";
                      case 22:
                        break;
                    } 
                    return "mountObb";
                  } 
                  return "runMaintenance";
                } 
                return "lastMaintenance";
              } 
              return "verifyEncryptionPassword";
            } 
            return "getEncryptionState";
          } 
          return "shutdown";
        } 
        return "unregisterListener";
      } 
      return "registerListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        IStorageShutdownObserver iStorageShutdownObserver;
        if (param1Int1 != 2) {
          String str1;
          if (param1Int1 != 20) {
            if (param1Int1 != 1598968902) {
              if (param1Int1 != 32) {
                if (param1Int1 != 33) {
                  if (param1Int1 != 42) {
                    if (param1Int1 != 43) {
                      boolean bool5;
                      int n;
                      boolean bool4;
                      int m;
                      boolean bool3;
                      int k;
                      boolean bool2;
                      int j;
                      boolean bool1;
                      String str5;
                      byte[] arrayOfByte2;
                      String str4;
                      ParcelFileDescriptor parcelFileDescriptor;
                      AppFuseMount appFuseMount;
                      IVoldTaskListener iVoldTaskListener2;
                      byte[] arrayOfByte1;
                      IVoldTaskListener iVoldTaskListener1;
                      IPackageMoveObserver iPackageMoveObserver;
                      String str3;
                      VolumeRecord[] arrayOfVolumeRecord;
                      VolumeInfo[] arrayOfVolumeInfo;
                      DiskInfo[] arrayOfDiskInfo;
                      String str2;
                      StorageVolume[] arrayOfStorageVolume;
                      byte[] arrayOfByte5;
                      String str9;
                      byte[] arrayOfByte4;
                      String str8;
                      byte[] arrayOfByte3;
                      String str7;
                      IObbActionListener iObbActionListener1;
                      long l1;
                      int i1;
                      String str10;
                      boolean bool6 = false, bool7 = false, bool8 = false;
                      switch (param1Int1) {
                        default:
                          switch (param1Int1) {
                            default:
                              switch (param1Int1) {
                                default:
                                  switch (param1Int1) {
                                    default:
                                      switch (param1Int1) {
                                        default:
                                          switch (param1Int1) {
                                            default:
                                              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
                                            case 90:
                                              param1Parcel1.enforceInterface("android.os.storage.IStorageManager");
                                              str5 = param1Parcel1.readString();
                                              fixupAppDir(str5);
                                              param1Parcel2.writeNoException();
                                              return true;
                                            case 89:
                                              str5.enforceInterface("android.os.storage.IStorageManager");
                                              param1Int2 = str5.readInt();
                                              param1Int1 = str5.readInt();
                                              arrayOfByte5 = str5.createByteArray();
                                              arrayOfByte2 = str5.createByteArray();
                                              clearUserKeyAuth(param1Int2, param1Int1, arrayOfByte5, arrayOfByte2);
                                              param1Parcel2.writeNoException();
                                              return true;
                                            case 88:
                                              arrayOfByte2.enforceInterface("android.os.storage.IStorageManager");
                                              str9 = arrayOfByte2.readString();
                                              if (arrayOfByte2.readInt() != 0)
                                                bool8 = true; 
                                              abortChanges(str9, bool8);
                                              param1Parcel2.writeNoException();
                                              return true;
                                            case 87:
                                              arrayOfByte2.enforceInterface("android.os.storage.IStorageManager");
                                              bool5 = needsCheckpoint();
                                              param1Parcel2.writeNoException();
                                              param1Parcel2.writeInt(bool5);
                                              return true;
                                            case 86:
                                              arrayOfByte2.enforceInterface("android.os.storage.IStorageManager");
                                              n = arrayOfByte2.readInt();
                                              startCheckpoint(n);
                                              param1Parcel2.writeNoException();
                                              return true;
                                            case 85:
                                              arrayOfByte2.enforceInterface("android.os.storage.IStorageManager");
                                              bool4 = supportsCheckpoint();
                                              param1Parcel2.writeNoException();
                                              param1Parcel2.writeInt(bool4);
                                              return true;
                                            case 84:
                                              break;
                                          } 
                                          arrayOfByte2.enforceInterface("android.os.storage.IStorageManager");
                                          commitChanges();
                                          param1Parcel2.writeNoException();
                                          return true;
                                        case 81:
                                          arrayOfByte2.enforceInterface("android.os.storage.IStorageManager");
                                          abortIdleMaintenance();
                                          param1Parcel2.writeNoException();
                                          return true;
                                        case 80:
                                          arrayOfByte2.enforceInterface("android.os.storage.IStorageManager");
                                          runIdleMaintenance();
                                          param1Parcel2.writeNoException();
                                          return true;
                                        case 79:
                                          arrayOfByte2.enforceInterface("android.os.storage.IStorageManager");
                                          str9 = arrayOfByte2.readString();
                                          l1 = arrayOfByte2.readLong();
                                          m = arrayOfByte2.readInt();
                                          str4 = arrayOfByte2.readString();
                                          allocateBytes(str9, l1, m, str4);
                                          param1Parcel2.writeNoException();
                                          return true;
                                        case 78:
                                          str4.enforceInterface("android.os.storage.IStorageManager");
                                          str9 = str4.readString();
                                          m = str4.readInt();
                                          str4 = str4.readString();
                                          l1 = getAllocatableBytes(str9, m, str4);
                                          param1Parcel2.writeNoException();
                                          param1Parcel2.writeLong(l1);
                                          return true;
                                        case 77:
                                          str4.enforceInterface("android.os.storage.IStorageManager");
                                          str9 = str4.readString();
                                          m = str4.readInt();
                                          l1 = getCacheSizeBytes(str9, m);
                                          param1Parcel2.writeNoException();
                                          param1Parcel2.writeLong(l1);
                                          return true;
                                        case 76:
                                          str4.enforceInterface("android.os.storage.IStorageManager");
                                          str9 = str4.readString();
                                          m = str4.readInt();
                                          l1 = getCacheQuotaBytes(str9, m);
                                          param1Parcel2.writeNoException();
                                          param1Parcel2.writeLong(l1);
                                          return true;
                                        case 75:
                                          str4.enforceInterface("android.os.storage.IStorageManager");
                                          m = str4.readInt();
                                          param1Int2 = str4.readInt();
                                          i1 = str4.readInt();
                                          parcelFileDescriptor = openProxyFileDescriptor(m, param1Int2, i1);
                                          param1Parcel2.writeNoException();
                                          if (parcelFileDescriptor != null) {
                                            param1Parcel2.writeInt(1);
                                            parcelFileDescriptor.writeToParcel(param1Parcel2, 1);
                                          } else {
                                            param1Parcel2.writeInt(0);
                                          } 
                                          return true;
                                        case 74:
                                          parcelFileDescriptor.enforceInterface("android.os.storage.IStorageManager");
                                          appFuseMount = mountProxyFileDescriptorBridge();
                                          param1Parcel2.writeNoException();
                                          if (appFuseMount != null) {
                                            param1Parcel2.writeInt(1);
                                            appFuseMount.writeToParcel(param1Parcel2, 1);
                                          } else {
                                            param1Parcel2.writeInt(0);
                                          } 
                                          return true;
                                        case 73:
                                          appFuseMount.enforceInterface("android.os.storage.IStorageManager");
                                          m = appFuseMount.readInt();
                                          iVoldTaskListener2 = IVoldTaskListener.Stub.asInterface(appFuseMount.readStrongBinder());
                                          fstrim(m, iVoldTaskListener2);
                                          param1Parcel2.writeNoException();
                                          return true;
                                        case 72:
                                          iVoldTaskListener2.enforceInterface("android.os.storage.IStorageManager");
                                          m = iVoldTaskListener2.readInt();
                                          fixateNewestUserKeyAuth(m);
                                          param1Parcel2.writeNoException();
                                          return true;
                                        case 71:
                                          break;
                                      } 
                                      iVoldTaskListener2.enforceInterface("android.os.storage.IStorageManager");
                                      m = iVoldTaskListener2.readInt();
                                      param1Int2 = iVoldTaskListener2.readInt();
                                      arrayOfByte4 = iVoldTaskListener2.createByteArray();
                                      arrayOfByte1 = iVoldTaskListener2.createByteArray();
                                      addUserKeyAuth(m, param1Int2, arrayOfByte4, arrayOfByte1);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 69:
                                      arrayOfByte1.enforceInterface("android.os.storage.IStorageManager");
                                      bool3 = isConvertibleToFBE();
                                      param1Parcel2.writeNoException();
                                      param1Parcel2.writeInt(bool3);
                                      return true;
                                    case 68:
                                      arrayOfByte1.enforceInterface("android.os.storage.IStorageManager");
                                      str8 = arrayOfByte1.readString();
                                      param1Int2 = arrayOfByte1.readInt();
                                      k = arrayOfByte1.readInt();
                                      destroyUserStorage(str8, param1Int2, k);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 67:
                                      arrayOfByte1.enforceInterface("android.os.storage.IStorageManager");
                                      str8 = arrayOfByte1.readString();
                                      k = arrayOfByte1.readInt();
                                      i1 = arrayOfByte1.readInt();
                                      param1Int2 = arrayOfByte1.readInt();
                                      prepareUserStorage(str8, k, i1, param1Int2);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 66:
                                      arrayOfByte1.enforceInterface("android.os.storage.IStorageManager");
                                      k = arrayOfByte1.readInt();
                                      bool2 = isUserKeyUnlocked(k);
                                      param1Parcel2.writeNoException();
                                      param1Parcel2.writeInt(bool2);
                                      return true;
                                    case 65:
                                      arrayOfByte1.enforceInterface("android.os.storage.IStorageManager");
                                      j = arrayOfByte1.readInt();
                                      lockUserKey(j);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 64:
                                      arrayOfByte1.enforceInterface("android.os.storage.IStorageManager");
                                      j = arrayOfByte1.readInt();
                                      param1Int2 = arrayOfByte1.readInt();
                                      arrayOfByte3 = arrayOfByte1.createByteArray();
                                      arrayOfByte1 = arrayOfByte1.createByteArray();
                                      unlockUserKey(j, param1Int2, arrayOfByte3, arrayOfByte1);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 63:
                                      arrayOfByte1.enforceInterface("android.os.storage.IStorageManager");
                                      j = arrayOfByte1.readInt();
                                      destroyUserKey(j);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 62:
                                      arrayOfByte1.enforceInterface("android.os.storage.IStorageManager");
                                      j = arrayOfByte1.readInt();
                                      param1Int2 = arrayOfByte1.readInt();
                                      bool8 = bool6;
                                      if (arrayOfByte1.readInt() != 0)
                                        bool8 = true; 
                                      createUserKey(j, param1Int2, bool8);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 61:
                                      arrayOfByte1.enforceInterface("android.os.storage.IStorageManager");
                                      j = arrayOfByte1.readInt();
                                      param1Int2 = arrayOfByte1.readInt();
                                      setDebugFlags(j, param1Int2);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 60:
                                      arrayOfByte1.enforceInterface("android.os.storage.IStorageManager");
                                      str7 = arrayOfByte1.readString();
                                      iVoldTaskListener1 = IVoldTaskListener.Stub.asInterface(arrayOfByte1.readStrongBinder());
                                      benchmark(str7, iVoldTaskListener1);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 59:
                                      iVoldTaskListener1.enforceInterface("android.os.storage.IStorageManager");
                                      str7 = iVoldTaskListener1.readString();
                                      iPackageMoveObserver = IPackageMoveObserver.Stub.asInterface(iVoldTaskListener1.readStrongBinder());
                                      setPrimaryStorageUuid(str7, iPackageMoveObserver);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 58:
                                      iPackageMoveObserver.enforceInterface("android.os.storage.IStorageManager");
                                      str3 = getPrimaryStorageUuid();
                                      param1Parcel2.writeNoException();
                                      param1Parcel2.writeString(str3);
                                      return true;
                                    case 57:
                                      str3.enforceInterface("android.os.storage.IStorageManager");
                                      forgetAllVolumes();
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 56:
                                      str3.enforceInterface("android.os.storage.IStorageManager");
                                      str3 = str3.readString();
                                      forgetVolume(str3);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 55:
                                      str3.enforceInterface("android.os.storage.IStorageManager");
                                      str7 = str3.readString();
                                      j = str3.readInt();
                                      param1Int2 = str3.readInt();
                                      setVolumeUserFlags(str7, j, param1Int2);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 54:
                                      str3.enforceInterface("android.os.storage.IStorageManager");
                                      str7 = str3.readString();
                                      str3 = str3.readString();
                                      setVolumeNickname(str7, str3);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 53:
                                      str3.enforceInterface("android.os.storage.IStorageManager");
                                      str7 = str3.readString();
                                      j = str3.readInt();
                                      partitionMixed(str7, j);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 52:
                                      str3.enforceInterface("android.os.storage.IStorageManager");
                                      str3 = str3.readString();
                                      partitionPrivate(str3);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 51:
                                      str3.enforceInterface("android.os.storage.IStorageManager");
                                      str3 = str3.readString();
                                      partitionPublic(str3);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 50:
                                      str3.enforceInterface("android.os.storage.IStorageManager");
                                      str3 = str3.readString();
                                      format(str3);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 49:
                                      str3.enforceInterface("android.os.storage.IStorageManager");
                                      str3 = str3.readString();
                                      unmount(str3);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 48:
                                      str3.enforceInterface("android.os.storage.IStorageManager");
                                      str3 = str3.readString();
                                      mount(str3);
                                      param1Parcel2.writeNoException();
                                      return true;
                                    case 47:
                                      str3.enforceInterface("android.os.storage.IStorageManager");
                                      j = str3.readInt();
                                      arrayOfVolumeRecord = getVolumeRecords(j);
                                      param1Parcel2.writeNoException();
                                      param1Parcel2.writeTypedArray(arrayOfVolumeRecord, 1);
                                      return true;
                                    case 46:
                                      arrayOfVolumeRecord.enforceInterface("android.os.storage.IStorageManager");
                                      j = arrayOfVolumeRecord.readInt();
                                      arrayOfVolumeInfo = getVolumes(j);
                                      param1Parcel2.writeNoException();
                                      param1Parcel2.writeTypedArray(arrayOfVolumeInfo, 1);
                                      return true;
                                    case 45:
                                      break;
                                  } 
                                  arrayOfVolumeInfo.enforceInterface("android.os.storage.IStorageManager");
                                  arrayOfDiskInfo = getDisks();
                                  param1Parcel2.writeNoException();
                                  param1Parcel2.writeTypedArray(arrayOfDiskInfo, 1);
                                  return true;
                                case 40:
                                  arrayOfDiskInfo.enforceInterface("android.os.storage.IStorageManager");
                                  str2 = arrayOfDiskInfo.readString();
                                  str2 = getField(str2);
                                  param1Parcel2.writeNoException();
                                  param1Parcel2.writeString(str2);
                                  return true;
                                case 39:
                                  str2.enforceInterface("android.os.storage.IStorageManager");
                                  str = str2.readString();
                                  str2 = str2.readString();
                                  setField(str, str2);
                                  return true;
                                case 38:
                                  str2.enforceInterface("android.os.storage.IStorageManager");
                                  clearPassword();
                                  return true;
                                case 37:
                                  str2.enforceInterface("android.os.storage.IStorageManager");
                                  str2 = getPassword();
                                  str.writeNoException();
                                  str.writeString(str2);
                                  return true;
                                case 36:
                                  str2.enforceInterface("android.os.storage.IStorageManager");
                                  j = getPasswordType();
                                  str.writeNoException();
                                  str.writeInt(j);
                                  return true;
                                case 35:
                                  break;
                              } 
                              str2.enforceInterface("android.os.storage.IStorageManager");
                              str7 = str2.readString();
                              str2 = str2.readString();
                              mkdirs(str7, str2);
                              str.writeNoException();
                              return true;
                            case 30:
                              str2.enforceInterface("android.os.storage.IStorageManager");
                              j = str2.readInt();
                              str7 = str2.readString();
                              param1Int2 = str2.readInt();
                              arrayOfStorageVolume = getVolumeList(j, str7, param1Int2);
                              str.writeNoException();
                              str.writeTypedArray(arrayOfStorageVolume, 1);
                              return true;
                            case 29:
                              arrayOfStorageVolume.enforceInterface("android.os.storage.IStorageManager");
                              j = arrayOfStorageVolume.readInt();
                              str1 = arrayOfStorageVolume.readString();
                              j = changeEncryptionPassword(j, str1);
                              str.writeNoException();
                              str.writeInt(j);
                              return true;
                            case 28:
                              str1.enforceInterface("android.os.storage.IStorageManager");
                              j = str1.readInt();
                              str1 = str1.readString();
                              j = encryptStorage(j, str1);
                              str.writeNoException();
                              str.writeInt(j);
                              return true;
                            case 27:
                              break;
                          } 
                          str1.enforceInterface("android.os.storage.IStorageManager");
                          str1 = str1.readString();
                          j = decryptStorage(str1);
                          str.writeNoException();
                          str.writeInt(j);
                          return true;
                        case 25:
                          str1.enforceInterface("android.os.storage.IStorageManager");
                          str1 = str1.readString();
                          str1 = getMountedObbPath(str1);
                          str.writeNoException();
                          str.writeString(str1);
                          return true;
                        case 24:
                          str1.enforceInterface("android.os.storage.IStorageManager");
                          str1 = str1.readString();
                          bool1 = isObbMounted(str1);
                          str.writeNoException();
                          str.writeInt(bool1);
                          return true;
                        case 23:
                          str1.enforceInterface("android.os.storage.IStorageManager");
                          str10 = str1.readString();
                          bool8 = bool7;
                          if (str1.readInt() != 0)
                            bool8 = true; 
                          iObbActionListener1 = IObbActionListener.Stub.asInterface(str1.readStrongBinder());
                          i = str1.readInt();
                          unmountObb(str10, bool8, iObbActionListener1, i);
                          str.writeNoException();
                          return true;
                        case 22:
                          break;
                      } 
                      str1.enforceInterface("android.os.storage.IStorageManager");
                      String str11 = str1.readString();
                      String str6 = str1.readString();
                      String str12 = str1.readString();
                      IObbActionListener iObbActionListener2 = IObbActionListener.Stub.asInterface(str1.readStrongBinder());
                      int i = str1.readInt();
                      if (str1.readInt() != 0) {
                        ObbInfo obbInfo = ObbInfo.CREATOR.createFromParcel((Parcel)str1);
                      } else {
                        str1 = null;
                      } 
                      mountObb(str11, str6, str12, iObbActionListener2, i, (ObbInfo)str1);
                      str.writeNoException();
                      return true;
                    } 
                    str1.enforceInterface("android.os.storage.IStorageManager");
                    runMaintenance();
                    str.writeNoException();
                    return true;
                  } 
                  str1.enforceInterface("android.os.storage.IStorageManager");
                  long l = lastMaintenance();
                  str.writeNoException();
                  str.writeLong(l);
                  return true;
                } 
                str1.enforceInterface("android.os.storage.IStorageManager");
                str1 = str1.readString();
                param1Int1 = verifyEncryptionPassword(str1);
                str.writeNoException();
                str.writeInt(param1Int1);
                return true;
              } 
              str1.enforceInterface("android.os.storage.IStorageManager");
              param1Int1 = getEncryptionState();
              str.writeNoException();
              str.writeInt(param1Int1);
              return true;
            } 
            str.writeString("android.os.storage.IStorageManager");
            return true;
          } 
          str1.enforceInterface("android.os.storage.IStorageManager");
          iStorageShutdownObserver = IStorageShutdownObserver.Stub.asInterface(str1.readStrongBinder());
          shutdown(iStorageShutdownObserver);
          str.writeNoException();
          return true;
        } 
        iStorageShutdownObserver.enforceInterface("android.os.storage.IStorageManager");
        iStorageEventListener = IStorageEventListener.Stub.asInterface(iStorageShutdownObserver.readStrongBinder());
        unregisterListener(iStorageEventListener);
        str.writeNoException();
        return true;
      } 
      iStorageEventListener.enforceInterface("android.os.storage.IStorageManager");
      IStorageEventListener iStorageEventListener = IStorageEventListener.Stub.asInterface(iStorageEventListener.readStrongBinder());
      registerListener(iStorageEventListener);
      str.writeNoException();
      return true;
    }
    
    private static class Proxy implements IStorageManager {
      public static IStorageManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.storage.IStorageManager";
      }
      
      public void registerListener(IStorageEventListener param2IStorageEventListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          if (param2IStorageEventListener != null) {
            iBinder = param2IStorageEventListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().registerListener(param2IStorageEventListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterListener(IStorageEventListener param2IStorageEventListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          if (param2IStorageEventListener != null) {
            iBinder = param2IStorageEventListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().unregisterListener(param2IStorageEventListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void shutdown(IStorageShutdownObserver param2IStorageShutdownObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          if (param2IStorageShutdownObserver != null) {
            iBinder = param2IStorageShutdownObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().shutdown(param2IStorageShutdownObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void mountObb(String param2String1, String param2String2, String param2String3, IObbActionListener param2IObbActionListener, int param2Int, ObbInfo param2ObbInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                IBinder iBinder;
                parcel1.writeString(param2String3);
                if (param2IObbActionListener != null) {
                  iBinder = param2IObbActionListener.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel1.writeStrongBinder(iBinder);
                try {
                  parcel1.writeInt(param2Int);
                  if (param2ObbInfo != null) {
                    parcel1.writeInt(1);
                    param2ObbInfo.writeToParcel(parcel1, 0);
                  } else {
                    parcel1.writeInt(0);
                  } 
                  try {
                    boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
                    if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
                      IStorageManager.Stub.getDefaultImpl().mountObb(param2String1, param2String2, param2String3, param2IObbActionListener, param2Int, param2ObbInfo);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void unmountObb(String param2String, boolean param2Boolean, IObbActionListener param2IObbActionListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          if (param2IObbActionListener != null) {
            iBinder = param2IObbActionListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool1 && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().unmountObb(param2String, param2Boolean, param2IObbActionListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isObbMounted(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(24, parcel1, parcel2, 0);
          if (!bool2 && IStorageManager.Stub.getDefaultImpl() != null) {
            bool1 = IStorageManager.Stub.getDefaultImpl().isObbMounted(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getMountedObbPath(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            param2String = IStorageManager.Stub.getDefaultImpl().getMountedObbPath(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int decryptStorage(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().decryptStorage(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int encryptStorage(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            param2Int = IStorageManager.Stub.getDefaultImpl().encryptStorage(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int changeEncryptionPassword(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            param2Int = IStorageManager.Stub.getDefaultImpl().changeEncryptionPassword(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StorageVolume[] getVolumeList(int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().getVolumeList(param2Int1, param2String, param2Int2); 
          parcel2.readException();
          return parcel2.<StorageVolume>createTypedArray(StorageVolume.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getEncryptionState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().getEncryptionState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int verifyEncryptionPassword(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().verifyEncryptionPassword(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void mkdirs(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().mkdirs(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPasswordType() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().getPasswordType(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getPassword() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().getPassword(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearPassword() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.storage.IStorageManager");
          boolean bool = this.mRemote.transact(38, parcel, (Parcel)null, 1);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().clearPassword();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setField(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(39, parcel, (Parcel)null, 1);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().setField(param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public String getField(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            param2String = IStorageManager.Stub.getDefaultImpl().getField(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long lastMaintenance() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().lastMaintenance(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void runMaintenance() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().runMaintenance();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public DiskInfo[] getDisks() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().getDisks(); 
          parcel2.readException();
          return parcel2.<DiskInfo>createTypedArray(DiskInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public VolumeInfo[] getVolumes(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().getVolumes(param2Int); 
          parcel2.readException();
          return parcel2.<VolumeInfo>createTypedArray(VolumeInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public VolumeRecord[] getVolumeRecords(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().getVolumeRecords(param2Int); 
          parcel2.readException();
          return parcel2.<VolumeRecord>createTypedArray(VolumeRecord.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void mount(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().mount(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unmount(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().unmount(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void format(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().format(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void partitionPublic(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().partitionPublic(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void partitionPrivate(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().partitionPrivate(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void partitionMixed(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().partitionMixed(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVolumeNickname(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(54, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().setVolumeNickname(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVolumeUserFlags(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().setVolumeUserFlags(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void forgetVolume(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(56, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().forgetVolume(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void forgetAllVolumes() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          boolean bool = this.mRemote.transact(57, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().forgetAllVolumes();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getPrimaryStorageUuid() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          boolean bool = this.mRemote.transact(58, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().getPrimaryStorageUuid(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPrimaryStorageUuid(String param2String, IPackageMoveObserver param2IPackageMoveObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          if (param2IPackageMoveObserver != null) {
            iBinder = param2IPackageMoveObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().setPrimaryStorageUuid(param2String, param2IPackageMoveObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void benchmark(String param2String, IVoldTaskListener param2IVoldTaskListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          if (param2IVoldTaskListener != null) {
            iBinder = param2IVoldTaskListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(60, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().benchmark(param2String, param2IVoldTaskListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDebugFlags(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().setDebugFlags(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void createUserKey(int param2Int1, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool1 && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().createUserKey(param2Int1, param2Int2, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void destroyUserKey(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(63, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().destroyUserKey(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unlockUserKey(int param2Int1, int param2Int2, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeByteArray(param2ArrayOfbyte1);
          parcel1.writeByteArray(param2ArrayOfbyte2);
          boolean bool = this.mRemote.transact(64, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().unlockUserKey(param2Int1, param2Int2, param2ArrayOfbyte1, param2ArrayOfbyte2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void lockUserKey(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(65, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().lockUserKey(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUserKeyUnlocked(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(66, parcel1, parcel2, 0);
          if (!bool2 && IStorageManager.Stub.getDefaultImpl() != null) {
            bool1 = IStorageManager.Stub.getDefaultImpl().isUserKeyUnlocked(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void prepareUserStorage(String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(67, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().prepareUserStorage(param2String, param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void destroyUserStorage(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(68, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().destroyUserStorage(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isConvertibleToFBE() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(69, parcel1, parcel2, 0);
          if (!bool2 && IStorageManager.Stub.getDefaultImpl() != null) {
            bool1 = IStorageManager.Stub.getDefaultImpl().isConvertibleToFBE();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addUserKeyAuth(int param2Int1, int param2Int2, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeByteArray(param2ArrayOfbyte1);
          parcel1.writeByteArray(param2ArrayOfbyte2);
          boolean bool = this.mRemote.transact(71, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().addUserKeyAuth(param2Int1, param2Int2, param2ArrayOfbyte1, param2ArrayOfbyte2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fixateNewestUserKeyAuth(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(72, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().fixateNewestUserKeyAuth(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fstrim(int param2Int, IVoldTaskListener param2IVoldTaskListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int);
          if (param2IVoldTaskListener != null) {
            iBinder = param2IVoldTaskListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(73, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().fstrim(param2Int, param2IVoldTaskListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public AppFuseMount mountProxyFileDescriptorBridge() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          AppFuseMount appFuseMount;
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          boolean bool = this.mRemote.transact(74, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            appFuseMount = IStorageManager.Stub.getDefaultImpl().mountProxyFileDescriptorBridge();
            return appFuseMount;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            appFuseMount = AppFuseMount.CREATOR.createFromParcel(parcel2);
          } else {
            appFuseMount = null;
          } 
          return appFuseMount;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelFileDescriptor openProxyFileDescriptor(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParcelFileDescriptor parcelFileDescriptor;
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(75, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            parcelFileDescriptor = IStorageManager.Stub.getDefaultImpl().openProxyFileDescriptor(param2Int1, param2Int2, param2Int3);
            return parcelFileDescriptor;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            parcelFileDescriptor = null;
          } 
          return parcelFileDescriptor;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getCacheQuotaBytes(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(76, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().getCacheQuotaBytes(param2String, param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getCacheSizeBytes(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(77, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().getCacheSizeBytes(param2String, param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getAllocatableBytes(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(78, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null)
            return IStorageManager.Stub.getDefaultImpl().getAllocatableBytes(param2String1, param2Int, param2String2); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void allocateBytes(String param2String1, long param2Long, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String1);
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(79, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().allocateBytes(param2String1, param2Long, param2Int, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void runIdleMaintenance() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          boolean bool = this.mRemote.transact(80, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().runIdleMaintenance();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void abortIdleMaintenance() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          boolean bool = this.mRemote.transact(81, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().abortIdleMaintenance();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void commitChanges() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          boolean bool = this.mRemote.transact(84, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().commitChanges();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean supportsCheckpoint() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(85, parcel1, parcel2, 0);
          if (!bool2 && IStorageManager.Stub.getDefaultImpl() != null) {
            bool1 = IStorageManager.Stub.getDefaultImpl().supportsCheckpoint();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startCheckpoint(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(86, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().startCheckpoint(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean needsCheckpoint() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(87, parcel1, parcel2, 0);
          if (!bool2 && IStorageManager.Stub.getDefaultImpl() != null) {
            bool1 = IStorageManager.Stub.getDefaultImpl().needsCheckpoint();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void abortChanges(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(88, parcel1, parcel2, 0);
          if (!bool1 && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().abortChanges(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearUserKeyAuth(int param2Int1, int param2Int2, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeByteArray(param2ArrayOfbyte1);
          parcel1.writeByteArray(param2ArrayOfbyte2);
          boolean bool = this.mRemote.transact(89, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().clearUserKeyAuth(param2Int1, param2Int2, param2ArrayOfbyte1, param2ArrayOfbyte2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fixupAppDir(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(90, parcel1, parcel2, 0);
          if (!bool && IStorageManager.Stub.getDefaultImpl() != null) {
            IStorageManager.Stub.getDefaultImpl().fixupAppDir(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IStorageManager param1IStorageManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IStorageManager != null) {
          Proxy.sDefaultImpl = param1IStorageManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IStorageManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
