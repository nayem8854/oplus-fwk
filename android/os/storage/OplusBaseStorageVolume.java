package android.os.storage;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.IndentingPrintWriter;

public abstract class OplusBaseStorageVolume implements Parcelable {
  public int mReadonlyType;
  
  protected OplusBaseStorageVolume() {}
  
  protected OplusBaseStorageVolume(int paramInt) {
    this.mReadonlyType = paramInt;
  }
  
  public int getReadOnlyType() {
    return this.mReadonlyType;
  }
  
  protected void setReadOnlyType(int paramInt) {
    this.mReadonlyType = paramInt;
  }
  
  protected void initFromParcel(Parcel paramParcel) {
    this.mReadonlyType = paramParcel.readInt();
  }
  
  public void dump(IndentingPrintWriter paramIndentingPrintWriter) {
    paramIndentingPrintWriter.printPair("mReadonlyType", Integer.valueOf(this.mReadonlyType));
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mReadonlyType);
  }
}
