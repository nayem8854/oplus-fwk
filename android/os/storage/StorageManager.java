package android.os.storage;

import android.annotation.SystemApi;
import android.app.ActivityThread;
import android.app.AppGlobals;
import android.app.AppOpsManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.content.pm.IPackageMoveObserver;
import android.content.res.ObbInfo;
import android.content.res.ObbScanner;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Environment;
import android.os.FileUtils;
import android.os.Handler;
import android.os.IBinder;
import android.os.IVoldTaskListener;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.ParcelableException;
import android.os.PersistableBundle;
import android.os.Process;
import android.os.ProxyFileDescriptorCallback;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.sysprop.VoldProperties;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.text.TextUtils;
import android.util.DataUnit;
import android.util.Log;
import android.util.Pair;
import android.util.Slog;
import android.util.SparseArray;
import com.android.internal.os.FuseAppLoop;
import com.android.internal.os.RoSystemProperties;
import com.android.internal.util.Preconditions;
import dalvik.system.BlockGuard;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class StorageManager {
  private static final boolean LOCAL_LOGV = Log.isLoggable("StorageManager", 2);
  
  public static final String UUID_PRIVATE_INTERNAL = null;
  
  static {
    UUID_DEFAULT = UUID.fromString("41217664-9172-527a-b3d5-edabb50a7d69");
    UUID_PRIMARY_PHYSICAL_ = UUID.fromString("0f95a519-dae7-5abf-9519-fbd6209e05fd");
    UUID_SYSTEM_ = UUID.fromString("5d258386-e60d-59e3-826d-0089cdd42cc0");
    sStorageManager = null;
    DEFAULT_THRESHOLD_MAX_BYTES = DataUnit.MEBIBYTES.toBytes(500L);
    DEFAULT_CACHE_MAX_BYTES = DataUnit.GIBIBYTES.toBytes(5L);
    DEFAULT_FULL_THRESHOLD_BYTES = DataUnit.MEBIBYTES.toBytes(1L);
  }
  
  private final AtomicInteger mNextNonce = new AtomicInteger(0);
  
  private final ArrayList<StorageEventListenerDelegate> mDelegates = new ArrayList<>();
  
  class StorageEventListenerDelegate extends IStorageEventListener.Stub {
    final StorageManager.StorageVolumeCallback mCallback;
    
    final Executor mExecutor;
    
    final StorageEventListener mListener;
    
    final StorageManager this$0;
    
    public StorageEventListenerDelegate(Executor param1Executor, StorageEventListener param1StorageEventListener, StorageManager.StorageVolumeCallback param1StorageVolumeCallback) {
      this.mExecutor = param1Executor;
      this.mListener = param1StorageEventListener;
      this.mCallback = param1StorageVolumeCallback;
    }
    
    public void onUsbMassStorageConnectionChanged(boolean param1Boolean) throws RemoteException {
      this.mExecutor.execute(new _$$Lambda$StorageManager$StorageEventListenerDelegate$CPp_W20gR1fGCkqpcUmxn_igDvo(this, param1Boolean));
    }
    
    public void onStorageStateChanged(String param1String1, String param1String2, String param1String3) {
      this.mExecutor.execute(new _$$Lambda$StorageManager$StorageEventListenerDelegate$GoEFKT1rhv7KuSkGeH69DO738lA(this, param1String1, param1String2, param1String3));
    }
    
    public void onVolumeStateChanged(VolumeInfo param1VolumeInfo, int param1Int1, int param1Int2) {
      this.mExecutor.execute(new _$$Lambda$StorageManager$StorageEventListenerDelegate$pyZP4UQS232_tqmtk5lSCyZx9qU(this, param1VolumeInfo, param1Int1, param1Int2));
    }
    
    public void onVolumeRecordChanged(VolumeRecord param1VolumeRecord) {
      this.mExecutor.execute(new _$$Lambda$StorageManager$StorageEventListenerDelegate$4eBMYk2W7Kja0aFJXtF_mwY_0ZA(this, param1VolumeRecord));
    }
    
    public void onVolumeForgotten(String param1String) {
      this.mExecutor.execute(new _$$Lambda$StorageManager$StorageEventListenerDelegate$yvuDY9iE3nJ_9H3lnLN54G_Lqb8(this, param1String));
    }
    
    public void onDiskScanned(DiskInfo param1DiskInfo, int param1Int) {
      this.mExecutor.execute(new _$$Lambda$StorageManager$StorageEventListenerDelegate$omFTtK_HVLnUiKLog2nIDD0yvGg(this, param1DiskInfo, param1Int));
    }
    
    public void onDiskDestroyed(DiskInfo param1DiskInfo) throws RemoteException {
      this.mExecutor.execute(new _$$Lambda$StorageManager$StorageEventListenerDelegate$WWIVLHEAL8WGcsLlQCXFZ9b3vCg(this, param1DiskInfo));
    }
  }
  
  private final ObbActionListener mObbActionListener = new ObbActionListener();
  
  public static final String ACTION_CLEAR_APP_CACHE = "android.os.storage.action.CLEAR_APP_CACHE";
  
  public static final String ACTION_MANAGE_STORAGE = "android.os.storage.action.MANAGE_STORAGE";
  
  public static final String ACTION_MEDIA_MOUNTED_RO = "android.intent.action.MEDIA_MOUNTED_RO";
  
  public static final int CRYPT_TYPE_DEFAULT = 1;
  
  public static final int CRYPT_TYPE_PASSWORD = 0;
  
  public static final int CRYPT_TYPE_PATTERN = 2;
  
  public static final int CRYPT_TYPE_PIN = 3;
  
  public static final int DEBUG_ADOPTABLE_FORCE_OFF = 2;
  
  public static final int DEBUG_ADOPTABLE_FORCE_ON = 1;
  
  public static final int DEBUG_EMULATE_FBE = 4;
  
  public static final int DEBUG_ISOLATED_STORAGE_FORCE_OFF = 128;
  
  public static final int DEBUG_ISOLATED_STORAGE_FORCE_ON = 64;
  
  public static final int DEBUG_SDCARDFS_FORCE_OFF = 16;
  
  public static final int DEBUG_SDCARDFS_FORCE_ON = 8;
  
  public static final int DEBUG_VIRTUAL_DISK = 32;
  
  private static final long DEFAULT_CACHE_MAX_BYTES;
  
  private static final int DEFAULT_CACHE_PERCENTAGE = 10;
  
  private static final long DEFAULT_FULL_THRESHOLD_BYTES;
  
  private static final long DEFAULT_THRESHOLD_MAX_BYTES;
  
  private static final int DEFAULT_THRESHOLD_PERCENTAGE = 5;
  
  public static final int ENCRYPTION_STATE_ERROR_CORRUPT = -4;
  
  public static final int ENCRYPTION_STATE_ERROR_INCOMPLETE = -2;
  
  public static final int ENCRYPTION_STATE_ERROR_INCONSISTENT = -3;
  
  public static final int ENCRYPTION_STATE_ERROR_UNKNOWN = -1;
  
  public static final int ENCRYPTION_STATE_NONE = 1;
  
  public static final int ENCRYPTION_STATE_OK = 0;
  
  public static final String EXTRA_REQUESTED_BYTES = "android.os.storage.extra.REQUESTED_BYTES";
  
  public static final String EXTRA_UUID = "android.os.storage.extra.UUID";
  
  @SystemApi
  public static final int FLAG_ALLOCATE_AGGRESSIVE = 1;
  
  public static final int FLAG_ALLOCATE_CACHE_ONLY = 16;
  
  public static final int FLAG_ALLOCATE_DEFY_ALL_RESERVED = 2;
  
  public static final int FLAG_ALLOCATE_DEFY_HALF_RESERVED = 4;
  
  public static final int FLAG_ALLOCATE_NON_CACHE_ONLY = 8;
  
  public static final int FLAG_FOR_WRITE = 256;
  
  public static final int FLAG_INCLUDE_INVISIBLE = 1024;
  
  public static final int FLAG_INCLUDE_RECENT = 2048;
  
  public static final int FLAG_REAL_STATE = 512;
  
  public static final int FLAG_STORAGE_CE = 2;
  
  public static final int FLAG_STORAGE_DE = 1;
  
  public static final int FLAG_STORAGE_EXTERNAL = 4;
  
  public static final int FSTRIM_FLAG_DEEP = 1;
  
  public static final String OWNER_INFO_KEY = "OwnerInfo";
  
  public static final String PASSWORD_VISIBLE_KEY = "PasswordVisible";
  
  public static final String PATTERN_VISIBLE_KEY = "PatternVisible";
  
  public static final int PROJECT_ID_EXT_DEFAULT = 1000;
  
  public static final int PROJECT_ID_EXT_MEDIA_AUDIO = 1001;
  
  public static final int PROJECT_ID_EXT_MEDIA_IMAGE = 1003;
  
  public static final int PROJECT_ID_EXT_MEDIA_VIDEO = 1002;
  
  public static final String PROP_ADOPTABLE = "persist.sys.adoptable";
  
  public static final String PROP_EMULATE_FBE = "persist.sys.emulate_fbe";
  
  public static final String PROP_FORCED_SCOPED_STORAGE_WHITELIST = "forced_scoped_storage_whitelist";
  
  public static final String PROP_FUSE = "persist.sys.fuse";
  
  public static final String PROP_HAS_ADOPTABLE = "vold.has_adoptable";
  
  public static final String PROP_HAS_RESERVED = "vold.has_reserved";
  
  public static final String PROP_ISOLATED_STORAGE = "persist.sys.isolated_storage";
  
  public static final String PROP_ISOLATED_STORAGE_SNAPSHOT = "sys.isolated_storage_snapshot";
  
  public static final String PROP_PRIMARY_PHYSICAL = "ro.vold.primary_physical";
  
  public static final String PROP_SDCARDFS = "persist.sys.sdcardfs";
  
  public static final String PROP_SETTINGS_FUSE = "persist.sys.fflag.override.settings_fuse";
  
  public static final String PROP_VIRTUAL_DISK = "persist.sys.virtual_disk";
  
  @SystemApi
  public static final int QUOTA_TYPE_MEDIA_AUDIO = 2;
  
  @SystemApi
  public static final int QUOTA_TYPE_MEDIA_IMAGE = 1;
  
  @SystemApi
  public static final int QUOTA_TYPE_MEDIA_NONE = 0;
  
  @SystemApi
  public static final int QUOTA_TYPE_MEDIA_VIDEO = 3;
  
  public static final String SYSTEM_LOCALE_KEY = "SystemLocale";
  
  private static final String TAG = "StorageManager";
  
  public static final UUID UUID_DEFAULT;
  
  public static final String UUID_PRIMARY_PHYSICAL = "primary_physical";
  
  public static final UUID UUID_PRIMARY_PHYSICAL_;
  
  public static final String UUID_SYSTEM = "system";
  
  public static final UUID UUID_SYSTEM_;
  
  private static final String XATTR_CACHE_GROUP = "user.cache_group";
  
  private static final String XATTR_CACHE_TOMBSTONE = "user.cache_tombstone";
  
  private static volatile IStorageManager sStorageManager;
  
  private final AppOpsManager mAppOps;
  
  private final Context mContext;
  
  private FuseAppLoop mFuseAppLoop;
  
  private final Object mFuseAppLoopLock;
  
  private final Looper mLooper;
  
  private final ContentResolver mResolver;
  
  private final IStorageManager mStorageManager;
  
  class ObbActionListener extends IObbActionListener.Stub {
    private SparseArray<StorageManager.ObbListenerDelegate> mListeners = new SparseArray();
    
    final StorageManager this$0;
    
    public void onObbResult(String param1String, int param1Int1, int param1Int2) {
      synchronized (this.mListeners) {
        StorageManager.ObbListenerDelegate obbListenerDelegate = (StorageManager.ObbListenerDelegate)this.mListeners.get(param1Int1);
        if (obbListenerDelegate != null)
          this.mListeners.remove(param1Int1); 
        if (obbListenerDelegate != null)
          obbListenerDelegate.sendObbStateChanged(param1String, param1Int2); 
        return;
      } 
    }
    
    public int addListener(OnObbStateChangeListener param1OnObbStateChangeListener) {
      null = new StorageManager.ObbListenerDelegate(param1OnObbStateChangeListener);
      synchronized (this.mListeners) {
        this.mListeners.put(null.nonce, null);
        return null.nonce;
      } 
    }
    
    private ObbActionListener() {}
  }
  
  private int getNextNonce() {
    return this.mNextNonce.getAndIncrement();
  }
  
  private class ObbListenerDelegate {
    private final Handler mHandler;
    
    private final WeakReference<OnObbStateChangeListener> mObbEventListenerRef;
    
    private final int nonce;
    
    final StorageManager this$0;
    
    ObbListenerDelegate(OnObbStateChangeListener param1OnObbStateChangeListener) {
      this.nonce = StorageManager.this.getNextNonce();
      this.mObbEventListenerRef = new WeakReference<>(param1OnObbStateChangeListener);
      this.mHandler = (Handler)new Object(this, StorageManager.this.mLooper, StorageManager.this);
    }
    
    OnObbStateChangeListener getListener() {
      WeakReference<OnObbStateChangeListener> weakReference = this.mObbEventListenerRef;
      if (weakReference == null)
        return null; 
      return weakReference.get();
    }
    
    void sendObbStateChanged(String param1String, int param1Int) {
      this.mHandler.obtainMessage(0, param1Int, 0, param1String).sendToTarget();
    }
  }
  
  @Deprecated
  public static StorageManager from(Context paramContext) {
    return (StorageManager)paramContext.getSystemService(StorageManager.class);
  }
  
  public void registerListener(StorageEventListener paramStorageEventListener) {
    synchronized (this.mDelegates) {
      StorageEventListenerDelegate storageEventListenerDelegate = new StorageEventListenerDelegate();
      Context context = this.mContext;
      Executor executor = context.getMainExecutor();
      StorageVolumeCallback storageVolumeCallback = new StorageVolumeCallback();
      this();
      this(this, executor, paramStorageEventListener, storageVolumeCallback);
      try {
        this.mStorageManager.registerListener(storageEventListenerDelegate);
        this.mDelegates.add(storageEventListenerDelegate);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  public void unregisterListener(StorageEventListener paramStorageEventListener) {
    synchronized (this.mDelegates) {
      for (Iterator<StorageEventListenerDelegate> iterator = this.mDelegates.iterator(); iterator.hasNext(); ) {
        StorageEventListenerDelegate storageEventListenerDelegate = iterator.next();
        StorageEventListener storageEventListener = storageEventListenerDelegate.mListener;
        if (storageEventListener == paramStorageEventListener)
          try {
            this.mStorageManager.unregisterListener(storageEventListenerDelegate);
            iterator.remove();
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowFromSystemServer();
          }  
      } 
      return;
    } 
  }
  
  public static class StorageVolumeCallback {
    public void onStateChanged(StorageVolume param1StorageVolume) {}
  }
  
  public void registerStorageVolumeCallback(Executor paramExecutor, StorageVolumeCallback paramStorageVolumeCallback) {
    synchronized (this.mDelegates) {
      StorageEventListenerDelegate storageEventListenerDelegate = new StorageEventListenerDelegate();
      StorageEventListener storageEventListener = new StorageEventListener();
      this();
      this(this, paramExecutor, storageEventListener, paramStorageVolumeCallback);
      try {
        this.mStorageManager.registerListener(storageEventListenerDelegate);
        this.mDelegates.add(storageEventListenerDelegate);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  public void unregisterStorageVolumeCallback(StorageVolumeCallback paramStorageVolumeCallback) {
    synchronized (this.mDelegates) {
      for (Iterator<StorageEventListenerDelegate> iterator = this.mDelegates.iterator(); iterator.hasNext(); ) {
        StorageEventListenerDelegate storageEventListenerDelegate = iterator.next();
        StorageVolumeCallback storageVolumeCallback = storageEventListenerDelegate.mCallback;
        if (storageVolumeCallback == paramStorageVolumeCallback)
          try {
            this.mStorageManager.unregisterListener(storageEventListenerDelegate);
            iterator.remove();
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowFromSystemServer();
          }  
      } 
      return;
    } 
  }
  
  @Deprecated
  public void enableUsbMassStorage() {}
  
  @Deprecated
  public void disableUsbMassStorage() {}
  
  @Deprecated
  public boolean isUsbMassStorageConnected() {
    return false;
  }
  
  @Deprecated
  public boolean isUsbMassStorageEnabled() {
    return false;
  }
  
  public boolean mountObb(String paramString1, String paramString2, OnObbStateChangeListener paramOnObbStateChangeListener) {
    Preconditions.checkNotNull(paramString1, "rawPath cannot be null");
    Preconditions.checkNotNull(paramOnObbStateChangeListener, "listener cannot be null");
    try {
      File file = new File();
      this(paramString1);
      String str = file.getCanonicalPath();
      int i = this.mObbActionListener.addListener(paramOnObbStateChangeListener);
      IStorageManager iStorageManager = this.mStorageManager;
      ObbActionListener obbActionListener = this.mObbActionListener;
      ObbInfo obbInfo = getObbInfo(str);
      iStorageManager.mountObb(paramString1, str, paramString2, obbActionListener, i, obbInfo);
      return true;
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to resolve path: ");
      stringBuilder.append(paramString1);
      throw new IllegalArgumentException(stringBuilder.toString(), iOException);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private ObbInfo getObbInfo(String paramString) {
    try {
      return ObbScanner.getObbInfo(paramString);
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't get OBB info for ");
      stringBuilder.append(paramString);
      throw new IllegalArgumentException(stringBuilder.toString(), iOException);
    } 
  }
  
  public boolean unmountObb(String paramString, boolean paramBoolean, OnObbStateChangeListener paramOnObbStateChangeListener) {
    Preconditions.checkNotNull(paramString, "rawPath cannot be null");
    Preconditions.checkNotNull(paramOnObbStateChangeListener, "listener cannot be null");
    try {
      int i = this.mObbActionListener.addListener(paramOnObbStateChangeListener);
      this.mStorageManager.unmountObb(paramString, paramBoolean, this.mObbActionListener, i);
      return true;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isObbMounted(String paramString) {
    Preconditions.checkNotNull(paramString, "rawPath cannot be null");
    try {
      return this.mStorageManager.isObbMounted(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getMountedObbPath(String paramString) {
    Preconditions.checkNotNull(paramString, "rawPath cannot be null");
    try {
      return this.mStorageManager.getMountedObbPath(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<DiskInfo> getDisks() {
    try {
      return Arrays.asList(this.mStorageManager.getDisks());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public DiskInfo findDiskById(String paramString) {
    Preconditions.checkNotNull(paramString);
    for (DiskInfo diskInfo : getDisks()) {
      if (Objects.equals(diskInfo.id, paramString))
        return diskInfo; 
    } 
    return null;
  }
  
  public VolumeInfo findVolumeById(String paramString) {
    Preconditions.checkNotNull(paramString);
    for (VolumeInfo volumeInfo : getVolumes()) {
      if (Objects.equals(volumeInfo.id, paramString))
        return volumeInfo; 
    } 
    return null;
  }
  
  public VolumeInfo findVolumeByUuid(String paramString) {
    Preconditions.checkNotNull(paramString);
    for (VolumeInfo volumeInfo : getVolumes()) {
      if (Objects.equals(volumeInfo.fsUuid, paramString))
        return volumeInfo; 
    } 
    return null;
  }
  
  public VolumeRecord findRecordByUuid(String paramString) {
    Preconditions.checkNotNull(paramString);
    for (VolumeRecord volumeRecord : getVolumeRecords()) {
      if (Objects.equals(volumeRecord.fsUuid, paramString))
        return volumeRecord; 
    } 
    return null;
  }
  
  public VolumeInfo findPrivateForEmulated(VolumeInfo paramVolumeInfo) {
    if (paramVolumeInfo != null) {
      String str2 = paramVolumeInfo.getId();
      int i = str2.indexOf(";");
      String str1 = str2;
      if (i != -1)
        str1 = str2.substring(0, i); 
      return findVolumeById(str1.replace("emulated", "private"));
    } 
    return null;
  }
  
  public VolumeInfo findEmulatedForPrivate(VolumeInfo paramVolumeInfo) {
    if (paramVolumeInfo != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramVolumeInfo.getId().replace("private", "emulated"));
      stringBuilder.append(";");
      Context context = this.mContext;
      stringBuilder.append(context.getUserId());
      String str = stringBuilder.toString();
      return findVolumeById(str);
    } 
    return null;
  }
  
  public VolumeInfo findVolumeByQualifiedUuid(String paramString) {
    if (Objects.equals(UUID_PRIVATE_INTERNAL, paramString))
      return findVolumeById("private"); 
    if (Objects.equals("primary_physical", paramString))
      return getPrimaryPhysicalVolume(); 
    return findVolumeByUuid(paramString);
  }
  
  public UUID getUuidForPath(File paramFile) throws IOException {
    Preconditions.checkNotNull(paramFile);
    String str = paramFile.getCanonicalPath();
    if (FileUtils.contains(Environment.getDataDirectory().getAbsolutePath(), str))
      return UUID_DEFAULT; 
    try {
      IStorageManager iStorageManager;
      VolumeInfo[] arrayOfVolumeInfo;
      byte b;
      int i;
      for (iStorageManager = this.mStorageManager, b = 0, arrayOfVolumeInfo = iStorageManager.getVolumes(0), i = arrayOfVolumeInfo.length; b < i; ) {
        VolumeInfo volumeInfo = arrayOfVolumeInfo[b];
        if (volumeInfo.path != null && FileUtils.contains(volumeInfo.path, str) && volumeInfo.type != 0) {
          int j = volumeInfo.type;
          if (j != 5)
            try {
              return convert(volumeInfo.fsUuid);
            } catch (IllegalArgumentException illegalArgumentException) {} 
        } 
        b++;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to find a storage device for ");
      stringBuilder.append(paramFile);
      throw new FileNotFoundException(stringBuilder.toString());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public File findPathForUuid(String paramString) throws FileNotFoundException {
    VolumeInfo volumeInfo = findVolumeByQualifiedUuid(paramString);
    if (volumeInfo != null)
      return volumeInfo.getPath(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to find a storage device for ");
    stringBuilder.append(paramString);
    throw new FileNotFoundException(stringBuilder.toString());
  }
  
  public boolean isAllocationSupported(FileDescriptor paramFileDescriptor) {
    try {
      getUuidForPath(ParcelFileDescriptor.getFile(paramFileDescriptor));
      return true;
    } catch (IOException iOException) {
      return false;
    } 
  }
  
  public List<VolumeInfo> getVolumes() {
    try {
      return Arrays.asList(this.mStorageManager.getVolumes(0));
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<VolumeInfo> getWritablePrivateVolumes() {
    try {
      ArrayList<VolumeInfo> arrayList = new ArrayList();
      this();
      VolumeInfo[] arrayOfVolumeInfo;
      IStorageManager iStorageManager;
      byte b;
      int i;
      for (iStorageManager = this.mStorageManager, b = 0, arrayOfVolumeInfo = iStorageManager.getVolumes(0), i = arrayOfVolumeInfo.length; b < i; ) {
        VolumeInfo volumeInfo = arrayOfVolumeInfo[b];
        if (volumeInfo.getType() == 1 && volumeInfo.isMountedWritable())
          arrayList.add(volumeInfo); 
        b++;
      } 
      return arrayList;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<VolumeRecord> getVolumeRecords() {
    try {
      return Arrays.asList(this.mStorageManager.getVolumeRecords(0));
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getBestVolumeDescription(VolumeInfo paramVolumeInfo) {
    if (paramVolumeInfo == null)
      return null; 
    if (!TextUtils.isEmpty(paramVolumeInfo.fsUuid)) {
      VolumeRecord volumeRecord = findRecordByUuid(paramVolumeInfo.fsUuid);
      if (volumeRecord != null && !TextUtils.isEmpty(volumeRecord.nickname))
        return volumeRecord.nickname; 
    } 
    if (paramVolumeInfo.disk != null && paramVolumeInfo.disk.isSd())
      return this.mContext.getString(17041372); 
    if (!TextUtils.isEmpty(paramVolumeInfo.getDescription()))
      return paramVolumeInfo.getDescription(); 
    if (paramVolumeInfo.disk != null)
      return paramVolumeInfo.disk.getDescription(); 
    return null;
  }
  
  public VolumeInfo getPrimaryPhysicalVolume() {
    List<VolumeInfo> list = getVolumes();
    for (VolumeInfo volumeInfo : list) {
      if (volumeInfo.isPrimaryPhysical())
        return volumeInfo; 
    } 
    return null;
  }
  
  public void mount(String paramString) {
    try {
      this.mStorageManager.mount(paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unmount(String paramString) {
    try {
      this.mStorageManager.unmount(paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void format(String paramString) {
    try {
      this.mStorageManager.format(paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public long benchmark(String paramString) {
    CompletableFuture<PersistableBundle> completableFuture = new CompletableFuture();
    benchmark(paramString, (IVoldTaskListener)new Object(this, completableFuture));
    try {
      long l = ((PersistableBundle)completableFuture.get(3L, TimeUnit.MINUTES)).getLong("run", Long.MAX_VALUE);
      return l * 1000000L;
    } catch (Exception exception) {
      return Long.MAX_VALUE;
    } 
  }
  
  public void benchmark(String paramString, IVoldTaskListener paramIVoldTaskListener) {
    try {
      this.mStorageManager.benchmark(paramString, paramIVoldTaskListener);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void partitionPublic(String paramString) {
    try {
      this.mStorageManager.partitionPublic(paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void partitionPrivate(String paramString) {
    try {
      this.mStorageManager.partitionPrivate(paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void partitionMixed(String paramString, int paramInt) {
    try {
      this.mStorageManager.partitionMixed(paramString, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void wipeAdoptableDisks() {
    List<DiskInfo> list = getDisks();
    for (DiskInfo diskInfo : list) {
      String str = diskInfo.getId();
      if (diskInfo.isAdoptable()) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Found adoptable ");
        stringBuilder1.append(str);
        stringBuilder1.append("; wiping");
        Slog.d("StorageManager", stringBuilder1.toString());
        try {
          this.mStorageManager.partitionPublic(str);
        } catch (Exception exception) {
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Failed to wipe ");
          stringBuilder2.append(str);
          stringBuilder2.append(", but soldiering onward");
          Slog.w("StorageManager", stringBuilder2.toString(), exception);
        } 
        continue;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Ignorning non-adoptable disk ");
      stringBuilder.append(exception.getId());
      Slog.d("StorageManager", stringBuilder.toString());
    } 
  }
  
  public void setVolumeNickname(String paramString1, String paramString2) {
    try {
      this.mStorageManager.setVolumeNickname(paramString1, paramString2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setVolumeInited(String paramString, boolean paramBoolean) {
    try {
      boolean bool;
      IStorageManager iStorageManager = this.mStorageManager;
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      iStorageManager.setVolumeUserFlags(paramString, bool, 1);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setVolumeSnoozed(String paramString, boolean paramBoolean) {
    try {
      boolean bool;
      IStorageManager iStorageManager = this.mStorageManager;
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      iStorageManager.setVolumeUserFlags(paramString, bool, 2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void forgetVolume(String paramString) {
    try {
      this.mStorageManager.forgetVolume(paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getPrimaryStorageUuid() {
    try {
      return this.mStorageManager.getPrimaryStorageUuid();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setPrimaryStorageUuid(String paramString, IPackageMoveObserver paramIPackageMoveObserver) {
    try {
      this.mStorageManager.setPrimaryStorageUuid(paramString, paramIPackageMoveObserver);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public StorageVolume getStorageVolume(File paramFile) {
    return getStorageVolume(getVolumeList(), paramFile);
  }
  
  public StorageVolume getStorageVolume(Uri paramUri) {
    String str1 = MediaStore.getVolumeName(paramUri);
    boolean bool = Objects.equals(str1, "external");
    byte b = 0;
    String str2 = str1;
    if (bool) {
      Cursor cursor = this.mContext.getContentResolver().query(paramUri, new String[] { "volume_name" }, null, null);
      try {
        if (cursor.moveToFirst())
          str1 = cursor.getString(0); 
        str2 = str1;
      } finally {
        if (cursor != null)
          try {
            cursor.close();
          } finally {
            str1 = null;
          }  
      } 
    } 
    if (str2.hashCode() != -1921573490 || !str2.equals("external_primary"))
      b = -1; 
    if (b != 0) {
      for (StorageVolume storageVolume : getStorageVolumes()) {
        if (Objects.equals(storageVolume.getMediaStoreVolumeName(), str2))
          return storageVolume; 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown volume for ");
      stringBuilder.append(paramUri);
      throw new IllegalStateException(stringBuilder.toString());
    } 
    return getPrimaryStorageVolume();
  }
  
  public static StorageVolume getStorageVolume(File paramFile, int paramInt) {
    return getStorageVolume(getVolumeList(paramInt, 0), paramFile);
  }
  
  private static StorageVolume getStorageVolume(StorageVolume[] paramArrayOfStorageVolume, File paramFile) {
    StorageManager storageManager;
    if (paramFile == null)
      return null; 
    String str = paramFile.getAbsolutePath();
    if (str.startsWith("/mnt/content/")) {
      Uri uri = ContentResolver.translateDeprecatedDataPath(str);
      storageManager = (StorageManager)AppGlobals.getInitialApplication().getSystemService(StorageManager.class);
      return storageManager.getStorageVolume(uri);
    } 
    try {
      File file = storageManager.getCanonicalFile();
      int i;
      byte b;
      for (i = paramArrayOfStorageVolume.length, b = 0; b < i; ) {
        StorageVolume storageVolume = paramArrayOfStorageVolume[b];
        File file1 = storageVolume.getPathFile();
        try {
          file1 = file1.getCanonicalFile();
          if (FileUtils.contains(file1, file))
            return storageVolume; 
        } catch (IOException iOException) {}
        b++;
      } 
      return null;
    } catch (IOException iOException1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Could not get canonical path for ");
      stringBuilder.append(iOException);
      Slog.d("StorageManager", stringBuilder.toString());
      return null;
    } 
  }
  
  @Deprecated
  public String getVolumeState(String paramString) {
    StorageVolume storageVolume = getStorageVolume(new File(paramString));
    if (storageVolume != null)
      return storageVolume.getState(); 
    return "unknown";
  }
  
  public List<StorageVolume> getStorageVolumes() {
    ArrayList<? super StorageVolume> arrayList = new ArrayList();
    Context context = this.mContext;
    StorageVolume[] arrayOfStorageVolume = getVolumeList(context.getUserId(), 1536);
    Collections.addAll(arrayList, arrayOfStorageVolume);
    return (List)arrayList;
  }
  
  public List<StorageVolume> getRecentStorageVolumes() {
    ArrayList<? super StorageVolume> arrayList = new ArrayList();
    Context context = this.mContext;
    StorageVolume[] arrayOfStorageVolume = getVolumeList(context.getUserId(), 3584);
    Collections.addAll(arrayList, arrayOfStorageVolume);
    return (List)arrayList;
  }
  
  public StorageVolume getPrimaryStorageVolume() {
    return getVolumeList(this.mContext.getUserId(), 1536)[0];
  }
  
  public static Pair<String, Long> getPrimaryStoragePathAndSize() {
    long l1 = Environment.getDataDirectory().getTotalSpace();
    long l2 = Environment.getRootDirectory().getTotalSpace();
    l1 = FileUtils.roundStorageSize(l1 + l2);
    return Pair.create(null, Long.valueOf(l1));
  }
  
  public long getPrimaryStorageSize() {
    long l1 = Environment.getDataDirectory().getTotalSpace();
    long l2 = Environment.getRootDirectory().getTotalSpace();
    return FileUtils.roundStorageSize(l1 + l2);
  }
  
  public void mkdirs(File paramFile) {
    BlockGuard.getVmPolicy().onPathAccess(paramFile.getAbsolutePath());
    try {
      this.mStorageManager.mkdirs(this.mContext.getOpPackageName(), paramFile.getAbsolutePath());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public StorageVolume[] getVolumeList() {
    return getVolumeList(this.mContext.getUserId(), 0);
  }
  
  public static StorageVolume[] getVolumeList(int paramInt1, int paramInt2) {
    IBinder iBinder = ServiceManager.getService("mount");
    IStorageManager iStorageManager = IStorageManager.Stub.asInterface(iBinder);
    try {
      String str2 = ActivityThread.currentOpPackageName();
      String str1 = str2;
      if (str2 == null) {
        IPackageManager iPackageManager = ActivityThread.getPackageManager();
        int i = Process.myUid();
        String[] arrayOfString = iPackageManager.getPackagesForUid(i);
        if (arrayOfString == null || arrayOfString.length <= 0) {
          Log.w("StorageManager", "Missing package names; no storage volumes available");
          return new StorageVolume[0];
        } 
        str1 = arrayOfString[0];
      } 
      paramInt1 = ActivityThread.getPackageManager().getPackageUid(str1, 268435456, paramInt1);
      if (paramInt1 <= 0) {
        Log.w("StorageManager", "Missing UID; no storage volumes available");
        return new StorageVolume[0];
      } 
      return iStorageManager.getVolumeList(paramInt1, str1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public String[] getVolumePaths() {
    StorageVolume[] arrayOfStorageVolume = getVolumeList();
    int i = arrayOfStorageVolume.length;
    String[] arrayOfString = new String[i];
    for (byte b = 0; b < i; b++)
      arrayOfString[b] = arrayOfStorageVolume[b].getPath(); 
    return arrayOfString;
  }
  
  public StorageVolume getPrimaryVolume() {
    return getPrimaryVolume(getVolumeList());
  }
  
  public static StorageVolume getPrimaryVolume(StorageVolume[] paramArrayOfStorageVolume) {
    int i;
    byte b;
    for (i = paramArrayOfStorageVolume.length, b = 0; b < i; ) {
      StorageVolume storageVolume = paramArrayOfStorageVolume[b];
      if (storageVolume.isPrimary())
        return storageVolume; 
      b++;
    } 
    throw new IllegalStateException("Missing primary storage");
  }
  
  public long getStorageBytesUntilLow(File paramFile) {
    return paramFile.getUsableSpace() - getStorageFullBytes(paramFile);
  }
  
  public long getStorageLowBytes(File paramFile) {
    long l1 = Settings.Global.getInt(this.mResolver, "sys_storage_threshold_percentage", 5);
    l1 = paramFile.getTotalSpace() * l1 / 100L;
    long l2 = Settings.Global.getLong(this.mResolver, "sys_storage_threshold_max_bytes", DEFAULT_THRESHOLD_MAX_BYTES);
    return Math.min(l1, l2);
  }
  
  public long getStorageCacheBytes(File paramFile, int paramInt) {
    long l1 = Settings.Global.getInt(this.mResolver, "sys_storage_cache_percentage", 10);
    l1 = paramFile.getTotalSpace() * l1 / 100L;
    long l2 = Settings.Global.getLong(this.mResolver, "sys_storage_cache_max_bytes", DEFAULT_CACHE_MAX_BYTES);
    l1 = Math.min(l1, l2);
    if ((paramInt & 0x1) != 0)
      return 0L; 
    if ((paramInt & 0x2) != 0)
      return 0L; 
    if ((paramInt & 0x4) != 0)
      return l1 / 2L; 
    return l1;
  }
  
  public long getStorageFullBytes(File paramFile) {
    return Settings.Global.getLong(this.mResolver, "sys_storage_full_threshold_bytes", DEFAULT_FULL_THRESHOLD_BYTES);
  }
  
  public void createUserKey(int paramInt1, int paramInt2, boolean paramBoolean) {
    try {
      this.mStorageManager.createUserKey(paramInt1, paramInt2, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void destroyUserKey(int paramInt) {
    try {
      this.mStorageManager.destroyUserKey(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unlockUserKey(int paramInt1, int paramInt2, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) {
    try {
      this.mStorageManager.unlockUserKey(paramInt1, paramInt2, paramArrayOfbyte1, paramArrayOfbyte2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void lockUserKey(int paramInt) {
    try {
      this.mStorageManager.lockUserKey(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void prepareUserStorage(String paramString, int paramInt1, int paramInt2, int paramInt3) {
    try {
      this.mStorageManager.prepareUserStorage(paramString, paramInt1, paramInt2, paramInt3);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void destroyUserStorage(String paramString, int paramInt1, int paramInt2) {
    try {
      this.mStorageManager.destroyUserStorage(paramString, paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean isUserKeyUnlocked(int paramInt) {
    Exception exception;
    if (sStorageManager == null)
      sStorageManager = IStorageManager.Stub.asInterface(ServiceManager.getService("mount")); 
    if (sStorageManager == null) {
      Slog.w("StorageManager", "Early during boot, assuming locked");
      return false;
    } 
    long l = Binder.clearCallingIdentity();
    try {
      boolean bool = sStorageManager.isUserKeyUnlocked(paramInt);
      Binder.restoreCallingIdentity(l);
      return bool;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } finally {}
    Binder.restoreCallingIdentity(l);
    throw exception;
  }
  
  public boolean isEncrypted(File paramFile) {
    if (FileUtils.contains(Environment.getDataDirectory(), paramFile))
      return isEncrypted(); 
    if (FileUtils.contains(Environment.getExpandDirectory(), paramFile))
      return true; 
    return false;
  }
  
  public static boolean isEncryptable() {
    return RoSystemProperties.CRYPTO_ENCRYPTABLE;
  }
  
  public static boolean isEncrypted() {
    return RoSystemProperties.CRYPTO_ENCRYPTED;
  }
  
  public static boolean isFileEncryptedNativeOnly() {
    if (!isEncrypted())
      return false; 
    return RoSystemProperties.CRYPTO_FILE_ENCRYPTED;
  }
  
  public static boolean isBlockEncrypted() {
    if (!isEncrypted())
      return false; 
    return RoSystemProperties.CRYPTO_BLOCK_ENCRYPTED;
  }
  
  public static boolean isNonDefaultBlockEncrypted() {
    boolean bool = isBlockEncrypted();
    boolean bool1 = false;
    if (!bool)
      return false; 
    try {
      IBinder iBinder = ServiceManager.getService("mount");
      IStorageManager iStorageManager = IStorageManager.Stub.asInterface(iBinder);
      int i = iStorageManager.getPasswordType();
      if (i != 1)
        bool1 = true; 
      return bool1;
    } catch (RemoteException remoteException) {
      Log.e("StorageManager", "Error getting encryption type");
      return false;
    } 
  }
  
  public static boolean isBlockEncrypting() {
    String str = VoldProperties.encrypt_progress().orElse("");
    return "".equalsIgnoreCase(str) ^ true;
  }
  
  public static boolean inCryptKeeperBounce() {
    String str = VoldProperties.decrypt().orElse("");
    return "trigger_restart_min_framework".equals(str);
  }
  
  public static boolean isFileEncryptedEmulatedOnly() {
    return SystemProperties.getBoolean("persist.sys.emulate_fbe", false);
  }
  
  public static boolean isFileEncryptedNativeOrEmulated() {
    return (isFileEncryptedNativeOnly() || 
      isFileEncryptedEmulatedOnly());
  }
  
  public static boolean hasAdoptable() {
    // Byte code:
    //   0: ldc 'persist.sys.adoptable'
    //   2: invokestatic get : (Ljava/lang/String;)Ljava/lang/String;
    //   5: astore_0
    //   6: aload_0
    //   7: invokevirtual hashCode : ()I
    //   10: istore_1
    //   11: iload_1
    //   12: ldc_w 464944051
    //   15: if_icmpeq -> 43
    //   18: iload_1
    //   19: ldc_w 1528363547
    //   22: if_icmpeq -> 28
    //   25: goto -> 58
    //   28: aload_0
    //   29: ldc_w 'force_off'
    //   32: invokevirtual equals : (Ljava/lang/Object;)Z
    //   35: ifeq -> 25
    //   38: iconst_1
    //   39: istore_1
    //   40: goto -> 60
    //   43: aload_0
    //   44: ldc_w 'force_on'
    //   47: invokevirtual equals : (Ljava/lang/Object;)Z
    //   50: ifeq -> 25
    //   53: iconst_0
    //   54: istore_1
    //   55: goto -> 60
    //   58: iconst_m1
    //   59: istore_1
    //   60: iload_1
    //   61: ifeq -> 78
    //   64: iload_1
    //   65: iconst_1
    //   66: if_icmpeq -> 76
    //   69: ldc 'vold.has_adoptable'
    //   71: iconst_0
    //   72: invokestatic getBoolean : (Ljava/lang/String;Z)Z
    //   75: ireturn
    //   76: iconst_0
    //   77: ireturn
    //   78: iconst_1
    //   79: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1707	-> 0
    //   #1713	-> 69
    //   #1711	-> 76
    //   #1709	-> 78
  }
  
  @SystemApi
  public static boolean hasIsolatedStorage() {
    boolean bool = SystemProperties.getBoolean("persist.sys.isolated_storage", true);
    return SystemProperties.getBoolean("sys.isolated_storage_snapshot", bool);
  }
  
  @Deprecated
  public static File maybeTranslateEmulatedPathToInternal(File paramFile) {
    return paramFile;
  }
  
  public File translateAppToSystem(File paramFile, int paramInt1, int paramInt2) {
    return paramFile;
  }
  
  public File translateSystemToApp(File paramFile, int paramInt1, int paramInt2) {
    return paramFile;
  }
  
  public static boolean checkPermissionAndAppOp(Context paramContext, boolean paramBoolean, int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, int paramInt3) {
    return checkPermissionAndAppOp(paramContext, paramBoolean, paramInt1, paramInt2, paramString1, paramString2, paramString3, paramInt3, true);
  }
  
  public static boolean checkPermissionAndCheckOp(Context paramContext, boolean paramBoolean, int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3) {
    return checkPermissionAndAppOp(paramContext, paramBoolean, paramInt1, paramInt2, paramString1, null, paramString2, paramInt3, false);
  }
  
  private static boolean checkPermissionAndAppOp(Context paramContext, boolean paramBoolean1, int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, int paramInt3, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: aload #6
    //   3: iload_2
    //   4: iload_3
    //   5: invokevirtual checkPermission : (Ljava/lang/String;II)I
    //   8: ifeq -> 67
    //   11: iload_1
    //   12: ifne -> 17
    //   15: iconst_0
    //   16: ireturn
    //   17: new java/lang/StringBuilder
    //   20: dup
    //   21: invokespecial <init> : ()V
    //   24: astore_0
    //   25: aload_0
    //   26: ldc_w 'Permission '
    //   29: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   32: pop
    //   33: aload_0
    //   34: aload #6
    //   36: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   39: pop
    //   40: aload_0
    //   41: ldc_w ' denied for package '
    //   44: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: aload_0
    //   49: aload #4
    //   51: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   54: pop
    //   55: new java/lang/SecurityException
    //   58: dup
    //   59: aload_0
    //   60: invokevirtual toString : ()Ljava/lang/String;
    //   63: invokespecial <init> : (Ljava/lang/String;)V
    //   66: athrow
    //   67: aload_0
    //   68: ldc_w android/app/AppOpsManager
    //   71: invokevirtual getSystemService : (Ljava/lang/Class;)Ljava/lang/Object;
    //   74: checkcast android/app/AppOpsManager
    //   77: astore_0
    //   78: iload #8
    //   80: ifeq -> 99
    //   83: aload_0
    //   84: iload #7
    //   86: iload_3
    //   87: aload #4
    //   89: aload #5
    //   91: aconst_null
    //   92: invokevirtual noteOpNoThrow : (IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    //   95: istore_2
    //   96: goto -> 116
    //   99: aload_0
    //   100: iload_3
    //   101: aload #4
    //   103: invokevirtual checkPackage : (ILjava/lang/String;)V
    //   106: aload_0
    //   107: iload #7
    //   109: iload_3
    //   110: aload #4
    //   112: invokevirtual checkOpNoThrow : (IILjava/lang/String;)I
    //   115: istore_2
    //   116: iload_2
    //   117: ifeq -> 261
    //   120: iload_2
    //   121: iconst_1
    //   122: if_icmpeq -> 185
    //   125: iload_2
    //   126: iconst_2
    //   127: if_icmpeq -> 185
    //   130: iload_2
    //   131: iconst_3
    //   132: if_icmpne -> 138
    //   135: goto -> 185
    //   138: new java/lang/StringBuilder
    //   141: dup
    //   142: invokespecial <init> : ()V
    //   145: astore_0
    //   146: aload_0
    //   147: iload #7
    //   149: invokestatic opToName : (I)Ljava/lang/String;
    //   152: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: aload_0
    //   157: ldc_w ' has unknown mode '
    //   160: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   163: pop
    //   164: aload_0
    //   165: iload_2
    //   166: invokestatic modeToName : (I)Ljava/lang/String;
    //   169: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: pop
    //   173: new java/lang/IllegalStateException
    //   176: dup
    //   177: aload_0
    //   178: invokevirtual toString : ()Ljava/lang/String;
    //   181: invokespecial <init> : (Ljava/lang/String;)V
    //   184: athrow
    //   185: iload_1
    //   186: ifne -> 191
    //   189: iconst_0
    //   190: ireturn
    //   191: new java/lang/StringBuilder
    //   194: dup
    //   195: invokespecial <init> : ()V
    //   198: astore_0
    //   199: aload_0
    //   200: ldc_w 'Op '
    //   203: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   206: pop
    //   207: aload_0
    //   208: iload #7
    //   210: invokestatic opToName : (I)Ljava/lang/String;
    //   213: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   216: pop
    //   217: aload_0
    //   218: ldc_w ' '
    //   221: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   224: pop
    //   225: aload_0
    //   226: iload_2
    //   227: invokestatic modeToName : (I)Ljava/lang/String;
    //   230: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   233: pop
    //   234: aload_0
    //   235: ldc_w ' for package '
    //   238: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   241: pop
    //   242: aload_0
    //   243: aload #4
    //   245: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   248: pop
    //   249: new java/lang/SecurityException
    //   252: dup
    //   253: aload_0
    //   254: invokevirtual toString : ()Ljava/lang/String;
    //   257: invokespecial <init> : (Ljava/lang/String;)V
    //   260: athrow
    //   261: iconst_1
    //   262: ireturn
    //   263: astore_0
    //   264: iload_1
    //   265: ifne -> 270
    //   268: iconst_0
    //   269: ireturn
    //   270: aload_0
    //   271: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1789	-> 0
    //   #1790	-> 11
    //   #1794	-> 15
    //   #1791	-> 17
    //   #1798	-> 67
    //   #1800	-> 78
    //   #1801	-> 83
    //   #1804	-> 99
    //   #1811	-> 106
    //   #1812	-> 106
    //   #1814	-> 116
    //   #1827	-> 138
    //   #1828	-> 146
    //   #1829	-> 164
    //   #1820	-> 185
    //   #1824	-> 189
    //   #1821	-> 191
    //   #1822	-> 225
    //   #1816	-> 261
    //   #1805	-> 263
    //   #1806	-> 264
    //   #1809	-> 268
    //   #1807	-> 270
    // Exception table:
    //   from	to	target	type
    //   99	106	263	java/lang/SecurityException
  }
  
  private boolean checkPermissionAndAppOp(boolean paramBoolean, int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, int paramInt3) {
    return checkPermissionAndAppOp(this.mContext, paramBoolean, paramInt1, paramInt2, paramString1, paramString2, paramString3, paramInt3);
  }
  
  private boolean noteAppOpAllowingLegacy(boolean paramBoolean, int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3) {
    paramInt1 = this.mAppOps.noteOpNoThrow(paramInt3, paramInt2, paramString1, paramString2, null);
    if (paramInt1 != 0) {
      if (paramInt1 == 1 || paramInt1 == 2 || paramInt1 == 3) {
        if (this.mAppOps.checkOpNoThrow(87, paramInt2, paramString1) == 0)
          return true; 
        if (!paramBoolean)
          return false; 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Op ");
        stringBuilder1.append(AppOpsManager.opToName(paramInt3));
        stringBuilder1.append(" ");
        stringBuilder1.append(AppOpsManager.modeToName(paramInt1));
        stringBuilder1.append(" for package ");
        stringBuilder1.append(paramString1);
        throw new SecurityException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(AppOpsManager.opToName(paramInt3));
      stringBuilder.append(" has unknown mode ");
      stringBuilder.append(AppOpsManager.modeToName(paramInt1));
      throw new IllegalStateException(stringBuilder.toString());
    } 
    return true;
  }
  
  public boolean checkPermissionReadAudio(boolean paramBoolean, int paramInt1, int paramInt2, String paramString1, String paramString2) {
    if (!checkExternalStoragePermissionAndAppOp(paramBoolean, paramInt1, paramInt2, paramString1, paramString2, "android.permission.READ_EXTERNAL_STORAGE", 59))
      return false; 
    return noteAppOpAllowingLegacy(paramBoolean, paramInt1, paramInt2, paramString1, paramString2, 81);
  }
  
  public boolean checkPermissionWriteAudio(boolean paramBoolean, int paramInt1, int paramInt2, String paramString1, String paramString2) {
    if (!checkExternalStoragePermissionAndAppOp(paramBoolean, paramInt1, paramInt2, paramString1, paramString2, "android.permission.WRITE_EXTERNAL_STORAGE", 60))
      return false; 
    return noteAppOpAllowingLegacy(paramBoolean, paramInt1, paramInt2, paramString1, paramString2, 82);
  }
  
  public boolean checkPermissionReadVideo(boolean paramBoolean, int paramInt1, int paramInt2, String paramString1, String paramString2) {
    if (!checkExternalStoragePermissionAndAppOp(paramBoolean, paramInt1, paramInt2, paramString1, paramString2, "android.permission.READ_EXTERNAL_STORAGE", 59))
      return false; 
    return noteAppOpAllowingLegacy(paramBoolean, paramInt1, paramInt2, paramString1, paramString2, 83);
  }
  
  public boolean checkPermissionWriteVideo(boolean paramBoolean, int paramInt1, int paramInt2, String paramString1, String paramString2) {
    if (!checkExternalStoragePermissionAndAppOp(paramBoolean, paramInt1, paramInt2, paramString1, paramString2, "android.permission.WRITE_EXTERNAL_STORAGE", 60))
      return false; 
    return noteAppOpAllowingLegacy(paramBoolean, paramInt1, paramInt2, paramString1, paramString2, 84);
  }
  
  public boolean checkPermissionReadImages(boolean paramBoolean, int paramInt1, int paramInt2, String paramString1, String paramString2) {
    if (!checkExternalStoragePermissionAndAppOp(paramBoolean, paramInt1, paramInt2, paramString1, paramString2, "android.permission.READ_EXTERNAL_STORAGE", 59))
      return false; 
    return noteAppOpAllowingLegacy(paramBoolean, paramInt1, paramInt2, paramString1, paramString2, 85);
  }
  
  public boolean checkPermissionWriteImages(boolean paramBoolean, int paramInt1, int paramInt2, String paramString1, String paramString2) {
    if (!checkExternalStoragePermissionAndAppOp(paramBoolean, paramInt1, paramInt2, paramString1, paramString2, "android.permission.WRITE_EXTERNAL_STORAGE", 60))
      return false; 
    return noteAppOpAllowingLegacy(paramBoolean, paramInt1, paramInt2, paramString1, paramString2, 86);
  }
  
  private boolean checkExternalStoragePermissionAndAppOp(boolean paramBoolean, int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, int paramInt3) {
    int i = this.mAppOps.noteOpNoThrow(92, paramInt2, paramString1, paramString2, null);
    if (i == 0)
      return true; 
    if (i == 3 && this.mContext.checkPermission("android.permission.MANAGE_EXTERNAL_STORAGE", paramInt1, paramInt2) == 0)
      return true; 
    return checkPermissionAndAppOp(paramBoolean, paramInt1, paramInt2, paramString1, paramString2, paramString3, paramInt3);
  }
  
  public ParcelFileDescriptor openProxyFileDescriptor(int paramInt, ProxyFileDescriptorCallback paramProxyFileDescriptorCallback, Handler paramHandler, ThreadFactory paramThreadFactory) throws IOException {
    // Byte code:
    //   0: aload_2
    //   1: invokestatic checkNotNull : (Ljava/lang/Object;)Ljava/lang/Object;
    //   4: pop
    //   5: aload_0
    //   6: getfield mContext : Landroid/content/Context;
    //   9: ldc_w 'storage_open_proxy_file_descriptor'
    //   12: iconst_1
    //   13: invokestatic count : (Landroid/content/Context;Ljava/lang/String;I)V
    //   16: aload_3
    //   17: astore #5
    //   19: aload_0
    //   20: getfield mFuseAppLoopLock : Ljava/lang/Object;
    //   23: astore #6
    //   25: aload #6
    //   27: monitorenter
    //   28: iconst_0
    //   29: istore #7
    //   31: aload_0
    //   32: getfield mFuseAppLoop : Lcom/android/internal/os/FuseAppLoop;
    //   35: ifnonnull -> 97
    //   38: aload_0
    //   39: getfield mStorageManager : Landroid/os/storage/IStorageManager;
    //   42: invokeinterface mountProxyFileDescriptorBridge : ()Lcom/android/internal/os/AppFuseMount;
    //   47: astore_3
    //   48: aload_3
    //   49: ifnull -> 84
    //   52: new com/android/internal/os/FuseAppLoop
    //   55: astore #8
    //   57: aload #8
    //   59: aload_3
    //   60: getfield mountPointId : I
    //   63: aload_3
    //   64: getfield fd : Landroid/os/ParcelFileDescriptor;
    //   67: aload #4
    //   69: invokespecial <init> : (ILandroid/os/ParcelFileDescriptor;Ljava/util/concurrent/ThreadFactory;)V
    //   72: aload_0
    //   73: aload #8
    //   75: putfield mFuseAppLoop : Lcom/android/internal/os/FuseAppLoop;
    //   78: iconst_1
    //   79: istore #7
    //   81: goto -> 97
    //   84: new java/io/IOException
    //   87: astore_2
    //   88: aload_2
    //   89: ldc_w 'Failed to mount proxy bridge'
    //   92: invokespecial <init> : (Ljava/lang/String;)V
    //   95: aload_2
    //   96: athrow
    //   97: aload #5
    //   99: astore_3
    //   100: aload #5
    //   102: ifnonnull -> 116
    //   105: new android/os/Handler
    //   108: astore_3
    //   109: aload_3
    //   110: invokestatic getMainLooper : ()Landroid/os/Looper;
    //   113: invokespecial <init> : (Landroid/os/Looper;)V
    //   116: aload_0
    //   117: getfield mFuseAppLoop : Lcom/android/internal/os/FuseAppLoop;
    //   120: aload_2
    //   121: aload_3
    //   122: invokevirtual registerCallback : (Landroid/os/ProxyFileDescriptorCallback;Landroid/os/Handler;)I
    //   125: istore #9
    //   127: aload_0
    //   128: getfield mStorageManager : Landroid/os/storage/IStorageManager;
    //   131: astore #5
    //   133: aload_0
    //   134: getfield mFuseAppLoop : Lcom/android/internal/os/FuseAppLoop;
    //   137: astore #8
    //   139: aload #8
    //   141: invokevirtual getMountPointId : ()I
    //   144: istore #10
    //   146: aload #5
    //   148: iload #10
    //   150: iload #9
    //   152: iload_1
    //   153: invokeinterface openProxyFileDescriptor : (III)Landroid/os/ParcelFileDescriptor;
    //   158: astore #5
    //   160: aload #5
    //   162: ifnull -> 171
    //   165: aload #6
    //   167: monitorexit
    //   168: aload #5
    //   170: areturn
    //   171: aload_0
    //   172: getfield mFuseAppLoop : Lcom/android/internal/os/FuseAppLoop;
    //   175: iload #9
    //   177: invokevirtual unregisterCallback : (I)V
    //   180: new com/android/internal/os/FuseUnavailableMountException
    //   183: astore #5
    //   185: aload_0
    //   186: getfield mFuseAppLoop : Lcom/android/internal/os/FuseAppLoop;
    //   189: astore #8
    //   191: aload #5
    //   193: aload #8
    //   195: invokevirtual getMountPointId : ()I
    //   198: invokespecial <init> : (I)V
    //   201: aload #5
    //   203: athrow
    //   204: astore #5
    //   206: iload #7
    //   208: ifne -> 225
    //   211: aload_0
    //   212: aconst_null
    //   213: putfield mFuseAppLoop : Lcom/android/internal/os/FuseAppLoop;
    //   216: aload #6
    //   218: monitorexit
    //   219: aload_3
    //   220: astore #5
    //   222: goto -> 19
    //   225: new java/io/IOException
    //   228: astore_2
    //   229: aload_2
    //   230: aload #5
    //   232: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   235: aload_2
    //   236: athrow
    //   237: astore_2
    //   238: aload #6
    //   240: monitorexit
    //   241: aload_2
    //   242: athrow
    //   243: astore_2
    //   244: new java/io/IOException
    //   247: dup
    //   248: aload_2
    //   249: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   252: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1959	-> 0
    //   #1960	-> 5
    //   #1966	-> 19
    //   #1967	-> 28
    //   #1968	-> 31
    //   #1969	-> 38
    //   #1970	-> 48
    //   #1973	-> 52
    //   #1974	-> 78
    //   #1971	-> 84
    //   #1976	-> 97
    //   #1977	-> 105
    //   #1980	-> 116
    //   #1981	-> 127
    //   #1982	-> 139
    //   #1981	-> 146
    //   #1983	-> 160
    //   #1988	-> 165
    //   #1984	-> 171
    //   #1985	-> 180
    //   #1986	-> 191
    //   #1989	-> 204
    //   #1992	-> 206
    //   #1995	-> 211
    //   #1996	-> 216
    //   #1993	-> 225
    //   #1998	-> 237
    //   #1999	-> 243
    //   #2001	-> 244
    // Exception table:
    //   from	to	target	type
    //   19	28	243	android/os/RemoteException
    //   31	38	237	finally
    //   38	48	237	finally
    //   52	78	237	finally
    //   84	97	237	finally
    //   105	116	237	finally
    //   116	127	204	com/android/internal/os/FuseUnavailableMountException
    //   116	127	237	finally
    //   127	139	204	com/android/internal/os/FuseUnavailableMountException
    //   127	139	237	finally
    //   139	146	204	com/android/internal/os/FuseUnavailableMountException
    //   139	146	237	finally
    //   146	160	204	com/android/internal/os/FuseUnavailableMountException
    //   146	160	237	finally
    //   165	168	237	finally
    //   171	180	204	com/android/internal/os/FuseUnavailableMountException
    //   171	180	237	finally
    //   180	191	204	com/android/internal/os/FuseUnavailableMountException
    //   180	191	237	finally
    //   191	204	204	com/android/internal/os/FuseUnavailableMountException
    //   191	204	237	finally
    //   211	216	237	finally
    //   216	219	237	finally
    //   225	237	237	finally
    //   238	241	237	finally
    //   241	243	243	android/os/RemoteException
  }
  
  public ParcelFileDescriptor openProxyFileDescriptor(int paramInt, ProxyFileDescriptorCallback paramProxyFileDescriptorCallback) throws IOException {
    return openProxyFileDescriptor(paramInt, paramProxyFileDescriptorCallback, null, null);
  }
  
  public ParcelFileDescriptor openProxyFileDescriptor(int paramInt, ProxyFileDescriptorCallback paramProxyFileDescriptorCallback, Handler paramHandler) throws IOException {
    Preconditions.checkNotNull(paramHandler);
    return openProxyFileDescriptor(paramInt, paramProxyFileDescriptorCallback, paramHandler, null);
  }
  
  public int getProxyFileDescriptorMountPointId() {
    synchronized (this.mFuseAppLoopLock) {
      byte b;
      if (this.mFuseAppLoop != null) {
        b = this.mFuseAppLoop.getMountPointId();
      } else {
        b = -1;
      } 
      return b;
    } 
  }
  
  public long getCacheQuotaBytes(UUID paramUUID) throws IOException {
    try {
      ApplicationInfo applicationInfo = this.mContext.getApplicationInfo();
      return this.mStorageManager.getCacheQuotaBytes(convert(paramUUID), applicationInfo.uid);
    } catch (ParcelableException parcelableException) {
      parcelableException.maybeRethrow(IOException.class);
      throw new RuntimeException(parcelableException);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public long getCacheSizeBytes(UUID paramUUID) throws IOException {
    try {
      ApplicationInfo applicationInfo = this.mContext.getApplicationInfo();
      return this.mStorageManager.getCacheSizeBytes(convert(paramUUID), applicationInfo.uid);
    } catch (ParcelableException parcelableException) {
      parcelableException.maybeRethrow(IOException.class);
      throw new RuntimeException(parcelableException);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public long getAllocatableBytes(UUID paramUUID) throws IOException {
    return getAllocatableBytes(paramUUID, 0);
  }
  
  @SystemApi
  public long getAllocatableBytes(UUID paramUUID, int paramInt) throws IOException {
    try {
      IStorageManager iStorageManager = this.mStorageManager;
      String str1 = convert(paramUUID);
      Context context = this.mContext;
      String str2 = context.getOpPackageName();
      return iStorageManager.getAllocatableBytes(str1, paramInt, str2);
    } catch (ParcelableException parcelableException) {
      parcelableException.maybeRethrow(IOException.class);
      throw new RuntimeException(parcelableException);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void allocateBytes(UUID paramUUID, long paramLong) throws IOException {
    allocateBytes(paramUUID, paramLong, 0);
  }
  
  @SystemApi
  public void allocateBytes(UUID paramUUID, long paramLong, int paramInt) throws IOException {
    try {
      IStorageManager iStorageManager = this.mStorageManager;
      String str1 = convert(paramUUID);
      Context context = this.mContext;
      String str2 = context.getOpPackageName();
      iStorageManager.allocateBytes(str1, paramLong, paramInt, str2);
    } catch (ParcelableException parcelableException) {
      parcelableException.maybeRethrow(IOException.class);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void allocateBytes(FileDescriptor paramFileDescriptor, long paramLong) throws IOException {
    allocateBytes(paramFileDescriptor, paramLong, 0);
  }
  
  @SystemApi
  public void allocateBytes(FileDescriptor paramFileDescriptor, long paramLong, int paramInt) throws IOException {
    File file = ParcelFileDescriptor.getFile(paramFileDescriptor);
    UUID uUID = getUuidForPath(file);
    for (byte b = 0; b < 3;) {
      try {
        long l = (Os.fstat(paramFileDescriptor)).st_blocks;
        l = paramLong - l * 512L;
        if (l > 0L)
          allocateBytes(uUID, l, paramInt); 
        try {
          Os.posix_fallocate(paramFileDescriptor, 0L, paramLong);
          return;
        } catch (ErrnoException errnoException) {
          if (errnoException.errno == OsConstants.ENOSYS || errnoException.errno == OsConstants.ENOTSUP) {
            Log.w("StorageManager", "fallocate() not supported; falling back to ftruncate()");
            Os.ftruncate(paramFileDescriptor, paramLong);
            return;
          } 
          throw errnoException;
        } 
      } catch (ErrnoException errnoException) {
        if (errnoException.errno == OsConstants.ENOSPC) {
          Log.w("StorageManager", "Odd, not enough space; let's try again?");
          b++;
        } 
        throw errnoException.rethrowAsIOException();
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Well this is embarassing; we can't allocate ");
    stringBuilder.append(paramLong);
    stringBuilder.append(" for ");
    stringBuilder.append(file);
    throw new IOException(stringBuilder.toString());
  }
  
  private static long getProjectIdForUser(int paramInt1, int paramInt2) {
    return (100000 * paramInt1 + paramInt2);
  }
  
  @SystemApi
  public void updateExternalStorageFileQuotaType(File paramFile, int paramInt) throws IOException {
    String str = paramFile.getCanonicalPath();
    StorageVolume storageVolume = getStorageVolume(paramFile);
    if (storageVolume == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to update quota type for ");
      stringBuilder.append(str);
      Log.w("StorageManager", stringBuilder.toString());
      return;
    } 
    if (!stringBuilder.isEmulated())
      return; 
    int i = stringBuilder.getOwner().getIdentifier();
    if (i >= 0) {
      long l;
      if (paramInt != 0) {
        if (paramInt != 1) {
          if (paramInt != 2) {
            if (paramInt == 3) {
              l = getProjectIdForUser(i, 1002);
            } else {
              stringBuilder = new StringBuilder();
              stringBuilder.append("Invalid quota type: ");
              stringBuilder.append(paramInt);
              throw new IllegalArgumentException(stringBuilder.toString());
            } 
          } else {
            l = getProjectIdForUser(i, 1001);
          } 
        } else {
          l = getProjectIdForUser(i, 1003);
        } 
      } else {
        l = getProjectIdForUser(i, 1000);
      } 
      if (setQuotaProjectId(str, l))
        return; 
      stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to update quota type for ");
      stringBuilder.append(str);
      throw new IOException(stringBuilder.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to update quota type for ");
    stringBuilder.append(str);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public void fixupAppDir(File paramFile) {
    try {
      this.mStorageManager.fixupAppDir(paramFile.getCanonicalPath());
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to get canonical path for ");
      stringBuilder.append(paramFile.getPath());
      Log.e("StorageManager", stringBuilder.toString(), iOException);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private static void setCacheBehavior(File paramFile, String paramString, boolean paramBoolean) throws IOException {
    if (paramFile.isDirectory()) {
      if (paramBoolean) {
        try {
          String str = paramFile.getAbsolutePath();
          Charset charset = StandardCharsets.UTF_8;
          byte[] arrayOfByte = "1".getBytes(charset);
          Os.setxattr(str, paramString, arrayOfByte, 0);
        } catch (ErrnoException errnoException) {
          throw errnoException.rethrowAsIOException();
        } 
      } else {
        try {
          Os.removexattr(errnoException.getAbsolutePath(), paramString);
        } catch (ErrnoException errnoException1) {
          if (errnoException1.errno != OsConstants.ENODATA)
            throw errnoException1.rethrowAsIOException(); 
        } 
      } 
      return;
    } 
    throw new IOException("Cache behavior can only be set on directories");
  }
  
  private static boolean isCacheBehavior(File paramFile, String paramString) throws IOException {
    try {
      Os.getxattr(paramFile.getAbsolutePath(), paramString);
      return true;
    } catch (ErrnoException errnoException) {
      if (errnoException.errno == OsConstants.ENODATA)
        return false; 
      throw errnoException.rethrowAsIOException();
    } 
  }
  
  public void setCacheBehaviorGroup(File paramFile, boolean paramBoolean) throws IOException {
    setCacheBehavior(paramFile, "user.cache_group", paramBoolean);
  }
  
  public boolean isCacheBehaviorGroup(File paramFile) throws IOException {
    return isCacheBehavior(paramFile, "user.cache_group");
  }
  
  public void setCacheBehaviorTombstone(File paramFile, boolean paramBoolean) throws IOException {
    setCacheBehavior(paramFile, "user.cache_tombstone", paramBoolean);
  }
  
  public boolean isCacheBehaviorTombstone(File paramFile) throws IOException {
    return isCacheBehavior(paramFile, "user.cache_tombstone");
  }
  
  public static UUID convert(String paramString) {
    if (Objects.equals(paramString, UUID_PRIVATE_INTERNAL))
      return UUID_DEFAULT; 
    if (Objects.equals(paramString, "primary_physical"))
      return UUID_PRIMARY_PHYSICAL_; 
    if (Objects.equals(paramString, "system"))
      return UUID_SYSTEM_; 
    return UUID.fromString(paramString);
  }
  
  public static String convert(UUID paramUUID) {
    if (UUID_DEFAULT.equals(paramUUID))
      return UUID_PRIVATE_INTERNAL; 
    if (UUID_PRIMARY_PHYSICAL_.equals(paramUUID))
      return "primary_physical"; 
    if (UUID_SYSTEM_.equals(paramUUID))
      return "system"; 
    return paramUUID.toString();
  }
  
  public boolean isCheckpointSupported() {
    try {
      return this.mStorageManager.supportsCheckpoint();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public StorageManager(Context paramContext, Looper paramLooper) throws ServiceManager.ServiceNotFoundException {
    this.mFuseAppLoopLock = new Object();
    this.mFuseAppLoop = null;
    this.mContext = paramContext;
    this.mResolver = paramContext.getContentResolver();
    this.mLooper = paramLooper;
    this.mStorageManager = IStorageManager.Stub.asInterface(ServiceManager.getServiceOrThrow("mount"));
    this.mAppOps = (AppOpsManager)this.mContext.getSystemService(AppOpsManager.class);
  }
  
  private static native boolean setQuotaProjectId(String paramString, long paramLong);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface AllocateFlags {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface QuotaType {}
}
