package android.os.storage;

import android.os.Parcel;
import android.os.Parcelable;

public class CrateMetadata implements Parcelable {
  public static final Parcelable.Creator<CrateMetadata> CREATOR = new Parcelable.Creator<CrateMetadata>() {
      public CrateMetadata createFromParcel(Parcel param1Parcel) {
        CrateMetadata crateMetadata = new CrateMetadata();
        crateMetadata.readFromParcel(param1Parcel);
        return crateMetadata;
      }
      
      public CrateMetadata[] newArray(int param1Int) {
        return new CrateMetadata[param1Int];
      }
    };
  
  public String id;
  
  public String packageName;
  
  public int uid;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.uid);
    paramParcel.writeString(this.packageName);
    paramParcel.writeString(this.id);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.uid = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.packageName = paramParcel.readString();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.id = paramParcel.readString();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
