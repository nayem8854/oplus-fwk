package android.os.storage;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.DebugUtils;
import com.android.internal.util.IndentingPrintWriter;
import com.android.internal.util.Preconditions;
import java.io.CharArrayWriter;
import java.util.Objects;

public class DiskInfo implements Parcelable {
  public static final String ACTION_DISK_SCANNED = "android.os.storage.action.DISK_SCANNED";
  
  public DiskInfo(String paramString, int paramInt) {
    this.id = (String)Preconditions.checkNotNull(paramString);
    this.flags = paramInt;
  }
  
  public DiskInfo(Parcel paramParcel) {
    this.id = paramParcel.readString();
    this.flags = paramParcel.readInt();
    this.size = paramParcel.readLong();
    this.label = paramParcel.readString();
    this.volumeCount = paramParcel.readInt();
    this.sysPath = paramParcel.readString();
  }
  
  public String getId() {
    return this.id;
  }
  
  private boolean isInteresting(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return false; 
    if (paramString.equalsIgnoreCase("ata"))
      return false; 
    if (paramString.toLowerCase().contains("generic"))
      return false; 
    if (paramString.toLowerCase().startsWith("usb"))
      return false; 
    if (paramString.toLowerCase().startsWith("multiple"))
      return false; 
    return true;
  }
  
  public String getDescription() {
    Resources resources = Resources.getSystem();
    int i = this.flags;
    if ((i & 0x4) != 0) {
      if (isInteresting(this.label))
        return resources.getString(17041373, new Object[] { this.label }); 
      return resources.getString(17041372);
    } 
    if ((i & 0x8) != 0) {
      if (isInteresting(this.label))
        return resources.getString(17041376, new Object[] { this.label }); 
      return resources.getString(17041375);
    } 
    return null;
  }
  
  public String getShortDescription() {
    Resources resources = Resources.getSystem();
    if (isSd())
      return resources.getString(17041372); 
    if (isUsb())
      return resources.getString(17041375); 
    return null;
  }
  
  public boolean isAdoptable() {
    int i = this.flags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public boolean isDefaultPrimary() {
    boolean bool;
    if ((this.flags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isSd() {
    boolean bool;
    if ((this.flags & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isUsb() {
    boolean bool;
    if ((this.flags & 0x8) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String toString() {
    CharArrayWriter charArrayWriter = new CharArrayWriter();
    dump(new IndentingPrintWriter(charArrayWriter, "    ", 80));
    return charArrayWriter.toString();
  }
  
  public void dump(IndentingPrintWriter paramIndentingPrintWriter) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DiskInfo{");
    stringBuilder.append(this.id);
    stringBuilder.append("}:");
    paramIndentingPrintWriter.println(stringBuilder.toString());
    paramIndentingPrintWriter.increaseIndent();
    paramIndentingPrintWriter.printPair("flags", DebugUtils.flagsToString(getClass(), "FLAG_", this.flags));
    paramIndentingPrintWriter.printPair("size", Long.valueOf(this.size));
    paramIndentingPrintWriter.printPair("label", this.label);
    paramIndentingPrintWriter.println();
    paramIndentingPrintWriter.printPair("sysPath", this.sysPath);
    paramIndentingPrintWriter.decreaseIndent();
    paramIndentingPrintWriter.println();
  }
  
  public DiskInfo clone() {
    Parcel parcel = Parcel.obtain();
    try {
      writeToParcel(parcel, 0);
      parcel.setDataPosition(0);
      return CREATOR.createFromParcel(parcel);
    } finally {
      parcel.recycle();
    } 
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject instanceof DiskInfo)
      return Objects.equals(this.id, ((DiskInfo)paramObject).id); 
    return false;
  }
  
  public int hashCode() {
    return this.id.hashCode();
  }
  
  public static final Parcelable.Creator<DiskInfo> CREATOR = new Parcelable.Creator<DiskInfo>() {
      public DiskInfo createFromParcel(Parcel param1Parcel) {
        return new DiskInfo(param1Parcel);
      }
      
      public DiskInfo[] newArray(int param1Int) {
        return new DiskInfo[param1Int];
      }
    };
  
  public static final String EXTRA_DISK_ID = "android.os.storage.extra.DISK_ID";
  
  public static final String EXTRA_VOLUME_COUNT = "android.os.storage.extra.VOLUME_COUNT";
  
  public static final int FLAG_ADOPTABLE = 1;
  
  public static final int FLAG_DEFAULT_PRIMARY = 2;
  
  public static final int FLAG_EMMC = 16;
  
  public static final int FLAG_SD = 4;
  
  public static final int FLAG_UFS_CARD = 32;
  
  public static final int FLAG_USB = 8;
  
  public final int flags;
  
  public final String id;
  
  public String label;
  
  public long size;
  
  public String sysPath;
  
  public int volumeCount;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.id);
    paramParcel.writeInt(this.flags);
    paramParcel.writeLong(this.size);
    paramParcel.writeString(this.label);
    paramParcel.writeInt(this.volumeCount);
    paramParcel.writeString(this.sysPath);
  }
}
