package android.os.storage;

public class StorageHealthInfoItem {
  private String mDescription;
  
  private boolean mHighFirst;
  
  private int mLength;
  
  private String mName;
  
  private int mOffset;
  
  private int mValue;
  
  public StorageHealthInfoItem(String paramString1, int paramInt1, int paramInt2, boolean paramBoolean, String paramString2) {
    this.mName = paramString1;
    this.mOffset = paramInt1;
    this.mLength = paramInt2;
    this.mHighFirst = paramBoolean;
    this.mDescription = paramString2;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public int getOffset() {
    return this.mOffset;
  }
  
  public int getLength() {
    return this.mLength;
  }
  
  public boolean getHighFirst() {
    return this.mHighFirst;
  }
  
  public String getDescription() {
    return this.mDescription;
  }
  
  public void setValue(int paramInt) {
    this.mValue = paramInt;
  }
  
  public int getValue() {
    return this.mValue;
  }
}
