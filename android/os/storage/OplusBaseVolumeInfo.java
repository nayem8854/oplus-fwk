package android.os.storage;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.android.internal.util.IndentingPrintWriter;

public abstract class OplusBaseVolumeInfo implements Parcelable {
  public int readOnlyType = -1;
  
  public boolean hasMountedStateBrocasted = false;
  
  public int getReadOnlyType() {
    return this.readOnlyType;
  }
  
  public void setReadOnlyTypeValue(int paramInt) {
    if (paramInt < -1 || paramInt > 2) {
      Log.w("OplusBaseVolumeInfo", "value illegal, must in [-1, 2]");
      return;
    } 
    this.readOnlyType = paramInt;
  }
  
  protected void initFromParcel(Parcel paramParcel) {
    this.readOnlyType = paramParcel.readInt();
    this.hasMountedStateBrocasted = paramParcel.readBoolean();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.readOnlyType);
    paramParcel.writeBoolean(this.hasMountedStateBrocasted);
  }
  
  protected void dump(IndentingPrintWriter paramIndentingPrintWriter) {
    paramIndentingPrintWriter.println();
    paramIndentingPrintWriter.printPair("readOnlyType", Integer.valueOf(this.readOnlyType));
    paramIndentingPrintWriter.println();
    paramIndentingPrintWriter.printPair("hasMountedStateBrocasted", Boolean.valueOf(this.hasMountedStateBrocasted));
  }
}
