package android.os.storage;

import android.app.AppGlobals;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Slog;

public class OplusStorageManager {
  private static final String TAG = "OplusStorageManager";
  
  private IOplusStorageManagerService mOplusStorageManagerService;
  
  public int getStorageData() {
    StringBuilder stringBuilder1;
    String[] arrayOfString = null;
    int i = Binder.getCallingUid();
    IBinder iBinder = ServiceManager.getService("oplusstoragemanagerservice");
    if (iBinder == null) {
      Slog.d("OplusStorageManager", "get oplusstoragemanagerservice is null");
    } else if (this.mOplusStorageManagerService != IOplusStorageManagerService.Stub.asInterface(iBinder)) {
      this.mOplusStorageManagerService = IOplusStorageManagerService.Stub.asInterface(iBinder);
    } 
    try {
      String[] arrayOfString1 = AppGlobals.getPackageManager().getPackagesForUid(i);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getPackagesForUid failed for uid:");
      stringBuilder.append(i);
      Slog.e("OplusStorageManager", stringBuilder.toString(), (Throwable)remoteException);
    } 
    if (arrayOfString == null) {
      Slog.d("OplusStorageManager", "pkgs is null");
      return -1;
    } 
    if (arrayOfString[0].equals("com.coloros.phonemanager") || arrayOfString[0].equals("com.coloros.assistantscreen"))
      try {
        if (this.mOplusStorageManagerService != null)
          return this.mOplusStorageManagerService.getStorageData(); 
        Slog.d("OplusStorageManager", "oplusstoragemanagerservice is null");
        return -1;
      } catch (RemoteException remoteException) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("err = ");
        stringBuilder1.append(remoteException.toString());
        Slog.e("OplusStorageManager", stringBuilder1.toString());
        throw remoteException.rethrowFromSystemServer();
      }  
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("not allowed ");
    stringBuilder2.append((String)stringBuilder1[0]);
    stringBuilder2.append(" to get the interface");
    Slog.d("OplusStorageManager", stringBuilder2.toString());
    return -1;
  }
}
