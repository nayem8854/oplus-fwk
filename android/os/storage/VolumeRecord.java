package android.os.storage;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.UserHandle;
import android.util.DebugUtils;
import android.util.TimeUtils;
import com.android.internal.util.IndentingPrintWriter;
import com.android.internal.util.Preconditions;
import java.io.File;
import java.util.Locale;
import java.util.Objects;

public class VolumeRecord implements Parcelable {
  public VolumeRecord(int paramInt, String paramString) {
    this.type = paramInt;
    this.fsUuid = (String)Preconditions.checkNotNull(paramString);
  }
  
  public VolumeRecord(Parcel paramParcel) {
    this.type = paramParcel.readInt();
    this.fsUuid = paramParcel.readString();
    this.partGuid = paramParcel.readString();
    this.nickname = paramParcel.readString();
    this.userFlags = paramParcel.readInt();
    this.createdMillis = paramParcel.readLong();
    this.lastSeenMillis = paramParcel.readLong();
    this.lastTrimMillis = paramParcel.readLong();
    this.lastBenchMillis = paramParcel.readLong();
  }
  
  public int getType() {
    return this.type;
  }
  
  public String getFsUuid() {
    return this.fsUuid;
  }
  
  public String getNormalizedFsUuid() {
    String str = this.fsUuid;
    if (str != null) {
      str = str.toLowerCase(Locale.US);
    } else {
      str = null;
    } 
    return str;
  }
  
  public String getNickname() {
    return this.nickname;
  }
  
  public boolean isInited() {
    int i = this.userFlags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public boolean isSnoozed() {
    boolean bool;
    if ((this.userFlags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public StorageVolume buildStorageVolume(Context paramContext) {
    String str1;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("unknown:");
    stringBuilder.append(this.fsUuid);
    String str3 = stringBuilder.toString();
    File file1 = new File("/dev/null");
    File file2 = new File("/dev/null");
    UserHandle userHandle = new UserHandle(-10000);
    String str2 = this.nickname;
    if (str2 == null) {
      str1 = paramContext.getString(17039374);
    } else {
      str1 = str2;
    } 
    return new StorageVolume(str3, file1, file2, str1, false, true, false, false, 0L, userHandle, this.fsUuid, "unknown");
  }
  
  public void dump(IndentingPrintWriter paramIndentingPrintWriter) {
    paramIndentingPrintWriter.println("VolumeRecord:");
    paramIndentingPrintWriter.increaseIndent();
    paramIndentingPrintWriter.printPair("type", DebugUtils.valueToString(VolumeInfo.class, "TYPE_", this.type));
    paramIndentingPrintWriter.printPair("fsUuid", this.fsUuid);
    paramIndentingPrintWriter.printPair("partGuid", this.partGuid);
    paramIndentingPrintWriter.println();
    paramIndentingPrintWriter.printPair("nickname", this.nickname);
    int i = this.userFlags;
    String str = DebugUtils.flagsToString(VolumeRecord.class, "USER_FLAG_", i);
    paramIndentingPrintWriter.printPair("userFlags", str);
    paramIndentingPrintWriter.println();
    paramIndentingPrintWriter.printPair("createdMillis", TimeUtils.formatForLogging(this.createdMillis));
    paramIndentingPrintWriter.printPair("lastSeenMillis", TimeUtils.formatForLogging(this.lastSeenMillis));
    paramIndentingPrintWriter.printPair("lastTrimMillis", TimeUtils.formatForLogging(this.lastTrimMillis));
    paramIndentingPrintWriter.printPair("lastBenchMillis", TimeUtils.formatForLogging(this.lastBenchMillis));
    paramIndentingPrintWriter.decreaseIndent();
    paramIndentingPrintWriter.println();
  }
  
  public VolumeRecord clone() {
    Parcel parcel = Parcel.obtain();
    try {
      writeToParcel(parcel, 0);
      parcel.setDataPosition(0);
      return CREATOR.createFromParcel(parcel);
    } finally {
      parcel.recycle();
    } 
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject instanceof VolumeRecord)
      return Objects.equals(this.fsUuid, ((VolumeRecord)paramObject).fsUuid); 
    return false;
  }
  
  public int hashCode() {
    return this.fsUuid.hashCode();
  }
  
  public static final Parcelable.Creator<VolumeRecord> CREATOR = new Parcelable.Creator<VolumeRecord>() {
      public VolumeRecord createFromParcel(Parcel param1Parcel) {
        return new VolumeRecord(param1Parcel);
      }
      
      public VolumeRecord[] newArray(int param1Int) {
        return new VolumeRecord[param1Int];
      }
    };
  
  public static final String EXTRA_FS_UUID = "android.os.storage.extra.FS_UUID";
  
  public static final int USER_FLAG_INITED = 1;
  
  public static final int USER_FLAG_SNOOZED = 2;
  
  public long createdMillis;
  
  public final String fsUuid;
  
  public long lastBenchMillis;
  
  public long lastSeenMillis;
  
  public long lastTrimMillis;
  
  public String nickname;
  
  public String partGuid;
  
  public final int type;
  
  public int userFlags;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.type);
    paramParcel.writeString(this.fsUuid);
    paramParcel.writeString(this.partGuid);
    paramParcel.writeString(this.nickname);
    paramParcel.writeInt(this.userFlags);
    paramParcel.writeLong(this.createdMillis);
    paramParcel.writeLong(this.lastSeenMillis);
    paramParcel.writeLong(this.lastTrimMillis);
    paramParcel.writeLong(this.lastBenchMillis);
  }
}
