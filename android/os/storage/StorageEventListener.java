package android.os.storage;

public class StorageEventListener {
  public void onUsbMassStorageConnectionChanged(boolean paramBoolean) {}
  
  public void onStorageStateChanged(String paramString1, String paramString2, String paramString3) {}
  
  public void onVolumeStateChanged(VolumeInfo paramVolumeInfo, int paramInt1, int paramInt2) {}
  
  public void onVolumeRecordChanged(VolumeRecord paramVolumeRecord) {}
  
  public void onVolumeForgotten(String paramString) {}
  
  public void onDiskScanned(DiskInfo paramDiskInfo, int paramInt) {}
  
  public void onDiskDestroyed(DiskInfo paramDiskInfo) {}
}
