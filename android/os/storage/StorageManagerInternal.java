package android.os.storage;

import android.os.IVold;
import java.util.Set;

public abstract class StorageManagerInternal {
  public abstract void addExternalStoragePolicy(ExternalStorageMountPolicy paramExternalStorageMountPolicy);
  
  public abstract void addResetListener(ResetListener paramResetListener);
  
  public abstract int getExternalStorageMountMode(int paramInt, String paramString);
  
  public abstract boolean hasLegacyExternalStorage(int paramInt);
  
  public abstract boolean isExternalStorageService(int paramInt);
  
  public abstract void onAppOpsChanged(int paramInt1, int paramInt2, String paramString, int paramInt3);
  
  public abstract void onExternalStoragePolicyChanged(int paramInt, String paramString);
  
  public abstract void prepareAppDataAfterInstall(String paramString, int paramInt);
  
  public abstract boolean prepareStorageDirs(int paramInt, Set<String> paramSet, String paramString);
  
  public abstract void resetUser(int paramInt);
  
  public static interface ExternalStorageMountPolicy {
    int getMountMode(int param1Int, String param1String);
    
    boolean hasExternalStorage(int param1Int, String param1String);
  }
  
  public static interface ResetListener {
    void onReset(IVold param1IVold);
  }
}
