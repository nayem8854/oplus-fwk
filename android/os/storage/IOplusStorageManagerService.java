package android.os.storage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusStorageManagerService extends IInterface {
  int getStorageData() throws RemoteException;
  
  class Default implements IOplusStorageManagerService {
    public int getStorageData() throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusStorageManagerService {
    private static final String DESCRIPTOR = "android.os.storage.IOplusStorageManagerService";
    
    static final int TRANSACTION_getStorageData = 1;
    
    public Stub() {
      attachInterface(this, "android.os.storage.IOplusStorageManagerService");
    }
    
    public static IOplusStorageManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.storage.IOplusStorageManagerService");
      if (iInterface != null && iInterface instanceof IOplusStorageManagerService)
        return (IOplusStorageManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getStorageData";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.storage.IOplusStorageManagerService");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.storage.IOplusStorageManagerService");
      param1Int1 = getStorageData();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    private static class Proxy implements IOplusStorageManagerService {
      public static IOplusStorageManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.storage.IOplusStorageManagerService";
      }
      
      public int getStorageData() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IOplusStorageManagerService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusStorageManagerService.Stub.getDefaultImpl() != null)
            return IOplusStorageManagerService.Stub.getDefaultImpl().getStorageData(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusStorageManagerService param1IOplusStorageManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusStorageManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusStorageManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusStorageManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
