package android.os.storage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IStorageShutdownObserver extends IInterface {
  void onShutDownComplete(int paramInt) throws RemoteException;
  
  class Default implements IStorageShutdownObserver {
    public void onShutDownComplete(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IStorageShutdownObserver {
    private static final String DESCRIPTOR = "android.os.storage.IStorageShutdownObserver";
    
    static final int TRANSACTION_onShutDownComplete = 1;
    
    public Stub() {
      attachInterface(this, "android.os.storage.IStorageShutdownObserver");
    }
    
    public static IStorageShutdownObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.storage.IStorageShutdownObserver");
      if (iInterface != null && iInterface instanceof IStorageShutdownObserver)
        return (IStorageShutdownObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onShutDownComplete";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.storage.IStorageShutdownObserver");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.storage.IStorageShutdownObserver");
      param1Int1 = param1Parcel1.readInt();
      onShutDownComplete(param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IStorageShutdownObserver {
      public static IStorageShutdownObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.storage.IStorageShutdownObserver";
      }
      
      public void onShutDownComplete(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.storage.IStorageShutdownObserver");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IStorageShutdownObserver.Stub.getDefaultImpl() != null) {
            IStorageShutdownObserver.Stub.getDefaultImpl().onShutDownComplete(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IStorageShutdownObserver param1IStorageShutdownObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IStorageShutdownObserver != null) {
          Proxy.sDefaultImpl = param1IStorageShutdownObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IStorageShutdownObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
