package android.os.storage;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IObbActionListener extends IInterface {
  void onObbResult(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IObbActionListener {
    public void onObbResult(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IObbActionListener {
    private static final String DESCRIPTOR = "android.os.storage.IObbActionListener";
    
    static final int TRANSACTION_onObbResult = 1;
    
    public Stub() {
      attachInterface(this, "android.os.storage.IObbActionListener");
    }
    
    public static IObbActionListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.storage.IObbActionListener");
      if (iInterface != null && iInterface instanceof IObbActionListener)
        return (IObbActionListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onObbResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.storage.IObbActionListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.storage.IObbActionListener");
      String str = param1Parcel1.readString();
      param1Int1 = param1Parcel1.readInt();
      param1Int2 = param1Parcel1.readInt();
      onObbResult(str, param1Int1, param1Int2);
      return true;
    }
    
    private static class Proxy implements IObbActionListener {
      public static IObbActionListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.storage.IObbActionListener";
      }
      
      public void onObbResult(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.storage.IObbActionListener");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IObbActionListener.Stub.getDefaultImpl() != null) {
            IObbActionListener.Stub.getDefaultImpl().onObbResult(param2String, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IObbActionListener param1IObbActionListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IObbActionListener != null) {
          Proxy.sDefaultImpl = param1IObbActionListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IObbActionListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
