package android.os;

public interface IUpdateEngine extends IInterface {
  long allocateSpaceForPayload(String paramString, String[] paramArrayOfString) throws RemoteException;
  
  void applyPayload(String paramString, long paramLong1, long paramLong2, String[] paramArrayOfString) throws RemoteException;
  
  void applyPayloadFd(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong1, long paramLong2, String[] paramArrayOfString) throws RemoteException;
  
  boolean bind(IUpdateEngineCallback paramIUpdateEngineCallback) throws RemoteException;
  
  void cancel() throws RemoteException;
  
  void cleanupSuccessfulUpdate(IUpdateEngineCallback paramIUpdateEngineCallback) throws RemoteException;
  
  void resetStatus() throws RemoteException;
  
  void resume() throws RemoteException;
  
  void suspend() throws RemoteException;
  
  boolean unbind(IUpdateEngineCallback paramIUpdateEngineCallback) throws RemoteException;
  
  boolean verifyPayloadApplicable(String paramString) throws RemoteException;
  
  class Default implements IUpdateEngine {
    public void applyPayload(String param1String, long param1Long1, long param1Long2, String[] param1ArrayOfString) throws RemoteException {}
    
    public void applyPayloadFd(ParcelFileDescriptor param1ParcelFileDescriptor, long param1Long1, long param1Long2, String[] param1ArrayOfString) throws RemoteException {}
    
    public boolean bind(IUpdateEngineCallback param1IUpdateEngineCallback) throws RemoteException {
      return false;
    }
    
    public boolean unbind(IUpdateEngineCallback param1IUpdateEngineCallback) throws RemoteException {
      return false;
    }
    
    public void suspend() throws RemoteException {}
    
    public void resume() throws RemoteException {}
    
    public void cancel() throws RemoteException {}
    
    public void resetStatus() throws RemoteException {}
    
    public boolean verifyPayloadApplicable(String param1String) throws RemoteException {
      return false;
    }
    
    public long allocateSpaceForPayload(String param1String, String[] param1ArrayOfString) throws RemoteException {
      return 0L;
    }
    
    public void cleanupSuccessfulUpdate(IUpdateEngineCallback param1IUpdateEngineCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IUpdateEngine {
    private static final String DESCRIPTOR = "android.os.IUpdateEngine";
    
    static final int TRANSACTION_allocateSpaceForPayload = 10;
    
    static final int TRANSACTION_applyPayload = 1;
    
    static final int TRANSACTION_applyPayloadFd = 2;
    
    static final int TRANSACTION_bind = 3;
    
    static final int TRANSACTION_cancel = 7;
    
    static final int TRANSACTION_cleanupSuccessfulUpdate = 11;
    
    static final int TRANSACTION_resetStatus = 8;
    
    static final int TRANSACTION_resume = 6;
    
    static final int TRANSACTION_suspend = 5;
    
    static final int TRANSACTION_unbind = 4;
    
    static final int TRANSACTION_verifyPayloadApplicable = 9;
    
    public Stub() {
      attachInterface(this, "android.os.IUpdateEngine");
    }
    
    public static IUpdateEngine asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IUpdateEngine");
      if (iInterface != null && iInterface instanceof IUpdateEngine)
        return (IUpdateEngine)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "cleanupSuccessfulUpdate";
        case 10:
          return "allocateSpaceForPayload";
        case 9:
          return "verifyPayloadApplicable";
        case 8:
          return "resetStatus";
        case 7:
          return "cancel";
        case 6:
          return "resume";
        case 5:
          return "suspend";
        case 4:
          return "unbind";
        case 3:
          return "bind";
        case 2:
          return "applyPayloadFd";
        case 1:
          break;
      } 
      return "applyPayload";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        IUpdateEngineCallback iUpdateEngineCallback2;
        String arrayOfString2[], str1;
        IUpdateEngineCallback iUpdateEngineCallback1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.os.IUpdateEngine");
            iUpdateEngineCallback2 = IUpdateEngineCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            cleanupSuccessfulUpdate(iUpdateEngineCallback2);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            iUpdateEngineCallback2.enforceInterface("android.os.IUpdateEngine");
            str2 = iUpdateEngineCallback2.readString();
            arrayOfString2 = iUpdateEngineCallback2.createStringArray();
            l1 = allocateSpaceForPayload(str2, arrayOfString2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l1);
            return true;
          case 9:
            arrayOfString2.enforceInterface("android.os.IUpdateEngine");
            str1 = arrayOfString2.readString();
            bool = verifyPayloadApplicable(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 8:
            str1.enforceInterface("android.os.IUpdateEngine");
            resetStatus();
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str1.enforceInterface("android.os.IUpdateEngine");
            cancel();
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str1.enforceInterface("android.os.IUpdateEngine");
            resume();
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("android.os.IUpdateEngine");
            suspend();
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("android.os.IUpdateEngine");
            iUpdateEngineCallback1 = IUpdateEngineCallback.Stub.asInterface(str1.readStrongBinder());
            bool = unbind(iUpdateEngineCallback1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 3:
            iUpdateEngineCallback1.enforceInterface("android.os.IUpdateEngine");
            iUpdateEngineCallback1 = IUpdateEngineCallback.Stub.asInterface(iUpdateEngineCallback1.readStrongBinder());
            bool = bind(iUpdateEngineCallback1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 2:
            iUpdateEngineCallback1.enforceInterface("android.os.IUpdateEngine");
            if (iUpdateEngineCallback1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)iUpdateEngineCallback1);
            } else {
              str2 = null;
            } 
            l2 = iUpdateEngineCallback1.readLong();
            l1 = iUpdateEngineCallback1.readLong();
            arrayOfString1 = iUpdateEngineCallback1.createStringArray();
            applyPayloadFd((ParcelFileDescriptor)str2, l2, l1, arrayOfString1);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfString1.enforceInterface("android.os.IUpdateEngine");
        String str2 = arrayOfString1.readString();
        long l2 = arrayOfString1.readLong();
        long l1 = arrayOfString1.readLong();
        String[] arrayOfString1 = arrayOfString1.createStringArray();
        applyPayload(str2, l2, l1, arrayOfString1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.os.IUpdateEngine");
      return true;
    }
    
    private static class Proxy implements IUpdateEngine {
      public static IUpdateEngine sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IUpdateEngine";
      }
      
      public void applyPayload(String param2String, long param2Long1, long param2Long2, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUpdateEngine");
          try {
            parcel1.writeString(param2String);
            try {
              parcel1.writeLong(param2Long1);
              try {
                parcel1.writeLong(param2Long2);
                try {
                  parcel1.writeStringArray(param2ArrayOfString);
                  boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
                  if (!bool && IUpdateEngine.Stub.getDefaultImpl() != null) {
                    IUpdateEngine.Stub.getDefaultImpl().applyPayload(param2String, param2Long1, param2Long2, param2ArrayOfString);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void applyPayloadFd(ParcelFileDescriptor param2ParcelFileDescriptor, long param2Long1, long param2Long2, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUpdateEngine");
          if (param2ParcelFileDescriptor != null) {
            parcel1.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeLong(param2Long1);
            try {
              parcel1.writeLong(param2Long2);
              try {
                parcel1.writeStringArray(param2ArrayOfString);
                boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
                if (!bool && IUpdateEngine.Stub.getDefaultImpl() != null) {
                  IUpdateEngine.Stub.getDefaultImpl().applyPayloadFd(param2ParcelFileDescriptor, param2Long1, param2Long2, param2ArrayOfString);
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } 
                parcel2.readException();
                parcel2.recycle();
                parcel1.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ParcelFileDescriptor;
      }
      
      public boolean bind(IUpdateEngineCallback param2IUpdateEngineCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUpdateEngine");
          if (param2IUpdateEngineCallback != null) {
            iBinder = param2IUpdateEngineCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IUpdateEngine.Stub.getDefaultImpl() != null) {
            bool1 = IUpdateEngine.Stub.getDefaultImpl().bind(param2IUpdateEngineCallback);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unbind(IUpdateEngineCallback param2IUpdateEngineCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUpdateEngine");
          if (param2IUpdateEngineCallback != null) {
            iBinder = param2IUpdateEngineCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IUpdateEngine.Stub.getDefaultImpl() != null) {
            bool1 = IUpdateEngine.Stub.getDefaultImpl().unbind(param2IUpdateEngineCallback);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void suspend() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUpdateEngine");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IUpdateEngine.Stub.getDefaultImpl() != null) {
            IUpdateEngine.Stub.getDefaultImpl().suspend();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resume() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUpdateEngine");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IUpdateEngine.Stub.getDefaultImpl() != null) {
            IUpdateEngine.Stub.getDefaultImpl().resume();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancel() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUpdateEngine");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IUpdateEngine.Stub.getDefaultImpl() != null) {
            IUpdateEngine.Stub.getDefaultImpl().cancel();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUpdateEngine");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IUpdateEngine.Stub.getDefaultImpl() != null) {
            IUpdateEngine.Stub.getDefaultImpl().resetStatus();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean verifyPayloadApplicable(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUpdateEngine");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IUpdateEngine.Stub.getDefaultImpl() != null) {
            bool1 = IUpdateEngine.Stub.getDefaultImpl().verifyPayloadApplicable(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long allocateSpaceForPayload(String param2String, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUpdateEngine");
          parcel1.writeString(param2String);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IUpdateEngine.Stub.getDefaultImpl() != null)
            return IUpdateEngine.Stub.getDefaultImpl().allocateSpaceForPayload(param2String, param2ArrayOfString); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cleanupSuccessfulUpdate(IUpdateEngineCallback param2IUpdateEngineCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IUpdateEngine");
          if (param2IUpdateEngineCallback != null) {
            iBinder = param2IUpdateEngineCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IUpdateEngine.Stub.getDefaultImpl() != null) {
            IUpdateEngine.Stub.getDefaultImpl().cleanupSuccessfulUpdate(param2IUpdateEngineCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IUpdateEngine param1IUpdateEngine) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IUpdateEngine != null) {
          Proxy.sDefaultImpl = param1IUpdateEngine;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IUpdateEngine getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
