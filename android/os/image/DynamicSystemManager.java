package android.os.image;

import android.gsi.AvbPublicKey;
import android.gsi.GsiProgress;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public class DynamicSystemManager {
  private static final String TAG = "DynamicSystemManager";
  
  private final IDynamicSystemService mService;
  
  public DynamicSystemManager(IDynamicSystemService paramIDynamicSystemService) {
    this.mService = paramIDynamicSystemService;
  }
  
  public class Session {
    final DynamicSystemManager this$0;
    
    private Session() {}
    
    public boolean setAshmem(ParcelFileDescriptor param1ParcelFileDescriptor, long param1Long) {
      try {
        return DynamicSystemManager.this.mService.setAshmem(param1ParcelFileDescriptor, param1Long);
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException.toString());
      } 
    }
    
    public boolean submitFromAshmem(int param1Int) {
      try {
        return DynamicSystemManager.this.mService.submitFromAshmem(param1Int);
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException.toString());
      } 
    }
    
    public boolean getAvbPublicKey(AvbPublicKey param1AvbPublicKey) {
      try {
        return DynamicSystemManager.this.mService.getAvbPublicKey(param1AvbPublicKey);
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException.toString());
      } 
    }
    
    public boolean commit() {
      try {
        return DynamicSystemManager.this.mService.setEnable(true, true);
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException.toString());
      } 
    }
  }
  
  public boolean startInstallation(String paramString) {
    try {
      return this.mService.startInstallation(paramString);
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException.toString());
    } 
  }
  
  public Session createPartition(String paramString, long paramLong, boolean paramBoolean) {
    try {
      if (this.mService.createPartition(paramString, paramLong, paramBoolean))
        return new Session(); 
      return null;
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException.toString());
    } 
  }
  
  public boolean finishInstallation() {
    try {
      return this.mService.finishInstallation();
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException.toString());
    } 
  }
  
  public GsiProgress getInstallationProgress() {
    try {
      return this.mService.getInstallationProgress();
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException.toString());
    } 
  }
  
  public boolean abort() {
    try {
      return this.mService.abort();
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException.toString());
    } 
  }
  
  public boolean isInUse() {
    try {
      return this.mService.isInUse();
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException.toString());
    } 
  }
  
  public boolean isInstalled() {
    try {
      return this.mService.isInstalled();
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException.toString());
    } 
  }
  
  public boolean isEnabled() {
    try {
      return this.mService.isEnabled();
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException.toString());
    } 
  }
  
  public boolean remove() {
    try {
      return this.mService.remove();
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException.toString());
    } 
  }
  
  public boolean setEnable(boolean paramBoolean1, boolean paramBoolean2) {
    try {
      return this.mService.setEnable(paramBoolean1, paramBoolean2);
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException.toString());
    } 
  }
}
