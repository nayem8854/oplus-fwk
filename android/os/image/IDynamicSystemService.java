package android.os.image;

import android.gsi.AvbPublicKey;
import android.gsi.GsiProgress;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public interface IDynamicSystemService extends IInterface {
  boolean abort() throws RemoteException;
  
  boolean createPartition(String paramString, long paramLong, boolean paramBoolean) throws RemoteException;
  
  boolean finishInstallation() throws RemoteException;
  
  boolean getAvbPublicKey(AvbPublicKey paramAvbPublicKey) throws RemoteException;
  
  GsiProgress getInstallationProgress() throws RemoteException;
  
  boolean isEnabled() throws RemoteException;
  
  boolean isInUse() throws RemoteException;
  
  boolean isInstalled() throws RemoteException;
  
  boolean remove() throws RemoteException;
  
  boolean setAshmem(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong) throws RemoteException;
  
  boolean setEnable(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  boolean startInstallation(String paramString) throws RemoteException;
  
  boolean submitFromAshmem(long paramLong) throws RemoteException;
  
  class Default implements IDynamicSystemService {
    public boolean startInstallation(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean createPartition(String param1String, long param1Long, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean finishInstallation() throws RemoteException {
      return false;
    }
    
    public GsiProgress getInstallationProgress() throws RemoteException {
      return null;
    }
    
    public boolean abort() throws RemoteException {
      return false;
    }
    
    public boolean isInUse() throws RemoteException {
      return false;
    }
    
    public boolean isInstalled() throws RemoteException {
      return false;
    }
    
    public boolean isEnabled() throws RemoteException {
      return false;
    }
    
    public boolean remove() throws RemoteException {
      return false;
    }
    
    public boolean setEnable(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {
      return false;
    }
    
    public boolean setAshmem(ParcelFileDescriptor param1ParcelFileDescriptor, long param1Long) throws RemoteException {
      return false;
    }
    
    public boolean submitFromAshmem(long param1Long) throws RemoteException {
      return false;
    }
    
    public boolean getAvbPublicKey(AvbPublicKey param1AvbPublicKey) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDynamicSystemService {
    private static final String DESCRIPTOR = "android.os.image.IDynamicSystemService";
    
    static final int TRANSACTION_abort = 5;
    
    static final int TRANSACTION_createPartition = 2;
    
    static final int TRANSACTION_finishInstallation = 3;
    
    static final int TRANSACTION_getAvbPublicKey = 13;
    
    static final int TRANSACTION_getInstallationProgress = 4;
    
    static final int TRANSACTION_isEnabled = 8;
    
    static final int TRANSACTION_isInUse = 6;
    
    static final int TRANSACTION_isInstalled = 7;
    
    static final int TRANSACTION_remove = 9;
    
    static final int TRANSACTION_setAshmem = 11;
    
    static final int TRANSACTION_setEnable = 10;
    
    static final int TRANSACTION_startInstallation = 1;
    
    static final int TRANSACTION_submitFromAshmem = 12;
    
    public Stub() {
      attachInterface(this, "android.os.image.IDynamicSystemService");
    }
    
    public static IDynamicSystemService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.image.IDynamicSystemService");
      if (iInterface != null && iInterface instanceof IDynamicSystemService)
        return (IDynamicSystemService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 13:
          return "getAvbPublicKey";
        case 12:
          return "submitFromAshmem";
        case 11:
          return "setAshmem";
        case 10:
          return "setEnable";
        case 9:
          return "remove";
        case 8:
          return "isEnabled";
        case 7:
          return "isInstalled";
        case 6:
          return "isInUse";
        case 5:
          return "abort";
        case 4:
          return "getInstallationProgress";
        case 3:
          return "finishInstallation";
        case 2:
          return "createPartition";
        case 1:
          break;
      } 
      return "startInstallation";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        AvbPublicKey avbPublicKey;
        GsiProgress gsiProgress;
        long l;
        ParcelFileDescriptor parcelFileDescriptor;
        String str2;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 13:
            param1Parcel1.enforceInterface("android.os.image.IDynamicSystemService");
            avbPublicKey = new AvbPublicKey();
            bool = getAvbPublicKey(avbPublicKey);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            param1Parcel2.writeInt(1);
            avbPublicKey.writeToParcel(param1Parcel2, 1);
            return true;
          case 12:
            avbPublicKey.enforceInterface("android.os.image.IDynamicSystemService");
            l = avbPublicKey.readLong();
            bool = submitFromAshmem(l);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 11:
            avbPublicKey.enforceInterface("android.os.image.IDynamicSystemService");
            if (avbPublicKey.readInt() != 0) {
              parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)avbPublicKey);
            } else {
              parcelFileDescriptor = null;
            } 
            l = avbPublicKey.readLong();
            bool = setAshmem(parcelFileDescriptor, l);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 10:
            avbPublicKey.enforceInterface("android.os.image.IDynamicSystemService");
            if (avbPublicKey.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            if (avbPublicKey.readInt() != 0)
              bool2 = true; 
            bool = setEnable(bool1, bool2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 9:
            avbPublicKey.enforceInterface("android.os.image.IDynamicSystemService");
            bool = remove();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 8:
            avbPublicKey.enforceInterface("android.os.image.IDynamicSystemService");
            bool = isEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 7:
            avbPublicKey.enforceInterface("android.os.image.IDynamicSystemService");
            bool = isInstalled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 6:
            avbPublicKey.enforceInterface("android.os.image.IDynamicSystemService");
            bool = isInUse();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 5:
            avbPublicKey.enforceInterface("android.os.image.IDynamicSystemService");
            bool = abort();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            avbPublicKey.enforceInterface("android.os.image.IDynamicSystemService");
            gsiProgress = getInstallationProgress();
            param1Parcel2.writeNoException();
            if (gsiProgress != null) {
              param1Parcel2.writeInt(1);
              gsiProgress.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            gsiProgress.enforceInterface("android.os.image.IDynamicSystemService");
            bool = finishInstallation();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 2:
            gsiProgress.enforceInterface("android.os.image.IDynamicSystemService");
            str2 = gsiProgress.readString();
            l = gsiProgress.readLong();
            if (gsiProgress.readInt() != 0)
              bool1 = true; 
            bool = createPartition(str2, l, bool1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 1:
            break;
        } 
        gsiProgress.enforceInterface("android.os.image.IDynamicSystemService");
        String str1 = gsiProgress.readString();
        boolean bool = startInstallation(str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool);
        return true;
      } 
      param1Parcel2.writeString("android.os.image.IDynamicSystemService");
      return true;
    }
    
    private static class Proxy implements IDynamicSystemService {
      public static IDynamicSystemService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.image.IDynamicSystemService";
      }
      
      public boolean startInstallation(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.image.IDynamicSystemService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IDynamicSystemService.Stub.getDefaultImpl() != null) {
            bool1 = IDynamicSystemService.Stub.getDefaultImpl().startInstallation(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean createPartition(String param2String, long param2Long, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.image.IDynamicSystemService");
          parcel1.writeString(param2String);
          parcel1.writeLong(param2Long);
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IDynamicSystemService.Stub.getDefaultImpl() != null) {
            param2Boolean = IDynamicSystemService.Stub.getDefaultImpl().createPartition(param2String, param2Long, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean finishInstallation() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.image.IDynamicSystemService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IDynamicSystemService.Stub.getDefaultImpl() != null) {
            bool1 = IDynamicSystemService.Stub.getDefaultImpl().finishInstallation();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public GsiProgress getInstallationProgress() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          GsiProgress gsiProgress;
          parcel1.writeInterfaceToken("android.os.image.IDynamicSystemService");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IDynamicSystemService.Stub.getDefaultImpl() != null) {
            gsiProgress = IDynamicSystemService.Stub.getDefaultImpl().getInstallationProgress();
            return gsiProgress;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            gsiProgress = GsiProgress.CREATOR.createFromParcel(parcel2);
          } else {
            gsiProgress = null;
          } 
          return gsiProgress;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean abort() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.image.IDynamicSystemService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IDynamicSystemService.Stub.getDefaultImpl() != null) {
            bool1 = IDynamicSystemService.Stub.getDefaultImpl().abort();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInUse() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.image.IDynamicSystemService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IDynamicSystemService.Stub.getDefaultImpl() != null) {
            bool1 = IDynamicSystemService.Stub.getDefaultImpl().isInUse();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInstalled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.image.IDynamicSystemService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IDynamicSystemService.Stub.getDefaultImpl() != null) {
            bool1 = IDynamicSystemService.Stub.getDefaultImpl().isInstalled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.image.IDynamicSystemService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IDynamicSystemService.Stub.getDefaultImpl() != null) {
            bool1 = IDynamicSystemService.Stub.getDefaultImpl().isEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean remove() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.image.IDynamicSystemService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IDynamicSystemService.Stub.getDefaultImpl() != null) {
            bool1 = IDynamicSystemService.Stub.getDefaultImpl().remove();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setEnable(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.image.IDynamicSystemService");
          boolean bool = true;
          if (param2Boolean1) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          if (param2Boolean2) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool1 && IDynamicSystemService.Stub.getDefaultImpl() != null) {
            param2Boolean1 = IDynamicSystemService.Stub.getDefaultImpl().setEnable(param2Boolean1, param2Boolean2);
            return param2Boolean1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean1 = bool;
          } else {
            param2Boolean1 = false;
          } 
          return param2Boolean1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAshmem(ParcelFileDescriptor param2ParcelFileDescriptor, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.image.IDynamicSystemService");
          boolean bool1 = true;
          if (param2ParcelFileDescriptor != null) {
            parcel1.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeLong(param2Long);
          boolean bool2 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IDynamicSystemService.Stub.getDefaultImpl() != null) {
            bool1 = IDynamicSystemService.Stub.getDefaultImpl().setAshmem(param2ParcelFileDescriptor, param2Long);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean submitFromAshmem(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.image.IDynamicSystemService");
          parcel1.writeLong(param2Long);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && IDynamicSystemService.Stub.getDefaultImpl() != null) {
            bool1 = IDynamicSystemService.Stub.getDefaultImpl().submitFromAshmem(param2Long);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getAvbPublicKey(AvbPublicKey param2AvbPublicKey) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.image.IDynamicSystemService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IDynamicSystemService.Stub.getDefaultImpl() != null) {
            bool1 = IDynamicSystemService.Stub.getDefaultImpl().getAvbPublicKey(param2AvbPublicKey);
            return bool1;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            bool1 = true; 
          if (parcel2.readInt() != 0)
            param2AvbPublicKey.readFromParcel(parcel2); 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDynamicSystemService param1IDynamicSystemService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDynamicSystemService != null) {
          Proxy.sDefaultImpl = param1IDynamicSystemService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDynamicSystemService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
