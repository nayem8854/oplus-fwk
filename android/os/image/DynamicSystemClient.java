package android.os.image;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.ParcelableException;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.Slog;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;

@SystemApi
public class DynamicSystemClient {
  public static final String ACTION_NOTIFY_IF_IN_USE = "android.os.image.action.NOTIFY_IF_IN_USE";
  
  public static final String ACTION_START_INSTALL = "android.os.image.action.START_INSTALL";
  
  public static final int CAUSE_ERROR_EXCEPTION = 6;
  
  public static final int CAUSE_ERROR_INVALID_URL = 4;
  
  public static final int CAUSE_ERROR_IO = 3;
  
  public static final int CAUSE_ERROR_IPC = 5;
  
  public static final int CAUSE_INSTALL_CANCELLED = 2;
  
  public static final int CAUSE_INSTALL_COMPLETED = 1;
  
  public static final int CAUSE_NOT_SPECIFIED = 0;
  
  public static final String KEY_EXCEPTION_DETAIL = "KEY_EXCEPTION_DETAIL";
  
  public static final String KEY_INSTALLED_SIZE = "KEY_INSTALLED_SIZE";
  
  public static final String KEY_SYSTEM_SIZE = "KEY_SYSTEM_SIZE";
  
  public static final String KEY_USERDATA_SIZE = "KEY_USERDATA_SIZE";
  
  public static final int MSG_POST_STATUS = 3;
  
  public static final int MSG_REGISTER_LISTENER = 1;
  
  public static final int MSG_UNREGISTER_LISTENER = 2;
  
  public static final int STATUS_IN_PROGRESS = 2;
  
  public static final int STATUS_IN_USE = 4;
  
  public static final int STATUS_NOT_STARTED = 1;
  
  public static final int STATUS_READY = 3;
  
  public static final int STATUS_UNKNOWN = 0;
  
  private static final String TAG = "DynSystemClient";
  
  private boolean mBound;
  
  private final DynSystemServiceConnection mConnection;
  
  private final Context mContext;
  
  private Executor mExecutor;
  
  private OnStatusChangedListener mListener;
  
  private final Messenger mMessenger;
  
  private Messenger mService;
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface StatusChangedCause {}
  
  public static interface OnStatusChangedListener {
    void onStatusChanged(int param1Int1, int param1Int2, long param1Long, Throwable param1Throwable);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface InstallationStatus {}
  
  class IncomingHandler extends Handler {
    private final WeakReference<DynamicSystemClient> mWeakClient;
    
    IncomingHandler(DynamicSystemClient this$0) {
      super(Looper.getMainLooper());
      this.mWeakClient = new WeakReference<>(this$0);
    }
    
    public void handleMessage(Message param1Message) {
      DynamicSystemClient dynamicSystemClient = this.mWeakClient.get();
      if (dynamicSystemClient != null)
        dynamicSystemClient.handleMessage(param1Message); 
    }
  }
  
  class DynSystemServiceConnection implements ServiceConnection {
    final DynamicSystemClient this$0;
    
    private DynSystemServiceConnection() {}
    
    public void onServiceConnected(ComponentName param1ComponentName, IBinder param1IBinder) {
      Slog.v("DynSystemClient", "DynSystemService connected");
      DynamicSystemClient.access$102(DynamicSystemClient.this, new Messenger(param1IBinder));
      try {
        Message message = Message.obtain((Handler)null, 1);
        message.replyTo = DynamicSystemClient.this.mMessenger;
        DynamicSystemClient.this.mService.send(message);
      } catch (RemoteException remoteException) {
        Slog.e("DynSystemClient", "Unable to get status from installation service");
        if (DynamicSystemClient.this.mExecutor != null) {
          DynamicSystemClient.this.mExecutor.execute(new _$$Lambda$DynamicSystemClient$DynSystemServiceConnection$Q_VWaYUew87mkpsE47b33p5XLa8(this, remoteException));
        } else {
          DynamicSystemClient.this.mListener.onStatusChanged(0, 5, 0L, (Throwable)remoteException);
        } 
      } 
    }
    
    public void onServiceDisconnected(ComponentName param1ComponentName) {
      Slog.v("DynSystemClient", "DynSystemService disconnected");
      DynamicSystemClient.access$102(DynamicSystemClient.this, null);
    }
  }
  
  @SystemApi
  public DynamicSystemClient(Context paramContext) {
    this.mContext = paramContext;
    this.mConnection = new DynSystemServiceConnection();
    this.mMessenger = new Messenger(new IncomingHandler(this));
  }
  
  public void setOnStatusChangedListener(Executor paramExecutor, OnStatusChangedListener paramOnStatusChangedListener) {
    this.mListener = paramOnStatusChangedListener;
    this.mExecutor = paramExecutor;
  }
  
  public void setOnStatusChangedListener(OnStatusChangedListener paramOnStatusChangedListener) {
    this.mListener = paramOnStatusChangedListener;
    this.mExecutor = null;
  }
  
  @SystemApi
  public void bind() {
    if (!featureFlagEnabled()) {
      Slog.w("DynSystemClient", "settings_dynamic_system not enabled; bind() aborted.");
      return;
    } 
    Intent intent = new Intent();
    intent.setClassName("com.android.dynsystem", "com.android.dynsystem.DynamicSystemInstallationService");
    this.mContext.bindService(intent, this.mConnection, 1);
    this.mBound = true;
  }
  
  @SystemApi
  public void unbind() {
    if (!this.mBound)
      return; 
    if (this.mService != null)
      try {
        Message message = Message.obtain((Handler)null, 2);
        message.replyTo = this.mMessenger;
        this.mService.send(message);
      } catch (RemoteException remoteException) {
        Slog.e("DynSystemClient", "Unable to unregister from installation service");
      }  
    this.mContext.unbindService(this.mConnection);
    this.mBound = false;
  }
  
  @SystemApi
  public void start(Uri paramUri, long paramLong) {
    start(paramUri, paramLong, 0L);
  }
  
  public void start(Uri paramUri, long paramLong1, long paramLong2) {
    if (!featureFlagEnabled()) {
      Slog.w("DynSystemClient", "settings_dynamic_system not enabled; start() aborted.");
      return;
    } 
    Intent intent = new Intent();
    intent.setClassName("com.android.dynsystem", "com.android.dynsystem.VerificationActivity");
    intent.setData(paramUri);
    intent.setAction("android.os.image.action.START_INSTALL");
    intent.putExtra("KEY_SYSTEM_SIZE", paramLong1);
    intent.putExtra("KEY_USERDATA_SIZE", paramLong2);
    this.mContext.startActivity(intent);
  }
  
  private boolean featureFlagEnabled() {
    return SystemProperties.getBoolean("persist.sys.fflag.override.settings_dynamic_system", false);
  }
  
  private void handleMessage(Message paramMessage) {
    if (paramMessage.what == 3) {
      int i = paramMessage.arg1;
      int j = paramMessage.arg2;
      Bundle bundle = (Bundle)paramMessage.obj;
      long l = bundle.getLong("KEY_INSTALLED_SIZE");
      Throwable throwable = (ParcelableException)bundle.getSerializable("KEY_EXCEPTION_DETAIL");
      if (throwable == null) {
        throwable = null;
      } else {
        throwable = throwable.getCause();
      } 
      Executor executor = this.mExecutor;
      if (executor != null) {
        executor.execute(new _$$Lambda$DynamicSystemClient$j9BjPR3q6kOr_cwQrk0KAsVFWNQ(this, i, j, l, throwable));
      } else {
        this.mListener.onStatusChanged(i, j, l, throwable);
      } 
    } 
  }
}
