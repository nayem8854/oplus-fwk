package android.os;

import android.annotation.SystemApi;
import android.app.ActivityThread;
import android.app.ContextImpl;
import android.content.Context;
import android.media.AudioAttributes;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.concurrent.Executor;

public abstract class Vibrator {
  private static final String TAG = "Vibrator";
  
  public static final int VIBRATION_EFFECT_SUPPORT_NO = 2;
  
  public static final int VIBRATION_EFFECT_SUPPORT_UNKNOWN = 0;
  
  public static final int VIBRATION_EFFECT_SUPPORT_YES = 1;
  
  public static final int VIBRATION_INTENSITY_HIGH = 3;
  
  public static final int VIBRATION_INTENSITY_LOW = 1;
  
  public static final int VIBRATION_INTENSITY_MEDIUM = 2;
  
  public static final int VIBRATION_INTENSITY_OFF = 0;
  
  private int mDefaultHapticFeedbackIntensity;
  
  private int mDefaultNotificationVibrationIntensity;
  
  private int mDefaultRingVibrationIntensity;
  
  private final String mPackageName;
  
  public Vibrator() {
    this.mPackageName = ActivityThread.currentPackageName();
    ContextImpl contextImpl = ActivityThread.currentActivityThread().getSystemContext();
    loadVibrationIntensities((Context)contextImpl);
  }
  
  protected Vibrator(Context paramContext) {
    this.mPackageName = paramContext.getOpPackageName();
    loadVibrationIntensities(paramContext);
  }
  
  private void loadVibrationIntensities(Context paramContext) {
    this.mDefaultHapticFeedbackIntensity = loadDefaultIntensity(paramContext, 17694773);
    this.mDefaultNotificationVibrationIntensity = loadDefaultIntensity(paramContext, 17694780);
    this.mDefaultRingVibrationIntensity = loadDefaultIntensity(paramContext, 17694785);
  }
  
  private int loadDefaultIntensity(Context paramContext, int paramInt) {
    if (paramContext != null) {
      paramInt = paramContext.getResources().getInteger(paramInt);
    } else {
      paramInt = 2;
    } 
    return paramInt;
  }
  
  public int getDefaultHapticFeedbackIntensity() {
    return this.mDefaultHapticFeedbackIntensity;
  }
  
  public int getDefaultNotificationVibrationIntensity() {
    return this.mDefaultNotificationVibrationIntensity;
  }
  
  public int getDefaultRingVibrationIntensity() {
    return this.mDefaultRingVibrationIntensity;
  }
  
  public boolean setAlwaysOnEffect(int paramInt, VibrationEffect paramVibrationEffect, AudioAttributes paramAudioAttributes) {
    return setAlwaysOnEffect(Process.myUid(), this.mPackageName, paramInt, paramVibrationEffect, paramAudioAttributes);
  }
  
  public boolean setAlwaysOnEffect(int paramInt1, String paramString, int paramInt2, VibrationEffect paramVibrationEffect, AudioAttributes paramAudioAttributes) {
    Log.w("Vibrator", "Always-on effects aren't supported");
    return false;
  }
  
  @Deprecated
  public void vibrate(long paramLong) {
    vibrate(paramLong, (AudioAttributes)null);
  }
  
  @Deprecated
  public void vibrate(long paramLong, AudioAttributes paramAudioAttributes) {
    try {
      VibrationEffect vibrationEffect = VibrationEffect.createOneShot(paramLong, -1);
      vibrate(vibrationEffect, paramAudioAttributes);
    } catch (IllegalArgumentException illegalArgumentException) {
      Log.e("Vibrator", "Failed to create VibrationEffect", illegalArgumentException);
    } 
  }
  
  @Deprecated
  public void vibrate(long[] paramArrayOflong, int paramInt) {
    vibrate(paramArrayOflong, paramInt, null);
  }
  
  @Deprecated
  public void vibrate(long[] paramArrayOflong, int paramInt, AudioAttributes paramAudioAttributes) {
    if (paramInt >= -1 && paramInt < paramArrayOflong.length) {
      try {
        vibrate(VibrationEffect.createWaveform(paramArrayOflong, paramInt), paramAudioAttributes);
      } catch (IllegalArgumentException illegalArgumentException) {
        Log.e("Vibrator", "Failed to create VibrationEffect", illegalArgumentException);
      } 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("vibrate called with repeat index out of bounds (pattern.length=");
    stringBuilder.append(illegalArgumentException.length);
    stringBuilder.append(", index=");
    stringBuilder.append(paramInt);
    stringBuilder.append(")");
    Log.e("Vibrator", stringBuilder.toString());
    throw new ArrayIndexOutOfBoundsException();
  }
  
  public void vibrate(VibrationEffect paramVibrationEffect) {
    vibrate(paramVibrationEffect, (AudioAttributes)null);
  }
  
  public void vibrate(VibrationEffect paramVibrationEffect, AudioAttributes paramAudioAttributes) {
    vibrate(Process.myUid(), this.mPackageName, paramVibrationEffect, null, paramAudioAttributes);
  }
  
  public int[] areEffectsSupported(int... paramVarArgs) {
    paramVarArgs = new int[paramVarArgs.length];
    Arrays.fill(paramVarArgs, 2);
    return paramVarArgs;
  }
  
  public final int areAllEffectsSupported(int... paramVarArgs) {
    boolean bool = true;
    int i;
    byte b;
    for (paramVarArgs = areEffectsSupported(paramVarArgs), i = paramVarArgs.length, b = 0; b < i; ) {
      int j = paramVarArgs[b];
      if (j == 2)
        return 2; 
      if (j == 0)
        bool = false; 
      b++;
    } 
    return bool;
  }
  
  public boolean[] arePrimitivesSupported(int... paramVarArgs) {
    return new boolean[paramVarArgs.length];
  }
  
  public final boolean areAllPrimitivesSupported(int... paramVarArgs) {
    for (boolean bool : arePrimitivesSupported(paramVarArgs)) {
      if (!bool)
        return false; 
    } 
    return true;
  }
  
  @SystemApi
  public boolean isVibrating() {
    return false;
  }
  
  @SystemApi
  public void addVibratorStateListener(OnVibratorStateChangedListener paramOnVibratorStateChangedListener) {}
  
  @SystemApi
  public void addVibratorStateListener(Executor paramExecutor, OnVibratorStateChangedListener paramOnVibratorStateChangedListener) {}
  
  @SystemApi
  public void removeVibratorStateListener(OnVibratorStateChangedListener paramOnVibratorStateChangedListener) {}
  
  public void linearMotorVibrate(int paramInt1, String paramString1, int[] paramArrayOfint, long[] paramArrayOflong, int paramInt2, int paramInt3, String paramString2, AudioAttributes paramAudioAttributes, IBinder paramIBinder) {}
  
  public abstract void cancel();
  
  public abstract boolean hasAmplitudeControl();
  
  public abstract boolean hasVibrator();
  
  public abstract void vibrate(int paramInt, String paramString1, VibrationEffect paramVibrationEffect, String paramString2, AudioAttributes paramAudioAttributes);
  
  @SystemApi
  public static interface OnVibratorStateChangedListener {
    void onVibratorStateChanged(boolean param1Boolean);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface VibrationEffectSupport {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface VibrationIntensity {}
}
