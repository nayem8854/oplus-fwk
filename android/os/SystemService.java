package android.os;

import com.google.android.collect.Maps;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;

public class SystemService {
  private static Object sPropertyLock;
  
  private static HashMap<String, State> sStates = Maps.newHashMap();
  
  public enum State {
    RESTARTING,
    RUNNING("running"),
    STOPPED("running"),
    STOPPING("stopping");
    
    private static final State[] $VALUES;
    
    static {
      State state = new State("RESTARTING", 3, "restarting");
      $VALUES = new State[] { RUNNING, STOPPING, STOPPED, state };
    }
    
    State(String param1String1) {
      SystemService.sStates.put(param1String1, this);
    }
  }
  
  static {
    sPropertyLock = new Object();
    SystemProperties.addChangeCallback(new Runnable() {
          public void run() {
            synchronized (SystemService.sPropertyLock) {
              SystemService.sPropertyLock.notifyAll();
              return;
            } 
          }
        });
  }
  
  public static void start(String paramString) {
    SystemProperties.set("ctl.start", paramString);
  }
  
  public static void stop(String paramString) {
    SystemProperties.set("ctl.stop", paramString);
  }
  
  public static void restart(String paramString) {
    SystemProperties.set("ctl.restart", paramString);
  }
  
  public static State getState(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("init.svc.");
    stringBuilder.append(paramString);
    paramString = SystemProperties.get(stringBuilder.toString());
    State state = sStates.get(paramString);
    if (state != null)
      return state; 
    return State.STOPPED;
  }
  
  public static boolean isStopped(String paramString) {
    return State.STOPPED.equals(getState(paramString));
  }
  
  public static boolean isRunning(String paramString) {
    return State.RUNNING.equals(getState(paramString));
  }
  
  public static void waitForState(String paramString, State paramState, long paramLong) throws TimeoutException {
    long l = SystemClock.elapsedRealtime();
    while (true) {
      synchronized (sPropertyLock) {
        State state = getState(paramString);
        if (paramState.equals(state))
          return; 
        long l1 = SystemClock.elapsedRealtime();
        if (l1 < l + paramLong) {
          try {
            sPropertyLock.wait(paramLong);
          } catch (InterruptedException interruptedException) {}
          continue;
        } 
        TimeoutException timeoutException = new TimeoutException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Service ");
        stringBuilder.append(paramString);
        stringBuilder.append(" currently ");
        stringBuilder.append(state);
        stringBuilder.append("; waited ");
        stringBuilder.append(paramLong);
        stringBuilder.append("ms for ");
        stringBuilder.append(paramState);
        this(stringBuilder.toString());
        throw timeoutException;
      } 
    } 
  }
  
  public static void waitForAnyStopped(String... paramVarArgs) {
    while (true) {
      synchronized (sPropertyLock) {
        int i;
        byte b;
        for (i = paramVarArgs.length, b = 0; b < i; ) {
          String str = paramVarArgs[b];
          if (State.STOPPED.equals(getState(str)))
            return; 
          b++;
        } 
        try {
          sPropertyLock.wait();
        } catch (InterruptedException interruptedException) {}
      } 
    } 
  }
}
