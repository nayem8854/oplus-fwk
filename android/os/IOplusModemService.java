package android.os;

public interface IOplusModemService extends IInterface {
  class Default implements IOplusModemService {
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusModemService {
    private static final String DESCRIPTOR = "android.os.IOplusModemService";
    
    public Stub() {
      attachInterface(this, "android.os.IOplusModemService");
    }
    
    public static IOplusModemService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IOplusModemService");
      if (iInterface != null && iInterface instanceof IOplusModemService)
        return (IOplusModemService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      return null;
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902)
        return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
      param1Parcel2.writeString("android.os.IOplusModemService");
      return true;
    }
    
    private static class Proxy implements IOplusModemService {
      public static IOplusModemService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IOplusModemService";
      }
    }
    
    public static boolean setDefaultImpl(IOplusModemService param1IOplusModemService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusModemService != null) {
          Proxy.sDefaultImpl = param1IOplusModemService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusModemService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
