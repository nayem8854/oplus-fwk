package android.os;

import android.util.Log;
import vendor.oplus.hardware.lmvibrator.V1_0.ILinearMotorVibrator;

public class LinearMotorVibratorManager {
  private static final String TAG = "LinearMotorVibratorManager";
  
  private static ILinearMotorVibrator sLinearMotorVibrateService = null;
  
  private static ILinearMotorVibrator getLinearMotorVibrateService() {
    if (sLinearMotorVibrateService == null)
      try {
        sLinearMotorVibrateService = ILinearMotorVibrator.getService();
      } catch (Exception exception) {
        Log.e("LinearMotorVibratorManager", "Failed to get linear motor vibrator interface", exception);
      }  
    return sLinearMotorVibrateService;
  }
  
  public static void turnOffLinearMotorVibrator() {
    try {
      ILinearMotorVibrator iLinearMotorVibrator = getLinearMotorVibrateService();
      if (iLinearMotorVibrator != null)
        iLinearMotorVibrator.linearmotorVibratorOff(); 
    } catch (RemoteException remoteException) {
      Log.e("LinearMotorVibratorManager", "turnOffLinearMotorVibrator failed.", (Throwable)remoteException);
    } 
  }
  
  public static void turnOnLinearmotorVibrator(int paramInt, short paramShort, boolean paramBoolean) {
    try {
      ILinearMotorVibrator iLinearMotorVibrator = getLinearMotorVibrateService();
      if (iLinearMotorVibrator != null)
        iLinearMotorVibrator.linearmotorVibratorOn(paramInt, paramShort, paramBoolean); 
    } catch (RemoteException remoteException) {
      Log.e("LinearMotorVibratorManager", "turnOnLinearmotorVibrator failed.", (Throwable)remoteException);
    } 
  }
}
