package android.os;

import android.util.Slog;
import java.util.Map;

public class VintfObject {
  private static final String LOG_TAG = "VintfObject";
  
  public static native String[] getHalNamesAndVersions();
  
  public static native String getSepolicyVersion();
  
  public static native Long getTargetFrameworkCompatibilityMatrixVersion();
  
  public static native Map<String, String[]> getVndkSnapshots();
  
  public static native String[] report();
  
  @Deprecated
  public static int verify(String[] paramArrayOfString) {
    if (paramArrayOfString != null && paramArrayOfString.length > 0) {
      Slog.w("VintfObject", "VintfObject.verify() with non-empty packageInfo is deprecated. Skipping compatibility checks for update package.");
      return 0;
    } 
    Slog.w("VintfObject", "VintfObject.verify() is deprecated. Call verifyWithoutAvb() instead.");
    return verifyWithoutAvb();
  }
  
  public static native int verifyWithoutAvb();
}
