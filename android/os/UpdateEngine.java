package android.os;

import android.annotation.SystemApi;
import android.content.res.AssetFileDescriptor;

@SystemApi
public class UpdateEngine {
  private static final String TAG = "UpdateEngine";
  
  private static final String UPDATE_ENGINE_SERVICE = "android.os.UpdateEngineService";
  
  private IUpdateEngine mUpdateEngine;
  
  public static final class ErrorCodeConstants {
    public static final int DEVICE_CORRUPTED = 61;
    
    public static final int DOWNLOAD_PAYLOAD_VERIFICATION_ERROR = 12;
    
    public static final int DOWNLOAD_TRANSFER_ERROR = 9;
    
    public static final int ERROR = 1;
    
    public static final int FILESYSTEM_COPIER_ERROR = 4;
    
    public static final int INSTALL_DEVICE_OPEN_ERROR = 7;
    
    public static final int KERNEL_DEVICE_OPEN_ERROR = 8;
    
    public static final int NOT_ENOUGH_SPACE = 60;
    
    public static final int PAYLOAD_HASH_MISMATCH_ERROR = 10;
    
    public static final int PAYLOAD_MISMATCHED_TYPE_ERROR = 6;
    
    public static final int PAYLOAD_SIZE_MISMATCH_ERROR = 11;
    
    public static final int PAYLOAD_TIMESTAMP_ERROR = 51;
    
    public static final int POST_INSTALL_RUNNER_ERROR = 5;
    
    public static final int SUCCESS = 0;
    
    public static final int UPDATED_BUT_NOT_ACTIVE = 52;
  }
  
  public static final class UpdateStatusConstants {
    public static final int ATTEMPTING_ROLLBACK = 8;
    
    public static final int CHECKING_FOR_UPDATE = 1;
    
    public static final int DISABLED = 9;
    
    public static final int DOWNLOADING = 3;
    
    public static final int FINALIZING = 5;
    
    public static final int IDLE = 0;
    
    public static final int REPORTING_ERROR_EVENT = 7;
    
    public static final int UPDATED_NEED_REBOOT = 6;
    
    public static final int UPDATE_AVAILABLE = 2;
    
    public static final int VERIFYING = 4;
  }
  
  private IUpdateEngineCallback mUpdateEngineCallback = null;
  
  private final Object mUpdateEngineCallbackLock = new Object();
  
  public UpdateEngine() {
    IBinder iBinder = ServiceManager.getService("android.os.UpdateEngineService");
    this.mUpdateEngine = IUpdateEngine.Stub.asInterface(iBinder);
  }
  
  public boolean bind(UpdateEngineCallback paramUpdateEngineCallback, Handler paramHandler) {
    synchronized (this.mUpdateEngineCallbackLock) {
      Object object = new Object();
      super(this, paramHandler, paramUpdateEngineCallback);
      this.mUpdateEngineCallback = (IUpdateEngineCallback)object;
      try {
        return this.mUpdateEngine.bind((IUpdateEngineCallback)object);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  public boolean bind(UpdateEngineCallback paramUpdateEngineCallback) {
    return bind(paramUpdateEngineCallback, null);
  }
  
  public void applyPayload(String paramString, long paramLong1, long paramLong2, String[] paramArrayOfString) {
    try {
      this.mUpdateEngine.applyPayload(paramString, paramLong1, paramLong2, paramArrayOfString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void applyPayload(AssetFileDescriptor paramAssetFileDescriptor, String[] paramArrayOfString) {
    try {
      IUpdateEngine iUpdateEngine = this.mUpdateEngine;
      ParcelFileDescriptor parcelFileDescriptor = paramAssetFileDescriptor.getParcelFileDescriptor();
      long l1 = paramAssetFileDescriptor.getStartOffset(), l2 = paramAssetFileDescriptor.getLength();
      iUpdateEngine.applyPayloadFd(parcelFileDescriptor, l1, l2, paramArrayOfString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void cancel() {
    try {
      this.mUpdateEngine.cancel();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void suspend() {
    try {
      this.mUpdateEngine.suspend();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void resume() {
    try {
      this.mUpdateEngine.resume();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void resetStatus() {
    try {
      this.mUpdateEngine.resetStatus();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean unbind() {
    synchronized (this.mUpdateEngineCallbackLock) {
      if (this.mUpdateEngineCallback == null)
        return true; 
      try {
        boolean bool = this.mUpdateEngine.unbind(this.mUpdateEngineCallback);
        this.mUpdateEngineCallback = null;
        return bool;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  public boolean verifyPayloadMetadata(String paramString) {
    try {
      return this.mUpdateEngine.verifyPayloadApplicable(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static final class AllocateSpaceResult {
    private AllocateSpaceResult() {}
    
    private int mErrorCode = 0;
    
    private long mFreeSpaceRequired = 0L;
    
    public int getErrorCode() {
      return this.mErrorCode;
    }
    
    public long getFreeSpaceRequired() {
      int i = this.mErrorCode;
      if (i == 0)
        return 0L; 
      if (i == 60)
        return this.mFreeSpaceRequired; 
      i = this.mErrorCode;
      throw new IllegalStateException(String.format("getFreeSpaceRequired() is not available when error code is %d", new Object[] { Integer.valueOf(i) }));
    }
  }
  
  public AllocateSpaceResult allocateSpace(String paramString, String[] paramArrayOfString) {
    AllocateSpaceResult allocateSpaceResult = new AllocateSpaceResult();
    try {
      byte b;
      AllocateSpaceResult.access$102(allocateSpaceResult, this.mUpdateEngine.allocateSpaceForPayload(paramString, paramArrayOfString));
      if (allocateSpaceResult.mFreeSpaceRequired == 0L) {
        b = 0;
      } else {
        b = 60;
      } 
      AllocateSpaceResult.access$202(allocateSpaceResult, b);
      return allocateSpaceResult;
    } catch (ServiceSpecificException serviceSpecificException) {
      AllocateSpaceResult.access$202(allocateSpaceResult, serviceSpecificException.errorCode);
      AllocateSpaceResult.access$102(allocateSpaceResult, 0L);
      return allocateSpaceResult;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  class CleanupAppliedPayloadCallback extends IUpdateEngineCallback.Stub {
    private boolean mCompleted;
    
    private int mErrorCode;
    
    private Object mLock;
    
    private CleanupAppliedPayloadCallback() {
      this.mErrorCode = 1;
      this.mCompleted = false;
      this.mLock = new Object();
    }
    
    private int getResult() {
      synchronized (this.mLock) {
        while (true) {
          boolean bool = this.mCompleted;
          if (!bool) {
            try {
              this.mLock.wait();
            } catch (InterruptedException interruptedException) {}
            continue;
          } 
          break;
        } 
        return this.mErrorCode;
      } 
    }
    
    public void onStatusUpdate(int param1Int, float param1Float) {}
    
    public void onPayloadApplicationComplete(int param1Int) {
      synchronized (this.mLock) {
        this.mErrorCode = param1Int;
        this.mCompleted = true;
        this.mLock.notifyAll();
        return;
      } 
    }
  }
  
  public int cleanupAppliedPayload() {
    CleanupAppliedPayloadCallback cleanupAppliedPayloadCallback = new CleanupAppliedPayloadCallback();
    try {
      this.mUpdateEngine.cleanupSuccessfulUpdate(cleanupAppliedPayloadCallback);
      return cleanupAppliedPayloadCallback.getResult();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static @interface ErrorCode {}
}
