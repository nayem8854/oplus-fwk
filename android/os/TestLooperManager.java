package android.os;

import android.util.ArraySet;
import java.util.concurrent.LinkedBlockingQueue;

public class TestLooperManager {
  private static final ArraySet<Looper> sHeldLoopers = new ArraySet();
  
  private final LinkedBlockingQueue<MessageExecution> mExecuteQueue = new LinkedBlockingQueue<>();
  
  private final Looper mLooper;
  
  private boolean mLooperBlocked;
  
  private final MessageQueue mQueue;
  
  private boolean mReleased;
  
  public TestLooperManager(Looper paramLooper) {
    synchronized (sHeldLoopers) {
      if (!sHeldLoopers.contains(paramLooper)) {
        sHeldLoopers.add(paramLooper);
        this.mLooper = paramLooper;
        this.mQueue = paramLooper.getQueue();
        (new Handler(paramLooper)).post(new LooperHolder());
        return;
      } 
      RuntimeException runtimeException = new RuntimeException();
      this("TestLooperManager already held for this looper");
      throw runtimeException;
    } 
  }
  
  public MessageQueue getMessageQueue() {
    checkReleased();
    return this.mQueue;
  }
  
  @Deprecated
  public MessageQueue getQueue() {
    return getMessageQueue();
  }
  
  public Message next() {
    /* monitor enter ThisExpression{ObjectType{android/os/TestLooperManager}} */
    while (!this.mLooperBlocked) {
      try {
        wait();
      } catch (InterruptedException interruptedException) {
      
      } finally {
        Exception exception;
      } 
      /* monitor exit ThisExpression{ObjectType{android/os/TestLooperManager}} */
    } 
    checkReleased();
    return this.mQueue.next();
  }
  
  public void release() {
    synchronized (sHeldLoopers) {
      sHeldLoopers.remove(this.mLooper);
      checkReleased();
      this.mReleased = true;
      this.mExecuteQueue.add(new MessageExecution());
      return;
    } 
  }
  
  public void execute(Message paramMessage) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial checkReleased : ()V
    //   4: invokestatic myLooper : ()Landroid/os/Looper;
    //   7: aload_0
    //   8: getfield mLooper : Landroid/os/Looper;
    //   11: if_acmpne -> 25
    //   14: aload_1
    //   15: getfield target : Landroid/os/Handler;
    //   18: aload_1
    //   19: invokevirtual dispatchMessage : (Landroid/os/Message;)V
    //   22: goto -> 68
    //   25: new android/os/TestLooperManager$MessageExecution
    //   28: dup
    //   29: aconst_null
    //   30: invokespecial <init> : (Landroid/os/TestLooperManager$1;)V
    //   33: astore_2
    //   34: aload_2
    //   35: aload_1
    //   36: invokestatic access$202 : (Landroid/os/TestLooperManager$MessageExecution;Landroid/os/Message;)Landroid/os/Message;
    //   39: pop
    //   40: aload_2
    //   41: monitorenter
    //   42: aload_0
    //   43: getfield mExecuteQueue : Ljava/util/concurrent/LinkedBlockingQueue;
    //   46: aload_2
    //   47: invokevirtual add : (Ljava/lang/Object;)Z
    //   50: pop
    //   51: aload_2
    //   52: invokevirtual wait : ()V
    //   55: goto -> 59
    //   58: astore_1
    //   59: aload_2
    //   60: invokestatic access$300 : (Landroid/os/TestLooperManager$MessageExecution;)Ljava/lang/Throwable;
    //   63: ifnonnull -> 69
    //   66: aload_2
    //   67: monitorexit
    //   68: return
    //   69: new java/lang/RuntimeException
    //   72: astore_1
    //   73: aload_1
    //   74: aload_2
    //   75: invokestatic access$300 : (Landroid/os/TestLooperManager$MessageExecution;)Ljava/lang/Throwable;
    //   78: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   81: aload_1
    //   82: athrow
    //   83: astore_1
    //   84: aload_2
    //   85: monitorexit
    //   86: aload_1
    //   87: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #117	-> 0
    //   #118	-> 4
    //   #120	-> 14
    //   #122	-> 25
    //   #123	-> 34
    //   #124	-> 40
    //   #125	-> 42
    //   #128	-> 51
    //   #130	-> 55
    //   #129	-> 58
    //   #131	-> 59
    //   #134	-> 66
    //   #136	-> 68
    //   #132	-> 69
    //   #134	-> 83
    // Exception table:
    //   from	to	target	type
    //   42	51	83	finally
    //   51	55	58	java/lang/InterruptedException
    //   51	55	83	finally
    //   59	66	83	finally
    //   66	68	83	finally
    //   69	83	83	finally
    //   84	86	83	finally
  }
  
  public void recycle(Message paramMessage) {
    checkReleased();
    paramMessage.recycleUnchecked();
  }
  
  public boolean hasMessages(Handler paramHandler, Object paramObject, int paramInt) {
    checkReleased();
    return this.mQueue.hasMessages(paramHandler, paramInt, paramObject);
  }
  
  public boolean hasMessages(Handler paramHandler, Object paramObject, Runnable paramRunnable) {
    checkReleased();
    return this.mQueue.hasMessages(paramHandler, paramRunnable, paramObject);
  }
  
  private void checkReleased() {
    if (!this.mReleased)
      return; 
    throw new RuntimeException("release() has already be called");
  }
  
  private class LooperHolder implements Runnable {
    final TestLooperManager this$0;
    
    private LooperHolder() {}
    
    public void run() {
      synchronized (TestLooperManager.this) {
        TestLooperManager.access$402(TestLooperManager.this, true);
        TestLooperManager.this.notify();
        while (!TestLooperManager.this.mReleased) {
          try {
            TestLooperManager.MessageExecution messageExecution = TestLooperManager.this.mExecuteQueue.take();
            if (messageExecution.m != null)
              processMessage(messageExecution); 
          } catch (InterruptedException interruptedException) {}
        } 
        synchronized (TestLooperManager.this) {
          TestLooperManager.access$402(TestLooperManager.this, false);
          return;
        } 
      } 
    }
    
    private void processMessage(TestLooperManager.MessageExecution param1MessageExecution) {
      // Byte code:
      //   0: aload_1
      //   1: monitorenter
      //   2: aload_1
      //   3: invokestatic access$200 : (Landroid/os/TestLooperManager$MessageExecution;)Landroid/os/Message;
      //   6: getfield target : Landroid/os/Handler;
      //   9: aload_1
      //   10: invokestatic access$200 : (Landroid/os/TestLooperManager$MessageExecution;)Landroid/os/Message;
      //   13: invokevirtual dispatchMessage : (Landroid/os/Message;)V
      //   16: aload_1
      //   17: aconst_null
      //   18: invokestatic access$302 : (Landroid/os/TestLooperManager$MessageExecution;Ljava/lang/Throwable;)Ljava/lang/Throwable;
      //   21: pop
      //   22: goto -> 32
      //   25: astore_2
      //   26: aload_1
      //   27: aload_2
      //   28: invokestatic access$302 : (Landroid/os/TestLooperManager$MessageExecution;Ljava/lang/Throwable;)Ljava/lang/Throwable;
      //   31: pop
      //   32: aload_1
      //   33: invokevirtual notifyAll : ()V
      //   36: aload_1
      //   37: monitorexit
      //   38: return
      //   39: astore_2
      //   40: aload_1
      //   41: monitorexit
      //   42: aload_2
      //   43: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #199	-> 0
      //   #201	-> 2
      //   #202	-> 16
      //   #205	-> 22
      //   #203	-> 25
      //   #204	-> 26
      //   #206	-> 32
      //   #207	-> 36
      //   #208	-> 38
      //   #207	-> 39
      // Exception table:
      //   from	to	target	type
      //   2	16	25	finally
      //   16	22	25	finally
      //   26	32	39	finally
      //   32	36	39	finally
      //   36	38	39	finally
      //   40	42	39	finally
    }
  }
  
  private static class MessageExecution {
    private Message m;
    
    private Throwable response;
    
    private MessageExecution() {}
  }
}
