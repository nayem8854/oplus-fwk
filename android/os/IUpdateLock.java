package android.os;

public interface IUpdateLock extends IInterface {
  void acquireUpdateLock(IBinder paramIBinder, String paramString) throws RemoteException;
  
  void releaseUpdateLock(IBinder paramIBinder) throws RemoteException;
  
  class Default implements IUpdateLock {
    public void acquireUpdateLock(IBinder param1IBinder, String param1String) throws RemoteException {}
    
    public void releaseUpdateLock(IBinder param1IBinder) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IUpdateLock {
    private static final String DESCRIPTOR = "android.os.IUpdateLock";
    
    static final int TRANSACTION_acquireUpdateLock = 1;
    
    static final int TRANSACTION_releaseUpdateLock = 2;
    
    public Stub() {
      attachInterface(this, "android.os.IUpdateLock");
    }
    
    public static IUpdateLock asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IUpdateLock");
      if (iInterface != null && iInterface instanceof IUpdateLock)
        return (IUpdateLock)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "releaseUpdateLock";
      } 
      return "acquireUpdateLock";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder1;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.os.IUpdateLock");
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.IUpdateLock");
        iBinder1 = param1Parcel1.readStrongBinder();
        releaseUpdateLock(iBinder1);
        param1Parcel2.writeNoException();
        return true;
      } 
      iBinder1.enforceInterface("android.os.IUpdateLock");
      IBinder iBinder2 = iBinder1.readStrongBinder();
      String str = iBinder1.readString();
      acquireUpdateLock(iBinder2, str);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IUpdateLock {
      public static IUpdateLock sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IUpdateLock";
      }
      
      public void acquireUpdateLock(IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUpdateLock");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IUpdateLock.Stub.getDefaultImpl() != null) {
            IUpdateLock.Stub.getDefaultImpl().acquireUpdateLock(param2IBinder, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseUpdateLock(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUpdateLock");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IUpdateLock.Stub.getDefaultImpl() != null) {
            IUpdateLock.Stub.getDefaultImpl().releaseUpdateLock(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IUpdateLock param1IUpdateLock) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IUpdateLock != null) {
          Proxy.sDefaultImpl = param1IUpdateLock;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IUpdateLock getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
