package android.os;

import android.annotation.SystemApi;
import android.content.Context;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collections;
import java.util.List;

@SystemApi
public class PowerWhitelistManager {
  public static final int EVENT_MMS = 2;
  
  public static final int EVENT_SMS = 1;
  
  public static final int EVENT_UNSPECIFIED = 0;
  
  private final Context mContext;
  
  private final IDeviceIdleController mService;
  
  public PowerWhitelistManager(Context paramContext) {
    this.mContext = paramContext;
    this.mService = ((DeviceIdleManager)paramContext.getSystemService(DeviceIdleManager.class)).getService();
  }
  
  public void addToWhitelist(String paramString) {
    addToWhitelist(Collections.singletonList(paramString));
  }
  
  public void addToWhitelist(List<String> paramList) {
    try {
      this.mService.addPowerSaveWhitelistApps(paramList);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int[] getWhitelistedAppIds(boolean paramBoolean) {
    if (paramBoolean)
      try {
        return this.mService.getAppIdWhitelist();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    return this.mService.getAppIdWhitelistExceptIdle();
  }
  
  public boolean isWhitelisted(String paramString, boolean paramBoolean) {
    if (paramBoolean)
      try {
        return this.mService.isPowerSaveWhitelistApp(paramString);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    return this.mService.isPowerSaveWhitelistExceptIdleApp((String)remoteException);
  }
  
  public void whitelistAppTemporarily(String paramString, long paramLong) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("from:");
    stringBuilder.append(UserHandle.formatUid(Binder.getCallingUid()));
    String str = stringBuilder.toString();
    try {
      this.mService.addPowerSaveTempWhitelistApp(paramString, paramLong, this.mContext.getUserId(), str);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public long whitelistAppTemporarilyForEvent(String paramString1, int paramInt, String paramString2) {
    if (paramInt != 1) {
      if (paramInt != 2)
        try {
          IDeviceIdleController iDeviceIdleController2 = this.mService;
          Context context2 = this.mContext;
          paramInt = context2.getUserId();
          return iDeviceIdleController2.whitelistAppTemporarily(paramString1, paramInt, paramString2);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      IDeviceIdleController iDeviceIdleController1 = this.mService;
      Context context1 = this.mContext;
      paramInt = context1.getUserId();
      return iDeviceIdleController1.addPowerSaveTempWhitelistAppForMms((String)remoteException, paramInt, paramString2);
    } 
    IDeviceIdleController iDeviceIdleController = this.mService;
    Context context = this.mContext;
    paramInt = context.getUserId();
    return iDeviceIdleController.addPowerSaveTempWhitelistAppForSms((String)remoteException, paramInt, paramString2);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface WhitelistEvent {}
}
