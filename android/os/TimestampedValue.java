package android.os;

import java.util.Objects;

public final class TimestampedValue<T> implements Parcelable {
  public TimestampedValue(long paramLong, T paramT) {
    this.mReferenceTimeMillis = paramLong;
    this.mValue = paramT;
  }
  
  public long getReferenceTimeMillis() {
    return this.mReferenceTimeMillis;
  }
  
  public T getValue() {
    return this.mValue;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    TimestampedValue timestampedValue = (TimestampedValue)paramObject;
    if (this.mReferenceTimeMillis == timestampedValue.mReferenceTimeMillis) {
      paramObject = this.mValue;
      T t = timestampedValue.mValue;
      if (Objects.equals(paramObject, t))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Long.valueOf(this.mReferenceTimeMillis), this.mValue });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("TimestampedValue{mReferenceTimeMillis=");
    stringBuilder.append(this.mReferenceTimeMillis);
    stringBuilder.append(", mValue=");
    stringBuilder.append(this.mValue);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public static long referenceTimeDifference(TimestampedValue<?> paramTimestampedValue1, TimestampedValue<?> paramTimestampedValue2) {
    return paramTimestampedValue1.mReferenceTimeMillis - paramTimestampedValue2.mReferenceTimeMillis;
  }
  
  public static final Parcelable.Creator<TimestampedValue<?>> CREATOR = (Parcelable.Creator<TimestampedValue<?>>)new Object();
  
  private final long mReferenceTimeMillis;
  
  private final T mValue;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mReferenceTimeMillis);
    paramParcel.writeValue(this.mValue);
  }
}
