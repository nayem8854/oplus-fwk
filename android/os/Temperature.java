package android.os;

import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class Temperature implements Parcelable {
  public static boolean isValidType(int paramInt) {
    boolean bool;
    if (paramInt >= -1 && paramInt <= 9) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isValidStatus(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt <= 6) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Temperature(float paramFloat, int paramInt1, String paramString, int paramInt2) {
    Preconditions.checkArgument(isValidType(paramInt1), "Invalid Type");
    Preconditions.checkArgument(isValidStatus(paramInt2), "Invalid Status");
    this.mValue = paramFloat;
    this.mType = paramInt1;
    this.mName = (String)Preconditions.checkStringNotEmpty(paramString);
    this.mStatus = paramInt2;
  }
  
  public float getValue() {
    return this.mValue;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public int getStatus() {
    return this.mStatus;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Temperature{mValue=");
    stringBuilder.append(this.mValue);
    stringBuilder.append(", mType=");
    stringBuilder.append(this.mType);
    stringBuilder.append(", mName=");
    stringBuilder.append(this.mName);
    stringBuilder.append(", mStatus=");
    stringBuilder.append(this.mStatus);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    int i = this.mName.hashCode();
    int j = Float.hashCode(this.mValue);
    int k = this.mType;
    int m = this.mStatus;
    return ((i * 31 + j) * 31 + k) * 31 + m;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof Temperature;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (((Temperature)paramObject).mValue == this.mValue && ((Temperature)paramObject).mType == this.mType) {
      String str1 = ((Temperature)paramObject).mName, str2 = this.mName;
      if (str1.equals(str2) && ((Temperature)paramObject).mStatus == this.mStatus)
        bool1 = true; 
    } 
    return bool1;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeFloat(this.mValue);
    paramParcel.writeInt(this.mType);
    paramParcel.writeString(this.mName);
    paramParcel.writeInt(this.mStatus);
  }
  
  public static final Parcelable.Creator<Temperature> CREATOR = new Parcelable.Creator<Temperature>() {
      public Temperature createFromParcel(Parcel param1Parcel) {
        float f = param1Parcel.readFloat();
        int i = param1Parcel.readInt();
        String str = param1Parcel.readString();
        int j = param1Parcel.readInt();
        return new Temperature(f, i, str, j);
      }
      
      public Temperature[] newArray(int param1Int) {
        return new Temperature[param1Int];
      }
    };
  
  public static final int THROTTLING_CRITICAL = 4;
  
  public static final int THROTTLING_EMERGENCY = 5;
  
  public static final int THROTTLING_LIGHT = 1;
  
  public static final int THROTTLING_MODERATE = 2;
  
  public static final int THROTTLING_NONE = 0;
  
  public static final int THROTTLING_SEVERE = 3;
  
  public static final int THROTTLING_SHUTDOWN = 6;
  
  public static final int TYPE_BATTERY = 2;
  
  public static final int TYPE_BCL_CURRENT = 7;
  
  public static final int TYPE_BCL_PERCENTAGE = 8;
  
  public static final int TYPE_BCL_VOLTAGE = 6;
  
  public static final int TYPE_CPU = 0;
  
  public static final int TYPE_GPU = 1;
  
  public static final int TYPE_NPU = 9;
  
  public static final int TYPE_POWER_AMPLIFIER = 5;
  
  public static final int TYPE_SKIN = 3;
  
  public static final int TYPE_UNKNOWN = -1;
  
  public static final int TYPE_USB_PORT = 4;
  
  private final String mName;
  
  private final int mStatus;
  
  private final int mType;
  
  private final float mValue;
  
  public int describeContents() {
    return 0;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ThrottlingStatus implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Type implements Annotation {}
}
