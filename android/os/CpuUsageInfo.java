package android.os;

public final class CpuUsageInfo implements Parcelable {
  public static final Parcelable.Creator<CpuUsageInfo> CREATOR = new Parcelable.Creator<CpuUsageInfo>() {
      public CpuUsageInfo createFromParcel(Parcel param1Parcel) {
        return new CpuUsageInfo(param1Parcel);
      }
      
      public CpuUsageInfo[] newArray(int param1Int) {
        return new CpuUsageInfo[param1Int];
      }
    };
  
  private long mActive;
  
  private long mTotal;
  
  public CpuUsageInfo(long paramLong1, long paramLong2) {
    this.mActive = paramLong1;
    this.mTotal = paramLong2;
  }
  
  private CpuUsageInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public long getActive() {
    return this.mActive;
  }
  
  public long getTotal() {
    return this.mTotal;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mActive);
    paramParcel.writeLong(this.mTotal);
  }
  
  private void readFromParcel(Parcel paramParcel) {
    this.mActive = paramParcel.readLong();
    this.mTotal = paramParcel.readLong();
  }
}
