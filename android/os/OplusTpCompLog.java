package android.os;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class OplusTpCompLog {
  private static final String TAG = "OplusTpCompLog";
  
  private static final boolean TP_COMP_LOG_JAVA_SWITCH_ON = true;
  
  public static final int TP_COMP_LOG_LEVEL_ANR = 8;
  
  public static final int TP_COMP_LOG_LEVEL_CRASH = 4;
  
  public static final int TP_COMP_LOG_LEVEL_ERROR = 2;
  
  public static final int TP_COMP_LOG_LEVEL_INFO = 1;
  
  private static final String TP_COMP_LOG_SWITCH_PROP = "oppo.thirdparty.cmpt.logs.switcher";
  
  public static void i(String paramString1, String paramString2) {
    if (isEnableFor(1))
      native_oplusTpCompLog_info(paramString1, paramString2); 
  }
  
  public static void e(String paramString1, String paramString2) {
    if (isEnableFor(2)) {
      Exception exception = new Exception();
      String str1 = exception.getStackTrace()[1].getMethodName();
      String str2 = exception.getStackTrace()[1].getFileName();
      int i = exception.getStackTrace()[1].getLineNumber();
      native_oplusTpCompLog_error(paramString1, paramString2, str2, str1, i);
    } 
  }
  
  public static void c(String paramString1, String paramString2) {
    if (isEnableFor(4))
      native_oplusTpCompLog_crash(paramString1, paramString2); 
  }
  
  public static void a(String paramString1, String paramString2) {
    if (isEnableFor(8))
      native_oplusTpCompLog_anr(paramString1, paramString2); 
  }
  
  public static boolean isEnableFor(int paramInt) {
    boolean bool = false;
    int i = SystemProperties.getInt("oppo.thirdparty.cmpt.logs.switcher", 0);
    if ((i & paramInt) != 0)
      bool = true; 
    return bool;
  }
  
  public static void initialize() {
    native_oplusTpCompLog_initialize();
  }
  
  private static native void native_oplusTpCompLog_anr(String paramString1, String paramString2);
  
  private static native void native_oplusTpCompLog_crash(String paramString1, String paramString2);
  
  private static native void native_oplusTpCompLog_error(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt);
  
  private static native void native_oplusTpCompLog_info(String paramString1, String paramString2);
  
  private static native void native_oplusTpCompLog_initialize();
  
  private static native boolean native_oplusTpCompLog_isEnableFor(int paramInt);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface TpCompLogLevel {}
}
