package android.os;

import android.annotation.SystemApi;
import java.io.PrintWriter;

public final class UserHandle implements Parcelable {
  public static final int AID_APP_END = 19999;
  
  public static final int AID_APP_START = 10000;
  
  public static final int AID_CACHE_GID_START = 20000;
  
  public static final int AID_ROOT = 0;
  
  public static final int AID_SHARED_GID_START = 50000;
  
  @SystemApi
  public static final UserHandle ALL = new UserHandle(-1);
  
  private static final UserHandle[] CACHED_USER_INFOS;
  
  public static final Parcelable.Creator<UserHandle> CREATOR;
  
  @SystemApi
  public static final UserHandle CURRENT = new UserHandle(-2);
  
  public static final UserHandle CURRENT_OR_SELF = new UserHandle(-3);
  
  public static final int ERR_GID = -1;
  
  public static final int MIN_SECONDARY_USER_ID = 10;
  
  public static final boolean MU_ENABLED = true;
  
  private static final UserHandle NULL = new UserHandle(-10000);
  
  private static final int NUM_CACHED_USERS = 4;
  
  @Deprecated
  public static final UserHandle OWNER = new UserHandle(0);
  
  public static final int PER_USER_RANGE = 100000;
  
  @SystemApi
  public static final UserHandle SYSTEM = new UserHandle(0);
  
  public static final int USER_ALL = -1;
  
  public static final int USER_CURRENT = -2;
  
  public static final int USER_CURRENT_OR_SELF = -3;
  
  public static final int USER_NULL = -10000;
  
  @Deprecated
  public static final int USER_OWNER = 0;
  
  public static final int USER_SERIAL_SYSTEM = 0;
  
  public static final int USER_SYSTEM = 0;
  
  final int mHandle;
  
  static {
    CACHED_USER_INFOS = new UserHandle[4];
    byte b = 0;
    while (true) {
      UserHandle[] arrayOfUserHandle = CACHED_USER_INFOS;
      if (b < arrayOfUserHandle.length) {
        arrayOfUserHandle[b] = new UserHandle(b + 10);
        b++;
        continue;
      } 
      break;
    } 
    CREATOR = new Parcelable.Creator<UserHandle>() {
        public UserHandle createFromParcel(Parcel param1Parcel) {
          return UserHandle.of(param1Parcel.readInt());
        }
        
        public UserHandle[] newArray(int param1Int) {
          return new UserHandle[param1Int];
        }
      };
  }
  
  public static boolean isSameUser(int paramInt1, int paramInt2) {
    boolean bool;
    if (getUserId(paramInt1) == getUserId(paramInt2)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isSameApp(int paramInt1, int paramInt2) {
    boolean bool;
    if (getAppId(paramInt1) == getAppId(paramInt2)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isIsolated(int paramInt) {
    if (paramInt > 0)
      return Process.isIsolated(paramInt); 
    return false;
  }
  
  public static boolean isApp(int paramInt) {
    boolean bool = false;
    if (paramInt > 0) {
      paramInt = getAppId(paramInt);
      boolean bool1 = bool;
      if (paramInt >= 10000) {
        bool1 = bool;
        if (paramInt <= 19999)
          bool1 = true; 
      } 
      return bool1;
    } 
    return false;
  }
  
  public static boolean isCore(int paramInt) {
    boolean bool = false;
    if (paramInt >= 0) {
      paramInt = getAppId(paramInt);
      if (paramInt < 10000)
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  public static UserHandle getUserHandleForUid(int paramInt) {
    return of(getUserId(paramInt));
  }
  
  public static int getUserId(int paramInt) {
    return paramInt / 100000;
  }
  
  public static int getCallingUserId() {
    return getUserId(Binder.getCallingUid());
  }
  
  public static int getCallingAppId() {
    return getAppId(Binder.getCallingUid());
  }
  
  @SystemApi
  public static UserHandle of(int paramInt) {
    if (paramInt == 0)
      return SYSTEM; 
    if (paramInt != -3) {
      if (paramInt != -2) {
        if (paramInt != -1) {
          if (paramInt >= 10) {
            UserHandle[] arrayOfUserHandle = CACHED_USER_INFOS;
            if (paramInt < arrayOfUserHandle.length + 10)
              return arrayOfUserHandle[paramInt - 10]; 
          } 
          if (paramInt == -10000)
            return NULL; 
          return new UserHandle(paramInt);
        } 
        return ALL;
      } 
      return CURRENT;
    } 
    return CURRENT_OR_SELF;
  }
  
  public static int getUid(int paramInt1, int paramInt2) {
    return paramInt1 * 100000 + paramInt2 % 100000;
  }
  
  @SystemApi
  public static int getAppId(int paramInt) {
    return paramInt % 100000;
  }
  
  public static int getUserGid(int paramInt) {
    return getUid(paramInt, 9997);
  }
  
  public static int getSharedAppGid(int paramInt) {
    return getSharedAppGid(getUserId(paramInt), getAppId(paramInt));
  }
  
  public static int getSharedAppGid(int paramInt1, int paramInt2) {
    if (paramInt2 >= 10000 && paramInt2 <= 19999)
      return paramInt2 - 10000 + 50000; 
    if (paramInt2 >= 0 && paramInt2 <= 10000)
      return paramInt2; 
    return -1;
  }
  
  public static int getAppIdFromSharedAppGid(int paramInt) {
    paramInt = getAppId(paramInt) + 10000 - 50000;
    if (paramInt < 0 || paramInt >= 50000)
      return -1; 
    return paramInt;
  }
  
  public static int getCacheAppGid(int paramInt) {
    return getCacheAppGid(getUserId(paramInt), getAppId(paramInt));
  }
  
  public static int getCacheAppGid(int paramInt1, int paramInt2) {
    if (paramInt2 >= 10000 && paramInt2 <= 19999)
      return getUid(paramInt1, paramInt2 - 10000 + 20000); 
    return -1;
  }
  
  public static void formatUid(StringBuilder paramStringBuilder, int paramInt) {
    if (paramInt < 10000) {
      paramStringBuilder.append(paramInt);
    } else {
      paramStringBuilder.append('u');
      paramStringBuilder.append(getUserId(paramInt));
      paramInt = getAppId(paramInt);
      if (isIsolated(paramInt)) {
        if (paramInt > 99000) {
          paramStringBuilder.append('i');
          paramStringBuilder.append(paramInt - 99000);
        } else {
          paramStringBuilder.append("ai");
          paramStringBuilder.append(paramInt - 90000);
        } 
      } else if (paramInt >= 10000) {
        paramStringBuilder.append('a');
        paramStringBuilder.append(paramInt - 10000);
      } else {
        paramStringBuilder.append('s');
        paramStringBuilder.append(paramInt);
      } 
    } 
  }
  
  @SystemApi
  public static String formatUid(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    formatUid(stringBuilder, paramInt);
    return stringBuilder.toString();
  }
  
  public static void formatUid(PrintWriter paramPrintWriter, int paramInt) {
    if (paramInt < 10000) {
      paramPrintWriter.print(paramInt);
    } else {
      paramPrintWriter.print('u');
      paramPrintWriter.print(getUserId(paramInt));
      paramInt = getAppId(paramInt);
      if (isIsolated(paramInt)) {
        if (paramInt > 99000) {
          paramPrintWriter.print('i');
          paramPrintWriter.print(paramInt - 99000);
        } else {
          paramPrintWriter.print("ai");
          paramPrintWriter.print(paramInt - 90000);
        } 
      } else if (paramInt >= 10000) {
        paramPrintWriter.print('a');
        paramPrintWriter.print(paramInt - 10000);
      } else {
        paramPrintWriter.print('s');
        paramPrintWriter.print(paramInt);
      } 
    } 
  }
  
  public static int parseUserArg(String paramString) {
    int i;
    if ("all".equals(paramString)) {
      i = -1;
    } else {
      if ("current".equals(paramString) || "cur".equals(paramString)) {
        i = -2;
        return i;
      } 
      try {
        i = Integer.parseInt(paramString);
      } catch (NumberFormatException numberFormatException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Bad user number: ");
        stringBuilder.append(paramString);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    } 
    return i;
  }
  
  @SystemApi
  public static int myUserId() {
    return getUserId(Process.myUid());
  }
  
  @SystemApi
  @Deprecated
  public boolean isOwner() {
    return equals(OWNER);
  }
  
  @SystemApi
  public boolean isSystem() {
    return equals(SYSTEM);
  }
  
  public UserHandle(int paramInt) {
    this.mHandle = paramInt;
  }
  
  @SystemApi
  public int getIdentifier() {
    return this.mHandle;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UserHandle{");
    stringBuilder.append(this.mHandle);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject != null)
      try {
        paramObject = paramObject;
        int i = this.mHandle, j = ((UserHandle)paramObject).mHandle;
        if (i == j)
          bool = true; 
        return bool;
      } catch (ClassCastException classCastException) {} 
    return false;
  }
  
  public int hashCode() {
    return this.mHandle;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mHandle);
  }
  
  public static void writeToParcel(UserHandle paramUserHandle, Parcel paramParcel) {
    if (paramUserHandle != null) {
      paramUserHandle.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(-10000);
    } 
  }
  
  public static UserHandle readFromParcel(Parcel paramParcel) {
    int i = paramParcel.readInt();
    if (i != -10000) {
      UserHandle userHandle = new UserHandle(i);
    } else {
      paramParcel = null;
    } 
    return (UserHandle)paramParcel;
  }
  
  public UserHandle(Parcel paramParcel) {
    this.mHandle = paramParcel.readInt();
  }
}
