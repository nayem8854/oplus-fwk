package android.os;

import android.view.InputChannel;
import android.view.InputEvent;
import android.view.InputEventReceiver;
import android.view.OplusWindowManager;

public final class OplusExManager {
  OplusWindowManager mWindowManager = new OplusWindowManager();
  
  public OplusExManager() {
    this(null);
  }
  
  private OppoExInputEventReceiver mExInputEventReceiver = null;
  
  private InputChannel mExInputChannel = new InputChannel();
  
  public static final String TAG = "OplusExManager";
  
  public static final String SERVICE_NAME = "OPPOExService";
  
  private static final String OPPO_EX_CHANNEL_NAME = "OppoExInputReceiver";
  
  public boolean enableInputReceiver(Binder paramBinder, IExInputEventReceiverCallback paramIExInputEventReceiverCallback) {
    return false;
  }
  
  public void disableInputReceiver() {}
  
  public OplusExManager(IOplusExService paramIOplusExService) {}
  
  public static interface IExInputEventReceiverCallback {
    boolean onInputEvent(InputEvent param1InputEvent);
  }
  
  class OppoExInputEventReceiver extends InputEventReceiver {
    private OplusExManager.IExInputEventReceiverCallback mCallback;
    
    final OplusExManager this$0;
    
    public void setCallback(OplusExManager.IExInputEventReceiverCallback param1IExInputEventReceiverCallback) {
      this.mCallback = param1IExInputEventReceiverCallback;
    }
    
    public OppoExInputEventReceiver(InputChannel param1InputChannel, Looper param1Looper) {
      super(param1InputChannel, param1Looper);
    }
    
    public void onInputEvent(InputEvent param1InputEvent) {
      OplusExManager.IExInputEventReceiverCallback iExInputEventReceiverCallback = this.mCallback;
      if (iExInputEventReceiverCallback != null) {
        boolean bool = iExInputEventReceiverCallback.onInputEvent(param1InputEvent);
        finishInputEvent(param1InputEvent, bool);
      } 
    }
  }
}
