package android.os;

public interface IVibratorService extends IInterface {
  int[] areEffectsSupported(int[] paramArrayOfint) throws RemoteException;
  
  boolean[] arePrimitivesSupported(int[] paramArrayOfint) throws RemoteException;
  
  void cancelVibrate(IBinder paramIBinder) throws RemoteException;
  
  boolean hasAmplitudeControl() throws RemoteException;
  
  boolean hasVibrator() throws RemoteException;
  
  boolean isVibrating() throws RemoteException;
  
  boolean registerVibratorStateListener(IVibratorStateListener paramIVibratorStateListener) throws RemoteException;
  
  boolean setAlwaysOnEffect(int paramInt1, String paramString, int paramInt2, VibrationEffect paramVibrationEffect, VibrationAttributes paramVibrationAttributes) throws RemoteException;
  
  boolean unregisterVibratorStateListener(IVibratorStateListener paramIVibratorStateListener) throws RemoteException;
  
  void vibrate(int paramInt, String paramString1, VibrationEffect paramVibrationEffect, VibrationAttributes paramVibrationAttributes, String paramString2, IBinder paramIBinder) throws RemoteException;
  
  class Default implements IVibratorService {
    public boolean hasVibrator() throws RemoteException {
      return false;
    }
    
    public boolean isVibrating() throws RemoteException {
      return false;
    }
    
    public boolean registerVibratorStateListener(IVibratorStateListener param1IVibratorStateListener) throws RemoteException {
      return false;
    }
    
    public boolean unregisterVibratorStateListener(IVibratorStateListener param1IVibratorStateListener) throws RemoteException {
      return false;
    }
    
    public boolean hasAmplitudeControl() throws RemoteException {
      return false;
    }
    
    public int[] areEffectsSupported(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public boolean[] arePrimitivesSupported(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public boolean setAlwaysOnEffect(int param1Int1, String param1String, int param1Int2, VibrationEffect param1VibrationEffect, VibrationAttributes param1VibrationAttributes) throws RemoteException {
      return false;
    }
    
    public void vibrate(int param1Int, String param1String1, VibrationEffect param1VibrationEffect, VibrationAttributes param1VibrationAttributes, String param1String2, IBinder param1IBinder) throws RemoteException {}
    
    public void cancelVibrate(IBinder param1IBinder) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVibratorService {
    private static final String DESCRIPTOR = "android.os.IVibratorService";
    
    static final int TRANSACTION_areEffectsSupported = 6;
    
    static final int TRANSACTION_arePrimitivesSupported = 7;
    
    static final int TRANSACTION_cancelVibrate = 10;
    
    static final int TRANSACTION_hasAmplitudeControl = 5;
    
    static final int TRANSACTION_hasVibrator = 1;
    
    static final int TRANSACTION_isVibrating = 2;
    
    static final int TRANSACTION_registerVibratorStateListener = 3;
    
    static final int TRANSACTION_setAlwaysOnEffect = 8;
    
    static final int TRANSACTION_unregisterVibratorStateListener = 4;
    
    static final int TRANSACTION_vibrate = 9;
    
    public Stub() {
      attachInterface(this, "android.os.IVibratorService");
    }
    
    public static IVibratorService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IVibratorService");
      if (iInterface != null && iInterface instanceof IVibratorService)
        return (IVibratorService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "cancelVibrate";
        case 9:
          return "vibrate";
        case 8:
          return "setAlwaysOnEffect";
        case 7:
          return "arePrimitivesSupported";
        case 6:
          return "areEffectsSupported";
        case 5:
          return "hasAmplitudeControl";
        case 4:
          return "unregisterVibratorStateListener";
        case 3:
          return "registerVibratorStateListener";
        case 2:
          return "isVibrating";
        case 1:
          break;
      } 
      return "hasVibrator";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        IBinder iBinder;
        int[] arrayOfInt2;
        boolean[] arrayOfBoolean;
        int[] arrayOfInt1;
        IVibratorStateListener iVibratorStateListener;
        String str1;
        VibrationEffect vibrationEffect;
        VibrationAttributes vibrationAttributes;
        String str2, str3;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.os.IVibratorService");
            iBinder = param1Parcel1.readStrongBinder();
            cancelVibrate(iBinder);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            iBinder.enforceInterface("android.os.IVibratorService");
            param1Int1 = iBinder.readInt();
            str1 = iBinder.readString();
            if (iBinder.readInt() != 0) {
              vibrationEffect = VibrationEffect.CREATOR.createFromParcel((Parcel)iBinder);
            } else {
              vibrationEffect = null;
            } 
            if (iBinder.readInt() != 0) {
              vibrationAttributes = VibrationAttributes.CREATOR.createFromParcel((Parcel)iBinder);
            } else {
              vibrationAttributes = null;
            } 
            str3 = iBinder.readString();
            iBinder = iBinder.readStrongBinder();
            vibrate(param1Int1, str1, vibrationEffect, vibrationAttributes, str3, iBinder);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iBinder.enforceInterface("android.os.IVibratorService");
            param1Int1 = iBinder.readInt();
            str2 = iBinder.readString();
            param1Int2 = iBinder.readInt();
            if (iBinder.readInt() != 0) {
              vibrationEffect = VibrationEffect.CREATOR.createFromParcel((Parcel)iBinder);
            } else {
              vibrationEffect = null;
            } 
            if (iBinder.readInt() != 0) {
              VibrationAttributes vibrationAttributes1 = VibrationAttributes.CREATOR.createFromParcel((Parcel)iBinder);
            } else {
              iBinder = null;
            } 
            bool = setAlwaysOnEffect(param1Int1, str2, param1Int2, vibrationEffect, (VibrationAttributes)iBinder);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 7:
            iBinder.enforceInterface("android.os.IVibratorService");
            arrayOfInt2 = iBinder.createIntArray();
            arrayOfBoolean = arePrimitivesSupported(arrayOfInt2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeBooleanArray(arrayOfBoolean);
            return true;
          case 6:
            arrayOfBoolean.enforceInterface("android.os.IVibratorService");
            arrayOfInt1 = arrayOfBoolean.createIntArray();
            arrayOfInt1 = areEffectsSupported(arrayOfInt1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt1);
            return true;
          case 5:
            arrayOfInt1.enforceInterface("android.os.IVibratorService");
            bool = hasAmplitudeControl();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            arrayOfInt1.enforceInterface("android.os.IVibratorService");
            iVibratorStateListener = IVibratorStateListener.Stub.asInterface(arrayOfInt1.readStrongBinder());
            bool = unregisterVibratorStateListener(iVibratorStateListener);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 3:
            iVibratorStateListener.enforceInterface("android.os.IVibratorService");
            iVibratorStateListener = IVibratorStateListener.Stub.asInterface(iVibratorStateListener.readStrongBinder());
            bool = registerVibratorStateListener(iVibratorStateListener);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 2:
            iVibratorStateListener.enforceInterface("android.os.IVibratorService");
            bool = isVibrating();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 1:
            break;
        } 
        iVibratorStateListener.enforceInterface("android.os.IVibratorService");
        boolean bool = hasVibrator();
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool);
        return true;
      } 
      param1Parcel2.writeString("android.os.IVibratorService");
      return true;
    }
    
    private static class Proxy implements IVibratorService {
      public static IVibratorService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IVibratorService";
      }
      
      public boolean hasVibrator() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVibratorService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IVibratorService.Stub.getDefaultImpl() != null) {
            bool1 = IVibratorService.Stub.getDefaultImpl().hasVibrator();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVibrating() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVibratorService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IVibratorService.Stub.getDefaultImpl() != null) {
            bool1 = IVibratorService.Stub.getDefaultImpl().isVibrating();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerVibratorStateListener(IVibratorStateListener param2IVibratorStateListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVibratorService");
          if (param2IVibratorStateListener != null) {
            iBinder = param2IVibratorStateListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IVibratorService.Stub.getDefaultImpl() != null) {
            bool1 = IVibratorService.Stub.getDefaultImpl().registerVibratorStateListener(param2IVibratorStateListener);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unregisterVibratorStateListener(IVibratorStateListener param2IVibratorStateListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVibratorService");
          if (param2IVibratorStateListener != null) {
            iBinder = param2IVibratorStateListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IVibratorService.Stub.getDefaultImpl() != null) {
            bool1 = IVibratorService.Stub.getDefaultImpl().unregisterVibratorStateListener(param2IVibratorStateListener);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasAmplitudeControl() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVibratorService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IVibratorService.Stub.getDefaultImpl() != null) {
            bool1 = IVibratorService.Stub.getDefaultImpl().hasAmplitudeControl();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] areEffectsSupported(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVibratorService");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IVibratorService.Stub.getDefaultImpl() != null) {
            param2ArrayOfint = IVibratorService.Stub.getDefaultImpl().areEffectsSupported(param2ArrayOfint);
            return param2ArrayOfint;
          } 
          parcel2.readException();
          param2ArrayOfint = parcel2.createIntArray();
          return param2ArrayOfint;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean[] arePrimitivesSupported(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVibratorService");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IVibratorService.Stub.getDefaultImpl() != null)
            return IVibratorService.Stub.getDefaultImpl().arePrimitivesSupported(param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createBooleanArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAlwaysOnEffect(int param2Int1, String param2String, int param2Int2, VibrationEffect param2VibrationEffect, VibrationAttributes param2VibrationAttributes) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVibratorService");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String);
              try {
                parcel1.writeInt(param2Int2);
                boolean bool = true;
                if (param2VibrationEffect != null) {
                  parcel1.writeInt(1);
                  param2VibrationEffect.writeToParcel(parcel1, 0);
                } else {
                  parcel1.writeInt(0);
                } 
                if (param2VibrationAttributes != null) {
                  parcel1.writeInt(1);
                  param2VibrationAttributes.writeToParcel(parcel1, 0);
                } else {
                  parcel1.writeInt(0);
                } 
                try {
                  boolean bool1 = this.mRemote.transact(8, parcel1, parcel2, 0);
                  if (!bool1 && IVibratorService.Stub.getDefaultImpl() != null) {
                    bool = IVibratorService.Stub.getDefaultImpl().setAlwaysOnEffect(param2Int1, param2String, param2Int2, param2VibrationEffect, param2VibrationAttributes);
                    parcel2.recycle();
                    parcel1.recycle();
                    return bool;
                  } 
                  parcel2.readException();
                  param2Int1 = parcel2.readInt();
                  if (param2Int1 == 0)
                    bool = false; 
                  parcel2.recycle();
                  parcel1.recycle();
                  return bool;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void vibrate(int param2Int, String param2String1, VibrationEffect param2VibrationEffect, VibrationAttributes param2VibrationAttributes, String param2String2, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVibratorService");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeString(param2String1);
              if (param2VibrationEffect != null) {
                parcel1.writeInt(1);
                param2VibrationEffect.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              if (param2VibrationAttributes != null) {
                parcel1.writeInt(1);
                param2VibrationAttributes.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeString(param2String2);
                try {
                  parcel1.writeStrongBinder(param2IBinder);
                  boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
                  if (!bool && IVibratorService.Stub.getDefaultImpl() != null) {
                    IVibratorService.Stub.getDefaultImpl().vibrate(param2Int, param2String1, param2VibrationEffect, param2VibrationAttributes, param2String2, param2IBinder);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void cancelVibrate(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVibratorService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IVibratorService.Stub.getDefaultImpl() != null) {
            IVibratorService.Stub.getDefaultImpl().cancelVibrate(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVibratorService param1IVibratorService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVibratorService != null) {
          Proxy.sDefaultImpl = param1IVibratorService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVibratorService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
