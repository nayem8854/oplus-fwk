package android.os;

import android.content.IntentSender;
import android.content.pm.UserInfo;
import android.graphics.Bitmap;
import java.util.List;

public interface IUserManager extends IInterface {
  void addUserRestrictionsListener(IUserRestrictionsListener paramIUserRestrictionsListener) throws RemoteException;
  
  boolean canAddMoreManagedProfiles(int paramInt, boolean paramBoolean) throws RemoteException;
  
  boolean canAddMoreProfilesToUser(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  boolean canHaveRestrictedProfile(int paramInt) throws RemoteException;
  
  void clearSeedAccountData() throws RemoteException;
  
  UserInfo createProfileForUserEvenWhenDisallowedWithThrow(String paramString1, String paramString2, int paramInt1, int paramInt2, String[] paramArrayOfString) throws RemoteException;
  
  UserInfo createProfileForUserWithThrow(String paramString1, String paramString2, int paramInt1, int paramInt2, String[] paramArrayOfString) throws RemoteException;
  
  UserInfo createRestrictedProfileWithThrow(String paramString, int paramInt) throws RemoteException;
  
  UserInfo createUserWithThrow(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  void evictCredentialEncryptionKey(int paramInt) throws RemoteException;
  
  UserInfo findCurrentGuestUser() throws RemoteException;
  
  Bundle getApplicationRestrictions(String paramString) throws RemoteException;
  
  Bundle getApplicationRestrictionsForUser(String paramString, int paramInt) throws RemoteException;
  
  int getCredentialOwnerProfile(int paramInt) throws RemoteException;
  
  Bundle getDefaultGuestRestrictions() throws RemoteException;
  
  UserInfo getPrimaryUser() throws RemoteException;
  
  int[] getProfileIds(int paramInt, boolean paramBoolean) throws RemoteException;
  
  UserInfo getProfileParent(int paramInt) throws RemoteException;
  
  int getProfileParentId(int paramInt) throws RemoteException;
  
  List<UserInfo> getProfiles(int paramInt, boolean paramBoolean) throws RemoteException;
  
  String getSeedAccountName() throws RemoteException;
  
  PersistableBundle getSeedAccountOptions() throws RemoteException;
  
  String getSeedAccountType() throws RemoteException;
  
  String getUserAccount(int paramInt) throws RemoteException;
  
  int getUserBadgeColorResId(int paramInt) throws RemoteException;
  
  int getUserBadgeDarkColorResId(int paramInt) throws RemoteException;
  
  int getUserBadgeLabelResId(int paramInt) throws RemoteException;
  
  int getUserBadgeNoBackgroundResId(int paramInt) throws RemoteException;
  
  int getUserBadgeResId(int paramInt) throws RemoteException;
  
  long getUserCreationTime(int paramInt) throws RemoteException;
  
  int getUserHandle(int paramInt) throws RemoteException;
  
  ParcelFileDescriptor getUserIcon(int paramInt) throws RemoteException;
  
  int getUserIconBadgeResId(int paramInt) throws RemoteException;
  
  UserInfo getUserInfo(int paramInt) throws RemoteException;
  
  String getUserName() throws RemoteException;
  
  int getUserRestrictionSource(String paramString, int paramInt) throws RemoteException;
  
  List<UserManager.EnforcingUser> getUserRestrictionSources(String paramString, int paramInt) throws RemoteException;
  
  Bundle getUserRestrictions(int paramInt) throws RemoteException;
  
  int getUserSerialNumber(int paramInt) throws RemoteException;
  
  long getUserStartRealtime() throws RemoteException;
  
  long getUserUnlockRealtime() throws RemoteException;
  
  List<UserInfo> getUsers(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) throws RemoteException;
  
  boolean hasBadge(int paramInt) throws RemoteException;
  
  boolean hasBaseUserRestriction(String paramString, int paramInt) throws RemoteException;
  
  boolean hasRestrictedProfiles() throws RemoteException;
  
  boolean hasUserRestriction(String paramString, int paramInt) throws RemoteException;
  
  boolean hasUserRestrictionOnAnyUser(String paramString) throws RemoteException;
  
  boolean isDemoUser(int paramInt) throws RemoteException;
  
  boolean isManagedProfile(int paramInt) throws RemoteException;
  
  boolean isPreCreated(int paramInt) throws RemoteException;
  
  boolean isProfile(int paramInt) throws RemoteException;
  
  boolean isQuietModeEnabled(int paramInt) throws RemoteException;
  
  boolean isRestricted() throws RemoteException;
  
  boolean isSameProfileGroup(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean isSettingRestrictedForUser(String paramString1, int paramInt1, String paramString2, int paramInt2) throws RemoteException;
  
  boolean isUserNameSet(int paramInt) throws RemoteException;
  
  boolean isUserOfType(int paramInt, String paramString) throws RemoteException;
  
  boolean isUserRunning(int paramInt) throws RemoteException;
  
  boolean isUserUnlocked(int paramInt) throws RemoteException;
  
  boolean isUserUnlockingOrUnlocked(int paramInt) throws RemoteException;
  
  boolean markGuestForDeletion(int paramInt) throws RemoteException;
  
  UserInfo preCreateUserWithThrow(String paramString) throws RemoteException;
  
  boolean removeUser(int paramInt) throws RemoteException;
  
  boolean removeUserEvenWhenDisallowed(int paramInt) throws RemoteException;
  
  boolean requestQuietModeEnabled(String paramString, boolean paramBoolean, int paramInt1, IntentSender paramIntentSender, int paramInt2) throws RemoteException;
  
  void setApplicationRestrictions(String paramString, Bundle paramBundle, int paramInt) throws RemoteException;
  
  void setDefaultGuestRestrictions(Bundle paramBundle) throws RemoteException;
  
  void setSeedAccountData(int paramInt, String paramString1, String paramString2, PersistableBundle paramPersistableBundle, boolean paramBoolean) throws RemoteException;
  
  void setUserAccount(int paramInt, String paramString) throws RemoteException;
  
  void setUserAdmin(int paramInt) throws RemoteException;
  
  void setUserEnabled(int paramInt) throws RemoteException;
  
  void setUserIcon(int paramInt, Bitmap paramBitmap) throws RemoteException;
  
  void setUserName(int paramInt, String paramString) throws RemoteException;
  
  void setUserRestriction(String paramString, boolean paramBoolean, int paramInt) throws RemoteException;
  
  boolean someUserHasSeedAccount(String paramString1, String paramString2) throws RemoteException;
  
  class Default implements IUserManager {
    public int getCredentialOwnerProfile(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getProfileParentId(int param1Int) throws RemoteException {
      return 0;
    }
    
    public UserInfo createUserWithThrow(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return null;
    }
    
    public UserInfo preCreateUserWithThrow(String param1String) throws RemoteException {
      return null;
    }
    
    public UserInfo createProfileForUserWithThrow(String param1String1, String param1String2, int param1Int1, int param1Int2, String[] param1ArrayOfString) throws RemoteException {
      return null;
    }
    
    public UserInfo createRestrictedProfileWithThrow(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void setUserEnabled(int param1Int) throws RemoteException {}
    
    public void setUserAdmin(int param1Int) throws RemoteException {}
    
    public void evictCredentialEncryptionKey(int param1Int) throws RemoteException {}
    
    public boolean removeUser(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean removeUserEvenWhenDisallowed(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setUserName(int param1Int, String param1String) throws RemoteException {}
    
    public void setUserIcon(int param1Int, Bitmap param1Bitmap) throws RemoteException {}
    
    public ParcelFileDescriptor getUserIcon(int param1Int) throws RemoteException {
      return null;
    }
    
    public UserInfo getPrimaryUser() throws RemoteException {
      return null;
    }
    
    public List<UserInfo> getUsers(boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3) throws RemoteException {
      return null;
    }
    
    public List<UserInfo> getProfiles(int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public int[] getProfileIds(int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public boolean canAddMoreProfilesToUser(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean canAddMoreManagedProfiles(int param1Int, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public UserInfo getProfileParent(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean isSameProfileGroup(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public boolean isUserOfType(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public UserInfo getUserInfo(int param1Int) throws RemoteException {
      return null;
    }
    
    public String getUserAccount(int param1Int) throws RemoteException {
      return null;
    }
    
    public void setUserAccount(int param1Int, String param1String) throws RemoteException {}
    
    public long getUserCreationTime(int param1Int) throws RemoteException {
      return 0L;
    }
    
    public boolean isRestricted() throws RemoteException {
      return false;
    }
    
    public boolean canHaveRestrictedProfile(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getUserSerialNumber(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getUserHandle(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getUserRestrictionSource(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public List<UserManager.EnforcingUser> getUserRestrictionSources(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public Bundle getUserRestrictions(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean hasBaseUserRestriction(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean hasUserRestriction(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean hasUserRestrictionOnAnyUser(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean isSettingRestrictedForUser(String param1String1, int param1Int1, String param1String2, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void addUserRestrictionsListener(IUserRestrictionsListener param1IUserRestrictionsListener) throws RemoteException {}
    
    public void setUserRestriction(String param1String, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void setApplicationRestrictions(String param1String, Bundle param1Bundle, int param1Int) throws RemoteException {}
    
    public Bundle getApplicationRestrictions(String param1String) throws RemoteException {
      return null;
    }
    
    public Bundle getApplicationRestrictionsForUser(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void setDefaultGuestRestrictions(Bundle param1Bundle) throws RemoteException {}
    
    public Bundle getDefaultGuestRestrictions() throws RemoteException {
      return null;
    }
    
    public boolean markGuestForDeletion(int param1Int) throws RemoteException {
      return false;
    }
    
    public UserInfo findCurrentGuestUser() throws RemoteException {
      return null;
    }
    
    public boolean isQuietModeEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setSeedAccountData(int param1Int, String param1String1, String param1String2, PersistableBundle param1PersistableBundle, boolean param1Boolean) throws RemoteException {}
    
    public String getSeedAccountName() throws RemoteException {
      return null;
    }
    
    public String getSeedAccountType() throws RemoteException {
      return null;
    }
    
    public PersistableBundle getSeedAccountOptions() throws RemoteException {
      return null;
    }
    
    public void clearSeedAccountData() throws RemoteException {}
    
    public boolean someUserHasSeedAccount(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean isProfile(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isManagedProfile(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isDemoUser(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isPreCreated(int param1Int) throws RemoteException {
      return false;
    }
    
    public UserInfo createProfileForUserEvenWhenDisallowedWithThrow(String param1String1, String param1String2, int param1Int1, int param1Int2, String[] param1ArrayOfString) throws RemoteException {
      return null;
    }
    
    public boolean isUserUnlockingOrUnlocked(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getUserIconBadgeResId(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getUserBadgeResId(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getUserBadgeNoBackgroundResId(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getUserBadgeLabelResId(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getUserBadgeColorResId(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getUserBadgeDarkColorResId(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean hasBadge(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isUserUnlocked(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isUserRunning(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isUserNameSet(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean hasRestrictedProfiles() throws RemoteException {
      return false;
    }
    
    public boolean requestQuietModeEnabled(String param1String, boolean param1Boolean, int param1Int1, IntentSender param1IntentSender, int param1Int2) throws RemoteException {
      return false;
    }
    
    public String getUserName() throws RemoteException {
      return null;
    }
    
    public long getUserStartRealtime() throws RemoteException {
      return 0L;
    }
    
    public long getUserUnlockRealtime() throws RemoteException {
      return 0L;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IUserManager {
    private static final String DESCRIPTOR = "android.os.IUserManager";
    
    static final int TRANSACTION_addUserRestrictionsListener = 39;
    
    static final int TRANSACTION_canAddMoreManagedProfiles = 20;
    
    static final int TRANSACTION_canAddMoreProfilesToUser = 19;
    
    static final int TRANSACTION_canHaveRestrictedProfile = 29;
    
    static final int TRANSACTION_clearSeedAccountData = 53;
    
    static final int TRANSACTION_createProfileForUserEvenWhenDisallowedWithThrow = 59;
    
    static final int TRANSACTION_createProfileForUserWithThrow = 5;
    
    static final int TRANSACTION_createRestrictedProfileWithThrow = 6;
    
    static final int TRANSACTION_createUserWithThrow = 3;
    
    static final int TRANSACTION_evictCredentialEncryptionKey = 9;
    
    static final int TRANSACTION_findCurrentGuestUser = 47;
    
    static final int TRANSACTION_getApplicationRestrictions = 42;
    
    static final int TRANSACTION_getApplicationRestrictionsForUser = 43;
    
    static final int TRANSACTION_getCredentialOwnerProfile = 1;
    
    static final int TRANSACTION_getDefaultGuestRestrictions = 45;
    
    static final int TRANSACTION_getPrimaryUser = 15;
    
    static final int TRANSACTION_getProfileIds = 18;
    
    static final int TRANSACTION_getProfileParent = 21;
    
    static final int TRANSACTION_getProfileParentId = 2;
    
    static final int TRANSACTION_getProfiles = 17;
    
    static final int TRANSACTION_getSeedAccountName = 50;
    
    static final int TRANSACTION_getSeedAccountOptions = 52;
    
    static final int TRANSACTION_getSeedAccountType = 51;
    
    static final int TRANSACTION_getUserAccount = 25;
    
    static final int TRANSACTION_getUserBadgeColorResId = 65;
    
    static final int TRANSACTION_getUserBadgeDarkColorResId = 66;
    
    static final int TRANSACTION_getUserBadgeLabelResId = 64;
    
    static final int TRANSACTION_getUserBadgeNoBackgroundResId = 63;
    
    static final int TRANSACTION_getUserBadgeResId = 62;
    
    static final int TRANSACTION_getUserCreationTime = 27;
    
    static final int TRANSACTION_getUserHandle = 31;
    
    static final int TRANSACTION_getUserIcon = 14;
    
    static final int TRANSACTION_getUserIconBadgeResId = 61;
    
    static final int TRANSACTION_getUserInfo = 24;
    
    static final int TRANSACTION_getUserName = 73;
    
    static final int TRANSACTION_getUserRestrictionSource = 32;
    
    static final int TRANSACTION_getUserRestrictionSources = 33;
    
    static final int TRANSACTION_getUserRestrictions = 34;
    
    static final int TRANSACTION_getUserSerialNumber = 30;
    
    static final int TRANSACTION_getUserStartRealtime = 74;
    
    static final int TRANSACTION_getUserUnlockRealtime = 75;
    
    static final int TRANSACTION_getUsers = 16;
    
    static final int TRANSACTION_hasBadge = 67;
    
    static final int TRANSACTION_hasBaseUserRestriction = 35;
    
    static final int TRANSACTION_hasRestrictedProfiles = 71;
    
    static final int TRANSACTION_hasUserRestriction = 36;
    
    static final int TRANSACTION_hasUserRestrictionOnAnyUser = 37;
    
    static final int TRANSACTION_isDemoUser = 57;
    
    static final int TRANSACTION_isManagedProfile = 56;
    
    static final int TRANSACTION_isPreCreated = 58;
    
    static final int TRANSACTION_isProfile = 55;
    
    static final int TRANSACTION_isQuietModeEnabled = 48;
    
    static final int TRANSACTION_isRestricted = 28;
    
    static final int TRANSACTION_isSameProfileGroup = 22;
    
    static final int TRANSACTION_isSettingRestrictedForUser = 38;
    
    static final int TRANSACTION_isUserNameSet = 70;
    
    static final int TRANSACTION_isUserOfType = 23;
    
    static final int TRANSACTION_isUserRunning = 69;
    
    static final int TRANSACTION_isUserUnlocked = 68;
    
    static final int TRANSACTION_isUserUnlockingOrUnlocked = 60;
    
    static final int TRANSACTION_markGuestForDeletion = 46;
    
    static final int TRANSACTION_preCreateUserWithThrow = 4;
    
    static final int TRANSACTION_removeUser = 10;
    
    static final int TRANSACTION_removeUserEvenWhenDisallowed = 11;
    
    static final int TRANSACTION_requestQuietModeEnabled = 72;
    
    static final int TRANSACTION_setApplicationRestrictions = 41;
    
    static final int TRANSACTION_setDefaultGuestRestrictions = 44;
    
    static final int TRANSACTION_setSeedAccountData = 49;
    
    static final int TRANSACTION_setUserAccount = 26;
    
    static final int TRANSACTION_setUserAdmin = 8;
    
    static final int TRANSACTION_setUserEnabled = 7;
    
    static final int TRANSACTION_setUserIcon = 13;
    
    static final int TRANSACTION_setUserName = 12;
    
    static final int TRANSACTION_setUserRestriction = 40;
    
    static final int TRANSACTION_someUserHasSeedAccount = 54;
    
    public Stub() {
      attachInterface(this, "android.os.IUserManager");
    }
    
    public static IUserManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IUserManager");
      if (iInterface != null && iInterface instanceof IUserManager)
        return (IUserManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 75:
          return "getUserUnlockRealtime";
        case 74:
          return "getUserStartRealtime";
        case 73:
          return "getUserName";
        case 72:
          return "requestQuietModeEnabled";
        case 71:
          return "hasRestrictedProfiles";
        case 70:
          return "isUserNameSet";
        case 69:
          return "isUserRunning";
        case 68:
          return "isUserUnlocked";
        case 67:
          return "hasBadge";
        case 66:
          return "getUserBadgeDarkColorResId";
        case 65:
          return "getUserBadgeColorResId";
        case 64:
          return "getUserBadgeLabelResId";
        case 63:
          return "getUserBadgeNoBackgroundResId";
        case 62:
          return "getUserBadgeResId";
        case 61:
          return "getUserIconBadgeResId";
        case 60:
          return "isUserUnlockingOrUnlocked";
        case 59:
          return "createProfileForUserEvenWhenDisallowedWithThrow";
        case 58:
          return "isPreCreated";
        case 57:
          return "isDemoUser";
        case 56:
          return "isManagedProfile";
        case 55:
          return "isProfile";
        case 54:
          return "someUserHasSeedAccount";
        case 53:
          return "clearSeedAccountData";
        case 52:
          return "getSeedAccountOptions";
        case 51:
          return "getSeedAccountType";
        case 50:
          return "getSeedAccountName";
        case 49:
          return "setSeedAccountData";
        case 48:
          return "isQuietModeEnabled";
        case 47:
          return "findCurrentGuestUser";
        case 46:
          return "markGuestForDeletion";
        case 45:
          return "getDefaultGuestRestrictions";
        case 44:
          return "setDefaultGuestRestrictions";
        case 43:
          return "getApplicationRestrictionsForUser";
        case 42:
          return "getApplicationRestrictions";
        case 41:
          return "setApplicationRestrictions";
        case 40:
          return "setUserRestriction";
        case 39:
          return "addUserRestrictionsListener";
        case 38:
          return "isSettingRestrictedForUser";
        case 37:
          return "hasUserRestrictionOnAnyUser";
        case 36:
          return "hasUserRestriction";
        case 35:
          return "hasBaseUserRestriction";
        case 34:
          return "getUserRestrictions";
        case 33:
          return "getUserRestrictionSources";
        case 32:
          return "getUserRestrictionSource";
        case 31:
          return "getUserHandle";
        case 30:
          return "getUserSerialNumber";
        case 29:
          return "canHaveRestrictedProfile";
        case 28:
          return "isRestricted";
        case 27:
          return "getUserCreationTime";
        case 26:
          return "setUserAccount";
        case 25:
          return "getUserAccount";
        case 24:
          return "getUserInfo";
        case 23:
          return "isUserOfType";
        case 22:
          return "isSameProfileGroup";
        case 21:
          return "getProfileParent";
        case 20:
          return "canAddMoreManagedProfiles";
        case 19:
          return "canAddMoreProfilesToUser";
        case 18:
          return "getProfileIds";
        case 17:
          return "getProfiles";
        case 16:
          return "getUsers";
        case 15:
          return "getPrimaryUser";
        case 14:
          return "getUserIcon";
        case 13:
          return "setUserIcon";
        case 12:
          return "setUserName";
        case 11:
          return "removeUserEvenWhenDisallowed";
        case 10:
          return "removeUser";
        case 9:
          return "evictCredentialEncryptionKey";
        case 8:
          return "setUserAdmin";
        case 7:
          return "setUserEnabled";
        case 6:
          return "createRestrictedProfileWithThrow";
        case 5:
          return "createProfileForUserWithThrow";
        case 4:
          return "preCreateUserWithThrow";
        case 3:
          return "createUserWithThrow";
        case 2:
          return "getProfileParentId";
        case 1:
          break;
      } 
      return "getCredentialOwnerProfile";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool22;
        int i17;
        boolean bool21;
        int i16;
        boolean bool20;
        int i15;
        boolean bool19;
        int i14;
        boolean bool18;
        int i13;
        boolean bool17;
        int i12;
        boolean bool16;
        int i11;
        boolean bool15;
        int i10;
        boolean bool14;
        int i9;
        boolean bool13;
        int i8;
        boolean bool12;
        int i7;
        boolean bool11;
        int i6;
        boolean bool10;
        int i5;
        boolean bool9;
        int i4;
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        String str9, arrayOfString2[];
        UserInfo userInfo8;
        String str8;
        PersistableBundle persistableBundle;
        String str7;
        UserInfo userInfo7;
        Bundle bundle3;
        String str6;
        Bundle bundle2;
        IUserRestrictionsListener iUserRestrictionsListener;
        String str5;
        Bundle bundle1;
        List<UserManager.EnforcingUser> list1;
        String str4;
        UserInfo userInfo6;
        String str3;
        UserInfo userInfo5;
        int[] arrayOfInt;
        List<UserInfo> list;
        UserInfo userInfo4;
        ParcelFileDescriptor parcelFileDescriptor;
        String str2;
        UserInfo userInfo3;
        String[] arrayOfString1;
        UserInfo userInfo2;
        String str1;
        UserInfo userInfo1;
        long l;
        String str10;
        IntentSender intentSender;
        String str11, str12;
        boolean bool23 = false, bool24 = false, bool25 = false, bool26 = false, bool27 = false, bool28 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 75:
            param1Parcel1.enforceInterface("android.os.IUserManager");
            l = getUserUnlockRealtime();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 74:
            param1Parcel1.enforceInterface("android.os.IUserManager");
            l = getUserStartRealtime();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 73:
            param1Parcel1.enforceInterface("android.os.IUserManager");
            str9 = getUserName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str9);
            return true;
          case 72:
            str9.enforceInterface("android.os.IUserManager");
            str10 = str9.readString();
            if (str9.readInt() != 0) {
              bool26 = true;
            } else {
              bool26 = false;
            } 
            param1Int2 = str9.readInt();
            if (str9.readInt() != 0) {
              intentSender = IntentSender.CREATOR.createFromParcel((Parcel)str9);
            } else {
              intentSender = null;
            } 
            param1Int1 = str9.readInt();
            bool22 = requestQuietModeEnabled(str10, bool26, param1Int2, intentSender, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool22);
            return true;
          case 71:
            str9.enforceInterface("android.os.IUserManager");
            bool22 = hasRestrictedProfiles();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool22);
            return true;
          case 70:
            str9.enforceInterface("android.os.IUserManager");
            i17 = str9.readInt();
            bool21 = isUserNameSet(i17);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool21);
            return true;
          case 69:
            str9.enforceInterface("android.os.IUserManager");
            i16 = str9.readInt();
            bool20 = isUserRunning(i16);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool20);
            return true;
          case 68:
            str9.enforceInterface("android.os.IUserManager");
            i15 = str9.readInt();
            bool19 = isUserUnlocked(i15);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool19);
            return true;
          case 67:
            str9.enforceInterface("android.os.IUserManager");
            i14 = str9.readInt();
            bool18 = hasBadge(i14);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool18);
            return true;
          case 66:
            str9.enforceInterface("android.os.IUserManager");
            i13 = str9.readInt();
            i13 = getUserBadgeDarkColorResId(i13);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i13);
            return true;
          case 65:
            str9.enforceInterface("android.os.IUserManager");
            i13 = str9.readInt();
            i13 = getUserBadgeColorResId(i13);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i13);
            return true;
          case 64:
            str9.enforceInterface("android.os.IUserManager");
            i13 = str9.readInt();
            i13 = getUserBadgeLabelResId(i13);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i13);
            return true;
          case 63:
            str9.enforceInterface("android.os.IUserManager");
            i13 = str9.readInt();
            i13 = getUserBadgeNoBackgroundResId(i13);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i13);
            return true;
          case 62:
            str9.enforceInterface("android.os.IUserManager");
            i13 = str9.readInt();
            i13 = getUserBadgeResId(i13);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i13);
            return true;
          case 61:
            str9.enforceInterface("android.os.IUserManager");
            i13 = str9.readInt();
            i13 = getUserIconBadgeResId(i13);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i13);
            return true;
          case 60:
            str9.enforceInterface("android.os.IUserManager");
            i13 = str9.readInt();
            bool17 = isUserUnlockingOrUnlocked(i13);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool17);
            return true;
          case 59:
            str9.enforceInterface("android.os.IUserManager");
            str10 = str9.readString();
            str11 = str9.readString();
            i12 = str9.readInt();
            param1Int2 = str9.readInt();
            arrayOfString2 = str9.createStringArray();
            userInfo8 = createProfileForUserEvenWhenDisallowedWithThrow(str10, str11, i12, param1Int2, arrayOfString2);
            param1Parcel2.writeNoException();
            if (userInfo8 != null) {
              param1Parcel2.writeInt(1);
              userInfo8.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 58:
            userInfo8.enforceInterface("android.os.IUserManager");
            i12 = userInfo8.readInt();
            bool16 = isPreCreated(i12);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 57:
            userInfo8.enforceInterface("android.os.IUserManager");
            i11 = userInfo8.readInt();
            bool15 = isDemoUser(i11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 56:
            userInfo8.enforceInterface("android.os.IUserManager");
            i10 = userInfo8.readInt();
            bool14 = isManagedProfile(i10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool14);
            return true;
          case 55:
            userInfo8.enforceInterface("android.os.IUserManager");
            i9 = userInfo8.readInt();
            bool13 = isProfile(i9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool13);
            return true;
          case 54:
            userInfo8.enforceInterface("android.os.IUserManager");
            str11 = userInfo8.readString();
            str8 = userInfo8.readString();
            bool13 = someUserHasSeedAccount(str11, str8);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool13);
            return true;
          case 53:
            str8.enforceInterface("android.os.IUserManager");
            clearSeedAccountData();
            param1Parcel2.writeNoException();
            return true;
          case 52:
            str8.enforceInterface("android.os.IUserManager");
            persistableBundle = getSeedAccountOptions();
            param1Parcel2.writeNoException();
            if (persistableBundle != null) {
              param1Parcel2.writeInt(1);
              persistableBundle.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 51:
            persistableBundle.enforceInterface("android.os.IUserManager");
            str7 = getSeedAccountType();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str7);
            return true;
          case 50:
            str7.enforceInterface("android.os.IUserManager");
            str7 = getSeedAccountName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str7);
            return true;
          case 49:
            str7.enforceInterface("android.os.IUserManager");
            i8 = str7.readInt();
            str12 = str7.readString();
            str10 = str7.readString();
            if (str7.readInt() != 0) {
              PersistableBundle persistableBundle1 = PersistableBundle.CREATOR.createFromParcel((Parcel)str7);
            } else {
              str11 = null;
            } 
            if (str7.readInt() != 0) {
              bool26 = true;
            } else {
              bool26 = false;
            } 
            setSeedAccountData(i8, str12, str10, (PersistableBundle)str11, bool26);
            param1Parcel2.writeNoException();
            return true;
          case 48:
            str7.enforceInterface("android.os.IUserManager");
            i8 = str7.readInt();
            bool12 = isQuietModeEnabled(i8);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 47:
            str7.enforceInterface("android.os.IUserManager");
            userInfo7 = findCurrentGuestUser();
            param1Parcel2.writeNoException();
            if (userInfo7 != null) {
              param1Parcel2.writeInt(1);
              userInfo7.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 46:
            userInfo7.enforceInterface("android.os.IUserManager");
            i7 = userInfo7.readInt();
            bool11 = markGuestForDeletion(i7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool11);
            return true;
          case 45:
            userInfo7.enforceInterface("android.os.IUserManager");
            bundle3 = getDefaultGuestRestrictions();
            param1Parcel2.writeNoException();
            if (bundle3 != null) {
              param1Parcel2.writeInt(1);
              bundle3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 44:
            bundle3.enforceInterface("android.os.IUserManager");
            if (bundle3.readInt() != 0) {
              bundle3 = Bundle.CREATOR.createFromParcel((Parcel)bundle3);
            } else {
              bundle3 = null;
            } 
            setDefaultGuestRestrictions(bundle3);
            param1Parcel2.writeNoException();
            return true;
          case 43:
            bundle3.enforceInterface("android.os.IUserManager");
            str11 = bundle3.readString();
            i6 = bundle3.readInt();
            bundle3 = getApplicationRestrictionsForUser(str11, i6);
            param1Parcel2.writeNoException();
            if (bundle3 != null) {
              param1Parcel2.writeInt(1);
              bundle3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 42:
            bundle3.enforceInterface("android.os.IUserManager");
            str6 = bundle3.readString();
            bundle2 = getApplicationRestrictions(str6);
            param1Parcel2.writeNoException();
            if (bundle2 != null) {
              param1Parcel2.writeInt(1);
              bundle2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 41:
            bundle2.enforceInterface("android.os.IUserManager");
            str10 = bundle2.readString();
            if (bundle2.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)bundle2);
            } else {
              str11 = null;
            } 
            i6 = bundle2.readInt();
            setApplicationRestrictions(str10, (Bundle)str11, i6);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            bundle2.enforceInterface("android.os.IUserManager");
            str11 = bundle2.readString();
            bool26 = bool28;
            if (bundle2.readInt() != 0)
              bool26 = true; 
            i6 = bundle2.readInt();
            setUserRestriction(str11, bool26, i6);
            param1Parcel2.writeNoException();
            return true;
          case 39:
            bundle2.enforceInterface("android.os.IUserManager");
            iUserRestrictionsListener = IUserRestrictionsListener.Stub.asInterface(bundle2.readStrongBinder());
            addUserRestrictionsListener(iUserRestrictionsListener);
            param1Parcel2.writeNoException();
            return true;
          case 38:
            iUserRestrictionsListener.enforceInterface("android.os.IUserManager");
            str10 = iUserRestrictionsListener.readString();
            param1Int2 = iUserRestrictionsListener.readInt();
            str11 = iUserRestrictionsListener.readString();
            i6 = iUserRestrictionsListener.readInt();
            bool10 = isSettingRestrictedForUser(str10, param1Int2, str11, i6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 37:
            iUserRestrictionsListener.enforceInterface("android.os.IUserManager");
            str5 = iUserRestrictionsListener.readString();
            bool10 = hasUserRestrictionOnAnyUser(str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 36:
            str5.enforceInterface("android.os.IUserManager");
            str11 = str5.readString();
            i5 = str5.readInt();
            bool9 = hasUserRestriction(str11, i5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 35:
            str5.enforceInterface("android.os.IUserManager");
            str11 = str5.readString();
            i4 = str5.readInt();
            bool8 = hasBaseUserRestriction(str11, i4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 34:
            str5.enforceInterface("android.os.IUserManager");
            i3 = str5.readInt();
            bundle1 = getUserRestrictions(i3);
            param1Parcel2.writeNoException();
            if (bundle1 != null) {
              param1Parcel2.writeInt(1);
              bundle1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 33:
            bundle1.enforceInterface("android.os.IUserManager");
            str11 = bundle1.readString();
            i3 = bundle1.readInt();
            list1 = getUserRestrictionSources(str11, i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 32:
            list1.enforceInterface("android.os.IUserManager");
            str11 = list1.readString();
            i3 = list1.readInt();
            i3 = getUserRestrictionSource(str11, i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i3);
            return true;
          case 31:
            list1.enforceInterface("android.os.IUserManager");
            i3 = list1.readInt();
            i3 = getUserHandle(i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i3);
            return true;
          case 30:
            list1.enforceInterface("android.os.IUserManager");
            i3 = list1.readInt();
            i3 = getUserSerialNumber(i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i3);
            return true;
          case 29:
            list1.enforceInterface("android.os.IUserManager");
            i3 = list1.readInt();
            bool7 = canHaveRestrictedProfile(i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 28:
            list1.enforceInterface("android.os.IUserManager");
            bool7 = isRestricted();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 27:
            list1.enforceInterface("android.os.IUserManager");
            i2 = list1.readInt();
            l = getUserCreationTime(i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 26:
            list1.enforceInterface("android.os.IUserManager");
            i2 = list1.readInt();
            str4 = list1.readString();
            setUserAccount(i2, str4);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str4.enforceInterface("android.os.IUserManager");
            i2 = str4.readInt();
            str4 = getUserAccount(i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str4);
            return true;
          case 24:
            str4.enforceInterface("android.os.IUserManager");
            i2 = str4.readInt();
            userInfo6 = getUserInfo(i2);
            param1Parcel2.writeNoException();
            if (userInfo6 != null) {
              param1Parcel2.writeInt(1);
              userInfo6.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 23:
            userInfo6.enforceInterface("android.os.IUserManager");
            i2 = userInfo6.readInt();
            str3 = userInfo6.readString();
            bool6 = isUserOfType(i2, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 22:
            str3.enforceInterface("android.os.IUserManager");
            i1 = str3.readInt();
            param1Int2 = str3.readInt();
            bool5 = isSameProfileGroup(i1, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 21:
            str3.enforceInterface("android.os.IUserManager");
            n = str3.readInt();
            userInfo5 = getProfileParent(n);
            param1Parcel2.writeNoException();
            if (userInfo5 != null) {
              param1Parcel2.writeInt(1);
              userInfo5.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 20:
            userInfo5.enforceInterface("android.os.IUserManager");
            n = userInfo5.readInt();
            bool26 = bool23;
            if (userInfo5.readInt() != 0)
              bool26 = true; 
            bool4 = canAddMoreManagedProfiles(n, bool26);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 19:
            userInfo5.enforceInterface("android.os.IUserManager");
            str11 = userInfo5.readString();
            m = userInfo5.readInt();
            bool26 = bool24;
            if (userInfo5.readInt() != 0)
              bool26 = true; 
            bool3 = canAddMoreProfilesToUser(str11, m, bool26);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 18:
            userInfo5.enforceInterface("android.os.IUserManager");
            k = userInfo5.readInt();
            bool26 = bool25;
            if (userInfo5.readInt() != 0)
              bool26 = true; 
            arrayOfInt = getProfileIds(k, bool26);
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 17:
            arrayOfInt.enforceInterface("android.os.IUserManager");
            k = arrayOfInt.readInt();
            if (arrayOfInt.readInt() != 0)
              bool26 = true; 
            list = getProfiles(k, bool26);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 16:
            list.enforceInterface("android.os.IUserManager");
            if (list.readInt() != 0) {
              bool26 = true;
            } else {
              bool26 = false;
            } 
            if (list.readInt() != 0) {
              bool23 = true;
            } else {
              bool23 = false;
            } 
            if (list.readInt() != 0)
              bool27 = true; 
            list = getUsers(bool26, bool23, bool27);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 15:
            list.enforceInterface("android.os.IUserManager");
            userInfo4 = getPrimaryUser();
            param1Parcel2.writeNoException();
            if (userInfo4 != null) {
              param1Parcel2.writeInt(1);
              userInfo4.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 14:
            userInfo4.enforceInterface("android.os.IUserManager");
            k = userInfo4.readInt();
            parcelFileDescriptor = getUserIcon(k);
            param1Parcel2.writeNoException();
            if (parcelFileDescriptor != null) {
              param1Parcel2.writeInt(1);
              parcelFileDescriptor.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 13:
            parcelFileDescriptor.enforceInterface("android.os.IUserManager");
            k = parcelFileDescriptor.readInt();
            if (parcelFileDescriptor.readInt() != 0) {
              Bitmap bitmap = Bitmap.CREATOR.createFromParcel((Parcel)parcelFileDescriptor);
            } else {
              parcelFileDescriptor = null;
            } 
            setUserIcon(k, (Bitmap)parcelFileDescriptor);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            parcelFileDescriptor.enforceInterface("android.os.IUserManager");
            k = parcelFileDescriptor.readInt();
            str2 = parcelFileDescriptor.readString();
            setUserName(k, str2);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str2.enforceInterface("android.os.IUserManager");
            k = str2.readInt();
            bool2 = removeUserEvenWhenDisallowed(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 10:
            str2.enforceInterface("android.os.IUserManager");
            j = str2.readInt();
            bool1 = removeUser(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 9:
            str2.enforceInterface("android.os.IUserManager");
            i = str2.readInt();
            evictCredentialEncryptionKey(i);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str2.enforceInterface("android.os.IUserManager");
            i = str2.readInt();
            setUserAdmin(i);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str2.enforceInterface("android.os.IUserManager");
            i = str2.readInt();
            setUserEnabled(i);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str2.enforceInterface("android.os.IUserManager");
            str11 = str2.readString();
            i = str2.readInt();
            userInfo3 = createRestrictedProfileWithThrow(str11, i);
            param1Parcel2.writeNoException();
            if (userInfo3 != null) {
              param1Parcel2.writeInt(1);
              userInfo3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 5:
            userInfo3.enforceInterface("android.os.IUserManager");
            str11 = userInfo3.readString();
            str10 = userInfo3.readString();
            i = userInfo3.readInt();
            param1Int2 = userInfo3.readInt();
            arrayOfString1 = userInfo3.createStringArray();
            userInfo2 = createProfileForUserWithThrow(str11, str10, i, param1Int2, arrayOfString1);
            param1Parcel2.writeNoException();
            if (userInfo2 != null) {
              param1Parcel2.writeInt(1);
              userInfo2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            userInfo2.enforceInterface("android.os.IUserManager");
            str1 = userInfo2.readString();
            userInfo1 = preCreateUserWithThrow(str1);
            param1Parcel2.writeNoException();
            if (userInfo1 != null) {
              param1Parcel2.writeInt(1);
              userInfo1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            userInfo1.enforceInterface("android.os.IUserManager");
            str10 = userInfo1.readString();
            str11 = userInfo1.readString();
            i = userInfo1.readInt();
            userInfo1 = createUserWithThrow(str10, str11, i);
            param1Parcel2.writeNoException();
            if (userInfo1 != null) {
              param1Parcel2.writeInt(1);
              userInfo1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            userInfo1.enforceInterface("android.os.IUserManager");
            i = userInfo1.readInt();
            i = getProfileParentId(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 1:
            break;
        } 
        userInfo1.enforceInterface("android.os.IUserManager");
        int i = userInfo1.readInt();
        i = getCredentialOwnerProfile(i);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("android.os.IUserManager");
      return true;
    }
    
    private static class Proxy implements IUserManager {
      public static IUserManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IUserManager";
      }
      
      public int getCredentialOwnerProfile(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            param2Int = IUserManager.Stub.getDefaultImpl().getCredentialOwnerProfile(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getProfileParentId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            param2Int = IUserManager.Stub.getDefaultImpl().getProfileParentId(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UserInfo createUserWithThrow(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().createUserWithThrow(param2String1, param2String2, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            UserInfo userInfo = UserInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (UserInfo)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UserInfo preCreateUserWithThrow(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().preCreateUserWithThrow(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            UserInfo userInfo = UserInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (UserInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UserInfo createProfileForUserWithThrow(String param2String1, String param2String2, int param2Int1, int param2Int2, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().createProfileForUserWithThrow(param2String1, param2String2, param2Int1, param2Int2, param2ArrayOfString); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            UserInfo userInfo = UserInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (UserInfo)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UserInfo createRestrictedProfileWithThrow(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().createRestrictedProfileWithThrow(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            UserInfo userInfo = UserInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (UserInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUserEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            IUserManager.Stub.getDefaultImpl().setUserEnabled(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUserAdmin(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            IUserManager.Stub.getDefaultImpl().setUserAdmin(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void evictCredentialEncryptionKey(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            IUserManager.Stub.getDefaultImpl().evictCredentialEncryptionKey(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().removeUser(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeUserEvenWhenDisallowed(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().removeUserEvenWhenDisallowed(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUserName(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            IUserManager.Stub.getDefaultImpl().setUserName(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUserIcon(int param2Int, Bitmap param2Bitmap) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          if (param2Bitmap != null) {
            parcel1.writeInt(1);
            param2Bitmap.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            IUserManager.Stub.getDefaultImpl().setUserIcon(param2Int, param2Bitmap);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelFileDescriptor getUserIcon(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParcelFileDescriptor parcelFileDescriptor;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            parcelFileDescriptor = IUserManager.Stub.getDefaultImpl().getUserIcon(param2Int);
            return parcelFileDescriptor;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            parcelFileDescriptor = null;
          } 
          return parcelFileDescriptor;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UserInfo getPrimaryUser() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          UserInfo userInfo;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            userInfo = IUserManager.Stub.getDefaultImpl().getPrimaryUser();
            return userInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            userInfo = UserInfo.CREATOR.createFromParcel(parcel2);
          } else {
            userInfo = null;
          } 
          return userInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<UserInfo> getUsers(boolean param2Boolean1, boolean param2Boolean2, boolean param2Boolean3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean3) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().getUsers(param2Boolean1, param2Boolean2, param2Boolean3); 
          parcel2.readException();
          return (List)parcel2.createTypedArrayList(UserInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<UserInfo> getProfiles(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool1 && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().getProfiles(param2Int, param2Boolean); 
          parcel2.readException();
          return (List)parcel2.createTypedArrayList(UserInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getProfileIds(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool1 && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().getProfileIds(param2Int, param2Boolean); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canAddMoreProfilesToUser(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IUserManager.Stub.getDefaultImpl().canAddMoreProfilesToUser(param2String, param2Int, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canAddMoreManagedProfiles(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IUserManager.Stub.getDefaultImpl().canAddMoreManagedProfiles(param2Int, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UserInfo getProfileParent(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          UserInfo userInfo;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            userInfo = IUserManager.Stub.getDefaultImpl().getProfileParent(param2Int);
            return userInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            userInfo = UserInfo.CREATOR.createFromParcel(parcel2);
          } else {
            userInfo = null;
          } 
          return userInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSameProfileGroup(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(22, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().isSameProfileGroup(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUserOfType(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(23, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().isUserOfType(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UserInfo getUserInfo(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          UserInfo userInfo;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            userInfo = IUserManager.Stub.getDefaultImpl().getUserInfo(param2Int);
            return userInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            userInfo = UserInfo.CREATOR.createFromParcel(parcel2);
          } else {
            userInfo = null;
          } 
          return userInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getUserAccount(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().getUserAccount(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUserAccount(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            IUserManager.Stub.getDefaultImpl().setUserAccount(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getUserCreationTime(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().getUserCreationTime(param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRestricted() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(28, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().isRestricted();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canHaveRestrictedProfile(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(29, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().canHaveRestrictedProfile(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUserSerialNumber(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            param2Int = IUserManager.Stub.getDefaultImpl().getUserSerialNumber(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUserHandle(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            param2Int = IUserManager.Stub.getDefaultImpl().getUserHandle(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUserRestrictionSource(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            param2Int = IUserManager.Stub.getDefaultImpl().getUserRestrictionSource(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<UserManager.EnforcingUser> getUserRestrictionSources(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().getUserRestrictionSources(param2String, param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(UserManager.EnforcingUser.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getUserRestrictions(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Bundle bundle;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            bundle = IUserManager.Stub.getDefaultImpl().getUserRestrictions(param2Int);
            return bundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bundle = Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            bundle = null;
          } 
          return bundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasBaseUserRestriction(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(35, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().hasBaseUserRestriction(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasUserRestriction(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(36, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().hasUserRestriction(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasUserRestrictionOnAnyUser(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(37, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().hasUserRestrictionOnAnyUser(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSettingRestrictedForUser(String param2String1, int param2Int1, String param2String2, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(38, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().isSettingRestrictedForUser(param2String1, param2Int1, param2String2, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addUserRestrictionsListener(IUserRestrictionsListener param2IUserRestrictionsListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          if (param2IUserRestrictionsListener != null) {
            iBinder = param2IUserRestrictionsListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            IUserManager.Stub.getDefaultImpl().addUserRestrictionsListener(param2IUserRestrictionsListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUserRestriction(String param2String, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool1 && IUserManager.Stub.getDefaultImpl() != null) {
            IUserManager.Stub.getDefaultImpl().setUserRestriction(param2String, param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setApplicationRestrictions(String param2String, Bundle param2Bundle, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            IUserManager.Stub.getDefaultImpl().setApplicationRestrictions(param2String, param2Bundle, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getApplicationRestrictions(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().getApplicationRestrictions(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bundle bundle = Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Bundle)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getApplicationRestrictionsForUser(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().getApplicationRestrictionsForUser(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bundle bundle = Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Bundle)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDefaultGuestRestrictions(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            IUserManager.Stub.getDefaultImpl().setDefaultGuestRestrictions(param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getDefaultGuestRestrictions() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Bundle bundle;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            bundle = IUserManager.Stub.getDefaultImpl().getDefaultGuestRestrictions();
            return bundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bundle = Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            bundle = null;
          } 
          return bundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean markGuestForDeletion(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(46, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().markGuestForDeletion(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UserInfo findCurrentGuestUser() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          UserInfo userInfo;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            userInfo = IUserManager.Stub.getDefaultImpl().findCurrentGuestUser();
            return userInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            userInfo = UserInfo.CREATOR.createFromParcel(parcel2);
          } else {
            userInfo = null;
          } 
          return userInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isQuietModeEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(48, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().isQuietModeEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSeedAccountData(int param2Int, String param2String1, String param2String2, PersistableBundle param2PersistableBundle, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = true;
          if (param2PersistableBundle != null) {
            parcel1.writeInt(1);
            param2PersistableBundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool1 && IUserManager.Stub.getDefaultImpl() != null) {
            IUserManager.Stub.getDefaultImpl().setSeedAccountData(param2Int, param2String1, param2String2, param2PersistableBundle, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSeedAccountName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          boolean bool = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().getSeedAccountName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSeedAccountType() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().getSeedAccountType(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PersistableBundle getSeedAccountOptions() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          PersistableBundle persistableBundle;
          parcel1.writeInterfaceToken("android.os.IUserManager");
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            persistableBundle = IUserManager.Stub.getDefaultImpl().getSeedAccountOptions();
            return persistableBundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            persistableBundle = PersistableBundle.CREATOR.createFromParcel(parcel2);
          } else {
            persistableBundle = null;
          } 
          return persistableBundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearSeedAccountData() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            IUserManager.Stub.getDefaultImpl().clearSeedAccountData();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean someUserHasSeedAccount(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(54, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().someUserHasSeedAccount(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isProfile(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(55, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().isProfile(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isManagedProfile(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(56, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().isManagedProfile(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDemoUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(57, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().isDemoUser(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPreCreated(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(58, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().isPreCreated(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UserInfo createProfileForUserEvenWhenDisallowedWithThrow(String param2String1, String param2String2, int param2Int1, int param2Int2, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().createProfileForUserEvenWhenDisallowedWithThrow(param2String1, param2String2, param2Int1, param2Int2, param2ArrayOfString); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            UserInfo userInfo = UserInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (UserInfo)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUserUnlockingOrUnlocked(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(60, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().isUserUnlockingOrUnlocked(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUserIconBadgeResId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            param2Int = IUserManager.Stub.getDefaultImpl().getUserIconBadgeResId(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUserBadgeResId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            param2Int = IUserManager.Stub.getDefaultImpl().getUserBadgeResId(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUserBadgeNoBackgroundResId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(63, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            param2Int = IUserManager.Stub.getDefaultImpl().getUserBadgeNoBackgroundResId(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUserBadgeLabelResId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(64, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            param2Int = IUserManager.Stub.getDefaultImpl().getUserBadgeLabelResId(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUserBadgeColorResId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(65, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            param2Int = IUserManager.Stub.getDefaultImpl().getUserBadgeColorResId(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUserBadgeDarkColorResId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(66, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
            param2Int = IUserManager.Stub.getDefaultImpl().getUserBadgeDarkColorResId(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasBadge(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(67, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().hasBadge(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUserUnlocked(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(68, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().isUserUnlocked(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUserRunning(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(69, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().isUserRunning(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUserNameSet(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(70, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().isUserNameSet(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasRestrictedProfiles() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(71, parcel1, parcel2, 0);
          if (!bool2 && IUserManager.Stub.getDefaultImpl() != null) {
            bool1 = IUserManager.Stub.getDefaultImpl().hasRestrictedProfiles();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestQuietModeEnabled(String param2String, boolean param2Boolean, int param2Int1, IntentSender param2IntentSender, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          try {
            boolean bool2;
            parcel1.writeString(param2String);
            boolean bool1 = true;
            if (param2Boolean) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            parcel1.writeInt(bool2);
            try {
              parcel1.writeInt(param2Int1);
              if (param2IntentSender != null) {
                parcel1.writeInt(1);
                param2IntentSender.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeInt(param2Int2);
                try {
                  boolean bool = this.mRemote.transact(72, parcel1, parcel2, 0);
                  if (!bool && IUserManager.Stub.getDefaultImpl() != null) {
                    param2Boolean = IUserManager.Stub.getDefaultImpl().requestQuietModeEnabled(param2String, param2Boolean, param2Int1, param2IntentSender, param2Int2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Boolean;
                  } 
                  parcel2.readException();
                  param2Int1 = parcel2.readInt();
                  if (param2Int1 != 0) {
                    param2Boolean = bool1;
                  } else {
                    param2Boolean = false;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Boolean;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public String getUserName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          boolean bool = this.mRemote.transact(73, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().getUserName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getUserStartRealtime() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          boolean bool = this.mRemote.transact(74, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().getUserStartRealtime(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getUserUnlockRealtime() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IUserManager");
          boolean bool = this.mRemote.transact(75, parcel1, parcel2, 0);
          if (!bool && IUserManager.Stub.getDefaultImpl() != null)
            return IUserManager.Stub.getDefaultImpl().getUserUnlockRealtime(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IUserManager param1IUserManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IUserManager != null) {
          Proxy.sDefaultImpl = param1IUserManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IUserManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
