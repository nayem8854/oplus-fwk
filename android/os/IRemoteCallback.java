package android.os;

public interface IRemoteCallback extends IInterface {
  void sendResult(Bundle paramBundle) throws RemoteException;
  
  class Default implements IRemoteCallback {
    public void sendResult(Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRemoteCallback {
    private static final String DESCRIPTOR = "android.os.IRemoteCallback";
    
    static final int TRANSACTION_sendResult = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IRemoteCallback");
    }
    
    public static IRemoteCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IRemoteCallback");
      if (iInterface != null && iInterface instanceof IRemoteCallback)
        return (IRemoteCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "sendResult";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.IRemoteCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IRemoteCallback");
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      sendResult((Bundle)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IRemoteCallback {
      public static IRemoteCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IRemoteCallback";
      }
      
      public void sendResult(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IRemoteCallback");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IRemoteCallback.Stub.getDefaultImpl() != null) {
            IRemoteCallback.Stub.getDefaultImpl().sendResult(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRemoteCallback param1IRemoteCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRemoteCallback != null) {
          Proxy.sDefaultImpl = param1IRemoteCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRemoteCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
