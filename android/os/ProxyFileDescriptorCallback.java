package android.os;

import android.system.ErrnoException;
import android.system.OsConstants;

public abstract class ProxyFileDescriptorCallback {
  public long onGetSize() throws ErrnoException {
    throw new ErrnoException("onGetSize", OsConstants.EBADF);
  }
  
  public int onRead(long paramLong, int paramInt, byte[] paramArrayOfbyte) throws ErrnoException {
    throw new ErrnoException("onRead", OsConstants.EBADF);
  }
  
  public int onWrite(long paramLong, int paramInt, byte[] paramArrayOfbyte) throws ErrnoException {
    throw new ErrnoException("onWrite", OsConstants.EBADF);
  }
  
  public void onFsync() throws ErrnoException {
    throw new ErrnoException("onFsync", OsConstants.EINVAL);
  }
  
  public abstract void onRelease();
}
