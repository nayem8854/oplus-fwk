package android.os;

import java.util.function.Supplier;

public final class ThreadLocalWorkSource {
  public static final int UID_NONE = -1;
  
  private static final ThreadLocal<Integer> sWorkSourceUid;
  
  static {
    -$.Lambda.ThreadLocalWorkSource.IP9vRFCDG5YwbWbXAEGHH52B9IE iP9vRFCDG5YwbWbXAEGHH52B9IE = _$$Lambda$ThreadLocalWorkSource$IP9vRFCDG5YwbWbXAEGHH52B9IE.INSTANCE;
    sWorkSourceUid = ThreadLocal.withInitial((Supplier<? extends Integer>)iP9vRFCDG5YwbWbXAEGHH52B9IE);
  }
  
  public static int getUid() {
    return ((Integer)sWorkSourceUid.get()).intValue();
  }
  
  public static long setUid(int paramInt) {
    long l = getToken();
    sWorkSourceUid.set(Integer.valueOf(paramInt));
    return l;
  }
  
  public static void restore(long paramLong) {
    sWorkSourceUid.set(Integer.valueOf(parseUidFromToken(paramLong)));
  }
  
  public static long clear() {
    return setUid(-1);
  }
  
  private static int parseUidFromToken(long paramLong) {
    return (int)paramLong;
  }
  
  private static long getToken() {
    return ((Integer)sWorkSourceUid.get()).intValue();
  }
}
