package android.os;

public class OplusBaseUserManager {
  public static final String USER_TYPE_MULTI_APP = "android.os.usertype.MULTI_APP";
  
  public static final String USER_TYPE_MULTI_SYSTEM = "android.os.usertype.MULTI_SYSTEM";
}
