package android.os;

import java.util.List;

public interface IOplusUsageService extends IInterface {
  boolean accumulateDialOutDuration(int paramInt) throws RemoteException;
  
  boolean accumulateHistoryCountOfReceivedMsg(int paramInt) throws RemoteException;
  
  boolean accumulateHistoryCountOfSendedMsg(int paramInt) throws RemoteException;
  
  boolean accumulateInComingCallDuration(int paramInt) throws RemoteException;
  
  boolean deleteOplusFile(String paramString) throws RemoteException;
  
  byte[] engineerReadDevBlock(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  int engineerWriteDevBlock(String paramString, byte[] paramArrayOfbyte, int paramInt) throws RemoteException;
  
  int getApkDeleteEventRecordCount() throws RemoteException;
  
  List<String> getApkDeleteEventRecords(int paramInt1, int paramInt2) throws RemoteException;
  
  int getApkInstallEventRecordCount() throws RemoteException;
  
  List<String> getApkInstallEventRecords(int paramInt1, int paramInt2) throws RemoteException;
  
  List<String> getAppUsageCountHistoryRecords(int paramInt1, int paramInt2) throws RemoteException;
  
  int getAppUsageHistoryRecordCount() throws RemoteException;
  
  List<String> getAppUsageHistoryRecords(int paramInt1, int paramInt2) throws RemoteException;
  
  List<String> getDialCountHistoryRecords(int paramInt1, int paramInt2) throws RemoteException;
  
  int getDialOutDuration() throws RemoteException;
  
  String getDownloadStatusString(int paramInt) throws RemoteException;
  
  int getFileSize(String paramString) throws RemoteException;
  
  List<String> getHistoryBootTime() throws RemoteException;
  
  int getHistoryCountOfReceivedMsg() throws RemoteException;
  
  int getHistoryCountOfSendedMsg() throws RemoteException;
  
  List<String> getHistoryImeiNO() throws RemoteException;
  
  List<String> getHistoryPcbaNO() throws RemoteException;
  
  int getHistoryRecordsCountOfPhoneCalls() throws RemoteException;
  
  int getInComingCallDuration() throws RemoteException;
  
  int getMaxChargeCurrent() throws RemoteException;
  
  int getMaxChargeTemperature() throws RemoteException;
  
  String getMcsConnectID() throws RemoteException;
  
  int getMinChargeTemperature() throws RemoteException;
  
  List<String> getOriginalSimcardData() throws RemoteException;
  
  List<String> getPhoneCallHistoryRecords(int paramInt1, int paramInt2) throws RemoteException;
  
  int getProductLineLastTestFlag() throws RemoteException;
  
  String loadSecrecyConfig() throws RemoteException;
  
  boolean readEntireOplusDir(String paramString1, String paramString2, boolean paramBoolean) throws RemoteException;
  
  boolean readEntireOplusFile(String paramString1, String paramString2, boolean paramBoolean) throws RemoteException;
  
  byte[] readOplusFile(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  boolean recordApkDeleteEvent(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  boolean recordApkInstallEvent(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  boolean recordMcsConnectID(String paramString) throws RemoteException;
  
  boolean saveEntireOplusDir(String paramString1, String paramString2, boolean paramBoolean) throws RemoteException;
  
  int saveEntireOplusFile(String paramString1, String paramString2, boolean paramBoolean) throws RemoteException;
  
  int saveOplusFile(int paramInt1, String paramString, int paramInt2, boolean paramBoolean, int paramInt3, byte[] paramArrayOfbyte) throws RemoteException;
  
  int saveSecrecyConfig(String paramString) throws RemoteException;
  
  boolean setProductLineLastTestFlag(int paramInt) throws RemoteException;
  
  void shutDown() throws RemoteException;
  
  void testSaveSomeData(int paramInt, String paramString) throws RemoteException;
  
  boolean updateMaxChargeCurrent(int paramInt) throws RemoteException;
  
  boolean updateMaxChargeTemperature(int paramInt) throws RemoteException;
  
  boolean updateMinChargeTemperature(int paramInt) throws RemoteException;
  
  boolean writeAppUsageHistoryRecord(String paramString1, String paramString2) throws RemoteException;
  
  boolean writePhoneCallHistoryRecord(String paramString1, String paramString2) throws RemoteException;
  
  class Default implements IOplusUsageService {
    public void testSaveSomeData(int param1Int, String param1String) throws RemoteException {}
    
    public List<String> getHistoryBootTime() throws RemoteException {
      return null;
    }
    
    public List<String> getOriginalSimcardData() throws RemoteException {
      return null;
    }
    
    public List<String> getHistoryImeiNO() throws RemoteException {
      return null;
    }
    
    public List<String> getHistoryPcbaNO() throws RemoteException {
      return null;
    }
    
    public int getAppUsageHistoryRecordCount() throws RemoteException {
      return 0;
    }
    
    public List<String> getAppUsageHistoryRecords(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public List<String> getAppUsageCountHistoryRecords(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public List<String> getDialCountHistoryRecords(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public boolean writeAppUsageHistoryRecord(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public int getHistoryCountOfSendedMsg() throws RemoteException {
      return 0;
    }
    
    public int getHistoryCountOfReceivedMsg() throws RemoteException {
      return 0;
    }
    
    public boolean accumulateHistoryCountOfSendedMsg(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean accumulateHistoryCountOfReceivedMsg(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getDialOutDuration() throws RemoteException {
      return 0;
    }
    
    public int getInComingCallDuration() throws RemoteException {
      return 0;
    }
    
    public boolean accumulateDialOutDuration(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean accumulateInComingCallDuration(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getHistoryRecordsCountOfPhoneCalls() throws RemoteException {
      return 0;
    }
    
    public List<String> getPhoneCallHistoryRecords(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public boolean writePhoneCallHistoryRecord(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public void shutDown() throws RemoteException {}
    
    public int getMinChargeTemperature() throws RemoteException {
      return 0;
    }
    
    public int getMaxChargeTemperature() throws RemoteException {
      return 0;
    }
    
    public int getMaxChargeCurrent() throws RemoteException {
      return 0;
    }
    
    public boolean updateMinChargeTemperature(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean updateMaxChargeTemperature(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean updateMaxChargeCurrent(int param1Int) throws RemoteException {
      return false;
    }
    
    public byte[] engineerReadDevBlock(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public int engineerWriteDevBlock(String param1String, byte[] param1ArrayOfbyte, int param1Int) throws RemoteException {
      return 0;
    }
    
    public String getDownloadStatusString(int param1Int) throws RemoteException {
      return null;
    }
    
    public int saveSecrecyConfig(String param1String) throws RemoteException {
      return 0;
    }
    
    public String loadSecrecyConfig() throws RemoteException {
      return null;
    }
    
    public boolean setProductLineLastTestFlag(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getProductLineLastTestFlag() throws RemoteException {
      return 0;
    }
    
    public boolean recordApkDeleteEvent(String param1String1, String param1String2, String param1String3) throws RemoteException {
      return false;
    }
    
    public int getApkDeleteEventRecordCount() throws RemoteException {
      return 0;
    }
    
    public List<String> getApkDeleteEventRecords(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public boolean recordApkInstallEvent(String param1String1, String param1String2, String param1String3) throws RemoteException {
      return false;
    }
    
    public int getApkInstallEventRecordCount() throws RemoteException {
      return 0;
    }
    
    public List<String> getApkInstallEventRecords(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public boolean recordMcsConnectID(String param1String) throws RemoteException {
      return false;
    }
    
    public String getMcsConnectID() throws RemoteException {
      return null;
    }
    
    public int getFileSize(String param1String) throws RemoteException {
      return 0;
    }
    
    public byte[] readOplusFile(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public int saveOplusFile(int param1Int1, String param1String, int param1Int2, boolean param1Boolean, int param1Int3, byte[] param1ArrayOfbyte) throws RemoteException {
      return 0;
    }
    
    public int saveEntireOplusFile(String param1String1, String param1String2, boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public boolean readEntireOplusFile(String param1String1, String param1String2, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean deleteOplusFile(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean saveEntireOplusDir(String param1String1, String param1String2, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean readEntireOplusDir(String param1String1, String param1String2, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusUsageService {
    private static final String DESCRIPTOR = "android.os.IOplusUsageService";
    
    static final int TRANSACTION_accumulateDialOutDuration = 17;
    
    static final int TRANSACTION_accumulateHistoryCountOfReceivedMsg = 14;
    
    static final int TRANSACTION_accumulateHistoryCountOfSendedMsg = 13;
    
    static final int TRANSACTION_accumulateInComingCallDuration = 18;
    
    static final int TRANSACTION_deleteOplusFile = 49;
    
    static final int TRANSACTION_engineerReadDevBlock = 29;
    
    static final int TRANSACTION_engineerWriteDevBlock = 30;
    
    static final int TRANSACTION_getApkDeleteEventRecordCount = 37;
    
    static final int TRANSACTION_getApkDeleteEventRecords = 38;
    
    static final int TRANSACTION_getApkInstallEventRecordCount = 40;
    
    static final int TRANSACTION_getApkInstallEventRecords = 41;
    
    static final int TRANSACTION_getAppUsageCountHistoryRecords = 8;
    
    static final int TRANSACTION_getAppUsageHistoryRecordCount = 6;
    
    static final int TRANSACTION_getAppUsageHistoryRecords = 7;
    
    static final int TRANSACTION_getDialCountHistoryRecords = 9;
    
    static final int TRANSACTION_getDialOutDuration = 15;
    
    static final int TRANSACTION_getDownloadStatusString = 31;
    
    static final int TRANSACTION_getFileSize = 44;
    
    static final int TRANSACTION_getHistoryBootTime = 2;
    
    static final int TRANSACTION_getHistoryCountOfReceivedMsg = 12;
    
    static final int TRANSACTION_getHistoryCountOfSendedMsg = 11;
    
    static final int TRANSACTION_getHistoryImeiNO = 4;
    
    static final int TRANSACTION_getHistoryPcbaNO = 5;
    
    static final int TRANSACTION_getHistoryRecordsCountOfPhoneCalls = 19;
    
    static final int TRANSACTION_getInComingCallDuration = 16;
    
    static final int TRANSACTION_getMaxChargeCurrent = 25;
    
    static final int TRANSACTION_getMaxChargeTemperature = 24;
    
    static final int TRANSACTION_getMcsConnectID = 43;
    
    static final int TRANSACTION_getMinChargeTemperature = 23;
    
    static final int TRANSACTION_getOriginalSimcardData = 3;
    
    static final int TRANSACTION_getPhoneCallHistoryRecords = 20;
    
    static final int TRANSACTION_getProductLineLastTestFlag = 35;
    
    static final int TRANSACTION_loadSecrecyConfig = 33;
    
    static final int TRANSACTION_readEntireOplusDir = 51;
    
    static final int TRANSACTION_readEntireOplusFile = 48;
    
    static final int TRANSACTION_readOplusFile = 45;
    
    static final int TRANSACTION_recordApkDeleteEvent = 36;
    
    static final int TRANSACTION_recordApkInstallEvent = 39;
    
    static final int TRANSACTION_recordMcsConnectID = 42;
    
    static final int TRANSACTION_saveEntireOplusDir = 50;
    
    static final int TRANSACTION_saveEntireOplusFile = 47;
    
    static final int TRANSACTION_saveOplusFile = 46;
    
    static final int TRANSACTION_saveSecrecyConfig = 32;
    
    static final int TRANSACTION_setProductLineLastTestFlag = 34;
    
    static final int TRANSACTION_shutDown = 22;
    
    static final int TRANSACTION_testSaveSomeData = 1;
    
    static final int TRANSACTION_updateMaxChargeCurrent = 28;
    
    static final int TRANSACTION_updateMaxChargeTemperature = 27;
    
    static final int TRANSACTION_updateMinChargeTemperature = 26;
    
    static final int TRANSACTION_writeAppUsageHistoryRecord = 10;
    
    static final int TRANSACTION_writePhoneCallHistoryRecord = 21;
    
    public Stub() {
      attachInterface(this, "android.os.IOplusUsageService");
    }
    
    public static IOplusUsageService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IOplusUsageService");
      if (iInterface != null && iInterface instanceof IOplusUsageService)
        return (IOplusUsageService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 51:
          return "readEntireOplusDir";
        case 50:
          return "saveEntireOplusDir";
        case 49:
          return "deleteOplusFile";
        case 48:
          return "readEntireOplusFile";
        case 47:
          return "saveEntireOplusFile";
        case 46:
          return "saveOplusFile";
        case 45:
          return "readOplusFile";
        case 44:
          return "getFileSize";
        case 43:
          return "getMcsConnectID";
        case 42:
          return "recordMcsConnectID";
        case 41:
          return "getApkInstallEventRecords";
        case 40:
          return "getApkInstallEventRecordCount";
        case 39:
          return "recordApkInstallEvent";
        case 38:
          return "getApkDeleteEventRecords";
        case 37:
          return "getApkDeleteEventRecordCount";
        case 36:
          return "recordApkDeleteEvent";
        case 35:
          return "getProductLineLastTestFlag";
        case 34:
          return "setProductLineLastTestFlag";
        case 33:
          return "loadSecrecyConfig";
        case 32:
          return "saveSecrecyConfig";
        case 31:
          return "getDownloadStatusString";
        case 30:
          return "engineerWriteDevBlock";
        case 29:
          return "engineerReadDevBlock";
        case 28:
          return "updateMaxChargeCurrent";
        case 27:
          return "updateMaxChargeTemperature";
        case 26:
          return "updateMinChargeTemperature";
        case 25:
          return "getMaxChargeCurrent";
        case 24:
          return "getMaxChargeTemperature";
        case 23:
          return "getMinChargeTemperature";
        case 22:
          return "shutDown";
        case 21:
          return "writePhoneCallHistoryRecord";
        case 20:
          return "getPhoneCallHistoryRecords";
        case 19:
          return "getHistoryRecordsCountOfPhoneCalls";
        case 18:
          return "accumulateInComingCallDuration";
        case 17:
          return "accumulateDialOutDuration";
        case 16:
          return "getInComingCallDuration";
        case 15:
          return "getDialOutDuration";
        case 14:
          return "accumulateHistoryCountOfReceivedMsg";
        case 13:
          return "accumulateHistoryCountOfSendedMsg";
        case 12:
          return "getHistoryCountOfReceivedMsg";
        case 11:
          return "getHistoryCountOfSendedMsg";
        case 10:
          return "writeAppUsageHistoryRecord";
        case 9:
          return "getDialCountHistoryRecords";
        case 8:
          return "getAppUsageCountHistoryRecords";
        case 7:
          return "getAppUsageHistoryRecords";
        case 6:
          return "getAppUsageHistoryRecordCount";
        case 5:
          return "getHistoryPcbaNO";
        case 4:
          return "getHistoryImeiNO";
        case 3:
          return "getOriginalSimcardData";
        case 2:
          return "getHistoryBootTime";
        case 1:
          break;
      } 
      return "testSaveSomeData";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool14;
        int i9;
        boolean bool13;
        int i8;
        boolean bool12;
        int i7;
        boolean bool11;
        int i6;
        boolean bool10;
        int i5;
        boolean bool9;
        int i4;
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        String str7;
        byte[] arrayOfByte2;
        String str6;
        List<String> list4;
        String str5;
        List<String> list3;
        String str4;
        byte[] arrayOfByte1;
        String str3;
        List<String> list2;
        String str2;
        List<String> list1;
        String str9;
        byte[] arrayOfByte3;
        String str8, str10;
        int i10;
        boolean bool15 = false, bool16 = false, bool17 = false, bool18 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 51:
            param1Parcel1.enforceInterface("android.os.IOplusUsageService");
            str9 = param1Parcel1.readString();
            str10 = param1Parcel1.readString();
            bool16 = bool18;
            if (param1Parcel1.readInt() != 0)
              bool16 = true; 
            bool14 = readEntireOplusDir(str9, str10, bool16);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool14);
            return true;
          case 50:
            param1Parcel1.enforceInterface("android.os.IOplusUsageService");
            str10 = param1Parcel1.readString();
            str9 = param1Parcel1.readString();
            bool16 = bool15;
            if (param1Parcel1.readInt() != 0)
              bool16 = true; 
            bool14 = saveEntireOplusDir(str10, str9, bool16);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool14);
            return true;
          case 49:
            param1Parcel1.enforceInterface("android.os.IOplusUsageService");
            str7 = param1Parcel1.readString();
            bool14 = deleteOplusFile(str7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool14);
            return true;
          case 48:
            str7.enforceInterface("android.os.IOplusUsageService");
            str9 = str7.readString();
            str10 = str7.readString();
            if (str7.readInt() != 0)
              bool16 = true; 
            bool14 = readEntireOplusFile(str9, str10, bool16);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool14);
            return true;
          case 47:
            str7.enforceInterface("android.os.IOplusUsageService");
            str9 = str7.readString();
            str10 = str7.readString();
            bool16 = bool17;
            if (str7.readInt() != 0)
              bool16 = true; 
            i9 = saveEntireOplusFile(str9, str10, bool16);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i9);
            return true;
          case 46:
            str7.enforceInterface("android.os.IOplusUsageService");
            i9 = str7.readInt();
            str9 = str7.readString();
            i10 = str7.readInt();
            if (str7.readInt() != 0) {
              bool16 = true;
            } else {
              bool16 = false;
            } 
            param1Int2 = str7.readInt();
            arrayOfByte2 = str7.createByteArray();
            i9 = saveOplusFile(i9, str9, i10, bool16, param1Int2, arrayOfByte2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i9);
            return true;
          case 45:
            arrayOfByte2.enforceInterface("android.os.IOplusUsageService");
            str9 = arrayOfByte2.readString();
            param1Int2 = arrayOfByte2.readInt();
            i9 = arrayOfByte2.readInt();
            arrayOfByte2 = readOplusFile(str9, param1Int2, i9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 44:
            arrayOfByte2.enforceInterface("android.os.IOplusUsageService");
            str6 = arrayOfByte2.readString();
            i9 = getFileSize(str6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i9);
            return true;
          case 43:
            str6.enforceInterface("android.os.IOplusUsageService");
            str6 = getMcsConnectID();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str6);
            return true;
          case 42:
            str6.enforceInterface("android.os.IOplusUsageService");
            str6 = str6.readString();
            bool13 = recordMcsConnectID(str6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool13);
            return true;
          case 41:
            str6.enforceInterface("android.os.IOplusUsageService");
            param1Int2 = str6.readInt();
            i8 = str6.readInt();
            list4 = getApkInstallEventRecords(param1Int2, i8);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list4);
            return true;
          case 40:
            list4.enforceInterface("android.os.IOplusUsageService");
            i8 = getApkInstallEventRecordCount();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i8);
            return true;
          case 39:
            list4.enforceInterface("android.os.IOplusUsageService");
            str10 = list4.readString();
            str9 = list4.readString();
            str5 = list4.readString();
            bool12 = recordApkInstallEvent(str10, str9, str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 38:
            str5.enforceInterface("android.os.IOplusUsageService");
            param1Int2 = str5.readInt();
            i7 = str5.readInt();
            list3 = getApkDeleteEventRecords(param1Int2, i7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list3);
            return true;
          case 37:
            list3.enforceInterface("android.os.IOplusUsageService");
            i7 = getApkDeleteEventRecordCount();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i7);
            return true;
          case 36:
            list3.enforceInterface("android.os.IOplusUsageService");
            str10 = list3.readString();
            str9 = list3.readString();
            str4 = list3.readString();
            bool11 = recordApkDeleteEvent(str10, str9, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool11);
            return true;
          case 35:
            str4.enforceInterface("android.os.IOplusUsageService");
            i6 = getProductLineLastTestFlag();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i6);
            return true;
          case 34:
            str4.enforceInterface("android.os.IOplusUsageService");
            i6 = str4.readInt();
            bool10 = setProductLineLastTestFlag(i6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 33:
            str4.enforceInterface("android.os.IOplusUsageService");
            str4 = loadSecrecyConfig();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str4);
            return true;
          case 32:
            str4.enforceInterface("android.os.IOplusUsageService");
            str4 = str4.readString();
            i5 = saveSecrecyConfig(str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 31:
            str4.enforceInterface("android.os.IOplusUsageService");
            i5 = str4.readInt();
            str4 = getDownloadStatusString(i5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str4);
            return true;
          case 30:
            str4.enforceInterface("android.os.IOplusUsageService");
            str10 = str4.readString();
            arrayOfByte3 = str4.createByteArray();
            i5 = str4.readInt();
            i5 = engineerWriteDevBlock(str10, arrayOfByte3, i5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 29:
            str4.enforceInterface("android.os.IOplusUsageService");
            str8 = str4.readString();
            i5 = str4.readInt();
            param1Int2 = str4.readInt();
            arrayOfByte1 = engineerReadDevBlock(str8, i5, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte1);
            return true;
          case 28:
            arrayOfByte1.enforceInterface("android.os.IOplusUsageService");
            i5 = arrayOfByte1.readInt();
            bool9 = updateMaxChargeCurrent(i5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 27:
            arrayOfByte1.enforceInterface("android.os.IOplusUsageService");
            i4 = arrayOfByte1.readInt();
            bool8 = updateMaxChargeTemperature(i4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 26:
            arrayOfByte1.enforceInterface("android.os.IOplusUsageService");
            i3 = arrayOfByte1.readInt();
            bool7 = updateMinChargeTemperature(i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 25:
            arrayOfByte1.enforceInterface("android.os.IOplusUsageService");
            i2 = getMaxChargeCurrent();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 24:
            arrayOfByte1.enforceInterface("android.os.IOplusUsageService");
            i2 = getMaxChargeTemperature();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 23:
            arrayOfByte1.enforceInterface("android.os.IOplusUsageService");
            i2 = getMinChargeTemperature();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 22:
            arrayOfByte1.enforceInterface("android.os.IOplusUsageService");
            shutDown();
            param1Parcel2.writeNoException();
            return true;
          case 21:
            arrayOfByte1.enforceInterface("android.os.IOplusUsageService");
            str8 = arrayOfByte1.readString();
            str3 = arrayOfByte1.readString();
            bool6 = writePhoneCallHistoryRecord(str8, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 20:
            str3.enforceInterface("android.os.IOplusUsageService");
            param1Int2 = str3.readInt();
            i1 = str3.readInt();
            list2 = getPhoneCallHistoryRecords(param1Int2, i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list2);
            return true;
          case 19:
            list2.enforceInterface("android.os.IOplusUsageService");
            i1 = getHistoryRecordsCountOfPhoneCalls();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i1);
            return true;
          case 18:
            list2.enforceInterface("android.os.IOplusUsageService");
            i1 = list2.readInt();
            bool5 = accumulateInComingCallDuration(i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 17:
            list2.enforceInterface("android.os.IOplusUsageService");
            n = list2.readInt();
            bool4 = accumulateDialOutDuration(n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 16:
            list2.enforceInterface("android.os.IOplusUsageService");
            m = getInComingCallDuration();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 15:
            list2.enforceInterface("android.os.IOplusUsageService");
            m = getDialOutDuration();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 14:
            list2.enforceInterface("android.os.IOplusUsageService");
            m = list2.readInt();
            bool3 = accumulateHistoryCountOfReceivedMsg(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 13:
            list2.enforceInterface("android.os.IOplusUsageService");
            k = list2.readInt();
            bool2 = accumulateHistoryCountOfSendedMsg(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 12:
            list2.enforceInterface("android.os.IOplusUsageService");
            j = getHistoryCountOfReceivedMsg();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 11:
            list2.enforceInterface("android.os.IOplusUsageService");
            j = getHistoryCountOfSendedMsg();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 10:
            list2.enforceInterface("android.os.IOplusUsageService");
            str8 = list2.readString();
            str2 = list2.readString();
            bool1 = writeAppUsageHistoryRecord(str8, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 9:
            str2.enforceInterface("android.os.IOplusUsageService");
            param1Int2 = str2.readInt();
            i = str2.readInt();
            list1 = getDialCountHistoryRecords(param1Int2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 8:
            list1.enforceInterface("android.os.IOplusUsageService");
            i = list1.readInt();
            param1Int2 = list1.readInt();
            list1 = getAppUsageCountHistoryRecords(i, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 7:
            list1.enforceInterface("android.os.IOplusUsageService");
            param1Int2 = list1.readInt();
            i = list1.readInt();
            list1 = getAppUsageHistoryRecords(param1Int2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 6:
            list1.enforceInterface("android.os.IOplusUsageService");
            i = getAppUsageHistoryRecordCount();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 5:
            list1.enforceInterface("android.os.IOplusUsageService");
            list1 = getHistoryPcbaNO();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 4:
            list1.enforceInterface("android.os.IOplusUsageService");
            list1 = getHistoryImeiNO();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 3:
            list1.enforceInterface("android.os.IOplusUsageService");
            list1 = getOriginalSimcardData();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 2:
            list1.enforceInterface("android.os.IOplusUsageService");
            list1 = getHistoryBootTime();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 1:
            break;
        } 
        list1.enforceInterface("android.os.IOplusUsageService");
        int i = list1.readInt();
        String str1 = list1.readString();
        testSaveSomeData(i, str1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.os.IOplusUsageService");
      return true;
    }
    
    private static class Proxy implements IOplusUsageService {
      public static IOplusUsageService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IOplusUsageService";
      }
      
      public void testSaveSomeData(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null) {
            IOplusUsageService.Stub.getDefaultImpl().testSaveSomeData(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getHistoryBootTime() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getHistoryBootTime(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getOriginalSimcardData() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getOriginalSimcardData(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getHistoryImeiNO() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getHistoryImeiNO(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getHistoryPcbaNO() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getHistoryPcbaNO(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getAppUsageHistoryRecordCount() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getAppUsageHistoryRecordCount(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAppUsageHistoryRecords(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getAppUsageHistoryRecords(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAppUsageCountHistoryRecords(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getAppUsageCountHistoryRecords(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getDialCountHistoryRecords(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getDialCountHistoryRecords(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean writeAppUsageHistoryRecord(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().writeAppUsageHistoryRecord(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getHistoryCountOfSendedMsg() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getHistoryCountOfSendedMsg(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getHistoryCountOfReceivedMsg() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getHistoryCountOfReceivedMsg(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean accumulateHistoryCountOfSendedMsg(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().accumulateHistoryCountOfSendedMsg(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean accumulateHistoryCountOfReceivedMsg(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(14, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().accumulateHistoryCountOfReceivedMsg(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDialOutDuration() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getDialOutDuration(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getInComingCallDuration() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getInComingCallDuration(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean accumulateDialOutDuration(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().accumulateDialOutDuration(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean accumulateInComingCallDuration(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(18, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().accumulateInComingCallDuration(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getHistoryRecordsCountOfPhoneCalls() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getHistoryRecordsCountOfPhoneCalls(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getPhoneCallHistoryRecords(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getPhoneCallHistoryRecords(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean writePhoneCallHistoryRecord(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(21, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().writePhoneCallHistoryRecord(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void shutDown() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null) {
            IOplusUsageService.Stub.getDefaultImpl().shutDown();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMinChargeTemperature() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getMinChargeTemperature(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMaxChargeTemperature() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getMaxChargeTemperature(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMaxChargeCurrent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getMaxChargeCurrent(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateMinChargeTemperature(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(26, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().updateMinChargeTemperature(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateMaxChargeTemperature(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(27, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().updateMaxChargeTemperature(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateMaxChargeCurrent(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(28, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().updateMaxChargeCurrent(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] engineerReadDevBlock(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().engineerReadDevBlock(param2String, param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int engineerWriteDevBlock(String param2String, byte[] param2ArrayOfbyte, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null) {
            param2Int = IOplusUsageService.Stub.getDefaultImpl().engineerWriteDevBlock(param2String, param2ArrayOfbyte, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDownloadStatusString(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getDownloadStatusString(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int saveSecrecyConfig(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().saveSecrecyConfig(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String loadSecrecyConfig() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().loadSecrecyConfig(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setProductLineLastTestFlag(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(34, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().setProductLineLastTestFlag(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getProductLineLastTestFlag() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getProductLineLastTestFlag(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean recordApkDeleteEvent(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(36, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().recordApkDeleteEvent(param2String1, param2String2, param2String3);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getApkDeleteEventRecordCount() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getApkDeleteEventRecordCount(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getApkDeleteEventRecords(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getApkDeleteEventRecords(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean recordApkInstallEvent(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(39, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().recordApkInstallEvent(param2String1, param2String2, param2String3);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getApkInstallEventRecordCount() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getApkInstallEventRecordCount(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getApkInstallEventRecords(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getApkInstallEventRecords(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean recordMcsConnectID(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(42, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().recordMcsConnectID(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getMcsConnectID() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getMcsConnectID(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFileSize(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().getFileSize(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] readOplusFile(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null)
            return IOplusUsageService.Stub.getDefaultImpl().readOplusFile(param2String, param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int saveOplusFile(int param2Int1, String param2String, int param2Int2, boolean param2Boolean, int param2Int3, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String);
              try {
                boolean bool;
                parcel1.writeInt(param2Int2);
                if (param2Boolean) {
                  bool = true;
                } else {
                  bool = false;
                } 
                parcel1.writeInt(bool);
                try {
                  parcel1.writeInt(param2Int3);
                  try {
                    parcel1.writeByteArray(param2ArrayOfbyte);
                    try {
                      boolean bool1 = this.mRemote.transact(46, parcel1, parcel2, 0);
                      if (!bool1 && IOplusUsageService.Stub.getDefaultImpl() != null) {
                        param2Int1 = IOplusUsageService.Stub.getDefaultImpl().saveOplusFile(param2Int1, param2String, param2Int2, param2Boolean, param2Int3, param2ArrayOfbyte);
                        parcel2.recycle();
                        parcel1.recycle();
                        return param2Int1;
                      } 
                      parcel2.readException();
                      param2Int1 = parcel2.readInt();
                      parcel2.recycle();
                      parcel1.recycle();
                      return param2Int1;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public int saveEntireOplusFile(String param2String1, String param2String2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IOplusUsageService.Stub.getDefaultImpl() != null) {
            i = IOplusUsageService.Stub.getDefaultImpl().saveEntireOplusFile(param2String1, param2String2, param2Boolean);
            return i;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean readEntireOplusFile(String param2String1, String param2String2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool1 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusUsageService.Stub.getDefaultImpl().readEntireOplusFile(param2String1, param2String2, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean deleteOplusFile(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(49, parcel1, parcel2, 0);
          if (!bool2 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusUsageService.Stub.getDefaultImpl().deleteOplusFile(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean saveEntireOplusDir(String param2String1, String param2String2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool1 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusUsageService.Stub.getDefaultImpl().saveEntireOplusDir(param2String1, param2String2, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean readEntireOplusDir(String param2String1, String param2String2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusUsageService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool1 && IOplusUsageService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusUsageService.Stub.getDefaultImpl().readEntireOplusDir(param2String1, param2String2, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusUsageService param1IOplusUsageService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusUsageService != null) {
          Proxy.sDefaultImpl = param1IOplusUsageService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusUsageService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
