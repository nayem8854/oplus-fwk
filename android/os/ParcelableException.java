package android.os;

public final class ParcelableException extends RuntimeException implements Parcelable {
  public ParcelableException(Throwable paramThrowable) {
    super(paramThrowable);
  }
  
  public <T extends Throwable> void maybeRethrow(Class<T> paramClass) throws T {
    if (!paramClass.isAssignableFrom(getCause().getClass()))
      return; 
    throw (T)getCause();
  }
  
  public static Throwable readFromParcel(Parcel paramParcel) {
    String str2 = paramParcel.readString();
    String str1 = paramParcel.readString();
    try {
      Class<?> clazz = Class.forName(str2, true, Parcelable.class.getClassLoader());
      if (Throwable.class.isAssignableFrom(clazz))
        return clazz.getConstructor(new Class[] { String.class }).newInstance(new Object[] { str1 }); 
    } catch (ReflectiveOperationException reflectiveOperationException) {}
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str2);
    stringBuilder.append(": ");
    stringBuilder.append(str1);
    return new RuntimeException(stringBuilder.toString());
  }
  
  public static void writeToParcel(Parcel paramParcel, Throwable paramThrowable) {
    paramParcel.writeString(paramThrowable.getClass().getName());
    paramParcel.writeString(paramThrowable.getMessage());
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcel(paramParcel, getCause());
  }
  
  public static final Parcelable.Creator<ParcelableException> CREATOR = new Parcelable.Creator<ParcelableException>() {
      public ParcelableException createFromParcel(Parcel param1Parcel) {
        return new ParcelableException(ParcelableException.readFromParcel(param1Parcel));
      }
      
      public ParcelableException[] newArray(int param1Int) {
        return new ParcelableException[param1Int];
      }
    };
}
