package android.os;

public final class ServiceManagerNative {
  public static IServiceManager asInterface(IBinder paramIBinder) {
    if (paramIBinder == null)
      return null; 
    return new ServiceManagerProxy(paramIBinder);
  }
}
