package android.os;

public interface IDumpstateListener extends IInterface {
  public static final int BUGREPORT_ERROR_ANOTHER_REPORT_IN_PROGRESS = 5;
  
  public static final int BUGREPORT_ERROR_INVALID_INPUT = 1;
  
  public static final int BUGREPORT_ERROR_RUNTIME_ERROR = 2;
  
  public static final int BUGREPORT_ERROR_USER_CONSENT_TIMED_OUT = 4;
  
  public static final int BUGREPORT_ERROR_USER_DENIED_CONSENT = 3;
  
  void onError(int paramInt) throws RemoteException;
  
  void onFinished() throws RemoteException;
  
  void onProgress(int paramInt) throws RemoteException;
  
  void onScreenshotTaken(boolean paramBoolean) throws RemoteException;
  
  void onUiIntensiveBugreportDumpsFinished(String paramString) throws RemoteException;
  
  class Default implements IDumpstateListener {
    public void onProgress(int param1Int) throws RemoteException {}
    
    public void onError(int param1Int) throws RemoteException {}
    
    public void onFinished() throws RemoteException {}
    
    public void onScreenshotTaken(boolean param1Boolean) throws RemoteException {}
    
    public void onUiIntensiveBugreportDumpsFinished(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDumpstateListener {
    private static final String DESCRIPTOR = "android.os.IDumpstateListener";
    
    static final int TRANSACTION_onError = 2;
    
    static final int TRANSACTION_onFinished = 3;
    
    static final int TRANSACTION_onProgress = 1;
    
    static final int TRANSACTION_onScreenshotTaken = 4;
    
    static final int TRANSACTION_onUiIntensiveBugreportDumpsFinished = 5;
    
    public Stub() {
      attachInterface(this, "android.os.IDumpstateListener");
    }
    
    public static IDumpstateListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IDumpstateListener");
      if (iInterface != null && iInterface instanceof IDumpstateListener)
        return (IDumpstateListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "onUiIntensiveBugreportDumpsFinished";
            } 
            return "onScreenshotTaken";
          } 
          return "onFinished";
        } 
        return "onError";
      } 
      return "onProgress";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            boolean bool;
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.os.IDumpstateListener");
                return true;
              } 
              param1Parcel1.enforceInterface("android.os.IDumpstateListener");
              str = param1Parcel1.readString();
              onUiIntensiveBugreportDumpsFinished(str);
              return true;
            } 
            str.enforceInterface("android.os.IDumpstateListener");
            if (str.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            onScreenshotTaken(bool);
            return true;
          } 
          str.enforceInterface("android.os.IDumpstateListener");
          onFinished();
          return true;
        } 
        str.enforceInterface("android.os.IDumpstateListener");
        param1Int1 = str.readInt();
        onError(param1Int1);
        param1Parcel2.writeNoException();
        return true;
      } 
      str.enforceInterface("android.os.IDumpstateListener");
      param1Int1 = str.readInt();
      onProgress(param1Int1);
      return true;
    }
    
    private static class Proxy implements IDumpstateListener {
      public static IDumpstateListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IDumpstateListener";
      }
      
      public void onProgress(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IDumpstateListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IDumpstateListener.Stub.getDefaultImpl() != null) {
            IDumpstateListener.Stub.getDefaultImpl().onProgress(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onError(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDumpstateListener");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IDumpstateListener.Stub.getDefaultImpl() != null) {
            IDumpstateListener.Stub.getDefaultImpl().onError(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onFinished() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IDumpstateListener");
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IDumpstateListener.Stub.getDefaultImpl() != null) {
            IDumpstateListener.Stub.getDefaultImpl().onFinished();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onScreenshotTaken(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.os.IDumpstateListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool1 && IDumpstateListener.Stub.getDefaultImpl() != null) {
            IDumpstateListener.Stub.getDefaultImpl().onScreenshotTaken(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onUiIntensiveBugreportDumpsFinished(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IDumpstateListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IDumpstateListener.Stub.getDefaultImpl() != null) {
            IDumpstateListener.Stub.getDefaultImpl().onUiIntensiveBugreportDumpsFinished(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDumpstateListener param1IDumpstateListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDumpstateListener != null) {
          Proxy.sDefaultImpl = param1IDumpstateListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDumpstateListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
