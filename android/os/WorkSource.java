package android.os;

import android.annotation.SystemApi;
import android.content.ContentResolver;
import android.content.Context;
import android.provider.Settings;
import android.util.proto.ProtoOutputStream;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WorkSource implements Parcelable {
  public static final Parcelable.Creator<WorkSource> CREATOR;
  
  static final boolean DEBUG = false;
  
  static final String TAG = "WorkSource";
  
  static WorkSource sGoneWork;
  
  static WorkSource sNewbWork;
  
  static final WorkSource sTmpWorkSource = new WorkSource(0);
  
  private ArrayList<WorkChain> mChains;
  
  String[] mNames;
  
  int mNum;
  
  int[] mUids;
  
  public WorkSource() {
    this.mNum = 0;
    this.mChains = null;
  }
  
  public WorkSource(WorkSource paramWorkSource) {
    if (paramWorkSource == null) {
      this.mNum = 0;
      this.mChains = null;
      return;
    } 
    this.mNum = paramWorkSource.mNum;
    int[] arrayOfInt = paramWorkSource.mUids;
    if (arrayOfInt != null) {
      this.mUids = (int[])arrayOfInt.clone();
      String[] arrayOfString = paramWorkSource.mNames;
      if (arrayOfString != null) {
        arrayOfString = (String[])arrayOfString.clone();
      } else {
        arrayOfString = null;
      } 
      this.mNames = arrayOfString;
    } else {
      this.mUids = null;
      this.mNames = null;
    } 
    if (paramWorkSource.mChains != null) {
      this.mChains = new ArrayList<>(paramWorkSource.mChains.size());
      for (WorkChain workChain : paramWorkSource.mChains)
        this.mChains.add(new WorkChain(workChain)); 
    } else {
      this.mChains = null;
    } 
  }
  
  @SystemApi
  public WorkSource(int paramInt) {
    this.mNum = 1;
    this.mUids = new int[] { paramInt, 0 };
    this.mNames = null;
    this.mChains = null;
  }
  
  @SystemApi
  public WorkSource(int paramInt, String paramString) {
    Preconditions.checkNotNull(paramString, "packageName can't be null");
    this.mNum = 1;
    this.mUids = new int[] { paramInt, 0 };
    this.mNames = new String[] { paramString, null };
    this.mChains = null;
  }
  
  WorkSource(Parcel paramParcel) {
    this.mNum = paramParcel.readInt();
    this.mUids = paramParcel.createIntArray();
    this.mNames = paramParcel.createStringArray();
    int i = paramParcel.readInt();
    if (i > 0) {
      ArrayList<WorkChain> arrayList = new ArrayList(i);
      paramParcel.readParcelableList(arrayList, WorkChain.class.getClassLoader());
    } else {
      this.mChains = null;
    } 
  }
  
  public static boolean isChainedBatteryAttributionEnabled(Context paramContext) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    boolean bool = false;
    if (Settings.Global.getInt(contentResolver, "chained_battery_attribution_enabled", 0) == 1)
      bool = true; 
    return bool;
  }
  
  @SystemApi
  public int size() {
    return this.mNum;
  }
  
  @Deprecated
  public int get(int paramInt) {
    return getUid(paramInt);
  }
  
  @SystemApi
  public int getUid(int paramInt) {
    return this.mUids[paramInt];
  }
  
  public int getAttributionUid() {
    int i;
    if (isEmpty())
      return -1; 
    if (this.mNum > 0) {
      i = this.mUids[0];
    } else {
      i = ((WorkChain)this.mChains.get(0)).getAttributionUid();
    } 
    return i;
  }
  
  @Deprecated
  public String getName(int paramInt) {
    return getPackageName(paramInt);
  }
  
  @SystemApi
  public String getPackageName(int paramInt) {
    String[] arrayOfString = this.mNames;
    if (arrayOfString != null) {
      String str = arrayOfString[paramInt];
    } else {
      arrayOfString = null;
    } 
    return (String)arrayOfString;
  }
  
  private void clearNames() {
    if (this.mNames != null) {
      this.mNames = null;
      byte b1 = 1;
      int i = this.mNum;
      for (byte b2 = 1; b2 < this.mNum; b2++) {
        int[] arrayOfInt = this.mUids;
        if (arrayOfInt[b2] == arrayOfInt[b2 - 1]) {
          i--;
        } else {
          arrayOfInt[b1] = arrayOfInt[b2];
          b1++;
        } 
      } 
      this.mNum = i;
    } 
  }
  
  public void clear() {
    this.mNum = 0;
    ArrayList<WorkChain> arrayList = this.mChains;
    if (arrayList != null)
      arrayList.clear(); 
  }
  
  public boolean equals(Object<WorkChain> paramObject) {
    boolean bool = paramObject instanceof WorkSource;
    boolean bool1 = false;
    if (bool) {
      paramObject = paramObject;
      if (diff((WorkSource)paramObject))
        return false; 
      ArrayList<WorkChain> arrayList = this.mChains;
      if (arrayList != null && !arrayList.isEmpty())
        return this.mChains.equals(((WorkSource)paramObject).mChains); 
      paramObject = (Object<WorkChain>)((WorkSource)paramObject).mChains;
      if (paramObject == null || paramObject.isEmpty())
        bool1 = true; 
      return bool1;
    } 
    return false;
  }
  
  public int hashCode() {
    int i = 0;
    int j;
    for (j = 0; j < this.mNum; j++)
      i = (i << 4 | i >>> 28) ^ this.mUids[j]; 
    j = i;
    if (this.mNames != null) {
      byte b = 0;
      while (true) {
        j = i;
        if (b < this.mNum) {
          i = (i << 4 | i >>> 28) ^ this.mNames[b].hashCode();
          b++;
          continue;
        } 
        break;
      } 
    } 
    ArrayList<WorkChain> arrayList = this.mChains;
    i = j;
    if (arrayList != null)
      i = (j << 4 | j >>> 28) ^ arrayList.hashCode(); 
    return i;
  }
  
  public boolean diff(WorkSource paramWorkSource) {
    int i = this.mNum;
    if (i != paramWorkSource.mNum)
      return true; 
    int[] arrayOfInt1 = this.mUids;
    int[] arrayOfInt2 = paramWorkSource.mUids;
    String[] arrayOfString2 = this.mNames;
    String[] arrayOfString1 = paramWorkSource.mNames;
    for (byte b = 0; b < i; b++) {
      if (arrayOfInt1[b] != arrayOfInt2[b])
        return true; 
      if (arrayOfString2 != null && arrayOfString1 != null && !arrayOfString2[b].equals(arrayOfString1[b]))
        return true; 
    } 
    return false;
  }
  
  public void set(WorkSource paramWorkSource) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 23
    //   4: aload_0
    //   5: iconst_0
    //   6: putfield mNum : I
    //   9: aload_0
    //   10: getfield mChains : Ljava/util/ArrayList;
    //   13: astore_1
    //   14: aload_1
    //   15: ifnull -> 22
    //   18: aload_1
    //   19: invokevirtual clear : ()V
    //   22: return
    //   23: aload_1
    //   24: getfield mNum : I
    //   27: istore_2
    //   28: aload_0
    //   29: iload_2
    //   30: putfield mNum : I
    //   33: aload_1
    //   34: getfield mUids : [I
    //   37: astore_3
    //   38: aload_3
    //   39: ifnull -> 160
    //   42: aload_0
    //   43: getfield mUids : [I
    //   46: astore #4
    //   48: aload #4
    //   50: ifnull -> 72
    //   53: aload #4
    //   55: arraylength
    //   56: iload_2
    //   57: if_icmplt -> 72
    //   60: aload_3
    //   61: iconst_0
    //   62: aload #4
    //   64: iconst_0
    //   65: iload_2
    //   66: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   69: goto -> 86
    //   72: aload_0
    //   73: aload_1
    //   74: getfield mUids : [I
    //   77: invokevirtual clone : ()Ljava/lang/Object;
    //   80: checkcast [I
    //   83: putfield mUids : [I
    //   86: aload_1
    //   87: getfield mNames : [Ljava/lang/String;
    //   90: astore_3
    //   91: aload_3
    //   92: ifnull -> 152
    //   95: aload_0
    //   96: getfield mNames : [Ljava/lang/String;
    //   99: astore #4
    //   101: aload #4
    //   103: ifnull -> 135
    //   106: aload #4
    //   108: arraylength
    //   109: istore_2
    //   110: aload_0
    //   111: getfield mNum : I
    //   114: istore #5
    //   116: iload_2
    //   117: iload #5
    //   119: if_icmplt -> 135
    //   122: aload_3
    //   123: iconst_0
    //   124: aload #4
    //   126: iconst_0
    //   127: iload #5
    //   129: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   132: goto -> 170
    //   135: aload_0
    //   136: aload_1
    //   137: getfield mNames : [Ljava/lang/String;
    //   140: invokevirtual clone : ()Ljava/lang/Object;
    //   143: checkcast [Ljava/lang/String;
    //   146: putfield mNames : [Ljava/lang/String;
    //   149: goto -> 170
    //   152: aload_0
    //   153: aconst_null
    //   154: putfield mNames : [Ljava/lang/String;
    //   157: goto -> 170
    //   160: aload_0
    //   161: aconst_null
    //   162: putfield mUids : [I
    //   165: aload_0
    //   166: aconst_null
    //   167: putfield mNames : [Ljava/lang/String;
    //   170: aload_1
    //   171: getfield mChains : Ljava/util/ArrayList;
    //   174: ifnull -> 257
    //   177: aload_0
    //   178: getfield mChains : Ljava/util/ArrayList;
    //   181: astore_3
    //   182: aload_3
    //   183: ifnull -> 193
    //   186: aload_3
    //   187: invokevirtual clear : ()V
    //   190: goto -> 211
    //   193: aload_0
    //   194: new java/util/ArrayList
    //   197: dup
    //   198: aload_1
    //   199: getfield mChains : Ljava/util/ArrayList;
    //   202: invokevirtual size : ()I
    //   205: invokespecial <init> : (I)V
    //   208: putfield mChains : Ljava/util/ArrayList;
    //   211: aload_1
    //   212: getfield mChains : Ljava/util/ArrayList;
    //   215: invokevirtual iterator : ()Ljava/util/Iterator;
    //   218: astore_3
    //   219: aload_3
    //   220: invokeinterface hasNext : ()Z
    //   225: ifeq -> 257
    //   228: aload_3
    //   229: invokeinterface next : ()Ljava/lang/Object;
    //   234: checkcast android/os/WorkSource$WorkChain
    //   237: astore_1
    //   238: aload_0
    //   239: getfield mChains : Ljava/util/ArrayList;
    //   242: new android/os/WorkSource$WorkChain
    //   245: dup
    //   246: aload_1
    //   247: invokespecial <init> : (Landroid/os/WorkSource$WorkChain;)V
    //   250: invokevirtual add : (Ljava/lang/Object;)Z
    //   253: pop
    //   254: goto -> 219
    //   257: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #324	-> 0
    //   #325	-> 4
    //   #326	-> 9
    //   #327	-> 18
    //   #329	-> 22
    //   #331	-> 23
    //   #332	-> 33
    //   #333	-> 42
    //   #334	-> 60
    //   #336	-> 72
    //   #338	-> 86
    //   #339	-> 95
    //   #340	-> 122
    //   #342	-> 135
    //   #345	-> 152
    //   #348	-> 160
    //   #349	-> 165
    //   #352	-> 170
    //   #353	-> 177
    //   #354	-> 186
    //   #356	-> 193
    //   #359	-> 211
    //   #360	-> 238
    //   #361	-> 254
    //   #363	-> 257
  }
  
  public void set(int paramInt) {
    this.mNum = 1;
    if (this.mUids == null)
      this.mUids = new int[2]; 
    this.mUids[0] = paramInt;
    this.mNames = null;
    ArrayList<WorkChain> arrayList = this.mChains;
    if (arrayList != null)
      arrayList.clear(); 
  }
  
  public void set(int paramInt, String paramString) {
    if (paramString != null) {
      this.mNum = 1;
      if (this.mUids == null) {
        this.mUids = new int[2];
        this.mNames = new String[2];
      } 
      this.mUids[0] = paramInt;
      this.mNames[0] = paramString;
      ArrayList<WorkChain> arrayList = this.mChains;
      if (arrayList != null)
        arrayList.clear(); 
      return;
    } 
    throw new NullPointerException("Name can't be null");
  }
  
  @Deprecated
  public WorkSource[] setReturningDiffs(WorkSource paramWorkSource) {
    synchronized (sTmpWorkSource) {
      sNewbWork = null;
      sGoneWork = null;
      updateLocked(paramWorkSource, true, true);
      if (sNewbWork != null || sGoneWork != null) {
        paramWorkSource = sNewbWork;
        WorkSource workSource = sGoneWork;
        return new WorkSource[] { paramWorkSource, workSource };
      } 
      return null;
    } 
  }
  
  public boolean add(WorkSource paramWorkSource) {
    // Byte code:
    //   0: getstatic android/os/WorkSource.sTmpWorkSource : Landroid/os/WorkSource;
    //   3: astore_2
    //   4: aload_2
    //   5: monitorenter
    //   6: iconst_0
    //   7: istore_3
    //   8: aload_0
    //   9: aload_1
    //   10: iconst_0
    //   11: iconst_0
    //   12: invokespecial updateLocked : (Landroid/os/WorkSource;ZZ)Z
    //   15: istore #4
    //   17: aload_1
    //   18: getfield mChains : Ljava/util/ArrayList;
    //   21: ifnull -> 123
    //   24: aload_0
    //   25: getfield mChains : Ljava/util/ArrayList;
    //   28: ifnonnull -> 54
    //   31: new java/util/ArrayList
    //   34: astore #5
    //   36: aload #5
    //   38: aload_1
    //   39: getfield mChains : Ljava/util/ArrayList;
    //   42: invokevirtual size : ()I
    //   45: invokespecial <init> : (I)V
    //   48: aload_0
    //   49: aload #5
    //   51: putfield mChains : Ljava/util/ArrayList;
    //   54: aload_1
    //   55: getfield mChains : Ljava/util/ArrayList;
    //   58: invokevirtual iterator : ()Ljava/util/Iterator;
    //   61: astore_1
    //   62: aload_1
    //   63: invokeinterface hasNext : ()Z
    //   68: ifeq -> 123
    //   71: aload_1
    //   72: invokeinterface next : ()Ljava/lang/Object;
    //   77: checkcast android/os/WorkSource$WorkChain
    //   80: astore #6
    //   82: aload_0
    //   83: getfield mChains : Ljava/util/ArrayList;
    //   86: aload #6
    //   88: invokevirtual contains : (Ljava/lang/Object;)Z
    //   91: ifne -> 120
    //   94: aload_0
    //   95: getfield mChains : Ljava/util/ArrayList;
    //   98: astore #7
    //   100: new android/os/WorkSource$WorkChain
    //   103: astore #5
    //   105: aload #5
    //   107: aload #6
    //   109: invokespecial <init> : (Landroid/os/WorkSource$WorkChain;)V
    //   112: aload #7
    //   114: aload #5
    //   116: invokevirtual add : (Ljava/lang/Object;)Z
    //   119: pop
    //   120: goto -> 62
    //   123: iload #4
    //   125: ifne -> 132
    //   128: iconst_0
    //   129: ifeq -> 134
    //   132: iconst_1
    //   133: istore_3
    //   134: aload_2
    //   135: monitorexit
    //   136: iload_3
    //   137: ireturn
    //   138: astore_1
    //   139: aload_2
    //   140: monitorexit
    //   141: aload_1
    //   142: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #428	-> 0
    //   #429	-> 6
    //   #431	-> 17
    //   #432	-> 17
    //   #435	-> 24
    //   #436	-> 31
    //   #439	-> 54
    //   #440	-> 82
    //   #441	-> 94
    //   #443	-> 120
    //   #446	-> 123
    //   #447	-> 138
    // Exception table:
    //   from	to	target	type
    //   8	17	138	finally
    //   17	24	138	finally
    //   24	31	138	finally
    //   31	54	138	finally
    //   54	62	138	finally
    //   62	82	138	finally
    //   82	94	138	finally
    //   94	120	138	finally
    //   134	136	138	finally
    //   139	141	138	finally
  }
  
  @SystemApi
  public WorkSource withoutNames() {
    WorkSource workSource = new WorkSource(this);
    workSource.clearNames();
    return workSource;
  }
  
  @Deprecated
  public WorkSource addReturningNewbs(WorkSource paramWorkSource) {
    synchronized (sTmpWorkSource) {
      sNewbWork = null;
      updateLocked(paramWorkSource, false, true);
      paramWorkSource = sNewbWork;
      return paramWorkSource;
    } 
  }
  
  public boolean add(int paramInt) {
    int i = this.mNum;
    if (i <= 0) {
      this.mNames = null;
      insert(0, paramInt);
      return true;
    } 
    if (this.mNames == null) {
      i = Arrays.binarySearch(this.mUids, 0, i, paramInt);
      if (i >= 0)
        return false; 
      insert(-i - 1, paramInt);
      return true;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Adding without name to named ");
    stringBuilder.append(this);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public boolean add(int paramInt, String paramString) {
    if (this.mNum <= 0) {
      insert(0, paramInt, paramString);
      return true;
    } 
    if (this.mNames != null) {
      byte b;
      for (b = 0; b < this.mNum; b++) {
        int[] arrayOfInt = this.mUids;
        if (arrayOfInt[b] > paramInt)
          break; 
        if (arrayOfInt[b] == paramInt) {
          int i = this.mNames[b].compareTo(paramString);
          if (i > 0)
            break; 
          if (i == 0)
            return false; 
        } 
      } 
      insert(b, paramInt, paramString);
      return true;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Adding name to unnamed ");
    stringBuilder.append(this);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public boolean remove(WorkSource paramWorkSource) {
    null = isEmpty();
    boolean bool = false;
    if (null || paramWorkSource.isEmpty())
      return false; 
    if (this.mNames == null && paramWorkSource.mNames == null) {
      null = removeUids(paramWorkSource);
    } else if (this.mNames != null) {
      if (paramWorkSource.mNames != null) {
        null = removeUidsAndNames(paramWorkSource);
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Target ");
        stringBuilder.append(this);
        stringBuilder.append(" has names, but other ");
        stringBuilder.append(paramWorkSource);
        stringBuilder.append(" does not");
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Other ");
      stringBuilder.append(paramWorkSource);
      stringBuilder.append(" has names, but target ");
      stringBuilder.append(this);
      stringBuilder.append(" does not");
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    boolean bool1 = false;
    ArrayList<WorkChain> arrayList = paramWorkSource.mChains;
    boolean bool2 = bool1;
    if (arrayList != null) {
      ArrayList<WorkChain> arrayList1 = this.mChains;
      bool2 = bool1;
      if (arrayList1 != null)
        bool2 = arrayList1.removeAll(arrayList); 
    } 
    if (!null) {
      null = bool;
      return bool2 ? true : null;
    } 
    return true;
  }
  
  @SystemApi
  public WorkChain createWorkChain() {
    if (this.mChains == null)
      this.mChains = new ArrayList<>(4); 
    WorkChain workChain = new WorkChain();
    this.mChains.add(workChain);
    return workChain;
  }
  
  @SystemApi
  public boolean isEmpty() {
    if (this.mNum == 0) {
      ArrayList<WorkChain> arrayList = this.mChains;
      if (arrayList == null || arrayList.isEmpty())
        return true; 
    } 
    return false;
  }
  
  @SystemApi
  public List<WorkChain> getWorkChains() {
    return this.mChains;
  }
  
  public void transferWorkChains(WorkSource paramWorkSource) {
    ArrayList<WorkChain> arrayList = this.mChains;
    if (arrayList != null)
      arrayList.clear(); 
    arrayList = paramWorkSource.mChains;
    if (arrayList == null || arrayList.isEmpty())
      return; 
    if (this.mChains == null)
      this.mChains = new ArrayList<>(4); 
    this.mChains.addAll(paramWorkSource.mChains);
    paramWorkSource.mChains.clear();
  }
  
  private boolean removeUids(WorkSource paramWorkSource) {
    int i = this.mNum;
    int[] arrayOfInt2 = this.mUids;
    int j = paramWorkSource.mNum;
    int[] arrayOfInt1 = paramWorkSource.mUids;
    boolean bool = false;
    byte b1 = 0, b2 = 0;
    while (b1 < i && b2 < j) {
      if (arrayOfInt1[b2] == arrayOfInt2[b1]) {
        i--;
        bool = true;
        if (b1 < i)
          System.arraycopy(arrayOfInt2, b1 + 1, arrayOfInt2, b1, i - b1); 
        b2++;
        continue;
      } 
      if (arrayOfInt1[b2] > arrayOfInt2[b1]) {
        b1++;
        continue;
      } 
      b2++;
    } 
    this.mNum = i;
    return bool;
  }
  
  private boolean removeUidsAndNames(WorkSource paramWorkSource) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mNum : I
    //   4: istore_2
    //   5: aload_0
    //   6: getfield mUids : [I
    //   9: astore_3
    //   10: aload_0
    //   11: getfield mNames : [Ljava/lang/String;
    //   14: astore #4
    //   16: aload_1
    //   17: getfield mNum : I
    //   20: istore #5
    //   22: aload_1
    //   23: getfield mUids : [I
    //   26: astore #6
    //   28: aload_1
    //   29: getfield mNames : [Ljava/lang/String;
    //   32: astore_1
    //   33: iconst_0
    //   34: istore #7
    //   36: iconst_0
    //   37: istore #8
    //   39: iconst_0
    //   40: istore #9
    //   42: iload #8
    //   44: iload_2
    //   45: if_icmpge -> 194
    //   48: iload #9
    //   50: iload #5
    //   52: if_icmpge -> 194
    //   55: aload #6
    //   57: iload #9
    //   59: iaload
    //   60: aload_3
    //   61: iload #8
    //   63: iaload
    //   64: if_icmpne -> 132
    //   67: aload_1
    //   68: iload #9
    //   70: aaload
    //   71: aload #4
    //   73: iload #8
    //   75: aaload
    //   76: invokevirtual equals : (Ljava/lang/Object;)Z
    //   79: ifeq -> 132
    //   82: iinc #2, -1
    //   85: iconst_1
    //   86: istore #7
    //   88: iload #8
    //   90: iload_2
    //   91: if_icmpge -> 126
    //   94: aload_3
    //   95: iload #8
    //   97: iconst_1
    //   98: iadd
    //   99: aload_3
    //   100: iload #8
    //   102: iload_2
    //   103: iload #8
    //   105: isub
    //   106: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   109: aload #4
    //   111: iload #8
    //   113: iconst_1
    //   114: iadd
    //   115: aload #4
    //   117: iload #8
    //   119: iload_2
    //   120: iload #8
    //   122: isub
    //   123: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   126: iinc #9, 1
    //   129: goto -> 42
    //   132: aload #6
    //   134: iload #9
    //   136: iaload
    //   137: aload_3
    //   138: iload #8
    //   140: iaload
    //   141: if_icmpgt -> 188
    //   144: aload #6
    //   146: iload #9
    //   148: iaload
    //   149: aload_3
    //   150: iload #8
    //   152: iaload
    //   153: if_icmpne -> 182
    //   156: aload_1
    //   157: iload #9
    //   159: aaload
    //   160: astore #10
    //   162: aload #4
    //   164: iload #8
    //   166: aaload
    //   167: astore #11
    //   169: aload #10
    //   171: aload #11
    //   173: invokevirtual compareTo : (Ljava/lang/String;)I
    //   176: ifle -> 182
    //   179: goto -> 188
    //   182: iinc #9, 1
    //   185: goto -> 42
    //   188: iinc #8, 1
    //   191: goto -> 42
    //   194: aload_0
    //   195: iload_2
    //   196: putfield mNum : I
    //   199: iload #7
    //   201: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #656	-> 0
    //   #657	-> 5
    //   #658	-> 10
    //   #659	-> 16
    //   #660	-> 22
    //   #661	-> 28
    //   #662	-> 33
    //   #663	-> 36
    //   #665	-> 42
    //   #668	-> 55
    //   #671	-> 82
    //   #672	-> 85
    //   #673	-> 88
    //   #674	-> 94
    //   #675	-> 109
    //   #677	-> 126
    //   #678	-> 132
    //   #679	-> 169
    //   #684	-> 182
    //   #681	-> 188
    //   #688	-> 194
    //   #690	-> 199
  }
  
  private boolean updateLocked(WorkSource paramWorkSource, boolean paramBoolean1, boolean paramBoolean2) {
    if (this.mNames == null && paramWorkSource.mNames == null)
      return updateUidsLocked(paramWorkSource, paramBoolean1, paramBoolean2); 
    if (this.mNum <= 0 || this.mNames != null) {
      if (paramWorkSource.mNum <= 0 || paramWorkSource.mNames != null)
        return updateUidsAndNamesLocked(paramWorkSource, paramBoolean1, paramBoolean2); 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Target ");
      stringBuilder1.append(this);
      stringBuilder1.append(" has names, but other ");
      stringBuilder1.append(paramWorkSource);
      stringBuilder1.append(" does not");
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Other ");
    stringBuilder.append(paramWorkSource);
    stringBuilder.append(" has names, but target ");
    stringBuilder.append(this);
    stringBuilder.append(" does not");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private static WorkSource addWork(WorkSource paramWorkSource, int paramInt) {
    if (paramWorkSource == null)
      return new WorkSource(paramInt); 
    paramWorkSource.insert(paramWorkSource.mNum, paramInt);
    return paramWorkSource;
  }
  
  private boolean updateUidsLocked(WorkSource paramWorkSource, boolean paramBoolean1, boolean paramBoolean2) {
    int i = this.mNum;
    int[] arrayOfInt2 = this.mUids;
    int j = paramWorkSource.mNum;
    int[] arrayOfInt3 = paramWorkSource.mUids;
    boolean bool = false;
    int k = 0, m = 0, arrayOfInt1[] = arrayOfInt2;
    while (true) {
      if (k < i || m < j) {
        if (k >= i || (m < j && arrayOfInt3[m] < arrayOfInt1[k])) {
          bool = true;
          if (arrayOfInt1 == null) {
            arrayOfInt1 = new int[4];
            arrayOfInt1[0] = arrayOfInt3[m];
          } else if (i >= arrayOfInt1.length) {
            arrayOfInt2 = new int[arrayOfInt1.length * 3 / 2];
            if (k > 0)
              System.arraycopy(arrayOfInt1, 0, arrayOfInt2, 0, k); 
            if (k < i)
              System.arraycopy(arrayOfInt1, k, arrayOfInt2, k + 1, i - k); 
            arrayOfInt1 = arrayOfInt2;
            arrayOfInt1[k] = arrayOfInt3[m];
          } else {
            if (k < i)
              System.arraycopy(arrayOfInt1, k, arrayOfInt1, k + 1, i - k); 
            arrayOfInt1[k] = arrayOfInt3[m];
          } 
          if (paramBoolean2)
            sNewbWork = addWork(sNewbWork, arrayOfInt3[m]); 
          i++;
          k++;
          m++;
          continue;
        } 
        if (!paramBoolean1) {
          int i3 = m;
          if (m < j) {
            i3 = m;
            if (arrayOfInt3[m] == arrayOfInt1[k])
              i3 = m + 1; 
          } 
          k++;
          m = i3;
          continue;
        } 
        int n = k;
        while (true) {
          i1 = n;
          if (i1 < i && (m >= j || arrayOfInt3[m] > arrayOfInt1[i1])) {
            sGoneWork = addWork(sGoneWork, arrayOfInt1[i1]);
            n = i1 + 1;
            continue;
          } 
          break;
        } 
        int i2 = i;
        n = i1;
        if (k < i1) {
          System.arraycopy(arrayOfInt1, i1, arrayOfInt1, k, i - i1);
          i2 = i - i1 - k;
          n = k;
        } 
        k = n;
        int i1 = m;
        if (n < i2) {
          k = n;
          i1 = m;
          if (m < j) {
            k = n;
            i1 = m;
            if (arrayOfInt3[m] == arrayOfInt1[n]) {
              k = n + 1;
              i1 = m + 1;
            } 
          } 
        } 
        i = i2;
        m = i1;
        continue;
      } 
      this.mNum = i;
      this.mUids = arrayOfInt1;
      return bool;
    } 
  }
  
  private int compare(WorkSource paramWorkSource, int paramInt1, int paramInt2) {
    int i = this.mUids[paramInt1] - paramWorkSource.mUids[paramInt2];
    if (i != 0)
      return i; 
    return this.mNames[paramInt1].compareTo(paramWorkSource.mNames[paramInt2]);
  }
  
  private static WorkSource addWork(WorkSource paramWorkSource, int paramInt, String paramString) {
    if (paramWorkSource == null)
      return new WorkSource(paramInt, paramString); 
    paramWorkSource.insert(paramWorkSource.mNum, paramInt, paramString);
    return paramWorkSource;
  }
  
  private boolean updateUidsAndNamesLocked(WorkSource paramWorkSource, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_1
    //   1: getfield mNum : I
    //   4: istore #4
    //   6: aload_1
    //   7: getfield mUids : [I
    //   10: astore #5
    //   12: aload_1
    //   13: getfield mNames : [Ljava/lang/String;
    //   16: astore #6
    //   18: iconst_0
    //   19: istore #7
    //   21: iconst_0
    //   22: istore #8
    //   24: iconst_0
    //   25: istore #9
    //   27: iload #8
    //   29: aload_0
    //   30: getfield mNum : I
    //   33: if_icmplt -> 49
    //   36: iload #9
    //   38: iload #4
    //   40: if_icmpge -> 46
    //   43: goto -> 49
    //   46: iload #7
    //   48: ireturn
    //   49: iconst_m1
    //   50: istore #10
    //   52: iload #8
    //   54: aload_0
    //   55: getfield mNum : I
    //   58: if_icmpge -> 363
    //   61: iload #9
    //   63: iload #4
    //   65: if_icmpge -> 91
    //   68: aload_0
    //   69: aload_1
    //   70: iload #8
    //   72: iload #9
    //   74: invokespecial compare : (Landroid/os/WorkSource;II)I
    //   77: istore #11
    //   79: iload #11
    //   81: istore #10
    //   83: iload #11
    //   85: ifle -> 91
    //   88: goto -> 363
    //   91: iload_2
    //   92: ifne -> 131
    //   95: iload #9
    //   97: istore #11
    //   99: iload #9
    //   101: iload #4
    //   103: if_icmpge -> 121
    //   106: iload #9
    //   108: istore #11
    //   110: iload #10
    //   112: ifne -> 121
    //   115: iload #9
    //   117: iconst_1
    //   118: iadd
    //   119: istore #11
    //   121: iinc #8, 1
    //   124: iload #7
    //   126: istore #12
    //   128: goto -> 414
    //   131: iload #10
    //   133: istore #13
    //   135: iload #8
    //   137: istore #11
    //   139: iload #11
    //   141: istore #10
    //   143: iload #10
    //   145: istore #11
    //   147: iload #13
    //   149: ifge -> 229
    //   152: getstatic android/os/WorkSource.sGoneWork : Landroid/os/WorkSource;
    //   155: aload_0
    //   156: getfield mUids : [I
    //   159: iload #10
    //   161: iaload
    //   162: aload_0
    //   163: getfield mNames : [Ljava/lang/String;
    //   166: iload #10
    //   168: aaload
    //   169: invokestatic addWork : (Landroid/os/WorkSource;ILjava/lang/String;)Landroid/os/WorkSource;
    //   172: putstatic android/os/WorkSource.sGoneWork : Landroid/os/WorkSource;
    //   175: iinc #10, 1
    //   178: iload #10
    //   180: aload_0
    //   181: getfield mNum : I
    //   184: if_icmplt -> 194
    //   187: iload #10
    //   189: istore #11
    //   191: goto -> 229
    //   194: iload #9
    //   196: iload #4
    //   198: if_icmpge -> 215
    //   201: aload_0
    //   202: aload_1
    //   203: iload #10
    //   205: iload #9
    //   207: invokespecial compare : (Landroid/os/WorkSource;II)I
    //   210: istore #11
    //   212: goto -> 218
    //   215: iconst_m1
    //   216: istore #11
    //   218: iload #11
    //   220: istore #13
    //   222: iload #10
    //   224: istore #11
    //   226: goto -> 139
    //   229: iload #11
    //   231: istore #10
    //   233: iload #8
    //   235: iload #11
    //   237: if_icmpge -> 306
    //   240: aload_0
    //   241: getfield mUids : [I
    //   244: astore #14
    //   246: aload #14
    //   248: iload #11
    //   250: aload #14
    //   252: iload #8
    //   254: aload_0
    //   255: getfield mNum : I
    //   258: iload #11
    //   260: isub
    //   261: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   264: aload_0
    //   265: getfield mNames : [Ljava/lang/String;
    //   268: astore #14
    //   270: aload #14
    //   272: iload #11
    //   274: aload #14
    //   276: iload #8
    //   278: aload_0
    //   279: getfield mNum : I
    //   282: iload #11
    //   284: isub
    //   285: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   288: aload_0
    //   289: aload_0
    //   290: getfield mNum : I
    //   293: iload #11
    //   295: iload #8
    //   297: isub
    //   298: isub
    //   299: putfield mNum : I
    //   302: iload #8
    //   304: istore #10
    //   306: iload #7
    //   308: istore #12
    //   310: iload #10
    //   312: istore #8
    //   314: iload #9
    //   316: istore #11
    //   318: iload #10
    //   320: aload_0
    //   321: getfield mNum : I
    //   324: if_icmpge -> 414
    //   327: iload #7
    //   329: istore #12
    //   331: iload #10
    //   333: istore #8
    //   335: iload #9
    //   337: istore #11
    //   339: iload #13
    //   341: ifne -> 414
    //   344: iload #10
    //   346: iconst_1
    //   347: iadd
    //   348: istore #8
    //   350: iload #9
    //   352: iconst_1
    //   353: iadd
    //   354: istore #11
    //   356: iload #7
    //   358: istore #12
    //   360: goto -> 414
    //   363: iconst_1
    //   364: istore #12
    //   366: aload_0
    //   367: iload #8
    //   369: aload #5
    //   371: iload #9
    //   373: iaload
    //   374: aload #6
    //   376: iload #9
    //   378: aaload
    //   379: invokespecial insert : (IILjava/lang/String;)V
    //   382: iload_3
    //   383: ifeq -> 405
    //   386: getstatic android/os/WorkSource.sNewbWork : Landroid/os/WorkSource;
    //   389: aload #5
    //   391: iload #9
    //   393: iaload
    //   394: aload #6
    //   396: iload #9
    //   398: aaload
    //   399: invokestatic addWork : (Landroid/os/WorkSource;ILjava/lang/String;)Landroid/os/WorkSource;
    //   402: putstatic android/os/WorkSource.sNewbWork : Landroid/os/WorkSource;
    //   405: iinc #8, 1
    //   408: iload #9
    //   410: iconst_1
    //   411: iadd
    //   412: istore #11
    //   414: iload #12
    //   416: istore #7
    //   418: iload #11
    //   420: istore #9
    //   422: goto -> 27
    // Line number table:
    //   Java source line number -> byte code offset
    //   #811	-> 0
    //   #812	-> 6
    //   #813	-> 12
    //   #814	-> 18
    //   #815	-> 21
    //   #818	-> 27
    //   #869	-> 46
    //   #821	-> 49
    //   #822	-> 52
    //   #834	-> 91
    //   #836	-> 95
    //   #837	-> 115
    //   #839	-> 121
    //   #842	-> 131
    //   #843	-> 139
    //   #846	-> 152
    //   #847	-> 175
    //   #848	-> 178
    //   #849	-> 187
    //   #851	-> 194
    //   #853	-> 229
    //   #854	-> 240
    //   #855	-> 264
    //   #856	-> 288
    //   #857	-> 302
    //   #860	-> 306
    //   #862	-> 344
    //   #863	-> 350
    //   #824	-> 363
    //   #827	-> 366
    //   #828	-> 382
    //   #829	-> 386
    //   #831	-> 405
    //   #832	-> 408
    //   #867	-> 414
  }
  
  private void insert(int paramInt1, int paramInt2) {
    int[] arrayOfInt = this.mUids;
    if (arrayOfInt == null) {
      this.mUids = arrayOfInt = new int[4];
      arrayOfInt[0] = paramInt2;
      this.mNum = 1;
    } else {
      int i = this.mNum;
      if (i >= arrayOfInt.length) {
        int[] arrayOfInt1 = new int[i * 3 / 2];
        if (paramInt1 > 0)
          System.arraycopy(arrayOfInt, 0, arrayOfInt1, 0, paramInt1); 
        i = this.mNum;
        if (paramInt1 < i)
          System.arraycopy(this.mUids, paramInt1, arrayOfInt1, paramInt1 + 1, i - paramInt1); 
        this.mUids = arrayOfInt1;
        arrayOfInt1[paramInt1] = paramInt2;
        this.mNum++;
      } else {
        if (paramInt1 < i)
          System.arraycopy(arrayOfInt, paramInt1, arrayOfInt, paramInt1 + 1, i - paramInt1); 
        this.mUids[paramInt1] = paramInt2;
        this.mNum++;
      } 
    } 
  }
  
  private void insert(int paramInt1, int paramInt2, String paramString) {
    int[] arrayOfInt = this.mUids;
    if (arrayOfInt == null) {
      int[] arrayOfInt1 = new int[4];
      arrayOfInt1[0] = paramInt2;
      String[] arrayOfString = new String[4];
      arrayOfString[0] = paramString;
      this.mNum = 1;
    } else {
      int i = this.mNum;
      if (i >= arrayOfInt.length) {
        int[] arrayOfInt1 = new int[i * 3 / 2];
        String[] arrayOfString = new String[i * 3 / 2];
        if (paramInt1 > 0) {
          System.arraycopy(arrayOfInt, 0, arrayOfInt1, 0, paramInt1);
          System.arraycopy(this.mNames, 0, arrayOfString, 0, paramInt1);
        } 
        i = this.mNum;
        if (paramInt1 < i) {
          System.arraycopy(this.mUids, paramInt1, arrayOfInt1, paramInt1 + 1, i - paramInt1);
          System.arraycopy(this.mNames, paramInt1, arrayOfString, paramInt1 + 1, this.mNum - paramInt1);
        } 
        this.mUids = arrayOfInt1;
        this.mNames = arrayOfString;
        arrayOfInt1[paramInt1] = paramInt2;
        arrayOfString[paramInt1] = paramString;
        this.mNum++;
      } else {
        if (paramInt1 < i) {
          System.arraycopy(arrayOfInt, paramInt1, arrayOfInt, paramInt1 + 1, i - paramInt1);
          String[] arrayOfString = this.mNames;
          System.arraycopy(arrayOfString, paramInt1, arrayOfString, paramInt1 + 1, this.mNum - paramInt1);
        } 
        this.mUids[paramInt1] = paramInt2;
        this.mNames[paramInt1] = paramString;
        this.mNum++;
      } 
    } 
  }
  
  @SystemApi
  public static final class WorkChain implements Parcelable {
    public WorkChain() {
      this.mSize = 0;
      this.mUids = new int[4];
      this.mTags = new String[4];
    }
    
    public WorkChain(WorkChain param1WorkChain) {
      this.mSize = param1WorkChain.mSize;
      this.mUids = (int[])param1WorkChain.mUids.clone();
      this.mTags = (String[])param1WorkChain.mTags.clone();
    }
    
    private WorkChain(Parcel param1Parcel) {
      this.mSize = param1Parcel.readInt();
      this.mUids = param1Parcel.createIntArray();
      this.mTags = param1Parcel.createStringArray();
    }
    
    public WorkChain addNode(int param1Int, String param1String) {
      if (this.mSize == this.mUids.length)
        resizeArrays(); 
      int arrayOfInt[] = this.mUids, i = this.mSize;
      arrayOfInt[i] = param1Int;
      this.mTags[i] = param1String;
      this.mSize = i + 1;
      return this;
    }
    
    public int getAttributionUid() {
      byte b;
      if (this.mSize > 0) {
        b = this.mUids[0];
      } else {
        b = -1;
      } 
      return b;
    }
    
    public String getAttributionTag() {
      String[] arrayOfString = this.mTags;
      if (arrayOfString.length > 0) {
        String str = arrayOfString[0];
      } else {
        arrayOfString = null;
      } 
      return (String)arrayOfString;
    }
    
    public int[] getUids() {
      int i = this.mSize, arrayOfInt[] = new int[i];
      System.arraycopy(this.mUids, 0, arrayOfInt, 0, i);
      return arrayOfInt;
    }
    
    public String[] getTags() {
      int i = this.mSize;
      String[] arrayOfString = new String[i];
      System.arraycopy(this.mTags, 0, arrayOfString, 0, i);
      return arrayOfString;
    }
    
    public int getSize() {
      return this.mSize;
    }
    
    private void resizeArrays() {
      int i = this.mSize, j = i * 2;
      int[] arrayOfInt = new int[j];
      String[] arrayOfString = new String[j];
      System.arraycopy(this.mUids, 0, arrayOfInt, 0, i);
      System.arraycopy(this.mTags, 0, arrayOfString, 0, this.mSize);
      this.mUids = arrayOfInt;
      this.mTags = arrayOfString;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder("WorkChain{");
      for (byte b = 0; b < this.mSize; b++) {
        if (b != 0)
          stringBuilder.append(", "); 
        stringBuilder.append("(");
        stringBuilder.append(this.mUids[b]);
        if (this.mTags[b] != null) {
          stringBuilder.append(", ");
          stringBuilder.append(this.mTags[b]);
        } 
        stringBuilder.append(")");
      } 
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public int hashCode() {
      return (this.mSize + Arrays.hashCode(this.mUids) * 31) * 31 + Arrays.hashCode((Object[])this.mTags);
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof WorkChain;
      boolean bool1 = false;
      if (bool) {
        param1Object = param1Object;
        if (this.mSize == ((WorkChain)param1Object).mSize) {
          int[] arrayOfInt1 = this.mUids, arrayOfInt2 = ((WorkChain)param1Object).mUids;
          if (Arrays.equals(arrayOfInt1, arrayOfInt2)) {
            String[] arrayOfString = this.mTags;
            param1Object = ((WorkChain)param1Object).mTags;
            if (Arrays.equals((Object[])arrayOfString, (Object[])param1Object))
              bool1 = true; 
          } 
        } 
        return bool1;
      } 
      return false;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mSize);
      param1Parcel.writeIntArray(this.mUids);
      param1Parcel.writeStringArray(this.mTags);
    }
    
    public static final Parcelable.Creator<WorkChain> CREATOR = new Parcelable.Creator<WorkChain>() {
        public WorkSource.WorkChain createFromParcel(Parcel param2Parcel) {
          return new WorkSource.WorkChain(param2Parcel);
        }
        
        public WorkSource.WorkChain[] newArray(int param2Int) {
          return new WorkSource.WorkChain[param2Int];
        }
      };
    
    private int mSize;
    
    private String[] mTags;
    
    private int[] mUids;
  }
  
  class null implements Parcelable.Creator<WorkChain> {
    public WorkSource.WorkChain createFromParcel(Parcel param1Parcel) {
      return new WorkSource.WorkChain(param1Parcel);
    }
    
    public WorkSource.WorkChain[] newArray(int param1Int) {
      return new WorkSource.WorkChain[param1Int];
    }
  }
  
  public static ArrayList<WorkChain>[] diffChains(WorkSource paramWorkSource1, WorkSource paramWorkSource2) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aconst_null
    //   3: astore_3
    //   4: aconst_null
    //   5: astore #4
    //   7: aconst_null
    //   8: astore #5
    //   10: aload_0
    //   11: getfield mChains : Ljava/util/ArrayList;
    //   14: ifnull -> 118
    //   17: iconst_0
    //   18: istore #6
    //   20: aload #5
    //   22: astore #4
    //   24: iload #6
    //   26: aload_0
    //   27: getfield mChains : Ljava/util/ArrayList;
    //   30: invokevirtual size : ()I
    //   33: if_icmpge -> 118
    //   36: aload_0
    //   37: getfield mChains : Ljava/util/ArrayList;
    //   40: iload #6
    //   42: invokevirtual get : (I)Ljava/lang/Object;
    //   45: checkcast android/os/WorkSource$WorkChain
    //   48: astore #4
    //   50: aload_1
    //   51: getfield mChains : Ljava/util/ArrayList;
    //   54: astore #7
    //   56: aload #7
    //   58: ifnull -> 75
    //   61: aload #5
    //   63: astore #8
    //   65: aload #7
    //   67: aload #4
    //   69: invokevirtual contains : (Ljava/lang/Object;)Z
    //   72: ifne -> 108
    //   75: aload #5
    //   77: astore #8
    //   79: aload #5
    //   81: ifnonnull -> 100
    //   84: new java/util/ArrayList
    //   87: dup
    //   88: aload_0
    //   89: getfield mChains : Ljava/util/ArrayList;
    //   92: invokevirtual size : ()I
    //   95: invokespecial <init> : (I)V
    //   98: astore #8
    //   100: aload #8
    //   102: aload #4
    //   104: invokevirtual add : (Ljava/lang/Object;)Z
    //   107: pop
    //   108: iinc #6, 1
    //   111: aload #8
    //   113: astore #5
    //   115: goto -> 20
    //   118: aload_2
    //   119: astore #8
    //   121: aload_1
    //   122: getfield mChains : Ljava/util/ArrayList;
    //   125: ifnull -> 226
    //   128: iconst_0
    //   129: istore #6
    //   131: aload_3
    //   132: astore #5
    //   134: aload #5
    //   136: astore #8
    //   138: iload #6
    //   140: aload_1
    //   141: getfield mChains : Ljava/util/ArrayList;
    //   144: invokevirtual size : ()I
    //   147: if_icmpge -> 226
    //   150: aload_1
    //   151: getfield mChains : Ljava/util/ArrayList;
    //   154: iload #6
    //   156: invokevirtual get : (I)Ljava/lang/Object;
    //   159: checkcast android/os/WorkSource$WorkChain
    //   162: astore_3
    //   163: aload_0
    //   164: getfield mChains : Ljava/util/ArrayList;
    //   167: astore_2
    //   168: aload_2
    //   169: ifnull -> 184
    //   172: aload #5
    //   174: astore #8
    //   176: aload_2
    //   177: aload_3
    //   178: invokevirtual contains : (Ljava/lang/Object;)Z
    //   181: ifne -> 216
    //   184: aload #5
    //   186: astore #8
    //   188: aload #5
    //   190: ifnonnull -> 209
    //   193: new java/util/ArrayList
    //   196: dup
    //   197: aload_1
    //   198: getfield mChains : Ljava/util/ArrayList;
    //   201: invokevirtual size : ()I
    //   204: invokespecial <init> : (I)V
    //   207: astore #8
    //   209: aload #8
    //   211: aload_3
    //   212: invokevirtual add : (Ljava/lang/Object;)Z
    //   215: pop
    //   216: iinc #6, 1
    //   219: aload #8
    //   221: astore #5
    //   223: goto -> 134
    //   226: aload #8
    //   228: ifnonnull -> 241
    //   231: aload #4
    //   233: ifnull -> 239
    //   236: goto -> 241
    //   239: aconst_null
    //   240: areturn
    //   241: iconst_2
    //   242: anewarray java/util/ArrayList
    //   245: dup
    //   246: iconst_0
    //   247: aload #8
    //   249: aastore
    //   250: dup
    //   251: iconst_1
    //   252: aload #4
    //   254: aastore
    //   255: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1133	-> 0
    //   #1134	-> 4
    //   #1140	-> 10
    //   #1141	-> 17
    //   #1142	-> 36
    //   #1143	-> 50
    //   #1144	-> 75
    //   #1145	-> 84
    //   #1147	-> 100
    //   #1141	-> 108
    //   #1152	-> 118
    //   #1153	-> 128
    //   #1154	-> 150
    //   #1155	-> 163
    //   #1156	-> 184
    //   #1157	-> 193
    //   #1159	-> 209
    //   #1153	-> 216
    //   #1164	-> 226
    //   #1168	-> 239
    //   #1165	-> 241
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mNum);
    paramParcel.writeIntArray(this.mUids);
    paramParcel.writeStringArray(this.mNames);
    ArrayList<WorkChain> arrayList = this.mChains;
    if (arrayList == null) {
      paramParcel.writeInt(-1);
    } else {
      paramParcel.writeInt(arrayList.size());
      paramParcel.writeParcelableList(this.mChains, paramInt);
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("WorkSource{");
    byte b;
    for (b = 0; b < this.mNum; b++) {
      if (b != 0)
        stringBuilder.append(", "); 
      stringBuilder.append(this.mUids[b]);
      if (this.mNames != null) {
        stringBuilder.append(" ");
        stringBuilder.append(this.mNames[b]);
      } 
    } 
    if (this.mChains != null) {
      stringBuilder.append(" chains=");
      for (b = 0; b < this.mChains.size(); b++) {
        if (b != 0)
          stringBuilder.append(", "); 
        stringBuilder.append(this.mChains.get(b));
      } 
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    byte b;
    for (b = 0; b < this.mNum; b++) {
      long l = paramProtoOutputStream.start(2246267895809L);
      paramProtoOutputStream.write(1120986464257L, this.mUids[b]);
      String[] arrayOfString = this.mNames;
      if (arrayOfString != null)
        paramProtoOutputStream.write(1138166333442L, arrayOfString[b]); 
      paramProtoOutputStream.end(l);
    } 
    if (this.mChains != null)
      for (b = 0; b < this.mChains.size(); b++) {
        WorkChain workChain = this.mChains.get(b);
        long l = paramProtoOutputStream.start(2246267895810L);
        String[] arrayOfString = workChain.getTags();
        int[] arrayOfInt = workChain.getUids();
        for (byte b1 = 0; b1 < arrayOfString.length; b1++) {
          long l1 = paramProtoOutputStream.start(2246267895809L);
          paramProtoOutputStream.write(1120986464257L, arrayOfInt[b1]);
          paramProtoOutputStream.write(1138166333442L, arrayOfString[b1]);
          paramProtoOutputStream.end(l1);
        } 
        paramProtoOutputStream.end(l);
      }  
    paramProtoOutputStream.end(paramLong);
  }
  
  static {
    CREATOR = new Parcelable.Creator<WorkSource>() {
        public WorkSource createFromParcel(Parcel param1Parcel) {
          return new WorkSource(param1Parcel);
        }
        
        public WorkSource[] newArray(int param1Int) {
          return new WorkSource[param1Int];
        }
      };
  }
}
