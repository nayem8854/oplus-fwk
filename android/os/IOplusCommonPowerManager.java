package android.os;

import com.oplus.os.IOplusScreenStatusListener;

public interface IOplusCommonPowerManager extends IOplusBasePowerManager {
  void registerScreenStatusListener(IOplusScreenStatusListener paramIOplusScreenStatusListener) throws RemoteException;
  
  void unregisterScreenStatusListener(IOplusScreenStatusListener paramIOplusScreenStatusListener) throws RemoteException;
}
