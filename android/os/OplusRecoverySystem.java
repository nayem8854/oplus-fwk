package android.os;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.engineer.OplusEngineerManager;
import android.os.storage.IStorageManager;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

public class OplusRecoverySystem {
  private static final File LOG_FILE;
  
  private static final String PACKAGE_NAME_WIPING_EUICC_DATA_CALLBACK = "android";
  
  private static final File RECOVERY_DIR = new File("/cache/recovery");
  
  private static final String TAG = "OplusRecoverySystem";
  
  static {
    LOG_FILE = new File(RECOVERY_DIR, "log");
  }
  
  private static String sanitizeArg(String paramString) {
    paramString = paramString.replace(false, '?');
    paramString = paramString.replace('\n', '?');
    return paramString;
  }
  
  private static void bootCommand(Context paramContext, String... paramVarArgs) throws IOException {
    LOG_FILE.delete();
    StringBuilder stringBuilder = new StringBuilder();
    int i;
    byte b;
    for (i = paramVarArgs.length, b = 0; b < i; ) {
      String str = paramVarArgs[b];
      if (!TextUtils.isEmpty(str)) {
        stringBuilder.append(str);
        stringBuilder.append("\n");
      } 
      b++;
    } 
    try {
      IBinder iBinder = ServiceManager.getService("recovery");
      IRecoverySystem iRecoverySystem = IRecoverySystem.Stub.asInterface(iBinder);
      iRecoverySystem.rebootRecoveryWithCommand(stringBuilder.toString());
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
    } 
    throw new IOException("Reboot failed (no permissions?)");
  }
  
  public static void clearBackupProperty() {
    Log.d("OplusRecoverySystem", "clearBackupProperty!");
    try {
      IBinder iBinder = ServiceManager.getService("mount");
      IStorageManager iStorageManager = IStorageManager.Stub.asInterface(iBinder);
      iStorageManager.setField("SystemLocale", "");
      iStorageManager.setField("PatternVisible", "");
      iStorageManager.setField("PasswordVisible", "");
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
    } 
  }
  
  public static void rebootFormatUserData(Context paramContext, boolean paramBoolean1, String paramString, boolean paramBoolean2, boolean paramBoolean3) throws IOException {
    Log.d("OplusRecoverySystem", "rebootFormatUserData!");
    UserManager userManager = (UserManager)paramContext.getSystemService("user");
    if (paramBoolean2 || !userManager.hasUserRestriction("no_factory_reset")) {
      String str2, str3;
      ConditionVariable conditionVariable = new ConditionVariable();
      Intent intent = new Intent("android.intent.action.MASTER_CLEAR_NOTIFICATION");
      intent.addFlags(285212672);
      paramContext.sendOrderedBroadcastAsUser(intent, UserHandle.SYSTEM, "android.permission.MASTER_CLEAR", (BroadcastReceiver)new Object(conditionVariable), null, 0, null, null);
      conditionVariable.block();
      resetTWOPFlag();
      if (paramBoolean3)
        RecoverySystem.wipeEuiccData(paramContext, "android"); 
      conditionVariable = null;
      if (paramBoolean1)
        str2 = "--shutdown_after"; 
      intent = null;
      if (!TextUtils.isEmpty(paramString)) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("--reason=");
        stringBuilder1.append(sanitizeArg(paramString));
        str3 = stringBuilder1.toString();
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("--locale=");
      stringBuilder.append(Locale.getDefault().toLanguageTag());
      String str1 = stringBuilder.toString();
      bootCommand(paramContext, new String[] { str2, "--format_data", str3, str1 });
      return;
    } 
    throw new SecurityException("Wiping data is not allowed for this user.");
  }
  
  public static void rebootFormatUserDataBackup(Context paramContext, boolean paramBoolean1, String paramString, boolean paramBoolean2, boolean paramBoolean3) throws IOException {
    Log.d("OplusRecoverySystem", "rebootFormatUserDataBackup!");
    UserManager userManager = (UserManager)paramContext.getSystemService("user");
    if (paramBoolean2 || !userManager.hasUserRestriction("no_factory_reset")) {
      String str2, str3;
      ConditionVariable conditionVariable = new ConditionVariable();
      Intent intent = new Intent("android.intent.action.MASTER_CLEAR_NOTIFICATION");
      intent.addFlags(285212672);
      paramContext.sendOrderedBroadcastAsUser(intent, UserHandle.SYSTEM, "android.permission.MASTER_CLEAR", (BroadcastReceiver)new Object(conditionVariable), null, 0, null, null);
      conditionVariable.block();
      resetTWOPFlag();
      if (paramBoolean3)
        RecoverySystem.wipeEuiccData(paramContext, "android"); 
      conditionVariable = null;
      if (paramBoolean1)
        str2 = "--shutdown_after"; 
      intent = null;
      if (!TextUtils.isEmpty(paramString)) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("--reason=");
        stringBuilder1.append(sanitizeArg(paramString));
        str3 = stringBuilder1.toString();
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("--locale=");
      stringBuilder.append(Locale.getDefault().toLanguageTag());
      String str1 = stringBuilder.toString();
      bootCommand(paramContext, new String[] { str2, "--format_data_backup", str3, str1 });
      return;
    } 
    throw new SecurityException("Wiping data is not allowed for this user.");
  }
  
  private static void resetTWOPFlag() {
    String str = SystemProperties.get("ro.vendor.oplus.operator", "");
    if (str.equals("FET") || str.equals("TWM") || str.equals("APT")) {
      Log.d("OplusRecoverySystem", "reset operator to TWOP on masterclear!");
      OplusEngineerManager.setCarrierVersion("00011010");
      OplusEngineerManager.setSimOperatorSwitch("1");
    } 
  }
}
