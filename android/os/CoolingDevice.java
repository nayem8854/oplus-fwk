package android.os;

import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class CoolingDevice implements Parcelable {
  public static boolean isValidType(int paramInt) {
    boolean bool;
    if (paramInt >= 0 && paramInt <= 6) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public CoolingDevice(long paramLong, int paramInt, String paramString) {
    Preconditions.checkArgument(isValidType(paramInt), "Invalid Type");
    this.mValue = paramLong;
    this.mType = paramInt;
    this.mName = (String)Preconditions.checkStringNotEmpty(paramString);
  }
  
  public long getValue() {
    return this.mValue;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CoolingDevice{mValue=");
    stringBuilder.append(this.mValue);
    stringBuilder.append(", mType=");
    stringBuilder.append(this.mType);
    stringBuilder.append(", mName=");
    stringBuilder.append(this.mName);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    int i = this.mName.hashCode();
    int j = Long.hashCode(this.mValue);
    int k = this.mType;
    return (i * 31 + j) * 31 + k;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof CoolingDevice;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    bool = bool1;
    if (((CoolingDevice)paramObject).mValue == this.mValue) {
      bool = bool1;
      if (((CoolingDevice)paramObject).mType == this.mType) {
        bool = bool1;
        if (((CoolingDevice)paramObject).mName.equals(this.mName))
          bool = true; 
      } 
    } 
    return bool;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mValue);
    paramParcel.writeInt(this.mType);
    paramParcel.writeString(this.mName);
  }
  
  public static final Parcelable.Creator<CoolingDevice> CREATOR = new Parcelable.Creator<CoolingDevice>() {
      public CoolingDevice createFromParcel(Parcel param1Parcel) {
        long l = param1Parcel.readLong();
        int i = param1Parcel.readInt();
        String str = param1Parcel.readString();
        return new CoolingDevice(l, i, str);
      }
      
      public CoolingDevice[] newArray(int param1Int) {
        return new CoolingDevice[param1Int];
      }
    };
  
  public static final int TYPE_BATTERY = 1;
  
  public static final int TYPE_COMPONENT = 6;
  
  public static final int TYPE_CPU = 2;
  
  public static final int TYPE_FAN = 0;
  
  public static final int TYPE_GPU = 3;
  
  public static final int TYPE_MODEM = 4;
  
  public static final int TYPE_NPU = 5;
  
  private final String mName;
  
  private final int mType;
  
  private final long mValue;
  
  public int describeContents() {
    return 0;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Type implements Annotation {}
}
