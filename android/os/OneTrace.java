package android.os;

public class OneTrace {
  private static final String TAG = "OneTrace";
  
  private static boolean isOneTraceEnable(long paramLong) {
    boolean bool;
    long l = SystemProperties.getLong("debug.onetrace.tag", 0L);
    if ((l & paramLong) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static void oneTraceBegin(long paramLong, String paramString) {
    if (isOneTraceEnable(paramLong))
      OplusTraceManager.oneTraceBegin(paramString); 
  }
  
  public static void oneTraceEnd(long paramLong) {
    if (isOneTraceEnable(paramLong))
      OplusTraceManager.oneTraceEnd(); 
  }
  
  public static void oneTraceBeginAsync(long paramLong, String paramString, int paramInt) {
    if (isOneTraceEnable(paramLong))
      OplusTraceManager.oneTraceBeginAsync(paramString, paramInt); 
  }
  
  public static void oneTraceEndAsync(long paramLong, String paramString, int paramInt) {
    if (isOneTraceEnable(paramLong))
      OplusTraceManager.oneTraceEndAsync(paramString, paramInt); 
  }
}
