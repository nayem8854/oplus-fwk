package android.os;

import android.util.Log;
import com.oplus.os.IOplusScreenStatusListener;
import com.oplus.util.OplusLog;

public class OplusIPowerManager extends OplusBaseIPowerManager {
  private static final String TAG = "OplusIPowerManager";
  
  private OplusCommonPowerManager mCommonManager;
  
  protected void init(IBinder paramIBinder) {
    super.init(paramIBinder);
    this.mCommonManager = new OplusCommonPowerManager(paramIBinder);
  }
  
  public void registerScreenStatusListener(IOplusScreenStatusListener paramIOplusScreenStatusListener) {
    try {
      this.mCommonManager.registerScreenStatusListener(paramIOplusScreenStatusListener);
    } catch (RemoteException remoteException) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("registerScreenStatusListener : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool, "OplusIPowerManager", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "OplusIPowerManager", Log.getStackTraceString(exception));
    } 
  }
  
  public void unregisterScreenStatusListener(IOplusScreenStatusListener paramIOplusScreenStatusListener) {
    try {
      this.mCommonManager.unregisterScreenStatusListener(paramIOplusScreenStatusListener);
    } catch (RemoteException remoteException) {
      boolean bool = DBG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("unregisterScreenStatusListener : ");
      stringBuilder.append(remoteException.toString());
      OplusLog.e(bool, "OplusIPowerManager", stringBuilder.toString());
    } catch (Exception exception) {
      OplusLog.e(DBG, "OplusIPowerManager", Log.getStackTraceString(exception));
    } 
  }
}
