package android.os;

public interface ISchedulingPolicyService extends IInterface {
  int requestCpusetBoost(boolean paramBoolean, IBinder paramIBinder) throws RemoteException;
  
  int requestPriority(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) throws RemoteException;
  
  class Default implements ISchedulingPolicyService {
    public int requestPriority(int param1Int1, int param1Int2, int param1Int3, boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public int requestCpusetBoost(boolean param1Boolean, IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISchedulingPolicyService {
    private static final String DESCRIPTOR = "android.os.ISchedulingPolicyService";
    
    static final int TRANSACTION_requestCpusetBoost = 2;
    
    static final int TRANSACTION_requestPriority = 1;
    
    public Stub() {
      attachInterface(this, "android.os.ISchedulingPolicyService");
    }
    
    public static ISchedulingPolicyService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.ISchedulingPolicyService");
      if (iInterface != null && iInterface instanceof ISchedulingPolicyService)
        return (ISchedulingPolicyService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "requestCpusetBoost";
      } 
      return "requestPriority";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder;
      boolean bool1 = false, bool2 = false;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.os.ISchedulingPolicyService");
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.ISchedulingPolicyService");
        if (param1Parcel1.readInt() != 0)
          bool2 = true; 
        iBinder = param1Parcel1.readStrongBinder();
        param1Int1 = requestCpusetBoost(bool2, iBinder);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(param1Int1);
        return true;
      } 
      iBinder.enforceInterface("android.os.ISchedulingPolicyService");
      int i = iBinder.readInt();
      param1Int1 = iBinder.readInt();
      param1Int2 = iBinder.readInt();
      bool2 = bool1;
      if (iBinder.readInt() != 0)
        bool2 = true; 
      param1Int1 = requestPriority(i, param1Int1, param1Int2, bool2);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    private static class Proxy implements ISchedulingPolicyService {
      public static ISchedulingPolicyService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.ISchedulingPolicyService";
      }
      
      public int requestPriority(int param2Int1, int param2Int2, int param2Int3, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.ISchedulingPolicyService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && ISchedulingPolicyService.Stub.getDefaultImpl() != null) {
            param2Int1 = ISchedulingPolicyService.Stub.getDefaultImpl().requestPriority(param2Int1, param2Int2, param2Int3, param2Boolean);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int requestCpusetBoost(boolean param2Boolean, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.ISchedulingPolicyService");
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISchedulingPolicyService.Stub.getDefaultImpl() != null) {
            i = ISchedulingPolicyService.Stub.getDefaultImpl().requestCpusetBoost(param2Boolean, param2IBinder);
            return i;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISchedulingPolicyService param1ISchedulingPolicyService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISchedulingPolicyService != null) {
          Proxy.sDefaultImpl = param1ISchedulingPolicyService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISchedulingPolicyService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
