package android.os;

public interface IStoraged extends IInterface {
  int getRecentPerf() throws RemoteException;
  
  void onUserStarted(int paramInt) throws RemoteException;
  
  void onUserStopped(int paramInt) throws RemoteException;
  
  class Default implements IStoraged {
    public void onUserStarted(int param1Int) throws RemoteException {}
    
    public void onUserStopped(int param1Int) throws RemoteException {}
    
    public int getRecentPerf() throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IStoraged {
    private static final String DESCRIPTOR = "android.os.IStoraged";
    
    static final int TRANSACTION_getRecentPerf = 3;
    
    static final int TRANSACTION_onUserStarted = 1;
    
    static final int TRANSACTION_onUserStopped = 2;
    
    public Stub() {
      attachInterface(this, "android.os.IStoraged");
    }
    
    public static IStoraged asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IStoraged");
      if (iInterface != null && iInterface instanceof IStoraged)
        return (IStoraged)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "getRecentPerf";
        } 
        return "onUserStopped";
      } 
      return "onUserStarted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.os.IStoraged");
            return true;
          } 
          param1Parcel1.enforceInterface("android.os.IStoraged");
          param1Int1 = getRecentPerf();
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(param1Int1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.IStoraged");
        param1Int1 = param1Parcel1.readInt();
        onUserStopped(param1Int1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IStoraged");
      param1Int1 = param1Parcel1.readInt();
      onUserStarted(param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IStoraged {
      public static IStoraged sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IStoraged";
      }
      
      public void onUserStarted(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IStoraged");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IStoraged.Stub.getDefaultImpl() != null) {
            IStoraged.Stub.getDefaultImpl().onUserStarted(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onUserStopped(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IStoraged");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IStoraged.Stub.getDefaultImpl() != null) {
            IStoraged.Stub.getDefaultImpl().onUserStopped(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRecentPerf() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IStoraged");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IStoraged.Stub.getDefaultImpl() != null)
            return IStoraged.Stub.getDefaultImpl().getRecentPerf(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IStoraged param1IStoraged) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IStoraged != null) {
          Proxy.sDefaultImpl = param1IStoraged;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IStoraged getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
