package android.os;

import android.annotation.SystemApi;
import android.util.IntArray;
import java.util.ArrayList;

@SystemApi
public final class IncidentReportArgs implements Parcelable {
  private final IntArray mSections = new IntArray();
  
  private String mReceiverPkg;
  
  private String mReceiverCls;
  
  private int mPrivacyPolicy;
  
  private final ArrayList<byte[]> mHeaders = (ArrayList)new ArrayList<>();
  
  private boolean mAll;
  
  public IncidentReportArgs() {
    this.mPrivacyPolicy = 200;
  }
  
  public IncidentReportArgs(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mAll);
    int i = this.mSections.size();
    paramParcel.writeInt(i);
    for (paramInt = 0; paramInt < i; paramInt++)
      paramParcel.writeInt(this.mSections.get(paramInt)); 
    i = this.mHeaders.size();
    paramParcel.writeInt(i);
    for (paramInt = 0; paramInt < i; paramInt++)
      paramParcel.writeByteArray(this.mHeaders.get(paramInt)); 
    paramParcel.writeInt(this.mPrivacyPolicy);
    paramParcel.writeString(this.mReceiverPkg);
    paramParcel.writeString(this.mReceiverCls);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    boolean bool;
    if (paramParcel.readInt() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mAll = bool;
    this.mSections.clear();
    int i = paramParcel.readInt();
    byte b;
    for (b = 0; b < i; b++)
      this.mSections.add(paramParcel.readInt()); 
    this.mHeaders.clear();
    i = paramParcel.readInt();
    for (b = 0; b < i; b++)
      this.mHeaders.add(paramParcel.createByteArray()); 
    this.mPrivacyPolicy = paramParcel.readInt();
    this.mReceiverPkg = paramParcel.readString();
    this.mReceiverCls = paramParcel.readString();
  }
  
  public static final Parcelable.Creator<IncidentReportArgs> CREATOR = new Parcelable.Creator<IncidentReportArgs>() {
      public IncidentReportArgs createFromParcel(Parcel param1Parcel) {
        return new IncidentReportArgs(param1Parcel);
      }
      
      public IncidentReportArgs[] newArray(int param1Int) {
        return new IncidentReportArgs[param1Int];
      }
    };
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("Incident(");
    if (this.mAll) {
      stringBuilder.append("all");
    } else {
      int i = this.mSections.size();
      if (i > 0)
        stringBuilder.append(this.mSections.get(0)); 
      for (byte b = 1; b < i; b++) {
        stringBuilder.append(" ");
        stringBuilder.append(this.mSections.get(b));
      } 
    } 
    stringBuilder.append(", ");
    stringBuilder.append(this.mHeaders.size());
    stringBuilder.append(" headers), ");
    stringBuilder.append("privacy: ");
    stringBuilder.append(this.mPrivacyPolicy);
    stringBuilder.append("receiver pkg: ");
    stringBuilder.append(this.mReceiverPkg);
    stringBuilder.append("receiver cls: ");
    stringBuilder.append(this.mReceiverCls);
    return stringBuilder.toString();
  }
  
  public void setAll(boolean paramBoolean) {
    this.mAll = paramBoolean;
    if (paramBoolean)
      this.mSections.clear(); 
  }
  
  public void setPrivacyPolicy(int paramInt) {
    if (paramInt != 0 && paramInt != 100 && paramInt != 200) {
      this.mPrivacyPolicy = 200;
    } else {
      this.mPrivacyPolicy = paramInt;
    } 
  }
  
  public void addSection(int paramInt) {
    if (!this.mAll && paramInt > 1)
      this.mSections.add(paramInt); 
  }
  
  public boolean isAll() {
    return this.mAll;
  }
  
  public boolean containsSection(int paramInt) {
    return (this.mAll || this.mSections.indexOf(paramInt) >= 0);
  }
  
  public int sectionCount() {
    return this.mSections.size();
  }
  
  public void addHeader(byte[] paramArrayOfbyte) {
    this.mHeaders.add(paramArrayOfbyte);
  }
}
