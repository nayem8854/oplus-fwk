package android.os;

import android.util.Log;

public final class OplusFlashLightManager {
  private static final boolean DEBUG = true;
  
  public static final String SERVICE_NAME = "OPPO";
  
  private static final String TAG = "OplusFlashLightManager";
  
  private static OplusFlashLightManager mInstance = null;
  
  private static IOplusService sService;
  
  private OplusFlashLightManager() {
    boolean bool;
    sService = IOplusService.Stub.asInterface(ServiceManager.getService("OPPO"));
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("get service res:");
    if (sService != null) {
      bool = true;
    } else {
      bool = false;
    } 
    stringBuilder.append(bool);
    Log.d("OplusFlashLightManager", stringBuilder.toString());
  }
  
  public static OplusFlashLightManager getOplusFlashLightManager() {
    if (mInstance == null)
      mInstance = new OplusFlashLightManager(); 
    return mInstance;
  }
  
  public boolean openFlashLight() {
    boolean bool = false;
    try {
      if (sService != null)
        bool = sService.openFlashLight(); 
      return bool;
    } catch (RemoteException remoteException) {
      Log.e("OplusFlashLightManager", "openFlashLight failed.", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean closeFlashLight() {
    boolean bool = false;
    try {
      if (sService != null)
        bool = sService.closeFlashLight(); 
      return bool;
    } catch (RemoteException remoteException) {
      Log.e("OplusFlashLightManager", "closeFlashLight failed.", (Throwable)remoteException);
      return false;
    } 
  }
  
  public String getFlashLightState() {
    try {
      String str;
      if (sService != null) {
        str = sService.getFlashLightState();
      } else {
        str = "";
      } 
      return str;
    } catch (RemoteException remoteException) {
      Log.e("OplusFlashLightManager", "getFlashLightState failed.", (Throwable)remoteException);
      return null;
    } 
  }
}
