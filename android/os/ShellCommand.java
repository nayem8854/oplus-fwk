package android.os;

import java.io.FileDescriptor;
import java.io.PrintWriter;

public abstract class ShellCommand extends BasicShellCommandHandler {
  private ResultReceiver mResultReceiver;
  
  private ShellCallback mShellCallback;
  
  public int exec(Binder paramBinder, FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, FileDescriptor paramFileDescriptor3, String[] paramArrayOfString, ShellCallback paramShellCallback, ResultReceiver paramResultReceiver) {
    this.mShellCallback = paramShellCallback;
    this.mResultReceiver = paramResultReceiver;
    int i = exec(paramBinder, paramFileDescriptor1, paramFileDescriptor2, paramFileDescriptor3, paramArrayOfString);
    ResultReceiver resultReceiver = this.mResultReceiver;
    if (resultReceiver != null)
      resultReceiver.send(i, null); 
    return i;
  }
  
  public ResultReceiver adoptResultReceiver() {
    ResultReceiver resultReceiver = this.mResultReceiver;
    this.mResultReceiver = null;
    return resultReceiver;
  }
  
  public ParcelFileDescriptor openFileForSystem(String paramString1, String paramString2) {
    try {
      ParcelFileDescriptor parcelFileDescriptor = getShellCallback().openFile(paramString1, "u:r:system_server:s0", paramString2);
      if (parcelFileDescriptor != null)
        return parcelFileDescriptor; 
    } catch (RuntimeException runtimeException) {
      PrintWriter printWriter1 = getErrPrintWriter();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Failure opening file: ");
      stringBuilder1.append(runtimeException.getMessage());
      printWriter1.println(stringBuilder1.toString());
    } 
    PrintWriter printWriter = getErrPrintWriter();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Error: Unable to open file: ");
    stringBuilder.append(paramString1);
    printWriter.println(stringBuilder.toString());
    if (paramString1 == null || !paramString1.startsWith("/data/local/tmp/")) {
      PrintWriter printWriter1 = getErrPrintWriter();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Consider using a file under ");
      stringBuilder1.append("/data/local/tmp/");
      printWriter1.println(stringBuilder1.toString());
    } 
    return null;
  }
  
  public int handleDefaultCommands(String paramString) {
    String[] arrayOfString;
    if ("dump".equals(paramString)) {
      arrayOfString = new String[(getAllArgs()).length - 1];
      System.arraycopy(getAllArgs(), 1, arrayOfString, 0, (getAllArgs()).length - 1);
      getTarget().doDump(getOutFileDescriptor(), getOutPrintWriter(), arrayOfString);
      return 0;
    } 
    return super.handleDefaultCommands((String)arrayOfString);
  }
  
  public String peekNextArg() {
    return super.peekNextArg();
  }
  
  public ShellCallback getShellCallback() {
    return this.mShellCallback;
  }
}
