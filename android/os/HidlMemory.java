package android.os;

import android.annotation.SystemApi;
import java.io.Closeable;
import java.io.IOException;

@SystemApi
public class HidlMemory implements Closeable {
  private NativeHandle mHandle;
  
  private final String mName;
  
  private long mNativeContext;
  
  private final long mSize;
  
  public HidlMemory(String paramString, long paramLong, NativeHandle paramNativeHandle) {
    this.mName = paramString;
    this.mSize = paramLong;
    this.mHandle = paramNativeHandle;
  }
  
  public HidlMemory dup() throws IOException {
    String str = this.mName;
    long l = this.mSize;
    NativeHandle nativeHandle = this.mHandle;
    if (nativeHandle != null) {
      nativeHandle = nativeHandle.dup();
    } else {
      nativeHandle = null;
    } 
    return new HidlMemory(str, l, nativeHandle);
  }
  
  public void close() throws IOException {
    NativeHandle nativeHandle = this.mHandle;
    if (nativeHandle != null)
      nativeHandle.close(); 
  }
  
  public NativeHandle releaseHandle() {
    NativeHandle nativeHandle = this.mHandle;
    this.mHandle = null;
    return nativeHandle;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public long getSize() {
    return this.mSize;
  }
  
  public NativeHandle getHandle() {
    return this.mHandle;
  }
  
  protected void finalize() {
    Exception exception;
    try {
      close();
      nativeFinalize();
      return;
    } catch (IOException null) {
      RuntimeException runtimeException = new RuntimeException();
      this(exception);
      throw runtimeException;
    } finally {}
    nativeFinalize();
    throw exception;
  }
  
  private native void nativeFinalize();
}
