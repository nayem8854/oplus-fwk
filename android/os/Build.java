package android.os;

import android.annotation.SystemApi;
import android.app.ActivityThread;
import android.app.Application;
import android.sysprop.TelephonyProperties;
import android.text.TextUtils;
import android.util.Slog;
import dalvik.system.VMRuntime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Build {
  public static final String BOARD;
  
  public static final String BOOTLOADER;
  
  public static final String BRAND;
  
  @Deprecated
  public static final String CPU_ABI;
  
  @Deprecated
  public static final String CPU_ABI2;
  
  public static final String DEVICE;
  
  public static final String DISPLAY;
  
  public static final String FINGERPRINT;
  
  public static final String HARDWARE;
  
  public static final String HOST;
  
  static {
    String[] arrayOfString;
  }
  
  public static final String ID = getString("ro.build.id");
  
  public static final boolean IS_CONTAINER;
  
  public static final boolean IS_DEBUGGABLE;
  
  public static final boolean IS_EMULATOR;
  
  public static final boolean IS_ENG;
  
  public static final boolean IS_TREBLE_ENABLED;
  
  public static final boolean IS_USER;
  
  public static final boolean IS_USERDEBUG;
  
  public static final String MANUFACTURER;
  
  public static final String MODEL;
  
  @SystemApi
  public static final boolean PERMISSIONS_REVIEW_REQUIRED = true;
  
  public static final String PRODUCT;
  
  @Deprecated
  public static final String RADIO;
  
  @Deprecated
  public static final String SERIAL;
  
  public static final String[] SUPPORTED_32_BIT_ABIS;
  
  public static final String[] SUPPORTED_64_BIT_ABIS;
  
  public static final String[] SUPPORTED_ABIS;
  
  private static final String TAG = "Build";
  
  public static final String TAGS;
  
  public static final long TIME;
  
  public static final String TYPE;
  
  public static final String UNKNOWN = "unknown";
  
  public static final String USER;
  
  static {
    DISPLAY = getString("ro.build.display.id");
    PRODUCT = getString("ro.product.name");
    DEVICE = getString("ro.product.device");
    BOARD = getString("ro.product.board");
    MANUFACTURER = getString("ro.product.manufacturer");
    BRAND = getString("ro.product.brand");
    MODEL = getString("ro.product.model");
    BOOTLOADER = getString("ro.bootloader");
    List<?> list = TelephonyProperties.baseband_version();
    RADIO = joinListOrElse(list, "unknown");
    HARDWARE = getString("ro.hardware");
    IS_EMULATOR = getString("ro.kernel.qemu").equals("1");
    SERIAL = getString("no.such.thing");
    SUPPORTED_ABIS = getStringList("ro.product.cpu.abilist", ",");
    SUPPORTED_32_BIT_ABIS = getStringList("ro.product.cpu.abilist32", ",");
    SUPPORTED_64_BIT_ABIS = getStringList("ro.product.cpu.abilist64", ",");
    if (VMRuntime.getRuntime().is64Bit()) {
      arrayOfString = SUPPORTED_64_BIT_ABIS;
    } else {
      arrayOfString = SUPPORTED_32_BIT_ABIS;
    } 
    CPU_ABI = arrayOfString[0];
    int i = arrayOfString.length;
    boolean bool = true;
    if (i > 1) {
      CPU_ABI2 = arrayOfString[1];
    } else {
      CPU_ABI2 = "";
    } 
    TYPE = getString("ro.build.type");
    TAGS = getString("ro.build.tags");
    FINGERPRINT = deriveFingerprint();
    IS_TREBLE_ENABLED = SystemProperties.getBoolean("ro.treble.enabled", false);
    TIME = getLong("ro.build.date.utc") * 1000L;
    USER = getString("ro.build.user");
    HOST = getString("ro.build.host");
    if (SystemProperties.getInt("ro.debuggable", 0) != 1)
      bool = false; 
    IS_DEBUGGABLE = bool;
    IS_ENG = "eng".equals(TYPE);
    IS_USERDEBUG = "userdebug".equals(TYPE);
    IS_USER = "user".equals(TYPE);
    IS_CONTAINER = SystemProperties.getBoolean("ro.boot.container", false);
  }
  
  public static String getSerial() {
    IDeviceIdentifiersPolicyService iDeviceIdentifiersPolicyService = IDeviceIdentifiersPolicyService.Stub.asInterface(ServiceManager.getService("device_identifiers"));
    try {
      Application application = ActivityThread.currentApplication();
      if (application != null) {
        String str = application.getPackageName();
      } else {
        application = null;
      } 
      return iDeviceIdentifiersPolicyService.getSerialForPackage((String)application, null);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return "unknown";
    } 
  }
  
  public static boolean is64BitAbi(String paramString) {
    return VMRuntime.is64BitAbi(paramString);
  }
  
  public static class VERSION {
    public static final String[] ACTIVE_CODENAMES;
    
    private static final String[] ALL_CODENAMES;
    
    public static final String BASE_OS = SystemProperties.get("ro.build.version.base_os", "");
    
    public static final String CODENAME;
    
    public static final int FIRST_SDK_INT;
    
    public static final String INCREMENTAL = Build.getString("ro.build.version.incremental");
    
    public static final int MIN_SUPPORTED_TARGET_SDK_INT;
    
    @SystemApi
    public static final String PREVIEW_SDK_FINGERPRINT;
    
    public static final int PREVIEW_SDK_INT;
    
    public static final String RELEASE = Build.getString("ro.build.version.release");
    
    public static final String RELEASE_OR_CODENAME = Build.getString("ro.build.version.release_or_codename");
    
    public static final int RESOURCES_SDK_INT;
    
    @Deprecated
    public static final String SDK;
    
    public static final int SDK_INT;
    
    public static final String SECURITY_PATCH = SystemProperties.get("ro.build.version.security_patch", "");
    
    static {
      SDK = Build.getString("ro.build.version.sdk");
      SDK_INT = SystemProperties.getInt("ro.build.version.sdk", 0);
      FIRST_SDK_INT = SystemProperties.getInt("ro.product.first_api_level", 0);
      PREVIEW_SDK_INT = SystemProperties.getInt("ro.build.version.preview_sdk", 0);
      PREVIEW_SDK_FINGERPRINT = SystemProperties.get("ro.build.version.preview_sdk_fingerprint", "REL");
      CODENAME = Build.getString("ro.build.version.codename");
      String[] arrayOfString = Build.getStringList("ro.build.version.all_codenames", ",");
      if ("REL".equals(arrayOfString[0])) {
        arrayOfString = new String[0];
      } else {
        arrayOfString = ALL_CODENAMES;
      } 
      ACTIVE_CODENAMES = arrayOfString;
      RESOURCES_SDK_INT = SDK_INT + arrayOfString.length;
      MIN_SUPPORTED_TARGET_SDK_INT = SystemProperties.getInt("ro.build.version.min_supported_target_sdk", 0);
    }
  }
  
  public static class VERSION_CODES {
    public static final int BASE = 1;
    
    public static final int BASE_1_1 = 2;
    
    public static final int CUPCAKE = 3;
    
    public static final int CUR_DEVELOPMENT = 10000;
    
    public static final int DONUT = 4;
    
    public static final int ECLAIR = 5;
    
    public static final int ECLAIR_0_1 = 6;
    
    public static final int ECLAIR_MR1 = 7;
    
    public static final int FROYO = 8;
    
    public static final int GINGERBREAD = 9;
    
    public static final int GINGERBREAD_MR1 = 10;
    
    public static final int HONEYCOMB = 11;
    
    public static final int HONEYCOMB_MR1 = 12;
    
    public static final int HONEYCOMB_MR2 = 13;
    
    public static final int ICE_CREAM_SANDWICH = 14;
    
    public static final int ICE_CREAM_SANDWICH_MR1 = 15;
    
    public static final int JELLY_BEAN = 16;
    
    public static final int JELLY_BEAN_MR1 = 17;
    
    public static final int JELLY_BEAN_MR2 = 18;
    
    public static final int KITKAT = 19;
    
    public static final int KITKAT_WATCH = 20;
    
    public static final int L = 21;
    
    public static final int LOLLIPOP = 21;
    
    public static final int LOLLIPOP_MR1 = 22;
    
    public static final int M = 23;
    
    public static final int N = 24;
    
    public static final int N_MR1 = 25;
    
    public static final int O = 26;
    
    public static final int O_MR1 = 27;
    
    public static final int P = 28;
    
    public static final int Q = 29;
    
    public static final int R = 30;
  }
  
  private static String deriveFingerprint() {
    String str1 = SystemProperties.get("ro.build.fingerprint");
    String str2 = str1;
    if (TextUtils.isEmpty(str1)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getString("ro.product.brand"));
      stringBuilder.append('/');
      stringBuilder.append(getString("ro.product.name"));
      stringBuilder.append('/');
      stringBuilder.append(getString("ro.product.device"));
      stringBuilder.append(':');
      stringBuilder.append(getString("ro.build.version.release"));
      stringBuilder.append('/');
      stringBuilder.append(getString("ro.build.id"));
      stringBuilder.append('/');
      stringBuilder.append(getString("ro.build.version.incremental"));
      stringBuilder.append(':');
      stringBuilder.append(getString("ro.build.type"));
      stringBuilder.append('/');
      stringBuilder.append(getString("ro.build.tags"));
      str2 = stringBuilder.toString();
    } 
    return str2;
  }
  
  public static void ensureFingerprintProperty() {
    if (TextUtils.isEmpty(SystemProperties.get("ro.build.fingerprint")))
      try {
        SystemProperties.set("ro.build.fingerprint", FINGERPRINT);
      } catch (IllegalArgumentException illegalArgumentException) {
        Slog.e("Build", "Failed to set fingerprint property", illegalArgumentException);
      }  
  }
  
  public static boolean isBuildConsistent() {
    boolean bool = IS_ENG;
    boolean bool1 = true;
    if (bool)
      return true; 
    if (IS_TREBLE_ENABLED) {
      int i = VintfObject.verifyWithoutAvb();
      if (i != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Vendor interface is incompatible, error=");
        stringBuilder.append(String.valueOf(i));
        String str = stringBuilder.toString();
        Slog.e("Build", str);
      } 
      if (i != 0)
        bool1 = false; 
      return bool1;
    } 
    String str1 = SystemProperties.get("ro.system.build.fingerprint");
    String str2 = SystemProperties.get("ro.vendor.build.fingerprint");
    SystemProperties.get("ro.bootimage.build.fingerprint");
    SystemProperties.get("ro.build.expect.bootloader");
    SystemProperties.get("ro.bootloader");
    SystemProperties.get("ro.build.expect.baseband");
    List<?> list = TelephonyProperties.baseband_version();
    joinListOrElse(list, "");
    if (TextUtils.isEmpty(str1)) {
      Slog.e("Build", "Required ro.system.build.fingerprint is empty!");
      return false;
    } 
    if (!TextUtils.isEmpty(str2) && !Objects.equals(str1, str2)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Mismatched fingerprints; system reported ");
      stringBuilder.append(str1);
      stringBuilder.append(" but vendor reported ");
      stringBuilder.append(str2);
      Slog.e("Build", stringBuilder.toString());
      return false;
    } 
    return true;
  }
  
  public static class Partition {
    public static final String PARTITION_NAME_SYSTEM = "system";
    
    private final String mFingerprint;
    
    private final String mName;
    
    private final long mTimeMs;
    
    private Partition(String param1String1, String param1String2, long param1Long) {
      this.mName = param1String1;
      this.mFingerprint = param1String2;
      this.mTimeMs = param1Long;
    }
    
    public String getName() {
      return this.mName;
    }
    
    public String getFingerprint() {
      return this.mFingerprint;
    }
    
    public long getBuildTimeMillis() {
      return this.mTimeMs;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof Partition;
      boolean bool1 = false;
      if (!bool)
        return false; 
      Partition partition = (Partition)param1Object;
      if (this.mName.equals(partition.mName)) {
        param1Object = this.mFingerprint;
        String str = partition.mFingerprint;
        if (param1Object.equals(str) && this.mTimeMs == partition.mTimeMs)
          bool1 = true; 
      } 
      return bool1;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { this.mName, this.mFingerprint, Long.valueOf(this.mTimeMs) });
    }
  }
  
  public static List<Partition> getFingerprintedPartitions() {
    ArrayList<Partition> arrayList = new ArrayList();
    String[] arrayOfString = new String[6];
    arrayOfString[0] = "bootimage";
    arrayOfString[1] = "odm";
    arrayOfString[2] = "product";
    arrayOfString[3] = "system_ext";
    arrayOfString[4] = "system";
    arrayOfString[5] = "vendor";
    int i;
    byte b;
    for (i = arrayOfString.length, b = 0; b < i; ) {
      String str1 = arrayOfString[b];
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ro.");
      stringBuilder.append(str1);
      stringBuilder.append(".build.fingerprint");
      String str2 = SystemProperties.get(stringBuilder.toString());
      if (!TextUtils.isEmpty(str2)) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("ro.");
        stringBuilder1.append(str1);
        stringBuilder1.append(".build.date.utc");
        long l = getLong(stringBuilder1.toString());
        arrayList.add(new Partition(str1, str2, l * 1000L));
      } 
      b++;
    } 
    return arrayList;
  }
  
  public static String getRadioVersion() {
    return joinListOrElse(TelephonyProperties.baseband_version(), null);
  }
  
  private static String getString(String paramString) {
    return SystemProperties.get(paramString, "unknown");
  }
  
  private static String[] getStringList(String paramString1, String paramString2) {
    paramString1 = SystemProperties.get(paramString1);
    if (paramString1.isEmpty())
      return new String[0]; 
    return paramString1.split(paramString2);
  }
  
  private static long getLong(String paramString) {
    try {
      return Long.parseLong(SystemProperties.get(paramString));
    } catch (NumberFormatException numberFormatException) {
      return -1L;
    } 
  }
  
  private static <T> String joinListOrElse(List<T> paramList, String paramString) {
    Stream<CharSequence> stream = paramList.stream().map((Function)_$$Lambda$Build$WrC6eL7oW2Zm9UDTcXXKr0DnOMw.INSTANCE);
    String str = stream.collect(Collectors.joining(","));
    if (str.isEmpty())
      str = paramString; 
    return str;
  }
}
