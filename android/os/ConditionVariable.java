package android.os;

public class ConditionVariable {
  private volatile boolean mCondition;
  
  public ConditionVariable() {
    this.mCondition = false;
  }
  
  public ConditionVariable(boolean paramBoolean) {
    this.mCondition = paramBoolean;
  }
  
  public void open() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mCondition : Z
    //   6: istore_1
    //   7: aload_0
    //   8: iconst_1
    //   9: putfield mCondition : Z
    //   12: iload_1
    //   13: ifne -> 20
    //   16: aload_0
    //   17: invokevirtual notifyAll : ()V
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore_2
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_2
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #64	-> 0
    //   #65	-> 2
    //   #66	-> 7
    //   #67	-> 12
    //   #68	-> 16
    //   #70	-> 20
    //   #71	-> 22
    //   #70	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	7	23	finally
    //   7	12	23	finally
    //   16	20	23	finally
    //   20	22	23	finally
    //   24	26	23	finally
  }
  
  public void close() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_0
    //   4: putfield mCondition : Z
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_1
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_1
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #81	-> 0
    //   #82	-> 2
    //   #83	-> 7
    //   #84	-> 9
    //   #83	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
    //   7	9	10	finally
    //   11	13	10	finally
  }
  
  public void block() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mCondition : Z
    //   6: istore_1
    //   7: iload_1
    //   8: ifne -> 22
    //   11: aload_0
    //   12: invokevirtual wait : ()V
    //   15: goto -> 2
    //   18: astore_2
    //   19: goto -> 15
    //   22: aload_0
    //   23: monitorexit
    //   24: return
    //   25: astore_2
    //   26: aload_0
    //   27: monitorexit
    //   28: aload_2
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #94	-> 0
    //   #95	-> 2
    //   #97	-> 11
    //   #100	-> 15
    //   #99	-> 18
    //   #102	-> 22
    //   #103	-> 24
    //   #102	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	7	25	finally
    //   11	15	18	java/lang/InterruptedException
    //   11	15	25	finally
    //   22	24	25	finally
    //   26	28	25	finally
  }
  
  public boolean block(long paramLong) {
    // Byte code:
    //   0: lload_1
    //   1: lconst_0
    //   2: lcmp
    //   3: ifeq -> 75
    //   6: aload_0
    //   7: monitorenter
    //   8: invokestatic elapsedRealtime : ()J
    //   11: lstore_3
    //   12: lload_3
    //   13: lload_1
    //   14: ladd
    //   15: lstore #5
    //   17: lload_3
    //   18: lstore_1
    //   19: aload_0
    //   20: getfield mCondition : Z
    //   23: istore #7
    //   25: iload #7
    //   27: ifne -> 57
    //   30: lload_1
    //   31: lload #5
    //   33: lcmp
    //   34: ifge -> 57
    //   37: aload_0
    //   38: lload #5
    //   40: lload_1
    //   41: lsub
    //   42: invokevirtual wait : (J)V
    //   45: goto -> 50
    //   48: astore #8
    //   50: invokestatic elapsedRealtime : ()J
    //   53: lstore_1
    //   54: goto -> 19
    //   57: aload_0
    //   58: getfield mCondition : Z
    //   61: istore #7
    //   63: aload_0
    //   64: monitorexit
    //   65: iload #7
    //   67: ireturn
    //   68: astore #8
    //   70: aload_0
    //   71: monitorexit
    //   72: aload #8
    //   74: athrow
    //   75: aload_0
    //   76: invokevirtual block : ()V
    //   79: iconst_1
    //   80: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #122	-> 0
    //   #123	-> 6
    //   #124	-> 8
    //   #125	-> 12
    //   #126	-> 19
    //   #128	-> 37
    //   #131	-> 45
    //   #130	-> 48
    //   #132	-> 50
    //   #134	-> 57
    //   #135	-> 68
    //   #137	-> 75
    //   #138	-> 79
    // Exception table:
    //   from	to	target	type
    //   8	12	68	finally
    //   19	25	68	finally
    //   37	45	48	java/lang/InterruptedException
    //   37	45	68	finally
    //   50	54	68	finally
    //   57	65	68	finally
    //   70	72	68	finally
  }
}
