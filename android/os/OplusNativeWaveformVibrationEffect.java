package android.os;

import java.util.Arrays;
import java.util.Objects;

public class OplusNativeWaveformVibrationEffect extends VibrationEffect implements Parcelable {
  public OplusNativeWaveformVibrationEffect(Parcel paramParcel) {
    this(paramParcel.createLongArray(), paramParcel.createIntArray(), paramParcel.readInt());
    this.mEffectStrength = paramParcel.readInt();
  }
  
  public OplusNativeWaveformVibrationEffect(long[] paramArrayOflong, int[] paramArrayOfint, int paramInt) {
    long[] arrayOfLong = new long[paramArrayOflong.length];
    System.arraycopy(paramArrayOflong, 0, arrayOfLong, 0, paramArrayOflong.length);
    int[] arrayOfInt = new int[paramArrayOfint.length];
    System.arraycopy(paramArrayOfint, 0, arrayOfInt, 0, paramArrayOfint.length);
    this.mRepeat = paramInt;
    this.mEffectStrength = 2;
  }
  
  public long[] getTimings() {
    return this.mTimings;
  }
  
  public int[] getWaveformIds() {
    return this.mWaveformIds;
  }
  
  public int getRepeatIndex() {
    return this.mRepeat;
  }
  
  public void setEffectStrength(int paramInt) {
    if (isValidEffectStrength(paramInt)) {
      this.mEffectStrength = paramInt;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid effect strength: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int getEffectStrength() {
    return this.mEffectStrength;
  }
  
  public long getDuration() {
    if (this.mRepeat >= 0)
      return Long.MAX_VALUE; 
    long l = 0L;
    for (long l1 : this.mTimings)
      l += l1; 
    return l;
  }
  
  private static boolean isValidEffectStrength(int paramInt) {
    if (paramInt != 0 && paramInt != 1 && paramInt != 2)
      return false; 
    return true;
  }
  
  public void validate() {
    long[] arrayOfLong = this.mTimings;
    if (arrayOfLong.length == this.mWaveformIds.length) {
      if (hasNonZeroEntry(arrayOfLong)) {
        int i, j;
        for (arrayOfLong = this.mTimings, i = arrayOfLong.length, j = 0; j < i; ) {
          long l = arrayOfLong[j];
          if (l >= 0L) {
            j++;
            continue;
          } 
          StringBuilder stringBuilder3 = new StringBuilder();
          stringBuilder3.append("timings must all be >= 0 (timings=");
          arrayOfLong = this.mTimings;
          stringBuilder3.append(Arrays.toString(arrayOfLong));
          stringBuilder3.append(")");
          throw new IllegalArgumentException(stringBuilder3.toString());
        } 
        j = this.mRepeat;
        if (j >= -1 && j < this.mTimings.length)
          return; 
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("repeat index must be within the bounds of the timings array (timings.length=");
        stringBuilder2.append(this.mTimings.length);
        stringBuilder2.append(", index=");
        stringBuilder2.append(this.mRepeat);
        stringBuilder2.append(")");
        throw new IllegalArgumentException(stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("at least one timing must be non-zero (timings=");
      arrayOfLong = this.mTimings;
      stringBuilder1.append(Arrays.toString(arrayOfLong));
      stringBuilder1.append(")");
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("timing and waveform arrays must be of equal length (timings.length=");
    stringBuilder.append(this.mTimings.length);
    stringBuilder.append(", waveforms.length=");
    stringBuilder.append(this.mWaveformIds.length);
    stringBuilder.append(")");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mEffectStrength == ((OplusNativeWaveformVibrationEffect)paramObject).mEffectStrength && this.mRepeat == ((OplusNativeWaveformVibrationEffect)paramObject).mRepeat) {
      int[] arrayOfInt1 = this.mWaveformIds, arrayOfInt2 = ((OplusNativeWaveformVibrationEffect)paramObject).mWaveformIds;
      if (Arrays.equals(arrayOfInt1, arrayOfInt2)) {
        long[] arrayOfLong = this.mTimings;
        paramObject = ((OplusNativeWaveformVibrationEffect)paramObject).mTimings;
        if (Arrays.equals(arrayOfLong, (long[])paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hash(new Object[] { Integer.valueOf(this.mEffectStrength), Integer.valueOf(this.mRepeat) });
    int j = Arrays.hashCode(this.mWaveformIds);
    int k = Arrays.hashCode(this.mTimings);
    return (i * 31 + j) * 31 + k;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OplusNativeWaveformVibrationEffect{mTimings=");
    stringBuilder.append(Arrays.toString(this.mTimings));
    stringBuilder.append(", mWaveformIds=");
    int[] arrayOfInt = this.mWaveformIds;
    stringBuilder.append(Arrays.toString(arrayOfInt));
    stringBuilder.append(", mRepeat=");
    stringBuilder.append(this.mRepeat);
    stringBuilder.append(", mEffectStrength=");
    stringBuilder.append(this.mEffectStrength);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(4);
    paramParcel.writeLongArray(this.mTimings);
    paramParcel.writeIntArray(this.mWaveformIds);
    paramParcel.writeInt(this.mRepeat);
    paramParcel.writeInt(this.mEffectStrength);
  }
  
  private static boolean hasNonZeroEntry(long[] paramArrayOflong) {
    int i;
    byte b;
    for (i = paramArrayOflong.length, b = 0; b < i; ) {
      long l = paramArrayOflong[b];
      if (l != 0L)
        return true; 
      b++;
    } 
    return false;
  }
  
  public static final Parcelable.Creator<OplusNativeWaveformVibrationEffect> CREATOR = (Parcelable.Creator<OplusNativeWaveformVibrationEffect>)new Object();
  
  public static final int PARCEL_TOKEN_OPPPO_NATIVE_WAVEFORM = 4;
  
  private int mEffectStrength;
  
  private final int mRepeat;
  
  private final long[] mTimings;
  
  private final int[] mWaveformIds;
}
