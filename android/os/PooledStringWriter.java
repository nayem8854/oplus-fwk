package android.os;

import java.util.HashMap;

public class PooledStringWriter {
  private int mNext;
  
  private final Parcel mOut;
  
  private final HashMap<String, Integer> mPool;
  
  private int mStart;
  
  public PooledStringWriter(Parcel paramParcel) {
    this.mOut = paramParcel;
    this.mPool = new HashMap<>();
    this.mStart = paramParcel.dataPosition();
    paramParcel.writeInt(0);
  }
  
  public void writeString(String paramString) {
    Integer integer = this.mPool.get(paramString);
    if (integer != null) {
      this.mOut.writeInt(integer.intValue());
    } else {
      this.mPool.put(paramString, Integer.valueOf(this.mNext));
      this.mOut.writeInt(-(this.mNext + 1));
      this.mOut.writeString(paramString);
      this.mNext++;
    } 
  }
  
  public int getStringCount() {
    return this.mPool.size();
  }
  
  public void finish() {
    int i = this.mOut.dataPosition();
    this.mOut.setDataPosition(this.mStart);
    this.mOut.writeInt(this.mNext);
    this.mOut.setDataPosition(i);
  }
}
