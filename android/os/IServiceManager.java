package android.os;

public interface IServiceManager extends IInterface {
  public static final int DUMP_FLAG_PRIORITY_ALL = 15;
  
  public static final int DUMP_FLAG_PRIORITY_CRITICAL = 1;
  
  public static final int DUMP_FLAG_PRIORITY_DEFAULT = 8;
  
  public static final int DUMP_FLAG_PRIORITY_HIGH = 2;
  
  public static final int DUMP_FLAG_PRIORITY_NORMAL = 4;
  
  public static final int DUMP_FLAG_PROTO = 16;
  
  void addService(String paramString, IBinder paramIBinder, boolean paramBoolean, int paramInt) throws RemoteException;
  
  IBinder checkService(String paramString) throws RemoteException;
  
  IBinder getService(String paramString) throws RemoteException;
  
  boolean isDeclared(String paramString) throws RemoteException;
  
  String[] listServices(int paramInt) throws RemoteException;
  
  void registerClientCallback(String paramString, IBinder paramIBinder, IClientCallback paramIClientCallback) throws RemoteException;
  
  void registerForNotifications(String paramString, IServiceCallback paramIServiceCallback) throws RemoteException;
  
  void tryUnregisterService(String paramString, IBinder paramIBinder) throws RemoteException;
  
  void unregisterForNotifications(String paramString, IServiceCallback paramIServiceCallback) throws RemoteException;
  
  class Default implements IServiceManager {
    public IBinder getService(String param1String) throws RemoteException {
      return null;
    }
    
    public IBinder checkService(String param1String) throws RemoteException {
      return null;
    }
    
    public void addService(String param1String, IBinder param1IBinder, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public String[] listServices(int param1Int) throws RemoteException {
      return null;
    }
    
    public void registerForNotifications(String param1String, IServiceCallback param1IServiceCallback) throws RemoteException {}
    
    public void unregisterForNotifications(String param1String, IServiceCallback param1IServiceCallback) throws RemoteException {}
    
    public boolean isDeclared(String param1String) throws RemoteException {
      return false;
    }
    
    public void registerClientCallback(String param1String, IBinder param1IBinder, IClientCallback param1IClientCallback) throws RemoteException {}
    
    public void tryUnregisterService(String param1String, IBinder param1IBinder) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IServiceManager {
    private static final String DESCRIPTOR = "android.os.IServiceManager";
    
    static final int TRANSACTION_addService = 3;
    
    static final int TRANSACTION_checkService = 2;
    
    static final int TRANSACTION_getService = 1;
    
    static final int TRANSACTION_isDeclared = 7;
    
    static final int TRANSACTION_listServices = 4;
    
    static final int TRANSACTION_registerClientCallback = 8;
    
    static final int TRANSACTION_registerForNotifications = 5;
    
    static final int TRANSACTION_tryUnregisterService = 9;
    
    static final int TRANSACTION_unregisterForNotifications = 6;
    
    public Stub() {
      attachInterface(this, "android.os.IServiceManager");
    }
    
    public static IServiceManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IServiceManager");
      if (iInterface != null && iInterface instanceof IServiceManager)
        return (IServiceManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "tryUnregisterService";
        case 8:
          return "registerClientCallback";
        case 7:
          return "isDeclared";
        case 6:
          return "unregisterForNotifications";
        case 5:
          return "registerForNotifications";
        case 4:
          return "listServices";
        case 3:
          return "addService";
        case 2:
          return "checkService";
        case 1:
          break;
      } 
      return "getService";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        int i;
        IBinder iBinder3;
        IClientCallback iClientCallback;
        String str3;
        IServiceCallback iServiceCallback;
        String arrayOfString[], str2;
        IBinder iBinder2;
        String str5;
        IBinder iBinder4;
        String str4, str6;
        IBinder iBinder5;
        boolean bool1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.os.IServiceManager");
            str5 = param1Parcel1.readString();
            iBinder3 = param1Parcel1.readStrongBinder();
            tryUnregisterService(str5, iBinder3);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iBinder3.enforceInterface("android.os.IServiceManager");
            str6 = iBinder3.readString();
            iBinder4 = iBinder3.readStrongBinder();
            iClientCallback = IClientCallback.Stub.asInterface(iBinder3.readStrongBinder());
            registerClientCallback(str6, iBinder4, iClientCallback);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iClientCallback.enforceInterface("android.os.IServiceManager");
            str3 = iClientCallback.readString();
            bool = isDeclared(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 6:
            str3.enforceInterface("android.os.IServiceManager");
            str4 = str3.readString();
            iServiceCallback = IServiceCallback.Stub.asInterface(str3.readStrongBinder());
            unregisterForNotifications(str4, iServiceCallback);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iServiceCallback.enforceInterface("android.os.IServiceManager");
            str4 = iServiceCallback.readString();
            iServiceCallback = IServiceCallback.Stub.asInterface(iServiceCallback.readStrongBinder());
            registerForNotifications(str4, iServiceCallback);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iServiceCallback.enforceInterface("android.os.IServiceManager");
            i = iServiceCallback.readInt();
            arrayOfString = listServices(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 3:
            arrayOfString.enforceInterface("android.os.IServiceManager");
            str4 = arrayOfString.readString();
            iBinder5 = arrayOfString.readStrongBinder();
            if (arrayOfString.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            i = arrayOfString.readInt();
            addService(str4, iBinder5, bool1, i);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            arrayOfString.enforceInterface("android.os.IServiceManager");
            str2 = arrayOfString.readString();
            iBinder2 = checkService(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStrongBinder(iBinder2);
            return true;
          case 1:
            break;
        } 
        iBinder2.enforceInterface("android.os.IServiceManager");
        String str1 = iBinder2.readString();
        IBinder iBinder1 = getService(str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeStrongBinder(iBinder1);
        return true;
      } 
      param1Parcel2.writeString("android.os.IServiceManager");
      return true;
    }
    
    private static class Proxy implements IServiceManager {
      public static IServiceManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IServiceManager";
      }
      
      public IBinder getService(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IServiceManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IServiceManager.Stub.getDefaultImpl() != null)
            return IServiceManager.Stub.getDefaultImpl().getService(param2String); 
          parcel2.readException();
          return parcel2.readStrongBinder();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IBinder checkService(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IServiceManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IServiceManager.Stub.getDefaultImpl() != null)
            return IServiceManager.Stub.getDefaultImpl().checkService(param2String); 
          parcel2.readException();
          return parcel2.readStrongBinder();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addService(String param2String, IBinder param2IBinder, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.IServiceManager");
          parcel1.writeString(param2String);
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && IServiceManager.Stub.getDefaultImpl() != null) {
            IServiceManager.Stub.getDefaultImpl().addService(param2String, param2IBinder, param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] listServices(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IServiceManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IServiceManager.Stub.getDefaultImpl() != null)
            return IServiceManager.Stub.getDefaultImpl().listServices(param2Int); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerForNotifications(String param2String, IServiceCallback param2IServiceCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IServiceManager");
          parcel1.writeString(param2String);
          if (param2IServiceCallback != null) {
            iBinder = param2IServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IServiceManager.Stub.getDefaultImpl() != null) {
            IServiceManager.Stub.getDefaultImpl().registerForNotifications(param2String, param2IServiceCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterForNotifications(String param2String, IServiceCallback param2IServiceCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IServiceManager");
          parcel1.writeString(param2String);
          if (param2IServiceCallback != null) {
            iBinder = param2IServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IServiceManager.Stub.getDefaultImpl() != null) {
            IServiceManager.Stub.getDefaultImpl().unregisterForNotifications(param2String, param2IServiceCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDeclared(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IServiceManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IServiceManager.Stub.getDefaultImpl() != null) {
            bool1 = IServiceManager.Stub.getDefaultImpl().isDeclared(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerClientCallback(String param2String, IBinder param2IBinder, IClientCallback param2IClientCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IServiceManager");
          parcel1.writeString(param2String);
          parcel1.writeStrongBinder(param2IBinder);
          if (param2IClientCallback != null) {
            iBinder = param2IClientCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IServiceManager.Stub.getDefaultImpl() != null) {
            IServiceManager.Stub.getDefaultImpl().registerClientCallback(param2String, param2IBinder, param2IClientCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void tryUnregisterService(String param2String, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IServiceManager");
          parcel1.writeString(param2String);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IServiceManager.Stub.getDefaultImpl() != null) {
            IServiceManager.Stub.getDefaultImpl().tryUnregisterService(param2String, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IServiceManager param1IServiceManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IServiceManager != null) {
          Proxy.sDefaultImpl = param1IServiceManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IServiceManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
