package android.os;

import com.oplus.util.OplusBaseServiceManager;

public abstract class OplusBaseIPowerManager extends OplusBaseServiceManager {
  public OplusBaseIPowerManager() {
    super("power");
  }
  
  protected void init(IBinder paramIBinder) {}
}
