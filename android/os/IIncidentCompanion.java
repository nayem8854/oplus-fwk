package android.os;

import java.util.List;

public interface IIncidentCompanion extends IInterface {
  void approveReport(String paramString) throws RemoteException;
  
  void authorizeReport(int paramInt1, String paramString1, String paramString2, String paramString3, int paramInt2, IIncidentAuthListener paramIIncidentAuthListener) throws RemoteException;
  
  void cancelAuthorization(IIncidentAuthListener paramIIncidentAuthListener) throws RemoteException;
  
  void deleteAllIncidentReports(String paramString) throws RemoteException;
  
  void deleteIncidentReports(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  void denyReport(String paramString) throws RemoteException;
  
  IncidentManager.IncidentReport getIncidentReport(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  List<String> getIncidentReportList(String paramString1, String paramString2) throws RemoteException;
  
  List<String> getPendingReports() throws RemoteException;
  
  void sendReportReadyBroadcast(String paramString1, String paramString2) throws RemoteException;
  
  class Default implements IIncidentCompanion {
    public void authorizeReport(int param1Int1, String param1String1, String param1String2, String param1String3, int param1Int2, IIncidentAuthListener param1IIncidentAuthListener) throws RemoteException {}
    
    public void cancelAuthorization(IIncidentAuthListener param1IIncidentAuthListener) throws RemoteException {}
    
    public void sendReportReadyBroadcast(String param1String1, String param1String2) throws RemoteException {}
    
    public List<String> getPendingReports() throws RemoteException {
      return null;
    }
    
    public void approveReport(String param1String) throws RemoteException {}
    
    public void denyReport(String param1String) throws RemoteException {}
    
    public List<String> getIncidentReportList(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public IncidentManager.IncidentReport getIncidentReport(String param1String1, String param1String2, String param1String3) throws RemoteException {
      return null;
    }
    
    public void deleteIncidentReports(String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public void deleteAllIncidentReports(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IIncidentCompanion {
    private static final String DESCRIPTOR = "android.os.IIncidentCompanion";
    
    static final int TRANSACTION_approveReport = 5;
    
    static final int TRANSACTION_authorizeReport = 1;
    
    static final int TRANSACTION_cancelAuthorization = 2;
    
    static final int TRANSACTION_deleteAllIncidentReports = 10;
    
    static final int TRANSACTION_deleteIncidentReports = 9;
    
    static final int TRANSACTION_denyReport = 6;
    
    static final int TRANSACTION_getIncidentReport = 8;
    
    static final int TRANSACTION_getIncidentReportList = 7;
    
    static final int TRANSACTION_getPendingReports = 4;
    
    static final int TRANSACTION_sendReportReadyBroadcast = 3;
    
    public Stub() {
      attachInterface(this, "android.os.IIncidentCompanion");
    }
    
    public static IIncidentCompanion asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IIncidentCompanion");
      if (iInterface != null && iInterface instanceof IIncidentCompanion)
        return (IIncidentCompanion)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "deleteAllIncidentReports";
        case 9:
          return "deleteIncidentReports";
        case 8:
          return "getIncidentReport";
        case 7:
          return "getIncidentReportList";
        case 6:
          return "denyReport";
        case 5:
          return "approveReport";
        case 4:
          return "getPendingReports";
        case 3:
          return "sendReportReadyBroadcast";
        case 2:
          return "cancelAuthorization";
        case 1:
          break;
      } 
      return "authorizeReport";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str4;
        IncidentManager.IncidentReport incidentReport;
        String str3;
        List<String> list2;
        String str2;
        List<String> list1;
        String str1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.os.IIncidentCompanion");
            str4 = param1Parcel1.readString();
            deleteAllIncidentReports(str4);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str4.enforceInterface("android.os.IIncidentCompanion");
            str5 = str4.readString();
            str6 = str4.readString();
            str4 = str4.readString();
            deleteIncidentReports(str5, str6, str4);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str4.enforceInterface("android.os.IIncidentCompanion");
            str5 = str4.readString();
            str6 = str4.readString();
            str4 = str4.readString();
            incidentReport = getIncidentReport(str5, str6, str4);
            param1Parcel2.writeNoException();
            if (incidentReport != null) {
              param1Parcel2.writeInt(1);
              incidentReport.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 7:
            incidentReport.enforceInterface("android.os.IIncidentCompanion");
            str5 = incidentReport.readString();
            str3 = incidentReport.readString();
            list2 = getIncidentReportList(str5, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list2);
            return true;
          case 6:
            list2.enforceInterface("android.os.IIncidentCompanion");
            str2 = list2.readString();
            denyReport(str2);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str2.enforceInterface("android.os.IIncidentCompanion");
            str2 = str2.readString();
            approveReport(str2);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str2.enforceInterface("android.os.IIncidentCompanion");
            list1 = getPendingReports();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 3:
            list1.enforceInterface("android.os.IIncidentCompanion");
            str = list1.readString();
            str1 = list1.readString();
            sendReportReadyBroadcast(str, str1);
            return true;
          case 2:
            str1.enforceInterface("android.os.IIncidentCompanion");
            iIncidentAuthListener = IIncidentAuthListener.Stub.asInterface(str1.readStrongBinder());
            cancelAuthorization(iIncidentAuthListener);
            return true;
          case 1:
            break;
        } 
        iIncidentAuthListener.enforceInterface("android.os.IIncidentCompanion");
        param1Int1 = iIncidentAuthListener.readInt();
        String str6 = iIncidentAuthListener.readString();
        String str5 = iIncidentAuthListener.readString();
        str = iIncidentAuthListener.readString();
        param1Int2 = iIncidentAuthListener.readInt();
        IIncidentAuthListener iIncidentAuthListener = IIncidentAuthListener.Stub.asInterface(iIncidentAuthListener.readStrongBinder());
        authorizeReport(param1Int1, str6, str5, str, param1Int2, iIncidentAuthListener);
        return true;
      } 
      str.writeString("android.os.IIncidentCompanion");
      return true;
    }
    
    private static class Proxy implements IIncidentCompanion {
      public static IIncidentCompanion sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IIncidentCompanion";
      }
      
      public void authorizeReport(int param2Int1, String param2String1, String param2String2, String param2String3, int param2Int2, IIncidentAuthListener param2IIncidentAuthListener) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IIncidentCompanion");
          try {
            parcel.writeInt(param2Int1);
            try {
              parcel.writeString(param2String1);
              try {
                parcel.writeString(param2String2);
                try {
                  parcel.writeString(param2String3);
                  try {
                    IBinder iBinder;
                    parcel.writeInt(param2Int2);
                    if (param2IIncidentAuthListener != null) {
                      iBinder = param2IIncidentAuthListener.asBinder();
                    } else {
                      iBinder = null;
                    } 
                    parcel.writeStrongBinder(iBinder);
                    try {
                      boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
                      if (!bool && IIncidentCompanion.Stub.getDefaultImpl() != null) {
                        IIncidentCompanion.Stub.getDefaultImpl().authorizeReport(param2Int1, param2String1, param2String2, param2String3, param2Int2, param2IIncidentAuthListener);
                        parcel.recycle();
                        return;
                      } 
                      parcel.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2String1;
      }
      
      public void cancelAuthorization(IIncidentAuthListener param2IIncidentAuthListener) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.os.IIncidentCompanion");
          if (param2IIncidentAuthListener != null) {
            iBinder = param2IIncidentAuthListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IIncidentCompanion.Stub.getDefaultImpl() != null) {
            IIncidentCompanion.Stub.getDefaultImpl().cancelAuthorization(param2IIncidentAuthListener);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendReportReadyBroadcast(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IIncidentCompanion");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IIncidentCompanion.Stub.getDefaultImpl() != null) {
            IIncidentCompanion.Stub.getDefaultImpl().sendReportReadyBroadcast(param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public List<String> getPendingReports() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IIncidentCompanion");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IIncidentCompanion.Stub.getDefaultImpl() != null)
            return IIncidentCompanion.Stub.getDefaultImpl().getPendingReports(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void approveReport(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IIncidentCompanion");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IIncidentCompanion.Stub.getDefaultImpl() != null) {
            IIncidentCompanion.Stub.getDefaultImpl().approveReport(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void denyReport(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IIncidentCompanion");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IIncidentCompanion.Stub.getDefaultImpl() != null) {
            IIncidentCompanion.Stub.getDefaultImpl().denyReport(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getIncidentReportList(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IIncidentCompanion");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IIncidentCompanion.Stub.getDefaultImpl() != null)
            return IIncidentCompanion.Stub.getDefaultImpl().getIncidentReportList(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IncidentManager.IncidentReport getIncidentReport(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IIncidentCompanion");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IIncidentCompanion.Stub.getDefaultImpl() != null)
            return IIncidentCompanion.Stub.getDefaultImpl().getIncidentReport(param2String1, param2String2, param2String3); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            IncidentManager.IncidentReport incidentReport = IncidentManager.IncidentReport.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (IncidentManager.IncidentReport)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteIncidentReports(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IIncidentCompanion");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IIncidentCompanion.Stub.getDefaultImpl() != null) {
            IIncidentCompanion.Stub.getDefaultImpl().deleteIncidentReports(param2String1, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteAllIncidentReports(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IIncidentCompanion");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IIncidentCompanion.Stub.getDefaultImpl() != null) {
            IIncidentCompanion.Stub.getDefaultImpl().deleteAllIncidentReports(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IIncidentCompanion param1IIncidentCompanion) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IIncidentCompanion != null) {
          Proxy.sDefaultImpl = param1IIncidentCompanion;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IIncidentCompanion getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
