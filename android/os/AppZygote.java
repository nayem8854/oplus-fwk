package android.os;

import android.content.pm.ApplicationInfo;
import android.util.Log;
import dalvik.system.VMRuntime;

public class AppZygote {
  private static final String LOG_TAG = "AppZygote";
  
  private final ApplicationInfo mAppInfo;
  
  private final Object mLock = new Object();
  
  private ChildZygoteProcess mZygote;
  
  private final int mZygoteUid;
  
  private final int mZygoteUidGidMax;
  
  private final int mZygoteUidGidMin;
  
  public AppZygote(ApplicationInfo paramApplicationInfo, int paramInt1, int paramInt2, int paramInt3) {
    this.mAppInfo = paramApplicationInfo;
    this.mZygoteUid = paramInt1;
    this.mZygoteUidGidMin = paramInt2;
    this.mZygoteUidGidMax = paramInt3;
  }
  
  public ChildZygoteProcess getProcess() {
    synchronized (this.mLock) {
      if (this.mZygote != null)
        return this.mZygote; 
      connectToZygoteIfNeededLocked();
      return this.mZygote;
    } 
  }
  
  public void stopZygote() {
    synchronized (this.mLock) {
      stopZygoteLocked();
      return;
    } 
  }
  
  public ApplicationInfo getAppInfo() {
    return this.mAppInfo;
  }
  
  private void stopZygoteLocked() {
    ChildZygoteProcess childZygoteProcess = this.mZygote;
    if (childZygoteProcess != null) {
      childZygoteProcess.close();
      Process.killProcessGroup(this.mZygoteUid, this.mZygote.getPid());
      this.mZygote = null;
    } 
  }
  
  private void connectToZygoteIfNeededLocked() {
    String str;
    if (this.mAppInfo.primaryCpuAbi != null) {
      str = this.mAppInfo.primaryCpuAbi;
    } else {
      str = Build.SUPPORTED_ABIS[0];
    } 
    try {
      ZygoteProcess zygoteProcess = Process.ZYGOTE_PROCESS;
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(this.mAppInfo.processName);
      stringBuilder.append("_zygote");
      String str1 = stringBuilder.toString();
      int i = this.mZygoteUid, j = this.mZygoteUid;
      String str2 = VMRuntime.getInstructionSet(str);
      int k = this.mZygoteUidGidMin, m = this.mZygoteUidGidMax;
      this.mZygote = (ChildZygoteProcess)(zygoteProcess = zygoteProcess.startChildZygote("com.android.internal.os.AppZygoteInit", str1, i, j, null, 0, "app_zygote", str, str, str2, k, m));
      ZygoteProcess.waitForConnectionToZygote(zygoteProcess.getPrimarySocketAddress());
      Log.i("AppZygote", "Starting application preload.");
      this.mZygote.preloadApp(this.mAppInfo, str);
      Log.i("AppZygote", "Application preload done.");
    } catch (Exception exception) {
      Log.e("AppZygote", "Error connecting to app zygote", exception);
      stopZygoteLocked();
    } 
  }
}
