package android.os;

import com.android.internal.os.IResultReceiver;

public class ResultReceiver implements Parcelable {
  class MyRunnable implements Runnable {
    final int mResultCode;
    
    final Bundle mResultData;
    
    final ResultReceiver this$0;
    
    MyRunnable(int param1Int, Bundle param1Bundle) {
      this.mResultCode = param1Int;
      this.mResultData = param1Bundle;
    }
    
    public void run() {
      ResultReceiver.this.onReceiveResult(this.mResultCode, this.mResultData);
    }
  }
  
  class MyResultReceiver extends IResultReceiver.Stub {
    final ResultReceiver this$0;
    
    public void send(int param1Int, Bundle param1Bundle) {
      if (ResultReceiver.this.mHandler != null) {
        ResultReceiver.this.mHandler.post(new ResultReceiver.MyRunnable(param1Int, param1Bundle));
      } else {
        ResultReceiver.this.onReceiveResult(param1Int, param1Bundle);
      } 
    }
  }
  
  public ResultReceiver(Handler paramHandler) {
    this.mLocal = true;
    this.mHandler = paramHandler;
  }
  
  public void send(int paramInt, Bundle paramBundle) {
    if (this.mLocal) {
      Handler handler = this.mHandler;
      if (handler != null) {
        handler.post(new MyRunnable(paramInt, paramBundle));
      } else {
        onReceiveResult(paramInt, paramBundle);
      } 
      return;
    } 
    IResultReceiver iResultReceiver = this.mReceiver;
    if (iResultReceiver != null)
      try {
        iResultReceiver.send(paramInt, paramBundle);
      } catch (RemoteException remoteException) {} 
  }
  
  protected void onReceiveResult(int paramInt, Bundle paramBundle) {}
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mReceiver : Lcom/android/internal/os/IResultReceiver;
    //   6: ifnonnull -> 23
    //   9: new android/os/ResultReceiver$MyResultReceiver
    //   12: astore_3
    //   13: aload_3
    //   14: aload_0
    //   15: invokespecial <init> : (Landroid/os/ResultReceiver;)V
    //   18: aload_0
    //   19: aload_3
    //   20: putfield mReceiver : Lcom/android/internal/os/IResultReceiver;
    //   23: aload_1
    //   24: aload_0
    //   25: getfield mReceiver : Lcom/android/internal/os/IResultReceiver;
    //   28: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   33: invokevirtual writeStrongBinder : (Landroid/os/IBinder;)V
    //   36: aload_0
    //   37: monitorexit
    //   38: return
    //   39: astore_1
    //   40: aload_0
    //   41: monitorexit
    //   42: aload_1
    //   43: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #114	-> 0
    //   #115	-> 2
    //   #116	-> 9
    //   #118	-> 23
    //   #119	-> 36
    //   #120	-> 38
    //   #119	-> 39
    // Exception table:
    //   from	to	target	type
    //   2	9	39	finally
    //   9	23	39	finally
    //   23	36	39	finally
    //   36	38	39	finally
    //   40	42	39	finally
  }
  
  ResultReceiver(Parcel paramParcel) {
    this.mLocal = false;
    this.mHandler = null;
    this.mReceiver = IResultReceiver.Stub.asInterface(paramParcel.readStrongBinder());
  }
  
  public static final Parcelable.Creator<ResultReceiver> CREATOR = new Parcelable.Creator<ResultReceiver>() {
      public ResultReceiver createFromParcel(Parcel param1Parcel) {
        return new ResultReceiver(param1Parcel);
      }
      
      public ResultReceiver[] newArray(int param1Int) {
        return new ResultReceiver[param1Int];
      }
    };
  
  final Handler mHandler;
  
  final boolean mLocal;
  
  IResultReceiver mReceiver;
}
