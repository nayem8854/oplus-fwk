package android.os;

import android.util.MathUtils;

public class ParcelableParcel implements Parcelable {
  public ParcelableParcel(ClassLoader paramClassLoader) {
    this.mParcel = Parcel.obtain();
    this.mClassLoader = paramClassLoader;
  }
  
  public ParcelableParcel(Parcel paramParcel, ClassLoader paramClassLoader) {
    this.mParcel = Parcel.obtain();
    this.mClassLoader = paramClassLoader;
    int i = paramParcel.readInt();
    if (i >= 0) {
      int j = paramParcel.dataPosition();
      paramParcel.setDataPosition(MathUtils.addOrThrow(j, i));
      this.mParcel.appendFrom(paramParcel, j, i);
      return;
    } 
    throw new IllegalArgumentException("Negative size read from parcel");
  }
  
  public Parcel getParcel() {
    this.mParcel.setDataPosition(0);
    return this.mParcel;
  }
  
  public ClassLoader getClassLoader() {
    return this.mClassLoader;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mParcel.dataSize());
    Parcel parcel = this.mParcel;
    paramParcel.appendFrom(parcel, 0, parcel.dataSize());
  }
  
  public static final Parcelable.ClassLoaderCreator<ParcelableParcel> CREATOR = (Parcelable.ClassLoaderCreator<ParcelableParcel>)new Object();
  
  final ClassLoader mClassLoader;
  
  final Parcel mParcel;
}
