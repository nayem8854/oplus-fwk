package android.os;

import java.util.UUID;

public final class ParcelUuid implements Parcelable {
  public ParcelUuid(UUID paramUUID) {
    this.mUuid = paramUUID;
  }
  
  public static ParcelUuid fromString(String paramString) {
    return new ParcelUuid(UUID.fromString(paramString));
  }
  
  public UUID getUuid() {
    return this.mUuid;
  }
  
  public String toString() {
    return this.mUuid.toString();
  }
  
  public int hashCode() {
    return this.mUuid.hashCode();
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == null)
      return false; 
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof ParcelUuid))
      return false; 
    paramObject = paramObject;
    return this.mUuid.equals(((ParcelUuid)paramObject).mUuid);
  }
  
  public static final Parcelable.Creator<ParcelUuid> CREATOR = new Parcelable.Creator<ParcelUuid>() {
      public ParcelUuid createFromParcel(Parcel param1Parcel) {
        long l1 = param1Parcel.readLong();
        long l2 = param1Parcel.readLong();
        UUID uUID = new UUID(l1, l2);
        return new ParcelUuid(uUID);
      }
      
      public ParcelUuid[] newArray(int param1Int) {
        return new ParcelUuid[param1Int];
      }
    };
  
  private final UUID mUuid;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mUuid.getMostSignificantBits());
    paramParcel.writeLong(this.mUuid.getLeastSignificantBits());
  }
}
