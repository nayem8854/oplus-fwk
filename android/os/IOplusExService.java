package android.os;

public interface IOplusExService extends IInterface {
  void dealScreenoffGesture(int paramInt) throws RemoteException;
  
  boolean getGestureState(int paramInt) throws RemoteException;
  
  void pauseExInputEvent() throws RemoteException;
  
  void pilferPointers() throws RemoteException;
  
  boolean registerInputEvent(IOplusExInputCallBack paramIOplusExInputCallBack) throws RemoteException;
  
  boolean registerRawInputEvent(IOplusExInputCallBack paramIOplusExInputCallBack) throws RemoteException;
  
  boolean registerScreenoffGesture(IOplusGestureCallBack paramIOplusGestureCallBack) throws RemoteException;
  
  void resumeExInputEvent() throws RemoteException;
  
  void setGestureState(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void unregisterInputEvent(IOplusExInputCallBack paramIOplusExInputCallBack) throws RemoteException;
  
  void unregisterScreenoffGesture(IOplusGestureCallBack paramIOplusGestureCallBack) throws RemoteException;
  
  class Default implements IOplusExService {
    public boolean registerInputEvent(IOplusExInputCallBack param1IOplusExInputCallBack) throws RemoteException {
      return false;
    }
    
    public boolean registerRawInputEvent(IOplusExInputCallBack param1IOplusExInputCallBack) throws RemoteException {
      return false;
    }
    
    public void unregisterInputEvent(IOplusExInputCallBack param1IOplusExInputCallBack) throws RemoteException {}
    
    public void pauseExInputEvent() throws RemoteException {}
    
    public void resumeExInputEvent() throws RemoteException {}
    
    public boolean registerScreenoffGesture(IOplusGestureCallBack param1IOplusGestureCallBack) throws RemoteException {
      return false;
    }
    
    public void unregisterScreenoffGesture(IOplusGestureCallBack param1IOplusGestureCallBack) throws RemoteException {}
    
    public void dealScreenoffGesture(int param1Int) throws RemoteException {}
    
    public void setGestureState(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean getGestureState(int param1Int) throws RemoteException {
      return false;
    }
    
    public void pilferPointers() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusExService {
    private static final String DESCRIPTOR = "android.os.IOplusExService";
    
    static final int TRANSACTION_dealScreenoffGesture = 8;
    
    static final int TRANSACTION_getGestureState = 10;
    
    static final int TRANSACTION_pauseExInputEvent = 4;
    
    static final int TRANSACTION_pilferPointers = 11;
    
    static final int TRANSACTION_registerInputEvent = 1;
    
    static final int TRANSACTION_registerRawInputEvent = 2;
    
    static final int TRANSACTION_registerScreenoffGesture = 6;
    
    static final int TRANSACTION_resumeExInputEvent = 5;
    
    static final int TRANSACTION_setGestureState = 9;
    
    static final int TRANSACTION_unregisterInputEvent = 3;
    
    static final int TRANSACTION_unregisterScreenoffGesture = 7;
    
    public Stub() {
      attachInterface(this, "android.os.IOplusExService");
    }
    
    public static IOplusExService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IOplusExService");
      if (iInterface != null && iInterface instanceof IOplusExService)
        return (IOplusExService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "pilferPointers";
        case 10:
          return "getGestureState";
        case 9:
          return "setGestureState";
        case 8:
          return "dealScreenoffGesture";
        case 7:
          return "unregisterScreenoffGesture";
        case 6:
          return "registerScreenoffGesture";
        case 5:
          return "resumeExInputEvent";
        case 4:
          return "pauseExInputEvent";
        case 3:
          return "unregisterInputEvent";
        case 2:
          return "registerRawInputEvent";
        case 1:
          break;
      } 
      return "registerInputEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int i;
        IOplusGestureCallBack iOplusGestureCallBack;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.os.IOplusExService");
            pilferPointers();
            param1Parcel2.writeNoException();
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.os.IOplusExService");
            param1Int1 = param1Parcel1.readInt();
            bool2 = getGestureState(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.os.IOplusExService");
            i = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            setGestureState(i, bool);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.os.IOplusExService");
            i = param1Parcel1.readInt();
            dealScreenoffGesture(i);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.os.IOplusExService");
            iOplusGestureCallBack = IOplusGestureCallBack.Stub.asInterface(param1Parcel1.readStrongBinder());
            unregisterScreenoffGesture(iOplusGestureCallBack);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iOplusGestureCallBack.enforceInterface("android.os.IOplusExService");
            iOplusGestureCallBack = IOplusGestureCallBack.Stub.asInterface(iOplusGestureCallBack.readStrongBinder());
            bool1 = registerScreenoffGesture(iOplusGestureCallBack);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 5:
            iOplusGestureCallBack.enforceInterface("android.os.IOplusExService");
            resumeExInputEvent();
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iOplusGestureCallBack.enforceInterface("android.os.IOplusExService");
            pauseExInputEvent();
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iOplusGestureCallBack.enforceInterface("android.os.IOplusExService");
            iOplusExInputCallBack = IOplusExInputCallBack.Stub.asInterface(iOplusGestureCallBack.readStrongBinder());
            unregisterInputEvent(iOplusExInputCallBack);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iOplusExInputCallBack.enforceInterface("android.os.IOplusExService");
            iOplusExInputCallBack = IOplusExInputCallBack.Stub.asInterface(iOplusExInputCallBack.readStrongBinder());
            bool1 = registerRawInputEvent(iOplusExInputCallBack);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        iOplusExInputCallBack.enforceInterface("android.os.IOplusExService");
        IOplusExInputCallBack iOplusExInputCallBack = IOplusExInputCallBack.Stub.asInterface(iOplusExInputCallBack.readStrongBinder());
        boolean bool1 = registerInputEvent(iOplusExInputCallBack);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.os.IOplusExService");
      return true;
    }
    
    private static class Proxy implements IOplusExService {
      public static IOplusExService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IOplusExService";
      }
      
      public boolean registerInputEvent(IOplusExInputCallBack param2IOplusExInputCallBack) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusExService");
          if (param2IOplusExInputCallBack != null) {
            iBinder = param2IOplusExInputCallBack.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IOplusExService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusExService.Stub.getDefaultImpl().registerInputEvent(param2IOplusExInputCallBack);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerRawInputEvent(IOplusExInputCallBack param2IOplusExInputCallBack) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusExService");
          if (param2IOplusExInputCallBack != null) {
            iBinder = param2IOplusExInputCallBack.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IOplusExService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusExService.Stub.getDefaultImpl().registerRawInputEvent(param2IOplusExInputCallBack);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterInputEvent(IOplusExInputCallBack param2IOplusExInputCallBack) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IOplusExService");
          if (param2IOplusExInputCallBack != null) {
            iBinder = param2IOplusExInputCallBack.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusExService.Stub.getDefaultImpl() != null) {
            IOplusExService.Stub.getDefaultImpl().unregisterInputEvent(param2IOplusExInputCallBack);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void pauseExInputEvent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusExService");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusExService.Stub.getDefaultImpl() != null) {
            IOplusExService.Stub.getDefaultImpl().pauseExInputEvent();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resumeExInputEvent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusExService");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusExService.Stub.getDefaultImpl() != null) {
            IOplusExService.Stub.getDefaultImpl().resumeExInputEvent();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerScreenoffGesture(IOplusGestureCallBack param2IOplusGestureCallBack) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusExService");
          if (param2IOplusGestureCallBack != null) {
            iBinder = param2IOplusGestureCallBack.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusExService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusExService.Stub.getDefaultImpl().registerScreenoffGesture(param2IOplusGestureCallBack);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterScreenoffGesture(IOplusGestureCallBack param2IOplusGestureCallBack) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IOplusExService");
          if (param2IOplusGestureCallBack != null) {
            iBinder = param2IOplusGestureCallBack.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusExService.Stub.getDefaultImpl() != null) {
            IOplusExService.Stub.getDefaultImpl().unregisterScreenoffGesture(param2IOplusGestureCallBack);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dealScreenoffGesture(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusExService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusExService.Stub.getDefaultImpl() != null) {
            IOplusExService.Stub.getDefaultImpl().dealScreenoffGesture(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setGestureState(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.IOplusExService");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool1 && IOplusExService.Stub.getDefaultImpl() != null) {
            IOplusExService.Stub.getDefaultImpl().setGestureState(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getGestureState(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusExService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IOplusExService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusExService.Stub.getDefaultImpl().getGestureState(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void pilferPointers() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusExService");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOplusExService.Stub.getDefaultImpl() != null) {
            IOplusExService.Stub.getDefaultImpl().pilferPointers();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusExService param1IOplusExService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusExService != null) {
          Proxy.sDefaultImpl = param1IOplusExService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusExService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
