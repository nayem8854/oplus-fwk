package android.os;

public interface TraceNameSupplier {
  String getTraceName();
}
