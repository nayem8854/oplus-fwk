package android.os;

import android.system.ErrnoException;
import android.system.Os;
import android.system.StructStatVfs;

public class StatFs {
  private StructStatVfs mStat;
  
  public StatFs(String paramString) {
    this.mStat = doStat(paramString);
  }
  
  private static StructStatVfs doStat(String paramString) {
    try {
      return Os.statvfs(paramString);
    } catch (ErrnoException errnoException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid path: ");
      stringBuilder.append(paramString);
      throw new IllegalArgumentException(stringBuilder.toString(), errnoException);
    } 
  }
  
  public void restat(String paramString) {
    this.mStat = doStat(paramString);
  }
  
  @Deprecated
  public int getBlockSize() {
    return (int)this.mStat.f_frsize;
  }
  
  public long getBlockSizeLong() {
    return this.mStat.f_frsize;
  }
  
  @Deprecated
  public int getBlockCount() {
    return (int)this.mStat.f_blocks;
  }
  
  public long getBlockCountLong() {
    return this.mStat.f_blocks;
  }
  
  @Deprecated
  public int getFreeBlocks() {
    return (int)this.mStat.f_bfree;
  }
  
  public long getFreeBlocksLong() {
    return this.mStat.f_bfree;
  }
  
  public long getFreeBytes() {
    return this.mStat.f_bfree * this.mStat.f_frsize;
  }
  
  @Deprecated
  public int getAvailableBlocks() {
    return (int)this.mStat.f_bavail;
  }
  
  public long getAvailableBlocksLong() {
    return this.mStat.f_bavail;
  }
  
  public long getAvailableBytes() {
    return this.mStat.f_bavail * this.mStat.f_frsize;
  }
  
  public long getTotalBytes() {
    return this.mStat.f_blocks * this.mStat.f_frsize;
  }
}
