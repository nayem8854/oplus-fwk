package android.os;

public interface IExternalVibratorService extends IInterface {
  public static final int SCALE_HIGH = 1;
  
  public static final int SCALE_LOW = -1;
  
  public static final int SCALE_MUTE = -100;
  
  public static final int SCALE_NONE = 0;
  
  public static final int SCALE_VERY_HIGH = 2;
  
  public static final int SCALE_VERY_LOW = -2;
  
  int onExternalVibrationStart(ExternalVibration paramExternalVibration) throws RemoteException;
  
  void onExternalVibrationStop(ExternalVibration paramExternalVibration) throws RemoteException;
  
  class Default implements IExternalVibratorService {
    public int onExternalVibrationStart(ExternalVibration param1ExternalVibration) throws RemoteException {
      return 0;
    }
    
    public void onExternalVibrationStop(ExternalVibration param1ExternalVibration) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IExternalVibratorService {
    private static final String DESCRIPTOR = "android.os.IExternalVibratorService";
    
    static final int TRANSACTION_onExternalVibrationStart = 1;
    
    static final int TRANSACTION_onExternalVibrationStop = 2;
    
    public Stub() {
      attachInterface(this, "android.os.IExternalVibratorService");
    }
    
    public static IExternalVibratorService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IExternalVibratorService");
      if (iInterface != null && iInterface instanceof IExternalVibratorService)
        return (IExternalVibratorService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onExternalVibrationStop";
      } 
      return "onExternalVibrationStart";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.os.IExternalVibratorService");
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.IExternalVibratorService");
        if (param1Parcel1.readInt() != 0) {
          ExternalVibration externalVibration = ExternalVibration.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onExternalVibrationStop((ExternalVibration)param1Parcel1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IExternalVibratorService");
      if (param1Parcel1.readInt() != 0) {
        ExternalVibration externalVibration = ExternalVibration.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      param1Int1 = onExternalVibrationStart((ExternalVibration)param1Parcel1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    private static class Proxy implements IExternalVibratorService {
      public static IExternalVibratorService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IExternalVibratorService";
      }
      
      public int onExternalVibrationStart(ExternalVibration param2ExternalVibration) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IExternalVibratorService");
          if (param2ExternalVibration != null) {
            parcel1.writeInt(1);
            param2ExternalVibration.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IExternalVibratorService.Stub.getDefaultImpl() != null)
            return IExternalVibratorService.Stub.getDefaultImpl().onExternalVibrationStart(param2ExternalVibration); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onExternalVibrationStop(ExternalVibration param2ExternalVibration) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IExternalVibratorService");
          if (param2ExternalVibration != null) {
            parcel1.writeInt(1);
            param2ExternalVibration.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IExternalVibratorService.Stub.getDefaultImpl() != null) {
            IExternalVibratorService.Stub.getDefaultImpl().onExternalVibrationStop(param2ExternalVibration);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IExternalVibratorService param1IExternalVibratorService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IExternalVibratorService != null) {
          Proxy.sDefaultImpl = param1IExternalVibratorService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IExternalVibratorService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
