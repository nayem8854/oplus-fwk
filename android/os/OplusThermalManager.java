package android.os;

import java.util.ArrayList;

public class OplusThermalManager {
  public static final String EXTRA_BATTERY_PHONETEMP = "phoneTemp";
  
  public static int mConfigVersion = 2018030210;
  
  public static int mCpuLoadRecInterv;
  
  public static int mCpuLoadRecThreshold;
  
  public static int mDetectEnvironmentTempThreshold;
  
  public static long mDetectEnvironmentTimeThreshold;
  
  public static int mHeat1Align;
  
  public static int mHeat2Align;
  
  public static int mHeat3Align;
  
  public static int mHeatAlign;
  
  public static int mHeatCaptureCpuFreqInterval;
  
  public static int mHeatHoldTimeThreshold;
  
  public static int mHeatHoldUploadTime;
  
  public static int mHeatIncRatioThreshold;
  
  public static int mHeatRecInterv;
  
  public static int mHeatThreshold;
  
  public static int mHeatTopProCounts;
  
  public static boolean mHeatTopProFeatureOn;
  
  public static int mHeatTopProInterval;
  
  public static int mLessHeatThreshold;
  
  public static int mMonitorAppLimitTime;
  
  public static ArrayList<String> mMonitorAppList;
  
  public static int mMoreHeatThreshold;
  
  public static int mPreHeatDexOatThreshold;
  
  public static int mPreHeatThreshold;
  
  public static boolean mRecordThermalHistory;
  
  public static boolean mThermalBatteryTemp;
  
  public static boolean mThermalCaptureLog;
  
  public static int mThermalCaptureLogThreshold;
  
  public static boolean mThermalFeatureOn;
  
  public static String mThermalHeatPath = "/sys/class/thermal/thermal_zone5/temp";
  
  public static String mThermalHeatPath1 = "";
  
  public static String mThermalHeatPath2 = "/sys/class/power_supply/battery/temp";
  
  public static String mThermalHeatPath3 = "";
  
  public static boolean mThermalUploadDcs;
  
  public static boolean mThermalUploadErrLog;
  
  public static boolean mThermalUploadLog;
  
  public static int mTopCpuRecInterv;
  
  public static int mTopCpuRecThreshold;
  
  static {
    mHeatAlign = 100;
    mHeat1Align = 100;
    mHeat2Align = 1;
    mHeat3Align = 100;
    mThermalFeatureOn = false;
    mThermalUploadDcs = true;
    mThermalUploadLog = false;
    mThermalCaptureLog = false;
    mRecordThermalHistory = false;
    mThermalCaptureLogThreshold = 400;
    mThermalUploadErrLog = false;
    mThermalBatteryTemp = true;
    mHeatRecInterv = 2;
    mCpuLoadRecThreshold = 200;
    mCpuLoadRecInterv = 50;
    mTopCpuRecThreshold = 50;
    mTopCpuRecInterv = 20;
    mMoreHeatThreshold = 490;
    mHeatThreshold = 440;
    mLessHeatThreshold = 410;
    mPreHeatThreshold = 400;
    mHeatIncRatioThreshold = 1;
    mHeatHoldTimeThreshold = 30;
    mHeatHoldUploadTime = 40000;
    mMonitorAppList = new ArrayList<>();
    mMonitorAppLimitTime = 2400000;
    mHeatTopProInterval = 120;
    mHeatTopProFeatureOn = true;
    mHeatCaptureCpuFreqInterval = 120;
    mHeatTopProCounts = 5;
    mPreHeatDexOatThreshold = 400;
    mDetectEnvironmentTimeThreshold = 600000L;
    mDetectEnvironmentTempThreshold = 30;
  }
}
