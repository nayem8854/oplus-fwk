package android.os;

import android.util.Log;
import com.oplus.annotation.OplusProperty;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class OplusSystemProperties {
  private ArrayList<String> mOplusPropertyReadOnlyList = new ArrayList<>();
  
  private ArrayList<String> mOplusPropertyPersistList = new ArrayList<>();
  
  private ArrayList<String> mOplusPropertySysList = new ArrayList<>();
  
  private static final String TAG = "OplusSystemProperties";
  
  private OplusSystemProperties() {
    if (Build.IS_DEBUGGABLE)
      initOplusSystemPropertiesList(); 
  }
  
  private static class InstanceHolder {
    static OplusSystemProperties INSTANCE = new OplusSystemProperties();
  }
  
  public static OplusSystemProperties getInstance() {
    return InstanceHolder.INSTANCE;
  }
  
  private void initOplusSystemPropertiesList() {
    try {
      for (Field field : OplusPropertyList.class.getDeclaredFields()) {
        boolean bool = field.isAnnotationPresent((Class)OplusProperty.class);
        if (bool) {
          String str = (String)field.get((Object)null);
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("load prop:");
          stringBuilder.append(str);
          Log.d("OplusSystemProperties", stringBuilder.toString());
          OplusProperty oplusProperty = field.<OplusProperty>getDeclaredAnnotation(OplusProperty.class);
          if (oplusProperty != null) {
            StringBuilder stringBuilder1;
            int i = null.$SwitchMap$com$oplus$annotation$OplusProperty$OplusPropertyType[oplusProperty.value().ordinal()];
            if (i != 1) {
              if (i != 2) {
                if (i != 3) {
                  stringBuilder1 = new StringBuilder();
                  this();
                  stringBuilder1.append("Unknown type:");
                  stringBuilder1.append(oplusProperty.value());
                  Log.w("OplusSystemProperties", stringBuilder1.toString());
                } else {
                  this.mOplusPropertySysList.add(stringBuilder1);
                } 
              } else {
                this.mOplusPropertyPersistList.add(stringBuilder1);
              } 
            } else {
              this.mOplusPropertyReadOnlyList.add(stringBuilder1);
            } 
          } 
        } 
      } 
    } catch (Exception exception) {
      Log.e("OplusSystemProperties", "initOplusSystemPropertiesList failed.", exception);
    } 
  }
  
  private boolean isPredefinedProperty(String paramString) {
    if (paramString == null)
      return false; 
    if (!this.mOplusPropertyReadOnlyList.contains(paramString)) {
      ArrayList<String> arrayList = this.mOplusPropertyPersistList;
      if (!arrayList.contains(paramString)) {
        arrayList = this.mOplusPropertySysList;
        if (!arrayList.contains(paramString))
          return false; 
      } 
    } 
    return true;
  }
  
  private static boolean isPredefinedOplusProperty(String paramString) {
    if (Build.IS_DEBUGGABLE)
      return getInstance().isPredefinedProperty(paramString); 
    return true;
  }
  
  public static String get(String paramString) {
    if (isPredefinedOplusProperty(paramString))
      return SystemProperties.get(paramString); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Warning: This property is not predefined, prop:");
    stringBuilder.append(paramString);
    Log.e("OplusSystemProperties", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("Warning: This property is not predefined, prop:");
    stringBuilder.append(paramString);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static String get(String paramString1, String paramString2) {
    if (isPredefinedOplusProperty(paramString1))
      return SystemProperties.get(paramString1, paramString2); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Warning: This property is not predefined, prop:");
    stringBuilder.append(paramString1);
    Log.e("OplusSystemProperties", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("Warning: This property is not predefined, prop:");
    stringBuilder.append(paramString1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static int getInt(String paramString, int paramInt) {
    if (isPredefinedOplusProperty(paramString))
      return SystemProperties.getInt(paramString, paramInt); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Warning: This property is not predefined, prop:");
    stringBuilder.append(paramString);
    Log.e("OplusSystemProperties", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("Warning: This property is not predefined, prop:");
    stringBuilder.append(paramString);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static long getLong(String paramString, long paramLong) {
    if (isPredefinedOplusProperty(paramString))
      return SystemProperties.getLong(paramString, paramLong); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Warning: This property is not predefined, prop:");
    stringBuilder.append(paramString);
    Log.e("OplusSystemProperties", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("Warning: This property is not predefined, prop:");
    stringBuilder.append(paramString);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static boolean getBoolean(String paramString, boolean paramBoolean) {
    if (isPredefinedOplusProperty(paramString))
      return SystemProperties.getBoolean(paramString, paramBoolean); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Warning: This property is not predefined, prop:");
    stringBuilder.append(paramString);
    Log.e("OplusSystemProperties", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("Warning: This property is not predefined, prop:");
    stringBuilder.append(paramString);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static void set(String paramString1, String paramString2) {
    if (isPredefinedOplusProperty(paramString1)) {
      SystemProperties.set(paramString1, paramString2);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Warning: This property is not predefined, prop:");
    stringBuilder.append(paramString1);
    Log.e("OplusSystemProperties", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("Warning: This property is not predefined, prop:");
    stringBuilder.append(paramString1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void dumpOplusProperty(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    if (!Build.IS_DEBUGGABLE) {
      paramPrintWriter.println("release version, unsupported");
      return;
    } 
    paramPrintWriter.println("RO properties:");
    for (String str : this.mOplusPropertyReadOnlyList) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Ro: ");
      stringBuilder.append(str);
      paramPrintWriter.println(stringBuilder.toString());
    } 
    paramPrintWriter.println("Persist properties:");
    for (String str : this.mOplusPropertyPersistList) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Persist: ");
      stringBuilder.append(str);
      paramPrintWriter.println(stringBuilder.toString());
    } 
    paramPrintWriter.println("Sys properties:");
    for (String str : this.mOplusPropertySysList) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Sys: ");
      stringBuilder.append(str);
      paramPrintWriter.println(stringBuilder.toString());
    } 
  }
}
