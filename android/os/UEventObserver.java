package android.os;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class UEventObserver {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "UEventObserver";
  
  private static UEventThread sThread;
  
  protected void finalize() throws Throwable {
    try {
      stopObserving();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private static UEventThread getThread() {
    // Byte code:
    //   0: ldc android/os/UEventObserver
    //   2: monitorenter
    //   3: getstatic android/os/UEventObserver.sThread : Landroid/os/UEventObserver$UEventThread;
    //   6: ifnonnull -> 25
    //   9: new android/os/UEventObserver$UEventThread
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic android/os/UEventObserver.sThread : Landroid/os/UEventObserver$UEventThread;
    //   21: aload_0
    //   22: invokevirtual start : ()V
    //   25: getstatic android/os/UEventObserver.sThread : Landroid/os/UEventObserver$UEventThread;
    //   28: astore_0
    //   29: ldc android/os/UEventObserver
    //   31: monitorexit
    //   32: aload_0
    //   33: areturn
    //   34: astore_0
    //   35: ldc android/os/UEventObserver
    //   37: monitorexit
    //   38: aload_0
    //   39: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #67	-> 0
    //   #68	-> 3
    //   #69	-> 9
    //   #70	-> 21
    //   #72	-> 25
    //   #73	-> 34
    // Exception table:
    //   from	to	target	type
    //   3	9	34	finally
    //   9	21	34	finally
    //   21	25	34	finally
    //   25	32	34	finally
    //   35	38	34	finally
  }
  
  private static UEventThread peekThread() {
    // Byte code:
    //   0: ldc android/os/UEventObserver
    //   2: monitorenter
    //   3: getstatic android/os/UEventObserver.sThread : Landroid/os/UEventObserver$UEventThread;
    //   6: astore_0
    //   7: ldc android/os/UEventObserver
    //   9: monitorexit
    //   10: aload_0
    //   11: areturn
    //   12: astore_0
    //   13: ldc android/os/UEventObserver
    //   15: monitorexit
    //   16: aload_0
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #77	-> 0
    //   #78	-> 3
    //   #79	-> 12
    // Exception table:
    //   from	to	target	type
    //   3	10	12	finally
    //   13	16	12	finally
  }
  
  public final void startObserving(String paramString) {
    if (paramString != null && !paramString.isEmpty()) {
      UEventThread uEventThread = getThread();
      uEventThread.addObserver(paramString, this);
      return;
    } 
    throw new IllegalArgumentException("match substring must be non-empty");
  }
  
  public final void stopObserving() {
    UEventThread uEventThread = peekThread();
    if (uEventThread != null)
      uEventThread.removeObserver(this); 
  }
  
  private static native void nativeAddMatch(String paramString);
  
  private static native void nativeRemoveMatch(String paramString);
  
  private static native void nativeSetup();
  
  private static native String nativeWaitForNextEvent();
  
  public abstract void onUEvent(UEvent paramUEvent);
  
  public static final class UEvent {
    private final HashMap<String, String> mMap = new HashMap<>();
    
    public UEvent(String param1String) {
      int i = 0;
      int j = param1String.length();
      while (i < j) {
        int k = param1String.indexOf('=', i);
        int m = param1String.indexOf(false, i);
        if (m < 0)
          break; 
        if (k > i && k < m) {
          HashMap<String, String> hashMap = this.mMap;
          String str1 = param1String.substring(i, k);
          String str2 = param1String.substring(k + 1, m);
          hashMap.put(str1, str2);
        } 
        i = m + 1;
      } 
    }
    
    public String get(String param1String) {
      return this.mMap.get(param1String);
    }
    
    public String get(String param1String1, String param1String2) {
      param1String1 = this.mMap.get(param1String1);
      if (param1String1 == null)
        param1String1 = param1String2; 
      return param1String1;
    }
    
    public String toString() {
      return this.mMap.toString();
    }
  }
  
  private static final class UEventThread extends Thread {
    private final ArrayList<Object> mKeysAndObservers = new ArrayList();
    
    private final ArrayList<UEventObserver> mTempObserversToSignal = new ArrayList<>();
    
    public UEventThread() {
      super("UEventObserver");
    }
    
    public void run() {
      UEventObserver.nativeSetup();
      while (true) {
        String str = UEventObserver.nativeWaitForNextEvent();
        if (str != null)
          sendEvent(str); 
      } 
    }
    
    private void sendEvent(String param1String) {
      ArrayList<Object> arrayList;
      UEventObserver uEventObserver;
      synchronized (this.mKeysAndObservers) {
        int i = this.mKeysAndObservers.size();
        byte b;
        for (b = 0; b < i; b += 2) {
          String str = (String)this.mKeysAndObservers.get(b);
          if (param1String.contains(str)) {
            ArrayList<Object> arrayList1 = this.mKeysAndObservers;
            UEventObserver uEventObserver1 = (UEventObserver)arrayList1.get(b + 1);
            this.mTempObserversToSignal.add(uEventObserver1);
          } 
        } 
        if (!this.mTempObserversToSignal.isEmpty()) {
          UEventObserver.UEvent uEvent = new UEventObserver.UEvent(param1String);
          i = this.mTempObserversToSignal.size();
          for (b = 0; b < i; b++) {
            uEventObserver = this.mTempObserversToSignal.get(b);
            uEventObserver.onUEvent(uEvent);
          } 
          this.mTempObserversToSignal.clear();
        } 
        return;
      } 
    }
    
    public void addObserver(String param1String, UEventObserver param1UEventObserver) {
      synchronized (this.mKeysAndObservers) {
        this.mKeysAndObservers.add(param1String);
        this.mKeysAndObservers.add(param1UEventObserver);
        UEventObserver.nativeAddMatch(param1String);
        return;
      } 
    }
    
    public void removeObserver(UEventObserver param1UEventObserver) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mKeysAndObservers : Ljava/util/ArrayList;
      //   4: astore_2
      //   5: aload_2
      //   6: monitorenter
      //   7: iconst_0
      //   8: istore_3
      //   9: iload_3
      //   10: aload_0
      //   11: getfield mKeysAndObservers : Ljava/util/ArrayList;
      //   14: invokevirtual size : ()I
      //   17: if_icmpge -> 72
      //   20: aload_0
      //   21: getfield mKeysAndObservers : Ljava/util/ArrayList;
      //   24: iload_3
      //   25: iconst_1
      //   26: iadd
      //   27: invokevirtual get : (I)Ljava/lang/Object;
      //   30: aload_1
      //   31: if_acmpne -> 66
      //   34: aload_0
      //   35: getfield mKeysAndObservers : Ljava/util/ArrayList;
      //   38: iload_3
      //   39: iconst_1
      //   40: iadd
      //   41: invokevirtual remove : (I)Ljava/lang/Object;
      //   44: pop
      //   45: aload_0
      //   46: getfield mKeysAndObservers : Ljava/util/ArrayList;
      //   49: iload_3
      //   50: invokevirtual remove : (I)Ljava/lang/Object;
      //   53: checkcast java/lang/String
      //   56: astore #4
      //   58: aload #4
      //   60: invokestatic access$300 : (Ljava/lang/String;)V
      //   63: goto -> 9
      //   66: iinc #3, 2
      //   69: goto -> 9
      //   72: aload_2
      //   73: monitorexit
      //   74: return
      //   75: astore_1
      //   76: aload_2
      //   77: monitorexit
      //   78: aload_1
      //   79: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #233	-> 0
      //   #234	-> 7
      //   #235	-> 20
      //   #236	-> 34
      //   #237	-> 45
      //   #238	-> 58
      //   #239	-> 63
      //   #240	-> 66
      //   #243	-> 72
      //   #244	-> 74
      //   #243	-> 75
      // Exception table:
      //   from	to	target	type
      //   9	20	75	finally
      //   20	34	75	finally
      //   34	45	75	finally
      //   45	58	75	finally
      //   58	63	75	finally
      //   72	74	75	finally
      //   76	78	75	finally
    }
  }
}
