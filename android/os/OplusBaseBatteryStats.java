package android.os;

import android.text.format.DateFormat;
import java.io.PrintWriter;

public abstract class OplusBaseBatteryStats {
  class ThermalItem implements Parcelable {
    public byte cmd = 0;
    
    public int phoneTemp = -1023;
    
    public int phoneTemp1 = -1023;
    
    public int phoneTemp2 = -1023;
    
    public int phoneTemp3 = -1023;
    
    public int enviTemp = -1023;
    
    public String jobSchedule = "null";
    
    public String netSync = "null";
    
    public String foreProc = "null";
    
    public String topProc = "null";
    
    public static final byte CMD_AUDIOONOFF = 11;
    
    public static final byte CMD_BACKLIGHTINFO = 3;
    
    public static final byte CMD_BAT_INFO = 1;
    
    public static final byte CMD_CAMEARAONOFF = 10;
    
    public static final byte CMD_COMMON_UPDATE = 26;
    
    public static final byte CMD_CONNECTNETTYPE = 9;
    
    public static final byte CMD_ENVITEMP = 24;
    
    public static final byte CMD_FLASHLIGHTONOFF = 14;
    
    public static final byte CMD_FOREPRCINFO = 17;
    
    public static final byte CMD_GPSONOFF = 13;
    
    public static final byte CMD_JOBINFO = 15;
    
    public static final byte CMD_NETSTATE = 8;
    
    public static final byte CMD_NETSYNCINFO = 16;
    
    public static final byte CMD_NULL = 0;
    
    public static final byte CMD_PHONE_ONFF = 5;
    
    public static final byte CMD_PHONE_SIGNAL = 7;
    
    public static final byte CMD_PHONE_STATE = 6;
    
    public static final byte CMD_RESET = 19;
    
    public static final byte CMD_TEMPINFO = 2;
    
    public static final byte CMD_THERMALRATIO = 20;
    
    public static final byte CMD_THERMALRATIO1 = 21;
    
    public static final byte CMD_THERMALRATIO2 = 22;
    
    public static final byte CMD_THERMALRATIO3 = 23;
    
    public static final byte CMD_TOPPROCINFO = 18;
    
    public static final byte CMD_UPDATE_TIME = 25;
    
    public static final byte CMD_VIDEOONOFF = 12;
    
    public static final byte CMD_WIFIINFO = 4;
    
    public static final int CONNECT_MOBILE = 0;
    
    public static final int CONNECT_NONE = -1;
    
    public static final int CONNECT_WIFI = 1;
    
    private static final int INVALID_DATA = -1023;
    
    public static final byte NETWORK_CLASS_2_G = 2;
    
    public static final byte NETWORK_CLASS_3_G = 3;
    
    public static final byte NETWORK_CLASS_4_G = 4;
    
    public static final byte NETWORK_CLASS_UNKNOWN = 0;
    
    public static final byte NETWORK_CLASS_WIFI = 1;
    
    public static final int WIFI_OFF = 0;
    
    public static final int WIFI_ON = 1;
    
    public static final int WIFI_RUN = 2;
    
    public static final int WIFI_STOP = 3;
    
    public boolean audioOn;
    
    public int backlight;
    
    public long baseElapsedRealtime;
    
    public int batPercent;
    
    public int batRm;
    
    public int batTemp;
    
    public boolean cameraOn;
    
    public int chargePlug;
    
    public byte connectNetType;
    
    public int cpuLoading;
    
    public long currentTime;
    
    public boolean dataNetStatus;
    
    public long elapsedRealtime;
    
    public boolean flashlightOn;
    
    public boolean gpsOn;
    
    public boolean isAutoBrightness;
    
    public ThermalItem next;
    
    public int numReadInts;
    
    public boolean phoneOnff;
    
    public byte phoneSignal;
    
    public byte phoneState;
    
    public byte thermalRatio;
    
    public byte thermalRatio1;
    
    public byte thermalRatio2;
    
    public byte thermalRatio3;
    
    public int topCpu;
    
    public long upTime;
    
    public String versionName;
    
    public boolean videoOn;
    
    public int volume;
    
    public int wifiSignal;
    
    public int wifiStats;
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeByte(this.cmd);
      param1Parcel.writeLong(this.currentTime);
      param1Parcel.writeLong(this.elapsedRealtime);
      param1Parcel.writeLong(this.upTime);
      param1Parcel.writeLong(this.baseElapsedRealtime);
      param1Parcel.writeInt(this.batRm);
      param1Parcel.writeInt(this.batTemp);
      param1Parcel.writeInt(this.phoneTemp);
      param1Parcel.writeInt(this.phoneTemp1);
      param1Parcel.writeInt(this.phoneTemp2);
      param1Parcel.writeInt(this.phoneTemp3);
      param1Parcel.writeByte(this.thermalRatio);
      param1Parcel.writeByte(this.thermalRatio1);
      param1Parcel.writeByte(this.thermalRatio2);
      param1Parcel.writeByte(this.thermalRatio3);
      param1Parcel.writeInt(this.enviTemp);
      param1Parcel.writeInt(this.batPercent);
      param1Parcel.writeInt(this.chargePlug);
      param1Parcel.writeInt(this.backlight);
      param1Parcel.writeInt(this.volume);
      param1Parcel.writeInt(this.wifiStats);
      param1Parcel.writeInt(this.wifiSignal);
      param1Parcel.writeBoolean(this.phoneOnff);
      param1Parcel.writeByte(this.phoneState);
      param1Parcel.writeByte(this.phoneSignal);
      param1Parcel.writeBoolean(this.dataNetStatus);
      param1Parcel.writeByte(this.connectNetType);
      param1Parcel.writeBoolean(this.cameraOn);
      param1Parcel.writeBoolean(this.audioOn);
      param1Parcel.writeBoolean(this.videoOn);
      param1Parcel.writeBoolean(this.gpsOn);
      param1Parcel.writeBoolean(this.flashlightOn);
      param1Parcel.writeString(this.jobSchedule);
      param1Parcel.writeString(this.netSync);
      param1Parcel.writeString(this.foreProc);
      param1Parcel.writeString(this.topProc);
      param1Parcel.writeInt(this.cpuLoading);
      param1Parcel.writeInt(this.topCpu);
    }
    
    public void readFromParcel(Parcel param1Parcel) {
      int i = param1Parcel.dataPosition();
      this.cmd = param1Parcel.readByte();
      this.currentTime = param1Parcel.readLong();
      this.elapsedRealtime = param1Parcel.readLong();
      this.upTime = param1Parcel.readLong();
      this.batRm = param1Parcel.readInt();
      this.batTemp = param1Parcel.readInt();
      this.phoneTemp = param1Parcel.readInt();
      this.phoneTemp1 = param1Parcel.readInt();
      this.phoneTemp2 = param1Parcel.readInt();
      this.phoneTemp3 = param1Parcel.readInt();
      this.thermalRatio = param1Parcel.readByte();
      this.thermalRatio1 = param1Parcel.readByte();
      this.thermalRatio2 = param1Parcel.readByte();
      this.thermalRatio3 = param1Parcel.readByte();
      this.enviTemp = param1Parcel.readInt();
      this.batPercent = param1Parcel.readInt();
      this.chargePlug = param1Parcel.readInt();
      this.backlight = param1Parcel.readInt();
      this.volume = param1Parcel.readInt();
      this.wifiStats = param1Parcel.readInt();
      this.wifiSignal = param1Parcel.readInt();
      this.phoneOnff = param1Parcel.readBoolean();
      this.phoneState = param1Parcel.readByte();
      this.phoneSignal = param1Parcel.readByte();
      this.dataNetStatus = param1Parcel.readBoolean();
      this.connectNetType = param1Parcel.readByte();
      this.cameraOn = param1Parcel.readBoolean();
      this.audioOn = param1Parcel.readBoolean();
      this.videoOn = param1Parcel.readBoolean();
      this.gpsOn = param1Parcel.readBoolean();
      this.flashlightOn = param1Parcel.readBoolean();
      this.jobSchedule = param1Parcel.readString();
      this.netSync = param1Parcel.readString();
      this.foreProc = param1Parcel.readString();
      this.topProc = param1Parcel.readString();
      this.cpuLoading = param1Parcel.readInt();
      this.topCpu = param1Parcel.readInt();
      this.numReadInts += (param1Parcel.dataPosition() - i) / 4;
    }
    
    public void clear() {
      this.cmd = 0;
      this.currentTime = -1L;
      this.elapsedRealtime = -1L;
      this.upTime = -1L;
      this.batRm = -1;
      this.batTemp = -1;
      this.phoneTemp = -1023;
      this.phoneTemp1 = -1023;
      this.phoneTemp2 = -1023;
      this.phoneTemp3 = -1023;
      this.thermalRatio = -127;
      this.thermalRatio1 = -127;
      this.thermalRatio2 = -127;
      this.thermalRatio3 = -127;
      this.enviTemp = -1023;
      this.batPercent = -1;
      this.chargePlug = -1;
      this.backlight = -1;
      this.volume = 0;
      this.wifiStats = -1;
      this.wifiSignal = -1;
      this.phoneState = -1;
      this.phoneOnff = false;
      this.phoneSignal = 0;
      this.dataNetStatus = false;
      this.connectNetType = 0;
      this.cameraOn = false;
      this.audioOn = false;
      this.videoOn = false;
      this.gpsOn = false;
      this.flashlightOn = false;
      this.jobSchedule = "null";
      this.netSync = "null";
      this.foreProc = "null";
      this.topProc = "null";
      this.cpuLoading = 0;
      this.topCpu = 0;
      this.isAutoBrightness = false;
    }
    
    public void setTo(ThermalItem param1ThermalItem) {
      setToCommon(param1ThermalItem);
    }
    
    private void setToCommon(ThermalItem param1ThermalItem) {
      this.cmd = param1ThermalItem.cmd;
      this.currentTime = param1ThermalItem.currentTime;
      this.elapsedRealtime = param1ThermalItem.elapsedRealtime;
      this.upTime = param1ThermalItem.upTime;
      this.baseElapsedRealtime = param1ThermalItem.baseElapsedRealtime;
      this.batRm = param1ThermalItem.batRm;
      this.batTemp = param1ThermalItem.batTemp;
      this.phoneTemp = param1ThermalItem.phoneTemp;
      this.phoneTemp1 = param1ThermalItem.phoneTemp1;
      this.phoneTemp2 = param1ThermalItem.phoneTemp2;
      this.phoneTemp3 = param1ThermalItem.phoneTemp3;
      this.thermalRatio = param1ThermalItem.thermalRatio;
      this.thermalRatio1 = param1ThermalItem.thermalRatio1;
      this.thermalRatio2 = param1ThermalItem.thermalRatio2;
      this.thermalRatio3 = param1ThermalItem.thermalRatio3;
      this.enviTemp = param1ThermalItem.enviTemp;
      this.batPercent = param1ThermalItem.batPercent;
      this.chargePlug = param1ThermalItem.chargePlug;
      this.backlight = param1ThermalItem.backlight;
      this.volume = param1ThermalItem.volume;
      this.wifiStats = param1ThermalItem.wifiStats;
      this.wifiSignal = param1ThermalItem.wifiSignal;
      this.phoneOnff = param1ThermalItem.phoneOnff;
      this.phoneState = param1ThermalItem.phoneState;
      this.phoneSignal = param1ThermalItem.phoneSignal;
      this.dataNetStatus = param1ThermalItem.dataNetStatus;
      this.connectNetType = param1ThermalItem.connectNetType;
      this.cameraOn = param1ThermalItem.cameraOn;
      this.audioOn = param1ThermalItem.audioOn;
      this.videoOn = param1ThermalItem.videoOn;
      this.gpsOn = param1ThermalItem.gpsOn;
      this.flashlightOn = param1ThermalItem.flashlightOn;
      this.jobSchedule = param1ThermalItem.jobSchedule;
      this.netSync = param1ThermalItem.netSync;
      this.foreProc = param1ThermalItem.foreProc;
      this.versionName = param1ThermalItem.versionName;
      this.topProc = param1ThermalItem.topProc;
      this.cpuLoading = param1ThermalItem.cpuLoading;
      this.topCpu = param1ThermalItem.topCpu;
      this.isAutoBrightness = param1ThermalItem.isAutoBrightness;
    }
    
    public boolean same(ThermalItem param1ThermalItem) {
      boolean bool;
      if (this.currentTime == param1ThermalItem.currentTime && this.elapsedRealtime == param1ThermalItem.elapsedRealtime && this.upTime == param1ThermalItem.upTime && this.baseElapsedRealtime == param1ThermalItem.baseElapsedRealtime && this.batRm == param1ThermalItem.batRm && this.batTemp == param1ThermalItem.batTemp && this.phoneTemp == param1ThermalItem.phoneTemp && this.phoneTemp1 == param1ThermalItem.phoneTemp1 && this.phoneTemp2 == param1ThermalItem.phoneTemp2 && this.phoneTemp3 == param1ThermalItem.phoneTemp3 && this.thermalRatio == param1ThermalItem.thermalRatio && this.thermalRatio1 == param1ThermalItem.thermalRatio1 && this.thermalRatio2 == param1ThermalItem.thermalRatio2 && this.thermalRatio3 == param1ThermalItem.thermalRatio3 && this.enviTemp == param1ThermalItem.enviTemp && this.batPercent == param1ThermalItem.batPercent && this.chargePlug == param1ThermalItem.chargePlug && this.backlight == param1ThermalItem.backlight && this.volume == param1ThermalItem.volume && this.wifiStats == param1ThermalItem.wifiStats && this.wifiSignal == param1ThermalItem.wifiSignal && this.phoneOnff == param1ThermalItem.phoneOnff && this.phoneState == param1ThermalItem.phoneState && this.phoneSignal == param1ThermalItem.phoneSignal && this.dataNetStatus == param1ThermalItem.dataNetStatus && this.connectNetType == param1ThermalItem.connectNetType && this.audioOn == param1ThermalItem.audioOn && this.videoOn == param1ThermalItem.videoOn && this.gpsOn == param1ThermalItem.gpsOn && this.flashlightOn == param1ThermalItem.flashlightOn && this.jobSchedule == param1ThermalItem.jobSchedule && this.netSync == param1ThermalItem.netSync && this.foreProc == param1ThermalItem.foreProc && this.versionName == param1ThermalItem.versionName && this.topProc == param1ThermalItem.topProc && this.cpuLoading == param1ThermalItem.cpuLoading && this.topCpu == param1ThermalItem.topCpu && this.isAutoBrightness == param1ThermalItem.isAutoBrightness) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int describeContents() {
      return 0;
    }
  }
  
  public static class ThermalHistoryPrinter {
    public void printNextItem(PrintWriter param1PrintWriter, OplusBaseBatteryStats.ThermalItem param1ThermalItem) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(DateFormat.format("yyyy-MM-dd-HH-mm-ss", param1ThermalItem.elapsedRealtime - param1ThermalItem.baseElapsedRealtime + param1ThermalItem.currentTime).toString());
      stringBuilder.append(" ");
      OplusBaseBatteryStats.formatThermalTimeMsNoSpace(stringBuilder, param1ThermalItem.elapsedRealtime);
      stringBuilder.append(" ");
      OplusBaseBatteryStats.formatThermalTimeMsNoSpace(stringBuilder, param1ThermalItem.upTime);
      stringBuilder.append(" ");
      stringBuilder.append(Integer.toString(param1ThermalItem.batPercent));
      stringBuilder.append(" ");
      stringBuilder.append(Integer.toString(param1ThermalItem.backlight));
      stringBuilder.append(" ");
      stringBuilder.append(Integer.toString(param1ThermalItem.volume));
      if (param1ThermalItem.cmd == 0) {
        stringBuilder.append(" START RECORD");
        param1PrintWriter.print(stringBuilder.toString());
        param1PrintWriter.println();
        return;
      } 
      if (param1ThermalItem.cmd == 19) {
        stringBuilder.append(" THERMAL HISTORY RESET");
        param1PrintWriter.print(stringBuilder.toString());
        param1PrintWriter.println();
        return;
      } 
      stringBuilder.append(" batTemp=");
      stringBuilder.append(Integer.toString(param1ThermalItem.batTemp));
      stringBuilder.append(" phoneTemp=");
      stringBuilder.append(Integer.toString(param1ThermalItem.phoneTemp));
      stringBuilder.append(" thermalRatio=");
      stringBuilder.append(Float.toString(param1ThermalItem.thermalRatio / 10.0F));
      if (param1ThermalItem.enviTemp == -1023) {
        stringBuilder.append(" enviTemp=");
        stringBuilder.append("unknow");
      } else {
        stringBuilder.append(" enviTemp=");
        stringBuilder.append(Integer.toString(param1ThermalItem.enviTemp));
      } 
      stringBuilder.append(" batRm=");
      stringBuilder.append(Integer.toString(param1ThermalItem.batRm));
      stringBuilder.append(" plug=");
      int i = param1ThermalItem.chargePlug;
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 4) {
              stringBuilder.append("none");
            } else {
              stringBuilder.append("wireless");
            } 
          } else {
            stringBuilder.append("usb");
          } 
        } else {
          stringBuilder.append("ac");
        } 
      } else {
        stringBuilder.append("none");
      } 
      stringBuilder.append(" wifiStats=");
      stringBuilder.append(Integer.toString(param1ThermalItem.wifiStats));
      stringBuilder.append(" wifiSignal=");
      stringBuilder.append(Integer.toString(param1ThermalItem.wifiSignal));
      stringBuilder.append(" phoneOnff=");
      stringBuilder.append(Boolean.toString(param1ThermalItem.phoneOnff));
      byte b = param1ThermalItem.phoneState;
      i = param1ThermalItem.phoneState;
      stringBuilder.append(" simState=");
      stringBuilder.append(OplusBaseBatteryStats.formatSimState(b >> 4 & 0xF));
      stringBuilder.append(" phoneState=");
      stringBuilder.append(OplusBaseBatteryStats.formatPhoneState(i & 0xF));
      stringBuilder.append(" phoneSignal=");
      stringBuilder.append(" dataNetStatus=");
      stringBuilder.append(Boolean.toString(param1ThermalItem.dataNetStatus));
      stringBuilder.append(" connectNetType=");
      stringBuilder.append(OplusBaseBatteryStats.formatNetType(param1ThermalItem.connectNetType));
      stringBuilder.append(" cameraOn=");
      stringBuilder.append(Boolean.toString(param1ThermalItem.cameraOn));
      stringBuilder.append(" audioOn=");
      stringBuilder.append(Boolean.toString(param1ThermalItem.audioOn));
      stringBuilder.append(" videoOn=");
      stringBuilder.append(Boolean.toString(param1ThermalItem.videoOn));
      stringBuilder.append(" gpsOn=");
      stringBuilder.append(Boolean.toString(param1ThermalItem.gpsOn));
      stringBuilder.append(" flashlightOn=");
      stringBuilder.append(Boolean.toString(param1ThermalItem.flashlightOn));
      stringBuilder.append(" jobSchedule=");
      stringBuilder.append(param1ThermalItem.jobSchedule);
      stringBuilder.append(" netSync=");
      stringBuilder.append(param1ThermalItem.netSync);
      stringBuilder.append(" foreProc=");
      stringBuilder.append(param1ThermalItem.foreProc);
      stringBuilder.append(" cpuLoading=");
      stringBuilder.append(Float.toString(param1ThermalItem.cpuLoading / 10.0F));
      stringBuilder.append("%");
      stringBuilder.append(" topProc=");
      stringBuilder.append(param1ThermalItem.topProc);
      stringBuilder.append(" topCpu=");
      stringBuilder.append(Float.toString(param1ThermalItem.topCpu / 10.0F));
      stringBuilder.append("%");
      stringBuilder.append(" isAutoBrightness=");
      stringBuilder.append(Boolean.toString(param1ThermalItem.isAutoBrightness));
      stringBuilder.append(" version=");
      stringBuilder.append(param1ThermalItem.versionName);
      param1PrintWriter.print(stringBuilder.toString());
      param1PrintWriter.println();
    }
  }
  
  private static final void formatThermalTimeRaw(StringBuilder paramStringBuilder, long paramLong) {
    long l1 = paramLong / 86400L;
    if (l1 != 0L) {
      paramStringBuilder.append(l1);
      paramStringBuilder.append("d ");
    } 
    l1 = l1 * 60L * 60L * 24L;
    long l2 = (paramLong - l1) / 3600L;
    if (l2 != 0L || l1 != 0L) {
      paramStringBuilder.append(l2);
      paramStringBuilder.append("h");
    } 
    l2 = l1 + l2 * 60L * 60L;
    l1 = (paramLong - l2) / 60L;
    if (l1 != 0L || l2 != 0L) {
      paramStringBuilder.append(l1);
      paramStringBuilder.append("m");
    } 
    l1 = l2 + 60L * l1;
    if (paramLong != 0L || l1 != 0L) {
      paramStringBuilder.append(paramLong - l1);
      paramStringBuilder.append("s");
    } 
  }
  
  private static final String formatNetType(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3) {
          if (paramInt != 4)
            return "none"; 
          return "4g";
        } 
        return "3g";
      } 
      return "2g";
    } 
    return "wifi";
  }
  
  private static final String formatSimState(int paramInt) {
    switch (paramInt) {
      default:
        return "unknow";
      case 9:
        return "card_restricted";
      case 8:
        return "card_io_error";
      case 7:
        return "perm_disabled";
      case 6:
        return "not_ready";
      case 5:
        return "ready";
      case 4:
        return "network_locked";
      case 3:
        return "puk_required";
      case 2:
        return "pin_required";
      case 1:
        break;
    } 
    return "absent";
  }
  
  private static final String formatPhoneState(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 2) {
        if (paramInt != 3)
          return "out_of_service"; 
        return "state_power_off";
      } 
      return "emergency_only";
    } 
    return "in_service";
  }
  
  private static final void formatThermalTimeMsNoSpace(StringBuilder paramStringBuilder, long paramLong) {
    long l = paramLong / 1000L;
    formatThermalTimeRaw(paramStringBuilder, l);
    paramStringBuilder.append(paramLong - 1000L * l);
    paramStringBuilder.append("ms");
  }
}
