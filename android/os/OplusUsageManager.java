package android.os;

import android.util.Log;
import java.util.List;

public final class OplusUsageManager {
  private IOplusUsageService mOplusUsageService = null;
  
  private static OplusUsageManager mInstance = null;
  
  public static final String TAG = "OplusUsageManager";
  
  public static final String SERVICE_NAME = "usage";
  
  private static final boolean DEBUG_W = true;
  
  private static final boolean DEBUG_E = true;
  
  private static final boolean DEBUG = true;
  
  private OplusUsageManager() {
    IBinder iBinder = ServiceManager.getService("usage");
    this.mOplusUsageService = IOplusUsageService.Stub.asInterface(iBinder);
  }
  
  public static OplusUsageManager getOplusUsageManager() {
    if (mInstance == null)
      mInstance = new OplusUsageManager(); 
    return mInstance;
  }
  
  public void testSaveSomeData(int paramInt, String paramString) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        iOplusUsageService.testSaveSomeData(paramInt, paramString);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "testSaveSomeData failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "testSaveSomeData:service not publish!");
    } 
  }
  
  public List<String> getHistoryBootTime() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getHistoryBootTime();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getHistoryBootTime failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getHistoryBootTime:service not publish!");
    } 
    return null;
  }
  
  public List<String> getHistoryImeiNO() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getHistoryImeiNO();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getHistoryImeiNO failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getHistoryImeiNO:service not publish!");
    } 
    return null;
  }
  
  public List<String> getOriginalSimcardData() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getOriginalSimcardData();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getOriginalImeiMeidNO failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getOriginalImeiMeidNO:service not publish!");
    } 
    return null;
  }
  
  public List<String> getHistoryPcbaNO() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getHistoryPcbaNO();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getHistoryPcbaNO failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getHistoryPcbaNO:service not publish!");
    } 
    return null;
  }
  
  public int getAppUsageHistoryRecordCount() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getAppUsageHistoryRecordCount();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getAppUsageHistoryRecordCount failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getAppUsageHistoryRecordCount:service not publish!");
    } 
    return 0;
  }
  
  public List<String> getAppUsageHistoryRecords(int paramInt1, int paramInt2) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getAppUsageHistoryRecords(paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getAppUsageHistoryRecords failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getAppUsageHistoryRecords:service not publish!");
    } 
    return null;
  }
  
  public List<String> getAppUsageCountHistoryRecords(int paramInt1, int paramInt2) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getAppUsageCountHistoryRecords(paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getAppUsageCountHistoryRecords failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getAppUsageCountHistoryRecords:service not publish!");
    } 
    return null;
  }
  
  public List<String> getDialCountHistoryRecords(int paramInt1, int paramInt2) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getDialCountHistoryRecords(paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getDialCountHistoryRecords failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getDialCountHistoryRecords:service not publish!");
    } 
    return null;
  }
  
  public boolean writeAppUsageHistoryRecord(String paramString1, String paramString2) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.writeAppUsageHistoryRecord(paramString1, paramString2);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getAppUsageHistoryRecords failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getAppUsageHistoryRecords:service not publish!");
    } 
    return false;
  }
  
  public int getHistoryCountOfSendedMsg() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getHistoryCountOfSendedMsg();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getHistoryCountOfSendedMsg failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getHistoryCountOfSendedMsg:service not publish!");
    } 
    return 0;
  }
  
  public int getHistoryCountOfReceivedMsg() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getHistoryCountOfReceivedMsg();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getHistoryCountOfReceivedMsg failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getHistoryCountOfReceivedMsg:service not publish!");
    } 
    return 0;
  }
  
  public boolean accumulateHistoryCountOfSendedMsg(int paramInt) {
    if (paramInt <= 0) {
      Log.w("OplusUsageManager", "accumulateHistoryCountOfSendedMsg:illegal param!");
      return false;
    } 
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.accumulateHistoryCountOfSendedMsg(paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "accumulateHistoryCountOfSendedMsg failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "accumulateHistoryCountOfSendedMsg:service not publish!");
    } 
    return false;
  }
  
  public boolean accumulateHistoryCountOfReceivedMsg(int paramInt) {
    if (paramInt <= 0) {
      Log.w("OplusUsageManager", "accumulateHistoryCountOfReceivedMsg:illegal param!");
      return false;
    } 
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.accumulateHistoryCountOfReceivedMsg(paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "accumulateHistoryCountOfReceivedMsg failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "accumulateHistoryCountOfReceivedMsg:service not publish!");
    } 
    return false;
  }
  
  public int getDialOutDuration() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getDialOutDuration();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getDialOutDuration failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getDialOutDuration:service not publish!");
    } 
    return 0;
  }
  
  public int getInComingCallDuration() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getInComingCallDuration();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getInComingCallDuration failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getInComingCallDuration:service not publish!");
    } 
    return 0;
  }
  
  public boolean accumulateDialOutDuration(int paramInt) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.accumulateDialOutDuration(paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "accumulateDialOutDuration failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "accumulateDialOutDuration:service not publish!");
    } 
    return false;
  }
  
  public boolean accumulateInComingCallDuration(int paramInt) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.accumulateInComingCallDuration(paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "accumulateInComingCallDuration failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "accumulateInComingCallDuration:service not publish!");
    } 
    return false;
  }
  
  public int getHistoryRecordsCountOfPhoneCalls() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getHistoryRecordsCountOfPhoneCalls();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getHistoryRecordsCountOfPhoneCalls failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getHistoryRecordsCountOfPhoneCalls:service not publish!");
    } 
    return 0;
  }
  
  public List<String> getPhoneCallHistoryRecords(int paramInt1, int paramInt2) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getPhoneCallHistoryRecords(paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getPhoneCallHistoryRecords failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getPhoneCallHistoryRecords:service not publish!");
    } 
    return null;
  }
  
  public boolean writePhoneCallHistoryRecord(String paramString1, String paramString2) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.writePhoneCallHistoryRecord(paramString1, paramString2);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "writePhoneCallHistoryRecord failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "writePhoneCallHistoryRecord:service not publish!");
    } 
    return false;
  }
  
  public boolean updateMaxChargeCurrent(int paramInt) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.updateMaxChargeCurrent(paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "updateMaxChargeCurrent failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "updateMaxChargeCurrent:service not publish!");
    } 
    return false;
  }
  
  public boolean updateMaxChargeTemperature(int paramInt) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.updateMaxChargeCurrent(paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "updateMaxChargeTemperature failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "updateMaxChargeTemperature:service not publish!");
    } 
    return false;
  }
  
  public boolean updateMinChargeTemperature(int paramInt) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.updateMinChargeTemperature(paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "updateMinChargeTemperature failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "updateMinChargeTemperature:service not publish!");
    } 
    return false;
  }
  
  public int getMaxChargeCurrent() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getMaxChargeCurrent();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getMaxChargeCurrent failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getMaxChargeCurrent:service not publish!");
    } 
    return 0;
  }
  
  public int getMaxChargeTemperature() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getMaxChargeTemperature();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getMaxChargeTemperature failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getMaxChargeTemperature:service not publish!");
    } 
    return 0;
  }
  
  public int getMinChargeTemperature() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getMinChargeTemperature();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getMinChargeTemperature failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getMinChargeTemperature:service not publish!");
    } 
    return 0;
  }
  
  public byte[] engineerReadDevBlock(String paramString, int paramInt1, int paramInt2) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.engineerReadDevBlock(paramString, paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "engineerReadDevBlock failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "engineerReadDevBlock:service not publish!");
    } 
    return null;
  }
  
  public int engineerWriteDevBlock(String paramString, byte[] paramArrayOfbyte, int paramInt) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.engineerWriteDevBlock(paramString, paramArrayOfbyte, paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "engineerWriteDevBlock failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "engineerWriteDevBlock:service not publish!");
    } 
    return -1;
  }
  
  public String getDownloadStatusString(int paramInt) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getDownloadStatusString(paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getDownloadStatusString failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getDownloadStatusString:service not publish!");
    } 
    return null;
  }
  
  public String loadSecrecyConfig() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.loadSecrecyConfig();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "loadSecrecyConfig failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "loadSecrecyConfig:service not publish!");
    } 
    return null;
  }
  
  public int saveSecrecyConfig(String paramString) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.saveSecrecyConfig(paramString);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "saveSecrecyConfig failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "saveSecrecyConfig:service not publish!");
    } 
    return -1;
  }
  
  public boolean setProductLineLastTestFlag(int paramInt) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.setProductLineLastTestFlag(paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "setProductLineLastTestFlag failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "setProductLineLastTestFlag:service not publish!");
    } 
    return false;
  }
  
  public int getProductLineLastTestFlag() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getProductLineLastTestFlag();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getProductLineLastTestFlag failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getProductLineLastTestFlag:service not publish!");
    } 
    return -1;
  }
  
  public boolean recordApkDeleteEvent(String paramString1, String paramString2, String paramString3) {
    if (paramString1 == null || paramString1.isEmpty()) {
      Log.w("OplusUsageManager", "recordApkDeleteEvent:deleteAppPkgName empty!");
      return false;
    } 
    if (paramString2 == null || paramString2.isEmpty()) {
      Log.w("OplusUsageManager", "recordApkDeleteEvent:callerAppPkgName empty!");
      return false;
    } 
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.recordApkDeleteEvent(paramString1, paramString2, paramString3);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "recordApkDeleteEvent failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "recordApkDeleteEvent:service not publish!");
    } 
    return false;
  }
  
  public int getApkDeleteEventRecordCount() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getApkDeleteEventRecordCount();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getApkDeleteEventRecordCount failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getApkDeleteEventRecordCount:service not publish!");
    } 
    return 0;
  }
  
  public List<String> getApkDeleteEventRecords(int paramInt1, int paramInt2) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getApkDeleteEventRecords(paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getApkDeleteEventRecords failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getApkDeleteEventRecords:service not publish!");
    } 
    return null;
  }
  
  public boolean recordApkInstallEvent(String paramString1, String paramString2, String paramString3) {
    if (paramString1 == null || paramString1.isEmpty()) {
      Log.w("OplusUsageManager", "recordApkInstallEvent:deleteAppPkgName empty!");
      return false;
    } 
    if (paramString2 == null || paramString2.isEmpty()) {
      Log.w("OplusUsageManager", "recordApkInstallEvent:callerAppPkgName empty!");
      return false;
    } 
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.recordApkInstallEvent(paramString1, paramString2, paramString3);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "recordApkInstallEvent failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "recordApkInstallEvent:service not publish!");
    } 
    return false;
  }
  
  public int getApkInstallEventRecordCount() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getApkInstallEventRecordCount();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getApkInstallEventRecordCount failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getApkInstallEventRecordCount:service not publish!");
    } 
    return 0;
  }
  
  public List<String> getApkInstallEventRecords(int paramInt1, int paramInt2) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getApkInstallEventRecords(paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getApkInstallEventRecords failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getApkInstallEventRecords:service not publish!");
    } 
    return null;
  }
  
  public boolean recordMcsConnectID(String paramString) {
    if (paramString == null || paramString.isEmpty()) {
      Log.w("OplusUsageManager", "recordMcsConnectID:connectID empty!");
      return false;
    } 
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.recordMcsConnectID(paramString);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "recordMcsConnectID failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "recordMcsConnectID:service not publish!");
    } 
    return false;
  }
  
  public String getMcsConnectID() {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getMcsConnectID();
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getMcsConnectID failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getMcsConnectID:service not publish!");
    } 
    return null;
  }
  
  public int getFileSize(String paramString) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.getFileSize(paramString);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "getFileSize failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "getFileSize:service not publish!");
    } 
    return -1;
  }
  
  public byte[] readOplusFile(String paramString, int paramInt1, int paramInt2) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.readOplusFile(paramString, paramInt1, paramInt2);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "readOplusFile failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "readOplusFile:service not publish!");
    } 
    return null;
  }
  
  public int saveOplusFile(int paramInt1, String paramString, int paramInt2, boolean paramBoolean, int paramInt3, byte[] paramArrayOfbyte) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.saveOplusFile(paramInt1, paramString, paramInt2, paramBoolean, paramInt3, paramArrayOfbyte);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "saveOplusFile failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "saveOplusFile:service not publish!");
    } 
    return -1;
  }
  
  public int saveEntireOplusFile(String paramString1, String paramString2, boolean paramBoolean) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.saveEntireOplusFile(paramString1, paramString2, paramBoolean);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "saveEntireOplusFile failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "saveEntireOplusFile:service not publish!");
    } 
    return -1;
  }
  
  public boolean readEntireOplusFile(String paramString1, String paramString2, boolean paramBoolean) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.readEntireOplusFile(paramString1, paramString2, paramBoolean);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "readEntireOplusFile failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "readEntireOplusFile:service not publish!");
    } 
    return false;
  }
  
  public boolean deleteOplusFile(String paramString) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.deleteOplusFile(paramString);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "deleteOplusFile failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "deleteOplusFile:service not publish!");
    } 
    return false;
  }
  
  public boolean saveEntireOplusDir(String paramString1, String paramString2, boolean paramBoolean) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.saveEntireOplusDir(paramString1, paramString2, paramBoolean);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "saveEntireOplusDir failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "saveEntireOplusDir:service not publish!");
    } 
    return false;
  }
  
  public boolean readEntireOplusDir(String paramString1, String paramString2, boolean paramBoolean) {
    IOplusUsageService iOplusUsageService = this.mOplusUsageService;
    if (iOplusUsageService != null) {
      try {
        return iOplusUsageService.readEntireOplusDir(paramString1, paramString2, paramBoolean);
      } catch (RemoteException remoteException) {
        Log.e("OplusUsageManager", "saveEntireOplusDir failed!", (Throwable)remoteException);
      } 
    } else {
      Log.w("OplusUsageManager", "saveEntireOplusDir:service not publish!");
    } 
    return false;
  }
}
