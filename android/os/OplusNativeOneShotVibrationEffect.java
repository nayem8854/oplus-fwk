package android.os;

import java.util.Objects;

public class OplusNativeOneShotVibrationEffect extends VibrationEffect implements Parcelable {
  public OplusNativeOneShotVibrationEffect(Parcel paramParcel) {
    this(paramParcel.readInt(), paramParcel.readLong());
    this.mEffectStrength = paramParcel.readInt();
  }
  
  public OplusNativeOneShotVibrationEffect(int paramInt, long paramLong) {
    this.mWaveformId = paramInt;
    this.mTiming = paramLong;
    this.mEffectStrength = 2;
  }
  
  public int getId() {
    return this.mWaveformId;
  }
  
  public long getDuration() {
    return this.mTiming;
  }
  
  public void setEffectStrength(int paramInt) {
    if (isValidEffectStrength(paramInt)) {
      this.mEffectStrength = paramInt;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid effect strength: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int getEffectStrength() {
    return this.mEffectStrength;
  }
  
  private static boolean isValidEffectStrength(int paramInt) {
    if (paramInt != 0 && paramInt != 1 && paramInt != 2)
      return false; 
    return true;
  }
  
  public void validate() {
    if (this.mTiming > 0L) {
      if (isValidEffectStrength(this.mEffectStrength))
        return; 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Unknown effect strength (value=");
      stringBuilder1.append(this.mEffectStrength);
      stringBuilder1.append(")");
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("timing must be non-zero (timing=");
    stringBuilder.append(this.mTiming);
    stringBuilder.append(")");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mWaveformId != ((OplusNativeOneShotVibrationEffect)paramObject).mWaveformId || this.mTiming != ((OplusNativeOneShotVibrationEffect)paramObject).mTiming || this.mEffectStrength != ((OplusNativeOneShotVibrationEffect)paramObject).mEffectStrength)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mWaveformId), Long.valueOf(this.mTiming), Integer.valueOf(this.mEffectStrength) });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OplusNativeOneShotVibrationEffect{mWaveformId=");
    stringBuilder.append(this.mWaveformId);
    stringBuilder.append(", mTiming=");
    stringBuilder.append(this.mTiming);
    stringBuilder.append(", mEffectStrength=");
    stringBuilder.append(this.mEffectStrength);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(5);
    paramParcel.writeInt(this.mWaveformId);
    paramParcel.writeLong(this.mTiming);
    paramParcel.writeInt(this.mEffectStrength);
  }
  
  public static final Parcelable.Creator<OplusNativeOneShotVibrationEffect> CREATOR = (Parcelable.Creator<OplusNativeOneShotVibrationEffect>)new Object();
  
  public static final int PARCEL_TOKEN_OPLUS_NATIVE_ONESHOT = 5;
  
  private int mEffectStrength;
  
  private final long mTiming;
  
  private final int mWaveformId;
}
