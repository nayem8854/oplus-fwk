package android.os;

import android.util.ArrayMap;
import android.util.Log;
import com.android.internal.os.BinderInternal;
import com.android.internal.util.StatLogger;
import java.util.Map;

public final class ServiceManager {
  private static final int GET_SERVICE_LOG_EVERY_CALLS_CORE;
  
  private static final int GET_SERVICE_LOG_EVERY_CALLS_NON_CORE;
  
  private static final long GET_SERVICE_SLOW_THRESHOLD_US_CORE;
  
  private static final long GET_SERVICE_SLOW_THRESHOLD_US_NON_CORE;
  
  private static final int SLOW_LOG_INTERVAL_MS = 5000;
  
  private static final int STATS_LOG_INTERVAL_MS = 5000;
  
  private static final String TAG = "ServiceManager";
  
  private static Map<String, IBinder> sCache;
  
  private static int sGetServiceAccumulatedCallCount;
  
  private static int sGetServiceAccumulatedUs;
  
  private static long sLastSlowLogActualTime;
  
  private static long sLastSlowLogUptime;
  
  private static long sLastStatsLogUptime;
  
  private static final Object sLock = new Object();
  
  private static IServiceManager sServiceManager;
  
  public static final StatLogger sStatLogger;
  
  static {
    sCache = (Map<String, IBinder>)new ArrayMap();
    GET_SERVICE_SLOW_THRESHOLD_US_CORE = (SystemProperties.getInt("debug.servicemanager.slow_call_core_ms", 10) * 1000);
    GET_SERVICE_SLOW_THRESHOLD_US_NON_CORE = (SystemProperties.getInt("debug.servicemanager.slow_call_ms", 50) * 1000);
    GET_SERVICE_LOG_EVERY_CALLS_CORE = SystemProperties.getInt("debug.servicemanager.log_calls_core", 100);
    GET_SERVICE_LOG_EVERY_CALLS_NON_CORE = SystemProperties.getInt("debug.servicemanager.log_calls", 200);
    sStatLogger = new StatLogger(new String[] { "getService()" });
  }
  
  private static IServiceManager getIServiceManager() {
    IServiceManager iServiceManager = sServiceManager;
    if (iServiceManager != null)
      return iServiceManager; 
    sServiceManager = iServiceManager = ServiceManagerNative.asInterface(Binder.allowBlocking(BinderInternal.getContextObject()));
    return iServiceManager;
  }
  
  public static IBinder getService(String paramString) {
    try {
      IBinder iBinder = sCache.get(paramString);
      if (iBinder != null)
        return iBinder; 
      return Binder.allowBlocking(rawGetService(paramString));
    } catch (RemoteException remoteException) {
      Log.e("ServiceManager", "error in getService", (Throwable)remoteException);
      return null;
    } 
  }
  
  public static IBinder getServiceOrThrow(String paramString) throws ServiceNotFoundException {
    IBinder iBinder = getService(paramString);
    if (iBinder != null)
      return iBinder; 
    throw new ServiceNotFoundException(paramString);
  }
  
  public static void addService(String paramString, IBinder paramIBinder) {
    addService(paramString, paramIBinder, false, 8);
  }
  
  public static void addService(String paramString, IBinder paramIBinder, boolean paramBoolean) {
    addService(paramString, paramIBinder, paramBoolean, 8);
  }
  
  public static void addService(String paramString, IBinder paramIBinder, boolean paramBoolean, int paramInt) {
    try {
      getIServiceManager().addService(paramString, paramIBinder, paramBoolean, paramInt);
    } catch (RemoteException remoteException) {
      Log.e("ServiceManager", "error in addService", (Throwable)remoteException);
    } 
  }
  
  public static IBinder checkService(String paramString) {
    try {
      IBinder iBinder = sCache.get(paramString);
      if (iBinder != null)
        return iBinder; 
      return Binder.allowBlocking(getIServiceManager().checkService(paramString));
    } catch (RemoteException remoteException) {
      Log.e("ServiceManager", "error in checkService", (Throwable)remoteException);
      return null;
    } 
  }
  
  public static boolean isDeclared(String paramString) {
    try {
      return getIServiceManager().isDeclared(paramString);
    } catch (RemoteException remoteException) {
      Log.e("ServiceManager", "error in isDeclared", (Throwable)remoteException);
      return false;
    } 
  }
  
  public static IBinder waitForDeclaredService(String paramString) {
    if (isDeclared(paramString)) {
      IBinder iBinder = waitForService(paramString);
    } else {
      paramString = null;
    } 
    return (IBinder)paramString;
  }
  
  public static String[] listServices() {
    try {
      return getIServiceManager().listServices(15);
    } catch (RemoteException remoteException) {
      Log.e("ServiceManager", "error in listServices", (Throwable)remoteException);
      return null;
    } 
  }
  
  public static void initServiceCache(Map<String, IBinder> paramMap) {
    if (sCache.size() == 0) {
      sCache.putAll(paramMap);
      return;
    } 
    throw new IllegalStateException("setServiceCache may only be called once");
  }
  
  public static class ServiceNotFoundException extends Exception {
    public ServiceNotFoundException(String param1String) {
      super(stringBuilder.toString());
    }
  }
  
  private static IBinder rawGetService(String paramString) throws RemoteException {
    long l = sStatLogger.getTime();
    IBinder iBinder = getIServiceManager().getService(paramString);
    int i = (int)sStatLogger.logDurationStat(0, l);
    int j = Process.myUid();
    boolean bool = UserHandle.isCore(j);
    if (bool) {
      l = GET_SERVICE_SLOW_THRESHOLD_US_CORE;
    } else {
      l = GET_SERVICE_SLOW_THRESHOLD_US_NON_CORE;
    } 
    Object object = sLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      sGetServiceAccumulatedUs += i;
      sGetServiceAccumulatedCallCount++;
      long l1 = SystemClock.uptimeMillis();
      if (i >= l)
        try {
          if (l1 > sLastSlowLogUptime + 5000L || sLastSlowLogActualTime < i) {
            EventLogTags.writeServiceManagerSlow(i / 1000, paramString);
            sLastSlowLogUptime = l1;
            sLastSlowLogActualTime = i;
          } 
        } finally {} 
      if (bool) {
        j = GET_SERVICE_LOG_EVERY_CALLS_CORE;
      } else {
        j = GET_SERVICE_LOG_EVERY_CALLS_NON_CORE;
      } 
      if (sGetServiceAccumulatedCallCount >= j && l1 >= sLastStatsLogUptime + 5000L) {
        int k = sGetServiceAccumulatedCallCount;
        j = sGetServiceAccumulatedUs / 1000;
        l = sLastStatsLogUptime;
        i = (int)(l1 - l);
        try {
          EventLogTags.writeServiceManagerStats(k, j, i);
          sGetServiceAccumulatedCallCount = 0;
          sGetServiceAccumulatedUs = 0;
          sLastStatsLogUptime = l1;
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return iBinder;
        } finally {}
      } else {
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        return iBinder;
      } 
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    throw paramString;
  }
  
  public static native IBinder waitForService(String paramString);
  
  static interface Stats {
    public static final int COUNT = 1;
    
    public static final int GET_SERVICE = 0;
  }
}
