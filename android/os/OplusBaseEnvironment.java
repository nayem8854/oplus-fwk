package android.os;

import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class OplusBaseEnvironment {
  private static final String COTA_ENABLE;
  
  private static final String DEVICE_INFO_PATH = "/proc/devinfo/emmc";
  
  private static final String DEVICE_MANUFACTURE = "manufacture";
  
  private static final String DEVICE_MANUFACTURE_HYNIX = "HYNIX";
  
  private static final String DEVICE_MANUFACTURE_MICRON = "MICRON";
  
  private static final String DEVICE_MANUFACTURE_SANDISK = "SANDISK";
  
  private static final String DEVICE_VERSION = "version";
  
  private static final String DEVICE_VERSION_MICRON = "S0J9F8";
  
  private static File DIR_MY_CARRIER_ROOT;
  
  private static File DIR_MY_COMPANY_ROOT;
  
  private static File DIR_MY_COUNTRY_ROOT;
  
  private static File DIR_MY_ENGINEER_ROOT;
  
  private static File DIR_MY_HEYTAP_ROOT;
  
  private static File DIR_MY_OPERATOR_ROOT;
  
  private static File DIR_MY_PRELOAD_ROOT;
  
  private static File DIR_MY_PRODUCT_ROOT;
  
  private static File DIR_MY_REGION_ROOT;
  
  private static File DIR_MY_STOCK_ROOT;
  
  private static File DIR_OPPO_CUSTOM_ROOT = null;
  
  private static File DIR_OPPO_ENGINEER_ROOT;
  
  private static File DIR_OPPO_OPERATOR_ROOT = null;
  
  private static File DIR_OPPO_PRODUCT_ROOT;
  
  private static final String DIR_OPPO_RESERVE = "/mnt/vendor/opporeserve";
  
  private static File DIR_OPPO_VERSION_ROOT;
  
  private static final String ENV_MY_CARRIER_ROOT = "MY_CARRIER_ROOT";
  
  private static final String ENV_MY_COMPANY_ROOT = "MY_COMPANY_ROOT";
  
  private static final String ENV_MY_COUNTRY_ROOT = "MY_REGION_ROOT";
  
  private static final String ENV_MY_ENGINEER_ROOT = "MY_ENGINEERING_ROOT";
  
  private static final String ENV_MY_HEYTAP_ROOT = "MY_HEYTAP_ROOT";
  
  private static final String ENV_MY_OPERATOR_ROOT = "MY_CARRIER_ROOT";
  
  private static final String ENV_MY_PRELOAD_ROOT = "MY_PRELOAD_ROOT";
  
  private static final String ENV_MY_PRODUCT_ROOT = "MY_PRODUCT_ROOT";
  
  private static final String ENV_MY_REGION_ROOT = "MY_REGION_ROOT";
  
  private static final String ENV_MY_STOCK_ROOT = "MY_STOCK_ROOT";
  
  private static final String ENV_OPPO_CUSTOM_ROOT = "OPPO_CUSTOM_ROOT";
  
  private static final String ENV_OPPO_ENGINEER_ROOT = "OPPO_ENGINEER_ROOT";
  
  private static final String ENV_OPPO_OPERATOR_ROOT = "MY_OPERATOR_ROOT";
  
  private static final String ENV_OPPO_PRODUCT_ROOT = "OPPO_PRODUCT_ROOT";
  
  private static final String ENV_OPPO_VERSION_ROOT = "OPPO_VERSION_ROOT";
  
  public static final boolean NOT_ALLOW_EXT4_ACCESS = true;
  
  private static final String TAG = "OplusBaseEnvironment";
  
  static {
    DIR_OPPO_ENGINEER_ROOT = null;
    DIR_OPPO_PRODUCT_ROOT = null;
    DIR_OPPO_VERSION_ROOT = null;
    DIR_MY_PRELOAD_ROOT = null;
    DIR_MY_HEYTAP_ROOT = null;
    DIR_MY_STOCK_ROOT = null;
    DIR_MY_PRODUCT_ROOT = null;
    DIR_MY_COUNTRY_ROOT = null;
    DIR_MY_REGION_ROOT = null;
    DIR_MY_OPERATOR_ROOT = null;
    DIR_MY_CARRIER_ROOT = null;
    DIR_MY_COMPANY_ROOT = null;
    DIR_MY_ENGINEER_ROOT = null;
    COTA_ENABLE = SystemProperties.get("sys.cotaimg.verify", "0");
  }
  
  public static File getReserveDirectory() {
    return new File("/mnt/vendor/opporeserve");
  }
  
  private static Map<String, String> getDeviceInfo() {
    try {
      FileReader fileReader = new FileReader();
      this("/proc/devinfo/emmc");
      BufferedReader bufferedReader = new BufferedReader();
      this(fileReader, 256);
      HashMap<Object, Object> hashMap = new HashMap<>();
      this();
      while (true) {
        String str = bufferedReader.readLine();
        if (str != null) {
          String[] arrayOfString;
          boolean bool = str.contains("manufacture");
          if (bool) {
            arrayOfString = str.split("\\s+");
            hashMap.put("manufacture", arrayOfString[2]);
            continue;
          } 
          if (arrayOfString.contains("version")) {
            arrayOfString = arrayOfString.split("\\s+");
            hashMap.put("version", arrayOfString[2]);
          } 
          continue;
        } 
        break;
      } 
      bufferedReader.close();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getDeviceInfo,map=");
      stringBuilder.append(hashMap.toString());
      Log.d("OplusBaseEnvironment", stringBuilder.toString());
      return (Map)hashMap;
    } catch (Exception exception) {
      exception.printStackTrace();
      return null;
    } 
  }
  
  public static boolean isWhiteListMcp() {
    Map<String, String> map = getDeviceInfo();
    if (map == null) {
      Log.d("OplusBaseEnvironment", "isWhiteListMcp,getDeviceInfo result is null,return false");
      return false;
    } 
    Iterator<Map.Entry> iterator = map.entrySet().iterator();
    String str = null;
    map = null;
    while (iterator.hasNext()) {
      Map<String, String> map1;
      Map.Entry entry = iterator.next();
      String str1 = (String)entry.getKey();
      if (str1.equals("manufacture")) {
        String str2 = (String)entry.getValue();
      } else {
        map1 = map;
        if (str1.equals("version")) {
          str = (String)entry.getValue();
          map1 = map;
        } 
      } 
      map = map1;
    } 
    if (str != null && map != null)
      if (map.equalsIgnoreCase("HYNIX") || 
        map.equalsIgnoreCase("MICRON") || 
        map.equalsIgnoreCase("SANDISK"))
        return true;  
    return false;
  }
  
  protected static File getDirectorySup(String paramString1, String paramString2) {
    paramString1 = System.getenv(paramString1);
    File file = new File();
    if (paramString1 == null) {
      this(paramString2);
    } else {
      this(paramString1);
    } 
    return file;
  }
  
  public static File getOplusCustomDirectory() {
    if (DIR_OPPO_CUSTOM_ROOT == null)
      DIR_OPPO_CUSTOM_ROOT = getDirectorySup("OPPO_CUSTOM_ROOT", "/my_company"); 
    return DIR_OPPO_CUSTOM_ROOT;
  }
  
  public static File getOplusCotaDirectory() {
    if (DIR_OPPO_OPERATOR_ROOT == null)
      DIR_OPPO_OPERATOR_ROOT = getDirectorySup("MY_OPERATOR_ROOT", "/my_operator"); 
    return DIR_OPPO_OPERATOR_ROOT;
  }
  
  public static boolean isOplusCustomDirectoryEmpty() {
    File file = getOplusCustomDirectory();
    File[] arrayOfFile = file.listFiles();
    if (arrayOfFile != null && arrayOfFile.length > 0)
      return false; 
    return true;
  }
  
  public static boolean isOplusCotaDirectoryEmpty() {
    File file = getOplusCotaDirectory();
    File[] arrayOfFile = file.listFiles();
    if (arrayOfFile != null && arrayOfFile.length > 0)
      return false; 
    return true;
  }
  
  public static File getResourceDirectory() {
    if (!isOplusCustomDirectoryEmpty())
      return getOplusCustomDirectory(); 
    if (!isOplusCotaDirectoryEmpty())
      return getOplusCotaDirectory(); 
    return getOplusCustomDirectory();
  }
  
  public static File getOplusProductDirectory() {
    if (DIR_OPPO_PRODUCT_ROOT == null)
      DIR_OPPO_PRODUCT_ROOT = getDirectorySup("OPPO_PRODUCT_ROOT", "/oppo_product"); 
    return DIR_OPPO_PRODUCT_ROOT;
  }
  
  public static File getOplusEngineerDirectory() {
    if (DIR_OPPO_ENGINEER_ROOT == null)
      DIR_OPPO_ENGINEER_ROOT = getDirectorySup("OPPO_ENGINEER_ROOT", "/oppo_engineering"); 
    return DIR_OPPO_ENGINEER_ROOT;
  }
  
  public static File getOplusVersionDirectory() {
    if (DIR_OPPO_VERSION_ROOT == null)
      DIR_OPPO_VERSION_ROOT = getDirectorySup("OPPO_VERSION_ROOT", "/oppo_version"); 
    return DIR_OPPO_VERSION_ROOT;
  }
  
  public static File getMyPreloadDirectory() {
    if (DIR_MY_PRELOAD_ROOT == null)
      DIR_MY_PRELOAD_ROOT = getDirectorySup("MY_PRELOAD_ROOT", "/my_preload"); 
    return DIR_MY_PRELOAD_ROOT;
  }
  
  public static File getMyHeytapDirectory() {
    if (DIR_MY_HEYTAP_ROOT == null)
      DIR_MY_HEYTAP_ROOT = getDirectorySup("MY_HEYTAP_ROOT", "/my_heytap"); 
    return DIR_MY_HEYTAP_ROOT;
  }
  
  public static File getMyStockDirectory() {
    if (DIR_MY_STOCK_ROOT == null)
      DIR_MY_STOCK_ROOT = getDirectorySup("MY_STOCK_ROOT", "/my_stock"); 
    return DIR_MY_STOCK_ROOT;
  }
  
  public static File getMyProductDirectory() {
    if (DIR_MY_PRODUCT_ROOT == null)
      DIR_MY_PRODUCT_ROOT = getDirectorySup("MY_PRODUCT_ROOT", "/my_product"); 
    return DIR_MY_PRODUCT_ROOT;
  }
  
  public static File getMyCountryDirectory() {
    if (DIR_MY_COUNTRY_ROOT == null)
      DIR_MY_COUNTRY_ROOT = getDirectorySup("MY_REGION_ROOT", "/my_region"); 
    return DIR_MY_COUNTRY_ROOT;
  }
  
  public static File getMyRegionDirectory() {
    if (DIR_MY_REGION_ROOT == null)
      DIR_MY_REGION_ROOT = getDirectorySup("MY_REGION_ROOT", "/my_region"); 
    return DIR_MY_REGION_ROOT;
  }
  
  public static File getMyOperatorDirectory() {
    if (DIR_MY_OPERATOR_ROOT == null)
      DIR_MY_OPERATOR_ROOT = getDirectorySup("MY_CARRIER_ROOT", "/my_carrier"); 
    return DIR_MY_OPERATOR_ROOT;
  }
  
  public static File getMyCarrierDirectory() {
    if (DIR_MY_CARRIER_ROOT == null)
      DIR_MY_CARRIER_ROOT = getDirectorySup("MY_CARRIER_ROOT", "/my_carrier"); 
    return DIR_MY_CARRIER_ROOT;
  }
  
  public static File getMyCompanyDirectory() {
    if (DIR_MY_COMPANY_ROOT == null)
      DIR_MY_COMPANY_ROOT = getDirectorySup("MY_COMPANY_ROOT", "/my_company"); 
    return DIR_MY_COMPANY_ROOT;
  }
  
  public static File getMyEngineeringDirectory() {
    if (DIR_MY_ENGINEER_ROOT == null)
      DIR_MY_ENGINEER_ROOT = getDirectorySup("MY_ENGINEERING_ROOT", "/my_engineering"); 
    return DIR_MY_ENGINEER_ROOT;
  }
}
