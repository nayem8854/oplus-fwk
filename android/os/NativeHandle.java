package android.os;

import android.annotation.SystemApi;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import java.io.Closeable;
import java.io.FileDescriptor;
import java.io.IOException;

@SystemApi
public final class NativeHandle implements Closeable {
  private FileDescriptor[] mFds;
  
  private int[] mInts;
  
  private boolean mOwn = false;
  
  public NativeHandle() {
    this(new FileDescriptor[0], new int[0], false);
  }
  
  public NativeHandle(FileDescriptor paramFileDescriptor, boolean paramBoolean) {
    this(new FileDescriptor[] { paramFileDescriptor }, new int[0], paramBoolean);
  }
  
  private static FileDescriptor[] createFileDescriptorArray(int[] paramArrayOfint) {
    FileDescriptor[] arrayOfFileDescriptor = new FileDescriptor[paramArrayOfint.length];
    for (byte b = 0; b < paramArrayOfint.length; b++) {
      FileDescriptor fileDescriptor = new FileDescriptor();
      fileDescriptor.setInt$(paramArrayOfint[b]);
      arrayOfFileDescriptor[b] = fileDescriptor;
    } 
    return arrayOfFileDescriptor;
  }
  
  private NativeHandle(int[] paramArrayOfint1, int[] paramArrayOfint2, boolean paramBoolean) {
    this(createFileDescriptorArray(paramArrayOfint1), paramArrayOfint2, paramBoolean);
  }
  
  public NativeHandle(FileDescriptor[] paramArrayOfFileDescriptor, int[] paramArrayOfint, boolean paramBoolean) {
    this.mFds = (FileDescriptor[])paramArrayOfFileDescriptor.clone();
    this.mInts = (int[])paramArrayOfint.clone();
    this.mOwn = paramBoolean;
  }
  
  public boolean hasSingleFileDescriptor() {
    checkOpen();
    int i = this.mFds.length;
    boolean bool = true;
    if (i != 1 || this.mInts.length != 0)
      bool = false; 
    return bool;
  }
  
  public NativeHandle dup() throws IOException {
    FileDescriptor[] arrayOfFileDescriptor = new FileDescriptor[this.mFds.length];
    byte b = 0;
    try {
      for (; b < this.mFds.length; b++) {
        FileDescriptor fileDescriptor = new FileDescriptor();
        this();
        int i = Os.fcntlInt(this.mFds[b], OsConstants.F_DUPFD_CLOEXEC, 0);
        fileDescriptor.setInt$(i);
        arrayOfFileDescriptor[b] = fileDescriptor;
      } 
    } catch (ErrnoException errnoException) {
      errnoException.rethrowAsIOException();
    } 
    return new NativeHandle(arrayOfFileDescriptor, this.mInts, true);
  }
  
  private void checkOpen() {
    if (this.mFds != null)
      return; 
    throw new IllegalStateException("NativeHandle is invalidated after close.");
  }
  
  public void close() throws IOException {
    checkOpen();
    if (this.mOwn) {
      try {
        for (FileDescriptor fileDescriptor : this.mFds)
          Os.close(fileDescriptor); 
      } catch (ErrnoException errnoException) {
        errnoException.rethrowAsIOException();
      } 
      this.mOwn = false;
    } 
    this.mFds = null;
    this.mInts = null;
  }
  
  public FileDescriptor getFileDescriptor() {
    checkOpen();
    if (hasSingleFileDescriptor())
      return this.mFds[0]; 
    throw new IllegalStateException("NativeHandle is not single file descriptor. Contents must be retreived through getFileDescriptors and getInts.");
  }
  
  private int[] getFdsAsIntArray() {
    checkOpen();
    int i = this.mFds.length;
    int[] arrayOfInt = new int[i];
    for (byte b = 0; b < i; b++)
      arrayOfInt[b] = this.mFds[b].getInt$(); 
    return arrayOfInt;
  }
  
  public FileDescriptor[] getFileDescriptors() {
    checkOpen();
    return this.mFds;
  }
  
  public int[] getInts() {
    checkOpen();
    return this.mInts;
  }
}
