package android.os;

import android.util.proto.ProtoOutputStream;

public class PatternMatcher implements Parcelable {
  public static final Parcelable.Creator<PatternMatcher> CREATOR;
  
  private static final int MAX_PATTERN_STORAGE = 2048;
  
  private static final int NO_MATCH = -1;
  
  private static final int PARSED_MODIFIER_ONE_OR_MORE = -8;
  
  private static final int PARSED_MODIFIER_RANGE_START = -5;
  
  private static final int PARSED_MODIFIER_RANGE_STOP = -6;
  
  private static final int PARSED_MODIFIER_ZERO_OR_MORE = -7;
  
  private static final int PARSED_TOKEN_CHAR_ANY = -4;
  
  private static final int PARSED_TOKEN_CHAR_SET_INVERSE_START = -2;
  
  private static final int PARSED_TOKEN_CHAR_SET_START = -1;
  
  private static final int PARSED_TOKEN_CHAR_SET_STOP = -3;
  
  public static final int PATTERN_ADVANCED_GLOB = 3;
  
  public static final int PATTERN_LITERAL = 0;
  
  public static final int PATTERN_PREFIX = 1;
  
  public static final int PATTERN_SIMPLE_GLOB = 2;
  
  private static final String TAG = "PatternMatcher";
  
  private static final int TOKEN_TYPE_ANY = 1;
  
  private static final int TOKEN_TYPE_INVERSE_SET = 3;
  
  private static final int TOKEN_TYPE_LITERAL = 0;
  
  private static final int TOKEN_TYPE_SET = 2;
  
  private static final int[] sParsedPatternScratch = new int[2048];
  
  private final int[] mParsedPattern;
  
  private final String mPattern;
  
  private final int mType;
  
  public PatternMatcher(String paramString, int paramInt) {
    this.mPattern = paramString;
    this.mType = paramInt;
    if (paramInt == 3) {
      this.mParsedPattern = parseAndVerifyAdvancedPattern(paramString);
    } else {
      this.mParsedPattern = null;
    } 
  }
  
  public final String getPath() {
    return this.mPattern;
  }
  
  public final int getType() {
    return this.mType;
  }
  
  public boolean match(String paramString) {
    return matchPattern(paramString, this.mPattern, this.mParsedPattern, this.mType);
  }
  
  public String toString() {
    String str = "? ";
    int i = this.mType;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i == 3)
            str = "ADVANCED: "; 
        } else {
          str = "GLOB: ";
        } 
      } else {
        str = "PREFIX: ";
      } 
    } else {
      str = "LITERAL: ";
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PatternMatcher{");
    stringBuilder.append(str);
    stringBuilder.append(this.mPattern);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1138166333441L, this.mPattern);
    paramProtoOutputStream.write(1159641169922L, this.mType);
    paramProtoOutputStream.end(paramLong);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mPattern);
    paramParcel.writeInt(this.mType);
    paramParcel.writeIntArray(this.mParsedPattern);
  }
  
  public PatternMatcher(Parcel paramParcel) {
    this.mPattern = paramParcel.readString();
    this.mType = paramParcel.readInt();
    this.mParsedPattern = paramParcel.createIntArray();
  }
  
  static {
    CREATOR = new Parcelable.Creator<PatternMatcher>() {
        public PatternMatcher createFromParcel(Parcel param1Parcel) {
          return new PatternMatcher(param1Parcel);
        }
        
        public PatternMatcher[] newArray(int param1Int) {
          return new PatternMatcher[param1Int];
        }
      };
  }
  
  static boolean matchPattern(String paramString1, String paramString2, int[] paramArrayOfint, int paramInt) {
    if (paramString1 == null)
      return false; 
    if (paramInt == 0)
      return paramString2.equals(paramString1); 
    if (paramInt == 1)
      return paramString1.startsWith(paramString2); 
    if (paramInt == 2)
      return matchGlobPattern(paramString2, paramString1); 
    if (paramInt == 3)
      return matchAdvancedPattern(paramArrayOfint, paramString1); 
    return false;
  }
  
  static boolean matchGlobPattern(String paramString1, String paramString2) {
    int k, n, i = paramString1.length();
    boolean bool = false;
    if (i <= 0) {
      if (paramString2.length() <= 0)
        bool = true; 
      return bool;
    } 
    int j = paramString2.length();
    int m = 0, i1 = 0;
    int i2 = paramString1.charAt(0);
    while (m < i && i1 < j) {
      int i3, i6, i7, i4 = i2;
      int i5 = m + 1;
      if (i5 < i) {
        i6 = paramString1.charAt(i5);
      } else {
        i6 = 0;
      } 
      if (i4 == 92) {
        i7 = 1;
      } else {
        i7 = 0;
      } 
      m = i5;
      i2 = i6;
      if (i7) {
        i5++;
        if (i5 < i) {
          m = paramString1.charAt(i5);
        } else {
          m = 0;
        } 
        i4 = i6;
        i3 = m;
        m = i5;
      } 
      if (i3 == 42) {
        i3 = i1;
        if (!i7) {
          i3 = i1;
          if (i4 == 46) {
            int i10;
            if (m >= i - 1)
              return true; 
            i7 = m + 1;
            i4 = paramString1.charAt(i7);
            i3 = i7;
            m = i1;
            i6 = i4;
            if (i4 == 92) {
              i3 = i7 + 1;
              if (i3 < i) {
                m = paramString1.charAt(i3);
              } else {
                m = 0;
              } 
              i6 = m;
              i10 = i1;
            } 
            while (paramString2.charAt(i10) != i6) {
              n = i10 + 1;
              k = n;
              if (n >= j) {
                k = n;
                break;
              } 
            } 
            if (k == j)
              return false; 
            int i11 = i3 + 1;
            if (i11 < i) {
              n = paramString1.charAt(i11);
            } else {
              n = 0;
            } 
            i8 = n;
            n = k + 1;
            k = i11;
            continue;
          } 
        } 
        do {
          if (paramString2.charAt(i8) != i4) {
            int i10 = i8;
            break;
          } 
          n = i8 + 1;
          i8 = n;
        } while (n < j);
        int i9 = k + 1;
        if (i9 < i) {
          k = paramString1.charAt(i9);
        } else {
          k = 0;
        } 
        int i8 = k;
        k = i9;
        continue;
      } 
      if (i4 != 46 && paramString2.charAt(n) != i4)
        return false; 
      n++;
    } 
    if (k >= i && n >= j)
      return true; 
    if (k == i - 2 && paramString1.charAt(k) == '.' && 
      paramString1.charAt(k + 1) == '*')
      return true; 
    return false;
  }
  
  static int[] parseAndVerifyAdvancedPattern(String paramString) {
    // Byte code:
    //   0: ldc android/os/PatternMatcher
    //   2: monitorenter
    //   3: iconst_0
    //   4: istore_1
    //   5: aload_0
    //   6: invokevirtual length : ()I
    //   9: istore_2
    //   10: iconst_0
    //   11: istore_3
    //   12: iconst_0
    //   13: istore #4
    //   15: iconst_0
    //   16: istore #5
    //   18: iconst_0
    //   19: istore #6
    //   21: iload_1
    //   22: iload_2
    //   23: if_icmpge -> 880
    //   26: iload_3
    //   27: sipush #2045
    //   30: if_icmpgt -> 868
    //   33: aload_0
    //   34: iload_1
    //   35: invokevirtual charAt : (I)C
    //   38: istore #7
    //   40: iconst_0
    //   41: istore #8
    //   43: iload #7
    //   45: bipush #42
    //   47: if_icmpeq -> 454
    //   50: iload #7
    //   52: bipush #43
    //   54: if_icmpeq -> 398
    //   57: iload #7
    //   59: bipush #46
    //   61: if_icmpeq -> 371
    //   64: iload #7
    //   66: bipush #123
    //   68: if_icmpeq -> 309
    //   71: iload #7
    //   73: bipush #125
    //   75: if_icmpeq -> 279
    //   78: iload #7
    //   80: tableswitch default -> 108, 91 -> 222, 92 -> 187, 93 -> 114
    //   108: iconst_1
    //   109: istore #8
    //   111: goto -> 510
    //   114: iload #4
    //   116: ifne -> 125
    //   119: iconst_1
    //   120: istore #8
    //   122: goto -> 510
    //   125: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   128: iload_3
    //   129: iconst_1
    //   130: isub
    //   131: iaload
    //   132: istore #6
    //   134: iload #6
    //   136: iconst_m1
    //   137: if_icmpeq -> 175
    //   140: iload #6
    //   142: bipush #-2
    //   144: if_icmpeq -> 175
    //   147: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   150: astore #9
    //   152: iload_3
    //   153: iconst_1
    //   154: iadd
    //   155: istore #10
    //   157: aload #9
    //   159: iload_3
    //   160: bipush #-3
    //   162: iastore
    //   163: iconst_0
    //   164: istore #6
    //   166: iconst_0
    //   167: istore #4
    //   169: iload #10
    //   171: istore_3
    //   172: goto -> 510
    //   175: new java/lang/IllegalArgumentException
    //   178: astore_0
    //   179: aload_0
    //   180: ldc 'You must define characters in a set.'
    //   182: invokespecial <init> : (Ljava/lang/String;)V
    //   185: aload_0
    //   186: athrow
    //   187: iload_1
    //   188: iconst_1
    //   189: iadd
    //   190: iload_2
    //   191: if_icmpge -> 210
    //   194: iinc #1, 1
    //   197: aload_0
    //   198: iload_1
    //   199: invokevirtual charAt : (I)C
    //   202: istore #7
    //   204: iconst_1
    //   205: istore #8
    //   207: goto -> 510
    //   210: new java/lang/IllegalArgumentException
    //   213: astore_0
    //   214: aload_0
    //   215: ldc 'Escape found at end of pattern!'
    //   217: invokespecial <init> : (Ljava/lang/String;)V
    //   220: aload_0
    //   221: athrow
    //   222: iload #4
    //   224: ifeq -> 233
    //   227: iconst_1
    //   228: istore #8
    //   230: goto -> 510
    //   233: aload_0
    //   234: iload_1
    //   235: iconst_1
    //   236: iadd
    //   237: invokevirtual charAt : (I)C
    //   240: bipush #94
    //   242: if_icmpne -> 261
    //   245: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   248: iload_3
    //   249: bipush #-2
    //   251: iastore
    //   252: iinc #1, 1
    //   255: iinc #3, 1
    //   258: goto -> 270
    //   261: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   264: iload_3
    //   265: iconst_m1
    //   266: iastore
    //   267: iinc #3, 1
    //   270: iinc #1, 1
    //   273: iconst_1
    //   274: istore #4
    //   276: goto -> 21
    //   279: iload #5
    //   281: ifeq -> 510
    //   284: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   287: astore #9
    //   289: iload_3
    //   290: iconst_1
    //   291: iadd
    //   292: istore #10
    //   294: aload #9
    //   296: iload_3
    //   297: bipush #-6
    //   299: iastore
    //   300: iconst_0
    //   301: istore #5
    //   303: iload #10
    //   305: istore_3
    //   306: goto -> 510
    //   309: iload #4
    //   311: ifne -> 510
    //   314: iload_3
    //   315: ifeq -> 359
    //   318: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   321: iload_3
    //   322: iconst_1
    //   323: isub
    //   324: iaload
    //   325: invokestatic isParsedModifier : (I)Z
    //   328: ifne -> 359
    //   331: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   334: astore #9
    //   336: iload_3
    //   337: iconst_1
    //   338: iadd
    //   339: istore #10
    //   341: aload #9
    //   343: iload_3
    //   344: bipush #-5
    //   346: iastore
    //   347: iconst_1
    //   348: istore #5
    //   350: iinc #1, 1
    //   353: iload #10
    //   355: istore_3
    //   356: goto -> 510
    //   359: new java/lang/IllegalArgumentException
    //   362: astore_0
    //   363: aload_0
    //   364: ldc 'Modifier must follow a token.'
    //   366: invokespecial <init> : (Ljava/lang/String;)V
    //   369: aload_0
    //   370: athrow
    //   371: iload #4
    //   373: ifne -> 510
    //   376: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   379: astore #9
    //   381: iload_3
    //   382: iconst_1
    //   383: iadd
    //   384: istore #10
    //   386: aload #9
    //   388: iload_3
    //   389: bipush #-4
    //   391: iastore
    //   392: iload #10
    //   394: istore_3
    //   395: goto -> 510
    //   398: iload #4
    //   400: ifne -> 510
    //   403: iload_3
    //   404: ifeq -> 442
    //   407: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   410: iload_3
    //   411: iconst_1
    //   412: isub
    //   413: iaload
    //   414: invokestatic isParsedModifier : (I)Z
    //   417: ifne -> 442
    //   420: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   423: astore #9
    //   425: iload_3
    //   426: iconst_1
    //   427: iadd
    //   428: istore #10
    //   430: aload #9
    //   432: iload_3
    //   433: bipush #-8
    //   435: iastore
    //   436: iload #10
    //   438: istore_3
    //   439: goto -> 510
    //   442: new java/lang/IllegalArgumentException
    //   445: astore_0
    //   446: aload_0
    //   447: ldc 'Modifier must follow a token.'
    //   449: invokespecial <init> : (Ljava/lang/String;)V
    //   452: aload_0
    //   453: athrow
    //   454: iload #4
    //   456: ifne -> 510
    //   459: iload_3
    //   460: ifeq -> 498
    //   463: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   466: iload_3
    //   467: iconst_1
    //   468: isub
    //   469: iaload
    //   470: invokestatic isParsedModifier : (I)Z
    //   473: ifne -> 498
    //   476: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   479: astore #9
    //   481: iload_3
    //   482: iconst_1
    //   483: iadd
    //   484: istore #10
    //   486: aload #9
    //   488: iload_3
    //   489: bipush #-7
    //   491: iastore
    //   492: iload #10
    //   494: istore_3
    //   495: goto -> 510
    //   498: new java/lang/IllegalArgumentException
    //   501: astore_0
    //   502: aload_0
    //   503: ldc 'Modifier must follow a token.'
    //   505: invokespecial <init> : (Ljava/lang/String;)V
    //   508: aload_0
    //   509: athrow
    //   510: iload #4
    //   512: ifeq -> 640
    //   515: iload #6
    //   517: ifeq -> 545
    //   520: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   523: astore #9
    //   525: iload_3
    //   526: iconst_1
    //   527: iadd
    //   528: istore #6
    //   530: aload #9
    //   532: iload_3
    //   533: iload #7
    //   535: iastore
    //   536: iconst_0
    //   537: istore #8
    //   539: iload #6
    //   541: istore_3
    //   542: goto -> 858
    //   545: iload_1
    //   546: iconst_2
    //   547: iadd
    //   548: iload_2
    //   549: if_icmpge -> 604
    //   552: aload_0
    //   553: iload_1
    //   554: iconst_1
    //   555: iadd
    //   556: invokevirtual charAt : (I)C
    //   559: bipush #45
    //   561: if_icmpne -> 604
    //   564: aload_0
    //   565: iload_1
    //   566: iconst_2
    //   567: iadd
    //   568: invokevirtual charAt : (I)C
    //   571: bipush #93
    //   573: if_icmpeq -> 604
    //   576: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   579: astore #9
    //   581: iload_3
    //   582: iconst_1
    //   583: iadd
    //   584: istore #6
    //   586: aload #9
    //   588: iload_3
    //   589: iload #7
    //   591: iastore
    //   592: iinc #1, 1
    //   595: iconst_1
    //   596: istore #8
    //   598: iload #6
    //   600: istore_3
    //   601: goto -> 858
    //   604: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   607: astore #9
    //   609: iload_3
    //   610: iconst_1
    //   611: iadd
    //   612: istore #8
    //   614: aload #9
    //   616: iload_3
    //   617: iload #7
    //   619: iastore
    //   620: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   623: iload #8
    //   625: iload #7
    //   627: iastore
    //   628: iload #8
    //   630: iconst_1
    //   631: iadd
    //   632: istore_3
    //   633: iload #6
    //   635: istore #8
    //   637: goto -> 858
    //   640: iload #5
    //   642: ifeq -> 823
    //   645: aload_0
    //   646: bipush #125
    //   648: iload_1
    //   649: invokevirtual indexOf : (II)I
    //   652: istore #7
    //   654: iload #7
    //   656: iflt -> 811
    //   659: aload_0
    //   660: iload_1
    //   661: iload #7
    //   663: invokevirtual substring : (II)Ljava/lang/String;
    //   666: astore #9
    //   668: aload #9
    //   670: bipush #44
    //   672: invokevirtual indexOf : (I)I
    //   675: istore_1
    //   676: iload_1
    //   677: ifge -> 693
    //   680: aload #9
    //   682: invokestatic parseInt : (Ljava/lang/String;)I
    //   685: istore #8
    //   687: iload #8
    //   689: istore_1
    //   690: goto -> 734
    //   693: aload #9
    //   695: iconst_0
    //   696: iload_1
    //   697: invokevirtual substring : (II)Ljava/lang/String;
    //   700: invokestatic parseInt : (Ljava/lang/String;)I
    //   703: istore #8
    //   705: iload_1
    //   706: aload #9
    //   708: invokevirtual length : ()I
    //   711: iconst_1
    //   712: isub
    //   713: if_icmpne -> 722
    //   716: ldc 2147483647
    //   718: istore_1
    //   719: goto -> 734
    //   722: aload #9
    //   724: iload_1
    //   725: iconst_1
    //   726: iadd
    //   727: invokevirtual substring : (I)Ljava/lang/String;
    //   730: invokestatic parseInt : (Ljava/lang/String;)I
    //   733: istore_1
    //   734: iload #8
    //   736: iload_1
    //   737: if_icmpgt -> 782
    //   740: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   743: astore #9
    //   745: iload_3
    //   746: iconst_1
    //   747: iadd
    //   748: istore #10
    //   750: aload #9
    //   752: iload_3
    //   753: iload #8
    //   755: iastore
    //   756: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   759: astore #9
    //   761: aload #9
    //   763: iload #10
    //   765: iload_1
    //   766: iastore
    //   767: iload #7
    //   769: istore_1
    //   770: iload #10
    //   772: iconst_1
    //   773: iadd
    //   774: istore_3
    //   775: goto -> 21
    //   778: astore_0
    //   779: goto -> 795
    //   782: new java/lang/IllegalArgumentException
    //   785: astore_0
    //   786: aload_0
    //   787: ldc 'Range quantifier minimum is greater than maximum'
    //   789: invokespecial <init> : (Ljava/lang/String;)V
    //   792: aload_0
    //   793: athrow
    //   794: astore_0
    //   795: new java/lang/IllegalArgumentException
    //   798: astore #9
    //   800: aload #9
    //   802: ldc 'Range number format incorrect'
    //   804: aload_0
    //   805: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   808: aload #9
    //   810: athrow
    //   811: new java/lang/IllegalArgumentException
    //   814: astore_0
    //   815: aload_0
    //   816: ldc 'Range not ended with '}''
    //   818: invokespecial <init> : (Ljava/lang/String;)V
    //   821: aload_0
    //   822: athrow
    //   823: iload #8
    //   825: ifeq -> 854
    //   828: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   831: astore #9
    //   833: iload_3
    //   834: iconst_1
    //   835: iadd
    //   836: istore #10
    //   838: aload #9
    //   840: iload_3
    //   841: iload #7
    //   843: iastore
    //   844: iload #6
    //   846: istore #8
    //   848: iload #10
    //   850: istore_3
    //   851: goto -> 858
    //   854: iload #6
    //   856: istore #8
    //   858: iinc #1, 1
    //   861: iload #8
    //   863: istore #6
    //   865: goto -> 21
    //   868: new java/lang/IllegalArgumentException
    //   871: astore_0
    //   872: aload_0
    //   873: ldc 'Pattern is too large!'
    //   875: invokespecial <init> : (Ljava/lang/String;)V
    //   878: aload_0
    //   879: athrow
    //   880: iload #4
    //   882: ifne -> 898
    //   885: getstatic android/os/PatternMatcher.sParsedPatternScratch : [I
    //   888: iload_3
    //   889: invokestatic copyOf : ([II)[I
    //   892: astore_0
    //   893: ldc android/os/PatternMatcher
    //   895: monitorexit
    //   896: aload_0
    //   897: areturn
    //   898: new java/lang/IllegalArgumentException
    //   901: astore_0
    //   902: aload_0
    //   903: ldc 'Set was not terminated!'
    //   905: invokespecial <init> : (Ljava/lang/String;)V
    //   908: aload_0
    //   909: athrow
    //   910: astore_0
    //   911: ldc android/os/PatternMatcher
    //   913: monitorexit
    //   914: aload_0
    //   915: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #272	-> 3
    //   #273	-> 5
    //   #275	-> 10
    //   #277	-> 12
    //   #278	-> 15
    //   #279	-> 18
    //   #283	-> 21
    //   #284	-> 26
    //   #288	-> 33
    //   #289	-> 40
    //   #291	-> 43
    //   #367	-> 108
    //   #308	-> 114
    //   #309	-> 119
    //   #311	-> 125
    //   #312	-> 134
    //   #317	-> 147
    //   #318	-> 163
    //   #319	-> 163
    //   #321	-> 163
    //   #314	-> 175
    //   #360	-> 187
    //   #363	-> 194
    //   #364	-> 204
    //   #365	-> 207
    //   #361	-> 210
    //   #293	-> 222
    //   #294	-> 227
    //   #296	-> 233
    //   #297	-> 245
    //   #298	-> 252
    //   #300	-> 261
    //   #302	-> 270
    //   #303	-> 273
    //   #304	-> 276
    //   #333	-> 279
    //   #334	-> 284
    //   #335	-> 300
    //   #323	-> 309
    //   #324	-> 314
    //   #327	-> 331
    //   #328	-> 347
    //   #329	-> 347
    //   #325	-> 359
    //   #355	-> 371
    //   #356	-> 376
    //   #347	-> 398
    //   #348	-> 403
    //   #351	-> 420
    //   #349	-> 442
    //   #339	-> 454
    //   #340	-> 459
    //   #343	-> 476
    //   #341	-> 498
    //   #370	-> 510
    //   #371	-> 515
    //   #372	-> 520
    //   #373	-> 536
    //   #376	-> 545
    //   #377	-> 552
    //   #378	-> 564
    //   #379	-> 576
    //   #380	-> 576
    //   #381	-> 592
    //   #383	-> 604
    //   #384	-> 620
    //   #387	-> 640
    //   #388	-> 645
    //   #389	-> 654
    //   #392	-> 659
    //   #393	-> 668
    //   #397	-> 676
    //   #398	-> 680
    //   #399	-> 687
    //   #400	-> 690
    //   #401	-> 693
    //   #402	-> 705
    //   #403	-> 716
    //   #405	-> 722
    //   #408	-> 734
    //   #412	-> 740
    //   #413	-> 756
    //   #416	-> 767
    //   #417	-> 767
    //   #418	-> 770
    //   #409	-> 782
    //   #414	-> 794
    //   #415	-> 795
    //   #390	-> 811
    //   #419	-> 823
    //   #420	-> 828
    //   #419	-> 854
    //   #422	-> 858
    //   #423	-> 861
    //   #285	-> 868
    //   #424	-> 880
    //   #427	-> 885
    //   #425	-> 898
    //   #271	-> 910
    // Exception table:
    //   from	to	target	type
    //   5	10	910	finally
    //   33	40	910	finally
    //   125	134	910	finally
    //   147	152	910	finally
    //   175	187	910	finally
    //   197	204	910	finally
    //   210	222	910	finally
    //   233	245	910	finally
    //   245	252	910	finally
    //   261	267	910	finally
    //   284	289	910	finally
    //   318	331	910	finally
    //   331	336	910	finally
    //   359	371	910	finally
    //   376	381	910	finally
    //   407	420	910	finally
    //   420	425	910	finally
    //   442	454	910	finally
    //   463	476	910	finally
    //   476	481	910	finally
    //   498	510	910	finally
    //   520	525	910	finally
    //   552	564	910	finally
    //   564	576	910	finally
    //   576	581	910	finally
    //   604	609	910	finally
    //   620	628	910	finally
    //   645	654	910	finally
    //   659	668	910	finally
    //   668	676	910	finally
    //   680	687	794	java/lang/NumberFormatException
    //   680	687	910	finally
    //   693	705	794	java/lang/NumberFormatException
    //   693	705	910	finally
    //   705	716	794	java/lang/NumberFormatException
    //   705	716	910	finally
    //   722	734	794	java/lang/NumberFormatException
    //   722	734	910	finally
    //   740	745	794	java/lang/NumberFormatException
    //   740	745	910	finally
    //   756	761	778	java/lang/NumberFormatException
    //   756	761	910	finally
    //   782	794	794	java/lang/NumberFormatException
    //   782	794	910	finally
    //   795	811	910	finally
    //   811	823	910	finally
    //   828	833	910	finally
    //   868	880	910	finally
    //   885	893	910	finally
    //   898	910	910	finally
  }
  
  private static boolean isParsedModifier(int paramInt) {
    return (paramInt == -8 || paramInt == -7 || paramInt == -6 || paramInt == -5);
  }
  
  static boolean matchAdvancedPattern(int[] paramArrayOfint, String paramString) {
    boolean bool1;
    int i = 0;
    int j = paramArrayOfint.length, k = paramString.length();
    int m = 0, n = 0, i1 = 0;
    while (true) {
      bool1 = false;
      if (i < j) {
        boolean bool;
        int i3, i2 = paramArrayOfint[i];
        if (i2 != -4) {
          if (i2 != -2 && i2 != -1) {
            m = i + 1;
            i2 = i;
            bool = false;
            i = m;
            i3 = n;
          } else {
            if (i2 == -1) {
              n = 2;
            } else {
              n = 3;
            } 
            m = i;
            while (true) {
              i3 = m + 1;
              if (i3 < j && paramArrayOfint[i3] != -3) {
                m = i3;
                continue;
              } 
              break;
            } 
            m = i3 + 1;
            i2 = i + 1;
            i3--;
            i = m;
            bool = n;
          } 
        } else {
          i++;
          bool = true;
          i3 = n;
          i2 = m;
        } 
        if (i >= j) {
          m = 1;
          n = 1;
        } else {
          n = paramArrayOfint[i];
          if (n != -8) {
            if (n != -7) {
              if (n != -5) {
                m = 1;
                n = 1;
              } else {
                m = paramArrayOfint[++i];
                n = paramArrayOfint[++i];
                i += 2;
              } 
            } else {
              i++;
              m = 0;
              n = Integer.MAX_VALUE;
            } 
          } else {
            i++;
            m = 1;
            n = Integer.MAX_VALUE;
          } 
        } 
        if (m > n)
          return false; 
        n = matchChars(paramString, i1, k, bool, m, n, paramArrayOfint, i2, i3);
        if (n == -1)
          return false; 
        i1 += n;
        m = i2;
        n = i3;
        continue;
      } 
      break;
    } 
    boolean bool2 = bool1;
    if (i >= j) {
      bool2 = bool1;
      if (i1 >= k)
        bool2 = true; 
    } 
    return bool2;
  }
  
  private static int matchChars(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int[] paramArrayOfint, int paramInt6, int paramInt7) {
    byte b = 0;
    while (b < paramInt5 && 
      matchChar(paramString, paramInt1 + b, paramInt2, paramInt3, paramArrayOfint, paramInt6, paramInt7))
      b++; 
    if (b < paramInt4) {
      paramInt1 = -1;
    } else {
      paramInt1 = b;
    } 
    return paramInt1;
  }
  
  private static boolean matchChar(String paramString, int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfint, int paramInt4, int paramInt5) {
    boolean bool = false;
    if (paramInt1 >= paramInt2)
      return false; 
    if (paramInt3 != 0) {
      if (paramInt3 != 1) {
        if (paramInt3 != 2) {
          if (paramInt3 != 3)
            return false; 
          for (paramInt2 = paramInt4; paramInt2 < paramInt5; paramInt2 += 2) {
            paramInt3 = paramString.charAt(paramInt1);
            if (paramInt3 >= paramArrayOfint[paramInt2] && paramInt3 <= paramArrayOfint[paramInt2 + 1])
              return false; 
          } 
          return true;
        } 
        for (paramInt2 = paramInt4; paramInt2 < paramInt5; paramInt2 += 2) {
          paramInt3 = paramString.charAt(paramInt1);
          if (paramInt3 >= paramArrayOfint[paramInt2] && paramInt3 <= paramArrayOfint[paramInt2 + 1])
            return true; 
        } 
        return false;
      } 
      return true;
    } 
    if (paramString.charAt(paramInt1) == paramArrayOfint[paramInt4])
      bool = true; 
    return bool;
  }
}
