package android.os;

import android.annotation.SystemApi;

@SystemApi
public interface IHwBinder {
  boolean linkToDeath(DeathRecipient paramDeathRecipient, long paramLong);
  
  IHwInterface queryLocalInterface(String paramString);
  
  void transact(int paramInt1, HwParcel paramHwParcel1, HwParcel paramHwParcel2, int paramInt2) throws RemoteException;
  
  boolean unlinkToDeath(DeathRecipient paramDeathRecipient);
  
  public static interface DeathRecipient {
    void serviceDied(long param1Long);
  }
}
