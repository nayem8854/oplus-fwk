package android.os;

public class TelephonyServiceManager {
  public static final class ServiceRegisterer {
    private final String mServiceName;
    
    public ServiceRegisterer(String param1String) {
      this.mServiceName = param1String;
    }
    
    public void register(IBinder param1IBinder) {
      ServiceManager.addService(this.mServiceName, param1IBinder);
    }
    
    public IBinder get() {
      return ServiceManager.getService(this.mServiceName);
    }
    
    public IBinder getOrThrow() throws TelephonyServiceManager.ServiceNotFoundException {
      try {
        return ServiceManager.getServiceOrThrow(this.mServiceName);
      } catch (ServiceNotFoundException serviceNotFoundException) {
        throw new TelephonyServiceManager.ServiceNotFoundException(this.mServiceName);
      } 
    }
    
    public IBinder tryGet() {
      return ServiceManager.checkService(this.mServiceName);
    }
  }
  
  class ServiceNotFoundException extends ServiceManager.ServiceNotFoundException {
    public ServiceNotFoundException(TelephonyServiceManager this$0) {
      super((String)this$0);
    }
  }
  
  public ServiceRegisterer getTelephonyServiceRegisterer() {
    return new ServiceRegisterer("phone");
  }
  
  public ServiceRegisterer getTelephonyImsServiceRegisterer() {
    return new ServiceRegisterer("telephony_ims");
  }
  
  public ServiceRegisterer getTelephonyRcsMessageServiceRegisterer() {
    return new ServiceRegisterer("ircsmessage");
  }
  
  public ServiceRegisterer getSubscriptionServiceRegisterer() {
    return new ServiceRegisterer("isub");
  }
  
  public ServiceRegisterer getPhoneSubServiceRegisterer() {
    return new ServiceRegisterer("iphonesubinfo");
  }
  
  public ServiceRegisterer getOpportunisticNetworkServiceRegisterer() {
    return new ServiceRegisterer("ions");
  }
  
  public ServiceRegisterer getCarrierConfigServiceRegisterer() {
    return new ServiceRegisterer("carrier_config");
  }
  
  public ServiceRegisterer getSmsServiceRegisterer() {
    return new ServiceRegisterer("isms");
  }
  
  public ServiceRegisterer getEuiccControllerService() {
    return new ServiceRegisterer("econtroller");
  }
  
  public ServiceRegisterer getEuiccCardControllerServiceRegisterer() {
    return new ServiceRegisterer("euicc_card_controller");
  }
  
  public ServiceRegisterer getIccPhoneBookServiceRegisterer() {
    return new ServiceRegisterer("simphonebook");
  }
}
