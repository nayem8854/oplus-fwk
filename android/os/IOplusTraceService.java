package android.os;

import com.oplus.onetrace.entities.TaskInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface IOplusTraceService extends IInterface {
  void callUpdateContentFilter(int paramInt, IOplusFilterListener paramIOplusFilterListener) throws RemoteException;
  
  boolean flushTraceBuffer() throws RemoteException;
  
  List<TaskInfo> getProcessTree() throws RemoteException;
  
  void handleTraceShmemBuffer(SharedMemory paramSharedMemory, int paramInt) throws RemoteException;
  
  SharedMemory obtainMemoryCache(String paramString, int paramInt, IOplusFilterListener paramIOplusFilterListener) throws RemoteException;
  
  SharedMemory obtainSharedMemory(String paramString) throws RemoteException;
  
  boolean registerCallBack(IOplusTraceCallBack paramIOplusTraceCallBack) throws RemoteException;
  
  void unregisterCallBack(IOplusTraceCallBack paramIOplusTraceCallBack) throws RemoteException;
  
  void updateContentFilterList(Map paramMap) throws RemoteException;
  
  void updateProcessWhitelist(String[] paramArrayOfString) throws RemoteException;
  
  void uploadProcessTree(int paramInt, String paramString, Map paramMap) throws RemoteException;
  
  class Default implements IOplusTraceService {
    public boolean registerCallBack(IOplusTraceCallBack param1IOplusTraceCallBack) throws RemoteException {
      return false;
    }
    
    public void unregisterCallBack(IOplusTraceCallBack param1IOplusTraceCallBack) throws RemoteException {}
    
    public void handleTraceShmemBuffer(SharedMemory param1SharedMemory, int param1Int) throws RemoteException {}
    
    public SharedMemory obtainSharedMemory(String param1String) throws RemoteException {
      return null;
    }
    
    public boolean flushTraceBuffer() throws RemoteException {
      return false;
    }
    
    public void updateProcessWhitelist(String[] param1ArrayOfString) throws RemoteException {}
    
    public SharedMemory obtainMemoryCache(String param1String, int param1Int, IOplusFilterListener param1IOplusFilterListener) throws RemoteException {
      return null;
    }
    
    public void updateContentFilterList(Map param1Map) throws RemoteException {}
    
    public void uploadProcessTree(int param1Int, String param1String, Map param1Map) throws RemoteException {}
    
    public List<TaskInfo> getProcessTree() throws RemoteException {
      return null;
    }
    
    public void callUpdateContentFilter(int param1Int, IOplusFilterListener param1IOplusFilterListener) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusTraceService {
    private static final String DESCRIPTOR = "android.os.IOplusTraceService";
    
    static final int TRANSACTION_callUpdateContentFilter = 11;
    
    static final int TRANSACTION_flushTraceBuffer = 5;
    
    static final int TRANSACTION_getProcessTree = 10;
    
    static final int TRANSACTION_handleTraceShmemBuffer = 3;
    
    static final int TRANSACTION_obtainMemoryCache = 7;
    
    static final int TRANSACTION_obtainSharedMemory = 4;
    
    static final int TRANSACTION_registerCallBack = 1;
    
    static final int TRANSACTION_unregisterCallBack = 2;
    
    static final int TRANSACTION_updateContentFilterList = 8;
    
    static final int TRANSACTION_updateProcessWhitelist = 6;
    
    static final int TRANSACTION_uploadProcessTree = 9;
    
    public Stub() {
      attachInterface(this, "android.os.IOplusTraceService");
    }
    
    public static IOplusTraceService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IOplusTraceService");
      if (iInterface != null && iInterface instanceof IOplusTraceService)
        return (IOplusTraceService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "callUpdateContentFilter";
        case 10:
          return "getProcessTree";
        case 9:
          return "uploadProcessTree";
        case 8:
          return "updateContentFilterList";
        case 7:
          return "obtainMemoryCache";
        case 6:
          return "updateProcessWhitelist";
        case 5:
          return "flushTraceBuffer";
        case 4:
          return "obtainSharedMemory";
        case 3:
          return "handleTraceShmemBuffer";
        case 2:
          return "unregisterCallBack";
        case 1:
          break;
      } 
      return "registerCallBack";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ClassLoader classLoader;
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int i;
        IOplusFilterListener iOplusFilterListener2;
        List<TaskInfo> list;
        HashMap hashMap;
        IOplusFilterListener iOplusFilterListener1;
        SharedMemory sharedMemory2;
        String arrayOfString[], str1;
        SharedMemory sharedMemory1;
        String str2;
        ClassLoader classLoader1;
        String str3;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.os.IOplusTraceService");
            param1Int1 = param1Parcel1.readInt();
            iOplusFilterListener2 = IOplusFilterListener.Stub.asInterface(param1Parcel1.readStrongBinder());
            callUpdateContentFilter(param1Int1, iOplusFilterListener2);
            return true;
          case 10:
            iOplusFilterListener2.enforceInterface("android.os.IOplusTraceService");
            list = getProcessTree();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 9:
            list.enforceInterface("android.os.IOplusTraceService");
            param1Int1 = list.readInt();
            str2 = list.readString();
            classLoader1 = getClass().getClassLoader();
            hashMap = list.readHashMap(classLoader1);
            uploadProcessTree(param1Int1, str2, hashMap);
            return true;
          case 8:
            hashMap.enforceInterface("android.os.IOplusTraceService");
            classLoader = getClass().getClassLoader();
            hashMap = hashMap.readHashMap(classLoader);
            updateContentFilterList(hashMap);
            return true;
          case 7:
            hashMap.enforceInterface("android.os.IOplusTraceService");
            str3 = hashMap.readString();
            param1Int1 = hashMap.readInt();
            iOplusFilterListener1 = IOplusFilterListener.Stub.asInterface(hashMap.readStrongBinder());
            sharedMemory2 = obtainMemoryCache(str3, param1Int1, iOplusFilterListener1);
            classLoader.writeNoException();
            if (sharedMemory2 != null) {
              classLoader.writeInt(1);
              sharedMemory2.writeToParcel((Parcel)classLoader, 1);
            } else {
              classLoader.writeInt(0);
            } 
            return true;
          case 6:
            sharedMemory2.enforceInterface("android.os.IOplusTraceService");
            arrayOfString = sharedMemory2.createStringArray();
            updateProcessWhitelist(arrayOfString);
            return true;
          case 5:
            arrayOfString.enforceInterface("android.os.IOplusTraceService");
            bool2 = flushTraceBuffer();
            classLoader.writeNoException();
            classLoader.writeInt(bool2);
            return true;
          case 4:
            arrayOfString.enforceInterface("android.os.IOplusTraceService");
            str1 = arrayOfString.readString();
            sharedMemory1 = obtainSharedMemory(str1);
            classLoader.writeNoException();
            if (sharedMemory1 != null) {
              classLoader.writeInt(1);
              sharedMemory1.writeToParcel((Parcel)classLoader, 1);
            } else {
              classLoader.writeInt(0);
            } 
            return true;
          case 3:
            sharedMemory1.enforceInterface("android.os.IOplusTraceService");
            if (sharedMemory1.readInt() != 0) {
              SharedMemory sharedMemory = SharedMemory.CREATOR.createFromParcel((Parcel)sharedMemory1);
            } else {
              str3 = null;
            } 
            i = sharedMemory1.readInt();
            handleTraceShmemBuffer((SharedMemory)str3, i);
            classLoader.writeNoException();
            return true;
          case 2:
            sharedMemory1.enforceInterface("android.os.IOplusTraceService");
            iOplusTraceCallBack = IOplusTraceCallBack.Stub.asInterface(sharedMemory1.readStrongBinder());
            unregisterCallBack(iOplusTraceCallBack);
            classLoader.writeNoException();
            return true;
          case 1:
            break;
        } 
        iOplusTraceCallBack.enforceInterface("android.os.IOplusTraceService");
        IOplusTraceCallBack iOplusTraceCallBack = IOplusTraceCallBack.Stub.asInterface(iOplusTraceCallBack.readStrongBinder());
        boolean bool1 = registerCallBack(iOplusTraceCallBack);
        classLoader.writeNoException();
        classLoader.writeInt(bool1);
        return true;
      } 
      classLoader.writeString("android.os.IOplusTraceService");
      return true;
    }
    
    private static class Proxy implements IOplusTraceService {
      public static IOplusTraceService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IOplusTraceService";
      }
      
      public boolean registerCallBack(IOplusTraceCallBack param2IOplusTraceCallBack) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusTraceService");
          if (param2IOplusTraceCallBack != null) {
            iBinder = param2IOplusTraceCallBack.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IOplusTraceService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusTraceService.Stub.getDefaultImpl().registerCallBack(param2IOplusTraceCallBack);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterCallBack(IOplusTraceCallBack param2IOplusTraceCallBack) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IOplusTraceService");
          if (param2IOplusTraceCallBack != null) {
            iBinder = param2IOplusTraceCallBack.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusTraceService.Stub.getDefaultImpl() != null) {
            IOplusTraceService.Stub.getDefaultImpl().unregisterCallBack(param2IOplusTraceCallBack);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void handleTraceShmemBuffer(SharedMemory param2SharedMemory, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusTraceService");
          if (param2SharedMemory != null) {
            parcel1.writeInt(1);
            param2SharedMemory.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusTraceService.Stub.getDefaultImpl() != null) {
            IOplusTraceService.Stub.getDefaultImpl().handleTraceShmemBuffer(param2SharedMemory, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SharedMemory obtainSharedMemory(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusTraceService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusTraceService.Stub.getDefaultImpl() != null)
            return IOplusTraceService.Stub.getDefaultImpl().obtainSharedMemory(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            SharedMemory sharedMemory = SharedMemory.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (SharedMemory)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean flushTraceBuffer() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusTraceService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IOplusTraceService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusTraceService.Stub.getDefaultImpl().flushTraceBuffer();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateProcessWhitelist(String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusTraceService");
          parcel.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IOplusTraceService.Stub.getDefaultImpl() != null) {
            IOplusTraceService.Stub.getDefaultImpl().updateProcessWhitelist(param2ArrayOfString);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public SharedMemory obtainMemoryCache(String param2String, int param2Int, IOplusFilterListener param2IOplusFilterListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IOplusTraceService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2IOplusFilterListener != null) {
            iBinder = param2IOplusFilterListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusTraceService.Stub.getDefaultImpl() != null)
            return IOplusTraceService.Stub.getDefaultImpl().obtainMemoryCache(param2String, param2Int, param2IOplusFilterListener); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            SharedMemory sharedMemory = SharedMemory.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (SharedMemory)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateContentFilterList(Map param2Map) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusTraceService");
          parcel.writeMap(param2Map);
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && IOplusTraceService.Stub.getDefaultImpl() != null) {
            IOplusTraceService.Stub.getDefaultImpl().updateContentFilterList(param2Map);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void uploadProcessTree(int param2Int, String param2String, Map param2Map) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusTraceService");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          parcel.writeMap(param2Map);
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && IOplusTraceService.Stub.getDefaultImpl() != null) {
            IOplusTraceService.Stub.getDefaultImpl().uploadProcessTree(param2Int, param2String, param2Map);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public List<TaskInfo> getProcessTree() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusTraceService");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusTraceService.Stub.getDefaultImpl() != null)
            return IOplusTraceService.Stub.getDefaultImpl().getProcessTree(); 
          parcel2.readException();
          return (List)parcel2.createTypedArrayList(TaskInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void callUpdateContentFilter(int param2Int, IOplusFilterListener param2IOplusFilterListener) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.os.IOplusTraceService");
          parcel.writeInt(param2Int);
          if (param2IOplusFilterListener != null) {
            iBinder = param2IOplusFilterListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && IOplusTraceService.Stub.getDefaultImpl() != null) {
            IOplusTraceService.Stub.getDefaultImpl().callUpdateContentFilter(param2Int, param2IOplusFilterListener);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusTraceService param1IOplusTraceService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusTraceService != null) {
          Proxy.sDefaultImpl = param1IOplusTraceService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusTraceService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
