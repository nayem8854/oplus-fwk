package android.os;

import android.annotation.SystemApi;

@SystemApi
public final class RemoteCallback implements Parcelable {
  public RemoteCallback(OnResultListener paramOnResultListener) {
    this(paramOnResultListener, null);
  }
  
  public RemoteCallback(OnResultListener paramOnResultListener, Handler paramHandler) {
    if (paramOnResultListener != null) {
      this.mListener = paramOnResultListener;
      this.mHandler = paramHandler;
      this.mCallback = (IRemoteCallback)new Object(this);
      return;
    } 
    throw new NullPointerException("listener cannot be null");
  }
  
  RemoteCallback(Parcel paramParcel) {
    this.mListener = null;
    this.mHandler = null;
    IBinder iBinder = paramParcel.readStrongBinder();
    this.mCallback = IRemoteCallback.Stub.asInterface(iBinder);
  }
  
  public void sendResult(Bundle paramBundle) {
    OnResultListener onResultListener = this.mListener;
    if (onResultListener != null) {
      Handler handler = this.mHandler;
      if (handler != null) {
        handler.post((Runnable)new Object(this, paramBundle));
      } else {
        onResultListener.onResult(paramBundle);
      } 
    } else {
      try {
        this.mCallback.sendResult(paramBundle);
      } catch (RemoteException remoteException) {}
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStrongBinder(this.mCallback.asBinder());
  }
  
  public static final Parcelable.Creator<RemoteCallback> CREATOR = new Parcelable.Creator<RemoteCallback>() {
      public RemoteCallback createFromParcel(Parcel param1Parcel) {
        return new RemoteCallback(param1Parcel);
      }
      
      public RemoteCallback[] newArray(int param1Int) {
        return new RemoteCallback[param1Int];
      }
    };
  
  private final IRemoteCallback mCallback;
  
  private final Handler mHandler;
  
  private final OnResultListener mListener;
  
  class OnResultListener {
    public abstract void onResult(Bundle param1Bundle);
  }
}
