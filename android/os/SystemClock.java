package android.os;

import android.app.IAlarmManager;
import android.util.Slog;
import dalvik.annotation.optimization.CriticalNative;
import java.time.Clock;
import java.time.DateTimeException;
import java.time.ZoneOffset;

public final class SystemClock {
  private static final String TAG = "SystemClock";
  
  public static void sleep(long paramLong) {
    long l1 = uptimeMillis();
    long l2 = paramLong;
    boolean bool = false;
    do {
      try {
        Thread.sleep(l2);
      } catch (InterruptedException interruptedException) {
        bool = true;
      } 
      l2 = l1 + paramLong - uptimeMillis();
    } while (l2 > 0L);
    if (bool)
      Thread.currentThread().interrupt(); 
  }
  
  public static boolean setCurrentTimeMillis(long paramLong) {
    IAlarmManager iAlarmManager = IAlarmManager.Stub.asInterface(ServiceManager.getService("alarm"));
    if (iAlarmManager == null) {
      Slog.e("SystemClock", "Unable to set RTC: mgr == null");
      return false;
    } 
    try {
      return iAlarmManager.setTime(paramLong);
    } catch (RemoteException remoteException) {
      Slog.e("SystemClock", "Unable to set RTC", (Throwable)remoteException);
    } catch (SecurityException securityException) {
      Slog.e("SystemClock", "Unable to set RTC", securityException);
    } 
    return false;
  }
  
  public static Clock uptimeClock() {
    return (Clock)new Object(ZoneOffset.UTC);
  }
  
  public static Clock elapsedRealtimeClock() {
    return (Clock)new Object(ZoneOffset.UTC);
  }
  
  public static long currentNetworkTimeMillis() {
    IAlarmManager iAlarmManager = IAlarmManager.Stub.asInterface(ServiceManager.getService("alarm"));
    if (iAlarmManager != null)
      try {
        return iAlarmManager.currentNetworkTimeMillis();
      } catch (ParcelableException parcelableException) {
        parcelableException.maybeRethrow(DateTimeException.class);
        throw new RuntimeException(parcelableException);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    throw new RuntimeException(new DeadSystemException());
  }
  
  public static Clock currentNetworkTimeClock() {
    return (Clock)new Object(ZoneOffset.UTC);
  }
  
  public static Clock currentGnssTimeClock() {
    return (Clock)new Object(ZoneOffset.UTC);
  }
  
  @CriticalNative
  public static native long currentThreadTimeMicro();
  
  @CriticalNative
  public static native long currentThreadTimeMillis();
  
  @CriticalNative
  public static native long currentTimeMicro();
  
  @CriticalNative
  public static native long elapsedRealtime();
  
  @CriticalNative
  public static native long elapsedRealtimeNanos();
  
  @CriticalNative
  public static native long uptimeMillis();
}
