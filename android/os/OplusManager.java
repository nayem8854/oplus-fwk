package android.os;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public final class OplusManager {
  public static final int ANDROID_MSG_INPUTMETHOD_FAILD = 1004;
  
  public static final int ANDROID_MSG_INSTALL_FAILD = 1003;
  
  public static final int ANDROID_MSG_LAUNCHACTIVITY = 1002;
  
  public static final int ANDROID_MSG_SKIPFRAMES = 1001;
  
  public static final String ANDROID_PANIC_TAG = "SYSTEM_SERVER";
  
  public static final String ANDROID_PANIC_TAG_BEGIN = "<android-panic-begin>\n";
  
  public static final String ANDROID_PANIC_TAG_END = "<android-panic-end>\n";
  
  public static final String ANDROID_TAG = "ANDROID";
  
  public static final String CAMERA_TAG = "CAMERA";
  
  public static final String CONNECT_TAG = "CONNECTIVITY";
  
  private static int DATA_SIZE = 16;
  
  private static final boolean DEBUG = true;
  
  public static final String DO_GR_CHECK_INTERNET = "DO_GR_CHECK_INTERNET";
  
  public static final String DO_GR_INSTALL_TALKBACK = "DO_GR_INSTALL_TALKBACK";
  
  public static final String DO_GR_TALKBACK_SUCC = "DO_GR_TALKBACK_SUCC";
  
  public static final String ENGINEERINGMODE_TEST_BEGIN = "<engineeringmode-test-begin>\n";
  
  public static final String ENGINEERINGMODE_TEST_END = "<engineeringmode-test-end>\n";
  
  public static final String ENGINEERINGMODE_TEST_TAG = "ENGINEERINGMODE_TEST";
  
  public static final String GMAP_PNAME = "com.google.android.apps.maps";
  
  private static final int INIT_TRY_TIMES = 3;
  
  public static final String ISSUE_ANDROID_ADSP_CRASH = "adsp_crash";
  
  public static final String ISSUE_ANDROID_AVERAGE_CURRENT_EVENT = "average_current_event";
  
  public static final String ISSUE_ANDROID_CHARGER_PLUGIN_625 = "charger_plugin";
  
  public static final String ISSUE_ANDROID_CHARGER_PLUGOUT_626 = "charger_plugout";
  
  public static final String ISSUE_ANDROID_CRASH = "crash";
  
  public static final String ISSUE_ANDROID_FP_DIE = "fp_die";
  
  public static final String ISSUE_ANDROID_FP_HW_ERROR = "fp_hw_error";
  
  public static final String ISSUE_ANDROID_FP_RESET_BYHM = "fp_reset_byhm";
  
  public static final String ISSUE_ANDROID_INPUTMETHOD_FAIL = "inputmethod_fail";
  
  public static final String ISSUE_ANDROID_INSTALL_FAIL = "install_fail";
  
  public static final String ISSUE_ANDROID_LAUNCH_ACTIVITY = "launch_activity";
  
  public static final String ISSUE_ANDROID_MODEM_CRASH = "modem_crash";
  
  public static final String ISSUE_ANDROID_OTA_UPGRADE = "ota_upgrade";
  
  public static final String ISSUE_ANDROID_PM_50 = "scan_event";
  
  public static final String ISSUE_ANDROID_PM_51 = "wifi_discounnect_event";
  
  public static final String ISSUE_ANDROID_PM_52 = "key_exchange_event";
  
  public static final String ISSUE_ANDROID_PM_53 = "dhcp_relet_event";
  
  public static final String ISSUE_ANDROID_PM_54 = "data_call_count";
  
  public static final String ISSUE_ANDROID_PM_55 = "no_service_time";
  
  public static final String ISSUE_ANDROID_PM_56 = "reselect_per_min";
  
  public static final String ISSUE_ANDROID_PM_57 = "sms_send_count";
  
  public static final String ISSUE_ANDROID_PM_58 = "background_music";
  
  public static final String ISSUE_ANDROID_PM_59 = "background_download";
  
  public static final String ISSUE_ANDROID_PM_60 = "wifi_wakeup";
  
  public static final String ISSUE_ANDROID_PM_61 = "modem_wakeup";
  
  public static final String ISSUE_ANDROID_PM_62 = "alarm_wakeup";
  
  public static final String ISSUE_ANDROID_PM_63 = "base_subsystem";
  
  public static final String ISSUE_ANDROID_PM_64 = "power_other";
  
  public static final String ISSUE_ANDROID_REBOOT_FROM_BLOCKED = "reboot_from_blocked";
  
  public static final String ISSUE_ANDROID_SKIP_FRAMES = "skip_frames";
  
  public static final String ISSUE_ANDROID_SYSTEM_REBOOT_FROM_BLOCKED = "system_server_reboot_from_blocked";
  
  public static final String ISSUE_ANDROID_VENUS_CRASH = "venus_crash";
  
  public static final String ISSUE_ANDROID_WCN_CRASH = "wcn_crash";
  
  public static final String ISSUE_KERNEL_HANG = "HANG";
  
  public static final String ISSUE_KERNEL_HARDWARE_REBOOT = "Hardware Reboot";
  
  public static final String ISSUE_KERNEL_HWT = "HWT";
  
  public static final String ISSUE_KERNEL_PANIC = "panic";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_AS_FAILED = "as_failed";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_AUTHENTICATION_REJECT = "authentication_reject";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_CARD_DROP_RX_BREAK = "card_drop_rx_break";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_CARD_DROP_TIME_OUT = "card_drop_time_out";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_DATA_NOT_ALLOWED = "data_no_allowed";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_DATA_NO_AVAILABLE_APN = "data_no_acailable_apn";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_DATA_SET_UP_DATA_ERROR = "data_set_up_data_error";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_GSM_T3126_EXPIRED = "gsm_t3126_expired";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_LTE_AS_FAILED = "lte_as_failed";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_LTE_REG_REJECT = "ltc_reg_reject";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_LTE_REG_WITHOUT_LTE = "lte_reg_without_lte";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_MCFG_ICCID_FAILED = "mcfg_iccid_failed";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_MO_DROP = "mo_drop";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_MT_CSFB = "mt_csfb";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_MT_PCH = "mt_pch";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_MT_RACH = "mt_rach";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_MT_REJECT = "mt_reject";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_MT_RLF = "mt_rlf";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_MT_RRC = "mt_rrc";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_REG_REJECT = "reg_rejetc";
  
  public static final String ISSUE_SYS_OEM_NW_DIAG_CAUSE_RF_MIPI_HW_FAILED = "rf_mipi_hw_failed";
  
  public static final String ISSUE_WIFI_CONNECTING_FAILURE = "wifi_connecting_failure";
  
  public static final String ISSUE_WIFI_LOAD_DRIVER_FAILURE = "wifi_load_driver_failure";
  
  public static final String ISSUE_WIFI_TURN_ON_OFF_FAILURE = "wifi_turn_on_off_failure";
  
  public static final String KERNEL_PANIC_TAG = "SYSTEM_LAST_KMSG";
  
  public static final String KERNEL_PANIC_TAG_BEGIN = "<kernel-panic-begin>\n";
  
  public static final String KERNEL_PANIC_TAG_END = "<kernel-panic-end>\n";
  
  public static final String KERNEL_TAG = "KERNEL";
  
  public static final String MULTIMEDIA_TAG = "MULTIMEDIA";
  
  public static final String NETWORK_TAG = "NETWORK";
  
  public static final String SERVICE_NAME = "OPPO";
  
  public static final String SHUTDOWN_TAG = "SYSTEM_SHUTDOWN";
  
  public static final String SHUTDOWN_TAG_BEGIN = "<shutdown-begin>\n";
  
  public static final String SHUTDOWN_TAG_END = "<shutdown-end>\n";
  
  public static final String SPMI_BEGIN = "<spmi-begin>\n";
  
  public static final String SPMI_END = "<spmi-end>\n";
  
  public static final String SPMI_TAG = "SPMI";
  
  public static final String TAG = "OplusManager";
  
  public static int TYEP_Android_VER = 0;
  
  public static int TYEP_BUILD_VER = 0;
  
  public static int TYEP_DEVICE = 0;
  
  public static int TYEP_PHONE_IMEI = 0;
  
  public static int TYPE_ANDROID_ADSP_CRASH = 0;
  
  public static int TYPE_ANDROID_AVERAGE_CURRENT_EVENT = 0;
  
  public static int TYPE_ANDROID_BACK_KEY = 0;
  
  public static int TYPE_ANDROID_CAMERA = 0;
  
  public static final int TYPE_ANDROID_CHARGER_PLUGIN_625 = 625;
  
  public static final int TYPE_ANDROID_CHARGER_PLUGOUT_626 = 626;
  
  public static int TYPE_ANDROID_CRASH = 0;
  
  public static int TYPE_ANDROID_FP_DIE = 0;
  
  public static int TYPE_ANDROID_FP_HW_ERROR = 0;
  
  public static int TYPE_ANDROID_FP_RESET_BYHM = 0;
  
  public static int TYPE_ANDROID_HOME_KEY = 0;
  
  public static int TYPE_ANDROID_INPUTMETHOD_FAIL = 0;
  
  public static int TYPE_ANDROID_INSTALL_FAILD = 0;
  
  public static int TYPE_ANDROID_LAUNCH_ACTIVITY = 0;
  
  public static int TYPE_ANDROID_MENU_KEY = 0;
  
  public static int TYPE_ANDROID_OTA_FAILD = 0;
  
  public static int TYPE_ANDROID_OTA_UPGRADE = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_50 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_51 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_52 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_53 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_54 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_55 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_56 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_57 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_58 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_59 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_60 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_61 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_62 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_63 = 0;
  
  public static int TYPE_ANDROID_PM_EVENT_64 = 0;
  
  public static int TYPE_ANDROID_POWER_KEY = 0;
  
  public static int TYPE_ANDROID_REBOOT_HANG = 0;
  
  public static int TYPE_ANDROID_REBOOT_HARDWARE_REBOOT = 0;
  
  public static int TYPE_ANDROID_REBOOT_HWT = 0;
  
  public static int TYPE_ANDROID_SKIPFRAMES = 0;
  
  public static int TYPE_ANDROID_SPMI = 0;
  
  public static int TYPE_ANDROID_SYSTEM_REBOOT_FROM_BLOCKED = 0;
  
  public static int TYPE_ANDROID_UNKNOWN_REBOOT = 0;
  
  public static int TYPE_ANDROID_USB = 0;
  
  public static int TYPE_ANDROID_VENUS_CRASH = 0;
  
  public static int TYPE_ANDROID_VOLDOWN_KEY = 0;
  
  public static int TYPE_ANDROID_VOLUP_KEY = 0;
  
  public static int TYPE_ANDROID_WCN_CRASH = 0;
  
  public static int TYPE_BATTERY_CHARGE_HISTORY = 0;
  
  public static int TYPE_CRITICAL_DATA_SIZE = 0;
  
  public static int TYPE_HW_SHUTDOWN = 0;
  
  public static int TYPE_LOGSIZE = 1022;
  
  public static int TYPE_LOGVER = 0;
  
  public static int TYPE_MODERN = 0;
  
  public static int TYPE_OTA_FLAG = 0;
  
  public static int TYPE_PANIC = 0;
  
  public static int TYPE_REBOOT = 0;
  
  public static int TYPE_REBOOT_FROM_BLOCKED = 0;
  
  public static int TYPE_RESMON = 0;
  
  public static int TYPE_ROOT_FLAG = 0;
  
  public static int TYPE_SHUTDOWN = 0;
  
  public static final int TYPE_SYMBOL_VERSION_DISAGREE = 803;
  
  public static final int TYPE_WDI_EXCEPTION = 804;
  
  public static int TYPE_WIFI_CONNECT_FAILED;
  
  private static IOplusService sService;
  
  static {
    TYPE_CRITICAL_DATA_SIZE = 512;
    TYPE_LOGVER = 0;
    TYEP_PHONE_IMEI = 1;
    TYEP_Android_VER = 2;
    TYEP_BUILD_VER = 3;
    TYEP_DEVICE = 4;
    TYPE_HW_SHUTDOWN = 5;
    TYPE_OTA_FLAG = 6;
    TYPE_ROOT_FLAG = 7;
    TYPE_BATTERY_CHARGE_HISTORY = 8;
    TYPE_SHUTDOWN = 20;
    TYPE_REBOOT = 21;
    TYPE_ANDROID_CRASH = 22;
    TYPE_ANDROID_OTA_UPGRADE = 29;
    TYPE_ANDROID_CAMERA = 28;
    TYPE_ANDROID_USB = 30;
    TYPE_ANDROID_HOME_KEY = 31;
    TYPE_ANDROID_MENU_KEY = 32;
    TYPE_ANDROID_BACK_KEY = 33;
    TYPE_ANDROID_VOLUP_KEY = 34;
    TYPE_ANDROID_VOLDOWN_KEY = 35;
    TYPE_ANDROID_POWER_KEY = 36;
    TYPE_ANDROID_SKIPFRAMES = 38;
    TYPE_ANDROID_LAUNCH_ACTIVITY = 39;
    TYPE_ANDROID_INSTALL_FAILD = 40;
    TYPE_ANDROID_OTA_FAILD = 41;
    TYPE_ANDROID_UNKNOWN_REBOOT = 42;
    TYPE_ANDROID_INPUTMETHOD_FAIL = 43;
    TYPE_ANDROID_ADSP_CRASH = 44;
    TYPE_ANDROID_VENUS_CRASH = 45;
    TYPE_ANDROID_WCN_CRASH = 46;
    TYPE_ANDROID_FP_DIE = 47;
    TYPE_ANDROID_FP_RESET_BYHM = 48;
    TYPE_ANDROID_FP_HW_ERROR = 49;
    TYPE_ANDROID_AVERAGE_CURRENT_EVENT = 37;
    TYPE_ANDROID_PM_EVENT_50 = 50;
    TYPE_ANDROID_PM_EVENT_51 = 51;
    TYPE_ANDROID_PM_EVENT_52 = 52;
    TYPE_ANDROID_PM_EVENT_53 = 53;
    TYPE_ANDROID_PM_EVENT_54 = 54;
    TYPE_ANDROID_PM_EVENT_55 = 55;
    TYPE_ANDROID_PM_EVENT_56 = 56;
    TYPE_ANDROID_PM_EVENT_57 = 57;
    TYPE_ANDROID_PM_EVENT_58 = 58;
    TYPE_ANDROID_PM_EVENT_59 = 59;
    TYPE_ANDROID_PM_EVENT_60 = 60;
    TYPE_ANDROID_PM_EVENT_61 = 61;
    TYPE_ANDROID_PM_EVENT_62 = 62;
    TYPE_ANDROID_PM_EVENT_63 = 63;
    TYPE_ANDROID_PM_EVENT_64 = 64;
    TYPE_ANDROID_REBOOT_HWT = 120;
    TYPE_ANDROID_REBOOT_HARDWARE_REBOOT = 121;
    TYPE_ANDROID_REBOOT_HANG = 122;
    TYPE_MODERN = 23;
    TYPE_ANDROID_SPMI = 24;
    TYPE_RESMON = 25;
    TYPE_ANDROID_SYSTEM_REBOOT_FROM_BLOCKED = 26;
    TYPE_REBOOT_FROM_BLOCKED = 27;
    TYPE_PANIC = 600;
    TYPE_WIFI_CONNECT_FAILED = 800;
  }
  
  public static final boolean init() {
    if (sService != null)
      return true; 
    byte b = 3;
    while (true) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Try to OppoService Instance! times = ");
      stringBuilder.append(b);
      Log.w("OplusManager", stringBuilder.toString());
      IOplusService iOplusService = IOplusService.Stub.asInterface(ServiceManager.getService("OPPO"));
      if (iOplusService != null)
        return true; 
      if (--b <= 0)
        return false; 
    } 
  }
  
  public static String readRawPartition(int paramInt1, int paramInt2) {
    String str = null;
    try {
      String str1 = native_oppoManager_readRawPartition(paramInt1, paramInt2);
    } catch (Exception exception) {
      Log.e("OplusManager", "read Raw Partition exception!");
      exception.printStackTrace();
    } 
    return str;
  }
  
  public static int writeRawPartition(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, int paramInt2) {
    byte b = -1;
    try {
      paramInt1 = native_oppoManager_writeRawPartition(paramInt1, paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramInt2);
    } catch (Exception exception) {
      Log.e("OplusManager", "write Raw Partition exception!");
      exception.printStackTrace();
      paramInt1 = b;
    } 
    return paramInt1;
  }
  
  public static int readCriticalData(int paramInt) {
    boolean bool = false;
    String str = readCriticalData(paramInt, DATA_SIZE);
    if (str == null)
      return 0; 
    str = str.trim();
    if (str == null || str.length() == 0)
      return 0; 
    try {
      paramInt = Integer.parseInt(str);
      paramInt += 0;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("read critical data failed!! e = ");
      stringBuilder.append(exception.toString());
      Log.e("OplusManager", stringBuilder.toString());
      exception.printStackTrace();
      paramInt = bool;
    } 
    return paramInt;
  }
  
  public static String readCriticalData(int paramInt1, int paramInt2) {
    String str = null;
    try {
      String str1 = native_oppoManager_readCriticalData(paramInt1, paramInt2);
    } catch (Exception exception) {
      Log.e("OplusManager", "read Critical Data exception!\n");
      exception.printStackTrace();
    } 
    return str;
  }
  
  public static int writeCriticalData(int paramInt, String paramString) {
    byte b = -1;
    String str = paramString;
    if (paramString != null) {
      str = paramString;
      try {
        if (paramString.length() > TYPE_CRITICAL_DATA_SIZE - 10)
          str = paramString.substring(0, TYPE_CRITICAL_DATA_SIZE - 10); 
        paramInt = native_oppoManager_writeCriticalData(paramInt, str);
      } catch (Exception exception) {
        Log.e("OplusManager", "write Critical Data exception!\n");
        exception.printStackTrace();
        paramInt = b;
      } 
      return paramInt;
    } 
    paramInt = native_oppoManager_writeCriticalData(paramInt, str);
  }
  
  public static String getTime() {
    long l = System.currentTimeMillis();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date = new Date(l);
    String str = simpleDateFormat.format(date);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str);
    stringBuilder.append("\n");
    return stringBuilder.toString();
  }
  
  public static String getIMEINums(Context paramContext) {
    if (paramContext.getPackageManager().hasSystemFeature("oppo.qualcomm.gemini.support")) {
      try {
        Log.i("OplusManager", "get imei in qcom");
      } catch (Exception exception) {
        Log.e("OplusManager", "Exception: ", exception);
      } 
    } else {
      Log.i("OplusManager", "get imei in MTK or sim1");
    } 
    return "";
  }
  
  public static String getVersionFOrAndroid() {
    if (TextUtils.isEmpty(Build.VERSION.RELEASE))
      return "null"; 
    return Build.VERSION.RELEASE;
  }
  
  public static String getOplusRomVersion() {
    String str = SystemProperties.get("ro.build.version.opporom");
    if (str == null || str.isEmpty())
      return "null"; 
    return str;
  }
  
  public static String getBuildVersion() {
    String str = SystemProperties.get("ro.build.version.ota");
    if (str == null || str.isEmpty())
      return "null"; 
    return str;
  }
  
  public static void recordEventForLog(int paramInt, String paramString) {
    if (paramInt == 1004)
      writeLogToPartition(TYPE_ANDROID_INPUTMETHOD_FAIL, paramString, "ANDROID", "inputmethod_fail", "type_issue_inputmethod_fail"); 
  }
  
  public static int writeLogToPartition(String paramString1, String paramString2, String paramString3) {
    Log.v("OplusManager", "this is the old api");
    return -1;
  }
  
  public static int writeLogToPartition(int paramInt1, String paramString1, String paramString2, String paramString3, int paramInt2) {
    String str1;
    if (paramString1 == null)
      return -1; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("log-time: ");
    stringBuilder.append(getTime());
    String str2 = stringBuilder.toString();
    stringBuilder = new StringBuilder();
    stringBuilder.append("log-buildTime: ");
    stringBuilder.append(SystemProperties.get("ro.build.version.ota", ""));
    stringBuilder.append("\n");
    String str3 = stringBuilder.toString();
    stringBuilder = new StringBuilder();
    stringBuilder.append("log-colorOS: ");
    stringBuilder.append(SystemProperties.get("ro.build.version.opporom", ""));
    stringBuilder.append("\n");
    String str4 = stringBuilder.toString();
    String str5 = String.format("LOGTYPE: %d\n", new Object[] { Integer.valueOf(paramInt1) });
    if (paramString3 == null || paramString3.isEmpty())
      paramString3 = paramString2; 
    if (paramString2.equals("ANDROID")) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("<android-");
      stringBuilder1.append(paramString3);
      stringBuilder1.append("-begin>\n");
      str1 = stringBuilder1.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append("\n<android-");
      stringBuilder.append(paramString3);
      stringBuilder.append("-end>\n");
      String str = stringBuilder.toString();
      paramString3 = str1;
      str1 = str;
    } else {
      String str;
      if (str1.equals("MULTIMEDIA")) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("<multimedia-");
        stringBuilder1.append(paramString3);
        stringBuilder1.append("-begin>\n");
        str = stringBuilder1.toString();
        stringBuilder = new StringBuilder();
        stringBuilder.append("\n<multimedia-");
        stringBuilder.append(paramString3);
        stringBuilder.append("-end>\n");
        String str6 = stringBuilder.toString();
        paramString3 = str;
        str = str6;
      } else {
        String str6;
        if (str.equals("NETWORK")) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("<network-");
          stringBuilder1.append(paramString3);
          stringBuilder1.append("-begin>\n");
          str6 = stringBuilder1.toString();
          stringBuilder = new StringBuilder();
          stringBuilder.append("\n<network-");
          stringBuilder.append(paramString3);
          stringBuilder.append("-end>\n");
          String str7 = stringBuilder.toString();
          paramString3 = str6;
          str6 = str7;
        } else {
          String str7;
          if (str6.equals("KERNEL")) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("<kernel-");
            stringBuilder1.append(paramString3);
            stringBuilder1.append("-begin>\n");
            str7 = stringBuilder1.toString();
            stringBuilder = new StringBuilder();
            stringBuilder.append("\n<kernel-");
            stringBuilder.append(paramString3);
            stringBuilder.append("-end>\n");
            String str8 = stringBuilder.toString();
            paramString3 = str7;
            str7 = str8;
          } else {
            String str8;
            if (str7.equals("CONNECTIVITY")) {
              StringBuilder stringBuilder1 = new StringBuilder();
              stringBuilder1.append("<connectivity-");
              stringBuilder1.append(paramString3);
              stringBuilder1.append("-begin>\n");
              str8 = stringBuilder1.toString();
              stringBuilder = new StringBuilder();
              stringBuilder.append("\n<connectivity-");
              stringBuilder.append(paramString3);
              stringBuilder.append("-end>\n");
              String str9 = stringBuilder.toString();
              paramString3 = str8;
              str8 = str9;
            } else {
              if (str8.equals("CAMERA")) {
                StringBuilder stringBuilder1 = new StringBuilder();
                stringBuilder1.append("<camera-");
                stringBuilder1.append(paramString3);
                stringBuilder1.append("-begin>\n");
                str1 = stringBuilder1.toString();
                stringBuilder = new StringBuilder();
                stringBuilder.append("\n<camera-");
                stringBuilder.append(paramString3);
                stringBuilder.append("-end>\n");
                String str9 = stringBuilder.toString();
                paramString3 = str1;
                str1 = str9;
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.append(paramString3);
                stringBuilder2.append(str5);
                stringBuilder2.append(str2);
                stringBuilder2.append(str3);
                stringBuilder2.append(str4);
                stringBuilder2.append(paramString1);
                stringBuilder2.append(str1);
                stringBuilder2.toString();
                return writeRawPartition(paramInt1, paramString3, str5, str2, str3, str4, paramString1, str1, paramInt2);
              } 
              Log.v("OplusManager", "the invalid tag");
              return -1;
            } 
          } 
        } 
      } 
    } 
    stringBuilder = new StringBuilder();
    stringBuilder.append(paramString3);
    stringBuilder.append(str5);
    stringBuilder.append(str2);
    stringBuilder.append(str3);
    stringBuilder.append(str4);
    stringBuilder.append(paramString1);
    stringBuilder.append(str1);
    stringBuilder.toString();
    return writeRawPartition(paramInt1, paramString3, str5, str2, str3, str4, paramString1, str1, paramInt2);
  }
  
  public static int incrementCriticalData(int paramInt, String paramString) {
    return writeLogToPartition(paramInt, (String)null, (String)null, (String)null, paramString);
  }
  
  public static int writeLogToPartition(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4) {
    boolean bool;
    if (paramString1 == null) {
      bool = false;
    } else if (!paramString1.isEmpty()) {
      bool = writeLogToPartition(paramInt, paramString1, paramString2, paramString3, -1);
    } else {
      Log.v("OplusManager", "log is empty");
      bool = false;
    } 
    int i = updateLogReference(paramInt, paramString4, false);
    if (paramInt > 19)
      i = updateLogReference(paramInt, paramString4, true); 
    if (i == -1 && bool == -1)
      return -3; 
    if (i == -1 && bool != -1)
      return -2; 
    if (i != -1 && bool == -1)
      return -1; 
    return 1;
  }
  
  private static int updateLogReference(int paramInt, String paramString, boolean paramBoolean) {
    String str;
    if (paramBoolean) {
      str = readCriticalData(paramInt + 1024, 256);
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("updateLogReference read backup type=");
      stringBuilder1.append(paramInt + 1024);
      stringBuilder1.append(" ref=");
      stringBuilder1.append(str);
      Log.v("OplusManager", stringBuilder1.toString());
    } else {
      str = readCriticalData(paramInt, 256);
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("updateLogReference read now type=");
      stringBuilder1.append(paramInt);
      stringBuilder1.append(" ref=");
      stringBuilder1.append(str);
      Log.v("OplusManager", stringBuilder1.toString());
    } 
    if (str == null || str.isEmpty()) {
      paramString = String.format("%d:%s:%d", new Object[] { Integer.valueOf(paramInt), paramString, Integer.valueOf(1) });
    } else {
      String[] arrayOfString = str.split(":");
      if (arrayOfString != null && arrayOfString.length >= 2) {
        try {
          int i = Integer.parseInt(arrayOfString[2]);
          if (paramString.equals(arrayOfString[1])) {
            String str1 = String.format("%d:%s:%d", new Object[] { Integer.valueOf(paramInt), paramString, Integer.valueOf(i + 1) });
          } else {
            String str1 = String.format("%d:%s:%d", new Object[] { Integer.valueOf(paramInt), paramString, Integer.valueOf(1) });
          } 
        } catch (Exception exception) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("catch e = ");
          stringBuilder1.append(exception.toString());
          Log.v("OplusManager", stringBuilder1.toString());
          paramString = String.format("%d:%s:%d", new Object[] { Integer.valueOf(paramInt), paramString, Integer.valueOf(1) });
        } 
      } else {
        Log.v("OplusManager", "update can not get any keyword");
        paramString = String.format("%d:%s:%d", new Object[] { Integer.valueOf(paramInt), paramString, Integer.valueOf(1) });
      } 
    } 
    if (paramBoolean) {
      paramInt = writeCriticalData(paramInt + 1024, paramString);
    } else {
      paramInt = writeCriticalData(paramInt, paramString);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("updateLogReference res=");
    stringBuilder.append(paramInt);
    Log.v("OplusManager", stringBuilder.toString());
    return paramInt;
  }
  
  public static boolean isEmmcLimit(int paramInt) {
    try {
      String str = readCriticalData(paramInt + 1024, 256);
      String[] arrayOfString = str.split(":");
      if (arrayOfString != null) {
        int i = arrayOfString.length;
        if (i >= 2) {
          try {
            i = Integer.parseInt(arrayOfString[2]);
            if (i < 5000)
              return false; 
          } catch (Exception exception) {
            StringBuilder stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append("catch e = ");
            stringBuilder1.append(exception.toString());
            Log.v("OplusManager", stringBuilder1.toString());
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("limit to record type = ");
          stringBuilder.append(paramInt);
          Log.v("OplusManager", stringBuilder.toString());
          return true;
        } 
      } 
      Log.v("OplusManager", "the refs is not formative");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isEmmcLimit exception e = ");
      stringBuilder.append(exception.toString());
      Log.v("OplusManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public static int cleanItem(int paramInt) {
    return native_oppoManager_cleanItem(paramInt);
  }
  
  public static int syncCacheToEmmc() {
    native_oppoManager_syncCahceToEmmc();
    return 0;
  }
  
  public static int updateConfig() {
    native_oppoManager_updateConfig();
    return 0;
  }
  
  public static int testFunc(int paramInt1, int paramInt2) {
    native_oppoManager_testFunc(paramInt1, paramInt2);
    return 0;
  }
  
  public static void onStamp(String paramString, Map<String, String> paramMap) {
    if (sService == null && !init()) {
      Log.d("OplusManager", "can not init the oppo service");
      return;
    } 
    try {
      if (sService != null) {
        sService.sendOnStampEvent(paramString, paramMap);
        Log.d("OplusManager", "send on stamp success");
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("error : ");
      stringBuilder.append(exception.toString());
      Log.e("OplusManager", stringBuilder.toString());
    } 
  }
  
  public static void readAllStamp(String paramString, IStampCallBack paramIStampCallBack) {
    if (sService == null && !init()) {
      Log.d("OplusManager", "can not init the oppo service");
      return;
    } 
  }
  
  public static void readLastStamp(String paramString, int paramInt, IStampCallBack paramIStampCallBack) {
    if (sService == null && !init()) {
      Log.d("OplusManager", "can not init the oppo service");
      return;
    } 
  }
  
  public static void rawQueryEvent(String paramString) {
    if (sService == null && !init()) {
      Log.d("OplusManager", "can not init the oppo service");
      return;
    } 
  }
  
  public static void onDeleteStampId(String paramString) {
    if (sService == null && !init()) {
      Log.d("OplusManager", "can not init the oppo service");
      return;
    } 
    try {
      if (sService != null) {
        sService.sendDeleteStampId(paramString);
        Log.d("OplusManager", "send on delete stamp success");
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("error : ");
      stringBuilder.append(exception.toString());
      Log.e("OplusManager", stringBuilder.toString());
    } 
  }
  
  public static class StampEvent {
    public String dayno;
    
    public String eventId;
    
    public String hour;
    
    public Map<String, String> logMap;
    
    public String otaVersion;
    
    public String timestamp;
  }
  
  public static class StampId {
    public static String AD_BATTERYOFF;
    
    public static String AD_DEVICE = "000000";
    
    public static String AD_EMMCCHECK;
    
    public static String AD_FILECHECK;
    
    public static String AD_JE = "010102";
    
    public static String AD_KE = "010101";
    
    public static String AD_OCP = "010103";
    
    public static String AD_PMICREASON;
    
    static {
      AD_FILECHECK = "010104";
      AD_EMMCCHECK = "010105";
      AD_PMICREASON = "010107";
      AD_BATTERYOFF = "010201";
    }
  }
  
  public static void sendQualityDCSEvent(String paramString, Map<String, String> paramMap) {
    if (sService == null && !init()) {
      Log.d("OplusManager", "sendOnQualityDCSEvent failed. Can not init the oppo service");
      return;
    } 
    try {
      sService.sendQualityDCSEvent(paramString, paramMap);
      Log.d("OplusManager", "sendOnQualityDCSEvent success");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("error : ");
      stringBuilder.append(exception.toString());
      Log.e("OplusManager", stringBuilder.toString());
    } 
  }
  
  private static native int native_oppoManager_cleanItem(int paramInt);
  
  private static native String native_oppoManager_readCriticalData(int paramInt1, int paramInt2);
  
  private static native String native_oppoManager_readRawPartition(int paramInt1, int paramInt2);
  
  private static native int native_oppoManager_syncCahceToEmmc();
  
  private static native String native_oppoManager_testFunc(int paramInt1, int paramInt2);
  
  private static native int native_oppoManager_updateConfig();
  
  private static native int native_oppoManager_writeCriticalData(int paramInt, String paramString);
  
  private static native int native_oppoManager_writeRawPartition(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, int paramInt2);
  
  public static class QualityEventId {
    public static String EV_STABILITY_ANDROID_REBOOT_FROM_BLOCKED;
    
    public static String EV_STABILITY_DEATH_HEALER;
    
    public static String EV_STABILITY_HANG_BOOTANIM;
    
    public static String EV_STABILITY_HANG_BOOT_LOGO;
    
    public static String EV_STABILITY_HECATE_BLANK_SCREEN;
    
    public static String EV_STABILITY_HECATE_HWC_HANG;
    
    public static String EV_STABILITY_HECATE_SF_HANGO;
    
    public static String EV_STABILITY_IOHUNG;
    
    public static String EV_STABILITY_KERNEL_PANIC = "100100002";
    
    public static String EV_STABILITY_MTK_HANG;
    
    public static String EV_STABILITY_MTK_HARDWARE_REBOOT;
    
    public static String EV_STABILITY_MTK_HWT;
    
    public static String EV_STABILITY_OCP;
    
    public static String EV_STABILITY_PMIC_WT;
    
    public static String EV_STABILITY_REBOOT = "100100001";
    
    public static String EV_STABILITY_REBOOT_FROM_BLOCKED;
    
    public static String EV_STABILITY_SHUTDOWN_ERROR;
    
    public static String EV_STABILITY_SYSTEM_CRASH = "100100003";
    
    public static String EV_STABILITY_UNKNOWN_REBOOT;
    
    static {
      EV_STABILITY_DEATH_HEALER = "100100004";
      EV_STABILITY_IOHUNG = "100100005";
      EV_STABILITY_OCP = "100100006";
      EV_STABILITY_PMIC_WT = "100100007";
      EV_STABILITY_MTK_HWT = "100100008";
      EV_STABILITY_MTK_HARDWARE_REBOOT = "100100009";
      EV_STABILITY_MTK_HANG = "100100010";
      EV_STABILITY_REBOOT_FROM_BLOCKED = "100100011";
      EV_STABILITY_ANDROID_REBOOT_FROM_BLOCKED = "100100012";
      EV_STABILITY_UNKNOWN_REBOOT = "100100013";
      EV_STABILITY_HANG_BOOT_LOGO = "100200001";
      EV_STABILITY_HANG_BOOTANIM = "100200002";
      EV_STABILITY_SHUTDOWN_ERROR = "100200003";
      EV_STABILITY_HECATE_BLANK_SCREEN = "100300001";
      EV_STABILITY_HECATE_SF_HANGO = "100300002";
      EV_STABILITY_HECATE_HWC_HANG = "100300003";
    }
  }
  
  public static interface IStampCallBack {
    void onComplete(Object param1Object);
  }
}
