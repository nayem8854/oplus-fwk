package android.os;

public interface IMessenger extends IInterface {
  void send(Message paramMessage) throws RemoteException;
  
  class Default implements IMessenger {
    public void send(Message param1Message) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMessenger {
    private static final String DESCRIPTOR = "android.os.IMessenger";
    
    static final int TRANSACTION_send = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IMessenger");
    }
    
    public static IMessenger asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IMessenger");
      if (iInterface != null && iInterface instanceof IMessenger)
        return (IMessenger)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "send";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.IMessenger");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IMessenger");
      if (param1Parcel1.readInt() != 0) {
        Message message = Message.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      send((Message)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IMessenger {
      public static IMessenger sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IMessenger";
      }
      
      public void send(Message param2Message) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IMessenger");
          if (param2Message != null) {
            parcel.writeInt(1);
            param2Message.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMessenger.Stub.getDefaultImpl() != null) {
            IMessenger.Stub.getDefaultImpl().send(param2Message);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMessenger param1IMessenger) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMessenger != null) {
          Proxy.sDefaultImpl = param1IMessenger;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMessenger getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
