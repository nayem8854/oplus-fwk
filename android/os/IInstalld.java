package android.os;

import android.os.storage.CrateMetadata;
import java.io.FileDescriptor;

public interface IInstalld extends IInterface {
  public static final int FLAG_CLEAR_APP_DATA_KEEP_ART_PROFILES = 131072;
  
  public static final int FLAG_CLEAR_CACHE_ONLY = 16;
  
  public static final int FLAG_CLEAR_CODE_CACHE_ONLY = 32;
  
  public static final int FLAG_FORCE = 8192;
  
  public static final int FLAG_FREE_CACHE_NOOP = 1024;
  
  public static final int FLAG_FREE_CACHE_V2 = 256;
  
  public static final int FLAG_FREE_CACHE_V2_DEFY_QUOTA = 512;
  
  public static final int FLAG_STORAGE_CE = 2;
  
  public static final int FLAG_STORAGE_DE = 1;
  
  public static final int FLAG_STORAGE_EXTERNAL = 4;
  
  public static final int FLAG_USE_QUOTA = 4096;
  
  void assertFsverityRootHashMatches(String paramString, byte[] paramArrayOfbyte) throws RemoteException;
  
  void clearAppData(String paramString1, String paramString2, int paramInt1, int paramInt2, long paramLong) throws RemoteException;
  
  void clearAppProfiles(String paramString1, String paramString2) throws RemoteException;
  
  boolean compileLayouts(String paramString1, String paramString2, String paramString3, int paramInt) throws RemoteException;
  
  boolean copySystemProfile(String paramString1, int paramInt, String paramString2, String paramString3) throws RemoteException;
  
  long createAppData(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3, int paramInt4) throws RemoteException;
  
  long createAppDataBatched(String[] paramArrayOfString1, String[] paramArrayOfString2, int paramInt1, int paramInt2, int[] paramArrayOfint1, String[] paramArrayOfString3, int[] paramArrayOfint2) throws RemoteException;
  
  boolean createAppProfile(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4) throws RemoteException;
  
  void createOatDir(String paramString1, String paramString2) throws RemoteException;
  
  boolean createProfileSnapshot(int paramInt, String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  void createUserData(String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void deleteOdex(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  void destroyAppData(String paramString1, String paramString2, int paramInt1, int paramInt2, long paramLong) throws RemoteException;
  
  void destroyAppDataSnapshot(String paramString1, String paramString2, int paramInt1, long paramLong, int paramInt2, int paramInt3) throws RemoteException;
  
  void destroyAppProfiles(String paramString) throws RemoteException;
  
  void destroyCeSnapshotsNotSpecified(String paramString, int paramInt, int[] paramArrayOfint) throws RemoteException;
  
  void destroyProfileSnapshot(String paramString1, String paramString2) throws RemoteException;
  
  void destroyUserData(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void dexopt(String paramString1, int paramInt1, String paramString2, String paramString3, int paramInt2, String paramString4, int paramInt3, String paramString5, String paramString6, String paramString7, String paramString8, boolean paramBoolean, int paramInt4, String paramString9, String paramString10, String paramString11) throws RemoteException;
  
  boolean dumpAppClassAndMethod(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4) throws RemoteException;
  
  boolean dumpProfiles(int paramInt, String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  void fixupAppData(String paramString, int paramInt) throws RemoteException;
  
  long fixupAppDataForUidChange(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3, int paramInt4) throws RemoteException;
  
  void freeCache(String paramString, long paramLong1, long paramLong2, int paramInt) throws RemoteException;
  
  CrateMetadata[] getAppCrates(String paramString, String[] paramArrayOfString, int paramInt) throws RemoteException;
  
  long[] getAppSize(String paramString, String[] paramArrayOfString1, int paramInt1, int paramInt2, int paramInt3, long[] paramArrayOflong, String[] paramArrayOfString2) throws RemoteException;
  
  long[] getExternalSize(String paramString, int paramInt1, int paramInt2, int[] paramArrayOfint) throws RemoteException;
  
  CrateMetadata[] getUserCrates(String paramString, int paramInt) throws RemoteException;
  
  long[] getUserSize(String paramString, int paramInt1, int paramInt2, int[] paramArrayOfint) throws RemoteException;
  
  byte[] hashSecondaryDexFile(String paramString1, String paramString2, int paramInt1, String paramString3, int paramInt2) throws RemoteException;
  
  void installApkVerity(String paramString, FileDescriptor paramFileDescriptor, int paramInt) throws RemoteException;
  
  void invalidateMounts() throws RemoteException;
  
  boolean isQuotaSupported(String paramString) throws RemoteException;
  
  void killRunDex2Oat() throws RemoteException;
  
  void linkFile(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  void linkNativeLibraryDirectory(String paramString1, String paramString2, String paramString3, int paramInt) throws RemoteException;
  
  boolean mergeProfiles(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  void migrateAppData(String paramString1, String paramString2, int paramInt1, int paramInt2) throws RemoteException;
  
  void migrateLegacyObbData() throws RemoteException;
  
  void moveAb(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  void moveCompleteApp(String paramString1, String paramString2, String paramString3, int paramInt1, String paramString4, int paramInt2, String paramString5) throws RemoteException;
  
  void onPrivateVolumeRemoved(String paramString) throws RemoteException;
  
  boolean prepareAppProfile(String paramString1, int paramInt1, int paramInt2, String paramString2, String paramString3, String paramString4) throws RemoteException;
  
  boolean reconcileSecondaryDexFile(String paramString1, String paramString2, int paramInt1, String[] paramArrayOfString, String paramString3, int paramInt2) throws RemoteException;
  
  void restoreAppDataSnapshot(String paramString1, String paramString2, int paramInt1, String paramString3, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void restoreconAppData(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3) throws RemoteException;
  
  void rmPackageDir(String paramString) throws RemoteException;
  
  void rmdex(String paramString1, String paramString2) throws RemoteException;
  
  void setAppQuota(String paramString, int paramInt1, int paramInt2, long paramLong) throws RemoteException;
  
  long snapshotAppData(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void tryMountDataMirror(String paramString) throws RemoteException;
  
  boolean updateAppProfile(String paramString1, int paramInt1, int paramInt2, String paramString2, String paramString3, String paramString4) throws RemoteException;
  
  class Default implements IInstalld {
    public void createUserData(String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void destroyUserData(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public long createAppData(String param1String1, String param1String2, int param1Int1, int param1Int2, int param1Int3, String param1String3, int param1Int4) throws RemoteException {
      return 0L;
    }
    
    public long createAppDataBatched(String[] param1ArrayOfString1, String[] param1ArrayOfString2, int param1Int1, int param1Int2, int[] param1ArrayOfint1, String[] param1ArrayOfString3, int[] param1ArrayOfint2) throws RemoteException {
      return 0L;
    }
    
    public void restoreconAppData(String param1String1, String param1String2, int param1Int1, int param1Int2, int param1Int3, String param1String3) throws RemoteException {}
    
    public void migrateAppData(String param1String1, String param1String2, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void clearAppData(String param1String1, String param1String2, int param1Int1, int param1Int2, long param1Long) throws RemoteException {}
    
    public void destroyAppData(String param1String1, String param1String2, int param1Int1, int param1Int2, long param1Long) throws RemoteException {}
    
    public void fixupAppData(String param1String, int param1Int) throws RemoteException {}
    
    public long[] getAppSize(String param1String, String[] param1ArrayOfString1, int param1Int1, int param1Int2, int param1Int3, long[] param1ArrayOflong, String[] param1ArrayOfString2) throws RemoteException {
      return null;
    }
    
    public long[] getUserSize(String param1String, int param1Int1, int param1Int2, int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public long[] getExternalSize(String param1String, int param1Int1, int param1Int2, int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public CrateMetadata[] getAppCrates(String param1String, String[] param1ArrayOfString, int param1Int) throws RemoteException {
      return null;
    }
    
    public CrateMetadata[] getUserCrates(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void setAppQuota(String param1String, int param1Int1, int param1Int2, long param1Long) throws RemoteException {}
    
    public void moveCompleteApp(String param1String1, String param1String2, String param1String3, int param1Int1, String param1String4, int param1Int2, String param1String5) throws RemoteException {}
    
    public void dexopt(String param1String1, int param1Int1, String param1String2, String param1String3, int param1Int2, String param1String4, int param1Int3, String param1String5, String param1String6, String param1String7, String param1String8, boolean param1Boolean, int param1Int4, String param1String9, String param1String10, String param1String11) throws RemoteException {}
    
    public boolean compileLayouts(String param1String1, String param1String2, String param1String3, int param1Int) throws RemoteException {
      return false;
    }
    
    public void rmdex(String param1String1, String param1String2) throws RemoteException {}
    
    public boolean mergeProfiles(int param1Int, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean dumpProfiles(int param1Int, String param1String1, String param1String2, String param1String3) throws RemoteException {
      return false;
    }
    
    public boolean copySystemProfile(String param1String1, int param1Int, String param1String2, String param1String3) throws RemoteException {
      return false;
    }
    
    public void clearAppProfiles(String param1String1, String param1String2) throws RemoteException {}
    
    public void destroyAppProfiles(String param1String) throws RemoteException {}
    
    public boolean createProfileSnapshot(int param1Int, String param1String1, String param1String2, String param1String3) throws RemoteException {
      return false;
    }
    
    public void destroyProfileSnapshot(String param1String1, String param1String2) throws RemoteException {}
    
    public void rmPackageDir(String param1String) throws RemoteException {}
    
    public void freeCache(String param1String, long param1Long1, long param1Long2, int param1Int) throws RemoteException {}
    
    public void linkNativeLibraryDirectory(String param1String1, String param1String2, String param1String3, int param1Int) throws RemoteException {}
    
    public void createOatDir(String param1String1, String param1String2) throws RemoteException {}
    
    public void linkFile(String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public void moveAb(String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public void deleteOdex(String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public void installApkVerity(String param1String, FileDescriptor param1FileDescriptor, int param1Int) throws RemoteException {}
    
    public void assertFsverityRootHashMatches(String param1String, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public boolean reconcileSecondaryDexFile(String param1String1, String param1String2, int param1Int1, String[] param1ArrayOfString, String param1String3, int param1Int2) throws RemoteException {
      return false;
    }
    
    public byte[] hashSecondaryDexFile(String param1String1, String param1String2, int param1Int1, String param1String3, int param1Int2) throws RemoteException {
      return null;
    }
    
    public void invalidateMounts() throws RemoteException {}
    
    public boolean isQuotaSupported(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean prepareAppProfile(String param1String1, int param1Int1, int param1Int2, String param1String2, String param1String3, String param1String4) throws RemoteException {
      return false;
    }
    
    public long snapshotAppData(String param1String1, String param1String2, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return 0L;
    }
    
    public void restoreAppDataSnapshot(String param1String1, String param1String2, int param1Int1, String param1String3, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void destroyAppDataSnapshot(String param1String1, String param1String2, int param1Int1, long param1Long, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void destroyCeSnapshotsNotSpecified(String param1String, int param1Int, int[] param1ArrayOfint) throws RemoteException {}
    
    public void tryMountDataMirror(String param1String) throws RemoteException {}
    
    public void onPrivateVolumeRemoved(String param1String) throws RemoteException {}
    
    public void migrateLegacyObbData() throws RemoteException {}
    
    public boolean updateAppProfile(String param1String1, int param1Int1, int param1Int2, String param1String2, String param1String3, String param1String4) throws RemoteException {
      return false;
    }
    
    public boolean dumpAppClassAndMethod(int param1Int, String param1String1, String param1String2, String param1String3, String param1String4) throws RemoteException {
      return false;
    }
    
    public boolean createAppProfile(int param1Int, String param1String1, String param1String2, String param1String3, String param1String4) throws RemoteException {
      return false;
    }
    
    public long fixupAppDataForUidChange(String param1String1, String param1String2, int param1Int1, int param1Int2, int param1Int3, String param1String3, int param1Int4) throws RemoteException {
      return 0L;
    }
    
    public void killRunDex2Oat() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInstalld {
    private static final String DESCRIPTOR = "android.os.IInstalld";
    
    static final int TRANSACTION_assertFsverityRootHashMatches = 35;
    
    static final int TRANSACTION_clearAppData = 7;
    
    static final int TRANSACTION_clearAppProfiles = 23;
    
    static final int TRANSACTION_compileLayouts = 18;
    
    static final int TRANSACTION_copySystemProfile = 22;
    
    static final int TRANSACTION_createAppData = 3;
    
    static final int TRANSACTION_createAppDataBatched = 4;
    
    static final int TRANSACTION_createAppProfile = 50;
    
    static final int TRANSACTION_createOatDir = 30;
    
    static final int TRANSACTION_createProfileSnapshot = 25;
    
    static final int TRANSACTION_createUserData = 1;
    
    static final int TRANSACTION_deleteOdex = 33;
    
    static final int TRANSACTION_destroyAppData = 8;
    
    static final int TRANSACTION_destroyAppDataSnapshot = 43;
    
    static final int TRANSACTION_destroyAppProfiles = 24;
    
    static final int TRANSACTION_destroyCeSnapshotsNotSpecified = 44;
    
    static final int TRANSACTION_destroyProfileSnapshot = 26;
    
    static final int TRANSACTION_destroyUserData = 2;
    
    static final int TRANSACTION_dexopt = 17;
    
    static final int TRANSACTION_dumpAppClassAndMethod = 49;
    
    static final int TRANSACTION_dumpProfiles = 21;
    
    static final int TRANSACTION_fixupAppData = 9;
    
    static final int TRANSACTION_fixupAppDataForUidChange = 51;
    
    static final int TRANSACTION_freeCache = 28;
    
    static final int TRANSACTION_getAppCrates = 13;
    
    static final int TRANSACTION_getAppSize = 10;
    
    static final int TRANSACTION_getExternalSize = 12;
    
    static final int TRANSACTION_getUserCrates = 14;
    
    static final int TRANSACTION_getUserSize = 11;
    
    static final int TRANSACTION_hashSecondaryDexFile = 37;
    
    static final int TRANSACTION_installApkVerity = 34;
    
    static final int TRANSACTION_invalidateMounts = 38;
    
    static final int TRANSACTION_isQuotaSupported = 39;
    
    static final int TRANSACTION_killRunDex2Oat = 52;
    
    static final int TRANSACTION_linkFile = 31;
    
    static final int TRANSACTION_linkNativeLibraryDirectory = 29;
    
    static final int TRANSACTION_mergeProfiles = 20;
    
    static final int TRANSACTION_migrateAppData = 6;
    
    static final int TRANSACTION_migrateLegacyObbData = 47;
    
    static final int TRANSACTION_moveAb = 32;
    
    static final int TRANSACTION_moveCompleteApp = 16;
    
    static final int TRANSACTION_onPrivateVolumeRemoved = 46;
    
    static final int TRANSACTION_prepareAppProfile = 40;
    
    static final int TRANSACTION_reconcileSecondaryDexFile = 36;
    
    static final int TRANSACTION_restoreAppDataSnapshot = 42;
    
    static final int TRANSACTION_restoreconAppData = 5;
    
    static final int TRANSACTION_rmPackageDir = 27;
    
    static final int TRANSACTION_rmdex = 19;
    
    static final int TRANSACTION_setAppQuota = 15;
    
    static final int TRANSACTION_snapshotAppData = 41;
    
    static final int TRANSACTION_tryMountDataMirror = 45;
    
    static final int TRANSACTION_updateAppProfile = 48;
    
    public Stub() {
      attachInterface(this, "android.os.IInstalld");
    }
    
    public static IInstalld asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IInstalld");
      if (iInterface != null && iInterface instanceof IInstalld)
        return (IInstalld)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 52:
          return "killRunDex2Oat";
        case 51:
          return "fixupAppDataForUidChange";
        case 50:
          return "createAppProfile";
        case 49:
          return "dumpAppClassAndMethod";
        case 48:
          return "updateAppProfile";
        case 47:
          return "migrateLegacyObbData";
        case 46:
          return "onPrivateVolumeRemoved";
        case 45:
          return "tryMountDataMirror";
        case 44:
          return "destroyCeSnapshotsNotSpecified";
        case 43:
          return "destroyAppDataSnapshot";
        case 42:
          return "restoreAppDataSnapshot";
        case 41:
          return "snapshotAppData";
        case 40:
          return "prepareAppProfile";
        case 39:
          return "isQuotaSupported";
        case 38:
          return "invalidateMounts";
        case 37:
          return "hashSecondaryDexFile";
        case 36:
          return "reconcileSecondaryDexFile";
        case 35:
          return "assertFsverityRootHashMatches";
        case 34:
          return "installApkVerity";
        case 33:
          return "deleteOdex";
        case 32:
          return "moveAb";
        case 31:
          return "linkFile";
        case 30:
          return "createOatDir";
        case 29:
          return "linkNativeLibraryDirectory";
        case 28:
          return "freeCache";
        case 27:
          return "rmPackageDir";
        case 26:
          return "destroyProfileSnapshot";
        case 25:
          return "createProfileSnapshot";
        case 24:
          return "destroyAppProfiles";
        case 23:
          return "clearAppProfiles";
        case 22:
          return "copySystemProfile";
        case 21:
          return "dumpProfiles";
        case 20:
          return "mergeProfiles";
        case 19:
          return "rmdex";
        case 18:
          return "compileLayouts";
        case 17:
          return "dexopt";
        case 16:
          return "moveCompleteApp";
        case 15:
          return "setAppQuota";
        case 14:
          return "getUserCrates";
        case 13:
          return "getAppCrates";
        case 12:
          return "getExternalSize";
        case 11:
          return "getUserSize";
        case 10:
          return "getAppSize";
        case 9:
          return "fixupAppData";
        case 8:
          return "destroyAppData";
        case 7:
          return "clearAppData";
        case 6:
          return "migrateAppData";
        case 5:
          return "restoreconAppData";
        case 4:
          return "createAppDataBatched";
        case 3:
          return "createAppData";
        case 2:
          return "destroyUserData";
        case 1:
          break;
      } 
      return "createUserData";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool10;
        int i5;
        boolean bool9;
        int i4;
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        String str4;
        int[] arrayOfInt4;
        String str3;
        byte[] arrayOfByte;
        String str2;
        CrateMetadata[] arrayOfCrateMetadata;
        int[] arrayOfInt3;
        long[] arrayOfLong3;
        int[] arrayOfInt2;
        long[] arrayOfLong2;
        String[] arrayOfString1;
        long[] arrayOfLong1;
        String str1;
        int[] arrayOfInt1;
        String str8, arrayOfString4[], str7, arrayOfString3[], str6, arrayOfString2[], str11, arrayOfString6[], str10;
        long[] arrayOfLong4;
        String arrayOfString5[], str9, str14;
        FileDescriptor fileDescriptor;
        String str13;
        int[] arrayOfInt5;
        String str12;
        int i7;
        long l1;
        String str15, arrayOfString7[];
        long l2;
        String str16, str17, str18, str19, str20, str21;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 52:
            param1Parcel1.enforceInterface("android.os.IInstalld");
            killRunDex2Oat();
            param1Parcel2.writeNoException();
            return true;
          case 51:
            param1Parcel1.enforceInterface("android.os.IInstalld");
            str8 = param1Parcel1.readString();
            str11 = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            i6 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            str14 = param1Parcel1.readString();
            i7 = param1Parcel1.readInt();
            l1 = fixupAppDataForUidChange(str8, str11, param1Int2, i6, param1Int1, str14, i7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l1);
            return true;
          case 50:
            param1Parcel1.enforceInterface("android.os.IInstalld");
            param1Int1 = param1Parcel1.readInt();
            str14 = param1Parcel1.readString();
            str11 = param1Parcel1.readString();
            str8 = param1Parcel1.readString();
            str4 = param1Parcel1.readString();
            bool10 = createAppProfile(param1Int1, str14, str11, str8, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 49:
            str4.enforceInterface("android.os.IInstalld");
            i5 = str4.readInt();
            str11 = str4.readString();
            str8 = str4.readString();
            str14 = str4.readString();
            str4 = str4.readString();
            bool9 = dumpAppClassAndMethod(i5, str11, str8, str14, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 48:
            str4.enforceInterface("android.os.IInstalld");
            str14 = str4.readString();
            param1Int2 = str4.readInt();
            i4 = str4.readInt();
            str11 = str4.readString();
            str8 = str4.readString();
            str4 = str4.readString();
            bool8 = updateAppProfile(str14, param1Int2, i4, str11, str8, str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 47:
            str4.enforceInterface("android.os.IInstalld");
            migrateLegacyObbData();
            param1Parcel2.writeNoException();
            return true;
          case 46:
            str4.enforceInterface("android.os.IInstalld");
            str4 = str4.readString();
            onPrivateVolumeRemoved(str4);
            param1Parcel2.writeNoException();
            return true;
          case 45:
            str4.enforceInterface("android.os.IInstalld");
            str4 = str4.readString();
            tryMountDataMirror(str4);
            param1Parcel2.writeNoException();
            return true;
          case 44:
            str4.enforceInterface("android.os.IInstalld");
            str8 = str4.readString();
            i3 = str4.readInt();
            arrayOfInt4 = str4.createIntArray();
            destroyCeSnapshotsNotSpecified(str8, i3, arrayOfInt4);
            param1Parcel2.writeNoException();
            return true;
          case 43:
            arrayOfInt4.enforceInterface("android.os.IInstalld");
            str14 = arrayOfInt4.readString();
            str8 = arrayOfInt4.readString();
            i3 = arrayOfInt4.readInt();
            l1 = arrayOfInt4.readLong();
            param1Int2 = arrayOfInt4.readInt();
            i6 = arrayOfInt4.readInt();
            destroyAppDataSnapshot(str14, str8, i3, l1, param1Int2, i6);
            param1Parcel2.writeNoException();
            return true;
          case 42:
            arrayOfInt4.enforceInterface("android.os.IInstalld");
            str14 = arrayOfInt4.readString();
            str8 = arrayOfInt4.readString();
            param1Int2 = arrayOfInt4.readInt();
            str11 = arrayOfInt4.readString();
            i7 = arrayOfInt4.readInt();
            i3 = arrayOfInt4.readInt();
            i6 = arrayOfInt4.readInt();
            restoreAppDataSnapshot(str14, str8, param1Int2, str11, i7, i3, i6);
            param1Parcel2.writeNoException();
            return true;
          case 41:
            arrayOfInt4.enforceInterface("android.os.IInstalld");
            str14 = arrayOfInt4.readString();
            str8 = arrayOfInt4.readString();
            param1Int2 = arrayOfInt4.readInt();
            i3 = arrayOfInt4.readInt();
            i6 = arrayOfInt4.readInt();
            l1 = snapshotAppData(str14, str8, param1Int2, i3, i6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l1);
            return true;
          case 40:
            arrayOfInt4.enforceInterface("android.os.IInstalld");
            str11 = arrayOfInt4.readString();
            param1Int2 = arrayOfInt4.readInt();
            i3 = arrayOfInt4.readInt();
            str14 = arrayOfInt4.readString();
            str8 = arrayOfInt4.readString();
            str3 = arrayOfInt4.readString();
            bool7 = prepareAppProfile(str11, param1Int2, i3, str14, str8, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 39:
            str3.enforceInterface("android.os.IInstalld");
            str3 = str3.readString();
            bool7 = isQuotaSupported(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 38:
            str3.enforceInterface("android.os.IInstalld");
            invalidateMounts();
            param1Parcel2.writeNoException();
            return true;
          case 37:
            str3.enforceInterface("android.os.IInstalld");
            str11 = str3.readString();
            str14 = str3.readString();
            param1Int2 = str3.readInt();
            str8 = str3.readString();
            i2 = str3.readInt();
            arrayOfByte = hashSecondaryDexFile(str11, str14, param1Int2, str8, i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte);
            return true;
          case 36:
            arrayOfByte.enforceInterface("android.os.IInstalld");
            str14 = arrayOfByte.readString();
            str8 = arrayOfByte.readString();
            param1Int2 = arrayOfByte.readInt();
            arrayOfString6 = arrayOfByte.createStringArray();
            str15 = arrayOfByte.readString();
            i2 = arrayOfByte.readInt();
            bool6 = reconcileSecondaryDexFile(str14, str8, param1Int2, arrayOfString6, str15, i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 35:
            arrayOfByte.enforceInterface("android.os.IInstalld");
            str8 = arrayOfByte.readString();
            arrayOfByte = arrayOfByte.createByteArray();
            assertFsverityRootHashMatches(str8, arrayOfByte);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            arrayOfByte.enforceInterface("android.os.IInstalld");
            str8 = arrayOfByte.readString();
            fileDescriptor = arrayOfByte.readRawFileDescriptor();
            i1 = arrayOfByte.readInt();
            installApkVerity(str8, fileDescriptor, i1);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            arrayOfByte.enforceInterface("android.os.IInstalld");
            str13 = arrayOfByte.readString();
            str8 = arrayOfByte.readString();
            str2 = arrayOfByte.readString();
            deleteOdex(str13, str8, str2);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            str2.enforceInterface("android.os.IInstalld");
            str13 = str2.readString();
            str8 = str2.readString();
            str2 = str2.readString();
            moveAb(str13, str8, str2);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            str2.enforceInterface("android.os.IInstalld");
            str8 = str2.readString();
            str13 = str2.readString();
            str2 = str2.readString();
            linkFile(str8, str13, str2);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            str2.enforceInterface("android.os.IInstalld");
            str8 = str2.readString();
            str2 = str2.readString();
            createOatDir(str8, str2);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            str2.enforceInterface("android.os.IInstalld");
            str13 = str2.readString();
            str10 = str2.readString();
            str8 = str2.readString();
            i1 = str2.readInt();
            linkNativeLibraryDirectory(str13, str10, str8, i1);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            str2.enforceInterface("android.os.IInstalld");
            str8 = str2.readString();
            l2 = str2.readLong();
            l1 = str2.readLong();
            i1 = str2.readInt();
            freeCache(str8, l2, l1, i1);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            str2.enforceInterface("android.os.IInstalld");
            str2 = str2.readString();
            rmPackageDir(str2);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            str2.enforceInterface("android.os.IInstalld");
            str8 = str2.readString();
            str2 = str2.readString();
            destroyProfileSnapshot(str8, str2);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str2.enforceInterface("android.os.IInstalld");
            i1 = str2.readInt();
            str13 = str2.readString();
            str8 = str2.readString();
            str2 = str2.readString();
            bool5 = createProfileSnapshot(i1, str13, str8, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 24:
            str2.enforceInterface("android.os.IInstalld");
            str2 = str2.readString();
            destroyAppProfiles(str2);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str2.enforceInterface("android.os.IInstalld");
            str8 = str2.readString();
            str2 = str2.readString();
            clearAppProfiles(str8, str2);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            str2.enforceInterface("android.os.IInstalld");
            str8 = str2.readString();
            n = str2.readInt();
            str13 = str2.readString();
            str2 = str2.readString();
            bool4 = copySystemProfile(str8, n, str13, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 21:
            str2.enforceInterface("android.os.IInstalld");
            m = str2.readInt();
            str8 = str2.readString();
            str13 = str2.readString();
            str2 = str2.readString();
            bool3 = dumpProfiles(m, str8, str13, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 20:
            str2.enforceInterface("android.os.IInstalld");
            k = str2.readInt();
            str8 = str2.readString();
            str2 = str2.readString();
            bool2 = mergeProfiles(k, str8, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 19:
            str2.enforceInterface("android.os.IInstalld");
            str8 = str2.readString();
            str2 = str2.readString();
            rmdex(str8, str2);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            str2.enforceInterface("android.os.IInstalld");
            str10 = str2.readString();
            str8 = str2.readString();
            str13 = str2.readString();
            j = str2.readInt();
            bool1 = compileLayouts(str10, str8, str13, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 17:
            str2.enforceInterface("android.os.IInstalld");
            str16 = str2.readString();
            i6 = str2.readInt();
            str17 = str2.readString();
            str18 = str2.readString();
            i = str2.readInt();
            str10 = str2.readString();
            i7 = str2.readInt();
            str19 = str2.readString();
            str20 = str2.readString();
            str21 = str2.readString();
            str8 = str2.readString();
            if (str2.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            param1Int2 = str2.readInt();
            str13 = str2.readString();
            str15 = str2.readString();
            str2 = str2.readString();
            dexopt(str16, i6, str17, str18, i, str10, i7, str19, str20, str21, str8, bool, param1Int2, str13, str15, str2);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            str2.enforceInterface("android.os.IInstalld");
            str10 = str2.readString();
            str15 = str2.readString();
            str8 = str2.readString();
            i = str2.readInt();
            str13 = str2.readString();
            param1Int2 = str2.readInt();
            str2 = str2.readString();
            moveCompleteApp(str10, str15, str8, i, str13, param1Int2, str2);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str2.enforceInterface("android.os.IInstalld");
            str8 = str2.readString();
            param1Int2 = str2.readInt();
            i = str2.readInt();
            l1 = str2.readLong();
            setAppQuota(str8, param1Int2, i, l1);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str2.enforceInterface("android.os.IInstalld");
            str8 = str2.readString();
            i = str2.readInt();
            arrayOfCrateMetadata = getUserCrates(str8, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfCrateMetadata, 1);
            return true;
          case 13:
            arrayOfCrateMetadata.enforceInterface("android.os.IInstalld");
            str13 = arrayOfCrateMetadata.readString();
            arrayOfString4 = arrayOfCrateMetadata.createStringArray();
            i = arrayOfCrateMetadata.readInt();
            arrayOfCrateMetadata = getAppCrates(str13, arrayOfString4, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfCrateMetadata, 1);
            return true;
          case 12:
            arrayOfCrateMetadata.enforceInterface("android.os.IInstalld");
            str7 = arrayOfCrateMetadata.readString();
            param1Int2 = arrayOfCrateMetadata.readInt();
            i = arrayOfCrateMetadata.readInt();
            arrayOfInt3 = arrayOfCrateMetadata.createIntArray();
            arrayOfLong3 = getExternalSize(str7, param1Int2, i, arrayOfInt3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLongArray(arrayOfLong3);
            return true;
          case 11:
            arrayOfLong3.enforceInterface("android.os.IInstalld");
            str7 = arrayOfLong3.readString();
            i = arrayOfLong3.readInt();
            param1Int2 = arrayOfLong3.readInt();
            arrayOfInt2 = arrayOfLong3.createIntArray();
            arrayOfLong2 = getUserSize(str7, i, param1Int2, arrayOfInt2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLongArray(arrayOfLong2);
            return true;
          case 10:
            arrayOfLong2.enforceInterface("android.os.IInstalld");
            str13 = arrayOfLong2.readString();
            arrayOfString3 = arrayOfLong2.createStringArray();
            i6 = arrayOfLong2.readInt();
            i = arrayOfLong2.readInt();
            param1Int2 = arrayOfLong2.readInt();
            arrayOfLong4 = arrayOfLong2.createLongArray();
            arrayOfString1 = arrayOfLong2.createStringArray();
            arrayOfLong1 = getAppSize(str13, arrayOfString3, i6, i, param1Int2, arrayOfLong4, arrayOfString1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLongArray(arrayOfLong1);
            return true;
          case 9:
            arrayOfLong1.enforceInterface("android.os.IInstalld");
            str6 = arrayOfLong1.readString();
            i = arrayOfLong1.readInt();
            fixupAppData(str6, i);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            arrayOfLong1.enforceInterface("android.os.IInstalld");
            str13 = arrayOfLong1.readString();
            str6 = arrayOfLong1.readString();
            param1Int2 = arrayOfLong1.readInt();
            i = arrayOfLong1.readInt();
            l1 = arrayOfLong1.readLong();
            destroyAppData(str13, str6, param1Int2, i, l1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            arrayOfLong1.enforceInterface("android.os.IInstalld");
            str6 = arrayOfLong1.readString();
            str13 = arrayOfLong1.readString();
            i = arrayOfLong1.readInt();
            param1Int2 = arrayOfLong1.readInt();
            l1 = arrayOfLong1.readLong();
            clearAppData(str6, str13, i, param1Int2, l1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            arrayOfLong1.enforceInterface("android.os.IInstalld");
            str6 = arrayOfLong1.readString();
            str13 = arrayOfLong1.readString();
            i = arrayOfLong1.readInt();
            param1Int2 = arrayOfLong1.readInt();
            migrateAppData(str6, str13, i, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            arrayOfLong1.enforceInterface("android.os.IInstalld");
            str13 = arrayOfLong1.readString();
            str6 = arrayOfLong1.readString();
            i = arrayOfLong1.readInt();
            param1Int2 = arrayOfLong1.readInt();
            i6 = arrayOfLong1.readInt();
            str1 = arrayOfLong1.readString();
            restoreconAppData(str13, str6, i, param1Int2, i6, str1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("android.os.IInstalld");
            arrayOfString7 = str1.createStringArray();
            arrayOfString2 = str1.createStringArray();
            i = str1.readInt();
            param1Int2 = str1.readInt();
            arrayOfInt5 = str1.createIntArray();
            arrayOfString5 = str1.createStringArray();
            arrayOfInt1 = str1.createIntArray();
            l1 = createAppDataBatched(arrayOfString7, arrayOfString2, i, param1Int2, arrayOfInt5, arrayOfString5, arrayOfInt1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l1);
            return true;
          case 3:
            arrayOfInt1.enforceInterface("android.os.IInstalld");
            str5 = arrayOfInt1.readString();
            str12 = arrayOfInt1.readString();
            param1Int2 = arrayOfInt1.readInt();
            i = arrayOfInt1.readInt();
            i6 = arrayOfInt1.readInt();
            str9 = arrayOfInt1.readString();
            i7 = arrayOfInt1.readInt();
            l1 = createAppData(str5, str12, param1Int2, i, i6, str9, i7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l1);
            return true;
          case 2:
            arrayOfInt1.enforceInterface("android.os.IInstalld");
            str5 = arrayOfInt1.readString();
            i = arrayOfInt1.readInt();
            param1Int2 = arrayOfInt1.readInt();
            destroyUserData(str5, i, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfInt1.enforceInterface("android.os.IInstalld");
        String str5 = arrayOfInt1.readString();
        param1Int2 = arrayOfInt1.readInt();
        int i6 = arrayOfInt1.readInt();
        int i = arrayOfInt1.readInt();
        createUserData(str5, param1Int2, i6, i);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.os.IInstalld");
      return true;
    }
    
    private static class Proxy implements IInstalld {
      public static IInstalld sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IInstalld";
      }
      
      public void createUserData(String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().createUserData(param2String, param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void destroyUserData(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().destroyUserData(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long createAppData(String param2String1, String param2String2, int param2Int1, int param2Int2, int param2Int3, String param2String3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    parcel1.writeInt(param2Int3);
                    parcel1.writeString(param2String3);
                    parcel1.writeInt(param2Int4);
                    boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
                    if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
                      long l1 = IInstalld.Stub.getDefaultImpl().createAppData(param2String1, param2String2, param2Int1, param2Int2, param2Int3, param2String3, param2Int4);
                      parcel2.recycle();
                      parcel1.recycle();
                      return l1;
                    } 
                    parcel2.readException();
                    long l = parcel2.readLong();
                    parcel2.recycle();
                    parcel1.recycle();
                    return l;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public long createAppDataBatched(String[] param2ArrayOfString1, String[] param2ArrayOfString2, int param2Int1, int param2Int2, int[] param2ArrayOfint1, String[] param2ArrayOfString3, int[] param2ArrayOfint2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeStringArray(param2ArrayOfString1);
            try {
              parcel1.writeStringArray(param2ArrayOfString2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    parcel1.writeIntArray(param2ArrayOfint1);
                    parcel1.writeStringArray(param2ArrayOfString3);
                    parcel1.writeIntArray(param2ArrayOfint2);
                    boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
                    if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
                      long l1 = IInstalld.Stub.getDefaultImpl().createAppDataBatched(param2ArrayOfString1, param2ArrayOfString2, param2Int1, param2Int2, param2ArrayOfint1, param2ArrayOfString3, param2ArrayOfint2);
                      parcel2.recycle();
                      parcel1.recycle();
                      return l1;
                    } 
                    parcel2.readException();
                    long l = parcel2.readLong();
                    parcel2.recycle();
                    parcel1.recycle();
                    return l;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ArrayOfString1;
      }
      
      public void restoreconAppData(String param2String1, String param2String2, int param2Int1, int param2Int2, int param2Int3, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    parcel1.writeInt(param2Int3);
                    try {
                      parcel1.writeString(param2String3);
                      boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
                      if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
                        IInstalld.Stub.getDefaultImpl().restoreconAppData(param2String1, param2String2, param2Int1, param2Int2, param2Int3, param2String3);
                        parcel2.recycle();
                        parcel1.recycle();
                        return;
                      } 
                      parcel2.readException();
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void migrateAppData(String param2String1, String param2String2, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().migrateAppData(param2String1, param2String2, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearAppData(String param2String1, String param2String2, int param2Int1, int param2Int2, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    parcel1.writeLong(param2Long);
                    boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
                    if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
                      IInstalld.Stub.getDefaultImpl().clearAppData(param2String1, param2String2, param2Int1, param2Int2, param2Long);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void destroyAppData(String param2String1, String param2String2, int param2Int1, int param2Int2, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    parcel1.writeLong(param2Long);
                    boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
                    if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
                      IInstalld.Stub.getDefaultImpl().destroyAppData(param2String1, param2String2, param2Int1, param2Int2, param2Long);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void fixupAppData(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().fixupAppData(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long[] getAppSize(String param2String, String[] param2ArrayOfString1, int param2Int1, int param2Int2, int param2Int3, long[] param2ArrayOflong, String[] param2ArrayOfString2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeString(param2String);
            try {
              parcel1.writeStringArray(param2ArrayOfString1);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    parcel1.writeInt(param2Int3);
                    parcel1.writeLongArray(param2ArrayOflong);
                    parcel1.writeStringArray(param2ArrayOfString2);
                    boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
                    if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
                      long[] arrayOfLong1 = IInstalld.Stub.getDefaultImpl().getAppSize(param2String, param2ArrayOfString1, param2Int1, param2Int2, param2Int3, param2ArrayOflong, param2ArrayOfString2);
                      parcel2.recycle();
                      parcel1.recycle();
                      return arrayOfLong1;
                    } 
                    parcel2.readException();
                    long[] arrayOfLong = parcel2.createLongArray();
                    parcel2.recycle();
                    parcel1.recycle();
                    return arrayOfLong;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public long[] getUserSize(String param2String, int param2Int1, int param2Int2, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null)
            return IInstalld.Stub.getDefaultImpl().getUserSize(param2String, param2Int1, param2Int2, param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createLongArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long[] getExternalSize(String param2String, int param2Int1, int param2Int2, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null)
            return IInstalld.Stub.getDefaultImpl().getExternalSize(param2String, param2Int1, param2Int2, param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createLongArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CrateMetadata[] getAppCrates(String param2String, String[] param2ArrayOfString, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null)
            return IInstalld.Stub.getDefaultImpl().getAppCrates(param2String, param2ArrayOfString, param2Int); 
          parcel2.readException();
          return parcel2.<CrateMetadata>createTypedArray(CrateMetadata.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CrateMetadata[] getUserCrates(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null)
            return IInstalld.Stub.getDefaultImpl().getUserCrates(param2String, param2Int); 
          parcel2.readException();
          return parcel2.<CrateMetadata>createTypedArray(CrateMetadata.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAppQuota(String param2String, int param2Int1, int param2Int2, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().setAppQuota(param2String, param2Int1, param2Int2, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void moveCompleteApp(String param2String1, String param2String2, String param2String3, int param2Int1, String param2String4, int param2Int2, String param2String5) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeString(param2String3);
                try {
                  parcel1.writeInt(param2Int1);
                  try {
                    parcel1.writeString(param2String4);
                    parcel1.writeInt(param2Int2);
                    parcel1.writeString(param2String5);
                    boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
                    if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
                      IInstalld.Stub.getDefaultImpl().moveCompleteApp(param2String1, param2String2, param2String3, param2Int1, param2String4, param2Int2, param2String5);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void dexopt(String param2String1, int param2Int1, String param2String2, String param2String3, int param2Int2, String param2String4, int param2Int3, String param2String5, String param2String6, String param2String7, String param2String8, boolean param2Boolean, int param2Int4, String param2String9, String param2String10, String param2String11) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String4);
          parcel1.writeInt(param2Int3);
          parcel1.writeString(param2String5);
          parcel1.writeString(param2String6);
          parcel1.writeString(param2String7);
          parcel1.writeString(param2String8);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int4);
          parcel1.writeString(param2String9);
          parcel1.writeString(param2String10);
          parcel1.writeString(param2String11);
          boolean bool1 = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool1 && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().dexopt(param2String1, param2Int1, param2String2, param2String3, param2Int2, param2String4, param2Int3, param2String5, param2String6, param2String7, param2String8, param2Boolean, param2Int4, param2String9, param2String10, param2String11);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean compileLayouts(String param2String1, String param2String2, String param2String3, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(18, parcel1, parcel2, 0);
          if (!bool2 && IInstalld.Stub.getDefaultImpl() != null) {
            bool1 = IInstalld.Stub.getDefaultImpl().compileLayouts(param2String1, param2String2, param2String3, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void rmdex(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().rmdex(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean mergeProfiles(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(20, parcel1, parcel2, 0);
          if (!bool2 && IInstalld.Stub.getDefaultImpl() != null) {
            bool1 = IInstalld.Stub.getDefaultImpl().mergeProfiles(param2Int, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean dumpProfiles(int param2Int, String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(21, parcel1, parcel2, 0);
          if (!bool2 && IInstalld.Stub.getDefaultImpl() != null) {
            bool1 = IInstalld.Stub.getDefaultImpl().dumpProfiles(param2Int, param2String1, param2String2, param2String3);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean copySystemProfile(String param2String1, int param2Int, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(22, parcel1, parcel2, 0);
          if (!bool2 && IInstalld.Stub.getDefaultImpl() != null) {
            bool1 = IInstalld.Stub.getDefaultImpl().copySystemProfile(param2String1, param2Int, param2String2, param2String3);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearAppProfiles(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().clearAppProfiles(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void destroyAppProfiles(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().destroyAppProfiles(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean createProfileSnapshot(int param2Int, String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(25, parcel1, parcel2, 0);
          if (!bool2 && IInstalld.Stub.getDefaultImpl() != null) {
            bool1 = IInstalld.Stub.getDefaultImpl().createProfileSnapshot(param2Int, param2String1, param2String2, param2String3);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void destroyProfileSnapshot(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().destroyProfileSnapshot(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void rmPackageDir(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().rmPackageDir(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void freeCache(String param2String, long param2Long1, long param2Long2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeString(param2String);
            try {
              parcel1.writeLong(param2Long1);
              try {
                parcel1.writeLong(param2Long2);
                try {
                  parcel1.writeInt(param2Int);
                  boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
                  if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
                    IInstalld.Stub.getDefaultImpl().freeCache(param2String, param2Long1, param2Long2, param2Int);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void linkNativeLibraryDirectory(String param2String1, String param2String2, String param2String3, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().linkNativeLibraryDirectory(param2String1, param2String2, param2String3, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void createOatDir(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().createOatDir(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void linkFile(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().linkFile(param2String1, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void moveAb(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().moveAb(param2String1, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteOdex(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().deleteOdex(param2String1, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void installApkVerity(String param2String, FileDescriptor param2FileDescriptor, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          parcel1.writeRawFileDescriptor(param2FileDescriptor);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().installApkVerity(param2String, param2FileDescriptor, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void assertFsverityRootHashMatches(String param2String, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().assertFsverityRootHashMatches(param2String, param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean reconcileSecondaryDexFile(String param2String1, String param2String2, int param2Int1, String[] param2ArrayOfString, String param2String3, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeStringArray(param2ArrayOfString);
                  try {
                    parcel1.writeString(param2String3);
                    try {
                      parcel1.writeInt(param2Int2);
                      IBinder iBinder = this.mRemote;
                      boolean bool1 = false, bool2 = iBinder.transact(36, parcel1, parcel2, 0);
                      if (!bool2 && IInstalld.Stub.getDefaultImpl() != null) {
                        bool1 = IInstalld.Stub.getDefaultImpl().reconcileSecondaryDexFile(param2String1, param2String2, param2Int1, param2ArrayOfString, param2String3, param2Int2);
                        parcel2.recycle();
                        parcel1.recycle();
                        return bool1;
                      } 
                      parcel2.readException();
                      param2Int1 = parcel2.readInt();
                      if (param2Int1 != 0)
                        bool1 = true; 
                      parcel2.recycle();
                      parcel1.recycle();
                      return bool1;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public byte[] hashSecondaryDexFile(String param2String1, String param2String2, int param2Int1, String param2String3, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String3);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null)
            return IInstalld.Stub.getDefaultImpl().hashSecondaryDexFile(param2String1, param2String2, param2Int1, param2String3, param2Int2); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void invalidateMounts() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().invalidateMounts();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isQuotaSupported(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(39, parcel1, parcel2, 0);
          if (!bool2 && IInstalld.Stub.getDefaultImpl() != null) {
            bool1 = IInstalld.Stub.getDefaultImpl().isQuotaSupported(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean prepareAppProfile(String param2String1, int param2Int1, int param2Int2, String param2String2, String param2String3, String param2String4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeInt(param2Int1);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeString(param2String2);
                  try {
                    parcel1.writeString(param2String3);
                    try {
                      parcel1.writeString(param2String4);
                      IBinder iBinder = this.mRemote;
                      boolean bool1 = false, bool2 = iBinder.transact(40, parcel1, parcel2, 0);
                      if (!bool2 && IInstalld.Stub.getDefaultImpl() != null) {
                        bool1 = IInstalld.Stub.getDefaultImpl().prepareAppProfile(param2String1, param2Int1, param2Int2, param2String2, param2String3, param2String4);
                        parcel2.recycle();
                        parcel1.recycle();
                        return bool1;
                      } 
                      parcel2.readException();
                      param2Int1 = parcel2.readInt();
                      if (param2Int1 != 0)
                        bool1 = true; 
                      parcel2.recycle();
                      parcel1.recycle();
                      return bool1;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public long snapshotAppData(String param2String1, String param2String2, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null)
            return IInstalld.Stub.getDefaultImpl().snapshotAppData(param2String1, param2String2, param2Int1, param2Int2, param2Int3); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void restoreAppDataSnapshot(String param2String1, String param2String2, int param2Int1, String param2String3, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeString(param2String3);
                  try {
                    parcel1.writeInt(param2Int2);
                    parcel1.writeInt(param2Int3);
                    parcel1.writeInt(param2Int4);
                    boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
                    if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
                      IInstalld.Stub.getDefaultImpl().restoreAppDataSnapshot(param2String1, param2String2, param2Int1, param2String3, param2Int2, param2Int3, param2Int4);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void destroyAppDataSnapshot(String param2String1, String param2String2, int param2Int1, long param2Long, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeLong(param2Long);
                  parcel1.writeInt(param2Int2);
                  parcel1.writeInt(param2Int3);
                  boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
                  if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
                    IInstalld.Stub.getDefaultImpl().destroyAppDataSnapshot(param2String1, param2String2, param2Int1, param2Long, param2Int2, param2Int3);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void destroyCeSnapshotsNotSpecified(String param2String, int param2Int, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().destroyCeSnapshotsNotSpecified(param2String, param2Int, param2ArrayOfint);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void tryMountDataMirror(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().tryMountDataMirror(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onPrivateVolumeRemoved(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().onPrivateVolumeRemoved(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void migrateLegacyObbData() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().migrateLegacyObbData();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateAppProfile(String param2String1, int param2Int1, int param2Int2, String param2String2, String param2String3, String param2String4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeInt(param2Int1);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeString(param2String2);
                  try {
                    parcel1.writeString(param2String3);
                    try {
                      parcel1.writeString(param2String4);
                      IBinder iBinder = this.mRemote;
                      boolean bool1 = false, bool2 = iBinder.transact(48, parcel1, parcel2, 0);
                      if (!bool2 && IInstalld.Stub.getDefaultImpl() != null) {
                        bool1 = IInstalld.Stub.getDefaultImpl().updateAppProfile(param2String1, param2Int1, param2Int2, param2String2, param2String3, param2String4);
                        parcel2.recycle();
                        parcel1.recycle();
                        return bool1;
                      } 
                      parcel2.readException();
                      param2Int1 = parcel2.readInt();
                      if (param2Int1 != 0)
                        bool1 = true; 
                      parcel2.recycle();
                      parcel1.recycle();
                      return bool1;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public boolean dumpAppClassAndMethod(int param2Int, String param2String1, String param2String2, String param2String3, String param2String4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeString(param2String1);
              try {
                parcel1.writeString(param2String2);
                try {
                  parcel1.writeString(param2String3);
                  try {
                    parcel1.writeString(param2String4);
                    try {
                      IBinder iBinder = this.mRemote;
                      boolean bool1 = false, bool2 = iBinder.transact(49, parcel1, parcel2, 0);
                      if (!bool2 && IInstalld.Stub.getDefaultImpl() != null) {
                        bool1 = IInstalld.Stub.getDefaultImpl().dumpAppClassAndMethod(param2Int, param2String1, param2String2, param2String3, param2String4);
                        parcel2.recycle();
                        parcel1.recycle();
                        return bool1;
                      } 
                      parcel2.readException();
                      param2Int = parcel2.readInt();
                      if (param2Int != 0)
                        bool1 = true; 
                      parcel2.recycle();
                      parcel1.recycle();
                      return bool1;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public boolean createAppProfile(int param2Int, String param2String1, String param2String2, String param2String3, String param2String4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeString(param2String1);
              try {
                parcel1.writeString(param2String2);
                try {
                  parcel1.writeString(param2String3);
                  try {
                    parcel1.writeString(param2String4);
                    try {
                      IBinder iBinder = this.mRemote;
                      boolean bool1 = false, bool2 = iBinder.transact(50, parcel1, parcel2, 0);
                      if (!bool2 && IInstalld.Stub.getDefaultImpl() != null) {
                        bool1 = IInstalld.Stub.getDefaultImpl().createAppProfile(param2Int, param2String1, param2String2, param2String3, param2String4);
                        parcel2.recycle();
                        parcel1.recycle();
                        return bool1;
                      } 
                      parcel2.readException();
                      param2Int = parcel2.readInt();
                      if (param2Int != 0)
                        bool1 = true; 
                      parcel2.recycle();
                      parcel1.recycle();
                      return bool1;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public long fixupAppDataForUidChange(String param2String1, String param2String2, int param2Int1, int param2Int2, int param2Int3, String param2String3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    parcel1.writeInt(param2Int3);
                    parcel1.writeString(param2String3);
                    parcel1.writeInt(param2Int4);
                    boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
                    if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
                      long l1 = IInstalld.Stub.getDefaultImpl().fixupAppDataForUidChange(param2String1, param2String2, param2Int1, param2Int2, param2Int3, param2String3, param2Int4);
                      parcel2.recycle();
                      parcel1.recycle();
                      return l1;
                    } 
                    parcel2.readException();
                    long l = parcel2.readLong();
                    parcel2.recycle();
                    parcel1.recycle();
                    return l;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void killRunDex2Oat() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IInstalld");
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && IInstalld.Stub.getDefaultImpl() != null) {
            IInstalld.Stub.getDefaultImpl().killRunDex2Oat();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInstalld param1IInstalld) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInstalld != null) {
          Proxy.sDefaultImpl = param1IInstalld;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInstalld getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
