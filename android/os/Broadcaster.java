package android.os;

public class Broadcaster {
  private Registration mReg;
  
  public void request(int paramInt1, Handler paramHandler, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mReg : Landroid/os/Broadcaster$Registration;
    //   6: ifnonnull -> 83
    //   9: new android/os/Broadcaster$Registration
    //   12: astore #4
    //   14: aload #4
    //   16: aload_0
    //   17: aconst_null
    //   18: invokespecial <init> : (Landroid/os/Broadcaster;Landroid/os/Broadcaster$1;)V
    //   21: aload #4
    //   23: iload_1
    //   24: putfield senderWhat : I
    //   27: aload #4
    //   29: iconst_1
    //   30: anewarray android/os/Handler
    //   33: putfield targets : [Landroid/os/Handler;
    //   36: aload #4
    //   38: iconst_1
    //   39: newarray int
    //   41: putfield targetWhats : [I
    //   44: aload #4
    //   46: getfield targets : [Landroid/os/Handler;
    //   49: iconst_0
    //   50: aload_2
    //   51: aastore
    //   52: aload #4
    //   54: getfield targetWhats : [I
    //   57: iconst_0
    //   58: iload_3
    //   59: iastore
    //   60: aload_0
    //   61: aload #4
    //   63: putfield mReg : Landroid/os/Broadcaster$Registration;
    //   66: aload #4
    //   68: aload #4
    //   70: putfield next : Landroid/os/Broadcaster$Registration;
    //   73: aload #4
    //   75: aload #4
    //   77: putfield prev : Landroid/os/Broadcaster$Registration;
    //   80: goto -> 367
    //   83: aload_0
    //   84: getfield mReg : Landroid/os/Broadcaster$Registration;
    //   87: astore #5
    //   89: aload #5
    //   91: astore #4
    //   93: aload #4
    //   95: getfield senderWhat : I
    //   98: iload_1
    //   99: if_icmplt -> 105
    //   102: goto -> 127
    //   105: aload #4
    //   107: getfield next : Landroid/os/Broadcaster$Registration;
    //   110: astore #6
    //   112: aload #6
    //   114: astore #4
    //   116: aload #6
    //   118: aload #5
    //   120: if_acmpne -> 93
    //   123: aload #6
    //   125: astore #4
    //   127: aload #4
    //   129: getfield senderWhat : I
    //   132: iload_1
    //   133: if_icmpeq -> 242
    //   136: new android/os/Broadcaster$Registration
    //   139: astore #6
    //   141: aload #6
    //   143: aload_0
    //   144: aconst_null
    //   145: invokespecial <init> : (Landroid/os/Broadcaster;Landroid/os/Broadcaster$1;)V
    //   148: aload #6
    //   150: iload_1
    //   151: putfield senderWhat : I
    //   154: aload #6
    //   156: iconst_1
    //   157: anewarray android/os/Handler
    //   160: putfield targets : [Landroid/os/Handler;
    //   163: aload #6
    //   165: iconst_1
    //   166: newarray int
    //   168: putfield targetWhats : [I
    //   171: aload #6
    //   173: aload #4
    //   175: putfield next : Landroid/os/Broadcaster$Registration;
    //   178: aload #6
    //   180: aload #4
    //   182: getfield prev : Landroid/os/Broadcaster$Registration;
    //   185: putfield prev : Landroid/os/Broadcaster$Registration;
    //   188: aload #4
    //   190: getfield prev : Landroid/os/Broadcaster$Registration;
    //   193: aload #6
    //   195: putfield next : Landroid/os/Broadcaster$Registration;
    //   198: aload #4
    //   200: aload #6
    //   202: putfield prev : Landroid/os/Broadcaster$Registration;
    //   205: aload #4
    //   207: aload_0
    //   208: getfield mReg : Landroid/os/Broadcaster$Registration;
    //   211: if_acmpne -> 233
    //   214: aload #4
    //   216: getfield senderWhat : I
    //   219: aload #6
    //   221: getfield senderWhat : I
    //   224: if_icmple -> 233
    //   227: aload_0
    //   228: aload #6
    //   230: putfield mReg : Landroid/os/Broadcaster$Registration;
    //   233: aload #6
    //   235: astore #4
    //   237: iconst_0
    //   238: istore_1
    //   239: goto -> 351
    //   242: aload #4
    //   244: getfield targets : [Landroid/os/Handler;
    //   247: arraylength
    //   248: istore #7
    //   250: aload #4
    //   252: getfield targets : [Landroid/os/Handler;
    //   255: astore #6
    //   257: aload #4
    //   259: getfield targetWhats : [I
    //   262: astore #5
    //   264: iconst_0
    //   265: istore_1
    //   266: iload_1
    //   267: iload #7
    //   269: if_icmpge -> 297
    //   272: aload #6
    //   274: iload_1
    //   275: aaload
    //   276: aload_2
    //   277: if_acmpne -> 291
    //   280: aload #5
    //   282: iload_1
    //   283: iaload
    //   284: iload_3
    //   285: if_icmpne -> 291
    //   288: aload_0
    //   289: monitorexit
    //   290: return
    //   291: iinc #1, 1
    //   294: goto -> 266
    //   297: aload #4
    //   299: iload #7
    //   301: iconst_1
    //   302: iadd
    //   303: anewarray android/os/Handler
    //   306: putfield targets : [Landroid/os/Handler;
    //   309: aload #6
    //   311: iconst_0
    //   312: aload #4
    //   314: getfield targets : [Landroid/os/Handler;
    //   317: iconst_0
    //   318: iload #7
    //   320: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   323: aload #4
    //   325: iload #7
    //   327: iconst_1
    //   328: iadd
    //   329: newarray int
    //   331: putfield targetWhats : [I
    //   334: aload #5
    //   336: iconst_0
    //   337: aload #4
    //   339: getfield targetWhats : [I
    //   342: iconst_0
    //   343: iload #7
    //   345: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   348: iload #7
    //   350: istore_1
    //   351: aload #4
    //   353: getfield targets : [Landroid/os/Handler;
    //   356: iload_1
    //   357: aload_2
    //   358: aastore
    //   359: aload #4
    //   361: getfield targetWhats : [I
    //   364: iload_1
    //   365: iload_3
    //   366: iastore
    //   367: aload_0
    //   368: monitorexit
    //   369: return
    //   370: astore_2
    //   371: aload_0
    //   372: monitorexit
    //   373: aload_2
    //   374: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #38	-> 0
    //   #39	-> 2
    //   #40	-> 2
    //   #41	-> 9
    //   #42	-> 21
    //   #43	-> 27
    //   #44	-> 36
    //   #45	-> 44
    //   #46	-> 52
    //   #47	-> 60
    //   #48	-> 66
    //   #49	-> 73
    //   #52	-> 83
    //   #53	-> 89
    //   #55	-> 93
    //   #56	-> 102
    //   #58	-> 105
    //   #59	-> 112
    //   #61	-> 127
    //   #64	-> 136
    //   #65	-> 148
    //   #66	-> 154
    //   #67	-> 163
    //   #68	-> 171
    //   #69	-> 178
    //   #70	-> 188
    //   #71	-> 198
    //   #73	-> 205
    //   #74	-> 227
    //   #77	-> 233
    //   #78	-> 237
    //   #79	-> 239
    //   #80	-> 242
    //   #81	-> 250
    //   #82	-> 257
    //   #84	-> 264
    //   #85	-> 272
    //   #86	-> 288
    //   #84	-> 291
    //   #89	-> 297
    //   #90	-> 309
    //   #91	-> 323
    //   #92	-> 334
    //   #94	-> 351
    //   #95	-> 359
    //   #97	-> 367
    //   #98	-> 369
    //   #97	-> 370
    // Exception table:
    //   from	to	target	type
    //   2	9	370	finally
    //   9	21	370	finally
    //   21	27	370	finally
    //   27	36	370	finally
    //   36	44	370	finally
    //   44	52	370	finally
    //   52	60	370	finally
    //   60	66	370	finally
    //   66	73	370	finally
    //   73	80	370	finally
    //   83	89	370	finally
    //   93	102	370	finally
    //   105	112	370	finally
    //   127	136	370	finally
    //   136	148	370	finally
    //   148	154	370	finally
    //   154	163	370	finally
    //   163	171	370	finally
    //   171	178	370	finally
    //   178	188	370	finally
    //   188	198	370	finally
    //   198	205	370	finally
    //   205	227	370	finally
    //   227	233	370	finally
    //   242	250	370	finally
    //   250	257	370	finally
    //   257	264	370	finally
    //   288	290	370	finally
    //   297	309	370	finally
    //   309	323	370	finally
    //   323	334	370	finally
    //   334	348	370	finally
    //   351	359	370	finally
    //   359	367	370	finally
    //   367	369	370	finally
    //   371	373	370	finally
  }
  
  public void cancelRequest(int paramInt1, Handler paramHandler, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mReg : Landroid/os/Broadcaster$Registration;
    //   6: astore #4
    //   8: aload #4
    //   10: astore #5
    //   12: aload #5
    //   14: astore #6
    //   16: aload #5
    //   18: ifnonnull -> 24
    //   21: aload_0
    //   22: monitorexit
    //   23: return
    //   24: aload #6
    //   26: getfield senderWhat : I
    //   29: iload_1
    //   30: if_icmplt -> 36
    //   33: goto -> 58
    //   36: aload #6
    //   38: getfield next : Landroid/os/Broadcaster$Registration;
    //   41: astore #5
    //   43: aload #5
    //   45: astore #6
    //   47: aload #5
    //   49: aload #4
    //   51: if_acmpne -> 24
    //   54: aload #5
    //   56: astore #6
    //   58: aload #6
    //   60: getfield senderWhat : I
    //   63: iload_1
    //   64: if_icmpne -> 213
    //   67: aload #6
    //   69: getfield targets : [Landroid/os/Handler;
    //   72: astore #5
    //   74: aload #6
    //   76: getfield targetWhats : [I
    //   79: astore #4
    //   81: aload #5
    //   83: arraylength
    //   84: istore #7
    //   86: iconst_0
    //   87: istore_1
    //   88: iload_1
    //   89: iload #7
    //   91: if_icmpge -> 213
    //   94: aload #5
    //   96: iload_1
    //   97: aaload
    //   98: aload_2
    //   99: if_acmpne -> 207
    //   102: aload #4
    //   104: iload_1
    //   105: iaload
    //   106: iload_3
    //   107: if_icmpne -> 207
    //   110: aload #6
    //   112: iload #7
    //   114: iconst_1
    //   115: isub
    //   116: anewarray android/os/Handler
    //   119: putfield targets : [Landroid/os/Handler;
    //   122: aload #6
    //   124: iload #7
    //   126: iconst_1
    //   127: isub
    //   128: newarray int
    //   130: putfield targetWhats : [I
    //   133: iload_1
    //   134: ifle -> 163
    //   137: aload #5
    //   139: iconst_0
    //   140: aload #6
    //   142: getfield targets : [Landroid/os/Handler;
    //   145: iconst_0
    //   146: iload_1
    //   147: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   150: aload #4
    //   152: iconst_0
    //   153: aload #6
    //   155: getfield targetWhats : [I
    //   158: iconst_0
    //   159: iload_1
    //   160: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   163: iload #7
    //   165: iload_1
    //   166: isub
    //   167: iconst_1
    //   168: isub
    //   169: istore_3
    //   170: iload_3
    //   171: ifeq -> 213
    //   174: aload #5
    //   176: iload_1
    //   177: iconst_1
    //   178: iadd
    //   179: aload #6
    //   181: getfield targets : [Landroid/os/Handler;
    //   184: iload_1
    //   185: iload_3
    //   186: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   189: aload #4
    //   191: iload_1
    //   192: iconst_1
    //   193: iadd
    //   194: aload #6
    //   196: getfield targetWhats : [I
    //   199: iload_1
    //   200: iload_3
    //   201: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   204: goto -> 213
    //   207: iinc #1, 1
    //   210: goto -> 88
    //   213: aload_0
    //   214: monitorexit
    //   215: return
    //   216: astore_2
    //   217: aload_0
    //   218: monitorexit
    //   219: aload_2
    //   220: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #106	-> 0
    //   #107	-> 2
    //   #108	-> 8
    //   #110	-> 12
    //   #111	-> 21
    //   #115	-> 24
    //   #116	-> 33
    //   #118	-> 36
    //   #119	-> 43
    //   #121	-> 58
    //   #122	-> 67
    //   #123	-> 74
    //   #124	-> 81
    //   #125	-> 86
    //   #126	-> 94
    //   #127	-> 110
    //   #128	-> 122
    //   #129	-> 133
    //   #130	-> 137
    //   #131	-> 150
    //   #134	-> 163
    //   #135	-> 170
    //   #136	-> 174
    //   #138	-> 189
    //   #125	-> 207
    //   #145	-> 213
    //   #146	-> 215
    //   #145	-> 216
    // Exception table:
    //   from	to	target	type
    //   2	8	216	finally
    //   21	23	216	finally
    //   24	33	216	finally
    //   36	43	216	finally
    //   58	67	216	finally
    //   67	74	216	finally
    //   74	81	216	finally
    //   81	86	216	finally
    //   110	122	216	finally
    //   122	133	216	finally
    //   137	150	216	finally
    //   150	163	216	finally
    //   174	189	216	finally
    //   189	204	216	finally
    //   213	215	216	finally
    //   217	219	216	finally
  }
  
  public void dumpRegistrations() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mReg : Landroid/os/Broadcaster$Registration;
    //   6: astore_1
    //   7: getstatic java/lang/System.out : Ljava/io/PrintStream;
    //   10: astore_2
    //   11: new java/lang/StringBuilder
    //   14: astore_3
    //   15: aload_3
    //   16: invokespecial <init> : ()V
    //   19: aload_3
    //   20: ldc 'Broadcaster '
    //   22: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   25: pop
    //   26: aload_3
    //   27: aload_0
    //   28: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   31: pop
    //   32: aload_3
    //   33: ldc ' {'
    //   35: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   38: pop
    //   39: aload_2
    //   40: aload_3
    //   41: invokevirtual toString : ()Ljava/lang/String;
    //   44: invokevirtual println : (Ljava/lang/String;)V
    //   47: aload_1
    //   48: ifnull -> 194
    //   51: aload_1
    //   52: astore_3
    //   53: getstatic java/lang/System.out : Ljava/io/PrintStream;
    //   56: astore_2
    //   57: new java/lang/StringBuilder
    //   60: astore #4
    //   62: aload #4
    //   64: invokespecial <init> : ()V
    //   67: aload #4
    //   69: ldc '    senderWhat='
    //   71: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   74: pop
    //   75: aload #4
    //   77: aload_3
    //   78: getfield senderWhat : I
    //   81: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   84: pop
    //   85: aload_2
    //   86: aload #4
    //   88: invokevirtual toString : ()Ljava/lang/String;
    //   91: invokevirtual println : (Ljava/lang/String;)V
    //   94: aload_3
    //   95: getfield targets : [Landroid/os/Handler;
    //   98: arraylength
    //   99: istore #5
    //   101: iconst_0
    //   102: istore #6
    //   104: iload #6
    //   106: iload #5
    //   108: if_icmpge -> 182
    //   111: getstatic java/lang/System.out : Ljava/io/PrintStream;
    //   114: astore_2
    //   115: new java/lang/StringBuilder
    //   118: astore #4
    //   120: aload #4
    //   122: invokespecial <init> : ()V
    //   125: aload #4
    //   127: ldc '        ['
    //   129: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   132: pop
    //   133: aload #4
    //   135: aload_3
    //   136: getfield targetWhats : [I
    //   139: iload #6
    //   141: iaload
    //   142: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   145: pop
    //   146: aload #4
    //   148: ldc '] '
    //   150: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   153: pop
    //   154: aload #4
    //   156: aload_3
    //   157: getfield targets : [Landroid/os/Handler;
    //   160: iload #6
    //   162: aaload
    //   163: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   166: pop
    //   167: aload_2
    //   168: aload #4
    //   170: invokevirtual toString : ()Ljava/lang/String;
    //   173: invokevirtual println : (Ljava/lang/String;)V
    //   176: iinc #6, 1
    //   179: goto -> 104
    //   182: aload_3
    //   183: getfield next : Landroid/os/Broadcaster$Registration;
    //   186: astore_2
    //   187: aload_2
    //   188: astore_3
    //   189: aload_2
    //   190: aload_1
    //   191: if_acmpne -> 53
    //   194: getstatic java/lang/System.out : Ljava/io/PrintStream;
    //   197: ldc '}'
    //   199: invokevirtual println : (Ljava/lang/String;)V
    //   202: aload_0
    //   203: monitorexit
    //   204: return
    //   205: astore_3
    //   206: aload_0
    //   207: monitorexit
    //   208: aload_3
    //   209: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #153	-> 0
    //   #154	-> 2
    //   #155	-> 7
    //   #156	-> 47
    //   #157	-> 51
    //   #159	-> 53
    //   #160	-> 94
    //   #161	-> 101
    //   #162	-> 111
    //   #161	-> 176
    //   #165	-> 182
    //   #166	-> 187
    //   #168	-> 194
    //   #169	-> 202
    //   #170	-> 204
    //   #169	-> 205
    // Exception table:
    //   from	to	target	type
    //   2	7	205	finally
    //   7	47	205	finally
    //   53	94	205	finally
    //   94	101	205	finally
    //   111	176	205	finally
    //   182	187	205	finally
    //   194	202	205	finally
    //   202	204	205	finally
    //   206	208	205	finally
  }
  
  public void broadcast(Message paramMessage) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mReg : Landroid/os/Broadcaster$Registration;
    //   6: ifnonnull -> 12
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: aload_1
    //   13: getfield what : I
    //   16: istore_2
    //   17: aload_0
    //   18: getfield mReg : Landroid/os/Broadcaster$Registration;
    //   21: astore_3
    //   22: aload_3
    //   23: astore #4
    //   25: aload #4
    //   27: getfield senderWhat : I
    //   30: iload_2
    //   31: if_icmplt -> 37
    //   34: goto -> 58
    //   37: aload #4
    //   39: getfield next : Landroid/os/Broadcaster$Registration;
    //   42: astore #5
    //   44: aload #5
    //   46: astore #4
    //   48: aload #5
    //   50: aload_3
    //   51: if_acmpne -> 25
    //   54: aload #5
    //   56: astore #4
    //   58: aload #4
    //   60: getfield senderWhat : I
    //   63: iload_2
    //   64: if_icmpne -> 132
    //   67: aload #4
    //   69: getfield targets : [Landroid/os/Handler;
    //   72: astore #5
    //   74: aload #4
    //   76: getfield targetWhats : [I
    //   79: astore #6
    //   81: aload #5
    //   83: arraylength
    //   84: istore #7
    //   86: iconst_0
    //   87: istore_2
    //   88: iload_2
    //   89: iload #7
    //   91: if_icmpge -> 132
    //   94: aload #5
    //   96: iload_2
    //   97: aaload
    //   98: astore_3
    //   99: invokestatic obtain : ()Landroid/os/Message;
    //   102: astore #4
    //   104: aload #4
    //   106: aload_1
    //   107: invokevirtual copyFrom : (Landroid/os/Message;)V
    //   110: aload #4
    //   112: aload #6
    //   114: iload_2
    //   115: iaload
    //   116: putfield what : I
    //   119: aload_3
    //   120: aload #4
    //   122: invokevirtual sendMessage : (Landroid/os/Message;)Z
    //   125: pop
    //   126: iinc #2, 1
    //   129: goto -> 88
    //   132: aload_0
    //   133: monitorexit
    //   134: return
    //   135: astore_1
    //   136: aload_0
    //   137: monitorexit
    //   138: aload_1
    //   139: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #179	-> 0
    //   #180	-> 2
    //   #181	-> 9
    //   #184	-> 12
    //   #185	-> 17
    //   #186	-> 22
    //   #188	-> 25
    //   #189	-> 34
    //   #191	-> 37
    //   #192	-> 44
    //   #193	-> 58
    //   #194	-> 67
    //   #195	-> 74
    //   #196	-> 81
    //   #197	-> 86
    //   #198	-> 94
    //   #199	-> 99
    //   #200	-> 104
    //   #201	-> 110
    //   #202	-> 119
    //   #197	-> 126
    //   #205	-> 132
    //   #206	-> 134
    //   #205	-> 135
    // Exception table:
    //   from	to	target	type
    //   2	9	135	finally
    //   9	11	135	finally
    //   12	17	135	finally
    //   17	22	135	finally
    //   25	34	135	finally
    //   37	44	135	finally
    //   58	67	135	finally
    //   67	74	135	finally
    //   74	81	135	finally
    //   81	86	135	finally
    //   99	104	135	finally
    //   104	110	135	finally
    //   110	119	135	finally
    //   119	126	135	finally
    //   132	134	135	finally
    //   136	138	135	finally
  }
  
  private class Registration {
    Registration next;
    
    Registration prev;
    
    int senderWhat;
    
    int[] targetWhats;
    
    Handler[] targets;
    
    final Broadcaster this$0;
    
    private Registration() {}
  }
}
