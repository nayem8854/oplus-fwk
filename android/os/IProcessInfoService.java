package android.os;

public interface IProcessInfoService extends IInterface {
  void getProcessStatesAndOomScoresFromPids(int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3) throws RemoteException;
  
  void getProcessStatesFromPids(int[] paramArrayOfint1, int[] paramArrayOfint2) throws RemoteException;
  
  class Default implements IProcessInfoService {
    public void getProcessStatesFromPids(int[] param1ArrayOfint1, int[] param1ArrayOfint2) throws RemoteException {}
    
    public void getProcessStatesAndOomScoresFromPids(int[] param1ArrayOfint1, int[] param1ArrayOfint2, int[] param1ArrayOfint3) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IProcessInfoService {
    private static final String DESCRIPTOR = "android.os.IProcessInfoService";
    
    static final int TRANSACTION_getProcessStatesAndOomScoresFromPids = 2;
    
    static final int TRANSACTION_getProcessStatesFromPids = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IProcessInfoService");
    }
    
    public static IProcessInfoService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IProcessInfoService");
      if (iInterface != null && iInterface instanceof IProcessInfoService)
        return (IProcessInfoService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "getProcessStatesAndOomScoresFromPids";
      } 
      return "getProcessStatesFromPids";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      int[] arrayOfInt1;
      if (param1Int1 != 1) {
        int[] arrayOfInt4;
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.os.IProcessInfoService");
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.IProcessInfoService");
        int[] arrayOfInt3 = param1Parcel1.createIntArray();
        param1Int1 = param1Parcel1.readInt();
        if (param1Int1 < 0) {
          arrayOfInt4 = null;
        } else {
          arrayOfInt4 = new int[param1Int1];
        } 
        param1Int1 = param1Parcel1.readInt();
        if (param1Int1 < 0) {
          param1Parcel1 = null;
        } else {
          arrayOfInt1 = new int[param1Int1];
        } 
        getProcessStatesAndOomScoresFromPids(arrayOfInt3, arrayOfInt4, arrayOfInt1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeIntArray(arrayOfInt4);
        param1Parcel2.writeIntArray(arrayOfInt1);
        return true;
      } 
      arrayOfInt1.enforceInterface("android.os.IProcessInfoService");
      int[] arrayOfInt2 = arrayOfInt1.createIntArray();
      param1Int1 = arrayOfInt1.readInt();
      if (param1Int1 < 0) {
        arrayOfInt1 = null;
      } else {
        arrayOfInt1 = new int[param1Int1];
      } 
      getProcessStatesFromPids(arrayOfInt2, arrayOfInt1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeIntArray(arrayOfInt1);
      return true;
    }
    
    private static class Proxy implements IProcessInfoService {
      public static IProcessInfoService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IProcessInfoService";
      }
      
      public void getProcessStatesFromPids(int[] param2ArrayOfint1, int[] param2ArrayOfint2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IProcessInfoService");
          parcel1.writeIntArray(param2ArrayOfint1);
          if (param2ArrayOfint2 == null) {
            parcel1.writeInt(-1);
          } else {
            parcel1.writeInt(param2ArrayOfint2.length);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IProcessInfoService.Stub.getDefaultImpl() != null) {
            IProcessInfoService.Stub.getDefaultImpl().getProcessStatesFromPids(param2ArrayOfint1, param2ArrayOfint2);
            return;
          } 
          parcel2.readException();
          parcel2.readIntArray(param2ArrayOfint2);
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getProcessStatesAndOomScoresFromPids(int[] param2ArrayOfint1, int[] param2ArrayOfint2, int[] param2ArrayOfint3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IProcessInfoService");
          parcel1.writeIntArray(param2ArrayOfint1);
          if (param2ArrayOfint2 == null) {
            parcel1.writeInt(-1);
          } else {
            parcel1.writeInt(param2ArrayOfint2.length);
          } 
          if (param2ArrayOfint3 == null) {
            parcel1.writeInt(-1);
          } else {
            parcel1.writeInt(param2ArrayOfint3.length);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IProcessInfoService.Stub.getDefaultImpl() != null) {
            IProcessInfoService.Stub.getDefaultImpl().getProcessStatesAndOomScoresFromPids(param2ArrayOfint1, param2ArrayOfint2, param2ArrayOfint3);
            return;
          } 
          parcel2.readException();
          parcel2.readIntArray(param2ArrayOfint2);
          parcel2.readIntArray(param2ArrayOfint3);
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IProcessInfoService param1IProcessInfoService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IProcessInfoService != null) {
          Proxy.sDefaultImpl = param1IProcessInfoService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IProcessInfoService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
