package android.os;

import android.content.Context;
import android.util.Log;
import com.android.internal.os.IDropBoxManagerService;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public class DropBoxManager {
  public static final String ACTION_DROPBOX_ENTRY_ADDED = "android.intent.action.DROPBOX_ENTRY_ADDED";
  
  public static final String EXTRA_DROPPED_COUNT = "android.os.extra.DROPPED_COUNT";
  
  public static final String EXTRA_TAG = "tag";
  
  public static final String EXTRA_TIME = "time";
  
  private static final int HAS_BYTE_ARRAY = 8;
  
  public static final int IS_EMPTY = 1;
  
  public static final int IS_GZIPPED = 4;
  
  public static final int IS_TEXT = 2;
  
  private static final String TAG = "DropBoxManager";
  
  private final Context mContext;
  
  private final IDropBoxManagerService mService;
  
  class Entry implements Parcelable, Closeable {
    public Entry(DropBoxManager this$0, long param1Long) {
      if (this$0 != null) {
        this.mTag = (String)this$0;
        this.mTimeMillis = param1Long;
        this.mData = null;
        this.mFileDescriptor = null;
        this.mFlags = 1;
        return;
      } 
      throw new NullPointerException("tag == null");
    }
    
    public Entry(DropBoxManager this$0, long param1Long, String param1String1) {
      if (this$0 != null) {
        if (param1String1 != null) {
          this.mTag = (String)this$0;
          this.mTimeMillis = param1Long;
          this.mData = param1String1.getBytes();
          this.mFileDescriptor = null;
          this.mFlags = 2;
          return;
        } 
        throw new NullPointerException("text == null");
      } 
      throw new NullPointerException("tag == null");
    }
    
    public Entry(DropBoxManager this$0, long param1Long, byte[] param1ArrayOfbyte, int param1Int) {
      if (this$0 != null) {
        boolean bool2, bool1 = false;
        if ((param1Int & 0x1) != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        if (param1ArrayOfbyte == null)
          bool1 = true; 
        if (bool2 == bool1) {
          this.mTag = (String)this$0;
          this.mTimeMillis = param1Long;
          this.mData = param1ArrayOfbyte;
          this.mFileDescriptor = null;
          this.mFlags = param1Int;
          return;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Bad flags: ");
        stringBuilder.append(param1Int);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      throw new NullPointerException("tag == null");
    }
    
    public Entry(DropBoxManager this$0, long param1Long, ParcelFileDescriptor param1ParcelFileDescriptor, int param1Int) {
      if (this$0 != null) {
        boolean bool2, bool1 = false;
        if ((param1Int & 0x1) != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        if (param1ParcelFileDescriptor == null)
          bool1 = true; 
        if (bool2 == bool1) {
          this.mTag = (String)this$0;
          this.mTimeMillis = param1Long;
          this.mData = null;
          this.mFileDescriptor = param1ParcelFileDescriptor;
          this.mFlags = param1Int;
          return;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Bad flags: ");
        stringBuilder.append(param1Int);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      throw new NullPointerException("tag == null");
    }
    
    public Entry(DropBoxManager this$0, long param1Long, File param1File, int param1Int) throws IOException {
      if (this$0 != null) {
        if ((param1Int & 0x1) == 0) {
          this.mTag = (String)this$0;
          this.mTimeMillis = param1Long;
          this.mData = null;
          this.mFileDescriptor = ParcelFileDescriptor.open(param1File, 268435456);
          this.mFlags = param1Int;
          return;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Bad flags: ");
        stringBuilder.append(param1Int);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      throw new NullPointerException("tag == null");
    }
    
    public void close() {
      try {
        if (this.mFileDescriptor != null)
          this.mFileDescriptor.close(); 
      } catch (IOException iOException) {}
    }
    
    public String getTag() {
      return this.mTag;
    }
    
    public long getTimeMillis() {
      return this.mTimeMillis;
    }
    
    public int getFlags() {
      return this.mFlags & 0xFFFFFFFB;
    }
    
    public String getText(int param1Int) {
      if ((this.mFlags & 0x2) == 0)
        return null; 
      if (this.mData != null) {
        byte[] arrayOfByte = this.mData;
        return new String(arrayOfByte, 0, Math.min(param1Int, arrayOfByte.length));
      } 
      IOException iOException1 = null, iOException2 = null;
      try {
        int k;
        InputStream inputStream = getInputStream();
        if (inputStream == null)
          return null; 
        iOException2 = iOException;
        iOException1 = iOException;
        byte[] arrayOfByte = new byte[param1Int];
        int i = 0;
        int j = 0;
        while (true) {
          k = i;
          if (j) {
            k = i = j = i + j;
            if (j < param1Int) {
              iOException2 = iOException;
              iOException1 = iOException;
              j = iOException.read(arrayOfByte, i, param1Int - i);
              continue;
            } 
          } 
          break;
        } 
        iOException2 = iOException;
        iOException1 = iOException;
        return new String(arrayOfByte, 0, k);
      } catch (IOException iOException) {
        return null;
      } finally {
        if (iOException2 != null)
          try {
            iOException2.close();
          } catch (IOException iOException) {} 
      } 
    }
    
    public InputStream getInputStream() throws IOException {
      ParcelFileDescriptor.AutoCloseInputStream autoCloseInputStream;
      GZIPInputStream gZIPInputStream;
      if (this.mData != null) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.mData);
      } else if (this.mFileDescriptor != null) {
        autoCloseInputStream = new ParcelFileDescriptor.AutoCloseInputStream(this.mFileDescriptor);
      } else {
        return null;
      } 
      if ((this.mFlags & 0x4) != 0)
        gZIPInputStream = new GZIPInputStream(autoCloseInputStream); 
      return gZIPInputStream;
    }
    
    public static final Parcelable.Creator<Entry> CREATOR = new Parcelable.Creator<Entry>() {
        public DropBoxManager.Entry[] newArray(int param2Int) {
          return new DropBoxManager.Entry[param2Int];
        }
        
        public DropBoxManager.Entry createFromParcel(Parcel param2Parcel) {
          String str = param2Parcel.readString();
          long l = param2Parcel.readLong();
          int i = param2Parcel.readInt();
          if ((i & 0x8) != 0)
            return new DropBoxManager.Entry(str, l, param2Parcel.createByteArray(), i & 0xFFFFFFF7); 
          ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel(param2Parcel);
          return new DropBoxManager.Entry(str, l, parcelFileDescriptor, i);
        }
      };
    
    private final byte[] mData;
    
    private final ParcelFileDescriptor mFileDescriptor;
    
    private final int mFlags;
    
    private final String mTag;
    
    private final long mTimeMillis;
    
    public int describeContents() {
      boolean bool;
      if (this.mFileDescriptor != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(this.mTag);
      param1Parcel.writeLong(this.mTimeMillis);
      if (this.mFileDescriptor != null) {
        param1Parcel.writeInt(this.mFlags & 0xFFFFFFF7);
        this.mFileDescriptor.writeToParcel(param1Parcel, param1Int);
      } else {
        param1Parcel.writeInt(this.mFlags | 0x8);
        param1Parcel.writeByteArray(this.mData);
      } 
    }
  }
  
  public DropBoxManager(Context paramContext, IDropBoxManagerService paramIDropBoxManagerService) {
    this.mContext = paramContext;
    this.mService = paramIDropBoxManagerService;
  }
  
  protected DropBoxManager() {
    this.mContext = null;
    this.mService = null;
  }
  
  public void addText(String paramString1, String paramString2) {
    try {
      IDropBoxManagerService iDropBoxManagerService = this.mService;
      Entry entry = new Entry();
      this(paramString1, 0L, paramString2);
      iDropBoxManagerService.add(entry);
      return;
    } catch (RemoteException remoteException) {
      if (remoteException instanceof TransactionTooLargeException) {
        Context context = this.mContext;
        if ((context.getApplicationInfo()).targetSdkVersion < 24) {
          Log.e("DropBoxManager", "App sent too much data, so it was ignored", (Throwable)remoteException);
          return;
        } 
      } 
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void addData(String paramString, byte[] paramArrayOfbyte, int paramInt) {
    if (paramArrayOfbyte != null)
      try {
        IDropBoxManagerService iDropBoxManagerService = this.mService;
        Entry entry = new Entry();
        this(paramString, 0L, paramArrayOfbyte, paramInt);
        iDropBoxManagerService.add(entry);
        return;
      } catch (RemoteException remoteException) {
        if (remoteException instanceof TransactionTooLargeException) {
          Context context = this.mContext;
          if ((context.getApplicationInfo()).targetSdkVersion < 24) {
            Log.e("DropBoxManager", "App sent too much data, so it was ignored", (Throwable)remoteException);
            return;
          } 
        } 
        throw remoteException.rethrowFromSystemServer();
      }  
    throw new NullPointerException("data == null");
  }
  
  public void addFile(String paramString, File paramFile, int paramInt) throws IOException {
    if (paramFile != null) {
      Entry entry = new Entry(paramString, 0L, paramFile, paramInt);
      try {
        this.mService.add(entry);
        entry.close();
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } finally {}
      entry.close();
      throw paramFile;
    } 
    throw new NullPointerException("file == null");
  }
  
  public boolean isTagEnabled(String paramString) {
    try {
      return this.mService.isTagEnabled(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Entry getNextEntry(String paramString, long paramLong) {
    try {
      return this.mService.getNextEntry(paramString, paramLong, this.mContext.getOpPackageName());
    } catch (SecurityException securityException) {
      if ((this.mContext.getApplicationInfo()).targetSdkVersion < 28) {
        Log.w("DropBoxManager", securityException.getMessage());
        return null;
      } 
      throw securityException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
