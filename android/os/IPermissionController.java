package android.os;

public interface IPermissionController extends IInterface {
  boolean checkPermission(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  int getPackageUid(String paramString, int paramInt) throws RemoteException;
  
  String[] getPackagesForUid(int paramInt) throws RemoteException;
  
  boolean isRuntimePermission(String paramString) throws RemoteException;
  
  int noteOp(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  class Default implements IPermissionController {
    public boolean checkPermission(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public int noteOp(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return 0;
    }
    
    public String[] getPackagesForUid(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean isRuntimePermission(String param1String) throws RemoteException {
      return false;
    }
    
    public int getPackageUid(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPermissionController {
    private static final String DESCRIPTOR = "android.os.IPermissionController";
    
    static final int TRANSACTION_checkPermission = 1;
    
    static final int TRANSACTION_getPackageUid = 5;
    
    static final int TRANSACTION_getPackagesForUid = 3;
    
    static final int TRANSACTION_isRuntimePermission = 4;
    
    static final int TRANSACTION_noteOp = 2;
    
    public Stub() {
      attachInterface(this, "android.os.IPermissionController");
    }
    
    public static IPermissionController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IPermissionController");
      if (iInterface != null && iInterface instanceof IPermissionController)
        return (IPermissionController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "getPackageUid";
            } 
            return "isRuntimePermission";
          } 
          return "getPackagesForUid";
        } 
        return "noteOp";
      } 
      return "checkPermission";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str1;
      if (param1Int1 != 1) {
        String[] arrayOfString;
        if (param1Int1 != 2) {
          String str3;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.os.IPermissionController");
                return true;
              } 
              param1Parcel1.enforceInterface("android.os.IPermissionController");
              String str4 = param1Parcel1.readString();
              param1Int1 = param1Parcel1.readInt();
              param1Int1 = getPackageUid(str4, param1Int1);
              param1Parcel2.writeNoException();
              param1Parcel2.writeInt(param1Int1);
              return true;
            } 
            param1Parcel1.enforceInterface("android.os.IPermissionController");
            str3 = param1Parcel1.readString();
            boolean bool1 = isRuntimePermission(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          } 
          str3.enforceInterface("android.os.IPermissionController");
          param1Int1 = str3.readInt();
          arrayOfString = getPackagesForUid(param1Int1);
          param1Parcel2.writeNoException();
          param1Parcel2.writeStringArray(arrayOfString);
          return true;
        } 
        arrayOfString.enforceInterface("android.os.IPermissionController");
        String str = arrayOfString.readString();
        param1Int1 = arrayOfString.readInt();
        str1 = arrayOfString.readString();
        param1Int1 = noteOp(str, param1Int1, str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(param1Int1);
        return true;
      } 
      str1.enforceInterface("android.os.IPermissionController");
      String str2 = str1.readString();
      param1Int1 = str1.readInt();
      param1Int2 = str1.readInt();
      boolean bool = checkPermission(str2, param1Int1, param1Int2);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements IPermissionController {
      public static IPermissionController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IPermissionController";
      }
      
      public boolean checkPermission(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPermissionController");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IPermissionController.Stub.getDefaultImpl() != null) {
            bool1 = IPermissionController.Stub.getDefaultImpl().checkPermission(param2String, param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int noteOp(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPermissionController");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IPermissionController.Stub.getDefaultImpl() != null) {
            param2Int = IPermissionController.Stub.getDefaultImpl().noteOp(param2String1, param2Int, param2String2);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getPackagesForUid(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPermissionController");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IPermissionController.Stub.getDefaultImpl() != null)
            return IPermissionController.Stub.getDefaultImpl().getPackagesForUid(param2Int); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRuntimePermission(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPermissionController");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IPermissionController.Stub.getDefaultImpl() != null) {
            bool1 = IPermissionController.Stub.getDefaultImpl().isRuntimePermission(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPackageUid(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPermissionController");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IPermissionController.Stub.getDefaultImpl() != null) {
            param2Int = IPermissionController.Stub.getDefaultImpl().getPackageUid(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPermissionController param1IPermissionController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPermissionController != null) {
          Proxy.sDefaultImpl = param1IPermissionController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPermissionController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
