package android.os;

import android.util.ArrayMap;
import android.util.proto.ProtoOutputStream;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

public final class PersistableBundle extends BaseBundle implements Cloneable, Parcelable, XmlUtils.WriteMapCallback {
  public static final Parcelable.Creator<PersistableBundle> CREATOR;
  
  public static final PersistableBundle EMPTY;
  
  private static final String TAG_PERSISTABLEMAP = "pbundle_as_map";
  
  static {
    PersistableBundle persistableBundle = new PersistableBundle();
    persistableBundle.mMap = ArrayMap.EMPTY;
    CREATOR = new Parcelable.Creator<PersistableBundle>() {
        public PersistableBundle createFromParcel(Parcel param1Parcel) {
          return param1Parcel.readPersistableBundle();
        }
        
        public PersistableBundle[] newArray(int param1Int) {
          return new PersistableBundle[param1Int];
        }
      };
  }
  
  public static boolean isValidType(Object paramObject) {
    return (paramObject instanceof Integer || paramObject instanceof Long || paramObject instanceof Double || paramObject instanceof String || paramObject instanceof int[] || paramObject instanceof long[] || paramObject instanceof double[] || paramObject instanceof String[] || paramObject instanceof PersistableBundle || paramObject == null || paramObject instanceof Boolean || paramObject instanceof boolean[]);
  }
  
  public PersistableBundle() {
    this.mFlags = 1;
  }
  
  public PersistableBundle(int paramInt) {
    super(paramInt);
    this.mFlags = 1;
  }
  
  public PersistableBundle(PersistableBundle paramPersistableBundle) {
    super(paramPersistableBundle);
    this.mFlags = paramPersistableBundle.mFlags;
  }
  
  public PersistableBundle(Bundle paramBundle) {
    this(paramBundle.getMap());
  }
  
  private PersistableBundle(ArrayMap<String, Object> paramArrayMap) {
    this.mFlags = 1;
    putAll(paramArrayMap);
    int i = this.mMap.size();
    for (byte b = 0; b < i; b++) {
      Object object = this.mMap.valueAt(b);
      if (object instanceof ArrayMap) {
        this.mMap.setValueAt(b, new PersistableBundle((ArrayMap<String, Object>)object));
      } else if (object instanceof Bundle) {
        this.mMap.setValueAt(b, new PersistableBundle((Bundle)object));
      } else if (!isValidType(object)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Bad value in PersistableBundle key=");
        ArrayMap<String, Object> arrayMap = this.mMap;
        stringBuilder.append((String)arrayMap.keyAt(b));
        stringBuilder.append(" value=");
        stringBuilder.append(object);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    } 
  }
  
  PersistableBundle(Parcel paramParcel, int paramInt) {
    super(paramParcel, paramInt);
    this.mFlags = 1;
  }
  
  PersistableBundle(boolean paramBoolean) {
    super(paramBoolean);
  }
  
  public static PersistableBundle forPair(String paramString1, String paramString2) {
    PersistableBundle persistableBundle = new PersistableBundle(1);
    persistableBundle.putString(paramString1, paramString2);
    return persistableBundle;
  }
  
  public Object clone() {
    return new PersistableBundle(this);
  }
  
  public PersistableBundle deepCopy() {
    PersistableBundle persistableBundle = new PersistableBundle(false);
    persistableBundle.copyInternal(this, true);
    return persistableBundle;
  }
  
  public void putPersistableBundle(String paramString, PersistableBundle paramPersistableBundle) {
    unparcel();
    this.mMap.put(paramString, paramPersistableBundle);
  }
  
  public PersistableBundle getPersistableBundle(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (PersistableBundle)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Bundle", classCastException);
      return null;
    } 
  }
  
  public void writeUnknownObject(Object paramObject, String paramString, XmlSerializer paramXmlSerializer) throws XmlPullParserException, IOException {
    if (paramObject instanceof PersistableBundle) {
      paramXmlSerializer.startTag(null, "pbundle_as_map");
      paramXmlSerializer.attribute(null, "name", paramString);
      ((PersistableBundle)paramObject).saveToXml(paramXmlSerializer);
      paramXmlSerializer.endTag(null, "pbundle_as_map");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown Object o=");
    stringBuilder.append(paramObject);
    throw new XmlPullParserException(stringBuilder.toString());
  }
  
  public void saveToXml(XmlSerializer paramXmlSerializer) throws IOException, XmlPullParserException {
    unparcel();
    XmlUtils.writeMapXml((Map)this.mMap, paramXmlSerializer, this);
  }
  
  static class MyReadMapCallback implements XmlUtils.ReadMapCallback {
    public Object readThisUnknownObjectXml(XmlPullParser param1XmlPullParser, String param1String) throws XmlPullParserException, IOException {
      if ("pbundle_as_map".equals(param1String))
        return PersistableBundle.restoreFromXml(param1XmlPullParser); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown tag=");
      stringBuilder.append(param1String);
      throw new XmlPullParserException(stringBuilder.toString());
    }
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    boolean bool = paramParcel.pushAllowFds(false);
    try {
      writeToParcelInner(paramParcel, paramInt);
      return;
    } finally {
      paramParcel.restoreAllowFds(bool);
    } 
  }
  
  public static PersistableBundle restoreFromXml(XmlPullParser paramXmlPullParser) throws IOException, XmlPullParserException {
    int i = paramXmlPullParser.getDepth();
    String str = paramXmlPullParser.getName();
    String[] arrayOfString = new String[1];
    while (true) {
      int j = paramXmlPullParser.next();
      if (j != 1 && (j != 3 || 
        paramXmlPullParser.getDepth() < i)) {
        if (j == 2) {
          MyReadMapCallback myReadMapCallback = new MyReadMapCallback();
          return 
            new PersistableBundle(XmlUtils.readThisArrayMapXml(paramXmlPullParser, str, arrayOfString, myReadMapCallback));
        } 
        continue;
      } 
      break;
    } 
    return EMPTY;
  }
  
  public String toString() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mParcelledData : Landroid/os/Parcel;
    //   6: ifnull -> 69
    //   9: aload_0
    //   10: invokevirtual isEmptyParcel : ()Z
    //   13: ifeq -> 22
    //   16: aload_0
    //   17: monitorexit
    //   18: ldc_w 'PersistableBundle[EMPTY_PARCEL]'
    //   21: areturn
    //   22: new java/lang/StringBuilder
    //   25: astore_1
    //   26: aload_1
    //   27: invokespecial <init> : ()V
    //   30: aload_1
    //   31: ldc_w 'PersistableBundle[mParcelledData.dataSize='
    //   34: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   37: pop
    //   38: aload_0
    //   39: getfield mParcelledData : Landroid/os/Parcel;
    //   42: astore_2
    //   43: aload_1
    //   44: aload_2
    //   45: invokevirtual dataSize : ()I
    //   48: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   51: pop
    //   52: aload_1
    //   53: ldc_w ']'
    //   56: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   59: pop
    //   60: aload_1
    //   61: invokevirtual toString : ()Ljava/lang/String;
    //   64: astore_2
    //   65: aload_0
    //   66: monitorexit
    //   67: aload_2
    //   68: areturn
    //   69: new java/lang/StringBuilder
    //   72: astore_2
    //   73: aload_2
    //   74: invokespecial <init> : ()V
    //   77: aload_2
    //   78: ldc_w 'PersistableBundle['
    //   81: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   84: pop
    //   85: aload_2
    //   86: aload_0
    //   87: getfield mMap : Landroid/util/ArrayMap;
    //   90: invokevirtual toString : ()Ljava/lang/String;
    //   93: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   96: pop
    //   97: aload_2
    //   98: ldc_w ']'
    //   101: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   104: pop
    //   105: aload_2
    //   106: invokevirtual toString : ()Ljava/lang/String;
    //   109: astore_2
    //   110: aload_0
    //   111: monitorexit
    //   112: aload_2
    //   113: areturn
    //   114: astore_2
    //   115: aload_0
    //   116: monitorexit
    //   117: aload_2
    //   118: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #310	-> 2
    //   #311	-> 9
    //   #312	-> 16
    //   #314	-> 22
    //   #315	-> 43
    //   #314	-> 65
    //   #318	-> 69
    //   #309	-> 114
    // Exception table:
    //   from	to	target	type
    //   2	9	114	finally
    //   9	16	114	finally
    //   22	43	114	finally
    //   43	65	114	finally
    //   69	110	114	finally
  }
  
  public String toShortString() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mParcelledData : Landroid/os/Parcel;
    //   6: ifnull -> 59
    //   9: aload_0
    //   10: invokevirtual isEmptyParcel : ()Z
    //   13: ifeq -> 22
    //   16: aload_0
    //   17: monitorexit
    //   18: ldc_w 'EMPTY_PARCEL'
    //   21: areturn
    //   22: new java/lang/StringBuilder
    //   25: astore_1
    //   26: aload_1
    //   27: invokespecial <init> : ()V
    //   30: aload_1
    //   31: ldc_w 'mParcelledData.dataSize='
    //   34: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   37: pop
    //   38: aload_1
    //   39: aload_0
    //   40: getfield mParcelledData : Landroid/os/Parcel;
    //   43: invokevirtual dataSize : ()I
    //   46: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: aload_1
    //   51: invokevirtual toString : ()Ljava/lang/String;
    //   54: astore_1
    //   55: aload_0
    //   56: monitorexit
    //   57: aload_1
    //   58: areturn
    //   59: aload_0
    //   60: getfield mMap : Landroid/util/ArrayMap;
    //   63: invokevirtual toString : ()Ljava/lang/String;
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: areturn
    //   71: astore_1
    //   72: aload_0
    //   73: monitorexit
    //   74: aload_1
    //   75: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #323	-> 2
    //   #324	-> 9
    //   #325	-> 16
    //   #327	-> 22
    //   #330	-> 59
    //   #322	-> 71
    // Exception table:
    //   from	to	target	type
    //   2	9	71	finally
    //   9	16	71	finally
    //   22	55	71	finally
    //   59	67	71	finally
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    if (this.mParcelledData != null) {
      if (isEmptyParcel()) {
        paramProtoOutputStream.write(1120986464257L, 0);
      } else {
        paramProtoOutputStream.write(1120986464257L, this.mParcelledData.dataSize());
      } 
    } else {
      paramProtoOutputStream.write(1138166333442L, this.mMap.toString());
    } 
    paramProtoOutputStream.end(paramLong);
  }
  
  public void writeToStream(OutputStream paramOutputStream) throws IOException {
    FastXmlSerializer fastXmlSerializer = new FastXmlSerializer();
    fastXmlSerializer.setOutput(paramOutputStream, StandardCharsets.UTF_8.name());
    fastXmlSerializer.startTag(null, "bundle");
    try {
      saveToXml((XmlSerializer)fastXmlSerializer);
      fastXmlSerializer.endTag(null, "bundle");
      fastXmlSerializer.flush();
      return;
    } catch (XmlPullParserException xmlPullParserException) {
      throw new IOException(xmlPullParserException);
    } 
  }
  
  public static PersistableBundle readFromStream(InputStream paramInputStream) throws IOException {
    try {
      XmlPullParser xmlPullParser = XmlPullParserFactory.newInstance().newPullParser();
      xmlPullParser.setInput(paramInputStream, StandardCharsets.UTF_8.name());
      xmlPullParser.next();
      return restoreFromXml(xmlPullParser);
    } catch (XmlPullParserException xmlPullParserException) {
      throw new IOException(xmlPullParserException);
    } 
  }
}
