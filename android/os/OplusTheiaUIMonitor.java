package android.os;

import android.app.ActivityManager;
import android.app.ActivityThread;
import android.app.IActivityManager;
import android.app.servertransaction.ActivityLifecycleItem;
import android.app.servertransaction.ClientTransaction;
import android.content.Intent;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OplusTheiaUIMonitor {
  private static final boolean PRINT_DETAIL_LOG = SystemProperties.getBoolean("persist.sys.oppo.uito.detaillog", false);
  
  static {
    LOG_SWITCH_ON = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    mLock = new Object();
    sInstance = null;
    sPid = Process.myPid();
    sMainLooper = null;
  }
  
  private boolean mUITimeoutEnable = false;
  
  private long mMessageProcessStartTime = Long.MAX_VALUE;
  
  private MonitorThread mMonitorThread = null;
  
  private int mMessageGetStackCount = 0;
  
  private String mFirstGetStackCountStat = "";
  
  private String mPackageName = null;
  
  private SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
  
  private static final int BIND_APPLICATION_MSG = 110;
  
  private static final int EXECUTE_TRANSACTION_MSG = 159;
  
  private static final int GOT_TRACE_FREQUENTLY_THRESHOLD = 60000;
  
  private static final int JIFFIES_MS = 10;
  
  private static final boolean LOG_SWITCH_ON;
  
  private static final int MAX_TRACE_LENGTH = 30;
  
  private static final int MESSAGE_GET_STACK_COUNT_MAX_DEFAULT = 2;
  
  private static final int RELAUNCH_ACTIVITY_MSG = 160;
  
  private static final String TAG = "OplusTheiaUIMonitorTag";
  
  private static final int UI_LOOPER_MESSAGE_TIMEOUT_DEFAULT = 3000;
  
  private static final Object mLock;
  
  private static OplusTheiaUIMonitor sInstance;
  
  private static Looper sMainLooper;
  
  private static int sPid;
  
  private String mBlockTrace;
  
  private int mMessageGetStackCountMax;
  
  private int mMessageGetStackInterval;
  
  private int mUILooperMessageTimeout;
  
  private class TheiaConst {
    public static final long THEIA_ST_BASE = 4298113024L;
    
    public static final long THEIA_ST_UTO = 4298113029L;
    
    final OplusTheiaUIMonitor this$0;
  }
  
  protected OplusTheiaUIMonitor() {
    this.mUILooperMessageTimeout = 3000;
    this.mMessageGetStackCountMax = 2;
    this.mMessageGetStackInterval = 3000 / 2;
    this.mUITimeoutEnable = getUITimeoutEnableFromProp();
  }
  
  public static OplusTheiaUIMonitor getInstance() {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusTheiaUIMonitor oplusTheiaUIMonitor = new OplusTheiaUIMonitor();
          this();
          sInstance = oplusTheiaUIMonitor;
        } 
      }  
    return sInstance;
  }
  
  private class MonitorThread extends Thread {
    final OplusTheiaUIMonitor this$0;
    
    public boolean mStarted = false;
    
    private long mLastGotTracedMessageStartTime = -1L;
    
    public MonitorThread() {
      super("UIMonitorThread");
    }
    
    private boolean isUILooperGetStackTimeout(int param1Int) {
      boolean bool = OplusTheiaUIMonitor.this.isMessageProcessing();
      boolean bool1 = false;
      if (!bool)
        return false; 
      if (SystemClock.uptimeMillis() - OplusTheiaUIMonitor.this.mMessageProcessStartTime >= (OplusTheiaUIMonitor.this.mMessageGetStackInterval * param1Int))
        bool1 = true; 
      return bool1;
    }
    
    private void handleOnce() throws InterruptedException {
      // Byte code:
      //   0: iconst_1
      //   1: istore_1
      //   2: aload_0
      //   3: getfield this$0 : Landroid/os/OplusTheiaUIMonitor;
      //   6: invokestatic access$000 : (Landroid/os/OplusTheiaUIMonitor;)Z
      //   9: ifeq -> 148
      //   12: iload_1
      //   13: aload_0
      //   14: getfield this$0 : Landroid/os/OplusTheiaUIMonitor;
      //   17: invokestatic access$300 : (Landroid/os/OplusTheiaUIMonitor;)I
      //   20: if_icmpgt -> 148
      //   23: aload_0
      //   24: iload_1
      //   25: invokespecial isUILooperGetStackTimeout : (I)Z
      //   28: ifeq -> 148
      //   31: aload_0
      //   32: getfield this$0 : Landroid/os/OplusTheiaUIMonitor;
      //   35: astore_2
      //   36: aload_2
      //   37: invokestatic access$400 : (Landroid/os/OplusTheiaUIMonitor;)Ljava/lang/String;
      //   40: astore_3
      //   41: iconst_1
      //   42: istore #4
      //   44: iload_1
      //   45: iconst_1
      //   46: if_icmpne -> 52
      //   49: goto -> 55
      //   52: iconst_0
      //   53: istore #4
      //   55: aload_2
      //   56: aload_3
      //   57: iload #4
      //   59: invokestatic access$500 : (Landroid/os/OplusTheiaUIMonitor;Ljava/lang/String;Z)V
      //   62: iload_1
      //   63: aload_0
      //   64: getfield this$0 : Landroid/os/OplusTheiaUIMonitor;
      //   67: invokestatic access$300 : (Landroid/os/OplusTheiaUIMonitor;)I
      //   70: if_icmpne -> 121
      //   73: aload_0
      //   74: aload_0
      //   75: getfield this$0 : Landroid/os/OplusTheiaUIMonitor;
      //   78: invokestatic access$100 : (Landroid/os/OplusTheiaUIMonitor;)J
      //   81: putfield mLastGotTracedMessageStartTime : J
      //   84: aload_0
      //   85: getfield this$0 : Landroid/os/OplusTheiaUIMonitor;
      //   88: astore_2
      //   89: new java/lang/StringBuilder
      //   92: dup
      //   93: invokespecial <init> : ()V
      //   96: astore_3
      //   97: aload_3
      //   98: ldc 'handleOnce set mLastGotTracedMessageStartTime:'
      //   100: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   103: pop
      //   104: aload_3
      //   105: aload_0
      //   106: getfield mLastGotTracedMessageStartTime : J
      //   109: invokevirtual append : (J)Ljava/lang/StringBuilder;
      //   112: pop
      //   113: aload_2
      //   114: aload_3
      //   115: invokevirtual toString : ()Ljava/lang/String;
      //   118: invokestatic access$600 : (Landroid/os/OplusTheiaUIMonitor;Ljava/lang/String;)V
      //   121: aload_0
      //   122: monitorenter
      //   123: aload_0
      //   124: aload_0
      //   125: getfield this$0 : Landroid/os/OplusTheiaUIMonitor;
      //   128: invokestatic access$200 : (Landroid/os/OplusTheiaUIMonitor;)I
      //   131: i2l
      //   132: invokevirtual wait : (J)V
      //   135: aload_0
      //   136: monitorexit
      //   137: iinc #1, 1
      //   140: goto -> 2
      //   143: astore_3
      //   144: aload_0
      //   145: monitorexit
      //   146: aload_3
      //   147: athrow
      //   148: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #109	-> 0
      //   #110	-> 2
      //   #111	-> 23
      //   #113	-> 31
      //   #114	-> 62
      //   #115	-> 73
      //   #116	-> 84
      //   #119	-> 121
      //   #121	-> 123
      //   #122	-> 135
      //   #123	-> 137
      //   #122	-> 143
      //   #129	-> 148
      // Exception table:
      //   from	to	target	type
      //   123	135	143	finally
      //   135	137	143	finally
      //   144	146	143	finally
    }
    
    private boolean isMessageHaveGotTrace() {
      boolean bool;
      if (this.mLastGotTracedMessageStartTime == OplusTheiaUIMonitor.this.mMessageProcessStartTime) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private boolean isGotTraceTooFrequently() {
      boolean bool;
      if (this.mLastGotTracedMessageStartTime != -1L && SystemClock.uptimeMillis() - this.mLastGotTracedMessageStartTime < (OplusTheiaUIMonitor.this.mUILooperMessageTimeout + 60000)) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private boolean needHandle() {
      boolean bool;
      if (!isMessageHaveGotTrace() && !isGotTraceTooFrequently()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void run() {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial run : ()V
      //   4: aload_0
      //   5: invokevirtual isInterrupted : ()Z
      //   8: ifne -> 165
      //   11: aload_0
      //   12: getfield mStarted : Z
      //   15: ifeq -> 165
      //   18: aload_0
      //   19: invokespecial isGotTraceTooFrequently : ()Z
      //   22: ifeq -> 72
      //   25: aload_0
      //   26: getfield mLastGotTracedMessageStartTime : J
      //   29: aload_0
      //   30: getfield this$0 : Landroid/os/OplusTheiaUIMonitor;
      //   33: invokestatic access$700 : (Landroid/os/OplusTheiaUIMonitor;)I
      //   36: i2l
      //   37: ladd
      //   38: lstore_1
      //   39: lload_1
      //   40: ldc2_w -1
      //   43: lcmp
      //   44: ifne -> 59
      //   47: aload_0
      //   48: getfield this$0 : Landroid/os/OplusTheiaUIMonitor;
      //   51: invokestatic access$200 : (Landroid/os/OplusTheiaUIMonitor;)I
      //   54: i2l
      //   55: lstore_1
      //   56: goto -> 69
      //   59: ldc2_w 60000
      //   62: lload_1
      //   63: ladd
      //   64: invokestatic uptimeMillis : ()J
      //   67: lsub
      //   68: lstore_1
      //   69: goto -> 138
      //   72: aload_0
      //   73: invokespecial isMessageHaveGotTrace : ()Z
      //   76: ifeq -> 91
      //   79: aload_0
      //   80: getfield this$0 : Landroid/os/OplusTheiaUIMonitor;
      //   83: invokestatic access$200 : (Landroid/os/OplusTheiaUIMonitor;)I
      //   86: i2l
      //   87: lstore_1
      //   88: goto -> 138
      //   91: aload_0
      //   92: invokespecial handleOnce : ()V
      //   95: aload_0
      //   96: getfield this$0 : Landroid/os/OplusTheiaUIMonitor;
      //   99: invokestatic access$100 : (Landroid/os/OplusTheiaUIMonitor;)J
      //   102: lstore_1
      //   103: ldc2_w 9223372036854775807
      //   106: lload_1
      //   107: lcmp
      //   108: ifne -> 123
      //   111: aload_0
      //   112: getfield this$0 : Landroid/os/OplusTheiaUIMonitor;
      //   115: invokestatic access$200 : (Landroid/os/OplusTheiaUIMonitor;)I
      //   118: i2l
      //   119: lstore_1
      //   120: goto -> 138
      //   123: aload_0
      //   124: getfield this$0 : Landroid/os/OplusTheiaUIMonitor;
      //   127: invokestatic access$200 : (Landroid/os/OplusTheiaUIMonitor;)I
      //   130: i2l
      //   131: lload_1
      //   132: ladd
      //   133: invokestatic uptimeMillis : ()J
      //   136: lsub
      //   137: lstore_1
      //   138: lload_1
      //   139: lconst_0
      //   140: lcmp
      //   141: ifle -> 162
      //   144: aload_0
      //   145: monitorenter
      //   146: aload_0
      //   147: lload_1
      //   148: invokevirtual wait : (J)V
      //   151: aload_0
      //   152: monitorexit
      //   153: goto -> 162
      //   156: astore_3
      //   157: aload_0
      //   158: monitorexit
      //   159: aload_3
      //   160: athrow
      //   161: astore_3
      //   162: goto -> 4
      //   165: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #148	-> 0
      //   #150	-> 4
      //   #152	-> 18
      //   #153	-> 18
      //   #154	-> 25
      //   #155	-> 39
      //   #157	-> 69
      //   #158	-> 79
      //   #161	-> 91
      //   #163	-> 95
      //   #164	-> 103
      //   #168	-> 138
      //   #170	-> 144
      //   #171	-> 146
      //   #172	-> 151
      //   #174	-> 161
      //   #176	-> 162
      //   #178	-> 165
      // Exception table:
      //   from	to	target	type
      //   18	25	161	java/lang/InterruptedException
      //   25	39	161	java/lang/InterruptedException
      //   47	56	161	java/lang/InterruptedException
      //   59	69	161	java/lang/InterruptedException
      //   72	79	161	java/lang/InterruptedException
      //   79	88	161	java/lang/InterruptedException
      //   91	95	161	java/lang/InterruptedException
      //   95	103	161	java/lang/InterruptedException
      //   111	120	161	java/lang/InterruptedException
      //   123	138	161	java/lang/InterruptedException
      //   144	146	161	java/lang/InterruptedException
      //   146	151	156	finally
      //   151	153	156	finally
      //   157	159	156	finally
      //   159	161	161	java/lang/InterruptedException
    }
  }
  
  private boolean isClickMessage(Message paramMessage) {
    Runnable runnable = paramMessage.getCallback();
    if (runnable != null && "PerformClick".equals(runnable.getClass().getSimpleName()))
      return true; 
    return false;
  }
  
  private void checkStartMonitorThreadIfNeeded(boolean paramBoolean) {
    if (paramBoolean && this.mMonitorThread == null) {
      MonitorThread monitorThread = new MonitorThread();
      monitorThread.mStarted = true;
      this.mMonitorThread.start();
    } 
  }
  
  private void checkStopMonitorThreadIfNeeded(boolean paramBoolean) {
    if (!paramBoolean) {
      MonitorThread monitorThread = this.mMonitorThread;
      if (monitorThread != null) {
        monitorThread.mStarted = false;
        this.mMonitorThread.interrupt();
        this.mMonitorThread = null;
      } 
    } 
  }
  
  private void checkStartOrStopMonitorThreadIfNeeded(boolean paramBoolean) {
    checkStartMonitorThreadIfNeeded(paramBoolean);
    checkStopMonitorThreadIfNeeded(paramBoolean);
  }
  
  private boolean isMessageProcessing() {
    boolean bool;
    if (this.mMessageProcessStartTime != Long.MAX_VALUE) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void messageBegin(Message paramMessage, boolean paramBoolean) {
    if (!this.mUITimeoutEnable || !isMainLooper())
      return; 
    checkStartOrStopMonitorThreadIfNeeded(paramBoolean);
    if (!paramBoolean || isStartupMessage(paramMessage))
      return; 
    this.mMessageProcessStartTime = SystemClock.uptimeMillis();
  }
  
  public void messageEnd(Message paramMessage, boolean paramBoolean) {
    if (!this.mUITimeoutEnable || !isMainLooper() || !isMessageProcessing())
      return; 
    if (isMessageTimeoutBlock())
      sendUITimoutEvent(paramMessage); 
    this.mMessageProcessStartTime = Long.MAX_VALUE;
    checkStopMonitorThreadIfNeeded(paramBoolean);
  }
  
  private String getCurrentTimeString() {
    Date date = new Date(System.currentTimeMillis());
    return this.mDateFormat.format(date);
  }
  
  private String getTimeString(long paramLong) {
    Date date = new Date(paramLong);
    return this.mDateFormat.format(date);
  }
  
  private String getUIThreadMiniStackInfo() {
    Thread thread = getMainThreadLooper().getThread();
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("");
    stringBuilder2.append("----- pid ");
    stringBuilder2.append(sPid);
    stringBuilder2.append(" at ");
    stringBuilder2.append(getCurrentTimeString());
    stringBuilder2.append(" -----\n");
    String str = stringBuilder2.toString();
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(str);
    stringBuilder3.append("Cmd line: ");
    stringBuilder3.append(getPackageName());
    stringBuilder3.append("\n");
    str = stringBuilder3.toString();
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append(str);
    stringBuilder3.append("\"");
    stringBuilder3.append(thread.getName());
    stringBuilder3.append("\" prio=");
    stringBuilder3.append(thread.getPriority());
    stringBuilder3.append(" tid=");
    stringBuilder3.append(thread.getId());
    stringBuilder3.append(" ");
    stringBuilder3.append(thread.getState());
    stringBuilder3.append(" sysTid=");
    stringBuilder3.append(sPid);
    stringBuilder3.append("\n");
    str = stringBuilder3.toString();
    StackTraceElement[] arrayOfStackTraceElement = thread.getStackTrace();
    int i = arrayOfStackTraceElement.length, j = 30;
    if (i < 30)
      j = arrayOfStackTraceElement.length; 
    for (i = 0; i < j; i++) {
      StackTraceElement stackTraceElement = arrayOfStackTraceElement[i];
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append(str);
      stringBuilder3.append("  at ");
      stringBuilder3.append(stackTraceElement.toString());
      stringBuilder3.append("\n");
      str = stringBuilder3.toString();
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("getUIThreadMiniStackInfo info:");
    stringBuilder1.append(str);
    detailLog(stringBuilder1.toString());
    return str;
  }
  
  private Looper getMainThreadLooper() {
    if (sMainLooper == null)
      sMainLooper = Looper.getMainLooper(); 
    return sMainLooper;
  }
  
  private boolean isMainLooper() {
    boolean bool;
    if (getMainThreadLooper() == Looper.myLooper()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private String getMessageInfo(Message paramMessage) {
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append("Message:");
    stringBuilder3.append(paramMessage);
    stringBuilder3.append(" \n");
    String str2 = stringBuilder3.toString();
    long l = SystemClock.uptimeMillis();
    l -= this.mMessageProcessStartTime;
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(str2);
    stringBuilder1.append("Message start time:");
    stringBuilder1.append(getTimeString(System.currentTimeMillis() - l));
    stringBuilder1.append("\n");
    str2 = stringBuilder1.toString();
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(str2);
    stringBuilder1.append("Message end time:");
    stringBuilder1.append(getCurrentTimeString());
    stringBuilder1.append("\n");
    String str1 = stringBuilder1.toString();
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(str1);
    stringBuilder2.append("Message cost time:");
    stringBuilder2.append(l);
    stringBuilder2.append(" ms\n");
    str1 = stringBuilder2.toString();
    return str1;
  }
  
  private void sendUITimoutEvent(Message paramMessage) {
    this.mMessageGetStackCount = 0;
    StringBuilder stringBuilder5 = new StringBuilder();
    stringBuilder5.append("");
    stringBuilder5.append("[UITimeout MainThread Info]\n");
    String str4 = stringBuilder5.toString();
    stringBuilder5 = new StringBuilder();
    stringBuilder5.append(str4);
    stringBuilder5.append(getMessageInfo(paramMessage));
    String str3 = stringBuilder5.toString();
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(str3);
    stringBuilder2.append("\n");
    String str1 = stringBuilder2.toString();
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.append(str1);
    stringBuilder4.append("Cpu cost time:");
    stringBuilder4.append(getMessageCpuCostTime());
    stringBuilder4.append(" ms(between first got stack to message end)\n");
    String str2 = stringBuilder4.toString();
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(str2);
    stringBuilder1.append(this.mBlockTrace);
    str2 = stringBuilder1.toString();
    Intent intent = new Intent();
    intent.putExtra("packageName", getPackageName());
    intent.putExtra("mainThreadInfo", str2);
    intent.putExtra("pid", sPid);
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append("sendUITimoutEvent args:");
    stringBuilder3.append(intent);
    detailLog(stringBuilder3.toString());
    if (LOG_SWITCH_ON) {
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append("sendUITimoutEvent args:");
      stringBuilder3.append(intent);
      Log.i("OplusTheiaUIMonitorTag", stringBuilder3.toString());
    } 
    sendTheiaEvent(4298113029L, intent);
  }
  
  private String getPackageName() {
    String str = this.mPackageName;
    if (str != null)
      return str; 
    this.mPackageName = str = ActivityThread.currentPackageName();
    if (str == null)
      this.mPackageName = "system"; 
    return this.mPackageName;
  }
  
  private boolean isMessageTimeoutBlock() {
    boolean bool;
    if (this.mMessageGetStackCount >= this.mMessageGetStackCountMax && SystemClock.uptimeMillis() - this.mMessageProcessStartTime >= this.mUILooperMessageTimeout) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void appendBlockTrace(String paramString, boolean paramBoolean) {
    if (paramBoolean) {
      this.mBlockTrace = "";
      this.mMessageGetStackCount = 0;
      this.mFirstGetStackCountStat = getMainThreadStatString();
    } 
    this.mMessageGetStackCount++;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mBlockTrace);
    stringBuilder.append("Stack ");
    stringBuilder.append(this.mMessageGetStackCount);
    stringBuilder.append(":\n");
    this.mBlockTrace = stringBuilder.toString();
    stringBuilder = new StringBuilder();
    stringBuilder.append(this.mBlockTrace);
    stringBuilder.append(paramString);
    stringBuilder.append("\n");
    this.mBlockTrace = stringBuilder.toString();
  }
  
  private boolean isStartupMessage(Message paramMessage) {
    boolean bool1 = false;
    if (paramMessage.what == 110 || paramMessage.what == 160)
      return true; 
    boolean bool2 = bool1;
    if (paramMessage.what == 159) {
      bool2 = bool1;
      if (paramMessage.obj != null) {
        bool2 = bool1;
        if (paramMessage.obj instanceof ClientTransaction) {
          ClientTransaction clientTransaction = (ClientTransaction)paramMessage.obj;
          ActivityLifecycleItem activityLifecycleItem = clientTransaction.getLifecycleStateRequest();
          bool2 = bool1;
          if (activityLifecycleItem != null) {
            int i = activityLifecycleItem.getTargetState();
            if (i != 1 && i != 2 && i != 3) {
              bool2 = bool1;
              if (i == 7) {
                bool2 = true;
                return bool2;
              } 
              return bool2;
            } 
          } else {
            return bool2;
          } 
        } else {
          return bool2;
        } 
      } else {
        return bool2;
      } 
    } else {
      return bool2;
    } 
    bool2 = true;
    return bool2;
  }
  
  private Object callDeclaredMethod(Object paramObject, String paramString1, String paramString2, Class[] paramArrayOfClass, Object[] paramArrayOfObject) {
    SecurityException securityException2 = null;
    ClassNotFoundException classNotFoundException = null;
    try {
      Class<?> clazz = Class.forName(paramString1);
      Method method = clazz.getDeclaredMethod(paramString2, paramArrayOfClass);
      method.setAccessible(true);
      paramObject = method.invoke(paramObject, paramArrayOfObject);
    } catch (ClassNotFoundException classNotFoundException1) {
      classNotFoundException1.printStackTrace();
      classNotFoundException1 = classNotFoundException;
    } catch (NoSuchMethodException noSuchMethodException) {
      noSuchMethodException.printStackTrace();
      ClassNotFoundException classNotFoundException1 = classNotFoundException;
    } catch (IllegalAccessException illegalAccessException) {
      illegalAccessException.printStackTrace();
      ClassNotFoundException classNotFoundException1 = classNotFoundException;
    } catch (InvocationTargetException invocationTargetException) {
      invocationTargetException.printStackTrace();
      ClassNotFoundException classNotFoundException1 = classNotFoundException;
    } catch (SecurityException securityException1) {
      securityException1.printStackTrace();
      securityException1 = securityException2;
    } 
    return securityException1;
  }
  
  private void sendTheiaEvent(long paramLong, Intent paramIntent) {
    IActivityManager iActivityManager = ActivityManager.getService();
    if (iActivityManager != null) {
      Class<long> clazz = long.class;
      callDeclaredMethod(iActivityManager, "android.app.IActivityManager", "sendTheiaEvent", new Class[] { clazz, Intent.class }, new Object[] { Long.valueOf(paramLong), paramIntent });
    } else {
      detailLog("sendTheiaEvent, am is null");
    } 
  }
  
  private long getMainThreadUtmStmCount() {
    long l1 = -1L;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("/proc/");
    stringBuilder.append(sPid);
    stringBuilder.append("/task/");
    stringBuilder.append(sPid);
    stringBuilder.append("/stat");
    File file = new File(stringBuilder.toString());
    long l2 = l1;
    try {
      BufferedReader bufferedReader = new BufferedReader();
      l2 = l1;
      FileReader fileReader = new FileReader();
      l2 = l1;
      this(file);
      l2 = l1;
      this(fileReader);
      try {
        String str = bufferedReader.readLine();
        int i = str.indexOf(")");
        String[] arrayOfString = str.substring(i + 2).split(" ");
        int j = Integer.valueOf(arrayOfString[11]).intValue();
        i = Integer.valueOf(arrayOfString[12]).intValue();
        l1 = (j + i);
        l2 = l1;
      } finally {
        try {
          bufferedReader.close();
        } finally {
          bufferedReader = null;
          l2 = l1;
        } 
        l2 = l1;
      } 
    } catch (Exception exception) {}
    return l2;
  }
  
  private String getMainThreadStatString() {
    String str;
    StringBuilder stringBuilder1 = null;
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("/proc/");
    stringBuilder2.append(sPid);
    stringBuilder2.append("/task/");
    stringBuilder2.append(sPid);
    stringBuilder2.append("/stat");
    File file = new File(stringBuilder2.toString());
    stringBuilder2 = stringBuilder1;
    try {
      String str1;
      BufferedReader bufferedReader = new BufferedReader();
      stringBuilder2 = stringBuilder1;
      FileReader fileReader = new FileReader();
      stringBuilder2 = stringBuilder1;
      this(file);
      stringBuilder2 = stringBuilder1;
      this(fileReader);
      try {
        str1 = str = bufferedReader.readLine();
        str = str1;
      } finally {
        try {
          bufferedReader.close();
        } finally {
          bufferedReader = null;
          String str2 = str1;
        } 
        str = str1;
      } 
    } catch (Exception exception) {}
    return str;
  }
  
  private long getMainThreadUtmStmCount(String paramString) {
    long l = -1L;
    if (paramString == null)
      return -1L; 
    try {
      int i = paramString.indexOf(")");
      String[] arrayOfString = paramString.substring(i + 2).split(" ");
      int j = Integer.valueOf(arrayOfString[11]).intValue();
      i = Integer.valueOf(arrayOfString[12]).intValue();
      l = (j + i);
    } catch (Exception exception) {}
    return l;
  }
  
  private long getMessageCpuCostTime() {
    long l1 = -1L;
    long l2 = getMainThreadUtmStmCount();
    long l3 = getMainThreadUtmStmCount(this.mFirstGetStackCountStat);
    long l4 = l1;
    if (l2 != -1L) {
      l4 = l1;
      if (l3 != -1L)
        l4 = (l2 - l3) * 10L; 
    } 
    return l4;
  }
  
  private void detailLog(String paramString) {
    if (PRINT_DETAIL_LOG)
      Log.i("OplusTheiaUIMonitorTag-Detail", paramString); 
  }
  
  private boolean getUITimeoutEnableFromProp() {
    boolean bool = false;
    long l1 = SystemProperties.getInt("sys.theia.event_enable_mask", 0);
    long l2 = (1 << (int)5L);
    if ((l1 & l2) == l2)
      bool = true; 
    return bool;
  }
}
