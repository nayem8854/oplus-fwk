package android.os.connectivity;

import android.annotation.SystemApi;
import android.os.BatteryStats;
import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.CellSignalStrength;
import java.util.Arrays;
import java.util.Objects;

@SystemApi
public final class CellularBatteryStats implements Parcelable {
  public static final Parcelable.Creator<CellularBatteryStats> CREATOR = new Parcelable.Creator<CellularBatteryStats>() {
      public CellularBatteryStats createFromParcel(Parcel param1Parcel) {
        long l1 = param1Parcel.readLong();
        long l2 = param1Parcel.readLong();
        long l3 = param1Parcel.readLong();
        long l4 = param1Parcel.readLong();
        long l5 = param1Parcel.readLong();
        long l6 = param1Parcel.readLong();
        long l7 = param1Parcel.readLong();
        long l8 = param1Parcel.readLong();
        long l9 = param1Parcel.readLong();
        long l10 = param1Parcel.readLong();
        long[] arrayOfLong1 = param1Parcel.createLongArray();
        long[] arrayOfLong2 = param1Parcel.createLongArray();
        long[] arrayOfLong3 = param1Parcel.createLongArray();
        long l11 = param1Parcel.readLong();
        return 
          
          new CellularBatteryStats(l1, l2, l3, l4, l5, l6, l7, l8, l9, Long.valueOf(l10), arrayOfLong1, arrayOfLong2, arrayOfLong3, l11);
      }
      
      public CellularBatteryStats[] newArray(int param1Int) {
        return new CellularBatteryStats[param1Int];
      }
    };
  
  private final long mEnergyConsumedMaMs;
  
  private final long mIdleTimeMs;
  
  private final long mKernelActiveTimeMs;
  
  private final long mLoggingDurationMs;
  
  private final long mMonitoredRailChargeConsumedMaMs;
  
  private final long mNumBytesRx;
  
  private final long mNumBytesTx;
  
  private final long mNumPacketsRx;
  
  private final long mNumPacketsTx;
  
  private final long mRxTimeMs;
  
  private final long mSleepTimeMs;
  
  private final long[] mTimeInRatMs;
  
  private final long[] mTimeInRxSignalStrengthLevelMs;
  
  private final long[] mTxTimeMs;
  
  public CellularBatteryStats(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, long paramLong8, long paramLong9, Long paramLong, long[] paramArrayOflong1, long[] paramArrayOflong2, long[] paramArrayOflong3, long paramLong10) {
    this.mLoggingDurationMs = paramLong1;
    this.mKernelActiveTimeMs = paramLong2;
    this.mNumPacketsTx = paramLong3;
    this.mNumBytesTx = paramLong4;
    this.mNumPacketsRx = paramLong5;
    this.mNumBytesRx = paramLong6;
    this.mSleepTimeMs = paramLong7;
    this.mIdleTimeMs = paramLong8;
    this.mRxTimeMs = paramLong9;
    this.mEnergyConsumedMaMs = paramLong.longValue();
    int i = paramArrayOflong1.length, j = BatteryStats.NUM_DATA_CONNECTION_TYPES;
    j = Math.min(i, j);
    this.mTimeInRatMs = Arrays.copyOfRange(paramArrayOflong1, 0, j);
    j = paramArrayOflong2.length;
    i = CellSignalStrength.getNumSignalStrengthLevels();
    j = Math.min(j, i);
    this.mTimeInRxSignalStrengthLevelMs = Arrays.copyOfRange(paramArrayOflong2, 0, j);
    j = paramArrayOflong3.length;
    j = Math.min(j, 5);
    this.mTxTimeMs = Arrays.copyOfRange(paramArrayOflong3, 0, j);
    this.mMonitoredRailChargeConsumedMaMs = paramLong10;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mLoggingDurationMs);
    paramParcel.writeLong(this.mKernelActiveTimeMs);
    paramParcel.writeLong(this.mNumPacketsTx);
    paramParcel.writeLong(this.mNumBytesTx);
    paramParcel.writeLong(this.mNumPacketsRx);
    paramParcel.writeLong(this.mNumBytesRx);
    paramParcel.writeLong(this.mSleepTimeMs);
    paramParcel.writeLong(this.mIdleTimeMs);
    paramParcel.writeLong(this.mRxTimeMs);
    paramParcel.writeLong(this.mEnergyConsumedMaMs);
    paramParcel.writeLongArray(this.mTimeInRatMs);
    paramParcel.writeLongArray(this.mTimeInRxSignalStrengthLevelMs);
    paramParcel.writeLongArray(this.mTxTimeMs);
    paramParcel.writeLong(this.mMonitoredRailChargeConsumedMaMs);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof CellularBatteryStats;
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (paramObject == this)
      return true; 
    paramObject = paramObject;
    if (this.mLoggingDurationMs == ((CellularBatteryStats)paramObject).mLoggingDurationMs && this.mKernelActiveTimeMs == ((CellularBatteryStats)paramObject).mKernelActiveTimeMs && this.mNumPacketsTx == ((CellularBatteryStats)paramObject).mNumPacketsTx && this.mNumBytesTx == ((CellularBatteryStats)paramObject).mNumBytesTx && this.mNumPacketsRx == ((CellularBatteryStats)paramObject).mNumPacketsRx && this.mNumBytesRx == ((CellularBatteryStats)paramObject).mNumBytesRx && this.mSleepTimeMs == ((CellularBatteryStats)paramObject).mSleepTimeMs && this.mIdleTimeMs == ((CellularBatteryStats)paramObject).mIdleTimeMs && this.mRxTimeMs == ((CellularBatteryStats)paramObject).mRxTimeMs && this.mEnergyConsumedMaMs == ((CellularBatteryStats)paramObject).mEnergyConsumedMaMs) {
      long[] arrayOfLong1 = this.mTimeInRatMs, arrayOfLong2 = ((CellularBatteryStats)paramObject).mTimeInRatMs;
      if (Arrays.equals(arrayOfLong1, arrayOfLong2)) {
        arrayOfLong1 = this.mTimeInRxSignalStrengthLevelMs;
        arrayOfLong2 = ((CellularBatteryStats)paramObject).mTimeInRxSignalStrengthLevelMs;
        if (Arrays.equals(arrayOfLong1, arrayOfLong2)) {
          arrayOfLong2 = this.mTxTimeMs;
          arrayOfLong1 = ((CellularBatteryStats)paramObject).mTxTimeMs;
          if (Arrays.equals(arrayOfLong2, arrayOfLong1) && this.mMonitoredRailChargeConsumedMaMs == ((CellularBatteryStats)paramObject).mMonitoredRailChargeConsumedMaMs)
            bool1 = true; 
        } 
      } 
    } 
    return bool1;
  }
  
  public int hashCode() {
    long l1 = this.mLoggingDurationMs, l2 = this.mKernelActiveTimeMs, l3 = this.mNumPacketsTx, l4 = this.mNumBytesTx;
    long l5 = this.mNumPacketsRx, l6 = this.mNumBytesRx, l7 = this.mSleepTimeMs, l8 = this.mIdleTimeMs, l9 = this.mRxTimeMs;
    long l10 = this.mEnergyConsumedMaMs;
    int i = Arrays.hashCode(this.mTimeInRatMs);
    long[] arrayOfLong = this.mTimeInRxSignalStrengthLevelMs;
    int j = Arrays.hashCode(arrayOfLong), k = Arrays.hashCode(this.mTxTimeMs);
    long l11 = this.mMonitoredRailChargeConsumedMaMs;
    return Objects.hash(new Object[] { 
          Long.valueOf(l1), Long.valueOf(l2), Long.valueOf(l3), Long.valueOf(l4), Long.valueOf(l5), Long.valueOf(l6), Long.valueOf(l7), Long.valueOf(l8), Long.valueOf(l9), Long.valueOf(l10), 
          Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Long.valueOf(l11) });
  }
  
  public long getLoggingDurationMillis() {
    return this.mLoggingDurationMs;
  }
  
  public long getKernelActiveTimeMillis() {
    return this.mKernelActiveTimeMs;
  }
  
  public long getNumPacketsTx() {
    return this.mNumPacketsTx;
  }
  
  public long getNumBytesTx() {
    return this.mNumBytesTx;
  }
  
  public long getNumPacketsRx() {
    return this.mNumPacketsRx;
  }
  
  public long getNumBytesRx() {
    return this.mNumBytesRx;
  }
  
  public long getSleepTimeMillis() {
    return this.mSleepTimeMs;
  }
  
  public long getIdleTimeMillis() {
    return this.mIdleTimeMs;
  }
  
  public long getRxTimeMillis() {
    return this.mRxTimeMs;
  }
  
  public long getEnergyConsumedMaMillis() {
    return this.mEnergyConsumedMaMs;
  }
  
  public long getTimeInRatMicros(int paramInt) {
    long[] arrayOfLong = this.mTimeInRatMs;
    if (paramInt >= arrayOfLong.length)
      return -1L; 
    return arrayOfLong[paramInt];
  }
  
  public long getTimeInRxSignalStrengthLevelMicros(int paramInt) {
    long[] arrayOfLong = this.mTimeInRxSignalStrengthLevelMs;
    if (paramInt >= arrayOfLong.length)
      return -1L; 
    return arrayOfLong[paramInt];
  }
  
  public long getTxTimeMillis(int paramInt) {
    long[] arrayOfLong = this.mTxTimeMs;
    if (paramInt >= arrayOfLong.length)
      return -1L; 
    return arrayOfLong[paramInt];
  }
  
  public long getMonitoredRailChargeConsumedMaMillis() {
    return this.mMonitoredRailChargeConsumedMaMs;
  }
  
  public int describeContents() {
    return 0;
  }
}
