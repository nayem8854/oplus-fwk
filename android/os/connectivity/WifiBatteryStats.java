package android.os.connectivity;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Objects;

@SystemApi
public final class WifiBatteryStats implements Parcelable {
  public static final Parcelable.Creator<WifiBatteryStats> CREATOR = new Parcelable.Creator<WifiBatteryStats>() {
      public WifiBatteryStats createFromParcel(Parcel param1Parcel) {
        long l1 = param1Parcel.readLong();
        long l2 = param1Parcel.readLong();
        long l3 = param1Parcel.readLong();
        long l4 = param1Parcel.readLong();
        long l5 = param1Parcel.readLong();
        long l6 = param1Parcel.readLong();
        long l7 = param1Parcel.readLong();
        long l8 = param1Parcel.readLong();
        long l9 = param1Parcel.readLong();
        long l10 = param1Parcel.readLong();
        long l11 = param1Parcel.readLong();
        long l12 = param1Parcel.readLong();
        long l13 = param1Parcel.readLong();
        long[] arrayOfLong1 = param1Parcel.createLongArray();
        long[] arrayOfLong2 = param1Parcel.createLongArray();
        long[] arrayOfLong3 = param1Parcel.createLongArray();
        long l14 = param1Parcel.readLong();
        return new WifiBatteryStats(l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, arrayOfLong1, arrayOfLong2, arrayOfLong3, l14);
      }
      
      public WifiBatteryStats[] newArray(int param1Int) {
        return new WifiBatteryStats[param1Int];
      }
    };
  
  private final long mAppScanRequestCount;
  
  private final long mEnergyConsumedMaMillis;
  
  private final long mIdleTimeMillis;
  
  private final long mKernelActiveTimeMillis;
  
  private final long mLoggingDurationMillis;
  
  private final long mMonitoredRailChargeConsumedMaMillis;
  
  private final long mNumBytesRx;
  
  private final long mNumBytesTx;
  
  private final long mNumPacketsRx;
  
  private final long mNumPacketsTx;
  
  private final long mRxTimeMillis;
  
  private final long mScanTimeMillis;
  
  private final long mSleepTimeMillis;
  
  private final long[] mTimeInRxSignalStrengthLevelMillis;
  
  private final long[] mTimeInStateMillis;
  
  private final long[] mTimeInSupplicantStateMillis;
  
  private final long mTxTimeMillis;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mLoggingDurationMillis);
    paramParcel.writeLong(this.mKernelActiveTimeMillis);
    paramParcel.writeLong(this.mNumPacketsTx);
    paramParcel.writeLong(this.mNumBytesTx);
    paramParcel.writeLong(this.mNumPacketsRx);
    paramParcel.writeLong(this.mNumBytesRx);
    paramParcel.writeLong(this.mSleepTimeMillis);
    paramParcel.writeLong(this.mScanTimeMillis);
    paramParcel.writeLong(this.mIdleTimeMillis);
    paramParcel.writeLong(this.mRxTimeMillis);
    paramParcel.writeLong(this.mTxTimeMillis);
    paramParcel.writeLong(this.mEnergyConsumedMaMillis);
    paramParcel.writeLong(this.mAppScanRequestCount);
    paramParcel.writeLongArray(this.mTimeInStateMillis);
    paramParcel.writeLongArray(this.mTimeInRxSignalStrengthLevelMillis);
    paramParcel.writeLongArray(this.mTimeInSupplicantStateMillis);
    paramParcel.writeLong(this.mMonitoredRailChargeConsumedMaMillis);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof WifiBatteryStats;
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (paramObject == this)
      return true; 
    paramObject = paramObject;
    if (this.mLoggingDurationMillis == ((WifiBatteryStats)paramObject).mLoggingDurationMillis && this.mKernelActiveTimeMillis == ((WifiBatteryStats)paramObject).mKernelActiveTimeMillis && this.mNumPacketsTx == ((WifiBatteryStats)paramObject).mNumPacketsTx && this.mNumBytesTx == ((WifiBatteryStats)paramObject).mNumBytesTx && this.mNumPacketsRx == ((WifiBatteryStats)paramObject).mNumPacketsRx && this.mNumBytesRx == ((WifiBatteryStats)paramObject).mNumBytesRx && this.mSleepTimeMillis == ((WifiBatteryStats)paramObject).mSleepTimeMillis && this.mScanTimeMillis == ((WifiBatteryStats)paramObject).mScanTimeMillis && this.mIdleTimeMillis == ((WifiBatteryStats)paramObject).mIdleTimeMillis && this.mRxTimeMillis == ((WifiBatteryStats)paramObject).mRxTimeMillis && this.mTxTimeMillis == ((WifiBatteryStats)paramObject).mTxTimeMillis && this.mEnergyConsumedMaMillis == ((WifiBatteryStats)paramObject).mEnergyConsumedMaMillis && this.mAppScanRequestCount == ((WifiBatteryStats)paramObject).mAppScanRequestCount) {
      long[] arrayOfLong1 = this.mTimeInStateMillis, arrayOfLong2 = ((WifiBatteryStats)paramObject).mTimeInStateMillis;
      if (Arrays.equals(arrayOfLong1, arrayOfLong2)) {
        arrayOfLong2 = this.mTimeInSupplicantStateMillis;
        arrayOfLong1 = ((WifiBatteryStats)paramObject).mTimeInSupplicantStateMillis;
        if (Arrays.equals(arrayOfLong2, arrayOfLong1)) {
          arrayOfLong2 = this.mTimeInRxSignalStrengthLevelMillis;
          arrayOfLong1 = ((WifiBatteryStats)paramObject).mTimeInRxSignalStrengthLevelMillis;
          if (Arrays.equals(arrayOfLong2, arrayOfLong1) && this.mMonitoredRailChargeConsumedMaMillis == ((WifiBatteryStats)paramObject).mMonitoredRailChargeConsumedMaMillis)
            bool1 = true; 
        } 
      } 
    } 
    return bool1;
  }
  
  public int hashCode() {
    long l1 = this.mLoggingDurationMillis, l2 = this.mKernelActiveTimeMillis, l3 = this.mNumPacketsTx, l4 = this.mNumBytesTx;
    long l5 = this.mNumPacketsRx, l6 = this.mNumBytesRx, l7 = this.mSleepTimeMillis, l8 = this.mScanTimeMillis, l9 = this.mIdleTimeMillis;
    long l10 = this.mRxTimeMillis, l11 = this.mTxTimeMillis, l12 = this.mEnergyConsumedMaMillis, l13 = this.mAppScanRequestCount;
    int i = Arrays.hashCode(this.mTimeInStateMillis);
    long[] arrayOfLong = this.mTimeInSupplicantStateMillis;
    int j = Arrays.hashCode(arrayOfLong);
    arrayOfLong = this.mTimeInRxSignalStrengthLevelMillis;
    int k = Arrays.hashCode(arrayOfLong);
    long l14 = this.mMonitoredRailChargeConsumedMaMillis;
    return Objects.hash(new Object[] { 
          Long.valueOf(l1), Long.valueOf(l2), Long.valueOf(l3), Long.valueOf(l4), Long.valueOf(l5), Long.valueOf(l6), Long.valueOf(l7), Long.valueOf(l8), Long.valueOf(l9), Long.valueOf(l10), 
          Long.valueOf(l11), Long.valueOf(l12), Long.valueOf(l13), Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Long.valueOf(l14) });
  }
  
  public WifiBatteryStats(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, long paramLong8, long paramLong9, long paramLong10, long paramLong11, long paramLong12, long paramLong13, long[] paramArrayOflong1, long[] paramArrayOflong2, long[] paramArrayOflong3, long paramLong14) {
    this.mLoggingDurationMillis = paramLong1;
    this.mKernelActiveTimeMillis = paramLong2;
    this.mNumPacketsTx = paramLong3;
    this.mNumBytesTx = paramLong4;
    this.mNumPacketsRx = paramLong5;
    this.mNumBytesRx = paramLong6;
    this.mSleepTimeMillis = paramLong7;
    this.mScanTimeMillis = paramLong8;
    this.mIdleTimeMillis = paramLong9;
    this.mRxTimeMillis = paramLong10;
    this.mTxTimeMillis = paramLong11;
    this.mEnergyConsumedMaMillis = paramLong12;
    this.mAppScanRequestCount = paramLong13;
    int i = paramArrayOflong1.length;
    i = Math.min(i, 8);
    this.mTimeInStateMillis = Arrays.copyOfRange(paramArrayOflong1, 0, i);
    i = paramArrayOflong2.length;
    i = Math.min(i, 5);
    this.mTimeInRxSignalStrengthLevelMillis = Arrays.copyOfRange(paramArrayOflong2, 0, i);
    i = paramArrayOflong3.length;
    i = Math.min(i, 13);
    this.mTimeInSupplicantStateMillis = Arrays.copyOfRange(paramArrayOflong3, 0, i);
    this.mMonitoredRailChargeConsumedMaMillis = paramLong14;
  }
  
  public long getLoggingDurationMillis() {
    return this.mLoggingDurationMillis;
  }
  
  public long getKernelActiveTimeMillis() {
    return this.mKernelActiveTimeMillis;
  }
  
  public long getNumPacketsTx() {
    return this.mNumPacketsTx;
  }
  
  public long getNumBytesTx() {
    return this.mNumBytesTx;
  }
  
  public long getNumPacketsRx() {
    return this.mNumPacketsRx;
  }
  
  public long getNumBytesRx() {
    return this.mNumBytesRx;
  }
  
  public long getSleepTimeMillis() {
    return this.mSleepTimeMillis;
  }
  
  public long getScanTimeMillis() {
    return this.mScanTimeMillis;
  }
  
  public long getIdleTimeMillis() {
    return this.mIdleTimeMillis;
  }
  
  public long getRxTimeMillis() {
    return this.mRxTimeMillis;
  }
  
  public long getTxTimeMillis() {
    return this.mTxTimeMillis;
  }
  
  public long getEnergyConsumedMaMillis() {
    return this.mEnergyConsumedMaMillis;
  }
  
  public long getAppScanRequestCount() {
    return this.mAppScanRequestCount;
  }
  
  public long getMonitoredRailChargeConsumedMaMillis() {
    return this.mMonitoredRailChargeConsumedMaMillis;
  }
}
