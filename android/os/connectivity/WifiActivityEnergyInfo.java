package android.os.connectivity;

import android.annotation.SystemApi;
import android.app.ActivityThread;
import android.app.ContextImpl;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.os.PowerProfile;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class WifiActivityEnergyInfo implements Parcelable {
  public WifiActivityEnergyInfo(long paramLong1, int paramInt, long paramLong2, long paramLong3, long paramLong4, long paramLong5) {
    this(paramLong1, paramInt, paramLong2, paramLong3, paramLong4, paramLong5, l);
  }
  
  private static long calculateEnergyMicroJoules(long paramLong1, long paramLong2, long paramLong3) {
    ContextImpl contextImpl = ActivityThread.currentActivityThread().getSystemContext();
    if (contextImpl == null)
      return 0L; 
    PowerProfile powerProfile = new PowerProfile((Context)contextImpl);
    double d1 = powerProfile.getAveragePower("wifi.controller.idle");
    double d2 = powerProfile.getAveragePower("wifi.controller.rx");
    double d3 = powerProfile.getAveragePower("wifi.controller.tx");
    double d4 = powerProfile.getAveragePower("wifi.controller.voltage") / 1000.0D;
    return (long)((paramLong1 * d3 + paramLong2 * d2 + paramLong3 * d1) * d4);
  }
  
  public WifiActivityEnergyInfo(long paramLong1, int paramInt, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6) {
    this.mTimeSinceBootMillis = paramLong1;
    this.mStackState = paramInt;
    this.mControllerTxDurationMillis = paramLong2;
    this.mControllerRxDurationMillis = paramLong3;
    this.mControllerScanDurationMillis = paramLong4;
    this.mControllerIdleDurationMillis = paramLong5;
    this.mControllerEnergyUsedMicroJoules = paramLong6;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("WifiActivityEnergyInfo{ mTimeSinceBootMillis=");
    stringBuilder.append(this.mTimeSinceBootMillis);
    stringBuilder.append(" mStackState=");
    stringBuilder.append(this.mStackState);
    stringBuilder.append(" mControllerTxDurationMillis=");
    stringBuilder.append(this.mControllerTxDurationMillis);
    stringBuilder.append(" mControllerRxDurationMillis=");
    stringBuilder.append(this.mControllerRxDurationMillis);
    stringBuilder.append(" mControllerScanDurationMillis=");
    stringBuilder.append(this.mControllerScanDurationMillis);
    stringBuilder.append(" mControllerIdleDurationMillis=");
    stringBuilder.append(this.mControllerIdleDurationMillis);
    stringBuilder.append(" mControllerEnergyUsedMicroJoules=");
    stringBuilder.append(this.mControllerEnergyUsedMicroJoules);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<WifiActivityEnergyInfo> CREATOR = new Parcelable.Creator<WifiActivityEnergyInfo>() {
      public WifiActivityEnergyInfo createFromParcel(Parcel param1Parcel) {
        long l1 = param1Parcel.readLong();
        int i = param1Parcel.readInt();
        long l2 = param1Parcel.readLong();
        long l3 = param1Parcel.readLong();
        long l4 = param1Parcel.readLong();
        long l5 = param1Parcel.readLong();
        return new WifiActivityEnergyInfo(l1, i, l2, l3, l4, l5);
      }
      
      public WifiActivityEnergyInfo[] newArray(int param1Int) {
        return new WifiActivityEnergyInfo[param1Int];
      }
    };
  
  public static final int STACK_STATE_INVALID = 0;
  
  public static final int STACK_STATE_STATE_ACTIVE = 1;
  
  public static final int STACK_STATE_STATE_IDLE = 3;
  
  public static final int STACK_STATE_STATE_SCANNING = 2;
  
  private final long mControllerEnergyUsedMicroJoules;
  
  private final long mControllerIdleDurationMillis;
  
  private final long mControllerRxDurationMillis;
  
  private final long mControllerScanDurationMillis;
  
  private final long mControllerTxDurationMillis;
  
  private final int mStackState;
  
  private final long mTimeSinceBootMillis;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mTimeSinceBootMillis);
    paramParcel.writeInt(this.mStackState);
    paramParcel.writeLong(this.mControllerTxDurationMillis);
    paramParcel.writeLong(this.mControllerRxDurationMillis);
    paramParcel.writeLong(this.mControllerScanDurationMillis);
    paramParcel.writeLong(this.mControllerIdleDurationMillis);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public long getTimeSinceBootMillis() {
    return this.mTimeSinceBootMillis;
  }
  
  public int getStackState() {
    return this.mStackState;
  }
  
  public long getControllerTxDurationMillis() {
    return this.mControllerTxDurationMillis;
  }
  
  public long getControllerRxDurationMillis() {
    return this.mControllerRxDurationMillis;
  }
  
  public long getControllerScanDurationMillis() {
    return this.mControllerScanDurationMillis;
  }
  
  public long getControllerIdleDurationMillis() {
    return this.mControllerIdleDurationMillis;
  }
  
  public long getControllerEnergyUsedMicroJoules() {
    return this.mControllerEnergyUsedMicroJoules;
  }
  
  public boolean isValid() {
    boolean bool;
    if (this.mControllerTxDurationMillis >= 0L && this.mControllerRxDurationMillis >= 0L && this.mControllerScanDurationMillis >= 0L && this.mControllerIdleDurationMillis >= 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class StackState implements Annotation {}
}
