package android.os.connectivity;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public final class GpsBatteryStats implements Parcelable {
  public static final Parcelable.Creator<GpsBatteryStats> CREATOR = new Parcelable.Creator<GpsBatteryStats>() {
      public GpsBatteryStats createFromParcel(Parcel param1Parcel) {
        return new GpsBatteryStats(param1Parcel);
      }
      
      public GpsBatteryStats[] newArray(int param1Int) {
        return new GpsBatteryStats[param1Int];
      }
    };
  
  private long mEnergyConsumedMaMs;
  
  private long mLoggingDurationMs;
  
  private long[] mTimeInGpsSignalQualityLevel;
  
  public GpsBatteryStats() {
    initialize();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mLoggingDurationMs);
    paramParcel.writeLong(this.mEnergyConsumedMaMs);
    paramParcel.writeLongArray(this.mTimeInGpsSignalQualityLevel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mLoggingDurationMs = paramParcel.readLong();
    this.mEnergyConsumedMaMs = paramParcel.readLong();
    paramParcel.readLongArray(this.mTimeInGpsSignalQualityLevel);
  }
  
  public long getLoggingDurationMs() {
    return this.mLoggingDurationMs;
  }
  
  public long getEnergyConsumedMaMs() {
    return this.mEnergyConsumedMaMs;
  }
  
  public long[] getTimeInGpsSignalQualityLevel() {
    return this.mTimeInGpsSignalQualityLevel;
  }
  
  public void setLoggingDurationMs(long paramLong) {
    this.mLoggingDurationMs = paramLong;
  }
  
  public void setEnergyConsumedMaMs(long paramLong) {
    this.mEnergyConsumedMaMs = paramLong;
  }
  
  public void setTimeInGpsSignalQualityLevel(long[] paramArrayOflong) {
    int i = paramArrayOflong.length;
    i = Math.min(i, 2);
    this.mTimeInGpsSignalQualityLevel = Arrays.copyOfRange(paramArrayOflong, 0, i);
  }
  
  public int describeContents() {
    return 0;
  }
  
  private GpsBatteryStats(Parcel paramParcel) {
    initialize();
    readFromParcel(paramParcel);
  }
  
  private void initialize() {
    this.mLoggingDurationMs = 0L;
    this.mEnergyConsumedMaMs = 0L;
    this.mTimeInGpsSignalQualityLevel = new long[2];
  }
}
