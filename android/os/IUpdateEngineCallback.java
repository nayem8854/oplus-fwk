package android.os;

public interface IUpdateEngineCallback extends IInterface {
  void onPayloadApplicationComplete(int paramInt) throws RemoteException;
  
  void onStatusUpdate(int paramInt, float paramFloat) throws RemoteException;
  
  class Default implements IUpdateEngineCallback {
    public void onStatusUpdate(int param1Int, float param1Float) throws RemoteException {}
    
    public void onPayloadApplicationComplete(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IUpdateEngineCallback {
    private static final String DESCRIPTOR = "android.os.IUpdateEngineCallback";
    
    static final int TRANSACTION_onPayloadApplicationComplete = 2;
    
    static final int TRANSACTION_onStatusUpdate = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IUpdateEngineCallback");
    }
    
    public static IUpdateEngineCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IUpdateEngineCallback");
      if (iInterface != null && iInterface instanceof IUpdateEngineCallback)
        return (IUpdateEngineCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onPayloadApplicationComplete";
      } 
      return "onStatusUpdate";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.os.IUpdateEngineCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.IUpdateEngineCallback");
        param1Int1 = param1Parcel1.readInt();
        onPayloadApplicationComplete(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IUpdateEngineCallback");
      param1Int1 = param1Parcel1.readInt();
      float f = param1Parcel1.readFloat();
      onStatusUpdate(param1Int1, f);
      return true;
    }
    
    private static class Proxy implements IUpdateEngineCallback {
      public static IUpdateEngineCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IUpdateEngineCallback";
      }
      
      public void onStatusUpdate(int param2Int, float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IUpdateEngineCallback");
          parcel.writeInt(param2Int);
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IUpdateEngineCallback.Stub.getDefaultImpl() != null) {
            IUpdateEngineCallback.Stub.getDefaultImpl().onStatusUpdate(param2Int, param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPayloadApplicationComplete(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IUpdateEngineCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IUpdateEngineCallback.Stub.getDefaultImpl() != null) {
            IUpdateEngineCallback.Stub.getDefaultImpl().onPayloadApplicationComplete(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IUpdateEngineCallback param1IUpdateEngineCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IUpdateEngineCallback != null) {
          Proxy.sDefaultImpl = param1IUpdateEngineCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IUpdateEngineCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
