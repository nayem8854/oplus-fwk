package android.os;

import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.system.StructStat;
import android.text.TextUtils;
import android.util.Log;
import android.util.Slog;
import android.webkit.MimeTypeMap;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.SizedInputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.regex.Pattern;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import libcore.io.IoUtils;
import libcore.util.EmptyArray;

public final class FileUtils {
  private static final long COPY_CHECKPOINT_BYTES = 524288L;
  
  public static final int S_IRGRP = 32;
  
  public static final int S_IROTH = 4;
  
  public static final int S_IRUSR = 256;
  
  public static final int S_IRWXG = 56;
  
  public static final int S_IRWXO = 7;
  
  public static final int S_IRWXU = 448;
  
  public static final int S_IWGRP = 16;
  
  public static final int S_IWOTH = 2;
  
  public static final int S_IWUSR = 128;
  
  public static final int S_IXGRP = 8;
  
  public static final int S_IXOTH = 1;
  
  public static final int S_IXUSR = 64;
  
  private static final String TAG = "FileUtils";
  
  private static class NoImagePreloadHolder {
    public static final Pattern SAFE_FILENAME_PATTERN = Pattern.compile("[\\w%+,./=_-]+");
  }
  
  private static boolean sEnableCopyOptimizations = true;
  
  public static int setPermissions(File paramFile, int paramInt1, int paramInt2, int paramInt3) {
    return setPermissions(paramFile.getAbsolutePath(), paramInt1, paramInt2, paramInt3);
  }
  
  public static int setPermissions(String paramString, int paramInt1, int paramInt2, int paramInt3) {
    try {
      Os.chmod(paramString, paramInt1);
      if (paramInt2 >= 0 || paramInt3 >= 0)
        try {
          Os.chown(paramString, paramInt2, paramInt3);
          return 0;
        } catch (ErrnoException errnoException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to chown(");
          stringBuilder.append(paramString);
          stringBuilder.append("): ");
          stringBuilder.append(errnoException);
          Slog.w("FileUtils", stringBuilder.toString());
          return errnoException.errno;
        }  
      return 0;
    } catch (ErrnoException errnoException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to chmod(");
      stringBuilder.append(paramString);
      stringBuilder.append("): ");
      stringBuilder.append(errnoException);
      Slog.w("FileUtils", stringBuilder.toString());
      return errnoException.errno;
    } 
  }
  
  public static int setPermissions(FileDescriptor paramFileDescriptor, int paramInt1, int paramInt2, int paramInt3) {
    try {
      Os.fchmod(paramFileDescriptor, paramInt1);
      if (paramInt2 >= 0 || paramInt3 >= 0)
        try {
          Os.fchown(paramFileDescriptor, paramInt2, paramInt3);
          return 0;
        } catch (ErrnoException errnoException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to fchown(): ");
          stringBuilder.append(errnoException);
          Slog.w("FileUtils", stringBuilder.toString());
          return errnoException.errno;
        }  
      return 0;
    } catch (ErrnoException errnoException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to fchmod(): ");
      stringBuilder.append(errnoException);
      Slog.w("FileUtils", stringBuilder.toString());
      return errnoException.errno;
    } 
  }
  
  public static void copyPermissions(File paramFile1, File paramFile2) throws IOException {
    try {
      StructStat structStat = Os.stat(paramFile1.getAbsolutePath());
      Os.chmod(paramFile2.getAbsolutePath(), structStat.st_mode);
      Os.chown(paramFile2.getAbsolutePath(), structStat.st_uid, structStat.st_gid);
      return;
    } catch (ErrnoException errnoException) {
      throw errnoException.rethrowAsIOException();
    } 
  }
  
  @Deprecated
  public static int getUid(String paramString) {
    try {
      return (Os.stat(paramString)).st_uid;
    } catch (ErrnoException errnoException) {
      return -1;
    } 
  }
  
  public static boolean sync(FileOutputStream paramFileOutputStream) {
    if (paramFileOutputStream != null)
      try {
        paramFileOutputStream.getFD().sync();
      } catch (IOException iOException) {
        return false;
      }  
    return true;
  }
  
  @Deprecated
  public static boolean copyFile(File paramFile1, File paramFile2) {
    try {
      copyFileOrThrow(paramFile1, paramFile2);
      return true;
    } catch (IOException iOException) {
      return false;
    } 
  }
  
  @Deprecated
  public static void copyFileOrThrow(File paramFile1, File paramFile2) throws IOException {
    FileInputStream fileInputStream = new FileInputStream(paramFile1);
    try {
      copyToFileOrThrow(fileInputStream, paramFile2);
      return;
    } finally {
      try {
        fileInputStream.close();
      } finally {
        paramFile2 = null;
      } 
    } 
  }
  
  @Deprecated
  public static boolean copyToFile(InputStream paramInputStream, File paramFile) {
    try {
      copyToFileOrThrow(paramInputStream, paramFile);
      return true;
    } catch (IOException iOException) {
      return false;
    } 
  }
  
  @Deprecated
  public static void copyToFileOrThrow(InputStream paramInputStream, File paramFile) throws IOException {
    if (paramFile.exists())
      paramFile.delete(); 
    FileOutputStream fileOutputStream = new FileOutputStream(paramFile);
    try {
      copy(paramInputStream, fileOutputStream);
    } finally {
      try {
        fileOutputStream.close();
      } finally {
        fileOutputStream = null;
      } 
    } 
  }
  
  public static long copy(File paramFile1, File paramFile2) throws IOException {
    return copy(paramFile1, paramFile2, (CancellationSignal)null, (Executor)null, (ProgressListener)null);
  }
  
  public static long copy(File paramFile1, File paramFile2, CancellationSignal paramCancellationSignal, Executor paramExecutor, ProgressListener paramProgressListener) throws IOException {
    FileInputStream fileInputStream = new FileInputStream(paramFile1);
    try {
      FileOutputStream fileOutputStream = new FileOutputStream();
      this(paramFile2);
    } finally {
      try {
        fileInputStream.close();
      } finally {
        fileInputStream = null;
      } 
    } 
  }
  
  public static long copy(InputStream paramInputStream, OutputStream paramOutputStream) throws IOException {
    return copy(paramInputStream, paramOutputStream, (CancellationSignal)null, (Executor)null, (ProgressListener)null);
  }
  
  public static long copy(InputStream paramInputStream, OutputStream paramOutputStream, CancellationSignal paramCancellationSignal, Executor paramExecutor, ProgressListener paramProgressListener) throws IOException {
    if (sEnableCopyOptimizations && 
      paramInputStream instanceof FileInputStream && paramOutputStream instanceof FileOutputStream)
      return copy(((FileInputStream)paramInputStream).getFD(), ((FileOutputStream)paramOutputStream).getFD(), paramCancellationSignal, paramExecutor, paramProgressListener); 
    return copyInternalUserspace(paramInputStream, paramOutputStream, paramCancellationSignal, paramExecutor, paramProgressListener);
  }
  
  public static long copy(FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2) throws IOException {
    return copy(paramFileDescriptor1, paramFileDescriptor2, (CancellationSignal)null, (Executor)null, (ProgressListener)null);
  }
  
  public static long copy(FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, CancellationSignal paramCancellationSignal, Executor paramExecutor, ProgressListener paramProgressListener) throws IOException {
    return copy(paramFileDescriptor1, paramFileDescriptor2, Long.MAX_VALUE, paramCancellationSignal, paramExecutor, paramProgressListener);
  }
  
  public static long copy(FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, long paramLong, CancellationSignal paramCancellationSignal, Executor paramExecutor, ProgressListener paramProgressListener) throws IOException {
    if (sEnableCopyOptimizations)
      try {
        StructStat structStat1 = Os.fstat(paramFileDescriptor1);
        StructStat structStat2 = Os.fstat(paramFileDescriptor2);
        if (OsConstants.S_ISREG(structStat1.st_mode) && OsConstants.S_ISREG(structStat2.st_mode))
          return copyInternalSendfile(paramFileDescriptor1, paramFileDescriptor2, paramLong, paramCancellationSignal, paramExecutor, paramProgressListener); 
        if (OsConstants.S_ISFIFO(structStat1.st_mode) || OsConstants.S_ISFIFO(structStat2.st_mode))
          return copyInternalSplice(paramFileDescriptor1, paramFileDescriptor2, paramLong, paramCancellationSignal, paramExecutor, paramProgressListener); 
      } catch (ErrnoException errnoException) {
        throw errnoException.rethrowAsIOException();
      }  
    return copyInternalUserspace((FileDescriptor)errnoException, paramFileDescriptor2, paramLong, paramCancellationSignal, paramExecutor, paramProgressListener);
  }
  
  public static long copyInternalSplice(FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, long paramLong, CancellationSignal paramCancellationSignal, Executor paramExecutor, ProgressListener paramProgressListener) throws ErrnoException {
    long l1 = 0L, l2 = 0L, l3 = paramLong;
    paramLong = l1;
    while (true) {
      long l = Os.splice(paramFileDescriptor1, null, paramFileDescriptor2, null, Math.min(l3, 524288L), OsConstants.SPLICE_F_MOVE | OsConstants.SPLICE_F_MORE);
      if (l != 0L) {
        l1 = l2 + l;
        long l4 = paramLong + l;
        l = l3 - l;
        l3 = l;
        l2 = l1;
        paramLong = l4;
        if (l4 >= 524288L) {
          if (paramCancellationSignal != null)
            paramCancellationSignal.throwIfCanceled(); 
          if (paramExecutor != null && paramProgressListener != null)
            paramExecutor.execute(new _$$Lambda$FileUtils$RlOy_0MlKMWkkCC1mk_jzWcLTKs(paramProgressListener, l1)); 
          paramLong = 0L;
          l3 = l;
          l2 = l1;
        } 
        continue;
      } 
      break;
    } 
    if (paramExecutor != null && paramProgressListener != null)
      paramExecutor.execute(new _$$Lambda$FileUtils$e0JoE_HjVf9vMX679eNxZixyUZ0(paramProgressListener, l2)); 
    return l2;
  }
  
  public static long copyInternalSendfile(FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, long paramLong, CancellationSignal paramCancellationSignal, Executor paramExecutor, ProgressListener paramProgressListener) throws ErrnoException {
    long l1 = 0L, l2 = 0L, l3 = paramLong;
    paramLong = l1;
    while (true) {
      long l = Os.sendfile(paramFileDescriptor2, paramFileDescriptor1, null, Math.min(l3, 524288L));
      if (l != 0L) {
        l1 = l2 + l;
        long l4 = paramLong + l;
        l = l3 - l;
        l3 = l;
        l2 = l1;
        paramLong = l4;
        if (l4 >= 524288L) {
          if (paramCancellationSignal != null)
            paramCancellationSignal.throwIfCanceled(); 
          if (paramExecutor != null && paramProgressListener != null)
            paramExecutor.execute(new _$$Lambda$FileUtils$QtbHtI8Y1rifwydngi6coGK5l2A(paramProgressListener, l1)); 
          paramLong = 0L;
          l3 = l;
          l2 = l1;
        } 
        continue;
      } 
      break;
    } 
    if (paramExecutor != null && paramProgressListener != null)
      paramExecutor.execute(new _$$Lambda$FileUtils$XQaJiyjsC2_MFNDbZFQcIhqPnNA(paramProgressListener, l2)); 
    return l2;
  }
  
  @Deprecated
  public static long copyInternalUserspace(FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, ProgressListener paramProgressListener, CancellationSignal paramCancellationSignal, long paramLong) throws IOException {
    return copyInternalUserspace(paramFileDescriptor1, paramFileDescriptor2, paramLong, paramCancellationSignal, (Executor)_$$Lambda$_14QHG018Z6p13d3hzJuGTWnNeo.INSTANCE, paramProgressListener);
  }
  
  public static long copyInternalUserspace(FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, long paramLong, CancellationSignal paramCancellationSignal, Executor paramExecutor, ProgressListener paramProgressListener) throws IOException {
    if (paramLong != Long.MAX_VALUE)
      return copyInternalUserspace((InputStream)new SizedInputStream(new FileInputStream(paramFileDescriptor1), paramLong), new FileOutputStream(paramFileDescriptor2), paramCancellationSignal, paramExecutor, paramProgressListener); 
    return copyInternalUserspace(new FileInputStream(paramFileDescriptor1), new FileOutputStream(paramFileDescriptor2), paramCancellationSignal, paramExecutor, paramProgressListener);
  }
  
  public static long copyInternalUserspace(InputStream paramInputStream, OutputStream paramOutputStream, CancellationSignal paramCancellationSignal, Executor paramExecutor, ProgressListener paramProgressListener) throws IOException {
    long l1 = 0L;
    long l2 = 0L;
    byte[] arrayOfByte = new byte[8192];
    while (true) {
      int i = paramInputStream.read(arrayOfByte);
      if (i != -1) {
        paramOutputStream.write(arrayOfByte, 0, i);
        long l3 = l1 + i;
        long l4 = l2 + i;
        l1 = l3;
        l2 = l4;
        if (l4 >= 524288L) {
          if (paramCancellationSignal != null)
            paramCancellationSignal.throwIfCanceled(); 
          if (paramExecutor != null && paramProgressListener != null)
            paramExecutor.execute(new _$$Lambda$FileUtils$TJeD9NeX5giO_5vlBrurGI_g4IY(paramProgressListener, l3)); 
          l2 = 0L;
          l1 = l3;
        } 
        continue;
      } 
      break;
    } 
    if (paramExecutor != null && paramProgressListener != null)
      paramExecutor.execute(new _$$Lambda$FileUtils$0SBPRWOXcbR9EMG_p_55sUuxJ_0(paramProgressListener, l1)); 
    return l1;
  }
  
  public static boolean isFilenameSafe(File paramFile) {
    return NoImagePreloadHolder.SAFE_FILENAME_PATTERN.matcher(paramFile.getPath()).matches();
  }
  
  public static String readTextFile(File paramFile, int paramInt, String paramString) throws IOException {
    // Byte code:
    //   0: new java/io/FileInputStream
    //   3: dup
    //   4: aload_0
    //   5: invokespecial <init> : (Ljava/io/File;)V
    //   8: astore_3
    //   9: new java/io/BufferedInputStream
    //   12: dup
    //   13: aload_3
    //   14: invokespecial <init> : (Ljava/io/InputStream;)V
    //   17: astore #4
    //   19: aload_0
    //   20: invokevirtual length : ()J
    //   23: lstore #5
    //   25: iload_1
    //   26: ifgt -> 346
    //   29: lload #5
    //   31: lconst_0
    //   32: lcmp
    //   33: ifle -> 43
    //   36: iload_1
    //   37: ifne -> 43
    //   40: goto -> 346
    //   43: iload_1
    //   44: ifge -> 292
    //   47: iconst_0
    //   48: istore #7
    //   50: aconst_null
    //   51: astore #8
    //   53: aconst_null
    //   54: astore_0
    //   55: iload #7
    //   57: istore #9
    //   59: aload #8
    //   61: ifnull -> 67
    //   64: iconst_1
    //   65: istore #9
    //   67: aload_0
    //   68: astore #10
    //   70: aload #8
    //   72: astore #11
    //   74: aload #8
    //   76: ifnonnull -> 89
    //   79: iload_1
    //   80: ineg
    //   81: istore #7
    //   83: iload #7
    //   85: newarray byte
    //   87: astore #11
    //   89: aload #4
    //   91: aload #11
    //   93: invokevirtual read : ([B)I
    //   96: istore #12
    //   98: aload #11
    //   100: arraylength
    //   101: istore #13
    //   103: iload #9
    //   105: istore #7
    //   107: aload #10
    //   109: astore #8
    //   111: aload #11
    //   113: astore_0
    //   114: iload #12
    //   116: iload #13
    //   118: if_icmpeq -> 55
    //   121: aload #10
    //   123: ifnonnull -> 144
    //   126: iload #12
    //   128: ifgt -> 144
    //   131: aload #4
    //   133: invokevirtual close : ()V
    //   136: aload_3
    //   137: invokevirtual close : ()V
    //   140: ldc_w ''
    //   143: areturn
    //   144: aload #10
    //   146: ifnonnull -> 173
    //   149: new java/lang/String
    //   152: dup
    //   153: aload #11
    //   155: iconst_0
    //   156: iload #12
    //   158: invokespecial <init> : ([BII)V
    //   161: astore_0
    //   162: aload #4
    //   164: invokevirtual close : ()V
    //   167: aload_3
    //   168: invokevirtual close : ()V
    //   171: aload_0
    //   172: areturn
    //   173: iload #12
    //   175: ifle -> 213
    //   178: iconst_1
    //   179: istore #9
    //   181: aload #10
    //   183: iload #12
    //   185: aload #10
    //   187: iconst_0
    //   188: aload #10
    //   190: arraylength
    //   191: iload #12
    //   193: isub
    //   194: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   197: aload #11
    //   199: iconst_0
    //   200: aload #10
    //   202: aload #10
    //   204: arraylength
    //   205: iload #12
    //   207: isub
    //   208: iload #12
    //   210: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   213: aload_2
    //   214: ifnull -> 271
    //   217: iload #9
    //   219: ifne -> 225
    //   222: goto -> 271
    //   225: new java/lang/StringBuilder
    //   228: astore_0
    //   229: aload_0
    //   230: invokespecial <init> : ()V
    //   233: aload_0
    //   234: aload_2
    //   235: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   238: pop
    //   239: new java/lang/String
    //   242: astore_2
    //   243: aload_2
    //   244: aload #10
    //   246: invokespecial <init> : ([B)V
    //   249: aload_0
    //   250: aload_2
    //   251: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   254: pop
    //   255: aload_0
    //   256: invokevirtual toString : ()Ljava/lang/String;
    //   259: astore_0
    //   260: aload #4
    //   262: invokevirtual close : ()V
    //   265: aload_3
    //   266: invokevirtual close : ()V
    //   269: aload_0
    //   270: areturn
    //   271: new java/lang/String
    //   274: dup
    //   275: aload #10
    //   277: invokespecial <init> : ([B)V
    //   280: astore_0
    //   281: aload #4
    //   283: invokevirtual close : ()V
    //   286: aload_3
    //   287: invokevirtual close : ()V
    //   290: aload_0
    //   291: areturn
    //   292: new java/io/ByteArrayOutputStream
    //   295: astore_0
    //   296: aload_0
    //   297: invokespecial <init> : ()V
    //   300: sipush #1024
    //   303: newarray byte
    //   305: astore_2
    //   306: aload #4
    //   308: aload_2
    //   309: invokevirtual read : ([B)I
    //   312: istore_1
    //   313: iload_1
    //   314: ifle -> 324
    //   317: aload_0
    //   318: aload_2
    //   319: iconst_0
    //   320: iload_1
    //   321: invokevirtual write : ([BII)V
    //   324: iload_1
    //   325: aload_2
    //   326: arraylength
    //   327: if_icmpeq -> 306
    //   330: aload_0
    //   331: invokevirtual toString : ()Ljava/lang/String;
    //   334: astore_0
    //   335: aload #4
    //   337: invokevirtual close : ()V
    //   340: aload_3
    //   341: invokevirtual close : ()V
    //   344: aload_0
    //   345: areturn
    //   346: iload_1
    //   347: istore #9
    //   349: lload #5
    //   351: lconst_0
    //   352: lcmp
    //   353: ifle -> 376
    //   356: iload_1
    //   357: ifeq -> 371
    //   360: iload_1
    //   361: istore #9
    //   363: lload #5
    //   365: iload_1
    //   366: i2l
    //   367: lcmp
    //   368: ifge -> 376
    //   371: lload #5
    //   373: l2i
    //   374: istore #9
    //   376: iload #9
    //   378: iconst_1
    //   379: iadd
    //   380: newarray byte
    //   382: astore #11
    //   384: aload #4
    //   386: aload #11
    //   388: invokevirtual read : ([B)I
    //   391: istore_1
    //   392: iload_1
    //   393: ifgt -> 409
    //   396: aload #4
    //   398: invokevirtual close : ()V
    //   401: aload_3
    //   402: invokevirtual close : ()V
    //   405: ldc_w ''
    //   408: areturn
    //   409: iload_1
    //   410: iload #9
    //   412: if_icmpgt -> 438
    //   415: new java/lang/String
    //   418: dup
    //   419: aload #11
    //   421: iconst_0
    //   422: iload_1
    //   423: invokespecial <init> : ([BII)V
    //   426: astore_0
    //   427: aload #4
    //   429: invokevirtual close : ()V
    //   432: aload_3
    //   433: invokevirtual close : ()V
    //   436: aload_0
    //   437: areturn
    //   438: aload_2
    //   439: ifnonnull -> 466
    //   442: new java/lang/String
    //   445: dup
    //   446: aload #11
    //   448: iconst_0
    //   449: iload #9
    //   451: invokespecial <init> : ([BII)V
    //   454: astore_0
    //   455: aload #4
    //   457: invokevirtual close : ()V
    //   460: aload_3
    //   461: invokevirtual close : ()V
    //   464: aload_0
    //   465: areturn
    //   466: new java/lang/StringBuilder
    //   469: astore_0
    //   470: aload_0
    //   471: invokespecial <init> : ()V
    //   474: new java/lang/String
    //   477: astore #8
    //   479: aload #8
    //   481: aload #11
    //   483: iconst_0
    //   484: iload #9
    //   486: invokespecial <init> : ([BII)V
    //   489: aload_0
    //   490: aload #8
    //   492: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   495: pop
    //   496: aload_0
    //   497: aload_2
    //   498: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   501: pop
    //   502: aload_0
    //   503: invokevirtual toString : ()Ljava/lang/String;
    //   506: astore_0
    //   507: aload #4
    //   509: invokevirtual close : ()V
    //   512: aload_3
    //   513: invokevirtual close : ()V
    //   516: aload_0
    //   517: areturn
    //   518: astore_0
    //   519: aload #4
    //   521: invokevirtual close : ()V
    //   524: aload_3
    //   525: invokevirtual close : ()V
    //   528: aload_0
    //   529: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #614	-> 0
    //   #618	-> 9
    //   #620	-> 19
    //   #621	-> 25
    //   #629	-> 43
    //   #631	-> 47
    //   #632	-> 50
    //   #633	-> 53
    //   #635	-> 55
    //   #636	-> 67
    //   #637	-> 70
    //   #638	-> 89
    //   #639	-> 98
    //   #641	-> 121
    //   #661	-> 131
    //   #662	-> 136
    //   #641	-> 140
    //   #642	-> 144
    //   #661	-> 162
    //   #662	-> 167
    //   #642	-> 171
    //   #643	-> 173
    //   #644	-> 178
    //   #645	-> 181
    //   #646	-> 197
    //   #648	-> 213
    //   #649	-> 225
    //   #661	-> 260
    //   #662	-> 265
    //   #649	-> 269
    //   #648	-> 271
    //   #661	-> 281
    //   #662	-> 286
    //   #648	-> 290
    //   #651	-> 292
    //   #653	-> 300
    //   #655	-> 306
    //   #656	-> 313
    //   #657	-> 324
    //   #658	-> 330
    //   #661	-> 335
    //   #662	-> 340
    //   #658	-> 344
    //   #622	-> 346
    //   #623	-> 376
    //   #624	-> 384
    //   #625	-> 392
    //   #661	-> 396
    //   #662	-> 401
    //   #625	-> 405
    //   #626	-> 409
    //   #661	-> 427
    //   #662	-> 432
    //   #626	-> 436
    //   #627	-> 438
    //   #661	-> 455
    //   #662	-> 460
    //   #627	-> 464
    //   #628	-> 466
    //   #661	-> 507
    //   #662	-> 512
    //   #628	-> 516
    //   #661	-> 518
    //   #662	-> 524
    //   #663	-> 528
    // Exception table:
    //   from	to	target	type
    //   19	25	518	finally
    //   83	89	518	finally
    //   89	98	518	finally
    //   98	103	518	finally
    //   149	162	518	finally
    //   181	197	518	finally
    //   197	213	518	finally
    //   225	260	518	finally
    //   271	281	518	finally
    //   292	300	518	finally
    //   300	306	518	finally
    //   306	313	518	finally
    //   317	324	518	finally
    //   324	330	518	finally
    //   330	335	518	finally
    //   376	384	518	finally
    //   384	392	518	finally
    //   415	427	518	finally
    //   442	455	518	finally
    //   466	507	518	finally
  }
  
  public static void stringToFile(File paramFile, String paramString) throws IOException {
    stringToFile(paramFile.getAbsolutePath(), paramString);
  }
  
  public static void bytesToFile(String paramString, byte[] paramArrayOfbyte) throws IOException {
    if (paramString.startsWith("/proc/")) {
      int i = StrictMode.allowThreadDiskWritesMask();
      try {
        FileOutputStream fileOutputStream = new FileOutputStream();
        this(paramString);
      } finally {
        StrictMode.setThreadPolicyMask(i);
      } 
    } else {
      FileOutputStream fileOutputStream = new FileOutputStream(paramString);
      try {
        fileOutputStream.write(paramArrayOfbyte);
        return;
      } finally {
        try {
          fileOutputStream.close();
        } finally {
          fileOutputStream = null;
        } 
      } 
    } 
  }
  
  public static void stringToFile(String paramString1, String paramString2) throws IOException {
    bytesToFile(paramString1, paramString2.getBytes(StandardCharsets.UTF_8));
  }
  
  @Deprecated
  public static long checksumCrc32(File paramFile) throws FileNotFoundException, IOException {
    CRC32 cRC32 = new CRC32();
    CheckedInputStream checkedInputStream1 = null;
    CheckedInputStream checkedInputStream2 = checkedInputStream1;
    try {
      CheckedInputStream checkedInputStream4 = new CheckedInputStream();
      checkedInputStream2 = checkedInputStream1;
      FileInputStream fileInputStream = new FileInputStream();
      checkedInputStream2 = checkedInputStream1;
      this(paramFile);
      checkedInputStream2 = checkedInputStream1;
      this(fileInputStream, cRC32);
      CheckedInputStream checkedInputStream3 = checkedInputStream4;
      checkedInputStream2 = checkedInputStream3;
      byte[] arrayOfByte = new byte[128];
      while (true) {
        checkedInputStream2 = checkedInputStream3;
        if (checkedInputStream3.read(arrayOfByte) >= 0)
          continue; 
        break;
      } 
      checkedInputStream2 = checkedInputStream3;
      return cRC32.getValue();
    } finally {
      if (checkedInputStream2 != null)
        try {
          checkedInputStream2.close();
        } catch (IOException iOException) {} 
    } 
  }
  
  public static byte[] digest(File paramFile, String paramString) throws IOException, NoSuchAlgorithmException {
    FileInputStream fileInputStream = new FileInputStream(paramFile);
    try {
      return digest(fileInputStream, paramString);
    } finally {
      try {
        fileInputStream.close();
      } finally {
        fileInputStream = null;
      } 
    } 
  }
  
  public static byte[] digest(InputStream paramInputStream, String paramString) throws IOException, NoSuchAlgorithmException {
    return digestInternalUserspace(paramInputStream, paramString);
  }
  
  public static byte[] digest(FileDescriptor paramFileDescriptor, String paramString) throws IOException, NoSuchAlgorithmException {
    return digestInternalUserspace(new FileInputStream(paramFileDescriptor), paramString);
  }
  
  private static byte[] digestInternalUserspace(InputStream paramInputStream, String paramString) throws IOException, NoSuchAlgorithmException {
    null = MessageDigest.getInstance(paramString);
    paramInputStream = new DigestInputStream(paramInputStream, null);
    try {
      byte[] arrayOfByte = new byte[8192];
      while (true) {
        int i = paramInputStream.read(arrayOfByte);
        if (i != -1)
          continue; 
        break;
      } 
      return null.digest();
    } finally {
      try {
        paramInputStream.close();
      } finally {
        paramInputStream = null;
      } 
    } 
  }
  
  public static boolean deleteOlderFiles(File paramFile, int paramInt, long paramLong) {
    if (paramInt >= 0 && paramLong >= 0L) {
      File[] arrayOfFile = paramFile.listFiles();
      if (arrayOfFile == null)
        return false; 
      Arrays.sort(arrayOfFile, new Comparator<File>() {
            public int compare(File param1File1, File param1File2) {
              return Long.compare(param1File2.lastModified(), param1File1.lastModified());
            }
          });
      boolean bool = false;
      for (; paramInt < arrayOfFile.length; paramInt++, bool = bool1) {
        File file = arrayOfFile[paramInt];
        long l1 = System.currentTimeMillis(), l2 = file.lastModified();
        boolean bool1 = bool;
        if (l1 - l2 > paramLong) {
          bool1 = bool;
          if (file.delete()) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Deleted old file ");
            stringBuilder.append(file);
            Log.d("FileUtils", stringBuilder.toString());
            bool1 = true;
          } 
        } 
      } 
      return bool;
    } 
    throw new IllegalArgumentException("Constraints must be positive or 0");
  }
  
  public static boolean contains(File[] paramArrayOfFile, File paramFile) {
    int i;
    byte b;
    for (i = paramArrayOfFile.length, b = 0; b < i; ) {
      File file = paramArrayOfFile[b];
      if (contains(file, paramFile))
        return true; 
      b++;
    } 
    return false;
  }
  
  public static boolean contains(Collection<File> paramCollection, File paramFile) {
    for (File file : paramCollection) {
      if (contains(file, paramFile))
        return true; 
    } 
    return false;
  }
  
  public static boolean contains(File paramFile1, File paramFile2) {
    if (paramFile1 == null || paramFile2 == null)
      return false; 
    return contains(paramFile1.getAbsolutePath(), paramFile2.getAbsolutePath());
  }
  
  public static boolean contains(String paramString1, String paramString2) {
    if (paramString1.equals(paramString2))
      return true; 
    String str = paramString1;
    if (!paramString1.endsWith("/")) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("/");
      str = stringBuilder.toString();
    } 
    return paramString2.startsWith(str);
  }
  
  public static boolean deleteContentsAndDir(File paramFile) {
    if (deleteContents(paramFile))
      return paramFile.delete(); 
    return false;
  }
  
  public static boolean deleteContents(File paramFile) {
    File[] arrayOfFile = paramFile.listFiles();
    boolean bool1 = true, bool2 = true;
    if (arrayOfFile != null) {
      int i = arrayOfFile.length;
      byte b = 0;
      while (true) {
        bool1 = bool2;
        if (b < i) {
          File file = arrayOfFile[b];
          bool1 = bool2;
          if (file.isDirectory())
            bool1 = bool2 & deleteContents(file); 
          bool2 = bool1;
          if (!file.delete()) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Failed to delete ");
            stringBuilder.append(file);
            Log.w("FileUtils", stringBuilder.toString());
            bool2 = false;
          } 
          b++;
          continue;
        } 
        break;
      } 
    } 
    return bool1;
  }
  
  private static boolean isValidExtFilenameChar(char paramChar) {
    if (paramChar != '\000' && paramChar != '/')
      return true; 
    return false;
  }
  
  public static boolean isValidExtFilename(String paramString) {
    boolean bool;
    if (paramString != null && paramString.equals(buildValidExtFilename(paramString))) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static String buildValidExtFilename(String paramString) {
    if (TextUtils.isEmpty(paramString) || ".".equals(paramString) || "..".equals(paramString))
      return "(invalid)"; 
    StringBuilder stringBuilder = new StringBuilder(paramString.length());
    for (byte b = 0; b < paramString.length(); b++) {
      char c = paramString.charAt(b);
      if (isValidExtFilenameChar(c)) {
        stringBuilder.append(c);
      } else {
        stringBuilder.append('_');
      } 
    } 
    trimFilename(stringBuilder, 255);
    return stringBuilder.toString();
  }
  
  private static boolean isValidFatFilenameChar(char paramChar) {
    if (paramChar >= '\000' && paramChar <= '\037')
      return false; 
    if (paramChar != '"' && paramChar != '*' && paramChar != '/' && paramChar != ':' && paramChar != '<' && paramChar != '\\' && paramChar != '|' && paramChar != '' && paramChar != '>' && paramChar != '?')
      return true; 
    return false;
  }
  
  public static boolean isValidFatFilename(String paramString) {
    boolean bool;
    if (paramString != null && paramString.equals(buildValidFatFilename(paramString))) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static String buildValidFatFilename(String paramString) {
    if (TextUtils.isEmpty(paramString) || ".".equals(paramString) || "..".equals(paramString))
      return "(invalid)"; 
    StringBuilder stringBuilder = new StringBuilder(paramString.length());
    for (byte b = 0; b < paramString.length(); b++) {
      char c = paramString.charAt(b);
      if (isValidFatFilenameChar(c)) {
        stringBuilder.append(c);
      } else {
        stringBuilder.append('_');
      } 
    } 
    trimFilename(stringBuilder, 255);
    return stringBuilder.toString();
  }
  
  public static String trimFilename(String paramString, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder(paramString);
    trimFilename(stringBuilder, paramInt);
    return stringBuilder.toString();
  }
  
  private static void trimFilename(StringBuilder paramStringBuilder, int paramInt) {
    byte[] arrayOfByte = paramStringBuilder.toString().getBytes(StandardCharsets.UTF_8);
    if (arrayOfByte.length > paramInt) {
      while (arrayOfByte.length > paramInt - 3) {
        paramStringBuilder.deleteCharAt(paramStringBuilder.length() / 2);
        arrayOfByte = paramStringBuilder.toString().getBytes(StandardCharsets.UTF_8);
      } 
      paramStringBuilder.insert(paramStringBuilder.length() / 2, "...");
    } 
  }
  
  public static String rewriteAfterRename(File paramFile1, File paramFile2, String paramString) {
    String str;
    File file = null;
    if (paramString == null)
      return null; 
    paramFile2 = rewriteAfterRename(paramFile1, paramFile2, new File(paramString));
    paramFile1 = file;
    if (paramFile2 != null)
      str = paramFile2.getAbsolutePath(); 
    return str;
  }
  
  public static String[] rewriteAfterRename(File paramFile1, File paramFile2, String[] paramArrayOfString) {
    if (paramArrayOfString == null)
      return null; 
    String[] arrayOfString = new String[paramArrayOfString.length];
    for (byte b = 0; b < paramArrayOfString.length; b++)
      arrayOfString[b] = rewriteAfterRename(paramFile1, paramFile2, paramArrayOfString[b]); 
    return arrayOfString;
  }
  
  public static File rewriteAfterRename(File paramFile1, File paramFile2, File paramFile3) {
    if (paramFile3 == null || paramFile1 == null || paramFile2 == null)
      return null; 
    if (contains(paramFile1, paramFile3)) {
      String str2 = paramFile3.getAbsolutePath();
      int i = paramFile1.getAbsolutePath().length();
      String str1 = str2.substring(i);
      return new File(paramFile2, str1);
    } 
    return null;
  }
  
  private static File buildUniqueFileWithExtension(File paramFile, String paramString1, String paramString2) throws FileNotFoundException {
    File file = buildFile(paramFile, paramString1, paramString2);
    int i = 0;
    while (file.exists()) {
      int j = i + 1;
      if (i < 32) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramString1);
        stringBuilder.append(" (");
        stringBuilder.append(j);
        stringBuilder.append(")");
        file = buildFile(paramFile, stringBuilder.toString(), paramString2);
        i = j;
        continue;
      } 
      throw new FileNotFoundException("Failed to create unique file");
    } 
    return file;
  }
  
  public static File buildUniqueFile(File paramFile, String paramString1, String paramString2) throws FileNotFoundException {
    String[] arrayOfString = splitFileName(paramString1, paramString2);
    return buildUniqueFileWithExtension(paramFile, arrayOfString[0], arrayOfString[1]);
  }
  
  public static File buildNonUniqueFile(File paramFile, String paramString1, String paramString2) {
    String[] arrayOfString = splitFileName(paramString1, paramString2);
    return buildFile(paramFile, arrayOfString[0], arrayOfString[1]);
  }
  
  public static File buildUniqueFile(File paramFile, String paramString) throws FileNotFoundException {
    String str;
    int i = paramString.lastIndexOf('.');
    if (i >= 0) {
      String str1 = paramString.substring(0, i);
      str = paramString.substring(i + 1);
      paramString = str1;
    } else {
      str = null;
    } 
    return buildUniqueFileWithExtension(paramFile, paramString, str);
  }
  
  public static String[] splitFileName(String paramString1, String paramString2) {
    if ("vnd.android.document/directory".equals(paramString1)) {
      paramString1 = null;
    } else {
      String str1, str2, str3;
      int i = paramString2.lastIndexOf('.');
      if (i >= 0) {
        str1 = paramString2.substring(0, i);
        str2 = paramString2.substring(i + 1);
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        String str5 = str2.toLowerCase();
        str3 = mimeTypeMap.getMimeTypeFromExtension(str5);
      } else {
        str1 = paramString2;
        str2 = null;
        str3 = null;
      } 
      String str4 = str3;
      if (str3 == null)
        str4 = "application/octet-stream"; 
      if ("application/octet-stream".equals(paramString1)) {
        str3 = null;
      } else {
        str3 = MimeTypeMap.getSingleton().getExtensionFromMimeType(paramString1);
      } 
      if (Objects.equals(paramString1, str4) || Objects.equals(str2, str3)) {
        paramString2 = str1;
        paramString1 = str2;
      } else {
        paramString1 = str3;
      } 
    } 
    String str = paramString1;
    if (paramString1 == null)
      str = ""; 
    return new String[] { paramString2, str };
  }
  
  private static File buildFile(File paramFile, String paramString1, String paramString2) {
    if (TextUtils.isEmpty(paramString2))
      return new File(paramFile, paramString1); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append(".");
    stringBuilder.append(paramString2);
    return new File(paramFile, stringBuilder.toString());
  }
  
  public static String[] listOrEmpty(File paramFile) {
    String[] arrayOfString;
    if (paramFile != null) {
      arrayOfString = ArrayUtils.defeatNullable(paramFile.list());
    } else {
      arrayOfString = EmptyArray.STRING;
    } 
    return arrayOfString;
  }
  
  public static File[] listFilesOrEmpty(File paramFile) {
    File[] arrayOfFile;
    if (paramFile != null) {
      arrayOfFile = ArrayUtils.defeatNullable(paramFile.listFiles());
    } else {
      arrayOfFile = ArrayUtils.EMPTY_FILE;
    } 
    return arrayOfFile;
  }
  
  public static File[] listFilesOrEmpty(File paramFile, FilenameFilter paramFilenameFilter) {
    File[] arrayOfFile;
    if (paramFile != null) {
      arrayOfFile = ArrayUtils.defeatNullable(paramFile.listFiles(paramFilenameFilter));
    } else {
      arrayOfFile = ArrayUtils.EMPTY_FILE;
    } 
    return arrayOfFile;
  }
  
  public static File newFileOrNull(String paramString) {
    if (paramString != null) {
      File file = new File(paramString);
    } else {
      paramString = null;
    } 
    return (File)paramString;
  }
  
  public static File createDir(File paramFile, String paramString) {
    paramFile = new File(paramFile, paramString);
    if (!createDir(paramFile))
      paramFile = null; 
    return paramFile;
  }
  
  public static boolean createDir(File paramFile) {
    if (paramFile.exists())
      return paramFile.isDirectory(); 
    return paramFile.mkdir();
  }
  
  public static long roundStorageSize(long paramLong) {
    long l1 = 1L;
    long l2 = 1L;
    while (l1 * l2 < paramLong) {
      long l = l1 << 1L;
      l1 = l;
      if (l > 512L) {
        l1 = 1L;
        l2 *= 1000L;
      } 
    } 
    return l1 * l2;
  }
  
  @Deprecated
  public static void closeQuietly(AutoCloseable paramAutoCloseable) {
    IoUtils.closeQuietly(paramAutoCloseable);
  }
  
  @Deprecated
  public static void closeQuietly(FileDescriptor paramFileDescriptor) {
    IoUtils.closeQuietly(paramFileDescriptor);
  }
  
  public static int translateModeStringToPosix(String paramString) {
    int i;
    for (i = 0; i < paramString.length(); ) {
      char c = paramString.charAt(i);
      if (c == 'a' || c == 'r' || c == 't' || c == 'w') {
        i++;
        continue;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Bad mode: ");
      stringBuilder.append(paramString);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    if (paramString.startsWith("rw")) {
      i = OsConstants.O_RDWR | OsConstants.O_CREAT;
    } else if (paramString.startsWith("w")) {
      i = OsConstants.O_WRONLY | OsConstants.O_CREAT;
    } else if (paramString.startsWith("r")) {
      i = OsConstants.O_RDONLY;
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Bad mode: ");
      stringBuilder.append(paramString);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    int j = i;
    if (paramString.indexOf('t') != -1)
      j = i | OsConstants.O_TRUNC; 
    i = j;
    if (paramString.indexOf('a') != -1)
      i = j | OsConstants.O_APPEND; 
    return i;
  }
  
  public static String translateModePosixToString(int paramInt) {
    StringBuilder stringBuilder1;
    String str2;
    if ((OsConstants.O_ACCMODE & paramInt) == OsConstants.O_RDWR) {
      String str = "rw";
    } else if ((OsConstants.O_ACCMODE & paramInt) == OsConstants.O_WRONLY) {
      String str = "w";
    } else if ((OsConstants.O_ACCMODE & paramInt) == OsConstants.O_RDONLY) {
      String str = "r";
    } else {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Bad mode: ");
      stringBuilder1.append(paramInt);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder2 = stringBuilder1;
    if ((OsConstants.O_TRUNC & paramInt) == OsConstants.O_TRUNC) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append((String)stringBuilder1);
      stringBuilder2.append("t");
      str2 = stringBuilder2.toString();
    } 
    String str1 = str2;
    if ((OsConstants.O_APPEND & paramInt) == OsConstants.O_APPEND) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str2);
      stringBuilder.append("a");
      str1 = stringBuilder.toString();
    } 
    return str1;
  }
  
  public static int translateModePosixToPfd(int paramInt) {
    if ((OsConstants.O_ACCMODE & paramInt) == OsConstants.O_RDWR) {
      i = 805306368;
    } else if ((OsConstants.O_ACCMODE & paramInt) == OsConstants.O_WRONLY) {
      i = 536870912;
    } else if ((OsConstants.O_ACCMODE & paramInt) == OsConstants.O_RDONLY) {
      i = 268435456;
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Bad mode: ");
      stringBuilder.append(paramInt);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    int j = i;
    if ((OsConstants.O_CREAT & paramInt) == OsConstants.O_CREAT)
      j = i | 0x8000000; 
    int i = j;
    if ((OsConstants.O_TRUNC & paramInt) == OsConstants.O_TRUNC)
      i = j | 0x4000000; 
    j = i;
    if ((OsConstants.O_APPEND & paramInt) == OsConstants.O_APPEND)
      j = i | 0x2000000; 
    return j;
  }
  
  public static int translateModePfdToPosix(int paramInt) {
    if ((paramInt & 0x30000000) == 805306368) {
      i = OsConstants.O_RDWR;
    } else if ((paramInt & 0x20000000) == 536870912) {
      i = OsConstants.O_WRONLY;
    } else if ((paramInt & 0x10000000) == 268435456) {
      i = OsConstants.O_RDONLY;
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Bad mode: ");
      stringBuilder.append(paramInt);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    int j = i;
    if ((paramInt & 0x8000000) == 134217728)
      j = i | OsConstants.O_CREAT; 
    int i = j;
    if ((paramInt & 0x4000000) == 67108864)
      i = j | OsConstants.O_TRUNC; 
    j = i;
    if ((paramInt & 0x2000000) == 33554432)
      j = i | OsConstants.O_APPEND; 
    return j;
  }
  
  public static int translateModeAccessToPosix(int paramInt) {
    if (paramInt == OsConstants.F_OK)
      return OsConstants.O_RDONLY; 
    if (((OsConstants.R_OK | OsConstants.W_OK) & paramInt) == (OsConstants.R_OK | OsConstants.W_OK))
      return OsConstants.O_RDWR; 
    if ((OsConstants.R_OK & paramInt) == OsConstants.R_OK)
      return OsConstants.O_RDONLY; 
    if ((OsConstants.W_OK & paramInt) == OsConstants.W_OK)
      return OsConstants.O_WRONLY; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad mode: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static class MemoryPipe extends Thread implements AutoCloseable {
    private final byte[] data;
    
    private final FileDescriptor[] pipe;
    
    private final boolean sink;
    
    private MemoryPipe(byte[] param1ArrayOfbyte, boolean param1Boolean) throws IOException {
      try {
        this.pipe = Os.pipe();
        this.data = param1ArrayOfbyte;
        this.sink = param1Boolean;
        return;
      } catch (ErrnoException errnoException) {
        throw errnoException.rethrowAsIOException();
      } 
    }
    
    private MemoryPipe startInternal() {
      start();
      return this;
    }
    
    public static MemoryPipe createSource(byte[] param1ArrayOfbyte) throws IOException {
      return (new MemoryPipe(param1ArrayOfbyte, false)).startInternal();
    }
    
    public static MemoryPipe createSink(byte[] param1ArrayOfbyte) throws IOException {
      return (new MemoryPipe(param1ArrayOfbyte, true)).startInternal();
    }
    
    public FileDescriptor getFD() {
      FileDescriptor fileDescriptor;
      if (this.sink) {
        fileDescriptor = this.pipe[1];
      } else {
        fileDescriptor = this.pipe[0];
      } 
      return fileDescriptor;
    }
    
    public FileDescriptor getInternalFD() {
      FileDescriptor fileDescriptor;
      if (this.sink) {
        fileDescriptor = this.pipe[0];
      } else {
        fileDescriptor = this.pipe[1];
      } 
      return fileDescriptor;
    }
    
    public void run() {
      // Byte code:
      //   0: aload_0
      //   1: invokevirtual getInternalFD : ()Ljava/io/FileDescriptor;
      //   4: astore_1
      //   5: iconst_0
      //   6: istore_2
      //   7: iload_2
      //   8: aload_0
      //   9: getfield data : [B
      //   12: arraylength
      //   13: if_icmpge -> 69
      //   16: aload_0
      //   17: getfield sink : Z
      //   20: ifeq -> 45
      //   23: iload_2
      //   24: aload_1
      //   25: aload_0
      //   26: getfield data : [B
      //   29: iload_2
      //   30: aload_0
      //   31: getfield data : [B
      //   34: arraylength
      //   35: iload_2
      //   36: isub
      //   37: invokestatic read : (Ljava/io/FileDescriptor;[BII)I
      //   40: iadd
      //   41: istore_2
      //   42: goto -> 7
      //   45: aload_1
      //   46: aload_0
      //   47: getfield data : [B
      //   50: iload_2
      //   51: aload_0
      //   52: getfield data : [B
      //   55: arraylength
      //   56: iload_2
      //   57: isub
      //   58: invokestatic write : (Ljava/io/FileDescriptor;[BII)I
      //   61: istore_3
      //   62: iload_2
      //   63: iload_3
      //   64: iadd
      //   65: istore_2
      //   66: goto -> 7
      //   69: aload_0
      //   70: getfield sink : Z
      //   73: ifeq -> 124
      //   76: goto -> 114
      //   79: astore #4
      //   81: aload_0
      //   82: getfield sink : Z
      //   85: ifeq -> 98
      //   88: getstatic java/util/concurrent/TimeUnit.SECONDS : Ljava/util/concurrent/TimeUnit;
      //   91: lconst_1
      //   92: invokevirtual toMillis : (J)J
      //   95: invokestatic sleep : (J)V
      //   98: aload_1
      //   99: invokestatic closeQuietly : (Ljava/io/FileDescriptor;)V
      //   102: aload #4
      //   104: athrow
      //   105: astore #4
      //   107: aload_0
      //   108: getfield sink : Z
      //   111: ifeq -> 124
      //   114: getstatic java/util/concurrent/TimeUnit.SECONDS : Ljava/util/concurrent/TimeUnit;
      //   117: lconst_1
      //   118: invokevirtual toMillis : (J)J
      //   121: invokestatic sleep : (J)V
      //   124: aload_1
      //   125: invokestatic closeQuietly : (Ljava/io/FileDescriptor;)V
      //   128: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1467	-> 0
      //   #1469	-> 5
      //   #1470	-> 7
      //   #1471	-> 16
      //   #1472	-> 23
      //   #1474	-> 45
      //   #1480	-> 69
      //   #1481	-> 76
      //   #1480	-> 79
      //   #1481	-> 88
      //   #1483	-> 98
      //   #1484	-> 102
      //   #1477	-> 105
      //   #1480	-> 107
      //   #1481	-> 114
      //   #1483	-> 124
      //   #1484	-> 128
      //   #1485	-> 128
      // Exception table:
      //   from	to	target	type
      //   7	16	105	java/io/IOException
      //   7	16	105	android/system/ErrnoException
      //   7	16	79	finally
      //   16	23	105	java/io/IOException
      //   16	23	105	android/system/ErrnoException
      //   16	23	79	finally
      //   23	42	105	java/io/IOException
      //   23	42	105	android/system/ErrnoException
      //   23	42	79	finally
      //   45	62	105	java/io/IOException
      //   45	62	105	android/system/ErrnoException
      //   45	62	79	finally
    }
    
    public void close() throws Exception {
      IoUtils.closeQuietly(getFD());
    }
  }
  
  public static interface ProgressListener {
    void onProgress(long param1Long);
  }
}
