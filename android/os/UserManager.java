package android.os;

import android.accounts.AccountManager;
import android.annotation.SystemApi;
import android.app.ActivityManager;
import android.app.IActivityManager;
import android.app.PropertyInvalidatedCache;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.pm.UserInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.AndroidException;
import com.android.internal.os.RoSystemProperties;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class UserManager extends OplusBaseUserManager {
  private static final String ACTION_CREATE_USER = "android.os.action.CREATE_USER";
  
  @SystemApi
  public static final String ACTION_USER_RESTRICTIONS_CHANGED = "android.os.action.USER_RESTRICTIONS_CHANGED";
  
  public static final String ALLOW_PARENT_PROFILE_APP_LINKING = "allow_parent_profile_app_linking";
  
  private static final String CACHE_KEY_IS_USER_UNLOCKED_PROPERTY = "cache_key.is_user_unlocked";
  
  @Deprecated
  public static final String DISALLOW_ADD_MANAGED_PROFILE = "no_add_managed_profile";
  
  public static final String DISALLOW_ADD_USER = "no_add_user";
  
  public static final String DISALLOW_ADJUST_VOLUME = "no_adjust_volume";
  
  public static final String DISALLOW_AIRPLANE_MODE = "no_airplane_mode";
  
  public static final String DISALLOW_AMBIENT_DISPLAY = "no_ambient_display";
  
  public static final String DISALLOW_APPS_CONTROL = "no_control_apps";
  
  public static final String DISALLOW_AUTOFILL = "no_autofill";
  
  public static final String DISALLOW_BLUETOOTH = "no_bluetooth";
  
  public static final String DISALLOW_BLUETOOTH_SHARING = "no_bluetooth_sharing";
  
  public static final String DISALLOW_CAMERA = "no_camera";
  
  public static final String DISALLOW_CONFIG_BLUETOOTH = "no_config_bluetooth";
  
  public static final String DISALLOW_CONFIG_BRIGHTNESS = "no_config_brightness";
  
  public static final String DISALLOW_CONFIG_CELL_BROADCASTS = "no_config_cell_broadcasts";
  
  public static final String DISALLOW_CONFIG_CREDENTIALS = "no_config_credentials";
  
  public static final String DISALLOW_CONFIG_DATE_TIME = "no_config_date_time";
  
  public static final String DISALLOW_CONFIG_LOCALE = "no_config_locale";
  
  public static final String DISALLOW_CONFIG_LOCATION = "no_config_location";
  
  public static final String DISALLOW_CONFIG_MOBILE_NETWORKS = "no_config_mobile_networks";
  
  public static final String DISALLOW_CONFIG_PRIVATE_DNS = "disallow_config_private_dns";
  
  public static final String DISALLOW_CONFIG_SCREEN_TIMEOUT = "no_config_screen_timeout";
  
  public static final String DISALLOW_CONFIG_TETHERING = "no_config_tethering";
  
  public static final String DISALLOW_CONFIG_VPN = "no_config_vpn";
  
  public static final String DISALLOW_CONFIG_WIFI = "no_config_wifi";
  
  public static final String DISALLOW_CONTENT_CAPTURE = "no_content_capture";
  
  public static final String DISALLOW_CONTENT_SUGGESTIONS = "no_content_suggestions";
  
  public static final String DISALLOW_CREATE_WINDOWS = "no_create_windows";
  
  public static final String DISALLOW_CROSS_PROFILE_COPY_PASTE = "no_cross_profile_copy_paste";
  
  public static final String DISALLOW_DATA_ROAMING = "no_data_roaming";
  
  public static final String DISALLOW_DEBUGGING_FEATURES = "no_debugging_features";
  
  public static final String DISALLOW_FACTORY_RESET = "no_factory_reset";
  
  public static final String DISALLOW_FUN = "no_fun";
  
  public static final String DISALLOW_INSTALL_APPS = "no_install_apps";
  
  public static final String DISALLOW_INSTALL_UNKNOWN_SOURCES = "no_install_unknown_sources";
  
  public static final String DISALLOW_INSTALL_UNKNOWN_SOURCES_GLOBALLY = "no_install_unknown_sources_globally";
  
  public static final String DISALLOW_MODIFY_ACCOUNTS = "no_modify_accounts";
  
  public static final String DISALLOW_MOUNT_PHYSICAL_MEDIA = "no_physical_media";
  
  public static final String DISALLOW_NETWORK_RESET = "no_network_reset";
  
  @SystemApi
  @Deprecated
  public static final String DISALLOW_OEM_UNLOCK = "no_oem_unlock";
  
  public static final String DISALLOW_OUTGOING_BEAM = "no_outgoing_beam";
  
  public static final String DISALLOW_OUTGOING_CALLS = "no_outgoing_calls";
  
  public static final String DISALLOW_PRINTING = "no_printing";
  
  public static final String DISALLOW_RECORD_AUDIO = "no_record_audio";
  
  @Deprecated
  public static final String DISALLOW_REMOVE_MANAGED_PROFILE = "no_remove_managed_profile";
  
  public static final String DISALLOW_REMOVE_USER = "no_remove_user";
  
  @SystemApi
  public static final String DISALLOW_RUN_IN_BACKGROUND = "no_run_in_background";
  
  public static final String DISALLOW_SAFE_BOOT = "no_safe_boot";
  
  public static final String DISALLOW_SET_USER_ICON = "no_set_user_icon";
  
  public static final String DISALLOW_SET_WALLPAPER = "no_set_wallpaper";
  
  public static final String DISALLOW_SHARE_INTO_MANAGED_PROFILE = "no_sharing_into_profile";
  
  public static final String DISALLOW_SHARE_LOCATION = "no_share_location";
  
  public static final String DISALLOW_SMS = "no_sms";
  
  public static final String DISALLOW_SYSTEM_ERROR_DIALOGS = "no_system_error_dialogs";
  
  public static final String DISALLOW_UNIFIED_PASSWORD = "no_unified_password";
  
  public static final String DISALLOW_UNINSTALL_APPS = "no_uninstall_apps";
  
  public static final String DISALLOW_UNMUTE_DEVICE = "disallow_unmute_device";
  
  public static final String DISALLOW_UNMUTE_MICROPHONE = "no_unmute_microphone";
  
  public static final String DISALLOW_USB_FILE_TRANSFER = "no_usb_file_transfer";
  
  public static final String DISALLOW_USER_SWITCH = "no_user_switch";
  
  public static final String DISALLOW_WALLPAPER = "no_wallpaper";
  
  public static final String ENSURE_VERIFY_APPS = "ensure_verify_apps";
  
  public static final String EXTRA_USER_ACCOUNT_NAME = "android.os.extra.USER_ACCOUNT_NAME";
  
  public static final String EXTRA_USER_ACCOUNT_OPTIONS = "android.os.extra.USER_ACCOUNT_OPTIONS";
  
  public static final String EXTRA_USER_ACCOUNT_TYPE = "android.os.extra.USER_ACCOUNT_TYPE";
  
  public static final String EXTRA_USER_NAME = "android.os.extra.USER_NAME";
  
  public static final String KEY_RESTRICTIONS_PENDING = "restrictions_pending";
  
  public static final int PIN_VERIFICATION_FAILED_INCORRECT = -3;
  
  public static final int PIN_VERIFICATION_FAILED_NOT_SET = -2;
  
  public static final int PIN_VERIFICATION_SUCCESS = -1;
  
  public static final int QUIET_MODE_DISABLE_DONT_ASK_CREDENTIAL = 2;
  
  public static final int QUIET_MODE_DISABLE_ONLY_IF_CREDENTIAL_NOT_REQUIRED = 1;
  
  @SystemApi
  public static final int RESTRICTION_NOT_SET = 0;
  
  @SystemApi
  public static final int RESTRICTION_SOURCE_DEVICE_OWNER = 2;
  
  @SystemApi
  public static final int RESTRICTION_SOURCE_PROFILE_OWNER = 4;
  
  @SystemApi
  public static final int RESTRICTION_SOURCE_SYSTEM = 1;
  
  @SystemApi
  public static final int SWITCHABILITY_STATUS_OK = 0;
  
  @SystemApi
  public static final int SWITCHABILITY_STATUS_SYSTEM_USER_LOCKED = 4;
  
  @SystemApi
  public static final int SWITCHABILITY_STATUS_USER_IN_CALL = 1;
  
  @SystemApi
  public static final int SWITCHABILITY_STATUS_USER_SWITCH_DISALLOWED = 2;
  
  private static final String TAG = "UserManager";
  
  public static final int USER_CREATION_FAILED_NOT_PERMITTED = 1;
  
  public static final int USER_CREATION_FAILED_NO_MORE_USERS = 2;
  
  public static final int USER_OPERATION_ERROR_CURRENT_USER = 4;
  
  public static final int USER_OPERATION_ERROR_LOW_STORAGE = 5;
  
  public static final int USER_OPERATION_ERROR_MANAGED_PROFILE = 2;
  
  public static final int USER_OPERATION_ERROR_MAX_RUNNING_USERS = 3;
  
  public static final int USER_OPERATION_ERROR_MAX_USERS = 6;
  
  public static final int USER_OPERATION_ERROR_UNKNOWN = 1;
  
  public static final int USER_OPERATION_SUCCESS = 0;
  
  public static final String USER_TYPE_FULL_DEMO = "android.os.usertype.full.DEMO";
  
  public static final String USER_TYPE_FULL_GUEST = "android.os.usertype.full.GUEST";
  
  public static final String USER_TYPE_FULL_RESTRICTED = "android.os.usertype.full.RESTRICTED";
  
  @SystemApi
  public static final String USER_TYPE_FULL_SECONDARY = "android.os.usertype.full.SECONDARY";
  
  @SystemApi
  public static final String USER_TYPE_FULL_SYSTEM = "android.os.usertype.full.SYSTEM";
  
  @SystemApi
  public static final String USER_TYPE_PROFILE_MANAGED = "android.os.usertype.profile.MANAGED";
  
  @SystemApi
  public static final String USER_TYPE_SYSTEM_HEADLESS = "android.os.usertype.system.HEADLESS";
  
  private final Context mContext;
  
  private Boolean mIsManagedProfileCached;
  
  private Boolean mIsProfileCached;
  
  private final PropertyInvalidatedCache<Integer, Boolean> mIsUserUnlockedCache;
  
  private final PropertyInvalidatedCache<Integer, Boolean> mIsUserUnlockingOrUnlockedCache;
  
  private final IUserManager mService;
  
  private final int mUserId;
  
  class UserOperationException extends RuntimeException {
    private final int mUserOperationResult;
    
    public UserOperationException(UserManager this$0, int param1Int) {
      super((String)this$0);
      this.mUserOperationResult = param1Int;
    }
    
    public int getUserOperationResult() {
      return this.mUserOperationResult;
    }
    
    public static UserOperationException from(ServiceSpecificException param1ServiceSpecificException) {
      return new UserOperationException(param1ServiceSpecificException.getMessage(), param1ServiceSpecificException.errorCode);
    }
  }
  
  private <T> T returnNullOrThrowUserOperationException(ServiceSpecificException paramServiceSpecificException, boolean paramBoolean) throws UserOperationException {
    if (!paramBoolean)
      return null; 
    throw UserOperationException.from(paramServiceSpecificException);
  }
  
  public static class CheckedUserOperationException extends AndroidException {
    private final int mUserOperationResult;
    
    public CheckedUserOperationException(String param1String, int param1Int) {
      super(param1String);
      this.mUserOperationResult = param1Int;
    }
    
    public int getUserOperationResult() {
      return this.mUserOperationResult;
    }
    
    public ServiceSpecificException toServiceSpecificException() {
      return new ServiceSpecificException(this.mUserOperationResult, getMessage());
    }
  }
  
  public static UserManager get(Context paramContext) {
    return (UserManager)paramContext.getSystemService("user");
  }
  
  public UserManager(Context paramContext, IUserManager paramIUserManager) {
    this.mIsUserUnlockedCache = new PropertyInvalidatedCache<Integer, Boolean>(32, "cache_key.is_user_unlocked") {
        final UserManager this$0;
        
        protected Boolean recompute(Integer param1Integer) {
          try {
            boolean bool = UserManager.this.mService.isUserUnlocked(param1Integer.intValue());
            return Boolean.valueOf(bool);
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowFromSystemServer();
          } 
        }
      };
    this.mIsUserUnlockingOrUnlockedCache = new PropertyInvalidatedCache<Integer, Boolean>(32, "cache_key.is_user_unlocked") {
        final UserManager this$0;
        
        protected Boolean recompute(Integer param1Integer) {
          try {
            boolean bool = UserManager.this.mService.isUserUnlockingOrUnlocked(param1Integer.intValue());
            return Boolean.valueOf(bool);
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowFromSystemServer();
          } 
        }
      };
    this.mService = paramIUserManager;
    this.mContext = paramContext.getApplicationContext();
    this.mUserId = paramContext.getUserId();
  }
  
  public static boolean supportsMultipleUsers() {
    int i = getMaxSupportedUsers();
    null = true;
    if (i > 1) {
      boolean bool = Resources.getSystem().getBoolean(17891446);
      if (SystemProperties.getBoolean("fw.show_multiuserui", bool))
        return null; 
    } 
    return false;
  }
  
  public static boolean isSplitSystemUser() {
    return RoSystemProperties.FW_SYSTEM_USER_SPLIT;
  }
  
  public static boolean isGuestUserEphemeral() {
    Resources resources = Resources.getSystem();
    return resources.getBoolean(17891468);
  }
  
  public static boolean isHeadlessSystemUserMode() {
    return RoSystemProperties.MULTIUSER_HEADLESS_SYSTEM_USER;
  }
  
  @Deprecated
  public boolean canSwitchUsers() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mContext : Landroid/content/Context;
    //   4: astore_1
    //   5: aload_1
    //   6: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
    //   9: astore_1
    //   10: iconst_0
    //   11: istore_2
    //   12: aload_1
    //   13: ldc_w 'allow_user_switching_when_system_user_locked'
    //   16: iconst_0
    //   17: invokestatic getInt : (Landroid/content/ContentResolver;Ljava/lang/String;I)I
    //   20: ifeq -> 28
    //   23: iconst_1
    //   24: istore_3
    //   25: goto -> 30
    //   28: iconst_0
    //   29: istore_3
    //   30: aload_0
    //   31: getstatic android/os/UserHandle.SYSTEM : Landroid/os/UserHandle;
    //   34: invokevirtual isUserUnlocked : (Landroid/os/UserHandle;)Z
    //   37: istore #4
    //   39: iconst_0
    //   40: istore #5
    //   42: aload_0
    //   43: getfield mContext : Landroid/content/Context;
    //   46: ldc_w android/telephony/TelephonyManager
    //   49: invokevirtual getSystemService : (Ljava/lang/Class;)Ljava/lang/Object;
    //   52: checkcast android/telephony/TelephonyManager
    //   55: astore_1
    //   56: aload_1
    //   57: ifnull -> 76
    //   60: aload_1
    //   61: invokevirtual getCallState : ()I
    //   64: ifeq -> 73
    //   67: iconst_1
    //   68: istore #5
    //   70: goto -> 76
    //   73: iconst_0
    //   74: istore #5
    //   76: aload_0
    //   77: ldc 'no_user_switch'
    //   79: invokevirtual hasUserRestriction : (Ljava/lang/String;)Z
    //   82: istore #6
    //   84: iload_3
    //   85: ifne -> 96
    //   88: iload_2
    //   89: istore #7
    //   91: iload #4
    //   93: ifeq -> 115
    //   96: iload_2
    //   97: istore #7
    //   99: iload #5
    //   101: ifne -> 115
    //   104: iload_2
    //   105: istore #7
    //   107: iload #6
    //   109: ifne -> 115
    //   112: iconst_1
    //   113: istore #7
    //   115: iload #7
    //   117: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1677	-> 0
    //   #1678	-> 5
    //   #1677	-> 10
    //   #1680	-> 30
    //   #1681	-> 39
    //   #1682	-> 42
    //   #1683	-> 56
    //   #1684	-> 60
    //   #1686	-> 76
    //   #1687	-> 84
  }
  
  @SystemApi
  public int getUserSwitchability() {
    return getUserSwitchability(Process.myUserHandle());
  }
  
  public int getUserSwitchability(UserHandle paramUserHandle) {
    Context context = this.mContext;
    TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService("phone");
    int i = 0;
    if (telephonyManager.getCallState() != 0)
      i = false | true; 
    int j = i;
    if (hasUserRestriction("no_user_switch", paramUserHandle))
      j = i | 0x2; 
    i = j;
    if (!isHeadlessSystemUserMode()) {
      Context context1 = this.mContext;
      ContentResolver contentResolver = context1.getContentResolver();
      boolean bool = false;
      if (Settings.Global.getInt(contentResolver, "allow_user_switching_when_system_user_locked", 0) != 0)
        bool = true; 
      boolean bool1 = isUserUnlocked(UserHandle.SYSTEM);
      i = j;
      if (!bool) {
        i = j;
        if (!bool1)
          i = j | 0x4; 
      } 
    } 
    return i;
  }
  
  public int getUserHandle() {
    return UserHandle.myUserId();
  }
  
  public String getUserName() {
    String str;
    int i = UserHandle.myUserId(), j = this.mUserId;
    if (i == j)
      try {
        return this.mService.getUserName();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    UserInfo userInfo = getUserInfo(j);
    if (userInfo == null) {
      str = "";
    } else {
      str = ((UserInfo)str).name;
    } 
    return str;
  }
  
  @SystemApi
  public boolean isUserNameSet() {
    try {
      return this.mService.isUserNameSet(getUserHandle());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isUserAGoat() {
    if ((this.mContext.getApplicationInfo()).targetSdkVersion >= 30)
      return false; 
    PackageManager packageManager = this.mContext.getPackageManager();
    return packageManager.isPackageAvailable("com.coffeestainstudios.goatsimulator");
  }
  
  @SystemApi
  public boolean isPrimaryUser() {
    boolean bool;
    UserInfo userInfo = getUserInfo(UserHandle.myUserId());
    if (userInfo != null && userInfo.isPrimary()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isSystemUser() {
    boolean bool;
    if (UserHandle.myUserId() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @SystemApi
  public boolean isAdminUser() {
    return isUserAdmin(UserHandle.myUserId());
  }
  
  public boolean isUserAdmin(int paramInt) {
    boolean bool;
    UserInfo userInfo = getUserInfo(paramInt);
    if (userInfo != null && userInfo.isAdmin()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @SystemApi
  public boolean isUserOfType(String paramString) {
    try {
      return this.mService.isUserOfType(this.mUserId, paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean isUserTypeManagedProfile(String paramString) {
    return "android.os.usertype.profile.MANAGED".equals(paramString);
  }
  
  public static boolean isUserTypeGuest(String paramString) {
    return "android.os.usertype.full.GUEST".equals(paramString);
  }
  
  public static boolean isUserTypeRestricted(String paramString) {
    return "android.os.usertype.full.RESTRICTED".equals(paramString);
  }
  
  public static boolean isUserTypeDemo(String paramString) {
    return "android.os.usertype.full.DEMO".equals(paramString);
  }
  
  public static int getUserTypeForStatsd(String paramString) {
    byte b;
    switch (paramString.hashCode()) {
      default:
        b = -1;
        break;
      case 1765400260:
        if (paramString.equals("android.os.usertype.full.DEMO")) {
          b = 3;
          break;
        } 
      case 1711075452:
        if (paramString.equals("android.os.usertype.full.RESTRICTED")) {
          b = 4;
          break;
        } 
      case 942013715:
        if (paramString.equals("android.os.usertype.full.SECONDARY")) {
          b = 1;
          break;
        } 
      case 485661392:
        if (paramString.equals("android.os.usertype.full.SYSTEM")) {
          b = 0;
          break;
        } 
      case 34001850:
        if (paramString.equals("android.os.usertype.system.HEADLESS")) {
          b = 6;
          break;
        } 
      case -159818852:
        if (paramString.equals("android.os.usertype.profile.MANAGED")) {
          b = 5;
          break;
        } 
      case -1103927049:
        if (paramString.equals("android.os.usertype.full.GUEST")) {
          b = 2;
          break;
        } 
    } 
    switch (b) {
      default:
        return 0;
      case 6:
        return 7;
      case 5:
        return 6;
      case 4:
        return 5;
      case 3:
        return 4;
      case 2:
        return 3;
      case 1:
        return 2;
      case 0:
        break;
    } 
    return 1;
  }
  
  @Deprecated
  public boolean isLinkedUser() {
    return isRestrictedProfile();
  }
  
  @SystemApi
  public boolean isRestrictedProfile() {
    try {
      return this.mService.isRestricted();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean isRestrictedProfile(UserHandle paramUserHandle) {
    try {
      return this.mService.getUserInfo(paramUserHandle.getIdentifier()).isRestricted();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean canHaveRestrictedProfile(int paramInt) {
    try {
      return this.mService.canHaveRestrictedProfile(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean hasRestrictedProfiles() {
    try {
      return this.mService.hasRestrictedProfiles();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isGuestUser(int paramInt) {
    boolean bool;
    UserInfo userInfo = getUserInfo(paramInt);
    if (userInfo != null && userInfo.isGuest()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @SystemApi
  public boolean isGuestUser() {
    boolean bool;
    UserInfo userInfo = getUserInfo(UserHandle.myUserId());
    if (userInfo != null && userInfo.isGuest()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isDemoUser() {
    try {
      return this.mService.isDemoUser(UserHandle.myUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean isProfile() {
    return isProfile(this.mUserId);
  }
  
  private boolean isProfile(int paramInt) {
    if (paramInt == UserHandle.myUserId()) {
      Boolean bool = this.mIsProfileCached;
      if (bool != null)
        return bool.booleanValue(); 
      try {
        this.mIsProfileCached = bool = Boolean.valueOf(this.mService.isProfile(paramInt));
        return bool.booleanValue();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    try {
      return this.mService.isProfile(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isManagedProfile() {
    Boolean bool = this.mIsManagedProfileCached;
    if (bool != null)
      return bool.booleanValue(); 
    try {
      this.mIsManagedProfileCached = bool = Boolean.valueOf(this.mService.isManagedProfile(UserHandle.myUserId()));
      return bool.booleanValue();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean isManagedProfile(int paramInt) {
    if (paramInt == UserHandle.myUserId())
      return isManagedProfile(); 
    try {
      return this.mService.isManagedProfile(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isEphemeralUser() {
    return isUserEphemeral(UserHandle.myUserId());
  }
  
  public boolean isUserEphemeral(int paramInt) {
    boolean bool;
    UserInfo userInfo = getUserInfo(paramInt);
    if (userInfo != null && userInfo.isEphemeral()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isUserRunning(UserHandle paramUserHandle) {
    return isUserRunning(paramUserHandle.getIdentifier());
  }
  
  public boolean isUserRunning(int paramInt) {
    try {
      return this.mService.isUserRunning(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isUserRunningOrStopping(UserHandle paramUserHandle) {
    try {
      IActivityManager iActivityManager = ActivityManager.getService();
      int i = paramUserHandle.getIdentifier();
      return iActivityManager.isUserRunning(i, 1);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isUserUnlocked() {
    return isUserUnlocked(Process.myUserHandle());
  }
  
  public boolean isUserUnlocked(UserHandle paramUserHandle) {
    return isUserUnlocked(paramUserHandle.getIdentifier());
  }
  
  public boolean isUserUnlocked(int paramInt) {
    return ((Boolean)this.mIsUserUnlockedCache.query(Integer.valueOf(paramInt))).booleanValue();
  }
  
  public void disableIsUserUnlockedCache() {
    this.mIsUserUnlockedCache.disableLocal();
    this.mIsUserUnlockingOrUnlockedCache.disableLocal();
  }
  
  public static final void invalidateIsUserUnlockedCache() {
    PropertyInvalidatedCache.invalidateCache("cache_key.is_user_unlocked");
  }
  
  @SystemApi
  public boolean isUserUnlockingOrUnlocked(UserHandle paramUserHandle) {
    return isUserUnlockingOrUnlocked(paramUserHandle.getIdentifier());
  }
  
  public boolean isUserUnlockingOrUnlocked(int paramInt) {
    return ((Boolean)this.mIsUserUnlockingOrUnlockedCache.query(Integer.valueOf(paramInt))).booleanValue();
  }
  
  public long getUserStartRealtime() {
    try {
      return this.mService.getUserStartRealtime();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public long getUserUnlockRealtime() {
    try {
      return this.mService.getUserUnlockRealtime();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public UserInfo getUserInfo(int paramInt) {
    try {
      return this.mService.getUserInfo(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  @Deprecated
  public int getUserRestrictionSource(String paramString, UserHandle paramUserHandle) {
    try {
      return this.mService.getUserRestrictionSource(paramString, paramUserHandle.getIdentifier());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public List<EnforcingUser> getUserRestrictionSources(String paramString, UserHandle paramUserHandle) {
    try {
      return this.mService.getUserRestrictionSources(paramString, paramUserHandle.getIdentifier());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Bundle getUserRestrictions() {
    return getUserRestrictions(Process.myUserHandle());
  }
  
  public Bundle getUserRestrictions(UserHandle paramUserHandle) {
    try {
      return this.mService.getUserRestrictions(paramUserHandle.getIdentifier());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean hasBaseUserRestriction(String paramString, UserHandle paramUserHandle) {
    try {
      return this.mService.hasBaseUserRestriction(paramString, paramUserHandle.getIdentifier());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void setUserRestrictions(Bundle paramBundle) {
    throw new UnsupportedOperationException("This method is no longer supported");
  }
  
  @Deprecated
  public void setUserRestrictions(Bundle paramBundle, UserHandle paramUserHandle) {
    throw new UnsupportedOperationException("This method is no longer supported");
  }
  
  @Deprecated
  public void setUserRestriction(String paramString, boolean paramBoolean) {
    setUserRestriction(paramString, paramBoolean, Process.myUserHandle());
  }
  
  @Deprecated
  public void setUserRestriction(String paramString, boolean paramBoolean, UserHandle paramUserHandle) {
    try {
      this.mService.setUserRestriction(paramString, paramBoolean, paramUserHandle.getIdentifier());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean hasUserRestriction(String paramString) {
    return hasUserRestrictionForUser(paramString, Process.myUserHandle());
  }
  
  public boolean hasUserRestriction(String paramString, UserHandle paramUserHandle) {
    return hasUserRestrictionForUser(paramString, paramUserHandle);
  }
  
  @SystemApi
  public boolean hasUserRestrictionForUser(String paramString, UserHandle paramUserHandle) {
    try {
      return this.mService.hasUserRestriction(paramString, paramUserHandle.getIdentifier());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean hasUserRestrictionOnAnyUser(String paramString) {
    try {
      return this.mService.hasUserRestrictionOnAnyUser(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isSettingRestrictedForUser(String paramString1, int paramInt1, String paramString2, int paramInt2) {
    try {
      return this.mService.isSettingRestrictedForUser(paramString1, paramInt1, paramString2, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void addUserRestrictionsListener(IUserRestrictionsListener paramIUserRestrictionsListener) {
    try {
      this.mService.addUserRestrictionsListener(paramIUserRestrictionsListener);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public long getSerialNumberForUser(UserHandle paramUserHandle) {
    return getUserSerialNumber(paramUserHandle.getIdentifier());
  }
  
  public UserHandle getUserForSerialNumber(long paramLong) {
    UserHandle userHandle;
    int i = getUserHandle((int)paramLong);
    if (i >= 0) {
      userHandle = new UserHandle(i);
    } else {
      userHandle = null;
    } 
    return userHandle;
  }
  
  @Deprecated
  public UserInfo createUser(String paramString, int paramInt) {
    return createUser(paramString, UserInfo.getDefaultUserType(paramInt), paramInt);
  }
  
  public UserInfo createUser(String paramString1, String paramString2, int paramInt) {
    try {
      return this.mService.createUserWithThrow(paramString1, paramString2, paramInt);
    } catch (ServiceSpecificException serviceSpecificException) {
      return null;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public UserInfo preCreateUser(String paramString) throws UserOperationException {
    try {
      return this.mService.preCreateUserWithThrow(paramString);
    } catch (ServiceSpecificException serviceSpecificException) {
      throw UserOperationException.from(serviceSpecificException);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public UserInfo createGuest(Context paramContext, String paramString) {
    try {
      UserInfo userInfo = this.mService.createUserWithThrow(paramString, "android.os.usertype.full.GUEST", 0);
      if (userInfo != null)
        Settings.Secure.putStringForUser(paramContext.getContentResolver(), "skip_first_use_hints", "1", userInfo.id); 
      return userInfo;
    } catch (ServiceSpecificException serviceSpecificException) {
      return null;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public UserInfo findCurrentGuestUser() {
    try {
      return this.mService.findCurrentGuestUser();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public UserHandle createProfile(String paramString1, String paramString2, Set<String> paramSet) throws UserOperationException {
    try {
      IUserManager iUserManager = this.mService;
      int i = this.mUserId;
      String[] arrayOfString2 = new String[paramSet.size()];
      String[] arrayOfString1 = paramSet.<String>toArray(arrayOfString2);
      UserInfo userInfo = iUserManager.createProfileForUserWithThrow(paramString1, paramString2, 0, i, arrayOfString1);
      return userInfo.getUserHandle();
    } catch (ServiceSpecificException serviceSpecificException) {
      boolean bool;
      Context context = this.mContext;
      if ((context.getApplicationInfo()).targetSdkVersion >= 30) {
        bool = true;
      } else {
        bool = false;
      } 
      return returnNullOrThrowUserOperationException(serviceSpecificException, bool);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public UserInfo createProfileForUser(String paramString, int paramInt1, int paramInt2) {
    return createProfileForUser(paramString, UserInfo.getDefaultUserType(paramInt1), paramInt1, paramInt2, null);
  }
  
  public UserInfo createProfileForUser(String paramString1, String paramString2, int paramInt1, int paramInt2) {
    return createProfileForUser(paramString1, paramString2, paramInt1, paramInt2, null);
  }
  
  public UserInfo createProfileForUser(String paramString1, String paramString2, int paramInt1, int paramInt2, String[] paramArrayOfString) {
    try {
      return this.mService.createProfileForUserWithThrow(paramString1, paramString2, paramInt1, paramInt2, paramArrayOfString);
    } catch (ServiceSpecificException serviceSpecificException) {
      return null;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public UserInfo createProfileForUserEvenWhenDisallowed(String paramString1, String paramString2, int paramInt1, int paramInt2, String[] paramArrayOfString) {
    try {
      return this.mService.createProfileForUserEvenWhenDisallowedWithThrow(paramString1, paramString2, paramInt1, paramInt2, paramArrayOfString);
    } catch (ServiceSpecificException serviceSpecificException) {
      return null;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public UserInfo createRestrictedProfile(String paramString) {
    try {
      UserHandle userHandle = Process.myUserHandle();
      IUserManager iUserManager = this.mService;
      int i = userHandle.getIdentifier();
      UserInfo userInfo = iUserManager.createRestrictedProfileWithThrow(paramString, i);
      if (userInfo != null) {
        AccountManager accountManager = AccountManager.get(this.mContext);
        i = userInfo.id;
        UserHandle userHandle1 = UserHandle.of(i);
        accountManager.addSharedAccountsFromParentUser(userHandle, userHandle1);
      } 
      return userInfo;
    } catch (ServiceSpecificException serviceSpecificException) {
      return null;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static Intent createUserCreationIntent(String paramString1, String paramString2, String paramString3, PersistableBundle paramPersistableBundle) {
    Intent intent = new Intent("android.os.action.CREATE_USER");
    if (paramString1 != null)
      intent.putExtra("android.os.extra.USER_NAME", paramString1); 
    if (paramString2 == null || paramString3 != null) {
      if (paramString2 != null)
        intent.putExtra("android.os.extra.USER_ACCOUNT_NAME", paramString2); 
      if (paramString3 != null)
        intent.putExtra("android.os.extra.USER_ACCOUNT_TYPE", paramString3); 
      if (paramPersistableBundle != null)
        intent.putExtra("android.os.extra.USER_ACCOUNT_OPTIONS", paramPersistableBundle); 
      return intent;
    } 
    throw new IllegalArgumentException("accountType must be specified if accountName is specified");
  }
  
  @SystemApi
  public String getSeedAccountName() {
    try {
      return this.mService.getSeedAccountName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public String getSeedAccountType() {
    try {
      return this.mService.getSeedAccountType();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public PersistableBundle getSeedAccountOptions() {
    try {
      return this.mService.getSeedAccountOptions();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setSeedAccountData(int paramInt, String paramString1, String paramString2, PersistableBundle paramPersistableBundle) {
    try {
      this.mService.setSeedAccountData(paramInt, paramString1, paramString2, paramPersistableBundle, true);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void clearSeedAccountData() {
    try {
      this.mService.clearSeedAccountData();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean markGuestForDeletion(int paramInt) {
    try {
      return this.mService.markGuestForDeletion(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setUserEnabled(int paramInt) {
    try {
      this.mService.setUserEnabled(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setUserAdmin(int paramInt) {
    try {
      this.mService.setUserAdmin(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void evictCredentialEncryptionKey(int paramInt) {
    try {
      this.mService.evictCredentialEncryptionKey(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getUserCount() {
    boolean bool;
    List<UserInfo> list = getUsers();
    if (list != null) {
      bool = list.size();
    } else {
      bool = true;
    } 
    return bool;
  }
  
  public List<UserInfo> getUsers() {
    return getUsers(false);
  }
  
  public List<UserInfo> getUsers(boolean paramBoolean) {
    return getUsers(true, paramBoolean, true);
  }
  
  public List<UserInfo> getUsers(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    try {
      return this.mService.getUsers(paramBoolean1, paramBoolean2, paramBoolean3);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public List<UserHandle> getUserHandles(boolean paramBoolean) {
    List<UserInfo> list = getUsers(paramBoolean);
    ArrayList<UserHandle> arrayList = new ArrayList(list.size());
    for (UserInfo userInfo : list)
      arrayList.add(userInfo.getUserHandle()); 
    return arrayList;
  }
  
  @SystemApi
  public long[] getSerialNumbersOfUsers(boolean paramBoolean) {
    List<UserInfo> list = getUsers(paramBoolean);
    long[] arrayOfLong = new long[list.size()];
    for (byte b = 0; b < arrayOfLong.length; b++)
      arrayOfLong[b] = ((UserInfo)list.get(b)).serialNumber; 
    return arrayOfLong;
  }
  
  public String getUserAccount(int paramInt) {
    try {
      return this.mService.getUserAccount(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setUserAccount(int paramInt, String paramString) {
    try {
      this.mService.setUserAccount(paramInt, paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public UserInfo getPrimaryUser() {
    try {
      return this.mService.getPrimaryUser();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean canAddMoreUsers() {
    boolean bool = true;
    List<UserInfo> list = getUsers(true);
    int i = list.size();
    int j = 0;
    for (byte b = 0; b < i; b++, j = k) {
      int k;
      UserInfo userInfo = list.get(b);
      if (999 == userInfo.id) {
        k = j;
      } else {
        k = j;
        if (!userInfo.isGuest())
          k = j + 1; 
      } 
    } 
    if (j >= getMaxSupportedUsers())
      bool = false; 
    return bool;
  }
  
  public boolean canAddMoreManagedProfiles(int paramInt, boolean paramBoolean) {
    try {
      return this.mService.canAddMoreManagedProfiles(paramInt, paramBoolean);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean canAddMoreProfilesToUser(String paramString, int paramInt) {
    try {
      return this.mService.canAddMoreProfilesToUser(paramString, paramInt, false);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<UserInfo> getProfiles(int paramInt) {
    try {
      return this.mService.getProfiles(paramInt, false);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean isSameProfileGroup(UserHandle paramUserHandle1, UserHandle paramUserHandle2) {
    return isSameProfileGroup(paramUserHandle1.getIdentifier(), paramUserHandle2.getIdentifier());
  }
  
  public boolean isSameProfileGroup(int paramInt1, int paramInt2) {
    try {
      return this.mService.isSameProfileGroup(paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<UserInfo> getEnabledProfiles(int paramInt) {
    try {
      return this.mService.getProfiles(paramInt, true);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<UserHandle> getUserProfiles() {
    int[] arrayOfInt = getProfileIds(UserHandle.myUserId(), true);
    ArrayList<UserHandle> arrayList = new ArrayList(arrayOfInt.length);
    int i;
    byte b;
    for (i = arrayOfInt.length, b = 0; b < i; ) {
      int j = arrayOfInt[b];
      arrayList.add(UserHandle.of(j));
      b++;
    } 
    return arrayList;
  }
  
  @SystemApi
  public List<UserHandle> getEnabledProfiles() {
    return getProfiles(true);
  }
  
  @SystemApi
  public List<UserHandle> getAllProfiles() {
    return getProfiles(false);
  }
  
  private List<UserHandle> getProfiles(boolean paramBoolean) {
    int[] arrayOfInt = getProfileIds(this.mUserId, paramBoolean);
    ArrayList<UserHandle> arrayList = new ArrayList(arrayOfInt.length);
    int i;
    byte b;
    for (i = arrayOfInt.length, b = 0; b < i; ) {
      int j = arrayOfInt[b];
      arrayList.add(UserHandle.of(j));
      b++;
    } 
    return arrayList;
  }
  
  public int[] getProfileIds(int paramInt, boolean paramBoolean) {
    try {
      return this.mService.getProfileIds(paramInt, paramBoolean);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int[] getProfileIdsWithDisabled(int paramInt) {
    return getProfileIds(paramInt, false);
  }
  
  public int[] getEnabledProfileIds(int paramInt) {
    return getProfileIds(paramInt, true);
  }
  
  public int getCredentialOwnerProfile(int paramInt) {
    try {
      return this.mService.getCredentialOwnerProfile(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public UserInfo getProfileParent(int paramInt) {
    try {
      return this.mService.getProfileParent(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public UserHandle getProfileParent(UserHandle paramUserHandle) {
    UserInfo userInfo = getProfileParent(paramUserHandle.getIdentifier());
    if (userInfo == null)
      return null; 
    return UserHandle.of(userInfo.id);
  }
  
  public boolean requestQuietModeEnabled(boolean paramBoolean, UserHandle paramUserHandle) {
    return requestQuietModeEnabled(paramBoolean, paramUserHandle, (IntentSender)null);
  }
  
  public boolean requestQuietModeEnabled(boolean paramBoolean, UserHandle paramUserHandle, int paramInt) {
    return requestQuietModeEnabled(paramBoolean, paramUserHandle, null, paramInt);
  }
  
  public boolean requestQuietModeEnabled(boolean paramBoolean, UserHandle paramUserHandle, IntentSender paramIntentSender) {
    return requestQuietModeEnabled(paramBoolean, paramUserHandle, paramIntentSender, 0);
  }
  
  public boolean requestQuietModeEnabled(boolean paramBoolean, UserHandle paramUserHandle, IntentSender paramIntentSender, int paramInt) {
    try {
      IUserManager iUserManager = this.mService;
      Context context = this.mContext;
      String str = context.getPackageName();
      int i = paramUserHandle.getIdentifier();
      return iUserManager.requestQuietModeEnabled(str, paramBoolean, i, paramIntentSender, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isQuietModeEnabled(UserHandle paramUserHandle) {
    try {
      return this.mService.isQuietModeEnabled(paramUserHandle.getIdentifier());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean hasBadge(int paramInt) {
    if (!isProfile(paramInt))
      return false; 
    try {
      return this.mService.hasBadge(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean hasBadge() {
    return hasBadge(this.mUserId);
  }
  
  public int getUserBadgeColor(int paramInt) {
    try {
      paramInt = this.mService.getUserBadgeColorResId(paramInt);
      return Resources.getSystem().getColor(paramInt, null);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getUserBadgeDarkColor(int paramInt) {
    try {
      paramInt = this.mService.getUserBadgeDarkColorResId(paramInt);
      return Resources.getSystem().getColor(paramInt, null);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getUserIconBadgeResId(int paramInt) {
    try {
      return this.mService.getUserIconBadgeResId(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getUserBadgeResId(int paramInt) {
    try {
      return this.mService.getUserBadgeResId(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getUserBadgeNoBackgroundResId(int paramInt) {
    try {
      return this.mService.getUserBadgeNoBackgroundResId(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Drawable getBadgedIconForUser(Drawable paramDrawable, UserHandle paramUserHandle) {
    return this.mContext.getPackageManager().getUserBadgedIcon(paramDrawable, paramUserHandle);
  }
  
  public Drawable getBadgedDrawableForUser(Drawable paramDrawable, UserHandle paramUserHandle, Rect paramRect, int paramInt) {
    return this.mContext.getPackageManager().getUserBadgedDrawableForDensity(paramDrawable, paramUserHandle, paramRect, paramInt);
  }
  
  public CharSequence getBadgedLabelForUser(CharSequence paramCharSequence, UserHandle paramUserHandle) {
    int i = paramUserHandle.getIdentifier();
    if (!hasBadge(i))
      return paramCharSequence; 
    try {
      i = this.mService.getUserBadgeLabelResId(i);
      return Resources.getSystem().getString(i, new Object[] { paramCharSequence });
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean removeUser(int paramInt) {
    try {
      return this.mService.removeUser(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean removeUser(UserHandle paramUserHandle) {
    if (paramUserHandle != null)
      return removeUser(paramUserHandle.getIdentifier()); 
    throw new IllegalArgumentException("user cannot be null");
  }
  
  public boolean removeUserEvenWhenDisallowed(int paramInt) {
    try {
      return this.mService.removeUserEvenWhenDisallowed(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setUserName(int paramInt, String paramString) {
    try {
      this.mService.setUserName(paramInt, paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void setUserName(String paramString) {
    setUserName(this.mUserId, paramString);
  }
  
  public void setUserIcon(int paramInt, Bitmap paramBitmap) {
    try {
      this.mService.setUserIcon(paramInt, paramBitmap);
      return;
    } catch (ServiceSpecificException serviceSpecificException) {
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void setUserIcon(Bitmap paramBitmap) throws UserOperationException {
    setUserIcon(this.mUserId, paramBitmap);
  }
  
  public Bitmap getUserIcon(int paramInt) {
    try {
      ParcelFileDescriptor parcelFileDescriptor = this.mService.getUserIcon(paramInt);
      if (parcelFileDescriptor != null)
        try {
          return BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.getFileDescriptor());
        } finally {
          try {
            iOException.close();
          } catch (IOException iOException1) {}
        }  
      return null;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public Bitmap getUserIcon() {
    return getUserIcon(this.mUserId);
  }
  
  public static int getMaxSupportedUsers() {
    if (Build.ID.startsWith("JVP"))
      return 1; 
    int i = Resources.getSystem().getInteger(17694849);
    return SystemProperties.getInt("fw.max_users", i);
  }
  
  public boolean isUserSwitcherEnabled() {
    return isUserSwitcherEnabled(true);
  }
  
  public boolean isUserSwitcherEnabled(boolean paramBoolean) {
    boolean bool1 = supportsMultipleUsers();
    boolean bool = false;
    if (!bool1)
      return false; 
    if (hasUserRestriction("no_user_switch"))
      return false; 
    if (isDeviceInDemoMode(this.mContext))
      return false; 
    ContentResolver contentResolver = this.mContext.getContentResolver();
    boolean bool2 = Resources.getSystem().getBoolean(17891527);
    if (Settings.Global.getInt(contentResolver, "user_switcher_enabled", bool2) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (!bool2)
      return false; 
    if (!paramBoolean && 
      !areThereUsersToWhichToSwitch()) {
      paramBoolean = bool;
      return !hasUserRestriction("no_add_user") ? true : paramBoolean;
    } 
    return true;
  }
  
  private boolean areThereUsersToWhichToSwitch() {
    boolean bool = true;
    List<UserInfo> list = getUsers(true);
    if (list == null)
      return false; 
    int i = 0;
    for (UserInfo userInfo : list) {
      int j = i;
      if (userInfo.supportsSwitchToByUser())
        j = i + 1; 
      i = j;
    } 
    if (i <= 1)
      bool = false; 
    return bool;
  }
  
  public static boolean isDeviceInDemoMode(Context paramContext) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    boolean bool = false;
    if (Settings.Global.getInt(contentResolver, "device_demo_mode", 0) > 0)
      bool = true; 
    return bool;
  }
  
  public int getUserSerialNumber(int paramInt) {
    try {
      return this.mService.getUserSerialNumber(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getUserHandle(int paramInt) {
    try {
      return this.mService.getUserHandle(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Bundle getApplicationRestrictions(String paramString) {
    try {
      return this.mService.getApplicationRestrictions(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Bundle getApplicationRestrictions(String paramString, UserHandle paramUserHandle) {
    try {
      return this.mService.getApplicationRestrictionsForUser(paramString, paramUserHandle.getIdentifier());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setApplicationRestrictions(String paramString, Bundle paramBundle, UserHandle paramUserHandle) {
    try {
      this.mService.setApplicationRestrictions(paramString, paramBundle, paramUserHandle.getIdentifier());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public boolean setRestrictionsChallenge(String paramString) {
    return false;
  }
  
  public void setDefaultGuestRestrictions(Bundle paramBundle) {
    try {
      this.mService.setDefaultGuestRestrictions(paramBundle);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Bundle getDefaultGuestRestrictions() {
    try {
      return this.mService.getDefaultGuestRestrictions();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public long getUserCreationTime(UserHandle paramUserHandle) {
    try {
      return this.mService.getUserCreationTime(paramUserHandle.getIdentifier());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean someUserHasSeedAccount(String paramString1, String paramString2) {
    try {
      return this.mService.someUserHasSeedAccount(paramString1, paramString2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public static final class EnforcingUser implements Parcelable {
    public EnforcingUser(int param1Int1, int param1Int2) {
      this.userId = param1Int1;
      this.userRestrictionSource = param1Int2;
    }
    
    private EnforcingUser(Parcel param1Parcel) {
      this.userId = param1Parcel.readInt();
      this.userRestrictionSource = param1Parcel.readInt();
    }
    
    public static final Parcelable.Creator<EnforcingUser> CREATOR = new Parcelable.Creator<EnforcingUser>() {
        public UserManager.EnforcingUser createFromParcel(Parcel param2Parcel) {
          return new UserManager.EnforcingUser(param2Parcel);
        }
        
        public UserManager.EnforcingUser[] newArray(int param2Int) {
          return new UserManager.EnforcingUser[param2Int];
        }
      };
    
    private final int userId;
    
    private final int userRestrictionSource;
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.userId);
      param1Parcel.writeInt(this.userRestrictionSource);
    }
    
    public UserHandle getUserHandle() {
      return UserHandle.of(this.userId);
    }
    
    public int getUserRestrictionSource() {
      return this.userRestrictionSource;
    }
  }
  
  class null implements Parcelable.Creator<EnforcingUser> {
    public UserManager.EnforcingUser createFromParcel(Parcel param1Parcel) {
      return new UserManager.EnforcingUser(param1Parcel);
    }
    
    public UserManager.EnforcingUser[] newArray(int param1Int) {
      return new UserManager.EnforcingUser[param1Int];
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class QuietModeFlag implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class UserOperationResult implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class UserRestrictionKey implements Annotation {}
  
  @SystemApi
  @Retention(RetentionPolicy.SOURCE)
  class UserRestrictionSource implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class UserSwitchabilityResult implements Annotation {}
}
