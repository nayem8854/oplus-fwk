package android.os;

public class OplusThermalState implements Parcelable {
  public OplusThermalState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, boolean paramBoolean, int paramInt10, int paramInt11, int paramInt12) {
    this.mPlugType = paramInt1;
    this.mFcc = paramInt2;
    this.mBatteryRm = paramInt3;
    this.mThermalHeat = paramInt4;
    this.mThermalHeat1 = paramInt5;
    this.mThermalHeat2 = paramInt6;
    this.mThermalHeat3 = paramInt7;
    this.mFast2Normal = paramInt8;
    this.mChargeId = paramInt9;
    this.mIsFastCharge = paramBoolean;
    this.mBatteryCurrent = paramInt10;
    this.mBatteryLevel = paramInt11;
    this.mBatteryTemperature = paramInt12;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OplusThermalState{");
    stringBuilder.append("pluginType:");
    stringBuilder.append(this.mPlugType);
    stringBuilder.append(", fcc:");
    stringBuilder.append(this.mFcc);
    stringBuilder.append(", mBatteryRm:");
    stringBuilder.append(this.mBatteryRm);
    stringBuilder.append(", mThermalHeat:");
    stringBuilder.append(this.mThermalHeat);
    stringBuilder.append(", mThermalHeat1:");
    stringBuilder.append(this.mThermalHeat1);
    stringBuilder.append(", mThermalHeat2:");
    stringBuilder.append(this.mThermalHeat2);
    stringBuilder.append(", mThermalHeat3:");
    stringBuilder.append(this.mThermalHeat3);
    stringBuilder.append(", mFast2Normal:");
    stringBuilder.append(this.mFast2Normal);
    stringBuilder.append(", mChargeId:");
    stringBuilder.append(this.mChargeId);
    stringBuilder.append(", mIsFastCharge:");
    stringBuilder.append(this.mIsFastCharge);
    stringBuilder.append(", mBatteryCurrent:");
    stringBuilder.append(this.mBatteryCurrent);
    stringBuilder.append(", mBatteryLevel:");
    stringBuilder.append(this.mBatteryLevel);
    stringBuilder.append(", mBatteryTemperature:");
    stringBuilder.append(this.mBatteryTemperature);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<OplusThermalState> CREATOR = new Parcelable.Creator<OplusThermalState>() {
      public OplusThermalState createFromParcel(Parcel param1Parcel) {
        boolean bool;
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        int n = param1Parcel.readInt();
        int i1 = param1Parcel.readInt();
        int i2 = param1Parcel.readInt();
        int i3 = param1Parcel.readInt();
        int i4 = param1Parcel.readInt();
        if (param1Parcel.readInt() == 1) {
          bool = true;
        } else {
          bool = false;
        } 
        int i5 = param1Parcel.readInt();
        int i6 = param1Parcel.readInt();
        int i7 = param1Parcel.readInt();
        return new OplusThermalState(i, j, k, m, n, i1, i2, i3, i4, bool, i5, i6, i7);
      }
      
      public OplusThermalState[] newArray(int param1Int) {
        return new OplusThermalState[param1Int];
      }
    };
  
  int mBatteryCurrent;
  
  int mBatteryLevel;
  
  int mBatteryRm;
  
  int mBatteryTemperature;
  
  int mChargeId;
  
  int mFast2Normal;
  
  int mFcc;
  
  boolean mIsFastCharge;
  
  int mPlugType;
  
  int mThermalHeat;
  
  int mThermalHeat1;
  
  int mThermalHeat2;
  
  int mThermalHeat3;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mPlugType);
    paramParcel.writeInt(this.mFcc);
    paramParcel.writeInt(this.mBatteryRm);
    paramParcel.writeInt(this.mThermalHeat);
    paramParcel.writeInt(this.mThermalHeat1);
    paramParcel.writeInt(this.mThermalHeat2);
    paramParcel.writeInt(this.mThermalHeat3);
    paramParcel.writeInt(this.mFast2Normal);
    paramParcel.writeInt(this.mChargeId);
    paramParcel.writeInt(this.mIsFastCharge);
    paramParcel.writeInt(this.mBatteryCurrent);
    paramParcel.writeInt(this.mBatteryLevel);
    paramParcel.writeInt(this.mBatteryTemperature);
  }
  
  public int getPlugType() {
    return this.mPlugType;
  }
  
  public int getFcc() {
    return this.mFcc;
  }
  
  public int getBatteryRm() {
    return this.mBatteryRm;
  }
  
  public int getThermalHeat(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3)
            return -1; 
          return this.mThermalHeat3;
        } 
        return this.mThermalHeat2;
      } 
      return this.mThermalHeat1;
    } 
    return this.mThermalHeat;
  }
  
  public int getFast2Normal() {
    return this.mFast2Normal;
  }
  
  public int getChargeId() {
    return this.mChargeId;
  }
  
  public boolean getIsFastCharge() {
    return this.mIsFastCharge;
  }
  
  public int getBatteryCurrent() {
    return this.mBatteryCurrent;
  }
  
  public int getBatteryLevel() {
    return this.mBatteryLevel;
  }
  
  public int getBatteryTemperature() {
    return this.mBatteryTemperature;
  }
}
