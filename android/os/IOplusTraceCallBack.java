package android.os;

import com.oplus.onetrace.entities.TaskInfo;
import java.util.ArrayList;
import java.util.List;

public interface IOplusTraceCallBack extends IInterface {
  void onDataChanged(SharedMemory paramSharedMemory, int paramInt) throws RemoteException;
  
  void onProcessReused(List<TaskInfo> paramList) throws RemoteException;
  
  class Default implements IOplusTraceCallBack {
    public void onDataChanged(SharedMemory param1SharedMemory, int param1Int) throws RemoteException {}
    
    public void onProcessReused(List<TaskInfo> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusTraceCallBack {
    private static final String DESCRIPTOR = "android.os.IOplusTraceCallBack";
    
    static final int TRANSACTION_onDataChanged = 1;
    
    static final int TRANSACTION_onProcessReused = 2;
    
    public Stub() {
      attachInterface(this, "android.os.IOplusTraceCallBack");
    }
    
    public static IOplusTraceCallBack asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IOplusTraceCallBack");
      if (iInterface != null && iInterface instanceof IOplusTraceCallBack)
        return (IOplusTraceCallBack)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onProcessReused";
      } 
      return "onDataChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ArrayList<?> arrayList;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.os.IOplusTraceCallBack");
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.IOplusTraceCallBack");
        arrayList = param1Parcel1.createTypedArrayList(TaskInfo.CREATOR);
        onProcessReused((List)arrayList);
        return true;
      } 
      arrayList.enforceInterface("android.os.IOplusTraceCallBack");
      if (arrayList.readInt() != 0) {
        SharedMemory sharedMemory = SharedMemory.CREATOR.createFromParcel((Parcel)arrayList);
      } else {
        param1Parcel2 = null;
      } 
      param1Int1 = arrayList.readInt();
      onDataChanged((SharedMemory)param1Parcel2, param1Int1);
      return true;
    }
    
    private static class Proxy implements IOplusTraceCallBack {
      public static IOplusTraceCallBack sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IOplusTraceCallBack";
      }
      
      public void onDataChanged(SharedMemory param2SharedMemory, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusTraceCallBack");
          if (param2SharedMemory != null) {
            parcel.writeInt(1);
            param2SharedMemory.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IOplusTraceCallBack.Stub.getDefaultImpl() != null) {
            IOplusTraceCallBack.Stub.getDefaultImpl().onDataChanged(param2SharedMemory, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onProcessReused(List<TaskInfo> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusTraceCallBack");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IOplusTraceCallBack.Stub.getDefaultImpl() != null) {
            IOplusTraceCallBack.Stub.getDefaultImpl().onProcessReused(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusTraceCallBack param1IOplusTraceCallBack) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusTraceCallBack != null) {
          Proxy.sDefaultImpl = param1IOplusTraceCallBack;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusTraceCallBack getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
