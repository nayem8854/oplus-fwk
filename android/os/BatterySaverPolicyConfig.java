package android.os;

import android.annotation.SystemApi;
import android.text.TextUtils;
import android.util.ArrayMap;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

@SystemApi
public final class BatterySaverPolicyConfig implements Parcelable {
  private BatterySaverPolicyConfig(Builder paramBuilder) {
    this.mAdjustBrightnessFactor = Math.max(0.0F, Math.min(paramBuilder.mAdjustBrightnessFactor, 1.0F));
    this.mAdvertiseIsEnabled = paramBuilder.mAdvertiseIsEnabled;
    this.mDeferFullBackup = paramBuilder.mDeferFullBackup;
    this.mDeferKeyValueBackup = paramBuilder.mDeferKeyValueBackup;
    ArrayMap arrayMap = new ArrayMap(paramBuilder.mDeviceSpecificSettings);
    this.mDeviceSpecificSettings = Collections.unmodifiableMap((Map<? extends String, ? extends String>)arrayMap);
    this.mDisableAnimation = paramBuilder.mDisableAnimation;
    this.mDisableAod = paramBuilder.mDisableAod;
    this.mDisableLaunchBoost = paramBuilder.mDisableLaunchBoost;
    this.mDisableOptionalSensors = paramBuilder.mDisableOptionalSensors;
    this.mDisableSoundTrigger = paramBuilder.mDisableSoundTrigger;
    this.mDisableVibration = paramBuilder.mDisableVibration;
    this.mEnableAdjustBrightness = paramBuilder.mEnableAdjustBrightness;
    this.mEnableDataSaver = paramBuilder.mEnableDataSaver;
    this.mEnableFirewall = paramBuilder.mEnableFirewall;
    this.mEnableNightMode = paramBuilder.mEnableNightMode;
    this.mEnableQuickDoze = paramBuilder.mEnableQuickDoze;
    this.mForceAllAppsStandby = paramBuilder.mForceAllAppsStandby;
    this.mForceBackgroundCheck = paramBuilder.mForceBackgroundCheck;
    int i = Math.min(paramBuilder.mLocationMode, 4);
    this.mLocationMode = Math.max(0, i);
  }
  
  private BatterySaverPolicyConfig(Parcel paramParcel) {
    this.mAdjustBrightnessFactor = Math.max(0.0F, Math.min(paramParcel.readFloat(), 1.0F));
    this.mAdvertiseIsEnabled = paramParcel.readBoolean();
    this.mDeferFullBackup = paramParcel.readBoolean();
    this.mDeferKeyValueBackup = paramParcel.readBoolean();
    int i = paramParcel.readInt();
    ArrayMap<String, String> arrayMap = new ArrayMap(i);
    int j;
    for (j = 0; j < i; j++) {
      String str1 = TextUtils.emptyIfNull(paramParcel.readString());
      String str2 = TextUtils.emptyIfNull(paramParcel.readString());
      if (!str1.trim().isEmpty())
        arrayMap.put(str1, str2); 
    } 
    this.mDeviceSpecificSettings = Collections.unmodifiableMap((Map<? extends String, ? extends String>)arrayMap);
    this.mDisableAnimation = paramParcel.readBoolean();
    this.mDisableAod = paramParcel.readBoolean();
    this.mDisableLaunchBoost = paramParcel.readBoolean();
    this.mDisableOptionalSensors = paramParcel.readBoolean();
    this.mDisableSoundTrigger = paramParcel.readBoolean();
    this.mDisableVibration = paramParcel.readBoolean();
    this.mEnableAdjustBrightness = paramParcel.readBoolean();
    this.mEnableDataSaver = paramParcel.readBoolean();
    this.mEnableFirewall = paramParcel.readBoolean();
    this.mEnableNightMode = paramParcel.readBoolean();
    this.mEnableQuickDoze = paramParcel.readBoolean();
    this.mForceAllAppsStandby = paramParcel.readBoolean();
    this.mForceBackgroundCheck = paramParcel.readBoolean();
    j = Math.min(paramParcel.readInt(), 4);
    this.mLocationMode = Math.max(0, j);
  }
  
  public static final Parcelable.Creator<BatterySaverPolicyConfig> CREATOR = new Parcelable.Creator<BatterySaverPolicyConfig>() {
      public BatterySaverPolicyConfig createFromParcel(Parcel param1Parcel) {
        return new BatterySaverPolicyConfig(param1Parcel);
      }
      
      public BatterySaverPolicyConfig[] newArray(int param1Int) {
        return new BatterySaverPolicyConfig[param1Int];
      }
    };
  
  private final float mAdjustBrightnessFactor;
  
  private final boolean mAdvertiseIsEnabled;
  
  private final boolean mDeferFullBackup;
  
  private final boolean mDeferKeyValueBackup;
  
  private final Map<String, String> mDeviceSpecificSettings;
  
  private final boolean mDisableAnimation;
  
  private final boolean mDisableAod;
  
  private final boolean mDisableLaunchBoost;
  
  private final boolean mDisableOptionalSensors;
  
  private final boolean mDisableSoundTrigger;
  
  private final boolean mDisableVibration;
  
  private final boolean mEnableAdjustBrightness;
  
  private final boolean mEnableDataSaver;
  
  private final boolean mEnableFirewall;
  
  private final boolean mEnableNightMode;
  
  private final boolean mEnableQuickDoze;
  
  private final boolean mForceAllAppsStandby;
  
  private final boolean mForceBackgroundCheck;
  
  private final int mLocationMode;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeFloat(this.mAdjustBrightnessFactor);
    paramParcel.writeBoolean(this.mAdvertiseIsEnabled);
    paramParcel.writeBoolean(this.mDeferFullBackup);
    paramParcel.writeBoolean(this.mDeferKeyValueBackup);
    Set<Map.Entry<String, String>> set = this.mDeviceSpecificSettings.entrySet();
    paramInt = set.size();
    paramParcel.writeInt(paramInt);
    for (Map.Entry<String, String> entry : set) {
      paramParcel.writeString((String)entry.getKey());
      paramParcel.writeString((String)entry.getValue());
    } 
    paramParcel.writeBoolean(this.mDisableAnimation);
    paramParcel.writeBoolean(this.mDisableAod);
    paramParcel.writeBoolean(this.mDisableLaunchBoost);
    paramParcel.writeBoolean(this.mDisableOptionalSensors);
    paramParcel.writeBoolean(this.mDisableSoundTrigger);
    paramParcel.writeBoolean(this.mDisableVibration);
    paramParcel.writeBoolean(this.mEnableAdjustBrightness);
    paramParcel.writeBoolean(this.mEnableDataSaver);
    paramParcel.writeBoolean(this.mEnableFirewall);
    paramParcel.writeBoolean(this.mEnableNightMode);
    paramParcel.writeBoolean(this.mEnableQuickDoze);
    paramParcel.writeBoolean(this.mForceAllAppsStandby);
    paramParcel.writeBoolean(this.mForceBackgroundCheck);
    paramParcel.writeInt(this.mLocationMode);
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    for (Map.Entry<String, String> entry : this.mDeviceSpecificSettings.entrySet()) {
      stringBuilder1.append((String)entry.getKey());
      stringBuilder1.append("=");
      stringBuilder1.append((String)entry.getValue());
      stringBuilder1.append(",");
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("adjust_brightness_disabled=");
    stringBuilder2.append(this.mEnableAdjustBrightness ^ true);
    stringBuilder2.append(",adjust_brightness_factor=");
    stringBuilder2.append(this.mAdjustBrightnessFactor);
    stringBuilder2.append(",advertise_is_enabled=");
    stringBuilder2.append(this.mAdvertiseIsEnabled);
    stringBuilder2.append(",animation_disabled=");
    stringBuilder2.append(this.mDisableAnimation);
    stringBuilder2.append(",aod_disabled=");
    stringBuilder2.append(this.mDisableAod);
    stringBuilder2.append(",datasaver_disabled=");
    stringBuilder2.append(this.mEnableDataSaver ^ true);
    stringBuilder2.append(",enable_night_mode=");
    stringBuilder2.append(this.mEnableNightMode);
    stringBuilder2.append(",firewall_disabled=");
    stringBuilder2.append(this.mEnableFirewall ^ true);
    stringBuilder2.append(",force_all_apps_standby=");
    stringBuilder2.append(this.mForceAllAppsStandby);
    stringBuilder2.append(",force_background_check=");
    stringBuilder2.append(this.mForceBackgroundCheck);
    stringBuilder2.append(",fullbackup_deferred=");
    stringBuilder2.append(this.mDeferFullBackup);
    stringBuilder2.append(",gps_mode=");
    stringBuilder2.append(this.mLocationMode);
    stringBuilder2.append(",keyvaluebackup_deferred=");
    stringBuilder2.append(this.mDeferKeyValueBackup);
    stringBuilder2.append(",launch_boost_disabled=");
    stringBuilder2.append(this.mDisableLaunchBoost);
    stringBuilder2.append(",optional_sensors_disabled=");
    stringBuilder2.append(this.mDisableOptionalSensors);
    stringBuilder2.append(",quick_doze_enabled=");
    stringBuilder2.append(this.mEnableQuickDoze);
    stringBuilder2.append(",soundtrigger_disabled=");
    stringBuilder2.append(this.mDisableSoundTrigger);
    stringBuilder2.append(",vibration_disabled=");
    stringBuilder2.append(this.mDisableVibration);
    stringBuilder2.append(",");
    stringBuilder2.append(stringBuilder1.toString());
    return stringBuilder2.toString();
  }
  
  public float getAdjustBrightnessFactor() {
    return this.mAdjustBrightnessFactor;
  }
  
  public boolean getAdvertiseIsEnabled() {
    return this.mAdvertiseIsEnabled;
  }
  
  public boolean getDeferFullBackup() {
    return this.mDeferFullBackup;
  }
  
  public boolean getDeferKeyValueBackup() {
    return this.mDeferKeyValueBackup;
  }
  
  public Map<String, String> getDeviceSpecificSettings() {
    return this.mDeviceSpecificSettings;
  }
  
  public boolean getDisableAnimation() {
    return this.mDisableAnimation;
  }
  
  public boolean getDisableAod() {
    return this.mDisableAod;
  }
  
  public boolean getDisableLaunchBoost() {
    return this.mDisableLaunchBoost;
  }
  
  public boolean getDisableOptionalSensors() {
    return this.mDisableOptionalSensors;
  }
  
  public boolean getDisableSoundTrigger() {
    return this.mDisableSoundTrigger;
  }
  
  public boolean getDisableVibration() {
    return this.mDisableVibration;
  }
  
  public boolean getEnableAdjustBrightness() {
    return this.mEnableAdjustBrightness;
  }
  
  public boolean getEnableDataSaver() {
    return this.mEnableDataSaver;
  }
  
  public boolean getEnableFirewall() {
    return this.mEnableFirewall;
  }
  
  public boolean getEnableNightMode() {
    return this.mEnableNightMode;
  }
  
  public boolean getEnableQuickDoze() {
    return this.mEnableQuickDoze;
  }
  
  public boolean getForceAllAppsStandby() {
    return this.mForceAllAppsStandby;
  }
  
  public boolean getForceBackgroundCheck() {
    return this.mForceBackgroundCheck;
  }
  
  public int getLocationMode() {
    return this.mLocationMode;
  }
  
  class Builder {
    private float mAdjustBrightnessFactor = 1.0F;
    
    private boolean mAdvertiseIsEnabled = false;
    
    private boolean mDeferFullBackup = false;
    
    private boolean mDeferKeyValueBackup = false;
    
    private final ArrayMap<String, String> mDeviceSpecificSettings = new ArrayMap();
    
    private boolean mDisableAnimation = false;
    
    private boolean mDisableAod = false;
    
    private boolean mDisableLaunchBoost = false;
    
    private boolean mDisableOptionalSensors = false;
    
    private boolean mDisableSoundTrigger = false;
    
    private boolean mDisableVibration = false;
    
    private boolean mEnableAdjustBrightness = false;
    
    private boolean mEnableDataSaver = false;
    
    private boolean mEnableFirewall = false;
    
    private boolean mEnableNightMode = false;
    
    private boolean mEnableQuickDoze = false;
    
    private boolean mForceAllAppsStandby = false;
    
    private boolean mForceBackgroundCheck = false;
    
    private int mLocationMode = 0;
    
    public Builder setAdjustBrightnessFactor(float param1Float) {
      this.mAdjustBrightnessFactor = param1Float;
      return this;
    }
    
    public Builder setAdvertiseIsEnabled(boolean param1Boolean) {
      this.mAdvertiseIsEnabled = param1Boolean;
      return this;
    }
    
    public Builder setDeferFullBackup(boolean param1Boolean) {
      this.mDeferFullBackup = param1Boolean;
      return this;
    }
    
    public Builder setDeferKeyValueBackup(boolean param1Boolean) {
      this.mDeferKeyValueBackup = param1Boolean;
      return this;
    }
    
    public Builder addDeviceSpecificSetting(String param1String1, String param1String2) {
      if (param1String1 != null) {
        param1String1 = param1String1.trim();
        if (!TextUtils.isEmpty(param1String1)) {
          this.mDeviceSpecificSettings.put(param1String1, TextUtils.emptyIfNull(param1String2));
          return this;
        } 
        throw new IllegalArgumentException("Key cannot be empty");
      } 
      throw new IllegalArgumentException("Key cannot be null");
    }
    
    public Builder setDisableAnimation(boolean param1Boolean) {
      this.mDisableAnimation = param1Boolean;
      return this;
    }
    
    public Builder setDisableAod(boolean param1Boolean) {
      this.mDisableAod = param1Boolean;
      return this;
    }
    
    public Builder setDisableLaunchBoost(boolean param1Boolean) {
      this.mDisableLaunchBoost = param1Boolean;
      return this;
    }
    
    public Builder setDisableOptionalSensors(boolean param1Boolean) {
      this.mDisableOptionalSensors = param1Boolean;
      return this;
    }
    
    public Builder setDisableSoundTrigger(boolean param1Boolean) {
      this.mDisableSoundTrigger = param1Boolean;
      return this;
    }
    
    public Builder setDisableVibration(boolean param1Boolean) {
      this.mDisableVibration = param1Boolean;
      return this;
    }
    
    public Builder setEnableAdjustBrightness(boolean param1Boolean) {
      this.mEnableAdjustBrightness = param1Boolean;
      return this;
    }
    
    public Builder setEnableDataSaver(boolean param1Boolean) {
      this.mEnableDataSaver = param1Boolean;
      return this;
    }
    
    public Builder setEnableFirewall(boolean param1Boolean) {
      this.mEnableFirewall = param1Boolean;
      return this;
    }
    
    public Builder setEnableNightMode(boolean param1Boolean) {
      this.mEnableNightMode = param1Boolean;
      return this;
    }
    
    public Builder setEnableQuickDoze(boolean param1Boolean) {
      this.mEnableQuickDoze = param1Boolean;
      return this;
    }
    
    public Builder setForceAllAppsStandby(boolean param1Boolean) {
      this.mForceAllAppsStandby = param1Boolean;
      return this;
    }
    
    public Builder setForceBackgroundCheck(boolean param1Boolean) {
      this.mForceBackgroundCheck = param1Boolean;
      return this;
    }
    
    public Builder setLocationMode(int param1Int) {
      this.mLocationMode = param1Int;
      return this;
    }
    
    public BatterySaverPolicyConfig build() {
      return new BatterySaverPolicyConfig(this);
    }
  }
}
