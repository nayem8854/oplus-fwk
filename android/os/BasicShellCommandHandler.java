package android.os;

import java.io.BufferedInputStream;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

public abstract class BasicShellCommandHandler {
  static final boolean DEBUG = false;
  
  static final String TAG = "ShellCommand";
  
  private int mArgPos;
  
  private String[] mArgs;
  
  private String mCmd;
  
  private String mCurArgData;
  
  private FileDescriptor mErr;
  
  private PrintWriter mErrPrintWriter;
  
  private FileOutputStream mFileErr;
  
  private FileInputStream mFileIn;
  
  private FileOutputStream mFileOut;
  
  private FileDescriptor mIn;
  
  private InputStream mInputStream;
  
  private FileDescriptor mOut;
  
  private PrintWriter mOutPrintWriter;
  
  private Binder mTarget;
  
  public void init(Binder paramBinder, FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, FileDescriptor paramFileDescriptor3, String[] paramArrayOfString, int paramInt) {
    this.mTarget = paramBinder;
    this.mIn = paramFileDescriptor1;
    this.mOut = paramFileDescriptor2;
    this.mErr = paramFileDescriptor3;
    this.mArgs = paramArrayOfString;
    this.mCmd = null;
    this.mArgPos = paramInt;
    this.mCurArgData = null;
    this.mFileIn = null;
    this.mFileOut = null;
    this.mFileErr = null;
    this.mOutPrintWriter = null;
    this.mErrPrintWriter = null;
    this.mInputStream = null;
  }
  
  public int exec(Binder paramBinder, FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, FileDescriptor paramFileDescriptor3, String[] paramArrayOfString) {
    String str;
    int i;
    if (paramArrayOfString != null && paramArrayOfString.length > 0) {
      str = paramArrayOfString[0];
      i = 1;
    } else {
      str = null;
      i = 0;
    } 
    init(paramBinder, paramFileDescriptor1, paramFileDescriptor2, paramFileDescriptor3, paramArrayOfString, i);
    this.mCmd = str;
    int j = -1;
    try {
      j = i = onCommand(str);
      PrintWriter printWriter = this.mOutPrintWriter;
      if (printWriter != null)
        printWriter.flush(); 
      printWriter = this.mErrPrintWriter;
      i = j;
      if (printWriter != null) {
        i = j;
      } else {
        return i;
      } 
    } finally {
      paramBinder = null;
    } 
    return i;
  }
  
  public FileDescriptor getOutFileDescriptor() {
    return this.mOut;
  }
  
  public OutputStream getRawOutputStream() {
    if (this.mFileOut == null)
      this.mFileOut = new FileOutputStream(this.mOut); 
    return this.mFileOut;
  }
  
  public PrintWriter getOutPrintWriter() {
    if (this.mOutPrintWriter == null)
      this.mOutPrintWriter = new PrintWriter(getRawOutputStream()); 
    return this.mOutPrintWriter;
  }
  
  public FileDescriptor getErrFileDescriptor() {
    return this.mErr;
  }
  
  public OutputStream getRawErrorStream() {
    if (this.mFileErr == null)
      this.mFileErr = new FileOutputStream(this.mErr); 
    return this.mFileErr;
  }
  
  public PrintWriter getErrPrintWriter() {
    if (this.mErr == null)
      return getOutPrintWriter(); 
    if (this.mErrPrintWriter == null)
      this.mErrPrintWriter = new PrintWriter(getRawErrorStream()); 
    return this.mErrPrintWriter;
  }
  
  public FileDescriptor getInFileDescriptor() {
    return this.mIn;
  }
  
  public InputStream getRawInputStream() {
    if (this.mFileIn == null)
      this.mFileIn = new FileInputStream(this.mIn); 
    return this.mFileIn;
  }
  
  public InputStream getBufferedInputStream() {
    if (this.mInputStream == null)
      this.mInputStream = new BufferedInputStream(getRawInputStream()); 
    return this.mInputStream;
  }
  
  public String getNextOption() {
    if (this.mCurArgData == null) {
      int i = this.mArgPos;
      String[] arrayOfString = this.mArgs;
      if (i >= arrayOfString.length)
        return null; 
      String str1 = arrayOfString[i];
      if (!str1.startsWith("-"))
        return null; 
      this.mArgPos++;
      if (str1.equals("--"))
        return null; 
      if (str1.length() > 1 && str1.charAt(1) != '-') {
        if (str1.length() > 2) {
          this.mCurArgData = str1.substring(2);
          return str1.substring(0, 2);
        } 
        this.mCurArgData = null;
        return str1;
      } 
      this.mCurArgData = null;
      return str1;
    } 
    String str = this.mArgs[this.mArgPos - 1];
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("No argument expected after \"");
    stringBuilder.append(str);
    stringBuilder.append("\"");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public String getNextArg() {
    if (this.mCurArgData != null) {
      String str = this.mCurArgData;
      this.mCurArgData = null;
      return str;
    } 
    int i = this.mArgPos;
    String[] arrayOfString = this.mArgs;
    if (i < arrayOfString.length) {
      this.mArgPos = i + 1;
      return arrayOfString[i];
    } 
    return null;
  }
  
  public String peekNextArg() {
    String str = this.mCurArgData;
    if (str != null)
      return str; 
    int i = this.mArgPos;
    String[] arrayOfString = this.mArgs;
    if (i < arrayOfString.length)
      return arrayOfString[i]; 
    return null;
  }
  
  public int getRemainingArgsCount() {
    int i = this.mArgPos;
    String[] arrayOfString = this.mArgs;
    if (i >= arrayOfString.length)
      return 0; 
    return arrayOfString.length - i;
  }
  
  public String getNextArgRequired() {
    String str = getNextArg();
    if (str != null)
      return str; 
    str = this.mArgs[this.mArgPos - 1];
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Argument expected after \"");
    stringBuilder.append(str);
    stringBuilder.append("\"");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int handleDefaultCommands(String paramString) {
    if (paramString == null || "help".equals(paramString) || "-h".equals(paramString)) {
      onHelp();
      return -1;
    } 
    PrintWriter printWriter = getOutPrintWriter();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown command: ");
    stringBuilder.append(paramString);
    printWriter.println(stringBuilder.toString());
    return -1;
  }
  
  public Binder getTarget() {
    return this.mTarget;
  }
  
  public String[] getAllArgs() {
    return this.mArgs;
  }
  
  public abstract int onCommand(String paramString);
  
  public abstract void onHelp();
}
