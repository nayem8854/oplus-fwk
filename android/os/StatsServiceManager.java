package android.os;

import android.annotation.SystemApi;

@SystemApi(client = SystemApi.Client.MODULE_LIBRARIES)
public class StatsServiceManager {
  public static final class ServiceRegisterer {
    private final String mServiceName;
    
    public ServiceRegisterer(String param1String) {
      this.mServiceName = param1String;
    }
    
    public IBinder get() {
      return ServiceManager.getService(this.mServiceName);
    }
    
    public IBinder getOrThrow() throws StatsServiceManager.ServiceNotFoundException {
      try {
        return ServiceManager.getServiceOrThrow(this.mServiceName);
      } catch (ServiceNotFoundException serviceNotFoundException) {
        throw new StatsServiceManager.ServiceNotFoundException(this.mServiceName);
      } 
    }
    
    private IBinder tryGet() {
      return ServiceManager.checkService(this.mServiceName);
    }
  }
  
  class ServiceNotFoundException extends ServiceManager.ServiceNotFoundException {
    public ServiceNotFoundException(StatsServiceManager this$0) {
      super((String)this$0);
    }
  }
  
  public ServiceRegisterer getStatsCompanionServiceRegisterer() {
    return new ServiceRegisterer("statscompanion");
  }
  
  public ServiceRegisterer getStatsManagerServiceRegisterer() {
    return new ServiceRegisterer("statsmanager");
  }
  
  public ServiceRegisterer getStatsdServiceRegisterer() {
    return new ServiceRegisterer("stats");
  }
}
