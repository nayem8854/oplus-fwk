package android.os;

import android.annotation.SystemApi;
import dalvik.annotation.optimization.FastNative;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import libcore.util.NativeAllocationRegistry;

@SystemApi
public class HwParcel {
  public static final int STATUS_SUCCESS = 0;
  
  private static final String TAG = "HwParcel";
  
  private static final NativeAllocationRegistry sNativeRegistry;
  
  private long mNativeContext;
  
  private HwParcel(boolean paramBoolean) {
    native_setup(paramBoolean);
    sNativeRegistry.registerNativeAllocation(this, this.mNativeContext);
  }
  
  public HwParcel() {
    native_setup(true);
    sNativeRegistry.registerNativeAllocation(this, this.mNativeContext);
  }
  
  public final void writeBoolVector(ArrayList<Boolean> paramArrayList) {
    int i = paramArrayList.size();
    boolean[] arrayOfBoolean = new boolean[i];
    for (byte b = 0; b < i; b++)
      arrayOfBoolean[b] = ((Boolean)paramArrayList.get(b)).booleanValue(); 
    writeBoolVector(arrayOfBoolean);
  }
  
  public final void writeInt8Vector(ArrayList<Byte> paramArrayList) {
    int i = paramArrayList.size();
    byte[] arrayOfByte = new byte[i];
    for (byte b = 0; b < i; b++)
      arrayOfByte[b] = ((Byte)paramArrayList.get(b)).byteValue(); 
    writeInt8Vector(arrayOfByte);
  }
  
  public final void writeInt16Vector(ArrayList<Short> paramArrayList) {
    int i = paramArrayList.size();
    short[] arrayOfShort = new short[i];
    for (byte b = 0; b < i; b++)
      arrayOfShort[b] = ((Short)paramArrayList.get(b)).shortValue(); 
    writeInt16Vector(arrayOfShort);
  }
  
  public final void writeInt32Vector(ArrayList<Integer> paramArrayList) {
    int i = paramArrayList.size();
    int[] arrayOfInt = new int[i];
    for (byte b = 0; b < i; b++)
      arrayOfInt[b] = ((Integer)paramArrayList.get(b)).intValue(); 
    writeInt32Vector(arrayOfInt);
  }
  
  public final void writeInt64Vector(ArrayList<Long> paramArrayList) {
    int i = paramArrayList.size();
    long[] arrayOfLong = new long[i];
    for (byte b = 0; b < i; b++)
      arrayOfLong[b] = ((Long)paramArrayList.get(b)).longValue(); 
    writeInt64Vector(arrayOfLong);
  }
  
  public final void writeFloatVector(ArrayList<Float> paramArrayList) {
    int i = paramArrayList.size();
    float[] arrayOfFloat = new float[i];
    for (byte b = 0; b < i; b++)
      arrayOfFloat[b] = ((Float)paramArrayList.get(b)).floatValue(); 
    writeFloatVector(arrayOfFloat);
  }
  
  public final void writeDoubleVector(ArrayList<Double> paramArrayList) {
    int i = paramArrayList.size();
    double[] arrayOfDouble = new double[i];
    for (byte b = 0; b < i; b++)
      arrayOfDouble[b] = ((Double)paramArrayList.get(b)).doubleValue(); 
    writeDoubleVector(arrayOfDouble);
  }
  
  public final void writeStringVector(ArrayList<String> paramArrayList) {
    writeStringVector(paramArrayList.<String>toArray(new String[paramArrayList.size()]));
  }
  
  public final void writeInt8Array(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null) {
      writeInt32(-1);
      return;
    } 
    writeInt8Vector(paramArrayOfbyte);
  }
  
  public final void writeNativeHandleVector(ArrayList<NativeHandle> paramArrayList) {
    writeNativeHandleVector(paramArrayList.<NativeHandle>toArray(new NativeHandle[paramArrayList.size()]));
  }
  
  public final ArrayList<Boolean> readBoolVector() {
    Boolean[] arrayOfBoolean = HwBlob.wrapArray(readBoolVectorAsArray());
    return new ArrayList<>(Arrays.asList(arrayOfBoolean));
  }
  
  public final ArrayList<Byte> readInt8Vector() {
    Byte[] arrayOfByte = HwBlob.wrapArray(readInt8VectorAsArray());
    return new ArrayList<>(Arrays.asList(arrayOfByte));
  }
  
  public final ArrayList<Short> readInt16Vector() {
    Short[] arrayOfShort = HwBlob.wrapArray(readInt16VectorAsArray());
    return new ArrayList<>(Arrays.asList(arrayOfShort));
  }
  
  public final ArrayList<Integer> readInt32Vector() {
    Integer[] arrayOfInteger = HwBlob.wrapArray(readInt32VectorAsArray());
    return new ArrayList<>(Arrays.asList(arrayOfInteger));
  }
  
  public final ArrayList<Long> readInt64Vector() {
    Long[] arrayOfLong = HwBlob.wrapArray(readInt64VectorAsArray());
    return new ArrayList<>(Arrays.asList(arrayOfLong));
  }
  
  public final ArrayList<Float> readFloatVector() {
    Float[] arrayOfFloat = HwBlob.wrapArray(readFloatVectorAsArray());
    return new ArrayList<>(Arrays.asList(arrayOfFloat));
  }
  
  public final ArrayList<Double> readDoubleVector() {
    Double[] arrayOfDouble = HwBlob.wrapArray(readDoubleVectorAsArray());
    return new ArrayList<>(Arrays.asList(arrayOfDouble));
  }
  
  public final ArrayList<String> readStringVector() {
    return new ArrayList<>(Arrays.asList(readStringVectorAsArray()));
  }
  
  public final ArrayList<NativeHandle> readNativeHandleVector() {
    return new ArrayList<>(Arrays.asList(readNativeHandleAsArray()));
  }
  
  static {
    long l = native_init();
    sNativeRegistry = new NativeAllocationRegistry(HwParcel.class.getClassLoader(), l, 128L);
  }
  
  private static final native long native_init();
  
  @FastNative
  private final native void native_setup(boolean paramBoolean);
  
  @FastNative
  private final native boolean[] readBoolVectorAsArray();
  
  @FastNative
  private final native double[] readDoubleVectorAsArray();
  
  @FastNative
  private final native float[] readFloatVectorAsArray();
  
  @FastNative
  private final native short[] readInt16VectorAsArray();
  
  @FastNative
  private final native int[] readInt32VectorAsArray();
  
  @FastNative
  private final native long[] readInt64VectorAsArray();
  
  @FastNative
  private final native byte[] readInt8VectorAsArray();
  
  @FastNative
  private final native NativeHandle[] readNativeHandleAsArray();
  
  @FastNative
  private final native String[] readStringVectorAsArray();
  
  @FastNative
  private final native void writeBoolVector(boolean[] paramArrayOfboolean);
  
  @FastNative
  private final native void writeDoubleVector(double[] paramArrayOfdouble);
  
  @FastNative
  private final native void writeFloatVector(float[] paramArrayOffloat);
  
  @FastNative
  private final native void writeInt16Vector(short[] paramArrayOfshort);
  
  @FastNative
  private final native void writeInt32Vector(int[] paramArrayOfint);
  
  @FastNative
  private final native void writeInt64Vector(long[] paramArrayOflong);
  
  @FastNative
  private final native void writeInt8Vector(byte[] paramArrayOfbyte);
  
  @FastNative
  private final native void writeNativeHandleVector(NativeHandle[] paramArrayOfNativeHandle);
  
  @FastNative
  private final native void writeStringVector(String[] paramArrayOfString);
  
  public final native void enforceInterface(String paramString);
  
  @FastNative
  public final native boolean readBool();
  
  @FastNative
  public final native HwBlob readBuffer(long paramLong);
  
  @FastNative
  public final native double readDouble();
  
  @FastNative
  public final native HwBlob readEmbeddedBuffer(long paramLong1, long paramLong2, long paramLong3, boolean paramBoolean);
  
  @FastNative
  public final native HidlMemory readEmbeddedHidlMemory(long paramLong1, long paramLong2, long paramLong3);
  
  @FastNative
  public final native NativeHandle readEmbeddedNativeHandle(long paramLong1, long paramLong2);
  
  @FastNative
  public final native float readFloat();
  
  @FastNative
  public final native HidlMemory readHidlMemory();
  
  @FastNative
  public final native short readInt16();
  
  @FastNative
  public final native int readInt32();
  
  @FastNative
  public final native long readInt64();
  
  @FastNative
  public final native byte readInt8();
  
  @FastNative
  public final native NativeHandle readNativeHandle();
  
  @FastNative
  public final native String readString();
  
  @FastNative
  public final native IHwBinder readStrongBinder();
  
  @FastNative
  public final native void release();
  
  @FastNative
  public final native void releaseTemporaryStorage();
  
  public final native void send();
  
  @FastNative
  public final native void verifySuccess();
  
  @FastNative
  public final native void writeBool(boolean paramBoolean);
  
  @FastNative
  public final native void writeBuffer(HwBlob paramHwBlob);
  
  @FastNative
  public final native void writeDouble(double paramDouble);
  
  @FastNative
  public final native void writeFloat(float paramFloat);
  
  @FastNative
  public final native void writeHidlMemory(HidlMemory paramHidlMemory);
  
  @FastNative
  public final native void writeInt16(short paramShort);
  
  @FastNative
  public final native void writeInt32(int paramInt);
  
  @FastNative
  public final native void writeInt64(long paramLong);
  
  @FastNative
  public final native void writeInt8(byte paramByte);
  
  @FastNative
  public final native void writeInterfaceToken(String paramString);
  
  @FastNative
  public final native void writeNativeHandle(NativeHandle paramNativeHandle);
  
  @FastNative
  public final native void writeStatus(int paramInt);
  
  @FastNative
  public final native void writeString(String paramString);
  
  @FastNative
  public final native void writeStrongBinder(IHwBinder paramIHwBinder);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Status {}
}
