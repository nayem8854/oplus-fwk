package android.os;

public interface IIncidentAuthListener extends IInterface {
  void onReportApproved() throws RemoteException;
  
  void onReportDenied() throws RemoteException;
  
  class Default implements IIncidentAuthListener {
    public void onReportApproved() throws RemoteException {}
    
    public void onReportDenied() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IIncidentAuthListener {
    private static final String DESCRIPTOR = "android.os.IIncidentAuthListener";
    
    static final int TRANSACTION_onReportApproved = 1;
    
    static final int TRANSACTION_onReportDenied = 2;
    
    public Stub() {
      attachInterface(this, "android.os.IIncidentAuthListener");
    }
    
    public static IIncidentAuthListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IIncidentAuthListener");
      if (iInterface != null && iInterface instanceof IIncidentAuthListener)
        return (IIncidentAuthListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onReportDenied";
      } 
      return "onReportApproved";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.os.IIncidentAuthListener");
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.IIncidentAuthListener");
        onReportDenied();
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IIncidentAuthListener");
      onReportApproved();
      return true;
    }
    
    private static class Proxy implements IIncidentAuthListener {
      public static IIncidentAuthListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IIncidentAuthListener";
      }
      
      public void onReportApproved() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IIncidentAuthListener");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IIncidentAuthListener.Stub.getDefaultImpl() != null) {
            IIncidentAuthListener.Stub.getDefaultImpl().onReportApproved();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onReportDenied() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IIncidentAuthListener");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IIncidentAuthListener.Stub.getDefaultImpl() != null) {
            IIncidentAuthListener.Stub.getDefaultImpl().onReportDenied();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IIncidentAuthListener param1IIncidentAuthListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IIncidentAuthListener != null) {
          Proxy.sDefaultImpl = param1IIncidentAuthListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IIncidentAuthListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
