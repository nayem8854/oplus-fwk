package android.os;

import android.app.KeyguardManager;
import android.app.OplusActivityManager;
import android.content.Context;
import android.media.AudioAttributes;
import android.util.Log;
import com.oplus.debug.InputLog;

public abstract class OplusBaseSystemVibrator extends Vibrator {
  private boolean mLogEnable = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private boolean mCameraAntiShake = false;
  
  private Context mContext = null;
  
  private OplusActivityManager mOppoActivityManager = null;
  
  private static final String TAG = "OplusBaseSystemVibrator";
  
  private KeyguardManager mKeyguardManager;
  
  public OplusBaseSystemVibrator() {}
  
  public OplusBaseSystemVibrator(Context paramContext) {
    super(paramContext);
    this.mCameraAntiShake = paramContext.getPackageManager().hasSystemFeature("oppo.camera.antishake.support");
    this.mContext = paramContext;
    this.mOppoActivityManager = new OplusActivityManager();
  }
  
  private boolean isVirtualKeyVibrate(int paramInt, VibrationEffect.Waveform paramWaveform) {
    long[] arrayOfLong = paramWaveform.getTimings();
    if (UserHandle.getAppId(paramInt) == 1000 && arrayOfLong.length == 2 && arrayOfLong[0] == 0L && arrayOfLong[1] == 35L) {
      if (this.mLogEnable) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("vibrate from user ");
        stringBuilder.append(paramInt);
        stringBuilder.append(", effect = ");
        stringBuilder.append(paramWaveform);
        Log.d("OplusBaseSystemVibrator", stringBuilder.toString());
      } 
      return true;
    } 
    return false;
  }
  
  protected boolean doVibrate(int paramInt, String paramString, VibrationEffect paramVibrationEffect) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mContext : Landroid/content/Context;
    //   4: ifnonnull -> 17
    //   7: ldc 'OplusBaseSystemVibrator'
    //   9: ldc 'doVibrate return!'
    //   11: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   14: pop
    //   15: iconst_0
    //   16: ireturn
    //   17: iconst_0
    //   18: istore #4
    //   20: iload #4
    //   22: istore #5
    //   24: aload_3
    //   25: instanceof android/os/VibrationEffect$Waveform
    //   28: ifeq -> 62
    //   31: aload_3
    //   32: checkcast android/os/VibrationEffect$Waveform
    //   35: astore #6
    //   37: aload #6
    //   39: invokevirtual getRepeatIndex : ()I
    //   42: ifge -> 59
    //   45: iload #4
    //   47: istore #5
    //   49: aload_0
    //   50: iload_1
    //   51: aload #6
    //   53: invokespecial isVirtualKeyVibrate : (ILandroid/os/VibrationEffect$Waveform;)Z
    //   56: ifeq -> 62
    //   59: iconst_1
    //   60: istore #5
    //   62: iload #5
    //   64: istore #4
    //   66: aload_3
    //   67: instanceof android/os/VibrationEffect$Prebaked
    //   70: ifeq -> 94
    //   73: aload_3
    //   74: checkcast android/os/VibrationEffect$Prebaked
    //   77: astore #6
    //   79: iload #5
    //   81: istore #4
    //   83: aload #6
    //   85: invokevirtual getId : ()I
    //   88: ifne -> 94
    //   91: iconst_1
    //   92: istore #4
    //   94: ldc 'oppo.camera.packname'
    //   96: ldc 'default'
    //   98: invokestatic get : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   101: astore #7
    //   103: aload_0
    //   104: getfield mCameraAntiShake : Z
    //   107: ifeq -> 307
    //   110: iload #4
    //   112: ifne -> 307
    //   115: aload #7
    //   117: ldc 'com.oppo.camera'
    //   119: invokevirtual equals : (Ljava/lang/Object;)Z
    //   122: ifeq -> 307
    //   125: aconst_null
    //   126: astore #6
    //   128: aload_0
    //   129: getfield mOppoActivityManager : Landroid/app/OplusActivityManager;
    //   132: invokevirtual getTopActivityComponentName : ()Landroid/content/ComponentName;
    //   135: astore #8
    //   137: aload #8
    //   139: astore #6
    //   141: goto -> 156
    //   144: astore #8
    //   146: ldc 'OplusBaseSystemVibrator'
    //   148: ldc 'get top activity failed.'
    //   150: aload #8
    //   152: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   155: pop
    //   156: aload #6
    //   158: ifnull -> 171
    //   161: aload #6
    //   163: invokevirtual toString : ()Ljava/lang/String;
    //   166: astore #6
    //   168: goto -> 174
    //   171: aconst_null
    //   172: astore #6
    //   174: aload #6
    //   176: ifnull -> 195
    //   179: aload #6
    //   181: aload #7
    //   183: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   186: ifeq -> 195
    //   189: iconst_1
    //   190: istore #5
    //   192: goto -> 198
    //   195: iconst_0
    //   196: istore #5
    //   198: aload_0
    //   199: getfield mKeyguardManager : Landroid/app/KeyguardManager;
    //   202: ifnonnull -> 221
    //   205: aload_0
    //   206: aload_0
    //   207: getfield mContext : Landroid/content/Context;
    //   210: ldc 'keyguard'
    //   212: invokevirtual getSystemService : (Ljava/lang/String;)Ljava/lang/Object;
    //   215: checkcast android/app/KeyguardManager
    //   218: putfield mKeyguardManager : Landroid/app/KeyguardManager;
    //   221: aload_0
    //   222: getfield mKeyguardManager : Landroid/app/KeyguardManager;
    //   225: astore #6
    //   227: aload #6
    //   229: ifnull -> 246
    //   232: aload #6
    //   234: invokevirtual inKeyguardRestrictedInputMode : ()Z
    //   237: ifeq -> 246
    //   240: iconst_1
    //   241: istore #9
    //   243: goto -> 249
    //   246: iconst_0
    //   247: istore #9
    //   249: iload #5
    //   251: ifeq -> 307
    //   254: iload #9
    //   256: ifne -> 307
    //   259: new java/lang/StringBuilder
    //   262: dup
    //   263: invokespecial <init> : ()V
    //   266: astore_2
    //   267: aload_2
    //   268: ldc 'vibrate return because isCameraAppFocus, cameraPkgName = '
    //   270: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   273: pop
    //   274: aload_2
    //   275: aload #7
    //   277: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   280: pop
    //   281: aload_2
    //   282: ldc '; isKeyguradFocus = '
    //   284: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   287: pop
    //   288: aload_2
    //   289: iload #9
    //   291: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   294: pop
    //   295: ldc 'OplusBaseSystemVibrator'
    //   297: aload_2
    //   298: invokevirtual toString : ()Ljava/lang/String;
    //   301: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   304: pop
    //   305: iconst_1
    //   306: ireturn
    //   307: aload_0
    //   308: getfield mLogEnable : Z
    //   311: ifeq -> 471
    //   314: aload_3
    //   315: ifnull -> 327
    //   318: aload_3
    //   319: invokevirtual getDuration : ()J
    //   322: lstore #10
    //   324: goto -> 330
    //   327: lconst_0
    //   328: lstore #10
    //   330: new java/lang/StringBuilder
    //   333: dup
    //   334: invokespecial <init> : ()V
    //   337: astore #6
    //   339: aload #6
    //   341: ldc 'SystemVibrator vibrate is uid= '
    //   343: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   346: pop
    //   347: aload #6
    //   349: iload_1
    //   350: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   353: pop
    //   354: aload #6
    //   356: ldc ',opPkg ='
    //   358: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   361: pop
    //   362: aload #6
    //   364: aload_2
    //   365: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   368: pop
    //   369: aload #6
    //   371: ldc ',duration='
    //   373: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   376: pop
    //   377: aload #6
    //   379: lload #10
    //   381: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   384: pop
    //   385: aload #6
    //   387: ldc ',effect='
    //   389: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   392: pop
    //   393: aload #6
    //   395: aload_3
    //   396: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   399: pop
    //   400: aload #6
    //   402: ldc ',Binder.getCallingPid()='
    //   404: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   407: pop
    //   408: aload #6
    //   410: invokestatic getCallingPid : ()I
    //   413: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   416: pop
    //   417: aload #6
    //   419: invokevirtual toString : ()Ljava/lang/String;
    //   422: astore_2
    //   423: ldc 'OplusBaseSystemVibrator'
    //   425: aload_2
    //   426: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   429: pop
    //   430: iload_1
    //   431: sipush #10000
    //   434: if_icmple -> 471
    //   437: new java/lang/StringBuilder
    //   440: dup
    //   441: invokespecial <init> : ()V
    //   444: astore_2
    //   445: aload_2
    //   446: ldc 'vibrate here dumpStack:  Callers='
    //   448: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   451: pop
    //   452: aload_2
    //   453: iconst_4
    //   454: invokestatic getCallers : (I)Ljava/lang/String;
    //   457: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   460: pop
    //   461: ldc 'OplusBaseSystemVibrator'
    //   463: aload_2
    //   464: invokevirtual toString : ()Ljava/lang/String;
    //   467: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   470: pop
    //   471: iconst_0
    //   472: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #68	-> 0
    //   #69	-> 7
    //   #70	-> 15
    //   #72	-> 17
    //   #73	-> 20
    //   #74	-> 31
    //   #75	-> 37
    //   #76	-> 59
    //   #80	-> 62
    //   #81	-> 73
    //   #82	-> 79
    //   #83	-> 91
    //   #87	-> 94
    //   #88	-> 103
    //   #89	-> 125
    //   #91	-> 128
    //   #94	-> 141
    //   #92	-> 144
    //   #93	-> 146
    //   #95	-> 156
    //   #96	-> 174
    //   #97	-> 198
    //   #98	-> 205
    //   #99	-> 221
    //   #100	-> 249
    //   #101	-> 259
    //   #103	-> 305
    //   #107	-> 307
    //   #108	-> 314
    //   #109	-> 330
    //   #110	-> 408
    //   #109	-> 423
    //   #111	-> 430
    //   #112	-> 437
    //   #116	-> 471
    // Exception table:
    //   from	to	target	type
    //   128	137	144	android/os/RemoteException
  }
  
  public void linearMotorVibrate(int paramInt1, String paramString1, int[] paramArrayOfint, long[] paramArrayOflong, int paramInt2, int paramInt3, String paramString2, AudioAttributes paramAudioAttributes, IBinder paramIBinder) {
    try {
      OplusNativeOneShotVibrationEffect oplusNativeOneShotVibrationEffect;
      OplusNativeWaveformVibrationEffect oplusNativeWaveformVibrationEffect;
      if (paramArrayOfint.length == 1 && paramArrayOflong.length == 1 && paramInt3 == -1) {
        OplusNativeOneShotVibrationEffect oplusNativeOneShotVibrationEffect1 = new OplusNativeOneShotVibrationEffect();
        this(paramArrayOfint[0], paramArrayOflong[0]);
        oplusNativeOneShotVibrationEffect1.setEffectStrength(paramInt2);
        oplusNativeOneShotVibrationEffect = oplusNativeOneShotVibrationEffect1;
      } else {
        OplusNativeWaveformVibrationEffect oplusNativeWaveformVibrationEffect1 = new OplusNativeWaveformVibrationEffect();
        this(paramArrayOflong, (int[])oplusNativeOneShotVibrationEffect, paramInt3);
        oplusNativeWaveformVibrationEffect1.setEffectStrength(paramInt2);
        oplusNativeWaveformVibrationEffect = oplusNativeWaveformVibrationEffect1;
      } 
      try {
        if (doVibrate(paramInt1, paramString1, oplusNativeWaveformVibrationEffect))
          return; 
        boolean bool = InputLog.DEBUG;
        if (bool)
          try {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("linearMotorVibrate uid= ");
            stringBuilder.append(paramInt1);
            stringBuilder.append(",opPkg =");
            stringBuilder.append(paramString1);
            stringBuilder.append(",effect=");
            stringBuilder.append(oplusNativeWaveformVibrationEffect);
            stringBuilder.append(",attributes=");
            stringBuilder.append(paramAudioAttributes);
            Log.i("OplusBaseSystemVibrator", stringBuilder.toString());
          } catch (IllegalArgumentException null) {
          
          } catch (Exception null) {} 
        vibrate(paramInt1, (String)exception, oplusNativeWaveformVibrationEffect, paramString2, paramAudioAttributes, paramIBinder);
      } catch (IllegalArgumentException null) {
        Log.e("OplusBaseSystemVibrator", "Failed to linearMotorVibrate", exception);
      } catch (Exception null) {}
    } catch (IllegalArgumentException null) {
    
    } catch (Exception exception) {
      Log.e("OplusBaseSystemVibrator", "linearMotorVibrate failed", exception);
    } 
    Log.e("OplusBaseSystemVibrator", "Failed to linearMotorVibrate", exception);
  }
  
  protected void vibrate(int paramInt, String paramString1, VibrationEffect paramVibrationEffect, String paramString2, AudioAttributes paramAudioAttributes, IBinder paramIBinder) {}
}
