package android.os;

import java.lang.ref.WeakReference;

public class Registrant {
  WeakReference refH;
  
  Object userObj;
  
  int what;
  
  public Registrant(Handler paramHandler, int paramInt, Object paramObject) {
    this.refH = new WeakReference<>(paramHandler);
    this.what = paramInt;
    this.userObj = paramObject;
  }
  
  public void clear() {
    this.refH = null;
    this.userObj = null;
  }
  
  public void notifyRegistrant() {
    internalNotifyRegistrant(null, null);
  }
  
  public void notifyResult(Object paramObject) {
    internalNotifyRegistrant(paramObject, null);
  }
  
  public void notifyException(Throwable paramThrowable) {
    internalNotifyRegistrant(null, paramThrowable);
  }
  
  public void notifyRegistrant(AsyncResult paramAsyncResult) {
    internalNotifyRegistrant(paramAsyncResult.result, paramAsyncResult.exception);
  }
  
  void internalNotifyRegistrant(Object paramObject, Throwable paramThrowable) {
    Handler handler = getHandler();
    if (handler == null) {
      clear();
    } else {
      Message message = Message.obtain();
      message.what = this.what;
      message.obj = new AsyncResult(this.userObj, paramObject, paramThrowable);
      handler.sendMessage(message);
    } 
  }
  
  public Message messageForRegistrant() {
    Handler handler = getHandler();
    if (handler == null) {
      clear();
      return null;
    } 
    Message message = handler.obtainMessage();
    message.what = this.what;
    message.obj = this.userObj;
    return message;
  }
  
  public Handler getHandler() {
    WeakReference<Handler> weakReference = this.refH;
    if (weakReference == null)
      return null; 
    return weakReference.get();
  }
}
