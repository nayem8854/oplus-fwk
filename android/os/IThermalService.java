package android.os;

public interface IThermalService extends IInterface {
  CoolingDevice[] getCurrentCoolingDevices() throws RemoteException;
  
  CoolingDevice[] getCurrentCoolingDevicesWithType(int paramInt) throws RemoteException;
  
  Temperature[] getCurrentTemperatures() throws RemoteException;
  
  Temperature[] getCurrentTemperaturesWithType(int paramInt) throws RemoteException;
  
  int getCurrentThermalStatus() throws RemoteException;
  
  float getThermalHeadroom(int paramInt) throws RemoteException;
  
  boolean registerThermalEventListener(IThermalEventListener paramIThermalEventListener) throws RemoteException;
  
  boolean registerThermalEventListenerWithType(IThermalEventListener paramIThermalEventListener, int paramInt) throws RemoteException;
  
  boolean registerThermalStatusListener(IThermalStatusListener paramIThermalStatusListener) throws RemoteException;
  
  boolean unregisterThermalEventListener(IThermalEventListener paramIThermalEventListener) throws RemoteException;
  
  boolean unregisterThermalStatusListener(IThermalStatusListener paramIThermalStatusListener) throws RemoteException;
  
  class Default implements IThermalService {
    public boolean registerThermalEventListener(IThermalEventListener param1IThermalEventListener) throws RemoteException {
      return false;
    }
    
    public boolean registerThermalEventListenerWithType(IThermalEventListener param1IThermalEventListener, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean unregisterThermalEventListener(IThermalEventListener param1IThermalEventListener) throws RemoteException {
      return false;
    }
    
    public Temperature[] getCurrentTemperatures() throws RemoteException {
      return null;
    }
    
    public Temperature[] getCurrentTemperaturesWithType(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean registerThermalStatusListener(IThermalStatusListener param1IThermalStatusListener) throws RemoteException {
      return false;
    }
    
    public boolean unregisterThermalStatusListener(IThermalStatusListener param1IThermalStatusListener) throws RemoteException {
      return false;
    }
    
    public int getCurrentThermalStatus() throws RemoteException {
      return 0;
    }
    
    public CoolingDevice[] getCurrentCoolingDevices() throws RemoteException {
      return null;
    }
    
    public CoolingDevice[] getCurrentCoolingDevicesWithType(int param1Int) throws RemoteException {
      return null;
    }
    
    public float getThermalHeadroom(int param1Int) throws RemoteException {
      return 0.0F;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IThermalService {
    private static final String DESCRIPTOR = "android.os.IThermalService";
    
    static final int TRANSACTION_getCurrentCoolingDevices = 9;
    
    static final int TRANSACTION_getCurrentCoolingDevicesWithType = 10;
    
    static final int TRANSACTION_getCurrentTemperatures = 4;
    
    static final int TRANSACTION_getCurrentTemperaturesWithType = 5;
    
    static final int TRANSACTION_getCurrentThermalStatus = 8;
    
    static final int TRANSACTION_getThermalHeadroom = 11;
    
    static final int TRANSACTION_registerThermalEventListener = 1;
    
    static final int TRANSACTION_registerThermalEventListenerWithType = 2;
    
    static final int TRANSACTION_registerThermalStatusListener = 6;
    
    static final int TRANSACTION_unregisterThermalEventListener = 3;
    
    static final int TRANSACTION_unregisterThermalStatusListener = 7;
    
    public Stub() {
      attachInterface(this, "android.os.IThermalService");
    }
    
    public static IThermalService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IThermalService");
      if (iInterface != null && iInterface instanceof IThermalService)
        return (IThermalService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "getThermalHeadroom";
        case 10:
          return "getCurrentCoolingDevicesWithType";
        case 9:
          return "getCurrentCoolingDevices";
        case 8:
          return "getCurrentThermalStatus";
        case 7:
          return "unregisterThermalStatusListener";
        case 6:
          return "registerThermalStatusListener";
        case 5:
          return "getCurrentTemperaturesWithType";
        case 4:
          return "getCurrentTemperatures";
        case 3:
          return "unregisterThermalEventListener";
        case 2:
          return "registerThermalEventListenerWithType";
        case 1:
          break;
      } 
      return "registerThermalEventListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        CoolingDevice[] arrayOfCoolingDevice;
        IThermalStatusListener iThermalStatusListener;
        Temperature[] arrayOfTemperature;
        float f;
        IThermalEventListener iThermalEventListener2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.os.IThermalService");
            param1Int1 = param1Parcel1.readInt();
            f = getThermalHeadroom(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeFloat(f);
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.os.IThermalService");
            param1Int1 = param1Parcel1.readInt();
            arrayOfCoolingDevice = getCurrentCoolingDevicesWithType(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfCoolingDevice, 1);
            return true;
          case 9:
            arrayOfCoolingDevice.enforceInterface("android.os.IThermalService");
            arrayOfCoolingDevice = getCurrentCoolingDevices();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfCoolingDevice, 1);
            return true;
          case 8:
            arrayOfCoolingDevice.enforceInterface("android.os.IThermalService");
            param1Int1 = getCurrentThermalStatus();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 7:
            arrayOfCoolingDevice.enforceInterface("android.os.IThermalService");
            iThermalStatusListener = IThermalStatusListener.Stub.asInterface(arrayOfCoolingDevice.readStrongBinder());
            bool3 = unregisterThermalStatusListener(iThermalStatusListener);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 6:
            iThermalStatusListener.enforceInterface("android.os.IThermalService");
            iThermalStatusListener = IThermalStatusListener.Stub.asInterface(iThermalStatusListener.readStrongBinder());
            bool3 = registerThermalStatusListener(iThermalStatusListener);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 5:
            iThermalStatusListener.enforceInterface("android.os.IThermalService");
            j = iThermalStatusListener.readInt();
            arrayOfTemperature = getCurrentTemperaturesWithType(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfTemperature, 1);
            return true;
          case 4:
            arrayOfTemperature.enforceInterface("android.os.IThermalService");
            arrayOfTemperature = getCurrentTemperatures();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfTemperature, 1);
            return true;
          case 3:
            arrayOfTemperature.enforceInterface("android.os.IThermalService");
            iThermalEventListener1 = IThermalEventListener.Stub.asInterface(arrayOfTemperature.readStrongBinder());
            bool2 = unregisterThermalEventListener(iThermalEventListener1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 2:
            iThermalEventListener1.enforceInterface("android.os.IThermalService");
            iThermalEventListener2 = IThermalEventListener.Stub.asInterface(iThermalEventListener1.readStrongBinder());
            i = iThermalEventListener1.readInt();
            bool1 = registerThermalEventListenerWithType(iThermalEventListener2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        iThermalEventListener1.enforceInterface("android.os.IThermalService");
        IThermalEventListener iThermalEventListener1 = IThermalEventListener.Stub.asInterface(iThermalEventListener1.readStrongBinder());
        boolean bool1 = registerThermalEventListener(iThermalEventListener1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.os.IThermalService");
      return true;
    }
    
    private static class Proxy implements IThermalService {
      public static IThermalService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IThermalService";
      }
      
      public boolean registerThermalEventListener(IThermalEventListener param2IThermalEventListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IThermalService");
          if (param2IThermalEventListener != null) {
            iBinder = param2IThermalEventListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IThermalService.Stub.getDefaultImpl() != null) {
            bool1 = IThermalService.Stub.getDefaultImpl().registerThermalEventListener(param2IThermalEventListener);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerThermalEventListenerWithType(IThermalEventListener param2IThermalEventListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IThermalService");
          if (param2IThermalEventListener != null) {
            iBinder = param2IThermalEventListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IThermalService.Stub.getDefaultImpl() != null) {
            bool1 = IThermalService.Stub.getDefaultImpl().registerThermalEventListenerWithType(param2IThermalEventListener, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unregisterThermalEventListener(IThermalEventListener param2IThermalEventListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IThermalService");
          if (param2IThermalEventListener != null) {
            iBinder = param2IThermalEventListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IThermalService.Stub.getDefaultImpl() != null) {
            bool1 = IThermalService.Stub.getDefaultImpl().unregisterThermalEventListener(param2IThermalEventListener);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Temperature[] getCurrentTemperatures() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IThermalService");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IThermalService.Stub.getDefaultImpl() != null)
            return IThermalService.Stub.getDefaultImpl().getCurrentTemperatures(); 
          parcel2.readException();
          return parcel2.<Temperature>createTypedArray(Temperature.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Temperature[] getCurrentTemperaturesWithType(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IThermalService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IThermalService.Stub.getDefaultImpl() != null)
            return IThermalService.Stub.getDefaultImpl().getCurrentTemperaturesWithType(param2Int); 
          parcel2.readException();
          return parcel2.<Temperature>createTypedArray(Temperature.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerThermalStatusListener(IThermalStatusListener param2IThermalStatusListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IThermalService");
          if (param2IThermalStatusListener != null) {
            iBinder = param2IThermalStatusListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IThermalService.Stub.getDefaultImpl() != null) {
            bool1 = IThermalService.Stub.getDefaultImpl().registerThermalStatusListener(param2IThermalStatusListener);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unregisterThermalStatusListener(IThermalStatusListener param2IThermalStatusListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IThermalService");
          if (param2IThermalStatusListener != null) {
            iBinder = param2IThermalStatusListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IThermalService.Stub.getDefaultImpl() != null) {
            bool1 = IThermalService.Stub.getDefaultImpl().unregisterThermalStatusListener(param2IThermalStatusListener);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCurrentThermalStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IThermalService");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IThermalService.Stub.getDefaultImpl() != null)
            return IThermalService.Stub.getDefaultImpl().getCurrentThermalStatus(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CoolingDevice[] getCurrentCoolingDevices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IThermalService");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IThermalService.Stub.getDefaultImpl() != null)
            return IThermalService.Stub.getDefaultImpl().getCurrentCoolingDevices(); 
          parcel2.readException();
          return parcel2.<CoolingDevice>createTypedArray(CoolingDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CoolingDevice[] getCurrentCoolingDevicesWithType(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IThermalService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IThermalService.Stub.getDefaultImpl() != null)
            return IThermalService.Stub.getDefaultImpl().getCurrentCoolingDevicesWithType(param2Int); 
          parcel2.readException();
          return parcel2.<CoolingDevice>createTypedArray(CoolingDevice.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public float getThermalHeadroom(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IThermalService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IThermalService.Stub.getDefaultImpl() != null)
            return IThermalService.Stub.getDefaultImpl().getThermalHeadroom(param2Int); 
          parcel2.readException();
          return parcel2.readFloat();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IThermalService param1IThermalService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IThermalService != null) {
          Proxy.sDefaultImpl = param1IThermalService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IThermalService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
