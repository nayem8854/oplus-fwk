package android.os;

public final class CancellationSignal {
  private boolean mCancelInProgress;
  
  private boolean mIsCanceled;
  
  private OnCancelListener mOnCancelListener;
  
  private ICancellationSignal mRemote;
  
  public boolean isCanceled() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIsCanceled : Z
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #46	-> 0
    //   #47	-> 2
    //   #48	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  public void throwIfCanceled() {
    if (!isCanceled())
      return; 
    throw new OperationCanceledException();
  }
  
  public void cancel() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIsCanceled : Z
    //   6: ifeq -> 12
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: aload_0
    //   13: iconst_1
    //   14: putfield mIsCanceled : Z
    //   17: aload_0
    //   18: iconst_1
    //   19: putfield mCancelInProgress : Z
    //   22: aload_0
    //   23: getfield mOnCancelListener : Landroid/os/CancellationSignal$OnCancelListener;
    //   26: astore_1
    //   27: aload_0
    //   28: getfield mRemote : Landroid/os/ICancellationSignal;
    //   31: astore_2
    //   32: aload_0
    //   33: monitorexit
    //   34: aload_1
    //   35: ifnull -> 51
    //   38: aload_1
    //   39: invokeinterface onCancel : ()V
    //   44: goto -> 51
    //   47: astore_1
    //   48: goto -> 64
    //   51: aload_2
    //   52: ifnull -> 85
    //   55: aload_2
    //   56: invokeinterface cancel : ()V
    //   61: goto -> 85
    //   64: aload_0
    //   65: monitorenter
    //   66: aload_0
    //   67: iconst_0
    //   68: putfield mCancelInProgress : Z
    //   71: aload_0
    //   72: invokevirtual notifyAll : ()V
    //   75: aload_0
    //   76: monitorexit
    //   77: aload_1
    //   78: athrow
    //   79: astore_1
    //   80: aload_0
    //   81: monitorexit
    //   82: aload_1
    //   83: athrow
    //   84: astore_1
    //   85: aload_0
    //   86: monitorenter
    //   87: aload_0
    //   88: iconst_0
    //   89: putfield mCancelInProgress : Z
    //   92: aload_0
    //   93: invokevirtual notifyAll : ()V
    //   96: aload_0
    //   97: monitorexit
    //   98: return
    //   99: astore_1
    //   100: aload_0
    //   101: monitorexit
    //   102: aload_1
    //   103: athrow
    //   104: astore_1
    //   105: aload_0
    //   106: monitorexit
    //   107: aload_1
    //   108: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #69	-> 0
    //   #70	-> 2
    //   #71	-> 9
    //   #73	-> 12
    //   #74	-> 17
    //   #75	-> 22
    //   #76	-> 27
    //   #77	-> 32
    //   #80	-> 34
    //   #81	-> 38
    //   #90	-> 47
    //   #83	-> 51
    //   #85	-> 55
    //   #87	-> 61
    //   #90	-> 64
    //   #91	-> 66
    //   #92	-> 71
    //   #93	-> 75
    //   #94	-> 77
    //   #93	-> 79
    //   #86	-> 84
    //   #90	-> 85
    //   #91	-> 87
    //   #92	-> 92
    //   #93	-> 96
    //   #94	-> 98
    //   #95	-> 98
    //   #93	-> 99
    //   #77	-> 104
    // Exception table:
    //   from	to	target	type
    //   2	9	104	finally
    //   9	11	104	finally
    //   12	17	104	finally
    //   17	22	104	finally
    //   22	27	104	finally
    //   27	32	104	finally
    //   32	34	104	finally
    //   38	44	47	finally
    //   55	61	84	android/os/RemoteException
    //   55	61	47	finally
    //   66	71	79	finally
    //   71	75	79	finally
    //   75	77	79	finally
    //   80	82	79	finally
    //   87	92	99	finally
    //   92	96	99	finally
    //   96	98	99	finally
    //   100	102	99	finally
    //   105	107	104	finally
  }
  
  public void setOnCancelListener(OnCancelListener paramOnCancelListener) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial waitForCancelFinishedLocked : ()V
    //   6: aload_0
    //   7: getfield mOnCancelListener : Landroid/os/CancellationSignal$OnCancelListener;
    //   10: aload_1
    //   11: if_acmpne -> 17
    //   14: aload_0
    //   15: monitorexit
    //   16: return
    //   17: aload_0
    //   18: aload_1
    //   19: putfield mOnCancelListener : Landroid/os/CancellationSignal$OnCancelListener;
    //   22: aload_0
    //   23: getfield mIsCanceled : Z
    //   26: ifeq -> 45
    //   29: aload_1
    //   30: ifnonnull -> 36
    //   33: goto -> 45
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_1
    //   39: invokeinterface onCancel : ()V
    //   44: return
    //   45: aload_0
    //   46: monitorexit
    //   47: return
    //   48: astore_1
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_1
    //   52: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #114	-> 0
    //   #115	-> 2
    //   #117	-> 6
    //   #118	-> 14
    //   #120	-> 17
    //   #121	-> 22
    //   #124	-> 36
    //   #125	-> 38
    //   #126	-> 44
    //   #122	-> 45
    //   #124	-> 48
    // Exception table:
    //   from	to	target	type
    //   2	6	48	finally
    //   6	14	48	finally
    //   14	16	48	finally
    //   17	22	48	finally
    //   22	29	48	finally
    //   36	38	48	finally
    //   45	47	48	finally
    //   49	51	48	finally
  }
  
  public void setRemote(ICancellationSignal paramICancellationSignal) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial waitForCancelFinishedLocked : ()V
    //   6: aload_0
    //   7: getfield mRemote : Landroid/os/ICancellationSignal;
    //   10: aload_1
    //   11: if_acmpne -> 17
    //   14: aload_0
    //   15: monitorexit
    //   16: return
    //   17: aload_0
    //   18: aload_1
    //   19: putfield mRemote : Landroid/os/ICancellationSignal;
    //   22: aload_0
    //   23: getfield mIsCanceled : Z
    //   26: ifeq -> 49
    //   29: aload_1
    //   30: ifnonnull -> 36
    //   33: goto -> 49
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_1
    //   39: invokeinterface cancel : ()V
    //   44: goto -> 48
    //   47: astore_1
    //   48: return
    //   49: aload_0
    //   50: monitorexit
    //   51: return
    //   52: astore_1
    //   53: aload_0
    //   54: monitorexit
    //   55: aload_1
    //   56: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #142	-> 0
    //   #143	-> 2
    //   #145	-> 6
    //   #146	-> 14
    //   #148	-> 17
    //   #149	-> 22
    //   #152	-> 36
    //   #154	-> 38
    //   #156	-> 44
    //   #155	-> 47
    //   #157	-> 48
    //   #150	-> 49
    //   #152	-> 52
    // Exception table:
    //   from	to	target	type
    //   2	6	52	finally
    //   6	14	52	finally
    //   14	16	52	finally
    //   17	22	52	finally
    //   22	29	52	finally
    //   36	38	52	finally
    //   38	44	47	android/os/RemoteException
    //   49	51	52	finally
    //   53	55	52	finally
  }
  
  private void waitForCancelFinishedLocked() {
    while (this.mCancelInProgress) {
      try {
        wait();
      } catch (InterruptedException interruptedException) {}
    } 
  }
  
  public static ICancellationSignal createTransport() {
    return new Transport();
  }
  
  public static CancellationSignal fromTransport(ICancellationSignal paramICancellationSignal) {
    if (paramICancellationSignal instanceof Transport)
      return ((Transport)paramICancellationSignal).mCancellationSignal; 
    return null;
  }
  
  public static interface OnCancelListener {
    void onCancel();
  }
  
  class Transport extends ICancellationSignal.Stub {
    private Transport() {}
    
    final CancellationSignal mCancellationSignal = new CancellationSignal();
    
    public void cancel() throws RemoteException {
      this.mCancellationSignal.cancel();
    }
  }
}
