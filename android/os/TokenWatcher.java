package android.os;

import android.util.Log;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Set;
import java.util.WeakHashMap;

public abstract class TokenWatcher {
  private volatile boolean mAcquired;
  
  private Handler mHandler;
  
  private int mNotificationQueue;
  
  private Runnable mNotificationTask;
  
  private String mTag;
  
  private WeakHashMap<IBinder, Death> mTokens;
  
  public TokenWatcher(Handler paramHandler, String paramString) {
    this.mNotificationTask = new Runnable() {
        final TokenWatcher this$0;
        
        public void run() {
          synchronized (TokenWatcher.this.mTokens) {
            int i = TokenWatcher.this.mNotificationQueue;
            TokenWatcher.access$102(TokenWatcher.this, -1);
            if (i == 1) {
              TokenWatcher.this.acquired();
            } else if (i == 0) {
              TokenWatcher.this.released();
            } 
            return;
          } 
        }
      };
    this.mTokens = new WeakHashMap<>();
    this.mNotificationQueue = -1;
    this.mAcquired = false;
    this.mHandler = paramHandler;
    if (paramString == null)
      paramString = "TokenWatcher"; 
    this.mTag = paramString;
  }
  
  public void acquire(IBinder paramIBinder, String paramString) {
    synchronized (this.mTokens) {
      if (this.mTokens.containsKey(paramIBinder))
        return; 
      int i = this.mTokens.size();
      Death death = new Death();
      this(this, paramIBinder, paramString);
      try {
        paramIBinder.linkToDeath(death, 0);
        this.mTokens.put(paramIBinder, death);
        if (i == 0 && !this.mAcquired) {
          sendNotificationLocked(true);
          this.mAcquired = true;
        } 
        return;
      } catch (RemoteException remoteException) {
        return;
      } 
    } 
  }
  
  public void cleanup(IBinder paramIBinder, boolean paramBoolean) {
    synchronized (this.mTokens) {
      Death death = this.mTokens.remove(paramIBinder);
      if (paramBoolean && death != null) {
        death.token.unlinkToDeath(death, 0);
        death.token = null;
      } 
      if (this.mTokens.size() == 0 && this.mAcquired) {
        sendNotificationLocked(false);
        this.mAcquired = false;
      } 
      return;
    } 
  }
  
  public void release(IBinder paramIBinder) {
    cleanup(paramIBinder, true);
  }
  
  public boolean isAcquired() {
    synchronized (this.mTokens) {
      return this.mAcquired;
    } 
  }
  
  public void dump() {
    ArrayList<String> arrayList = dumpInternal();
    for (String str : arrayList)
      Log.i(this.mTag, str); 
  }
  
  public void dump(PrintWriter paramPrintWriter) {
    ArrayList<String> arrayList = dumpInternal();
    for (String str : arrayList)
      paramPrintWriter.println(str); 
  }
  
  private ArrayList<String> dumpInternal() {
    null = new ArrayList();
    synchronized (this.mTokens) {
      Set<IBinder> set = this.mTokens.keySet();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Token count: ");
      stringBuilder.append(this.mTokens.size());
      null.add(stringBuilder.toString());
      byte b = 0;
      for (IBinder iBinder : set) {
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("[");
        stringBuilder1.append(b);
        stringBuilder1.append("] ");
        stringBuilder1.append(((Death)this.mTokens.get(iBinder)).tag);
        stringBuilder1.append(" - ");
        stringBuilder1.append(iBinder);
        null.add(stringBuilder1.toString());
        b++;
      } 
      return null;
    } 
  }
  
  private void sendNotificationLocked(int paramBoolean) {
    int i = this.mNotificationQueue;
    if (i == -1) {
      this.mNotificationQueue = paramBoolean;
      this.mHandler.post(this.mNotificationTask);
    } else if (i != paramBoolean) {
      this.mNotificationQueue = -1;
      this.mHandler.removeCallbacks(this.mNotificationTask);
    } 
  }
  
  public abstract void acquired();
  
  public abstract void released();
  
  class Death implements IBinder.DeathRecipient {
    String tag;
    
    final TokenWatcher this$0;
    
    IBinder token;
    
    Death(IBinder param1IBinder, String param1String) {
      this.token = param1IBinder;
      this.tag = param1String;
    }
    
    public void binderDied() {
      TokenWatcher.this.cleanup(this.token, false);
    }
    
    protected void finalize() throws Throwable {
      try {
        if (this.token != null) {
          String str = TokenWatcher.this.mTag;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("cleaning up leaked reference: ");
          stringBuilder.append(this.tag);
          Log.w(str, stringBuilder.toString());
          TokenWatcher.this.release(this.token);
        } 
        return;
      } finally {
        super.finalize();
      } 
    }
  }
}
