package android.os;

import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteOrder;
import libcore.io.IoBridge;
import libcore.io.IoUtils;
import libcore.io.Memory;
import libcore.io.Streams;
import libcore.util.ArrayUtils;

@Deprecated
public class FileBridge extends Thread {
  private FileDescriptor mTarget;
  
  private final FileDescriptor mServer = new FileDescriptor();
  
  private volatile boolean mClosed;
  
  private final FileDescriptor mClient = new FileDescriptor();
  
  private static final String TAG = "FileBridge";
  
  private static final int MSG_LENGTH = 8;
  
  private static final int CMD_WRITE = 1;
  
  private static final int CMD_FSYNC = 2;
  
  private static final int CMD_CLOSE = 3;
  
  public FileBridge() {
    try {
      Os.socketpair(OsConstants.AF_UNIX, OsConstants.SOCK_STREAM, 0, this.mServer, this.mClient);
      return;
    } catch (ErrnoException errnoException) {
      throw new RuntimeException("Failed to create bridge");
    } 
  }
  
  public boolean isClosed() {
    return this.mClosed;
  }
  
  public void forceClose() {
    IoUtils.closeQuietly(this.mTarget);
    IoUtils.closeQuietly(this.mServer);
    IoUtils.closeQuietly(this.mClient);
    this.mClosed = true;
  }
  
  public void setTargetFile(FileDescriptor paramFileDescriptor) {
    this.mTarget = paramFileDescriptor;
  }
  
  public FileDescriptor getClientSocket() {
    return this.mClient;
  }
  
  public void run() {
    byte[] arrayOfByte = new byte[8192];
    try {
      while (IoBridge.read(this.mServer, arrayOfByte, 0, 8) == 8) {
        IOException iOException;
        int i = Memory.peekInt(arrayOfByte, 0, ByteOrder.BIG_ENDIAN);
        if (i == 1) {
          i = Memory.peekInt(arrayOfByte, 4, ByteOrder.BIG_ENDIAN);
          while (i > 0) {
            int j = IoBridge.read(this.mServer, arrayOfByte, 0, Math.min(arrayOfByte.length, i));
            if (j != -1) {
              IoBridge.write(this.mTarget, arrayOfByte, 0, j);
              i -= j;
              continue;
            } 
            iOException = new IOException();
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Unexpected EOF; still expected ");
            stringBuilder.append(i);
            stringBuilder.append(" bytes");
            this(stringBuilder.toString());
            throw iOException;
          } 
          continue;
        } 
        if (i == 2) {
          Os.fsync(this.mTarget);
          IoBridge.write(this.mServer, (byte[])iOException, 0, 8);
          continue;
        } 
        if (i == 3) {
          Os.fsync(this.mTarget);
          Os.close(this.mTarget);
          this.mClosed = true;
          IoBridge.write(this.mServer, (byte[])iOException, 0, 8);
          break;
        } 
      } 
      forceClose();
    } catch (ErrnoException|IOException errnoException) {
      Log.wtf("FileBridge", "Failed during bridge", (Throwable)errnoException);
      forceClose();
    } finally {}
  }
  
  public static class FileBridgeOutputStream extends OutputStream {
    private final FileDescriptor mClient;
    
    private final ParcelFileDescriptor mClientPfd;
    
    private final byte[] mTemp = new byte[8];
    
    public FileBridgeOutputStream(ParcelFileDescriptor param1ParcelFileDescriptor) {
      this.mClientPfd = param1ParcelFileDescriptor;
      this.mClient = param1ParcelFileDescriptor.getFileDescriptor();
    }
    
    public FileBridgeOutputStream(FileDescriptor param1FileDescriptor) {
      this.mClientPfd = null;
      this.mClient = param1FileDescriptor;
    }
    
    public void close() throws IOException {
      try {
        writeCommandAndBlock(3, "close()");
        return;
      } finally {
        IoBridge.closeAndSignalBlockedThreads(this.mClient);
        IoUtils.closeQuietly(this.mClientPfd);
      } 
    }
    
    public void fsync() throws IOException {
      writeCommandAndBlock(2, "fsync()");
    }
    
    private void writeCommandAndBlock(int param1Int, String param1String) throws IOException {
      Memory.pokeInt(this.mTemp, 0, param1Int, ByteOrder.BIG_ENDIAN);
      IoBridge.write(this.mClient, this.mTemp, 0, 8);
      if (IoBridge.read(this.mClient, this.mTemp, 0, 8) == 8 && 
        Memory.peekInt(this.mTemp, 0, ByteOrder.BIG_ENDIAN) == param1Int)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to execute ");
      stringBuilder.append(param1String);
      stringBuilder.append(" across bridge");
      throw new IOException(stringBuilder.toString());
    }
    
    public void write(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      ArrayUtils.throwsIfOutOfBounds(param1ArrayOfbyte.length, param1Int1, param1Int2);
      Memory.pokeInt(this.mTemp, 0, 1, ByteOrder.BIG_ENDIAN);
      Memory.pokeInt(this.mTemp, 4, param1Int2, ByteOrder.BIG_ENDIAN);
      IoBridge.write(this.mClient, this.mTemp, 0, 8);
      IoBridge.write(this.mClient, param1ArrayOfbyte, param1Int1, param1Int2);
    }
    
    public void write(int param1Int) throws IOException {
      Streams.writeSingleByte(this, param1Int);
    }
  }
}
