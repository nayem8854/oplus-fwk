package android.os;

import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.io.FileDescriptor;
import java.nio.ByteBuffer;
import java.nio.DirectByteBuffer;
import java.util.ArrayList;
import java.util.List;

public final class HidlMemoryUtil {
  private static final String TAG = "HidlMemoryUtil";
  
  public static HidlMemory byteArrayToHidlMemory(byte[] paramArrayOfbyte) {
    return byteArrayToHidlMemory(paramArrayOfbyte, null);
  }
  
  public static HidlMemory byteArrayToHidlMemory(byte[] paramArrayOfbyte, String paramString) {
    Preconditions.checkNotNull(paramArrayOfbyte);
    if (paramArrayOfbyte.length == 0)
      return new HidlMemory("ashmem", 0L, null); 
    if (paramString == null)
      paramString = ""; 
    try {
      SharedMemory sharedMemory = SharedMemory.create(paramString, paramArrayOfbyte.length);
      ByteBuffer byteBuffer = sharedMemory.mapReadWrite();
      byteBuffer.put(paramArrayOfbyte);
      SharedMemory.unmap(byteBuffer);
      return sharedMemoryToHidlMemory(sharedMemory);
    } catch (ErrnoException errnoException) {
      throw new RuntimeException(errnoException);
    } 
  }
  
  public static HidlMemory byteListToHidlMemory(List<Byte> paramList) {
    return byteListToHidlMemory(paramList, null);
  }
  
  public static HidlMemory byteListToHidlMemory(List<Byte> paramList, String paramString) {
    Preconditions.checkNotNull(paramList);
    if (paramList.isEmpty())
      return new HidlMemory("ashmem", 0L, null); 
    if (paramString == null)
      paramString = ""; 
    try {
      SharedMemory sharedMemory = SharedMemory.create(paramString, paramList.size());
      ByteBuffer byteBuffer = sharedMemory.mapReadWrite();
      for (Byte byte_ : paramList)
        byteBuffer.put(byte_.byteValue()); 
      SharedMemory.unmap(byteBuffer);
      return sharedMemoryToHidlMemory(sharedMemory);
    } catch (ErrnoException errnoException) {
      throw new RuntimeException(errnoException);
    } 
  }
  
  public static byte[] hidlMemoryToByteArray(HidlMemory paramHidlMemory) {
    boolean bool;
    Preconditions.checkNotNull(paramHidlMemory);
    Preconditions.checkArgumentInRange(paramHidlMemory.getSize(), 0L, 2147483647L, "Memory size");
    if (paramHidlMemory.getSize() == 0L || paramHidlMemory.getName().equals("ashmem")) {
      bool = true;
    } else {
      bool = false;
    } 
    String str = paramHidlMemory.getName();
    Preconditions.checkArgument(bool, "Unsupported memory type: %s", new Object[] { str });
    if (paramHidlMemory.getSize() == 0L)
      return new byte[0]; 
    ByteBuffer byteBuffer = getBuffer(paramHidlMemory);
    byte[] arrayOfByte = new byte[byteBuffer.remaining()];
    byteBuffer.get(arrayOfByte);
    return arrayOfByte;
  }
  
  public static ArrayList<Byte> hidlMemoryToByteList(HidlMemory paramHidlMemory) {
    boolean bool;
    Preconditions.checkNotNull(paramHidlMemory);
    Preconditions.checkArgumentInRange(paramHidlMemory.getSize(), 0L, 2147483647L, "Memory size");
    if (paramHidlMemory.getSize() == 0L || paramHidlMemory.getName().equals("ashmem")) {
      bool = true;
    } else {
      bool = false;
    } 
    String str = paramHidlMemory.getName();
    Preconditions.checkArgument(bool, "Unsupported memory type: %s", new Object[] { str });
    if (paramHidlMemory.getSize() == 0L)
      return new ArrayList<>(); 
    ByteBuffer byteBuffer = getBuffer(paramHidlMemory);
    ArrayList<Byte> arrayList = new ArrayList(byteBuffer.remaining());
    while (byteBuffer.hasRemaining())
      arrayList.add(Byte.valueOf(byteBuffer.get())); 
    return arrayList;
  }
  
  public static HidlMemory sharedMemoryToHidlMemory(SharedMemory paramSharedMemory) {
    if (paramSharedMemory == null)
      return new HidlMemory("ashmem", 0L, null); 
    return fileDescriptorToHidlMemory(paramSharedMemory.getFileDescriptor(), paramSharedMemory.getSize());
  }
  
  public static HidlMemory fileDescriptorToHidlMemory(FileDescriptor paramFileDescriptor, int paramInt) {
    boolean bool;
    if (paramFileDescriptor != null || paramInt == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkArgument(bool);
    if (paramFileDescriptor == null)
      return new HidlMemory("ashmem", 0L, null); 
    NativeHandle nativeHandle = new NativeHandle(paramFileDescriptor, true);
    return new HidlMemory("ashmem", paramInt, nativeHandle);
  }
  
  private static ByteBuffer getBuffer(HidlMemory paramHidlMemory) {
    try {
      int i = (int)paramHidlMemory.getSize();
      if (i == 0)
        return ByteBuffer.wrap(new byte[0]); 
      NativeHandle nativeHandle = paramHidlMemory.getHandle();
      long l = Os.mmap(0L, i, OsConstants.PROT_READ, OsConstants.MAP_SHARED, nativeHandle.getFileDescriptor(), 0L);
      FileDescriptor fileDescriptor = nativeHandle.getFileDescriptor();
      _$$Lambda$HidlMemoryUtil$ymiTJHj4VkgK_My_r0e1J5wmQVI _$$Lambda$HidlMemoryUtil$ymiTJHj4VkgK_My_r0e1J5wmQVI = new _$$Lambda$HidlMemoryUtil$ymiTJHj4VkgK_My_r0e1J5wmQVI();
      this(l, i);
      return new DirectByteBuffer(i, l, fileDescriptor, _$$Lambda$HidlMemoryUtil$ymiTJHj4VkgK_My_r0e1J5wmQVI, true);
    } catch (ErrnoException errnoException) {
      throw new RuntimeException(errnoException);
    } 
  }
}
