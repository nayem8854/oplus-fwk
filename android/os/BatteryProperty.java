package android.os;

public class BatteryProperty implements Parcelable {
  public BatteryProperty() {
    this.mValueLong = Long.MIN_VALUE;
  }
  
  public long getLong() {
    return this.mValueLong;
  }
  
  public void setLong(long paramLong) {
    this.mValueLong = paramLong;
  }
  
  private BatteryProperty(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mValueLong = paramParcel.readLong();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.mValueLong);
  }
  
  public static final Parcelable.Creator<BatteryProperty> CREATOR = new Parcelable.Creator<BatteryProperty>() {
      public BatteryProperty createFromParcel(Parcel param1Parcel) {
        return new BatteryProperty(param1Parcel);
      }
      
      public BatteryProperty[] newArray(int param1Int) {
        return new BatteryProperty[param1Int];
      }
    };
  
  private long mValueLong;
  
  public int describeContents() {
    return 0;
  }
}
