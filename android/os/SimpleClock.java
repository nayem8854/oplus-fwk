package android.os;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

public abstract class SimpleClock extends Clock {
  private final ZoneId zone;
  
  public SimpleClock(ZoneId paramZoneId) {
    this.zone = paramZoneId;
  }
  
  public ZoneId getZone() {
    return this.zone;
  }
  
  public Clock withZone(ZoneId paramZoneId) {
    return (Clock)new Object(this, paramZoneId);
  }
  
  public Instant instant() {
    return Instant.ofEpochMilli(millis());
  }
  
  public abstract long millis();
}
