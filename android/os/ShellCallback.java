package android.os;

import android.util.Log;
import com.android.internal.os.IShellCallback;

public class ShellCallback implements Parcelable {
  class MyShellCallback extends IShellCallback.Stub {
    final ShellCallback this$0;
    
    public ParcelFileDescriptor openFile(String param1String1, String param1String2, String param1String3) {
      return ShellCallback.this.onOpenFile(param1String1, param1String2, param1String3);
    }
  }
  
  public ShellCallback() {
    this.mLocal = true;
  }
  
  public ParcelFileDescriptor openFile(String paramString1, String paramString2, String paramString3) {
    if (this.mLocal)
      return onOpenFile(paramString1, paramString2, paramString3); 
    IShellCallback iShellCallback = this.mShellCallback;
    if (iShellCallback != null)
      try {
        return iShellCallback.openFile(paramString1, paramString2, paramString3);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failure opening ");
        stringBuilder.append(paramString1);
        Log.w("ShellCallback", stringBuilder.toString(), (Throwable)remoteException);
      }  
    return null;
  }
  
  public ParcelFileDescriptor onOpenFile(String paramString1, String paramString2, String paramString3) {
    return null;
  }
  
  public static void writeToParcel(ShellCallback paramShellCallback, Parcel paramParcel) {
    if (paramShellCallback == null) {
      paramParcel.writeStrongBinder(null);
    } else {
      paramShellCallback.writeToParcel(paramParcel, 0);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mShellCallback : Lcom/android/internal/os/IShellCallback;
    //   6: ifnonnull -> 23
    //   9: new android/os/ShellCallback$MyShellCallback
    //   12: astore_3
    //   13: aload_3
    //   14: aload_0
    //   15: invokespecial <init> : (Landroid/os/ShellCallback;)V
    //   18: aload_0
    //   19: aload_3
    //   20: putfield mShellCallback : Lcom/android/internal/os/IShellCallback;
    //   23: aload_1
    //   24: aload_0
    //   25: getfield mShellCallback : Lcom/android/internal/os/IShellCallback;
    //   28: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   33: invokevirtual writeStrongBinder : (Landroid/os/IBinder;)V
    //   36: aload_0
    //   37: monitorexit
    //   38: return
    //   39: astore_1
    //   40: aload_0
    //   41: monitorexit
    //   42: aload_1
    //   43: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #97	-> 0
    //   #98	-> 2
    //   #99	-> 9
    //   #101	-> 23
    //   #102	-> 36
    //   #103	-> 38
    //   #102	-> 39
    // Exception table:
    //   from	to	target	type
    //   2	9	39	finally
    //   9	23	39	finally
    //   23	36	39	finally
    //   36	38	39	finally
    //   40	42	39	finally
  }
  
  ShellCallback(Parcel paramParcel) {
    this.mLocal = false;
    IShellCallback iShellCallback = IShellCallback.Stub.asInterface(paramParcel.readStrongBinder());
    if (iShellCallback != null)
      Binder.allowBlocking(iShellCallback.asBinder()); 
  }
  
  public static final Parcelable.Creator<ShellCallback> CREATOR = new Parcelable.Creator<ShellCallback>() {
      public ShellCallback createFromParcel(Parcel param1Parcel) {
        return new ShellCallback(param1Parcel);
      }
      
      public ShellCallback[] newArray(int param1Int) {
        return new ShellCallback[param1Int];
      }
    };
  
  static final boolean DEBUG = false;
  
  static final String TAG = "ShellCallback";
  
  final boolean mLocal;
  
  IShellCallback mShellCallback;
}
