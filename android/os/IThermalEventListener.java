package android.os;

public interface IThermalEventListener extends IInterface {
  void notifyThrottling(Temperature paramTemperature) throws RemoteException;
  
  class Default implements IThermalEventListener {
    public void notifyThrottling(Temperature param1Temperature) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IThermalEventListener {
    private static final String DESCRIPTOR = "android.os.IThermalEventListener";
    
    static final int TRANSACTION_notifyThrottling = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IThermalEventListener");
    }
    
    public static IThermalEventListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IThermalEventListener");
      if (iInterface != null && iInterface instanceof IThermalEventListener)
        return (IThermalEventListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "notifyThrottling";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.IThermalEventListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IThermalEventListener");
      if (param1Parcel1.readInt() != 0) {
        Temperature temperature = Temperature.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      notifyThrottling((Temperature)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IThermalEventListener {
      public static IThermalEventListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IThermalEventListener";
      }
      
      public void notifyThrottling(Temperature param2Temperature) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IThermalEventListener");
          if (param2Temperature != null) {
            parcel.writeInt(1);
            param2Temperature.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IThermalEventListener.Stub.getDefaultImpl() != null) {
            IThermalEventListener.Stub.getDefaultImpl().notifyThrottling(param2Temperature);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IThermalEventListener param1IThermalEventListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IThermalEventListener != null) {
          Proxy.sDefaultImpl = param1IThermalEventListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IThermalEventListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
