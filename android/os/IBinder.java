package android.os;

import java.io.FileDescriptor;

public interface IBinder {
  public static final int DUMP_TRANSACTION = 1598311760;
  
  public static final int FIRST_CALL_TRANSACTION = 1;
  
  public static final int FLAG_COLLECT_NOTED_APP_OPS = 2;
  
  public static final int FLAG_ONEWAY = 1;
  
  public static final int INTERFACE_TRANSACTION = 1598968902;
  
  public static final int LAST_CALL_TRANSACTION = 16777215;
  
  public static final int LIKE_TRANSACTION = 1598835019;
  
  public static final int MAX_IPC_SIZE = 65536;
  
  public static final int PING_TRANSACTION = 1599098439;
  
  public static final int SHELL_COMMAND_TRANSACTION = 1598246212;
  
  public static final int SYSPROPS_TRANSACTION = 1599295570;
  
  public static final int TWEET_TRANSACTION = 1599362900;
  
  static int getSuggestedMaxIpcSizeBytes() {
    return 65536;
  }
  
  void dump(FileDescriptor paramFileDescriptor, String[] paramArrayOfString) throws RemoteException;
  
  void dumpAsync(FileDescriptor paramFileDescriptor, String[] paramArrayOfString) throws RemoteException;
  
  default IBinder getExtension() throws RemoteException {
    throw new IllegalStateException("Method is not implemented");
  }
  
  String getInterfaceDescriptor() throws RemoteException;
  
  boolean isBinderAlive();
  
  void linkToDeath(DeathRecipient paramDeathRecipient, int paramInt) throws RemoteException;
  
  boolean pingBinder();
  
  IInterface queryLocalInterface(String paramString);
  
  void shellCommand(FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, FileDescriptor paramFileDescriptor3, String[] paramArrayOfString, ShellCallback paramShellCallback, ResultReceiver paramResultReceiver) throws RemoteException;
  
  boolean transact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2) throws RemoteException;
  
  boolean unlinkToDeath(DeathRecipient paramDeathRecipient, int paramInt);
  
  public static interface DeathRecipient {
    void binderDied();
    
    default void binderDied(IBinder param1IBinder) {
      binderDied();
    }
  }
}
