package android.os.incremental;

import android.os.ParcelFileDescriptor;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class V4Signature {
  public static final String EXT = ".idsig";
  
  public static final int HASHING_ALGORITHM_SHA256 = 1;
  
  public static final byte LOG2_BLOCK_SIZE_4096_BYTES = 12;
  
  public static final int SUPPORTED_VERSION = 2;
  
  public final byte[] hashingInfo;
  
  public final byte[] signingInfo;
  
  public final int version;
  
  public static class HashingInfo {
    public final int hashAlgorithm;
    
    public final byte log2BlockSize;
    
    public final byte[] rawRootHash;
    
    public final byte[] salt;
    
    HashingInfo(int param1Int, byte param1Byte, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      this.hashAlgorithm = param1Int;
      this.log2BlockSize = param1Byte;
      this.salt = param1ArrayOfbyte1;
      this.rawRootHash = param1ArrayOfbyte2;
    }
    
    public static HashingInfo fromByteArray(byte[] param1ArrayOfbyte) throws IOException {
      ByteBuffer byteBuffer = ByteBuffer.wrap(param1ArrayOfbyte).order(ByteOrder.LITTLE_ENDIAN);
      int i = byteBuffer.getInt();
      byte b = byteBuffer.get();
      param1ArrayOfbyte = V4Signature.readBytes(byteBuffer);
      byte[] arrayOfByte = V4Signature.readBytes(byteBuffer);
      return new HashingInfo(i, b, param1ArrayOfbyte, arrayOfByte);
    }
  }
  
  public static class SigningInfo {
    public final byte[] additionalData;
    
    public final byte[] apkDigest;
    
    public final byte[] certificate;
    
    public final byte[] publicKey;
    
    public final byte[] signature;
    
    public final int signatureAlgorithmId;
    
    SigningInfo(byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3, byte[] param1ArrayOfbyte4, int param1Int, byte[] param1ArrayOfbyte5) {
      this.apkDigest = param1ArrayOfbyte1;
      this.certificate = param1ArrayOfbyte2;
      this.additionalData = param1ArrayOfbyte3;
      this.publicKey = param1ArrayOfbyte4;
      this.signatureAlgorithmId = param1Int;
      this.signature = param1ArrayOfbyte5;
    }
    
    public static SigningInfo fromByteArray(byte[] param1ArrayOfbyte) throws IOException {
      ByteBuffer byteBuffer = ByteBuffer.wrap(param1ArrayOfbyte).order(ByteOrder.LITTLE_ENDIAN);
      param1ArrayOfbyte = V4Signature.readBytes(byteBuffer);
      byte[] arrayOfByte2 = V4Signature.readBytes(byteBuffer);
      byte[] arrayOfByte3 = V4Signature.readBytes(byteBuffer);
      byte[] arrayOfByte4 = V4Signature.readBytes(byteBuffer);
      int i = byteBuffer.getInt();
      byte[] arrayOfByte1 = V4Signature.readBytes(byteBuffer);
      return new SigningInfo(param1ArrayOfbyte, arrayOfByte2, arrayOfByte3, arrayOfByte4, i, arrayOfByte1);
    }
  }
  
  public static V4Signature readFrom(ParcelFileDescriptor paramParcelFileDescriptor) throws IOException {
    ParcelFileDescriptor.AutoCloseInputStream autoCloseInputStream = new ParcelFileDescriptor.AutoCloseInputStream(paramParcelFileDescriptor.dup());
    try {
      return readFrom(autoCloseInputStream);
    } finally {
      try {
        autoCloseInputStream.close();
      } finally {
        autoCloseInputStream = null;
      } 
    } 
  }
  
  public static V4Signature readFrom(byte[] paramArrayOfbyte) throws IOException {
    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(paramArrayOfbyte);
    try {
      return readFrom(byteArrayInputStream);
    } finally {
      try {
        byteArrayInputStream.close();
      } finally {
        byteArrayInputStream = null;
      } 
    } 
  }
  
  public byte[] toByteArray() {
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      this();
      try {
        writeTo(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
      } finally {
        try {
          byteArrayOutputStream.close();
        } finally {
          byteArrayOutputStream = null;
        } 
      } 
    } catch (IOException iOException) {
      return null;
    } 
  }
  
  public static byte[] getSigningData(long paramLong, HashingInfo paramHashingInfo, SigningInfo paramSigningInfo) {
    byte[] arrayOfByte = paramHashingInfo.salt;
    int i = bytesSize(arrayOfByte);
    arrayOfByte = paramHashingInfo.rawRootHash;
    int j = bytesSize(arrayOfByte), k = bytesSize(paramSigningInfo.apkDigest);
    arrayOfByte = paramSigningInfo.certificate;
    j = i + 17 + j + k + bytesSize(arrayOfByte) + bytesSize(paramSigningInfo.additionalData);
    ByteBuffer byteBuffer = ByteBuffer.allocate(j).order(ByteOrder.LITTLE_ENDIAN);
    byteBuffer.putInt(j);
    byteBuffer.putLong(paramLong);
    byteBuffer.putInt(paramHashingInfo.hashAlgorithm);
    byteBuffer.put(paramHashingInfo.log2BlockSize);
    writeBytes(byteBuffer, paramHashingInfo.salt);
    writeBytes(byteBuffer, paramHashingInfo.rawRootHash);
    writeBytes(byteBuffer, paramSigningInfo.apkDigest);
    writeBytes(byteBuffer, paramSigningInfo.certificate);
    writeBytes(byteBuffer, paramSigningInfo.additionalData);
    return byteBuffer.array();
  }
  
  public boolean isVersionSupported() {
    boolean bool;
    if (this.version == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private V4Signature(int paramInt, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) {
    this.version = paramInt;
    this.hashingInfo = paramArrayOfbyte1;
    this.signingInfo = paramArrayOfbyte2;
  }
  
  private static V4Signature readFrom(InputStream paramInputStream) throws IOException {
    int i = readIntLE(paramInputStream);
    byte[] arrayOfByte2 = readBytes(paramInputStream);
    byte[] arrayOfByte1 = readBytes(paramInputStream);
    return new V4Signature(i, arrayOfByte2, arrayOfByte1);
  }
  
  private void writeTo(OutputStream paramOutputStream) throws IOException {
    writeIntLE(paramOutputStream, this.version);
    writeBytes(paramOutputStream, this.hashingInfo);
    writeBytes(paramOutputStream, this.signingInfo);
  }
  
  private static int bytesSize(byte[] paramArrayOfbyte) {
    int i;
    if (paramArrayOfbyte == null) {
      i = 0;
    } else {
      i = paramArrayOfbyte.length;
    } 
    return i + 4;
  }
  
  private static void readFully(InputStream paramInputStream, byte[] paramArrayOfbyte) throws IOException {
    int i = paramArrayOfbyte.length;
    int j = 0;
    while (j < i) {
      int k = paramInputStream.read(paramArrayOfbyte, j, i - j);
      if (k >= 0) {
        j += k;
        continue;
      } 
      throw new EOFException();
    } 
  }
  
  private static int readIntLE(InputStream paramInputStream) throws IOException {
    byte[] arrayOfByte = new byte[4];
    readFully(paramInputStream, arrayOfByte);
    return ByteBuffer.wrap(arrayOfByte).order(ByteOrder.LITTLE_ENDIAN).getInt();
  }
  
  private static void writeIntLE(OutputStream paramOutputStream, int paramInt) throws IOException {
    ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[4]).order(ByteOrder.LITTLE_ENDIAN).putInt(paramInt);
    byte[] arrayOfByte = byteBuffer.array();
    paramOutputStream.write(arrayOfByte);
  }
  
  private static byte[] readBytes(InputStream paramInputStream) throws IOException {
    try {
      int i = readIntLE(paramInputStream);
      byte[] arrayOfByte = new byte[i];
      readFully(paramInputStream, arrayOfByte);
      return arrayOfByte;
    } catch (EOFException eOFException) {
      return null;
    } 
  }
  
  private static byte[] readBytes(ByteBuffer paramByteBuffer) throws IOException {
    if (paramByteBuffer.remaining() >= 4) {
      int i = paramByteBuffer.getInt();
      if (paramByteBuffer.remaining() >= i) {
        byte[] arrayOfByte = new byte[i];
        paramByteBuffer.get(arrayOfByte);
        return arrayOfByte;
      } 
      throw new EOFException();
    } 
    throw new EOFException();
  }
  
  private static void writeBytes(OutputStream paramOutputStream, byte[] paramArrayOfbyte) throws IOException {
    if (paramArrayOfbyte == null) {
      writeIntLE(paramOutputStream, 0);
      return;
    } 
    writeIntLE(paramOutputStream, paramArrayOfbyte.length);
    paramOutputStream.write(paramArrayOfbyte);
  }
  
  private static void writeBytes(ByteBuffer paramByteBuffer, byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null) {
      paramByteBuffer.putInt(0);
      return;
    } 
    paramByteBuffer.putInt(paramArrayOfbyte.length);
    paramByteBuffer.put(paramArrayOfbyte);
  }
}
