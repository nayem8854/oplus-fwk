package android.os.incremental;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IIncrementalServiceConnector extends IInterface {
  int setStorageParams(boolean paramBoolean) throws RemoteException;
  
  class Default implements IIncrementalServiceConnector {
    public int setStorageParams(boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IIncrementalServiceConnector {
    private static final String DESCRIPTOR = "android.os.incremental.IIncrementalServiceConnector";
    
    static final int TRANSACTION_setStorageParams = 1;
    
    public Stub() {
      attachInterface(this, "android.os.incremental.IIncrementalServiceConnector");
    }
    
    public static IIncrementalServiceConnector asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.incremental.IIncrementalServiceConnector");
      if (iInterface != null && iInterface instanceof IIncrementalServiceConnector)
        return (IIncrementalServiceConnector)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "setStorageParams";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.incremental.IIncrementalServiceConnector");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.incremental.IIncrementalServiceConnector");
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      param1Int1 = setStorageParams(bool);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(param1Int1);
      return true;
    }
    
    private static class Proxy implements IIncrementalServiceConnector {
      public static IIncrementalServiceConnector sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.incremental.IIncrementalServiceConnector";
      }
      
      public int setStorageParams(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalServiceConnector");
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IIncrementalServiceConnector.Stub.getDefaultImpl() != null) {
            i = IIncrementalServiceConnector.Stub.getDefaultImpl().setStorageParams(param2Boolean);
            return i;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IIncrementalServiceConnector param1IIncrementalServiceConnector) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IIncrementalServiceConnector != null) {
          Proxy.sDefaultImpl = param1IIncrementalServiceConnector;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IIncrementalServiceConnector getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
