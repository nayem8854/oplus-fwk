package android.os.incremental;

import android.content.Context;
import android.content.pm.DataLoaderParams;
import android.content.pm.IDataLoaderStatusListener;
import android.content.pm.InstallationFileParcel;
import android.text.TextUtils;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public final class IncrementalFileStorages {
  private static final String TAG = "IncrementalFileStorages";
  
  private IncrementalStorage mDefaultStorage;
  
  private final IncrementalManager mIncrementalManager;
  
  private final File mStageDir;
  
  public static IncrementalFileStorages initialize(Context paramContext, File paramFile, DataLoaderParams paramDataLoaderParams, IDataLoaderStatusListener paramIDataLoaderStatusListener, StorageHealthCheckParams paramStorageHealthCheckParams, IStorageHealthListener paramIStorageHealthListener, List<InstallationFileParcel> paramList) throws IOException {
    IncrementalManager incrementalManager = (IncrementalManager)paramContext.getSystemService("incremental");
    if (incrementalManager != null) {
      StringBuilder stringBuilder;
      IncrementalFileStorages incrementalFileStorages = new IncrementalFileStorages(paramFile, incrementalManager, paramDataLoaderParams, paramIDataLoaderStatusListener, paramStorageHealthCheckParams, paramIStorageHealthListener);
      for (Iterator<InstallationFileParcel> iterator = paramList.iterator(); iterator.hasNext(); ) {
        InstallationFileParcel installationFileParcel = iterator.next();
        if (installationFileParcel.location == 0) {
          try {
            incrementalFileStorages.addApkFile(installationFileParcel);
          } catch (IOException iOException) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Failed to add file to IncFS: ");
            stringBuilder1.append(installationFileParcel.name);
            stringBuilder1.append(", reason: ");
            throw new IOException(stringBuilder1.toString(), iOException);
          } 
          continue;
        } 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown file location: ");
        stringBuilder.append(installationFileParcel.location);
        throw new IOException(stringBuilder.toString());
      } 
      stringBuilder.startLoading();
      return (IncrementalFileStorages)stringBuilder;
    } 
    throw new IOException("Failed to obtain incrementalManager.");
  }
  
  private IncrementalFileStorages(File paramFile, IncrementalManager paramIncrementalManager, DataLoaderParams paramDataLoaderParams, IDataLoaderStatusListener paramIDataLoaderStatusListener, StorageHealthCheckParams paramStorageHealthCheckParams, IStorageHealthListener paramIStorageHealthListener) throws IOException {
    try {
      IOException iOException1, iOException2;
      this.mStageDir = paramFile;
      this.mIncrementalManager = paramIncrementalManager;
      if (paramDataLoaderParams.getComponentName().getPackageName().equals("local")) {
        String str = paramDataLoaderParams.getArguments();
        if (!TextUtils.isEmpty(str)) {
          IncrementalStorage incrementalStorage = this.mIncrementalManager.openStorage(str);
          if (incrementalStorage != null) {
            incrementalStorage.bind(paramFile.getAbsolutePath());
          } else {
            iOException2 = new IOException();
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Couldn't open incremental storage at ");
            stringBuilder.append(str);
            this(stringBuilder.toString());
            throw iOException2;
          } 
        } else {
          iOException1 = new IOException();
          this("Failed to create storage: incrementalPath is empty");
          throw iOException1;
        } 
      } else {
        IncrementalStorage incrementalStorage = this.mIncrementalManager.createStorage(iOException1.getAbsolutePath(), (DataLoaderParams)iOException2, 5, false, paramIDataLoaderStatusListener, paramStorageHealthCheckParams, paramIStorageHealthListener);
        if (incrementalStorage == null) {
          iOException2 = new IOException();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Couldn't create incremental storage at ");
          stringBuilder.append(iOException1);
          this(stringBuilder.toString());
          throw iOException2;
        } 
      } 
      return;
    } catch (IOException iOException) {
      cleanUp();
      throw iOException;
    } 
  }
  
  private void addApkFile(InstallationFileParcel paramInstallationFileParcel) throws IOException {
    String str = paramInstallationFileParcel.name;
    File file = new File(this.mStageDir, str);
    if (!file.exists())
      this.mDefaultStorage.makeFile(str, paramInstallationFileParcel.size, null, paramInstallationFileParcel.metadata, paramInstallationFileParcel.signature); 
  }
  
  public void startLoading() throws IOException {
    if (this.mDefaultStorage.startLoading())
      return; 
    throw new IOException("Failed to start loading data for Incremental installation.");
  }
  
  public void disableReadLogs() {
    this.mDefaultStorage.disableReadLogs();
  }
  
  public void cleanUp() {
    IncrementalStorage incrementalStorage = this.mDefaultStorage;
    if (incrementalStorage == null)
      return; 
    try {
      incrementalStorage.unBind(this.mStageDir.getAbsolutePath());
    } catch (IOException iOException) {}
    this.mDefaultStorage = null;
  }
}
