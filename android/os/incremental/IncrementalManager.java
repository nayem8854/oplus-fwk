package android.os.incremental;

import android.content.pm.DataLoaderParams;
import android.content.pm.IDataLoaderStatusListener;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.SparseArray;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public final class IncrementalManager {
  private static final String ALLOWED_PROPERTY = "incremental.allowed";
  
  public static final int CREATE_MODE_CREATE = 4;
  
  public static final int CREATE_MODE_OPEN_EXISTING = 8;
  
  public static final int CREATE_MODE_PERMANENT_BIND = 2;
  
  public static final int CREATE_MODE_TEMPORARY_BIND = 1;
  
  private static final String TAG = "IncrementalManager";
  
  private final IIncrementalService mService;
  
  private final SparseArray<IncrementalStorage> mStorages = new SparseArray();
  
  public IncrementalManager(IIncrementalService paramIIncrementalService) {
    this.mService = paramIIncrementalService;
  }
  
  public IncrementalStorage getStorage(int paramInt) {
    synchronized (this.mStorages) {
      return (IncrementalStorage)this.mStorages.get(paramInt);
    } 
  }
  
  public IncrementalStorage createStorage(String paramString, DataLoaderParams paramDataLoaderParams, int paramInt, boolean paramBoolean, IDataLoaderStatusListener paramIDataLoaderStatusListener, StorageHealthCheckParams paramStorageHealthCheckParams, IStorageHealthListener paramIStorageHealthListener) {
    try {
      paramInt = this.mService.createStorage(paramString, paramDataLoaderParams.getData(), paramInt, paramIDataLoaderStatusListener, paramStorageHealthCheckParams, paramIStorageHealthListener);
      if (paramInt < 0)
        return null; 
      null = new IncrementalStorage();
      this(this.mService, paramInt);
      synchronized (this.mStorages) {
        this.mStorages.put(paramInt, null);
        if (paramBoolean)
          null.startLoading(); 
        return null;
      } 
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public IncrementalStorage openStorage(String paramString) {
    try {
      int i = this.mService.openStorage(paramString);
      if (i < 0)
        return null; 
      null = new IncrementalStorage();
      this(this.mService, i);
      synchronized (this.mStorages) {
        this.mStorages.put(i, null);
        return null;
      } 
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public IncrementalStorage createStorage(String paramString, IncrementalStorage paramIncrementalStorage, int paramInt) {
    try {
      IIncrementalService iIncrementalService = this.mService;
      int i = paramIncrementalStorage.getId();
      paramInt = iIncrementalService.createLinkedStorage(paramString, i, paramInt);
      if (paramInt < 0)
        return null; 
      paramIncrementalStorage = new IncrementalStorage();
      this(this.mService, paramInt);
      synchronized (this.mStorages) {
        this.mStorages.put(paramInt, paramIncrementalStorage);
        return paramIncrementalStorage;
      } 
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void renameCodePath(File paramFile1, File paramFile2) throws IllegalArgumentException, IOException {
    File file = paramFile1.getAbsoluteFile();
    IncrementalStorage incrementalStorage = openStorage(file.toString());
    if (incrementalStorage != null) {
      String str = paramFile2.getAbsoluteFile().getParent();
      IncrementalStorage incrementalStorage1 = createStorage(str, incrementalStorage, 6);
      if (incrementalStorage1 != null)
        try {
          String str1 = paramFile2.getName();
          linkFiles(incrementalStorage, file, "", incrementalStorage1, str1);
          incrementalStorage.unBind(file.toString());
          return;
        } catch (Exception exception) {
          incrementalStorage1.unBind(str);
          throw exception;
        }  
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Failed to create linked storage at dir: ");
      stringBuilder1.append(str);
      throw new IOException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not an Incremental path: ");
    stringBuilder.append(file);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private void linkFiles(final IncrementalStorage sourceStorage, File paramFile, String paramString1, final IncrementalStorage targetStorage, String paramString2) throws IOException {
    final Path sourceBase = paramFile.toPath().resolve(paramString1);
    final Path targetRelative = Paths.get(paramString2, new String[0]);
    Files.walkFileTree(paramFile.toPath(), new SimpleFileVisitor<Path>() {
          final IncrementalManager this$0;
          
          final Path val$sourceBase;
          
          final IncrementalStorage val$sourceStorage;
          
          final Path val$targetRelative;
          
          final IncrementalStorage val$targetStorage;
          
          public FileVisitResult preVisitDirectory(Path param1Path, BasicFileAttributes param1BasicFileAttributes) throws IOException {
            param1Path = sourceBase.relativize(param1Path);
            targetStorage.makeDirectory(targetRelative.resolve(param1Path).toString());
            return FileVisitResult.CONTINUE;
          }
          
          public FileVisitResult visitFile(Path param1Path, BasicFileAttributes param1BasicFileAttributes) throws IOException {
            Path path1 = sourceBase.relativize(param1Path);
            IncrementalStorage incrementalStorage1 = sourceStorage;
            String str1 = param1Path.toAbsolutePath().toString();
            IncrementalStorage incrementalStorage2 = targetStorage;
            Path path2 = targetRelative;
            String str2 = path2.resolve(path1).toString();
            incrementalStorage1.makeLink(str1, incrementalStorage2, str2);
            return FileVisitResult.CONTINUE;
          }
        });
  }
  
  public void closeStorage(String paramString) {
    try {
      int i = this.mService.openStorage(paramString);
      if (i < 0)
        return; 
      this.mService.deleteStorage(i);
      synchronized (this.mStorages) {
        this.mStorages.remove(i);
        return;
      } 
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean isFeatureEnabled() {
    return nativeIsEnabled();
  }
  
  public static boolean isAllowed() {
    boolean bool = isFeatureEnabled();
    boolean bool1 = true;
    if (!bool || !SystemProperties.getBoolean("incremental.allowed", true))
      bool1 = false; 
    return bool1;
  }
  
  public static boolean isIncrementalPath(String paramString) {
    return nativeIsIncrementalPath(paramString);
  }
  
  public static byte[] unsafeGetFileSignature(String paramString) {
    return nativeUnsafeGetFileSignature(paramString);
  }
  
  private static native boolean nativeIsEnabled();
  
  private static native boolean nativeIsIncrementalPath(String paramString);
  
  private static native byte[] nativeUnsafeGetFileSignature(String paramString);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CreateMode {}
}
