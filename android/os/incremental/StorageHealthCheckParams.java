package android.os.incremental;

import android.os.Parcel;
import android.os.Parcelable;

public class StorageHealthCheckParams implements Parcelable {
  public static final Parcelable.Creator<StorageHealthCheckParams> CREATOR = new Parcelable.Creator<StorageHealthCheckParams>() {
      public StorageHealthCheckParams createFromParcel(Parcel param1Parcel) {
        StorageHealthCheckParams storageHealthCheckParams = new StorageHealthCheckParams();
        storageHealthCheckParams.readFromParcel(param1Parcel);
        return storageHealthCheckParams;
      }
      
      public StorageHealthCheckParams[] newArray(int param1Int) {
        return new StorageHealthCheckParams[param1Int];
      }
    };
  
  public int blockedTimeoutMs;
  
  public int unhealthyMonitoringMs;
  
  public int unhealthyTimeoutMs;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.blockedTimeoutMs);
    paramParcel.writeInt(this.unhealthyTimeoutMs);
    paramParcel.writeInt(this.unhealthyMonitoringMs);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.blockedTimeoutMs = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.unhealthyTimeoutMs = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.unhealthyMonitoringMs = paramParcel.readInt();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
