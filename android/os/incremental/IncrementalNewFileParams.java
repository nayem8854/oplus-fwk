package android.os.incremental;

import android.os.Parcel;
import android.os.Parcelable;

public class IncrementalNewFileParams implements Parcelable {
  public static final Parcelable.Creator<IncrementalNewFileParams> CREATOR = new Parcelable.Creator<IncrementalNewFileParams>() {
      public IncrementalNewFileParams createFromParcel(Parcel param1Parcel) {
        IncrementalNewFileParams incrementalNewFileParams = new IncrementalNewFileParams();
        incrementalNewFileParams.readFromParcel(param1Parcel);
        return incrementalNewFileParams;
      }
      
      public IncrementalNewFileParams[] newArray(int param1Int) {
        return new IncrementalNewFileParams[param1Int];
      }
    };
  
  public byte[] fileId;
  
  public byte[] metadata;
  
  public byte[] signature;
  
  public long size;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeLong(this.size);
    paramParcel.writeByteArray(this.fileId);
    paramParcel.writeByteArray(this.metadata);
    paramParcel.writeByteArray(this.signature);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.size = paramParcel.readLong();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.fileId = paramParcel.createByteArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.metadata = paramParcel.createByteArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.signature = paramParcel.createByteArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
