package android.os.incremental;

import android.os.RemoteException;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.UUID;

public final class IncrementalStorage {
  private static final int INCFS_MAX_ADD_DATA_SIZE = 128;
  
  private static final int INCFS_MAX_HASH_SIZE = 32;
  
  private static final String TAG = "IncrementalStorage";
  
  private static final int UUID_BYTE_SIZE = 16;
  
  private final int mId;
  
  private final IIncrementalService mService;
  
  public IncrementalStorage(IIncrementalService paramIIncrementalService, int paramInt) {
    this.mService = paramIIncrementalService;
    this.mId = paramInt;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public void bind(String paramString) throws IOException {
    bind("", paramString);
  }
  
  public void bind(String paramString1, String paramString2) throws IOException {
    try {
      int i = this.mService.makeBindMount(this.mId, paramString1, paramString2, 0);
      if (i < 0) {
        IOException iOException = new IOException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("bind() failed with errno ");
        stringBuilder.append(-i);
        this(stringBuilder.toString());
        throw iOException;
      } 
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void bindPermanent(String paramString) throws IOException {
    bindPermanent("", paramString);
  }
  
  public void bindPermanent(String paramString1, String paramString2) throws IOException {
    try {
      int i = this.mService.makeBindMount(this.mId, paramString1, paramString2, 1);
      if (i < 0) {
        IOException iOException = new IOException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("bind() permanent failed with errno ");
        stringBuilder.append(-i);
        this(stringBuilder.toString());
        throw iOException;
      } 
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unBind(String paramString) throws IOException {
    try {
      int i = this.mService.deleteBindMount(this.mId, paramString);
      if (i < 0) {
        IOException iOException = new IOException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("unbind() failed with errno ");
        stringBuilder.append(-i);
        this(stringBuilder.toString());
        throw iOException;
      } 
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void makeDirectory(String paramString) throws IOException {
    try {
      int i = this.mService.makeDirectory(this.mId, paramString);
      if (i < 0) {
        IOException iOException = new IOException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("makeDirectory() failed with errno ");
        stringBuilder.append(-i);
        this(stringBuilder.toString());
        throw iOException;
      } 
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void makeDirectories(String paramString) throws IOException {
    try {
      int i = this.mService.makeDirectories(this.mId, paramString);
      if (i < 0) {
        IOException iOException = new IOException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("makeDirectory() failed with errno ");
        stringBuilder.append(-i);
        this(stringBuilder.toString());
        throw iOException;
      } 
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void makeFile(String paramString, long paramLong, UUID paramUUID, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws IOException {
    if (paramUUID != null || paramArrayOfbyte1 != null) {
      validateV4Signature(paramArrayOfbyte2);
      IncrementalNewFileParams incrementalNewFileParams = new IncrementalNewFileParams();
      this();
      incrementalNewFileParams.size = paramLong;
      if (paramArrayOfbyte1 == null)
        paramArrayOfbyte1 = new byte[0]; 
      incrementalNewFileParams.metadata = paramArrayOfbyte1;
      incrementalNewFileParams.fileId = idToBytes(paramUUID);
      incrementalNewFileParams.signature = paramArrayOfbyte2;
      int i = this.mService.makeFile(this.mId, paramString, incrementalNewFileParams);
      if (i != 0) {
        IOException iOException = new IOException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("makeFile() failed with errno ");
        stringBuilder.append(-i);
        this(stringBuilder.toString());
        throw iOException;
      } 
      return;
    } 
    try {
      IOException iOException = new IOException();
      this("File ID and metadata cannot both be null");
      throw iOException;
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void makeFileFromRange(String paramString1, String paramString2, long paramLong1, long paramLong2) throws IOException {
    try {
      int i = this.mService.makeFileFromRange(this.mId, paramString1, paramString2, paramLong1, paramLong2);
      if (i < 0) {
        IOException iOException = new IOException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("makeFileFromRange() failed, errno ");
        stringBuilder.append(-i);
        this(stringBuilder.toString());
        throw iOException;
      } 
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void makeLink(String paramString1, IncrementalStorage paramIncrementalStorage, String paramString2) throws IOException {
    try {
      int i = this.mService.makeLink(this.mId, paramString1, paramIncrementalStorage.getId(), paramString2);
      if (i < 0) {
        IOException iOException = new IOException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("makeLink() failed with errno ");
        stringBuilder.append(-i);
        this(stringBuilder.toString());
        throw iOException;
      } 
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unlink(String paramString) throws IOException {
    try {
      int i = this.mService.unlink(this.mId, paramString);
      if (i < 0) {
        IOException iOException = new IOException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("unlink() failed with errno ");
        stringBuilder.append(-i);
        this(stringBuilder.toString());
        throw iOException;
      } 
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void moveFile(String paramString1, String paramString2) throws IOException {
    try {
      int i = this.mService.makeLink(this.mId, paramString1, this.mId, paramString2);
      if (i < 0) {
        IOException iOException = new IOException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("moveFile() failed at makeLink(), errno ");
        stringBuilder.append(-i);
        this(stringBuilder.toString());
        throw iOException;
      } 
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
    try {
      this.mService.unlink(this.mId, paramString1);
    } catch (RemoteException remoteException) {}
  }
  
  public void moveDir(String paramString1, String paramString2) throws IOException {
    if ((new File(paramString2)).exists()) {
      try {
        int i = this.mService.makeBindMount(this.mId, paramString1, paramString2, 1);
        if (i < 0) {
          IOException iOException = new IOException();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("moveDir() failed at making bind mount, errno ");
          stringBuilder.append(-i);
          this(stringBuilder.toString());
          throw iOException;
        } 
      } catch (RemoteException remoteException) {
        remoteException.rethrowFromSystemServer();
      } 
      try {
        this.mService.deleteBindMount(this.mId, paramString1);
      } catch (RemoteException remoteException) {}
      return;
    } 
    throw new IOException("moveDir() requires that destination dir already exists.");
  }
  
  public boolean isFileFullyLoaded(String paramString) {
    return isFileRangeLoaded(paramString, 0L, -1L);
  }
  
  public boolean isFileRangeLoaded(String paramString, long paramLong1, long paramLong2) {
    try {
      return this.mService.isFileRangeLoaded(this.mId, paramString, paramLong1, paramLong2);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return false;
    } 
  }
  
  public byte[] getFileMetadata(String paramString) {
    try {
      return this.mService.getMetadataByPath(this.mId, paramString);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return null;
    } 
  }
  
  public byte[] getFileMetadata(UUID paramUUID) {
    try {
      null = idToBytes(paramUUID);
      return this.mService.getMetadataById(this.mId, null);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return null;
    } 
  }
  
  public boolean startLoading() {
    try {
      return this.mService.startLoading(this.mId);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return false;
    } 
  }
  
  public static byte[] idToBytes(UUID paramUUID) {
    if (paramUUID == null)
      return new byte[0]; 
    ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[16]);
    byteBuffer.putLong(paramUUID.getMostSignificantBits());
    byteBuffer.putLong(paramUUID.getLeastSignificantBits());
    return byteBuffer.array();
  }
  
  public static UUID bytesToId(byte[] paramArrayOfbyte) throws IllegalArgumentException {
    ByteBuffer byteBuffer;
    if (paramArrayOfbyte.length == 16) {
      byteBuffer = ByteBuffer.wrap(paramArrayOfbyte);
      long l1 = byteBuffer.getLong();
      long l2 = byteBuffer.getLong();
      return new UUID(l1, l2);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Expected array of size 16, got ");
    stringBuilder.append(byteBuffer.length);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void disableReadLogs() {
    try {
      this.mService.disableReadLogs(this.mId);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  private static void validateV4Signature(byte[] paramArrayOfbyte) throws IOException {
    if (paramArrayOfbyte == null || paramArrayOfbyte.length == 0)
      return; 
    try {
      StringBuilder stringBuilder2;
      V4Signature v4Signature = V4Signature.readFrom(paramArrayOfbyte);
      if (v4Signature.isVersionSupported()) {
        V4Signature.HashingInfo hashingInfo = V4Signature.HashingInfo.fromByteArray(v4Signature.hashingInfo);
        V4Signature.SigningInfo signingInfo = V4Signature.SigningInfo.fromByteArray(v4Signature.signingInfo);
        if (hashingInfo.hashAlgorithm == 1) {
          if (hashingInfo.log2BlockSize == 12) {
            if (hashingInfo.salt == null || hashingInfo.salt.length <= 0) {
              if (hashingInfo.rawRootHash.length == 32) {
                if (signingInfo.additionalData.length <= 128)
                  return; 
                throw new IOException("additionalData has to be at most 128 bytes");
              } 
              throw new IOException("rawRootHash has to be 32 bytes");
            } 
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append("Unsupported salt: ");
            stringBuilder3.append(hashingInfo.salt);
            throw new IOException(stringBuilder3.toString());
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unsupported log2BlockSize: ");
          stringBuilder.append(hashingInfo.log2BlockSize);
          throw new IOException(stringBuilder.toString());
        } 
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Unsupported hashAlgorithm: ");
        stringBuilder2.append(hashingInfo.hashAlgorithm);
        throw new IOException(stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("v4 signature version ");
      stringBuilder1.append(((V4Signature)stringBuilder2).version);
      stringBuilder1.append(" is not supported");
      throw new IOException(stringBuilder1.toString());
    } catch (IOException iOException) {
      throw new IOException("Failed to read v4 signature:", iOException);
    } 
  }
  
  public boolean configureNativeBinaries(String paramString1, String paramString2, String paramString3, boolean paramBoolean) {
    try {
      return this.mService.configureNativeBinaries(this.mId, paramString1, paramString2, paramString3, paramBoolean);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return false;
    } 
  }
  
  public boolean waitForNativeBinariesExtraction() {
    try {
      return this.mService.waitForNativeBinariesExtraction(this.mId);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return false;
    } 
  }
}
