package android.os.incremental;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;

public class IncrementalFileSystemControlParcel implements Parcelable {
  public static final Parcelable.Creator<IncrementalFileSystemControlParcel> CREATOR = new Parcelable.Creator<IncrementalFileSystemControlParcel>() {
      public IncrementalFileSystemControlParcel createFromParcel(Parcel param1Parcel) {
        IncrementalFileSystemControlParcel incrementalFileSystemControlParcel = new IncrementalFileSystemControlParcel();
        incrementalFileSystemControlParcel.readFromParcel(param1Parcel);
        return incrementalFileSystemControlParcel;
      }
      
      public IncrementalFileSystemControlParcel[] newArray(int param1Int) {
        return new IncrementalFileSystemControlParcel[param1Int];
      }
    };
  
  public ParcelFileDescriptor cmd;
  
  public ParcelFileDescriptor log;
  
  public ParcelFileDescriptor pendingReads;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    if (this.cmd != null) {
      paramParcel.writeInt(1);
      this.cmd.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.pendingReads != null) {
      paramParcel.writeInt(1);
      this.pendingReads.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.log != null) {
      paramParcel.writeInt(1);
      this.log.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(0);
    } 
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      if (paramParcel.readInt() != 0) {
        this.cmd = ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel);
      } else {
        this.cmd = null;
      } 
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      if (paramParcel.readInt() != 0) {
        this.pendingReads = ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel);
      } else {
        this.pendingReads = null;
      } 
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      if (paramParcel.readInt() != 0) {
        this.log = ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel);
      } else {
        this.log = null;
      } 
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
