package android.os.incremental;

import android.content.pm.DataLoaderParamsParcel;
import android.content.pm.IDataLoaderStatusListener;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IIncrementalService extends IInterface {
  public static final int BIND_PERMANENT = 1;
  
  public static final int BIND_TEMPORARY = 0;
  
  public static final int CREATE_MODE_CREATE = 4;
  
  public static final int CREATE_MODE_OPEN_EXISTING = 8;
  
  public static final int CREATE_MODE_PERMANENT_BIND = 2;
  
  public static final int CREATE_MODE_TEMPORARY_BIND = 1;
  
  boolean configureNativeBinaries(int paramInt, String paramString1, String paramString2, String paramString3, boolean paramBoolean) throws RemoteException;
  
  int createLinkedStorage(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  int createStorage(String paramString, DataLoaderParamsParcel paramDataLoaderParamsParcel, int paramInt, IDataLoaderStatusListener paramIDataLoaderStatusListener, StorageHealthCheckParams paramStorageHealthCheckParams, IStorageHealthListener paramIStorageHealthListener) throws RemoteException;
  
  int deleteBindMount(int paramInt, String paramString) throws RemoteException;
  
  void deleteStorage(int paramInt) throws RemoteException;
  
  void disableReadLogs(int paramInt) throws RemoteException;
  
  byte[] getMetadataById(int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  byte[] getMetadataByPath(int paramInt, String paramString) throws RemoteException;
  
  boolean isFileRangeLoaded(int paramInt, String paramString, long paramLong1, long paramLong2) throws RemoteException;
  
  int makeBindMount(int paramInt1, String paramString1, String paramString2, int paramInt2) throws RemoteException;
  
  int makeDirectories(int paramInt, String paramString) throws RemoteException;
  
  int makeDirectory(int paramInt, String paramString) throws RemoteException;
  
  int makeFile(int paramInt, String paramString, IncrementalNewFileParams paramIncrementalNewFileParams) throws RemoteException;
  
  int makeFileFromRange(int paramInt, String paramString1, String paramString2, long paramLong1, long paramLong2) throws RemoteException;
  
  int makeLink(int paramInt1, String paramString1, int paramInt2, String paramString2) throws RemoteException;
  
  int openStorage(String paramString) throws RemoteException;
  
  boolean startLoading(int paramInt) throws RemoteException;
  
  int unlink(int paramInt, String paramString) throws RemoteException;
  
  boolean waitForNativeBinariesExtraction(int paramInt) throws RemoteException;
  
  class Default implements IIncrementalService {
    public int openStorage(String param1String) throws RemoteException {
      return 0;
    }
    
    public int createStorage(String param1String, DataLoaderParamsParcel param1DataLoaderParamsParcel, int param1Int, IDataLoaderStatusListener param1IDataLoaderStatusListener, StorageHealthCheckParams param1StorageHealthCheckParams, IStorageHealthListener param1IStorageHealthListener) throws RemoteException {
      return 0;
    }
    
    public int createLinkedStorage(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int makeBindMount(int param1Int1, String param1String1, String param1String2, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int deleteBindMount(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public int makeDirectory(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public int makeDirectories(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public int makeFile(int param1Int, String param1String, IncrementalNewFileParams param1IncrementalNewFileParams) throws RemoteException {
      return 0;
    }
    
    public int makeFileFromRange(int param1Int, String param1String1, String param1String2, long param1Long1, long param1Long2) throws RemoteException {
      return 0;
    }
    
    public int makeLink(int param1Int1, String param1String1, int param1Int2, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int unlink(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public boolean isFileRangeLoaded(int param1Int, String param1String, long param1Long1, long param1Long2) throws RemoteException {
      return false;
    }
    
    public byte[] getMetadataByPath(int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public byte[] getMetadataById(int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {
      return null;
    }
    
    public boolean startLoading(int param1Int) throws RemoteException {
      return false;
    }
    
    public void deleteStorage(int param1Int) throws RemoteException {}
    
    public void disableReadLogs(int param1Int) throws RemoteException {}
    
    public boolean configureNativeBinaries(int param1Int, String param1String1, String param1String2, String param1String3, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean waitForNativeBinariesExtraction(int param1Int) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IIncrementalService {
    private static final String DESCRIPTOR = "android.os.incremental.IIncrementalService";
    
    static final int TRANSACTION_configureNativeBinaries = 18;
    
    static final int TRANSACTION_createLinkedStorage = 3;
    
    static final int TRANSACTION_createStorage = 2;
    
    static final int TRANSACTION_deleteBindMount = 5;
    
    static final int TRANSACTION_deleteStorage = 16;
    
    static final int TRANSACTION_disableReadLogs = 17;
    
    static final int TRANSACTION_getMetadataById = 14;
    
    static final int TRANSACTION_getMetadataByPath = 13;
    
    static final int TRANSACTION_isFileRangeLoaded = 12;
    
    static final int TRANSACTION_makeBindMount = 4;
    
    static final int TRANSACTION_makeDirectories = 7;
    
    static final int TRANSACTION_makeDirectory = 6;
    
    static final int TRANSACTION_makeFile = 8;
    
    static final int TRANSACTION_makeFileFromRange = 9;
    
    static final int TRANSACTION_makeLink = 10;
    
    static final int TRANSACTION_openStorage = 1;
    
    static final int TRANSACTION_startLoading = 15;
    
    static final int TRANSACTION_unlink = 11;
    
    static final int TRANSACTION_waitForNativeBinariesExtraction = 19;
    
    public Stub() {
      attachInterface(this, "android.os.incremental.IIncrementalService");
    }
    
    public static IIncrementalService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.incremental.IIncrementalService");
      if (iInterface != null && iInterface instanceof IIncrementalService)
        return (IIncrementalService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 19:
          return "waitForNativeBinariesExtraction";
        case 18:
          return "configureNativeBinaries";
        case 17:
          return "disableReadLogs";
        case 16:
          return "deleteStorage";
        case 15:
          return "startLoading";
        case 14:
          return "getMetadataById";
        case 13:
          return "getMetadataByPath";
        case 12:
          return "isFileRangeLoaded";
        case 11:
          return "unlink";
        case 10:
          return "makeLink";
        case 9:
          return "makeFileFromRange";
        case 8:
          return "makeFile";
        case 7:
          return "makeDirectories";
        case 6:
          return "makeDirectory";
        case 5:
          return "deleteBindMount";
        case 4:
          return "makeBindMount";
        case 3:
          return "createLinkedStorage";
        case 2:
          return "createStorage";
        case 1:
          break;
      } 
      return "openStorage";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        byte[] arrayOfByte2;
        String str3;
        byte[] arrayOfByte1;
        String str2;
        IStorageHealthListener iStorageHealthListener;
        String str4, str5, str6;
        boolean bool;
        long l1, l2;
        IDataLoaderStatusListener iDataLoaderStatusListener;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 19:
            param1Parcel1.enforceInterface("android.os.incremental.IIncrementalService");
            param1Int1 = param1Parcel1.readInt();
            bool4 = waitForNativeBinariesExtraction(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 18:
            param1Parcel1.enforceInterface("android.os.incremental.IIncrementalService");
            m = param1Parcel1.readInt();
            str4 = param1Parcel1.readString();
            str5 = param1Parcel1.readString();
            str6 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            bool3 = configureNativeBinaries(m, str4, str5, str6, bool);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 17:
            param1Parcel1.enforceInterface("android.os.incremental.IIncrementalService");
            k = param1Parcel1.readInt();
            disableReadLogs(k);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            param1Parcel1.enforceInterface("android.os.incremental.IIncrementalService");
            k = param1Parcel1.readInt();
            deleteStorage(k);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            param1Parcel1.enforceInterface("android.os.incremental.IIncrementalService");
            k = param1Parcel1.readInt();
            bool2 = startLoading(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 14:
            param1Parcel1.enforceInterface("android.os.incremental.IIncrementalService");
            j = param1Parcel1.readInt();
            arrayOfByte2 = param1Parcel1.createByteArray();
            arrayOfByte2 = getMetadataById(j, arrayOfByte2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 13:
            arrayOfByte2.enforceInterface("android.os.incremental.IIncrementalService");
            j = arrayOfByte2.readInt();
            str3 = arrayOfByte2.readString();
            arrayOfByte1 = getMetadataByPath(j, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte1);
            return true;
          case 12:
            arrayOfByte1.enforceInterface("android.os.incremental.IIncrementalService");
            j = arrayOfByte1.readInt();
            str6 = arrayOfByte1.readString();
            l1 = arrayOfByte1.readLong();
            l2 = arrayOfByte1.readLong();
            bool1 = isFileRangeLoaded(j, str6, l1, l2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 11:
            arrayOfByte1.enforceInterface("android.os.incremental.IIncrementalService");
            i = arrayOfByte1.readInt();
            str2 = arrayOfByte1.readString();
            i = unlink(i, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 10:
            str2.enforceInterface("android.os.incremental.IIncrementalService");
            i = str2.readInt();
            str6 = str2.readString();
            param1Int2 = str2.readInt();
            str2 = str2.readString();
            i = makeLink(i, str6, param1Int2, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 9:
            str2.enforceInterface("android.os.incremental.IIncrementalService");
            i = str2.readInt();
            str6 = str2.readString();
            str5 = str2.readString();
            l2 = str2.readLong();
            l1 = str2.readLong();
            i = makeFileFromRange(i, str6, str5, l2, l1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 8:
            str2.enforceInterface("android.os.incremental.IIncrementalService");
            i = str2.readInt();
            str6 = str2.readString();
            if (str2.readInt() != 0) {
              IncrementalNewFileParams incrementalNewFileParams = IncrementalNewFileParams.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            i = makeFile(i, str6, (IncrementalNewFileParams)str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 7:
            str2.enforceInterface("android.os.incremental.IIncrementalService");
            i = str2.readInt();
            str2 = str2.readString();
            i = makeDirectories(i, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 6:
            str2.enforceInterface("android.os.incremental.IIncrementalService");
            i = str2.readInt();
            str2 = str2.readString();
            i = makeDirectory(i, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 5:
            str2.enforceInterface("android.os.incremental.IIncrementalService");
            i = str2.readInt();
            str2 = str2.readString();
            i = deleteBindMount(i, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 4:
            str2.enforceInterface("android.os.incremental.IIncrementalService");
            param1Int2 = str2.readInt();
            str6 = str2.readString();
            str5 = str2.readString();
            i = str2.readInt();
            i = makeBindMount(param1Int2, str6, str5, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 3:
            str2.enforceInterface("android.os.incremental.IIncrementalService");
            str6 = str2.readString();
            i = str2.readInt();
            param1Int2 = str2.readInt();
            i = createLinkedStorage(str6, i, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 2:
            str2.enforceInterface("android.os.incremental.IIncrementalService");
            str4 = str2.readString();
            if (str2.readInt() != 0) {
              DataLoaderParamsParcel dataLoaderParamsParcel = DataLoaderParamsParcel.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str6 = null;
            } 
            i = str2.readInt();
            iDataLoaderStatusListener = IDataLoaderStatusListener.Stub.asInterface(str2.readStrongBinder());
            if (str2.readInt() != 0) {
              StorageHealthCheckParams storageHealthCheckParams = StorageHealthCheckParams.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str5 = null;
            } 
            iStorageHealthListener = IStorageHealthListener.Stub.asInterface(str2.readStrongBinder());
            i = createStorage(str4, (DataLoaderParamsParcel)str6, i, iDataLoaderStatusListener, (StorageHealthCheckParams)str5, iStorageHealthListener);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 1:
            break;
        } 
        iStorageHealthListener.enforceInterface("android.os.incremental.IIncrementalService");
        String str1 = iStorageHealthListener.readString();
        int i = openStorage(str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("android.os.incremental.IIncrementalService");
      return true;
    }
    
    private static class Proxy implements IIncrementalService {
      public static IIncrementalService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.incremental.IIncrementalService";
      }
      
      public int openStorage(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IIncrementalService.Stub.getDefaultImpl() != null)
            return IIncrementalService.Stub.getDefaultImpl().openStorage(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int createStorage(String param2String, DataLoaderParamsParcel param2DataLoaderParamsParcel, int param2Int, IDataLoaderStatusListener param2IDataLoaderStatusListener, StorageHealthCheckParams param2StorageHealthCheckParams, IStorageHealthListener param2IStorageHealthListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          try {
            parcel1.writeString(param2String);
            if (param2DataLoaderParamsParcel != null) {
              parcel1.writeInt(1);
              param2DataLoaderParamsParcel.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeInt(param2Int);
              IBinder iBinder1 = null;
              if (param2IDataLoaderStatusListener != null) {
                iBinder2 = param2IDataLoaderStatusListener.asBinder();
              } else {
                iBinder2 = null;
              } 
              parcel1.writeStrongBinder(iBinder2);
              if (param2StorageHealthCheckParams != null) {
                parcel1.writeInt(1);
                param2StorageHealthCheckParams.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              IBinder iBinder2 = iBinder1;
              if (param2IStorageHealthListener != null)
                iBinder2 = param2IStorageHealthListener.asBinder(); 
              parcel1.writeStrongBinder(iBinder2);
              try {
                boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
                if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
                  param2Int = IIncrementalService.Stub.getDefaultImpl().createStorage(param2String, param2DataLoaderParamsParcel, param2Int, param2IDataLoaderStatusListener, param2StorageHealthCheckParams, param2IStorageHealthListener);
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Int;
                } 
                parcel2.readException();
                param2Int = parcel2.readInt();
                parcel2.recycle();
                parcel1.recycle();
                return param2Int;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public int createLinkedStorage(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
            param2Int1 = IIncrementalService.Stub.getDefaultImpl().createLinkedStorage(param2String, param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int makeBindMount(int param2Int1, String param2String1, String param2String2, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
            param2Int1 = IIncrementalService.Stub.getDefaultImpl().makeBindMount(param2Int1, param2String1, param2String2, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int deleteBindMount(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
            param2Int = IIncrementalService.Stub.getDefaultImpl().deleteBindMount(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int makeDirectory(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
            param2Int = IIncrementalService.Stub.getDefaultImpl().makeDirectory(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int makeDirectories(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
            param2Int = IIncrementalService.Stub.getDefaultImpl().makeDirectories(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int makeFile(int param2Int, String param2String, IncrementalNewFileParams param2IncrementalNewFileParams) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2IncrementalNewFileParams != null) {
            parcel1.writeInt(1);
            param2IncrementalNewFileParams.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
            param2Int = IIncrementalService.Stub.getDefaultImpl().makeFile(param2Int, param2String, param2IncrementalNewFileParams);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int makeFileFromRange(int param2Int, String param2String1, String param2String2, long param2Long1, long param2Long2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeString(param2String1);
              try {
                parcel1.writeString(param2String2);
                try {
                  parcel1.writeLong(param2Long1);
                  parcel1.writeLong(param2Long2);
                  boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
                  if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
                    param2Int = IIncrementalService.Stub.getDefaultImpl().makeFileFromRange(param2Int, param2String1, param2String2, param2Long1, param2Long2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Int;
                  } 
                  parcel2.readException();
                  param2Int = parcel2.readInt();
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Int;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public int makeLink(int param2Int1, String param2String1, int param2Int2, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
            param2Int1 = IIncrementalService.Stub.getDefaultImpl().makeLink(param2Int1, param2String1, param2Int2, param2String2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int unlink(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
            param2Int = IIncrementalService.Stub.getDefaultImpl().unlink(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isFileRangeLoaded(int param2Int, String param2String, long param2Long1, long param2Long2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeString(param2String);
              try {
                parcel1.writeLong(param2Long1);
                try {
                  parcel1.writeLong(param2Long2);
                  IBinder iBinder = this.mRemote;
                  boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
                  if (!bool2 && IIncrementalService.Stub.getDefaultImpl() != null) {
                    bool1 = IIncrementalService.Stub.getDefaultImpl().isFileRangeLoaded(param2Int, param2String, param2Long1, param2Long2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return bool1;
                  } 
                  parcel2.readException();
                  param2Int = parcel2.readInt();
                  if (param2Int != 0)
                    bool1 = true; 
                  parcel2.recycle();
                  parcel1.recycle();
                  return bool1;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public byte[] getMetadataByPath(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IIncrementalService.Stub.getDefaultImpl() != null)
            return IIncrementalService.Stub.getDefaultImpl().getMetadataByPath(param2Int, param2String); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getMetadataById(int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
            param2ArrayOfbyte = IIncrementalService.Stub.getDefaultImpl().getMetadataById(param2Int, param2ArrayOfbyte);
            return param2ArrayOfbyte;
          } 
          parcel2.readException();
          param2ArrayOfbyte = parcel2.createByteArray();
          return param2ArrayOfbyte;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean startLoading(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(15, parcel1, parcel2, 0);
          if (!bool2 && IIncrementalService.Stub.getDefaultImpl() != null) {
            bool1 = IIncrementalService.Stub.getDefaultImpl().startLoading(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteStorage(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
            IIncrementalService.Stub.getDefaultImpl().deleteStorage(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableReadLogs(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
            IIncrementalService.Stub.getDefaultImpl().disableReadLogs(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean configureNativeBinaries(int param2Int, String param2String1, String param2String2, String param2String3, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeString(param2String1);
              try {
                parcel1.writeString(param2String2);
                try {
                  boolean bool2;
                  parcel1.writeString(param2String3);
                  boolean bool1 = true;
                  if (param2Boolean) {
                    bool2 = true;
                  } else {
                    bool2 = false;
                  } 
                  parcel1.writeInt(bool2);
                  try {
                    boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
                    if (!bool && IIncrementalService.Stub.getDefaultImpl() != null) {
                      param2Boolean = IIncrementalService.Stub.getDefaultImpl().configureNativeBinaries(param2Int, param2String1, param2String2, param2String3, param2Boolean);
                      parcel2.recycle();
                      parcel1.recycle();
                      return param2Boolean;
                    } 
                    parcel2.readException();
                    param2Int = parcel2.readInt();
                    if (param2Int != 0) {
                      param2Boolean = bool1;
                    } else {
                      param2Boolean = false;
                    } 
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Boolean;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public boolean waitForNativeBinariesExtraction(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.incremental.IIncrementalService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(19, parcel1, parcel2, 0);
          if (!bool2 && IIncrementalService.Stub.getDefaultImpl() != null) {
            bool1 = IIncrementalService.Stub.getDefaultImpl().waitForNativeBinariesExtraction(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IIncrementalService param1IIncrementalService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IIncrementalService != null) {
          Proxy.sDefaultImpl = param1IIncrementalService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IIncrementalService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
