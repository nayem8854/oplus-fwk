package android.os;

import android.net.INetworkManagementEventObserver;
import android.net.ITetheringStatsProvider;
import android.net.InterfaceConfiguration;
import android.net.Network;
import android.net.NetworkStats;
import android.net.RouteInfo;
import android.net.UidRange;
import java.util.ArrayList;
import java.util.List;

public interface INetworkManagementService extends IInterface {
  void addIdleTimer(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void addInterfaceToLocalNetwork(String paramString, List<RouteInfo> paramList) throws RemoteException;
  
  void addInterfaceToNetwork(String paramString, int paramInt) throws RemoteException;
  
  void addLegacyRouteForNetId(int paramInt1, RouteInfo paramRouteInfo, int paramInt2) throws RemoteException;
  
  boolean addNetworkRestriction(int paramInt, String[] paramArrayOfString) throws RemoteException;
  
  void addRoute(int paramInt, RouteInfo paramRouteInfo) throws RemoteException;
  
  void addVpnUidRanges(int paramInt, UidRange[] paramArrayOfUidRange) throws RemoteException;
  
  void allowProtect(int paramInt) throws RemoteException;
  
  void clearDefaultNetId() throws RemoteException;
  
  void clearInterfaceAddresses(String paramString) throws RemoteException;
  
  void clearUidTos(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  void denyProtect(int paramInt) throws RemoteException;
  
  void disableIpv6(String paramString) throws RemoteException;
  
  void disableNat(String paramString1, String paramString2) throws RemoteException;
  
  void enableIpv6(String paramString) throws RemoteException;
  
  void enableNat(String paramString1, String paramString2) throws RemoteException;
  
  String executeShellToSetIptables(String paramString) throws RemoteException;
  
  String[] getDnsForwarders() throws RemoteException;
  
  InterfaceConfiguration getInterfaceConfig(String paramString) throws RemoteException;
  
  boolean getIpForwardingEnabled() throws RemoteException;
  
  NetworkStats getNetworkStatsTethering(int paramInt) throws RemoteException;
  
  boolean isBandwidthControlEnabled() throws RemoteException;
  
  boolean isFirewallEnabled() throws RemoteException;
  
  boolean isNetworkActive() throws RemoteException;
  
  boolean isNetworkRestricted(int paramInt) throws RemoteException;
  
  boolean isTetheringStarted() throws RemoteException;
  
  String[] listInterfaces() throws RemoteException;
  
  String[] listTetheredInterfaces() throws RemoteException;
  
  String oplusNetdGetSysProc(int paramInt1, int paramInt2, String paramString1, String paramString2) throws RemoteException;
  
  String oppoNetdCmdParse(String paramString, int[] paramArrayOfint) throws RemoteException;
  
  void registerNetworkActivityListener(INetworkActivityListener paramINetworkActivityListener) throws RemoteException;
  
  void registerObserver(INetworkManagementEventObserver paramINetworkManagementEventObserver) throws RemoteException;
  
  void registerTetheringStatsProvider(ITetheringStatsProvider paramITetheringStatsProvider, String paramString) throws RemoteException;
  
  void removeIdleTimer(String paramString) throws RemoteException;
  
  void removeInterfaceAlert(String paramString) throws RemoteException;
  
  void removeInterfaceFromLocalNetwork(String paramString) throws RemoteException;
  
  void removeInterfaceFromNetwork(String paramString, int paramInt) throws RemoteException;
  
  void removeInterfaceQuota(String paramString) throws RemoteException;
  
  boolean removeNetworkRestriction(int paramInt, String[] paramArrayOfString) throws RemoteException;
  
  boolean removeNetworkRestrictionAll(int paramInt) throws RemoteException;
  
  void removeRoute(int paramInt, RouteInfo paramRouteInfo) throws RemoteException;
  
  int removeRoutesFromLocalNetwork(List<RouteInfo> paramList) throws RemoteException;
  
  void removeVpnUidRanges(int paramInt, UidRange[] paramArrayOfUidRange) throws RemoteException;
  
  void setAllowOnlyVpnForUids(boolean paramBoolean, UidRange[] paramArrayOfUidRange) throws RemoteException;
  
  boolean setDataSaverModeEnabled(boolean paramBoolean) throws RemoteException;
  
  void setDefaultNetId(int paramInt) throws RemoteException;
  
  void setDnsForwarders(Network paramNetwork, String[] paramArrayOfString) throws RemoteException;
  
  void setFirewallChainEnabled(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setFirewallEnabled(boolean paramBoolean) throws RemoteException;
  
  void setFirewallInterfaceRule(String paramString, boolean paramBoolean) throws RemoteException;
  
  void setFirewallUidRule(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void setFirewallUidRuleForNetworkType(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void setFirewallUidRules(int paramInt, int[] paramArrayOfint1, int[] paramArrayOfint2) throws RemoteException;
  
  void setGlobalAlert(long paramLong) throws RemoteException;
  
  void setIPv6AddrGenMode(String paramString, int paramInt) throws RemoteException;
  
  void setInterfaceAlert(String paramString, long paramLong) throws RemoteException;
  
  void setInterfaceConfig(String paramString, InterfaceConfiguration paramInterfaceConfiguration) throws RemoteException;
  
  void setInterfaceDown(String paramString) throws RemoteException;
  
  void setInterfaceIpv6PrivacyExtensions(String paramString, boolean paramBoolean) throws RemoteException;
  
  void setInterfaceQuota(String paramString, long paramLong) throws RemoteException;
  
  void setInterfaceUp(String paramString) throws RemoteException;
  
  void setIpForwardingEnabled(boolean paramBoolean) throws RemoteException;
  
  void setMtu(String paramString, int paramInt) throws RemoteException;
  
  void setNetworkPermission(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean setNetworkRestriction(int paramInt) throws RemoteException;
  
  void setUidCleartextNetworkPolicy(int paramInt1, int paramInt2) throws RemoteException;
  
  void setUidMeteredNetworkBlacklist(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setUidMeteredNetworkWhitelist(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setUidTos(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  void shutdown() throws RemoteException;
  
  int[] socketAllDestroy(int[] paramArrayOfint) throws RemoteException;
  
  void startInterfaceForwarding(String paramString1, String paramString2) throws RemoteException;
  
  void startTethering(String[] paramArrayOfString) throws RemoteException;
  
  void startTetheringWithConfiguration(boolean paramBoolean, String[] paramArrayOfString) throws RemoteException;
  
  void stopInterfaceForwarding(String paramString1, String paramString2) throws RemoteException;
  
  void stopTethering() throws RemoteException;
  
  void tetherInterface(String paramString) throws RemoteException;
  
  void tetherLimitReached(ITetheringStatsProvider paramITetheringStatsProvider) throws RemoteException;
  
  void unregisterNetworkActivityListener(INetworkActivityListener paramINetworkActivityListener) throws RemoteException;
  
  void unregisterObserver(INetworkManagementEventObserver paramINetworkManagementEventObserver) throws RemoteException;
  
  void unregisterTetheringStatsProvider(ITetheringStatsProvider paramITetheringStatsProvider) throws RemoteException;
  
  void untetherInterface(String paramString) throws RemoteException;
  
  class Default implements INetworkManagementService {
    public void registerObserver(INetworkManagementEventObserver param1INetworkManagementEventObserver) throws RemoteException {}
    
    public void unregisterObserver(INetworkManagementEventObserver param1INetworkManagementEventObserver) throws RemoteException {}
    
    public String[] listInterfaces() throws RemoteException {
      return null;
    }
    
    public InterfaceConfiguration getInterfaceConfig(String param1String) throws RemoteException {
      return null;
    }
    
    public void setInterfaceConfig(String param1String, InterfaceConfiguration param1InterfaceConfiguration) throws RemoteException {}
    
    public void clearInterfaceAddresses(String param1String) throws RemoteException {}
    
    public void setInterfaceDown(String param1String) throws RemoteException {}
    
    public void setInterfaceUp(String param1String) throws RemoteException {}
    
    public void setInterfaceIpv6PrivacyExtensions(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void disableIpv6(String param1String) throws RemoteException {}
    
    public void enableIpv6(String param1String) throws RemoteException {}
    
    public void setIPv6AddrGenMode(String param1String, int param1Int) throws RemoteException {}
    
    public void addRoute(int param1Int, RouteInfo param1RouteInfo) throws RemoteException {}
    
    public void removeRoute(int param1Int, RouteInfo param1RouteInfo) throws RemoteException {}
    
    public void setMtu(String param1String, int param1Int) throws RemoteException {}
    
    public void shutdown() throws RemoteException {}
    
    public boolean getIpForwardingEnabled() throws RemoteException {
      return false;
    }
    
    public void setIpForwardingEnabled(boolean param1Boolean) throws RemoteException {}
    
    public void startTethering(String[] param1ArrayOfString) throws RemoteException {}
    
    public void startTetheringWithConfiguration(boolean param1Boolean, String[] param1ArrayOfString) throws RemoteException {}
    
    public void stopTethering() throws RemoteException {}
    
    public boolean isTetheringStarted() throws RemoteException {
      return false;
    }
    
    public void tetherInterface(String param1String) throws RemoteException {}
    
    public void untetherInterface(String param1String) throws RemoteException {}
    
    public String[] listTetheredInterfaces() throws RemoteException {
      return null;
    }
    
    public void setDnsForwarders(Network param1Network, String[] param1ArrayOfString) throws RemoteException {}
    
    public String[] getDnsForwarders() throws RemoteException {
      return null;
    }
    
    public void startInterfaceForwarding(String param1String1, String param1String2) throws RemoteException {}
    
    public void stopInterfaceForwarding(String param1String1, String param1String2) throws RemoteException {}
    
    public void enableNat(String param1String1, String param1String2) throws RemoteException {}
    
    public void disableNat(String param1String1, String param1String2) throws RemoteException {}
    
    public void registerTetheringStatsProvider(ITetheringStatsProvider param1ITetheringStatsProvider, String param1String) throws RemoteException {}
    
    public void unregisterTetheringStatsProvider(ITetheringStatsProvider param1ITetheringStatsProvider) throws RemoteException {}
    
    public void tetherLimitReached(ITetheringStatsProvider param1ITetheringStatsProvider) throws RemoteException {}
    
    public NetworkStats getNetworkStatsTethering(int param1Int) throws RemoteException {
      return null;
    }
    
    public void setInterfaceQuota(String param1String, long param1Long) throws RemoteException {}
    
    public void removeInterfaceQuota(String param1String) throws RemoteException {}
    
    public void setInterfaceAlert(String param1String, long param1Long) throws RemoteException {}
    
    public void removeInterfaceAlert(String param1String) throws RemoteException {}
    
    public void setGlobalAlert(long param1Long) throws RemoteException {}
    
    public void setUidMeteredNetworkBlacklist(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void setUidMeteredNetworkWhitelist(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean setDataSaverModeEnabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public void setUidCleartextNetworkPolicy(int param1Int1, int param1Int2) throws RemoteException {}
    
    public boolean isBandwidthControlEnabled() throws RemoteException {
      return false;
    }
    
    public void addIdleTimer(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void removeIdleTimer(String param1String) throws RemoteException {}
    
    public void setFirewallEnabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isFirewallEnabled() throws RemoteException {
      return false;
    }
    
    public void setFirewallInterfaceRule(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void setFirewallUidRule(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void setFirewallUidRules(int param1Int, int[] param1ArrayOfint1, int[] param1ArrayOfint2) throws RemoteException {}
    
    public void setFirewallChainEnabled(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void addVpnUidRanges(int param1Int, UidRange[] param1ArrayOfUidRange) throws RemoteException {}
    
    public void removeVpnUidRanges(int param1Int, UidRange[] param1ArrayOfUidRange) throws RemoteException {}
    
    public void registerNetworkActivityListener(INetworkActivityListener param1INetworkActivityListener) throws RemoteException {}
    
    public void unregisterNetworkActivityListener(INetworkActivityListener param1INetworkActivityListener) throws RemoteException {}
    
    public boolean isNetworkActive() throws RemoteException {
      return false;
    }
    
    public void addInterfaceToNetwork(String param1String, int param1Int) throws RemoteException {}
    
    public void removeInterfaceFromNetwork(String param1String, int param1Int) throws RemoteException {}
    
    public void addLegacyRouteForNetId(int param1Int1, RouteInfo param1RouteInfo, int param1Int2) throws RemoteException {}
    
    public void setDefaultNetId(int param1Int) throws RemoteException {}
    
    public void clearDefaultNetId() throws RemoteException {}
    
    public void setNetworkPermission(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void allowProtect(int param1Int) throws RemoteException {}
    
    public void denyProtect(int param1Int) throws RemoteException {}
    
    public void addInterfaceToLocalNetwork(String param1String, List<RouteInfo> param1List) throws RemoteException {}
    
    public void removeInterfaceFromLocalNetwork(String param1String) throws RemoteException {}
    
    public int removeRoutesFromLocalNetwork(List<RouteInfo> param1List) throws RemoteException {
      return 0;
    }
    
    public void setAllowOnlyVpnForUids(boolean param1Boolean, UidRange[] param1ArrayOfUidRange) throws RemoteException {}
    
    public boolean isNetworkRestricted(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setFirewallUidRuleForNetworkType(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void setUidTos(String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public void clearUidTos(String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public String oppoNetdCmdParse(String param1String, int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public String oplusNetdGetSysProc(int param1Int1, int param1Int2, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public String executeShellToSetIptables(String param1String) throws RemoteException {
      return null;
    }
    
    public boolean setNetworkRestriction(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean addNetworkRestriction(int param1Int, String[] param1ArrayOfString) throws RemoteException {
      return false;
    }
    
    public boolean removeNetworkRestriction(int param1Int, String[] param1ArrayOfString) throws RemoteException {
      return false;
    }
    
    public boolean removeNetworkRestrictionAll(int param1Int) throws RemoteException {
      return false;
    }
    
    public int[] socketAllDestroy(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INetworkManagementService {
    private static final String DESCRIPTOR = "android.os.INetworkManagementService";
    
    static final int TRANSACTION_addIdleTimer = 46;
    
    static final int TRANSACTION_addInterfaceToLocalNetwork = 67;
    
    static final int TRANSACTION_addInterfaceToNetwork = 59;
    
    static final int TRANSACTION_addLegacyRouteForNetId = 61;
    
    static final int TRANSACTION_addNetworkRestriction = 79;
    
    static final int TRANSACTION_addRoute = 13;
    
    static final int TRANSACTION_addVpnUidRanges = 54;
    
    static final int TRANSACTION_allowProtect = 65;
    
    static final int TRANSACTION_clearDefaultNetId = 63;
    
    static final int TRANSACTION_clearInterfaceAddresses = 6;
    
    static final int TRANSACTION_clearUidTos = 74;
    
    static final int TRANSACTION_denyProtect = 66;
    
    static final int TRANSACTION_disableIpv6 = 10;
    
    static final int TRANSACTION_disableNat = 31;
    
    static final int TRANSACTION_enableIpv6 = 11;
    
    static final int TRANSACTION_enableNat = 30;
    
    static final int TRANSACTION_executeShellToSetIptables = 77;
    
    static final int TRANSACTION_getDnsForwarders = 27;
    
    static final int TRANSACTION_getInterfaceConfig = 4;
    
    static final int TRANSACTION_getIpForwardingEnabled = 17;
    
    static final int TRANSACTION_getNetworkStatsTethering = 35;
    
    static final int TRANSACTION_isBandwidthControlEnabled = 45;
    
    static final int TRANSACTION_isFirewallEnabled = 49;
    
    static final int TRANSACTION_isNetworkActive = 58;
    
    static final int TRANSACTION_isNetworkRestricted = 71;
    
    static final int TRANSACTION_isTetheringStarted = 22;
    
    static final int TRANSACTION_listInterfaces = 3;
    
    static final int TRANSACTION_listTetheredInterfaces = 25;
    
    static final int TRANSACTION_oplusNetdGetSysProc = 76;
    
    static final int TRANSACTION_oppoNetdCmdParse = 75;
    
    static final int TRANSACTION_registerNetworkActivityListener = 56;
    
    static final int TRANSACTION_registerObserver = 1;
    
    static final int TRANSACTION_registerTetheringStatsProvider = 32;
    
    static final int TRANSACTION_removeIdleTimer = 47;
    
    static final int TRANSACTION_removeInterfaceAlert = 39;
    
    static final int TRANSACTION_removeInterfaceFromLocalNetwork = 68;
    
    static final int TRANSACTION_removeInterfaceFromNetwork = 60;
    
    static final int TRANSACTION_removeInterfaceQuota = 37;
    
    static final int TRANSACTION_removeNetworkRestriction = 80;
    
    static final int TRANSACTION_removeNetworkRestrictionAll = 81;
    
    static final int TRANSACTION_removeRoute = 14;
    
    static final int TRANSACTION_removeRoutesFromLocalNetwork = 69;
    
    static final int TRANSACTION_removeVpnUidRanges = 55;
    
    static final int TRANSACTION_setAllowOnlyVpnForUids = 70;
    
    static final int TRANSACTION_setDataSaverModeEnabled = 43;
    
    static final int TRANSACTION_setDefaultNetId = 62;
    
    static final int TRANSACTION_setDnsForwarders = 26;
    
    static final int TRANSACTION_setFirewallChainEnabled = 53;
    
    static final int TRANSACTION_setFirewallEnabled = 48;
    
    static final int TRANSACTION_setFirewallInterfaceRule = 50;
    
    static final int TRANSACTION_setFirewallUidRule = 51;
    
    static final int TRANSACTION_setFirewallUidRuleForNetworkType = 72;
    
    static final int TRANSACTION_setFirewallUidRules = 52;
    
    static final int TRANSACTION_setGlobalAlert = 40;
    
    static final int TRANSACTION_setIPv6AddrGenMode = 12;
    
    static final int TRANSACTION_setInterfaceAlert = 38;
    
    static final int TRANSACTION_setInterfaceConfig = 5;
    
    static final int TRANSACTION_setInterfaceDown = 7;
    
    static final int TRANSACTION_setInterfaceIpv6PrivacyExtensions = 9;
    
    static final int TRANSACTION_setInterfaceQuota = 36;
    
    static final int TRANSACTION_setInterfaceUp = 8;
    
    static final int TRANSACTION_setIpForwardingEnabled = 18;
    
    static final int TRANSACTION_setMtu = 15;
    
    static final int TRANSACTION_setNetworkPermission = 64;
    
    static final int TRANSACTION_setNetworkRestriction = 78;
    
    static final int TRANSACTION_setUidCleartextNetworkPolicy = 44;
    
    static final int TRANSACTION_setUidMeteredNetworkBlacklist = 41;
    
    static final int TRANSACTION_setUidMeteredNetworkWhitelist = 42;
    
    static final int TRANSACTION_setUidTos = 73;
    
    static final int TRANSACTION_shutdown = 16;
    
    static final int TRANSACTION_socketAllDestroy = 82;
    
    static final int TRANSACTION_startInterfaceForwarding = 28;
    
    static final int TRANSACTION_startTethering = 19;
    
    static final int TRANSACTION_startTetheringWithConfiguration = 20;
    
    static final int TRANSACTION_stopInterfaceForwarding = 29;
    
    static final int TRANSACTION_stopTethering = 21;
    
    static final int TRANSACTION_tetherInterface = 23;
    
    static final int TRANSACTION_tetherLimitReached = 34;
    
    static final int TRANSACTION_unregisterNetworkActivityListener = 57;
    
    static final int TRANSACTION_unregisterObserver = 2;
    
    static final int TRANSACTION_unregisterTetheringStatsProvider = 33;
    
    static final int TRANSACTION_untetherInterface = 24;
    
    public Stub() {
      attachInterface(this, "android.os.INetworkManagementService");
    }
    
    public static INetworkManagementService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.INetworkManagementService");
      if (iInterface != null && iInterface instanceof INetworkManagementService)
        return (INetworkManagementService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 82:
          return "socketAllDestroy";
        case 81:
          return "removeNetworkRestrictionAll";
        case 80:
          return "removeNetworkRestriction";
        case 79:
          return "addNetworkRestriction";
        case 78:
          return "setNetworkRestriction";
        case 77:
          return "executeShellToSetIptables";
        case 76:
          return "oplusNetdGetSysProc";
        case 75:
          return "oppoNetdCmdParse";
        case 74:
          return "clearUidTos";
        case 73:
          return "setUidTos";
        case 72:
          return "setFirewallUidRuleForNetworkType";
        case 71:
          return "isNetworkRestricted";
        case 70:
          return "setAllowOnlyVpnForUids";
        case 69:
          return "removeRoutesFromLocalNetwork";
        case 68:
          return "removeInterfaceFromLocalNetwork";
        case 67:
          return "addInterfaceToLocalNetwork";
        case 66:
          return "denyProtect";
        case 65:
          return "allowProtect";
        case 64:
          return "setNetworkPermission";
        case 63:
          return "clearDefaultNetId";
        case 62:
          return "setDefaultNetId";
        case 61:
          return "addLegacyRouteForNetId";
        case 60:
          return "removeInterfaceFromNetwork";
        case 59:
          return "addInterfaceToNetwork";
        case 58:
          return "isNetworkActive";
        case 57:
          return "unregisterNetworkActivityListener";
        case 56:
          return "registerNetworkActivityListener";
        case 55:
          return "removeVpnUidRanges";
        case 54:
          return "addVpnUidRanges";
        case 53:
          return "setFirewallChainEnabled";
        case 52:
          return "setFirewallUidRules";
        case 51:
          return "setFirewallUidRule";
        case 50:
          return "setFirewallInterfaceRule";
        case 49:
          return "isFirewallEnabled";
        case 48:
          return "setFirewallEnabled";
        case 47:
          return "removeIdleTimer";
        case 46:
          return "addIdleTimer";
        case 45:
          return "isBandwidthControlEnabled";
        case 44:
          return "setUidCleartextNetworkPolicy";
        case 43:
          return "setDataSaverModeEnabled";
        case 42:
          return "setUidMeteredNetworkWhitelist";
        case 41:
          return "setUidMeteredNetworkBlacklist";
        case 40:
          return "setGlobalAlert";
        case 39:
          return "removeInterfaceAlert";
        case 38:
          return "setInterfaceAlert";
        case 37:
          return "removeInterfaceQuota";
        case 36:
          return "setInterfaceQuota";
        case 35:
          return "getNetworkStatsTethering";
        case 34:
          return "tetherLimitReached";
        case 33:
          return "unregisterTetheringStatsProvider";
        case 32:
          return "registerTetheringStatsProvider";
        case 31:
          return "disableNat";
        case 30:
          return "enableNat";
        case 29:
          return "stopInterfaceForwarding";
        case 28:
          return "startInterfaceForwarding";
        case 27:
          return "getDnsForwarders";
        case 26:
          return "setDnsForwarders";
        case 25:
          return "listTetheredInterfaces";
        case 24:
          return "untetherInterface";
        case 23:
          return "tetherInterface";
        case 22:
          return "isTetheringStarted";
        case 21:
          return "stopTethering";
        case 20:
          return "startTetheringWithConfiguration";
        case 19:
          return "startTethering";
        case 18:
          return "setIpForwardingEnabled";
        case 17:
          return "getIpForwardingEnabled";
        case 16:
          return "shutdown";
        case 15:
          return "setMtu";
        case 14:
          return "removeRoute";
        case 13:
          return "addRoute";
        case 12:
          return "setIPv6AddrGenMode";
        case 11:
          return "enableIpv6";
        case 10:
          return "disableIpv6";
        case 9:
          return "setInterfaceIpv6PrivacyExtensions";
        case 8:
          return "setInterfaceUp";
        case 7:
          return "setInterfaceDown";
        case 6:
          return "clearInterfaceAddresses";
        case 5:
          return "setInterfaceConfig";
        case 4:
          return "getInterfaceConfig";
        case 3:
          return "listInterfaces";
        case 2:
          return "unregisterObserver";
        case 1:
          break;
      } 
      return "registerObserver";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool10;
        int i5;
        boolean bool9;
        int i4;
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int i, arrayOfInt3[];
        String arrayOfString4[], str7;
        int[] arrayOfInt2;
        String str6;
        UidRange[] arrayOfUidRange2;
        ArrayList<RouteInfo> arrayList2;
        String str5;
        ArrayList<RouteInfo> arrayList1;
        INetworkActivityListener iNetworkActivityListener;
        UidRange[] arrayOfUidRange1;
        int[] arrayOfInt1;
        String str4;
        NetworkStats networkStats;
        ITetheringStatsProvider iTetheringStatsProvider1;
        String str3, arrayOfString3[], str2, arrayOfString2[], str1;
        InterfaceConfiguration interfaceConfiguration;
        String arrayOfString1[], str10;
        int[] arrayOfInt4;
        String str9;
        ITetheringStatsProvider iTetheringStatsProvider2;
        String str8, str11;
        int i6;
        long l;
        boolean bool11 = false, bool12 = false, bool13 = false, bool14 = false, bool15 = false, bool16 = false, bool17 = false, bool18 = false, bool19 = false, bool20 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 82:
            param1Parcel1.enforceInterface("android.os.INetworkManagementService");
            arrayOfInt3 = param1Parcel1.createIntArray();
            arrayOfInt3 = socketAllDestroy(arrayOfInt3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt3);
            return true;
          case 81:
            arrayOfInt3.enforceInterface("android.os.INetworkManagementService");
            param1Int1 = arrayOfInt3.readInt();
            bool10 = removeNetworkRestrictionAll(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 80:
            arrayOfInt3.enforceInterface("android.os.INetworkManagementService");
            i5 = arrayOfInt3.readInt();
            arrayOfString4 = arrayOfInt3.createStringArray();
            bool9 = removeNetworkRestriction(i5, arrayOfString4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 79:
            arrayOfString4.enforceInterface("android.os.INetworkManagementService");
            i4 = arrayOfString4.readInt();
            arrayOfString4 = arrayOfString4.createStringArray();
            bool8 = addNetworkRestriction(i4, arrayOfString4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 78:
            arrayOfString4.enforceInterface("android.os.INetworkManagementService");
            i3 = arrayOfString4.readInt();
            bool7 = setNetworkRestriction(i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 77:
            arrayOfString4.enforceInterface("android.os.INetworkManagementService");
            str7 = arrayOfString4.readString();
            str7 = executeShellToSetIptables(str7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str7);
            return true;
          case 76:
            str7.enforceInterface("android.os.INetworkManagementService");
            param1Int2 = str7.readInt();
            i2 = str7.readInt();
            str10 = str7.readString();
            str7 = str7.readString();
            str7 = oplusNetdGetSysProc(param1Int2, i2, str10, str7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str7);
            return true;
          case 75:
            str7.enforceInterface("android.os.INetworkManagementService");
            str10 = str7.readString();
            arrayOfInt2 = str7.createIntArray();
            str6 = oppoNetdCmdParse(str10, arrayOfInt2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str6);
            return true;
          case 74:
            str6.enforceInterface("android.os.INetworkManagementService");
            str10 = str6.readString();
            str11 = str6.readString();
            str6 = str6.readString();
            clearUidTos(str10, str11, str6);
            param1Parcel2.writeNoException();
            return true;
          case 73:
            str6.enforceInterface("android.os.INetworkManagementService");
            str10 = str6.readString();
            str11 = str6.readString();
            str6 = str6.readString();
            setUidTos(str10, str11, str6);
            param1Parcel2.writeNoException();
            return true;
          case 72:
            str6.enforceInterface("android.os.INetworkManagementService");
            param1Int2 = str6.readInt();
            i2 = str6.readInt();
            i6 = str6.readInt();
            setFirewallUidRuleForNetworkType(param1Int2, i2, i6);
            param1Parcel2.writeNoException();
            return true;
          case 71:
            str6.enforceInterface("android.os.INetworkManagementService");
            i2 = str6.readInt();
            bool6 = isNetworkRestricted(i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 70:
            str6.enforceInterface("android.os.INetworkManagementService");
            bool19 = bool20;
            if (str6.readInt() != 0)
              bool19 = true; 
            arrayOfUidRange2 = str6.<UidRange>createTypedArray(UidRange.CREATOR);
            setAllowOnlyVpnForUids(bool19, arrayOfUidRange2);
            param1Parcel2.writeNoException();
            return true;
          case 69:
            arrayOfUidRange2.enforceInterface("android.os.INetworkManagementService");
            arrayList2 = arrayOfUidRange2.createTypedArrayList(RouteInfo.CREATOR);
            i1 = removeRoutesFromLocalNetwork(arrayList2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i1);
            return true;
          case 68:
            arrayList2.enforceInterface("android.os.INetworkManagementService");
            str5 = arrayList2.readString();
            removeInterfaceFromLocalNetwork(str5);
            param1Parcel2.writeNoException();
            return true;
          case 67:
            str5.enforceInterface("android.os.INetworkManagementService");
            str10 = str5.readString();
            arrayList1 = str5.createTypedArrayList(RouteInfo.CREATOR);
            addInterfaceToLocalNetwork(str10, arrayList1);
            param1Parcel2.writeNoException();
            return true;
          case 66:
            arrayList1.enforceInterface("android.os.INetworkManagementService");
            i1 = arrayList1.readInt();
            denyProtect(i1);
            param1Parcel2.writeNoException();
            return true;
          case 65:
            arrayList1.enforceInterface("android.os.INetworkManagementService");
            i1 = arrayList1.readInt();
            allowProtect(i1);
            param1Parcel2.writeNoException();
            return true;
          case 64:
            arrayList1.enforceInterface("android.os.INetworkManagementService");
            i1 = arrayList1.readInt();
            param1Int2 = arrayList1.readInt();
            setNetworkPermission(i1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 63:
            arrayList1.enforceInterface("android.os.INetworkManagementService");
            clearDefaultNetId();
            param1Parcel2.writeNoException();
            return true;
          case 62:
            arrayList1.enforceInterface("android.os.INetworkManagementService");
            i1 = arrayList1.readInt();
            setDefaultNetId(i1);
            param1Parcel2.writeNoException();
            return true;
          case 61:
            arrayList1.enforceInterface("android.os.INetworkManagementService");
            i1 = arrayList1.readInt();
            if (arrayList1.readInt() != 0) {
              RouteInfo routeInfo = RouteInfo.CREATOR.createFromParcel((Parcel)arrayList1);
            } else {
              str10 = null;
            } 
            param1Int2 = arrayList1.readInt();
            addLegacyRouteForNetId(i1, (RouteInfo)str10, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 60:
            arrayList1.enforceInterface("android.os.INetworkManagementService");
            str10 = arrayList1.readString();
            i1 = arrayList1.readInt();
            removeInterfaceFromNetwork(str10, i1);
            param1Parcel2.writeNoException();
            return true;
          case 59:
            arrayList1.enforceInterface("android.os.INetworkManagementService");
            str10 = arrayList1.readString();
            i1 = arrayList1.readInt();
            addInterfaceToNetwork(str10, i1);
            param1Parcel2.writeNoException();
            return true;
          case 58:
            arrayList1.enforceInterface("android.os.INetworkManagementService");
            bool5 = isNetworkActive();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 57:
            arrayList1.enforceInterface("android.os.INetworkManagementService");
            iNetworkActivityListener = INetworkActivityListener.Stub.asInterface(arrayList1.readStrongBinder());
            unregisterNetworkActivityListener(iNetworkActivityListener);
            param1Parcel2.writeNoException();
            return true;
          case 56:
            iNetworkActivityListener.enforceInterface("android.os.INetworkManagementService");
            iNetworkActivityListener = INetworkActivityListener.Stub.asInterface(iNetworkActivityListener.readStrongBinder());
            registerNetworkActivityListener(iNetworkActivityListener);
            param1Parcel2.writeNoException();
            return true;
          case 55:
            iNetworkActivityListener.enforceInterface("android.os.INetworkManagementService");
            n = iNetworkActivityListener.readInt();
            arrayOfUidRange1 = iNetworkActivityListener.<UidRange>createTypedArray(UidRange.CREATOR);
            removeVpnUidRanges(n, arrayOfUidRange1);
            param1Parcel2.writeNoException();
            return true;
          case 54:
            arrayOfUidRange1.enforceInterface("android.os.INetworkManagementService");
            n = arrayOfUidRange1.readInt();
            arrayOfUidRange1 = arrayOfUidRange1.<UidRange>createTypedArray(UidRange.CREATOR);
            addVpnUidRanges(n, arrayOfUidRange1);
            param1Parcel2.writeNoException();
            return true;
          case 53:
            arrayOfUidRange1.enforceInterface("android.os.INetworkManagementService");
            n = arrayOfUidRange1.readInt();
            bool19 = bool11;
            if (arrayOfUidRange1.readInt() != 0)
              bool19 = true; 
            setFirewallChainEnabled(n, bool19);
            param1Parcel2.writeNoException();
            return true;
          case 52:
            arrayOfUidRange1.enforceInterface("android.os.INetworkManagementService");
            n = arrayOfUidRange1.readInt();
            arrayOfInt4 = arrayOfUidRange1.createIntArray();
            arrayOfInt1 = arrayOfUidRange1.createIntArray();
            setFirewallUidRules(n, arrayOfInt4, arrayOfInt1);
            param1Parcel2.writeNoException();
            return true;
          case 51:
            arrayOfInt1.enforceInterface("android.os.INetworkManagementService");
            param1Int2 = arrayOfInt1.readInt();
            n = arrayOfInt1.readInt();
            i6 = arrayOfInt1.readInt();
            setFirewallUidRule(param1Int2, n, i6);
            param1Parcel2.writeNoException();
            return true;
          case 50:
            arrayOfInt1.enforceInterface("android.os.INetworkManagementService");
            str9 = arrayOfInt1.readString();
            bool19 = bool12;
            if (arrayOfInt1.readInt() != 0)
              bool19 = true; 
            setFirewallInterfaceRule(str9, bool19);
            param1Parcel2.writeNoException();
            return true;
          case 49:
            arrayOfInt1.enforceInterface("android.os.INetworkManagementService");
            bool4 = isFirewallEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 48:
            arrayOfInt1.enforceInterface("android.os.INetworkManagementService");
            bool19 = bool13;
            if (arrayOfInt1.readInt() != 0)
              bool19 = true; 
            setFirewallEnabled(bool19);
            param1Parcel2.writeNoException();
            return true;
          case 47:
            arrayOfInt1.enforceInterface("android.os.INetworkManagementService");
            str4 = arrayOfInt1.readString();
            removeIdleTimer(str4);
            param1Parcel2.writeNoException();
            return true;
          case 46:
            str4.enforceInterface("android.os.INetworkManagementService");
            str9 = str4.readString();
            m = str4.readInt();
            param1Int2 = str4.readInt();
            addIdleTimer(str9, m, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 45:
            str4.enforceInterface("android.os.INetworkManagementService");
            bool3 = isBandwidthControlEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 44:
            str4.enforceInterface("android.os.INetworkManagementService");
            param1Int2 = str4.readInt();
            k = str4.readInt();
            setUidCleartextNetworkPolicy(param1Int2, k);
            param1Parcel2.writeNoException();
            return true;
          case 43:
            str4.enforceInterface("android.os.INetworkManagementService");
            bool19 = bool14;
            if (str4.readInt() != 0)
              bool19 = true; 
            bool2 = setDataSaverModeEnabled(bool19);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 42:
            str4.enforceInterface("android.os.INetworkManagementService");
            j = str4.readInt();
            bool19 = bool15;
            if (str4.readInt() != 0)
              bool19 = true; 
            setUidMeteredNetworkWhitelist(j, bool19);
            param1Parcel2.writeNoException();
            return true;
          case 41:
            str4.enforceInterface("android.os.INetworkManagementService");
            j = str4.readInt();
            bool19 = bool16;
            if (str4.readInt() != 0)
              bool19 = true; 
            setUidMeteredNetworkBlacklist(j, bool19);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            str4.enforceInterface("android.os.INetworkManagementService");
            l = str4.readLong();
            setGlobalAlert(l);
            param1Parcel2.writeNoException();
            return true;
          case 39:
            str4.enforceInterface("android.os.INetworkManagementService");
            str4 = str4.readString();
            removeInterfaceAlert(str4);
            param1Parcel2.writeNoException();
            return true;
          case 38:
            str4.enforceInterface("android.os.INetworkManagementService");
            str9 = str4.readString();
            l = str4.readLong();
            setInterfaceAlert(str9, l);
            param1Parcel2.writeNoException();
            return true;
          case 37:
            str4.enforceInterface("android.os.INetworkManagementService");
            str4 = str4.readString();
            removeInterfaceQuota(str4);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            str4.enforceInterface("android.os.INetworkManagementService");
            str9 = str4.readString();
            l = str4.readLong();
            setInterfaceQuota(str9, l);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            str4.enforceInterface("android.os.INetworkManagementService");
            j = str4.readInt();
            networkStats = getNetworkStatsTethering(j);
            param1Parcel2.writeNoException();
            if (networkStats != null) {
              param1Parcel2.writeInt(1);
              networkStats.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 34:
            networkStats.enforceInterface("android.os.INetworkManagementService");
            iTetheringStatsProvider1 = ITetheringStatsProvider.Stub.asInterface(networkStats.readStrongBinder());
            tetherLimitReached(iTetheringStatsProvider1);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            iTetheringStatsProvider1.enforceInterface("android.os.INetworkManagementService");
            iTetheringStatsProvider1 = ITetheringStatsProvider.Stub.asInterface(iTetheringStatsProvider1.readStrongBinder());
            unregisterTetheringStatsProvider(iTetheringStatsProvider1);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            iTetheringStatsProvider1.enforceInterface("android.os.INetworkManagementService");
            iTetheringStatsProvider2 = ITetheringStatsProvider.Stub.asInterface(iTetheringStatsProvider1.readStrongBinder());
            str3 = iTetheringStatsProvider1.readString();
            registerTetheringStatsProvider(iTetheringStatsProvider2, str3);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            str3.enforceInterface("android.os.INetworkManagementService");
            str8 = str3.readString();
            str3 = str3.readString();
            disableNat(str8, str3);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            str3.enforceInterface("android.os.INetworkManagementService");
            str8 = str3.readString();
            str3 = str3.readString();
            enableNat(str8, str3);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            str3.enforceInterface("android.os.INetworkManagementService");
            str8 = str3.readString();
            str3 = str3.readString();
            stopInterfaceForwarding(str8, str3);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            str3.enforceInterface("android.os.INetworkManagementService");
            str8 = str3.readString();
            str3 = str3.readString();
            startInterfaceForwarding(str8, str3);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            str3.enforceInterface("android.os.INetworkManagementService");
            arrayOfString3 = getDnsForwarders();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString3);
            return true;
          case 26:
            arrayOfString3.enforceInterface("android.os.INetworkManagementService");
            if (arrayOfString3.readInt() != 0) {
              Network network = Network.CREATOR.createFromParcel((Parcel)arrayOfString3);
            } else {
              str8 = null;
            } 
            arrayOfString3 = arrayOfString3.createStringArray();
            setDnsForwarders((Network)str8, arrayOfString3);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            arrayOfString3.enforceInterface("android.os.INetworkManagementService");
            arrayOfString3 = listTetheredInterfaces();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString3);
            return true;
          case 24:
            arrayOfString3.enforceInterface("android.os.INetworkManagementService");
            str2 = arrayOfString3.readString();
            untetherInterface(str2);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str2.enforceInterface("android.os.INetworkManagementService");
            str2 = str2.readString();
            tetherInterface(str2);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            str2.enforceInterface("android.os.INetworkManagementService");
            bool1 = isTetheringStarted();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 21:
            str2.enforceInterface("android.os.INetworkManagementService");
            stopTethering();
            param1Parcel2.writeNoException();
            return true;
          case 20:
            str2.enforceInterface("android.os.INetworkManagementService");
            bool19 = bool17;
            if (str2.readInt() != 0)
              bool19 = true; 
            arrayOfString2 = str2.createStringArray();
            startTetheringWithConfiguration(bool19, arrayOfString2);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            arrayOfString2.enforceInterface("android.os.INetworkManagementService");
            arrayOfString2 = arrayOfString2.createStringArray();
            startTethering(arrayOfString2);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            arrayOfString2.enforceInterface("android.os.INetworkManagementService");
            bool19 = bool18;
            if (arrayOfString2.readInt() != 0)
              bool19 = true; 
            setIpForwardingEnabled(bool19);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            arrayOfString2.enforceInterface("android.os.INetworkManagementService");
            bool1 = getIpForwardingEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 16:
            arrayOfString2.enforceInterface("android.os.INetworkManagementService");
            shutdown();
            param1Parcel2.writeNoException();
            return true;
          case 15:
            arrayOfString2.enforceInterface("android.os.INetworkManagementService");
            str8 = arrayOfString2.readString();
            i = arrayOfString2.readInt();
            setMtu(str8, i);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            arrayOfString2.enforceInterface("android.os.INetworkManagementService");
            i = arrayOfString2.readInt();
            if (arrayOfString2.readInt() != 0) {
              RouteInfo routeInfo = RouteInfo.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              arrayOfString2 = null;
            } 
            removeRoute(i, (RouteInfo)arrayOfString2);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            arrayOfString2.enforceInterface("android.os.INetworkManagementService");
            i = arrayOfString2.readInt();
            if (arrayOfString2.readInt() != 0) {
              RouteInfo routeInfo = RouteInfo.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              arrayOfString2 = null;
            } 
            addRoute(i, (RouteInfo)arrayOfString2);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            arrayOfString2.enforceInterface("android.os.INetworkManagementService");
            str8 = arrayOfString2.readString();
            i = arrayOfString2.readInt();
            setIPv6AddrGenMode(str8, i);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            arrayOfString2.enforceInterface("android.os.INetworkManagementService");
            str1 = arrayOfString2.readString();
            enableIpv6(str1);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            str1.enforceInterface("android.os.INetworkManagementService");
            str1 = str1.readString();
            disableIpv6(str1);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str1.enforceInterface("android.os.INetworkManagementService");
            str8 = str1.readString();
            if (str1.readInt() != 0)
              bool19 = true; 
            setInterfaceIpv6PrivacyExtensions(str8, bool19);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str1.enforceInterface("android.os.INetworkManagementService");
            str1 = str1.readString();
            setInterfaceUp(str1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str1.enforceInterface("android.os.INetworkManagementService");
            str1 = str1.readString();
            setInterfaceDown(str1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str1.enforceInterface("android.os.INetworkManagementService");
            str1 = str1.readString();
            clearInterfaceAddresses(str1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("android.os.INetworkManagementService");
            str8 = str1.readString();
            if (str1.readInt() != 0) {
              InterfaceConfiguration interfaceConfiguration1 = InterfaceConfiguration.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            setInterfaceConfig(str8, (InterfaceConfiguration)str1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("android.os.INetworkManagementService");
            str1 = str1.readString();
            interfaceConfiguration = getInterfaceConfig(str1);
            param1Parcel2.writeNoException();
            if (interfaceConfiguration != null) {
              param1Parcel2.writeInt(1);
              interfaceConfiguration.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            interfaceConfiguration.enforceInterface("android.os.INetworkManagementService");
            arrayOfString1 = listInterfaces();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString1);
            return true;
          case 2:
            arrayOfString1.enforceInterface("android.os.INetworkManagementService");
            iNetworkManagementEventObserver = INetworkManagementEventObserver.Stub.asInterface(arrayOfString1.readStrongBinder());
            unregisterObserver(iNetworkManagementEventObserver);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iNetworkManagementEventObserver.enforceInterface("android.os.INetworkManagementService");
        INetworkManagementEventObserver iNetworkManagementEventObserver = INetworkManagementEventObserver.Stub.asInterface(iNetworkManagementEventObserver.readStrongBinder());
        registerObserver(iNetworkManagementEventObserver);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.os.INetworkManagementService");
      return true;
    }
    
    private static class Proxy implements INetworkManagementService {
      public static INetworkManagementService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.INetworkManagementService";
      }
      
      public void registerObserver(INetworkManagementEventObserver param2INetworkManagementEventObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          if (param2INetworkManagementEventObserver != null) {
            iBinder = param2INetworkManagementEventObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().registerObserver(param2INetworkManagementEventObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterObserver(INetworkManagementEventObserver param2INetworkManagementEventObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          if (param2INetworkManagementEventObserver != null) {
            iBinder = param2INetworkManagementEventObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().unregisterObserver(param2INetworkManagementEventObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] listInterfaces() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null)
            return INetworkManagementService.Stub.getDefaultImpl().listInterfaces(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public InterfaceConfiguration getInterfaceConfig(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null)
            return INetworkManagementService.Stub.getDefaultImpl().getInterfaceConfig(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            InterfaceConfiguration interfaceConfiguration = InterfaceConfiguration.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (InterfaceConfiguration)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInterfaceConfig(String param2String, InterfaceConfiguration param2InterfaceConfiguration) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          if (param2InterfaceConfiguration != null) {
            parcel1.writeInt(1);
            param2InterfaceConfiguration.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setInterfaceConfig(param2String, param2InterfaceConfiguration);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearInterfaceAddresses(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().clearInterfaceAddresses(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInterfaceDown(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setInterfaceDown(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInterfaceUp(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setInterfaceUp(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInterfaceIpv6PrivacyExtensions(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool1 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setInterfaceIpv6PrivacyExtensions(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableIpv6(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().disableIpv6(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableIpv6(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().enableIpv6(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setIPv6AddrGenMode(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setIPv6AddrGenMode(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addRoute(int param2Int, RouteInfo param2RouteInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          if (param2RouteInfo != null) {
            parcel1.writeInt(1);
            param2RouteInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().addRoute(param2Int, param2RouteInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeRoute(int param2Int, RouteInfo param2RouteInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          if (param2RouteInfo != null) {
            parcel1.writeInt(1);
            param2RouteInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().removeRoute(param2Int, param2RouteInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMtu(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setMtu(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void shutdown() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().shutdown();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getIpForwardingEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkManagementService.Stub.getDefaultImpl().getIpForwardingEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setIpForwardingEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool1 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setIpForwardingEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startTethering(String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().startTethering(param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startTetheringWithConfiguration(boolean param2Boolean, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool1 = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool1 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().startTetheringWithConfiguration(param2Boolean, param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopTethering() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().stopTethering();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTetheringStarted() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(22, parcel1, parcel2, 0);
          if (!bool2 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkManagementService.Stub.getDefaultImpl().isTetheringStarted();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void tetherInterface(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().tetherInterface(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void untetherInterface(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().untetherInterface(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] listTetheredInterfaces() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null)
            return INetworkManagementService.Stub.getDefaultImpl().listTetheredInterfaces(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDnsForwarders(Network param2Network, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          if (param2Network != null) {
            parcel1.writeInt(1);
            param2Network.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setDnsForwarders(param2Network, param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getDnsForwarders() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null)
            return INetworkManagementService.Stub.getDefaultImpl().getDnsForwarders(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startInterfaceForwarding(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().startInterfaceForwarding(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopInterfaceForwarding(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().stopInterfaceForwarding(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableNat(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().enableNat(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableNat(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().disableNat(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerTetheringStatsProvider(ITetheringStatsProvider param2ITetheringStatsProvider, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          if (param2ITetheringStatsProvider != null) {
            iBinder = param2ITetheringStatsProvider.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().registerTetheringStatsProvider(param2ITetheringStatsProvider, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterTetheringStatsProvider(ITetheringStatsProvider param2ITetheringStatsProvider) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          if (param2ITetheringStatsProvider != null) {
            iBinder = param2ITetheringStatsProvider.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().unregisterTetheringStatsProvider(param2ITetheringStatsProvider);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void tetherLimitReached(ITetheringStatsProvider param2ITetheringStatsProvider) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          if (param2ITetheringStatsProvider != null) {
            iBinder = param2ITetheringStatsProvider.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().tetherLimitReached(param2ITetheringStatsProvider);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NetworkStats getNetworkStatsTethering(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          NetworkStats networkStats;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            networkStats = INetworkManagementService.Stub.getDefaultImpl().getNetworkStatsTethering(param2Int);
            return networkStats;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            networkStats = NetworkStats.CREATOR.createFromParcel(parcel2);
          } else {
            networkStats = null;
          } 
          return networkStats;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInterfaceQuota(String param2String, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setInterfaceQuota(param2String, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeInterfaceQuota(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().removeInterfaceQuota(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInterfaceAlert(String param2String, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setInterfaceAlert(param2String, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeInterfaceAlert(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().removeInterfaceAlert(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setGlobalAlert(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setGlobalAlert(param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUidMeteredNetworkBlacklist(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool1 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setUidMeteredNetworkBlacklist(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUidMeteredNetworkWhitelist(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool1 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setUidMeteredNetworkWhitelist(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDataSaverModeEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool1 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            param2Boolean = INetworkManagementService.Stub.getDefaultImpl().setDataSaverModeEnabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUidCleartextNetworkPolicy(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setUidCleartextNetworkPolicy(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBandwidthControlEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(45, parcel1, parcel2, 0);
          if (!bool2 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkManagementService.Stub.getDefaultImpl().isBandwidthControlEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addIdleTimer(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().addIdleTimer(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeIdleTimer(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().removeIdleTimer(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFirewallEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool1 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setFirewallEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isFirewallEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(49, parcel1, parcel2, 0);
          if (!bool2 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkManagementService.Stub.getDefaultImpl().isFirewallEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFirewallInterfaceRule(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool1 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setFirewallInterfaceRule(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFirewallUidRule(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setFirewallUidRule(param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFirewallUidRules(int param2Int, int[] param2ArrayOfint1, int[] param2ArrayOfint2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          parcel1.writeIntArray(param2ArrayOfint1);
          parcel1.writeIntArray(param2ArrayOfint2);
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setFirewallUidRules(param2Int, param2ArrayOfint1, param2ArrayOfint2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFirewallChainEnabled(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool1 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setFirewallChainEnabled(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addVpnUidRanges(int param2Int, UidRange[] param2ArrayOfUidRange) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          parcel1.writeTypedArray(param2ArrayOfUidRange, 0);
          boolean bool = this.mRemote.transact(54, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().addVpnUidRanges(param2Int, param2ArrayOfUidRange);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeVpnUidRanges(int param2Int, UidRange[] param2ArrayOfUidRange) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          parcel1.writeTypedArray(param2ArrayOfUidRange, 0);
          boolean bool = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().removeVpnUidRanges(param2Int, param2ArrayOfUidRange);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerNetworkActivityListener(INetworkActivityListener param2INetworkActivityListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          if (param2INetworkActivityListener != null) {
            iBinder = param2INetworkActivityListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(56, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().registerNetworkActivityListener(param2INetworkActivityListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterNetworkActivityListener(INetworkActivityListener param2INetworkActivityListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          if (param2INetworkActivityListener != null) {
            iBinder = param2INetworkActivityListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(57, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().unregisterNetworkActivityListener(param2INetworkActivityListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNetworkActive() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(58, parcel1, parcel2, 0);
          if (!bool2 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkManagementService.Stub.getDefaultImpl().isNetworkActive();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addInterfaceToNetwork(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().addInterfaceToNetwork(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeInterfaceFromNetwork(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(60, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().removeInterfaceFromNetwork(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addLegacyRouteForNetId(int param2Int1, RouteInfo param2RouteInfo, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int1);
          if (param2RouteInfo != null) {
            parcel1.writeInt(1);
            param2RouteInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().addLegacyRouteForNetId(param2Int1, param2RouteInfo, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDefaultNetId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setDefaultNetId(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearDefaultNetId() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          boolean bool = this.mRemote.transact(63, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().clearDefaultNetId();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNetworkPermission(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(64, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setNetworkPermission(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void allowProtect(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(65, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().allowProtect(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void denyProtect(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(66, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().denyProtect(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addInterfaceToLocalNetwork(String param2String, List<RouteInfo> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          parcel1.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(67, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().addInterfaceToLocalNetwork(param2String, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeInterfaceFromLocalNetwork(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(68, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().removeInterfaceFromLocalNetwork(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int removeRoutesFromLocalNetwork(List<RouteInfo> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(69, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null)
            return INetworkManagementService.Stub.getDefaultImpl().removeRoutesFromLocalNetwork(param2List); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAllowOnlyVpnForUids(boolean param2Boolean, UidRange[] param2ArrayOfUidRange) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeTypedArray(param2ArrayOfUidRange, 0);
          boolean bool1 = this.mRemote.transact(70, parcel1, parcel2, 0);
          if (!bool1 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setAllowOnlyVpnForUids(param2Boolean, param2ArrayOfUidRange);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNetworkRestricted(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(71, parcel1, parcel2, 0);
          if (!bool2 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkManagementService.Stub.getDefaultImpl().isNetworkRestricted(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFirewallUidRuleForNetworkType(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(72, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setFirewallUidRuleForNetworkType(param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUidTos(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(73, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().setUidTos(param2String1, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearUidTos(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(74, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            INetworkManagementService.Stub.getDefaultImpl().clearUidTos(param2String1, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String oppoNetdCmdParse(String param2String, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(75, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            param2String = INetworkManagementService.Stub.getDefaultImpl().oppoNetdCmdParse(param2String, param2ArrayOfint);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String oplusNetdGetSysProc(int param2Int1, int param2Int2, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(76, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            param2String1 = INetworkManagementService.Stub.getDefaultImpl().oplusNetdGetSysProc(param2Int1, param2Int2, param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String executeShellToSetIptables(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(77, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            param2String = INetworkManagementService.Stub.getDefaultImpl().executeShellToSetIptables(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setNetworkRestriction(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(78, parcel1, parcel2, 0);
          if (!bool2 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkManagementService.Stub.getDefaultImpl().setNetworkRestriction(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addNetworkRestriction(int param2Int, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          parcel1.writeStringArray(param2ArrayOfString);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(79, parcel1, parcel2, 0);
          if (!bool2 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkManagementService.Stub.getDefaultImpl().addNetworkRestriction(param2Int, param2ArrayOfString);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeNetworkRestriction(int param2Int, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          parcel1.writeStringArray(param2ArrayOfString);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(80, parcel1, parcel2, 0);
          if (!bool2 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkManagementService.Stub.getDefaultImpl().removeNetworkRestriction(param2Int, param2ArrayOfString);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeNetworkRestrictionAll(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(81, parcel1, parcel2, 0);
          if (!bool2 && INetworkManagementService.Stub.getDefaultImpl() != null) {
            bool1 = INetworkManagementService.Stub.getDefaultImpl().removeNetworkRestrictionAll(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] socketAllDestroy(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.INetworkManagementService");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(82, parcel1, parcel2, 0);
          if (!bool && INetworkManagementService.Stub.getDefaultImpl() != null) {
            param2ArrayOfint = INetworkManagementService.Stub.getDefaultImpl().socketAllDestroy(param2ArrayOfint);
            return param2ArrayOfint;
          } 
          parcel2.readException();
          param2ArrayOfint = parcel2.createIntArray();
          return param2ArrayOfint;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INetworkManagementService param1INetworkManagementService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INetworkManagementService != null) {
          Proxy.sDefaultImpl = param1INetworkManagementService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INetworkManagementService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
