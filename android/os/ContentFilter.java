package android.os;

import java.util.Collections;
import java.util.Map;

class ContentFilter {
  private final Map<String, Integer> mContentFilter;
  
  private final int mHashCode;
  
  ContentFilter(int paramInt, Map<String, Integer> paramMap) {
    this.mHashCode = paramInt;
    this.mContentFilter = Collections.unmodifiableMap(paramMap);
  }
  
  public int matchContent(String paramString) {
    return ((Integer)this.mContentFilter.getOrDefault(paramString, Integer.valueOf(-1))).intValue();
  }
  
  public int getHashCode() {
    return this.mHashCode;
  }
}
