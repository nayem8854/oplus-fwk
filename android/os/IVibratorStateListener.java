package android.os;

public interface IVibratorStateListener extends IInterface {
  void onVibrating(boolean paramBoolean) throws RemoteException;
  
  class Default implements IVibratorStateListener {
    public void onVibrating(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVibratorStateListener {
    private static final String DESCRIPTOR = "android.os.IVibratorStateListener";
    
    static final int TRANSACTION_onVibrating = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IVibratorStateListener");
    }
    
    public static IVibratorStateListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IVibratorStateListener");
      if (iInterface != null && iInterface instanceof IVibratorStateListener)
        return (IVibratorStateListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onVibrating";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.IVibratorStateListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IVibratorStateListener");
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      onVibrating(bool);
      return true;
    }
    
    private static class Proxy implements IVibratorStateListener {
      public static IVibratorStateListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IVibratorStateListener";
      }
      
      public void onVibrating(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.os.IVibratorStateListener");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && IVibratorStateListener.Stub.getDefaultImpl() != null) {
            IVibratorStateListener.Stub.getDefaultImpl().onVibrating(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVibratorStateListener param1IVibratorStateListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVibratorStateListener != null) {
          Proxy.sDefaultImpl = param1IVibratorStateListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVibratorStateListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
