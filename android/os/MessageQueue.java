package android.os;

import android.util.Printer;
import android.util.SparseArray;
import android.util.proto.ProtoOutputStream;
import java.io.FileDescriptor;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

public final class MessageQueue {
  private boolean mQuitting;
  
  private final boolean mQuitAllowed;
  
  private long mPtr;
  
  private IdleHandler[] mPendingIdleHandlers;
  
  private int mNextBarrierToken;
  
  Message mMessages;
  
  private final ArrayList<IdleHandler> mIdleHandlers = new ArrayList<>();
  
  private SparseArray<FileDescriptorRecord> mFileDescriptorRecords;
  
  private boolean mBlocked;
  
  private static final String TAG = "MessageQueue";
  
  private static final int DUMP_MESSAGE_MAX = 10;
  
  MessageQueue(boolean paramBoolean) {
    this.mQuitAllowed = paramBoolean;
    this.mPtr = nativeInit();
  }
  
  protected void finalize() throws Throwable {
    try {
      dispose();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private void dispose() {
    long l = this.mPtr;
    if (l != 0L) {
      nativeDestroy(l);
      this.mPtr = 0L;
    } 
  }
  
  public boolean isIdle() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: invokestatic uptimeMillis : ()J
    //   5: lstore_1
    //   6: aload_0
    //   7: getfield mMessages : Landroid/os/Message;
    //   10: ifnull -> 33
    //   13: lload_1
    //   14: aload_0
    //   15: getfield mMessages : Landroid/os/Message;
    //   18: getfield when : J
    //   21: lcmp
    //   22: ifge -> 28
    //   25: goto -> 33
    //   28: iconst_0
    //   29: istore_3
    //   30: goto -> 35
    //   33: iconst_1
    //   34: istore_3
    //   35: aload_0
    //   36: monitorexit
    //   37: iload_3
    //   38: ireturn
    //   39: astore #4
    //   41: aload_0
    //   42: monitorexit
    //   43: aload #4
    //   45: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #112	-> 0
    //   #113	-> 2
    //   #114	-> 6
    //   #115	-> 39
    // Exception table:
    //   from	to	target	type
    //   2	6	39	finally
    //   6	25	39	finally
    //   35	37	39	finally
    //   41	43	39	finally
  }
  
  public void addIdleHandler(IdleHandler paramIdleHandler) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 23
    //   4: aload_0
    //   5: monitorenter
    //   6: aload_0
    //   7: getfield mIdleHandlers : Ljava/util/ArrayList;
    //   10: aload_1
    //   11: invokevirtual add : (Ljava/lang/Object;)Z
    //   14: pop
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_1
    //   22: athrow
    //   23: new java/lang/NullPointerException
    //   26: dup
    //   27: ldc 'Can't add a null IdleHandler'
    //   29: invokespecial <init> : (Ljava/lang/String;)V
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #129	-> 0
    //   #132	-> 4
    //   #133	-> 6
    //   #134	-> 15
    //   #135	-> 17
    //   #134	-> 18
    //   #130	-> 23
    // Exception table:
    //   from	to	target	type
    //   6	15	18	finally
    //   15	17	18	finally
    //   19	21	18	finally
  }
  
  public void removeIdleHandler(IdleHandler paramIdleHandler) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIdleHandlers : Ljava/util/ArrayList;
    //   6: aload_1
    //   7: invokevirtual remove : (Ljava/lang/Object;)Z
    //   10: pop
    //   11: aload_0
    //   12: monitorexit
    //   13: return
    //   14: astore_1
    //   15: aload_0
    //   16: monitorexit
    //   17: aload_1
    //   18: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #147	-> 0
    //   #148	-> 2
    //   #149	-> 11
    //   #150	-> 13
    //   #149	-> 14
    // Exception table:
    //   from	to	target	type
    //   2	11	14	finally
    //   11	13	14	finally
    //   15	17	14	finally
  }
  
  public boolean isPolling() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial isPollingLocked : ()Z
    //   6: istore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: iload_1
    //   10: ireturn
    //   11: astore_2
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_2
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #164	-> 0
    //   #165	-> 2
    //   #166	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  private boolean isPollingLocked() {
    boolean bool;
    if (!this.mQuitting && nativeIsPolling(this.mPtr)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void addOnFileDescriptorEventListener(FileDescriptor paramFileDescriptor, int paramInt, OnFileDescriptorEventListener paramOnFileDescriptorEventListener) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 35
    //   4: aload_3
    //   5: ifnull -> 25
    //   8: aload_0
    //   9: monitorenter
    //   10: aload_0
    //   11: aload_1
    //   12: iload_2
    //   13: aload_3
    //   14: invokespecial updateOnFileDescriptorEventListenerLocked : (Ljava/io/FileDescriptor;ILandroid/os/MessageQueue$OnFileDescriptorEventListener;)V
    //   17: aload_0
    //   18: monitorexit
    //   19: return
    //   20: astore_1
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_1
    //   24: athrow
    //   25: new java/lang/IllegalArgumentException
    //   28: dup
    //   29: ldc 'listener must not be null'
    //   31: invokespecial <init> : (Ljava/lang/String;)V
    //   34: athrow
    //   35: new java/lang/IllegalArgumentException
    //   38: dup
    //   39: ldc 'fd must not be null'
    //   41: invokespecial <init> : (Ljava/lang/String;)V
    //   44: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #201	-> 0
    //   #204	-> 4
    //   #208	-> 8
    //   #209	-> 10
    //   #210	-> 17
    //   #211	-> 19
    //   #210	-> 20
    //   #205	-> 25
    //   #202	-> 35
    // Exception table:
    //   from	to	target	type
    //   10	17	20	finally
    //   17	19	20	finally
    //   21	23	20	finally
  }
  
  public void removeOnFileDescriptorEventListener(FileDescriptor paramFileDescriptor) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 21
    //   4: aload_0
    //   5: monitorenter
    //   6: aload_0
    //   7: aload_1
    //   8: iconst_0
    //   9: aconst_null
    //   10: invokespecial updateOnFileDescriptorEventListenerLocked : (Ljava/io/FileDescriptor;ILandroid/os/MessageQueue$OnFileDescriptorEventListener;)V
    //   13: aload_0
    //   14: monitorexit
    //   15: return
    //   16: astore_1
    //   17: aload_0
    //   18: monitorexit
    //   19: aload_1
    //   20: athrow
    //   21: new java/lang/IllegalArgumentException
    //   24: dup
    //   25: ldc 'fd must not be null'
    //   27: invokespecial <init> : (Ljava/lang/String;)V
    //   30: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #226	-> 0
    //   #230	-> 4
    //   #231	-> 6
    //   #232	-> 13
    //   #233	-> 15
    //   #232	-> 16
    //   #227	-> 21
    // Exception table:
    //   from	to	target	type
    //   6	13	16	finally
    //   13	15	16	finally
    //   17	19	16	finally
  }
  
  private void updateOnFileDescriptorEventListenerLocked(FileDescriptor paramFileDescriptor, int paramInt, OnFileDescriptorEventListener paramOnFileDescriptorEventListener) {
    int i = paramFileDescriptor.getInt$();
    int j = -1;
    FileDescriptorRecord fileDescriptorRecord1 = null;
    SparseArray<FileDescriptorRecord> sparseArray = this.mFileDescriptorRecords;
    FileDescriptorRecord fileDescriptorRecord2 = fileDescriptorRecord1;
    if (sparseArray != null) {
      int k = sparseArray.indexOfKey(i);
      j = k;
      fileDescriptorRecord2 = fileDescriptorRecord1;
      if (k >= 0) {
        fileDescriptorRecord1 = (FileDescriptorRecord)this.mFileDescriptorRecords.valueAt(k);
        j = k;
        fileDescriptorRecord2 = fileDescriptorRecord1;
        if (fileDescriptorRecord1 != null) {
          j = k;
          fileDescriptorRecord2 = fileDescriptorRecord1;
          if (fileDescriptorRecord1.mEvents == paramInt)
            return; 
        } 
      } 
    } 
    if (paramInt != 0) {
      paramInt |= 0x4;
      if (fileDescriptorRecord2 == null) {
        if (this.mFileDescriptorRecords == null)
          this.mFileDescriptorRecords = new SparseArray(); 
        FileDescriptorRecord fileDescriptorRecord = new FileDescriptorRecord(paramFileDescriptor, paramInt, paramOnFileDescriptorEventListener);
        this.mFileDescriptorRecords.put(i, fileDescriptorRecord);
      } else {
        fileDescriptorRecord2.mListener = paramOnFileDescriptorEventListener;
        fileDescriptorRecord2.mEvents = paramInt;
        fileDescriptorRecord2.mSeq++;
      } 
      nativeSetFileDescriptorEvents(this.mPtr, i, paramInt);
    } else if (fileDescriptorRecord2 != null) {
      fileDescriptorRecord2.mEvents = 0;
      this.mFileDescriptorRecords.removeAt(j);
      nativeSetFileDescriptorEvents(this.mPtr, i, 0);
    } 
  }
  
  private int dispatchEvents(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mFileDescriptorRecords : Landroid/util/SparseArray;
    //   6: iload_1
    //   7: invokevirtual get : (I)Ljava/lang/Object;
    //   10: checkcast android/os/MessageQueue$FileDescriptorRecord
    //   13: astore_3
    //   14: aload_3
    //   15: ifnonnull -> 22
    //   18: aload_0
    //   19: monitorexit
    //   20: iconst_0
    //   21: ireturn
    //   22: aload_3
    //   23: getfield mEvents : I
    //   26: istore #4
    //   28: iload_2
    //   29: iload #4
    //   31: iand
    //   32: istore_2
    //   33: iload_2
    //   34: ifne -> 42
    //   37: aload_0
    //   38: monitorexit
    //   39: iload #4
    //   41: ireturn
    //   42: aload_3
    //   43: getfield mListener : Landroid/os/MessageQueue$OnFileDescriptorEventListener;
    //   46: astore #5
    //   48: aload_3
    //   49: getfield mSeq : I
    //   52: istore #6
    //   54: aload_0
    //   55: monitorexit
    //   56: aload #5
    //   58: aload_3
    //   59: getfield mDescriptor : Ljava/io/FileDescriptor;
    //   62: iload_2
    //   63: invokeinterface onFileDescriptorEvents : (Ljava/io/FileDescriptor;I)I
    //   68: istore #7
    //   70: iload #7
    //   72: istore_2
    //   73: iload #7
    //   75: ifeq -> 83
    //   78: iload #7
    //   80: iconst_4
    //   81: ior
    //   82: istore_2
    //   83: iload_2
    //   84: iload #4
    //   86: if_icmpeq -> 154
    //   89: aload_0
    //   90: monitorenter
    //   91: aload_0
    //   92: getfield mFileDescriptorRecords : Landroid/util/SparseArray;
    //   95: iload_1
    //   96: invokevirtual indexOfKey : (I)I
    //   99: istore_1
    //   100: iload_1
    //   101: iflt -> 142
    //   104: aload_0
    //   105: getfield mFileDescriptorRecords : Landroid/util/SparseArray;
    //   108: iload_1
    //   109: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   112: aload_3
    //   113: if_acmpne -> 142
    //   116: aload_3
    //   117: getfield mSeq : I
    //   120: iload #6
    //   122: if_icmpne -> 142
    //   125: aload_3
    //   126: iload_2
    //   127: putfield mEvents : I
    //   130: iload_2
    //   131: ifne -> 142
    //   134: aload_0
    //   135: getfield mFileDescriptorRecords : Landroid/util/SparseArray;
    //   138: iload_1
    //   139: invokevirtual removeAt : (I)V
    //   142: aload_0
    //   143: monitorexit
    //   144: goto -> 154
    //   147: astore #5
    //   149: aload_0
    //   150: monitorexit
    //   151: aload #5
    //   153: athrow
    //   154: iload_2
    //   155: ireturn
    //   156: astore #5
    //   158: aload_0
    //   159: monitorexit
    //   160: aload #5
    //   162: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #280	-> 0
    //   #281	-> 2
    //   #282	-> 14
    //   #283	-> 18
    //   #286	-> 22
    //   #287	-> 28
    //   #288	-> 33
    //   #289	-> 37
    //   #292	-> 42
    //   #293	-> 48
    //   #294	-> 54
    //   #297	-> 56
    //   #299	-> 70
    //   #300	-> 78
    //   #305	-> 83
    //   #306	-> 89
    //   #307	-> 91
    //   #308	-> 100
    //   #310	-> 125
    //   #311	-> 130
    //   #312	-> 134
    //   #315	-> 142
    //   #319	-> 154
    //   #294	-> 156
    // Exception table:
    //   from	to	target	type
    //   2	14	156	finally
    //   18	20	156	finally
    //   22	28	156	finally
    //   37	39	156	finally
    //   42	48	156	finally
    //   48	54	156	finally
    //   54	56	156	finally
    //   91	100	147	finally
    //   104	125	147	finally
    //   125	130	147	finally
    //   134	142	147	finally
    //   142	144	147	finally
    //   149	151	147	finally
    //   158	160	156	finally
  }
  
  Message next() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mPtr : J
    //   4: lstore_1
    //   5: lload_1
    //   6: lconst_0
    //   7: lcmp
    //   8: ifne -> 13
    //   11: aconst_null
    //   12: areturn
    //   13: iconst_m1
    //   14: istore_3
    //   15: iconst_0
    //   16: istore #4
    //   18: iload #4
    //   20: ifeq -> 26
    //   23: invokestatic flushPendingCommands : ()V
    //   26: aload_0
    //   27: lload_1
    //   28: iload #4
    //   30: invokespecial nativePollOnce : (JI)V
    //   33: aload_0
    //   34: monitorenter
    //   35: invokestatic uptimeMillis : ()J
    //   38: lstore #5
    //   40: aconst_null
    //   41: astore #7
    //   43: aload_0
    //   44: getfield mMessages : Landroid/os/Message;
    //   47: astore #8
    //   49: aload #7
    //   51: astore #9
    //   53: aload #8
    //   55: astore #10
    //   57: aload #8
    //   59: ifnull -> 126
    //   62: aload #7
    //   64: astore #9
    //   66: aload #8
    //   68: astore #10
    //   70: aload #8
    //   72: getfield target : Landroid/os/Handler;
    //   75: ifnonnull -> 126
    //   78: aload #8
    //   80: astore #10
    //   82: aload #10
    //   84: astore #7
    //   86: aload #10
    //   88: getfield next : Landroid/os/Message;
    //   91: astore #8
    //   93: aload #7
    //   95: astore #9
    //   97: aload #8
    //   99: astore #10
    //   101: aload #8
    //   103: ifnull -> 126
    //   106: aload #8
    //   108: astore #10
    //   110: aload #8
    //   112: invokevirtual isAsynchronous : ()Z
    //   115: ifeq -> 82
    //   118: aload #8
    //   120: astore #10
    //   122: aload #7
    //   124: astore #9
    //   126: aload #10
    //   128: ifnull -> 210
    //   131: lload #5
    //   133: aload #10
    //   135: getfield when : J
    //   138: lcmp
    //   139: ifge -> 162
    //   142: aload #10
    //   144: getfield when : J
    //   147: lload #5
    //   149: lsub
    //   150: ldc2_w 2147483647
    //   153: invokestatic min : (JJ)J
    //   156: l2i
    //   157: istore #4
    //   159: goto -> 213
    //   162: aload_0
    //   163: iconst_0
    //   164: putfield mBlocked : Z
    //   167: aload #9
    //   169: ifnull -> 185
    //   172: aload #9
    //   174: aload #10
    //   176: getfield next : Landroid/os/Message;
    //   179: putfield next : Landroid/os/Message;
    //   182: goto -> 194
    //   185: aload_0
    //   186: aload #10
    //   188: getfield next : Landroid/os/Message;
    //   191: putfield mMessages : Landroid/os/Message;
    //   194: aload #10
    //   196: aconst_null
    //   197: putfield next : Landroid/os/Message;
    //   200: aload #10
    //   202: invokevirtual markInUse : ()V
    //   205: aload_0
    //   206: monitorexit
    //   207: aload #10
    //   209: areturn
    //   210: iconst_m1
    //   211: istore #4
    //   213: aload_0
    //   214: getfield mQuitting : Z
    //   217: ifeq -> 228
    //   220: aload_0
    //   221: invokespecial dispose : ()V
    //   224: aload_0
    //   225: monitorexit
    //   226: aconst_null
    //   227: areturn
    //   228: iload_3
    //   229: istore #11
    //   231: iload_3
    //   232: ifge -> 267
    //   235: aload_0
    //   236: getfield mMessages : Landroid/os/Message;
    //   239: ifnull -> 258
    //   242: iload_3
    //   243: istore #11
    //   245: lload #5
    //   247: aload_0
    //   248: getfield mMessages : Landroid/os/Message;
    //   251: getfield when : J
    //   254: lcmp
    //   255: ifge -> 267
    //   258: aload_0
    //   259: getfield mIdleHandlers : Ljava/util/ArrayList;
    //   262: invokevirtual size : ()I
    //   265: istore #11
    //   267: iload #11
    //   269: ifgt -> 285
    //   272: aload_0
    //   273: iconst_1
    //   274: putfield mBlocked : Z
    //   277: aload_0
    //   278: monitorexit
    //   279: iload #11
    //   281: istore_3
    //   282: goto -> 18
    //   285: aload_0
    //   286: getfield mPendingIdleHandlers : [Landroid/os/MessageQueue$IdleHandler;
    //   289: ifnonnull -> 305
    //   292: aload_0
    //   293: iload #11
    //   295: iconst_4
    //   296: invokestatic max : (II)I
    //   299: anewarray android/os/MessageQueue$IdleHandler
    //   302: putfield mPendingIdleHandlers : [Landroid/os/MessageQueue$IdleHandler;
    //   305: aload_0
    //   306: aload_0
    //   307: getfield mIdleHandlers : Ljava/util/ArrayList;
    //   310: aload_0
    //   311: getfield mPendingIdleHandlers : [Landroid/os/MessageQueue$IdleHandler;
    //   314: invokevirtual toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   317: checkcast [Landroid/os/MessageQueue$IdleHandler;
    //   320: putfield mPendingIdleHandlers : [Landroid/os/MessageQueue$IdleHandler;
    //   323: aload_0
    //   324: monitorexit
    //   325: iconst_0
    //   326: istore_3
    //   327: iload_3
    //   328: iload #11
    //   330: if_icmpge -> 417
    //   333: aload_0
    //   334: getfield mPendingIdleHandlers : [Landroid/os/MessageQueue$IdleHandler;
    //   337: astore #9
    //   339: aload #9
    //   341: iload_3
    //   342: aaload
    //   343: astore #10
    //   345: aload #9
    //   347: iload_3
    //   348: aconst_null
    //   349: aastore
    //   350: iconst_0
    //   351: istore #12
    //   353: aload #10
    //   355: invokeinterface queueIdle : ()Z
    //   360: istore #13
    //   362: iload #13
    //   364: istore #12
    //   366: goto -> 382
    //   369: astore #9
    //   371: ldc 'MessageQueue'
    //   373: ldc_w 'IdleHandler threw exception'
    //   376: aload #9
    //   378: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   381: pop
    //   382: iload #12
    //   384: ifne -> 411
    //   387: aload_0
    //   388: monitorenter
    //   389: aload_0
    //   390: getfield mIdleHandlers : Ljava/util/ArrayList;
    //   393: aload #10
    //   395: invokevirtual remove : (Ljava/lang/Object;)Z
    //   398: pop
    //   399: aload_0
    //   400: monitorexit
    //   401: goto -> 411
    //   404: astore #10
    //   406: aload_0
    //   407: monitorexit
    //   408: aload #10
    //   410: athrow
    //   411: iinc #3, 1
    //   414: goto -> 327
    //   417: iconst_0
    //   418: istore_3
    //   419: iconst_0
    //   420: istore #4
    //   422: goto -> 18
    //   425: astore #10
    //   427: aload_0
    //   428: monitorexit
    //   429: aload #10
    //   431: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #327	-> 0
    //   #328	-> 5
    //   #329	-> 11
    //   #332	-> 13
    //   #333	-> 15
    //   #335	-> 18
    //   #336	-> 23
    //   #339	-> 26
    //   #341	-> 33
    //   #343	-> 35
    //   #344	-> 40
    //   #345	-> 43
    //   #346	-> 49
    //   #349	-> 82
    //   #350	-> 86
    //   #351	-> 93
    //   #353	-> 126
    //   #354	-> 131
    //   #356	-> 142
    //   #359	-> 162
    //   #360	-> 167
    //   #361	-> 172
    //   #363	-> 185
    //   #365	-> 194
    //   #367	-> 200
    //   #368	-> 205
    //   #372	-> 210
    //   #376	-> 213
    //   #377	-> 220
    //   #378	-> 224
    //   #384	-> 228
    //   #386	-> 258
    //   #388	-> 267
    //   #390	-> 272
    //   #391	-> 277
    //   #394	-> 285
    //   #395	-> 292
    //   #397	-> 305
    //   #398	-> 323
    //   #402	-> 325
    //   #403	-> 333
    //   #404	-> 345
    //   #406	-> 350
    //   #408	-> 353
    //   #411	-> 366
    //   #409	-> 369
    //   #410	-> 371
    //   #413	-> 382
    //   #414	-> 387
    //   #415	-> 389
    //   #416	-> 399
    //   #402	-> 411
    //   #421	-> 417
    //   #425	-> 419
    //   #398	-> 425
    // Exception table:
    //   from	to	target	type
    //   35	40	425	finally
    //   43	49	425	finally
    //   70	78	425	finally
    //   86	93	425	finally
    //   110	118	425	finally
    //   131	142	425	finally
    //   142	159	425	finally
    //   162	167	425	finally
    //   172	182	425	finally
    //   185	194	425	finally
    //   194	200	425	finally
    //   200	205	425	finally
    //   205	207	425	finally
    //   213	220	425	finally
    //   220	224	425	finally
    //   224	226	425	finally
    //   235	242	425	finally
    //   245	258	425	finally
    //   258	267	425	finally
    //   272	277	425	finally
    //   277	279	425	finally
    //   285	292	425	finally
    //   292	305	425	finally
    //   305	323	425	finally
    //   323	325	425	finally
    //   353	362	369	finally
    //   389	399	404	finally
    //   399	401	404	finally
    //   406	408	404	finally
    //   427	429	425	finally
  }
  
  void quit(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mQuitAllowed : Z
    //   4: ifeq -> 54
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mQuitting : Z
    //   13: ifeq -> 19
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: aload_0
    //   20: iconst_1
    //   21: putfield mQuitting : Z
    //   24: iload_1
    //   25: ifeq -> 35
    //   28: aload_0
    //   29: invokespecial removeAllFutureMessagesLocked : ()V
    //   32: goto -> 39
    //   35: aload_0
    //   36: invokespecial removeAllMessagesLocked : ()V
    //   39: aload_0
    //   40: getfield mPtr : J
    //   43: invokestatic nativeWake : (J)V
    //   46: aload_0
    //   47: monitorexit
    //   48: return
    //   49: astore_2
    //   50: aload_0
    //   51: monitorexit
    //   52: aload_2
    //   53: athrow
    //   54: new java/lang/IllegalStateException
    //   57: dup
    //   58: ldc_w 'Main thread not allowed to quit.'
    //   61: invokespecial <init> : (Ljava/lang/String;)V
    //   64: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #430	-> 0
    //   #434	-> 7
    //   #435	-> 9
    //   #436	-> 16
    //   #438	-> 19
    //   #440	-> 24
    //   #441	-> 28
    //   #443	-> 35
    //   #447	-> 39
    //   #448	-> 46
    //   #449	-> 48
    //   #448	-> 49
    //   #431	-> 54
    // Exception table:
    //   from	to	target	type
    //   9	16	49	finally
    //   16	18	49	finally
    //   19	24	49	finally
    //   28	32	49	finally
    //   35	39	49	finally
    //   39	46	49	finally
    //   46	48	49	finally
    //   50	52	49	finally
  }
  
  public int postSyncBarrier() {
    return postSyncBarrier(SystemClock.uptimeMillis());
  }
  
  private int postSyncBarrier(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mNextBarrierToken : I
    //   6: istore_3
    //   7: aload_0
    //   8: iload_3
    //   9: iconst_1
    //   10: iadd
    //   11: putfield mNextBarrierToken : I
    //   14: invokestatic obtain : ()Landroid/os/Message;
    //   17: astore #4
    //   19: aload #4
    //   21: invokevirtual markInUse : ()V
    //   24: aload #4
    //   26: lload_1
    //   27: putfield when : J
    //   30: aload #4
    //   32: iload_3
    //   33: putfield arg1 : I
    //   36: aconst_null
    //   37: astore #5
    //   39: aconst_null
    //   40: astore #6
    //   42: aload_0
    //   43: getfield mMessages : Landroid/os/Message;
    //   46: astore #7
    //   48: aload #7
    //   50: astore #8
    //   52: lload_1
    //   53: lconst_0
    //   54: lcmp
    //   55: ifeq -> 179
    //   58: iconst_0
    //   59: istore #9
    //   61: aload #6
    //   63: astore #5
    //   65: aload #7
    //   67: astore #8
    //   69: aload #7
    //   71: ifnull -> 179
    //   74: aload #6
    //   76: astore #5
    //   78: aload #7
    //   80: astore #8
    //   82: aload #7
    //   84: getfield when : J
    //   87: lload_1
    //   88: lcmp
    //   89: ifgt -> 179
    //   92: iinc #9, 1
    //   95: iload #9
    //   97: sipush #10000
    //   100: if_icmplt -> 165
    //   103: iload #9
    //   105: sipush #10000
    //   108: irem
    //   109: ifne -> 165
    //   112: new java/lang/StringBuilder
    //   115: astore #6
    //   117: aload #6
    //   119: invokespecial <init> : ()V
    //   122: aload #6
    //   124: ldc 'searched '
    //   126: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   129: pop
    //   130: aload #6
    //   132: iload #9
    //   134: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   137: pop
    //   138: aload #6
    //   140: ldc ' m='
    //   142: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   145: pop
    //   146: aload #6
    //   148: aload #7
    //   150: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   153: pop
    //   154: ldc 'MessageQueue'
    //   156: aload #6
    //   158: invokevirtual toString : ()Ljava/lang/String;
    //   161: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   164: pop
    //   165: aload #7
    //   167: astore #6
    //   169: aload #7
    //   171: getfield next : Landroid/os/Message;
    //   174: astore #7
    //   176: goto -> 61
    //   179: aload #5
    //   181: ifnull -> 201
    //   184: aload #4
    //   186: aload #8
    //   188: putfield next : Landroid/os/Message;
    //   191: aload #5
    //   193: aload #4
    //   195: putfield next : Landroid/os/Message;
    //   198: goto -> 214
    //   201: aload #4
    //   203: aload #8
    //   205: putfield next : Landroid/os/Message;
    //   208: aload_0
    //   209: aload #4
    //   211: putfield mMessages : Landroid/os/Message;
    //   214: aload_0
    //   215: monitorexit
    //   216: iload_3
    //   217: ireturn
    //   218: astore #7
    //   220: aload_0
    //   221: monitorexit
    //   222: aload #7
    //   224: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #483	-> 0
    //   #484	-> 2
    //   #485	-> 14
    //   #486	-> 19
    //   #487	-> 24
    //   #488	-> 30
    //   #490	-> 36
    //   #491	-> 42
    //   #492	-> 48
    //   #495	-> 58
    //   #497	-> 61
    //   #500	-> 92
    //   #501	-> 95
    //   #502	-> 112
    //   #504	-> 165
    //   #505	-> 169
    //   #508	-> 179
    //   #509	-> 184
    //   #510	-> 191
    //   #512	-> 201
    //   #513	-> 208
    //   #515	-> 214
    //   #516	-> 218
    // Exception table:
    //   from	to	target	type
    //   2	14	218	finally
    //   14	19	218	finally
    //   19	24	218	finally
    //   24	30	218	finally
    //   30	36	218	finally
    //   42	48	218	finally
    //   82	92	218	finally
    //   112	165	218	finally
    //   169	176	218	finally
    //   184	191	218	finally
    //   191	198	218	finally
    //   201	208	218	finally
    //   208	214	218	finally
    //   214	216	218	finally
    //   220	222	218	finally
  }
  
  public void removeSyncBarrier(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aconst_null
    //   3: astore_2
    //   4: aload_0
    //   5: getfield mMessages : Landroid/os/Message;
    //   8: astore_3
    //   9: aload_3
    //   10: ifnull -> 38
    //   13: aload_3
    //   14: getfield target : Landroid/os/Handler;
    //   17: ifnonnull -> 28
    //   20: aload_3
    //   21: getfield arg1 : I
    //   24: iload_1
    //   25: if_icmpeq -> 38
    //   28: aload_3
    //   29: astore_2
    //   30: aload_3
    //   31: getfield next : Landroid/os/Message;
    //   34: astore_3
    //   35: goto -> 9
    //   38: aload_3
    //   39: ifnull -> 115
    //   42: aload_2
    //   43: ifnull -> 59
    //   46: aload_2
    //   47: aload_3
    //   48: getfield next : Landroid/os/Message;
    //   51: putfield next : Landroid/os/Message;
    //   54: iconst_0
    //   55: istore_1
    //   56: goto -> 90
    //   59: aload_3
    //   60: getfield next : Landroid/os/Message;
    //   63: astore_2
    //   64: aload_0
    //   65: aload_2
    //   66: putfield mMessages : Landroid/os/Message;
    //   69: aload_2
    //   70: ifnull -> 88
    //   73: aload_2
    //   74: getfield target : Landroid/os/Handler;
    //   77: ifnull -> 83
    //   80: goto -> 88
    //   83: iconst_0
    //   84: istore_1
    //   85: goto -> 90
    //   88: iconst_1
    //   89: istore_1
    //   90: aload_3
    //   91: invokevirtual recycleUnchecked : ()V
    //   94: iload_1
    //   95: ifeq -> 112
    //   98: aload_0
    //   99: getfield mQuitting : Z
    //   102: ifne -> 112
    //   105: aload_0
    //   106: getfield mPtr : J
    //   109: invokestatic nativeWake : (J)V
    //   112: aload_0
    //   113: monitorexit
    //   114: return
    //   115: new java/lang/IllegalStateException
    //   118: astore_3
    //   119: aload_3
    //   120: ldc_w 'The specified message queue synchronization  barrier token has not been posted or has already been removed.'
    //   123: invokespecial <init> : (Ljava/lang/String;)V
    //   126: aload_3
    //   127: athrow
    //   128: astore_3
    //   129: aload_0
    //   130: monitorexit
    //   131: aload_3
    //   132: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #534	-> 0
    //   #535	-> 2
    //   #536	-> 4
    //   #537	-> 9
    //   #538	-> 28
    //   #539	-> 30
    //   #541	-> 38
    //   #546	-> 42
    //   #547	-> 46
    //   #548	-> 54
    //   #550	-> 59
    //   #551	-> 69
    //   #553	-> 90
    //   #557	-> 94
    //   #558	-> 105
    //   #560	-> 112
    //   #561	-> 114
    //   #542	-> 115
    //   #560	-> 128
    // Exception table:
    //   from	to	target	type
    //   4	9	128	finally
    //   13	28	128	finally
    //   30	35	128	finally
    //   46	54	128	finally
    //   59	69	128	finally
    //   73	80	128	finally
    //   90	94	128	finally
    //   98	105	128	finally
    //   105	112	128	finally
    //   112	114	128	finally
    //   115	128	128	finally
    //   129	131	128	finally
  }
  
  boolean enqueueMessage(Message paramMessage, long paramLong) {
    // Byte code:
    //   0: aload_1
    //   1: getfield target : Landroid/os/Handler;
    //   4: ifnull -> 454
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_1
    //   10: invokevirtual isInUse : ()Z
    //   13: ifne -> 405
    //   16: aload_0
    //   17: getfield mQuitting : Z
    //   20: istore #4
    //   22: iconst_0
    //   23: istore #5
    //   25: iload #4
    //   27: ifeq -> 95
    //   30: new java/lang/IllegalStateException
    //   33: astore #6
    //   35: new java/lang/StringBuilder
    //   38: astore #7
    //   40: aload #7
    //   42: invokespecial <init> : ()V
    //   45: aload #7
    //   47: aload_1
    //   48: getfield target : Landroid/os/Handler;
    //   51: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   54: pop
    //   55: aload #7
    //   57: ldc_w ' sending message to a Handler on a dead thread'
    //   60: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   63: pop
    //   64: aload #6
    //   66: aload #7
    //   68: invokevirtual toString : ()Ljava/lang/String;
    //   71: invokespecial <init> : (Ljava/lang/String;)V
    //   74: ldc 'MessageQueue'
    //   76: aload #6
    //   78: invokevirtual getMessage : ()Ljava/lang/String;
    //   81: aload #6
    //   83: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   86: pop
    //   87: aload_1
    //   88: invokevirtual recycle : ()V
    //   91: aload_0
    //   92: monitorexit
    //   93: iconst_0
    //   94: ireturn
    //   95: aload_1
    //   96: invokevirtual markInUse : ()V
    //   99: aload_1
    //   100: lload_2
    //   101: putfield when : J
    //   104: aload_0
    //   105: getfield mMessages : Landroid/os/Message;
    //   108: astore #7
    //   110: aload #7
    //   112: ifnull -> 372
    //   115: lload_2
    //   116: lconst_0
    //   117: lcmp
    //   118: ifeq -> 372
    //   121: lload_2
    //   122: aload #7
    //   124: getfield when : J
    //   127: lcmp
    //   128: ifge -> 134
    //   131: goto -> 372
    //   134: iload #5
    //   136: istore #4
    //   138: aload_0
    //   139: getfield mBlocked : Z
    //   142: ifeq -> 171
    //   145: iload #5
    //   147: istore #4
    //   149: aload #7
    //   151: getfield target : Landroid/os/Handler;
    //   154: ifnonnull -> 171
    //   157: iload #5
    //   159: istore #4
    //   161: aload_1
    //   162: invokevirtual isAsynchronous : ()Z
    //   165: ifeq -> 171
    //   168: iconst_1
    //   169: istore #4
    //   171: iconst_0
    //   172: istore #8
    //   174: aconst_null
    //   175: astore #6
    //   177: aload #7
    //   179: getfield next : Landroid/os/Message;
    //   182: astore #9
    //   184: aload #9
    //   186: ifnull -> 357
    //   189: lload_2
    //   190: aload #9
    //   192: getfield when : J
    //   195: lcmp
    //   196: ifge -> 202
    //   199: goto -> 357
    //   202: iload #4
    //   204: istore #5
    //   206: iload #4
    //   208: ifeq -> 226
    //   211: iload #4
    //   213: istore #5
    //   215: aload #9
    //   217: invokevirtual isAsynchronous : ()Z
    //   220: ifeq -> 226
    //   223: iconst_0
    //   224: istore #5
    //   226: iload #8
    //   228: iconst_1
    //   229: iadd
    //   230: istore #10
    //   232: iload #10
    //   234: sipush #10000
    //   237: if_icmplt -> 320
    //   240: iload #10
    //   242: sipush #10000
    //   245: irem
    //   246: ifne -> 320
    //   249: new java/lang/StringBuilder
    //   252: astore #7
    //   254: aload #7
    //   256: invokespecial <init> : ()V
    //   259: aload #7
    //   261: ldc 'searched '
    //   263: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   266: pop
    //   267: aload #7
    //   269: iload #10
    //   271: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   274: pop
    //   275: aload #7
    //   277: ldc_w 'messages,recent msg is '
    //   280: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   283: pop
    //   284: aload #7
    //   286: aload #9
    //   288: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   291: pop
    //   292: aload #7
    //   294: ldc_w 'head message is '
    //   297: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   300: pop
    //   301: aload #7
    //   303: aload #6
    //   305: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   308: pop
    //   309: ldc 'MessageQueue'
    //   311: aload #7
    //   313: invokevirtual toString : ()Ljava/lang/String;
    //   316: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   319: pop
    //   320: aload #9
    //   322: astore #7
    //   324: iload #5
    //   326: istore #4
    //   328: iload #10
    //   330: istore #8
    //   332: iload #10
    //   334: iconst_1
    //   335: if_icmpne -> 177
    //   338: aload #9
    //   340: astore #6
    //   342: aload #9
    //   344: astore #7
    //   346: iload #5
    //   348: istore #4
    //   350: iload #10
    //   352: istore #8
    //   354: goto -> 177
    //   357: aload_1
    //   358: aload #9
    //   360: putfield next : Landroid/os/Message;
    //   363: aload #7
    //   365: aload_1
    //   366: putfield next : Landroid/os/Message;
    //   369: goto -> 389
    //   372: aload_1
    //   373: aload #7
    //   375: putfield next : Landroid/os/Message;
    //   378: aload_0
    //   379: aload_1
    //   380: putfield mMessages : Landroid/os/Message;
    //   383: aload_0
    //   384: getfield mBlocked : Z
    //   387: istore #4
    //   389: iload #4
    //   391: ifeq -> 401
    //   394: aload_0
    //   395: getfield mPtr : J
    //   398: invokestatic nativeWake : (J)V
    //   401: aload_0
    //   402: monitorexit
    //   403: iconst_1
    //   404: ireturn
    //   405: new java/lang/IllegalStateException
    //   408: astore #7
    //   410: new java/lang/StringBuilder
    //   413: astore #6
    //   415: aload #6
    //   417: invokespecial <init> : ()V
    //   420: aload #6
    //   422: aload_1
    //   423: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   426: pop
    //   427: aload #6
    //   429: ldc_w ' This message is already in use.'
    //   432: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   435: pop
    //   436: aload #7
    //   438: aload #6
    //   440: invokevirtual toString : ()Ljava/lang/String;
    //   443: invokespecial <init> : (Ljava/lang/String;)V
    //   446: aload #7
    //   448: athrow
    //   449: astore_1
    //   450: aload_0
    //   451: monitorexit
    //   452: aload_1
    //   453: athrow
    //   454: new java/lang/IllegalArgumentException
    //   457: dup
    //   458: ldc_w 'Message must have a target.'
    //   461: invokespecial <init> : (Ljava/lang/String;)V
    //   464: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #564	-> 0
    //   #568	-> 7
    //   #569	-> 9
    //   #573	-> 16
    //   #574	-> 30
    //   #576	-> 74
    //   #577	-> 87
    //   #578	-> 91
    //   #581	-> 95
    //   #582	-> 99
    //   #583	-> 104
    //   #585	-> 110
    //   #594	-> 134
    //   #598	-> 171
    //   #599	-> 174
    //   #602	-> 177
    //   #603	-> 177
    //   #604	-> 184
    //   #605	-> 199
    //   #607	-> 202
    //   #608	-> 223
    //   #612	-> 226
    //   #613	-> 232
    //   #615	-> 249
    //   #617	-> 320
    //   #619	-> 338
    //   #623	-> 357
    //   #624	-> 363
    //   #587	-> 372
    //   #588	-> 378
    //   #589	-> 383
    //   #628	-> 389
    //   #629	-> 394
    //   #631	-> 401
    //   #632	-> 403
    //   #570	-> 405
    //   #631	-> 449
    //   #565	-> 454
    // Exception table:
    //   from	to	target	type
    //   9	16	449	finally
    //   16	22	449	finally
    //   30	74	449	finally
    //   74	87	449	finally
    //   87	91	449	finally
    //   91	93	449	finally
    //   95	99	449	finally
    //   99	104	449	finally
    //   104	110	449	finally
    //   121	131	449	finally
    //   138	145	449	finally
    //   149	157	449	finally
    //   161	168	449	finally
    //   177	184	449	finally
    //   189	199	449	finally
    //   215	223	449	finally
    //   249	320	449	finally
    //   357	363	449	finally
    //   363	369	449	finally
    //   372	378	449	finally
    //   378	383	449	finally
    //   383	389	449	finally
    //   394	401	449	finally
    //   401	403	449	finally
    //   405	449	449	finally
    //   450	452	449	finally
  }
  
  boolean hasMessages(Handler paramHandler, int paramInt, Object paramObject) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 6
    //   4: iconst_0
    //   5: ireturn
    //   6: aload_0
    //   7: monitorenter
    //   8: aload_0
    //   9: getfield mMessages : Landroid/os/Message;
    //   12: astore #4
    //   14: aload #4
    //   16: ifnull -> 64
    //   19: aload #4
    //   21: getfield target : Landroid/os/Handler;
    //   24: aload_1
    //   25: if_acmpne -> 54
    //   28: aload #4
    //   30: getfield what : I
    //   33: iload_2
    //   34: if_icmpne -> 54
    //   37: aload_3
    //   38: ifnull -> 50
    //   41: aload #4
    //   43: getfield obj : Ljava/lang/Object;
    //   46: aload_3
    //   47: if_acmpne -> 54
    //   50: aload_0
    //   51: monitorexit
    //   52: iconst_1
    //   53: ireturn
    //   54: aload #4
    //   56: getfield next : Landroid/os/Message;
    //   59: astore #4
    //   61: goto -> 14
    //   64: aload_0
    //   65: monitorexit
    //   66: iconst_0
    //   67: ireturn
    //   68: astore_1
    //   69: aload_0
    //   70: monitorexit
    //   71: aload_1
    //   72: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #636	-> 0
    //   #637	-> 4
    //   #640	-> 6
    //   #641	-> 8
    //   #642	-> 14
    //   #643	-> 19
    //   #644	-> 50
    //   #646	-> 54
    //   #648	-> 64
    //   #649	-> 68
    // Exception table:
    //   from	to	target	type
    //   8	14	68	finally
    //   19	37	68	finally
    //   41	50	68	finally
    //   50	52	68	finally
    //   54	61	68	finally
    //   64	66	68	finally
    //   69	71	68	finally
  }
  
  boolean hasEqualMessages(Handler paramHandler, int paramInt, Object paramObject) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 6
    //   4: iconst_0
    //   5: ireturn
    //   6: aload_0
    //   7: monitorenter
    //   8: aload_0
    //   9: getfield mMessages : Landroid/os/Message;
    //   12: astore #4
    //   14: aload #4
    //   16: ifnull -> 67
    //   19: aload #4
    //   21: getfield target : Landroid/os/Handler;
    //   24: aload_1
    //   25: if_acmpne -> 57
    //   28: aload #4
    //   30: getfield what : I
    //   33: iload_2
    //   34: if_icmpne -> 57
    //   37: aload_3
    //   38: ifnull -> 53
    //   41: aload_3
    //   42: aload #4
    //   44: getfield obj : Ljava/lang/Object;
    //   47: invokevirtual equals : (Ljava/lang/Object;)Z
    //   50: ifeq -> 57
    //   53: aload_0
    //   54: monitorexit
    //   55: iconst_1
    //   56: ireturn
    //   57: aload #4
    //   59: getfield next : Landroid/os/Message;
    //   62: astore #4
    //   64: goto -> 14
    //   67: aload_0
    //   68: monitorexit
    //   69: iconst_0
    //   70: ireturn
    //   71: astore_1
    //   72: aload_0
    //   73: monitorexit
    //   74: aload_1
    //   75: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #653	-> 0
    //   #654	-> 4
    //   #657	-> 6
    //   #658	-> 8
    //   #659	-> 14
    //   #660	-> 19
    //   #661	-> 53
    //   #663	-> 57
    //   #665	-> 67
    //   #666	-> 71
    // Exception table:
    //   from	to	target	type
    //   8	14	71	finally
    //   19	37	71	finally
    //   41	53	71	finally
    //   53	55	71	finally
    //   57	64	71	finally
    //   67	69	71	finally
    //   72	74	71	finally
  }
  
  boolean hasMessages(Handler paramHandler, Runnable paramRunnable, Object paramObject) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 6
    //   4: iconst_0
    //   5: ireturn
    //   6: aload_0
    //   7: monitorenter
    //   8: aload_0
    //   9: getfield mMessages : Landroid/os/Message;
    //   12: astore #4
    //   14: aload #4
    //   16: ifnull -> 64
    //   19: aload #4
    //   21: getfield target : Landroid/os/Handler;
    //   24: aload_1
    //   25: if_acmpne -> 54
    //   28: aload #4
    //   30: getfield callback : Ljava/lang/Runnable;
    //   33: aload_2
    //   34: if_acmpne -> 54
    //   37: aload_3
    //   38: ifnull -> 50
    //   41: aload #4
    //   43: getfield obj : Ljava/lang/Object;
    //   46: aload_3
    //   47: if_acmpne -> 54
    //   50: aload_0
    //   51: monitorexit
    //   52: iconst_1
    //   53: ireturn
    //   54: aload #4
    //   56: getfield next : Landroid/os/Message;
    //   59: astore #4
    //   61: goto -> 14
    //   64: aload_0
    //   65: monitorexit
    //   66: iconst_0
    //   67: ireturn
    //   68: astore_1
    //   69: aload_0
    //   70: monitorexit
    //   71: aload_1
    //   72: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #671	-> 0
    //   #672	-> 4
    //   #675	-> 6
    //   #676	-> 8
    //   #677	-> 14
    //   #678	-> 19
    //   #679	-> 50
    //   #681	-> 54
    //   #683	-> 64
    //   #684	-> 68
    // Exception table:
    //   from	to	target	type
    //   8	14	68	finally
    //   19	37	68	finally
    //   41	50	68	finally
    //   50	52	68	finally
    //   54	61	68	finally
    //   64	66	68	finally
    //   69	71	68	finally
  }
  
  boolean hasMessages(Handler paramHandler) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 6
    //   4: iconst_0
    //   5: ireturn
    //   6: aload_0
    //   7: monitorenter
    //   8: aload_0
    //   9: getfield mMessages : Landroid/os/Message;
    //   12: astore_2
    //   13: aload_2
    //   14: ifnull -> 37
    //   17: aload_2
    //   18: getfield target : Landroid/os/Handler;
    //   21: aload_1
    //   22: if_acmpne -> 29
    //   25: aload_0
    //   26: monitorexit
    //   27: iconst_1
    //   28: ireturn
    //   29: aload_2
    //   30: getfield next : Landroid/os/Message;
    //   33: astore_2
    //   34: goto -> 13
    //   37: aload_0
    //   38: monitorexit
    //   39: iconst_0
    //   40: ireturn
    //   41: astore_1
    //   42: aload_0
    //   43: monitorexit
    //   44: aload_1
    //   45: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #688	-> 0
    //   #689	-> 4
    //   #692	-> 6
    //   #693	-> 8
    //   #694	-> 13
    //   #695	-> 17
    //   #696	-> 25
    //   #698	-> 29
    //   #700	-> 37
    //   #701	-> 41
    // Exception table:
    //   from	to	target	type
    //   8	13	41	finally
    //   17	25	41	finally
    //   25	27	41	finally
    //   29	34	41	finally
    //   37	39	41	finally
    //   42	44	41	finally
  }
  
  void removeMessages(Handler paramHandler, int paramInt, Object paramObject) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 5
    //   4: return
    //   5: aload_0
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mMessages : Landroid/os/Message;
    //   11: astore #4
    //   13: aload #4
    //   15: astore #5
    //   17: aload #4
    //   19: ifnull -> 90
    //   22: aload #4
    //   24: astore #5
    //   26: aload #4
    //   28: getfield target : Landroid/os/Handler;
    //   31: aload_1
    //   32: if_acmpne -> 90
    //   35: aload #4
    //   37: astore #5
    //   39: aload #4
    //   41: getfield what : I
    //   44: iload_2
    //   45: if_icmpne -> 90
    //   48: aload_3
    //   49: ifnull -> 65
    //   52: aload #4
    //   54: astore #5
    //   56: aload #4
    //   58: getfield obj : Ljava/lang/Object;
    //   61: aload_3
    //   62: if_acmpne -> 90
    //   65: aload #4
    //   67: getfield next : Landroid/os/Message;
    //   70: astore #5
    //   72: aload_0
    //   73: aload #5
    //   75: putfield mMessages : Landroid/os/Message;
    //   78: aload #4
    //   80: invokevirtual recycleUnchecked : ()V
    //   83: aload #5
    //   85: astore #4
    //   87: goto -> 13
    //   90: aload #5
    //   92: ifnull -> 167
    //   95: aload #5
    //   97: getfield next : Landroid/os/Message;
    //   100: astore #4
    //   102: aload #4
    //   104: ifnull -> 160
    //   107: aload #4
    //   109: getfield target : Landroid/os/Handler;
    //   112: aload_1
    //   113: if_acmpne -> 160
    //   116: aload #4
    //   118: getfield what : I
    //   121: iload_2
    //   122: if_icmpne -> 160
    //   125: aload_3
    //   126: ifnull -> 138
    //   129: aload #4
    //   131: getfield obj : Ljava/lang/Object;
    //   134: aload_3
    //   135: if_acmpne -> 160
    //   138: aload #4
    //   140: getfield next : Landroid/os/Message;
    //   143: astore #6
    //   145: aload #4
    //   147: invokevirtual recycleUnchecked : ()V
    //   150: aload #5
    //   152: aload #6
    //   154: putfield next : Landroid/os/Message;
    //   157: goto -> 90
    //   160: aload #4
    //   162: astore #5
    //   164: goto -> 90
    //   167: aload_0
    //   168: monitorexit
    //   169: return
    //   170: astore_1
    //   171: aload_0
    //   172: monitorexit
    //   173: aload_1
    //   174: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #705	-> 0
    //   #706	-> 4
    //   #709	-> 5
    //   #710	-> 7
    //   #713	-> 13
    //   #715	-> 65
    //   #716	-> 72
    //   #717	-> 78
    //   #718	-> 83
    //   #719	-> 87
    //   #722	-> 90
    //   #723	-> 95
    //   #724	-> 102
    //   #725	-> 107
    //   #727	-> 138
    //   #728	-> 145
    //   #729	-> 150
    //   #730	-> 157
    //   #733	-> 160
    //   #734	-> 164
    //   #735	-> 167
    //   #736	-> 169
    //   #735	-> 170
    // Exception table:
    //   from	to	target	type
    //   7	13	170	finally
    //   26	35	170	finally
    //   39	48	170	finally
    //   56	65	170	finally
    //   65	72	170	finally
    //   72	78	170	finally
    //   78	83	170	finally
    //   95	102	170	finally
    //   107	125	170	finally
    //   129	138	170	finally
    //   138	145	170	finally
    //   145	150	170	finally
    //   150	157	170	finally
    //   167	169	170	finally
    //   171	173	170	finally
  }
  
  void removeEqualMessages(Handler paramHandler, int paramInt, Object paramObject) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 5
    //   4: return
    //   5: aload_0
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mMessages : Landroid/os/Message;
    //   11: astore #4
    //   13: aload #4
    //   15: astore #5
    //   17: aload #4
    //   19: ifnull -> 97
    //   22: aload #4
    //   24: astore #5
    //   26: aload #4
    //   28: getfield target : Landroid/os/Handler;
    //   31: aload_1
    //   32: if_acmpne -> 97
    //   35: aload #4
    //   37: astore #5
    //   39: aload #4
    //   41: getfield what : I
    //   44: iload_2
    //   45: if_icmpne -> 97
    //   48: aload_3
    //   49: ifnull -> 72
    //   52: aload #4
    //   54: getfield obj : Ljava/lang/Object;
    //   57: astore #6
    //   59: aload #4
    //   61: astore #5
    //   63: aload_3
    //   64: aload #6
    //   66: invokevirtual equals : (Ljava/lang/Object;)Z
    //   69: ifeq -> 97
    //   72: aload #4
    //   74: getfield next : Landroid/os/Message;
    //   77: astore #5
    //   79: aload_0
    //   80: aload #5
    //   82: putfield mMessages : Landroid/os/Message;
    //   85: aload #4
    //   87: invokevirtual recycleUnchecked : ()V
    //   90: aload #5
    //   92: astore #4
    //   94: goto -> 13
    //   97: aload #5
    //   99: ifnull -> 181
    //   102: aload #5
    //   104: getfield next : Landroid/os/Message;
    //   107: astore #4
    //   109: aload #4
    //   111: ifnull -> 174
    //   114: aload #4
    //   116: getfield target : Landroid/os/Handler;
    //   119: aload_1
    //   120: if_acmpne -> 174
    //   123: aload #4
    //   125: getfield what : I
    //   128: iload_2
    //   129: if_icmpne -> 174
    //   132: aload_3
    //   133: ifnull -> 152
    //   136: aload #4
    //   138: getfield obj : Ljava/lang/Object;
    //   141: astore #6
    //   143: aload_3
    //   144: aload #6
    //   146: invokevirtual equals : (Ljava/lang/Object;)Z
    //   149: ifeq -> 174
    //   152: aload #4
    //   154: getfield next : Landroid/os/Message;
    //   157: astore #6
    //   159: aload #4
    //   161: invokevirtual recycleUnchecked : ()V
    //   164: aload #5
    //   166: aload #6
    //   168: putfield next : Landroid/os/Message;
    //   171: goto -> 97
    //   174: aload #4
    //   176: astore #5
    //   178: goto -> 97
    //   181: aload_0
    //   182: monitorexit
    //   183: return
    //   184: astore_1
    //   185: aload_0
    //   186: monitorexit
    //   187: aload_1
    //   188: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #739	-> 0
    //   #740	-> 4
    //   #743	-> 5
    //   #744	-> 7
    //   #747	-> 13
    //   #748	-> 59
    //   #749	-> 72
    //   #750	-> 79
    //   #751	-> 85
    //   #752	-> 90
    //   #753	-> 94
    //   #756	-> 97
    //   #757	-> 102
    //   #758	-> 109
    //   #759	-> 114
    //   #760	-> 143
    //   #761	-> 152
    //   #762	-> 159
    //   #763	-> 164
    //   #764	-> 171
    //   #767	-> 174
    //   #768	-> 178
    //   #769	-> 181
    //   #770	-> 183
    //   #769	-> 184
    // Exception table:
    //   from	to	target	type
    //   7	13	184	finally
    //   26	35	184	finally
    //   39	48	184	finally
    //   52	59	184	finally
    //   63	72	184	finally
    //   72	79	184	finally
    //   79	85	184	finally
    //   85	90	184	finally
    //   102	109	184	finally
    //   114	132	184	finally
    //   136	143	184	finally
    //   143	152	184	finally
    //   152	159	184	finally
    //   159	164	184	finally
    //   164	171	184	finally
    //   181	183	184	finally
    //   185	187	184	finally
  }
  
  void removeMessages(Handler paramHandler, Runnable paramRunnable, Object paramObject) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 181
    //   4: aload_2
    //   5: ifnonnull -> 11
    //   8: goto -> 181
    //   11: aload_0
    //   12: monitorenter
    //   13: aload_0
    //   14: getfield mMessages : Landroid/os/Message;
    //   17: astore #4
    //   19: aload #4
    //   21: astore #5
    //   23: aload #4
    //   25: ifnull -> 96
    //   28: aload #4
    //   30: astore #5
    //   32: aload #4
    //   34: getfield target : Landroid/os/Handler;
    //   37: aload_1
    //   38: if_acmpne -> 96
    //   41: aload #4
    //   43: astore #5
    //   45: aload #4
    //   47: getfield callback : Ljava/lang/Runnable;
    //   50: aload_2
    //   51: if_acmpne -> 96
    //   54: aload_3
    //   55: ifnull -> 71
    //   58: aload #4
    //   60: astore #5
    //   62: aload #4
    //   64: getfield obj : Ljava/lang/Object;
    //   67: aload_3
    //   68: if_acmpne -> 96
    //   71: aload #4
    //   73: getfield next : Landroid/os/Message;
    //   76: astore #5
    //   78: aload_0
    //   79: aload #5
    //   81: putfield mMessages : Landroid/os/Message;
    //   84: aload #4
    //   86: invokevirtual recycleUnchecked : ()V
    //   89: aload #5
    //   91: astore #4
    //   93: goto -> 19
    //   96: aload #5
    //   98: ifnull -> 173
    //   101: aload #5
    //   103: getfield next : Landroid/os/Message;
    //   106: astore #4
    //   108: aload #4
    //   110: ifnull -> 166
    //   113: aload #4
    //   115: getfield target : Landroid/os/Handler;
    //   118: aload_1
    //   119: if_acmpne -> 166
    //   122: aload #4
    //   124: getfield callback : Ljava/lang/Runnable;
    //   127: aload_2
    //   128: if_acmpne -> 166
    //   131: aload_3
    //   132: ifnull -> 144
    //   135: aload #4
    //   137: getfield obj : Ljava/lang/Object;
    //   140: aload_3
    //   141: if_acmpne -> 166
    //   144: aload #4
    //   146: getfield next : Landroid/os/Message;
    //   149: astore #6
    //   151: aload #4
    //   153: invokevirtual recycleUnchecked : ()V
    //   156: aload #5
    //   158: aload #6
    //   160: putfield next : Landroid/os/Message;
    //   163: goto -> 96
    //   166: aload #4
    //   168: astore #5
    //   170: goto -> 96
    //   173: aload_0
    //   174: monitorexit
    //   175: return
    //   176: astore_1
    //   177: aload_0
    //   178: monitorexit
    //   179: aload_1
    //   180: athrow
    //   181: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #773	-> 0
    //   #777	-> 11
    //   #778	-> 13
    //   #781	-> 19
    //   #783	-> 71
    //   #784	-> 78
    //   #785	-> 84
    //   #786	-> 89
    //   #787	-> 93
    //   #790	-> 96
    //   #791	-> 101
    //   #792	-> 108
    //   #793	-> 113
    //   #795	-> 144
    //   #796	-> 151
    //   #797	-> 156
    //   #798	-> 163
    //   #801	-> 166
    //   #802	-> 170
    //   #803	-> 173
    //   #804	-> 175
    //   #803	-> 176
    //   #774	-> 181
    // Exception table:
    //   from	to	target	type
    //   13	19	176	finally
    //   32	41	176	finally
    //   45	54	176	finally
    //   62	71	176	finally
    //   71	78	176	finally
    //   78	84	176	finally
    //   84	89	176	finally
    //   101	108	176	finally
    //   113	131	176	finally
    //   135	144	176	finally
    //   144	151	176	finally
    //   151	156	176	finally
    //   156	163	176	finally
    //   173	175	176	finally
    //   177	179	176	finally
  }
  
  void removeEqualMessages(Handler paramHandler, Runnable paramRunnable, Object paramObject) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 195
    //   4: aload_2
    //   5: ifnonnull -> 11
    //   8: goto -> 195
    //   11: aload_0
    //   12: monitorenter
    //   13: aload_0
    //   14: getfield mMessages : Landroid/os/Message;
    //   17: astore #4
    //   19: aload #4
    //   21: astore #5
    //   23: aload #4
    //   25: ifnull -> 103
    //   28: aload #4
    //   30: astore #5
    //   32: aload #4
    //   34: getfield target : Landroid/os/Handler;
    //   37: aload_1
    //   38: if_acmpne -> 103
    //   41: aload #4
    //   43: astore #5
    //   45: aload #4
    //   47: getfield callback : Ljava/lang/Runnable;
    //   50: aload_2
    //   51: if_acmpne -> 103
    //   54: aload_3
    //   55: ifnull -> 78
    //   58: aload #4
    //   60: getfield obj : Ljava/lang/Object;
    //   63: astore #6
    //   65: aload #4
    //   67: astore #5
    //   69: aload_3
    //   70: aload #6
    //   72: invokevirtual equals : (Ljava/lang/Object;)Z
    //   75: ifeq -> 103
    //   78: aload #4
    //   80: getfield next : Landroid/os/Message;
    //   83: astore #5
    //   85: aload_0
    //   86: aload #5
    //   88: putfield mMessages : Landroid/os/Message;
    //   91: aload #4
    //   93: invokevirtual recycleUnchecked : ()V
    //   96: aload #5
    //   98: astore #4
    //   100: goto -> 19
    //   103: aload #5
    //   105: ifnull -> 187
    //   108: aload #5
    //   110: getfield next : Landroid/os/Message;
    //   113: astore #4
    //   115: aload #4
    //   117: ifnull -> 180
    //   120: aload #4
    //   122: getfield target : Landroid/os/Handler;
    //   125: aload_1
    //   126: if_acmpne -> 180
    //   129: aload #4
    //   131: getfield callback : Ljava/lang/Runnable;
    //   134: aload_2
    //   135: if_acmpne -> 180
    //   138: aload_3
    //   139: ifnull -> 158
    //   142: aload #4
    //   144: getfield obj : Ljava/lang/Object;
    //   147: astore #6
    //   149: aload_3
    //   150: aload #6
    //   152: invokevirtual equals : (Ljava/lang/Object;)Z
    //   155: ifeq -> 180
    //   158: aload #4
    //   160: getfield next : Landroid/os/Message;
    //   163: astore #6
    //   165: aload #4
    //   167: invokevirtual recycleUnchecked : ()V
    //   170: aload #5
    //   172: aload #6
    //   174: putfield next : Landroid/os/Message;
    //   177: goto -> 103
    //   180: aload #4
    //   182: astore #5
    //   184: goto -> 103
    //   187: aload_0
    //   188: monitorexit
    //   189: return
    //   190: astore_1
    //   191: aload_0
    //   192: monitorexit
    //   193: aload_1
    //   194: athrow
    //   195: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #807	-> 0
    //   #811	-> 11
    //   #812	-> 13
    //   #815	-> 19
    //   #816	-> 65
    //   #817	-> 78
    //   #818	-> 85
    //   #819	-> 91
    //   #820	-> 96
    //   #821	-> 100
    //   #824	-> 103
    //   #825	-> 108
    //   #826	-> 115
    //   #827	-> 120
    //   #828	-> 149
    //   #829	-> 158
    //   #830	-> 165
    //   #831	-> 170
    //   #832	-> 177
    //   #835	-> 180
    //   #836	-> 184
    //   #837	-> 187
    //   #838	-> 189
    //   #837	-> 190
    //   #808	-> 195
    // Exception table:
    //   from	to	target	type
    //   13	19	190	finally
    //   32	41	190	finally
    //   45	54	190	finally
    //   58	65	190	finally
    //   69	78	190	finally
    //   78	85	190	finally
    //   85	91	190	finally
    //   91	96	190	finally
    //   108	115	190	finally
    //   120	138	190	finally
    //   142	149	190	finally
    //   149	158	190	finally
    //   158	165	190	finally
    //   165	170	190	finally
    //   170	177	190	finally
    //   187	189	190	finally
    //   191	193	190	finally
  }
  
  void removeCallbacksAndMessages(Handler paramHandler, Object paramObject) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 5
    //   4: return
    //   5: aload_0
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mMessages : Landroid/os/Message;
    //   11: astore_3
    //   12: aload_3
    //   13: astore #4
    //   15: aload_3
    //   16: ifnull -> 67
    //   19: aload_3
    //   20: astore #4
    //   22: aload_3
    //   23: getfield target : Landroid/os/Handler;
    //   26: aload_1
    //   27: if_acmpne -> 67
    //   30: aload_2
    //   31: ifnull -> 45
    //   34: aload_3
    //   35: astore #4
    //   37: aload_3
    //   38: getfield obj : Ljava/lang/Object;
    //   41: aload_2
    //   42: if_acmpne -> 67
    //   45: aload_3
    //   46: getfield next : Landroid/os/Message;
    //   49: astore #4
    //   51: aload_0
    //   52: aload #4
    //   54: putfield mMessages : Landroid/os/Message;
    //   57: aload_3
    //   58: invokevirtual recycleUnchecked : ()V
    //   61: aload #4
    //   63: astore_3
    //   64: goto -> 12
    //   67: aload #4
    //   69: ifnull -> 128
    //   72: aload #4
    //   74: getfield next : Landroid/os/Message;
    //   77: astore_3
    //   78: aload_3
    //   79: ifnull -> 122
    //   82: aload_3
    //   83: getfield target : Landroid/os/Handler;
    //   86: aload_1
    //   87: if_acmpne -> 122
    //   90: aload_2
    //   91: ifnull -> 102
    //   94: aload_3
    //   95: getfield obj : Ljava/lang/Object;
    //   98: aload_2
    //   99: if_acmpne -> 122
    //   102: aload_3
    //   103: getfield next : Landroid/os/Message;
    //   106: astore #5
    //   108: aload_3
    //   109: invokevirtual recycleUnchecked : ()V
    //   112: aload #4
    //   114: aload #5
    //   116: putfield next : Landroid/os/Message;
    //   119: goto -> 67
    //   122: aload_3
    //   123: astore #4
    //   125: goto -> 67
    //   128: aload_0
    //   129: monitorexit
    //   130: return
    //   131: astore_1
    //   132: aload_0
    //   133: monitorexit
    //   134: aload_1
    //   135: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #842	-> 0
    //   #843	-> 4
    //   #846	-> 5
    //   #847	-> 7
    //   #850	-> 12
    //   #852	-> 45
    //   #853	-> 51
    //   #854	-> 57
    //   #855	-> 61
    //   #856	-> 64
    //   #859	-> 67
    //   #860	-> 72
    //   #861	-> 78
    //   #862	-> 82
    //   #863	-> 102
    //   #864	-> 108
    //   #865	-> 112
    //   #866	-> 119
    //   #869	-> 122
    //   #870	-> 125
    //   #871	-> 128
    //   #872	-> 130
    //   #871	-> 131
    // Exception table:
    //   from	to	target	type
    //   7	12	131	finally
    //   22	30	131	finally
    //   37	45	131	finally
    //   45	51	131	finally
    //   51	57	131	finally
    //   57	61	131	finally
    //   72	78	131	finally
    //   82	90	131	finally
    //   94	102	131	finally
    //   102	108	131	finally
    //   108	112	131	finally
    //   112	119	131	finally
    //   128	130	131	finally
    //   132	134	131	finally
  }
  
  void removeCallbacksAndEqualMessages(Handler paramHandler, Object paramObject) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 5
    //   4: return
    //   5: aload_0
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mMessages : Landroid/os/Message;
    //   11: astore_3
    //   12: aload_3
    //   13: astore #4
    //   15: aload_3
    //   16: ifnull -> 74
    //   19: aload_3
    //   20: astore #4
    //   22: aload_3
    //   23: getfield target : Landroid/os/Handler;
    //   26: aload_1
    //   27: if_acmpne -> 74
    //   30: aload_2
    //   31: ifnull -> 52
    //   34: aload_3
    //   35: getfield obj : Ljava/lang/Object;
    //   38: astore #5
    //   40: aload_3
    //   41: astore #4
    //   43: aload_2
    //   44: aload #5
    //   46: invokevirtual equals : (Ljava/lang/Object;)Z
    //   49: ifeq -> 74
    //   52: aload_3
    //   53: getfield next : Landroid/os/Message;
    //   56: astore #4
    //   58: aload_0
    //   59: aload #4
    //   61: putfield mMessages : Landroid/os/Message;
    //   64: aload_3
    //   65: invokevirtual recycleUnchecked : ()V
    //   68: aload #4
    //   70: astore_3
    //   71: goto -> 12
    //   74: aload #4
    //   76: ifnull -> 138
    //   79: aload #4
    //   81: getfield next : Landroid/os/Message;
    //   84: astore_3
    //   85: aload_3
    //   86: ifnull -> 132
    //   89: aload_3
    //   90: getfield target : Landroid/os/Handler;
    //   93: aload_1
    //   94: if_acmpne -> 132
    //   97: aload_2
    //   98: ifnull -> 112
    //   101: aload_2
    //   102: aload_3
    //   103: getfield obj : Ljava/lang/Object;
    //   106: invokevirtual equals : (Ljava/lang/Object;)Z
    //   109: ifeq -> 132
    //   112: aload_3
    //   113: getfield next : Landroid/os/Message;
    //   116: astore #5
    //   118: aload_3
    //   119: invokevirtual recycleUnchecked : ()V
    //   122: aload #4
    //   124: aload #5
    //   126: putfield next : Landroid/os/Message;
    //   129: goto -> 74
    //   132: aload_3
    //   133: astore #4
    //   135: goto -> 74
    //   138: aload_0
    //   139: monitorexit
    //   140: return
    //   141: astore_1
    //   142: aload_0
    //   143: monitorexit
    //   144: aload_1
    //   145: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #875	-> 0
    //   #876	-> 4
    //   #879	-> 5
    //   #880	-> 7
    //   #883	-> 12
    //   #884	-> 40
    //   #885	-> 52
    //   #886	-> 58
    //   #887	-> 64
    //   #888	-> 68
    //   #889	-> 71
    //   #892	-> 74
    //   #893	-> 79
    //   #894	-> 85
    //   #895	-> 89
    //   #896	-> 112
    //   #897	-> 118
    //   #898	-> 122
    //   #899	-> 129
    //   #902	-> 132
    //   #903	-> 135
    //   #904	-> 138
    //   #905	-> 140
    //   #904	-> 141
    // Exception table:
    //   from	to	target	type
    //   7	12	141	finally
    //   22	30	141	finally
    //   34	40	141	finally
    //   43	52	141	finally
    //   52	58	141	finally
    //   58	64	141	finally
    //   64	68	141	finally
    //   79	85	141	finally
    //   89	97	141	finally
    //   101	112	141	finally
    //   112	118	141	finally
    //   118	122	141	finally
    //   122	129	141	finally
    //   138	140	141	finally
    //   142	144	141	finally
  }
  
  private void removeAllMessagesLocked() {
    Message message = this.mMessages;
    while (message != null) {
      Message message1 = message.next;
      message.recycleUnchecked();
      message = message1;
    } 
    this.mMessages = null;
  }
  
  private void removeAllFutureMessagesLocked() {
    long l = SystemClock.uptimeMillis();
    Message message = this.mMessages;
    if (message != null) {
      Message message1 = message;
      if (message.when > l) {
        removeAllMessagesLocked();
      } else {
        while (true) {
          message = message1.next;
          if (message == null)
            return; 
          if (message.when > l) {
            message1.next = null;
            message1 = message;
            do {
              message = message1.next;
              message1.recycleUnchecked();
              message1 = message;
            } while (message != null);
            break;
          } 
          message1 = message;
        } 
      } 
    } 
  }
  
  void dump(Printer paramPrinter, String paramString, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: invokestatic uptimeMillis : ()J
    //   5: lstore #4
    //   7: iconst_0
    //   8: istore #6
    //   10: aload_0
    //   11: getfield mMessages : Landroid/os/Message;
    //   14: astore #7
    //   16: aload #7
    //   18: ifnull -> 112
    //   21: aload_3
    //   22: ifnull -> 34
    //   25: aload_3
    //   26: aload #7
    //   28: getfield target : Landroid/os/Handler;
    //   31: if_acmpne -> 99
    //   34: new java/lang/StringBuilder
    //   37: astore #8
    //   39: aload #8
    //   41: invokespecial <init> : ()V
    //   44: aload #8
    //   46: aload_2
    //   47: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   50: pop
    //   51: aload #8
    //   53: ldc 'Message '
    //   55: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   58: pop
    //   59: aload #8
    //   61: iload #6
    //   63: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   66: pop
    //   67: aload #8
    //   69: ldc ': '
    //   71: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   74: pop
    //   75: aload #8
    //   77: aload #7
    //   79: lload #4
    //   81: invokevirtual toString : (J)Ljava/lang/String;
    //   84: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   87: pop
    //   88: aload_1
    //   89: aload #8
    //   91: invokevirtual toString : ()Ljava/lang/String;
    //   94: invokeinterface println : (Ljava/lang/String;)V
    //   99: iinc #6, 1
    //   102: aload #7
    //   104: getfield next : Landroid/os/Message;
    //   107: astore #7
    //   109: goto -> 16
    //   112: new java/lang/StringBuilder
    //   115: astore_3
    //   116: aload_3
    //   117: invokespecial <init> : ()V
    //   120: aload_3
    //   121: aload_2
    //   122: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   125: pop
    //   126: aload_3
    //   127: ldc '(Total messages: '
    //   129: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   132: pop
    //   133: aload_3
    //   134: iload #6
    //   136: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   139: pop
    //   140: aload_3
    //   141: ldc_w ', polling='
    //   144: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   147: pop
    //   148: aload_3
    //   149: aload_0
    //   150: invokespecial isPollingLocked : ()Z
    //   153: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   156: pop
    //   157: aload_3
    //   158: ldc_w ', quitting='
    //   161: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   164: pop
    //   165: aload_3
    //   166: aload_0
    //   167: getfield mQuitting : Z
    //   170: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   173: pop
    //   174: aload_3
    //   175: ldc_w ')'
    //   178: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   181: pop
    //   182: aload_1
    //   183: aload_3
    //   184: invokevirtual toString : ()Ljava/lang/String;
    //   187: invokeinterface println : (Ljava/lang/String;)V
    //   192: aload_0
    //   193: monitorexit
    //   194: return
    //   195: astore_1
    //   196: aload_0
    //   197: monitorexit
    //   198: aload_1
    //   199: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #946	-> 0
    //   #947	-> 2
    //   #948	-> 7
    //   #949	-> 10
    //   #950	-> 21
    //   #951	-> 34
    //   #953	-> 99
    //   #949	-> 102
    //   #955	-> 112
    //   #957	-> 192
    //   #958	-> 194
    //   #957	-> 195
    // Exception table:
    //   from	to	target	type
    //   2	7	195	finally
    //   10	16	195	finally
    //   25	34	195	finally
    //   34	99	195	finally
    //   102	109	195	finally
    //   112	192	195	finally
    //   192	194	195	finally
    //   196	198	195	finally
  }
  
  void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    // Byte code:
    //   0: aload_1
    //   1: lload_2
    //   2: invokevirtual start : (J)J
    //   5: lstore_2
    //   6: aload_0
    //   7: monitorenter
    //   8: aload_0
    //   9: getfield mMessages : Landroid/os/Message;
    //   12: astore #4
    //   14: aload #4
    //   16: ifnull -> 38
    //   19: aload #4
    //   21: aload_1
    //   22: ldc2_w 2246267895809
    //   25: invokevirtual dumpDebug : (Landroid/util/proto/ProtoOutputStream;J)V
    //   28: aload #4
    //   30: getfield next : Landroid/os/Message;
    //   33: astore #4
    //   35: goto -> 14
    //   38: aload_1
    //   39: ldc2_w 1133871366146
    //   42: aload_0
    //   43: invokespecial isPollingLocked : ()Z
    //   46: invokevirtual write : (JZ)V
    //   49: aload_1
    //   50: ldc2_w 1133871366147
    //   53: aload_0
    //   54: getfield mQuitting : Z
    //   57: invokevirtual write : (JZ)V
    //   60: aload_0
    //   61: monitorexit
    //   62: aload_1
    //   63: lload_2
    //   64: invokevirtual end : (J)V
    //   67: return
    //   68: astore_1
    //   69: aload_0
    //   70: monitorexit
    //   71: aload_1
    //   72: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #961	-> 0
    //   #962	-> 6
    //   #963	-> 8
    //   #964	-> 19
    //   #963	-> 28
    //   #966	-> 38
    //   #967	-> 49
    //   #968	-> 60
    //   #969	-> 62
    //   #970	-> 67
    //   #968	-> 68
    // Exception table:
    //   from	to	target	type
    //   8	14	68	finally
    //   19	28	68	finally
    //   28	35	68	finally
    //   38	49	68	finally
    //   49	60	68	finally
    //   60	62	68	finally
    //   69	71	68	finally
  }
  
  private static final class FileDescriptorRecord {
    public final FileDescriptor mDescriptor;
    
    public int mEvents;
    
    public MessageQueue.OnFileDescriptorEventListener mListener;
    
    public int mSeq;
    
    public FileDescriptorRecord(FileDescriptor param1FileDescriptor, int param1Int, MessageQueue.OnFileDescriptorEventListener param1OnFileDescriptorEventListener) {
      this.mDescriptor = param1FileDescriptor;
      this.mEvents = param1Int;
      this.mListener = param1OnFileDescriptorEventListener;
    }
  }
  
  static boolean DEBUG_MESSAGE = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final boolean DEBUG = false;
  
  public final void dumpMessage() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mMessages : Landroid/os/Message;
    //   6: ifnull -> 114
    //   9: invokestatic uptimeMillis : ()J
    //   12: lstore_1
    //   13: ldc 'MessageQueue'
    //   15: ldc_w 'Dump messages in Queue: '
    //   18: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   21: pop
    //   22: aload_0
    //   23: getfield mMessages : Landroid/os/Message;
    //   26: astore_3
    //   27: iconst_0
    //   28: istore #4
    //   30: aload_3
    //   31: ifnull -> 111
    //   34: iinc #4, 1
    //   37: iload #4
    //   39: bipush #10
    //   41: if_icmpgt -> 111
    //   44: new java/lang/StringBuilder
    //   47: astore #5
    //   49: aload #5
    //   51: invokespecial <init> : ()V
    //   54: aload #5
    //   56: ldc_w 'Current msg <'
    //   59: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   62: pop
    //   63: aload #5
    //   65: iload #4
    //   67: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   70: pop
    //   71: aload #5
    //   73: ldc_w '>  = '
    //   76: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   79: pop
    //   80: aload #5
    //   82: aload_3
    //   83: lload_1
    //   84: iconst_0
    //   85: invokevirtual toStringLite : (JZ)Ljava/lang/String;
    //   88: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   91: pop
    //   92: ldc 'MessageQueue'
    //   94: aload #5
    //   96: invokevirtual toString : ()Ljava/lang/String;
    //   99: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   102: pop
    //   103: aload_3
    //   104: getfield next : Landroid/os/Message;
    //   107: astore_3
    //   108: goto -> 30
    //   111: goto -> 123
    //   114: ldc 'MessageQueue'
    //   116: ldc_w 'mMessages is null'
    //   119: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   122: pop
    //   123: aload_0
    //   124: monitorexit
    //   125: return
    //   126: astore_3
    //   127: aload_0
    //   128: monitorexit
    //   129: aload_3
    //   130: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1089	-> 0
    //   #1090	-> 2
    //   #1091	-> 9
    //   #1092	-> 13
    //   #1093	-> 22
    //   #1094	-> 27
    //   #1095	-> 30
    //   #1096	-> 34
    //   #1097	-> 37
    //   #1098	-> 44
    //   #1102	-> 103
    //   #1104	-> 111
    //   #1105	-> 114
    //   #1107	-> 123
    //   #1108	-> 125
    //   #1107	-> 126
    // Exception table:
    //   from	to	target	type
    //   2	9	126	finally
    //   9	13	126	finally
    //   13	22	126	finally
    //   22	27	126	finally
    //   44	103	126	finally
    //   103	108	126	finally
    //   114	123	126	finally
    //   123	125	126	finally
    //   127	129	126	finally
  }
  
  private static native void nativeDestroy(long paramLong);
  
  private static native long nativeInit();
  
  private static native boolean nativeIsPolling(long paramLong);
  
  private native void nativePollOnce(long paramLong, int paramInt);
  
  private static native void nativeSetFileDescriptorEvents(long paramLong, int paramInt1, int paramInt2);
  
  private static native void nativeWake(long paramLong);
  
  public static interface IdleHandler {
    boolean queueIdle();
  }
  
  public static interface OnFileDescriptorEventListener {
    public static final int EVENT_ERROR = 4;
    
    public static final int EVENT_INPUT = 1;
    
    public static final int EVENT_OUTPUT = 2;
    
    int onFileDescriptorEvents(FileDescriptor param1FileDescriptor, int param1Int);
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface Events {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Events {}
}
