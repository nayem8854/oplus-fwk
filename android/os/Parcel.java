package android.os;

import android.app.AppOpsManager;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.ExceptionUtils;
import android.util.Log;
import android.util.Size;
import android.util.SizeF;
import android.util.Slog;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import dalvik.annotation.optimization.CriticalNative;
import dalvik.annotation.optimization.FastNative;
import dalvik.system.VMRuntime;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import libcore.util.ArrayUtils;
import libcore.util.SneakyThrow;

public final class Parcel {
  private static final Parcel[] sOwnedPool = new Parcel[6];
  
  static {
    sHolderPool = new Parcel[6];
    STRING_CREATOR = (Parcelable.Creator<String>)new Object();
    mCreators = new HashMap<>();
  }
  
  public static interface SquashReadHelper<T> {
    T readRawParceled(Parcel param1Parcel);
  }
  
  public static class ReadWriteHelper {
    public static final ReadWriteHelper DEFAULT = new ReadWriteHelper();
    
    public void writeString8(Parcel param1Parcel, String param1String) {
      param1Parcel.writeString8NoHelper(param1String);
    }
    
    public void writeString16(Parcel param1Parcel, String param1String) {
      param1Parcel.writeString16NoHelper(param1String);
    }
    
    public String readString8(Parcel param1Parcel) {
      return param1Parcel.readString8NoHelper();
    }
    
    public String readString16(Parcel param1Parcel) {
      return param1Parcel.readString16NoHelper();
    }
  }
  
  private ReadWriteHelper mReadWriteHelper = ReadWriteHelper.DEFAULT;
  
  public static Parcel obtain() {
    // Byte code:
    //   0: getstatic android/os/Parcel.sOwnedPool : [Landroid/os/Parcel;
    //   3: astore_0
    //   4: aload_0
    //   5: monitorenter
    //   6: iconst_0
    //   7: istore_1
    //   8: iload_1
    //   9: bipush #6
    //   11: if_icmpge -> 43
    //   14: aload_0
    //   15: iload_1
    //   16: aaload
    //   17: astore_2
    //   18: aload_2
    //   19: ifnull -> 37
    //   22: aload_0
    //   23: iload_1
    //   24: aconst_null
    //   25: aastore
    //   26: aload_2
    //   27: getstatic android/os/Parcel$ReadWriteHelper.DEFAULT : Landroid/os/Parcel$ReadWriteHelper;
    //   30: putfield mReadWriteHelper : Landroid/os/Parcel$ReadWriteHelper;
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_2
    //   36: areturn
    //   37: iinc #1, 1
    //   40: goto -> 8
    //   43: aload_0
    //   44: monitorexit
    //   45: new android/os/Parcel
    //   48: dup
    //   49: lconst_0
    //   50: invokespecial <init> : (J)V
    //   53: areturn
    //   54: astore_2
    //   55: aload_0
    //   56: monitorexit
    //   57: aload_2
    //   58: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #422	-> 0
    //   #423	-> 4
    //   #425	-> 6
    //   #426	-> 14
    //   #427	-> 18
    //   #428	-> 22
    //   #432	-> 26
    //   #433	-> 33
    //   #425	-> 37
    //   #436	-> 43
    //   #437	-> 45
    //   #436	-> 54
    // Exception table:
    //   from	to	target	type
    //   26	33	54	finally
    //   33	35	54	finally
    //   43	45	54	finally
    //   55	57	54	finally
  }
  
  public final void recycle() {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial freeBuffer : ()V
    //   4: aload_0
    //   5: getfield mOwnsNativeParcelObject : Z
    //   8: ifeq -> 18
    //   11: getstatic android/os/Parcel.sOwnedPool : [Landroid/os/Parcel;
    //   14: astore_1
    //   15: goto -> 27
    //   18: aload_0
    //   19: lconst_0
    //   20: putfield mNativePtr : J
    //   23: getstatic android/os/Parcel.sHolderPool : [Landroid/os/Parcel;
    //   26: astore_1
    //   27: aload_1
    //   28: monitorenter
    //   29: iconst_0
    //   30: istore_2
    //   31: iload_2
    //   32: bipush #6
    //   34: if_icmpge -> 56
    //   37: aload_1
    //   38: iload_2
    //   39: aaload
    //   40: ifnonnull -> 50
    //   43: aload_1
    //   44: iload_2
    //   45: aload_0
    //   46: aastore
    //   47: aload_1
    //   48: monitorexit
    //   49: return
    //   50: iinc #2, 1
    //   53: goto -> 31
    //   56: aload_1
    //   57: monitorexit
    //   58: return
    //   59: astore_3
    //   60: aload_1
    //   61: monitorexit
    //   62: aload_3
    //   63: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #446	-> 0
    //   #449	-> 4
    //   #450	-> 11
    //   #452	-> 18
    //   #453	-> 23
    //   #456	-> 27
    //   #457	-> 29
    //   #458	-> 37
    //   #459	-> 43
    //   #460	-> 47
    //   #457	-> 50
    //   #463	-> 56
    //   #464	-> 58
    //   #463	-> 59
    // Exception table:
    //   from	to	target	type
    //   47	49	59	finally
    //   56	58	59	finally
    //   60	62	59	finally
  }
  
  public void setReadWriteHelper(ReadWriteHelper paramReadWriteHelper) {
    if (paramReadWriteHelper == null)
      paramReadWriteHelper = ReadWriteHelper.DEFAULT; 
    this.mReadWriteHelper = paramReadWriteHelper;
  }
  
  public boolean hasReadWriteHelper() {
    boolean bool;
    ReadWriteHelper readWriteHelper = this.mReadWriteHelper;
    if (readWriteHelper != null && readWriteHelper != ReadWriteHelper.DEFAULT) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final int dataSize() {
    return nativeDataSize(this.mNativePtr);
  }
  
  public final int dataAvail() {
    return nativeDataAvail(this.mNativePtr);
  }
  
  public final int dataPosition() {
    return nativeDataPosition(this.mNativePtr);
  }
  
  public final int dataCapacity() {
    return nativeDataCapacity(this.mNativePtr);
  }
  
  public final void setDataSize(int paramInt) {
    updateNativeSize(nativeSetDataSize(this.mNativePtr, paramInt));
  }
  
  public final void setDataPosition(int paramInt) {
    nativeSetDataPosition(this.mNativePtr, paramInt);
  }
  
  public final void setDataCapacity(int paramInt) {
    nativeSetDataCapacity(this.mNativePtr, paramInt);
  }
  
  public final boolean pushAllowFds(boolean paramBoolean) {
    return nativePushAllowFds(this.mNativePtr, paramBoolean);
  }
  
  public final void restoreAllowFds(boolean paramBoolean) {
    nativeRestoreAllowFds(this.mNativePtr, paramBoolean);
  }
  
  public final byte[] marshall() {
    return nativeMarshall(this.mNativePtr);
  }
  
  public final void unmarshall(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    updateNativeSize(nativeUnmarshall(this.mNativePtr, paramArrayOfbyte, paramInt1, paramInt2));
  }
  
  public final void appendFrom(Parcel paramParcel, int paramInt1, int paramInt2) {
    if (paramParcel != null)
      try {
        updateNativeSize(nativeAppendFrom(this.mNativePtr, paramParcel.mNativePtr, paramInt1, paramInt2));
      } catch (IllegalArgumentException illegalArgumentException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("appendFrom IllegalArgumentException, offset:");
        stringBuilder.append(paramInt1);
        stringBuilder.append(" length:");
        stringBuilder.append(paramInt2);
        stringBuilder.append(" parcel size:");
        stringBuilder.append(paramParcel.dataSize());
        Log.w("Parcel", stringBuilder.toString());
        throw illegalArgumentException;
      }  
  }
  
  public final int compareData(Parcel paramParcel) {
    return nativeCompareData(this.mNativePtr, paramParcel.mNativePtr);
  }
  
  public final void setClassCookie(Class paramClass, Object paramObject) {
    if (this.mClassCookies == null)
      this.mClassCookies = new ArrayMap(); 
    this.mClassCookies.put(paramClass, paramObject);
  }
  
  public final Object getClassCookie(Class paramClass) {
    ArrayMap<Class, Object> arrayMap = this.mClassCookies;
    if (arrayMap != null) {
      Object object = arrayMap.get(paramClass);
    } else {
      paramClass = null;
    } 
    return paramClass;
  }
  
  public final void adoptClassCookies(Parcel paramParcel) {
    this.mClassCookies = paramParcel.mClassCookies;
  }
  
  public Map<Class, Object> copyClassCookies() {
    return (Map<Class, Object>)new ArrayMap(this.mClassCookies);
  }
  
  public void putClassCookies(Map<Class, Object> paramMap) {
    if (paramMap == null)
      return; 
    if (this.mClassCookies == null)
      this.mClassCookies = new ArrayMap(); 
    this.mClassCookies.putAll(paramMap);
  }
  
  public final boolean hasFileDescriptors() {
    return nativeHasFileDescriptors(this.mNativePtr);
  }
  
  public final void writeInterfaceToken(String paramString) {
    nativeWriteInterfaceToken(this.mNativePtr, paramString);
  }
  
  public final void enforceInterface(String paramString) {
    nativeEnforceInterface(this.mNativePtr, paramString);
  }
  
  public boolean replaceCallingWorkSourceUid(int paramInt) {
    return nativeReplaceCallingWorkSourceUid(this.mNativePtr, paramInt);
  }
  
  public int readCallingWorkSourceUid() {
    return nativeReadCallingWorkSourceUid(this.mNativePtr);
  }
  
  public final void writeByteArray(byte[] paramArrayOfbyte) {
    boolean bool;
    if (paramArrayOfbyte != null) {
      bool = paramArrayOfbyte.length;
    } else {
      bool = false;
    } 
    writeByteArray(paramArrayOfbyte, 0, bool);
  }
  
  public final void writeByteArray(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    if (paramArrayOfbyte == null) {
      writeInt(-1);
      return;
    } 
    ArrayUtils.throwsIfOutOfBounds(paramArrayOfbyte.length, paramInt1, paramInt2);
    nativeWriteByteArray(this.mNativePtr, paramArrayOfbyte, paramInt1, paramInt2);
  }
  
  public final void writeBlob(byte[] paramArrayOfbyte) {
    boolean bool;
    if (paramArrayOfbyte != null) {
      bool = paramArrayOfbyte.length;
    } else {
      bool = false;
    } 
    writeBlob(paramArrayOfbyte, 0, bool);
  }
  
  public final void writeBlob(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    if (paramArrayOfbyte == null) {
      writeInt(-1);
      return;
    } 
    ArrayUtils.throwsIfOutOfBounds(paramArrayOfbyte.length, paramInt1, paramInt2);
    nativeWriteBlob(this.mNativePtr, paramArrayOfbyte, paramInt1, paramInt2);
  }
  
  public final void writeInt(int paramInt) {
    nativeWriteInt(this.mNativePtr, paramInt);
  }
  
  public final void writeLong(long paramLong) {
    nativeWriteLong(this.mNativePtr, paramLong);
  }
  
  public final void writeFloat(float paramFloat) {
    nativeWriteFloat(this.mNativePtr, paramFloat);
  }
  
  public final void writeDouble(double paramDouble) {
    nativeWriteDouble(this.mNativePtr, paramDouble);
  }
  
  public final void writeString(String paramString) {
    writeString16(paramString);
  }
  
  public final void writeString8(String paramString) {
    this.mReadWriteHelper.writeString8(this, paramString);
  }
  
  public final void writeString16(String paramString) {
    this.mReadWriteHelper.writeString16(this, paramString);
  }
  
  public void writeStringNoHelper(String paramString) {
    writeString16NoHelper(paramString);
  }
  
  public void writeString8NoHelper(String paramString) {
    nativeWriteString8(this.mNativePtr, paramString);
  }
  
  public void writeString16NoHelper(String paramString) {
    nativeWriteString16(this.mNativePtr, paramString);
  }
  
  public final void writeBoolean(boolean paramBoolean) {
    writeInt(paramBoolean);
  }
  
  public final void writeCharSequence(CharSequence paramCharSequence) {
    TextUtils.writeToParcel(paramCharSequence, this, 0);
  }
  
  public final void writeStrongBinder(IBinder paramIBinder) {
    nativeWriteStrongBinder(this.mNativePtr, paramIBinder);
  }
  
  public final void writeStrongInterface(IInterface paramIInterface) {
    IBinder iBinder;
    if (paramIInterface == null) {
      paramIInterface = null;
    } else {
      iBinder = paramIInterface.asBinder();
    } 
    writeStrongBinder(iBinder);
  }
  
  public final void writeFileDescriptor(FileDescriptor paramFileDescriptor) {
    updateNativeSize(nativeWriteFileDescriptor(this.mNativePtr, paramFileDescriptor));
  }
  
  private void updateNativeSize(long paramLong) {
    if (this.mOwnsNativeParcelObject) {
      long l = paramLong;
      if (paramLong > 2147483647L)
        l = 2147483647L; 
      paramLong = this.mNativeSize;
      if (l != paramLong) {
        int i = (int)(l - paramLong);
        if (i > 0) {
          VMRuntime.getRuntime().registerNativeAllocation(i);
        } else {
          VMRuntime.getRuntime().registerNativeFree(-i);
        } 
        this.mNativeSize = l;
      } 
    } 
  }
  
  public final void writeRawFileDescriptor(FileDescriptor paramFileDescriptor) {
    nativeWriteFileDescriptor(this.mNativePtr, paramFileDescriptor);
  }
  
  public final void writeRawFileDescriptorArray(FileDescriptor[] paramArrayOfFileDescriptor) {
    if (paramArrayOfFileDescriptor != null) {
      int i = paramArrayOfFileDescriptor.length;
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeRawFileDescriptor(paramArrayOfFileDescriptor[b]); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final void writeByte(byte paramByte) {
    writeInt(paramByte);
  }
  
  public final void writeMap(Map<String, Object> paramMap) {
    writeMapInternal(paramMap);
  }
  
  void writeMapInternal(Map<String, Object> paramMap) {
    if (paramMap == null) {
      writeInt(-1);
      return;
    } 
    Set<Map.Entry<String, Object>> set = paramMap.entrySet();
    int i = set.size();
    writeInt(i);
    for (Map.Entry<String, Object> entry : set) {
      writeValue(entry.getKey());
      writeValue(entry.getValue());
      i--;
    } 
    if (i == 0)
      return; 
    throw new BadParcelableException("Map size does not match number of entries!");
  }
  
  void writeArrayMapInternal(ArrayMap<String, Object> paramArrayMap) {
    if (paramArrayMap == null) {
      writeInt(-1);
      return;
    } 
    int i = paramArrayMap.size();
    writeInt(i);
    for (byte b = 0; b < i; b++) {
      writeString((String)paramArrayMap.keyAt(b));
      writeValue(paramArrayMap.valueAt(b));
    } 
  }
  
  public void writeArrayMap(ArrayMap<String, Object> paramArrayMap) {
    writeArrayMapInternal(paramArrayMap);
  }
  
  public <T extends Parcelable> void writeTypedArrayMap(ArrayMap<String, T> paramArrayMap, int paramInt) {
    if (paramArrayMap == null) {
      writeInt(-1);
      return;
    } 
    int i = paramArrayMap.size();
    writeInt(i);
    for (byte b = 0; b < i; b++) {
      writeString((String)paramArrayMap.keyAt(b));
      writeTypedObject((Parcelable)paramArrayMap.valueAt(b), paramInt);
    } 
  }
  
  public void writeArraySet(ArraySet<? extends Object> paramArraySet) {
    byte b;
    if (paramArraySet != null) {
      b = paramArraySet.size();
    } else {
      b = -1;
    } 
    writeInt(b);
    for (byte b1 = 0; b1 < b; b1++)
      writeValue(paramArraySet.valueAt(b1)); 
  }
  
  public final void writeBundle(Bundle paramBundle) {
    if (paramBundle == null) {
      writeInt(-1);
      return;
    } 
    paramBundle.writeToParcel(this, 0);
  }
  
  public final void writePersistableBundle(PersistableBundle paramPersistableBundle) {
    if (paramPersistableBundle == null) {
      writeInt(-1);
      return;
    } 
    paramPersistableBundle.writeToParcel(this, 0);
  }
  
  public final void writeSize(Size paramSize) {
    writeInt(paramSize.getWidth());
    writeInt(paramSize.getHeight());
  }
  
  public final void writeSizeF(SizeF paramSizeF) {
    writeFloat(paramSizeF.getWidth());
    writeFloat(paramSizeF.getHeight());
  }
  
  public final void writeList(List paramList) {
    if (paramList == null) {
      writeInt(-1);
      return;
    } 
    int i = paramList.size();
    byte b = 0;
    writeInt(i);
    while (b < i) {
      writeValue(paramList.get(b));
      b++;
    } 
  }
  
  public final void writeArray(Object[] paramArrayOfObject) {
    if (paramArrayOfObject == null) {
      writeInt(-1);
      return;
    } 
    int i = paramArrayOfObject.length;
    byte b = 0;
    writeInt(i);
    while (b < i) {
      writeValue(paramArrayOfObject[b]);
      b++;
    } 
  }
  
  public final <T> void writeSparseArray(SparseArray<T> paramSparseArray) {
    if (paramSparseArray == null) {
      writeInt(-1);
      return;
    } 
    int i = paramSparseArray.size();
    writeInt(i);
    byte b = 0;
    while (b < i) {
      writeInt(paramSparseArray.keyAt(b));
      writeValue(paramSparseArray.valueAt(b));
      b++;
    } 
  }
  
  public final void writeSparseBooleanArray(SparseBooleanArray paramSparseBooleanArray) {
    if (paramSparseBooleanArray == null) {
      writeInt(-1);
      return;
    } 
    int i = paramSparseBooleanArray.size();
    writeInt(i);
    byte b = 0;
    while (b < i) {
      writeInt(paramSparseBooleanArray.keyAt(b));
      writeByte((byte)paramSparseBooleanArray.valueAt(b));
      b++;
    } 
  }
  
  public final void writeSparseIntArray(SparseIntArray paramSparseIntArray) {
    if (paramSparseIntArray == null) {
      writeInt(-1);
      return;
    } 
    int i = paramSparseIntArray.size();
    writeInt(i);
    byte b = 0;
    while (b < i) {
      writeInt(paramSparseIntArray.keyAt(b));
      writeInt(paramSparseIntArray.valueAt(b));
      b++;
    } 
  }
  
  public final void writeBooleanArray(boolean[] paramArrayOfboolean) {
    if (paramArrayOfboolean != null) {
      int i = paramArrayOfboolean.length;
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeInt(paramArrayOfboolean[b]); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final boolean[] createBooleanArray() {
    int i = readInt();
    if (i >= 0 && i <= dataAvail() >> 2) {
      boolean[] arrayOfBoolean = new boolean[i];
      for (byte b = 0; b < i; b++) {
        boolean bool;
        if (readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        arrayOfBoolean[b] = bool;
      } 
      return arrayOfBoolean;
    } 
    return null;
  }
  
  public final void readBooleanArray(boolean[] paramArrayOfboolean) {
    int i = readInt();
    if (i == paramArrayOfboolean.length) {
      for (byte b = 0; b < i; b++) {
        boolean bool;
        if (readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        paramArrayOfboolean[b] = bool;
      } 
      return;
    } 
    throw new RuntimeException("bad array lengths");
  }
  
  public final void writeCharArray(char[] paramArrayOfchar) {
    if (paramArrayOfchar != null) {
      int i = paramArrayOfchar.length;
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeInt(paramArrayOfchar[b]); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final char[] createCharArray() {
    int i = readInt();
    if (i >= 0 && i <= dataAvail() >> 2) {
      char[] arrayOfChar = new char[i];
      for (byte b = 0; b < i; b++)
        arrayOfChar[b] = (char)readInt(); 
      return arrayOfChar;
    } 
    return null;
  }
  
  public final void readCharArray(char[] paramArrayOfchar) {
    int i = readInt();
    if (i == paramArrayOfchar.length) {
      for (byte b = 0; b < i; b++)
        paramArrayOfchar[b] = (char)readInt(); 
      return;
    } 
    throw new RuntimeException("bad array lengths");
  }
  
  public final void writeIntArray(int[] paramArrayOfint) {
    if (paramArrayOfint != null) {
      int i = paramArrayOfint.length;
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeInt(paramArrayOfint[b]); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final int[] createIntArray() {
    int i = readInt();
    if (i >= 0 && i <= dataAvail() >> 2) {
      int[] arrayOfInt = new int[i];
      for (byte b = 0; b < i; b++)
        arrayOfInt[b] = readInt(); 
      return arrayOfInt;
    } 
    return null;
  }
  
  public final void readIntArray(int[] paramArrayOfint) {
    int i = readInt();
    if (i == paramArrayOfint.length) {
      for (byte b = 0; b < i; b++)
        paramArrayOfint[b] = readInt(); 
      return;
    } 
    throw new RuntimeException("bad array lengths");
  }
  
  public final void writeLongArray(long[] paramArrayOflong) {
    if (paramArrayOflong != null) {
      int i = paramArrayOflong.length;
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeLong(paramArrayOflong[b]); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final long[] createLongArray() {
    int i = readInt();
    if (i >= 0 && i <= dataAvail() >> 3) {
      long[] arrayOfLong = new long[i];
      for (byte b = 0; b < i; b++)
        arrayOfLong[b] = readLong(); 
      return arrayOfLong;
    } 
    return null;
  }
  
  public final void readLongArray(long[] paramArrayOflong) {
    int i = readInt();
    if (i == paramArrayOflong.length) {
      for (byte b = 0; b < i; b++)
        paramArrayOflong[b] = readLong(); 
      return;
    } 
    throw new RuntimeException("bad array lengths");
  }
  
  public final void writeFloatArray(float[] paramArrayOffloat) {
    if (paramArrayOffloat != null) {
      int i = paramArrayOffloat.length;
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeFloat(paramArrayOffloat[b]); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final float[] createFloatArray() {
    int i = readInt();
    if (i >= 0 && i <= dataAvail() >> 2) {
      float[] arrayOfFloat = new float[i];
      for (byte b = 0; b < i; b++)
        arrayOfFloat[b] = readFloat(); 
      return arrayOfFloat;
    } 
    return null;
  }
  
  public final void readFloatArray(float[] paramArrayOffloat) {
    int i = readInt();
    if (i == paramArrayOffloat.length) {
      for (byte b = 0; b < i; b++)
        paramArrayOffloat[b] = readFloat(); 
      return;
    } 
    throw new RuntimeException("bad array lengths");
  }
  
  public final void writeDoubleArray(double[] paramArrayOfdouble) {
    if (paramArrayOfdouble != null) {
      int i = paramArrayOfdouble.length;
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeDouble(paramArrayOfdouble[b]); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final double[] createDoubleArray() {
    int i = readInt();
    if (i >= 0 && i <= dataAvail() >> 3) {
      double[] arrayOfDouble = new double[i];
      for (byte b = 0; b < i; b++)
        arrayOfDouble[b] = readDouble(); 
      return arrayOfDouble;
    } 
    return null;
  }
  
  public final void readDoubleArray(double[] paramArrayOfdouble) {
    int i = readInt();
    if (i == paramArrayOfdouble.length) {
      for (byte b = 0; b < i; b++)
        paramArrayOfdouble[b] = readDouble(); 
      return;
    } 
    throw new RuntimeException("bad array lengths");
  }
  
  public final void writeStringArray(String[] paramArrayOfString) {
    writeString16Array(paramArrayOfString);
  }
  
  public final String[] createStringArray() {
    return createString16Array();
  }
  
  public final void readStringArray(String[] paramArrayOfString) {
    readString16Array(paramArrayOfString);
  }
  
  public final void writeString8Array(String[] paramArrayOfString) {
    if (paramArrayOfString != null) {
      int i = paramArrayOfString.length;
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeString8(paramArrayOfString[b]); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final String[] createString8Array() {
    int i = readInt();
    if (i >= 0) {
      String[] arrayOfString = new String[i];
      for (byte b = 0; b < i; b++)
        arrayOfString[b] = readString8(); 
      return arrayOfString;
    } 
    return null;
  }
  
  public final void readString8Array(String[] paramArrayOfString) {
    int i = readInt();
    if (i == paramArrayOfString.length) {
      for (byte b = 0; b < i; b++)
        paramArrayOfString[b] = readString8(); 
      return;
    } 
    throw new RuntimeException("bad array lengths");
  }
  
  public final void writeString16Array(String[] paramArrayOfString) {
    if (paramArrayOfString != null) {
      int i = paramArrayOfString.length;
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeString16(paramArrayOfString[b]); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final String[] createString16Array() {
    int i = readInt();
    if (i >= 0) {
      String[] arrayOfString = new String[i];
      for (byte b = 0; b < i; b++)
        arrayOfString[b] = readString16(); 
      return arrayOfString;
    } 
    return null;
  }
  
  public final void readString16Array(String[] paramArrayOfString) {
    int i = readInt();
    if (i == paramArrayOfString.length) {
      for (byte b = 0; b < i; b++)
        paramArrayOfString[b] = readString16(); 
      return;
    } 
    throw new RuntimeException("bad array lengths");
  }
  
  public final void writeBinderArray(IBinder[] paramArrayOfIBinder) {
    if (paramArrayOfIBinder != null) {
      int i = paramArrayOfIBinder.length;
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeStrongBinder(paramArrayOfIBinder[b]); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final void writeCharSequenceArray(CharSequence[] paramArrayOfCharSequence) {
    if (paramArrayOfCharSequence != null) {
      int i = paramArrayOfCharSequence.length;
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeCharSequence(paramArrayOfCharSequence[b]); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final void writeCharSequenceList(ArrayList<CharSequence> paramArrayList) {
    if (paramArrayList != null) {
      int i = paramArrayList.size();
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeCharSequence(paramArrayList.get(b)); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final IBinder[] createBinderArray() {
    int i = readInt();
    if (i >= 0) {
      IBinder[] arrayOfIBinder = new IBinder[i];
      for (byte b = 0; b < i; b++)
        arrayOfIBinder[b] = readStrongBinder(); 
      return arrayOfIBinder;
    } 
    return null;
  }
  
  public final void readBinderArray(IBinder[] paramArrayOfIBinder) {
    int i = readInt();
    if (i == paramArrayOfIBinder.length) {
      for (byte b = 0; b < i; b++)
        paramArrayOfIBinder[b] = readStrongBinder(); 
      return;
    } 
    throw new RuntimeException("bad array lengths");
  }
  
  public final <T extends Parcelable> void writeTypedList(List<T> paramList) {
    writeTypedList(paramList, 0);
  }
  
  public final <T extends Parcelable> void writeTypedSparseArray(SparseArray<T> paramSparseArray, int paramInt) {
    if (paramSparseArray == null) {
      writeInt(-1);
      return;
    } 
    int i = paramSparseArray.size();
    writeInt(i);
    for (byte b = 0; b < i; b++) {
      writeInt(paramSparseArray.keyAt(b));
      writeTypedObject((Parcelable)paramSparseArray.valueAt(b), paramInt);
    } 
  }
  
  public <T extends Parcelable> void writeTypedList(List<T> paramList, int paramInt) {
    if (paramList == null) {
      writeInt(-1);
      return;
    } 
    int i = paramList.size();
    byte b = 0;
    writeInt(i);
    while (b < i) {
      writeTypedObject((Parcelable)paramList.get(b), paramInt);
      b++;
    } 
  }
  
  public final void writeStringList(List<String> paramList) {
    if (paramList == null) {
      writeInt(-1);
      return;
    } 
    int i = paramList.size();
    byte b = 0;
    writeInt(i);
    while (b < i) {
      writeString(paramList.get(b));
      b++;
    } 
  }
  
  public final void writeBinderList(List<IBinder> paramList) {
    if (paramList == null) {
      writeInt(-1);
      return;
    } 
    int i = paramList.size();
    byte b = 0;
    writeInt(i);
    while (b < i) {
      writeStrongBinder(paramList.get(b));
      b++;
    } 
  }
  
  public final <T extends Parcelable> void writeParcelableList(List<T> paramList, int paramInt) {
    if (paramList == null) {
      writeInt(-1);
      return;
    } 
    int i = paramList.size();
    byte b = 0;
    writeInt(i);
    while (b < i) {
      writeParcelable((Parcelable)paramList.get(b), paramInt);
      b++;
    } 
  }
  
  public final <T extends Parcelable> void writeTypedArray(T[] paramArrayOfT, int paramInt) {
    if (paramArrayOfT != null) {
      int i = paramArrayOfT.length;
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeTypedObject(paramArrayOfT[b], paramInt); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final <T extends Parcelable> void writeTypedObject(T paramT, int paramInt) {
    if (paramT != null) {
      writeInt(1);
      paramT.writeToParcel(this, paramInt);
    } else {
      writeInt(0);
    } 
  }
  
  public final void writeValue(Object paramObject) {
    if (paramObject == null) {
      writeInt(-1);
    } else if (paramObject instanceof String) {
      writeInt(0);
      writeString((String)paramObject);
    } else if (paramObject instanceof Integer) {
      writeInt(1);
      writeInt(((Integer)paramObject).intValue());
    } else if (paramObject instanceof Map) {
      writeInt(2);
      writeMap((Map)paramObject);
    } else if (paramObject instanceof Bundle) {
      writeInt(3);
      writeBundle((Bundle)paramObject);
    } else if (paramObject instanceof PersistableBundle) {
      writeInt(25);
      writePersistableBundle((PersistableBundle)paramObject);
    } else if (paramObject instanceof Parcelable) {
      writeInt(4);
      writeParcelable((Parcelable)paramObject, 0);
    } else if (paramObject instanceof Short) {
      writeInt(5);
      writeInt(((Short)paramObject).intValue());
    } else if (paramObject instanceof Long) {
      writeInt(6);
      writeLong(((Long)paramObject).longValue());
    } else if (paramObject instanceof Float) {
      writeInt(7);
      writeFloat(((Float)paramObject).floatValue());
    } else if (paramObject instanceof Double) {
      writeInt(8);
      writeDouble(((Double)paramObject).doubleValue());
    } else if (paramObject instanceof Boolean) {
      writeInt(9);
      writeInt(((Boolean)paramObject).booleanValue());
    } else if (paramObject instanceof CharSequence) {
      writeInt(10);
      writeCharSequence((CharSequence)paramObject);
    } else if (paramObject instanceof List) {
      writeInt(11);
      writeList((List)paramObject);
    } else if (paramObject instanceof SparseArray) {
      writeInt(12);
      writeSparseArray((SparseArray)paramObject);
    } else if (paramObject instanceof boolean[]) {
      writeInt(23);
      writeBooleanArray((boolean[])paramObject);
    } else if (paramObject instanceof byte[]) {
      writeInt(13);
      writeByteArray((byte[])paramObject);
    } else if (paramObject instanceof String[]) {
      writeInt(14);
      writeStringArray((String[])paramObject);
    } else if (paramObject instanceof CharSequence[]) {
      writeInt(24);
      writeCharSequenceArray((CharSequence[])paramObject);
    } else if (paramObject instanceof IBinder) {
      writeInt(15);
      writeStrongBinder((IBinder)paramObject);
    } else if (paramObject instanceof Parcelable[]) {
      writeInt(16);
      writeParcelableArray((Parcelable[])paramObject, 0);
    } else if (paramObject instanceof int[]) {
      writeInt(18);
      writeIntArray((int[])paramObject);
    } else if (paramObject instanceof long[]) {
      writeInt(19);
      writeLongArray((long[])paramObject);
    } else if (paramObject instanceof Byte) {
      writeInt(20);
      writeInt(((Byte)paramObject).byteValue());
    } else if (paramObject instanceof Size) {
      writeInt(26);
      writeSize((Size)paramObject);
    } else if (paramObject instanceof SizeF) {
      writeInt(27);
      writeSizeF((SizeF)paramObject);
    } else if (paramObject instanceof double[]) {
      writeInt(28);
      writeDoubleArray((double[])paramObject);
    } else {
      Class<?> clazz = paramObject.getClass();
      if (clazz.isArray() && clazz.getComponentType() == Object.class) {
        writeInt(17);
        writeArray((Object[])paramObject);
      } else {
        if (paramObject instanceof Serializable) {
          writeInt(21);
          writeSerializable((Serializable)paramObject);
          return;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Parcel: unable to marshal value ");
        stringBuilder.append(paramObject);
        throw new RuntimeException(stringBuilder.toString());
      } 
    } 
  }
  
  public final void writeParcelable(Parcelable paramParcelable, int paramInt) {
    if (paramParcelable == null) {
      writeString(null);
      return;
    } 
    writeParcelableCreator(paramParcelable);
    paramParcelable.writeToParcel(this, paramInt);
  }
  
  public final void writeParcelableCreator(Parcelable paramParcelable) {
    String str = paramParcelable.getClass().getName();
    writeString(str);
  }
  
  private void ensureWrittenSquashableParcelables() {
    if (this.mWrittenSquashableParcelables != null)
      return; 
    this.mWrittenSquashableParcelables = new ArrayMap();
  }
  
  private boolean mAllowSquashing = false;
  
  private static final boolean DEBUG_ARRAY_MAP = false;
  
  private static final boolean DEBUG_RECYCLE = false;
  
  private static final int EX_BAD_PARCELABLE = -2;
  
  public static final int EX_HAS_NOTED_APPOPS_REPLY_HEADER = -127;
  
  private static final int EX_HAS_STRICTMODE_REPLY_HEADER = -128;
  
  private static final int EX_ILLEGAL_ARGUMENT = -3;
  
  private static final int EX_ILLEGAL_STATE = -5;
  
  private static final int EX_NETWORK_MAIN_THREAD = -6;
  
  private static final int EX_NULL_POINTER = -4;
  
  private static final int EX_PARCELABLE = -9;
  
  private static final int EX_SECURITY = -1;
  
  private static final int EX_SERVICE_SPECIFIC = -8;
  
  private static final int EX_TRANSACTION_FAILED = -129;
  
  private static final int EX_UNSUPPORTED_OPERATION = -7;
  
  private static final int POOL_SIZE = 6;
  
  public static final Parcelable.Creator<String> STRING_CREATOR;
  
  private static final String TAG = "Parcel";
  
  private static final int VAL_BOOLEAN = 9;
  
  private static final int VAL_BOOLEANARRAY = 23;
  
  private static final int VAL_BUNDLE = 3;
  
  private static final int VAL_BYTE = 20;
  
  private static final int VAL_BYTEARRAY = 13;
  
  private static final int VAL_CHARSEQUENCE = 10;
  
  private static final int VAL_CHARSEQUENCEARRAY = 24;
  
  private static final int VAL_DOUBLE = 8;
  
  private static final int VAL_DOUBLEARRAY = 28;
  
  private static final int VAL_FLOAT = 7;
  
  private static final int VAL_IBINDER = 15;
  
  private static final int VAL_INTARRAY = 18;
  
  private static final int VAL_INTEGER = 1;
  
  private static final int VAL_LIST = 11;
  
  private static final int VAL_LONG = 6;
  
  private static final int VAL_LONGARRAY = 19;
  
  private static final int VAL_MAP = 2;
  
  private static final int VAL_NULL = -1;
  
  private static final int VAL_OBJECTARRAY = 17;
  
  private static final int VAL_PARCELABLE = 4;
  
  private static final int VAL_PARCELABLEARRAY = 16;
  
  private static final int VAL_PERSISTABLEBUNDLE = 25;
  
  private static final int VAL_SERIALIZABLE = 21;
  
  private static final int VAL_SHORT = 5;
  
  private static final int VAL_SIZE = 26;
  
  private static final int VAL_SIZEF = 27;
  
  private static final int VAL_SPARSEARRAY = 12;
  
  private static final int VAL_SPARSEBOOLEANARRAY = 22;
  
  private static final int VAL_STRING = 0;
  
  private static final int VAL_STRINGARRAY = 14;
  
  private static final int WRITE_EXCEPTION_STACK_TRACE_THRESHOLD_MS = 1000;
  
  private static final HashMap<ClassLoader, HashMap<String, Parcelable.Creator<?>>> mCreators;
  
  private static final Parcel[] sHolderPool;
  
  private static volatile long sLastWriteExceptionStackTrace;
  
  private static boolean sParcelExceptionStackTrace;
  
  private ArrayMap<Class, Object> mClassCookies;
  
  private long mNativePtr;
  
  private long mNativeSize;
  
  private boolean mOwnsNativeParcelObject;
  
  private ArrayMap<Integer, Parcelable> mReadSquashableParcelables;
  
  private RuntimeException mStack;
  
  private ArrayMap<Parcelable, Integer> mWrittenSquashableParcelables;
  
  public boolean allowSquashing() {
    boolean bool = this.mAllowSquashing;
    this.mAllowSquashing = true;
    return bool;
  }
  
  public void restoreAllowSquashing(boolean paramBoolean) {
    this.mAllowSquashing = paramBoolean;
    if (!paramBoolean)
      this.mWrittenSquashableParcelables = null; 
  }
  
  private void resetSqaushingState() {
    if (this.mAllowSquashing)
      Slog.wtf("Parcel", "allowSquashing wasn't restored."); 
    this.mWrittenSquashableParcelables = null;
    this.mReadSquashableParcelables = null;
    this.mAllowSquashing = false;
  }
  
  private void ensureReadSquashableParcelables() {
    if (this.mReadSquashableParcelables != null)
      return; 
    this.mReadSquashableParcelables = new ArrayMap();
  }
  
  public boolean maybeWriteSquashed(Parcelable paramParcelable) {
    if (!this.mAllowSquashing) {
      writeInt(0);
      return false;
    } 
    ensureWrittenSquashableParcelables();
    Integer integer = (Integer)this.mWrittenSquashableParcelables.get(paramParcelable);
    if (integer != null) {
      int j = dataPosition();
      writeInt(j - integer.intValue() + 4);
      return true;
    } 
    writeInt(0);
    int i = dataPosition();
    this.mWrittenSquashableParcelables.put(paramParcelable, Integer.valueOf(i));
    return false;
  }
  
  public <T extends Parcelable> T readSquashed(SquashReadHelper<T> paramSquashReadHelper) {
    int i = readInt();
    int j = dataPosition();
    if (i == 0) {
      Parcelable parcelable1 = (Parcelable)paramSquashReadHelper.readRawParceled(this);
      ensureReadSquashableParcelables();
      this.mReadSquashableParcelables.put(Integer.valueOf(j), parcelable1);
      return (T)parcelable1;
    } 
    i = j - i;
    Parcelable parcelable = (Parcelable)this.mReadSquashableParcelables.get(Integer.valueOf(i));
    if (parcelable == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Map doesn't contain offset ");
      stringBuilder.append(i);
      stringBuilder.append(" : contains=");
      ArrayMap<Integer, Parcelable> arrayMap = this.mReadSquashableParcelables;
      stringBuilder.append(new ArrayList(arrayMap.keySet()));
      String str = stringBuilder.toString();
      Slog.wtfStack("Parcel", str);
    } 
    return (T)parcelable;
  }
  
  public final void writeSerializable(Serializable paramSerializable) {
    if (paramSerializable == null) {
      writeString(null);
      return;
    } 
    String str = paramSerializable.getClass().getName();
    writeString(str);
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    try {
      ObjectOutputStream objectOutputStream = new ObjectOutputStream();
      this(byteArrayOutputStream);
      objectOutputStream.writeObject(paramSerializable);
      objectOutputStream.close();
      writeByteArray(byteArrayOutputStream.toByteArray());
      return;
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Parcelable encountered IOException writing serializable object (name = ");
      stringBuilder.append(str);
      stringBuilder.append(")");
      throw new RuntimeException(stringBuilder.toString(), iOException);
    } 
  }
  
  public static void setStackTraceParceling(boolean paramBoolean) {
    sParcelExceptionStackTrace = paramBoolean;
  }
  
  public final void writeException(Exception paramException) {
    long l;
    AppOpsManager.prefixParcelWithAppOpsIfNeeded(this);
    int i = getExceptionCode(paramException);
    writeInt(i);
    StrictMode.clearGatheredViolations();
    if (i == 0) {
      if (paramException instanceof RuntimeException)
        throw (RuntimeException)paramException; 
      throw new RuntimeException(paramException);
    } 
    writeString(paramException.getMessage());
    if (sParcelExceptionStackTrace) {
      l = SystemClock.elapsedRealtime();
    } else {
      l = 0L;
    } 
    if (sParcelExceptionStackTrace && l - sLastWriteExceptionStackTrace > 1000L) {
      sLastWriteExceptionStackTrace = l;
      writeStackTrace(paramException);
    } else {
      writeInt(0);
    } 
    if (i != -9) {
      if (i == -8)
        writeInt(((ServiceSpecificException)paramException).errorCode); 
    } else {
      int j = dataPosition();
      writeInt(0);
      writeParcelable((Parcelable)paramException, 1);
      i = dataPosition();
      setDataPosition(j);
      writeInt(i - j);
      setDataPosition(i);
    } 
  }
  
  public static int getExceptionCode(Throwable paramThrowable) {
    byte b = 0;
    if (paramThrowable instanceof Parcelable && paramThrowable.getClass().getClassLoader() == Parcelable.class.getClassLoader()) {
      b = -9;
    } else if (paramThrowable instanceof SecurityException) {
      b = -1;
    } else if (paramThrowable instanceof BadParcelableException) {
      b = -2;
    } else if (paramThrowable instanceof IllegalArgumentException) {
      b = -3;
    } else if (paramThrowable instanceof NullPointerException) {
      b = -4;
    } else if (paramThrowable instanceof IllegalStateException) {
      b = -5;
    } else if (paramThrowable instanceof NetworkOnMainThreadException) {
      b = -6;
    } else if (paramThrowable instanceof UnsupportedOperationException) {
      b = -7;
    } else if (paramThrowable instanceof ServiceSpecificException) {
      b = -8;
    } 
    return b;
  }
  
  public void writeStackTrace(Throwable paramThrowable) {
    int i = dataPosition();
    writeInt(0);
    StackTraceElement[] arrayOfStackTraceElement = paramThrowable.getStackTrace();
    int j = Math.min(arrayOfStackTraceElement.length, 5);
    StringBuilder stringBuilder = new StringBuilder();
    int k;
    for (k = 0; k < j; k++) {
      stringBuilder.append("\tat ");
      stringBuilder.append(arrayOfStackTraceElement[k]);
      stringBuilder.append('\n');
    } 
    writeString(stringBuilder.toString());
    k = dataPosition();
    setDataPosition(i);
    writeInt(k - i);
    setDataPosition(k);
  }
  
  public final void writeNoException() {
    AppOpsManager.prefixParcelWithAppOpsIfNeeded(this);
    if (StrictMode.hasGatheredViolations()) {
      writeInt(-128);
      int i = dataPosition();
      writeInt(0);
      StrictMode.writeGatheredViolationsToParcel(this);
      int j = dataPosition();
      setDataPosition(i);
      writeInt(j - i);
      setDataPosition(j);
    } else {
      writeInt(0);
    } 
  }
  
  public final void readException() {
    int i = readExceptionCode();
    if (i != 0) {
      String str = readString();
      readException(i, str);
    } 
  }
  
  public final int readExceptionCode() {
    int i = readInt();
    int j = i;
    if (i == -127) {
      AppOpsManager.readAndLogNotedAppops(this);
      j = readInt();
    } 
    if (j == -128) {
      j = readInt();
      if (j == 0) {
        Log.e("Parcel", "Unexpected zero-sized Parcel reply header.");
      } else {
        StrictMode.readAndHandleBinderCallViolations(this);
      } 
      return 0;
    } 
    return j;
  }
  
  public final void readException(int paramInt, String paramString) {
    String str = null;
    int i = readInt();
    if (i > 0)
      str = readString(); 
    Exception exception = createException(paramInt, paramString);
    if (str != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Remote stack trace:\n");
      stringBuilder.append(str);
      RemoteException remoteException = new RemoteException(stringBuilder.toString(), null, false, false);
      ExceptionUtils.appendCause(exception, (Throwable)remoteException);
    } 
    SneakyThrow.sneakyThrow(exception);
  }
  
  private Exception createException(int paramInt, String paramString) {
    Exception exception1, exception2 = createExceptionOrNull(paramInt, paramString);
    if (exception2 != null) {
      exception1 = exception2;
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown exception code: ");
      stringBuilder.append(paramInt);
      stringBuilder.append(" msg ");
      stringBuilder.append((String)exception1);
      exception1 = new RuntimeException(stringBuilder.toString());
    } 
    return exception1;
  }
  
  public Exception createExceptionOrNull(int paramInt, String paramString) {
    switch (paramInt) {
      default:
        return null;
      case -1:
        return new SecurityException(paramString);
      case -2:
        return (Exception)new BadParcelableException(paramString);
      case -3:
        return new IllegalArgumentException(paramString);
      case -4:
        return new NullPointerException(paramString);
      case -5:
        return new IllegalStateException(paramString);
      case -6:
        return new NetworkOnMainThreadException();
      case -7:
        return new UnsupportedOperationException(paramString);
      case -8:
        return new ServiceSpecificException(readInt(), paramString);
      case -9:
        break;
    } 
    if (readInt() > 0)
      return readParcelable(Parcelable.class.getClassLoader()); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(" [missing Parcelable]");
    return new RuntimeException(stringBuilder.toString());
  }
  
  public final int readInt() {
    return nativeReadInt(this.mNativePtr);
  }
  
  public final long readLong() {
    return nativeReadLong(this.mNativePtr);
  }
  
  public final float readFloat() {
    return nativeReadFloat(this.mNativePtr);
  }
  
  public final double readDouble() {
    return nativeReadDouble(this.mNativePtr);
  }
  
  public final String readString() {
    return readString16();
  }
  
  public final String readString8() {
    return this.mReadWriteHelper.readString8(this);
  }
  
  public final String readString16() {
    return this.mReadWriteHelper.readString16(this);
  }
  
  public String readStringNoHelper() {
    return readString16NoHelper();
  }
  
  public String readString8NoHelper() {
    return nativeReadString8(this.mNativePtr);
  }
  
  public String readString16NoHelper() {
    return nativeReadString16(this.mNativePtr);
  }
  
  public final boolean readBoolean() {
    boolean bool;
    if (readInt() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final CharSequence readCharSequence() {
    return TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(this);
  }
  
  public final IBinder readStrongBinder() {
    return nativeReadStrongBinder(this.mNativePtr);
  }
  
  public final ParcelFileDescriptor readFileDescriptor() {
    FileDescriptor fileDescriptor = nativeReadFileDescriptor(this.mNativePtr);
    if (fileDescriptor != null) {
      ParcelFileDescriptor parcelFileDescriptor = new ParcelFileDescriptor(fileDescriptor);
    } else {
      fileDescriptor = null;
    } 
    return (ParcelFileDescriptor)fileDescriptor;
  }
  
  public final FileDescriptor readRawFileDescriptor() {
    return nativeReadFileDescriptor(this.mNativePtr);
  }
  
  public final FileDescriptor[] createRawFileDescriptorArray() {
    int i = readInt();
    if (i < 0)
      return null; 
    FileDescriptor[] arrayOfFileDescriptor = new FileDescriptor[i];
    for (byte b = 0; b < i; b++)
      arrayOfFileDescriptor[b] = readRawFileDescriptor(); 
    return arrayOfFileDescriptor;
  }
  
  public final void readRawFileDescriptorArray(FileDescriptor[] paramArrayOfFileDescriptor) {
    int i = readInt();
    if (i == paramArrayOfFileDescriptor.length) {
      for (byte b = 0; b < i; b++)
        paramArrayOfFileDescriptor[b] = readRawFileDescriptor(); 
      return;
    } 
    throw new RuntimeException("bad array lengths");
  }
  
  public final byte readByte() {
    return (byte)(readInt() & 0xFF);
  }
  
  public final void readMap(Map paramMap, ClassLoader paramClassLoader) {
    int i = readInt();
    readMapInternal(paramMap, i, paramClassLoader);
  }
  
  public final void readList(List paramList, ClassLoader paramClassLoader) {
    int i = readInt();
    readListInternal(paramList, i, paramClassLoader);
  }
  
  public final HashMap readHashMap(ClassLoader paramClassLoader) {
    int i = readInt();
    if (i < 0)
      return null; 
    HashMap<Object, Object> hashMap = new HashMap<>(i);
    readMapInternal(hashMap, i, paramClassLoader);
    return hashMap;
  }
  
  public final Bundle readBundle() {
    return readBundle(null);
  }
  
  public final Bundle readBundle(ClassLoader paramClassLoader) {
    int i = readInt();
    if (i < 0)
      return null; 
    Bundle bundle = new Bundle(this, i);
    if (paramClassLoader != null)
      bundle.setClassLoader(paramClassLoader); 
    return bundle;
  }
  
  public final PersistableBundle readPersistableBundle() {
    return readPersistableBundle(null);
  }
  
  public final PersistableBundle readPersistableBundle(ClassLoader paramClassLoader) {
    int i = readInt();
    if (i < 0)
      return null; 
    PersistableBundle persistableBundle = new PersistableBundle(this, i);
    if (paramClassLoader != null)
      persistableBundle.setClassLoader(paramClassLoader); 
    return persistableBundle;
  }
  
  public final Size readSize() {
    int i = readInt();
    int j = readInt();
    return new Size(i, j);
  }
  
  public final SizeF readSizeF() {
    float f1 = readFloat();
    float f2 = readFloat();
    return new SizeF(f1, f2);
  }
  
  public final byte[] createByteArray() {
    return nativeCreateByteArray(this.mNativePtr);
  }
  
  public final void readByteArray(byte[] paramArrayOfbyte) {
    boolean bool;
    long l = this.mNativePtr;
    if (paramArrayOfbyte != null) {
      bool = paramArrayOfbyte.length;
    } else {
      bool = false;
    } 
    boolean bool1 = nativeReadByteArray(l, paramArrayOfbyte, bool);
    if (bool1)
      return; 
    throw new RuntimeException("bad array lengths");
  }
  
  public final byte[] readBlob() {
    return nativeReadBlob(this.mNativePtr);
  }
  
  public final String[] readStringArray() {
    return createString16Array();
  }
  
  public final CharSequence[] readCharSequenceArray() {
    CharSequence[] arrayOfCharSequence = null;
    int i = readInt();
    if (i >= 0) {
      CharSequence[] arrayOfCharSequence1 = new CharSequence[i];
      byte b = 0;
      while (true) {
        arrayOfCharSequence = arrayOfCharSequence1;
        if (b < i) {
          arrayOfCharSequence1[b] = readCharSequence();
          b++;
          continue;
        } 
        break;
      } 
    } 
    return arrayOfCharSequence;
  }
  
  public final ArrayList<CharSequence> readCharSequenceList() {
    ArrayList<CharSequence> arrayList = null;
    int i = readInt();
    if (i >= 0) {
      ArrayList<CharSequence> arrayList1 = new ArrayList(i);
      byte b = 0;
      while (true) {
        arrayList = arrayList1;
        if (b < i) {
          arrayList1.add(readCharSequence());
          b++;
          continue;
        } 
        break;
      } 
    } 
    return arrayList;
  }
  
  public final ArrayList readArrayList(ClassLoader paramClassLoader) {
    int i = readInt();
    if (i < 0)
      return null; 
    ArrayList arrayList = new ArrayList(i);
    readListInternal(arrayList, i, paramClassLoader);
    return arrayList;
  }
  
  public final Object[] readArray(ClassLoader paramClassLoader) {
    int i = readInt();
    if (i < 0)
      return null; 
    Object[] arrayOfObject = new Object[i];
    readArrayInternal(arrayOfObject, i, paramClassLoader);
    return arrayOfObject;
  }
  
  public final <T> SparseArray<T> readSparseArray(ClassLoader paramClassLoader) {
    int i = readInt();
    if (i < 0)
      return null; 
    SparseArray<T> sparseArray = new SparseArray(i);
    readSparseArrayInternal(sparseArray, i, paramClassLoader);
    return sparseArray;
  }
  
  public final SparseBooleanArray readSparseBooleanArray() {
    int i = readInt();
    if (i < 0)
      return null; 
    SparseBooleanArray sparseBooleanArray = new SparseBooleanArray(i);
    readSparseBooleanArrayInternal(sparseBooleanArray, i);
    return sparseBooleanArray;
  }
  
  public final SparseIntArray readSparseIntArray() {
    int i = readInt();
    if (i < 0)
      return null; 
    SparseIntArray sparseIntArray = new SparseIntArray(i);
    readSparseIntArrayInternal(sparseIntArray, i);
    return sparseIntArray;
  }
  
  public final <T> ArrayList<T> createTypedArrayList(Parcelable.Creator<T> paramCreator) {
    int i = readInt();
    if (i < 0)
      return null; 
    ArrayList<T> arrayList = new ArrayList(i);
    while (i > 0) {
      arrayList.add(readTypedObject(paramCreator));
      i--;
    } 
    return arrayList;
  }
  
  public final <T> void readTypedList(List<T> paramList, Parcelable.Creator<T> paramCreator) {
    byte b2;
    int i = paramList.size();
    int j = readInt();
    byte b1 = 0;
    while (true) {
      b2 = b1;
      if (b1 < i) {
        b2 = b1;
        if (b1 < j) {
          paramList.set(b1, readTypedObject(paramCreator));
          b1++;
          continue;
        } 
      } 
      break;
    } 
    while (true) {
      b1 = b2;
      if (b2 < j) {
        paramList.add(readTypedObject(paramCreator));
        b2++;
        continue;
      } 
      break;
    } 
    for (; b1 < i; b1++)
      paramList.remove(j); 
  }
  
  public final <T extends Parcelable> SparseArray<T> createTypedSparseArray(Parcelable.Creator<T> paramCreator) {
    int i = readInt();
    if (i < 0)
      return null; 
    SparseArray<T> sparseArray = new SparseArray(i);
    for (byte b = 0; b < i; b++) {
      int j = readInt();
      Parcelable parcelable = readTypedObject(paramCreator);
      sparseArray.append(j, parcelable);
    } 
    return sparseArray;
  }
  
  public final <T extends Parcelable> ArrayMap<String, T> createTypedArrayMap(Parcelable.Creator<T> paramCreator) {
    int i = readInt();
    if (i < 0)
      return null; 
    ArrayMap<String, T> arrayMap = new ArrayMap(i);
    for (byte b = 0; b < i; b++) {
      String str = readString();
      Parcelable parcelable = readTypedObject(paramCreator);
      arrayMap.append(str, parcelable);
    } 
    return arrayMap;
  }
  
  public final ArrayList<String> createStringArrayList() {
    int i = readInt();
    if (i < 0)
      return null; 
    ArrayList<String> arrayList = new ArrayList(i);
    while (i > 0) {
      arrayList.add(readString());
      i--;
    } 
    return arrayList;
  }
  
  public final ArrayList<IBinder> createBinderArrayList() {
    int i = readInt();
    if (i < 0)
      return null; 
    ArrayList<IBinder> arrayList = new ArrayList(i);
    while (i > 0) {
      arrayList.add(readStrongBinder());
      i--;
    } 
    return arrayList;
  }
  
  public final void readStringList(List<String> paramList) {
    byte b2;
    int i = paramList.size();
    int j = readInt();
    byte b1 = 0;
    while (true) {
      b2 = b1;
      if (b1 < i) {
        b2 = b1;
        if (b1 < j) {
          paramList.set(b1, readString());
          b1++;
          continue;
        } 
      } 
      break;
    } 
    while (true) {
      b1 = b2;
      if (b2 < j) {
        paramList.add(readString());
        b2++;
        continue;
      } 
      break;
    } 
    for (; b1 < i; b1++)
      paramList.remove(j); 
  }
  
  public final void readBinderList(List<IBinder> paramList) {
    byte b2;
    int i = paramList.size();
    int j = readInt();
    byte b1 = 0;
    while (true) {
      b2 = b1;
      if (b1 < i) {
        b2 = b1;
        if (b1 < j) {
          paramList.set(b1, readStrongBinder());
          b1++;
          continue;
        } 
      } 
      break;
    } 
    while (true) {
      b1 = b2;
      if (b2 < j) {
        paramList.add(readStrongBinder());
        b2++;
        continue;
      } 
      break;
    } 
    for (; b1 < i; b1++)
      paramList.remove(j); 
  }
  
  public final <T extends Parcelable> List<T> readParcelableList(List<T> paramList, ClassLoader paramClassLoader) {
    byte b2;
    int i = readInt();
    if (i == -1) {
      paramList.clear();
      return paramList;
    } 
    int j = paramList.size();
    byte b1 = 0;
    while (true) {
      b2 = b1;
      if (b1 < j) {
        b2 = b1;
        if (b1 < i) {
          paramList.set(b1, readParcelable(paramClassLoader));
          b1++;
          continue;
        } 
      } 
      break;
    } 
    while (true) {
      b1 = b2;
      if (b2 < i) {
        paramList.add(readParcelable(paramClassLoader));
        b2++;
        continue;
      } 
      break;
    } 
    for (; b1 < j; b1++)
      paramList.remove(i); 
    return paramList;
  }
  
  public final <T> T[] createTypedArray(Parcelable.Creator<T> paramCreator) {
    int i = readInt();
    if (i < 0)
      return null; 
    T[] arrayOfT = paramCreator.newArray(i);
    for (byte b = 0; b < i; b++)
      arrayOfT[b] = readTypedObject(paramCreator); 
    return arrayOfT;
  }
  
  public final <T> void readTypedArray(T[] paramArrayOfT, Parcelable.Creator<T> paramCreator) {
    int i = readInt();
    if (i == paramArrayOfT.length) {
      for (byte b = 0; b < i; b++)
        paramArrayOfT[b] = readTypedObject(paramCreator); 
      return;
    } 
    throw new RuntimeException("bad array lengths");
  }
  
  @Deprecated
  public final <T> T[] readTypedArray(Parcelable.Creator<T> paramCreator) {
    return createTypedArray(paramCreator);
  }
  
  public final <T> T readTypedObject(Parcelable.Creator<T> paramCreator) {
    if (readInt() != 0)
      return paramCreator.createFromParcel(this); 
    return null;
  }
  
  public final <T extends Parcelable> void writeParcelableArray(T[] paramArrayOfT, int paramInt) {
    if (paramArrayOfT != null) {
      int i = paramArrayOfT.length;
      writeInt(i);
      for (byte b = 0; b < i; b++)
        writeParcelable((Parcelable)paramArrayOfT[b], paramInt); 
    } else {
      writeInt(-1);
    } 
  }
  
  public final Object readValue(ClassLoader paramClassLoader) {
    StringBuilder stringBuilder;
    int j;
    boolean bool;
    int i = readInt();
    switch (i) {
      default:
        j = dataPosition();
        stringBuilder = new StringBuilder();
        stringBuilder.append("Parcel ");
        stringBuilder.append(this);
        stringBuilder.append(": Unmarshalling unknown type code ");
        stringBuilder.append(i);
        stringBuilder.append(" at offset ");
        stringBuilder.append(j - 4);
        throw new RuntimeException(stringBuilder.toString());
      case 28:
        return createDoubleArray();
      case 27:
        return readSizeF();
      case 26:
        return readSize();
      case 25:
        return readPersistableBundle((ClassLoader)stringBuilder);
      case 24:
        return readCharSequenceArray();
      case 23:
        return createBooleanArray();
      case 22:
        return readSparseBooleanArray();
      case 21:
        return readSerializable((ClassLoader)stringBuilder);
      case 20:
        return Byte.valueOf(readByte());
      case 19:
        return createLongArray();
      case 18:
        return createIntArray();
      case 17:
        return readArray((ClassLoader)stringBuilder);
      case 16:
        return readParcelableArray((ClassLoader)stringBuilder);
      case 15:
        return readStrongBinder();
      case 14:
        return readStringArray();
      case 13:
        return createByteArray();
      case 12:
        return readSparseArray((ClassLoader)stringBuilder);
      case 11:
        return readArrayList((ClassLoader)stringBuilder);
      case 10:
        return readCharSequence();
      case 9:
        i = readInt();
        bool = true;
        if (i != 1)
          bool = false; 
        return Boolean.valueOf(bool);
      case 8:
        return Double.valueOf(readDouble());
      case 7:
        return Float.valueOf(readFloat());
      case 6:
        return Long.valueOf(readLong());
      case 5:
        return Short.valueOf((short)readInt());
      case 4:
        return readParcelable((ClassLoader)stringBuilder);
      case 3:
        return readBundle((ClassLoader)stringBuilder);
      case 2:
        return readHashMap((ClassLoader)stringBuilder);
      case 1:
        return Integer.valueOf(readInt());
      case 0:
        return readString();
      case -1:
        break;
    } 
    return null;
  }
  
  public final <T extends Parcelable> T readParcelable(ClassLoader paramClassLoader) {
    Parcelable.Creator<?> creator = readParcelableCreator(paramClassLoader);
    if (creator == null)
      return null; 
    if (creator instanceof Parcelable.ClassLoaderCreator) {
      creator = creator;
      return (T)creator.createFromParcel(this, paramClassLoader);
    } 
    return (T)creator.createFromParcel(this);
  }
  
  public final <T extends Parcelable> T readCreator(Parcelable.Creator<?> paramCreator, ClassLoader paramClassLoader) {
    if (paramCreator instanceof Parcelable.ClassLoaderCreator) {
      paramCreator = paramCreator;
      return (T)paramCreator.createFromParcel(this, paramClassLoader);
    } 
    return (T)paramCreator.createFromParcel(this);
  }
  
  public final Parcelable.Creator<?> readParcelableCreator(ClassLoader paramClassLoader) {
    String str = readString();
    if (str == null)
      return null; 
    synchronized (mCreators) {
      HashMap<Object, Object> hashMap1 = (HashMap)mCreators.get(paramClassLoader);
      HashMap<Object, Object> hashMap2 = hashMap1;
      if (hashMap1 == null) {
        hashMap2 = new HashMap<>();
        this();
        mCreators.put(paramClassLoader, hashMap2);
      } 
      Parcelable.Creator<?> creator = (Parcelable.Creator)hashMap2.get(str);
      if (creator != null)
        return creator; 
      if (paramClassLoader == null)
        try {
          paramClassLoader = getClass().getClassLoader();
        } catch (IllegalAccessException illegalAccessException) {
        
        } catch (ClassNotFoundException classNotFoundException) {
        
        } catch (NoSuchFieldException noSuchFieldException) {} 
      Class<?> clazz = Class.forName(str, false, (ClassLoader)noSuchFieldException);
      if (Parcelable.class.isAssignableFrom(clazz)) {
        Field field = clazz.getField("CREATOR");
        if ((field.getModifiers() & 0x8) != 0) {
          Class<?> clazz1 = field.getType();
          if (Parcelable.Creator.class.isAssignableFrom(clazz1)) {
            Parcelable.Creator<?> creator1 = (Parcelable.Creator)field.get(null);
            if (creator1 != null)
              synchronized (mCreators) {
                classNotFoundException.put(str, creator1);
                return creator1;
              }  
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append("Parcelable protocol requires a non-null Parcelable.Creator object called CREATOR on class ");
            stringBuilder3.append(str);
            throw new BadParcelableException(stringBuilder3.toString());
          } 
          BadParcelableException badParcelableException2 = new BadParcelableException();
          StringBuilder stringBuilder2 = new StringBuilder();
          this();
          stringBuilder2.append("Parcelable protocol requires a Parcelable.Creator object called CREATOR on class ");
          stringBuilder2.append(str);
          this(stringBuilder2.toString());
          throw badParcelableException2;
        } 
        BadParcelableException badParcelableException1 = new BadParcelableException();
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Parcelable protocol requires the CREATOR object to be static on class ");
        stringBuilder1.append(str);
        this(stringBuilder1.toString());
        throw badParcelableException1;
      } 
      BadParcelableException badParcelableException = new BadParcelableException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Parcelable protocol requires subclassing from Parcelable on class ");
      stringBuilder.append(str);
      this(stringBuilder.toString());
      throw badParcelableException;
    } 
  }
  
  public final Parcelable[] readParcelableArray(ClassLoader paramClassLoader) {
    int i = readInt();
    if (i < 0)
      return null; 
    Parcelable[] arrayOfParcelable = new Parcelable[i];
    for (byte b = 0; b < i; b++)
      arrayOfParcelable[b] = readParcelable(paramClassLoader); 
    return arrayOfParcelable;
  }
  
  public final <T extends Parcelable> T[] readParcelableArray(ClassLoader paramClassLoader, Class<T> paramClass) {
    int i = readInt();
    if (i < 0)
      return null; 
    Parcelable[] arrayOfParcelable = (Parcelable[])Array.newInstance(paramClass, i);
    for (byte b = 0; b < i; b++)
      arrayOfParcelable[b] = readParcelable(paramClassLoader); 
    return (T[])arrayOfParcelable;
  }
  
  public final Serializable readSerializable() {
    return readSerializable(null);
  }
  
  private final Serializable readSerializable(ClassLoader paramClassLoader) {
    String str = readString();
    if (str == null)
      return null; 
    byte[] arrayOfByte = createByteArray();
    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(arrayOfByte);
    try {
      ObjectInputStream objectInputStream = new ObjectInputStream() {
          final Parcel this$0;
          
          final ClassLoader val$loader;
          
          protected Class<?> resolveClass(ObjectStreamClass param1ObjectStreamClass) throws IOException, ClassNotFoundException {
            if (loader != null) {
              Class<?> clazz = Class.forName(param1ObjectStreamClass.getName(), false, loader);
              if (clazz != null)
                return clazz; 
            } 
            return super.resolveClass(param1ObjectStreamClass);
          }
        };
      super(this, byteArrayInputStream, paramClassLoader);
      return (Serializable)objectInputStream.readObject();
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Parcelable encountered IOException reading a Serializable object (name = ");
      stringBuilder.append(str);
      stringBuilder.append(")");
      throw new RuntimeException(stringBuilder.toString(), iOException);
    } catch (ClassNotFoundException classNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Parcelable encountered ClassNotFoundException reading a Serializable object (name = ");
      stringBuilder.append(str);
      stringBuilder.append(")");
      throw new RuntimeException(stringBuilder.toString(), classNotFoundException);
    } 
  }
  
  protected static final Parcel obtain(int paramInt) {
    throw new UnsupportedOperationException();
  }
  
  protected static final Parcel obtain(long paramLong) {
    // Byte code:
    //   0: getstatic android/os/Parcel.sHolderPool : [Landroid/os/Parcel;
    //   3: astore_2
    //   4: aload_2
    //   5: monitorenter
    //   6: iconst_0
    //   7: istore_3
    //   8: iload_3
    //   9: bipush #6
    //   11: if_icmpge -> 45
    //   14: aload_2
    //   15: iload_3
    //   16: aaload
    //   17: astore #4
    //   19: aload #4
    //   21: ifnull -> 39
    //   24: aload_2
    //   25: iload_3
    //   26: aconst_null
    //   27: aastore
    //   28: aload #4
    //   30: lload_0
    //   31: invokespecial init : (J)V
    //   34: aload_2
    //   35: monitorexit
    //   36: aload #4
    //   38: areturn
    //   39: iinc #3, 1
    //   42: goto -> 8
    //   45: aload_2
    //   46: monitorexit
    //   47: new android/os/Parcel
    //   50: dup
    //   51: lload_0
    //   52: invokespecial <init> : (J)V
    //   55: areturn
    //   56: astore #4
    //   58: aload_2
    //   59: monitorexit
    //   60: aload #4
    //   62: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3497	-> 0
    //   #3498	-> 4
    //   #3500	-> 6
    //   #3501	-> 14
    //   #3502	-> 19
    //   #3503	-> 24
    //   #3507	-> 28
    //   #3508	-> 34
    //   #3500	-> 39
    //   #3511	-> 45
    //   #3512	-> 47
    //   #3511	-> 56
    // Exception table:
    //   from	to	target	type
    //   28	34	56	finally
    //   34	36	56	finally
    //   45	47	56	finally
    //   58	60	56	finally
  }
  
  private Parcel(long paramLong) {
    init(paramLong);
  }
  
  private void init(long paramLong) {
    if (paramLong != 0L) {
      this.mNativePtr = paramLong;
      this.mOwnsNativeParcelObject = false;
    } else {
      this.mNativePtr = nativeCreate();
      this.mOwnsNativeParcelObject = true;
    } 
  }
  
  private void freeBuffer() {
    resetSqaushingState();
    if (this.mOwnsNativeParcelObject)
      updateNativeSize(nativeFreeBuffer(this.mNativePtr)); 
    this.mReadWriteHelper = ReadWriteHelper.DEFAULT;
  }
  
  private void destroy() {
    resetSqaushingState();
    long l = this.mNativePtr;
    if (l != 0L) {
      if (this.mOwnsNativeParcelObject) {
        nativeDestroy(l);
        updateNativeSize(0L);
      } 
      this.mNativePtr = 0L;
    } 
  }
  
  protected void finalize() throws Throwable {
    destroy();
  }
  
  void readMapInternal(Map<Object, Object> paramMap, int paramInt, ClassLoader paramClassLoader) {
    while (paramInt > 0) {
      Object object1 = readValue(paramClassLoader);
      Object object2 = readValue(paramClassLoader);
      paramMap.put(object1, object2);
      paramInt--;
    } 
  }
  
  void readArrayMapInternal(ArrayMap paramArrayMap, int paramInt, ClassLoader paramClassLoader) {
    while (paramInt > 0) {
      String str = readString();
      Object object = readValue(paramClassLoader);
      paramArrayMap.append(str, object);
      paramInt--;
    } 
    paramArrayMap.validate();
  }
  
  void readArrayMapSafelyInternal(ArrayMap paramArrayMap, int paramInt, ClassLoader paramClassLoader) {
    while (paramInt > 0) {
      String str = readString();
      Object object = readValue(paramClassLoader);
      paramArrayMap.put(str, object);
      paramInt--;
    } 
  }
  
  public void readArrayMap(ArrayMap paramArrayMap, ClassLoader paramClassLoader) {
    int i = readInt();
    if (i < 0)
      return; 
    readArrayMapInternal(paramArrayMap, i, paramClassLoader);
  }
  
  public ArraySet<? extends Object> readArraySet(ClassLoader paramClassLoader) {
    int i = readInt();
    if (i < 0)
      return null; 
    ArraySet<? extends Object> arraySet = new ArraySet(i);
    for (byte b = 0; b < i; b++) {
      Object object = readValue(paramClassLoader);
      arraySet.append(object);
    } 
    return arraySet;
  }
  
  private void readListInternal(List<Object> paramList, int paramInt, ClassLoader paramClassLoader) {
    while (paramInt > 0) {
      Object object = readValue(paramClassLoader);
      paramList.add(object);
      paramInt--;
    } 
  }
  
  private void readArrayInternal(Object[] paramArrayOfObject, int paramInt, ClassLoader paramClassLoader) {
    for (byte b = 0; b < paramInt; b++) {
      Object object = readValue(paramClassLoader);
      paramArrayOfObject[b] = object;
    } 
  }
  
  private void readSparseArrayInternal(SparseArray paramSparseArray, int paramInt, ClassLoader paramClassLoader) {
    while (paramInt > 0) {
      int i = readInt();
      Object object = readValue(paramClassLoader);
      paramSparseArray.append(i, object);
      paramInt--;
    } 
  }
  
  private void readSparseBooleanArrayInternal(SparseBooleanArray paramSparseBooleanArray, int paramInt) {
    while (paramInt > 0) {
      int i = readInt();
      byte b = readByte();
      boolean bool = true;
      if (b != 1)
        bool = false; 
      paramSparseBooleanArray.append(i, bool);
      paramInt--;
    } 
  }
  
  private void readSparseIntArrayInternal(SparseIntArray paramSparseIntArray, int paramInt) {
    while (paramInt > 0) {
      int i = readInt();
      int j = readInt();
      paramSparseIntArray.append(i, j);
      paramInt--;
    } 
  }
  
  public long getBlobAshmemSize() {
    return nativeGetBlobAshmemSize(this.mNativePtr);
  }
  
  public static native long getGlobalAllocCount();
  
  public static native long getGlobalAllocSize();
  
  private static native long nativeAppendFrom(long paramLong1, long paramLong2, int paramInt1, int paramInt2);
  
  private static native int nativeCompareData(long paramLong1, long paramLong2);
  
  private static native long nativeCreate();
  
  private static native byte[] nativeCreateByteArray(long paramLong);
  
  @CriticalNative
  private static native int nativeDataAvail(long paramLong);
  
  @CriticalNative
  private static native int nativeDataCapacity(long paramLong);
  
  @CriticalNative
  private static native int nativeDataPosition(long paramLong);
  
  @CriticalNative
  private static native int nativeDataSize(long paramLong);
  
  private static native void nativeDestroy(long paramLong);
  
  private static native void nativeEnforceInterface(long paramLong, String paramString);
  
  private static native long nativeFreeBuffer(long paramLong);
  
  @CriticalNative
  private static native long nativeGetBlobAshmemSize(long paramLong);
  
  @CriticalNative
  private static native boolean nativeHasFileDescriptors(long paramLong);
  
  private static native byte[] nativeMarshall(long paramLong);
  
  @CriticalNative
  private static native boolean nativePushAllowFds(long paramLong, boolean paramBoolean);
  
  private static native byte[] nativeReadBlob(long paramLong);
  
  private static native boolean nativeReadByteArray(long paramLong, byte[] paramArrayOfbyte, int paramInt);
  
  @CriticalNative
  private static native int nativeReadCallingWorkSourceUid(long paramLong);
  
  @CriticalNative
  private static native double nativeReadDouble(long paramLong);
  
  @FastNative
  private static native FileDescriptor nativeReadFileDescriptor(long paramLong);
  
  @CriticalNative
  private static native float nativeReadFloat(long paramLong);
  
  @CriticalNative
  private static native int nativeReadInt(long paramLong);
  
  @CriticalNative
  private static native long nativeReadLong(long paramLong);
  
  @FastNative
  private static native String nativeReadString16(long paramLong);
  
  @FastNative
  private static native String nativeReadString8(long paramLong);
  
  @FastNative
  private static native IBinder nativeReadStrongBinder(long paramLong);
  
  @CriticalNative
  private static native boolean nativeReplaceCallingWorkSourceUid(long paramLong, int paramInt);
  
  @CriticalNative
  private static native void nativeRestoreAllowFds(long paramLong, boolean paramBoolean);
  
  @FastNative
  private static native void nativeSetDataCapacity(long paramLong, int paramInt);
  
  @CriticalNative
  private static native void nativeSetDataPosition(long paramLong, int paramInt);
  
  @FastNative
  private static native long nativeSetDataSize(long paramLong, int paramInt);
  
  private static native long nativeUnmarshall(long paramLong, byte[] paramArrayOfbyte, int paramInt1, int paramInt2);
  
  private static native void nativeWriteBlob(long paramLong, byte[] paramArrayOfbyte, int paramInt1, int paramInt2);
  
  private static native void nativeWriteByteArray(long paramLong, byte[] paramArrayOfbyte, int paramInt1, int paramInt2);
  
  @FastNative
  private static native void nativeWriteDouble(long paramLong, double paramDouble);
  
  @FastNative
  private static native long nativeWriteFileDescriptor(long paramLong, FileDescriptor paramFileDescriptor);
  
  @FastNative
  private static native void nativeWriteFloat(long paramLong, float paramFloat);
  
  @FastNative
  private static native void nativeWriteInt(long paramLong, int paramInt);
  
  private static native void nativeWriteInterfaceToken(long paramLong, String paramString);
  
  @FastNative
  private static native void nativeWriteLong(long paramLong1, long paramLong2);
  
  @FastNative
  private static native void nativeWriteString16(long paramLong, String paramString);
  
  @FastNative
  private static native void nativeWriteString8(long paramLong, String paramString);
  
  @FastNative
  private static native void nativeWriteStrongBinder(long paramLong, IBinder paramIBinder);
}
