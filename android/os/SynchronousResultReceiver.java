package android.os;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SynchronousResultReceiver extends ResultReceiver {
  class Result {
    public Bundle bundle;
    
    public int resultCode;
    
    public Result(SynchronousResultReceiver this$0, Bundle param1Bundle) {
      this.resultCode = this$0;
      this.bundle = param1Bundle;
    }
  }
  
  private final CompletableFuture<Result> mFuture = new CompletableFuture<>();
  
  private final String mName;
  
  public SynchronousResultReceiver() {
    super((Handler)null);
    this.mName = null;
  }
  
  public SynchronousResultReceiver(String paramString) {
    super((Handler)null);
    this.mName = paramString;
  }
  
  protected final void onReceiveResult(int paramInt, Bundle paramBundle) {
    super.onReceiveResult(paramInt, paramBundle);
    this.mFuture.complete(new Result(paramInt, paramBundle));
  }
  
  public String getName() {
    return this.mName;
  }
  
  public Result awaitResult(long paramLong) throws TimeoutException {
    long l1 = System.currentTimeMillis(), l2 = paramLong;
    while (l2 >= 0L) {
      try {
        return this.mFuture.get(l2, TimeUnit.MILLISECONDS);
      } catch (ExecutionException executionException) {
        throw new AssertionError("Error receiving response", executionException);
      } catch (InterruptedException interruptedException) {
        l2 -= l1 + paramLong - System.currentTimeMillis();
      } 
    } 
    throw new TimeoutException();
  }
}
