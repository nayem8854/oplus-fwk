package android.os;

import android.util.Log;
import vendor.oplus.hardware.performance.V1_0.IPerformance;
import vendor.oplus.hardware.performance.V1_0.ProcReqHal;

public class PerformanceManager {
  private static final String TAG = "PerformanceManager";
  
  private static IPerformance mPerformanceService = null;
  
  private static IPerformance getPerformanceService() {
    if (mPerformanceService == null)
      try {
        mPerformanceService = IPerformance.getService();
      } catch (Exception exception) {
        Log.e("PerformanceManager", "Failed to get performance hal service", exception);
      }  
    return mPerformanceService;
  }
  
  public static void disableMultiThreadOptimize() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        iPerformance.disableMultiThreadOptimize(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "disable multi thread optimization failed.", (Throwable)remoteException);
    } 
  }
  
  public static void enableMultiThreadOptimize() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        iPerformance.enableMultiThreadOptimize(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "enable multi thread optimization failed.", (Throwable)remoteException);
    } 
  }
  
  public static ProcReqHal getHICpuLoading() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        return iPerformance.getHICpuLoading(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "getHICpuLoading failed.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static ProcReqHal getHIEmcdrvIowait() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        return iPerformance.getHIEmcdrvIowait(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "getHIEmcdrvIowait failed.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static ProcReqHal getHIIowaitHung() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        return iPerformance.getHIIowaitHung(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "getHIIowaitHung failed.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static ProcReqHal getHISchedLatency() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        return iPerformance.getHISchedLatency(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "getHISchedLatency failed.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static ProcReqHal getHIFsyncWait() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        return iPerformance.getHIFsyncWait(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "get fync wait info failed.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static ProcReqHal getHIIowait() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        return iPerformance.getHIIowait(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "get iowait info failed.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static ProcReqHal getHIUfsFeature() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        return iPerformance.getHIUfsFeature(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "get ufs feature failed.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static ProcReqHal getHIIonWait() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        return iPerformance.getHIIonWait(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "get ion wait info failed.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static ProcReqHal getHIAllocWait() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        return iPerformance.getHIAllocWait(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "get alloc wait info failed.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static ProcReqHal getHIDState() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        return iPerformance.getHIDState(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "get DState info failed.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static ProcReqHal getHIKswapdLoading() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        return iPerformance.getHIKswapdLoading(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "get kswapd loading info failed.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static ProcReqHal getHIScmCall() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        return iPerformance.getHIScmCall(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "get scm_call info failed.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static String getHICpuInfo() {
    try {
      IPerformance iPerformance = getPerformanceService();
      if (iPerformance != null)
        return iPerformance.getHICpuInfo(); 
    } catch (RemoteException remoteException) {
      Log.e("PerformanceManager", "get scm_call info failed.", (Throwable)remoteException);
    } 
    return null;
  }
}
