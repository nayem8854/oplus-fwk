package android.os;

import android.annotation.SystemApi;

@SystemApi
public class ServiceSpecificException extends RuntimeException {
  public final int errorCode;
  
  public ServiceSpecificException(int paramInt, String paramString) {
    super(paramString);
    this.errorCode = paramInt;
  }
  
  public ServiceSpecificException(int paramInt) {
    this.errorCode = paramInt;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(super.toString());
    stringBuilder.append(" (code ");
    stringBuilder.append(this.errorCode);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
}
