package android.os;

public interface IThermalStatusListener extends IInterface {
  void onStatusChange(int paramInt) throws RemoteException;
  
  class Default implements IThermalStatusListener {
    public void onStatusChange(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IThermalStatusListener {
    private static final String DESCRIPTOR = "android.os.IThermalStatusListener";
    
    static final int TRANSACTION_onStatusChange = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IThermalStatusListener");
    }
    
    public static IThermalStatusListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IThermalStatusListener");
      if (iInterface != null && iInterface instanceof IThermalStatusListener)
        return (IThermalStatusListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onStatusChange";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.IThermalStatusListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IThermalStatusListener");
      param1Int1 = param1Parcel1.readInt();
      onStatusChange(param1Int1);
      return true;
    }
    
    private static class Proxy implements IThermalStatusListener {
      public static IThermalStatusListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IThermalStatusListener";
      }
      
      public void onStatusChange(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IThermalStatusListener");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IThermalStatusListener.Stub.getDefaultImpl() != null) {
            IThermalStatusListener.Stub.getDefaultImpl().onStatusChange(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IThermalStatusListener param1IThermalStatusListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IThermalStatusListener != null) {
          Proxy.sDefaultImpl = param1IThermalStatusListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IThermalStatusListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
