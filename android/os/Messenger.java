package android.os;

public final class Messenger implements Parcelable {
  public Messenger(Handler paramHandler) {
    this.mTarget = paramHandler.getIMessenger();
  }
  
  public void send(Message paramMessage) throws RemoteException {
    this.mTarget.send(paramMessage);
  }
  
  public IBinder getBinder() {
    return this.mTarget.asBinder();
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == null)
      return false; 
    try {
      IBinder iBinder = this.mTarget.asBinder();
      paramObject = ((Messenger)paramObject).mTarget;
      paramObject = paramObject.asBinder();
      return iBinder.equals(paramObject);
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
  
  public int hashCode() {
    return this.mTarget.asBinder().hashCode();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStrongBinder(this.mTarget.asBinder());
  }
  
  public static final Parcelable.Creator<Messenger> CREATOR = new Parcelable.Creator<Messenger>() {
      public Messenger createFromParcel(Parcel param1Parcel) {
        IBinder iBinder = param1Parcel.readStrongBinder();
        if (iBinder != null) {
          Messenger messenger = new Messenger(iBinder);
        } else {
          iBinder = null;
        } 
        return (Messenger)iBinder;
      }
      
      public Messenger[] newArray(int param1Int) {
        return new Messenger[param1Int];
      }
    };
  
  private final IMessenger mTarget;
  
  public static void writeMessengerOrNullToParcel(Messenger paramMessenger, Parcel paramParcel) {
    if (paramMessenger != null) {
      IBinder iBinder = paramMessenger.mTarget.asBinder();
    } else {
      paramMessenger = null;
    } 
    paramParcel.writeStrongBinder((IBinder)paramMessenger);
  }
  
  public static Messenger readMessengerOrNullFromParcel(Parcel paramParcel) {
    IBinder iBinder = paramParcel.readStrongBinder();
    if (iBinder != null) {
      Messenger messenger = new Messenger(iBinder);
    } else {
      iBinder = null;
    } 
    return (Messenger)iBinder;
  }
  
  public Messenger(IBinder paramIBinder) {
    this.mTarget = IMessenger.Stub.asInterface(paramIBinder);
  }
}
