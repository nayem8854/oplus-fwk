package android.os;

class ServiceManagerProxy implements IServiceManager {
  private IBinder mRemote;
  
  private IServiceManager mServiceManager;
  
  public ServiceManagerProxy(IBinder paramIBinder) {
    this.mRemote = paramIBinder;
    this.mServiceManager = IServiceManager.Stub.asInterface(paramIBinder);
  }
  
  public IBinder asBinder() {
    return this.mRemote;
  }
  
  public IBinder getService(String paramString) throws RemoteException {
    return this.mServiceManager.checkService(paramString);
  }
  
  public IBinder checkService(String paramString) throws RemoteException {
    return this.mServiceManager.checkService(paramString);
  }
  
  public void addService(String paramString, IBinder paramIBinder, boolean paramBoolean, int paramInt) throws RemoteException {
    this.mServiceManager.addService(paramString, paramIBinder, paramBoolean, paramInt);
  }
  
  public String[] listServices(int paramInt) throws RemoteException {
    return this.mServiceManager.listServices(paramInt);
  }
  
  public void registerForNotifications(String paramString, IServiceCallback paramIServiceCallback) throws RemoteException {
    throw new RemoteException();
  }
  
  public void unregisterForNotifications(String paramString, IServiceCallback paramIServiceCallback) throws RemoteException {
    throw new RemoteException();
  }
  
  public boolean isDeclared(String paramString) throws RemoteException {
    return this.mServiceManager.isDeclared(paramString);
  }
  
  public void registerClientCallback(String paramString, IBinder paramIBinder, IClientCallback paramIClientCallback) throws RemoteException {
    throw new RemoteException();
  }
  
  public void tryUnregisterService(String paramString, IBinder paramIBinder) throws RemoteException {
    throw new RemoteException();
  }
}
