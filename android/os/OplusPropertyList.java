package android.os;

import com.oplus.annotation.OplusProperty;

public class OplusPropertyList {
  public static final String BRIGHTNESS_DEFAULT_BRIGHTNESS;
  
  public static final String BRIGHTNESS_HBM_XS;
  
  public static final String BRIGHTNESS_HBM_XS_MAX;
  
  public static final String BRIGHTNESS_HBM_XS_MIN;
  
  public static final String BRIGHTNESS_HBM_YS;
  
  public static final String BRIGHTNESS_MIN_LIGHT_IN_DNM;
  
  public static final String BRIGHTNESS_NORMAL_MAX_BRIGHTNESS;
  
  public static final String BRIGHTNESS_NORMAL_MIN_BRIGHTNESS;
  
  public static final String BRIGHTNESS_PANEL_NUM;
  
  public static final String BRIGHTNESS_XS;
  
  public static final String BRIGHTNESS_YS;
  
  public static final String CAMERA_SOUND_FORCED;
  
  public static final String DENSITY_SCREENZOOM_FDH;
  
  public static final String DENSITY_SCREENZOOM_QDH;
  
  public static final String ENGINEERING_FEATURE_LOW_VOLUME;
  
  public static final String NOTSUPPORT_AUDIOSCENCE_STATUS;
  
  public static final String OPLUS_EUEX_COUNTRY;
  
  public static final String OPLUS_MARKET_ENNAME;
  
  public static final String OPLUS_MARKET_NAME;
  
  public static final String OPLUS_OPERATOR;
  
  public static final String OPLUS_REGIONMARK;
  
  public static final String OPLUS_REGION_NETLOCK;
  
  public static final String OPLUS_RSA_SUPPORT;
  
  public static final String OPLUS_VENDOR_MARKET_ENNAME;
  
  public static final String OPLUS_VENDOR_MARKET_NAME;
  
  public static final String OPLUS_VERSION = SystemProperties.get("ro.oppo.version", "CN");
  
  public static final String OTEST_CAMERA_TEST_ENABLE;
  
  public static final String OTEST_MONKEY_ENABLE;
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PERSIST_SYS_OPEN_CAMERA = "persist.sys.oplus.camera.open.camera";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PERSIST_SYS_OPEN_TORCH = "persist.sys.oplus.camera.open.torch";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_AUDIO_EFFECT_TYPE = "ro.oplus.audio.effect.type";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_AUDIO_NOTSUPPORT_AUDIOMICDET = "ro.oplus.audio.support.audioblocking";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_AUDIO_NOTSUPPORT_AUDIOSCENCE = "ro.oplus.audio.notsupport.audioscence";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_AUDIO_NOTSUPPORT_AUDIOSINGELMIC = "ro.oplus.audio.support.audiosinglemic";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_BEAM_ALWAYS_DISABLE = "persist.sys.oplus.nfc.beam_disable";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_BRIGHTNESS_DEFAULT_BRIGHTNESS = "ro.oplus.display.brightness.default_brightness";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_BRIGHTNESS_HBM_XS = "ro.display.brightness.hbm_xs";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_BRIGHTNESS_HBM_XS_MAX = "ro.display.brightness.hbm_xs_max";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_BRIGHTNESS_HBM_XS_MIN = "ro.display.brightness.hbm_xs_min";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_BRIGHTNESS_HBM_YS = "ro.oplus.display.brightness.hbm_ys";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_BRIGHTNESS_MIN_LIGHT_IN_DNM = "ro.oplus.display.brightness.min_light_in_dnm";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_BRIGHTNESS_NORMAL_MAX_BRIGHTNESS = "ro.oplus.display.brightness.normal_max_brightness";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_BRIGHTNESS_NORMAL_MIN_BRIGHTNESS = "ro.oplus.display.brightness.normal_min_brightness";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_BRIGHTNESS_PANEL_NUM = "ro.oplus.display.brightness.panel_num";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_BRIGHTNESS_XS = "ro.oplus.display.brightness.xs";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_BRIGHTNESS_YS = "ro.oplus.display.brightness.ys";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_BTN_APOLLO = "persist.brightness.apollo";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_CAMERA_SOUND_FORCED = "ro.oplus.audio.camerasound.forced";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_CARRIER_DATE_UTC_BACKUP = "persist.sys.oplus.my_carrier.data_utc_backup";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_CONNECTIVITY_OVERSEA = "ro.oplus.connectivity.oversea";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_CONNECTIVITY_PREVISION = "ro.oplus.connectivity.prevision_build";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_CUSTOMIZE_MARKET_APP_ALLOW_INSTALL = "persist.sys.custom.market.app.list.allow.install";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_CUSTOMIZE_MARKET_APP_UNINSTALL = "persist.sys.custom.market.app.list.uninstall";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_CUSTOMIZE_OPLUS_APP_ALLOW_INSTALL = "persist.sys.custom.oplus.app.list.allow.install";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_CUSTOMIZE_OPLUS_APP_UNINSTALL = "persist.sys.custom.oplus.app.list.uninstall";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_CUSTOMIZE_UNINSTALL_MINUS_SCREEN = "persist.sys.custom.disable.minus_screen";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_CUSTOMIZE_UNINSTALL_OPLUS_BROWSER = "persist.sys.custom.disable.oplus.browser";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_DENSITY_SCREENZOOM_FDH = "ro.oplus.display.density.screenzoom.fdh";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_DENSITY_SCREENZOOM_QDH = "ro.oplus.display.density.screenzoom.qdh";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_ENGINEERING_FEATURE_LOW_VOLUME = "ro.oplus.audio.low_volume_for_engineering";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_FEATURE_OCA_ON = "persist.sys.oplus.feature_oca_on";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_GPS_NFW_ENABLE = "persist.sys.oppo.nfw.enable";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_GPS_NLP_NAME = "persist.sys.oplus.gps.nlp_name";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_HW_PHONE_OPLUS = "ro.hw.phone.color";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_NEURON_SYSTEM = "persist.sys.oplus.neuron_system";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_NFC_ALWAYS_DISABLE = "persist.sys.oplus.nfc.always_disable";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_NFC_ALWAYS_ENABLE = "persist.sys.oplus.nfc.always_enable";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_NFC_ANTENNA_AREA = "persist.sys.oplus.nfc.antenna_area";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_NFC_DEFAULT_ENABLE = "persist.sys.oplus.nfc.defnfc";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_OPERATOR_SERVICE_OPTA = "persist.sys.oplus.operator.opta";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_OPERATOR_SERVICE_OPTB = "persist.sys.oplus.operator.optb";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_OPINSTALLED = "persist.sys.oppo.opinstalled";
  
  @OplusProperty(OplusProperty.OplusPropertyType.SYS_PROPERTY)
  public static final String PROPERTY_OPLUS_CROSS_UPGRADE = "sys.oplus.cross.upgrade";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_OPLUS_EUEX_COUNTRY = "ro.vendor.oplus.euex.country";
  
  @OplusProperty(OplusProperty.OplusPropertyType.SYS_PROPERTY)
  public static final String PROPERTY_OPLUS_FACTORY_CODE = "oppo.eng.factory.no";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_OPLUS_MARKET_ENNAME = "ro.oppo.market.enname";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_OPLUS_MARKET_NAME = "ro.oppo.market.name";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_OPLUS_OPERATOR = "ro.vendor.oplus.operator";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_OPLUS_REGIONMARK = "ro.vendor.oplus.regionmark";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_OPLUS_REGION_NETLOCK = "ro.oppo.region.netlock";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_OPLUS_RSA_SUPPORT = "ro.oplus.rsa3.support";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_OPLUS_SERIAL_NUMBER = "persist.sys.oppo.serialno";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_OPLUS_SUPPORT_GPFELICA = "ro.oplus.nfc.gpfelica.support";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_OPLUS_VENDOR_MARKET_ENNAME = "ro.vendor.oplus.market.enname";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_OPLUS_VENDOR_MARKET_NAME = "ro.vendor.oplus.market.name";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_OPLUS_VERSION = "ro.oppo.version";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_RC_SIZE = "ro.oplus.display.rc.size";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPERTY_REGION = "persist.sys.oppo.region";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_SCREENHOLE_POSITION = "ro.oplus.display.screenhole.positon";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_SCREEN_HETEROMORPHISM = "ro.oplus.display.screen.heteromorphism";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_SCREEN_UNDERLIGHTSENSOR_REGION = "ro.oplus.lcd.display.screen.underlightsensor.region";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_SENSOR_TYPE = "ro.oplus.display.sensortype";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_SEPARATE_SOFT = "ro.separate.soft";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPERTY_SUPPORT_JAPAN_FELICA = "ro.oplus.nfc.support_japan_felica";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPETY_BIOMETRICS_DEBUG = "persist.sys.biometrics.debug";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPETY_BRIGHTNESS_MODE = "persist.sys.oplus.display.brightness.mode";
  
  @OplusProperty(OplusProperty.OplusPropertyType.SYS_PROPERTY)
  public static final String PROPETY_COTA_MOUNTED_STATE = "sys.cotaimg.verify";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPETY_DC_FEATURE = "persist.sys.oplus.dc.feature";
  
  @OplusProperty(OplusProperty.OplusPropertyType.SYS_PROPERTY)
  public static final String PROPETY_DEEPSLEEP_DISABLE_NETWORK = "sys.deepsleep.disable.network";
  
  @OplusProperty(OplusProperty.OplusPropertyType.SYS_PROPERTY)
  public static final String PROPETY_DEEPSLEEP_RESTORE_NETWORK = "sys.deepsleep.restore.network";
  
  @OplusProperty(OplusProperty.OplusPropertyType.SYS_PROPERTY)
  public static final String PROPETY_DISPLAY_RATE = "sys.oppo.display.rate";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPETY_ENALBE_TORCH = "persist.sys.torch_available";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPETY_FACE_DATA_DEBUG = "persist.face.disable_debug_data";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPETY_FACE_POWER_BLOCK_DEBUG = "persist.sys.android.face.power.block";
  
  @OplusProperty(OplusProperty.OplusPropertyType.SYS_PROPERTY)
  public static final String PROPETY_HIGH_PREFORMANCE_MODE = "sys.oppo.high.performance";
  
  @OplusProperty(OplusProperty.OplusPropertyType.PERSIST_PROPERTY)
  public static final String PROPETY_LIST_OPTIMIZE_ENABLE = "persist.sys.flingopts.enable";
  
  @OplusProperty(OplusProperty.OplusPropertyType.SYS_PROPERTY)
  public static final String PROPETY_OTEST_CAMERA_TEST_ENABLE = "sys.oplus.otest.cameratest.enable";
  
  @OplusProperty(OplusProperty.OplusPropertyType.SYS_PROPERTY)
  public static final String PROPETY_OTEST_MONKEY_ENABLE = "sys.oplus.otest.monkey.enable";
  
  @OplusProperty(OplusProperty.OplusPropertyType.SYS_PROPERTY)
  public static final String PROPETY_RES_PRELOAD_VERSION = "sys.oplus.respreload.version";
  
  @OplusProperty(OplusProperty.OplusPropertyType.RO_PROPERTY)
  public static final String PROPETY_RO_BRIGHTNESS_MODE = "ro.display.brightness.brightness.mode";
  
  public static final String RC_SIZE;
  
  public static String REGION;
  
  public static final String RO_BRIGHTNESS_MODE;
  
  public static final String SCREENHOLE_POSITION;
  
  public static final String SCREEN_HETEROMORPHISM;
  
  public static final String SCREEN_UNDERLIGHTSENSOR_REGION;
  
  public static final String SENSOR_TYPE;
  
  public static final String SEPARATE_SOFT;
  
  private static final String TAG = "OplusPropertyList";
  
  public static final String UNKNOWN = "unknown";
  
  static {
    OPLUS_REGIONMARK = SystemProperties.get("ro.vendor.oplus.regionmark", "");
    OPLUS_OPERATOR = SystemProperties.get("ro.vendor.oplus.operator", "");
    OPLUS_EUEX_COUNTRY = SystemProperties.get("ro.vendor.oplus.euex.country", "");
    OPLUS_RSA_SUPPORT = SystemProperties.get("ro.oplus.rsa3.support", "false");
    SEPARATE_SOFT = SystemProperties.get("ro.separate.soft", "");
    OPLUS_MARKET_NAME = SystemProperties.get("ro.oppo.market.name", "");
    OPLUS_MARKET_ENNAME = SystemProperties.get("ro.oppo.market.enname", "");
    OPLUS_VENDOR_MARKET_NAME = SystemProperties.get("ro.vendor.oplus.market.name", "");
    OPLUS_VENDOR_MARKET_ENNAME = SystemProperties.get("ro.vendor.oplus.market.enname", "");
    OPLUS_REGION_NETLOCK = SystemProperties.get("ro.oppo.region.netlock", "");
    OTEST_MONKEY_ENABLE = SystemProperties.get("sys.oplus.otest.monkey.enable", "0");
    OTEST_CAMERA_TEST_ENABLE = SystemProperties.get("sys.oplus.otest.cameratest.enable", "0");
    REGION = getString("persist.sys.oppo.region");
    CAMERA_SOUND_FORCED = SystemProperties.get("ro.oplus.audio.camerasound.forced", "false");
    NOTSUPPORT_AUDIOSCENCE_STATUS = SystemProperties.get("ro.oplus.audio.notsupport.audioscence", "0");
    SCREEN_UNDERLIGHTSENSOR_REGION = SystemProperties.get("ro.oplus.lcd.display.screen.underlightsensor.region", "0");
    BRIGHTNESS_XS = SystemProperties.get("ro.oplus.display.brightness.xs", "0");
    BRIGHTNESS_YS = SystemProperties.get("ro.oplus.display.brightness.ys", "0");
    BRIGHTNESS_HBM_XS = SystemProperties.get("ro.display.brightness.hbm_xs", "0");
    BRIGHTNESS_HBM_XS_MIN = SystemProperties.get("ro.display.brightness.hbm_xs_min", "0");
    BRIGHTNESS_HBM_XS_MAX = SystemProperties.get("ro.display.brightness.hbm_xs_max", "0");
    BRIGHTNESS_HBM_YS = SystemProperties.get("ro.oplus.display.brightness.hbm_ys", "0");
    BRIGHTNESS_DEFAULT_BRIGHTNESS = SystemProperties.get("ro.oplus.display.brightness.default_brightness", "0");
    BRIGHTNESS_NORMAL_MAX_BRIGHTNESS = SystemProperties.get("ro.oplus.display.brightness.normal_max_brightness", "0");
    BRIGHTNESS_NORMAL_MIN_BRIGHTNESS = SystemProperties.get("ro.oplus.display.brightness.normal_min_brightness", "0");
    BRIGHTNESS_MIN_LIGHT_IN_DNM = SystemProperties.get("ro.oplus.display.brightness.min_light_in_dnm", "0");
    BRIGHTNESS_PANEL_NUM = SystemProperties.get("ro.oplus.display.brightness.panel_num", "1");
    DENSITY_SCREENZOOM_FDH = SystemProperties.get("ro.oplus.display.density.screenzoom.fdh", "0");
    DENSITY_SCREENZOOM_QDH = SystemProperties.get("ro.oplus.display.density.screenzoom.qdh", "0");
    SCREEN_HETEROMORPHISM = SystemProperties.get("ro.oplus.display.screen.heteromorphism", "0");
    RO_BRIGHTNESS_MODE = SystemProperties.get("ro.display.brightness.brightness.mode", "0");
    SENSOR_TYPE = SystemProperties.get("ro.oplus.display.sensortype", "0");
    SCREENHOLE_POSITION = SystemProperties.get("ro.oplus.display.screenhole.positon", "0");
    RC_SIZE = SystemProperties.get("ro.oplus.display.rc.size", "0");
    ENGINEERING_FEATURE_LOW_VOLUME = SystemProperties.get("ro.oplus.audio.low_volume_for_engineering", "false");
  }
  
  private static String getString(String paramString) {
    return SystemProperties.get(paramString, "unknown");
  }
}
