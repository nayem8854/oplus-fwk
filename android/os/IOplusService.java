package android.os;

import java.util.HashMap;
import java.util.Map;

public interface IOplusService extends IInterface {
  void StartLogCoreService() throws RemoteException;
  
  void assertKernelPanic() throws RemoteException;
  
  boolean closeFlashLight() throws RemoteException;
  
  boolean copyFile(String paramString1, String paramString2) throws RemoteException;
  
  boolean copyFileForDcs(String paramString1, String paramString2) throws RemoteException;
  
  boolean deleteFile(String paramString) throws RemoteException;
  
  void deleteFileForDcs(String paramString) throws RemoteException;
  
  void deleteSystemLogFile() throws RemoteException;
  
  String getFlashLightState() throws RemoteException;
  
  String getOplusLogInfoString(int paramInt) throws RemoteException;
  
  boolean iScoreLogServiceRunning() throws RemoteException;
  
  boolean openFlashLight() throws RemoteException;
  
  void sendDeleteStampId(String paramString) throws RemoteException;
  
  void sendOnStampEvent(String paramString, Map paramMap) throws RemoteException;
  
  void sendQualityDCSEvent(String paramString, Map paramMap) throws RemoteException;
  
  void startOplusFileEncodeHelperService() throws RemoteException;
  
  void startSensorLog(boolean paramBoolean) throws RemoteException;
  
  void stopSensorLog() throws RemoteException;
  
  void unbindCoreLogService() throws RemoteException;
  
  void unbindOplusFileEncodeHelperService() throws RemoteException;
  
  class Default implements IOplusService {
    public void startSensorLog(boolean param1Boolean) throws RemoteException {}
    
    public void stopSensorLog() throws RemoteException {}
    
    public boolean openFlashLight() throws RemoteException {
      return false;
    }
    
    public boolean closeFlashLight() throws RemoteException {
      return false;
    }
    
    public String getFlashLightState() throws RemoteException {
      return null;
    }
    
    public boolean iScoreLogServiceRunning() throws RemoteException {
      return false;
    }
    
    public void StartLogCoreService() throws RemoteException {}
    
    public void unbindCoreLogService() throws RemoteException {}
    
    public void assertKernelPanic() throws RemoteException {}
    
    public String getOplusLogInfoString(int param1Int) throws RemoteException {
      return null;
    }
    
    public void deleteSystemLogFile() throws RemoteException {}
    
    public boolean copyFileForDcs(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public void deleteFileForDcs(String param1String) throws RemoteException {}
    
    public void startOplusFileEncodeHelperService() throws RemoteException {}
    
    public void unbindOplusFileEncodeHelperService() throws RemoteException {}
    
    public boolean copyFile(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean deleteFile(String param1String) throws RemoteException {
      return false;
    }
    
    public void sendOnStampEvent(String param1String, Map param1Map) throws RemoteException {}
    
    public void sendDeleteStampId(String param1String) throws RemoteException {}
    
    public void sendQualityDCSEvent(String param1String, Map param1Map) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusService {
    private static final String DESCRIPTOR = "android.os.IOplusService";
    
    static final int TRANSACTION_StartLogCoreService = 7;
    
    static final int TRANSACTION_assertKernelPanic = 9;
    
    static final int TRANSACTION_closeFlashLight = 4;
    
    static final int TRANSACTION_copyFile = 16;
    
    static final int TRANSACTION_copyFileForDcs = 12;
    
    static final int TRANSACTION_deleteFile = 17;
    
    static final int TRANSACTION_deleteFileForDcs = 13;
    
    static final int TRANSACTION_deleteSystemLogFile = 11;
    
    static final int TRANSACTION_getFlashLightState = 5;
    
    static final int TRANSACTION_getOplusLogInfoString = 10;
    
    static final int TRANSACTION_iScoreLogServiceRunning = 6;
    
    static final int TRANSACTION_openFlashLight = 3;
    
    static final int TRANSACTION_sendDeleteStampId = 19;
    
    static final int TRANSACTION_sendOnStampEvent = 18;
    
    static final int TRANSACTION_sendQualityDCSEvent = 20;
    
    static final int TRANSACTION_startOplusFileEncodeHelperService = 14;
    
    static final int TRANSACTION_startSensorLog = 1;
    
    static final int TRANSACTION_stopSensorLog = 2;
    
    static final int TRANSACTION_unbindCoreLogService = 8;
    
    static final int TRANSACTION_unbindOplusFileEncodeHelperService = 15;
    
    public Stub() {
      attachInterface(this, "android.os.IOplusService");
    }
    
    public static IOplusService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IOplusService");
      if (iInterface != null && iInterface instanceof IOplusService)
        return (IOplusService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 20:
          return "sendQualityDCSEvent";
        case 19:
          return "sendDeleteStampId";
        case 18:
          return "sendOnStampEvent";
        case 17:
          return "deleteFile";
        case 16:
          return "copyFile";
        case 15:
          return "unbindOplusFileEncodeHelperService";
        case 14:
          return "startOplusFileEncodeHelperService";
        case 13:
          return "deleteFileForDcs";
        case 12:
          return "copyFileForDcs";
        case 11:
          return "deleteSystemLogFile";
        case 10:
          return "getOplusLogInfoString";
        case 9:
          return "assertKernelPanic";
        case 8:
          return "unbindCoreLogService";
        case 7:
          return "StartLogCoreService";
        case 6:
          return "iScoreLogServiceRunning";
        case 5:
          return "getFlashLightState";
        case 4:
          return "closeFlashLight";
        case 3:
          return "openFlashLight";
        case 2:
          return "stopSensorLog";
        case 1:
          break;
      } 
      return "startSensorLog";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int i;
        boolean bool1;
        HashMap hashMap2;
        String str2;
        HashMap hashMap1;
        String str1, str3;
        ClassLoader classLoader;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 20:
            param1Parcel1.enforceInterface("android.os.IOplusService");
            str3 = param1Parcel1.readString();
            classLoader = getClass().getClassLoader();
            hashMap2 = param1Parcel1.readHashMap(classLoader);
            sendQualityDCSEvent(str3, hashMap2);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            hashMap2.enforceInterface("android.os.IOplusService");
            str2 = hashMap2.readString();
            sendDeleteStampId(str2);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            str2.enforceInterface("android.os.IOplusService");
            str3 = str2.readString();
            classLoader = getClass().getClassLoader();
            hashMap1 = str2.readHashMap(classLoader);
            sendOnStampEvent(str3, hashMap1);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            hashMap1.enforceInterface("android.os.IOplusService");
            str1 = hashMap1.readString();
            bool2 = deleteFile(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 16:
            str1.enforceInterface("android.os.IOplusService");
            str3 = str1.readString();
            str1 = str1.readString();
            bool2 = copyFile(str3, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 15:
            str1.enforceInterface("android.os.IOplusService");
            unbindOplusFileEncodeHelperService();
            return true;
          case 14:
            str1.enforceInterface("android.os.IOplusService");
            startOplusFileEncodeHelperService();
            return true;
          case 13:
            str1.enforceInterface("android.os.IOplusService");
            str1 = str1.readString();
            deleteFileForDcs(str1);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str1.enforceInterface("android.os.IOplusService");
            str3 = str1.readString();
            str1 = str1.readString();
            bool2 = copyFileForDcs(str3, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 11:
            str1.enforceInterface("android.os.IOplusService");
            deleteSystemLogFile();
            return true;
          case 10:
            str1.enforceInterface("android.os.IOplusService");
            i = str1.readInt();
            str1 = getOplusLogInfoString(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 9:
            str1.enforceInterface("android.os.IOplusService");
            assertKernelPanic();
            return true;
          case 8:
            str1.enforceInterface("android.os.IOplusService");
            unbindCoreLogService();
            return true;
          case 7:
            str1.enforceInterface("android.os.IOplusService");
            StartLogCoreService();
            return true;
          case 6:
            str1.enforceInterface("android.os.IOplusService");
            bool1 = iScoreLogServiceRunning();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 5:
            str1.enforceInterface("android.os.IOplusService");
            str1 = getFlashLightState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 4:
            str1.enforceInterface("android.os.IOplusService");
            bool1 = closeFlashLight();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            str1.enforceInterface("android.os.IOplusService");
            bool1 = openFlashLight();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            str1.enforceInterface("android.os.IOplusService");
            stopSensorLog();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.os.IOplusService");
        if (str1.readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        startSensorLog(bool);
        return true;
      } 
      param1Parcel2.writeString("android.os.IOplusService");
      return true;
    }
    
    private static class Proxy implements IOplusService {
      public static IOplusService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IOplusService";
      }
      
      public void startSensorLog(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.os.IOplusService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && IOplusService.Stub.getDefaultImpl() != null) {
            IOplusService.Stub.getDefaultImpl().startSensorLog(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopSensorLog() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusService");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IOplusService.Stub.getDefaultImpl() != null) {
            IOplusService.Stub.getDefaultImpl().stopSensorLog();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean openFlashLight() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IOplusService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusService.Stub.getDefaultImpl().openFlashLight();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean closeFlashLight() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IOplusService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusService.Stub.getDefaultImpl().closeFlashLight();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getFlashLightState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusService");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusService.Stub.getDefaultImpl() != null)
            return IOplusService.Stub.getDefaultImpl().getFlashLightState(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean iScoreLogServiceRunning() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusService.Stub.getDefaultImpl().iScoreLogServiceRunning();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void StartLogCoreService() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusService");
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IOplusService.Stub.getDefaultImpl() != null) {
            IOplusService.Stub.getDefaultImpl().StartLogCoreService();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unbindCoreLogService() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusService");
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && IOplusService.Stub.getDefaultImpl() != null) {
            IOplusService.Stub.getDefaultImpl().unbindCoreLogService();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void assertKernelPanic() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusService");
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && IOplusService.Stub.getDefaultImpl() != null) {
            IOplusService.Stub.getDefaultImpl().assertKernelPanic();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public String getOplusLogInfoString(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusService.Stub.getDefaultImpl() != null)
            return IOplusService.Stub.getDefaultImpl().getOplusLogInfoString(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteSystemLogFile() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusService");
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && IOplusService.Stub.getDefaultImpl() != null) {
            IOplusService.Stub.getDefaultImpl().deleteSystemLogFile();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean copyFileForDcs(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && IOplusService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusService.Stub.getDefaultImpl().copyFileForDcs(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteFileForDcs(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IOplusService.Stub.getDefaultImpl() != null) {
            IOplusService.Stub.getDefaultImpl().deleteFileForDcs(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startOplusFileEncodeHelperService() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusService");
          boolean bool = this.mRemote.transact(14, parcel, (Parcel)null, 1);
          if (!bool && IOplusService.Stub.getDefaultImpl() != null) {
            IOplusService.Stub.getDefaultImpl().startOplusFileEncodeHelperService();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unbindOplusFileEncodeHelperService() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusService");
          boolean bool = this.mRemote.transact(15, parcel, (Parcel)null, 1);
          if (!bool && IOplusService.Stub.getDefaultImpl() != null) {
            IOplusService.Stub.getDefaultImpl().unbindOplusFileEncodeHelperService();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean copyFile(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && IOplusService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusService.Stub.getDefaultImpl().copyFile(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean deleteFile(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IOplusService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusService.Stub.getDefaultImpl().deleteFile(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendOnStampEvent(String param2String, Map param2Map) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusService");
          parcel1.writeString(param2String);
          parcel1.writeMap(param2Map);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IOplusService.Stub.getDefaultImpl() != null) {
            IOplusService.Stub.getDefaultImpl().sendOnStampEvent(param2String, param2Map);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendDeleteStampId(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IOplusService.Stub.getDefaultImpl() != null) {
            IOplusService.Stub.getDefaultImpl().sendDeleteStampId(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendQualityDCSEvent(String param2String, Map param2Map) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOplusService");
          parcel1.writeString(param2String);
          parcel1.writeMap(param2Map);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IOplusService.Stub.getDefaultImpl() != null) {
            IOplusService.Stub.getDefaultImpl().sendQualityDCSEvent(param2String, param2Map);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusService param1IOplusService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusService != null) {
          Proxy.sDefaultImpl = param1IOplusService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
