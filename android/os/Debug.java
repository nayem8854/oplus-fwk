package android.os;

import android.util.Log;
import com.android.internal.util.FastPrintWriter;
import com.android.internal.util.Preconditions;
import com.android.internal.util.TypedProperties;
import dalvik.system.VMDebug;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import org.apache.harmony.dalvik.ddmc.Chunk;
import org.apache.harmony.dalvik.ddmc.ChunkHandler;
import org.apache.harmony.dalvik.ddmc.DdmServer;

public final class Debug {
  private static final String DEFAULT_TRACE_BODY = "dmtrace";
  
  private static final String DEFAULT_TRACE_EXTENSION = ".trace";
  
  public static final int MEMINFO_BUFFERS = 2;
  
  public static final int MEMINFO_CACHED = 3;
  
  public static final int MEMINFO_COUNT = 18;
  
  public static final int MEMINFO_FREE = 1;
  
  public static final int MEMINFO_ION_CACHED = 16;
  
  public static final int MEMINFO_ION_ORPHANED = 17;
  
  public static final int MEMINFO_KERNEL_STACK = 14;
  
  public static final int MEMINFO_KRECLAIMABLE = 15;
  
  public static final int MEMINFO_MAPPED = 11;
  
  public static final int MEMINFO_PAGE_TABLES = 13;
  
  public static final int MEMINFO_SHMEM = 4;
  
  public static final int MEMINFO_SLAB = 5;
  
  public static final int MEMINFO_SLAB_RECLAIMABLE = 6;
  
  public static final int MEMINFO_SLAB_UNRECLAIMABLE = 7;
  
  public static final int MEMINFO_SWAP_FREE = 9;
  
  public static final int MEMINFO_SWAP_TOTAL = 8;
  
  public static final int MEMINFO_TOTAL = 0;
  
  public static final int MEMINFO_VM_ALLOC_USED = 12;
  
  public static final int MEMINFO_ZRAM_TOTAL = 10;
  
  private static final int MIN_DEBUGGER_IDLE = 1300;
  
  public static final int SHOW_CLASSLOADER = 2;
  
  public static final int SHOW_FULL_DETAIL = 1;
  
  public static final int SHOW_INITIALIZED = 4;
  
  private static final int SPIN_DELAY = 200;
  
  private static final String SYSFS_QEMU_TRACE_STATE = "/sys/qemu_trace/state";
  
  private static final String TAG = "Debug";
  
  @Deprecated
  public static final int TRACE_COUNT_ALLOCS = 1;
  
  private static final TypedProperties debugProperties;
  
  private static volatile boolean mWaiting = false;
  
  class MemoryInfo implements Parcelable {
    public int otherSwappedOutPss;
    
    public int otherSwappedOut;
    
    public int otherSwappablePss;
    
    private int[] otherStats = new int[288];
    
    public int otherSharedDirty;
    
    public int otherSharedClean;
    
    public int otherRss;
    
    public int otherPss;
    
    public int otherPrivateDirty;
    
    public int otherPrivateClean;
    
    public int nativeSwappedOutPss;
    
    public int nativeSwappedOut;
    
    public int nativeSwappablePss;
    
    public int nativeSharedDirty;
    
    public int nativeSharedClean;
    
    public int nativeRss;
    
    public int nativePss;
    
    public int nativePrivateDirty;
    
    public int nativePrivateClean;
    
    public boolean hasSwappedOutPss;
    
    public int dalvikSwappedOutPss;
    
    public int dalvikSwappedOut;
    
    public int dalvikSwappablePss;
    
    public int dalvikSharedDirty;
    
    public int dalvikSharedClean;
    
    public int dalvikRss;
    
    public int dalvikPss;
    
    public int dalvikPrivateDirty;
    
    public int dalvikPrivateClean;
    
    public static final int OTHER_UNKNOWN_MAP = 13;
    
    public static final int OTHER_UNKNOWN_DEV = 5;
    
    public static final int OTHER_TTF = 9;
    
    public static final int OTHER_STACK = 1;
    
    public static final int OTHER_SO = 6;
    
    public static final int OTHER_OTHER_MEMTRACK = 16;
    
    public static final int OTHER_OAT = 11;
    
    public static final int OTHER_JAR = 7;
    
    public static final int OTHER_GRAPHICS = 14;
    
    public static final int OTHER_GL_DEV = 4;
    
    public static final int OTHER_GL = 15;
    
    public static final int OTHER_DVK_STAT_DEX_START = 10;
    
    public static final int OTHER_DVK_STAT_DEX_END = 12;
    
    public static final int OTHER_DVK_STAT_DALVIK_START = 0;
    
    public static final int OTHER_DVK_STAT_DALVIK_OTHER_START = 4;
    
    public static final int OTHER_DVK_STAT_DALVIK_OTHER_END = 9;
    
    public static final int OTHER_DVK_STAT_DALVIK_END = 3;
    
    public static final int OTHER_DVK_STAT_ART_START = 13;
    
    public static final int OTHER_DVK_STAT_ART_END = 14;
    
    public static final int OTHER_DEX_BOOT_VDEX = 27;
    
    public static final int OTHER_DEX_APP_VDEX = 29;
    
    public static final int OTHER_DEX_APP_DEX = 28;
    
    public static final int OTHER_DEX = 10;
    
    public static final int OTHER_DALVIK_ZYGOTE = 19;
    
    public static final int OTHER_DALVIK_OTHER_ZYGOTE_CODE_CACHE = 23;
    
    public static final int OTHER_DALVIK_OTHER_LINEARALLOC = 21;
    
    public static final int OTHER_DALVIK_OTHER_INDIRECT_REFERENCE_TABLE = 26;
    
    public static final int OTHER_DALVIK_OTHER_COMPILER_METADATA = 25;
    
    public static final int OTHER_DALVIK_OTHER_APP_CODE_CACHE = 24;
    
    public static final int OTHER_DALVIK_OTHER_ACCOUNTING = 22;
    
    public static final int OTHER_DALVIK_OTHER = 0;
    
    public static final int OTHER_DALVIK_NORMAL = 17;
    
    public static final int OTHER_DALVIK_NON_MOVING = 20;
    
    public static final int OTHER_DALVIK_LARGE = 18;
    
    public static final int OTHER_CURSOR = 2;
    
    public static final int OTHER_ASHMEM = 3;
    
    public static final int OTHER_ART_BOOT = 31;
    
    public static final int OTHER_ART_APP = 30;
    
    public static final int OTHER_ART = 12;
    
    public static final int OTHER_APK = 8;
    
    public static final int OFFSET_SWAPPED_OUT_PSS = 8;
    
    public static final int OFFSET_SWAPPED_OUT = 7;
    
    public static final int OFFSET_SWAPPABLE_PSS = 1;
    
    public static final int OFFSET_SHARED_DIRTY = 4;
    
    public static final int OFFSET_SHARED_CLEAN = 6;
    
    public static final int OFFSET_RSS = 2;
    
    public static final int OFFSET_PSS = 0;
    
    public static final int OFFSET_PRIVATE_DIRTY = 3;
    
    public static final int OFFSET_PRIVATE_CLEAN = 5;
    
    public static final int NUM_OTHER_STATS = 17;
    
    public static final int NUM_DVK_STATS = 15;
    
    public static final int NUM_CATEGORIES = 9;
    
    public static final int HEAP_UNKNOWN = 0;
    
    public static final int HEAP_NATIVE = 2;
    
    public static final int HEAP_DALVIK = 1;
    
    public void set(MemoryInfo param1MemoryInfo) {
      this.dalvikPss = param1MemoryInfo.dalvikPss;
      this.dalvikSwappablePss = param1MemoryInfo.dalvikSwappablePss;
      this.dalvikRss = param1MemoryInfo.dalvikRss;
      this.dalvikPrivateDirty = param1MemoryInfo.dalvikPrivateDirty;
      this.dalvikSharedDirty = param1MemoryInfo.dalvikSharedDirty;
      this.dalvikPrivateClean = param1MemoryInfo.dalvikPrivateClean;
      this.dalvikSharedClean = param1MemoryInfo.dalvikSharedClean;
      this.dalvikSwappedOut = param1MemoryInfo.dalvikSwappedOut;
      this.dalvikSwappedOutPss = param1MemoryInfo.dalvikSwappedOutPss;
      this.nativePss = param1MemoryInfo.nativePss;
      this.nativeSwappablePss = param1MemoryInfo.nativeSwappablePss;
      this.nativeRss = param1MemoryInfo.nativeRss;
      this.nativePrivateDirty = param1MemoryInfo.nativePrivateDirty;
      this.nativeSharedDirty = param1MemoryInfo.nativeSharedDirty;
      this.nativePrivateClean = param1MemoryInfo.nativePrivateClean;
      this.nativeSharedClean = param1MemoryInfo.nativeSharedClean;
      this.nativeSwappedOut = param1MemoryInfo.nativeSwappedOut;
      this.nativeSwappedOutPss = param1MemoryInfo.nativeSwappedOutPss;
      this.otherPss = param1MemoryInfo.otherPss;
      this.otherSwappablePss = param1MemoryInfo.otherSwappablePss;
      this.otherRss = param1MemoryInfo.otherRss;
      this.otherPrivateDirty = param1MemoryInfo.otherPrivateDirty;
      this.otherSharedDirty = param1MemoryInfo.otherSharedDirty;
      this.otherPrivateClean = param1MemoryInfo.otherPrivateClean;
      this.otherSharedClean = param1MemoryInfo.otherSharedClean;
      this.otherSwappedOut = param1MemoryInfo.otherSwappedOut;
      this.otherSwappedOutPss = param1MemoryInfo.otherSwappedOutPss;
      this.hasSwappedOutPss = param1MemoryInfo.hasSwappedOutPss;
      int[] arrayOfInt2 = param1MemoryInfo.otherStats, arrayOfInt1 = this.otherStats;
      System.arraycopy(arrayOfInt2, 0, arrayOfInt1, 0, arrayOfInt1.length);
    }
    
    public int getTotalPss() {
      return this.dalvikPss + this.nativePss + this.otherPss + getTotalSwappedOutPss();
    }
    
    public int getTotalUss() {
      return this.dalvikPrivateClean + this.dalvikPrivateDirty + this.nativePrivateClean + this.nativePrivateDirty + this.otherPrivateClean + this.otherPrivateDirty;
    }
    
    public int getTotalSwappablePss() {
      return this.dalvikSwappablePss + this.nativeSwappablePss + this.otherSwappablePss;
    }
    
    public int getTotalRss() {
      return this.dalvikRss + this.nativeRss + this.otherRss;
    }
    
    public int getTotalPrivateDirty() {
      return this.dalvikPrivateDirty + this.nativePrivateDirty + this.otherPrivateDirty;
    }
    
    public int getTotalSharedDirty() {
      return this.dalvikSharedDirty + this.nativeSharedDirty + this.otherSharedDirty;
    }
    
    public int getTotalPrivateClean() {
      return this.dalvikPrivateClean + this.nativePrivateClean + this.otherPrivateClean;
    }
    
    public int getTotalSharedClean() {
      return this.dalvikSharedClean + this.nativeSharedClean + this.otherSharedClean;
    }
    
    public int getTotalSwappedOut() {
      return this.dalvikSwappedOut + this.nativeSwappedOut + this.otherSwappedOut;
    }
    
    public int getTotalSwappedOutPss() {
      return this.dalvikSwappedOutPss + this.nativeSwappedOutPss + this.otherSwappedOutPss;
    }
    
    public int getOtherPss(int param1Int) {
      return this.otherStats[param1Int * 9 + 0];
    }
    
    public int getOtherSwappablePss(int param1Int) {
      return this.otherStats[param1Int * 9 + 1];
    }
    
    public int getOtherRss(int param1Int) {
      return this.otherStats[param1Int * 9 + 2];
    }
    
    public int getOtherPrivateDirty(int param1Int) {
      return this.otherStats[param1Int * 9 + 3];
    }
    
    public int getOtherSharedDirty(int param1Int) {
      return this.otherStats[param1Int * 9 + 4];
    }
    
    public int getOtherPrivateClean(int param1Int) {
      return this.otherStats[param1Int * 9 + 5];
    }
    
    public int getOtherPrivate(int param1Int) {
      return getOtherPrivateClean(param1Int) + getOtherPrivateDirty(param1Int);
    }
    
    public int getOtherSharedClean(int param1Int) {
      return this.otherStats[param1Int * 9 + 6];
    }
    
    public int getOtherSwappedOut(int param1Int) {
      return this.otherStats[param1Int * 9 + 7];
    }
    
    public int getOtherSwappedOutPss(int param1Int) {
      return this.otherStats[param1Int * 9 + 8];
    }
    
    public static String getOtherLabel(int param1Int) {
      switch (param1Int) {
        default:
          return "????";
        case 31:
          return ".Boot art";
        case 30:
          return ".App art";
        case 29:
          return ".App vdex";
        case 28:
          return ".App dex";
        case 27:
          return ".Boot vdex";
        case 26:
          return ".IndirectRef";
        case 25:
          return ".CompilerMetadata";
        case 24:
          return ".AppJIT";
        case 23:
          return ".ZygoteJIT";
        case 22:
          return ".GC";
        case 21:
          return ".LinearAlloc";
        case 20:
          return ".NonMoving";
        case 19:
          return ".Zygote";
        case 18:
          return ".LOS";
        case 17:
          return ".Heap";
        case 16:
          return "Other mtrack";
        case 15:
          return "GL mtrack";
        case 14:
          return "EGL mtrack";
        case 13:
          return "Other mmap";
        case 12:
          return ".art mmap";
        case 11:
          return ".oat mmap";
        case 10:
          return ".dex mmap";
        case 9:
          return ".ttf mmap";
        case 8:
          return ".apk mmap";
        case 7:
          return ".jar mmap";
        case 6:
          return ".so mmap";
        case 5:
          return "Other dev";
        case 4:
          return "Gfx dev";
        case 3:
          return "Ashmem";
        case 2:
          return "Cursor";
        case 1:
          return "Stack";
        case 0:
          break;
      } 
      return "Dalvik Other";
    }
    
    public String getMemoryStat(String param1String) {
      byte b;
      switch (param1String.hashCode()) {
        default:
          b = -1;
          break;
        case 2069370308:
          if (param1String.equals("summary.total-swap")) {
            b = 8;
            break;
          } 
        case 2016489427:
          if (param1String.equals("summary.graphics")) {
            b = 4;
            break;
          } 
        case 1640306485:
          if (param1String.equals("summary.code")) {
            b = 2;
            break;
          } 
        case 549300599:
          if (param1String.equals("summary.system")) {
            b = 6;
            break;
          } 
        case -675184064:
          if (param1String.equals("summary.stack")) {
            b = 3;
            break;
          } 
        case -1040176230:
          if (param1String.equals("summary.native-heap")) {
            b = 1;
            break;
          } 
        case -1086991874:
          if (param1String.equals("summary.private-other")) {
            b = 5;
            break;
          } 
        case -1318722433:
          if (param1String.equals("summary.total-pss")) {
            b = 7;
            break;
          } 
        case -1629983121:
          if (param1String.equals("summary.java-heap")) {
            b = 0;
            break;
          } 
      } 
      switch (b) {
        default:
          return null;
        case 8:
          return Integer.toString(getSummaryTotalSwap());
        case 7:
          return Integer.toString(getSummaryTotalPss());
        case 6:
          return Integer.toString(getSummarySystem());
        case 5:
          return Integer.toString(getSummaryPrivateOther());
        case 4:
          return Integer.toString(getSummaryGraphics());
        case 3:
          return Integer.toString(getSummaryStack());
        case 2:
          return Integer.toString(getSummaryCode());
        case 1:
          return Integer.toString(getSummaryNativeHeap());
        case 0:
          break;
      } 
      return Integer.toString(getSummaryJavaHeap());
    }
    
    public Map<String, String> getMemoryStats() {
      HashMap<Object, Object> hashMap = new HashMap<>();
      hashMap.put("summary.java-heap", Integer.toString(getSummaryJavaHeap()));
      hashMap.put("summary.native-heap", Integer.toString(getSummaryNativeHeap()));
      hashMap.put("summary.code", Integer.toString(getSummaryCode()));
      hashMap.put("summary.stack", Integer.toString(getSummaryStack()));
      hashMap.put("summary.graphics", Integer.toString(getSummaryGraphics()));
      hashMap.put("summary.private-other", Integer.toString(getSummaryPrivateOther()));
      hashMap.put("summary.system", Integer.toString(getSummarySystem()));
      hashMap.put("summary.total-pss", Integer.toString(getSummaryTotalPss()));
      hashMap.put("summary.total-swap", Integer.toString(getSummaryTotalSwap()));
      return (Map)hashMap;
    }
    
    public int getSummaryJavaHeap() {
      return this.dalvikPrivateDirty + getOtherPrivate(12);
    }
    
    public int getSummaryNativeHeap() {
      return this.nativePrivateDirty;
    }
    
    public int getSummaryCode() {
      int i = getOtherPrivate(6);
      int j = getOtherPrivate(7);
      int k = getOtherPrivate(8);
      int m = getOtherPrivate(9);
      int n = getOtherPrivate(10);
      int i1 = getOtherPrivate(11);
      int i2 = getOtherPrivate(23);
      int i3 = getOtherPrivate(24);
      return i + j + k + m + n + i1 + i2 + i3;
    }
    
    public int getSummaryStack() {
      return getOtherPrivateDirty(1);
    }
    
    public int getSummaryGraphics() {
      int i = getOtherPrivate(4);
      int j = getOtherPrivate(14);
      int k = getOtherPrivate(15);
      return i + j + k;
    }
    
    public int getSummaryPrivateOther() {
      int i = getTotalPrivateClean();
      int j = getTotalPrivateDirty();
      int k = getSummaryJavaHeap();
      int m = getSummaryNativeHeap();
      int n = getSummaryCode();
      int i1 = getSummaryStack();
      int i2 = getSummaryGraphics();
      return i + j - k - m - n - i1 - i2;
    }
    
    public int getSummarySystem() {
      int i = getTotalPss();
      int j = getTotalPrivateClean();
      int k = getTotalPrivateDirty();
      return i - j - k;
    }
    
    public int getSummaryJavaHeapRss() {
      return this.dalvikRss + getOtherRss(12);
    }
    
    public int getSummaryNativeHeapRss() {
      return this.nativeRss;
    }
    
    public int getSummaryCodeRss() {
      int i = getOtherRss(6);
      int j = getOtherRss(7);
      int k = getOtherRss(8);
      int m = getOtherRss(9);
      int n = getOtherRss(10);
      int i1 = getOtherRss(11);
      int i2 = getOtherRss(23);
      int i3 = getOtherRss(24);
      return i + j + k + m + n + i1 + i2 + i3;
    }
    
    public int getSummaryStackRss() {
      return getOtherRss(1);
    }
    
    public int getSummaryGraphicsRss() {
      int i = getOtherRss(4);
      int j = getOtherRss(14);
      int k = getOtherRss(15);
      return i + j + k;
    }
    
    public int getSummaryUnknownRss() {
      int i = getTotalRss();
      int j = getSummaryJavaHeapRss();
      int k = getSummaryNativeHeapRss();
      int m = getSummaryCodeRss();
      int n = getSummaryStackRss();
      int i1 = getSummaryGraphicsRss();
      return i - j - k - m - n - i1;
    }
    
    public int getSummaryTotalPss() {
      return getTotalPss();
    }
    
    public int getSummaryTotalSwap() {
      return getTotalSwappedOut();
    }
    
    public int getSummaryTotalSwapPss() {
      return getTotalSwappedOutPss();
    }
    
    public boolean hasSwappedOutPss() {
      return this.hasSwappedOutPss;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.dalvikPss);
      param1Parcel.writeInt(this.dalvikSwappablePss);
      param1Parcel.writeInt(this.dalvikRss);
      param1Parcel.writeInt(this.dalvikPrivateDirty);
      param1Parcel.writeInt(this.dalvikSharedDirty);
      param1Parcel.writeInt(this.dalvikPrivateClean);
      param1Parcel.writeInt(this.dalvikSharedClean);
      param1Parcel.writeInt(this.dalvikSwappedOut);
      param1Parcel.writeInt(this.dalvikSwappedOutPss);
      param1Parcel.writeInt(this.nativePss);
      param1Parcel.writeInt(this.nativeSwappablePss);
      param1Parcel.writeInt(this.nativeRss);
      param1Parcel.writeInt(this.nativePrivateDirty);
      param1Parcel.writeInt(this.nativeSharedDirty);
      param1Parcel.writeInt(this.nativePrivateClean);
      param1Parcel.writeInt(this.nativeSharedClean);
      param1Parcel.writeInt(this.nativeSwappedOut);
      param1Parcel.writeInt(this.nativeSwappedOutPss);
      param1Parcel.writeInt(this.otherPss);
      param1Parcel.writeInt(this.otherSwappablePss);
      param1Parcel.writeInt(this.otherRss);
      param1Parcel.writeInt(this.otherPrivateDirty);
      param1Parcel.writeInt(this.otherSharedDirty);
      param1Parcel.writeInt(this.otherPrivateClean);
      param1Parcel.writeInt(this.otherSharedClean);
      param1Parcel.writeInt(this.otherSwappedOut);
      param1Parcel.writeInt(this.hasSwappedOutPss);
      param1Parcel.writeInt(this.otherSwappedOutPss);
      param1Parcel.writeIntArray(this.otherStats);
    }
    
    public void readFromParcel(Parcel param1Parcel) {
      boolean bool;
      this.dalvikPss = param1Parcel.readInt();
      this.dalvikSwappablePss = param1Parcel.readInt();
      this.dalvikRss = param1Parcel.readInt();
      this.dalvikPrivateDirty = param1Parcel.readInt();
      this.dalvikSharedDirty = param1Parcel.readInt();
      this.dalvikPrivateClean = param1Parcel.readInt();
      this.dalvikSharedClean = param1Parcel.readInt();
      this.dalvikSwappedOut = param1Parcel.readInt();
      this.dalvikSwappedOutPss = param1Parcel.readInt();
      this.nativePss = param1Parcel.readInt();
      this.nativeSwappablePss = param1Parcel.readInt();
      this.nativeRss = param1Parcel.readInt();
      this.nativePrivateDirty = param1Parcel.readInt();
      this.nativeSharedDirty = param1Parcel.readInt();
      this.nativePrivateClean = param1Parcel.readInt();
      this.nativeSharedClean = param1Parcel.readInt();
      this.nativeSwappedOut = param1Parcel.readInt();
      this.nativeSwappedOutPss = param1Parcel.readInt();
      this.otherPss = param1Parcel.readInt();
      this.otherSwappablePss = param1Parcel.readInt();
      this.otherRss = param1Parcel.readInt();
      this.otherPrivateDirty = param1Parcel.readInt();
      this.otherSharedDirty = param1Parcel.readInt();
      this.otherPrivateClean = param1Parcel.readInt();
      this.otherSharedClean = param1Parcel.readInt();
      this.otherSwappedOut = param1Parcel.readInt();
      if (param1Parcel.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.hasSwappedOutPss = bool;
      this.otherSwappedOutPss = param1Parcel.readInt();
      this.otherStats = param1Parcel.createIntArray();
    }
    
    public static final Parcelable.Creator<MemoryInfo> CREATOR = new Parcelable.Creator<MemoryInfo>() {
        public Debug.MemoryInfo createFromParcel(Parcel param2Parcel) {
          return new Debug.MemoryInfo();
        }
        
        public Debug.MemoryInfo[] newArray(int param2Int) {
          return new Debug.MemoryInfo[param2Int];
        }
      };
    
    private MemoryInfo(Debug this$0) {
      readFromParcel((Parcel)this$0);
    }
    
    public MemoryInfo() {}
  }
  
  public static void waitForDebugger() {
    if (!VMDebug.isDebuggingEnabled())
      return; 
    if (isDebuggerConnected())
      return; 
    System.out.println("Sending WAIT chunk");
    Chunk chunk = new Chunk(ChunkHandler.type("WAIT"), new byte[] { 0 }, 0, 1);
    DdmServer.sendChunk(chunk);
    mWaiting = true;
    while (!isDebuggerConnected()) {
      try {
        Thread.sleep(200L);
      } catch (InterruptedException interruptedException) {}
    } 
    mWaiting = false;
    System.out.println("Debugger has connected");
    while (true) {
      long l = VMDebug.lastDebuggerActivity();
      if (l < 0L) {
        System.out.println("debugger detached?");
        break;
      } 
      if (l < 1300L) {
        System.out.println("waiting for debugger to settle...");
        try {
          Thread.sleep(200L);
        } catch (InterruptedException interruptedException) {}
        continue;
      } 
      PrintStream printStream = System.out;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("debugger has settled (");
      stringBuilder.append(l);
      stringBuilder.append(")");
      printStream.println(stringBuilder.toString());
      break;
    } 
  }
  
  public static boolean waitingForDebugger() {
    return mWaiting;
  }
  
  public static boolean isDebuggerConnected() {
    return VMDebug.isDebuggerConnected();
  }
  
  public static String[] getVmFeatureList() {
    return VMDebug.getVmFeatureList();
  }
  
  @Deprecated
  public static void changeDebugPort(int paramInt) {}
  
  public static void startNativeTracing() {
    // Byte code:
    //   0: aconst_null
    //   1: astore_0
    //   2: aconst_null
    //   3: astore_1
    //   4: aload_1
    //   5: astore_2
    //   6: aload_0
    //   7: astore_3
    //   8: new java/io/FileOutputStream
    //   11: astore #4
    //   13: aload_1
    //   14: astore_2
    //   15: aload_0
    //   16: astore_3
    //   17: aload #4
    //   19: ldc '/sys/qemu_trace/state'
    //   21: invokespecial <init> : (Ljava/lang/String;)V
    //   24: aload_1
    //   25: astore_2
    //   26: aload_0
    //   27: astore_3
    //   28: new com/android/internal/util/FastPrintWriter
    //   31: astore #5
    //   33: aload_1
    //   34: astore_2
    //   35: aload_0
    //   36: astore_3
    //   37: aload #5
    //   39: aload #4
    //   41: invokespecial <init> : (Ljava/io/OutputStream;)V
    //   44: aload #5
    //   46: astore_0
    //   47: aload_0
    //   48: astore_2
    //   49: aload_0
    //   50: astore_3
    //   51: aload_0
    //   52: ldc_w '1'
    //   55: invokevirtual println : (Ljava/lang/String;)V
    //   58: aload_0
    //   59: invokevirtual close : ()V
    //   62: goto -> 86
    //   65: astore_0
    //   66: aload_2
    //   67: ifnull -> 74
    //   70: aload_2
    //   71: invokevirtual close : ()V
    //   74: aload_0
    //   75: athrow
    //   76: astore_0
    //   77: aload_3
    //   78: ifnull -> 86
    //   81: aload_3
    //   82: astore_0
    //   83: goto -> 58
    //   86: invokestatic startEmulatorTracing : ()V
    //   89: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1111	-> 0
    //   #1113	-> 4
    //   #1114	-> 24
    //   #1115	-> 47
    //   #1118	-> 58
    //   #1119	-> 58
    //   #1118	-> 65
    //   #1119	-> 70
    //   #1120	-> 74
    //   #1116	-> 76
    //   #1118	-> 77
    //   #1119	-> 81
    //   #1122	-> 86
    //   #1123	-> 89
    // Exception table:
    //   from	to	target	type
    //   8	13	76	java/lang/Exception
    //   8	13	65	finally
    //   17	24	76	java/lang/Exception
    //   17	24	65	finally
    //   28	33	76	java/lang/Exception
    //   28	33	65	finally
    //   37	44	76	java/lang/Exception
    //   37	44	65	finally
    //   51	58	76	java/lang/Exception
    //   51	58	65	finally
  }
  
  public static void stopNativeTracing() {
    VMDebug.stopEmulatorTracing();
    FastPrintWriter fastPrintWriter1 = null;
    null = null;
    FastPrintWriter fastPrintWriter2 = null, fastPrintWriter3 = fastPrintWriter1;
    try {
      FileOutputStream fileOutputStream = new FileOutputStream();
      fastPrintWriter2 = null;
      fastPrintWriter3 = fastPrintWriter1;
      this("/sys/qemu_trace/state");
      fastPrintWriter2 = null;
      fastPrintWriter3 = fastPrintWriter1;
      FastPrintWriter fastPrintWriter = new FastPrintWriter();
      fastPrintWriter2 = null;
      fastPrintWriter3 = fastPrintWriter1;
      this(fileOutputStream);
      null = fastPrintWriter;
      fastPrintWriter2 = null;
      fastPrintWriter3 = null;
      null.println("0");
    } catch (Exception exception) {
      FastPrintWriter fastPrintWriter;
      if (fastPrintWriter3 != null) {
        fastPrintWriter = fastPrintWriter3;
      } else {
        return;
      } 
    } finally {
      if (fastPrintWriter2 != null)
        fastPrintWriter2.close(); 
    } 
  }
  
  public static void enableEmulatorTraceOutput() {
    VMDebug.startEmulatorTracing();
  }
  
  public static void startMethodTracing() {
    VMDebug.startMethodTracing(fixTracePath(null), 0, 0, false, 0);
  }
  
  public static void startMethodTracing(String paramString) {
    startMethodTracing(paramString, 0, 0);
  }
  
  public static void startMethodTracing(String paramString, int paramInt) {
    startMethodTracing(paramString, paramInt, 0);
  }
  
  public static void startMethodTracing(String paramString, int paramInt1, int paramInt2) {
    VMDebug.startMethodTracing(fixTracePath(paramString), paramInt1, paramInt2, false, 0);
  }
  
  public static void startMethodTracingSampling(String paramString, int paramInt1, int paramInt2) {
    VMDebug.startMethodTracing(fixTracePath(paramString), paramInt1, 0, true, paramInt2);
  }
  
  private static String fixTracePath(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: ifnull -> 16
    //   4: aload_0
    //   5: astore_1
    //   6: aload_0
    //   7: iconst_0
    //   8: invokevirtual charAt : (I)C
    //   11: bipush #47
    //   13: if_icmpeq -> 71
    //   16: invokestatic getInitialApplication : ()Landroid/app/Application;
    //   19: astore_1
    //   20: aload_1
    //   21: ifnull -> 33
    //   24: aload_1
    //   25: aconst_null
    //   26: invokevirtual getExternalFilesDir : (Ljava/lang/String;)Ljava/io/File;
    //   29: astore_1
    //   30: goto -> 37
    //   33: invokestatic getExternalStorageDirectory : ()Ljava/io/File;
    //   36: astore_1
    //   37: aload_0
    //   38: ifnonnull -> 58
    //   41: new java/io/File
    //   44: dup
    //   45: aload_1
    //   46: ldc 'dmtrace'
    //   48: invokespecial <init> : (Ljava/io/File;Ljava/lang/String;)V
    //   51: invokevirtual getAbsolutePath : ()Ljava/lang/String;
    //   54: astore_1
    //   55: goto -> 71
    //   58: new java/io/File
    //   61: dup
    //   62: aload_1
    //   63: aload_0
    //   64: invokespecial <init> : (Ljava/io/File;Ljava/lang/String;)V
    //   67: invokevirtual getAbsolutePath : ()Ljava/lang/String;
    //   70: astore_1
    //   71: aload_1
    //   72: astore_0
    //   73: aload_1
    //   74: ldc '.trace'
    //   76: invokevirtual endsWith : (Ljava/lang/String;)Z
    //   79: ifne -> 108
    //   82: new java/lang/StringBuilder
    //   85: dup
    //   86: invokespecial <init> : ()V
    //   89: astore_0
    //   90: aload_0
    //   91: aload_1
    //   92: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   95: pop
    //   96: aload_0
    //   97: ldc '.trace'
    //   99: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   102: pop
    //   103: aload_0
    //   104: invokevirtual toString : ()Ljava/lang/String;
    //   107: astore_0
    //   108: aload_0
    //   109: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1316	-> 0
    //   #1317	-> 16
    //   #1319	-> 20
    //   #1320	-> 24
    //   #1322	-> 33
    //   #1325	-> 37
    //   #1326	-> 41
    //   #1328	-> 58
    //   #1331	-> 71
    //   #1332	-> 82
    //   #1334	-> 108
  }
  
  public static void startMethodTracing(String paramString, FileDescriptor paramFileDescriptor, int paramInt1, int paramInt2, boolean paramBoolean) {
    VMDebug.startMethodTracing(paramString, paramFileDescriptor, paramInt1, paramInt2, false, 0, paramBoolean);
  }
  
  public static void startMethodTracingDdms(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3) {
    VMDebug.startMethodTracingDdms(paramInt1, paramInt2, paramBoolean, paramInt3);
  }
  
  public static int getMethodTracingMode() {
    return VMDebug.getMethodTracingMode();
  }
  
  public static void stopMethodTracing() {
    VMDebug.stopMethodTracing();
  }
  
  public static long threadCpuTimeNanos() {
    return VMDebug.threadCpuTimeNanos();
  }
  
  @Deprecated
  public static void startAllocCounting() {
    VMDebug.startAllocCounting();
  }
  
  @Deprecated
  public static void stopAllocCounting() {
    VMDebug.stopAllocCounting();
  }
  
  @Deprecated
  public static int getGlobalAllocCount() {
    return VMDebug.getAllocCount(1);
  }
  
  @Deprecated
  public static void resetGlobalAllocCount() {
    VMDebug.resetAllocCount(1);
  }
  
  @Deprecated
  public static int getGlobalAllocSize() {
    return VMDebug.getAllocCount(2);
  }
  
  @Deprecated
  public static void resetGlobalAllocSize() {
    VMDebug.resetAllocCount(2);
  }
  
  @Deprecated
  public static int getGlobalFreedCount() {
    return VMDebug.getAllocCount(4);
  }
  
  @Deprecated
  public static void resetGlobalFreedCount() {
    VMDebug.resetAllocCount(4);
  }
  
  @Deprecated
  public static int getGlobalFreedSize() {
    return VMDebug.getAllocCount(8);
  }
  
  @Deprecated
  public static void resetGlobalFreedSize() {
    VMDebug.resetAllocCount(8);
  }
  
  @Deprecated
  public static int getGlobalGcInvocationCount() {
    return VMDebug.getAllocCount(16);
  }
  
  @Deprecated
  public static void resetGlobalGcInvocationCount() {
    VMDebug.resetAllocCount(16);
  }
  
  @Deprecated
  public static int getGlobalClassInitCount() {
    return VMDebug.getAllocCount(32);
  }
  
  @Deprecated
  public static void resetGlobalClassInitCount() {
    VMDebug.resetAllocCount(32);
  }
  
  @Deprecated
  public static int getGlobalClassInitTime() {
    return VMDebug.getAllocCount(64);
  }
  
  @Deprecated
  public static void resetGlobalClassInitTime() {
    VMDebug.resetAllocCount(64);
  }
  
  @Deprecated
  public static int getGlobalExternalAllocCount() {
    return 0;
  }
  
  @Deprecated
  public static void resetGlobalExternalAllocSize() {}
  
  @Deprecated
  public static void resetGlobalExternalAllocCount() {}
  
  @Deprecated
  public static int getGlobalExternalAllocSize() {
    return 0;
  }
  
  @Deprecated
  public static int getGlobalExternalFreedCount() {
    return 0;
  }
  
  @Deprecated
  public static void resetGlobalExternalFreedCount() {}
  
  @Deprecated
  public static int getGlobalExternalFreedSize() {
    return 0;
  }
  
  @Deprecated
  public static void resetGlobalExternalFreedSize() {}
  
  @Deprecated
  public static int getThreadAllocCount() {
    return VMDebug.getAllocCount(65536);
  }
  
  @Deprecated
  public static void resetThreadAllocCount() {
    VMDebug.resetAllocCount(65536);
  }
  
  @Deprecated
  public static int getThreadAllocSize() {
    return VMDebug.getAllocCount(131072);
  }
  
  @Deprecated
  public static void resetThreadAllocSize() {
    VMDebug.resetAllocCount(131072);
  }
  
  @Deprecated
  public static int getThreadExternalAllocCount() {
    return 0;
  }
  
  @Deprecated
  public static void resetThreadExternalAllocCount() {}
  
  @Deprecated
  public static int getThreadExternalAllocSize() {
    return 0;
  }
  
  @Deprecated
  public static void resetThreadExternalAllocSize() {}
  
  @Deprecated
  public static int getThreadGcInvocationCount() {
    return VMDebug.getAllocCount(1048576);
  }
  
  @Deprecated
  public static void resetThreadGcInvocationCount() {
    VMDebug.resetAllocCount(1048576);
  }
  
  @Deprecated
  public static void resetAllCounts() {
    VMDebug.resetAllocCount(-1);
  }
  
  public static String getRuntimeStat(String paramString) {
    return VMDebug.getRuntimeStat(paramString);
  }
  
  public static Map<String, String> getRuntimeStats() {
    return VMDebug.getRuntimeStats();
  }
  
  @Deprecated
  public static int setAllocationLimit(int paramInt) {
    return -1;
  }
  
  @Deprecated
  public static int setGlobalAllocationLimit(int paramInt) {
    return -1;
  }
  
  public static void printLoadedClasses(int paramInt) {
    VMDebug.printLoadedClasses(paramInt);
  }
  
  public static int getLoadedClassCount() {
    return VMDebug.getLoadedClassCount();
  }
  
  public static void dumpHprofData(String paramString) throws IOException {
    VMDebug.dumpHprofData(paramString);
  }
  
  public static void dumpHprofData(String paramString, FileDescriptor paramFileDescriptor) throws IOException {
    VMDebug.dumpHprofData(paramString, paramFileDescriptor);
  }
  
  public static void dumpHprofDataDdms() {
    VMDebug.dumpHprofDataDdms();
  }
  
  public static long countInstancesOfClass(Class paramClass) {
    return VMDebug.countInstancesOfClass(paramClass, true);
  }
  
  public static final boolean cacheRegisterMap(String paramString) {
    return VMDebug.cacheRegisterMap(paramString);
  }
  
  public static final void dumpReferenceTables() {
    VMDebug.dumpReferenceTables();
  }
  
  @Deprecated
  public static class InstructionCount {
    public boolean resetAndStart() {
      return false;
    }
    
    public boolean collect() {
      return false;
    }
    
    public int globalTotal() {
      return 0;
    }
    
    public int globalMethodInvocations() {
      return 0;
    }
  }
  
  static {
    debugProperties = null;
  }
  
  private static boolean fieldTypeMatches(Field paramField, Class<?> paramClass) {
    Class<?> clazz = paramField.getType();
    boolean bool = true;
    if (clazz == paramClass)
      return true; 
    try {
      Field field = paramClass.getField("TYPE");
      try {
        Class<?> clazz1 = (Class)field.get(null);
        if (clazz != clazz1)
          bool = false; 
        return bool;
      } catch (IllegalAccessException illegalAccessException) {
        return false;
      } 
    } catch (NoSuchFieldException noSuchFieldException) {
      return false;
    } 
  }
  
  private static void modifyFieldIfSet(Field paramField, TypedProperties paramTypedProperties, String paramString) {
    StringBuilder stringBuilder1, stringBuilder2;
    if (paramField.getType() == String.class) {
      int i = paramTypedProperties.getStringInfo(paramString);
      if (i != -2) {
        if (i != -1) {
          if (i != 0) {
            if (i != 1) {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Unexpected getStringInfo(");
              stringBuilder1.append(paramString);
              stringBuilder1.append(") return value ");
              stringBuilder1.append(i);
              throw new IllegalStateException(stringBuilder1.toString());
            } 
          } else {
            try {
              stringBuilder1.set(null, null);
              return;
            } catch (IllegalAccessException illegalAccessException) {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Cannot set field for ");
              stringBuilder1.append(paramString);
              throw new IllegalArgumentException(stringBuilder1.toString(), illegalAccessException);
            } 
          } 
        } else {
          return;
        } 
      } else {
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Type of ");
        stringBuilder2.append(paramString);
        stringBuilder2.append("  does not match field type (");
        stringBuilder2.append(stringBuilder1.getType());
        stringBuilder2.append(")");
        throw new IllegalArgumentException(stringBuilder2.toString());
      } 
    } 
    Object object = stringBuilder2.get(paramString);
    if (object != null)
      if (fieldTypeMatches((Field)stringBuilder1, object.getClass())) {
        try {
          stringBuilder1.set(null, object);
        } catch (IllegalAccessException illegalAccessException) {
          object = new StringBuilder();
          object.append("Cannot set field for ");
          object.append(paramString);
          throw new IllegalArgumentException(object.toString(), illegalAccessException);
        } 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Type of ");
        stringBuilder.append(paramString);
        stringBuilder.append(" (");
        stringBuilder.append(object.getClass());
        stringBuilder.append(")  does not match field type (");
        stringBuilder.append(illegalAccessException.getType());
        stringBuilder.append(")");
        throw new IllegalArgumentException(stringBuilder.toString());
      }  
  }
  
  public static void setFieldsOn(Class<?> paramClass) {
    setFieldsOn(paramClass, false);
  }
  
  public static void setFieldsOn(Class<?> paramClass, boolean paramBoolean) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("setFieldsOn(");
    if (paramClass == null) {
      str = "null";
    } else {
      str = str.getName();
    } 
    stringBuilder.append(str);
    stringBuilder.append(") called in non-DEBUG build");
    String str = stringBuilder.toString();
    Log.wtf("Debug", str);
  }
  
  public static boolean dumpService(String paramString, FileDescriptor paramFileDescriptor, String[] paramArrayOfString) {
    StringBuilder stringBuilder;
    IBinder iBinder = ServiceManager.getService(paramString);
    if (iBinder == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Can't find service to dump: ");
      stringBuilder.append(paramString);
      Log.e("Debug", stringBuilder.toString());
      return false;
    } 
    try {
      iBinder.dump((FileDescriptor)stringBuilder, paramArrayOfString);
      return true;
    } catch (RemoteException remoteException) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Can't dump service: ");
      stringBuilder.append(paramString);
      Log.e("Debug", stringBuilder.toString(), (Throwable)remoteException);
      return false;
    } 
  }
  
  private static String getCaller(StackTraceElement[] paramArrayOfStackTraceElement, int paramInt) {
    if (paramInt + 4 >= paramArrayOfStackTraceElement.length)
      return "<bottom of call stack>"; 
    StackTraceElement stackTraceElement = paramArrayOfStackTraceElement[paramInt + 4];
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(stackTraceElement.getClassName());
    stringBuilder.append(".");
    stringBuilder.append(stackTraceElement.getMethodName());
    stringBuilder.append(":");
    stringBuilder.append(stackTraceElement.getLineNumber());
    return stringBuilder.toString();
  }
  
  public static String getCallers(int paramInt) {
    StackTraceElement[] arrayOfStackTraceElement = Thread.currentThread().getStackTrace();
    StringBuffer stringBuffer = new StringBuffer();
    for (byte b = 0; b < paramInt; b++) {
      stringBuffer.append(getCaller(arrayOfStackTraceElement, b));
      stringBuffer.append(" ");
    } 
    return stringBuffer.toString();
  }
  
  public static String getCallers(int paramInt1, int paramInt2) {
    StackTraceElement[] arrayOfStackTraceElement = Thread.currentThread().getStackTrace();
    StringBuffer stringBuffer = new StringBuffer();
    for (int i = paramInt1; i < paramInt2 + paramInt1; i++) {
      stringBuffer.append(getCaller(arrayOfStackTraceElement, i));
      stringBuffer.append(" ");
    } 
    return stringBuffer.toString();
  }
  
  public static String getCallers(int paramInt, String paramString) {
    StackTraceElement[] arrayOfStackTraceElement = Thread.currentThread().getStackTrace();
    StringBuffer stringBuffer = new StringBuffer();
    for (byte b = 0; b < paramInt; b++) {
      stringBuffer.append(paramString);
      stringBuffer.append(getCaller(arrayOfStackTraceElement, b));
      stringBuffer.append("\n");
    } 
    return stringBuffer.toString();
  }
  
  public static String getCaller() {
    return getCaller(Thread.currentThread().getStackTrace(), 0);
  }
  
  public static void attachJvmtiAgent(String paramString1, String paramString2, ClassLoader paramClassLoader) throws IOException {
    Preconditions.checkNotNull(paramString1);
    Preconditions.checkArgument(paramString1.contains("=") ^ true);
    if (paramString2 == null) {
      VMDebug.attachAgent(paramString1, paramClassLoader);
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("=");
      stringBuilder.append(paramString2);
      VMDebug.attachAgent(stringBuilder.toString(), paramClassLoader);
    } 
  }
  
  public static native boolean dumpJavaBacktraceToFileTimeout(int paramInt1, String paramString, int paramInt2);
  
  public static native boolean dumpNativeBacktraceToFileTimeout(int paramInt1, String paramString, int paramInt2);
  
  public static native void dumpNativeHeap(FileDescriptor paramFileDescriptor);
  
  public static native void dumpNativeMallocInfo(FileDescriptor paramFileDescriptor);
  
  public static final native int getBinderDeathObjectCount();
  
  public static final native int getBinderLocalObjectCount();
  
  public static final native int getBinderProxyObjectCount();
  
  public static native int getBinderReceivedTransactions();
  
  public static native int getBinderSentTransactions();
  
  public static native long getIonHeapsSizeKb();
  
  public static native long getIonMappedSizeKb();
  
  public static native void getIonOrphanedProcess(String[] paramArrayOfString);
  
  public static native long getIonPoolsSizeKb();
  
  public static native void getMemInfo(long[] paramArrayOflong);
  
  public static native void getMemoryInfo(MemoryInfo paramMemoryInfo);
  
  public static native boolean getMemoryInfo(int paramInt, MemoryInfo paramMemoryInfo);
  
  public static native long getNativeHeapAllocatedSize();
  
  public static native long getNativeHeapFreeSize();
  
  public static native long getNativeHeapSize();
  
  public static native long getPss();
  
  public static native long getPss(int paramInt, long[] paramArrayOflong1, long[] paramArrayOflong2);
  
  public static native String getUnreachableMemory(int paramInt, boolean paramBoolean);
  
  public static native long getZramFreeKb();
  
  public static native boolean isVmapStack();
  
  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.FIELD})
  public static @interface DebugProperty {}
}
