package android.os;

import android.annotation.SystemApi;
import android.os.connectivity.CellularBatteryStats;
import android.os.connectivity.WifiBatteryStats;
import com.android.internal.app.IBatteryStats;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class BatteryStatsManager {
  public static final int NUM_WIFI_STATES = 8;
  
  public static final int NUM_WIFI_SUPPL_STATES = 13;
  
  public static final int WIFI_STATE_OFF = 0;
  
  public static final int WIFI_STATE_OFF_SCANNING = 1;
  
  public static final int WIFI_STATE_ON_CONNECTED_P2P = 5;
  
  public static final int WIFI_STATE_ON_CONNECTED_STA = 4;
  
  public static final int WIFI_STATE_ON_CONNECTED_STA_P2P = 6;
  
  public static final int WIFI_STATE_ON_DISCONNECTED = 3;
  
  public static final int WIFI_STATE_ON_NO_NETWORKS = 2;
  
  public static final int WIFI_STATE_SOFT_AP = 7;
  
  public static final int WIFI_SUPPL_STATE_ASSOCIATED = 7;
  
  public static final int WIFI_SUPPL_STATE_ASSOCIATING = 6;
  
  public static final int WIFI_SUPPL_STATE_AUTHENTICATING = 5;
  
  public static final int WIFI_SUPPL_STATE_COMPLETED = 10;
  
  public static final int WIFI_SUPPL_STATE_DISCONNECTED = 1;
  
  public static final int WIFI_SUPPL_STATE_DORMANT = 11;
  
  public static final int WIFI_SUPPL_STATE_FOUR_WAY_HANDSHAKE = 8;
  
  public static final int WIFI_SUPPL_STATE_GROUP_HANDSHAKE = 9;
  
  public static final int WIFI_SUPPL_STATE_INACTIVE = 3;
  
  public static final int WIFI_SUPPL_STATE_INTERFACE_DISABLED = 2;
  
  public static final int WIFI_SUPPL_STATE_INVALID = 0;
  
  public static final int WIFI_SUPPL_STATE_SCANNING = 4;
  
  public static final int WIFI_SUPPL_STATE_UNINITIALIZED = 12;
  
  private final IBatteryStats mBatteryStats;
  
  public BatteryStatsManager(IBatteryStats paramIBatteryStats) {
    this.mBatteryStats = paramIBatteryStats;
  }
  
  public void reportWifiRssiChanged(int paramInt) {
    try {
      this.mBatteryStats.noteWifiRssiChanged(paramInt);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportWifiOn() {
    try {
      this.mBatteryStats.noteWifiOn();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportWifiOff() {
    try {
      this.mBatteryStats.noteWifiOff();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportWifiState(int paramInt, String paramString) {
    try {
      this.mBatteryStats.noteWifiState(paramInt, paramString);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportWifiScanStartedFromSource(WorkSource paramWorkSource) {
    try {
      this.mBatteryStats.noteWifiScanStartedFromSource(paramWorkSource);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportWifiScanStoppedFromSource(WorkSource paramWorkSource) {
    try {
      this.mBatteryStats.noteWifiScanStoppedFromSource(paramWorkSource);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportWifiBatchedScanStartedFromSource(WorkSource paramWorkSource, int paramInt) {
    try {
      this.mBatteryStats.noteWifiBatchedScanStartedFromSource(paramWorkSource, paramInt);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportWifiBatchedScanStoppedFromSource(WorkSource paramWorkSource) {
    try {
      this.mBatteryStats.noteWifiBatchedScanStoppedFromSource(paramWorkSource);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public CellularBatteryStats getCellularBatteryStats() {
    try {
      return this.mBatteryStats.getCellularBatteryStats();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return null;
    } 
  }
  
  public WifiBatteryStats getWifiBatteryStats() {
    try {
      return this.mBatteryStats.getWifiBatteryStats();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return null;
    } 
  }
  
  public void reportFullWifiLockAcquiredFromSource(WorkSource paramWorkSource) {
    try {
      this.mBatteryStats.noteFullWifiLockAcquiredFromSource(paramWorkSource);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportFullWifiLockReleasedFromSource(WorkSource paramWorkSource) {
    try {
      this.mBatteryStats.noteFullWifiLockReleasedFromSource(paramWorkSource);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportWifiSupplicantStateChanged(int paramInt, boolean paramBoolean) {
    try {
      this.mBatteryStats.noteWifiSupplicantStateChanged(paramInt, paramBoolean);
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportWifiMulticastEnabled(WorkSource paramWorkSource) {
    try {
      this.mBatteryStats.noteWifiMulticastEnabled(paramWorkSource.getAttributionUid());
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportWifiMulticastDisabled(WorkSource paramWorkSource) {
    try {
      this.mBatteryStats.noteWifiMulticastDisabled(paramWorkSource.getAttributionUid());
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class WifiState implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class WifiSupplState implements Annotation {}
}
