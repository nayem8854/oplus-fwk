package android.os;

import android.annotation.SystemApi;

@SystemApi
public abstract class UpdateEngineCallback {
  public abstract void onPayloadApplicationComplete(int paramInt);
  
  public abstract void onStatusUpdate(int paramInt, float paramFloat);
}
