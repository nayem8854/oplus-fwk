package android.os;

import java.util.Map;

public class TransactionTracker {
  private Map<String, Long> mTraces;
  
  private void resetTraces() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new java/util/HashMap
    //   5: astore_1
    //   6: aload_1
    //   7: invokespecial <init> : ()V
    //   10: aload_0
    //   11: aload_1
    //   12: putfield mTraces : Ljava/util/Map;
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_1
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #37	-> 0
    //   #38	-> 2
    //   #39	-> 15
    //   #40	-> 17
    //   #39	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	15	18	finally
    //   15	17	18	finally
    //   19	21	18	finally
  }
  
  TransactionTracker() {
    resetTraces();
  }
  
  public void addTrace(Throwable paramThrowable) {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic getStackTraceString : (Ljava/lang/Throwable;)Ljava/lang/String;
    //   4: astore_1
    //   5: aload_0
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mTraces : Ljava/util/Map;
    //   11: aload_1
    //   12: invokeinterface containsKey : (Ljava/lang/Object;)Z
    //   17: ifeq -> 55
    //   20: aload_0
    //   21: getfield mTraces : Ljava/util/Map;
    //   24: aload_1
    //   25: aload_0
    //   26: getfield mTraces : Ljava/util/Map;
    //   29: aload_1
    //   30: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   35: checkcast java/lang/Long
    //   38: invokevirtual longValue : ()J
    //   41: lconst_1
    //   42: ladd
    //   43: invokestatic valueOf : (J)Ljava/lang/Long;
    //   46: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   51: pop
    //   52: goto -> 70
    //   55: aload_0
    //   56: getfield mTraces : Ljava/util/Map;
    //   59: aload_1
    //   60: lconst_1
    //   61: invokestatic valueOf : (J)Ljava/lang/Long;
    //   64: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   69: pop
    //   70: aload_0
    //   71: monitorexit
    //   72: return
    //   73: astore_1
    //   74: aload_0
    //   75: monitorexit
    //   76: aload_1
    //   77: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #47	-> 0
    //   #48	-> 5
    //   #49	-> 7
    //   #50	-> 20
    //   #52	-> 55
    //   #54	-> 70
    //   #55	-> 72
    //   #54	-> 73
    // Exception table:
    //   from	to	target	type
    //   7	20	73	finally
    //   20	52	73	finally
    //   55	70	73	finally
    //   70	72	73	finally
    //   74	76	73	finally
  }
  
  public void writeTracesToFile(ParcelFileDescriptor paramParcelFileDescriptor) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mTraces : Ljava/util/Map;
    //   4: invokeinterface isEmpty : ()Z
    //   9: ifeq -> 13
    //   12: return
    //   13: new com/android/internal/util/FastPrintWriter
    //   16: dup
    //   17: new java/io/FileOutputStream
    //   20: dup
    //   21: aload_1
    //   22: invokevirtual getFileDescriptor : ()Ljava/io/FileDescriptor;
    //   25: invokespecial <init> : (Ljava/io/FileDescriptor;)V
    //   28: invokespecial <init> : (Ljava/io/OutputStream;)V
    //   31: astore_1
    //   32: aload_0
    //   33: monitorenter
    //   34: aload_0
    //   35: getfield mTraces : Ljava/util/Map;
    //   38: invokeinterface keySet : ()Ljava/util/Set;
    //   43: invokeinterface iterator : ()Ljava/util/Iterator;
    //   48: astore_2
    //   49: aload_2
    //   50: invokeinterface hasNext : ()Z
    //   55: ifeq -> 152
    //   58: aload_2
    //   59: invokeinterface next : ()Ljava/lang/Object;
    //   64: checkcast java/lang/String
    //   67: astore_3
    //   68: new java/lang/StringBuilder
    //   71: astore #4
    //   73: aload #4
    //   75: invokespecial <init> : ()V
    //   78: aload #4
    //   80: ldc 'Count: '
    //   82: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   85: pop
    //   86: aload #4
    //   88: aload_0
    //   89: getfield mTraces : Ljava/util/Map;
    //   92: aload_3
    //   93: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   98: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   101: pop
    //   102: aload_1
    //   103: aload #4
    //   105: invokevirtual toString : ()Ljava/lang/String;
    //   108: invokevirtual println : (Ljava/lang/String;)V
    //   111: new java/lang/StringBuilder
    //   114: astore #4
    //   116: aload #4
    //   118: invokespecial <init> : ()V
    //   121: aload #4
    //   123: ldc 'Trace: '
    //   125: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   128: pop
    //   129: aload #4
    //   131: aload_3
    //   132: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   135: pop
    //   136: aload_1
    //   137: aload #4
    //   139: invokevirtual toString : ()Ljava/lang/String;
    //   142: invokevirtual println : (Ljava/lang/String;)V
    //   145: aload_1
    //   146: invokevirtual println : ()V
    //   149: goto -> 49
    //   152: aload_0
    //   153: monitorexit
    //   154: aload_1
    //   155: invokevirtual flush : ()V
    //   158: return
    //   159: astore_1
    //   160: aload_0
    //   161: monitorexit
    //   162: aload_1
    //   163: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #58	-> 0
    //   #59	-> 12
    //   #62	-> 13
    //   #63	-> 32
    //   #64	-> 34
    //   #65	-> 68
    //   #66	-> 111
    //   #67	-> 145
    //   #68	-> 149
    //   #69	-> 152
    //   #70	-> 154
    //   #71	-> 158
    //   #69	-> 159
    // Exception table:
    //   from	to	target	type
    //   34	49	159	finally
    //   49	68	159	finally
    //   68	111	159	finally
    //   111	145	159	finally
    //   145	149	159	finally
    //   152	154	159	finally
    //   160	162	159	finally
  }
  
  public void clearTraces() {
    resetTraces();
  }
}
