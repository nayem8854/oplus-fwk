package android.os;

import android.app.OplusActivityTaskManager;
import android.content.Context;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.KeyEvent;
import java.util.Map;

public class OplusKeyEventManager {
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  public int mVersion = 1;
  
  private final Map<OnKeyEventObserver, IOplusKeyEventObserver> mKeyEventObservers = (Map<OnKeyEventObserver, IOplusKeyEventObserver>)new ArrayMap();
  
  private final Map<String, IOplusKeyEventObserver> mKeyEventInterceptors = (Map<String, IOplusKeyEventObserver>)new ArrayMap();
  
  public static final int INTERCEPT_ALWAYS = 0;
  
  public static final int INTERCEPT_ONCE = 1;
  
  public static final int LISTEN_ALL_KEY_EVENT = 0;
  
  public static final int LISTEN_APP_SWITCH_KEY_EVENT = 4096;
  
  public static final int LISTEN_BACK_KEY_EVENT = 32;
  
  public static final int LISTEN_BRIGHTNESS_DOWN_KEY_EVENT = 32768;
  
  public static final int LISTEN_BRIGHTNESS_UP_KEY_EVENT = 16384;
  
  public static final int LISTEN_CAMERA_KEY_EVENT = 128;
  
  public static final int LISTEN_ENDCALL_KEY_EVENT = 65536;
  
  public static final int LISTEN_F4_KEY_EVENT = 64;
  
  public static final int LISTEN_HEADSETHOOK_KEY_EVENT = 1024;
  
  public static final int LISTEN_HOME_KEY_EVENT = 16;
  
  public static final int LISTEN_MENU_KEY_EVENT = 8;
  
  public static final int LISTEN_POWER_KEY_EVENT = 1;
  
  public static final int LISTEN_SLEEP_KEY_EVENT = 131072;
  
  public static final int LISTEN_VOLUME_DOWN_KEY_EVENT = 4;
  
  public static final int LISTEN_VOLUME_MUTE_KEY_EVENT = 2048;
  
  public static final int LISTEN_VOLUME_UP_KEY_EVENT = 2;
  
  public static final int LISTEN_WAKEUP_KEY_EVENT = 8192;
  
  public static final String TAG = "OplusKeyEventManager";
  
  private static volatile OplusKeyEventManager sInstance;
  
  private OplusActivityTaskManager mOAms;
  
  public static OplusKeyEventManager getInstance() {
    // Byte code:
    //   0: getstatic android/os/OplusKeyEventManager.sInstance : Landroid/os/OplusKeyEventManager;
    //   3: ifnonnull -> 39
    //   6: ldc android/os/OplusKeyEventManager
    //   8: monitorenter
    //   9: getstatic android/os/OplusKeyEventManager.sInstance : Landroid/os/OplusKeyEventManager;
    //   12: ifnonnull -> 27
    //   15: new android/os/OplusKeyEventManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic android/os/OplusKeyEventManager.sInstance : Landroid/os/OplusKeyEventManager;
    //   27: ldc android/os/OplusKeyEventManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc android/os/OplusKeyEventManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic android/os/OplusKeyEventManager.sInstance : Landroid/os/OplusKeyEventManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #66	-> 0
    //   #67	-> 6
    //   #68	-> 9
    //   #69	-> 15
    //   #71	-> 27
    //   #73	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusKeyEventManager() {
    this.mOAms = new OplusActivityTaskManager();
  }
  
  public boolean registerKeyEventObserver(Context paramContext, OnKeyEventObserver paramOnKeyEventObserver, int paramInt) {
    if (paramOnKeyEventObserver == null || paramContext == null) {
      Log.e("OplusKeyEventManager", "context is null or observer is null, registerKeyEventObserver failed.");
      return false;
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("start registerKeyEventObserver, pkg: ");
      stringBuilder.append(paramContext.getPackageName());
      Log.i("OplusKeyEventManager", stringBuilder.toString());
    } 
    synchronized (this.mKeyEventObservers) {
      if (this.mKeyEventObservers.get(paramOnKeyEventObserver) != null) {
        Log.e("OplusKeyEventManager", "already registered before");
        return false;
      } 
      OnKeyEventObserverDelegate onKeyEventObserverDelegate = new OnKeyEventObserverDelegate();
      this(this, paramOnKeyEventObserver);
      try {
        if (this.mOAms != null) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(paramOnKeyEventObserver.hashCode());
          stringBuilder.append(paramContext.getPackageName());
          String str = stringBuilder.toString();
          boolean bool = this.mOAms.registerKeyEventObserver(str, onKeyEventObserverDelegate, paramInt);
          if (bool)
            this.mKeyEventObservers.put(paramOnKeyEventObserver, onKeyEventObserverDelegate); 
          return bool;
        } 
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("registerKeyEventObserver RemoteException, err: ");
        stringBuilder.append(remoteException);
        Log.e("OplusKeyEventManager", stringBuilder.toString());
      } 
      return false;
    } 
  }
  
  public boolean unregisterKeyEventObserver(Context paramContext, OnKeyEventObserver paramOnKeyEventObserver) {
    if (paramOnKeyEventObserver == null || paramContext == null) {
      Log.e("OplusKeyEventManager", "context is null or observer is null, unregisterKeyEventObserver failed.");
      return false;
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("start unregisterKeyEventObserver, pkg: ");
      stringBuilder.append(paramContext.getPackageName());
      Log.i("OplusKeyEventManager", stringBuilder.toString());
    } 
    synchronized (this.mKeyEventObservers) {
      IOplusKeyEventObserver iOplusKeyEventObserver = this.mKeyEventObservers.get(paramOnKeyEventObserver);
      if (iOplusKeyEventObserver != null)
        try {
          if (this.mOAms != null) {
            this.mKeyEventObservers.remove(paramOnKeyEventObserver);
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append(paramOnKeyEventObserver.hashCode());
            stringBuilder.append(paramContext.getPackageName());
            String str = stringBuilder.toString();
            return this.mOAms.unregisterKeyEventObserver(str);
          } 
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("unregisterKeyEventObserver RemoteException, err: ");
          stringBuilder.append(remoteException);
          Log.e("OplusKeyEventManager", stringBuilder.toString());
        }  
      return false;
    } 
  }
  
  public int getVersion() {
    return this.mVersion;
  }
  
  public boolean registerKeyEventInterceptor(Context paramContext, String paramString, OnKeyEventObserver paramOnKeyEventObserver, ArrayMap<Integer, Integer> paramArrayMap) {
    OnKeyEventObserverDelegate onKeyEventObserverDelegate;
    if (paramContext == null || TextUtils.isEmpty(paramString) || paramArrayMap == null || paramArrayMap.isEmpty()) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("registerKeyEventInterceptor failed, interceptorKey: ");
      stringBuilder.append(paramString);
      stringBuilder.append(", configs: ");
      stringBuilder.append(paramArrayMap);
      Log.e("OplusKeyEventManager", stringBuilder.toString());
      return false;
    } 
    if (DEBUG) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("start registerKeyEventInterceptor, pkg: ");
      stringBuilder1.append(stringBuilder.getPackageName());
      stringBuilder1.append(", interceptorKey: ");
      stringBuilder1.append(paramString);
      Log.i("OplusKeyEventManager", stringBuilder1.toString());
    } 
    Map<String, IOplusKeyEventObserver> map = this.mKeyEventInterceptors;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/util/Map<[ObjectType{java/lang/String}, ObjectType{android/os/IOplusKeyEventObserver}]>}, name=null} */
    if (paramOnKeyEventObserver != null)
      try {
        if (this.mKeyEventInterceptors.containsKey(paramString)) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("interceptor already registered before, interceptorFingerPrint: ");
          stringBuilder.append(paramString);
          Log.e("OplusKeyEventManager", stringBuilder.toString());
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/util/Map<[ObjectType{java/lang/String}, ObjectType{android/os/IOplusKeyEventObserver}]>}, name=null} */
          return false;
        } 
      } finally {} 
    StringBuilder stringBuilder = null;
    if (paramOnKeyEventObserver != null) {
      onKeyEventObserverDelegate = new OnKeyEventObserverDelegate();
      this(this, paramOnKeyEventObserver);
    } 
    try {
      if (this.mOAms != null) {
        boolean bool = this.mOAms.registerKeyEventInterceptor(paramString, onKeyEventObserverDelegate, (Map)paramArrayMap);
        if (bool)
          this.mKeyEventInterceptors.put(paramString, onKeyEventObserverDelegate); 
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/util/Map<[ObjectType{java/lang/String}, ObjectType{android/os/IOplusKeyEventObserver}]>}, name=null} */
        return bool;
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append("registerKeyEventInterceptor RemoteException, err: ");
      stringBuilder1.append(remoteException);
      Log.e("OplusKeyEventManager", stringBuilder1.toString());
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/util/Map<[ObjectType{java/lang/String}, ObjectType{android/os/IOplusKeyEventObserver}]>}, name=null} */
    return false;
  }
  
  public boolean unregisterKeyEventInterceptor(Context paramContext, String paramString, OnKeyEventObserver paramOnKeyEventObserver) {
    StringBuilder stringBuilder;
    if (paramContext == null || TextUtils.isEmpty(paramString)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("context is null, unregisterKeyEventInterceptor failed, interceptorKey: ");
      stringBuilder.append(paramString);
      Log.e("OplusKeyEventManager", stringBuilder.toString());
      return false;
    } 
    if (DEBUG) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("start unregisterKeyEventInterceptor, pkg: ");
      stringBuilder1.append(stringBuilder.getPackageName());
      stringBuilder1.append(", interceptorKey: ");
      stringBuilder1.append(paramString);
      String str = stringBuilder1.toString();
      Log.i("OplusKeyEventManager", str);
    } 
    synchronized (this.mKeyEventInterceptors) {
      boolean bool = this.mKeyEventInterceptors.containsKey(paramString);
      if (bool)
        try {
          if (this.mOAms != null) {
            this.mKeyEventInterceptors.remove(paramString);
            bool = this.mOAms.unregisterKeyEventInterceptor(paramString);
            return bool;
          } 
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("unregisterKeyEventInterceptor RemoteException, err: ");
          stringBuilder1.append(remoteException);
          Log.e("OplusKeyEventManager", stringBuilder1.toString());
        }  
      return false;
    } 
  }
  
  public static interface OnKeyEventObserver {
    void onKeyEvent(KeyEvent param1KeyEvent);
  }
  
  class OnKeyEventObserverDelegate extends IOplusKeyEventObserver.Stub {
    private final OplusKeyEventManager.OnKeyEventObserver mObserver;
    
    final OplusKeyEventManager this$0;
    
    public OnKeyEventObserverDelegate(OplusKeyEventManager.OnKeyEventObserver param1OnKeyEventObserver) {
      this.mObserver = param1OnKeyEventObserver;
    }
    
    public void onKeyEvent(KeyEvent param1KeyEvent) {
      OplusKeyEventManager.OnKeyEventObserver onKeyEventObserver = this.mObserver;
      if (onKeyEventObserver != null)
        onKeyEventObserver.onKeyEvent(param1KeyEvent); 
    }
  }
}
