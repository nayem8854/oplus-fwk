package android.os;

public interface IIncidentReportStatusListener extends IInterface {
  public static final int STATUS_FINISHED = 2;
  
  public static final int STATUS_STARTING = 1;
  
  void onReportFailed() throws RemoteException;
  
  void onReportFinished() throws RemoteException;
  
  void onReportSectionStatus(int paramInt1, int paramInt2) throws RemoteException;
  
  void onReportStarted() throws RemoteException;
  
  class Default implements IIncidentReportStatusListener {
    public void onReportStarted() throws RemoteException {}
    
    public void onReportSectionStatus(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onReportFinished() throws RemoteException {}
    
    public void onReportFailed() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IIncidentReportStatusListener {
    private static final String DESCRIPTOR = "android.os.IIncidentReportStatusListener";
    
    static final int TRANSACTION_onReportFailed = 4;
    
    static final int TRANSACTION_onReportFinished = 3;
    
    static final int TRANSACTION_onReportSectionStatus = 2;
    
    static final int TRANSACTION_onReportStarted = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IIncidentReportStatusListener");
    }
    
    public static IIncidentReportStatusListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IIncidentReportStatusListener");
      if (iInterface != null && iInterface instanceof IIncidentReportStatusListener)
        return (IIncidentReportStatusListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "onReportFailed";
          } 
          return "onReportFinished";
        } 
        return "onReportSectionStatus";
      } 
      return "onReportStarted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.os.IIncidentReportStatusListener");
              return true;
            } 
            param1Parcel1.enforceInterface("android.os.IIncidentReportStatusListener");
            onReportFailed();
            return true;
          } 
          param1Parcel1.enforceInterface("android.os.IIncidentReportStatusListener");
          onReportFinished();
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.IIncidentReportStatusListener");
        param1Int2 = param1Parcel1.readInt();
        param1Int1 = param1Parcel1.readInt();
        onReportSectionStatus(param1Int2, param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IIncidentReportStatusListener");
      onReportStarted();
      return true;
    }
    
    private static class Proxy implements IIncidentReportStatusListener {
      public static IIncidentReportStatusListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IIncidentReportStatusListener";
      }
      
      public void onReportStarted() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IIncidentReportStatusListener");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IIncidentReportStatusListener.Stub.getDefaultImpl() != null) {
            IIncidentReportStatusListener.Stub.getDefaultImpl().onReportStarted();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onReportSectionStatus(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IIncidentReportStatusListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IIncidentReportStatusListener.Stub.getDefaultImpl() != null) {
            IIncidentReportStatusListener.Stub.getDefaultImpl().onReportSectionStatus(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onReportFinished() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IIncidentReportStatusListener");
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IIncidentReportStatusListener.Stub.getDefaultImpl() != null) {
            IIncidentReportStatusListener.Stub.getDefaultImpl().onReportFinished();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onReportFailed() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IIncidentReportStatusListener");
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IIncidentReportStatusListener.Stub.getDefaultImpl() != null) {
            IIncidentReportStatusListener.Stub.getDefaultImpl().onReportFailed();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IIncidentReportStatusListener param1IIncidentReportStatusListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IIncidentReportStatusListener != null) {
          Proxy.sDefaultImpl = param1IIncidentReportStatusListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IIncidentReportStatusListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
