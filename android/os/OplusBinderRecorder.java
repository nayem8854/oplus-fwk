package android.os;

import android.util.Log;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OplusBinderRecorder {
  static final int FLAG_ONEWAY = 1;
  
  static final int STATE_FINISH = 3;
  
  static final int STATE_FOUND_CONTEXT = 2;
  
  static final int STATE_FOUND_PROC = 1;
  
  static final int STATE_NOT_FOUND = 0;
  
  public static final String TAG = "OplusBinderRecorder";
  
  private static OplusBinderRecorder mInstance = null;
  
  private long mMaxTimeUsed = 0L;
  
  private String mMaxTimeUsedDescriptor = null;
  
  public final class ThreadUsage {
    public final class ThreadUsageElement {
      private int mToPid = 0;
      
      private int mCount = 0;
      
      private String mName = "unknown";
      
      final OplusBinderRecorder.ThreadUsage this$1;
      
      public ThreadUsageElement(int param2Int) {
        this.mToPid = param2Int;
        initName(param2Int);
      }
      
      private void initName(int param2Int) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("/proc/");
        stringBuilder1.append(param2Int);
        stringBuilder1.append("/cmdline");
        String str = stringBuilder1.toString();
        FileInputStream fileInputStream1 = null;
        StringBuilder stringBuilder2 = null;
        stringBuilder1 = stringBuilder2;
        FileInputStream fileInputStream2 = fileInputStream1;
        try {
          FileInputStream fileInputStream4 = new FileInputStream();
          stringBuilder1 = stringBuilder2;
          fileInputStream2 = fileInputStream1;
          this(str);
          FileInputStream fileInputStream3 = fileInputStream4;
          fileInputStream2 = fileInputStream4;
          byte[] arrayOfByte = new byte[2048];
          fileInputStream3 = fileInputStream4;
          fileInputStream2 = fileInputStream4;
          int i = fileInputStream4.read(arrayOfByte);
          if (i > 0) {
            for (param2Int = 0; param2Int < i && 
              arrayOfByte[param2Int] != 0; param2Int++);
            fileInputStream3 = fileInputStream4;
            fileInputStream2 = fileInputStream4;
            String str1 = new String();
            fileInputStream3 = fileInputStream4;
            fileInputStream2 = fileInputStream4;
            this(arrayOfByte, 0, param2Int);
            fileInputStream3 = fileInputStream4;
            fileInputStream2 = fileInputStream4;
            this.mName = str1;
          } 
          try {
            fileInputStream4.close();
          } catch (Exception exception) {}
        } catch (IOException iOException) {
          FileInputStream fileInputStream = fileInputStream2;
          StringBuilder stringBuilder = new StringBuilder();
          fileInputStream = fileInputStream2;
          this();
          fileInputStream = fileInputStream2;
          stringBuilder.append("Failed to read ");
          fileInputStream = fileInputStream2;
          stringBuilder.append(str);
          fileInputStream = fileInputStream2;
          Log.w("OplusBinderRecorder", stringBuilder.toString());
          fileInputStream = fileInputStream2;
          Log.w("OplusBinderRecorder", iOException);
          if (fileInputStream2 != null)
            fileInputStream2.close(); 
        } finally {}
      }
      
      public final String getName() {
        return this.mName;
      }
      
      public void increase() {
        this.mCount++;
      }
      
      public int getUsage() {
        return this.mCount;
      }
      
      public int getToPid() {
        return this.mToPid;
      }
    }
    
    final ArrayList<ThreadUsageElement> mUsageList = new ArrayList<>();
    
    final OplusBinderRecorder this$0;
    
    public void record(int param1Int) {
      ThreadUsageElement threadUsageElement1;
      byte b = 0;
      while (true) {
        threadUsageElement1 = null;
        if (b < this.mUsageList.size()) {
          threadUsageElement1 = this.mUsageList.get(b);
          if (threadUsageElement1.getToPid() == param1Int)
            break; 
          b++;
          continue;
        } 
        break;
      } 
      ThreadUsageElement threadUsageElement2 = threadUsageElement1;
      if (threadUsageElement1 == null) {
        threadUsageElement2 = new ThreadUsageElement(param1Int);
        this.mUsageList.add(threadUsageElement2);
      } 
      threadUsageElement2.increase();
    }
    
    class UsageListComparator implements Comparator<ThreadUsageElement> {
      final OplusBinderRecorder.ThreadUsage this$1;
      
      public int compare(OplusBinderRecorder.ThreadUsage.ThreadUsageElement param2ThreadUsageElement1, OplusBinderRecorder.ThreadUsage.ThreadUsageElement param2ThreadUsageElement2) {
        if (param2ThreadUsageElement1.getUsage() < param2ThreadUsageElement2.getUsage())
          return 1; 
        if (param2ThreadUsageElement1.getUsage() > param2ThreadUsageElement2.getUsage())
          return -1; 
        return 0;
      }
    }
    
    public int getLength() {
      return this.mUsageList.size();
    }
    
    private void sort() {
      Collections.sort(this.mUsageList, new UsageListComparator());
    }
    
    public void print() {
      for (byte b = 0; b < this.mUsageList.size(); b++) {
        ThreadUsageElement threadUsageElement = this.mUsageList.get(b);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(threadUsageElement.getName());
        stringBuilder.append("(");
        stringBuilder.append(threadUsageElement.getToPid());
        stringBuilder.append("):");
        stringBuilder.append(threadUsageElement.getUsage());
        Log.i("OplusBinderRecorder", stringBuilder.toString());
      } 
    }
    
    public final String getMapString() {
      StringBuilder stringBuilder = new StringBuilder("");
      for (byte b = 0; b < this.mUsageList.size(); b++) {
        ThreadUsageElement threadUsageElement = this.mUsageList.get(b);
        stringBuilder.append(threadUsageElement.getName());
        stringBuilder.append(":");
        stringBuilder.append(Integer.toString(threadUsageElement.getUsage()));
        stringBuilder.append(",");
      } 
      return stringBuilder.toString();
    }
  }
  
  public final class ThreadUsageElement {
    private int mToPid = 0;
    
    private int mCount = 0;
    
    private String mName = "unknown";
    
    final OplusBinderRecorder.ThreadUsage this$1;
    
    public ThreadUsageElement(int param1Int) {
      this.mToPid = param1Int;
      initName(param1Int);
    }
    
    private void initName(int param1Int) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("/proc/");
      stringBuilder1.append(param1Int);
      stringBuilder1.append("/cmdline");
      String str = stringBuilder1.toString();
      FileInputStream fileInputStream1 = null;
      StringBuilder stringBuilder2 = null;
      stringBuilder1 = stringBuilder2;
      FileInputStream fileInputStream2 = fileInputStream1;
      try {
        FileInputStream fileInputStream4 = new FileInputStream();
        stringBuilder1 = stringBuilder2;
        fileInputStream2 = fileInputStream1;
        this(str);
        FileInputStream fileInputStream3 = fileInputStream4;
        fileInputStream2 = fileInputStream4;
        byte[] arrayOfByte = new byte[2048];
        fileInputStream3 = fileInputStream4;
        fileInputStream2 = fileInputStream4;
        int i = fileInputStream4.read(arrayOfByte);
        if (i > 0) {
          for (param1Int = 0; param1Int < i && arrayOfByte[param1Int] != 0; param1Int++);
          fileInputStream3 = fileInputStream4;
          fileInputStream2 = fileInputStream4;
          String str1 = new String();
          fileInputStream3 = fileInputStream4;
          fileInputStream2 = fileInputStream4;
          this(arrayOfByte, 0, param1Int);
          fileInputStream3 = fileInputStream4;
          fileInputStream2 = fileInputStream4;
          this.mName = str1;
        } 
        try {
          fileInputStream4.close();
        } catch (Exception exception) {}
      } catch (IOException iOException) {
        FileInputStream fileInputStream = fileInputStream2;
        StringBuilder stringBuilder = new StringBuilder();
        fileInputStream = fileInputStream2;
        this();
        fileInputStream = fileInputStream2;
        stringBuilder.append("Failed to read ");
        fileInputStream = fileInputStream2;
        stringBuilder.append(str);
        fileInputStream = fileInputStream2;
        Log.w("OplusBinderRecorder", stringBuilder.toString());
        fileInputStream = fileInputStream2;
        Log.w("OplusBinderRecorder", iOException);
        if (fileInputStream2 != null)
          fileInputStream2.close(); 
      } finally {}
    }
    
    public final String getName() {
      return this.mName;
    }
    
    public void increase() {
      this.mCount++;
    }
    
    public int getUsage() {
      return this.mCount;
    }
    
    public int getToPid() {
      return this.mToPid;
    }
  }
  
  class UsageListComparator implements Comparator<ThreadUsage.ThreadUsageElement> {
    final OplusBinderRecorder.ThreadUsage this$1;
    
    public int compare(OplusBinderRecorder.ThreadUsage.ThreadUsageElement param1ThreadUsageElement1, OplusBinderRecorder.ThreadUsage.ThreadUsageElement param1ThreadUsageElement2) {
      if (param1ThreadUsageElement1.getUsage() < param1ThreadUsageElement2.getUsage())
        return 1; 
      if (param1ThreadUsageElement1.getUsage() > param1ThreadUsageElement2.getUsage())
        return -1; 
      return 0;
    }
  }
  
  public static OplusBinderRecorder getInstance() {
    // Byte code:
    //   0: ldc android/os/OplusBinderRecorder
    //   2: monitorenter
    //   3: getstatic android/os/OplusBinderRecorder.mInstance : Landroid/os/OplusBinderRecorder;
    //   6: ifnonnull -> 21
    //   9: new android/os/OplusBinderRecorder
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic android/os/OplusBinderRecorder.mInstance : Landroid/os/OplusBinderRecorder;
    //   21: getstatic android/os/OplusBinderRecorder.mInstance : Landroid/os/OplusBinderRecorder;
    //   24: astore_0
    //   25: ldc android/os/OplusBinderRecorder
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc android/os/OplusBinderRecorder
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #167	-> 3
    //   #168	-> 9
    //   #170	-> 21
    //   #166	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	25	30	finally
  }
  
  public void recordTimeUsed(Binder paramBinder, long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mMaxTimeUsed : J
    //   6: lload_2
    //   7: lcmp
    //   8: ifge -> 24
    //   11: aload_0
    //   12: lload_2
    //   13: putfield mMaxTimeUsed : J
    //   16: aload_0
    //   17: aload_1
    //   18: invokevirtual getInterfaceDescriptor : ()Ljava/lang/String;
    //   21: putfield mMaxTimeUsedDescriptor : Ljava/lang/String;
    //   24: aload_0
    //   25: monitorexit
    //   26: return
    //   27: astore_1
    //   28: aload_0
    //   29: monitorexit
    //   30: aload_1
    //   31: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #174	-> 2
    //   #175	-> 11
    //   #176	-> 16
    //   #178	-> 24
    //   #173	-> 27
    // Exception table:
    //   from	to	target	type
    //   2	11	27	finally
    //   11	16	27	finally
    //   16	24	27	finally
  }
  
  public void uploadMaxTimeUsed() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new java/lang/StringBuilder
    //   5: astore_1
    //   6: aload_1
    //   7: invokespecial <init> : ()V
    //   10: aload_1
    //   11: ldc 'max time used: '
    //   13: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   16: pop
    //   17: aload_1
    //   18: aload_0
    //   19: getfield mMaxTimeUsed : J
    //   22: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   25: pop
    //   26: aload_1
    //   27: ldc ' desc: '
    //   29: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   32: pop
    //   33: aload_1
    //   34: aload_0
    //   35: getfield mMaxTimeUsedDescriptor : Ljava/lang/String;
    //   38: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   41: pop
    //   42: ldc 'OplusBinderRecorder'
    //   44: aload_1
    //   45: invokevirtual toString : ()Ljava/lang/String;
    //   48: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   51: pop
    //   52: aload_0
    //   53: monitorexit
    //   54: return
    //   55: astore_1
    //   56: aload_0
    //   57: monitorexit
    //   58: aload_1
    //   59: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #181	-> 2
    //   #182	-> 52
    //   #180	-> 55
    // Exception table:
    //   from	to	target	type
    //   2	52	55	finally
  }
  
  public Map<String, String> getBinderUsageDscLogMap() {
    HashMap<Object, Object> hashMap1 = null;
    HashMap<Object, Object> hashMap2 = hashMap1;
    try {
      int i = Process.myPid();
      byte b = 0;
      hashMap2 = hashMap1;
      BufferedReader bufferedReader = new BufferedReader();
      hashMap2 = hashMap1;
      FileReader fileReader = new FileReader();
      hashMap2 = hashMap1;
      this("/sys/kernel/debug/binder/state");
      hashMap2 = hashMap1;
      this(fileReader);
      hashMap2 = hashMap1;
      ThreadUsage threadUsage1 = new ThreadUsage();
      hashMap2 = hashMap1;
      this(this);
      hashMap2 = hashMap1;
      ThreadUsage threadUsage2 = new ThreadUsage();
      hashMap2 = hashMap1;
      this(this);
      hashMap2 = hashMap1;
      StringBuilder stringBuilder2 = new StringBuilder();
      hashMap2 = hashMap1;
      this();
      hashMap2 = hashMap1;
      stringBuilder2.append("Uploading binder usage for process ");
      hashMap2 = hashMap1;
      stringBuilder2.append(i);
      hashMap2 = hashMap1;
      Log.i("OplusBinderRecorder", stringBuilder2.toString());
      while (true) {
        hashMap2 = hashMap1;
        String str = bufferedReader.readLine();
        if (str != null && b != 3) {
          Matcher matcher;
          if (b) {
            if (b != 1) {
              if (b != 2)
                continue; 
              hashMap2 = hashMap1;
              if (str.startsWith("    outgoing")) {
                hashMap2 = hashMap1;
                StringBuilder stringBuilder3 = new StringBuilder();
                hashMap2 = hashMap1;
                this();
                hashMap2 = hashMap1;
                stringBuilder3.append("found transaction: ");
                hashMap2 = hashMap1;
                stringBuilder3.append(str);
                hashMap2 = hashMap1;
                Log.i("OplusBinderRecorder", stringBuilder3.toString());
                hashMap2 = hashMap1;
                Pattern pattern = Pattern.compile("^    (outgoing|incoming).*from (\\d+):(\\d+) to (\\d+):(\\d+).*flags (\\d+).*");
                hashMap2 = hashMap1;
                matcher = pattern.matcher(str);
                hashMap2 = hashMap1;
                if (matcher.find()) {
                  hashMap2 = hashMap1;
                  String str3 = matcher.group(1);
                  hashMap2 = hashMap1;
                  int j = Integer.parseInt(matcher.group(2));
                  hashMap2 = hashMap1;
                  int k = Integer.parseInt(matcher.group(4));
                  hashMap2 = hashMap1;
                  long l = Integer.parseInt(matcher.group(6));
                  if ((l & 0x1L) == 0L) {
                    hashMap2 = hashMap1;
                    if (str3.equals("outgoing")) {
                      hashMap2 = hashMap1;
                      threadUsage2.record(k);
                      continue;
                    } 
                    hashMap2 = hashMap1;
                    if (str3.equals("incoming")) {
                      hashMap2 = hashMap1;
                      threadUsage1.record(j);
                    } 
                  } 
                } 
                continue;
              } 
              hashMap2 = hashMap1;
              if (matcher.startsWith("proc"))
                b = 3; 
              continue;
            } 
            hashMap2 = hashMap1;
            if (matcher.equals("context binder")) {
              b = 2;
              continue;
            } 
            b = 0;
            continue;
          } 
          hashMap2 = hashMap1;
          StringBuilder stringBuilder = new StringBuilder();
          hashMap2 = hashMap1;
          this();
          hashMap2 = hashMap1;
          stringBuilder.append("proc ");
          hashMap2 = hashMap1;
          stringBuilder.append(i);
          hashMap2 = hashMap1;
          if (matcher.equals(stringBuilder.toString()))
            b = 1; 
          continue;
        } 
        break;
      } 
      hashMap2 = hashMap1;
      bufferedReader.close();
      hashMap2 = hashMap1;
      HashMap<Object, Object> hashMap = new HashMap<>();
      hashMap2 = hashMap1;
      this();
      hashMap1 = hashMap;
      hashMap2 = hashMap1;
      threadUsage2.sort();
      hashMap2 = hashMap1;
      StringBuilder stringBuilder1 = new StringBuilder();
      hashMap2 = hashMap1;
      this();
      hashMap2 = hashMap1;
      stringBuilder1.append("Print outgoing thread usage for ");
      hashMap2 = hashMap1;
      stringBuilder1.append(Process.myPid());
      hashMap2 = hashMap1;
      Log.i("OplusBinderRecorder", stringBuilder1.toString());
      hashMap2 = hashMap1;
      threadUsage2.print();
      hashMap2 = hashMap1;
      String str2 = threadUsage2.getMapString();
      hashMap2 = hashMap1;
      stringBuilder1 = new StringBuilder();
      hashMap2 = hashMap1;
      this();
      hashMap2 = hashMap1;
      stringBuilder1.append("DCS mapstring is: ");
      hashMap2 = hashMap1;
      stringBuilder1.append(str2);
      hashMap2 = hashMap1;
      Log.i("OplusBinderRecorder", stringBuilder1.toString());
      hashMap2 = hashMap1;
      hashMap1.put("outGoingThreadUsage", str2);
      hashMap2 = hashMap1;
      threadUsage1.sort();
      hashMap2 = hashMap1;
      stringBuilder1 = new StringBuilder();
      hashMap2 = hashMap1;
      this();
      hashMap2 = hashMap1;
      stringBuilder1.append("Print incoming thread usage for ");
      hashMap2 = hashMap1;
      stringBuilder1.append(Process.myPid());
      hashMap2 = hashMap1;
      Log.i("OplusBinderRecorder", stringBuilder1.toString());
      hashMap2 = hashMap1;
      threadUsage1.print();
      hashMap2 = hashMap1;
      String str1 = threadUsage1.getMapString();
      hashMap2 = hashMap1;
      stringBuilder1 = new StringBuilder();
      hashMap2 = hashMap1;
      this();
      hashMap2 = hashMap1;
      stringBuilder1.append("DCS mapstring is: ");
      hashMap2 = hashMap1;
      stringBuilder1.append(str1);
      hashMap2 = hashMap1;
      Log.i("OplusBinderRecorder", stringBuilder1.toString());
      hashMap2 = hashMap1;
      hashMap1.put("inComingThreadUsage", str1);
      hashMap2 = hashMap1;
    } catch (IOException iOException) {
      Log.w("OplusBinderRecorder", "Failed to read binder state");
    } 
    return (Map)hashMap2;
  }
}
