package android.os;

import java.io.FileDescriptor;
import java.util.List;

public interface IIncidentManager extends IInterface {
  void deleteAllIncidentReports(String paramString) throws RemoteException;
  
  void deleteIncidentReports(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  IncidentManager.IncidentReport getIncidentReport(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  List<String> getIncidentReportList(String paramString1, String paramString2) throws RemoteException;
  
  void registerSection(int paramInt, String paramString, IIncidentDumpCallback paramIIncidentDumpCallback) throws RemoteException;
  
  void reportIncident(IncidentReportArgs paramIncidentReportArgs) throws RemoteException;
  
  void reportIncidentToDumpstate(FileDescriptor paramFileDescriptor, IIncidentReportStatusListener paramIIncidentReportStatusListener) throws RemoteException;
  
  void reportIncidentToStream(IncidentReportArgs paramIncidentReportArgs, IIncidentReportStatusListener paramIIncidentReportStatusListener, FileDescriptor paramFileDescriptor) throws RemoteException;
  
  void systemRunning() throws RemoteException;
  
  void unregisterSection(int paramInt) throws RemoteException;
  
  class Default implements IIncidentManager {
    public void reportIncident(IncidentReportArgs param1IncidentReportArgs) throws RemoteException {}
    
    public void reportIncidentToStream(IncidentReportArgs param1IncidentReportArgs, IIncidentReportStatusListener param1IIncidentReportStatusListener, FileDescriptor param1FileDescriptor) throws RemoteException {}
    
    public void reportIncidentToDumpstate(FileDescriptor param1FileDescriptor, IIncidentReportStatusListener param1IIncidentReportStatusListener) throws RemoteException {}
    
    public void registerSection(int param1Int, String param1String, IIncidentDumpCallback param1IIncidentDumpCallback) throws RemoteException {}
    
    public void unregisterSection(int param1Int) throws RemoteException {}
    
    public void systemRunning() throws RemoteException {}
    
    public List<String> getIncidentReportList(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public IncidentManager.IncidentReport getIncidentReport(String param1String1, String param1String2, String param1String3) throws RemoteException {
      return null;
    }
    
    public void deleteIncidentReports(String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public void deleteAllIncidentReports(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IIncidentManager {
    private static final String DESCRIPTOR = "android.os.IIncidentManager";
    
    static final int TRANSACTION_deleteAllIncidentReports = 10;
    
    static final int TRANSACTION_deleteIncidentReports = 9;
    
    static final int TRANSACTION_getIncidentReport = 8;
    
    static final int TRANSACTION_getIncidentReportList = 7;
    
    static final int TRANSACTION_registerSection = 4;
    
    static final int TRANSACTION_reportIncident = 1;
    
    static final int TRANSACTION_reportIncidentToDumpstate = 3;
    
    static final int TRANSACTION_reportIncidentToStream = 2;
    
    static final int TRANSACTION_systemRunning = 6;
    
    static final int TRANSACTION_unregisterSection = 5;
    
    public Stub() {
      attachInterface(this, "android.os.IIncidentManager");
    }
    
    public static IIncidentManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IIncidentManager");
      if (iInterface != null && iInterface instanceof IIncidentManager)
        return (IIncidentManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "deleteAllIncidentReports";
        case 9:
          return "deleteIncidentReports";
        case 8:
          return "getIncidentReport";
        case 7:
          return "getIncidentReportList";
        case 6:
          return "systemRunning";
        case 5:
          return "unregisterSection";
        case 4:
          return "registerSection";
        case 3:
          return "reportIncidentToDumpstate";
        case 2:
          return "reportIncidentToStream";
        case 1:
          break;
      } 
      return "reportIncident";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      FileDescriptor fileDescriptor;
      if (param1Int1 != 1598968902) {
        String str2;
        IncidentManager.IncidentReport incidentReport;
        String str1;
        List<String> list;
        IIncidentDumpCallback iIncidentDumpCallback;
        IIncidentReportStatusListener iIncidentReportStatusListener1;
        FileDescriptor fileDescriptor1;
        String str3, str4, str5;
        IIncidentReportStatusListener iIncidentReportStatusListener2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.os.IIncidentManager");
            str2 = param1Parcel1.readString();
            deleteAllIncidentReports(str2);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str2.enforceInterface("android.os.IIncidentManager");
            str4 = str2.readString();
            str5 = str2.readString();
            str2 = str2.readString();
            deleteIncidentReports(str4, str5, str2);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str2.enforceInterface("android.os.IIncidentManager");
            str5 = str2.readString();
            str4 = str2.readString();
            str2 = str2.readString();
            incidentReport = getIncidentReport(str5, str4, str2);
            param1Parcel2.writeNoException();
            if (incidentReport != null) {
              param1Parcel2.writeInt(1);
              incidentReport.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 7:
            incidentReport.enforceInterface("android.os.IIncidentManager");
            str5 = incidentReport.readString();
            str1 = incidentReport.readString();
            list = getIncidentReportList(str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list);
            return true;
          case 6:
            list.enforceInterface("android.os.IIncidentManager");
            systemRunning();
            return true;
          case 5:
            list.enforceInterface("android.os.IIncidentManager");
            param1Int1 = list.readInt();
            unregisterSection(param1Int1);
            return true;
          case 4:
            list.enforceInterface("android.os.IIncidentManager");
            param1Int1 = list.readInt();
            str3 = list.readString();
            iIncidentDumpCallback = IIncidentDumpCallback.Stub.asInterface(list.readStrongBinder());
            registerSection(param1Int1, str3, iIncidentDumpCallback);
            return true;
          case 3:
            iIncidentDumpCallback.enforceInterface("android.os.IIncidentManager");
            fileDescriptor = iIncidentDumpCallback.readRawFileDescriptor();
            iIncidentReportStatusListener1 = IIncidentReportStatusListener.Stub.asInterface(iIncidentDumpCallback.readStrongBinder());
            reportIncidentToDumpstate(fileDescriptor, iIncidentReportStatusListener1);
            return true;
          case 2:
            iIncidentReportStatusListener1.enforceInterface("android.os.IIncidentManager");
            if (iIncidentReportStatusListener1.readInt() != 0) {
              IncidentReportArgs incidentReportArgs = IncidentReportArgs.CREATOR.createFromParcel((Parcel)iIncidentReportStatusListener1);
            } else {
              fileDescriptor = null;
            } 
            iIncidentReportStatusListener2 = IIncidentReportStatusListener.Stub.asInterface(iIncidentReportStatusListener1.readStrongBinder());
            fileDescriptor1 = iIncidentReportStatusListener1.readRawFileDescriptor();
            reportIncidentToStream((IncidentReportArgs)fileDescriptor, iIncidentReportStatusListener2, fileDescriptor1);
            return true;
          case 1:
            break;
        } 
        fileDescriptor1.enforceInterface("android.os.IIncidentManager");
        if (fileDescriptor1.readInt() != 0) {
          IncidentReportArgs incidentReportArgs = IncidentReportArgs.CREATOR.createFromParcel((Parcel)fileDescriptor1);
        } else {
          fileDescriptor1 = null;
        } 
        reportIncident((IncidentReportArgs)fileDescriptor1);
        return true;
      } 
      fileDescriptor.writeString("android.os.IIncidentManager");
      return true;
    }
    
    private static class Proxy implements IIncidentManager {
      public static IIncidentManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IIncidentManager";
      }
      
      public void reportIncident(IncidentReportArgs param2IncidentReportArgs) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IIncidentManager");
          if (param2IncidentReportArgs != null) {
            parcel.writeInt(1);
            param2IncidentReportArgs.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IIncidentManager.Stub.getDefaultImpl() != null) {
            IIncidentManager.Stub.getDefaultImpl().reportIncident(param2IncidentReportArgs);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void reportIncidentToStream(IncidentReportArgs param2IncidentReportArgs, IIncidentReportStatusListener param2IIncidentReportStatusListener, FileDescriptor param2FileDescriptor) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.os.IIncidentManager");
          if (param2IncidentReportArgs != null) {
            parcel.writeInt(1);
            param2IncidentReportArgs.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IIncidentReportStatusListener != null) {
            iBinder = param2IIncidentReportStatusListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeRawFileDescriptor(param2FileDescriptor);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IIncidentManager.Stub.getDefaultImpl() != null) {
            IIncidentManager.Stub.getDefaultImpl().reportIncidentToStream(param2IncidentReportArgs, param2IIncidentReportStatusListener, param2FileDescriptor);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void reportIncidentToDumpstate(FileDescriptor param2FileDescriptor, IIncidentReportStatusListener param2IIncidentReportStatusListener) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.os.IIncidentManager");
          parcel.writeRawFileDescriptor(param2FileDescriptor);
          if (param2IIncidentReportStatusListener != null) {
            iBinder = param2IIncidentReportStatusListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IIncidentManager.Stub.getDefaultImpl() != null) {
            IIncidentManager.Stub.getDefaultImpl().reportIncidentToDumpstate(param2FileDescriptor, param2IIncidentReportStatusListener);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registerSection(int param2Int, String param2String, IIncidentDumpCallback param2IIncidentDumpCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.os.IIncidentManager");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          if (param2IIncidentDumpCallback != null) {
            iBinder = param2IIncidentDumpCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IIncidentManager.Stub.getDefaultImpl() != null) {
            IIncidentManager.Stub.getDefaultImpl().registerSection(param2Int, param2String, param2IIncidentDumpCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unregisterSection(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IIncidentManager");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IIncidentManager.Stub.getDefaultImpl() != null) {
            IIncidentManager.Stub.getDefaultImpl().unregisterSection(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void systemRunning() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IIncidentManager");
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IIncidentManager.Stub.getDefaultImpl() != null) {
            IIncidentManager.Stub.getDefaultImpl().systemRunning();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public List<String> getIncidentReportList(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IIncidentManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IIncidentManager.Stub.getDefaultImpl() != null)
            return IIncidentManager.Stub.getDefaultImpl().getIncidentReportList(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IncidentManager.IncidentReport getIncidentReport(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IIncidentManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IIncidentManager.Stub.getDefaultImpl() != null)
            return IIncidentManager.Stub.getDefaultImpl().getIncidentReport(param2String1, param2String2, param2String3); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            IncidentManager.IncidentReport incidentReport = IncidentManager.IncidentReport.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (IncidentManager.IncidentReport)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteIncidentReports(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IIncidentManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IIncidentManager.Stub.getDefaultImpl() != null) {
            IIncidentManager.Stub.getDefaultImpl().deleteIncidentReports(param2String1, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteAllIncidentReports(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IIncidentManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IIncidentManager.Stub.getDefaultImpl() != null) {
            IIncidentManager.Stub.getDefaultImpl().deleteAllIncidentReports(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IIncidentManager param1IIncidentManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IIncidentManager != null) {
          Proxy.sDefaultImpl = param1IIncidentManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IIncidentManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
