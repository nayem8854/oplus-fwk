package android.os;

public interface IOppoGuardElfThermalControl extends IInterface {
  void setChargeLevel(String paramString1, String paramString2) throws RemoteException;
  
  class Default implements IOppoGuardElfThermalControl {
    public void setChargeLevel(String param1String1, String param1String2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOppoGuardElfThermalControl {
    private static final String DESCRIPTOR = "android.os.IOppoGuardElfThermalControl";
    
    static final int TRANSACTION_setChargeLevel = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IOppoGuardElfThermalControl");
    }
    
    public static IOppoGuardElfThermalControl asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IOppoGuardElfThermalControl");
      if (iInterface != null && iInterface instanceof IOppoGuardElfThermalControl)
        return (IOppoGuardElfThermalControl)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "setChargeLevel";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.IOppoGuardElfThermalControl");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IOppoGuardElfThermalControl");
      String str2 = param1Parcel1.readString();
      String str1 = param1Parcel1.readString();
      setChargeLevel(str2, str1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOppoGuardElfThermalControl {
      public static IOppoGuardElfThermalControl sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IOppoGuardElfThermalControl";
      }
      
      public void setChargeLevel(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IOppoGuardElfThermalControl");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOppoGuardElfThermalControl.Stub.getDefaultImpl() != null) {
            IOppoGuardElfThermalControl.Stub.getDefaultImpl().setChargeLevel(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOppoGuardElfThermalControl param1IOppoGuardElfThermalControl) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOppoGuardElfThermalControl != null) {
          Proxy.sDefaultImpl = param1IOppoGuardElfThermalControl;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOppoGuardElfThermalControl getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
