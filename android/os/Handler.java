package android.os;

import android.util.Log;
import android.util.Printer;

public class Handler {
  private static final boolean FIND_POTENTIAL_LEAKS = false;
  
  private static Handler MAIN_THREAD_HANDLER = null;
  
  private static final String TAG = "Handler";
  
  final boolean mAsynchronous;
  
  final Callback mCallback;
  
  final Looper mLooper;
  
  IMessenger mMessenger;
  
  final MessageQueue mQueue;
  
  public void handleMessage(Message paramMessage) {}
  
  public void dispatchMessage(Message paramMessage) {
    if (paramMessage.callback != null) {
      handleCallback(paramMessage);
    } else {
      Callback callback = this.mCallback;
      if (callback != null && 
        callback.handleMessage(paramMessage))
        return; 
      handleMessage(paramMessage);
    } 
  }
  
  @Deprecated
  public Handler() {
    this((Callback)null, false);
  }
  
  @Deprecated
  public Handler(Callback paramCallback) {
    this(paramCallback, false);
  }
  
  public Handler(Looper paramLooper) {
    this(paramLooper, null, false);
  }
  
  public Handler(Looper paramLooper, Callback paramCallback) {
    this(paramLooper, paramCallback, false);
  }
  
  public Handler(boolean paramBoolean) {
    this((Callback)null, paramBoolean);
  }
  
  public Handler(Callback paramCallback, boolean paramBoolean) {
    Looper looper = Looper.myLooper();
    if (looper != null) {
      this.mQueue = looper.mQueue;
      this.mCallback = paramCallback;
      this.mAsynchronous = paramBoolean;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Can't create handler inside thread ");
    stringBuilder.append(Thread.currentThread());
    stringBuilder.append(" that has not called Looper.prepare()");
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public Handler(Looper paramLooper, Callback paramCallback, boolean paramBoolean) {
    this.mLooper = paramLooper;
    this.mQueue = paramLooper.mQueue;
    this.mCallback = paramCallback;
    this.mAsynchronous = paramBoolean;
  }
  
  public static Handler createAsync(Looper paramLooper) {
    if (paramLooper != null)
      return new Handler(paramLooper, null, true); 
    throw new NullPointerException("looper must not be null");
  }
  
  public static Handler createAsync(Looper paramLooper, Callback paramCallback) {
    if (paramLooper != null) {
      if (paramCallback != null)
        return new Handler(paramLooper, paramCallback, true); 
      throw new NullPointerException("callback must not be null");
    } 
    throw new NullPointerException("looper must not be null");
  }
  
  public static Handler getMain() {
    if (MAIN_THREAD_HANDLER == null)
      MAIN_THREAD_HANDLER = new Handler(Looper.getMainLooper()); 
    return MAIN_THREAD_HANDLER;
  }
  
  public static Handler mainIfNull(Handler paramHandler) {
    if (paramHandler == null)
      paramHandler = getMain(); 
    return paramHandler;
  }
  
  public String getTraceName(Message paramMessage) {
    if (paramMessage.callback instanceof TraceNameSupplier)
      return ((TraceNameSupplier)paramMessage.callback).getTraceName(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getClass().getName());
    stringBuilder.append(": ");
    if (paramMessage.callback != null) {
      stringBuilder.append(paramMessage.callback.getClass().getName());
    } else {
      stringBuilder.append("#");
      stringBuilder.append(paramMessage.what);
    } 
    return stringBuilder.toString();
  }
  
  public String getMessageName(Message paramMessage) {
    if (paramMessage.callback != null)
      return paramMessage.callback.getClass().getName(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramMessage.what));
    return stringBuilder.toString();
  }
  
  public final Message obtainMessage() {
    return Message.obtain(this);
  }
  
  public final Message obtainMessage(int paramInt) {
    return Message.obtain(this, paramInt);
  }
  
  public final Message obtainMessage(int paramInt, Object paramObject) {
    return Message.obtain(this, paramInt, paramObject);
  }
  
  public final Message obtainMessage(int paramInt1, int paramInt2, int paramInt3) {
    return Message.obtain(this, paramInt1, paramInt2, paramInt3);
  }
  
  public final Message obtainMessage(int paramInt1, int paramInt2, int paramInt3, Object paramObject) {
    return Message.obtain(this, paramInt1, paramInt2, paramInt3, paramObject);
  }
  
  public final boolean post(Runnable paramRunnable) {
    return sendMessageDelayed(getPostMessage(paramRunnable), 0L);
  }
  
  public final boolean postAtTime(Runnable paramRunnable, long paramLong) {
    return sendMessageAtTime(getPostMessage(paramRunnable), paramLong);
  }
  
  public final boolean postAtTime(Runnable paramRunnable, Object paramObject, long paramLong) {
    return sendMessageAtTime(getPostMessage(paramRunnable, paramObject), paramLong);
  }
  
  public final boolean postDelayed(Runnable paramRunnable, long paramLong) {
    return sendMessageDelayed(getPostMessage(paramRunnable), paramLong);
  }
  
  public final boolean postDelayed(Runnable paramRunnable, int paramInt, long paramLong) {
    return sendMessageDelayed(getPostMessage(paramRunnable).setWhat(paramInt), paramLong);
  }
  
  public final boolean postDelayed(Runnable paramRunnable, Object paramObject, long paramLong) {
    return sendMessageDelayed(getPostMessage(paramRunnable, paramObject), paramLong);
  }
  
  public final boolean postAtFrontOfQueue(Runnable paramRunnable) {
    return sendMessageAtFrontOfQueue(getPostMessage(paramRunnable));
  }
  
  public final boolean runWithScissors(Runnable paramRunnable, long paramLong) {
    if (paramRunnable != null) {
      if (paramLong >= 0L) {
        if (Looper.myLooper() == this.mLooper) {
          paramRunnable.run();
          return true;
        } 
        paramRunnable = new BlockingRunnable(paramRunnable);
        return paramRunnable.postAndWait(this, paramLong);
      } 
      throw new IllegalArgumentException("timeout must be non-negative");
    } 
    throw new IllegalArgumentException("runnable must not be null");
  }
  
  public final void removeCallbacks(Runnable paramRunnable) {
    this.mQueue.removeMessages(this, paramRunnable, (Object)null);
  }
  
  public final void removeCallbacks(Runnable paramRunnable, Object paramObject) {
    this.mQueue.removeMessages(this, paramRunnable, paramObject);
  }
  
  public final boolean sendMessage(Message paramMessage) {
    return sendMessageDelayed(paramMessage, 0L);
  }
  
  public final boolean sendEmptyMessage(int paramInt) {
    return sendEmptyMessageDelayed(paramInt, 0L);
  }
  
  public final boolean sendEmptyMessageDelayed(int paramInt, long paramLong) {
    Message message = Message.obtain();
    message.what = paramInt;
    return sendMessageDelayed(message, paramLong);
  }
  
  public final boolean sendEmptyMessageAtTime(int paramInt, long paramLong) {
    Message message = Message.obtain();
    message.what = paramInt;
    return sendMessageAtTime(message, paramLong);
  }
  
  public final boolean sendMessageDelayed(Message paramMessage, long paramLong) {
    long l = paramLong;
    if (paramLong < 0L)
      l = 0L; 
    return sendMessageAtTime(paramMessage, SystemClock.uptimeMillis() + l);
  }
  
  public boolean sendMessageAtTime(Message paramMessage, long paramLong) {
    RuntimeException runtimeException;
    MessageQueue messageQueue = this.mQueue;
    if (messageQueue == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this);
      stringBuilder.append(" sendMessageAtTime() called with no mQueue");
      runtimeException = new RuntimeException(stringBuilder.toString());
      Log.w("Looper", runtimeException.getMessage(), runtimeException);
      return false;
    } 
    return enqueueMessage(messageQueue, (Message)runtimeException, paramLong);
  }
  
  public final boolean sendMessageAtFrontOfQueue(Message paramMessage) {
    RuntimeException runtimeException;
    MessageQueue messageQueue = this.mQueue;
    if (messageQueue == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this);
      stringBuilder.append(" sendMessageAtTime() called with no mQueue");
      runtimeException = new RuntimeException(stringBuilder.toString());
      Log.w("Looper", runtimeException.getMessage(), runtimeException);
      return false;
    } 
    return enqueueMessage(messageQueue, (Message)runtimeException, 0L);
  }
  
  public final boolean executeOrSendMessage(Message paramMessage) {
    if (this.mLooper == Looper.myLooper()) {
      dispatchMessage(paramMessage);
      return true;
    } 
    return sendMessage(paramMessage);
  }
  
  private boolean enqueueMessage(MessageQueue paramMessageQueue, Message paramMessage, long paramLong) {
    paramMessage.target = this;
    paramMessage.workSourceUid = ThreadLocalWorkSource.getUid();
    if (this.mAsynchronous)
      paramMessage.setAsynchronous(true); 
    return paramMessageQueue.enqueueMessage(paramMessage, paramLong);
  }
  
  public final void removeMessages(int paramInt) {
    this.mQueue.removeMessages(this, paramInt, (Object)null);
  }
  
  public final void removeMessages(int paramInt, Object paramObject) {
    this.mQueue.removeMessages(this, paramInt, paramObject);
  }
  
  public final void removeEqualMessages(int paramInt, Object paramObject) {
    this.mQueue.removeEqualMessages(this, paramInt, paramObject);
  }
  
  public final void removeCallbacksAndMessages(Object paramObject) {
    this.mQueue.removeCallbacksAndMessages(this, paramObject);
  }
  
  public final void removeCallbacksAndEqualMessages(Object paramObject) {
    this.mQueue.removeCallbacksAndEqualMessages(this, paramObject);
  }
  
  public final boolean hasMessages(int paramInt) {
    return this.mQueue.hasMessages(this, paramInt, (Object)null);
  }
  
  public final boolean hasMessagesOrCallbacks() {
    return this.mQueue.hasMessages(this);
  }
  
  public final boolean hasMessages(int paramInt, Object paramObject) {
    return this.mQueue.hasMessages(this, paramInt, paramObject);
  }
  
  public final boolean hasEqualMessages(int paramInt, Object paramObject) {
    return this.mQueue.hasEqualMessages(this, paramInt, paramObject);
  }
  
  public final boolean hasCallbacks(Runnable paramRunnable) {
    return this.mQueue.hasMessages(this, paramRunnable, (Object)null);
  }
  
  public final Looper getLooper() {
    return this.mLooper;
  }
  
  public final void dump(Printer paramPrinter, String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(this);
    stringBuilder.append(" @ ");
    stringBuilder.append(SystemClock.uptimeMillis());
    paramPrinter.println(stringBuilder.toString());
    Looper looper = this.mLooper;
    if (looper == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append("looper uninitialized");
      paramPrinter.println(stringBuilder.toString());
    } else {
      stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append("  ");
      looper.dump(paramPrinter, stringBuilder.toString());
    } 
  }
  
  public final void dumpMine(Printer paramPrinter, String paramString) {
    StringBuilder stringBuilder1, stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramString);
    stringBuilder2.append(this);
    stringBuilder2.append(" @ ");
    stringBuilder2.append(SystemClock.uptimeMillis());
    paramPrinter.println(stringBuilder2.toString());
    Looper looper = this.mLooper;
    if (looper == null) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("looper uninitialized");
      paramPrinter.println(stringBuilder1.toString());
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append("  ");
      stringBuilder1.dump(paramPrinter, stringBuilder.toString(), this);
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Handler (");
    stringBuilder.append(getClass().getName());
    stringBuilder.append(") {");
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  final IMessenger getIMessenger() {
    synchronized (this.mQueue) {
      if (this.mMessenger != null)
        return this.mMessenger; 
      MessengerImpl messengerImpl = new MessengerImpl();
      this(this);
      this.mMessenger = messengerImpl;
      return messengerImpl;
    } 
  }
  
  class MessengerImpl extends IMessenger.Stub {
    final Handler this$0;
    
    private MessengerImpl() {}
    
    public void send(Message param1Message) {
      param1Message.sendingUid = Binder.getCallingUid();
      Handler.this.sendMessage(param1Message);
    }
  }
  
  private static Message getPostMessage(Runnable paramRunnable) {
    Message message = Message.obtain();
    message.callback = paramRunnable;
    return message;
  }
  
  private static Message getPostMessage(Runnable paramRunnable, Object paramObject) {
    Message message = Message.obtain();
    message.obj = paramObject;
    message.callback = paramRunnable;
    return message;
  }
  
  private static void handleCallback(Message paramMessage) {
    paramMessage.callback.run();
  }
  
  public static interface Callback {
    boolean handleMessage(Message param1Message);
  }
  
  private static final class BlockingRunnable implements Runnable {
    private boolean mDone;
    
    private final Runnable mTask;
    
    public BlockingRunnable(Runnable param1Runnable) {
      this.mTask = param1Runnable;
    }
    
    public void run() {
      // Byte code:
      //   0: aload_0
      //   1: getfield mTask : Ljava/lang/Runnable;
      //   4: invokeinterface run : ()V
      //   9: aload_0
      //   10: monitorenter
      //   11: aload_0
      //   12: iconst_1
      //   13: putfield mDone : Z
      //   16: aload_0
      //   17: invokevirtual notifyAll : ()V
      //   20: aload_0
      //   21: monitorexit
      //   22: return
      //   23: astore_1
      //   24: aload_0
      //   25: monitorexit
      //   26: aload_1
      //   27: athrow
      //   28: astore_1
      //   29: aload_0
      //   30: monitorenter
      //   31: aload_0
      //   32: iconst_1
      //   33: putfield mDone : Z
      //   36: aload_0
      //   37: invokevirtual notifyAll : ()V
      //   40: aload_0
      //   41: monitorexit
      //   42: aload_1
      //   43: athrow
      //   44: astore_1
      //   45: aload_0
      //   46: monitorexit
      //   47: aload_1
      //   48: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #961	-> 0
      //   #963	-> 9
      //   #964	-> 11
      //   #965	-> 16
      //   #966	-> 20
      //   #967	-> 22
      //   #968	-> 22
      //   #966	-> 23
      //   #963	-> 28
      //   #964	-> 31
      //   #965	-> 36
      //   #966	-> 40
      //   #967	-> 42
      //   #966	-> 44
      // Exception table:
      //   from	to	target	type
      //   0	9	28	finally
      //   11	16	23	finally
      //   16	20	23	finally
      //   20	22	23	finally
      //   24	26	23	finally
      //   31	36	44	finally
      //   36	40	44	finally
      //   40	42	44	finally
      //   45	47	44	finally
    }
    
    public boolean postAndWait(Handler param1Handler, long param1Long) {
      // Byte code:
      //   0: aload_1
      //   1: aload_0
      //   2: invokevirtual post : (Ljava/lang/Runnable;)Z
      //   5: ifne -> 10
      //   8: iconst_0
      //   9: ireturn
      //   10: aload_0
      //   11: monitorenter
      //   12: lload_2
      //   13: lconst_0
      //   14: lcmp
      //   15: ifle -> 67
      //   18: invokestatic uptimeMillis : ()J
      //   21: lstore #4
      //   23: aload_0
      //   24: getfield mDone : Z
      //   27: ifne -> 64
      //   30: lload #4
      //   32: lload_2
      //   33: ladd
      //   34: invokestatic uptimeMillis : ()J
      //   37: lsub
      //   38: lstore #6
      //   40: lload #6
      //   42: lconst_0
      //   43: lcmp
      //   44: ifgt -> 51
      //   47: aload_0
      //   48: monitorexit
      //   49: iconst_0
      //   50: ireturn
      //   51: aload_0
      //   52: lload #6
      //   54: invokevirtual wait : (J)V
      //   57: goto -> 61
      //   60: astore_1
      //   61: goto -> 23
      //   64: goto -> 89
      //   67: aload_0
      //   68: getfield mDone : Z
      //   71: istore #8
      //   73: iload #8
      //   75: ifne -> 89
      //   78: aload_0
      //   79: invokevirtual wait : ()V
      //   82: goto -> 67
      //   85: astore_1
      //   86: goto -> 82
      //   89: aload_0
      //   90: monitorexit
      //   91: iconst_1
      //   92: ireturn
      //   93: astore_1
      //   94: aload_0
      //   95: monitorexit
      //   96: aload_1
      //   97: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #971	-> 0
      //   #972	-> 8
      //   #975	-> 10
      //   #976	-> 12
      //   #977	-> 18
      //   #978	-> 23
      //   #979	-> 30
      //   #980	-> 40
      //   #981	-> 47
      //   #984	-> 51
      //   #986	-> 57
      //   #985	-> 60
      //   #987	-> 61
      //   #988	-> 64
      //   #989	-> 67
      //   #991	-> 78
      //   #993	-> 82
      //   #992	-> 85
      //   #996	-> 89
      //   #997	-> 91
      //   #996	-> 93
      // Exception table:
      //   from	to	target	type
      //   18	23	93	finally
      //   23	30	93	finally
      //   30	40	93	finally
      //   47	49	93	finally
      //   51	57	60	java/lang/InterruptedException
      //   51	57	93	finally
      //   67	73	93	finally
      //   78	82	85	java/lang/InterruptedException
      //   78	82	93	finally
      //   89	91	93	finally
      //   94	96	93	finally
    }
  }
}
