package android.os;

import android.annotation.SystemApi;
import android.content.Context;
import android.net.Uri;
import android.util.Slog;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;

@SystemApi
public class IncidentManager {
  public static final int FLAG_CONFIRMATION_DIALOG = 1;
  
  public static final int PRIVACY_POLICY_AUTO = 200;
  
  public static final int PRIVACY_POLICY_EXPLICIT = 100;
  
  public static final int PRIVACY_POLICY_LOCAL = 0;
  
  private static final String TAG = "IncidentManager";
  
  public static final String URI_AUTHORITY = "android.os.IncidentManager";
  
  public static final String URI_PARAM_CALLING_PACKAGE = "pkg";
  
  public static final String URI_PARAM_FLAGS = "flags";
  
  public static final String URI_PARAM_ID = "id";
  
  public static final String URI_PARAM_RECEIVER_CLASS = "receiver";
  
  public static final String URI_PARAM_REPORT_ID = "r";
  
  public static final String URI_PARAM_TIMESTAMP = "t";
  
  public static final String URI_PATH = "/pending";
  
  public static final String URI_SCHEME = "content";
  
  private IIncidentCompanion mCompanionService;
  
  private final Context mContext;
  
  private IIncidentManager mIncidentService;
  
  private Object mLock = new Object();
  
  @SystemApi
  public static class PendingReport {
    private final int mFlags;
    
    private final String mRequestingPackage;
    
    private final long mTimestamp;
    
    private final Uri mUri;
    
    public PendingReport(Uri param1Uri) {
      try {
        int i = Integer.parseInt(param1Uri.getQueryParameter("flags"));
        this.mFlags = i;
        String str = param1Uri.getQueryParameter("pkg");
        if (str != null) {
          this.mRequestingPackage = str;
          try {
            long l = Long.parseLong(param1Uri.getQueryParameter("t"));
            this.mTimestamp = l;
            this.mUri = param1Uri;
            return;
          } catch (NumberFormatException numberFormatException) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Invalid URI: No t parameter. ");
            stringBuilder1.append(param1Uri);
            throw new RuntimeException(stringBuilder1.toString());
          } 
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid URI: No pkg parameter. ");
        stringBuilder.append(param1Uri);
        throw new RuntimeException(stringBuilder.toString());
      } catch (NumberFormatException numberFormatException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid URI: No flags parameter. ");
        stringBuilder.append(param1Uri);
        throw new RuntimeException(stringBuilder.toString());
      } 
    }
    
    public String getRequestingPackage() {
      return this.mRequestingPackage;
    }
    
    public int getFlags() {
      return this.mFlags;
    }
    
    public long getTimestamp() {
      return this.mTimestamp;
    }
    
    public Uri getUri() {
      return this.mUri;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("PendingReport(");
      stringBuilder.append(getUri().toString());
      stringBuilder.append(")");
      return stringBuilder.toString();
    }
    
    public boolean equals(Object param1Object) {
      null = true;
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof PendingReport))
        return false; 
      param1Object = param1Object;
      if (this.mUri.equals(((PendingReport)param1Object).mUri) && this.mFlags == ((PendingReport)param1Object).mFlags) {
        String str1 = this.mRequestingPackage, str2 = ((PendingReport)param1Object).mRequestingPackage;
        if (str1.equals(str2) && this.mTimestamp == ((PendingReport)param1Object).mTimestamp)
          return null; 
      } 
      return false;
    }
  }
  
  @SystemApi
  class IncidentReport implements Parcelable, Closeable {
    public IncidentReport(IncidentManager this$0) {
      this.mTimestampNs = this$0.readLong();
      this.mPrivacyPolicy = this$0.readInt();
      if (this$0.readInt() != 0) {
        this.mFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)this$0);
      } else {
        this.mFileDescriptor = null;
      } 
    }
    
    public void close() {
      try {
        if (this.mFileDescriptor != null) {
          this.mFileDescriptor.close();
          this.mFileDescriptor = null;
        } 
      } catch (IOException iOException) {}
    }
    
    public long getTimestamp() {
      return this.mTimestampNs / 1000000L;
    }
    
    public long getPrivacyPolicy() {
      return this.mPrivacyPolicy;
    }
    
    public InputStream getInputStream() throws IOException {
      if (this.mFileDescriptor == null)
        return null; 
      return new ParcelFileDescriptor.AutoCloseInputStream(this.mFileDescriptor);
    }
    
    public int describeContents() {
      boolean bool;
      if (this.mFileDescriptor != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeLong(this.mTimestampNs);
      param1Parcel.writeInt(this.mPrivacyPolicy);
      if (this.mFileDescriptor != null) {
        param1Parcel.writeInt(1);
        this.mFileDescriptor.writeToParcel(param1Parcel, param1Int);
      } else {
        param1Parcel.writeInt(0);
      } 
    }
    
    public static final Parcelable.Creator<IncidentReport> CREATOR = new Parcelable.Creator<IncidentReport>() {
        public IncidentManager.IncidentReport[] newArray(int param2Int) {
          return new IncidentManager.IncidentReport[param2Int];
        }
        
        public IncidentManager.IncidentReport createFromParcel(Parcel param2Parcel) {
          return new IncidentManager.IncidentReport(param2Parcel);
        }
      };
    
    private ParcelFileDescriptor mFileDescriptor;
    
    private final int mPrivacyPolicy;
    
    private final long mTimestampNs;
  }
  
  public static class AuthListener {
    IIncidentAuthListener.Stub mBinder = (IIncidentAuthListener.Stub)new Object(this);
    
    Executor mExecutor;
    
    public void onReportApproved() {}
    
    public void onReportDenied() {}
  }
  
  public static class DumpCallback {
    IIncidentDumpCallback.Stub mBinder;
    
    private Executor mExecutor;
    
    private int mId;
    
    public DumpCallback() {
      this.mBinder = (IIncidentDumpCallback.Stub)new Object(this);
    }
    
    public void onDumpSection(int param1Int, OutputStream param1OutputStream) {}
  }
  
  public IncidentManager(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public void reportIncident(IncidentReportArgs paramIncidentReportArgs) {
    reportIncidentInternal(paramIncidentReportArgs);
  }
  
  public void requestAuthorization(int paramInt1, String paramString, int paramInt2, AuthListener paramAuthListener) {
    Context context = this.mContext;
    Executor executor = context.getMainExecutor();
    requestAuthorization(paramInt1, paramString, paramInt2, executor, paramAuthListener);
  }
  
  public void requestAuthorization(int paramInt1, String paramString, int paramInt2, Executor paramExecutor, AuthListener paramAuthListener) {
    try {
      if (paramAuthListener.mExecutor == null) {
        paramAuthListener.mExecutor = paramExecutor;
        getCompanionServiceLocked().authorizeReport(paramInt1, paramString, null, null, paramInt2, paramAuthListener.mBinder);
        return;
      } 
      RuntimeException runtimeException = new RuntimeException();
      this("Do not reuse AuthListener objects when calling requestAuthorization");
      throw runtimeException;
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException);
    } 
  }
  
  public void cancelAuthorization(AuthListener paramAuthListener) {
    try {
      getCompanionServiceLocked().cancelAuthorization(paramAuthListener.mBinder);
      return;
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException);
    } 
  }
  
  public List<PendingReport> getPendingReports() {
    try {
      List<String> list = getCompanionServiceLocked().getPendingReports();
      int i = list.size();
      ArrayList<PendingReport> arrayList = new ArrayList(i);
      for (byte b = 0; b < i; b++)
        arrayList.add(new PendingReport(Uri.parse(list.get(b)))); 
      return arrayList;
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException);
    } 
  }
  
  public void approveReport(Uri paramUri) {
    try {
      getCompanionServiceLocked().approveReport(paramUri.toString());
      return;
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException);
    } 
  }
  
  public void denyReport(Uri paramUri) {
    try {
      getCompanionServiceLocked().denyReport(paramUri.toString());
      return;
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException);
    } 
  }
  
  public void registerSection(int paramInt, String paramString, Executor paramExecutor, DumpCallback paramDumpCallback) {
    Objects.requireNonNull(paramExecutor, "executor cannot be null");
    Objects.requireNonNull(paramDumpCallback, "callback cannot be null");
    try {
      if (paramDumpCallback.mExecutor == null) {
        DumpCallback.access$002(paramDumpCallback, paramExecutor);
        DumpCallback.access$102(paramDumpCallback, paramInt);
        IIncidentManager iIncidentManager = getIIncidentManagerLocked();
        if (iIncidentManager == null) {
          Slog.e("IncidentManager", "registerSection can't find incident binder service");
          return;
        } 
        iIncidentManager.registerSection(paramInt, paramString, paramDumpCallback.mBinder);
      } else {
        RuntimeException runtimeException = new RuntimeException();
        this("Do not reuse DumpCallback objects when calling registerSection");
        throw runtimeException;
      } 
    } catch (RemoteException remoteException) {
      Slog.e("IncidentManager", "registerSection failed", (Throwable)remoteException);
    } 
  }
  
  public void unregisterSection(int paramInt) {
    try {
      IIncidentManager iIncidentManager = getIIncidentManagerLocked();
      if (iIncidentManager == null) {
        Slog.e("IncidentManager", "unregisterSection can't find incident binder service");
        return;
      } 
      iIncidentManager.unregisterSection(paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("IncidentManager", "unregisterSection failed", (Throwable)remoteException);
    } 
  }
  
  public List<Uri> getIncidentReportList(String paramString) {
    try {
      IIncidentCompanion iIncidentCompanion = getCompanionServiceLocked();
      Context context = this.mContext;
      String str = context.getPackageName();
      List<String> list = iIncidentCompanion.getIncidentReportList(str, paramString);
      int i = list.size();
      ArrayList<Uri> arrayList = new ArrayList(i);
      for (byte b = 0; b < i; b++)
        arrayList.add(Uri.parse(list.get(b))); 
      return arrayList;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("System server or incidentd going down", remoteException);
    } 
  }
  
  public IncidentReport getIncidentReport(Uri paramUri) {
    String str1 = paramUri.getQueryParameter("r");
    if (str1 == null)
      return null; 
    String str2 = paramUri.getQueryParameter("pkg");
    if (str2 != null) {
      String str = paramUri.getQueryParameter("receiver");
      if (str != null)
        try {
          return getCompanionServiceLocked().getIncidentReport(str2, str, str1);
        } catch (RemoteException remoteException) {
          throw new RuntimeException("System server or incidentd going down", remoteException);
        }  
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Invalid URI: No receiver parameter. ");
      stringBuilder1.append(remoteException);
      throw new RuntimeException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid URI: No pkg parameter. ");
    stringBuilder.append(remoteException);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public void deleteIncidentReports(Uri paramUri) {
    if (paramUri == null) {
      try {
        getCompanionServiceLocked().deleteAllIncidentReports(this.mContext.getPackageName());
      } catch (RemoteException remoteException) {
        throw new RuntimeException("System server or incidentd going down", remoteException);
      } 
    } else {
      String str = remoteException.getQueryParameter("pkg");
      if (str != null) {
        String str1 = remoteException.getQueryParameter("receiver");
        if (str1 != null) {
          String str2 = remoteException.getQueryParameter("r");
          if (str2 != null)
            try {
              getCompanionServiceLocked().deleteIncidentReports(str, str1, str2);
              return;
            } catch (RemoteException remoteException1) {
              throw new RuntimeException("System server or incidentd going down", remoteException1);
            }  
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Invalid URI: No r parameter. ");
          stringBuilder2.append(remoteException1);
          throw new RuntimeException(stringBuilder2.toString());
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Invalid URI: No receiver parameter. ");
        stringBuilder1.append(remoteException1);
        throw new RuntimeException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid URI: No pkg parameter. ");
      stringBuilder.append(remoteException1);
      throw new RuntimeException(stringBuilder.toString());
    } 
  }
  
  private void reportIncidentInternal(IncidentReportArgs paramIncidentReportArgs) {
    try {
      IIncidentManager iIncidentManager = getIIncidentManagerLocked();
      if (iIncidentManager == null) {
        Slog.e("IncidentManager", "reportIncident can't find incident binder service");
        return;
      } 
      iIncidentManager.reportIncident(paramIncidentReportArgs);
    } catch (RemoteException remoteException) {
      Slog.e("IncidentManager", "reportIncident failed", (Throwable)remoteException);
    } 
  }
  
  private IIncidentManager getIIncidentManagerLocked() throws RemoteException {
    IIncidentManager iIncidentManager = this.mIncidentService;
    if (iIncidentManager != null)
      return iIncidentManager; 
    synchronized (this.mLock) {
      if (this.mIncidentService != null)
        return this.mIncidentService; 
      IBinder iBinder = ServiceManager.getService("incident");
      IIncidentManager iIncidentManager1 = IIncidentManager.Stub.asInterface(iBinder);
      if (iIncidentManager1 != null) {
        IBinder iBinder1 = iIncidentManager1.asBinder();
        _$$Lambda$IncidentManager$yGukxCMuLDmoRlrh5jGUmq5BOTk _$$Lambda$IncidentManager$yGukxCMuLDmoRlrh5jGUmq5BOTk = new _$$Lambda$IncidentManager$yGukxCMuLDmoRlrh5jGUmq5BOTk();
        this(this);
        iBinder1.linkToDeath(_$$Lambda$IncidentManager$yGukxCMuLDmoRlrh5jGUmq5BOTk, 0);
      } 
      iIncidentManager1 = this.mIncidentService;
      return iIncidentManager1;
    } 
  }
  
  private IIncidentCompanion getCompanionServiceLocked() throws RemoteException {
    // Byte code:
    //   0: aload_0
    //   1: getfield mCompanionService : Landroid/os/IIncidentCompanion;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnull -> 11
    //   9: aload_1
    //   10: areturn
    //   11: aload_0
    //   12: monitorenter
    //   13: aload_0
    //   14: getfield mCompanionService : Landroid/os/IIncidentCompanion;
    //   17: ifnull -> 29
    //   20: aload_0
    //   21: getfield mCompanionService : Landroid/os/IIncidentCompanion;
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: areturn
    //   29: ldc 'incidentcompanion'
    //   31: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   34: astore_1
    //   35: aload_1
    //   36: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/os/IIncidentCompanion;
    //   39: astore_1
    //   40: aload_0
    //   41: aload_1
    //   42: putfield mCompanionService : Landroid/os/IIncidentCompanion;
    //   45: aload_1
    //   46: ifnull -> 73
    //   49: aload_1
    //   50: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   55: astore_1
    //   56: new android/os/_$$Lambda$IncidentManager$mfBTEJgu7VPkoPMTQdf1KC7oi5g
    //   59: astore_2
    //   60: aload_2
    //   61: aload_0
    //   62: invokespecial <init> : (Landroid/os/IncidentManager;)V
    //   65: aload_1
    //   66: aload_2
    //   67: iconst_0
    //   68: invokeinterface linkToDeath : (Landroid/os/IBinder$DeathRecipient;I)V
    //   73: aload_0
    //   74: getfield mCompanionService : Landroid/os/IIncidentCompanion;
    //   77: astore_1
    //   78: aload_0
    //   79: monitorexit
    //   80: aload_1
    //   81: areturn
    //   82: astore_1
    //   83: aload_0
    //   84: monitorexit
    //   85: aload_1
    //   86: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #773	-> 0
    //   #774	-> 9
    //   #777	-> 11
    //   #778	-> 13
    //   #779	-> 20
    //   #781	-> 29
    //   #782	-> 29
    //   #781	-> 35
    //   #783	-> 45
    //   #784	-> 49
    //   #790	-> 73
    //   #791	-> 82
    // Exception table:
    //   from	to	target	type
    //   13	20	82	finally
    //   20	27	82	finally
    //   29	35	82	finally
    //   35	45	82	finally
    //   49	73	82	finally
    //   73	80	82	finally
    //   83	85	82	finally
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PrivacyPolicy {}
}
