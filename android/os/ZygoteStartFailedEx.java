package android.os;

class ZygoteStartFailedEx extends Exception {
  ZygoteStartFailedEx(String paramString) {
    super(paramString);
  }
  
  ZygoteStartFailedEx(Throwable paramThrowable) {
    super(paramThrowable);
  }
  
  ZygoteStartFailedEx(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
