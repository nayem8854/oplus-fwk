package android.os;

import android.content.Context;
import android.os.storage.StorageManager;
import android.system.ErrnoException;
import android.system.Os;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.Arrays;
import libcore.io.IoUtils;
import libcore.util.EmptyArray;

public class RedactingFileDescriptor {
  private static final boolean DEBUG = true;
  
  private static final String TAG = "RedactingFileDescriptor";
  
  private final ProxyFileDescriptorCallback mCallback;
  
  private volatile long[] mFreeOffsets;
  
  private FileDescriptor mInner = null;
  
  private ParcelFileDescriptor mOuter = null;
  
  private volatile long[] mRedactRanges;
  
  private static long[] checkRangesArgument(long[] paramArrayOflong) {
    if (paramArrayOflong.length % 2 == 0) {
      for (byte b = 0; b < paramArrayOflong.length - 1; ) {
        if (paramArrayOflong[b] <= paramArrayOflong[b + 1]) {
          b += 2;
          continue;
        } 
        throw new IllegalArgumentException();
      } 
      return paramArrayOflong;
    } 
    throw new IllegalArgumentException();
  }
  
  public static ParcelFileDescriptor open(Context paramContext, File paramFile, int paramInt, long[] paramArrayOflong1, long[] paramArrayOflong2) throws IOException {
    return (new RedactingFileDescriptor(paramContext, paramFile, paramInt, paramArrayOflong1, paramArrayOflong2)).mOuter;
  }
  
  public static long[] removeRange(long[] paramArrayOflong, long paramLong1, long paramLong2) {
    if (paramLong1 == paramLong2)
      return paramArrayOflong; 
    if (paramLong1 <= paramLong2) {
      long[] arrayOfLong = EmptyArray.LONG;
      for (byte b = 0; b < paramArrayOflong.length; b += 2) {
        if (paramLong1 > paramArrayOflong[b] || paramLong2 < paramArrayOflong[b + 1])
          if (paramLong1 >= paramArrayOflong[b] && paramLong2 <= paramArrayOflong[b + 1]) {
            arrayOfLong = Arrays.copyOf(arrayOfLong, arrayOfLong.length + 4);
            arrayOfLong[arrayOfLong.length - 4] = paramArrayOflong[b];
            arrayOfLong[arrayOfLong.length - 3] = paramLong1;
            arrayOfLong[arrayOfLong.length - 2] = paramLong2;
            arrayOfLong[arrayOfLong.length - 1] = paramArrayOflong[b + 1];
          } else {
            arrayOfLong = Arrays.copyOf(arrayOfLong, arrayOfLong.length + 2);
            if (paramLong2 >= paramArrayOflong[b] && paramLong2 <= paramArrayOflong[b + 1]) {
              arrayOfLong[arrayOfLong.length - 2] = Math.max(paramArrayOflong[b], paramLong2);
            } else {
              arrayOfLong[arrayOfLong.length - 2] = paramArrayOflong[b];
            } 
            if (paramLong1 >= paramArrayOflong[b] && paramLong1 <= paramArrayOflong[b + 1]) {
              arrayOfLong[arrayOfLong.length - 1] = Math.min(paramArrayOflong[b + 1], paramLong1);
            } else {
              arrayOfLong[arrayOfLong.length - 1] = paramArrayOflong[b + 1];
            } 
          }  
      } 
      return arrayOfLong;
    } 
    throw new IllegalArgumentException();
  }
  
  private RedactingFileDescriptor(Context paramContext, File paramFile, int paramInt, long[] paramArrayOflong1, long[] paramArrayOflong2) throws IOException {
    this.mCallback = (ProxyFileDescriptorCallback)new Object(this);
    this.mRedactRanges = checkRangesArgument(paramArrayOflong1);
    this.mFreeOffsets = paramArrayOflong2;
    try {
      String str = paramFile.getAbsolutePath();
      int i = FileUtils.translateModePfdToPosix(paramInt);
      this.mInner = Os.open(str, i, 0);
      StorageManager storageManager = (StorageManager)paramContext.getSystemService(StorageManager.class);
      ProxyFileDescriptorCallback proxyFileDescriptorCallback = this.mCallback;
      this.mOuter = storageManager.openProxyFileDescriptor(paramInt, proxyFileDescriptorCallback);
      return;
    } catch (ErrnoException errnoException) {
      throw errnoException.rethrowAsIOException();
    } catch (IOException iOException) {}
    IoUtils.closeQuietly(this.mInner);
    IoUtils.closeQuietly(this.mOuter);
    throw iOException;
  }
}
