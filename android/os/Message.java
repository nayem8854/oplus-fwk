package android.os;

import android.util.TimeUtils;
import android.util.proto.ProtoOutputStream;

public final class Message implements Parcelable {
  public int sendingUid = -1;
  
  public int workSourceUid = -1;
  
  public static final Object sPoolSync = new Object();
  
  public static final Parcelable.Creator<Message> CREATOR;
  
  static final int FLAGS_TO_CLEAR_ON_COPY_FROM = 1;
  
  static final int FLAG_ASYNCHRONOUS = 2;
  
  static final int FLAG_IN_USE = 1;
  
  private static final int MAX_POOL_SIZE = 50;
  
  public static final int UID_NONE = -1;
  
  private static boolean gCheckRecycle;
  
  private static Message sPool;
  
  private static int sPoolSize;
  
  public int arg1;
  
  public int arg2;
  
  Runnable callback;
  
  Bundle data;
  
  int flags;
  
  Message next;
  
  public Object obj;
  
  public Messenger replyTo;
  
  Handler target;
  
  public int what;
  
  public long when;
  
  static {
    sPoolSize = 0;
    gCheckRecycle = true;
    CREATOR = new Parcelable.Creator<Message>() {
        public Message createFromParcel(Parcel param1Parcel) {
          Message message = Message.obtain();
          message.readFromParcel(param1Parcel);
          return message;
        }
        
        public Message[] newArray(int param1Int) {
          return new Message[param1Int];
        }
      };
  }
  
  public static Message obtain() {
    synchronized (sPoolSync) {
      if (sPool != null) {
        Message message = sPool;
        sPool = message.next;
        message.next = null;
        message.flags = 0;
        sPoolSize--;
        return message;
      } 
      return new Message();
    } 
  }
  
  public static Message obtain(Message paramMessage) {
    Message message = obtain();
    message.what = paramMessage.what;
    message.arg1 = paramMessage.arg1;
    message.arg2 = paramMessage.arg2;
    message.obj = paramMessage.obj;
    message.replyTo = paramMessage.replyTo;
    message.sendingUid = paramMessage.sendingUid;
    message.workSourceUid = paramMessage.workSourceUid;
    if (paramMessage.data != null)
      message.data = new Bundle(paramMessage.data); 
    message.target = paramMessage.target;
    message.callback = paramMessage.callback;
    return message;
  }
  
  public static Message obtain(Handler paramHandler) {
    Message message = obtain();
    message.target = paramHandler;
    return message;
  }
  
  public static Message obtain(Handler paramHandler, Runnable paramRunnable) {
    Message message = obtain();
    message.target = paramHandler;
    message.callback = paramRunnable;
    return message;
  }
  
  public static Message obtain(Handler paramHandler, int paramInt) {
    Message message = obtain();
    message.target = paramHandler;
    message.what = paramInt;
    return message;
  }
  
  public static Message obtain(Handler paramHandler, int paramInt, Object paramObject) {
    Message message = obtain();
    message.target = paramHandler;
    message.what = paramInt;
    message.obj = paramObject;
    return message;
  }
  
  public static Message obtain(Handler paramHandler, int paramInt1, int paramInt2, int paramInt3) {
    Message message = obtain();
    message.target = paramHandler;
    message.what = paramInt1;
    message.arg1 = paramInt2;
    message.arg2 = paramInt3;
    return message;
  }
  
  public static Message obtain(Handler paramHandler, int paramInt1, int paramInt2, int paramInt3, Object paramObject) {
    Message message = obtain();
    message.target = paramHandler;
    message.what = paramInt1;
    message.arg1 = paramInt2;
    message.arg2 = paramInt3;
    message.obj = paramObject;
    return message;
  }
  
  public static void updateCheckRecycle(int paramInt) {
    if (paramInt < 21)
      gCheckRecycle = false; 
  }
  
  public void recycle() {
    if (isInUse()) {
      if (!gCheckRecycle)
        return; 
      throw new IllegalStateException("This message cannot be recycled because it is still in use.");
    } 
    recycleUnchecked();
  }
  
  void recycleUnchecked() {
    this.flags = 1;
    this.what = 0;
    this.arg1 = 0;
    this.arg2 = 0;
    this.obj = null;
    this.replyTo = null;
    this.sendingUid = -1;
    this.workSourceUid = -1;
    this.when = 0L;
    this.target = null;
    this.callback = null;
    this.data = null;
    synchronized (sPoolSync) {
      if (sPoolSize < 50) {
        this.next = sPool;
        sPool = this;
        sPoolSize++;
      } 
      return;
    } 
  }
  
  public void copyFrom(Message paramMessage) {
    paramMessage.flags &= 0xFFFFFFFE;
    this.what = paramMessage.what;
    this.arg1 = paramMessage.arg1;
    this.arg2 = paramMessage.arg2;
    this.obj = paramMessage.obj;
    this.replyTo = paramMessage.replyTo;
    this.sendingUid = paramMessage.sendingUid;
    this.workSourceUid = paramMessage.workSourceUid;
    Bundle bundle = paramMessage.data;
    if (bundle != null) {
      this.data = (Bundle)bundle.clone();
    } else {
      this.data = null;
    } 
  }
  
  public long getWhen() {
    return this.when;
  }
  
  public void setTarget(Handler paramHandler) {
    this.target = paramHandler;
  }
  
  public Handler getTarget() {
    return this.target;
  }
  
  public Runnable getCallback() {
    return this.callback;
  }
  
  public Message setCallback(Runnable paramRunnable) {
    this.callback = paramRunnable;
    return this;
  }
  
  public Bundle getData() {
    if (this.data == null)
      this.data = new Bundle(); 
    return this.data;
  }
  
  public Bundle peekData() {
    return this.data;
  }
  
  public void setData(Bundle paramBundle) {
    this.data = paramBundle;
  }
  
  public Message setWhat(int paramInt) {
    this.what = paramInt;
    return this;
  }
  
  public void sendToTarget() {
    this.target.sendMessage(this);
  }
  
  public boolean isAsynchronous() {
    boolean bool;
    if ((this.flags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setAsynchronous(boolean paramBoolean) {
    if (paramBoolean) {
      this.flags |= 0x2;
    } else {
      this.flags &= 0xFFFFFFFD;
    } 
  }
  
  boolean isInUse() {
    int i = this.flags;
    boolean bool = true;
    if ((i & 0x1) != 1)
      bool = false; 
    return bool;
  }
  
  void markInUse() {
    this.flags |= 0x1;
  }
  
  public String toString() {
    return toString(SystemClock.uptimeMillis());
  }
  
  String toString(long paramLong) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{ when=");
    TimeUtils.formatDuration(this.when - paramLong, stringBuilder);
    if (this.target != null) {
      if (this.callback != null) {
        stringBuilder.append(" callback=");
        stringBuilder.append(this.callback.getClass().getName());
      } else {
        stringBuilder.append(" what=");
        stringBuilder.append(this.what);
      } 
      if (this.arg1 != 0) {
        stringBuilder.append(" arg1=");
        stringBuilder.append(this.arg1);
      } 
      if (this.arg2 != 0) {
        stringBuilder.append(" arg2=");
        stringBuilder.append(this.arg2);
      } 
      if (this.obj != null) {
        stringBuilder.append(" obj=");
        stringBuilder.append(this.obj);
      } 
      stringBuilder.append(" target=");
      stringBuilder.append(this.target.getClass().getName());
    } else {
      stringBuilder.append(" barrier=");
      stringBuilder.append(this.arg1);
    } 
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1112396529665L, this.when);
    if (this.target != null) {
      Runnable runnable = this.callback;
      if (runnable != null) {
        paramProtoOutputStream.write(1138166333442L, runnable.getClass().getName());
      } else {
        paramProtoOutputStream.write(1120986464259L, this.what);
      } 
      int i = this.arg1;
      if (i != 0)
        paramProtoOutputStream.write(1120986464260L, i); 
      i = this.arg2;
      if (i != 0)
        paramProtoOutputStream.write(1120986464261L, i); 
      Object object = this.obj;
      if (object != null)
        paramProtoOutputStream.write(1138166333446L, object.toString()); 
      paramProtoOutputStream.write(1138166333447L, this.target.getClass().getName());
    } else {
      paramProtoOutputStream.write(1120986464264L, this.arg1);
    } 
    paramProtoOutputStream.end(paramLong);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (this.callback == null) {
      paramParcel.writeInt(this.what);
      paramParcel.writeInt(this.arg1);
      paramParcel.writeInt(this.arg2);
      Object object = this.obj;
      if (object != null) {
        try {
          object = object;
          paramParcel.writeInt(1);
          paramParcel.writeParcelable((Parcelable)object, paramInt);
        } catch (ClassCastException classCastException) {
          throw new RuntimeException("Can't marshal non-Parcelable objects across processes.");
        } 
      } else {
        classCastException.writeInt(0);
      } 
      classCastException.writeLong(this.when);
      classCastException.writeBundle(this.data);
      Messenger.writeMessengerOrNullToParcel(this.replyTo, (Parcel)classCastException);
      classCastException.writeInt(this.sendingUid);
      classCastException.writeInt(this.workSourceUid);
      return;
    } 
    throw new RuntimeException("Can't marshal callbacks across processes.");
  }
  
  private void readFromParcel(Parcel paramParcel) {
    this.what = paramParcel.readInt();
    this.arg1 = paramParcel.readInt();
    this.arg2 = paramParcel.readInt();
    if (paramParcel.readInt() != 0)
      this.obj = paramParcel.readParcelable(getClass().getClassLoader()); 
    this.when = paramParcel.readLong();
    this.data = paramParcel.readBundle();
    this.replyTo = Messenger.readMessengerOrNullFromParcel(paramParcel);
    this.sendingUid = paramParcel.readInt();
    this.workSourceUid = paramParcel.readInt();
  }
  
  String toStringLite(long paramLong, boolean paramBoolean) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{ when=");
    TimeUtils.formatDuration(this.when - paramLong, stringBuilder);
    if (this.target != null) {
      stringBuilder.append(" what=");
      stringBuilder.append(this.what);
      stringBuilder.append(" target=");
      stringBuilder.append(this.target.getClass().getName());
      if (this.callback != null) {
        stringBuilder.append(" callback=");
        stringBuilder.append(this.callback.getClass().getName());
      } 
      if (this.arg1 != 0) {
        stringBuilder.append(" arg1=");
        stringBuilder.append(this.arg1);
      } 
      if (this.arg2 != 0) {
        stringBuilder.append(" arg2=");
        stringBuilder.append(this.arg2);
      } 
      if (paramBoolean && this.obj != null) {
        stringBuilder.append(" obj=");
        stringBuilder.append(this.obj);
      } 
    } else {
      stringBuilder.append(" barrier=");
      stringBuilder.append(this.arg1);
      if (this.callback != null) {
        stringBuilder.append(" callback=");
        stringBuilder.append(this.callback);
      } 
      if (paramBoolean && this.obj != null) {
        stringBuilder.append(" obj=");
        stringBuilder.append(this.obj);
      } 
    } 
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
}
