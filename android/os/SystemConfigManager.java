package android.os;

import android.annotation.SystemApi;
import android.util.ArraySet;
import android.util.Log;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SystemApi
public class SystemConfigManager {
  private static final String TAG = SystemConfigManager.class.getSimpleName();
  
  private final ISystemConfig mInterface;
  
  public SystemConfigManager() {
    IBinder iBinder = ServiceManager.getService("system_config");
    this.mInterface = ISystemConfig.Stub.asInterface(iBinder);
  }
  
  public Set<String> getDisabledUntilUsedPreinstalledCarrierApps() {
    try {
      List<String> list = this.mInterface.getDisabledUntilUsedPreinstalledCarrierApps();
      return (Set<String>)new ArraySet(list);
    } catch (RemoteException remoteException) {
      Log.e(TAG, "Caught remote exception");
      return Collections.emptySet();
    } 
  }
  
  public Map<String, List<String>> getDisabledUntilUsedPreinstalledCarrierAssociatedApps() {
    try {
      ISystemConfig iSystemConfig = this.mInterface;
      return 
        iSystemConfig.getDisabledUntilUsedPreinstalledCarrierAssociatedApps();
    } catch (RemoteException remoteException) {
      Log.e(TAG, "Caught remote exception");
      return Collections.emptyMap();
    } 
  }
  
  public Map<String, List<CarrierAssociatedAppEntry>> getDisabledUntilUsedPreinstalledCarrierAssociatedAppEntries() {
    try {
      ISystemConfig iSystemConfig = this.mInterface;
      return 
        iSystemConfig.getDisabledUntilUsedPreinstalledCarrierAssociatedAppEntries();
    } catch (RemoteException remoteException) {
      Log.e(TAG, "Caught remote exception", (Throwable)remoteException);
      return Collections.emptyMap();
    } 
  }
}
