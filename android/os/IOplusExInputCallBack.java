package android.os;

import android.view.InputEvent;

public interface IOplusExInputCallBack extends IInterface {
  void onInputEvent(InputEvent paramInputEvent) throws RemoteException;
  
  class Default implements IOplusExInputCallBack {
    public void onInputEvent(InputEvent param1InputEvent) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusExInputCallBack {
    private static final String DESCRIPTOR = "android.os.IOplusExInputCallBack";
    
    static final int TRANSACTION_onInputEvent = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IOplusExInputCallBack");
    }
    
    public static IOplusExInputCallBack asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IOplusExInputCallBack");
      if (iInterface != null && iInterface instanceof IOplusExInputCallBack)
        return (IOplusExInputCallBack)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onInputEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.IOplusExInputCallBack");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IOplusExInputCallBack");
      if (param1Parcel1.readInt() != 0) {
        InputEvent inputEvent = InputEvent.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onInputEvent((InputEvent)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOplusExInputCallBack {
      public static IOplusExInputCallBack sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IOplusExInputCallBack";
      }
      
      public void onInputEvent(InputEvent param2InputEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusExInputCallBack");
          if (param2InputEvent != null) {
            parcel.writeInt(1);
            param2InputEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IOplusExInputCallBack.Stub.getDefaultImpl() != null) {
            IOplusExInputCallBack.Stub.getDefaultImpl().onInputEvent(param2InputEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusExInputCallBack param1IOplusExInputCallBack) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusExInputCallBack != null) {
          Proxy.sDefaultImpl = param1IOplusExInputCallBack;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusExInputCallBack getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
