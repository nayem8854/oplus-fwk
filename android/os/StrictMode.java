package android.os;

import android.app.ActivityManager;
import android.app.IActivityManager;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.storage.IStorageManager;
import android.os.strictmode.CleartextNetworkViolation;
import android.os.strictmode.ContentUriWithoutPermissionViolation;
import android.os.strictmode.CredentialProtectedWhileLockedViolation;
import android.os.strictmode.CustomViolation;
import android.os.strictmode.DiskReadViolation;
import android.os.strictmode.DiskWriteViolation;
import android.os.strictmode.ExplicitGcViolation;
import android.os.strictmode.FileUriExposedViolation;
import android.os.strictmode.ImplicitDirectBootViolation;
import android.os.strictmode.IncorrectContextUseViolation;
import android.os.strictmode.InstanceCountViolation;
import android.os.strictmode.IntentReceiverLeakedViolation;
import android.os.strictmode.LeakedClosableViolation;
import android.os.strictmode.NetworkViolation;
import android.os.strictmode.NonSdkApiUsedViolation;
import android.os.strictmode.ResourceMismatchViolation;
import android.os.strictmode.ServiceConnectionLeakedViolation;
import android.os.strictmode.SqliteObjectLeakedViolation;
import android.os.strictmode.UnbufferedIoViolation;
import android.os.strictmode.UntaggedSocketViolation;
import android.os.strictmode.Violation;
import android.os.strictmode.WebViewMethodCalledOnWrongThreadViolation;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Printer;
import android.util.Singleton;
import android.view.IWindowManager;
import com.android.internal.os.BackgroundThread;
import com.android.internal.os.RuntimeInit;
import com.android.internal.util.FastPrintWriter;
import com.android.internal.util.HexDump;
import dalvik.system.BlockGuard;
import dalvik.system.CloseGuard;
import dalvik.system.VMDebug;
import dalvik.system.VMRuntime;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public final class StrictMode {
  private static final String CLEARTEXT_PROPERTY = "persist.sys.strictmode.clear";
  
  private static final int DETECT_THREAD_ALL = 65535;
  
  private static final int DETECT_THREAD_CUSTOM = 8;
  
  private static final int DETECT_THREAD_DISK_READ = 2;
  
  private static final int DETECT_THREAD_DISK_WRITE = 1;
  
  private static final int DETECT_THREAD_EXPLICIT_GC = 64;
  
  private static final int DETECT_THREAD_NETWORK = 4;
  
  private static final int DETECT_THREAD_RESOURCE_MISMATCH = 16;
  
  private static final int DETECT_THREAD_UNBUFFERED_IO = 32;
  
  private static final int DETECT_VM_ACTIVITY_LEAKS = 4;
  
  private static final int DETECT_VM_ALL = 65535;
  
  private static final int DETECT_VM_CLEARTEXT_NETWORK = 64;
  
  private static final int DETECT_VM_CLOSABLE_LEAKS = 2;
  
  private static final int DETECT_VM_CONTENT_URI_WITHOUT_PERMISSION = 128;
  
  private static final int DETECT_VM_CREDENTIAL_PROTECTED_WHILE_LOCKED = 2048;
  
  private static final int DETECT_VM_CURSOR_LEAKS = 1;
  
  private static final int DETECT_VM_FILE_URI_EXPOSURE = 32;
  
  private static final int DETECT_VM_IMPLICIT_DIRECT_BOOT = 1024;
  
  private static final int DETECT_VM_INCORRECT_CONTEXT_USE = 4096;
  
  private static final int DETECT_VM_INSTANCE_LEAKS = 8;
  
  private static final int DETECT_VM_NON_SDK_API_USAGE = 512;
  
  private static final int DETECT_VM_REGISTRATION_LEAKS = 16;
  
  private static final int DETECT_VM_UNTAGGED_SOCKET = 256;
  
  private static final boolean DISABLE = false;
  
  public static final String DISABLE_PROPERTY = "persist.sys.strictmode.disable";
  
  private static final HashMap<Class, Integer> EMPTY_CLASS_LIMIT_MAP;
  
  private static final ViolationLogger LOGCAT_LOGGER;
  
  private static final boolean LOG_V = Log.isLoggable("StrictMode", 2);
  
  private static final int MAX_OFFENSES_PER_LOOP = 10;
  
  private static final int MAX_SPAN_TAGS = 20;
  
  private static final long MIN_DIALOG_INTERVAL_MS = 30000L;
  
  private static final long MIN_DROPBOX_INTERVAL_MS = 3000L;
  
  private static final long MIN_LOG_INTERVAL_MS = 1000L;
  
  private static final long MIN_VM_INTERVAL_MS = 1000L;
  
  public static final int NETWORK_POLICY_ACCEPT = 0;
  
  public static final int NETWORK_POLICY_LOG = 1;
  
  public static final int NETWORK_POLICY_REJECT = 2;
  
  private static final Span NO_OP_SPAN;
  
  public static final int PENALTY_ALL = -65536;
  
  public static final int PENALTY_DEATH = 268435456;
  
  public static final int PENALTY_DEATH_ON_CLEARTEXT_NETWORK = 16777216;
  
  public static final int PENALTY_DEATH_ON_FILE_URI_EXPOSURE = 8388608;
  
  public static final int PENALTY_DEATH_ON_NETWORK = 33554432;
  
  public static final int PENALTY_DIALOG = 536870912;
  
  public static final int PENALTY_DROPBOX = 67108864;
  
  public static final int PENALTY_FLASH = 134217728;
  
  public static final int PENALTY_GATHER = -2147483648;
  
  public static final int PENALTY_LOG = 1073741824;
  
  private static final String TAG = "StrictMode";
  
  private static final ThreadLocal<AndroidBlockGuardPolicy> THREAD_ANDROID_POLICY;
  
  private static final ThreadLocal<Handler> THREAD_HANDLER;
  
  public static final String VISUAL_PROPERTY = "persist.sys.strictmode.visual";
  
  private static final BlockGuard.VmPolicy VM_ANDROID_POLICY;
  
  private static final ThreadLocal<ArrayList<ViolationInfo>> gatheredViolations;
  
  private static final AtomicInteger sDropboxCallsInFlight;
  
  private static final HashMap<Class, Integer> sExpectedActivityInstanceCount;
  
  private static boolean sIsIdlerRegistered;
  
  private static long sLastInstanceCountCheckMillis;
  
  private static final HashMap<Integer, Long> sLastVmViolationTime;
  
  private static volatile ViolationLogger sLogger;
  
  private static final Consumer<String> sNonSdkApiUsageConsumer;
  
  private static final MessageQueue.IdleHandler sProcessIdleHandler;
  
  private static final ThreadLocal<ThreadSpanState> sThisThreadSpanState;
  
  private static final ThreadLocal<Executor> sThreadViolationExecutor;
  
  private static final ThreadLocal<OnThreadViolationListener> sThreadViolationListener;
  
  private static volatile boolean sUserKeyUnlocked;
  
  private static volatile VmPolicy sVmPolicy;
  
  private static Singleton<IWindowManager> sWindowManager;
  
  private static final ThreadLocal<ArrayList<ViolationInfo>> violationsBeingTimed;
  
  static {
    EMPTY_CLASS_LIMIT_MAP = new HashMap<>();
    sVmPolicy = VmPolicy.LAX;
    -$.Lambda.StrictMode.yH8AK0bTwVwZOb9x8HoiSBdzr0 yH8AK0bTwVwZOb9x8HoiSBdzr0 = _$$Lambda$StrictMode$1yH8AK0bTwVwZOb9x8HoiSBdzr0.INSTANCE;
    sLogger = (ViolationLogger)yH8AK0bTwVwZOb9x8HoiSBdzr0;
    sThreadViolationListener = new ThreadLocal<>();
    sThreadViolationExecutor = new ThreadLocal<>();
    sDropboxCallsInFlight = new AtomicInteger(0);
    sNonSdkApiUsageConsumer = (Consumer<String>)_$$Lambda$StrictMode$lu9ekkHJ2HMz0jd3F8K8MnhenxQ.INSTANCE;
    gatheredViolations = new ThreadLocal<ArrayList<ViolationInfo>>() {
        protected ArrayList<StrictMode.ViolationInfo> initialValue() {
          return null;
        }
      };
    violationsBeingTimed = new ThreadLocal<ArrayList<ViolationInfo>>() {
        protected ArrayList<StrictMode.ViolationInfo> initialValue() {
          return new ArrayList<>();
        }
      };
    THREAD_HANDLER = new ThreadLocal<Handler>() {
        protected Handler initialValue() {
          return new Handler();
        }
      };
    THREAD_ANDROID_POLICY = new ThreadLocal<AndroidBlockGuardPolicy>() {
        protected StrictMode.AndroidBlockGuardPolicy initialValue() {
          return new StrictMode.AndroidBlockGuardPolicy(0);
        }
      };
    VM_ANDROID_POLICY = new BlockGuard.VmPolicy() {
        public void onPathAccess(String param1String) {
          if (param1String == null)
            return; 
          if (param1String.startsWith("/data/user/") || 
            param1String.startsWith("/data/media/") || 
            param1String.startsWith("/data/system_ce/") || 
            param1String.startsWith("/data/misc_ce/") || 
            param1String.startsWith("/data/vendor_ce/") || 
            param1String.startsWith("/storage/emulated/")) {
            int i = param1String.indexOf('/', 1);
            int j = param1String.indexOf('/', i + 1);
            i = param1String.indexOf('/', j + 1);
            if (i == -1)
              return; 
            try {
              i = Integer.parseInt(param1String.substring(j + 1, i));
              StrictMode.onCredentialProtectedPathAccess(param1String, i);
            } catch (NumberFormatException numberFormatException) {}
            return;
          } 
          if (numberFormatException.startsWith("/data/data/"))
            StrictMode.onCredentialProtectedPathAccess((String)numberFormatException, 0); 
        }
      };
    sLastInstanceCountCheckMillis = 0L;
    sIsIdlerRegistered = false;
    sProcessIdleHandler = (MessageQueue.IdleHandler)new Object();
    sUserKeyUnlocked = false;
    sLastVmViolationTime = new HashMap<>();
    NO_OP_SPAN = (Span)new Object();
    sThisThreadSpanState = new ThreadLocal<ThreadSpanState>() {
        protected StrictMode.ThreadSpanState initialValue() {
          return new StrictMode.ThreadSpanState();
        }
      };
    sWindowManager = (Singleton<IWindowManager>)new Object();
    sExpectedActivityInstanceCount = new HashMap<>();
  }
  
  public static void setViolationLogger(ViolationLogger paramViolationLogger) {
    ViolationLogger violationLogger = paramViolationLogger;
    if (paramViolationLogger == null)
      violationLogger = LOGCAT_LOGGER; 
    sLogger = violationLogger;
  }
  
  public static final class ThreadPolicy {
    public static final ThreadPolicy LAX = new ThreadPolicy(0, null, null);
    
    final Executor mCallbackExecutor;
    
    final StrictMode.OnThreadViolationListener mListener;
    
    final int mask;
    
    private ThreadPolicy(int param1Int, StrictMode.OnThreadViolationListener param1OnThreadViolationListener, Executor param1Executor) {
      this.mask = param1Int;
      this.mListener = param1OnThreadViolationListener;
      this.mCallbackExecutor = param1Executor;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[StrictMode.ThreadPolicy; mask=");
      stringBuilder.append(this.mask);
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public static final class Builder {
      private Executor mExecutor;
      
      private StrictMode.OnThreadViolationListener mListener;
      
      private int mMask = 0;
      
      public Builder() {
        this.mMask = 0;
      }
      
      public Builder(StrictMode.ThreadPolicy param2ThreadPolicy) {
        this.mMask = param2ThreadPolicy.mask;
        this.mListener = param2ThreadPolicy.mListener;
        this.mExecutor = param2ThreadPolicy.mCallbackExecutor;
      }
      
      public Builder detectAll() {
        detectDiskReads();
        detectDiskWrites();
        detectNetwork();
        int i = VMRuntime.getRuntime().getTargetSdkVersion();
        if (i >= 11)
          detectCustomSlowCalls(); 
        if (i >= 23)
          detectResourceMismatches(); 
        if (i >= 26)
          detectUnbufferedIo(); 
        return this;
      }
      
      public Builder permitAll() {
        return disable(65535);
      }
      
      public Builder detectNetwork() {
        return enable(4);
      }
      
      public Builder permitNetwork() {
        return disable(4);
      }
      
      public Builder detectDiskReads() {
        return enable(2);
      }
      
      public Builder permitDiskReads() {
        return disable(2);
      }
      
      public Builder detectCustomSlowCalls() {
        return enable(8);
      }
      
      public Builder permitCustomSlowCalls() {
        return disable(8);
      }
      
      public Builder permitResourceMismatches() {
        return disable(16);
      }
      
      public Builder detectUnbufferedIo() {
        return enable(32);
      }
      
      public Builder permitUnbufferedIo() {
        return disable(32);
      }
      
      public Builder detectResourceMismatches() {
        return enable(16);
      }
      
      public Builder detectDiskWrites() {
        return enable(1);
      }
      
      public Builder permitDiskWrites() {
        return disable(1);
      }
      
      public Builder detectExplicitGc() {
        return enable(64);
      }
      
      public Builder permitExplicitGc() {
        return disable(64);
      }
      
      public Builder penaltyDialog() {
        return enable(536870912);
      }
      
      public Builder penaltyDeath() {
        return enable(268435456);
      }
      
      public Builder penaltyDeathOnNetwork() {
        return enable(33554432);
      }
      
      public Builder penaltyFlashScreen() {
        return enable(134217728);
      }
      
      public Builder penaltyLog() {
        return enable(1073741824);
      }
      
      public Builder penaltyDropBox() {
        return enable(67108864);
      }
      
      public Builder penaltyListener(Executor param2Executor, StrictMode.OnThreadViolationListener param2OnThreadViolationListener) {
        if (param2Executor != null) {
          this.mListener = param2OnThreadViolationListener;
          this.mExecutor = param2Executor;
          return this;
        } 
        throw new NullPointerException("executor must not be null");
      }
      
      public Builder penaltyListener(StrictMode.OnThreadViolationListener param2OnThreadViolationListener, Executor param2Executor) {
        return penaltyListener(param2Executor, param2OnThreadViolationListener);
      }
      
      private Builder enable(int param2Int) {
        this.mMask |= param2Int;
        return this;
      }
      
      private Builder disable(int param2Int) {
        this.mMask &= param2Int ^ 0xFFFFFFFF;
        return this;
      }
      
      public StrictMode.ThreadPolicy build() {
        if (this.mListener == null) {
          int i = this.mMask;
          if (i != 0 && (i & 0x74000000) == 0)
            penaltyLog(); 
        } 
        return new StrictMode.ThreadPolicy(this.mMask, this.mListener, this.mExecutor);
      }
    }
  }
  
  public static final class Builder {
    private Executor mExecutor;
    
    private StrictMode.OnThreadViolationListener mListener;
    
    private int mMask = 0;
    
    public Builder() {
      this.mMask = 0;
    }
    
    public Builder(StrictMode.ThreadPolicy param1ThreadPolicy) {
      this.mMask = param1ThreadPolicy.mask;
      this.mListener = param1ThreadPolicy.mListener;
      this.mExecutor = param1ThreadPolicy.mCallbackExecutor;
    }
    
    public Builder detectAll() {
      detectDiskReads();
      detectDiskWrites();
      detectNetwork();
      int i = VMRuntime.getRuntime().getTargetSdkVersion();
      if (i >= 11)
        detectCustomSlowCalls(); 
      if (i >= 23)
        detectResourceMismatches(); 
      if (i >= 26)
        detectUnbufferedIo(); 
      return this;
    }
    
    public Builder permitAll() {
      return disable(65535);
    }
    
    public Builder detectNetwork() {
      return enable(4);
    }
    
    public Builder permitNetwork() {
      return disable(4);
    }
    
    public Builder detectDiskReads() {
      return enable(2);
    }
    
    public Builder permitDiskReads() {
      return disable(2);
    }
    
    public Builder detectCustomSlowCalls() {
      return enable(8);
    }
    
    public Builder permitCustomSlowCalls() {
      return disable(8);
    }
    
    public Builder permitResourceMismatches() {
      return disable(16);
    }
    
    public Builder detectUnbufferedIo() {
      return enable(32);
    }
    
    public Builder permitUnbufferedIo() {
      return disable(32);
    }
    
    public Builder detectResourceMismatches() {
      return enable(16);
    }
    
    public Builder detectDiskWrites() {
      return enable(1);
    }
    
    public Builder permitDiskWrites() {
      return disable(1);
    }
    
    public Builder detectExplicitGc() {
      return enable(64);
    }
    
    public Builder permitExplicitGc() {
      return disable(64);
    }
    
    public Builder penaltyDialog() {
      return enable(536870912);
    }
    
    public Builder penaltyDeath() {
      return enable(268435456);
    }
    
    public Builder penaltyDeathOnNetwork() {
      return enable(33554432);
    }
    
    public Builder penaltyFlashScreen() {
      return enable(134217728);
    }
    
    public Builder penaltyLog() {
      return enable(1073741824);
    }
    
    public Builder penaltyDropBox() {
      return enable(67108864);
    }
    
    public Builder penaltyListener(Executor param1Executor, StrictMode.OnThreadViolationListener param1OnThreadViolationListener) {
      if (param1Executor != null) {
        this.mListener = param1OnThreadViolationListener;
        this.mExecutor = param1Executor;
        return this;
      } 
      throw new NullPointerException("executor must not be null");
    }
    
    public Builder penaltyListener(StrictMode.OnThreadViolationListener param1OnThreadViolationListener, Executor param1Executor) {
      return penaltyListener(param1Executor, param1OnThreadViolationListener);
    }
    
    private Builder enable(int param1Int) {
      this.mMask |= param1Int;
      return this;
    }
    
    private Builder disable(int param1Int) {
      this.mMask &= param1Int ^ 0xFFFFFFFF;
      return this;
    }
    
    public StrictMode.ThreadPolicy build() {
      if (this.mListener == null) {
        int i = this.mMask;
        if (i != 0 && (i & 0x74000000) == 0)
          penaltyLog(); 
      } 
      return new StrictMode.ThreadPolicy(this.mMask, this.mListener, this.mExecutor);
    }
  }
  
  public static final class VmPolicy {
    public static final VmPolicy LAX = new VmPolicy(0, StrictMode.EMPTY_CLASS_LIMIT_MAP, null, null);
    
    final HashMap<Class, Integer> classInstanceLimit;
    
    final Executor mCallbackExecutor;
    
    final StrictMode.OnVmViolationListener mListener;
    
    final int mask;
    
    private VmPolicy(int param1Int, HashMap<Class, Integer> param1HashMap, StrictMode.OnVmViolationListener param1OnVmViolationListener, Executor param1Executor) {
      if (param1HashMap != null) {
        this.mask = param1Int;
        this.classInstanceLimit = param1HashMap;
        this.mListener = param1OnVmViolationListener;
        this.mCallbackExecutor = param1Executor;
        return;
      } 
      throw new NullPointerException("classInstanceLimit == null");
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("[StrictMode.VmPolicy; mask=");
      stringBuilder.append(this.mask);
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public static final class Builder {
      private HashMap<Class, Integer> mClassInstanceLimit;
      
      private boolean mClassInstanceLimitNeedCow = false;
      
      private Executor mExecutor;
      
      private StrictMode.OnVmViolationListener mListener;
      
      private int mMask;
      
      public Builder() {
        this.mMask = 0;
      }
      
      public Builder(StrictMode.VmPolicy param2VmPolicy) {
        this.mMask = param2VmPolicy.mask;
        this.mClassInstanceLimitNeedCow = true;
        this.mClassInstanceLimit = param2VmPolicy.classInstanceLimit;
        this.mListener = param2VmPolicy.mListener;
        this.mExecutor = param2VmPolicy.mCallbackExecutor;
      }
      
      public Builder setClassInstanceLimit(Class param2Class, int param2Int) {
        if (param2Class != null) {
          if (this.mClassInstanceLimitNeedCow) {
            if (this.mClassInstanceLimit.containsKey(param2Class)) {
              HashMap<Class, Integer> hashMap = this.mClassInstanceLimit;
              if (((Integer)hashMap.get(param2Class)).intValue() == param2Int)
                return this; 
            } 
            this.mClassInstanceLimitNeedCow = false;
            this.mClassInstanceLimit = (HashMap<Class, Integer>)this.mClassInstanceLimit.clone();
          } else if (this.mClassInstanceLimit == null) {
            this.mClassInstanceLimit = new HashMap<>();
          } 
          this.mMask |= 0x8;
          this.mClassInstanceLimit.put(param2Class, Integer.valueOf(param2Int));
          return this;
        } 
        throw new NullPointerException("klass == null");
      }
      
      public Builder detectActivityLeaks() {
        return enable(4);
      }
      
      public Builder permitActivityLeaks() {
        // Byte code:
        //   0: ldc android/os/StrictMode
        //   2: monitorenter
        //   3: invokestatic access$200 : ()Ljava/util/HashMap;
        //   6: invokevirtual clear : ()V
        //   9: ldc android/os/StrictMode
        //   11: monitorexit
        //   12: aload_0
        //   13: iconst_4
        //   14: invokevirtual disable : (I)Landroid/os/StrictMode$VmPolicy$Builder;
        //   17: areturn
        //   18: astore_1
        //   19: ldc android/os/StrictMode
        //   21: monitorexit
        //   22: aload_1
        //   23: athrow
        // Line number table:
        //   Java source line number -> byte code offset
        //   #825	-> 0
        //   #826	-> 3
        //   #827	-> 9
        //   #828	-> 12
        //   #827	-> 18
        // Exception table:
        //   from	to	target	type
        //   3	9	18	finally
        //   9	12	18	finally
        //   19	22	18	finally
      }
      
      public Builder detectNonSdkApiUsage() {
        return enable(512);
      }
      
      public Builder permitNonSdkApiUsage() {
        return disable(512);
      }
      
      public Builder detectAll() {
        detectLeakedSqlLiteObjects();
        int i = VMRuntime.getRuntime().getTargetSdkVersion();
        if (i >= 11) {
          detectActivityLeaks();
          detectLeakedClosableObjects();
        } 
        if (i >= 16)
          detectLeakedRegistrationObjects(); 
        if (i >= 18)
          detectFileUriExposure(); 
        if (i >= 23)
          if (SystemProperties.getBoolean("persist.sys.strictmode.clear", false))
            detectCleartextNetwork();  
        if (i >= 26) {
          detectContentUriWithoutPermission();
          detectUntaggedSockets();
        } 
        if (i >= 29)
          detectCredentialProtectedWhileLocked(); 
        if (i >= 30)
          detectIncorrectContextUse(); 
        return this;
      }
      
      public Builder detectLeakedSqlLiteObjects() {
        return enable(1);
      }
      
      public Builder detectLeakedClosableObjects() {
        return enable(2);
      }
      
      public Builder detectLeakedRegistrationObjects() {
        return enable(16);
      }
      
      public Builder detectFileUriExposure() {
        return enable(32);
      }
      
      public Builder detectCleartextNetwork() {
        return enable(64);
      }
      
      public Builder detectContentUriWithoutPermission() {
        return enable(128);
      }
      
      public Builder detectUntaggedSockets() {
        return enable(256);
      }
      
      public Builder permitUntaggedSockets() {
        return disable(256);
      }
      
      public Builder detectImplicitDirectBoot() {
        return enable(1024);
      }
      
      public Builder permitImplicitDirectBoot() {
        return disable(1024);
      }
      
      public Builder detectCredentialProtectedWhileLocked() {
        return enable(2048);
      }
      
      public Builder permitCredentialProtectedWhileLocked() {
        return disable(2048);
      }
      
      public Builder detectIncorrectContextUse() {
        return enable(4096);
      }
      
      public Builder permitIncorrectContextUse() {
        return disable(4096);
      }
      
      public Builder penaltyDeath() {
        return enable(268435456);
      }
      
      public Builder penaltyDeathOnCleartextNetwork() {
        return enable(16777216);
      }
      
      public Builder penaltyDeathOnFileUriExposure() {
        return enable(8388608);
      }
      
      public Builder penaltyLog() {
        return enable(1073741824);
      }
      
      public Builder penaltyDropBox() {
        return enable(67108864);
      }
      
      public Builder penaltyListener(Executor param2Executor, StrictMode.OnVmViolationListener param2OnVmViolationListener) {
        if (param2Executor != null) {
          this.mListener = param2OnVmViolationListener;
          this.mExecutor = param2Executor;
          return this;
        } 
        throw new NullPointerException("executor must not be null");
      }
      
      public Builder penaltyListener(StrictMode.OnVmViolationListener param2OnVmViolationListener, Executor param2Executor) {
        return penaltyListener(param2Executor, param2OnVmViolationListener);
      }
      
      private Builder enable(int param2Int) {
        this.mMask |= param2Int;
        return this;
      }
      
      Builder disable(int param2Int) {
        this.mMask &= param2Int ^ 0xFFFFFFFF;
        return this;
      }
      
      public StrictMode.VmPolicy build() {
        if (this.mListener == null) {
          int j = this.mMask;
          if (j != 0 && (j & 0x74000000) == 0)
            penaltyLog(); 
        } 
        int i = this.mMask;
        HashMap<Class, Integer> hashMap = this.mClassInstanceLimit;
        if (hashMap == null)
          hashMap = StrictMode.EMPTY_CLASS_LIMIT_MAP; 
        return new StrictMode.VmPolicy(i, hashMap, this.mListener, this.mExecutor);
      }
    }
  }
  
  public static final class Builder {
    private HashMap<Class, Integer> mClassInstanceLimit;
    
    private boolean mClassInstanceLimitNeedCow = false;
    
    private Executor mExecutor;
    
    private StrictMode.OnVmViolationListener mListener;
    
    private int mMask;
    
    public Builder() {
      this.mMask = 0;
    }
    
    public Builder(StrictMode.VmPolicy param1VmPolicy) {
      this.mMask = param1VmPolicy.mask;
      this.mClassInstanceLimitNeedCow = true;
      this.mClassInstanceLimit = param1VmPolicy.classInstanceLimit;
      this.mListener = param1VmPolicy.mListener;
      this.mExecutor = param1VmPolicy.mCallbackExecutor;
    }
    
    public Builder setClassInstanceLimit(Class param1Class, int param1Int) {
      if (param1Class != null) {
        if (this.mClassInstanceLimitNeedCow) {
          if (this.mClassInstanceLimit.containsKey(param1Class)) {
            HashMap<Class, Integer> hashMap = this.mClassInstanceLimit;
            if (((Integer)hashMap.get(param1Class)).intValue() == param1Int)
              return this; 
          } 
          this.mClassInstanceLimitNeedCow = false;
          this.mClassInstanceLimit = (HashMap<Class, Integer>)this.mClassInstanceLimit.clone();
        } else if (this.mClassInstanceLimit == null) {
          this.mClassInstanceLimit = new HashMap<>();
        } 
        this.mMask |= 0x8;
        this.mClassInstanceLimit.put(param1Class, Integer.valueOf(param1Int));
        return this;
      } 
      throw new NullPointerException("klass == null");
    }
    
    public Builder detectActivityLeaks() {
      return enable(4);
    }
    
    public Builder permitActivityLeaks() {
      // Byte code:
      //   0: ldc android/os/StrictMode
      //   2: monitorenter
      //   3: invokestatic access$200 : ()Ljava/util/HashMap;
      //   6: invokevirtual clear : ()V
      //   9: ldc android/os/StrictMode
      //   11: monitorexit
      //   12: aload_0
      //   13: iconst_4
      //   14: invokevirtual disable : (I)Landroid/os/StrictMode$VmPolicy$Builder;
      //   17: areturn
      //   18: astore_1
      //   19: ldc android/os/StrictMode
      //   21: monitorexit
      //   22: aload_1
      //   23: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #825	-> 0
      //   #826	-> 3
      //   #827	-> 9
      //   #828	-> 12
      //   #827	-> 18
      // Exception table:
      //   from	to	target	type
      //   3	9	18	finally
      //   9	12	18	finally
      //   19	22	18	finally
    }
    
    public Builder detectNonSdkApiUsage() {
      return enable(512);
    }
    
    public Builder permitNonSdkApiUsage() {
      return disable(512);
    }
    
    public Builder detectAll() {
      detectLeakedSqlLiteObjects();
      int i = VMRuntime.getRuntime().getTargetSdkVersion();
      if (i >= 11) {
        detectActivityLeaks();
        detectLeakedClosableObjects();
      } 
      if (i >= 16)
        detectLeakedRegistrationObjects(); 
      if (i >= 18)
        detectFileUriExposure(); 
      if (i >= 23)
        if (SystemProperties.getBoolean("persist.sys.strictmode.clear", false))
          detectCleartextNetwork();  
      if (i >= 26) {
        detectContentUriWithoutPermission();
        detectUntaggedSockets();
      } 
      if (i >= 29)
        detectCredentialProtectedWhileLocked(); 
      if (i >= 30)
        detectIncorrectContextUse(); 
      return this;
    }
    
    public Builder detectLeakedSqlLiteObjects() {
      return enable(1);
    }
    
    public Builder detectLeakedClosableObjects() {
      return enable(2);
    }
    
    public Builder detectLeakedRegistrationObjects() {
      return enable(16);
    }
    
    public Builder detectFileUriExposure() {
      return enable(32);
    }
    
    public Builder detectCleartextNetwork() {
      return enable(64);
    }
    
    public Builder detectContentUriWithoutPermission() {
      return enable(128);
    }
    
    public Builder detectUntaggedSockets() {
      return enable(256);
    }
    
    public Builder permitUntaggedSockets() {
      return disable(256);
    }
    
    public Builder detectImplicitDirectBoot() {
      return enable(1024);
    }
    
    public Builder permitImplicitDirectBoot() {
      return disable(1024);
    }
    
    public Builder detectCredentialProtectedWhileLocked() {
      return enable(2048);
    }
    
    public Builder permitCredentialProtectedWhileLocked() {
      return disable(2048);
    }
    
    public Builder detectIncorrectContextUse() {
      return enable(4096);
    }
    
    public Builder permitIncorrectContextUse() {
      return disable(4096);
    }
    
    public Builder penaltyDeath() {
      return enable(268435456);
    }
    
    public Builder penaltyDeathOnCleartextNetwork() {
      return enable(16777216);
    }
    
    public Builder penaltyDeathOnFileUriExposure() {
      return enable(8388608);
    }
    
    public Builder penaltyLog() {
      return enable(1073741824);
    }
    
    public Builder penaltyDropBox() {
      return enable(67108864);
    }
    
    public Builder penaltyListener(Executor param1Executor, StrictMode.OnVmViolationListener param1OnVmViolationListener) {
      if (param1Executor != null) {
        this.mListener = param1OnVmViolationListener;
        this.mExecutor = param1Executor;
        return this;
      } 
      throw new NullPointerException("executor must not be null");
    }
    
    public Builder penaltyListener(StrictMode.OnVmViolationListener param1OnVmViolationListener, Executor param1Executor) {
      return penaltyListener(param1Executor, param1OnVmViolationListener);
    }
    
    private Builder enable(int param1Int) {
      this.mMask |= param1Int;
      return this;
    }
    
    Builder disable(int param1Int) {
      this.mMask &= param1Int ^ 0xFFFFFFFF;
      return this;
    }
    
    public StrictMode.VmPolicy build() {
      if (this.mListener == null) {
        int j = this.mMask;
        if (j != 0 && (j & 0x74000000) == 0)
          penaltyLog(); 
      } 
      int i = this.mMask;
      HashMap<Class, Integer> hashMap = this.mClassInstanceLimit;
      if (hashMap == null)
        hashMap = StrictMode.EMPTY_CLASS_LIMIT_MAP; 
      return new StrictMode.VmPolicy(i, hashMap, this.mListener, this.mExecutor);
    }
  }
  
  public static void setThreadPolicy(ThreadPolicy paramThreadPolicy) {
    setThreadPolicyMask(paramThreadPolicy.mask);
    sThreadViolationListener.set(paramThreadPolicy.mListener);
    sThreadViolationExecutor.set(paramThreadPolicy.mCallbackExecutor);
  }
  
  public static void setThreadPolicyMask(int paramInt) {
    setBlockGuardPolicy(paramInt);
    Binder.setThreadStrictModePolicy(paramInt);
  }
  
  private static void setBlockGuardPolicy(int paramInt) {
    if (paramInt == 0) {
      BlockGuard.setThreadPolicy(BlockGuard.LAX_POLICY);
      return;
    } 
    BlockGuard.Policy policy = BlockGuard.getThreadPolicy();
    if (policy instanceof AndroidBlockGuardPolicy) {
      policy = policy;
    } else {
      policy = THREAD_ANDROID_POLICY.get();
      BlockGuard.setThreadPolicy(policy);
    } 
    policy.setThreadPolicyMask(paramInt);
  }
  
  private static void setBlockGuardVmPolicy(int paramInt) {
    if ((paramInt & 0x800) != 0) {
      BlockGuard.setVmPolicy(VM_ANDROID_POLICY);
    } else {
      BlockGuard.setVmPolicy(BlockGuard.LAX_VM_POLICY);
    } 
  }
  
  private static void setCloseGuardEnabled(boolean paramBoolean) {
    if (!(CloseGuard.getReporter() instanceof AndroidCloseGuardReporter))
      CloseGuard.setReporter(new AndroidCloseGuardReporter()); 
    CloseGuard.setEnabled(paramBoolean);
  }
  
  public static int getThreadPolicyMask() {
    BlockGuard.Policy policy = BlockGuard.getThreadPolicy();
    if (policy instanceof AndroidBlockGuardPolicy)
      return ((AndroidBlockGuardPolicy)policy).getThreadPolicyMask(); 
    return 0;
  }
  
  public static ThreadPolicy getThreadPolicy() {
    int i = getThreadPolicyMask();
    ThreadLocal<OnThreadViolationListener> threadLocal1 = sThreadViolationListener;
    OnThreadViolationListener onThreadViolationListener = threadLocal1.get();
    ThreadLocal<Executor> threadLocal = sThreadViolationExecutor;
    return new ThreadPolicy(i, onThreadViolationListener, threadLocal.get());
  }
  
  public static ThreadPolicy allowThreadDiskWrites() {
    int i = allowThreadDiskWritesMask();
    ThreadLocal<OnThreadViolationListener> threadLocal = sThreadViolationListener;
    OnThreadViolationListener onThreadViolationListener = threadLocal.get();
    ThreadLocal<Executor> threadLocal1 = sThreadViolationExecutor;
    return new ThreadPolicy(i, onThreadViolationListener, threadLocal1.get());
  }
  
  public static int allowThreadDiskWritesMask() {
    int i = getThreadPolicyMask();
    int j = i & 0xFFFFFFFC;
    if (j != i)
      setThreadPolicyMask(j); 
    return i;
  }
  
  public static ThreadPolicy allowThreadDiskReads() {
    int i = allowThreadDiskReadsMask();
    ThreadLocal<OnThreadViolationListener> threadLocal1 = sThreadViolationListener;
    OnThreadViolationListener onThreadViolationListener = threadLocal1.get();
    ThreadLocal<Executor> threadLocal = sThreadViolationExecutor;
    return new ThreadPolicy(i, onThreadViolationListener, threadLocal.get());
  }
  
  public static int allowThreadDiskReadsMask() {
    int i = getThreadPolicyMask();
    int j = i & 0xFFFFFFFD;
    if (j != i)
      setThreadPolicyMask(j); 
    return i;
  }
  
  public static ThreadPolicy allowThreadViolations() {
    ThreadPolicy threadPolicy = getThreadPolicy();
    setThreadPolicyMask(0);
    return threadPolicy;
  }
  
  public static VmPolicy allowVmViolations() {
    VmPolicy vmPolicy = getVmPolicy();
    sVmPolicy = VmPolicy.LAX;
    return vmPolicy;
  }
  
  public static boolean isBundledSystemApp(ApplicationInfo paramApplicationInfo) {
    if (paramApplicationInfo == null || paramApplicationInfo.packageName == null)
      return true; 
    if (paramApplicationInfo.isSystemApp())
      if (!paramApplicationInfo.packageName.equals("com.android.vending")) {
        String str = paramApplicationInfo.packageName;
        if (str.equals("com.android.chrome"))
          return false; 
        if (paramApplicationInfo.packageName.equals("com.android.phone"))
          return false; 
        if (!paramApplicationInfo.packageName.equals("android")) {
          str = paramApplicationInfo.packageName;
          if (!str.startsWith("android.")) {
            String str1 = paramApplicationInfo.packageName;
            if (str1.startsWith("com.android."))
              return true; 
          } else {
            return true;
          } 
        } else {
          return true;
        } 
      } else {
        return false;
      }  
    return false;
  }
  
  public static void initThreadDefaults(ApplicationInfo paramApplicationInfo) {
    char c;
    ThreadPolicy.Builder builder = new ThreadPolicy.Builder();
    if (paramApplicationInfo != null) {
      c = paramApplicationInfo.targetSdkVersion;
    } else {
      c = '✐';
    } 
    if (c >= '\013') {
      builder.detectNetwork();
      builder.penaltyDeathOnNetwork();
    } 
    if (!Build.IS_USER && !SystemProperties.getBoolean("persist.sys.strictmode.disable", false))
      if (Build.IS_USERDEBUG) {
        if (isBundledSystemApp(paramApplicationInfo)) {
          builder.detectAll();
          builder.penaltyDropBox();
          if (SystemProperties.getBoolean("persist.sys.strictmode.visual", false))
            builder.penaltyFlashScreen(); 
        } 
      } else if (Build.IS_ENG) {
        if (isBundledSystemApp(paramApplicationInfo)) {
          builder.detectAll();
          builder.penaltyDropBox();
          builder.penaltyLog();
          builder.penaltyFlashScreen();
        } 
      }  
    setThreadPolicy(builder.build());
  }
  
  public static void initVmDefaults(ApplicationInfo paramApplicationInfo) {
    char c;
    VmPolicy.Builder builder = new VmPolicy.Builder();
    if (paramApplicationInfo != null) {
      c = paramApplicationInfo.targetSdkVersion;
    } else {
      c = '✐';
    } 
    if (c >= '\030') {
      builder.detectFileUriExposure();
      builder.penaltyDeathOnFileUriExposure();
    } 
    if (!Build.IS_USER && !SystemProperties.getBoolean("persist.sys.strictmode.disable", false))
      if (Build.IS_USERDEBUG) {
        if (isBundledSystemApp(paramApplicationInfo)) {
          builder.detectAll();
          builder.permitActivityLeaks();
          builder.penaltyDropBox();
        } 
      } else if (Build.IS_ENG) {
        if (isBundledSystemApp(paramApplicationInfo)) {
          builder.detectAll();
          builder.penaltyDropBox();
          builder.penaltyLog();
        } 
      }  
    setVmPolicy(builder.build());
  }
  
  public static void enableDeathOnFileUriExposure() {
    sVmPolicy = new VmPolicy(0x800000 | sVmPolicy.mask | 0x20, sVmPolicy.classInstanceLimit, sVmPolicy.mListener, sVmPolicy.mCallbackExecutor);
  }
  
  public static void disableDeathOnFileUriExposure() {
    sVmPolicy = new VmPolicy(0xFF7FFFDF & sVmPolicy.mask, sVmPolicy.classInstanceLimit, sVmPolicy.mListener, sVmPolicy.mCallbackExecutor);
  }
  
  private static boolean tooManyViolationsThisLoop() {
    boolean bool;
    if (((ArrayList)violationsBeingTimed.get()).size() >= 10) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static class AndroidBlockGuardPolicy implements BlockGuard.Policy {
    private ArrayMap<Integer, Long> mLastViolationTime;
    
    private int mThreadPolicyMask;
    
    public AndroidBlockGuardPolicy(int param1Int) {
      this.mThreadPolicyMask = param1Int;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AndroidBlockGuardPolicy; mPolicyMask=");
      stringBuilder.append(this.mThreadPolicyMask);
      return stringBuilder.toString();
    }
    
    public int getPolicyMask() {
      return this.mThreadPolicyMask;
    }
    
    public void onWriteToDisk() {
      if ((this.mThreadPolicyMask & 0x1) == 0)
        return; 
      if (StrictMode.tooManyViolationsThisLoop())
        return; 
      startHandlingViolationException(new DiskWriteViolation());
    }
    
    void onCustomSlowCall(String param1String) {
      if ((this.mThreadPolicyMask & 0x8) == 0)
        return; 
      if (StrictMode.tooManyViolationsThisLoop())
        return; 
      startHandlingViolationException(new CustomViolation(param1String));
    }
    
    void onResourceMismatch(Object param1Object) {
      if ((this.mThreadPolicyMask & 0x10) == 0)
        return; 
      if (StrictMode.tooManyViolationsThisLoop())
        return; 
      startHandlingViolationException(new ResourceMismatchViolation(param1Object));
    }
    
    public void onUnbufferedIO() {
      if ((this.mThreadPolicyMask & 0x20) == 0)
        return; 
      if (StrictMode.tooManyViolationsThisLoop())
        return; 
      startHandlingViolationException(new UnbufferedIoViolation());
    }
    
    public void onReadFromDisk() {
      if ((this.mThreadPolicyMask & 0x2) == 0)
        return; 
      if (StrictMode.tooManyViolationsThisLoop())
        return; 
      startHandlingViolationException(new DiskReadViolation());
    }
    
    public void onNetwork() {
      int i = this.mThreadPolicyMask;
      if ((i & 0x4) == 0)
        return; 
      if ((i & 0x2000000) == 0) {
        if (StrictMode.tooManyViolationsThisLoop())
          return; 
        startHandlingViolationException(new NetworkViolation());
        return;
      } 
      throw new NetworkOnMainThreadException();
    }
    
    public void onExplicitGc() {
      if ((this.mThreadPolicyMask & 0x40) == 0)
        return; 
      if (StrictMode.tooManyViolationsThisLoop())
        return; 
      startHandlingViolationException(new ExplicitGcViolation());
    }
    
    public int getThreadPolicyMask() {
      return this.mThreadPolicyMask;
    }
    
    public void setThreadPolicyMask(int param1Int) {
      this.mThreadPolicyMask = param1Int;
    }
    
    void startHandlingViolationException(Violation param1Violation) {
      int i = this.mThreadPolicyMask;
      StrictMode.ViolationInfo violationInfo = new StrictMode.ViolationInfo(i & 0xFFFF0000);
      violationInfo.violationUptimeMillis = SystemClock.uptimeMillis();
      handleViolationWithTimingAttempt(violationInfo);
    }
    
    void handleViolationWithTimingAttempt(StrictMode.ViolationInfo param1ViolationInfo) {
      Looper looper = Looper.myLooper();
      if (looper == null || param1ViolationInfo.mPenaltyMask == 268435456) {
        param1ViolationInfo.durationMillis = -1;
        onThreadPolicyViolation(param1ViolationInfo);
        return;
      } 
      ArrayList<StrictMode.ViolationInfo> arrayList = StrictMode.violationsBeingTimed.get();
      if (arrayList.size() >= 10)
        return; 
      arrayList.add(param1ViolationInfo);
      if (arrayList.size() > 1)
        return; 
      if (param1ViolationInfo.penaltyEnabled(134217728)) {
        IWindowManager iWindowManager = (IWindowManager)StrictMode.sWindowManager.get();
      } else {
        param1ViolationInfo = null;
      } 
      if (param1ViolationInfo != null)
        try {
          param1ViolationInfo.showStrictModeViolation(true);
        } catch (RemoteException remoteException) {} 
      ThreadLocal<Handler> threadLocal = StrictMode.THREAD_HANDLER;
      Handler handler = threadLocal.get();
      _$$Lambda$StrictMode$AndroidBlockGuardPolicy$9nBulCQKaMajrWr41SB7f7YRT1I _$$Lambda$StrictMode$AndroidBlockGuardPolicy$9nBulCQKaMajrWr41SB7f7YRT1I = new _$$Lambda$StrictMode$AndroidBlockGuardPolicy$9nBulCQKaMajrWr41SB7f7YRT1I(this, (IWindowManager)param1ViolationInfo, arrayList);
      handler.postAtFrontOfQueue(_$$Lambda$StrictMode$AndroidBlockGuardPolicy$9nBulCQKaMajrWr41SB7f7YRT1I);
    }
    
    void onThreadPolicyViolation(StrictMode.ViolationInfo param1ViolationInfo) {
      if (StrictMode.LOG_V) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onThreadPolicyViolation; penalty=");
        stringBuilder.append(param1ViolationInfo.mPenaltyMask);
        Log.d("StrictMode", stringBuilder.toString());
      } 
      if (param1ViolationInfo.penaltyEnabled(-2147483648)) {
        ArrayList<StrictMode.ViolationInfo> arrayList2 = StrictMode.gatheredViolations.get();
        ArrayList<StrictMode.ViolationInfo> arrayList1 = arrayList2;
        if (arrayList2 == null) {
          arrayList1 = new ArrayList(1);
          StrictMode.gatheredViolations.set(arrayList1);
        } 
        for (StrictMode.ViolationInfo violationInfo : arrayList1) {
          if (param1ViolationInfo.getStackTrace().equals(violationInfo.getStackTrace()))
            return; 
        } 
        arrayList1.add(param1ViolationInfo);
        return;
      } 
      Integer integer = Integer.valueOf(param1ViolationInfo.hashCode());
      long l1 = 0L;
      long l2 = SystemClock.uptimeMillis();
      if (StrictMode.sLogger == StrictMode.LOGCAT_LOGGER) {
        ArrayMap<Integer, Long> arrayMap = this.mLastViolationTime;
        if (arrayMap != null) {
          Long long_ = (Long)arrayMap.get(integer);
          if (long_ != null)
            l1 = long_.longValue(); 
          ArrayMap<Integer, Long> arrayMap1 = this.mLastViolationTime;
          long l = Math.max(30000L, 3000L);
          StrictMode.clampViolationTimeMap((Map<Integer, Long>)arrayMap1, Math.max(1000L, l));
        } else {
          this.mLastViolationTime = new ArrayMap(1);
        } 
        this.mLastViolationTime.put(integer, Long.valueOf(l2));
      } 
      boolean bool = true;
      if (l1 == 0L) {
        l1 = Long.MAX_VALUE;
      } else {
        l1 = l2 - l1;
      } 
      if (param1ViolationInfo.penaltyEnabled(1073741824) && l1 > 1000L)
        StrictMode.sLogger.log(param1ViolationInfo); 
      Violation violation = param1ViolationInfo.mViolation;
      byte b = 0;
      int i = b;
      if (param1ViolationInfo.penaltyEnabled(536870912)) {
        i = b;
        if (l1 > 30000L)
          i = 0x0 | 0x20000000; 
      } 
      if (param1ViolationInfo.penaltyEnabled(67108864) && l1 > 3000L)
        i |= 0x4000000; 
      if (i != 0) {
        if (param1ViolationInfo.mPenaltyMask != 67108864)
          bool = false; 
        if (bool) {
          StrictMode.dropboxViolationAsync(i, param1ViolationInfo);
        } else {
          StrictMode.handleApplicationStrictModeViolation(i, param1ViolationInfo);
        } 
      } 
      if (!param1ViolationInfo.penaltyEnabled(268435456)) {
        StrictMode.OnThreadViolationListener onThreadViolationListener = StrictMode.sThreadViolationListener.get();
        Executor executor = StrictMode.sThreadViolationExecutor.get();
        if (onThreadViolationListener != null && executor != null)
          try {
            _$$Lambda$StrictMode$AndroidBlockGuardPolicy$FxZGA9KtfTewqdcxlUwvIe5Nx9I _$$Lambda$StrictMode$AndroidBlockGuardPolicy$FxZGA9KtfTewqdcxlUwvIe5Nx9I = new _$$Lambda$StrictMode$AndroidBlockGuardPolicy$FxZGA9KtfTewqdcxlUwvIe5Nx9I();
            this(onThreadViolationListener, violation);
            executor.execute(_$$Lambda$StrictMode$AndroidBlockGuardPolicy$FxZGA9KtfTewqdcxlUwvIe5Nx9I);
          } catch (RejectedExecutionException rejectedExecutionException) {
            Log.e("StrictMode", "ThreadPolicy penaltyCallback failed", rejectedExecutionException);
          }  
        return;
      } 
      throw new RuntimeException("StrictMode ThreadPolicy violation", violation);
    }
  }
  
  private static void dropboxViolationAsync(int paramInt, ViolationInfo paramViolationInfo) {
    int i = sDropboxCallsInFlight.incrementAndGet();
    if (i > 20) {
      sDropboxCallsInFlight.decrementAndGet();
      return;
    } 
    if (LOG_V) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Dropboxing async; in-flight=");
      stringBuilder.append(i);
      Log.d("StrictMode", stringBuilder.toString());
    } 
    BackgroundThread.getHandler().post(new _$$Lambda$StrictMode$yZJXPvy2veRNA_xL_SWdXzX_OLg(paramInt, paramViolationInfo));
  }
  
  private static void handleApplicationStrictModeViolation(int paramInt, ViolationInfo paramViolationInfo) {
    int i = getThreadPolicyMask();
    try {
      setThreadPolicyMask(0);
      IActivityManager iActivityManager = ActivityManager.getService();
      if (iActivityManager == null) {
        Log.w("StrictMode", "No activity manager; failed to Dropbox violation.");
      } else {
        IBinder iBinder = RuntimeInit.getApplicationObject();
        iActivityManager.handleApplicationStrictModeViolation(iBinder, paramInt, paramViolationInfo);
      } 
    } catch (RemoteException remoteException) {
      if (!(remoteException instanceof DeadObjectException))
        Log.e("StrictMode", "RemoteException handling StrictMode violation", (Throwable)remoteException); 
    } finally {}
    setThreadPolicyMask(i);
  }
  
  private static class AndroidCloseGuardReporter implements CloseGuard.Reporter {
    private AndroidCloseGuardReporter() {}
    
    public void report(String param1String, Throwable param1Throwable) {
      StrictMode.onVmPolicyViolation(new LeakedClosableViolation(param1String, param1Throwable));
    }
    
    public void report(String param1String) {
      StrictMode.onVmPolicyViolation(new LeakedClosableViolation(param1String));
    }
  }
  
  static boolean hasGatheredViolations() {
    boolean bool;
    if (gatheredViolations.get() != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  static void clearGatheredViolations() {
    gatheredViolations.set(null);
  }
  
  public static void conditionallyCheckInstanceCounts() {
    VmPolicy vmPolicy = getVmPolicy();
    int i = vmPolicy.classInstanceLimit.size();
    if (i == 0)
      return; 
    System.gc();
    System.runFinalization();
    System.gc();
    Class[] arrayOfClass = (Class[])vmPolicy.classInstanceLimit.keySet().toArray((Object[])new Class[i]);
    long[] arrayOfLong = VMDebug.countInstancesOfClasses(arrayOfClass, false);
    for (i = 0; i < arrayOfClass.length; i++) {
      Class clazz = arrayOfClass[i];
      int j = ((Integer)vmPolicy.classInstanceLimit.get(clazz)).intValue();
      long l = arrayOfLong[i];
      if (l > j)
        onVmPolicyViolation(new InstanceCountViolation(clazz, l, j)); 
    } 
  }
  
  public static void setVmPolicy(VmPolicy paramVmPolicy) {
    // Byte code:
    //   0: ldc android/os/StrictMode
    //   2: monitorenter
    //   3: aload_0
    //   4: putstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   7: invokestatic vmClosableObjectLeaksEnabled : ()Z
    //   10: invokestatic setCloseGuardEnabled : (Z)V
    //   13: invokestatic getMainLooper : ()Landroid/os/Looper;
    //   16: astore_1
    //   17: aload_1
    //   18: ifnull -> 82
    //   21: aload_1
    //   22: getfield mQueue : Landroid/os/MessageQueue;
    //   25: astore_1
    //   26: aload_0
    //   27: getfield classInstanceLimit : Ljava/util/HashMap;
    //   30: invokevirtual size : ()I
    //   33: ifeq -> 71
    //   36: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   39: getfield mask : I
    //   42: ldc -65536
    //   44: iand
    //   45: ifne -> 51
    //   48: goto -> 71
    //   51: getstatic android/os/StrictMode.sIsIdlerRegistered : Z
    //   54: ifne -> 82
    //   57: aload_1
    //   58: getstatic android/os/StrictMode.sProcessIdleHandler : Landroid/os/MessageQueue$IdleHandler;
    //   61: invokevirtual addIdleHandler : (Landroid/os/MessageQueue$IdleHandler;)V
    //   64: iconst_1
    //   65: putstatic android/os/StrictMode.sIsIdlerRegistered : Z
    //   68: goto -> 82
    //   71: aload_1
    //   72: getstatic android/os/StrictMode.sProcessIdleHandler : Landroid/os/MessageQueue$IdleHandler;
    //   75: invokevirtual removeIdleHandler : (Landroid/os/MessageQueue$IdleHandler;)V
    //   78: iconst_0
    //   79: putstatic android/os/StrictMode.sIsIdlerRegistered : Z
    //   82: iconst_0
    //   83: istore_2
    //   84: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   87: getfield mask : I
    //   90: bipush #64
    //   92: iand
    //   93: ifeq -> 130
    //   96: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   99: getfield mask : I
    //   102: ldc 268435456
    //   104: iand
    //   105: ifne -> 128
    //   108: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   111: getfield mask : I
    //   114: ldc 16777216
    //   116: iand
    //   117: ifeq -> 123
    //   120: goto -> 128
    //   123: iconst_1
    //   124: istore_2
    //   125: goto -> 130
    //   128: iconst_2
    //   129: istore_2
    //   130: ldc_w 'network_management'
    //   133: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   136: astore_0
    //   137: aload_0
    //   138: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/os/INetworkManagementService;
    //   141: astore_0
    //   142: aload_0
    //   143: ifnull -> 163
    //   146: aload_0
    //   147: invokestatic myUid : ()I
    //   150: iload_2
    //   151: invokeinterface setUidCleartextNetworkPolicy : (II)V
    //   156: goto -> 160
    //   159: astore_0
    //   160: goto -> 176
    //   163: iload_2
    //   164: ifeq -> 176
    //   167: ldc 'StrictMode'
    //   169: ldc_w 'Dropping requested network policy due to missing service!'
    //   172: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   175: pop
    //   176: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   179: getfield mask : I
    //   182: sipush #512
    //   185: iand
    //   186: ifeq -> 202
    //   189: getstatic android/os/StrictMode.sNonSdkApiUsageConsumer : Ljava/util/function/Consumer;
    //   192: invokestatic setNonSdkApiUsageConsumer : (Ljava/util/function/Consumer;)V
    //   195: iconst_0
    //   196: invokestatic setDedupeHiddenApiWarnings : (Z)V
    //   199: goto -> 210
    //   202: aconst_null
    //   203: invokestatic setNonSdkApiUsageConsumer : (Ljava/util/function/Consumer;)V
    //   206: iconst_1
    //   207: invokestatic setDedupeHiddenApiWarnings : (Z)V
    //   210: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   213: getfield mask : I
    //   216: invokestatic setBlockGuardVmPolicy : (I)V
    //   219: ldc android/os/StrictMode
    //   221: monitorexit
    //   222: return
    //   223: astore_0
    //   224: ldc android/os/StrictMode
    //   226: monitorexit
    //   227: aload_0
    //   228: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1995	-> 0
    //   #1996	-> 3
    //   #1997	-> 7
    //   #1999	-> 13
    //   #2000	-> 17
    //   #2001	-> 21
    //   #2002	-> 26
    //   #2006	-> 51
    //   #2007	-> 57
    //   #2008	-> 64
    //   #2004	-> 71
    //   #2005	-> 78
    //   #2012	-> 82
    //   #2013	-> 84
    //   #2014	-> 96
    //   #2018	-> 123
    //   #2016	-> 128
    //   #2022	-> 130
    //   #2024	-> 130
    //   #2023	-> 137
    //   #2025	-> 142
    //   #2027	-> 146
    //   #2028	-> 159
    //   #2029	-> 160
    //   #2030	-> 163
    //   #2031	-> 167
    //   #2035	-> 176
    //   #2036	-> 189
    //   #2037	-> 195
    //   #2039	-> 202
    //   #2040	-> 206
    //   #2043	-> 210
    //   #2044	-> 219
    //   #2045	-> 222
    //   #2044	-> 223
    // Exception table:
    //   from	to	target	type
    //   3	7	223	finally
    //   7	13	223	finally
    //   13	17	223	finally
    //   21	26	223	finally
    //   26	48	223	finally
    //   51	57	223	finally
    //   57	64	223	finally
    //   64	68	223	finally
    //   71	78	223	finally
    //   78	82	223	finally
    //   84	96	223	finally
    //   96	120	223	finally
    //   130	137	223	finally
    //   137	142	223	finally
    //   146	156	159	android/os/RemoteException
    //   146	156	223	finally
    //   167	176	223	finally
    //   176	189	223	finally
    //   189	195	223	finally
    //   195	199	223	finally
    //   202	206	223	finally
    //   206	210	223	finally
    //   210	219	223	finally
    //   219	222	223	finally
    //   224	227	223	finally
  }
  
  public static VmPolicy getVmPolicy() {
    // Byte code:
    //   0: ldc android/os/StrictMode
    //   2: monitorenter
    //   3: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   6: astore_0
    //   7: ldc android/os/StrictMode
    //   9: monitorexit
    //   10: aload_0
    //   11: areturn
    //   12: astore_0
    //   13: ldc android/os/StrictMode
    //   15: monitorexit
    //   16: aload_0
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2049	-> 0
    //   #2050	-> 3
    //   #2051	-> 12
    // Exception table:
    //   from	to	target	type
    //   3	10	12	finally
    //   13	16	12	finally
  }
  
  public static void enableDefaults() {
    setThreadPolicy((new ThreadPolicy.Builder()).detectAll().penaltyLog().build());
    setVmPolicy((new VmPolicy.Builder()).detectAll().penaltyLog().build());
  }
  
  public static boolean vmSqliteObjectLeaksEnabled() {
    int i = sVmPolicy.mask;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public static boolean vmClosableObjectLeaksEnabled() {
    boolean bool;
    if ((sVmPolicy.mask & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean vmRegistrationLeaksEnabled() {
    boolean bool;
    if ((sVmPolicy.mask & 0x10) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean vmFileUriExposureEnabled() {
    boolean bool;
    if ((sVmPolicy.mask & 0x20) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean vmCleartextNetworkEnabled() {
    boolean bool;
    if ((sVmPolicy.mask & 0x40) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean vmContentUriWithoutPermissionEnabled() {
    boolean bool;
    if ((sVmPolicy.mask & 0x80) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean vmUntaggedSocketEnabled() {
    boolean bool;
    if ((sVmPolicy.mask & 0x100) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean vmImplicitDirectBootEnabled() {
    boolean bool;
    if ((sVmPolicy.mask & 0x400) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean vmCredentialProtectedWhileLockedEnabled() {
    boolean bool;
    if ((sVmPolicy.mask & 0x800) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean vmIncorrectContextUseEnabled() {
    boolean bool;
    if ((sVmPolicy.mask & 0x1000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static void onSqliteObjectLeaked(String paramString, Throwable paramThrowable) {
    onVmPolicyViolation(new SqliteObjectLeakedViolation(paramString, paramThrowable));
  }
  
  public static void onWebViewMethodCalledOnWrongThread(Throwable paramThrowable) {
    onVmPolicyViolation(new WebViewMethodCalledOnWrongThreadViolation(paramThrowable));
  }
  
  public static void onIntentReceiverLeaked(Throwable paramThrowable) {
    onVmPolicyViolation(new IntentReceiverLeakedViolation(paramThrowable));
  }
  
  public static void onServiceConnectionLeaked(Throwable paramThrowable) {
    onVmPolicyViolation(new ServiceConnectionLeakedViolation(paramThrowable));
  }
  
  public static void onFileUriExposed(Uri paramUri, String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramUri);
    stringBuilder.append(" exposed beyond app through ");
    stringBuilder.append(paramString);
    String str = stringBuilder.toString();
    if ((sVmPolicy.mask & 0x800000) == 0) {
      onVmPolicyViolation(new FileUriExposedViolation(str));
      return;
    } 
    throw new FileUriExposedException(str);
  }
  
  public static void onContentUriWithoutPermission(Uri paramUri, String paramString) {
    onVmPolicyViolation(new ContentUriWithoutPermissionViolation(paramUri, paramString));
  }
  
  public static void onCleartextNetworkDetected(byte[] paramArrayOfbyte) {
    byte[] arrayOfByte1 = null;
    boolean bool = false;
    byte[] arrayOfByte2 = arrayOfByte1;
    if (paramArrayOfbyte != null)
      if (paramArrayOfbyte.length >= 20 && (paramArrayOfbyte[0] & 0xF0) == 64) {
        arrayOfByte2 = new byte[4];
        System.arraycopy(paramArrayOfbyte, 16, arrayOfByte2, 0, 4);
      } else {
        arrayOfByte2 = arrayOfByte1;
        if (paramArrayOfbyte.length >= 40) {
          arrayOfByte2 = arrayOfByte1;
          if ((paramArrayOfbyte[0] & 0xF0) == 96) {
            arrayOfByte2 = new byte[16];
            System.arraycopy(paramArrayOfbyte, 24, arrayOfByte2, 0, 16);
          } 
        } 
      }  
    int i = Process.myUid();
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Detected cleartext network traffic from UID ");
    stringBuilder1.append(i);
    String str3 = stringBuilder1.toString();
    String str2 = str3;
    if (arrayOfByte2 != null)
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(str3);
        stringBuilder.append(" to ");
        stringBuilder.append(InetAddress.getByAddress(arrayOfByte2));
        String str = stringBuilder.toString();
      } catch (UnknownHostException unknownHostException) {
        str2 = str3;
      }  
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(str2);
    stringBuilder2.append(HexDump.dumpHexString(paramArrayOfbyte).trim());
    stringBuilder2.append(" ");
    String str1 = stringBuilder2.toString();
    if ((sVmPolicy.mask & 0x1000000) != 0)
      bool = true; 
    onVmPolicyViolation(new CleartextNetworkViolation(str1), bool);
  }
  
  public static void onUntaggedSocket() {
    onVmPolicyViolation(new UntaggedSocketViolation());
  }
  
  public static void onImplicitDirectBoot() {
    onVmPolicyViolation(new ImplicitDirectBootViolation());
  }
  
  public static void onIncorrectContextUsed(String paramString, Throwable paramThrowable) {
    onVmPolicyViolation(new IncorrectContextUseViolation(paramString, paramThrowable));
  }
  
  private static boolean isUserKeyUnlocked(int paramInt) {
    IStorageManager iStorageManager = IStorageManager.Stub.asInterface(ServiceManager.getService("mount"));
    if (iStorageManager != null)
      try {
        return iStorageManager.isUserKeyUnlocked(paramInt);
      } catch (RemoteException remoteException) {} 
    return false;
  }
  
  private static void onCredentialProtectedPathAccess(String paramString, int paramInt) {
    if (paramInt == UserHandle.myUserId()) {
      if (sUserKeyUnlocked)
        return; 
      if (isUserKeyUnlocked(paramInt)) {
        sUserKeyUnlocked = true;
        return;
      } 
    } else if (isUserKeyUnlocked(paramInt)) {
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Accessed credential protected path ");
    stringBuilder.append(paramString);
    stringBuilder.append(" while user ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" was locked");
    onVmPolicyViolation(new CredentialProtectedWhileLockedViolation(stringBuilder.toString()));
  }
  
  private static void clampViolationTimeMap(Map<Integer, Long> paramMap, long paramLong) {
    Iterator<Map.Entry> iterator = paramMap.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry entry = iterator.next();
      if (((Long)entry.getValue()).longValue() < paramLong)
        iterator.remove(); 
    } 
  }
  
  public static void onVmPolicyViolation(Violation paramViolation) {
    onVmPolicyViolation(paramViolation, false);
  }
  
  public static void onVmPolicyViolation(Violation paramViolation, boolean paramBoolean) {
    // Byte code:
    //   0: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   3: getfield mask : I
    //   6: istore_2
    //   7: iconst_1
    //   8: istore_3
    //   9: iload_2
    //   10: ldc 67108864
    //   12: iand
    //   13: ifeq -> 21
    //   16: iconst_1
    //   17: istore_2
    //   18: goto -> 23
    //   21: iconst_0
    //   22: istore_2
    //   23: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   26: getfield mask : I
    //   29: ldc 268435456
    //   31: iand
    //   32: ifne -> 48
    //   35: iload_1
    //   36: ifeq -> 42
    //   39: goto -> 48
    //   42: iconst_0
    //   43: istore #4
    //   45: goto -> 51
    //   48: iconst_1
    //   49: istore #4
    //   51: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   54: getfield mask : I
    //   57: ldc 1073741824
    //   59: iand
    //   60: ifeq -> 66
    //   63: goto -> 68
    //   66: iconst_0
    //   67: istore_3
    //   68: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   71: getfield mask : I
    //   74: istore #5
    //   76: new android/os/StrictMode$ViolationInfo
    //   79: dup
    //   80: aload_0
    //   81: ldc -65536
    //   83: iload #5
    //   85: iand
    //   86: invokespecial <init> : (Landroid/os/strictmode/Violation;I)V
    //   89: astore #6
    //   91: aload #6
    //   93: iconst_0
    //   94: putfield numAnimationsRunning : I
    //   97: aload #6
    //   99: aconst_null
    //   100: putfield tags : [Ljava/lang/String;
    //   103: aload #6
    //   105: aconst_null
    //   106: putfield broadcastIntentAction : Ljava/lang/String;
    //   109: aload #6
    //   111: invokevirtual hashCode : ()I
    //   114: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   117: astore #7
    //   119: invokestatic uptimeMillis : ()J
    //   122: lstore #8
    //   124: ldc2_w 9223372036854775807
    //   127: lstore #10
    //   129: lload #10
    //   131: lstore #12
    //   133: getstatic android/os/StrictMode.sLogger : Landroid/os/StrictMode$ViolationLogger;
    //   136: getstatic android/os/StrictMode.LOGCAT_LOGGER : Landroid/os/StrictMode$ViolationLogger;
    //   139: if_acmpne -> 249
    //   142: getstatic android/os/StrictMode.sLastVmViolationTime : Ljava/util/HashMap;
    //   145: astore #14
    //   147: aload #14
    //   149: monitorenter
    //   150: lload #10
    //   152: lstore #12
    //   154: getstatic android/os/StrictMode.sLastVmViolationTime : Ljava/util/HashMap;
    //   157: aload #7
    //   159: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   162: ifeq -> 188
    //   165: getstatic android/os/StrictMode.sLastVmViolationTime : Ljava/util/HashMap;
    //   168: aload #7
    //   170: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   173: checkcast java/lang/Long
    //   176: invokevirtual longValue : ()J
    //   179: lstore #12
    //   181: lload #8
    //   183: lload #12
    //   185: lsub
    //   186: lstore #12
    //   188: lload #12
    //   190: ldc2_w 1000
    //   193: lcmp
    //   194: ifle -> 211
    //   197: getstatic android/os/StrictMode.sLastVmViolationTime : Ljava/util/HashMap;
    //   200: aload #7
    //   202: lload #8
    //   204: invokestatic valueOf : (J)Ljava/lang/Long;
    //   207: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   210: pop
    //   211: getstatic android/os/StrictMode.sLastVmViolationTime : Ljava/util/HashMap;
    //   214: astore #7
    //   216: ldc2_w 1000
    //   219: ldc2_w 1000
    //   222: invokestatic max : (JJ)J
    //   225: lstore #10
    //   227: aload #7
    //   229: lload #8
    //   231: lload #10
    //   233: lsub
    //   234: invokestatic clampViolationTimeMap : (Ljava/util/Map;J)V
    //   237: aload #14
    //   239: monitorexit
    //   240: goto -> 249
    //   243: astore_0
    //   244: aload #14
    //   246: monitorexit
    //   247: aload_0
    //   248: athrow
    //   249: lload #12
    //   251: ldc2_w 1000
    //   254: lcmp
    //   255: ifgt -> 259
    //   258: return
    //   259: iload_3
    //   260: ifeq -> 288
    //   263: getstatic android/os/StrictMode.sLogger : Landroid/os/StrictMode$ViolationLogger;
    //   266: ifnull -> 288
    //   269: lload #12
    //   271: ldc2_w 1000
    //   274: lcmp
    //   275: ifle -> 288
    //   278: getstatic android/os/StrictMode.sLogger : Landroid/os/StrictMode$ViolationLogger;
    //   281: aload #6
    //   283: invokeinterface log : (Landroid/os/StrictMode$ViolationInfo;)V
    //   288: iload_2
    //   289: ifeq -> 314
    //   292: iload #4
    //   294: ifeq -> 307
    //   297: ldc 67108864
    //   299: aload #6
    //   301: invokestatic handleApplicationStrictModeViolation : (ILandroid/os/StrictMode$ViolationInfo;)V
    //   304: goto -> 314
    //   307: ldc 67108864
    //   309: aload #6
    //   311: invokestatic dropboxViolationAsync : (ILandroid/os/StrictMode$ViolationInfo;)V
    //   314: iload #4
    //   316: ifeq -> 339
    //   319: getstatic java/lang/System.err : Ljava/io/PrintStream;
    //   322: ldc_w 'StrictMode VmPolicy violation with POLICY_DEATH; shutting down.'
    //   325: invokevirtual println : (Ljava/lang/String;)V
    //   328: invokestatic myPid : ()I
    //   331: invokestatic killProcess : (I)V
    //   334: bipush #10
    //   336: invokestatic exit : (I)V
    //   339: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   342: getfield mListener : Landroid/os/StrictMode$OnVmViolationListener;
    //   345: ifnull -> 409
    //   348: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   351: getfield mCallbackExecutor : Ljava/util/concurrent/Executor;
    //   354: ifnull -> 409
    //   357: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   360: getfield mListener : Landroid/os/StrictMode$OnVmViolationListener;
    //   363: astore #7
    //   365: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   368: getfield mCallbackExecutor : Ljava/util/concurrent/Executor;
    //   371: astore #14
    //   373: new android/os/_$$Lambda$StrictMode$UFC_nI1x6u8ZwMQmA7bmj9NHZz4
    //   376: astore #6
    //   378: aload #6
    //   380: aload #7
    //   382: aload_0
    //   383: invokespecial <init> : (Landroid/os/StrictMode$OnVmViolationListener;Landroid/os/strictmode/Violation;)V
    //   386: aload #14
    //   388: aload #6
    //   390: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   395: goto -> 409
    //   398: astore_0
    //   399: ldc 'StrictMode'
    //   401: ldc_w 'VmPolicy penaltyCallback failed'
    //   404: aload_0
    //   405: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   408: pop
    //   409: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2259	-> 0
    //   #2260	-> 23
    //   #2261	-> 51
    //   #2263	-> 68
    //   #2264	-> 76
    //   #2267	-> 91
    //   #2268	-> 97
    //   #2269	-> 103
    //   #2271	-> 109
    //   #2272	-> 119
    //   #2274	-> 124
    //   #2275	-> 129
    //   #2276	-> 142
    //   #2277	-> 150
    //   #2278	-> 165
    //   #2279	-> 181
    //   #2281	-> 188
    //   #2282	-> 197
    //   #2284	-> 211
    //   #2285	-> 216
    //   #2284	-> 227
    //   #2286	-> 237
    //   #2288	-> 249
    //   #2290	-> 258
    //   #2293	-> 259
    //   #2294	-> 278
    //   #2297	-> 288
    //   #2298	-> 292
    //   #2299	-> 297
    //   #2304	-> 307
    //   #2308	-> 314
    //   #2309	-> 319
    //   #2310	-> 328
    //   #2311	-> 334
    //   #2316	-> 339
    //   #2317	-> 357
    //   #2319	-> 365
    //   #2331	-> 395
    //   #2329	-> 398
    //   #2330	-> 399
    //   #2333	-> 409
    // Exception table:
    //   from	to	target	type
    //   154	165	243	finally
    //   165	181	243	finally
    //   197	211	243	finally
    //   211	216	243	finally
    //   216	227	243	finally
    //   227	237	243	finally
    //   237	240	243	finally
    //   244	247	243	finally
    //   365	395	398	java/util/concurrent/RejectedExecutionException
  }
  
  static void writeGatheredViolationsToParcel(Parcel paramParcel) {
    ArrayList<ViolationInfo> arrayList = gatheredViolations.get();
    if (arrayList == null) {
      paramParcel.writeInt(0);
    } else {
      int i = Math.min(arrayList.size(), 3);
      paramParcel.writeInt(i);
      for (byte b = 0; b < i; b++)
        ((ViolationInfo)arrayList.get(b)).writeToParcel(paramParcel, 0); 
    } 
    gatheredViolations.set(null);
  }
  
  static void readAndHandleBinderCallViolations(Parcel paramParcel) {
    Throwable throwable = new Throwable();
    int i = getThreadPolicyMask();
    if ((Integer.MIN_VALUE & i) != 0) {
      i = 1;
    } else {
      i = 0;
    } 
    int j = paramParcel.readInt();
    for (byte b = 0; b < j; b++) {
      boolean bool;
      if (i == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      ViolationInfo violationInfo = new ViolationInfo(bool);
      violationInfo.addLocalStack(throwable);
      BlockGuard.Policy policy = BlockGuard.getThreadPolicy();
      if (policy instanceof AndroidBlockGuardPolicy)
        ((AndroidBlockGuardPolicy)policy).handleViolationWithTimingAttempt(violationInfo); 
    } 
  }
  
  private static void onBinderStrictModePolicyChange(int paramInt) {
    setBlockGuardPolicy(paramInt);
  }
  
  public static class Span {
    private final StrictMode.ThreadSpanState mContainerState;
    
    private long mCreateMillis;
    
    private String mName;
    
    private Span mNext;
    
    private Span mPrev;
    
    Span(StrictMode.ThreadSpanState param1ThreadSpanState) {
      this.mContainerState = param1ThreadSpanState;
    }
    
    protected Span() {
      this.mContainerState = null;
    }
    
    public void finish() {
      synchronized (this.mContainerState) {
        if (this.mName == null)
          return; 
        if (this.mPrev != null)
          this.mPrev.mNext = this.mNext; 
        if (this.mNext != null)
          this.mNext.mPrev = this.mPrev; 
        if (null.mActiveHead == this)
          null.mActiveHead = this.mNext; 
        null.mActiveSize--;
        if (StrictMode.LOG_V) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Span finished=");
          stringBuilder.append(this.mName);
          stringBuilder.append("; size=");
          stringBuilder.append(null.mActiveSize);
          Log.d("StrictMode", stringBuilder.toString());
        } 
        this.mCreateMillis = -1L;
        this.mName = null;
        this.mPrev = null;
        this.mNext = null;
        if (null.mFreeListSize < 5) {
          this.mNext = null.mFreeListHead;
          null.mFreeListHead = this;
          null.mFreeListSize++;
        } 
        return;
      } 
    }
  }
  
  private static class ThreadSpanState {
    public StrictMode.Span mActiveHead;
    
    public int mActiveSize;
    
    public StrictMode.Span mFreeListHead;
    
    public int mFreeListSize;
    
    private ThreadSpanState() {}
  }
  
  public static Span enterCriticalSpan(String paramString) {
    if (Build.IS_USER)
      return NO_OP_SPAN; 
    if (paramString != null && !paramString.isEmpty())
      synchronized ((ThreadSpanState)sThisThreadSpanState.get()) {
        Span span;
        if (null.mFreeListHead != null) {
          span = null.mFreeListHead;
          null.mFreeListHead = span.mNext;
          null.mFreeListSize--;
        } else {
          span = new Span(null);
        } 
        Span.access$2402(span, paramString);
        Span.access$2502(span, SystemClock.uptimeMillis());
        Span.access$2302(span, null.mActiveHead);
        Span.access$2602(span, null);
        null.mActiveHead = span;
        null.mActiveSize++;
        if (span.mNext != null)
          Span.access$2602(span.mNext, span); 
        if (LOG_V) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Span enter=");
          stringBuilder.append(paramString);
          stringBuilder.append("; size=");
          stringBuilder.append(null.mActiveSize);
          Log.d("StrictMode", stringBuilder.toString());
        } 
        return span;
      }  
    throw new IllegalArgumentException("name must be non-null and non-empty");
  }
  
  public static void noteSlowCall(String paramString) {
    BlockGuard.Policy policy = BlockGuard.getThreadPolicy();
    if (!(policy instanceof AndroidBlockGuardPolicy))
      return; 
    ((AndroidBlockGuardPolicy)policy).onCustomSlowCall(paramString);
  }
  
  public static void noteResourceMismatch(Object paramObject) {
    BlockGuard.Policy policy = BlockGuard.getThreadPolicy();
    if (!(policy instanceof AndroidBlockGuardPolicy))
      return; 
    ((AndroidBlockGuardPolicy)policy).onResourceMismatch(paramObject);
  }
  
  public static void noteUnbufferedIO() {
    BlockGuard.Policy policy = BlockGuard.getThreadPolicy();
    if (!(policy instanceof AndroidBlockGuardPolicy))
      return; 
    policy.onUnbufferedIO();
  }
  
  public static void noteDiskRead() {
    BlockGuard.Policy policy = BlockGuard.getThreadPolicy();
    if (!(policy instanceof AndroidBlockGuardPolicy))
      return; 
    policy.onReadFromDisk();
  }
  
  public static void noteDiskWrite() {
    BlockGuard.Policy policy = BlockGuard.getThreadPolicy();
    if (!(policy instanceof AndroidBlockGuardPolicy))
      return; 
    policy.onWriteToDisk();
  }
  
  public static Object trackActivity(Object paramObject) {
    return new InstanceTracker(paramObject);
  }
  
  public static void incrementExpectedActivityCount(Class<?> paramClass) {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull -> 5
    //   4: return
    //   5: ldc android/os/StrictMode
    //   7: monitorenter
    //   8: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   11: getfield mask : I
    //   14: iconst_4
    //   15: iand
    //   16: ifne -> 23
    //   19: ldc android/os/StrictMode
    //   21: monitorexit
    //   22: return
    //   23: getstatic android/os/StrictMode.sExpectedActivityInstanceCount : Ljava/util/HashMap;
    //   26: aload_0
    //   27: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   30: checkcast java/lang/Integer
    //   33: astore_1
    //   34: aload_1
    //   35: ifnonnull -> 48
    //   38: aload_0
    //   39: invokestatic getInstanceCount : (Ljava/lang/Class;)I
    //   42: iconst_1
    //   43: iadd
    //   44: istore_2
    //   45: goto -> 55
    //   48: aload_1
    //   49: invokevirtual intValue : ()I
    //   52: iconst_1
    //   53: iadd
    //   54: istore_2
    //   55: getstatic android/os/StrictMode.sExpectedActivityInstanceCount : Ljava/util/HashMap;
    //   58: aload_0
    //   59: iload_2
    //   60: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   63: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   66: pop
    //   67: ldc android/os/StrictMode
    //   69: monitorexit
    //   70: return
    //   71: astore_0
    //   72: ldc android/os/StrictMode
    //   74: monitorexit
    //   75: aload_0
    //   76: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2621	-> 0
    //   #2622	-> 4
    //   #2625	-> 5
    //   #2626	-> 8
    //   #2627	-> 19
    //   #2631	-> 23
    //   #2633	-> 34
    //   #2634	-> 55
    //   #2635	-> 67
    //   #2636	-> 70
    //   #2635	-> 71
    // Exception table:
    //   from	to	target	type
    //   8	19	71	finally
    //   19	22	71	finally
    //   23	34	71	finally
    //   38	45	71	finally
    //   48	55	71	finally
    //   55	67	71	finally
    //   67	70	71	finally
    //   72	75	71	finally
  }
  
  public static void decrementExpectedActivityCount(Class<?> paramClass) {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull -> 5
    //   4: return
    //   5: ldc android/os/StrictMode
    //   7: monitorenter
    //   8: getstatic android/os/StrictMode.sVmPolicy : Landroid/os/StrictMode$VmPolicy;
    //   11: getfield mask : I
    //   14: iconst_4
    //   15: iand
    //   16: ifne -> 23
    //   19: ldc android/os/StrictMode
    //   21: monitorexit
    //   22: return
    //   23: getstatic android/os/StrictMode.sExpectedActivityInstanceCount : Ljava/util/HashMap;
    //   26: aload_0
    //   27: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   30: checkcast java/lang/Integer
    //   33: astore_1
    //   34: aload_1
    //   35: ifnull -> 58
    //   38: aload_1
    //   39: invokevirtual intValue : ()I
    //   42: ifne -> 48
    //   45: goto -> 58
    //   48: aload_1
    //   49: invokevirtual intValue : ()I
    //   52: iconst_1
    //   53: isub
    //   54: istore_2
    //   55: goto -> 60
    //   58: iconst_0
    //   59: istore_2
    //   60: iload_2
    //   61: ifne -> 75
    //   64: getstatic android/os/StrictMode.sExpectedActivityInstanceCount : Ljava/util/HashMap;
    //   67: aload_0
    //   68: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   71: pop
    //   72: goto -> 87
    //   75: getstatic android/os/StrictMode.sExpectedActivityInstanceCount : Ljava/util/HashMap;
    //   78: aload_0
    //   79: iload_2
    //   80: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   83: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   86: pop
    //   87: iinc #2, 1
    //   90: ldc android/os/StrictMode
    //   92: monitorexit
    //   93: aload_0
    //   94: invokestatic getInstanceCount : (Ljava/lang/Class;)I
    //   97: istore_3
    //   98: iload_3
    //   99: iload_2
    //   100: if_icmpgt -> 104
    //   103: return
    //   104: invokestatic gc : ()V
    //   107: invokestatic runFinalization : ()V
    //   110: invokestatic gc : ()V
    //   113: aload_0
    //   114: iconst_0
    //   115: invokestatic countInstancesOfClass : (Ljava/lang/Class;Z)J
    //   118: lstore #4
    //   120: lload #4
    //   122: iload_2
    //   123: i2l
    //   124: lcmp
    //   125: ifle -> 142
    //   128: new android/os/strictmode/InstanceCountViolation
    //   131: dup
    //   132: aload_0
    //   133: lload #4
    //   135: iload_2
    //   136: invokespecial <init> : (Ljava/lang/Class;JI)V
    //   139: invokestatic onVmPolicyViolation : (Landroid/os/strictmode/Violation;)V
    //   142: return
    //   143: astore_0
    //   144: ldc android/os/StrictMode
    //   146: monitorexit
    //   147: aload_0
    //   148: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2640	-> 0
    //   #2641	-> 4
    //   #2645	-> 5
    //   #2646	-> 8
    //   #2647	-> 19
    //   #2650	-> 23
    //   #2651	-> 34
    //   #2652	-> 60
    //   #2653	-> 64
    //   #2655	-> 75
    //   #2660	-> 87
    //   #2661	-> 90
    //   #2664	-> 93
    //   #2665	-> 98
    //   #2666	-> 103
    //   #2677	-> 104
    //   #2678	-> 107
    //   #2679	-> 110
    //   #2681	-> 113
    //   #2682	-> 120
    //   #2683	-> 128
    //   #2685	-> 142
    //   #2661	-> 143
    // Exception table:
    //   from	to	target	type
    //   8	19	143	finally
    //   19	22	143	finally
    //   23	34	143	finally
    //   38	45	143	finally
    //   48	55	143	finally
    //   64	72	143	finally
    //   75	87	143	finally
    //   90	93	143	finally
    //   144	147	143	finally
  }
  
  class ViolationInfo implements Parcelable {
    private final Deque<StackTraceElement[]> mBinderStack = (Deque)new ArrayDeque<>();
    
    public int durationMillis = -1;
    
    public int numAnimationsRunning = 0;
    
    public long numInstances = -1L;
    
    ViolationInfo(int param1Int) {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial <init> : ()V
      //   4: aload_0
      //   5: new java/util/ArrayDeque
      //   8: dup
      //   9: invokespecial <init> : ()V
      //   12: putfield mBinderStack : Ljava/util/Deque;
      //   15: aload_0
      //   16: iconst_m1
      //   17: putfield durationMillis : I
      //   20: aload_0
      //   21: iconst_0
      //   22: putfield numAnimationsRunning : I
      //   25: aload_0
      //   26: ldc2_w -1
      //   29: putfield numInstances : J
      //   32: aload_0
      //   33: aload_1
      //   34: putfield mViolation : Landroid/os/strictmode/Violation;
      //   37: aload_0
      //   38: iload_2
      //   39: putfield mPenaltyMask : I
      //   42: aload_0
      //   43: invokestatic uptimeMillis : ()J
      //   46: putfield violationUptimeMillis : J
      //   49: aload_0
      //   50: invokestatic getCurrentAnimationsCount : ()I
      //   53: putfield numAnimationsRunning : I
      //   56: invokestatic getIntentBeingBroadcast : ()Landroid/content/Intent;
      //   59: astore_3
      //   60: aload_3
      //   61: ifnull -> 72
      //   64: aload_0
      //   65: aload_3
      //   66: invokevirtual getAction : ()Ljava/lang/String;
      //   69: putfield broadcastIntentAction : Ljava/lang/String;
      //   72: invokestatic access$2700 : ()Ljava/lang/ThreadLocal;
      //   75: invokevirtual get : ()Ljava/lang/Object;
      //   78: checkcast android/os/StrictMode$ThreadSpanState
      //   81: astore_3
      //   82: aload_1
      //   83: instanceof android/os/strictmode/InstanceCountViolation
      //   86: ifeq -> 100
      //   89: aload_0
      //   90: aload_1
      //   91: checkcast android/os/strictmode/InstanceCountViolation
      //   94: invokevirtual getNumberOfInstances : ()J
      //   97: putfield numInstances : J
      //   100: aload_3
      //   101: monitorenter
      //   102: aload_3
      //   103: getfield mActiveSize : I
      //   106: istore #4
      //   108: iload #4
      //   110: istore_2
      //   111: iload #4
      //   113: bipush #20
      //   115: if_icmple -> 121
      //   118: bipush #20
      //   120: istore_2
      //   121: iload_2
      //   122: ifeq -> 173
      //   125: aload_0
      //   126: iload_2
      //   127: anewarray java/lang/String
      //   130: putfield tags : [Ljava/lang/String;
      //   133: aload_3
      //   134: getfield mActiveHead : Landroid/os/StrictMode$Span;
      //   137: astore_1
      //   138: iconst_0
      //   139: istore #4
      //   141: aload_1
      //   142: ifnull -> 173
      //   145: iload #4
      //   147: iload_2
      //   148: if_icmpge -> 173
      //   151: aload_0
      //   152: getfield tags : [Ljava/lang/String;
      //   155: iload #4
      //   157: aload_1
      //   158: invokestatic access$2400 : (Landroid/os/StrictMode$Span;)Ljava/lang/String;
      //   161: aastore
      //   162: iinc #4, 1
      //   165: aload_1
      //   166: invokestatic access$2300 : (Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;
      //   169: astore_1
      //   170: goto -> 141
      //   173: aload_3
      //   174: monitorexit
      //   175: return
      //   176: astore_1
      //   177: aload_3
      //   178: monitorexit
      //   179: aload_1
      //   180: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2736	-> 0
      //   #2699	-> 4
      //   #2708	-> 15
      //   #2711	-> 20
      //   #2733	-> 25
      //   #2737	-> 32
      //   #2738	-> 37
      //   #2739	-> 42
      //   #2740	-> 49
      //   #2741	-> 56
      //   #2742	-> 60
      //   #2743	-> 64
      //   #2745	-> 72
      //   #2746	-> 82
      //   #2747	-> 89
      //   #2749	-> 100
      //   #2750	-> 102
      //   #2751	-> 108
      //   #2752	-> 118
      //   #2754	-> 121
      //   #2755	-> 125
      //   #2756	-> 133
      //   #2757	-> 138
      //   #2758	-> 141
      //   #2759	-> 151
      //   #2760	-> 162
      //   #2761	-> 165
      //   #2764	-> 173
      //   #2765	-> 175
      //   #2764	-> 176
      // Exception table:
      //   from	to	target	type
      //   102	108	176	finally
      //   125	133	176	finally
      //   133	138	176	finally
      //   151	162	176	finally
      //   165	170	176	finally
      //   173	175	176	finally
      //   177	179	176	finally
    }
    
    public String getStackTrace() {
      if (this.mStackTrace == null) {
        StringWriter stringWriter = new StringWriter();
        FastPrintWriter fastPrintWriter = new FastPrintWriter(stringWriter, false, 256);
        this.mViolation.printStackTrace((PrintWriter)fastPrintWriter);
        for (StackTraceElement[] arrayOfStackTraceElement : this.mBinderStack) {
          fastPrintWriter.append("# via Binder call with stack:\n");
          int i;
          byte b;
          for (i = arrayOfStackTraceElement.length, b = 0; b < i; ) {
            StackTraceElement stackTraceElement = arrayOfStackTraceElement[b];
            fastPrintWriter.append("\tat ");
            fastPrintWriter.append(stackTraceElement.toString());
            fastPrintWriter.append('\n');
            b++;
          } 
        } 
        fastPrintWriter.flush();
        fastPrintWriter.close();
        this.mStackTrace = stringWriter.toString();
      } 
      return this.mStackTrace;
    }
    
    public Class<? extends Violation> getViolationClass() {
      return (Class)this.mViolation.getClass();
    }
    
    public String getViolationDetails() {
      return this.mViolation.getMessage();
    }
    
    boolean penaltyEnabled(int param1Int) {
      boolean bool;
      if ((this.mPenaltyMask & param1Int) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    void addLocalStack(Throwable param1Throwable) {
      this.mBinderStack.addFirst(param1Throwable.getStackTrace());
    }
    
    public int hashCode() {
      int i = 17;
      Violation violation = this.mViolation;
      if (violation != null)
        i = 17 * 37 + violation.hashCode(); 
      int j = i;
      if (this.numAnimationsRunning != 0)
        j = i * 37; 
      String str = this.broadcastIntentAction;
      i = j;
      if (str != null)
        i = j * 37 + str.hashCode(); 
      String[] arrayOfString = this.tags;
      int k = i;
      if (arrayOfString != null) {
        int m = arrayOfString.length;
        j = 0;
        while (true) {
          k = i;
          if (j < m) {
            str = arrayOfString[j];
            i = i * 37 + str.hashCode();
            j++;
            continue;
          } 
          break;
        } 
      } 
      return k;
    }
    
    public ViolationInfo() {
      this((Parcel)this$0, false);
    }
    
    public ViolationInfo(boolean param1Boolean) {
      this.mViolation = (Violation)this$0.readSerializable();
      int i = this$0.readInt();
      int j;
      for (j = 0; j < i; j++) {
        StackTraceElement[] arrayOfStackTraceElement = new StackTraceElement[this$0.readInt()];
        for (byte b = 0; b < arrayOfStackTraceElement.length; b++) {
          String str1 = this$0.readString();
          String str2 = this$0.readString();
          String str3 = this$0.readString();
          StackTraceElement stackTraceElement = new StackTraceElement(str1, str2, str3, this$0.readInt());
          arrayOfStackTraceElement[b] = stackTraceElement;
        } 
        this.mBinderStack.add(arrayOfStackTraceElement);
      } 
      j = this$0.readInt();
      if (param1Boolean) {
        this.mPenaltyMask = Integer.MAX_VALUE & j;
      } else {
        this.mPenaltyMask = j;
      } 
      this.durationMillis = this$0.readInt();
      this.violationNumThisLoop = this$0.readInt();
      this.numAnimationsRunning = this$0.readInt();
      this.violationUptimeMillis = this$0.readLong();
      this.numInstances = this$0.readLong();
      this.broadcastIntentAction = this$0.readString();
      this.tags = this$0.readStringArray();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeSerializable(this.mViolation);
      param1Parcel.writeInt(this.mBinderStack.size());
      for (StackTraceElement[] arrayOfStackTraceElement : this.mBinderStack) {
        param1Parcel.writeInt(arrayOfStackTraceElement.length);
        for (int i = arrayOfStackTraceElement.length; param1Int < i; ) {
          StackTraceElement stackTraceElement = arrayOfStackTraceElement[param1Int];
          param1Parcel.writeString(stackTraceElement.getClassName());
          param1Parcel.writeString(stackTraceElement.getMethodName());
          param1Parcel.writeString(stackTraceElement.getFileName());
          param1Parcel.writeInt(stackTraceElement.getLineNumber());
          param1Int++;
        } 
      } 
      param1Parcel.dataPosition();
      param1Parcel.writeInt(this.mPenaltyMask);
      param1Parcel.writeInt(this.durationMillis);
      param1Parcel.writeInt(this.violationNumThisLoop);
      param1Parcel.writeInt(this.numAnimationsRunning);
      param1Parcel.writeLong(this.violationUptimeMillis);
      param1Parcel.writeLong(this.numInstances);
      param1Parcel.writeString(this.broadcastIntentAction);
      param1Parcel.writeStringArray(this.tags);
      param1Parcel.dataPosition();
    }
    
    public void dump(Printer param1Printer, String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1String);
      stringBuilder.append("stackTrace: ");
      stringBuilder.append(getStackTrace());
      param1Printer.println(stringBuilder.toString());
      stringBuilder = new StringBuilder();
      stringBuilder.append(param1String);
      stringBuilder.append("penalty: ");
      stringBuilder.append(this.mPenaltyMask);
      param1Printer.println(stringBuilder.toString());
      if (this.durationMillis != -1) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(param1String);
        stringBuilder.append("durationMillis: ");
        stringBuilder.append(this.durationMillis);
        param1Printer.println(stringBuilder.toString());
      } 
      if (this.numInstances != -1L) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(param1String);
        stringBuilder.append("numInstances: ");
        stringBuilder.append(this.numInstances);
        param1Printer.println(stringBuilder.toString());
      } 
      if (this.violationNumThisLoop != 0) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(param1String);
        stringBuilder.append("violationNumThisLoop: ");
        stringBuilder.append(this.violationNumThisLoop);
        param1Printer.println(stringBuilder.toString());
      } 
      if (this.numAnimationsRunning != 0) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(param1String);
        stringBuilder.append("numAnimationsRunning: ");
        stringBuilder.append(this.numAnimationsRunning);
        param1Printer.println(stringBuilder.toString());
      } 
      stringBuilder = new StringBuilder();
      stringBuilder.append(param1String);
      stringBuilder.append("violationUptimeMillis: ");
      stringBuilder.append(this.violationUptimeMillis);
      param1Printer.println(stringBuilder.toString());
      if (this.broadcastIntentAction != null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(param1String);
        stringBuilder.append("broadcastIntentAction: ");
        stringBuilder.append(this.broadcastIntentAction);
        param1Printer.println(stringBuilder.toString());
      } 
      String[] arrayOfString = this.tags;
      if (arrayOfString != null) {
        byte b1 = 0;
        int i;
        byte b2;
        for (i = arrayOfString.length, b2 = 0; b2 < i; ) {
          String str = arrayOfString[b2];
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(param1String);
          stringBuilder1.append("tag[");
          stringBuilder1.append(b1);
          stringBuilder1.append("]: ");
          stringBuilder1.append(str);
          param1Printer.println(stringBuilder1.toString());
          b2++;
          b1++;
        } 
      } 
    }
    
    public int describeContents() {
      return 0;
    }
    
    public static final Parcelable.Creator<ViolationInfo> CREATOR = new Parcelable.Creator<ViolationInfo>() {
        public StrictMode.ViolationInfo createFromParcel(Parcel param2Parcel) {
          return new StrictMode.ViolationInfo();
        }
        
        public StrictMode.ViolationInfo[] newArray(int param2Int) {
          return new StrictMode.ViolationInfo[param2Int];
        }
      };
    
    public String broadcastIntentAction;
    
    private final int mPenaltyMask;
    
    private String mStackTrace;
    
    private final Violation mViolation;
    
    public String[] tags;
    
    public int violationNumThisLoop;
    
    public long violationUptimeMillis;
  }
  
  private static final class InstanceTracker {
    private static final HashMap<Class<?>, Integer> sInstanceCounts = new HashMap<>();
    
    private final Class<?> mKlass;
    
    public InstanceTracker(Object<Class<?>, Integer> param1Object) {
      this.mKlass = param1Object.getClass();
      synchronized (sInstanceCounts) {
        Integer integer = sInstanceCounts.get(this.mKlass);
        int i = 1;
        if (integer != null)
          i = 1 + integer.intValue(); 
        sInstanceCounts.put(this.mKlass, Integer.valueOf(i));
        return;
      } 
    }
    
    protected void finalize() throws Throwable {
      try {
      
      } finally {
        super.finalize();
      } 
    }
    
    public static int getInstanceCount(Class<?> param1Class) {
      synchronized (sInstanceCounts) {
        boolean bool;
        Integer integer = sInstanceCounts.get(param1Class);
        if (integer != null) {
          bool = integer.intValue();
        } else {
          bool = false;
        } 
        return bool;
      } 
    }
  }
  
  public static interface OnThreadViolationListener {
    void onThreadViolation(Violation param1Violation);
  }
  
  public static interface OnVmViolationListener {
    void onVmViolation(Violation param1Violation);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ThreadPolicyMask {}
  
  public static interface ViolationLogger {
    void log(StrictMode.ViolationInfo param1ViolationInfo);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface VmPolicyMask {}
}
