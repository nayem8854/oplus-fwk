package android.os;

import android.annotation.SystemApi;
import android.content.Context;
import com.android.internal.app.IBatteryStats;

public class BatteryManager {
  public static final String ACTION_CHARGING = "android.os.action.CHARGING";
  
  public static final String ACTION_DISCHARGING = "android.os.action.DISCHARGING";
  
  public static final int BATTERY_HEALTH_COLD = 7;
  
  public static final int BATTERY_HEALTH_DEAD = 4;
  
  public static final int BATTERY_HEALTH_GOOD = 2;
  
  public static final int BATTERY_HEALTH_OVERHEAT = 3;
  
  public static final int BATTERY_HEALTH_OVER_VOLTAGE = 5;
  
  public static final int BATTERY_HEALTH_UNKNOWN = 1;
  
  public static final int BATTERY_HEALTH_UNSPECIFIED_FAILURE = 6;
  
  public static final int BATTERY_PLUGGED_AC = 1;
  
  public static final int BATTERY_PLUGGED_ANY = 7;
  
  public static final int BATTERY_PLUGGED_USB = 2;
  
  public static final int BATTERY_PLUGGED_WIRELESS = 4;
  
  public static final int BATTERY_PROPERTY_CAPACITY = 4;
  
  public static final int BATTERY_PROPERTY_CHARGE_COUNTER = 1;
  
  public static final int BATTERY_PROPERTY_CURRENT_AVERAGE = 3;
  
  public static final int BATTERY_PROPERTY_CURRENT_NOW = 2;
  
  public static final int BATTERY_PROPERTY_ENERGY_COUNTER = 5;
  
  public static final int BATTERY_PROPERTY_STATUS = 6;
  
  public static final int BATTERY_STATUS_CHARGING = 2;
  
  public static final int BATTERY_STATUS_DISCHARGING = 3;
  
  public static final int BATTERY_STATUS_FULL = 5;
  
  public static final int BATTERY_STATUS_NOT_CHARGING = 4;
  
  public static final int BATTERY_STATUS_UNKNOWN = 1;
  
  public static final String EXTRA_BATTERY_CHG_BALANCE_TYPE = "battery_charge_balance_type";
  
  public static final String EXTRA_BATTERY_LOW = "battery_low";
  
  public static final String EXTRA_BATTERY_MIN_VOLTAGE_TYPE = "battery_min_voltage_type";
  
  public static final String EXTRA_BATTERY_NOW_VOLTAGE_TYPE = "battery_now_voltage_type";
  
  public static final String EXTRA_CHARGE_COUNTER = "charge_counter";
  
  @SystemApi
  public static final String EXTRA_EVENTS = "android.os.extra.EVENTS";
  
  @SystemApi
  public static final String EXTRA_EVENT_TIMESTAMP = "android.os.extra.EVENT_TIMESTAMP";
  
  public static final String EXTRA_HEALTH = "health";
  
  public static final String EXTRA_ICON_SMALL = "icon-small";
  
  public static final String EXTRA_INVALID_CHARGER = "invalid_charger";
  
  public static final String EXTRA_LEVEL = "level";
  
  public static final String EXTRA_MAX_CHARGING_CURRENT = "max_charging_current";
  
  public static final String EXTRA_MAX_CHARGING_VOLTAGE = "max_charging_voltage";
  
  public static final String EXTRA_PLUGGED = "plugged";
  
  public static final String EXTRA_PRESENT = "present";
  
  public static final String EXTRA_SCALE = "scale";
  
  public static final String EXTRA_SEQUENCE = "seq";
  
  public static final String EXTRA_STATUS = "status";
  
  public static final String EXTRA_TECHNOLOGY = "technology";
  
  public static final String EXTRA_TEMPERATURE = "temperature";
  
  public static final String EXTRA_VOLTAGE = "voltage";
  
  public static final String EXTRA_WIRELESS_DEVIATED_CHG_TYPE = "wireless_deviated_chg_type";
  
  public static final String EXTRA_WIRELESS_REVERSE_CHG_TYPE = "wireless_reverse_chg_type";
  
  private final IBatteryPropertiesRegistrar mBatteryPropertiesRegistrar;
  
  private final IBatteryStats mBatteryStats;
  
  private final Context mContext;
  
  public BatteryManager() {
    this.mContext = null;
    IBinder iBinder = ServiceManager.getService("batterystats");
    this.mBatteryStats = IBatteryStats.Stub.asInterface(iBinder);
    iBinder = ServiceManager.getService("batteryproperties");
    this.mBatteryPropertiesRegistrar = IBatteryPropertiesRegistrar.Stub.asInterface(iBinder);
  }
  
  public BatteryManager(Context paramContext, IBatteryStats paramIBatteryStats, IBatteryPropertiesRegistrar paramIBatteryPropertiesRegistrar) {
    this.mContext = paramContext;
    this.mBatteryStats = paramIBatteryStats;
    this.mBatteryPropertiesRegistrar = paramIBatteryPropertiesRegistrar;
  }
  
  public boolean isCharging() {
    try {
      return this.mBatteryStats.isCharging();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private long queryProperty(int paramInt) {
    if (this.mBatteryPropertiesRegistrar == null)
      return Long.MIN_VALUE; 
    try {
      long l;
      BatteryProperty batteryProperty = new BatteryProperty();
      this();
      if (this.mBatteryPropertiesRegistrar.getProperty(paramInt, batteryProperty) == 0) {
        l = batteryProperty.getLong();
      } else {
        l = Long.MIN_VALUE;
      } 
      return l;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getIntProperty(int paramInt) {
    long l = queryProperty(paramInt);
    if (l == Long.MIN_VALUE) {
      Context context = this.mContext;
      if (context != null && 
        (context.getApplicationInfo()).targetSdkVersion >= 28)
        return Integer.MIN_VALUE; 
    } 
    return (int)l;
  }
  
  public long getLongProperty(int paramInt) {
    return queryProperty(paramInt);
  }
  
  public static boolean isPlugWired(int paramInt) {
    boolean bool1 = true, bool2 = bool1;
    if (paramInt != 2)
      if (paramInt == 1) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    return bool2;
  }
  
  public long computeChargeTimeRemaining() {
    try {
      return this.mBatteryStats.computeChargeTimeRemaining();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean setChargingStateUpdateDelayMillis(int paramInt) {
    try {
      return this.mBatteryStats.setChargingStateUpdateDelayMillis(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getPsyOtgOnline() {
    IBatteryPropertiesRegistrar iBatteryPropertiesRegistrar = this.mBatteryPropertiesRegistrar;
    if (iBatteryPropertiesRegistrar == null)
      return -1; 
    try {
      return iBatteryPropertiesRegistrar.getPsyOtgOnline();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getPsyBatteryHmac() {
    IBatteryPropertiesRegistrar iBatteryPropertiesRegistrar = this.mBatteryPropertiesRegistrar;
    if (iBatteryPropertiesRegistrar == null)
      return -1; 
    try {
      return iBatteryPropertiesRegistrar.getPsyBatteryHmac();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getPsyFastChgType() {
    IBatteryPropertiesRegistrar iBatteryPropertiesRegistrar = this.mBatteryPropertiesRegistrar;
    if (iBatteryPropertiesRegistrar == null)
      return -1; 
    try {
      return iBatteryPropertiesRegistrar.getPsyFastChgType();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public float getBeginDecimal() {
    IBatteryPropertiesRegistrar iBatteryPropertiesRegistrar = this.mBatteryPropertiesRegistrar;
    if (iBatteryPropertiesRegistrar == null)
      return 0.0F; 
    try {
      return iBatteryPropertiesRegistrar.getBeginDecimal();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public float getEndDecimal() {
    IBatteryPropertiesRegistrar iBatteryPropertiesRegistrar = this.mBatteryPropertiesRegistrar;
    if (iBatteryPropertiesRegistrar == null)
      return 0.0F; 
    try {
      return iBatteryPropertiesRegistrar.getEndDecimal();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
