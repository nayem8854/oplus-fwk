package android.os;

public class HandlerThread extends Thread {
  private Handler mHandler;
  
  Looper mLooper;
  
  int mPriority;
  
  int mTid = -1;
  
  public HandlerThread(String paramString) {
    super(paramString);
    this.mPriority = 0;
  }
  
  public HandlerThread(String paramString, int paramInt) {
    super(paramString);
    this.mPriority = paramInt;
  }
  
  protected void onLooperPrepared() {}
  
  public void run() {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic myTid : ()I
    //   4: putfield mTid : I
    //   7: invokestatic prepare : ()V
    //   10: aload_0
    //   11: monitorenter
    //   12: aload_0
    //   13: invokestatic myLooper : ()Landroid/os/Looper;
    //   16: putfield mLooper : Landroid/os/Looper;
    //   19: aload_0
    //   20: invokevirtual notifyAll : ()V
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_0
    //   26: getfield mPriority : I
    //   29: invokestatic setThreadPriority : (I)V
    //   32: aload_0
    //   33: invokevirtual onLooperPrepared : ()V
    //   36: invokestatic loop : ()V
    //   39: aload_0
    //   40: iconst_m1
    //   41: putfield mTid : I
    //   44: return
    //   45: astore_1
    //   46: aload_0
    //   47: monitorexit
    //   48: aload_1
    //   49: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #59	-> 0
    //   #60	-> 7
    //   #61	-> 10
    //   #62	-> 12
    //   #63	-> 19
    //   #64	-> 23
    //   #65	-> 25
    //   #66	-> 32
    //   #67	-> 36
    //   #68	-> 39
    //   #69	-> 44
    //   #64	-> 45
    // Exception table:
    //   from	to	target	type
    //   12	19	45	finally
    //   19	23	45	finally
    //   23	25	45	finally
    //   46	48	45	finally
  }
  
  public Looper getLooper() {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual isAlive : ()Z
    //   4: ifne -> 9
    //   7: aconst_null
    //   8: areturn
    //   9: aload_0
    //   10: monitorenter
    //   11: aload_0
    //   12: invokevirtual isAlive : ()Z
    //   15: ifeq -> 38
    //   18: aload_0
    //   19: getfield mLooper : Landroid/os/Looper;
    //   22: astore_1
    //   23: aload_1
    //   24: ifnonnull -> 38
    //   27: aload_0
    //   28: invokevirtual wait : ()V
    //   31: goto -> 11
    //   34: astore_1
    //   35: goto -> 31
    //   38: aload_0
    //   39: monitorexit
    //   40: aload_0
    //   41: getfield mLooper : Landroid/os/Looper;
    //   44: areturn
    //   45: astore_1
    //   46: aload_0
    //   47: monitorexit
    //   48: aload_1
    //   49: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #78	-> 0
    //   #79	-> 7
    //   #83	-> 9
    //   #84	-> 11
    //   #86	-> 27
    //   #88	-> 31
    //   #87	-> 34
    //   #90	-> 38
    //   #91	-> 40
    //   #90	-> 45
    // Exception table:
    //   from	to	target	type
    //   11	23	45	finally
    //   27	31	34	java/lang/InterruptedException
    //   27	31	45	finally
    //   38	40	45	finally
    //   46	48	45	finally
  }
  
  public Handler getThreadHandler() {
    if (this.mHandler == null)
      this.mHandler = new Handler(getLooper()); 
    return this.mHandler;
  }
  
  public boolean quit() {
    Looper looper = getLooper();
    if (looper != null) {
      looper.quit();
      return true;
    } 
    return false;
  }
  
  public boolean quitSafely() {
    Looper looper = getLooper();
    if (looper != null) {
      looper.quitSafely();
      return true;
    } 
    return false;
  }
  
  public int getThreadId() {
    return this.mTid;
  }
}
