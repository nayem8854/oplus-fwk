package android.os;

public class AsyncResult {
  public Throwable exception;
  
  public Object result;
  
  public Object userObj;
  
  public static AsyncResult forMessage(Message paramMessage, Object paramObject, Throwable paramThrowable) {
    paramObject = new AsyncResult(paramMessage.obj, paramObject, paramThrowable);
    paramMessage.obj = paramObject;
    return (AsyncResult)paramObject;
  }
  
  public static AsyncResult forMessage(Message paramMessage) {
    AsyncResult asyncResult = new AsyncResult(paramMessage.obj, null, null);
    paramMessage.obj = asyncResult;
    return asyncResult;
  }
  
  public AsyncResult(Object paramObject1, Object paramObject2, Throwable paramThrowable) {
    this.userObj = paramObject1;
    this.result = paramObject2;
    this.exception = paramThrowable;
  }
}
