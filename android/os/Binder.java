package android.os;

import android.annotation.SystemApi;
import android.app.AppOpsManager;
import android.util.ExceptionUtils;
import android.util.Log;
import com.android.internal.os.BinderInternal;
import com.android.internal.util.FastPrintWriter;
import com.android.internal.util.FunctionalUtils;
import dalvik.annotation.optimization.CriticalNative;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.function.Supplier;
import libcore.io.IoUtils;
import libcore.util.NativeAllocationRegistry;

public class Binder implements IBinder {
  public static final boolean CHECK_PARCEL_SIZE = false;
  
  private static final boolean FIND_POTENTIAL_LEAKS = false;
  
  public static boolean LOG_RUNTIME_EXCEPTION = false;
  
  private static final int NATIVE_ALLOCATION_SIZE = 3000;
  
  static final String TAG = "Binder";
  
  public static final int UNSET_WORKSOURCE = -1;
  
  private static volatile String sDumpDisabled = null;
  
  private static BinderInternal.Observer sObserver;
  
  private static volatile boolean sTracingEnabled;
  
  private static volatile TransactionTracker sTransactionTracker = null;
  
  static volatile boolean sWarnOnBlocking;
  
  static ThreadLocal<Boolean> sWarnOnBlockingOnCurrentThread;
  
  private static volatile BinderInternal.WorkSourceProvider sWorkSourceProvider;
  
  private String mDescriptor;
  
  private final long mObject;
  
  private IInterface mOwner;
  
  static {
    sObserver = null;
    sTracingEnabled = false;
    sWarnOnBlocking = false;
    -$.Lambda.Binder.aNRcHb8WfLrWjcSlV42Wu5psFwU aNRcHb8WfLrWjcSlV42Wu5psFwU = _$$Lambda$Binder$aNRcHb8WfLrWjcSlV42Wu5psFwU.INSTANCE;
    sWarnOnBlockingOnCurrentThread = ThreadLocal.withInitial((Supplier<? extends Boolean>)aNRcHb8WfLrWjcSlV42Wu5psFwU);
    sWorkSourceProvider = (BinderInternal.WorkSourceProvider)_$$Lambda$Binder$sHSgT14Q7D_inZx204V4_ect_uA.INSTANCE;
  }
  
  class NoImagePreloadHolder {
    public static final NativeAllocationRegistry sRegistry = new NativeAllocationRegistry(Binder.class.getClassLoader(), Binder.getNativeFinalizer(), 3000L);
  }
  
  public static void enableTracing() {
    sTracingEnabled = true;
  }
  
  public static void disableTracing() {
    sTracingEnabled = false;
  }
  
  public static boolean isTracingEnabled() {
    return sTracingEnabled;
  }
  
  public static TransactionTracker getTransactionTracker() {
    // Byte code:
    //   0: ldc android/os/Binder
    //   2: monitorenter
    //   3: getstatic android/os/Binder.sTransactionTracker : Landroid/os/TransactionTracker;
    //   6: ifnonnull -> 21
    //   9: new android/os/TransactionTracker
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic android/os/Binder.sTransactionTracker : Landroid/os/TransactionTracker;
    //   21: getstatic android/os/Binder.sTransactionTracker : Landroid/os/TransactionTracker;
    //   24: astore_0
    //   25: ldc android/os/Binder
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc android/os/Binder
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #178	-> 3
    //   #179	-> 9
    //   #180	-> 21
    //   #177	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	25	30	finally
  }
  
  public static void setObserver(BinderInternal.Observer paramObserver) {
    sObserver = paramObserver;
  }
  
  public static void setWarnOnBlocking(boolean paramBoolean) {
    sWarnOnBlocking = paramBoolean;
  }
  
  public static IBinder allowBlocking(IBinder paramIBinder) {
    try {
      if (paramIBinder instanceof BinderProxy) {
        ((BinderProxy)paramIBinder).mWarnOnBlocking = false;
      } else if (paramIBinder != null && paramIBinder.getInterfaceDescriptor() != null && paramIBinder.queryLocalInterface(paramIBinder.getInterfaceDescriptor()) == null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unable to allow blocking on interface ");
        stringBuilder.append(paramIBinder);
        Log.w("Binder", stringBuilder.toString());
      } 
    } catch (RemoteException remoteException) {}
    return paramIBinder;
  }
  
  public static IBinder defaultBlocking(IBinder paramIBinder) {
    if (paramIBinder instanceof BinderProxy)
      ((BinderProxy)paramIBinder).mWarnOnBlocking = sWarnOnBlocking; 
    return paramIBinder;
  }
  
  public static void copyAllowBlocking(IBinder paramIBinder1, IBinder paramIBinder2) {
    if (paramIBinder1 instanceof BinderProxy && paramIBinder2 instanceof BinderProxy)
      ((BinderProxy)paramIBinder2).mWarnOnBlocking = ((BinderProxy)paramIBinder1).mWarnOnBlocking; 
  }
  
  public static void allowBlockingForCurrentThread() {
    sWarnOnBlockingOnCurrentThread.set(Boolean.valueOf(false));
  }
  
  public static void defaultBlockingForCurrentThread() {
    sWarnOnBlockingOnCurrentThread.set(Boolean.valueOf(sWarnOnBlocking));
  }
  
  public static final int getCallingUidOrThrow() {
    if (isHandlingTransaction())
      return getCallingUid(); 
    throw new IllegalStateException("Thread is not in a binder transcation");
  }
  
  public static final UserHandle getCallingUserHandle() {
    return UserHandle.of(UserHandle.getUserId(getCallingUid()));
  }
  
  public static final void withCleanCallingIdentity(FunctionalUtils.ThrowingRunnable paramThrowingRunnable) {
    long l = clearCallingIdentity();
    try {
      paramThrowingRunnable.runOrThrow();
      restoreCallingIdentity(l);
      if (!false)
        return; 
      throw ExceptionUtils.propagate(null);
    } finally {
      paramThrowingRunnable = null;
      restoreCallingIdentity(l);
    } 
  }
  
  public static final <T> T withCleanCallingIdentity(FunctionalUtils.ThrowingSupplier<T> paramThrowingSupplier) {
    long l = clearCallingIdentity();
    try {
      Object object = paramThrowingSupplier.getOrThrow();
      restoreCallingIdentity(l);
      if (!false)
        return (T)object; 
      throw ExceptionUtils.propagate(null);
    } finally {
      paramThrowingSupplier = null;
      restoreCallingIdentity(l);
    } 
  }
  
  public static final void joinThreadPool() {
    BinderInternal.joinThreadPool();
  }
  
  public static final boolean isProxy(IInterface paramIInterface) {
    boolean bool;
    if (paramIInterface.asBinder() != paramIInterface) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Binder() {
    this(null);
  }
  
  public Binder(String paramString) {
    this.mObject = getNativeBBinderHolder();
    NoImagePreloadHolder.sRegistry.registerNativeAllocation(this, this.mObject);
    this.mDescriptor = paramString;
  }
  
  public void attachInterface(IInterface paramIInterface, String paramString) {
    this.mOwner = paramIInterface;
    this.mDescriptor = paramString;
  }
  
  public String getInterfaceDescriptor() {
    return this.mDescriptor;
  }
  
  public boolean pingBinder() {
    return true;
  }
  
  public boolean isBinderAlive() {
    return true;
  }
  
  public IInterface queryLocalInterface(String paramString) {
    String str = this.mDescriptor;
    if (str != null && str.equals(paramString))
      return this.mOwner; 
    return null;
  }
  
  public static void setDumpDisabled(String paramString) {
    sDumpDisabled = paramString;
  }
  
  @SystemApi
  class ProxyTransactListener {
    public abstract void onTransactEnded(Object param1Object);
    
    public abstract Object onTransactStarted(IBinder param1IBinder, int param1Int);
    
    public Object onTransactStarted(IBinder param1IBinder, int param1Int1, int param1Int2) {
      return onTransactStarted(param1IBinder, param1Int1);
    }
  }
  
  public static class PropagateWorkSourceTransactListener implements ProxyTransactListener {
    public Object onTransactStarted(IBinder param1IBinder, int param1Int) {
      param1Int = ThreadLocalWorkSource.getUid();
      if (param1Int != -1)
        return Long.valueOf(Binder.setCallingWorkSourceUid(param1Int)); 
      return null;
    }
    
    public void onTransactEnded(Object param1Object) {
      if (param1Object != null) {
        long l = ((Long)param1Object).longValue();
        Binder.restoreCallingWorkSource(l);
      } 
    }
  }
  
  @SystemApi
  public static void setProxyTransactListener(ProxyTransactListener paramProxyTransactListener) {
    BinderProxy.setTransactListener(paramProxyTransactListener);
  }
  
  protected boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2) throws RemoteException {
    if (paramInt1 == 1598968902) {
      paramParcel2.writeString(getInterfaceDescriptor());
      return true;
    } 
    if (paramInt1 == 1598311760) {
      ParcelFileDescriptor parcelFileDescriptor = paramParcel1.readFileDescriptor();
      String[] arrayOfString = paramParcel1.readStringArray();
      if (parcelFileDescriptor != null) {
        try {
          FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
          try {
            dump(fileDescriptor, arrayOfString);
            IoUtils.closeQuietly(parcelFileDescriptor);
          } finally {}
        } finally {}
        IoUtils.closeQuietly(parcelFileDescriptor);
        throw paramParcel1;
      } 
    } else {
      if (paramInt1 == 1598246212) {
        ParcelFileDescriptor parcelFileDescriptor2 = paramParcel1.readFileDescriptor();
        ParcelFileDescriptor parcelFileDescriptor1 = paramParcel1.readFileDescriptor();
        ParcelFileDescriptor parcelFileDescriptor3 = paramParcel1.readFileDescriptor();
        String[] arrayOfString = paramParcel1.readStringArray();
        ShellCallback shellCallback = ShellCallback.CREATOR.createFromParcel(paramParcel1);
        ResultReceiver resultReceiver = ResultReceiver.CREATOR.createFromParcel(paramParcel1);
        if (parcelFileDescriptor1 != null) {
          FileDescriptor fileDescriptor1;
          if (parcelFileDescriptor2 != null) {
            try {
              FileDescriptor fileDescriptor = parcelFileDescriptor2.getFileDescriptor();
            } finally {}
          } else {
            paramParcel1 = null;
          } 
          FileDescriptor fileDescriptor2 = parcelFileDescriptor1.getFileDescriptor();
          if (parcelFileDescriptor3 != null) {
            fileDescriptor1 = parcelFileDescriptor3.getFileDescriptor();
          } else {
            fileDescriptor1 = parcelFileDescriptor1.getFileDescriptor();
          } 
          shellCommand((FileDescriptor)paramParcel1, fileDescriptor2, fileDescriptor1, arrayOfString, shellCallback, resultReceiver);
        } 
        IoUtils.closeQuietly(parcelFileDescriptor2);
        IoUtils.closeQuietly(parcelFileDescriptor1);
        IoUtils.closeQuietly(parcelFileDescriptor3);
        if (paramParcel2 != null) {
          paramParcel2.writeNoException();
        } else {
          StrictMode.clearGatheredViolations();
        } 
        return true;
      } 
      return false;
    } 
    if (paramParcel2 != null) {
      paramParcel2.writeNoException();
    } else {
      StrictMode.clearGatheredViolations();
    } 
    return true;
  }
  
  public String getTransactionName(int paramInt) {
    return null;
  }
  
  public void dump(FileDescriptor paramFileDescriptor, String[] paramArrayOfString) {
    FileOutputStream fileOutputStream = new FileOutputStream(paramFileDescriptor);
    FastPrintWriter fastPrintWriter = new FastPrintWriter(fileOutputStream);
    try {
      doDump(paramFileDescriptor, (PrintWriter)fastPrintWriter, paramArrayOfString);
      return;
    } finally {
      fastPrintWriter.flush();
    } 
  }
  
  void doDump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    String str = sDumpDisabled;
    if (str == null) {
      try {
        dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
      } catch (SecurityException securityException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Security exception: ");
        stringBuilder.append(securityException.getMessage());
        paramPrintWriter.println(stringBuilder.toString());
        throw securityException;
      } finally {
        paramFileDescriptor = null;
        paramPrintWriter.println();
        paramPrintWriter.println("Exception occurred while dumping:");
      } 
    } else {
      paramPrintWriter.println(sDumpDisabled);
    } 
  }
  
  public void dumpAsync(FileDescriptor paramFileDescriptor, String[] paramArrayOfString) {
    FileOutputStream fileOutputStream = new FileOutputStream(paramFileDescriptor);
    FastPrintWriter fastPrintWriter = new FastPrintWriter(fileOutputStream);
    Object object = new Object(this, "Binder.dumpAsync", paramFileDescriptor, (PrintWriter)fastPrintWriter, paramArrayOfString);
    object.start();
  }
  
  protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {}
  
  public void shellCommand(FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, FileDescriptor paramFileDescriptor3, String[] paramArrayOfString, ShellCallback paramShellCallback, ResultReceiver paramResultReceiver) throws RemoteException {
    onShellCommand(paramFileDescriptor1, paramFileDescriptor2, paramFileDescriptor3, paramArrayOfString, paramShellCallback, paramResultReceiver);
  }
  
  public void onShellCommand(FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, FileDescriptor paramFileDescriptor3, String[] paramArrayOfString, ShellCallback paramShellCallback, ResultReceiver paramResultReceiver) throws RemoteException {
    int i = getCallingUid();
    if (i == 0 || i == 2000) {
      FileDescriptor fileDescriptor1, fileDescriptor2 = paramFileDescriptor1;
      if (paramFileDescriptor1 == null)
        try {
          FileInputStream fileInputStream = new FileInputStream();
          this("/dev/null");
          fileDescriptor2 = fileInputStream.getFD();
        } catch (IOException iOException) {} 
      paramFileDescriptor1 = paramFileDescriptor2;
      if (paramFileDescriptor2 == null) {
        FileOutputStream fileOutputStream = new FileOutputStream();
        this("/dev/null");
        fileDescriptor1 = fileOutputStream.getFD();
      } 
      paramFileDescriptor2 = paramFileDescriptor3;
      if (paramFileDescriptor3 == null)
        paramFileDescriptor2 = fileDescriptor1; 
      String[] arrayOfString = paramArrayOfString;
      if (paramArrayOfString == null)
        arrayOfString = new String[0]; 
      i = -1;
      int j = i, k = i;
      try {
        ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.dup(fileDescriptor2);
        int m = i;
        try {
          ParcelFileDescriptor parcelFileDescriptor1;
        } finally {
          if (parcelFileDescriptor != null)
            try {
              parcelFileDescriptor.close();
            } finally {
              arrayOfString = null;
              j = m;
              k = m;
            }  
          j = m;
          k = m;
        } 
      } catch (IOException iOException) {
        j = k;
        FastPrintWriter fastPrintWriter = new FastPrintWriter();
        j = k;
        FileOutputStream fileOutputStream = new FileOutputStream();
        j = k;
        this(paramFileDescriptor2);
        j = k;
        this(fileOutputStream);
        j = k;
        StringBuilder stringBuilder = new StringBuilder();
        j = k;
        this();
        j = k;
        stringBuilder.append("dup() failed: ");
        j = k;
        stringBuilder.append(iOException.getMessage());
        j = k;
        fastPrintWriter.println(stringBuilder.toString());
        j = k;
        fastPrintWriter.flush();
        paramResultReceiver.send(k, null);
      } finally {}
      return;
    } 
    paramResultReceiver.send(-1, null);
    throw new SecurityException("Shell commands are only callable by ADB");
  }
  
  @SystemApi
  public int handleShellCommand(ParcelFileDescriptor paramParcelFileDescriptor1, ParcelFileDescriptor paramParcelFileDescriptor2, ParcelFileDescriptor paramParcelFileDescriptor3, String[] paramArrayOfString) {
    FileOutputStream fileOutputStream = new FileOutputStream(paramParcelFileDescriptor3.getFileDescriptor());
    FastPrintWriter fastPrintWriter = new FastPrintWriter(fileOutputStream);
    fastPrintWriter.println("No shell command implementation.");
    fastPrintWriter.flush();
    return 0;
  }
  
  public final boolean transact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2) throws RemoteException {
    if (paramParcel1 != null)
      paramParcel1.setDataPosition(0); 
    boolean bool = onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
    if (paramParcel2 != null)
      paramParcel2.setDataPosition(0); 
    return bool;
  }
  
  public void linkToDeath(IBinder.DeathRecipient paramDeathRecipient, int paramInt) {}
  
  public boolean unlinkToDeath(IBinder.DeathRecipient paramDeathRecipient, int paramInt) {
    return true;
  }
  
  static void checkParcel(IBinder paramIBinder, int paramInt, Parcel paramParcel, String paramString) {}
  
  public static void setWorkSourceProvider(BinderInternal.WorkSourceProvider paramWorkSourceProvider) {
    if (paramWorkSourceProvider != null) {
      sWorkSourceProvider = paramWorkSourceProvider;
      return;
    } 
    throw new IllegalArgumentException("workSourceProvider cannot be null");
  }
  
  private boolean execTransact(int paramInt1, long paramLong1, long paramLong2, int paramInt2) {
    int i = getCallingUid();
    long l = ThreadLocalWorkSource.setUid(i);
    try {
      return execTransactInternal(paramInt1, paramLong1, paramLong2, paramInt2, i);
    } finally {
      ThreadLocalWorkSource.restore(l);
    } 
  }
  
  private boolean execTransactInternal(int paramInt1, long paramLong1, long paramLong2, int paramInt2, int paramInt3) {
    BinderInternal.CallSession callSession;
    boolean bool2;
    BinderInternal.Observer observer = sObserver;
    if (observer != null) {
      callSession = observer.callStarted(this, paramInt1, -1);
    } else {
      callSession = null;
    } 
    Parcel parcel1 = Parcel.obtain(paramLong1);
    Parcel parcel2 = Parcel.obtain(paramLong2);
    boolean bool1 = isTracingEnabled();
    if (bool1)
      try {
        Integer integer;
        String str2 = getTransactionName(paramInt1);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(getClass().getName());
        stringBuilder.append(":");
        if (str2 == null)
          integer = Integer.valueOf(paramInt1); 
        stringBuilder.append(integer);
        String str1 = stringBuilder.toString();
        Trace.traceBegin(1L, str1);
      } catch (RemoteException|RuntimeException remoteException) {
      
      } finally {
        Exception exception;
      }  
    if ((paramInt2 & 0x2) != 0) {
      AppOpsManager.startNotedAppOpsCollection(paramInt3);
      try {
        bool2 = onTransact(paramInt1, parcel1, parcel2, paramInt2);
      } finally {
        AppOpsManager.finishNotedAppOpsCollection();
      } 
    } else {
      bool2 = onTransact(paramInt1, parcel1, parcel2, paramInt2);
    } 
    if (bool1)
      Trace.traceEnd(1L); 
    boolean bool3 = bool2;
    if (observer != null) {
      BinderInternal.WorkSourceProvider workSourceProvider = sWorkSourceProvider;
      paramInt2 = parcel1.readCallingWorkSourceUid();
      paramInt2 = workSourceProvider.resolveWorkSourceUid(paramInt2);
      observer.callEnded(callSession, parcel1.dataSize(), parcel2.dataSize(), paramInt2);
      bool3 = bool2;
      checkParcel(this, paramInt1, parcel2, "Unreasonably large binder reply buffer");
      parcel2.recycle();
      parcel1.recycle();
      StrictMode.clearGatheredViolations();
      return bool3;
    } 
    checkParcel(this, paramInt1, parcel2, "Unreasonably large binder reply buffer");
    parcel2.recycle();
    parcel1.recycle();
    StrictMode.clearGatheredViolations();
    return bool3;
  }
  
  public static final native void blockUntilThreadAvailable();
  
  @CriticalNative
  public static final native long clearCallingIdentity();
  
  @CriticalNative
  public static final native long clearCallingWorkSource();
  
  public static final native void flushPendingCommands();
  
  @CriticalNative
  public static final native int getCallingPid();
  
  @CriticalNative
  public static final native int getCallingUid();
  
  @CriticalNative
  public static final native int getCallingWorkSourceUid();
  
  private static native long getFinalizer();
  
  private static native long getNativeBBinderHolder();
  
  private static native long getNativeFinalizer();
  
  @CriticalNative
  public static final native int getThreadStrictModePolicy();
  
  @CriticalNative
  public static final native boolean isHandlingTransaction();
  
  public static final native void restoreCallingIdentity(long paramLong);
  
  @CriticalNative
  public static final native void restoreCallingWorkSource(long paramLong);
  
  @CriticalNative
  public static final native long setCallingWorkSourceUid(int paramInt);
  
  @CriticalNative
  public static final native void setThreadStrictModePolicy(int paramInt);
  
  public final native IBinder getExtension();
  
  public final native void markVintfStability();
  
  public final native void setExtension(IBinder paramIBinder);
}
