package android.os.customize;

import android.content.ComponentName;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IOplusCustomizeRestrictionManagerService extends IInterface {
  void addAppInstallPackageBlacklist(int paramInt, List<String> paramList) throws RemoteException;
  
  void addAppInstallPackageWhitelist(int paramInt, List<String> paramList) throws RemoteException;
  
  boolean allowWifiCellularNetwork(ComponentName paramComponentName, String paramString) throws RemoteException;
  
  void applyQSRestriction(String paramString, int paramInt) throws RemoteException;
  
  void disableQSRestriction(String paramString, int paramInt) throws RemoteException;
  
  int getAirplanePolices(ComponentName paramComponentName) throws RemoteException;
  
  List<String> getAppInstallPackageList(int paramInt) throws RemoteException;
  
  int getAppInstallRestrictionPolicies() throws RemoteException;
  
  List<String> getAppUninstallationPackageList(int paramInt) throws RemoteException;
  
  int getAppUninstallationPolicies() throws RemoteException;
  
  List<String> getApplicationDisabledInLauncherOrRecentTask(int paramInt) throws RemoteException;
  
  List<String> getBluetoothDisabledProfiles() throws RemoteException;
  
  int getCameraPolicies() throws RemoteException;
  
  boolean getClipboardStatus() throws RemoteException;
  
  int getDefaultDataCard(ComponentName paramComponentName) throws RemoteException;
  
  boolean getForbidRecordScreenState() throws RemoteException;
  
  int getGpsPolicies(ComponentName paramComponentName) throws RemoteException;
  
  int getMobileDataMode(ComponentName paramComponentName) throws RemoteException;
  
  int getNfcPolicies(ComponentName paramComponentName) throws RemoteException;
  
  boolean getPowerDisable() throws RemoteException;
  
  boolean getQSRestrictionState(String paramString, int paramInt) throws RemoteException;
  
  int getQSRestrictionValue(String paramString) throws RemoteException;
  
  long getRequiredStrongAuthTime(ComponentName paramComponentName) throws RemoteException;
  
  int getSideBarPolicies(ComponentName paramComponentName) throws RemoteException;
  
  int getSlot1DataConnectivityDisabled(ComponentName paramComponentName) throws RemoteException;
  
  int getSlot2DataConnectivityDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean getSplitScreenDisable(ComponentName paramComponentName) throws RemoteException;
  
  int getSystemUpdatePolicies(ComponentName paramComponentName) throws RemoteException;
  
  int getTorchPolicies() throws RemoteException;
  
  int getUnlockByFacePolicies(ComponentName paramComponentName) throws RemoteException;
  
  int getUnlockByFingerprintPolicies(ComponentName paramComponentName) throws RemoteException;
  
  int getUserPasswordPolicies(ComponentName paramComponentName) throws RemoteException;
  
  boolean isAdbDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isAndroidBeamDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isBackButtonDisabled() throws RemoteException;
  
  boolean isBiometricDisabled() throws RemoteException;
  
  boolean isBluetoothDataTransferDisabled() throws RemoteException;
  
  boolean isBluetoothDisabled() throws RemoteException;
  
  boolean isBluetoothEnabled() throws RemoteException;
  
  boolean isBluetoothOutGoingCallDisabled() throws RemoteException;
  
  boolean isBluetoothPairingDisabled() throws RemoteException;
  
  boolean isBluetoothTetheringDisabled() throws RemoteException;
  
  boolean isChangeWallpaperDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isDataRoamingDisabled() throws RemoteException;
  
  boolean isDiscoverableDisabled() throws RemoteException;
  
  boolean isExternalStorageDisabled() throws RemoteException;
  
  boolean isFloatTaskDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isHomeButtonDisabled() throws RemoteException;
  
  boolean isLanguageChangeDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isLimitedDiscoverableDisabled() throws RemoteException;
  
  boolean isMmsDisabled() throws RemoteException;
  
  boolean isMmsSendReceiveDisabled() throws RemoteException;
  
  boolean isMultiAppSupport() throws RemoteException;
  
  boolean isNFCDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isNFCTurnOn(ComponentName paramComponentName) throws RemoteException;
  
  boolean isNavigationBarDisabled() throws RemoteException;
  
  boolean isPowerSavingModeDisabled() throws RemoteException;
  
  boolean isSafeModeDisabled() throws RemoteException;
  
  boolean isSettingsApplicationDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isSmsReceiveDisabled() throws RemoteException;
  
  boolean isSmsSendDisabled() throws RemoteException;
  
  boolean isTaskButtonDisabled() throws RemoteException;
  
  boolean isUSBDataDisabled() throws RemoteException;
  
  boolean isUSBFileTransferDisabled() throws RemoteException;
  
  boolean isUSBOtgDisabled() throws RemoteException;
  
  boolean isUnknownSourceAppInstallDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isUnlockByFaceDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isUnlockByFingerprintDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isUsbDebugSwitchDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isUsbTetheringDisabled() throws RemoteException;
  
  boolean isVoiceDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isVoiceIncomingDisabled(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  boolean isVoiceOutgoingDisabled(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  boolean isWifiApDisabled() throws RemoteException;
  
  boolean isWifiDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isWifiOpen(ComponentName paramComponentName) throws RemoteException;
  
  boolean isWifiP2pDisabled() throws RemoteException;
  
  void openCloseNFC(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setAdbDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setAirplanePolices(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  boolean setAndroidBeamDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setAppInstallRestrictionPolicies(int paramInt) throws RemoteException;
  
  void setAppUninstallationPolicies(int paramInt, List<String> paramList) throws RemoteException;
  
  void setApplicationDisabledInLauncherOrRecentTask(List<String> paramList, int paramInt) throws RemoteException;
  
  void setBackButtonDisabled(boolean paramBoolean) throws RemoteException;
  
  void setBiometricDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean setBluetoothDataTransferDisable(boolean paramBoolean) throws RemoteException;
  
  void setBluetoothDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean setBluetoothDisabledProfiles(List<String> paramList) throws RemoteException;
  
  void setBluetoothEnabled(boolean paramBoolean) throws RemoteException;
  
  boolean setBluetoothOutGoingCallDisable(boolean paramBoolean) throws RemoteException;
  
  boolean setBluetoothPairingDisable(boolean paramBoolean) throws RemoteException;
  
  boolean setBluetoothTetheringDisable(boolean paramBoolean) throws RemoteException;
  
  boolean setCameraPolicies(int paramInt) throws RemoteException;
  
  void setChangeWallpaperDisable(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setClipboardEnabled(boolean paramBoolean) throws RemoteException;
  
  boolean setDataRoamingDisabled(boolean paramBoolean) throws RemoteException;
  
  Bundle setDefaultDataCard(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  boolean setDiscoverableDisabled(boolean paramBoolean) throws RemoteException;
  
  void setExternalStorageDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean setFloatTaskDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setGpsPolicies(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  void setHomeButtonDisabled(boolean paramBoolean) throws RemoteException;
  
  void setLanguageChangeDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setLimitedDiscoverableDisable(boolean paramBoolean) throws RemoteException;
  
  void setMmsDisabled(boolean paramBoolean) throws RemoteException;
  
  void setMobileDataMode(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  void setMultiAppSupport(boolean paramBoolean) throws RemoteException;
  
  void setNFCDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setNavigationBarDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean setNfcPolicies(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  void setPowerDisable(boolean paramBoolean) throws RemoteException;
  
  void setPowerSavingModeDisabled(boolean paramBoolean) throws RemoteException;
  
  void setRequiredStrongAuthTime(ComponentName paramComponentName, long paramLong) throws RemoteException;
  
  void setSafeModeDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean setScreenCaptureDisabled(boolean paramBoolean) throws RemoteException;
  
  void setSettingsApplicationDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setSideBarPolicies(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  void setSlot1DataConnectivityDisabled(ComponentName paramComponentName, String paramString) throws RemoteException;
  
  void setSlot2DataConnectivityDisabled(ComponentName paramComponentName, String paramString) throws RemoteException;
  
  boolean setSplitScreenDisable(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setSystemUpdatePolicies(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  void setTaskButtonDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean setTorchPolicies(int paramInt) throws RemoteException;
  
  void setUSBDataDisabled(boolean paramBoolean) throws RemoteException;
  
  void setUSBFileTransferDisabled(boolean paramBoolean) throws RemoteException;
  
  void setUSBOtgDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean setUnknownSourceAppInstallDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setUnlockByFaceDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setUnlockByFacePolicies(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  boolean setUnlockByFingerprintDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setUnlockByFingerprintPolicies(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  void setUsbDebugSwitchDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setUsbTetheringDisable(boolean paramBoolean) throws RemoteException;
  
  boolean setUserPasswordPolicies(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  void setVoiceDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setVoiceIncomingDisable(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setVoiceOutgoingDisable(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setWifiApDisabled(boolean paramBoolean) throws RemoteException;
  
  void setWifiDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setWifiInBackground(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setWifiP2pDisabled(boolean paramBoolean) throws RemoteException;
  
  class Default implements IOplusCustomizeRestrictionManagerService {
    public void setClipboardEnabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean getClipboardStatus() throws RemoteException {
      return false;
    }
    
    public void setAppInstallRestrictionPolicies(int param1Int) throws RemoteException {}
    
    public int getAppInstallRestrictionPolicies() throws RemoteException {
      return 0;
    }
    
    public List<String> getAppInstallPackageList(int param1Int) throws RemoteException {
      return null;
    }
    
    public void addAppInstallPackageBlacklist(int param1Int, List<String> param1List) throws RemoteException {}
    
    public void addAppInstallPackageWhitelist(int param1Int, List<String> param1List) throws RemoteException {}
    
    public void setUSBDataDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isUSBDataDisabled() throws RemoteException {
      return false;
    }
    
    public void setUSBFileTransferDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isUSBFileTransferDisabled() throws RemoteException {
      return false;
    }
    
    public void setUSBOtgDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isUSBOtgDisabled() throws RemoteException {
      return false;
    }
    
    public void setBiometricDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isBiometricDisabled() throws RemoteException {
      return false;
    }
    
    public void setSafeModeDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isSafeModeDisabled() throws RemoteException {
      return false;
    }
    
    public void setExternalStorageDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isExternalStorageDisabled() throws RemoteException {
      return false;
    }
    
    public void setUsbTetheringDisable(boolean param1Boolean) throws RemoteException {}
    
    public boolean isUsbTetheringDisabled() throws RemoteException {
      return false;
    }
    
    public void setRequiredStrongAuthTime(ComponentName param1ComponentName, long param1Long) throws RemoteException {}
    
    public long getRequiredStrongAuthTime(ComponentName param1ComponentName) throws RemoteException {
      return 0L;
    }
    
    public boolean setScreenCaptureDisabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean getForbidRecordScreenState() throws RemoteException {
      return false;
    }
    
    public void setAppUninstallationPolicies(int param1Int, List<String> param1List) throws RemoteException {}
    
    public List<String> getAppUninstallationPackageList(int param1Int) throws RemoteException {
      return null;
    }
    
    public int getAppUninstallationPolicies() throws RemoteException {
      return 0;
    }
    
    public void setSlot1DataConnectivityDisabled(ComponentName param1ComponentName, String param1String) throws RemoteException {}
    
    public void setSlot2DataConnectivityDisabled(ComponentName param1ComponentName, String param1String) throws RemoteException {}
    
    public void setVoiceOutgoingDisable(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public void setVoiceIncomingDisable(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean isVoiceDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public void setVoiceDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean isVoiceIncomingDisabled(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isVoiceOutgoingDisabled(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return false;
    }
    
    public void setPowerSavingModeDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isPowerSavingModeDisabled() throws RemoteException {
      return false;
    }
    
    public boolean setDataRoamingDisabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isDataRoamingDisabled() throws RemoteException {
      return false;
    }
    
    public void applyQSRestriction(String param1String, int param1Int) throws RemoteException {}
    
    public void disableQSRestriction(String param1String, int param1Int) throws RemoteException {}
    
    public int getQSRestrictionValue(String param1String) throws RemoteException {
      return 0;
    }
    
    public boolean getQSRestrictionState(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public void setBluetoothDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isBluetoothDisabled() throws RemoteException {
      return false;
    }
    
    public void setBluetoothEnabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isBluetoothEnabled() throws RemoteException {
      return false;
    }
    
    public boolean setDiscoverableDisabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isDiscoverableDisabled() throws RemoteException {
      return false;
    }
    
    public boolean setLimitedDiscoverableDisable(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isLimitedDiscoverableDisabled() throws RemoteException {
      return false;
    }
    
    public boolean setBluetoothPairingDisable(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isBluetoothPairingDisabled() throws RemoteException {
      return false;
    }
    
    public boolean setBluetoothOutGoingCallDisable(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isBluetoothOutGoingCallDisabled() throws RemoteException {
      return false;
    }
    
    public boolean setBluetoothDataTransferDisable(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isBluetoothDataTransferDisabled() throws RemoteException {
      return false;
    }
    
    public boolean setBluetoothTetheringDisable(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isBluetoothTetheringDisabled() throws RemoteException {
      return false;
    }
    
    public boolean setBluetoothDisabledProfiles(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public List<String> getBluetoothDisabledProfiles() throws RemoteException {
      return null;
    }
    
    public void setNFCDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean isNFCDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public void openCloseNFC(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean isNFCTurnOn(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setAndroidBeamDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isAndroidBeamDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setNfcPolicies(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getNfcPolicies(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public int getMobileDataMode(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public void setMobileDataMode(ComponentName param1ComponentName, int param1Int) throws RemoteException {}
    
    public boolean allowWifiCellularNetwork(ComponentName param1ComponentName, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean setCameraPolicies(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getCameraPolicies() throws RemoteException {
      return 0;
    }
    
    public boolean setTorchPolicies(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getTorchPolicies() throws RemoteException {
      return 0;
    }
    
    public void setChangeWallpaperDisable(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean isChangeWallpaperDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public void setSettingsApplicationDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean isSettingsApplicationDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public void setApplicationDisabledInLauncherOrRecentTask(List<String> param1List, int param1Int) throws RemoteException {}
    
    public List<String> getApplicationDisabledInLauncherOrRecentTask(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean setAirplanePolices(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getAirplanePolices(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public boolean setFloatTaskDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isFloatTaskDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean isMultiAppSupport() throws RemoteException {
      return false;
    }
    
    public void setMultiAppSupport(boolean param1Boolean) throws RemoteException {}
    
    public boolean setSplitScreenDisable(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean getSplitScreenDisable(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setUserPasswordPolicies(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getUserPasswordPolicies(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public boolean setUnlockByFingerprintPolicies(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getUnlockByFingerprintPolicies(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public boolean setUnlockByFacePolicies(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getUnlockByFacePolicies(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public boolean setUnlockByFingerprintDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isUnlockByFingerprintDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setUnlockByFaceDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isUnlockByFaceDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public void setTaskButtonDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isTaskButtonDisabled() throws RemoteException {
      return false;
    }
    
    public void setHomeButtonDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isHomeButtonDisabled() throws RemoteException {
      return false;
    }
    
    public void setBackButtonDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isBackButtonDisabled() throws RemoteException {
      return false;
    }
    
    public void setPowerDisable(boolean param1Boolean) throws RemoteException {}
    
    public boolean getPowerDisable() throws RemoteException {
      return false;
    }
    
    public boolean setGpsPolicies(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getGpsPolicies(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public int getDefaultDataCard(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public Bundle setDefaultDataCard(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return null;
    }
    
    public int getSlot1DataConnectivityDisabled(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public int getSlot2DataConnectivityDisabled(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public void setLanguageChangeDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean isLanguageChangeDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean isWifiOpen(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setWifiInBackground(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isWifiDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public void setWifiDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean setWifiApDisabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isWifiApDisabled() throws RemoteException {
      return false;
    }
    
    public boolean setWifiP2pDisabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isWifiP2pDisabled() throws RemoteException {
      return false;
    }
    
    public void setMmsDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isMmsDisabled() throws RemoteException {
      return false;
    }
    
    public boolean isSmsReceiveDisabled() throws RemoteException {
      return false;
    }
    
    public boolean isSmsSendDisabled() throws RemoteException {
      return false;
    }
    
    public boolean isMmsSendReceiveDisabled() throws RemoteException {
      return false;
    }
    
    public void setUsbDebugSwitchDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean isUsbDebugSwitchDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public void setAdbDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean isAdbDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public void setNavigationBarDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isNavigationBarDisabled() throws RemoteException {
      return false;
    }
    
    public boolean setSystemUpdatePolicies(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getSystemUpdatePolicies(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public boolean setUnknownSourceAppInstallDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isUnknownSourceAppInstallDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setSideBarPolicies(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getSideBarPolicies(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizeRestrictionManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizeRestrictionManagerService";
    
    static final int TRANSACTION_addAppInstallPackageBlacklist = 6;
    
    static final int TRANSACTION_addAppInstallPackageWhitelist = 7;
    
    static final int TRANSACTION_allowWifiCellularNetwork = 73;
    
    static final int TRANSACTION_applyQSRestriction = 41;
    
    static final int TRANSACTION_disableQSRestriction = 42;
    
    static final int TRANSACTION_getAirplanePolices = 85;
    
    static final int TRANSACTION_getAppInstallPackageList = 5;
    
    static final int TRANSACTION_getAppInstallRestrictionPolicies = 4;
    
    static final int TRANSACTION_getAppUninstallationPackageList = 27;
    
    static final int TRANSACTION_getAppUninstallationPolicies = 28;
    
    static final int TRANSACTION_getApplicationDisabledInLauncherOrRecentTask = 83;
    
    static final int TRANSACTION_getBluetoothDisabledProfiles = 62;
    
    static final int TRANSACTION_getCameraPolicies = 75;
    
    static final int TRANSACTION_getClipboardStatus = 2;
    
    static final int TRANSACTION_getDefaultDataCard = 112;
    
    static final int TRANSACTION_getForbidRecordScreenState = 25;
    
    static final int TRANSACTION_getGpsPolicies = 111;
    
    static final int TRANSACTION_getMobileDataMode = 71;
    
    static final int TRANSACTION_getNfcPolicies = 70;
    
    static final int TRANSACTION_getPowerDisable = 109;
    
    static final int TRANSACTION_getQSRestrictionState = 44;
    
    static final int TRANSACTION_getQSRestrictionValue = 43;
    
    static final int TRANSACTION_getRequiredStrongAuthTime = 23;
    
    static final int TRANSACTION_getSideBarPolicies = 142;
    
    static final int TRANSACTION_getSlot1DataConnectivityDisabled = 114;
    
    static final int TRANSACTION_getSlot2DataConnectivityDisabled = 115;
    
    static final int TRANSACTION_getSplitScreenDisable = 91;
    
    static final int TRANSACTION_getSystemUpdatePolicies = 138;
    
    static final int TRANSACTION_getTorchPolicies = 77;
    
    static final int TRANSACTION_getUnlockByFacePolicies = 97;
    
    static final int TRANSACTION_getUnlockByFingerprintPolicies = 95;
    
    static final int TRANSACTION_getUserPasswordPolicies = 93;
    
    static final int TRANSACTION_isAdbDisabled = 134;
    
    static final int TRANSACTION_isAndroidBeamDisabled = 68;
    
    static final int TRANSACTION_isBackButtonDisabled = 107;
    
    static final int TRANSACTION_isBiometricDisabled = 15;
    
    static final int TRANSACTION_isBluetoothDataTransferDisabled = 58;
    
    static final int TRANSACTION_isBluetoothDisabled = 46;
    
    static final int TRANSACTION_isBluetoothEnabled = 48;
    
    static final int TRANSACTION_isBluetoothOutGoingCallDisabled = 56;
    
    static final int TRANSACTION_isBluetoothPairingDisabled = 54;
    
    static final int TRANSACTION_isBluetoothTetheringDisabled = 60;
    
    static final int TRANSACTION_isChangeWallpaperDisabled = 79;
    
    static final int TRANSACTION_isDataRoamingDisabled = 40;
    
    static final int TRANSACTION_isDiscoverableDisabled = 50;
    
    static final int TRANSACTION_isExternalStorageDisabled = 19;
    
    static final int TRANSACTION_isFloatTaskDisabled = 87;
    
    static final int TRANSACTION_isHomeButtonDisabled = 105;
    
    static final int TRANSACTION_isLanguageChangeDisabled = 117;
    
    static final int TRANSACTION_isLimitedDiscoverableDisabled = 52;
    
    static final int TRANSACTION_isMmsDisabled = 127;
    
    static final int TRANSACTION_isMmsSendReceiveDisabled = 130;
    
    static final int TRANSACTION_isMultiAppSupport = 88;
    
    static final int TRANSACTION_isNFCDisabled = 64;
    
    static final int TRANSACTION_isNFCTurnOn = 66;
    
    static final int TRANSACTION_isNavigationBarDisabled = 136;
    
    static final int TRANSACTION_isPowerSavingModeDisabled = 38;
    
    static final int TRANSACTION_isSafeModeDisabled = 17;
    
    static final int TRANSACTION_isSettingsApplicationDisabled = 81;
    
    static final int TRANSACTION_isSmsReceiveDisabled = 128;
    
    static final int TRANSACTION_isSmsSendDisabled = 129;
    
    static final int TRANSACTION_isTaskButtonDisabled = 103;
    
    static final int TRANSACTION_isUSBDataDisabled = 9;
    
    static final int TRANSACTION_isUSBFileTransferDisabled = 11;
    
    static final int TRANSACTION_isUSBOtgDisabled = 13;
    
    static final int TRANSACTION_isUnknownSourceAppInstallDisabled = 140;
    
    static final int TRANSACTION_isUnlockByFaceDisabled = 101;
    
    static final int TRANSACTION_isUnlockByFingerprintDisabled = 99;
    
    static final int TRANSACTION_isUsbDebugSwitchDisabled = 132;
    
    static final int TRANSACTION_isUsbTetheringDisabled = 21;
    
    static final int TRANSACTION_isVoiceDisabled = 33;
    
    static final int TRANSACTION_isVoiceIncomingDisabled = 35;
    
    static final int TRANSACTION_isVoiceOutgoingDisabled = 36;
    
    static final int TRANSACTION_isWifiApDisabled = 123;
    
    static final int TRANSACTION_isWifiDisabled = 120;
    
    static final int TRANSACTION_isWifiOpen = 118;
    
    static final int TRANSACTION_isWifiP2pDisabled = 125;
    
    static final int TRANSACTION_openCloseNFC = 65;
    
    static final int TRANSACTION_setAdbDisabled = 133;
    
    static final int TRANSACTION_setAirplanePolices = 84;
    
    static final int TRANSACTION_setAndroidBeamDisabled = 67;
    
    static final int TRANSACTION_setAppInstallRestrictionPolicies = 3;
    
    static final int TRANSACTION_setAppUninstallationPolicies = 26;
    
    static final int TRANSACTION_setApplicationDisabledInLauncherOrRecentTask = 82;
    
    static final int TRANSACTION_setBackButtonDisabled = 106;
    
    static final int TRANSACTION_setBiometricDisabled = 14;
    
    static final int TRANSACTION_setBluetoothDataTransferDisable = 57;
    
    static final int TRANSACTION_setBluetoothDisabled = 45;
    
    static final int TRANSACTION_setBluetoothDisabledProfiles = 61;
    
    static final int TRANSACTION_setBluetoothEnabled = 47;
    
    static final int TRANSACTION_setBluetoothOutGoingCallDisable = 55;
    
    static final int TRANSACTION_setBluetoothPairingDisable = 53;
    
    static final int TRANSACTION_setBluetoothTetheringDisable = 59;
    
    static final int TRANSACTION_setCameraPolicies = 74;
    
    static final int TRANSACTION_setChangeWallpaperDisable = 78;
    
    static final int TRANSACTION_setClipboardEnabled = 1;
    
    static final int TRANSACTION_setDataRoamingDisabled = 39;
    
    static final int TRANSACTION_setDefaultDataCard = 113;
    
    static final int TRANSACTION_setDiscoverableDisabled = 49;
    
    static final int TRANSACTION_setExternalStorageDisabled = 18;
    
    static final int TRANSACTION_setFloatTaskDisabled = 86;
    
    static final int TRANSACTION_setGpsPolicies = 110;
    
    static final int TRANSACTION_setHomeButtonDisabled = 104;
    
    static final int TRANSACTION_setLanguageChangeDisabled = 116;
    
    static final int TRANSACTION_setLimitedDiscoverableDisable = 51;
    
    static final int TRANSACTION_setMmsDisabled = 126;
    
    static final int TRANSACTION_setMobileDataMode = 72;
    
    static final int TRANSACTION_setMultiAppSupport = 89;
    
    static final int TRANSACTION_setNFCDisabled = 63;
    
    static final int TRANSACTION_setNavigationBarDisabled = 135;
    
    static final int TRANSACTION_setNfcPolicies = 69;
    
    static final int TRANSACTION_setPowerDisable = 108;
    
    static final int TRANSACTION_setPowerSavingModeDisabled = 37;
    
    static final int TRANSACTION_setRequiredStrongAuthTime = 22;
    
    static final int TRANSACTION_setSafeModeDisabled = 16;
    
    static final int TRANSACTION_setScreenCaptureDisabled = 24;
    
    static final int TRANSACTION_setSettingsApplicationDisabled = 80;
    
    static final int TRANSACTION_setSideBarPolicies = 141;
    
    static final int TRANSACTION_setSlot1DataConnectivityDisabled = 29;
    
    static final int TRANSACTION_setSlot2DataConnectivityDisabled = 30;
    
    static final int TRANSACTION_setSplitScreenDisable = 90;
    
    static final int TRANSACTION_setSystemUpdatePolicies = 137;
    
    static final int TRANSACTION_setTaskButtonDisabled = 102;
    
    static final int TRANSACTION_setTorchPolicies = 76;
    
    static final int TRANSACTION_setUSBDataDisabled = 8;
    
    static final int TRANSACTION_setUSBFileTransferDisabled = 10;
    
    static final int TRANSACTION_setUSBOtgDisabled = 12;
    
    static final int TRANSACTION_setUnknownSourceAppInstallDisabled = 139;
    
    static final int TRANSACTION_setUnlockByFaceDisabled = 100;
    
    static final int TRANSACTION_setUnlockByFacePolicies = 96;
    
    static final int TRANSACTION_setUnlockByFingerprintDisabled = 98;
    
    static final int TRANSACTION_setUnlockByFingerprintPolicies = 94;
    
    static final int TRANSACTION_setUsbDebugSwitchDisabled = 131;
    
    static final int TRANSACTION_setUsbTetheringDisable = 20;
    
    static final int TRANSACTION_setUserPasswordPolicies = 92;
    
    static final int TRANSACTION_setVoiceDisabled = 34;
    
    static final int TRANSACTION_setVoiceIncomingDisable = 32;
    
    static final int TRANSACTION_setVoiceOutgoingDisable = 31;
    
    static final int TRANSACTION_setWifiApDisabled = 122;
    
    static final int TRANSACTION_setWifiDisabled = 121;
    
    static final int TRANSACTION_setWifiInBackground = 119;
    
    static final int TRANSACTION_setWifiP2pDisabled = 124;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizeRestrictionManagerService");
    }
    
    public static IOplusCustomizeRestrictionManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizeRestrictionManagerService)
        return (IOplusCustomizeRestrictionManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 142:
          return "getSideBarPolicies";
        case 141:
          return "setSideBarPolicies";
        case 140:
          return "isUnknownSourceAppInstallDisabled";
        case 139:
          return "setUnknownSourceAppInstallDisabled";
        case 138:
          return "getSystemUpdatePolicies";
        case 137:
          return "setSystemUpdatePolicies";
        case 136:
          return "isNavigationBarDisabled";
        case 135:
          return "setNavigationBarDisabled";
        case 134:
          return "isAdbDisabled";
        case 133:
          return "setAdbDisabled";
        case 132:
          return "isUsbDebugSwitchDisabled";
        case 131:
          return "setUsbDebugSwitchDisabled";
        case 130:
          return "isMmsSendReceiveDisabled";
        case 129:
          return "isSmsSendDisabled";
        case 128:
          return "isSmsReceiveDisabled";
        case 127:
          return "isMmsDisabled";
        case 126:
          return "setMmsDisabled";
        case 125:
          return "isWifiP2pDisabled";
        case 124:
          return "setWifiP2pDisabled";
        case 123:
          return "isWifiApDisabled";
        case 122:
          return "setWifiApDisabled";
        case 121:
          return "setWifiDisabled";
        case 120:
          return "isWifiDisabled";
        case 119:
          return "setWifiInBackground";
        case 118:
          return "isWifiOpen";
        case 117:
          return "isLanguageChangeDisabled";
        case 116:
          return "setLanguageChangeDisabled";
        case 115:
          return "getSlot2DataConnectivityDisabled";
        case 114:
          return "getSlot1DataConnectivityDisabled";
        case 113:
          return "setDefaultDataCard";
        case 112:
          return "getDefaultDataCard";
        case 111:
          return "getGpsPolicies";
        case 110:
          return "setGpsPolicies";
        case 109:
          return "getPowerDisable";
        case 108:
          return "setPowerDisable";
        case 107:
          return "isBackButtonDisabled";
        case 106:
          return "setBackButtonDisabled";
        case 105:
          return "isHomeButtonDisabled";
        case 104:
          return "setHomeButtonDisabled";
        case 103:
          return "isTaskButtonDisabled";
        case 102:
          return "setTaskButtonDisabled";
        case 101:
          return "isUnlockByFaceDisabled";
        case 100:
          return "setUnlockByFaceDisabled";
        case 99:
          return "isUnlockByFingerprintDisabled";
        case 98:
          return "setUnlockByFingerprintDisabled";
        case 97:
          return "getUnlockByFacePolicies";
        case 96:
          return "setUnlockByFacePolicies";
        case 95:
          return "getUnlockByFingerprintPolicies";
        case 94:
          return "setUnlockByFingerprintPolicies";
        case 93:
          return "getUserPasswordPolicies";
        case 92:
          return "setUserPasswordPolicies";
        case 91:
          return "getSplitScreenDisable";
        case 90:
          return "setSplitScreenDisable";
        case 89:
          return "setMultiAppSupport";
        case 88:
          return "isMultiAppSupport";
        case 87:
          return "isFloatTaskDisabled";
        case 86:
          return "setFloatTaskDisabled";
        case 85:
          return "getAirplanePolices";
        case 84:
          return "setAirplanePolices";
        case 83:
          return "getApplicationDisabledInLauncherOrRecentTask";
        case 82:
          return "setApplicationDisabledInLauncherOrRecentTask";
        case 81:
          return "isSettingsApplicationDisabled";
        case 80:
          return "setSettingsApplicationDisabled";
        case 79:
          return "isChangeWallpaperDisabled";
        case 78:
          return "setChangeWallpaperDisable";
        case 77:
          return "getTorchPolicies";
        case 76:
          return "setTorchPolicies";
        case 75:
          return "getCameraPolicies";
        case 74:
          return "setCameraPolicies";
        case 73:
          return "allowWifiCellularNetwork";
        case 72:
          return "setMobileDataMode";
        case 71:
          return "getMobileDataMode";
        case 70:
          return "getNfcPolicies";
        case 69:
          return "setNfcPolicies";
        case 68:
          return "isAndroidBeamDisabled";
        case 67:
          return "setAndroidBeamDisabled";
        case 66:
          return "isNFCTurnOn";
        case 65:
          return "openCloseNFC";
        case 64:
          return "isNFCDisabled";
        case 63:
          return "setNFCDisabled";
        case 62:
          return "getBluetoothDisabledProfiles";
        case 61:
          return "setBluetoothDisabledProfiles";
        case 60:
          return "isBluetoothTetheringDisabled";
        case 59:
          return "setBluetoothTetheringDisable";
        case 58:
          return "isBluetoothDataTransferDisabled";
        case 57:
          return "setBluetoothDataTransferDisable";
        case 56:
          return "isBluetoothOutGoingCallDisabled";
        case 55:
          return "setBluetoothOutGoingCallDisable";
        case 54:
          return "isBluetoothPairingDisabled";
        case 53:
          return "setBluetoothPairingDisable";
        case 52:
          return "isLimitedDiscoverableDisabled";
        case 51:
          return "setLimitedDiscoverableDisable";
        case 50:
          return "isDiscoverableDisabled";
        case 49:
          return "setDiscoverableDisabled";
        case 48:
          return "isBluetoothEnabled";
        case 47:
          return "setBluetoothEnabled";
        case 46:
          return "isBluetoothDisabled";
        case 45:
          return "setBluetoothDisabled";
        case 44:
          return "getQSRestrictionState";
        case 43:
          return "getQSRestrictionValue";
        case 42:
          return "disableQSRestriction";
        case 41:
          return "applyQSRestriction";
        case 40:
          return "isDataRoamingDisabled";
        case 39:
          return "setDataRoamingDisabled";
        case 38:
          return "isPowerSavingModeDisabled";
        case 37:
          return "setPowerSavingModeDisabled";
        case 36:
          return "isVoiceOutgoingDisabled";
        case 35:
          return "isVoiceIncomingDisabled";
        case 34:
          return "setVoiceDisabled";
        case 33:
          return "isVoiceDisabled";
        case 32:
          return "setVoiceIncomingDisable";
        case 31:
          return "setVoiceOutgoingDisable";
        case 30:
          return "setSlot2DataConnectivityDisabled";
        case 29:
          return "setSlot1DataConnectivityDisabled";
        case 28:
          return "getAppUninstallationPolicies";
        case 27:
          return "getAppUninstallationPackageList";
        case 26:
          return "setAppUninstallationPolicies";
        case 25:
          return "getForbidRecordScreenState";
        case 24:
          return "setScreenCaptureDisabled";
        case 23:
          return "getRequiredStrongAuthTime";
        case 22:
          return "setRequiredStrongAuthTime";
        case 21:
          return "isUsbTetheringDisabled";
        case 20:
          return "setUsbTetheringDisable";
        case 19:
          return "isExternalStorageDisabled";
        case 18:
          return "setExternalStorageDisabled";
        case 17:
          return "isSafeModeDisabled";
        case 16:
          return "setSafeModeDisabled";
        case 15:
          return "isBiometricDisabled";
        case 14:
          return "setBiometricDisabled";
        case 13:
          return "isUSBOtgDisabled";
        case 12:
          return "setUSBOtgDisabled";
        case 11:
          return "isUSBFileTransferDisabled";
        case 10:
          return "setUSBFileTransferDisabled";
        case 9:
          return "isUSBDataDisabled";
        case 8:
          return "setUSBDataDisabled";
        case 7:
          return "addAppInstallPackageWhitelist";
        case 6:
          return "addAppInstallPackageBlacklist";
        case 5:
          return "getAppInstallPackageList";
        case 4:
          return "getAppInstallRestrictionPolicies";
        case 3:
          return "setAppInstallRestrictionPolicies";
        case 2:
          return "getClipboardStatus";
        case 1:
          break;
      } 
      return "setClipboardEnabled";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool17;
        int i11;
        boolean bool16;
        int i10;
        boolean bool15;
        int i9;
        boolean bool14;
        int i8;
        boolean bool13;
        int i7;
        boolean bool12;
        int i6;
        boolean bool11;
        int i5;
        boolean bool10;
        int i4;
        boolean bool9;
        int i3;
        boolean bool8;
        int i2;
        boolean bool7;
        int i1;
        boolean bool6;
        int n;
        boolean bool5;
        int m;
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        boolean bool1;
        Bundle bundle;
        List<String> list3;
        String str2;
        List<String> list2;
        String str1;
        List<String> list1;
        ComponentName componentName;
        ArrayList<String> arrayList;
        String str3;
        long l;
        boolean bool18 = false, bool19 = false, bool20 = false, bool21 = false, bool22 = false, bool23 = false, bool24 = false, bool25 = false, bool26 = false, bool27 = false, bool28 = false, bool29 = false, bool30 = false, bool31 = false, bool32 = false, bool33 = false, bool34 = false, bool35 = false, bool36 = false, bool37 = false, bool38 = false, bool39 = false, bool40 = false, bool41 = false, bool42 = false, bool43 = false, bool44 = false, bool45 = false, bool46 = false, bool47 = false, bool48 = false, bool49 = false, bool50 = false, bool51 = false, bool52 = false, bool53 = false, bool54 = false, bool55 = false, bool56 = false, bool57 = false, bool58 = false, bool59 = false, bool60 = false, bool61 = false, bool62 = false, bool63 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 142:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            param1Int1 = getSideBarPolicies((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 141:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            bool17 = setSideBarPolicies(componentName, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool17);
            return true;
          case 140:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool17 = isUnknownSourceAppInstallDisabled((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool17);
            return true;
          case 139:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            bool53 = bool63;
            if (param1Parcel1.readInt() != 0)
              bool53 = true; 
            bool17 = setUnknownSourceAppInstallDisabled(componentName, bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool17);
            return true;
          case 138:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            i11 = getSystemUpdatePolicies((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i11);
            return true;
          case 137:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            i11 = param1Parcel1.readInt();
            bool16 = setSystemUpdatePolicies(componentName, i11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 136:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool16 = isNavigationBarDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 135:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool18;
            if (param1Parcel1.readInt() != 0)
              bool53 = true; 
            setNavigationBarDisabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 134:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool16 = isAdbDisabled((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 133:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            bool53 = bool19;
            if (param1Parcel1.readInt() != 0)
              bool53 = true; 
            setAdbDisabled(componentName, bool53);
            param1Parcel2.writeNoException();
            return true;
          case 132:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool16 = isUsbDebugSwitchDisabled((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 131:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            bool53 = bool20;
            if (param1Parcel1.readInt() != 0)
              bool53 = true; 
            setUsbDebugSwitchDisabled(componentName, bool53);
            param1Parcel2.writeNoException();
            return true;
          case 130:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool16 = isMmsSendReceiveDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 129:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool16 = isSmsSendDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 128:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool16 = isSmsReceiveDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 127:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool16 = isMmsDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 126:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool21;
            if (param1Parcel1.readInt() != 0)
              bool53 = true; 
            setMmsDisabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 125:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool16 = isWifiP2pDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 124:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool22;
            if (param1Parcel1.readInt() != 0)
              bool53 = true; 
            bool16 = setWifiP2pDisabled(bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 123:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool16 = isWifiApDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 122:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool23;
            if (param1Parcel1.readInt() != 0)
              bool53 = true; 
            bool16 = setWifiApDisabled(bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 121:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            bool53 = bool24;
            if (param1Parcel1.readInt() != 0)
              bool53 = true; 
            setWifiDisabled(componentName, bool53);
            param1Parcel2.writeNoException();
            return true;
          case 120:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool16 = isWifiDisabled((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 119:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            bool53 = bool25;
            if (param1Parcel1.readInt() != 0)
              bool53 = true; 
            bool16 = setWifiInBackground(componentName, bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 118:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool16 = isWifiOpen((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 117:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool16 = isLanguageChangeDisabled((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 116:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            bool53 = bool26;
            if (param1Parcel1.readInt() != 0)
              bool53 = true; 
            setLanguageChangeDisabled(componentName, bool53);
            param1Parcel2.writeNoException();
            return true;
          case 115:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            i10 = getSlot2DataConnectivityDisabled((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i10);
            return true;
          case 114:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            i10 = getSlot1DataConnectivityDisabled((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i10);
            return true;
          case 113:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            i10 = param1Parcel1.readInt();
            bundle = setDefaultDataCard(componentName, i10);
            param1Parcel2.writeNoException();
            if (bundle != null) {
              param1Parcel2.writeInt(1);
              bundle.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 112:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            i10 = getDefaultDataCard((ComponentName)bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i10);
            return true;
          case 111:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            i10 = getGpsPolicies((ComponentName)bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i10);
            return true;
          case 110:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            i10 = bundle.readInt();
            bool15 = setGpsPolicies(componentName, i10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 109:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool15 = getPowerDisable();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 108:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool27;
            if (bundle.readInt() != 0)
              bool53 = true; 
            setPowerDisable(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 107:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool15 = isBackButtonDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 106:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool28;
            if (bundle.readInt() != 0)
              bool53 = true; 
            setBackButtonDisabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 105:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool15 = isHomeButtonDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 104:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool29;
            if (bundle.readInt() != 0)
              bool53 = true; 
            setHomeButtonDisabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 103:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool15 = isTaskButtonDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 102:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool30;
            if (bundle.readInt() != 0)
              bool53 = true; 
            setTaskButtonDisabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 101:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            bool15 = isUnlockByFaceDisabled((ComponentName)bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 100:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            bool53 = bool31;
            if (bundle.readInt() != 0)
              bool53 = true; 
            bool15 = setUnlockByFaceDisabled(componentName, bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 99:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            bool15 = isUnlockByFingerprintDisabled((ComponentName)bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 98:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            bool53 = bool32;
            if (bundle.readInt() != 0)
              bool53 = true; 
            bool15 = setUnlockByFingerprintDisabled(componentName, bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 97:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            i9 = getUnlockByFacePolicies((ComponentName)bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i9);
            return true;
          case 96:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            i9 = bundle.readInt();
            bool14 = setUnlockByFacePolicies(componentName, i9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool14);
            return true;
          case 95:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            i8 = getUnlockByFingerprintPolicies((ComponentName)bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i8);
            return true;
          case 94:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            i8 = bundle.readInt();
            bool13 = setUnlockByFingerprintPolicies(componentName, i8);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool13);
            return true;
          case 93:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            i7 = getUserPasswordPolicies((ComponentName)bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i7);
            return true;
          case 92:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            i7 = bundle.readInt();
            bool12 = setUserPasswordPolicies(componentName, i7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 91:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            bool12 = getSplitScreenDisable((ComponentName)bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 90:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            bool53 = bool33;
            if (bundle.readInt() != 0)
              bool53 = true; 
            bool12 = setSplitScreenDisable(componentName, bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 89:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool34;
            if (bundle.readInt() != 0)
              bool53 = true; 
            setMultiAppSupport(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 88:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool12 = isMultiAppSupport();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 87:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            bool12 = isFloatTaskDisabled((ComponentName)bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 86:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            bool53 = bool35;
            if (bundle.readInt() != 0)
              bool53 = true; 
            bool12 = setFloatTaskDisabled(componentName, bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 85:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            i6 = getAirplanePolices((ComponentName)bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i6);
            return true;
          case 84:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            i6 = bundle.readInt();
            bool11 = setAirplanePolices(componentName, i6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool11);
            return true;
          case 83:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            i5 = bundle.readInt();
            list3 = getApplicationDisabledInLauncherOrRecentTask(i5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list3);
            return true;
          case 82:
            list3.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            arrayList = list3.createStringArrayList();
            i5 = list3.readInt();
            setApplicationDisabledInLauncherOrRecentTask(arrayList, i5);
            param1Parcel2.writeNoException();
            return true;
          case 81:
            list3.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (list3.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)list3);
            } else {
              list3 = null;
            } 
            bool10 = isSettingsApplicationDisabled((ComponentName)list3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 80:
            list3.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (list3.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)list3);
            } else {
              arrayList = null;
            } 
            bool53 = bool36;
            if (list3.readInt() != 0)
              bool53 = true; 
            setSettingsApplicationDisabled((ComponentName)arrayList, bool53);
            param1Parcel2.writeNoException();
            return true;
          case 79:
            list3.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (list3.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)list3);
            } else {
              list3 = null;
            } 
            bool10 = isChangeWallpaperDisabled((ComponentName)list3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 78:
            list3.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (list3.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)list3);
            } else {
              arrayList = null;
            } 
            bool53 = bool37;
            if (list3.readInt() != 0)
              bool53 = true; 
            setChangeWallpaperDisable((ComponentName)arrayList, bool53);
            param1Parcel2.writeNoException();
            return true;
          case 77:
            list3.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            i4 = getTorchPolicies();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i4);
            return true;
          case 76:
            list3.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            i4 = list3.readInt();
            bool9 = setTorchPolicies(i4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 75:
            list3.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            i3 = getCameraPolicies();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i3);
            return true;
          case 74:
            list3.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            i3 = list3.readInt();
            bool8 = setCameraPolicies(i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 73:
            list3.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (list3.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)list3);
            } else {
              arrayList = null;
            } 
            str2 = list3.readString();
            bool8 = allowWifiCellularNetwork((ComponentName)arrayList, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 72:
            str2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str2.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              arrayList = null;
            } 
            i2 = str2.readInt();
            setMobileDataMode((ComponentName)arrayList, i2);
            param1Parcel2.writeNoException();
            return true;
          case 71:
            str2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str2.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            i2 = getMobileDataMode((ComponentName)str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 70:
            str2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str2.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            i2 = getNfcPolicies((ComponentName)str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 69:
            str2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str2.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              arrayList = null;
            } 
            i2 = str2.readInt();
            bool7 = setNfcPolicies((ComponentName)arrayList, i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 68:
            str2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str2.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            bool7 = isAndroidBeamDisabled((ComponentName)str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 67:
            str2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str2.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              arrayList = null;
            } 
            bool53 = bool38;
            if (str2.readInt() != 0)
              bool53 = true; 
            bool7 = setAndroidBeamDisabled((ComponentName)arrayList, bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 66:
            str2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str2.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            bool7 = isNFCTurnOn((ComponentName)str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 65:
            str2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str2.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              arrayList = null;
            } 
            bool53 = bool39;
            if (str2.readInt() != 0)
              bool53 = true; 
            openCloseNFC((ComponentName)arrayList, bool53);
            param1Parcel2.writeNoException();
            return true;
          case 64:
            str2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str2.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            bool7 = isNFCDisabled((ComponentName)str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 63:
            str2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str2.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              arrayList = null;
            } 
            bool53 = bool40;
            if (str2.readInt() != 0)
              bool53 = true; 
            setNFCDisabled((ComponentName)arrayList, bool53);
            param1Parcel2.writeNoException();
            return true;
          case 62:
            str2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            list2 = getBluetoothDisabledProfiles();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list2);
            return true;
          case 61:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            list2 = list2.createStringArrayList();
            bool7 = setBluetoothDisabledProfiles(list2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 60:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool7 = isBluetoothTetheringDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 59:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool41;
            if (list2.readInt() != 0)
              bool53 = true; 
            bool7 = setBluetoothTetheringDisable(bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 58:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool7 = isBluetoothDataTransferDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 57:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool42;
            if (list2.readInt() != 0)
              bool53 = true; 
            bool7 = setBluetoothDataTransferDisable(bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 56:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool7 = isBluetoothOutGoingCallDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 55:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool43;
            if (list2.readInt() != 0)
              bool53 = true; 
            bool7 = setBluetoothOutGoingCallDisable(bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 54:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool7 = isBluetoothPairingDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 53:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool44;
            if (list2.readInt() != 0)
              bool53 = true; 
            bool7 = setBluetoothPairingDisable(bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 52:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool7 = isLimitedDiscoverableDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 51:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool45;
            if (list2.readInt() != 0)
              bool53 = true; 
            bool7 = setLimitedDiscoverableDisable(bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 50:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool7 = isDiscoverableDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 49:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool46;
            if (list2.readInt() != 0)
              bool53 = true; 
            bool7 = setDiscoverableDisabled(bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 48:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool7 = isBluetoothEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 47:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool47;
            if (list2.readInt() != 0)
              bool53 = true; 
            setBluetoothEnabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 46:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool7 = isBluetoothDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 45:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool48;
            if (list2.readInt() != 0)
              bool53 = true; 
            setBluetoothDisabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 44:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            str3 = list2.readString();
            i1 = list2.readInt();
            bool6 = getQSRestrictionState(str3, i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 43:
            list2.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            str1 = list2.readString();
            n = getQSRestrictionValue(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(n);
            return true;
          case 42:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            str3 = str1.readString();
            n = str1.readInt();
            disableQSRestriction(str3, n);
            param1Parcel2.writeNoException();
            return true;
          case 41:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            str3 = str1.readString();
            n = str1.readInt();
            applyQSRestriction(str3, n);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool5 = isDataRoamingDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 39:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool49;
            if (str1.readInt() != 0)
              bool53 = true; 
            bool5 = setDataRoamingDisabled(bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 38:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool5 = isPowerSavingModeDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 37:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool50;
            if (str1.readInt() != 0)
              bool53 = true; 
            setPowerSavingModeDisabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str3 = null;
            } 
            m = str1.readInt();
            bool4 = isVoiceOutgoingDisabled((ComponentName)str3, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 35:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str3 = null;
            } 
            k = str1.readInt();
            bool3 = isVoiceIncomingDisabled((ComponentName)str3, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 34:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str3 = null;
            } 
            bool53 = bool51;
            if (str1.readInt() != 0)
              bool53 = true; 
            setVoiceDisabled((ComponentName)str3, bool53);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            bool3 = isVoiceDisabled((ComponentName)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 32:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str3 = null;
            } 
            bool53 = bool52;
            if (str1.readInt() != 0)
              bool53 = true; 
            setVoiceIncomingDisable((ComponentName)str3, bool53);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str3 = null;
            } 
            if (str1.readInt() != 0)
              bool53 = true; 
            setVoiceOutgoingDisable((ComponentName)str3, bool53);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str3 = null;
            } 
            str1 = str1.readString();
            setSlot2DataConnectivityDisabled((ComponentName)str3, str1);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str3 = null;
            } 
            str1 = str1.readString();
            setSlot1DataConnectivityDisabled((ComponentName)str3, str1);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            j = getAppUninstallationPolicies();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 27:
            str1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            j = str1.readInt();
            list1 = getAppUninstallationPackageList(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 26:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            j = list1.readInt();
            list1 = list1.createStringArrayList();
            setAppUninstallationPolicies(j, list1);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool2 = getForbidRecordScreenState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 24:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool54;
            if (list1.readInt() != 0)
              bool53 = true; 
            bool2 = setScreenCaptureDisabled(bool53);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 23:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (list1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)list1);
            } else {
              list1 = null;
            } 
            l = getRequiredStrongAuthTime((ComponentName)list1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 22:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            if (list1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)list1);
            } else {
              str3 = null;
            } 
            l = list1.readLong();
            setRequiredStrongAuthTime((ComponentName)str3, l);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool2 = isUsbTetheringDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 20:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool55;
            if (list1.readInt() != 0)
              bool53 = true; 
            setUsbTetheringDisable(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool2 = isExternalStorageDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 18:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool56;
            if (list1.readInt() != 0)
              bool53 = true; 
            setExternalStorageDisabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool2 = isSafeModeDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 16:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool57;
            if (list1.readInt() != 0)
              bool53 = true; 
            setSafeModeDisabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool2 = isBiometricDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 14:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool58;
            if (list1.readInt() != 0)
              bool53 = true; 
            setBiometricDisabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool2 = isUSBOtgDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 12:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool59;
            if (list1.readInt() != 0)
              bool53 = true; 
            setUSBOtgDisabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool2 = isUSBFileTransferDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 10:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool60;
            if (list1.readInt() != 0)
              bool53 = true; 
            setUSBFileTransferDisabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool2 = isUSBDataDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 8:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool53 = bool61;
            if (list1.readInt() != 0)
              bool53 = true; 
            setUSBDataDisabled(bool53);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            i = list1.readInt();
            list1 = list1.createStringArrayList();
            addAppInstallPackageWhitelist(i, list1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            i = list1.readInt();
            list1 = list1.createStringArrayList();
            addAppInstallPackageBlacklist(i, list1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            i = list1.readInt();
            list1 = getAppInstallPackageList(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 4:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            i = getAppInstallRestrictionPolicies();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 3:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            i = list1.readInt();
            setAppInstallRestrictionPolicies(i);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
            bool1 = getClipboardStatus();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        list1.enforceInterface("android.os.customize.IOplusCustomizeRestrictionManagerService");
        bool53 = bool62;
        if (list1.readInt() != 0)
          bool53 = true; 
        setClipboardEnabled(bool53);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.os.customize.IOplusCustomizeRestrictionManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusCustomizeRestrictionManagerService {
      public static IOplusCustomizeRestrictionManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizeRestrictionManagerService";
      }
      
      public void setClipboardEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setClipboardEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getClipboardStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getClipboardStatus();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAppInstallRestrictionPolicies(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setAppInstallRestrictionPolicies(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getAppInstallRestrictionPolicies() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getAppInstallRestrictionPolicies(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAppInstallPackageList(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getAppInstallPackageList(param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addAppInstallPackageBlacklist(int param2Int, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeInt(param2Int);
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().addAppInstallPackageBlacklist(param2Int, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addAppInstallPackageWhitelist(int param2Int, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeInt(param2Int);
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().addAppInstallPackageWhitelist(param2Int, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUSBDataDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setUSBDataDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUSBDataDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isUSBDataDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUSBFileTransferDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setUSBFileTransferDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUSBFileTransferDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isUSBFileTransferDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUSBOtgDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setUSBOtgDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUSBOtgDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isUSBOtgDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setBiometricDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setBiometricDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBiometricDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(15, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isBiometricDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSafeModeDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setSafeModeDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSafeModeDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isSafeModeDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setExternalStorageDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setExternalStorageDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isExternalStorageDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(19, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isExternalStorageDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUsbTetheringDisable(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setUsbTetheringDisable(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUsbTetheringDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(21, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isUsbTetheringDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRequiredStrongAuthTime(ComponentName param2ComponentName, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setRequiredStrongAuthTime(param2ComponentName, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getRequiredStrongAuthTime(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getRequiredStrongAuthTime(param2ComponentName); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setScreenCaptureDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setScreenCaptureDisabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getForbidRecordScreenState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(25, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getForbidRecordScreenState();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAppUninstallationPolicies(int param2Int, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeInt(param2Int);
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setAppUninstallationPolicies(param2Int, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAppUninstallationPackageList(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getAppUninstallationPackageList(param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getAppUninstallationPolicies() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getAppUninstallationPolicies(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSlot1DataConnectivityDisabled(ComponentName param2ComponentName, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setSlot1DataConnectivityDisabled(param2ComponentName, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSlot2DataConnectivityDisabled(ComponentName param2ComponentName, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setSlot2DataConnectivityDisabled(param2ComponentName, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVoiceOutgoingDisable(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setVoiceOutgoingDisable(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVoiceIncomingDisable(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setVoiceIncomingDisable(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVoiceDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isVoiceDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVoiceDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setVoiceDisabled(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVoiceIncomingDisabled(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isVoiceIncomingDisabled(param2ComponentName, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVoiceOutgoingDisabled(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isVoiceOutgoingDisabled(param2ComponentName, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPowerSavingModeDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setPowerSavingModeDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPowerSavingModeDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(38, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isPowerSavingModeDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDataRoamingDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setDataRoamingDisabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDataRoamingDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(40, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isDataRoamingDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void applyQSRestriction(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().applyQSRestriction(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableQSRestriction(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().disableQSRestriction(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getQSRestrictionValue(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getQSRestrictionValue(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getQSRestrictionState(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(44, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getQSRestrictionState(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setBluetoothDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setBluetoothDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBluetoothDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(46, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isBluetoothDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setBluetoothEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setBluetoothEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBluetoothEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(48, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isBluetoothEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDiscoverableDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setDiscoverableDisabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDiscoverableDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(50, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isDiscoverableDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setLimitedDiscoverableDisable(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setLimitedDiscoverableDisable(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isLimitedDiscoverableDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(52, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isLimitedDiscoverableDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setBluetoothPairingDisable(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setBluetoothPairingDisable(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBluetoothPairingDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(54, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isBluetoothPairingDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setBluetoothOutGoingCallDisable(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setBluetoothOutGoingCallDisable(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBluetoothOutGoingCallDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(56, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isBluetoothOutGoingCallDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setBluetoothDataTransferDisable(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(57, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setBluetoothDataTransferDisable(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBluetoothDataTransferDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(58, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isBluetoothDataTransferDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setBluetoothTetheringDisable(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setBluetoothTetheringDisable(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBluetoothTetheringDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(60, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isBluetoothTetheringDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setBluetoothDisabledProfiles(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(61, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setBluetoothDisabledProfiles(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getBluetoothDisabledProfiles() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getBluetoothDisabledProfiles(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNFCDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(63, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setNFCDisabled(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNFCDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(64, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isNFCDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void openCloseNFC(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(65, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().openCloseNFC(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNFCTurnOn(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(66, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isNFCTurnOn(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAndroidBeamDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(67, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setAndroidBeamDisabled(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAndroidBeamDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(68, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isAndroidBeamDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setNfcPolicies(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(69, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setNfcPolicies(param2ComponentName, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getNfcPolicies(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(70, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getNfcPolicies(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMobileDataMode(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(71, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getMobileDataMode(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMobileDataMode(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(72, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setMobileDataMode(param2ComponentName, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean allowWifiCellularNetwork(ComponentName param2ComponentName, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(73, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().allowWifiCellularNetwork(param2ComponentName, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setCameraPolicies(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(74, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setCameraPolicies(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCameraPolicies() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = this.mRemote.transact(75, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getCameraPolicies(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setTorchPolicies(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(76, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setTorchPolicies(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getTorchPolicies() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = this.mRemote.transact(77, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getTorchPolicies(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setChangeWallpaperDisable(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(78, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setChangeWallpaperDisable(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isChangeWallpaperDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(79, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isChangeWallpaperDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSettingsApplicationDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(80, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setSettingsApplicationDisabled(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSettingsApplicationDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(81, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isSettingsApplicationDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setApplicationDisabledInLauncherOrRecentTask(List<String> param2List, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeStringList(param2List);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(82, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setApplicationDisabledInLauncherOrRecentTask(param2List, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getApplicationDisabledInLauncherOrRecentTask(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(83, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getApplicationDisabledInLauncherOrRecentTask(param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAirplanePolices(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(84, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setAirplanePolices(param2ComponentName, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getAirplanePolices(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(85, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getAirplanePolices(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setFloatTaskDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(86, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setFloatTaskDisabled(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isFloatTaskDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(87, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isFloatTaskDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isMultiAppSupport() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(88, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isMultiAppSupport();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMultiAppSupport(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(89, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setMultiAppSupport(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSplitScreenDisable(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(90, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setSplitScreenDisable(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getSplitScreenDisable(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(91, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getSplitScreenDisable(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setUserPasswordPolicies(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(92, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setUserPasswordPolicies(param2ComponentName, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUserPasswordPolicies(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(93, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getUserPasswordPolicies(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setUnlockByFingerprintPolicies(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(94, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setUnlockByFingerprintPolicies(param2ComponentName, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUnlockByFingerprintPolicies(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(95, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getUnlockByFingerprintPolicies(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setUnlockByFacePolicies(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(96, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setUnlockByFacePolicies(param2ComponentName, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUnlockByFacePolicies(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(97, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getUnlockByFacePolicies(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setUnlockByFingerprintDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(98, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setUnlockByFingerprintDisabled(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUnlockByFingerprintDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(99, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isUnlockByFingerprintDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setUnlockByFaceDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(100, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setUnlockByFaceDisabled(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUnlockByFaceDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(101, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isUnlockByFaceDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTaskButtonDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(102, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setTaskButtonDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTaskButtonDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(103, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isTaskButtonDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setHomeButtonDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(104, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setHomeButtonDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isHomeButtonDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(105, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isHomeButtonDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setBackButtonDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(106, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setBackButtonDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBackButtonDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(107, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isBackButtonDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPowerDisable(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(108, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setPowerDisable(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getPowerDisable() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(109, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getPowerDisable();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setGpsPolicies(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(110, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setGpsPolicies(param2ComponentName, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getGpsPolicies(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(111, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getGpsPolicies(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDefaultDataCard(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(112, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getDefaultDataCard(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle setDefaultDataCard(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(113, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setDefaultDataCard(param2ComponentName, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bundle bundle = Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2ComponentName = null;
          } 
          return (Bundle)param2ComponentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSlot1DataConnectivityDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(114, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getSlot1DataConnectivityDisabled(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSlot2DataConnectivityDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(115, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getSlot2DataConnectivityDisabled(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setLanguageChangeDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(116, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setLanguageChangeDisabled(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isLanguageChangeDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(117, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isLanguageChangeDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWifiOpen(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(118, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isWifiOpen(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setWifiInBackground(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(119, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setWifiInBackground(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWifiDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(120, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isWifiDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setWifiDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(121, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setWifiDisabled(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setWifiApDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(122, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setWifiApDisabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWifiApDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(123, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isWifiApDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setWifiP2pDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(124, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setWifiP2pDisabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWifiP2pDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(125, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isWifiP2pDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMmsDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(126, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setMmsDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isMmsDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(127, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isMmsDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSmsReceiveDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(128, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isSmsReceiveDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSmsSendDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(129, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isSmsSendDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isMmsSendReceiveDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(130, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isMmsSendReceiveDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUsbDebugSwitchDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(131, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setUsbDebugSwitchDisabled(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUsbDebugSwitchDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(132, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isUsbDebugSwitchDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAdbDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(133, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setAdbDisabled(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAdbDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(134, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isAdbDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNavigationBarDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(135, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setNavigationBarDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNavigationBarDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(136, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isNavigationBarDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSystemUpdatePolicies(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(137, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setSystemUpdatePolicies(param2ComponentName, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSystemUpdatePolicies(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(138, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getSystemUpdatePolicies(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setUnknownSourceAppInstallDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(139, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setUnknownSourceAppInstallDisabled(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUnknownSourceAppInstallDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(140, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().isUnknownSourceAppInstallDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSideBarPolicies(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(141, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().setSideBarPolicies(param2ComponentName, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSideBarPolicies(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeRestrictionManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(142, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeRestrictionManagerService.Stub.getDefaultImpl().getSideBarPolicies(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizeRestrictionManagerService param1IOplusCustomizeRestrictionManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizeRestrictionManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizeRestrictionManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizeRestrictionManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
