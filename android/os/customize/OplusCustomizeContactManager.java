package android.os.customize;

import android.content.Context;
import android.util.Slog;

public class OplusCustomizeContactManager {
  public static final int BLACK_LIST_BLOCK_PATTERN_BLACK_LIST = 1;
  
  public static final int BLACK_LIST_BLOCK_PATTERN_INVALID = 0;
  
  public static final int BLACK_LIST_BLOCK_PATTERN_WHITE_LIST = 2;
  
  public static final int BLACK_LIST_MATCH_PATTERN_ALL = 0;
  
  public static final int BLACK_LIST_MATCH_PATTERN_ALLOW_ALL = 4;
  
  public static final int BLACK_LIST_MATCH_PATTERN_FUZZY = 2;
  
  public static final int BLACK_LIST_MATCH_PATTERN_INTERCEPT_ALL = 3;
  
  public static final int BLACK_LIST_MATCH_PATTERN_PREFIX = 1;
  
  public static final int BLACK_LIST_OUTGO_OR_INCOME_PATTERN_ALL = 2;
  
  public static final int BLACK_LIST_OUTGO_OR_INCOME_PATTERN_INCOME = 1;
  
  public static final int BLACK_LIST_OUTGO_OR_INCOME_PATTERN_OUTGO = 0;
  
  public static final int FORBID_CALL_LOG_DISABLE = 0;
  
  public static final int FORBID_CALL_LOG_ENABLE = 1;
  
  public static final int NUMBER_MASK_HIDE_DISABLE = 2;
  
  public static final int NUMBER_MASK_HIDE_ENABLE = 1;
  
  public static final int NUMBER_MASK_HIDE_MODE_END = 2;
  
  public static final int NUMBER_MASK_HIDE_MODE_MIDDLE = 1;
  
  private static final String SERVICE_NAME = "OplusCustomizeContactManagerService";
  
  private static final String TAG = "OplusCustomizeContactManager";
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizeContactManager sInstance;
  
  private IOplusCustomizeContactManagerService mOplusCustomizeContactManagerService;
  
  private OplusCustomizeContactManager() {
    getOplusCustomizeContactManagerService();
  }
  
  public static final OplusCustomizeContactManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizeContactManager oplusCustomizeContactManager = new OplusCustomizeContactManager();
          this();
          sInstance = oplusCustomizeContactManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusCustomizeContactManagerService getOplusCustomizeContactManagerService() {
    synchronized (mServiceLock) {
      if (this.mOplusCustomizeContactManagerService == null)
        this.mOplusCustomizeContactManagerService = IOplusCustomizeContactManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizeContactManagerService")); 
      return this.mOplusCustomizeContactManagerService;
    } 
  }
  
  public boolean setContactBlackListEnable(boolean paramBoolean) {
    boolean bool = false;
    try {
      paramBoolean = this.mOplusCustomizeContactManagerService.setContactBlackListEnable(paramBoolean);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setContactBlackListEnable Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
      paramBoolean = bool;
    } 
    return paramBoolean;
  }
  
  public boolean isContactBlackListEnable() {
    boolean bool2, bool1 = false;
    try {
      bool2 = this.mOplusCustomizeContactManagerService.isContactBlackListEnable();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isContactBlackListEnable Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public boolean setContactBlockPattern(int paramInt) {
    boolean bool = false;
    try {
      boolean bool1 = this.mOplusCustomizeContactManagerService.setContactBlockPattern(paramInt);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setContactBlockPattern Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
    } 
    return bool;
  }
  
  public int getContactBlockPattern() {
    byte b2, b1 = -1;
    try {
      b2 = this.mOplusCustomizeContactManagerService.getContactBlockPattern();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getContactBlockPattern Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
      b2 = b1;
    } 
    return b2;
  }
  
  public boolean setContactMatchPattern(int paramInt) {
    boolean bool = false;
    try {
      boolean bool1 = this.mOplusCustomizeContactManagerService.setContactMatchPattern(paramInt);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setContactMatchPattern Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
    } 
    return bool;
  }
  
  public int getContactMatchPattern() {
    byte b2, b1 = -1;
    try {
      b2 = this.mOplusCustomizeContactManagerService.getContactMatchPattern();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getContactMatchPattern Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
      b2 = b1;
    } 
    return b2;
  }
  
  public boolean setContactOutgoOrIncomePattern(int paramInt) {
    boolean bool = false;
    try {
      boolean bool1 = this.mOplusCustomizeContactManagerService.setContactOutgoOrIncomePattern(paramInt);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setContactOutgoOrIncomePattern Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
    } 
    return bool;
  }
  
  public int getContactOutgoOrIncomePattern() {
    int i = -1;
    try {
      int j = this.mOplusCustomizeContactManagerService.getContactOutgoOrIncomePattern();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getContactOutgoOrIncomePattern Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
    } 
    return i;
  }
  
  public boolean setContactNumberHideMode(int paramInt) {
    boolean bool2, bool1 = false;
    try {
      bool2 = this.mOplusCustomizeContactManagerService.setContactNumberHideMode(paramInt);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setContactNumberHideMode Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public int getContactNumberHideMode() {
    byte b2, b1 = -1;
    try {
      b2 = this.mOplusCustomizeContactManagerService.getContactNumberHideMode();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getContactNumberHideMode Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
      b2 = b1;
    } 
    return b2;
  }
  
  public boolean setContactNumberMaskEnable(int paramInt) {
    boolean bool = false;
    try {
      boolean bool1 = this.mOplusCustomizeContactManagerService.setContactNumberMaskEnable(paramInt);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setContactNumberMaskEnable Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
    } 
    return bool;
  }
  
  public int getContactNumberMaskEnable() {
    int i = -1;
    try {
      int j = this.mOplusCustomizeContactManagerService.getContactNumberMaskEnable();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getContactNumberMaskEnable Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
    } 
    return i;
  }
  
  public boolean setForbidCallLogEnable(int paramInt) {
    boolean bool2, bool1 = false;
    try {
      bool2 = this.mOplusCustomizeContactManagerService.setForbidCallLogEnable(paramInt);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setForbidCallLogEnable Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public boolean isForbidCallLogEnable() {
    boolean bool2, bool1 = false;
    try {
      bool2 = this.mOplusCustomizeContactManagerService.isForbidCallLogEnable();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isForbidCallLogEnable Error :");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeContactManager", stringBuilder.toString());
      bool2 = bool1;
    } 
    return bool2;
  }
}
