package android.os.customize;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Slog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class OplusCustomizePackageManager {
  private static final String SERVICE_NAME = "OplusCustomizePackageManagerService";
  
  private static final String TAG = "OplusCustomizePackageManager";
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizePackageManager sInstance;
  
  private IOplusCustomizePackageManagerService mIOplusCustomizePackageManagerService;
  
  private OplusCustomizePackageManager() {
    getOplusCustomizePackageManagerService();
  }
  
  public static final OplusCustomizePackageManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizePackageManager oplusCustomizePackageManager = new OplusCustomizePackageManager();
          this();
          sInstance = oplusCustomizePackageManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusCustomizePackageManagerService getOplusCustomizePackageManagerService() {
    synchronized (mServiceLock) {
      if (this.mIOplusCustomizePackageManagerService == null)
        this.mIOplusCustomizePackageManagerService = IOplusCustomizePackageManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizePackageManagerService")); 
      if (this.mIOplusCustomizePackageManagerService == null)
        Slog.e("OplusCustomizePackageManager", "mIOplusCustomizePackageManagerService is null"); 
      return this.mIOplusCustomizePackageManagerService;
    } 
  }
  
  public void addDisabledDeactivateMdmPackages(List<String> paramList) {
    try {
      getOplusCustomizePackageManagerService().addDisabledDeactivateMdmPackages(paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "addDisabledDeactivateMdmPackages fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addDisabledDeactivateMdmPackages Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
  }
  
  public void removeDisabledDeactivateMdmPackages(List<String> paramList) {
    try {
      getOplusCustomizePackageManagerService().removeDisabledDeactivateMdmPackages(paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "removeDisabledDeactivateMdmPackages fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeDisabledDeactivateMdmPackages Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
  }
  
  public List<String> getDisabledDeactivateMdmPackages() {
    List<String> list = new ArrayList();
    try {
      List<String> list1 = getOplusCustomizePackageManagerService().getDisabledDeactivateMdmPackages();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "getDisabledDeactivateMdmPackages fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getDisabledDeactivateMdmPackages Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return list;
  }
  
  public void removeAllDisabledDeactivateMdmPackages() {
    try {
      getOplusCustomizePackageManagerService().removeAllDisabledDeactivateMdmPackages();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "removeAllDisabledDeactivateMdmPackages fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeAllDisabledDeactivateMdmPackages Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
  }
  
  public boolean isDisabledDeactivateMdmPackage(String paramString) {
    try {
      return getOplusCustomizePackageManagerService().isDisabledDeactivateMdmPackage(paramString);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "isDisabledDeactivateMdmPackage fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isDisabledDeactivateMdmPackage Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public void addDisallowedUninstallPackages(List<String> paramList) {
    try {
      getOplusCustomizePackageManagerService().addDisallowedUninstallPackages(paramList);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizePackageManager", "addDisallowedUninstallPackages: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addDisallowedUninstallPackages Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
  }
  
  public void removeDisallowedUninstallPackages(List<String> paramList) {
    try {
      getOplusCustomizePackageManagerService().removeDisallowedUninstallPackages(paramList);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizePackageManager", "removeDisallowedUninstallPackages: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeDisallowedUninstallPackages Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
  }
  
  public void removeAllDisallowedUninstallPackages() {
    try {
      getOplusCustomizePackageManagerService().removeAllDisallowedUninstallPackages();
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizePackageManager", "removeAllDisallowedUninstallPackages: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeAllDisallowedUninstallPackages Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
  }
  
  public List<String> getDisallowUninstallPackageList() {
    try {
      List<String> list = getOplusCustomizePackageManagerService().getDisallowUninstallPackageList();
      if (list == null)
        list = Collections.emptyList(); 
      return list;
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizePackageManager", "getDisallowUninstallPackageList: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getDisallowUninstallPackageList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return Collections.emptyList();
  }
  
  public void clearAppData(String paramString) {
    try {
      getOplusCustomizePackageManagerService().clearAppData(paramString);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "clearApplicationUserData: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("clearApplicationUserData Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
  }
  
  public List<String> getClearAppName() {
    List<String> list = null;
    RemoteException remoteException2 = null;
    try {
      List<String> list1 = getOplusCustomizePackageManagerService().getClearAppName();
    } catch (RemoteException remoteException1) {
      Slog.e("OplusCustomizePackageManager", "getClearAppName: fail", (Throwable)remoteException1);
      remoteException1 = remoteException2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getClearAppName Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return (List<String>)remoteException1;
  }
  
  public void setAdbInstallUninstallDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizePackageManagerService().setAdbInstallUninstallDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "setAdbInstallUninstallDisabled fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setAdbInstallUninstallDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
  }
  
  public boolean getAdbInstallUninstallDisabled() {
    try {
      return getOplusCustomizePackageManagerService().getAdbInstallUninstallDisabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "getAdbInstallUninstallDisabled fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAdbInstallUninstallDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public void setInstallSysAppBundle(Bundle paramBundle) {
    try {
      getOplusCustomizePackageManagerService().setInstallSysAppBundle(paramBundle);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "setInstallSysAppBundle: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setInstallSysAppBundle Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
  }
  
  public Bundle getInstallSysAppBundle() {
    try {
      return getOplusCustomizePackageManagerService().getInstallSysAppBundle();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "getInstallSysAppBundle: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getInstallSysAppBundle Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public List<String> getPrivInstallSysAppList() {
    try {
      return getOplusCustomizePackageManagerService().getPrivInstallSysAppList();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "getPrivInstallSysAppList: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getPrivInstallSysAppList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public List<String> getDetachableInstallSysAppList() {
    try {
      return getOplusCustomizePackageManagerService().getDetachableInstallSysAppList();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "getDetachableInstallSysAppList: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getDetachableInstallSysAppList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public List<String> getAllInstallSysAppList() {
    try {
      return getOplusCustomizePackageManagerService().getAllInstallSysAppList();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "getAllInstallSysAppList: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAllInstallSysAppList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public Map<String, String> getContainOplusCertificatePackages() {
    try {
      return getOplusCustomizePackageManagerService().getContainOplusCertificatePackages();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "getContainOplusCertificatePackages: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getContainOplusCertificatePackages Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public boolean setSuperWhiteList(List<String> paramList) {
    try {
      return getOplusCustomizePackageManagerService().setSuperWhiteList(paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "setSuperWhiteList fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSuperWhiteList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public List<String> getSuperWhiteList() {
    try {
      return getOplusCustomizePackageManagerService().getSuperWhiteList();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "getSuperWhiteList: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSuperWhiteList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public boolean clearSuperWhiteList(List<String> paramList) {
    try {
      return getOplusCustomizePackageManagerService().clearSuperWhiteList(paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "clearSuperWhiteList fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("clearSuperWhiteList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public boolean clearAllSuperWhiteList() {
    try {
      return getOplusCustomizePackageManagerService().clearAllSuperWhiteList();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePackageManager", "clearAllSuperWhiteList fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("clearAllSuperWhiteList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePackageManager", stringBuilder.toString());
    } 
    return false;
  }
}
