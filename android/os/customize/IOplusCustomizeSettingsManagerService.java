package android.os.customize;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusCustomizeSettingsManagerService extends IInterface {
  long getAutoScreenOffTime(ComponentName paramComponentName) throws RemoteException;
  
  String getRomVersion(ComponentName paramComponentName) throws RemoteException;
  
  boolean isBackupRestoreDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isDeveloperOptionsDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isProtectEyesOn(ComponentName paramComponentName) throws RemoteException;
  
  boolean isRestoreFactoryDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isSIMLockDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isScreenOffTimeSetByPolicy(ComponentName paramComponentName) throws RemoteException;
  
  boolean isSearchIndexDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isTimeAndDateSetDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean setAutoScreenOffTime(ComponentName paramComponentName, long paramLong) throws RemoteException;
  
  boolean setBackupRestoreDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setDevelopmentOptionsDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setInterceptAllNotifications(boolean paramBoolean) throws RemoteException;
  
  boolean setInterceptNonSystemNotifications(boolean paramBoolean) throws RemoteException;
  
  boolean setRestoreFactoryDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setSIMLockDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setSearchIndexDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setTimeAndDateSetDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean shouldInterceptAllNotifications() throws RemoteException;
  
  boolean shouldInterceptNonSystemNotifications() throws RemoteException;
  
  void storeLastManualScreenOffTimeout(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  boolean turnOnProtectEyes(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  class Default implements IOplusCustomizeSettingsManagerService {
    public boolean turnOnProtectEyes(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isProtectEyesOn(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setSIMLockDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isSIMLockDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setBackupRestoreDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isBackupRestoreDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setAutoScreenOffTime(ComponentName param1ComponentName, long param1Long) throws RemoteException {
      return false;
    }
    
    public long getAutoScreenOffTime(ComponentName param1ComponentName) throws RemoteException {
      return 0L;
    }
    
    public boolean isScreenOffTimeSetByPolicy(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public void storeLastManualScreenOffTimeout(ComponentName param1ComponentName, int param1Int) throws RemoteException {}
    
    public boolean setSearchIndexDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isSearchIndexDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public String getRomVersion(ComponentName param1ComponentName) throws RemoteException {
      return null;
    }
    
    public boolean setRestoreFactoryDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isRestoreFactoryDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setDevelopmentOptionsDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isDeveloperOptionsDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setTimeAndDateSetDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isTimeAndDateSetDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setInterceptAllNotifications(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean shouldInterceptAllNotifications() throws RemoteException {
      return false;
    }
    
    public boolean setInterceptNonSystemNotifications(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean shouldInterceptNonSystemNotifications() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizeSettingsManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizeSettingsManagerService";
    
    static final int TRANSACTION_getAutoScreenOffTime = 8;
    
    static final int TRANSACTION_getRomVersion = 13;
    
    static final int TRANSACTION_isBackupRestoreDisabled = 6;
    
    static final int TRANSACTION_isDeveloperOptionsDisabled = 17;
    
    static final int TRANSACTION_isProtectEyesOn = 2;
    
    static final int TRANSACTION_isRestoreFactoryDisabled = 15;
    
    static final int TRANSACTION_isSIMLockDisabled = 4;
    
    static final int TRANSACTION_isScreenOffTimeSetByPolicy = 9;
    
    static final int TRANSACTION_isSearchIndexDisabled = 12;
    
    static final int TRANSACTION_isTimeAndDateSetDisabled = 19;
    
    static final int TRANSACTION_setAutoScreenOffTime = 7;
    
    static final int TRANSACTION_setBackupRestoreDisabled = 5;
    
    static final int TRANSACTION_setDevelopmentOptionsDisabled = 16;
    
    static final int TRANSACTION_setInterceptAllNotifications = 20;
    
    static final int TRANSACTION_setInterceptNonSystemNotifications = 22;
    
    static final int TRANSACTION_setRestoreFactoryDisabled = 14;
    
    static final int TRANSACTION_setSIMLockDisabled = 3;
    
    static final int TRANSACTION_setSearchIndexDisabled = 11;
    
    static final int TRANSACTION_setTimeAndDateSetDisabled = 18;
    
    static final int TRANSACTION_shouldInterceptAllNotifications = 21;
    
    static final int TRANSACTION_shouldInterceptNonSystemNotifications = 23;
    
    static final int TRANSACTION_storeLastManualScreenOffTimeout = 10;
    
    static final int TRANSACTION_turnOnProtectEyes = 1;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizeSettingsManagerService");
    }
    
    public static IOplusCustomizeSettingsManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizeSettingsManagerService)
        return (IOplusCustomizeSettingsManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 23:
          return "shouldInterceptNonSystemNotifications";
        case 22:
          return "setInterceptNonSystemNotifications";
        case 21:
          return "shouldInterceptAllNotifications";
        case 20:
          return "setInterceptAllNotifications";
        case 19:
          return "isTimeAndDateSetDisabled";
        case 18:
          return "setTimeAndDateSetDisabled";
        case 17:
          return "isDeveloperOptionsDisabled";
        case 16:
          return "setDevelopmentOptionsDisabled";
        case 15:
          return "isRestoreFactoryDisabled";
        case 14:
          return "setRestoreFactoryDisabled";
        case 13:
          return "getRomVersion";
        case 12:
          return "isSearchIndexDisabled";
        case 11:
          return "setSearchIndexDisabled";
        case 10:
          return "storeLastManualScreenOffTimeout";
        case 9:
          return "isScreenOffTimeSetByPolicy";
        case 8:
          return "getAutoScreenOffTime";
        case 7:
          return "setAutoScreenOffTime";
        case 6:
          return "isBackupRestoreDisabled";
        case 5:
          return "setBackupRestoreDisabled";
        case 4:
          return "isSIMLockDisabled";
        case 3:
          return "setSIMLockDisabled";
        case 2:
          return "isProtectEyesOn";
        case 1:
          break;
      } 
      return "turnOnProtectEyes";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int i;
        String str;
        ComponentName componentName;
        long l;
        boolean bool3 = false, bool4 = false, bool5 = false, bool6 = false, bool7 = false, bool8 = false, bool9 = false, bool10 = false, bool11 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 23:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            bool2 = shouldInterceptNonSystemNotifications();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 22:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            bool9 = bool11;
            if (param1Parcel1.readInt() != 0)
              bool9 = true; 
            bool2 = setInterceptNonSystemNotifications(bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 21:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            bool2 = shouldInterceptAllNotifications();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 20:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            bool9 = bool3;
            if (param1Parcel1.readInt() != 0)
              bool9 = true; 
            bool2 = setInterceptAllNotifications(bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 19:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool2 = isTimeAndDateSetDisabled((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 18:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            bool9 = bool4;
            if (param1Parcel1.readInt() != 0)
              bool9 = true; 
            bool2 = setTimeAndDateSetDisabled(componentName, bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 17:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool2 = isDeveloperOptionsDisabled((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 16:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            bool9 = bool5;
            if (param1Parcel1.readInt() != 0)
              bool9 = true; 
            bool2 = setDevelopmentOptionsDisabled(componentName, bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 15:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool2 = isRestoreFactoryDisabled((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 14:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            bool9 = bool6;
            if (param1Parcel1.readInt() != 0)
              bool9 = true; 
            bool2 = setRestoreFactoryDisabled(componentName, bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 13:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            str = getRomVersion((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str);
            return true;
          case 12:
            str.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (str.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            bool2 = isSearchIndexDisabled((ComponentName)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 11:
            str.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (str.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              componentName = null;
            } 
            bool9 = bool7;
            if (str.readInt() != 0)
              bool9 = true; 
            bool2 = setSearchIndexDisabled(componentName, bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 10:
            str.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (str.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              componentName = null;
            } 
            i = str.readInt();
            storeLastManualScreenOffTimeout(componentName, i);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (str.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            bool1 = isScreenOffTimeSetByPolicy((ComponentName)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 8:
            str.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (str.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            l = getAutoScreenOffTime((ComponentName)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 7:
            str.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (str.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              componentName = null;
            } 
            l = str.readLong();
            bool1 = setAutoScreenOffTime(componentName, l);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 6:
            str.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (str.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            bool1 = isBackupRestoreDisabled((ComponentName)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 5:
            str.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (str.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              componentName = null;
            } 
            bool9 = bool8;
            if (str.readInt() != 0)
              bool9 = true; 
            bool1 = setBackupRestoreDisabled(componentName, bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 4:
            str.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (str.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            bool1 = isSIMLockDisabled((ComponentName)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            str.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (str.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              componentName = null;
            } 
            if (str.readInt() != 0)
              bool9 = true; 
            bool1 = setSIMLockDisabled(componentName, bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            str.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
            if (str.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            bool1 = isProtectEyesOn((ComponentName)str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        str.enforceInterface("android.os.customize.IOplusCustomizeSettingsManagerService");
        if (str.readInt() != 0) {
          componentName = ComponentName.CREATOR.createFromParcel((Parcel)str);
        } else {
          componentName = null;
        } 
        bool9 = bool10;
        if (str.readInt() != 0)
          bool9 = true; 
        boolean bool1 = turnOnProtectEyes(componentName, bool9);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.os.customize.IOplusCustomizeSettingsManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusCustomizeSettingsManagerService {
      public static IOplusCustomizeSettingsManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizeSettingsManagerService";
      }
      
      public boolean turnOnProtectEyes(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().turnOnProtectEyes(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isProtectEyesOn(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().isProtectEyesOn(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSIMLockDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().setSIMLockDisabled(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSIMLockDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().isSIMLockDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setBackupRestoreDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().setBackupRestoreDisabled(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBackupRestoreDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().isBackupRestoreDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAutoScreenOffTime(ComponentName param2ComponentName, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeLong(param2Long);
          boolean bool2 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().setAutoScreenOffTime(param2ComponentName, param2Long);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getAutoScreenOffTime(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().getAutoScreenOffTime(param2ComponentName); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isScreenOffTimeSetByPolicy(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().isScreenOffTimeSetByPolicy(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void storeLastManualScreenOffTimeout(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().storeLastManualScreenOffTimeout(param2ComponentName, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSearchIndexDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().setSearchIndexDisabled(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSearchIndexDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().isSearchIndexDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getRomVersion(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().getRomVersion(param2ComponentName); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setRestoreFactoryDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().setRestoreFactoryDisabled(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRestoreFactoryDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().isRestoreFactoryDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDevelopmentOptionsDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().setDevelopmentOptionsDisabled(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDeveloperOptionsDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().isDeveloperOptionsDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setTimeAndDateSetDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().setTimeAndDateSetDisabled(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTimeAndDateSetDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().isTimeAndDateSetDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setInterceptAllNotifications(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().setInterceptAllNotifications(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean shouldInterceptAllNotifications() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(21, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().shouldInterceptAllNotifications();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setInterceptNonSystemNotifications(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().setInterceptNonSystemNotifications(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean shouldInterceptNonSystemNotifications() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSettingsManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(23, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSettingsManagerService.Stub.getDefaultImpl().shouldInterceptNonSystemNotifications();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizeSettingsManagerService param1IOplusCustomizeSettingsManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizeSettingsManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizeSettingsManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizeSettingsManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
