package android.os.customize;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IOplusCustomizeApplicationManagerService extends IInterface {
  void addAppAlarmWhiteList(List<String> paramList) throws RemoteException;
  
  void addDisallowedRunningApp(List<String> paramList) throws RemoteException;
  
  void addPersistentApp(List<String> paramList) throws RemoteException;
  
  void addTrustedAppStore(String paramString) throws RemoteException;
  
  void cleanBackgroundProcess() throws RemoteException;
  
  void deleteTrustedAppStore(String paramString) throws RemoteException;
  
  void enableTrustedAppStore(boolean paramBoolean) throws RemoteException;
  
  boolean forceStopPackage(List<String> paramList) throws RemoteException;
  
  List<String> getAppAlarmWhiteList() throws RemoteException;
  
  List<String> getDisabledAppList() throws RemoteException;
  
  List<String> getDisallowedRunningApp() throws RemoteException;
  
  List getPersistentApp() throws RemoteException;
  
  boolean getStopLockTaskAvailability() throws RemoteException;
  
  String getTopAppPackageName() throws RemoteException;
  
  List<String> getTrustedAppStore() throws RemoteException;
  
  void interceptStopLockTask(boolean paramBoolean) throws RemoteException;
  
  boolean isAllowControlAppRun() throws RemoteException;
  
  boolean isAllowTrustedAppStore() throws RemoteException;
  
  boolean isTrustedAppStoreEnabled() throws RemoteException;
  
  void killApplicationProcess(String paramString) throws RemoteException;
  
  boolean removeAllAppAlarmWhiteList() throws RemoteException;
  
  boolean removeAppAlarmWhiteList(List<String> paramList) throws RemoteException;
  
  void removeDisallowedRunningApp(List<String> paramList) throws RemoteException;
  
  void removePersistentApp(List<String> paramList) throws RemoteException;
  
  void setAllowControlAppRun(boolean paramBoolean) throws RemoteException;
  
  void setAllowTrustedAppStore(boolean paramBoolean) throws RemoteException;
  
  boolean setDisabledAppList(List<String> paramList, int paramInt) throws RemoteException;
  
  class Default implements IOplusCustomizeApplicationManagerService {
    public boolean setDisabledAppList(List<String> param1List, int param1Int) throws RemoteException {
      return false;
    }
    
    public List<String> getDisabledAppList() throws RemoteException {
      return null;
    }
    
    public boolean forceStopPackage(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public String getTopAppPackageName() throws RemoteException {
      return null;
    }
    
    public void killApplicationProcess(String param1String) throws RemoteException {}
    
    public void cleanBackgroundProcess() throws RemoteException {}
    
    public void addDisallowedRunningApp(List<String> param1List) throws RemoteException {}
    
    public void removeDisallowedRunningApp(List<String> param1List) throws RemoteException {}
    
    public List<String> getDisallowedRunningApp() throws RemoteException {
      return null;
    }
    
    public void addTrustedAppStore(String param1String) throws RemoteException {}
    
    public void deleteTrustedAppStore(String param1String) throws RemoteException {}
    
    public void enableTrustedAppStore(boolean param1Boolean) throws RemoteException {}
    
    public boolean isTrustedAppStoreEnabled() throws RemoteException {
      return false;
    }
    
    public List<String> getTrustedAppStore() throws RemoteException {
      return null;
    }
    
    public void setAllowTrustedAppStore(boolean param1Boolean) throws RemoteException {}
    
    public boolean isAllowTrustedAppStore() throws RemoteException {
      return false;
    }
    
    public void addAppAlarmWhiteList(List<String> param1List) throws RemoteException {}
    
    public List<String> getAppAlarmWhiteList() throws RemoteException {
      return null;
    }
    
    public boolean removeAppAlarmWhiteList(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public boolean removeAllAppAlarmWhiteList() throws RemoteException {
      return false;
    }
    
    public void setAllowControlAppRun(boolean param1Boolean) throws RemoteException {}
    
    public boolean isAllowControlAppRun() throws RemoteException {
      return false;
    }
    
    public void addPersistentApp(List<String> param1List) throws RemoteException {}
    
    public void removePersistentApp(List<String> param1List) throws RemoteException {}
    
    public List getPersistentApp() throws RemoteException {
      return null;
    }
    
    public void interceptStopLockTask(boolean param1Boolean) throws RemoteException {}
    
    public boolean getStopLockTaskAvailability() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizeApplicationManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizeApplicationManagerService";
    
    static final int TRANSACTION_addAppAlarmWhiteList = 17;
    
    static final int TRANSACTION_addDisallowedRunningApp = 7;
    
    static final int TRANSACTION_addPersistentApp = 23;
    
    static final int TRANSACTION_addTrustedAppStore = 10;
    
    static final int TRANSACTION_cleanBackgroundProcess = 6;
    
    static final int TRANSACTION_deleteTrustedAppStore = 11;
    
    static final int TRANSACTION_enableTrustedAppStore = 12;
    
    static final int TRANSACTION_forceStopPackage = 3;
    
    static final int TRANSACTION_getAppAlarmWhiteList = 18;
    
    static final int TRANSACTION_getDisabledAppList = 2;
    
    static final int TRANSACTION_getDisallowedRunningApp = 9;
    
    static final int TRANSACTION_getPersistentApp = 25;
    
    static final int TRANSACTION_getStopLockTaskAvailability = 27;
    
    static final int TRANSACTION_getTopAppPackageName = 4;
    
    static final int TRANSACTION_getTrustedAppStore = 14;
    
    static final int TRANSACTION_interceptStopLockTask = 26;
    
    static final int TRANSACTION_isAllowControlAppRun = 22;
    
    static final int TRANSACTION_isAllowTrustedAppStore = 16;
    
    static final int TRANSACTION_isTrustedAppStoreEnabled = 13;
    
    static final int TRANSACTION_killApplicationProcess = 5;
    
    static final int TRANSACTION_removeAllAppAlarmWhiteList = 20;
    
    static final int TRANSACTION_removeAppAlarmWhiteList = 19;
    
    static final int TRANSACTION_removeDisallowedRunningApp = 8;
    
    static final int TRANSACTION_removePersistentApp = 24;
    
    static final int TRANSACTION_setAllowControlAppRun = 21;
    
    static final int TRANSACTION_setAllowTrustedAppStore = 15;
    
    static final int TRANSACTION_setDisabledAppList = 1;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizeApplicationManagerService");
    }
    
    public static IOplusCustomizeApplicationManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizeApplicationManagerService)
        return (IOplusCustomizeApplicationManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 27:
          return "getStopLockTaskAvailability";
        case 26:
          return "interceptStopLockTask";
        case 25:
          return "getPersistentApp";
        case 24:
          return "removePersistentApp";
        case 23:
          return "addPersistentApp";
        case 22:
          return "isAllowControlAppRun";
        case 21:
          return "setAllowControlAppRun";
        case 20:
          return "removeAllAppAlarmWhiteList";
        case 19:
          return "removeAppAlarmWhiteList";
        case 18:
          return "getAppAlarmWhiteList";
        case 17:
          return "addAppAlarmWhiteList";
        case 16:
          return "isAllowTrustedAppStore";
        case 15:
          return "setAllowTrustedAppStore";
        case 14:
          return "getTrustedAppStore";
        case 13:
          return "isTrustedAppStoreEnabled";
        case 12:
          return "enableTrustedAppStore";
        case 11:
          return "deleteTrustedAppStore";
        case 10:
          return "addTrustedAppStore";
        case 9:
          return "getDisallowedRunningApp";
        case 8:
          return "removeDisallowedRunningApp";
        case 7:
          return "addDisallowedRunningApp";
        case 6:
          return "cleanBackgroundProcess";
        case 5:
          return "killApplicationProcess";
        case 4:
          return "getTopAppPackageName";
        case 3:
          return "forceStopPackage";
        case 2:
          return "getDisabledAppList";
        case 1:
          break;
      } 
      return "setDisabledAppList";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        List<String> list3;
        String str2;
        List<String> list2;
        String str1;
        ArrayList<String> arrayList1;
        List<String> list1;
        boolean bool3 = false, bool4 = false, bool5 = false, bool6 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 27:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            bool2 = getStopLockTaskAvailability();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 26:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            if (param1Parcel1.readInt() != 0)
              bool6 = true; 
            interceptStopLockTask(bool6);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            list3 = getPersistentApp();
            param1Parcel2.writeNoException();
            param1Parcel2.writeList(list3);
            return true;
          case 24:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            list3 = list3.createStringArrayList();
            removePersistentApp(list3);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            list3 = list3.createStringArrayList();
            addPersistentApp(list3);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            bool2 = isAllowControlAppRun();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 21:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            bool6 = bool3;
            if (list3.readInt() != 0)
              bool6 = true; 
            setAllowControlAppRun(bool6);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            bool2 = removeAllAppAlarmWhiteList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 19:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            list3 = list3.createStringArrayList();
            bool2 = removeAppAlarmWhiteList(list3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 18:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            list3 = getAppAlarmWhiteList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list3);
            return true;
          case 17:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            list3 = list3.createStringArrayList();
            addAppAlarmWhiteList(list3);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            bool2 = isAllowTrustedAppStore();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 15:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            bool6 = bool4;
            if (list3.readInt() != 0)
              bool6 = true; 
            setAllowTrustedAppStore(bool6);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            list3 = getTrustedAppStore();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list3);
            return true;
          case 13:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            bool2 = isTrustedAppStoreEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 12:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            bool6 = bool5;
            if (list3.readInt() != 0)
              bool6 = true; 
            enableTrustedAppStore(bool6);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            list3.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            str2 = list3.readString();
            deleteTrustedAppStore(str2);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            str2.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            str2 = str2.readString();
            addTrustedAppStore(str2);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str2.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            list2 = getDisallowedRunningApp();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list2);
            return true;
          case 8:
            list2.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            list2 = list2.createStringArrayList();
            removeDisallowedRunningApp(list2);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            list2.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            list2 = list2.createStringArrayList();
            addDisallowedRunningApp(list2);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            list2.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            cleanBackgroundProcess();
            param1Parcel2.writeNoException();
            return true;
          case 5:
            list2.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            str1 = list2.readString();
            killApplicationProcess(str1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            str1 = getTopAppPackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 3:
            str1.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            arrayList1 = str1.createStringArrayList();
            bool2 = forceStopPackage(arrayList1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 2:
            arrayList1.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
            list1 = getDisabledAppList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 1:
            break;
        } 
        list1.enforceInterface("android.os.customize.IOplusCustomizeApplicationManagerService");
        ArrayList<String> arrayList2 = list1.createStringArrayList();
        int i = list1.readInt();
        boolean bool1 = setDisabledAppList(arrayList2, i);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.os.customize.IOplusCustomizeApplicationManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusCustomizeApplicationManagerService {
      public static IOplusCustomizeApplicationManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizeApplicationManagerService";
      }
      
      public boolean setDisabledAppList(List<String> param2List, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          parcel1.writeStringList(param2List);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().setDisabledAppList(param2List, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getDisabledAppList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().getDisabledAppList(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean forceStopPackage(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().forceStopPackage(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getTopAppPackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().getTopAppPackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void killApplicationProcess(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().killApplicationProcess(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cleanBackgroundProcess() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().cleanBackgroundProcess();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addDisallowedRunningApp(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().addDisallowedRunningApp(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeDisallowedRunningApp(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().removeDisallowedRunningApp(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getDisallowedRunningApp() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().getDisallowedRunningApp(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addTrustedAppStore(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().addTrustedAppStore(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteTrustedAppStore(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().deleteTrustedAppStore(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableTrustedAppStore(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().enableTrustedAppStore(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTrustedAppStoreEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().isTrustedAppStoreEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getTrustedAppStore() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().getTrustedAppStore(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAllowTrustedAppStore(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().setAllowTrustedAppStore(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAllowTrustedAppStore() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().isAllowTrustedAppStore();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addAppAlarmWhiteList(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().addAppAlarmWhiteList(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAppAlarmWhiteList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().getAppAlarmWhiteList(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeAppAlarmWhiteList(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(19, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().removeAppAlarmWhiteList(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeAllAppAlarmWhiteList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(20, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().removeAllAppAlarmWhiteList();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAllowControlAppRun(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().setAllowControlAppRun(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAllowControlAppRun() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(22, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().isAllowControlAppRun();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addPersistentApp(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().addPersistentApp(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removePersistentApp(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().removePersistentApp(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List getPersistentApp() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().getPersistentApp(); 
          parcel2.readException();
          ClassLoader classLoader = getClass().getClassLoader();
          return parcel2.readArrayList(classLoader);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void interceptStopLockTask(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().interceptStopLockTask(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getStopLockTaskAvailability() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeApplicationManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(27, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeApplicationManagerService.Stub.getDefaultImpl().getStopLockTaskAvailability();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizeApplicationManagerService param1IOplusCustomizeApplicationManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizeApplicationManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizeApplicationManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizeApplicationManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
