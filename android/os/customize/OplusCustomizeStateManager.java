package android.os.customize;

import android.content.Context;
import android.os.RemoteException;
import android.util.Slog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OplusCustomizeStateManager {
  private static final String SERVICE_NAME = "OplusCustomizeStateManagerService";
  
  private static final String TAG = "OplusCustomizeStateManager";
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizeStateManager sInstance;
  
  private IOplusCustomizeStateManagerService mOplusCustomizeStateManagerService;
  
  private OplusCustomizeStateManager() {
    getOplusCustomizeStateManagerService();
  }
  
  public static final OplusCustomizeStateManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizeStateManager oplusCustomizeStateManager = new OplusCustomizeStateManager();
          this();
          sInstance = oplusCustomizeStateManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusCustomizeStateManagerService getOplusCustomizeStateManagerService() {
    synchronized (mServiceLock) {
      if (this.mOplusCustomizeStateManagerService == null)
        this.mOplusCustomizeStateManagerService = IOplusCustomizeStateManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizeStateManagerService")); 
      if (this.mOplusCustomizeStateManagerService == null)
        Slog.e("OplusCustomizeStateManager", "mOplusCustomizeStateManagerService is null"); 
      return this.mOplusCustomizeStateManagerService;
    } 
  }
  
  public List<String> getRunningApplication() {
    List<?> list;
    ArrayList arrayList = new ArrayList();
    try {
      IOplusCustomizeStateManagerService iOplusCustomizeStateManagerService = getOplusCustomizeStateManagerService();
      list = arrayList;
      if (iOplusCustomizeStateManagerService != null)
        List<String> list1 = iOplusCustomizeStateManagerService.getRunningApplication(); 
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeStateManager", "getRunningApplication fail!", (Throwable)remoteException);
      list = arrayList;
    } 
    if (list == null)
      list = Collections.emptyList(); 
    return (List)list;
  }
  
  public String[] getDeviceState() {
    RemoteException remoteException1 = null;
    String[] arrayOfString = null;
    try {
      IOplusCustomizeStateManagerService iOplusCustomizeStateManagerService = getOplusCustomizeStateManagerService();
      if (iOplusCustomizeStateManagerService != null)
        arrayOfString = iOplusCustomizeStateManagerService.getDeviceState(); 
    } catch (RemoteException remoteException2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getDeviceState fail!");
      stringBuilder.append(remoteException2);
      Slog.e("OplusCustomizeStateManager", stringBuilder.toString());
      remoteException2 = remoteException1;
    } 
    return (String[])remoteException2;
  }
  
  public List<String> getAppRuntimeExceptionInfo() {
    RemoteException remoteException1 = null;
    List<String> list = null;
    try {
      IOplusCustomizeStateManagerService iOplusCustomizeStateManagerService = getOplusCustomizeStateManagerService();
      if (iOplusCustomizeStateManagerService != null)
        list = iOplusCustomizeStateManagerService.getAppRuntimeExceptionInfo(); 
    } catch (RemoteException remoteException2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAppRuntimeExceptionInfo error!");
      stringBuilder.append(remoteException2);
      Slog.e("OplusCustomizeStateManager", stringBuilder.toString());
      remoteException2 = remoteException1;
    } 
    return (List<String>)remoteException2;
  }
  
  public boolean getSystemIntegrity() {
    boolean bool = false;
    boolean bool1 = false;
    try {
      IOplusCustomizeStateManagerService iOplusCustomizeStateManagerService = getOplusCustomizeStateManagerService();
      if (iOplusCustomizeStateManagerService != null)
        bool1 = iOplusCustomizeStateManagerService.getSystemIntegrity(); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSystemIntegrity error!");
      stringBuilder.append(remoteException);
      Slog.e("OplusCustomizeStateManager", stringBuilder.toString());
      bool1 = bool;
    } 
    return bool1;
  }
  
  public void setScreenOnStatus(boolean paramBoolean) {
    try {
      IOplusCustomizeStateManagerService iOplusCustomizeStateManagerService = getOplusCustomizeStateManagerService();
      if (iOplusCustomizeStateManagerService != null)
        iOplusCustomizeStateManagerService.setScreenOnStatus(paramBoolean); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setScreenOnStatus error!");
      stringBuilder.append(remoteException);
      Slog.e("OplusCustomizeStateManager", stringBuilder.toString());
    } 
  }
  
  public boolean getScreenOnStatus() {
    boolean bool = false;
    boolean bool1 = false;
    try {
      IOplusCustomizeStateManagerService iOplusCustomizeStateManagerService = getOplusCustomizeStateManagerService();
      if (iOplusCustomizeStateManagerService != null)
        bool1 = iOplusCustomizeStateManagerService.getScreenOnStatus(); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getScreenOnStatus error!");
      stringBuilder.append(remoteException);
      Slog.e("OplusCustomizeStateManager", stringBuilder.toString());
      bool1 = bool;
    } 
    return bool1;
  }
}
