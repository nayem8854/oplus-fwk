package android.os.customize;

import android.content.ComponentName;
import android.content.Context;
import android.util.Slog;

public class OplusCustomizeSettingsManager {
  private static final String SERVICE_NAME = "OplusCustomizeSettingsManagerService";
  
  private static final String TAG = "OplusCustomizeSettingsManager";
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizeSettingsManager sInstance;
  
  private IOplusCustomizeSettingsManagerService mOplusCustomizeSettingsManagerService;
  
  private OplusCustomizeSettingsManager() {
    getOplusCustomizeSettingsManagerService();
  }
  
  public static final OplusCustomizeSettingsManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizeSettingsManager oplusCustomizeSettingsManager = new OplusCustomizeSettingsManager();
          this();
          sInstance = oplusCustomizeSettingsManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusCustomizeSettingsManagerService getOplusCustomizeSettingsManagerService() {
    synchronized (mServiceLock) {
      if (this.mOplusCustomizeSettingsManagerService == null)
        this.mOplusCustomizeSettingsManagerService = IOplusCustomizeSettingsManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizeSettingsManagerService")); 
      return this.mOplusCustomizeSettingsManagerService;
    } 
  }
  
  public boolean turnOnProtectEyes(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      return getOplusCustomizeSettingsManagerService().turnOnProtectEyes(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeSettingsManager", "turnOnProtectEyes fail!", exception);
      return false;
    } 
  }
  
  public boolean isProtectEyesOn(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeSettingsManagerService().isProtectEyesOn(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeSettingsManager", "isProtectEyesOn fail!", exception);
      return false;
    } 
  }
  
  public boolean setSIMLockDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      return getOplusCustomizeSettingsManagerService().setSIMLockDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeSettingsManager", "setSIMLockDisabled fail!", exception);
      return false;
    } 
  }
  
  public boolean isSIMLockDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeSettingsManagerService().isSIMLockDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeSettingsManager", "isSIMLockDisabled fail!", exception);
      return false;
    } 
  }
  
  public boolean setBackupRestoreDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      return getOplusCustomizeSettingsManagerService().setBackupRestoreDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeSettingsManager", "setBackupRestoreDisabled fail!", exception);
      return false;
    } 
  }
  
  public boolean isBackupRestoreDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeSettingsManagerService().isBackupRestoreDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeSettingsManager", "isBackupRestoreDisabled fail!", exception);
      return false;
    } 
  }
  
  public boolean setAutoScreenOffTime(ComponentName paramComponentName, long paramLong) {
    try {
      return getOplusCustomizeSettingsManagerService().setAutoScreenOffTime(paramComponentName, paramLong);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setAutoScreenOffTime error!");
      stringBuilder.append(exception);
      Slog.i("OplusCustomizeSettingsManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public long getAutoScreenOffTime(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeSettingsManagerService().getAutoScreenOffTime(paramComponentName);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAutoScreenOffTime error!");
      stringBuilder.append(exception);
      Slog.i("OplusCustomizeSettingsManager", stringBuilder.toString());
      return 0L;
    } 
  }
  
  public boolean isScreenOffTimeSetByPolicy(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeSettingsManagerService().isScreenOffTimeSetByPolicy(paramComponentName);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAutoScreenOffTime error!");
      stringBuilder.append(exception);
      Slog.i("OplusCustomizeSettingsManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public void storeLastManualScreenOffTimeout(ComponentName paramComponentName, int paramInt) {
    try {
      getOplusCustomizeSettingsManagerService().storeLastManualScreenOffTimeout(paramComponentName, paramInt);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAutoScreenOffTime error!");
      stringBuilder.append(exception);
      Slog.i("OplusCustomizeSettingsManager", stringBuilder.toString());
    } 
  }
  
  public boolean setSearchIndexDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      return getOplusCustomizeSettingsManagerService().setSearchIndexDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeSettingsManager", "setSearchIndexDisabled error!", exception);
      return false;
    } 
  }
  
  public boolean isSearchIndexDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeSettingsManagerService().isSearchIndexDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeSettingsManager", "isSearchIndexDisabled error!", exception);
      return false;
    } 
  }
  
  public String getRomVersion(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeSettingsManagerService().getRomVersion(paramComponentName);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getRomVersion error!");
      stringBuilder.append(exception);
      Slog.i("OplusCustomizeSettingsManager", stringBuilder.toString());
      return null;
    } 
  }
  
  public boolean setRestoreFactoryDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    boolean bool = false;
    try {
      paramBoolean = getOplusCustomizeSettingsManagerService().setRestoreFactoryDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setRestoreFactoryDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSettingsManager", stringBuilder.toString());
      paramBoolean = bool;
    } 
    return paramBoolean;
  }
  
  public boolean isRestoreFactoryDisabled(ComponentName paramComponentName) {
    boolean bool2, bool1 = false;
    try {
      bool2 = getOplusCustomizeSettingsManagerService().isRestoreFactoryDisabled(paramComponentName);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isRestoreFactoryDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSettingsManager", stringBuilder.toString());
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public boolean setDevelopmentOptionsDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    boolean bool = false;
    try {
      paramBoolean = getOplusCustomizeSettingsManagerService().setDevelopmentOptionsDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setDevelopmentOptionsDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSettingsManager", stringBuilder.toString());
      paramBoolean = bool;
    } 
    return paramBoolean;
  }
  
  public boolean isDeveloperOptionsDisabled(ComponentName paramComponentName) {
    boolean bool = false;
    try {
      boolean bool1 = getOplusCustomizeSettingsManagerService().isDeveloperOptionsDisabled(paramComponentName);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isDeveloperOptionsDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSettingsManager", stringBuilder.toString());
    } 
    return bool;
  }
  
  public boolean setTimeAndDateSetDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    boolean bool = false;
    try {
      paramBoolean = getOplusCustomizeSettingsManagerService().setTimeAndDateSetDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setTimeAndDateSetDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSettingsManager", stringBuilder.toString());
      paramBoolean = bool;
    } 
    return paramBoolean;
  }
  
  public boolean isTimeAndDateSetDisabled(ComponentName paramComponentName) {
    boolean bool = false;
    try {
      boolean bool1 = getOplusCustomizeSettingsManagerService().isTimeAndDateSetDisabled(paramComponentName);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isTimeAndDateSetDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSettingsManager", stringBuilder.toString());
    } 
    return bool;
  }
  
  public boolean setInterceptAllNotifications(boolean paramBoolean) {
    try {
      return getOplusCustomizeSettingsManagerService().setInterceptAllNotifications(paramBoolean);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setInterceptAllNotifications fail!");
      stringBuilder.append(exception.getMessage());
      Slog.e("OplusCustomizeSettingsManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean shouldInterceptAllNotifications() {
    try {
      return getOplusCustomizeSettingsManagerService().shouldInterceptAllNotifications();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("shouldInterceptAllNotifications fail!");
      stringBuilder.append(exception.getMessage());
      Slog.e("OplusCustomizeSettingsManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean setInterceptNonSystemNotifications(boolean paramBoolean) {
    try {
      return getOplusCustomizeSettingsManagerService().setInterceptNonSystemNotifications(paramBoolean);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setInterceptNonSystemNotifications fail!");
      stringBuilder.append(exception.getMessage());
      Slog.e("OplusCustomizeSettingsManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean shouldInterceptNonSystemNotifications() {
    try {
      return getOplusCustomizeSettingsManagerService().shouldInterceptNonSystemNotifications();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("shouldInterceptNonSystemNotifications fail!");
      stringBuilder.append(exception.getMessage());
      Slog.e("OplusCustomizeSettingsManager", stringBuilder.toString());
      return false;
    } 
  }
}
