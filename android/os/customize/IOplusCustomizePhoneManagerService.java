package android.os.customize;

import android.content.ComponentName;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusCustomizePhoneManagerService extends IInterface {
  void answerRingingCall() throws RemoteException;
  
  void endCall(ComponentName paramComponentName) throws RemoteException;
  
  int getDefaultVoiceCard(ComponentName paramComponentName) throws RemoteException;
  
  boolean getPropSetNonEmergencyCallDisabled() throws RemoteException;
  
  int getSlot1SmsLimitation(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  String getSlot1SmsReceiveDisabled() throws RemoteException;
  
  String getSlot1SmsSendDisabled() throws RemoteException;
  
  int getSlot2SmsLimitation(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  String getSlot2SmsReceiveDisabled() throws RemoteException;
  
  String getSlot2SmsSendDisabled() throws RemoteException;
  
  boolean isCallForwardSettingDisabled() throws RemoteException;
  
  boolean isEnablePhoneCallLimit(boolean paramBoolean) throws RemoteException;
  
  boolean isInComingThirdCallDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isRoamingCallDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isSlotTwoDisabled() throws RemoteException;
  
  boolean propEnablePhoneCallLimit(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  int propGetPhoneCallLimitation(boolean paramBoolean) throws RemoteException;
  
  boolean propRemoveCallLimitation(boolean paramBoolean) throws RemoteException;
  
  boolean propSetCallForwardSettingDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean propSetCallLimitTime(boolean paramBoolean, int paramInt) throws RemoteException;
  
  boolean propSetNonEmergencyCallDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean propSetPhoneCallLimitation(boolean paramBoolean, int paramInt) throws RemoteException;
  
  void removeSlot1SmsLimitation(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void removeSlot2SmsLimitation(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void removeSmsLimitation(ComponentName paramComponentName) throws RemoteException;
  
  Bundle setDefaultVoiceCard(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  boolean setIncomingThirdCallDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setRoamingCallDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setSlot1SmsLimitation(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  void setSlot1SmsReceiveDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setSlot1SmsSendDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setSlot2SmsLimitation(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  void setSlot2SmsReceiveDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setSlot2SmsSendDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setSlotTwoDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean setVoiceIncomingDisabledforSlot1(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setVoiceIncomingDisabledforSlot2(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setVoiceOutgoingDisabledforSlot1(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setVoiceOutgoingDisabledforSlot2(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  int showSlot1SmsTimes(boolean paramBoolean) throws RemoteException;
  
  int showSlot2SmsTimes(boolean paramBoolean) throws RemoteException;
  
  void storeSlot1SmsTimes(String paramString, boolean paramBoolean) throws RemoteException;
  
  void storeSlot2SmsTimes(String paramString, boolean paramBoolean) throws RemoteException;
  
  class Default implements IOplusCustomizePhoneManagerService {
    public boolean propSetNonEmergencyCallDisabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean getPropSetNonEmergencyCallDisabled() throws RemoteException {
      return false;
    }
    
    public boolean propSetCallForwardSettingDisabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isCallForwardSettingDisabled() throws RemoteException {
      return false;
    }
    
    public boolean propEnablePhoneCallLimit(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {
      return false;
    }
    
    public boolean isEnablePhoneCallLimit(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean propSetPhoneCallLimitation(boolean param1Boolean, int param1Int) throws RemoteException {
      return false;
    }
    
    public int propGetPhoneCallLimitation(boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public boolean propRemoveCallLimitation(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean propSetCallLimitTime(boolean param1Boolean, int param1Int) throws RemoteException {
      return false;
    }
    
    public void setSlot1SmsSendDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public void setSlot1SmsReceiveDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public void setSlot2SmsSendDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public void setSlot2SmsReceiveDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public void setSlot1SmsLimitation(ComponentName param1ComponentName, int param1Int) throws RemoteException {}
    
    public void setSlot2SmsLimitation(ComponentName param1ComponentName, int param1Int) throws RemoteException {}
    
    public void removeSmsLimitation(ComponentName param1ComponentName) throws RemoteException {}
    
    public int getSlot1SmsLimitation(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public int getSlot2SmsLimitation(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public void removeSlot1SmsLimitation(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public void removeSlot2SmsLimitation(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public String getSlot1SmsReceiveDisabled() throws RemoteException {
      return null;
    }
    
    public String getSlot2SmsReceiveDisabled() throws RemoteException {
      return null;
    }
    
    public String getSlot1SmsSendDisabled() throws RemoteException {
      return null;
    }
    
    public String getSlot2SmsSendDisabled() throws RemoteException {
      return null;
    }
    
    public int showSlot1SmsTimes(boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public void storeSlot1SmsTimes(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public int showSlot2SmsTimes(boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public void storeSlot2SmsTimes(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void endCall(ComponentName param1ComponentName) throws RemoteException {}
    
    public boolean setVoiceIncomingDisabledforSlot1(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean setVoiceOutgoingDisabledforSlot1(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean setVoiceIncomingDisabledforSlot2(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean setVoiceOutgoingDisabledforSlot2(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean setRoamingCallDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isRoamingCallDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setIncomingThirdCallDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isInComingThirdCallDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public Bundle setDefaultVoiceCard(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return null;
    }
    
    public int getDefaultVoiceCard(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public void setSlotTwoDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isSlotTwoDisabled() throws RemoteException {
      return false;
    }
    
    public void answerRingingCall() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizePhoneManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizePhoneManagerService";
    
    static final int TRANSACTION_answerRingingCall = 43;
    
    static final int TRANSACTION_endCall = 30;
    
    static final int TRANSACTION_getDefaultVoiceCard = 40;
    
    static final int TRANSACTION_getPropSetNonEmergencyCallDisabled = 2;
    
    static final int TRANSACTION_getSlot1SmsLimitation = 18;
    
    static final int TRANSACTION_getSlot1SmsReceiveDisabled = 22;
    
    static final int TRANSACTION_getSlot1SmsSendDisabled = 24;
    
    static final int TRANSACTION_getSlot2SmsLimitation = 19;
    
    static final int TRANSACTION_getSlot2SmsReceiveDisabled = 23;
    
    static final int TRANSACTION_getSlot2SmsSendDisabled = 25;
    
    static final int TRANSACTION_isCallForwardSettingDisabled = 4;
    
    static final int TRANSACTION_isEnablePhoneCallLimit = 6;
    
    static final int TRANSACTION_isInComingThirdCallDisabled = 38;
    
    static final int TRANSACTION_isRoamingCallDisabled = 36;
    
    static final int TRANSACTION_isSlotTwoDisabled = 42;
    
    static final int TRANSACTION_propEnablePhoneCallLimit = 5;
    
    static final int TRANSACTION_propGetPhoneCallLimitation = 8;
    
    static final int TRANSACTION_propRemoveCallLimitation = 9;
    
    static final int TRANSACTION_propSetCallForwardSettingDisabled = 3;
    
    static final int TRANSACTION_propSetCallLimitTime = 10;
    
    static final int TRANSACTION_propSetNonEmergencyCallDisabled = 1;
    
    static final int TRANSACTION_propSetPhoneCallLimitation = 7;
    
    static final int TRANSACTION_removeSlot1SmsLimitation = 20;
    
    static final int TRANSACTION_removeSlot2SmsLimitation = 21;
    
    static final int TRANSACTION_removeSmsLimitation = 17;
    
    static final int TRANSACTION_setDefaultVoiceCard = 39;
    
    static final int TRANSACTION_setIncomingThirdCallDisabled = 37;
    
    static final int TRANSACTION_setRoamingCallDisabled = 35;
    
    static final int TRANSACTION_setSlot1SmsLimitation = 15;
    
    static final int TRANSACTION_setSlot1SmsReceiveDisabled = 12;
    
    static final int TRANSACTION_setSlot1SmsSendDisabled = 11;
    
    static final int TRANSACTION_setSlot2SmsLimitation = 16;
    
    static final int TRANSACTION_setSlot2SmsReceiveDisabled = 14;
    
    static final int TRANSACTION_setSlot2SmsSendDisabled = 13;
    
    static final int TRANSACTION_setSlotTwoDisabled = 41;
    
    static final int TRANSACTION_setVoiceIncomingDisabledforSlot1 = 31;
    
    static final int TRANSACTION_setVoiceIncomingDisabledforSlot2 = 33;
    
    static final int TRANSACTION_setVoiceOutgoingDisabledforSlot1 = 32;
    
    static final int TRANSACTION_setVoiceOutgoingDisabledforSlot2 = 34;
    
    static final int TRANSACTION_showSlot1SmsTimes = 26;
    
    static final int TRANSACTION_showSlot2SmsTimes = 28;
    
    static final int TRANSACTION_storeSlot1SmsTimes = 27;
    
    static final int TRANSACTION_storeSlot2SmsTimes = 29;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizePhoneManagerService");
    }
    
    public static IOplusCustomizePhoneManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizePhoneManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizePhoneManagerService)
        return (IOplusCustomizePhoneManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 43:
          return "answerRingingCall";
        case 42:
          return "isSlotTwoDisabled";
        case 41:
          return "setSlotTwoDisabled";
        case 40:
          return "getDefaultVoiceCard";
        case 39:
          return "setDefaultVoiceCard";
        case 38:
          return "isInComingThirdCallDisabled";
        case 37:
          return "setIncomingThirdCallDisabled";
        case 36:
          return "isRoamingCallDisabled";
        case 35:
          return "setRoamingCallDisabled";
        case 34:
          return "setVoiceOutgoingDisabledforSlot2";
        case 33:
          return "setVoiceIncomingDisabledforSlot2";
        case 32:
          return "setVoiceOutgoingDisabledforSlot1";
        case 31:
          return "setVoiceIncomingDisabledforSlot1";
        case 30:
          return "endCall";
        case 29:
          return "storeSlot2SmsTimes";
        case 28:
          return "showSlot2SmsTimes";
        case 27:
          return "storeSlot1SmsTimes";
        case 26:
          return "showSlot1SmsTimes";
        case 25:
          return "getSlot2SmsSendDisabled";
        case 24:
          return "getSlot1SmsSendDisabled";
        case 23:
          return "getSlot2SmsReceiveDisabled";
        case 22:
          return "getSlot1SmsReceiveDisabled";
        case 21:
          return "removeSlot2SmsLimitation";
        case 20:
          return "removeSlot1SmsLimitation";
        case 19:
          return "getSlot2SmsLimitation";
        case 18:
          return "getSlot1SmsLimitation";
        case 17:
          return "removeSmsLimitation";
        case 16:
          return "setSlot2SmsLimitation";
        case 15:
          return "setSlot1SmsLimitation";
        case 14:
          return "setSlot2SmsReceiveDisabled";
        case 13:
          return "setSlot2SmsSendDisabled";
        case 12:
          return "setSlot1SmsReceiveDisabled";
        case 11:
          return "setSlot1SmsSendDisabled";
        case 10:
          return "propSetCallLimitTime";
        case 9:
          return "propRemoveCallLimitation";
        case 8:
          return "propGetPhoneCallLimitation";
        case 7:
          return "propSetPhoneCallLimitation";
        case 6:
          return "isEnablePhoneCallLimit";
        case 5:
          return "propEnablePhoneCallLimit";
        case 4:
          return "isCallForwardSettingDisabled";
        case 3:
          return "propSetCallForwardSettingDisabled";
        case 2:
          return "getPropSetNonEmergencyCallDisabled";
        case 1:
          break;
      } 
      return "propSetNonEmergencyCallDisabled";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        Bundle bundle;
        String str1;
        ComponentName componentName;
        String str2;
        boolean bool5 = false, bool6 = false, bool7 = false, bool8 = false, bool9 = false, bool10 = false, bool11 = false, bool12 = false, bool13 = false, bool14 = false, bool15 = false, bool16 = false, bool17 = false, bool18 = false, bool19 = false, bool20 = false, bool21 = false, bool22 = false, bool23 = false, bool24 = false, bool25 = false, bool26 = false, bool27 = false, bool28 = false, bool29 = false, bool30 = false, bool31 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 43:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            answerRingingCall();
            param1Parcel2.writeNoException();
            return true;
          case 42:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            bool4 = isSlotTwoDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 41:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            bool10 = bool31;
            if (param1Parcel1.readInt() != 0)
              bool10 = true; 
            setSlotTwoDisabled(bool10);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            k = getDefaultVoiceCard((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 39:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            k = param1Parcel1.readInt();
            bundle = setDefaultVoiceCard(componentName, k);
            param1Parcel2.writeNoException();
            if (bundle != null) {
              param1Parcel2.writeInt(1);
              bundle.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 38:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (bundle.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            bool3 = isInComingThirdCallDisabled((ComponentName)bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 37:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            bool10 = bool5;
            if (bundle.readInt() != 0)
              bool10 = true; 
            bool3 = setIncomingThirdCallDisabled(componentName, bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 36:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (bundle.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            bool3 = isRoamingCallDisabled((ComponentName)bundle);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 35:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            bool10 = bool6;
            if (bundle.readInt() != 0)
              bool10 = true; 
            bool3 = setRoamingCallDisabled(componentName, bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 34:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            bool10 = bool7;
            if (bundle.readInt() != 0)
              bool10 = true; 
            bool3 = setVoiceOutgoingDisabledforSlot2(componentName, bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 33:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            bool10 = bool8;
            if (bundle.readInt() != 0)
              bool10 = true; 
            bool3 = setVoiceIncomingDisabledforSlot2(componentName, bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 32:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            bool10 = bool9;
            if (bundle.readInt() != 0)
              bool10 = true; 
            bool3 = setVoiceOutgoingDisabledforSlot1(componentName, bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 31:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (bundle.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              componentName = null;
            } 
            if (bundle.readInt() != 0)
              bool10 = true; 
            bool3 = setVoiceIncomingDisabledforSlot1(componentName, bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 30:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (bundle.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            endCall((ComponentName)bundle);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            str2 = bundle.readString();
            bool10 = bool11;
            if (bundle.readInt() != 0)
              bool10 = true; 
            storeSlot2SmsTimes(str2, bool10);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            bool10 = bool12;
            if (bundle.readInt() != 0)
              bool10 = true; 
            j = showSlot2SmsTimes(bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 27:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            str2 = bundle.readString();
            bool10 = bool13;
            if (bundle.readInt() != 0)
              bool10 = true; 
            storeSlot1SmsTimes(str2, bool10);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            bool10 = bool14;
            if (bundle.readInt() != 0)
              bool10 = true; 
            j = showSlot1SmsTimes(bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 25:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            str1 = getSlot2SmsSendDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 24:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            str1 = getSlot1SmsSendDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 23:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            str1 = getSlot2SmsReceiveDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 22:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            str1 = getSlot1SmsReceiveDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 21:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            bool10 = bool15;
            if (str1.readInt() != 0)
              bool10 = true; 
            removeSlot2SmsLimitation((ComponentName)str2, bool10);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            bool10 = bool16;
            if (str1.readInt() != 0)
              bool10 = true; 
            removeSlot1SmsLimitation((ComponentName)str2, bool10);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            bool10 = bool17;
            if (str1.readInt() != 0)
              bool10 = true; 
            j = getSlot2SmsLimitation((ComponentName)str2, bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 18:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            bool10 = bool18;
            if (str1.readInt() != 0)
              bool10 = true; 
            j = getSlot1SmsLimitation((ComponentName)str2, bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 17:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            removeSmsLimitation((ComponentName)str1);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            j = str1.readInt();
            setSlot2SmsLimitation((ComponentName)str2, j);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            j = str1.readInt();
            setSlot1SmsLimitation((ComponentName)str2, j);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            bool10 = bool19;
            if (str1.readInt() != 0)
              bool10 = true; 
            setSlot2SmsReceiveDisabled((ComponentName)str2, bool10);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            bool10 = bool20;
            if (str1.readInt() != 0)
              bool10 = true; 
            setSlot2SmsSendDisabled((ComponentName)str2, bool10);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            bool10 = bool21;
            if (str1.readInt() != 0)
              bool10 = true; 
            setSlot1SmsReceiveDisabled((ComponentName)str2, bool10);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str2 = null;
            } 
            bool10 = bool22;
            if (str1.readInt() != 0)
              bool10 = true; 
            setSlot1SmsSendDisabled((ComponentName)str2, bool10);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            bool10 = bool23;
            if (str1.readInt() != 0)
              bool10 = true; 
            j = str1.readInt();
            bool2 = propSetCallLimitTime(bool10, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 9:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            bool10 = bool24;
            if (str1.readInt() != 0)
              bool10 = true; 
            bool2 = propRemoveCallLimitation(bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 8:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            bool10 = bool25;
            if (str1.readInt() != 0)
              bool10 = true; 
            i = propGetPhoneCallLimitation(bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 7:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            bool10 = bool26;
            if (str1.readInt() != 0)
              bool10 = true; 
            i = str1.readInt();
            bool1 = propSetPhoneCallLimitation(bool10, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 6:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            bool10 = bool27;
            if (str1.readInt() != 0)
              bool10 = true; 
            bool1 = isEnablePhoneCallLimit(bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 5:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            if (str1.readInt() != 0) {
              bool10 = true;
            } else {
              bool10 = false;
            } 
            if (str1.readInt() != 0)
              bool28 = true; 
            bool1 = propEnablePhoneCallLimit(bool10, bool28);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 4:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            bool1 = isCallForwardSettingDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            bool10 = bool29;
            if (str1.readInt() != 0)
              bool10 = true; 
            bool1 = propSetCallForwardSettingDisabled(bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
            bool1 = getPropSetNonEmergencyCallDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.os.customize.IOplusCustomizePhoneManagerService");
        bool10 = bool30;
        if (str1.readInt() != 0)
          bool10 = true; 
        boolean bool1 = propSetNonEmergencyCallDisabled(bool10);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.os.customize.IOplusCustomizePhoneManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusCustomizePhoneManagerService {
      public static IOplusCustomizePhoneManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizePhoneManagerService";
      }
      
      public boolean propSetNonEmergencyCallDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().propSetNonEmergencyCallDisabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getPropSetNonEmergencyCallDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().getPropSetNonEmergencyCallDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean propSetCallForwardSettingDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().propSetCallForwardSettingDisabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCallForwardSettingDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().isCallForwardSettingDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean propEnablePhoneCallLimit(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2Boolean1) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          if (param2Boolean2) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean1 = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().propEnablePhoneCallLimit(param2Boolean1, param2Boolean2);
            return param2Boolean1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean1 = bool;
          } else {
            param2Boolean1 = false;
          } 
          return param2Boolean1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isEnablePhoneCallLimit(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().isEnablePhoneCallLimit(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean propSetPhoneCallLimitation(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().propSetPhoneCallLimitation(param2Boolean, param2Int);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int propGetPhoneCallLimitation(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            i = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().propGetPhoneCallLimitation(param2Boolean);
            return i;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean propRemoveCallLimitation(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().propRemoveCallLimitation(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean propSetCallLimitTime(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().propSetCallLimitTime(param2Boolean, param2Int);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSlot1SmsSendDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setSlot1SmsSendDisabled(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSlot1SmsReceiveDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setSlot1SmsReceiveDisabled(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSlot2SmsSendDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setSlot2SmsSendDisabled(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSlot2SmsReceiveDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setSlot2SmsReceiveDisabled(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSlot1SmsLimitation(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setSlot1SmsLimitation(param2ComponentName, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSlot2SmsLimitation(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setSlot2SmsLimitation(param2ComponentName, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeSmsLimitation(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().removeSmsLimitation(param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSlot1SmsLimitation(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          int i = 1;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            i = 0; 
          parcel1.writeInt(i);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            i = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().getSlot1SmsLimitation(param2ComponentName, param2Boolean);
            return i;
          } 
          parcel2.readException();
          i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSlot2SmsLimitation(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          int i = 1;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            i = 0; 
          parcel1.writeInt(i);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            i = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().getSlot2SmsLimitation(param2ComponentName, param2Boolean);
            return i;
          } 
          parcel2.readException();
          i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeSlot1SmsLimitation(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().removeSlot1SmsLimitation(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeSlot2SmsLimitation(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().removeSlot2SmsLimitation(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSlot1SmsReceiveDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().getSlot1SmsReceiveDisabled(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSlot2SmsReceiveDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().getSlot2SmsReceiveDisabled(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSlot1SmsSendDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().getSlot1SmsSendDisabled(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSlot2SmsSendDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().getSlot2SmsSendDisabled(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int showSlot1SmsTimes(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            i = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().showSlot1SmsTimes(param2Boolean);
            return i;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void storeSlot1SmsTimes(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().storeSlot1SmsTimes(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int showSlot2SmsTimes(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            i = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().showSlot2SmsTimes(param2Boolean);
            return i;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void storeSlot2SmsTimes(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().storeSlot2SmsTimes(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void endCall(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().endCall(param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setVoiceIncomingDisabledforSlot1(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setVoiceIncomingDisabledforSlot1(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setVoiceOutgoingDisabledforSlot1(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setVoiceOutgoingDisabledforSlot1(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setVoiceIncomingDisabledforSlot2(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setVoiceIncomingDisabledforSlot2(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setVoiceOutgoingDisabledforSlot2(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setVoiceOutgoingDisabledforSlot2(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setRoamingCallDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setRoamingCallDisabled(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRoamingCallDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().isRoamingCallDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setIncomingThirdCallDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setIncomingThirdCallDisabled(param2ComponentName, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInComingThirdCallDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().isInComingThirdCallDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle setDefaultVoiceCard(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setDefaultVoiceCard(param2ComponentName, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bundle bundle = Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2ComponentName = null;
          } 
          return (Bundle)param2ComponentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDefaultVoiceCard(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().getDefaultVoiceCard(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSlotTwoDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().setSlotTwoDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSlotTwoDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(42, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().isSlotTwoDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void answerRingingCall() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePhoneManagerService");
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePhoneManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePhoneManagerService.Stub.getDefaultImpl().answerRingingCall();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizePhoneManagerService param1IOplusCustomizePhoneManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizePhoneManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizePhoneManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizePhoneManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
