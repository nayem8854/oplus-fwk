package android.os.customize;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IOplusCustomizeControlerManagerService extends IInterface {
  void addAccessibilityServiceToWhiteList(List<String> paramList) throws RemoteException;
  
  void deleteAccessibilityServiceWhiteList() throws RemoteException;
  
  void disableAccessibilityService(ComponentName paramComponentName) throws RemoteException;
  
  void enableAccessibilityService(ComponentName paramComponentName) throws RemoteException;
  
  boolean formatSDCard(String paramString) throws RemoteException;
  
  List<ComponentName> getAccessibilityService() throws RemoteException;
  
  List<String> getAccessibilityServiceWhiteList() throws RemoteException;
  
  boolean getAirplaneMode(ComponentName paramComponentName) throws RemoteException;
  
  String getEnabledAccessibilityServicesName() throws RemoteException;
  
  boolean isAccessibilityServiceEnabled() throws RemoteException;
  
  boolean isDisableKeyguardForgetPassword() throws RemoteException;
  
  boolean isDisabledKeyguardPolicy(String paramString) throws RemoteException;
  
  void rebootDevice() throws RemoteException;
  
  void removeAccessibilityServiceFromWhiteList(List<String> paramList) throws RemoteException;
  
  void setAirplaneMode(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setCustomSettingsMenu(ComponentName paramComponentName, List<String> paramList) throws RemoteException;
  
  boolean setDisableKeyguardForgetPassword(boolean paramBoolean) throws RemoteException;
  
  void setDisabledKeyguardPolicy(boolean paramBoolean, String paramString) throws RemoteException;
  
  boolean setSysTime(ComponentName paramComponentName, long paramLong) throws RemoteException;
  
  void shutdownDevice() throws RemoteException;
  
  void wipeDeviceData() throws RemoteException;
  
  class Default implements IOplusCustomizeControlerManagerService {
    public void shutdownDevice() throws RemoteException {}
    
    public void rebootDevice() throws RemoteException {}
    
    public void wipeDeviceData() throws RemoteException {}
    
    public boolean formatSDCard(String param1String) throws RemoteException {
      return false;
    }
    
    public void enableAccessibilityService(ComponentName param1ComponentName) throws RemoteException {}
    
    public void disableAccessibilityService(ComponentName param1ComponentName) throws RemoteException {}
    
    public boolean isAccessibilityServiceEnabled() throws RemoteException {
      return false;
    }
    
    public List<ComponentName> getAccessibilityService() throws RemoteException {
      return null;
    }
    
    public String getEnabledAccessibilityServicesName() throws RemoteException {
      return null;
    }
    
    public void addAccessibilityServiceToWhiteList(List<String> param1List) throws RemoteException {}
    
    public void removeAccessibilityServiceFromWhiteList(List<String> param1List) throws RemoteException {}
    
    public List<String> getAccessibilityServiceWhiteList() throws RemoteException {
      return null;
    }
    
    public void deleteAccessibilityServiceWhiteList() throws RemoteException {}
    
    public boolean setDisableKeyguardForgetPassword(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isDisableKeyguardForgetPassword() throws RemoteException {
      return false;
    }
    
    public void setDisabledKeyguardPolicy(boolean param1Boolean, String param1String) throws RemoteException {}
    
    public boolean isDisabledKeyguardPolicy(String param1String) throws RemoteException {
      return false;
    }
    
    public void setAirplaneMode(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean getAirplaneMode(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setSysTime(ComponentName param1ComponentName, long param1Long) throws RemoteException {
      return false;
    }
    
    public void setCustomSettingsMenu(ComponentName param1ComponentName, List<String> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizeControlerManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizeControlerManagerService";
    
    static final int TRANSACTION_addAccessibilityServiceToWhiteList = 10;
    
    static final int TRANSACTION_deleteAccessibilityServiceWhiteList = 13;
    
    static final int TRANSACTION_disableAccessibilityService = 6;
    
    static final int TRANSACTION_enableAccessibilityService = 5;
    
    static final int TRANSACTION_formatSDCard = 4;
    
    static final int TRANSACTION_getAccessibilityService = 8;
    
    static final int TRANSACTION_getAccessibilityServiceWhiteList = 12;
    
    static final int TRANSACTION_getAirplaneMode = 19;
    
    static final int TRANSACTION_getEnabledAccessibilityServicesName = 9;
    
    static final int TRANSACTION_isAccessibilityServiceEnabled = 7;
    
    static final int TRANSACTION_isDisableKeyguardForgetPassword = 15;
    
    static final int TRANSACTION_isDisabledKeyguardPolicy = 17;
    
    static final int TRANSACTION_rebootDevice = 2;
    
    static final int TRANSACTION_removeAccessibilityServiceFromWhiteList = 11;
    
    static final int TRANSACTION_setAirplaneMode = 18;
    
    static final int TRANSACTION_setCustomSettingsMenu = 21;
    
    static final int TRANSACTION_setDisableKeyguardForgetPassword = 14;
    
    static final int TRANSACTION_setDisabledKeyguardPolicy = 16;
    
    static final int TRANSACTION_setSysTime = 20;
    
    static final int TRANSACTION_shutdownDevice = 1;
    
    static final int TRANSACTION_wipeDeviceData = 3;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizeControlerManagerService");
    }
    
    public static IOplusCustomizeControlerManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizeControlerManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizeControlerManagerService)
        return (IOplusCustomizeControlerManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 21:
          return "setCustomSettingsMenu";
        case 20:
          return "setSysTime";
        case 19:
          return "getAirplaneMode";
        case 18:
          return "setAirplaneMode";
        case 17:
          return "isDisabledKeyguardPolicy";
        case 16:
          return "setDisabledKeyguardPolicy";
        case 15:
          return "isDisableKeyguardForgetPassword";
        case 14:
          return "setDisableKeyguardForgetPassword";
        case 13:
          return "deleteAccessibilityServiceWhiteList";
        case 12:
          return "getAccessibilityServiceWhiteList";
        case 11:
          return "removeAccessibilityServiceFromWhiteList";
        case 10:
          return "addAccessibilityServiceToWhiteList";
        case 9:
          return "getEnabledAccessibilityServicesName";
        case 8:
          return "getAccessibilityService";
        case 7:
          return "isAccessibilityServiceEnabled";
        case 6:
          return "disableAccessibilityService";
        case 5:
          return "enableAccessibilityService";
        case 4:
          return "formatSDCard";
        case 3:
          return "wipeDeviceData";
        case 2:
          return "rebootDevice";
        case 1:
          break;
      } 
      return "shutdownDevice";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        ArrayList<String> arrayList;
        String str3;
        List<String> list1;
        String str2;
        List<ComponentName> list;
        String str1;
        ComponentName componentName;
        long l;
        boolean bool1 = false, bool2 = false, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 21:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            arrayList = param1Parcel1.createStringArrayList();
            setCustomSettingsMenu(componentName, arrayList);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            arrayList.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            if (arrayList.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              componentName = null;
            } 
            l = arrayList.readLong();
            bool = setSysTime(componentName, l);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 19:
            arrayList.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            if (arrayList.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            bool = getAirplaneMode((ComponentName)arrayList);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 18:
            arrayList.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            if (arrayList.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              componentName = null;
            } 
            bool2 = bool3;
            if (arrayList.readInt() != 0)
              bool2 = true; 
            setAirplaneMode(componentName, bool2);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            arrayList.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            str3 = arrayList.readString();
            bool = isDisabledKeyguardPolicy(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 16:
            str3.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            bool2 = bool1;
            if (str3.readInt() != 0)
              bool2 = true; 
            str3 = str3.readString();
            setDisabledKeyguardPolicy(bool2, str3);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str3.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            bool = isDisableKeyguardForgetPassword();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 14:
            str3.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            if (str3.readInt() != 0)
              bool2 = true; 
            bool = setDisableKeyguardForgetPassword(bool2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 13:
            str3.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            deleteAccessibilityServiceWhiteList();
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str3.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            list1 = getAccessibilityServiceWhiteList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 11:
            list1.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            list1 = list1.createStringArrayList();
            removeAccessibilityServiceFromWhiteList(list1);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            list1.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            list1 = list1.createStringArrayList();
            addAccessibilityServiceToWhiteList(list1);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            list1.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            str2 = getEnabledAccessibilityServicesName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 8:
            str2.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            list = getAccessibilityService();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 7:
            list.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            bool = isAccessibilityServiceEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 6:
            list.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            if (list.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            disableAccessibilityService((ComponentName)list);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            list.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            if (list.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            enableAccessibilityService((ComponentName)list);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            list.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            str1 = list.readString();
            bool = formatSDCard(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 3:
            str1.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            wipeDeviceData();
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
            rebootDevice();
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.os.customize.IOplusCustomizeControlerManagerService");
        shutdownDevice();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.os.customize.IOplusCustomizeControlerManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusCustomizeControlerManagerService {
      public static IOplusCustomizeControlerManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizeControlerManagerService";
      }
      
      public void shutdownDevice() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().shutdownDevice();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void rebootDevice() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().rebootDevice();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void wipeDeviceData() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().wipeDeviceData();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean formatSDCard(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().formatSDCard(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableAccessibilityService(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().enableAccessibilityService(param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableAccessibilityService(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().disableAccessibilityService(param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAccessibilityServiceEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().isAccessibilityServiceEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ComponentName> getAccessibilityService() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().getAccessibilityService(); 
          parcel2.readException();
          return (List)parcel2.createTypedArrayList(ComponentName.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getEnabledAccessibilityServicesName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().getEnabledAccessibilityServicesName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addAccessibilityServiceToWhiteList(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().addAccessibilityServiceToWhiteList(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeAccessibilityServiceFromWhiteList(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().removeAccessibilityServiceFromWhiteList(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAccessibilityServiceWhiteList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().getAccessibilityServiceWhiteList(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteAccessibilityServiceWhiteList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().deleteAccessibilityServiceWhiteList();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDisableKeyguardForgetPassword(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().setDisableKeyguardForgetPassword(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDisableKeyguardForgetPassword() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(15, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().isDisableKeyguardForgetPassword();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDisabledKeyguardPolicy(boolean param2Boolean, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeString(param2String);
          boolean bool1 = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().setDisabledKeyguardPolicy(param2Boolean, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDisabledKeyguardPolicy(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().isDisabledKeyguardPolicy(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAirplaneMode(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().setAirplaneMode(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getAirplaneMode(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().getAirplaneMode(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSysTime(ComponentName param2ComponentName, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeLong(param2Long);
          boolean bool2 = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().setSysTime(param2ComponentName, param2Long);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCustomSettingsMenu(ComponentName param2ComponentName, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeControlerManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeControlerManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeControlerManagerService.Stub.getDefaultImpl().setCustomSettingsMenu(param2ComponentName, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizeControlerManagerService param1IOplusCustomizeControlerManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizeControlerManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizeControlerManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizeControlerManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
