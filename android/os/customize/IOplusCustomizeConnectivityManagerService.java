package android.os.customize;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IOplusCustomizeConnectivityManagerService extends IInterface {
  boolean addBluetoothDevicesToBlackLists(List<String> paramList) throws RemoteException;
  
  boolean addBluetoothDevicesToWhiteLists(List<String> paramList) throws RemoteException;
  
  List<String> getBluetoothDevicesFromBlackLists() throws RemoteException;
  
  List<String> getBluetoothDevicesFromWhiteLists() throws RemoteException;
  
  int getBluetoothPolicies() throws RemoteException;
  
  String getDevicePosition(ComponentName paramComponentName) throws RemoteException;
  
  int getSecurityLevel() throws RemoteException;
  
  int getWifiApPolicies() throws RemoteException;
  
  List<String> getWifiRestrictionList(String paramString) throws RemoteException;
  
  List<String> getWlanApClientBlackList() throws RemoteException;
  
  List<String> getWlanApClientWhiteList() throws RemoteException;
  
  int getWlanPolicies(ComponentName paramComponentName) throws RemoteException;
  
  boolean isBlackListedDevice(String paramString) throws RemoteException;
  
  boolean isGPSDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean isGPSTurnOn(ComponentName paramComponentName) throws RemoteException;
  
  boolean isUnSecureSoftApDisabled() throws RemoteException;
  
  boolean isUserProfilesDisabled() throws RemoteException;
  
  boolean isWhiteListedDevice(String paramString) throws RemoteException;
  
  boolean isWifiAutoConnectionDisabled() throws RemoteException;
  
  boolean isWifiEditDisabled() throws RemoteException;
  
  boolean isWlanForceDisabled() throws RemoteException;
  
  boolean isWlanForceEnabled() throws RemoteException;
  
  boolean removeBluetoothDevicesFromBlackLists(List<String> paramList) throws RemoteException;
  
  boolean removeBluetoothDevicesFromWhiteLists(List<String> paramList) throws RemoteException;
  
  boolean removeFromRestrictionList(List<String> paramList, String paramString) throws RemoteException;
  
  boolean removeWlanApClientBlackList(List<String> paramList) throws RemoteException;
  
  boolean setBluetoothPolicies(int paramInt) throws RemoteException;
  
  void setGPSDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  boolean setSecurityLevel(int paramInt) throws RemoteException;
  
  boolean setUnSecureSoftApDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean setUserProfilesDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean setWifiApPolicies(int paramInt) throws RemoteException;
  
  boolean setWifiAutoConnectionDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean setWifiEditDisabled(boolean paramBoolean) throws RemoteException;
  
  boolean setWifiRestrictionList(List<String> paramList, String paramString) throws RemoteException;
  
  boolean setWlanApClientBlackList(List<String> paramList) throws RemoteException;
  
  boolean setWlanPolicies(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  void turnOnGPS(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  class Default implements IOplusCustomizeConnectivityManagerService {
    public boolean setUserProfilesDisabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isUserProfilesDisabled() throws RemoteException {
      return false;
    }
    
    public int getBluetoothPolicies() throws RemoteException {
      return 0;
    }
    
    public boolean setBluetoothPolicies(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean addBluetoothDevicesToWhiteLists(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public boolean isWhiteListedDevice(String param1String) throws RemoteException {
      return false;
    }
    
    public List<String> getBluetoothDevicesFromWhiteLists() throws RemoteException {
      return null;
    }
    
    public boolean removeBluetoothDevicesFromWhiteLists(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public boolean addBluetoothDevicesToBlackLists(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public boolean isBlackListedDevice(String param1String) throws RemoteException {
      return false;
    }
    
    public List<String> getBluetoothDevicesFromBlackLists() throws RemoteException {
      return null;
    }
    
    public boolean removeBluetoothDevicesFromBlackLists(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public boolean setWifiEditDisabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isWifiEditDisabled() throws RemoteException {
      return false;
    }
    
    public boolean setWifiRestrictionList(List<String> param1List, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean removeFromRestrictionList(List<String> param1List, String param1String) throws RemoteException {
      return false;
    }
    
    public List<String> getWifiRestrictionList(String param1String) throws RemoteException {
      return null;
    }
    
    public boolean setWifiAutoConnectionDisabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isWifiAutoConnectionDisabled() throws RemoteException {
      return false;
    }
    
    public boolean setSecurityLevel(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getSecurityLevel() throws RemoteException {
      return 0;
    }
    
    public boolean setWifiApPolicies(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getWifiApPolicies() throws RemoteException {
      return 0;
    }
    
    public List<String> getWlanApClientWhiteList() throws RemoteException {
      return null;
    }
    
    public List<String> getWlanApClientBlackList() throws RemoteException {
      return null;
    }
    
    public boolean setWlanApClientBlackList(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public boolean removeWlanApClientBlackList(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public void turnOnGPS(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean isGPSTurnOn(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public void setGPSDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean isGPSDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public String getDevicePosition(ComponentName param1ComponentName) throws RemoteException {
      return null;
    }
    
    public boolean setUnSecureSoftApDisabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isUnSecureSoftApDisabled() throws RemoteException {
      return false;
    }
    
    public boolean setWlanPolicies(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getWlanPolicies(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public boolean isWlanForceEnabled() throws RemoteException {
      return false;
    }
    
    public boolean isWlanForceDisabled() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizeConnectivityManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizeConnectivityManagerService";
    
    static final int TRANSACTION_addBluetoothDevicesToBlackLists = 9;
    
    static final int TRANSACTION_addBluetoothDevicesToWhiteLists = 5;
    
    static final int TRANSACTION_getBluetoothDevicesFromBlackLists = 11;
    
    static final int TRANSACTION_getBluetoothDevicesFromWhiteLists = 7;
    
    static final int TRANSACTION_getBluetoothPolicies = 3;
    
    static final int TRANSACTION_getDevicePosition = 32;
    
    static final int TRANSACTION_getSecurityLevel = 21;
    
    static final int TRANSACTION_getWifiApPolicies = 23;
    
    static final int TRANSACTION_getWifiRestrictionList = 17;
    
    static final int TRANSACTION_getWlanApClientBlackList = 25;
    
    static final int TRANSACTION_getWlanApClientWhiteList = 24;
    
    static final int TRANSACTION_getWlanPolicies = 36;
    
    static final int TRANSACTION_isBlackListedDevice = 10;
    
    static final int TRANSACTION_isGPSDisabled = 31;
    
    static final int TRANSACTION_isGPSTurnOn = 29;
    
    static final int TRANSACTION_isUnSecureSoftApDisabled = 34;
    
    static final int TRANSACTION_isUserProfilesDisabled = 2;
    
    static final int TRANSACTION_isWhiteListedDevice = 6;
    
    static final int TRANSACTION_isWifiAutoConnectionDisabled = 19;
    
    static final int TRANSACTION_isWifiEditDisabled = 14;
    
    static final int TRANSACTION_isWlanForceDisabled = 38;
    
    static final int TRANSACTION_isWlanForceEnabled = 37;
    
    static final int TRANSACTION_removeBluetoothDevicesFromBlackLists = 12;
    
    static final int TRANSACTION_removeBluetoothDevicesFromWhiteLists = 8;
    
    static final int TRANSACTION_removeFromRestrictionList = 16;
    
    static final int TRANSACTION_removeWlanApClientBlackList = 27;
    
    static final int TRANSACTION_setBluetoothPolicies = 4;
    
    static final int TRANSACTION_setGPSDisabled = 30;
    
    static final int TRANSACTION_setSecurityLevel = 20;
    
    static final int TRANSACTION_setUnSecureSoftApDisabled = 33;
    
    static final int TRANSACTION_setUserProfilesDisabled = 1;
    
    static final int TRANSACTION_setWifiApPolicies = 22;
    
    static final int TRANSACTION_setWifiAutoConnectionDisabled = 18;
    
    static final int TRANSACTION_setWifiEditDisabled = 13;
    
    static final int TRANSACTION_setWifiRestrictionList = 15;
    
    static final int TRANSACTION_setWlanApClientBlackList = 26;
    
    static final int TRANSACTION_setWlanPolicies = 35;
    
    static final int TRANSACTION_turnOnGPS = 28;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizeConnectivityManagerService");
    }
    
    public static IOplusCustomizeConnectivityManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizeConnectivityManagerService)
        return (IOplusCustomizeConnectivityManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 38:
          return "isWlanForceDisabled";
        case 37:
          return "isWlanForceEnabled";
        case 36:
          return "getWlanPolicies";
        case 35:
          return "setWlanPolicies";
        case 34:
          return "isUnSecureSoftApDisabled";
        case 33:
          return "setUnSecureSoftApDisabled";
        case 32:
          return "getDevicePosition";
        case 31:
          return "isGPSDisabled";
        case 30:
          return "setGPSDisabled";
        case 29:
          return "isGPSTurnOn";
        case 28:
          return "turnOnGPS";
        case 27:
          return "removeWlanApClientBlackList";
        case 26:
          return "setWlanApClientBlackList";
        case 25:
          return "getWlanApClientBlackList";
        case 24:
          return "getWlanApClientWhiteList";
        case 23:
          return "getWifiApPolicies";
        case 22:
          return "setWifiApPolicies";
        case 21:
          return "getSecurityLevel";
        case 20:
          return "setSecurityLevel";
        case 19:
          return "isWifiAutoConnectionDisabled";
        case 18:
          return "setWifiAutoConnectionDisabled";
        case 17:
          return "getWifiRestrictionList";
        case 16:
          return "removeFromRestrictionList";
        case 15:
          return "setWifiRestrictionList";
        case 14:
          return "isWifiEditDisabled";
        case 13:
          return "setWifiEditDisabled";
        case 12:
          return "removeBluetoothDevicesFromBlackLists";
        case 11:
          return "getBluetoothDevicesFromBlackLists";
        case 10:
          return "isBlackListedDevice";
        case 9:
          return "addBluetoothDevicesToBlackLists";
        case 8:
          return "removeBluetoothDevicesFromWhiteLists";
        case 7:
          return "getBluetoothDevicesFromWhiteLists";
        case 6:
          return "isWhiteListedDevice";
        case 5:
          return "addBluetoothDevicesToWhiteLists";
        case 4:
          return "setBluetoothPolicies";
        case 3:
          return "getBluetoothPolicies";
        case 2:
          return "isUserProfilesDisabled";
        case 1:
          break;
      } 
      return "setUserProfilesDisabled";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool6;
        int n;
        boolean bool5;
        int m;
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        String str5;
        ArrayList<String> arrayList4;
        List<String> list4;
        String str4;
        List<String> list3;
        String str3;
        ArrayList<String> arrayList3;
        List<String> list2;
        String str2;
        ArrayList<String> arrayList2;
        List<String> list1;
        String str1;
        ArrayList<String> arrayList1;
        ComponentName componentName;
        ArrayList<String> arrayList5;
        boolean bool7 = false, bool8 = false, bool9 = false, bool10 = false, bool11 = false, bool12 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 38:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            bool6 = isWlanForceDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 37:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            bool6 = isWlanForceEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 36:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            n = getWlanPolicies((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(n);
            return true;
          case 35:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            if (param1Parcel1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              componentName = null;
            } 
            n = param1Parcel1.readInt();
            bool5 = setWlanPolicies(componentName, n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 34:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            bool5 = isUnSecureSoftApDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 33:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            bool9 = bool12;
            if (param1Parcel1.readInt() != 0)
              bool9 = true; 
            bool5 = setUnSecureSoftApDisabled(bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 32:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            str5 = getDevicePosition((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str5);
            return true;
          case 31:
            str5.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            if (str5.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            bool5 = isGPSDisabled((ComponentName)str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 30:
            str5.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            if (str5.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str5);
            } else {
              componentName = null;
            } 
            bool9 = bool7;
            if (str5.readInt() != 0)
              bool9 = true; 
            setGPSDisabled(componentName, bool9);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            str5.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            if (str5.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            bool5 = isGPSTurnOn((ComponentName)str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 28:
            str5.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            if (str5.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str5);
            } else {
              componentName = null;
            } 
            bool9 = bool8;
            if (str5.readInt() != 0)
              bool9 = true; 
            turnOnGPS(componentName, bool9);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            str5.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            arrayList4 = str5.createStringArrayList();
            bool5 = removeWlanApClientBlackList(arrayList4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 26:
            arrayList4.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            arrayList4 = arrayList4.createStringArrayList();
            bool5 = setWlanApClientBlackList(arrayList4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 25:
            arrayList4.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            list4 = getWlanApClientBlackList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list4);
            return true;
          case 24:
            list4.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            list4 = getWlanApClientWhiteList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list4);
            return true;
          case 23:
            list4.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            m = getWifiApPolicies();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 22:
            list4.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            m = list4.readInt();
            bool4 = setWifiApPolicies(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 21:
            list4.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            k = getSecurityLevel();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 20:
            list4.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            k = list4.readInt();
            bool3 = setSecurityLevel(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 19:
            list4.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            bool3 = isWifiAutoConnectionDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 18:
            list4.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            if (list4.readInt() != 0)
              bool9 = true; 
            bool3 = setWifiAutoConnectionDisabled(bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 17:
            list4.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            str4 = list4.readString();
            list3 = getWifiRestrictionList(str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list3);
            return true;
          case 16:
            list3.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            arrayList5 = list3.createStringArrayList();
            str3 = list3.readString();
            bool3 = removeFromRestrictionList(arrayList5, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 15:
            str3.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            arrayList5 = str3.createStringArrayList();
            str3 = str3.readString();
            bool3 = setWifiRestrictionList(arrayList5, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 14:
            str3.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            bool3 = isWifiEditDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 13:
            str3.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            bool9 = bool10;
            if (str3.readInt() != 0)
              bool9 = true; 
            bool3 = setWifiEditDisabled(bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 12:
            str3.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            arrayList3 = str3.createStringArrayList();
            bool3 = removeBluetoothDevicesFromBlackLists(arrayList3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 11:
            arrayList3.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            list2 = getBluetoothDevicesFromBlackLists();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list2);
            return true;
          case 10:
            list2.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            str2 = list2.readString();
            bool3 = isBlackListedDevice(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 9:
            str2.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            arrayList2 = str2.createStringArrayList();
            bool3 = addBluetoothDevicesToBlackLists(arrayList2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 8:
            arrayList2.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            arrayList2 = arrayList2.createStringArrayList();
            bool3 = removeBluetoothDevicesFromWhiteLists(arrayList2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 7:
            arrayList2.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            list1 = getBluetoothDevicesFromWhiteLists();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 6:
            list1.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            str1 = list1.readString();
            bool3 = isWhiteListedDevice(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 5:
            str1.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            arrayList1 = str1.createStringArrayList();
            bool3 = addBluetoothDevicesToWhiteLists(arrayList1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 4:
            arrayList1.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            j = arrayList1.readInt();
            bool2 = setBluetoothPolicies(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 3:
            arrayList1.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            i = getBluetoothPolicies();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 2:
            arrayList1.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
            bool1 = isUserProfilesDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        arrayList1.enforceInterface("android.os.customize.IOplusCustomizeConnectivityManagerService");
        bool9 = bool11;
        if (arrayList1.readInt() != 0)
          bool9 = true; 
        boolean bool1 = setUserProfilesDisabled(bool9);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.os.customize.IOplusCustomizeConnectivityManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusCustomizeConnectivityManagerService {
      public static IOplusCustomizeConnectivityManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizeConnectivityManagerService";
      }
      
      public boolean setUserProfilesDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().setUserProfilesDisabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUserProfilesDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().isUserProfilesDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getBluetoothPolicies() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().getBluetoothPolicies(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setBluetoothPolicies(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().setBluetoothPolicies(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addBluetoothDevicesToWhiteLists(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().addBluetoothDevicesToWhiteLists(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWhiteListedDevice(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().isWhiteListedDevice(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getBluetoothDevicesFromWhiteLists() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().getBluetoothDevicesFromWhiteLists(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeBluetoothDevicesFromWhiteLists(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().removeBluetoothDevicesFromWhiteLists(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addBluetoothDevicesToBlackLists(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().addBluetoothDevicesToBlackLists(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBlackListedDevice(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().isBlackListedDevice(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getBluetoothDevicesFromBlackLists() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().getBluetoothDevicesFromBlackLists(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeBluetoothDevicesFromBlackLists(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().removeBluetoothDevicesFromBlackLists(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setWifiEditDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().setWifiEditDisabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWifiEditDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(14, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().isWifiEditDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setWifiRestrictionList(List<String> param2List, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeStringList(param2List);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(15, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().setWifiRestrictionList(param2List, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeFromRestrictionList(List<String> param2List, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeStringList(param2List);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().removeFromRestrictionList(param2List, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getWifiRestrictionList(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().getWifiRestrictionList(param2String); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setWifiAutoConnectionDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().setWifiAutoConnectionDisabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWifiAutoConnectionDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(19, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().isWifiAutoConnectionDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSecurityLevel(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(20, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().setSecurityLevel(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSecurityLevel() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().getSecurityLevel(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setWifiApPolicies(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(22, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().setWifiApPolicies(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getWifiApPolicies() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().getWifiApPolicies(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getWlanApClientWhiteList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().getWlanApClientWhiteList(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getWlanApClientBlackList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().getWlanApClientBlackList(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setWlanApClientBlackList(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(26, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().setWlanApClientBlackList(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeWlanApClientBlackList(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(27, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().removeWlanApClientBlackList(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void turnOnGPS(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().turnOnGPS(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isGPSTurnOn(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().isGPSTurnOn(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setGPSDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().setGPSDisabled(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isGPSDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().isGPSDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDevicePosition(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().getDevicePosition(param2ComponentName); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setUnSecureSoftApDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().setUnSecureSoftApDisabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUnSecureSoftApDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(34, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().isUnSecureSoftApDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setWlanPolicies(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().setWlanPolicies(param2ComponentName, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getWlanPolicies(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().getWlanPolicies(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWlanForceEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(37, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().isWlanForceEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWlanForceDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeConnectivityManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(38, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeConnectivityManagerService.Stub.getDefaultImpl().isWlanForceDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizeConnectivityManagerService param1IOplusCustomizeConnectivityManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizeConnectivityManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizeConnectivityManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizeConnectivityManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
