package android.os.customize;

import android.content.ComponentName;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Slog;
import java.util.ArrayList;
import java.util.List;

public class OplusCustomizeSecurityManager {
  private static final String SERVICE_NAME = "OplusCustomizeSecurityManagerService";
  
  private static final String TAG = "OplusCustomizeSecurityManager";
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizeSecurityManager sInstance;
  
  private IOplusCustomizeSecurityManagerService mOplusCustomizeSecurityManagerService;
  
  private OplusCustomizeSecurityManager() {
    getOplusCustomizeSecurityManagerService();
  }
  
  public static final OplusCustomizeSecurityManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizeSecurityManager oplusCustomizeSecurityManager = new OplusCustomizeSecurityManager();
          this();
          sInstance = oplusCustomizeSecurityManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusCustomizeSecurityManagerService getOplusCustomizeSecurityManagerService() {
    synchronized (mServiceLock) {
      if (this.mOplusCustomizeSecurityManagerService == null)
        this.mOplusCustomizeSecurityManagerService = IOplusCustomizeSecurityManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizeSecurityManagerService")); 
      if (this.mOplusCustomizeSecurityManagerService == null)
        Slog.e("OplusCustomizeSecurityManager", "mOplusCustomizeSecurityManagerService is null"); 
      return this.mOplusCustomizeSecurityManagerService;
    } 
  }
  
  public void setEmmAdmin(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeSecurityManagerService().setEmmAdmin(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeSecurityManager", "setEmmAdmin fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setEmmAdmin Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSecurityManager", stringBuilder.toString());
    } 
  }
  
  public List<ComponentName> getEmmAdmin() {
    List<ComponentName> list = new ArrayList();
    try {
      List<ComponentName> list1 = getOplusCustomizeSecurityManagerService().getEmmAdmin();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeSecurityManager", "getEmmAdmin fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getEmmAdmin Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSecurityManager", stringBuilder.toString());
    } 
    return list;
  }
  
  public boolean setDeviceOwner(ComponentName paramComponentName) {
    if (paramComponentName != null)
      try {
        return getOplusCustomizeSecurityManagerService().setDeviceOwner(paramComponentName);
      } catch (RemoteException remoteException) {
        Slog.e("OplusCustomizeSecurityManager", "setDeviceOwner fail!", (Throwable)remoteException);
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setDeviceOwner Error");
        stringBuilder.append(exception);
        Slog.e("OplusCustomizeSecurityManager", stringBuilder.toString());
      }  
    return false;
  }
  
  public ComponentName getDeviceOwner() {
    try {
      return getOplusCustomizeSecurityManagerService().getDeviceOwner();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeSecurityManager", "getDeviceOwner fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getDeviceOwner Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSecurityManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public void clearDeviceOwner(String paramString) {
    try {
      getOplusCustomizeSecurityManagerService().clearDeviceOwner(paramString);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeSecurityManager", "clearDeviceOwner fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("clearDeviceOwner Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSecurityManager", stringBuilder.toString());
    } 
  }
  
  public void setCustomDevicePolicyEnabled(boolean paramBoolean) {
    try {
      getOplusCustomizeSecurityManagerService().setCustomDevicePolicyEnabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeSecurityManager", "setCustomDevicePolicyEnabled fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setCustomDevicePolicyEnabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSecurityManager", stringBuilder.toString());
    } 
  }
  
  public boolean isCustomDevicePolicyEnabled() {
    try {
      return getOplusCustomizeSecurityManagerService().isCustomDevicePolicyEnabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeSecurityManager", "isCustomDevicePolicyEnabled fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isCustomDevicePolicyEnabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSecurityManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public List<String> getDeviceInfo(ComponentName paramComponentName) {
    ArrayList<String> arrayList2, arrayList1 = new ArrayList();
    try {
      IOplusCustomizeSecurityManagerService iOplusCustomizeSecurityManagerService = getOplusCustomizeSecurityManagerService();
      arrayList2 = arrayList1;
      if (iOplusCustomizeSecurityManagerService != null)
        List<String> list = iOplusCustomizeSecurityManagerService.getDeviceInfo(paramComponentName); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getDeviceInfo error!");
      stringBuilder.append(remoteException);
      Slog.e("OplusCustomizeSecurityManager", stringBuilder.toString());
      arrayList2 = arrayList1;
    } 
    return arrayList2;
  }
  
  public Bitmap captureFullScreen() {
    try {
      IOplusCustomizeSecurityManagerService iOplusCustomizeSecurityManagerService = getOplusCustomizeSecurityManagerService();
      if (iOplusCustomizeSecurityManagerService != null)
        return iOplusCustomizeSecurityManagerService.captureFullScreen(); 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("captureFullScreen error!");
      stringBuilder.append(remoteException);
      Slog.e("OplusCustomizeSecurityManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public String executeShellToSetIptables(String paramString) {
    Exception exception2 = null;
    RemoteException remoteException = null;
    try {
      paramString = getOplusCustomizeSecurityManagerService().executeShellToSetIptables(paramString);
    } catch (RemoteException remoteException1) {
      Slog.e("OplusCustomizeSecurityManager", "setCustomDevicePolicyEnabled fail!", (Throwable)remoteException1);
      remoteException1 = remoteException;
    } catch (Exception exception1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setCustomDevicePolicyEnabled Error");
      stringBuilder.append(exception1);
      Slog.e("OplusCustomizeSecurityManager", stringBuilder.toString());
      exception1 = exception2;
    } 
    return (String)exception1;
  }
  
  public String getPhoneNumber(int paramInt) {
    String str = null;
    RemoteException remoteException2 = null;
    try {
      String str1 = getOplusCustomizeSecurityManagerService().getPhoneNumber(paramInt);
    } catch (RemoteException remoteException1) {
      Slog.e("OplusCustomizeSecurityManager", "getPhoneNumber fail!", (Throwable)remoteException1);
      remoteException1 = remoteException2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getPhoneNumber Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSecurityManager", stringBuilder.toString());
    } 
    return (String)remoteException1;
  }
  
  public Bundle getMobileCommSettings(ComponentName paramComponentName, String paramString1, String paramString2) {
    try {
      return getOplusCustomizeSecurityManagerService().getMobileCommSettings(paramComponentName, paramString1, paramString2);
    } catch (Exception exception) {
      Slog.d("OplusCustomizeSecurityManager", "getMobileCommSettings:err", exception);
      return null;
    } 
  }
  
  public void setMobileCommSettings(ComponentName paramComponentName, String paramString, Bundle paramBundle) {
    try {
      getOplusCustomizeSecurityManagerService().setMobileCommSettings(paramComponentName, paramString, paramBundle);
    } catch (Exception exception) {
      Slog.d("OplusCustomizeSecurityManager", "setMobileCommSettings:err", exception);
    } 
  }
  
  public boolean setTestEnvEnable(boolean paramBoolean) {
    boolean bool = false;
    try {
      paramBoolean = getOplusCustomizeSecurityManagerService().setTestEnvEnable(paramBoolean);
    } catch (Exception exception) {
      Slog.d("OplusCustomizeSecurityManager", "setTestEnvEnable:err", exception);
      paramBoolean = bool;
    } 
    return paramBoolean;
  }
  
  public boolean isTestEnvEnable() {
    boolean bool2, bool1 = false;
    try {
      bool2 = getOplusCustomizeSecurityManagerService().isTestEnvEnable();
    } catch (Exception exception) {
      Slog.d("OplusCustomizeSecurityManager", "isTestEnvEnable:err", exception);
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public boolean setDeviceLocked(ComponentName paramComponentName) {
    try {
      IOplusCustomizeSecurityManagerService iOplusCustomizeSecurityManagerService = getOplusCustomizeSecurityManagerService();
      if (iOplusCustomizeSecurityManagerService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("mdm service IDeviceSecurityManager manager:");
        stringBuilder.append(iOplusCustomizeSecurityManagerService);
        Slog.d("OplusCustomizeSecurityManager", stringBuilder.toString());
        return iOplusCustomizeSecurityManagerService.setDeviceLocked(paramComponentName);
      } 
      Slog.e("OplusCustomizeSecurityManager", "mdm service IDeviceSecurityManager manager is null");
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeSecurityManager", "setDeviceLocked error!");
      remoteException.printStackTrace();
    } 
    return false;
  }
  
  public boolean setDeviceUnLocked(ComponentName paramComponentName) {
    try {
      IOplusCustomizeSecurityManagerService iOplusCustomizeSecurityManagerService = getOplusCustomizeSecurityManagerService();
      if (iOplusCustomizeSecurityManagerService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("mdm service IDeviceSecurityManager manager:");
        stringBuilder.append(iOplusCustomizeSecurityManagerService);
        Slog.d("OplusCustomizeSecurityManager", stringBuilder.toString());
        return iOplusCustomizeSecurityManagerService.setDeviceUnLocked(paramComponentName);
      } 
      Slog.e("OplusCustomizeSecurityManager", "mdm service IDeviceSecurityManager manager is null");
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeSecurityManager", "setDeviceUnLocked error!");
      remoteException.printStackTrace();
    } 
    return false;
  }
  
  public boolean needLockDeadByMdm() {
    try {
      IOplusCustomizeSecurityManagerService iOplusCustomizeSecurityManagerService = getOplusCustomizeSecurityManagerService();
      if (iOplusCustomizeSecurityManagerService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("mdm service IDeviceSecurityManager manager:");
        stringBuilder.append(iOplusCustomizeSecurityManagerService);
        Slog.d("OplusCustomizeSecurityManager", stringBuilder.toString());
        return iOplusCustomizeSecurityManagerService.needLockDeadByMdm();
      } 
      Slog.e("OplusCustomizeSecurityManager", "mdm service IDeviceSecurityManager manager is null");
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeSecurityManager", "needLockDeadByMdm error!");
      remoteException.printStackTrace();
    } 
    return false;
  }
  
  public boolean enableThirdRecord(boolean paramBoolean) {
    try {
      return getOplusCustomizeSecurityManagerService().enableThirdRecord(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeSecurityManager", "enableThirdRecord Error");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("enableThirdRecord Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSecurityManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean isEnableThirdRecord() {
    try {
      return getOplusCustomizeSecurityManagerService().isEnableThirdRecord();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeSecurityManager", "isEnableThirdRecord Error");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isEnableThirdRecord Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeSecurityManager", stringBuilder.toString());
      return false;
    } 
  }
}
