package android.os.customize.util;

import android.app.ActivityThread;
import android.os.Process;
import android.os.SystemProperties;

public class OplusCustomizeScreenCaptureHelper {
  private static final String FORBCAP_SCREEN_ENABLE_PROPERTY_KEY = "persist.sys.oplus.customize.forbcap";
  
  private static final String TAG = "SCREEN_CAPTURE";
  
  public static boolean isForbidCaptureScreen() {
    String str = ActivityThread.currentPackageName();
    return isForbidCaptureScreen(str);
  }
  
  public static boolean isForbidCaptureScreen(String paramString) {
    boolean bool = getScreenCaptureDisabled();
    boolean bool1 = false;
    if (bool) {
      if ("com.heytap.appplatform".equals(paramString))
        return true; 
      if (Process.myUid() == 1000 || 
        "com.android.systemui".equals(paramString)) {
        bool1 = false;
        return bool1;
      } 
      bool1 = true;
    } 
    return bool1;
  }
  
  public static void setScreenCaptureDisabled(boolean paramBoolean) {
    String str;
    if (paramBoolean) {
      str = "true";
    } else {
      str = "false";
    } 
    SystemProperties.set("persist.sys.oplus.customize.forbcap", str);
  }
  
  public static boolean getScreenCaptureDisabled() {
    return SystemProperties.getBoolean("persist.sys.oplus.customize.forbcap", false);
  }
}
