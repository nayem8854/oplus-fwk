package android.os.customize;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IOplusCustomizeNetworkManagerService extends IInterface {
  void addAppMeteredDataBlackList(List<String> paramList) throws RemoteException;
  
  void addAppWlanDataBlackList(List<String> paramList) throws RemoteException;
  
  void addNetworkRestriction(int paramInt, List<String> paramList) throws RemoteException;
  
  List<String> getAppMeteredDataBlackList() throws RemoteException;
  
  List<String> getAppWlanDataBlackList() throws RemoteException;
  
  List<String> getNetworkRestrictionList(int paramInt) throws RemoteException;
  
  int getUserApnMgrPolicies() throws RemoteException;
  
  void removeAppMeteredDataBlackList(List<String> paramList) throws RemoteException;
  
  void removeAppWlanDataBlackList(List<String> paramList) throws RemoteException;
  
  void removeNetworkRestriction(int paramInt, List<String> paramList) throws RemoteException;
  
  void removeNetworkRestrictionAll(int paramInt) throws RemoteException;
  
  void setNetworkRestriction(int paramInt) throws RemoteException;
  
  void setUserApnMgrPolicies(int paramInt) throws RemoteException;
  
  class Default implements IOplusCustomizeNetworkManagerService {
    public void setNetworkRestriction(int param1Int) throws RemoteException {}
    
    public void addNetworkRestriction(int param1Int, List<String> param1List) throws RemoteException {}
    
    public void removeNetworkRestriction(int param1Int, List<String> param1List) throws RemoteException {}
    
    public void removeNetworkRestrictionAll(int param1Int) throws RemoteException {}
    
    public List<String> getNetworkRestrictionList(int param1Int) throws RemoteException {
      return null;
    }
    
    public void setUserApnMgrPolicies(int param1Int) throws RemoteException {}
    
    public int getUserApnMgrPolicies() throws RemoteException {
      return 0;
    }
    
    public void addAppMeteredDataBlackList(List<String> param1List) throws RemoteException {}
    
    public void addAppWlanDataBlackList(List<String> param1List) throws RemoteException {}
    
    public void removeAppMeteredDataBlackList(List<String> param1List) throws RemoteException {}
    
    public void removeAppWlanDataBlackList(List<String> param1List) throws RemoteException {}
    
    public List<String> getAppMeteredDataBlackList() throws RemoteException {
      return null;
    }
    
    public List<String> getAppWlanDataBlackList() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizeNetworkManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizeNetworkManagerService";
    
    static final int TRANSACTION_addAppMeteredDataBlackList = 8;
    
    static final int TRANSACTION_addAppWlanDataBlackList = 9;
    
    static final int TRANSACTION_addNetworkRestriction = 2;
    
    static final int TRANSACTION_getAppMeteredDataBlackList = 12;
    
    static final int TRANSACTION_getAppWlanDataBlackList = 13;
    
    static final int TRANSACTION_getNetworkRestrictionList = 5;
    
    static final int TRANSACTION_getUserApnMgrPolicies = 7;
    
    static final int TRANSACTION_removeAppMeteredDataBlackList = 10;
    
    static final int TRANSACTION_removeAppWlanDataBlackList = 11;
    
    static final int TRANSACTION_removeNetworkRestriction = 3;
    
    static final int TRANSACTION_removeNetworkRestrictionAll = 4;
    
    static final int TRANSACTION_setNetworkRestriction = 1;
    
    static final int TRANSACTION_setUserApnMgrPolicies = 6;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizeNetworkManagerService");
    }
    
    public static IOplusCustomizeNetworkManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizeNetworkManagerService)
        return (IOplusCustomizeNetworkManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 13:
          return "getAppWlanDataBlackList";
        case 12:
          return "getAppMeteredDataBlackList";
        case 11:
          return "removeAppWlanDataBlackList";
        case 10:
          return "removeAppMeteredDataBlackList";
        case 9:
          return "addAppWlanDataBlackList";
        case 8:
          return "addAppMeteredDataBlackList";
        case 7:
          return "getUserApnMgrPolicies";
        case 6:
          return "setUserApnMgrPolicies";
        case 5:
          return "getNetworkRestrictionList";
        case 4:
          return "removeNetworkRestrictionAll";
        case 3:
          return "removeNetworkRestriction";
        case 2:
          return "addNetworkRestriction";
        case 1:
          break;
      } 
      return "setNetworkRestriction";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        List<String> list;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 13:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
            list = getAppWlanDataBlackList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list);
            return true;
          case 12:
            list.enforceInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
            list = getAppMeteredDataBlackList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list);
            return true;
          case 11:
            list.enforceInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
            list = list.createStringArrayList();
            removeAppWlanDataBlackList(list);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            list.enforceInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
            list = list.createStringArrayList();
            removeAppMeteredDataBlackList(list);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            list.enforceInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
            list = list.createStringArrayList();
            addAppWlanDataBlackList(list);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            list.enforceInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
            list = list.createStringArrayList();
            addAppMeteredDataBlackList(list);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            list.enforceInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
            param1Int1 = getUserApnMgrPolicies();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 6:
            list.enforceInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
            param1Int1 = list.readInt();
            setUserApnMgrPolicies(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            list.enforceInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
            param1Int1 = list.readInt();
            list = getNetworkRestrictionList(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list);
            return true;
          case 4:
            list.enforceInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
            param1Int1 = list.readInt();
            removeNetworkRestrictionAll(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            list.enforceInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
            param1Int1 = list.readInt();
            list = list.createStringArrayList();
            removeNetworkRestriction(param1Int1, list);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            list.enforceInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
            param1Int1 = list.readInt();
            list = list.createStringArrayList();
            addNetworkRestriction(param1Int1, list);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        list.enforceInterface("android.os.customize.IOplusCustomizeNetworkManagerService");
        param1Int1 = list.readInt();
        setNetworkRestriction(param1Int1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.os.customize.IOplusCustomizeNetworkManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusCustomizeNetworkManagerService {
      public static IOplusCustomizeNetworkManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizeNetworkManagerService";
      }
      
      public void setNetworkRestriction(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeNetworkManagerService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl().setNetworkRestriction(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addNetworkRestriction(int param2Int, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeNetworkManagerService");
          parcel1.writeInt(param2Int);
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl().addNetworkRestriction(param2Int, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeNetworkRestriction(int param2Int, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeNetworkManagerService");
          parcel1.writeInt(param2Int);
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl().removeNetworkRestriction(param2Int, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeNetworkRestrictionAll(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeNetworkManagerService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl().removeNetworkRestrictionAll(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getNetworkRestrictionList(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeNetworkManagerService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl().getNetworkRestrictionList(param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUserApnMgrPolicies(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeNetworkManagerService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl().setUserApnMgrPolicies(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUserApnMgrPolicies() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeNetworkManagerService");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl().getUserApnMgrPolicies(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addAppMeteredDataBlackList(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeNetworkManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl().addAppMeteredDataBlackList(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addAppWlanDataBlackList(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeNetworkManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl().addAppWlanDataBlackList(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeAppMeteredDataBlackList(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeNetworkManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl().removeAppMeteredDataBlackList(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeAppWlanDataBlackList(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeNetworkManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl().removeAppWlanDataBlackList(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAppMeteredDataBlackList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeNetworkManagerService");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl().getAppMeteredDataBlackList(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAppWlanDataBlackList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeNetworkManagerService");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeNetworkManagerService.Stub.getDefaultImpl().getAppWlanDataBlackList(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizeNetworkManagerService param1IOplusCustomizeNetworkManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizeNetworkManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizeNetworkManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizeNetworkManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
