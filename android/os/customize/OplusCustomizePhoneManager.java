package android.os.customize;

import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;
import android.util.Slog;

public class OplusCustomizePhoneManager {
  private static final String SERVICE_NAME = "OplusCustomizePhoneManagerService";
  
  private static final String TAG = "OplusCustomizePhoneManager";
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizePhoneManager sInstance;
  
  private IOplusCustomizePhoneManagerService mOplusCustomizePhoneManagerService;
  
  private OplusCustomizePhoneManager() {
    getOplusCustomizePhoneManagerService();
  }
  
  public static final OplusCustomizePhoneManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizePhoneManager oplusCustomizePhoneManager = new OplusCustomizePhoneManager();
          this();
          sInstance = oplusCustomizePhoneManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusCustomizePhoneManagerService getOplusCustomizePhoneManagerService() {
    synchronized (mServiceLock) {
      if (this.mOplusCustomizePhoneManagerService == null)
        this.mOplusCustomizePhoneManagerService = IOplusCustomizePhoneManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizePhoneManagerService")); 
      return this.mOplusCustomizePhoneManagerService;
    } 
  }
  
  public boolean propSetNonEmergencyCallDisabled(boolean paramBoolean) {
    try {
      return this.mOplusCustomizePhoneManagerService.propSetNonEmergencyCallDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "propSetNonEmergencyCallDisabled Error");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("propSetNonEmergencyCallDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean getPropSetNonEmergencyCallDisabled() {
    try {
      return this.mOplusCustomizePhoneManagerService.getPropSetNonEmergencyCallDisabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "getPropSetNonEmergencyCallDisabled Error");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getPropSetNonEmergencyCallDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean propSetCallForwardSettingDisabled(boolean paramBoolean) {
    try {
      return this.mOplusCustomizePhoneManagerService.propSetCallForwardSettingDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "propSetForwardCallSettingDisabled Error");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("propSetForwardCallSettingDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean isCallForwardSettingDisabled() {
    try {
      return this.mOplusCustomizePhoneManagerService.isCallForwardSettingDisabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "isCallForwardSettingDisabled Error");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isCallForwardSettingDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean propEnablePhoneCallLimit(boolean paramBoolean1, boolean paramBoolean2) {
    try {
      return this.mOplusCustomizePhoneManagerService.propEnablePhoneCallLimit(paramBoolean1, paramBoolean2);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "propEnablePhoneCallLimit Error");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("propEnablePhoneCallLimit Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean isEnablePhoneCallLimit(boolean paramBoolean) {
    try {
      return this.mOplusCustomizePhoneManagerService.isEnablePhoneCallLimit(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "isEnablePhoneCallLimit Error");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isEnablePhoneCallLimit Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean propSetPhoneCallLimitation(boolean paramBoolean, int paramInt) {
    try {
      return this.mOplusCustomizePhoneManagerService.propSetPhoneCallLimitation(paramBoolean, paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "propSetPhoneCallLimitation Error");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("propSetPhoneCallLimitation Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public int propGetPhoneCallLimitation(boolean paramBoolean) {
    try {
      return this.mOplusCustomizePhoneManagerService.propGetPhoneCallLimitation(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "propGetPhoneCallLimitation Error");
      return 0;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("propGetPhoneCallLimitation Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
      return 0;
    } 
  }
  
  public boolean propRemoveCallLimitation(boolean paramBoolean) {
    try {
      return this.mOplusCustomizePhoneManagerService.propRemoveCallLimitation(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "propRemoveCallLimitation Error");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("propRemoveCallLimitation Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean propSetCallLimitTime(boolean paramBoolean, int paramInt) {
    try {
      return this.mOplusCustomizePhoneManagerService.propSetCallLimitTime(paramBoolean, paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "propSetCallLimitTime Error");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("propSetCallLimitTime Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public void setSlot1SmsSendDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      this.mOplusCustomizePhoneManagerService.setSlot1SmsSendDisabled(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "setSlot1SmsSendDisabled Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSlot1SmsSendDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
  }
  
  public void setSlot1SmsReceiveDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      this.mOplusCustomizePhoneManagerService.setSlot1SmsReceiveDisabled(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "setSlot1SmsReceiveDisabled Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSlot1SmsReceiveDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
  }
  
  public void setSlot2SmsSendDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      this.mOplusCustomizePhoneManagerService.setSlot2SmsSendDisabled(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "setSlot2SmsSendDisabled Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSlot2SmsSendDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
  }
  
  public void setSlot2SmsReceiveDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      this.mOplusCustomizePhoneManagerService.setSlot2SmsReceiveDisabled(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "setSlot2SmsReceiveDisabled Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSlot2SmsReceiveDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
  }
  
  public void setSlot1SmsLimitation(ComponentName paramComponentName, int paramInt) {
    try {
      this.mOplusCustomizePhoneManagerService.setSlot1SmsLimitation(paramComponentName, paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "setSlot1SmsLimitation Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSlot1SmsLimitation Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
  }
  
  public void setSlot2SmsLimitation(ComponentName paramComponentName, int paramInt) {
    try {
      this.mOplusCustomizePhoneManagerService.setSlot2SmsLimitation(paramComponentName, paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "setSlot2SmsLimitation Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSlot2SmsLimitation Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
  }
  
  public void removeSmsLimitation(ComponentName paramComponentName) {
    try {
      this.mOplusCustomizePhoneManagerService.removeSmsLimitation(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "removeSmsLimitation Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeSmsLimitation Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
  }
  
  public int getSlot1SmsLimitation(ComponentName paramComponentName, boolean paramBoolean) {
    int i = -1;
    try {
      int j = this.mOplusCustomizePhoneManagerService.getSlot1SmsLimitation(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "getSlot1SmsLimitation Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSlot1SmsLimitation Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
    return i;
  }
  
  public int getSlot2SmsLimitation(ComponentName paramComponentName, boolean paramBoolean) {
    int i = -1;
    try {
      int j = this.mOplusCustomizePhoneManagerService.getSlot2SmsLimitation(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "getSlot2SmsLimitation Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSlot2SmsLimitation Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
    return i;
  }
  
  public void removeSlot1SmsLimitation(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      this.mOplusCustomizePhoneManagerService.removeSlot1SmsLimitation(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "removeSlot1SmsLimitation Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeSlot1SmsLimitation Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
  }
  
  public void removeSlot2SmsLimitation(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      this.mOplusCustomizePhoneManagerService.removeSlot2SmsLimitation(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "removeSlot2SmsLimitation Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeSlot2SmsLimitation Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
  }
  
  public String getSlot1SmsReceiveDisabled() {
    String str1 = "1";
    String str2 = str1;
    try {
      if (this.mOplusCustomizePhoneManagerService != null)
        str2 = this.mOplusCustomizePhoneManagerService.getSlot1SmsReceiveDisabled(); 
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "getSlot1SmsReceiveDisabled error");
      str2 = str1;
    } 
    return str2;
  }
  
  public String getSlot2SmsReceiveDisabled() {
    String str1 = "1";
    String str2 = str1;
    try {
      if (this.mOplusCustomizePhoneManagerService != null)
        str2 = this.mOplusCustomizePhoneManagerService.getSlot2SmsReceiveDisabled(); 
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "getSlot2SmsReceiveDisabled error");
      str2 = str1;
    } 
    return str2;
  }
  
  public String getSlot1SmsSendDisabled() {
    String str1 = "1";
    String str2 = str1;
    try {
      if (this.mOplusCustomizePhoneManagerService != null)
        str2 = this.mOplusCustomizePhoneManagerService.getSlot1SmsSendDisabled(); 
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "getSlot1SmsSendDisabled error");
      str2 = str1;
    } 
    return str2;
  }
  
  public String getSlot2SmsSendDisabled() {
    String str1 = "1";
    String str2 = str1;
    try {
      if (this.mOplusCustomizePhoneManagerService != null)
        str2 = this.mOplusCustomizePhoneManagerService.getSlot2SmsSendDisabled(); 
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "getSlot2SmsSendDisabled error");
      str2 = str1;
    } 
    return str2;
  }
  
  public int showSlot1SmsTimes(boolean paramBoolean) {
    byte b = -1;
    int i = b;
    try {
      if (this.mOplusCustomizePhoneManagerService != null)
        i = this.mOplusCustomizePhoneManagerService.showSlot1SmsTimes(paramBoolean); 
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "showSlot1SmsTimes error");
      i = b;
    } 
    return i;
  }
  
  public void storeSlot1SmsTimes(String paramString, boolean paramBoolean) {
    try {
      this.mOplusCustomizePhoneManagerService.storeSlot1SmsTimes(paramString, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "getSlot2SmsSendDisabled Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSlot2SmsSendDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
  }
  
  public int showSlot2SmsTimes(boolean paramBoolean) {
    byte b = -1;
    int i = b;
    try {
      if (this.mOplusCustomizePhoneManagerService != null)
        i = this.mOplusCustomizePhoneManagerService.showSlot2SmsTimes(paramBoolean); 
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "showSlot2SmsTimes error");
      i = b;
    } 
    return i;
  }
  
  public void storeSlot2SmsTimes(String paramString, boolean paramBoolean) {
    try {
      this.mOplusCustomizePhoneManagerService.storeSlot2SmsTimes(paramString, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "getSlot2SmsSendDisabled Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSlot2SmsSendDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
  }
  
  public void endCall(ComponentName paramComponentName) {
    try {
      IOplusCustomizePhoneManagerService iOplusCustomizePhoneManagerService = getOplusCustomizePhoneManagerService();
      if (iOplusCustomizePhoneManagerService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("mdm service IOplusCustomizePhoneManagerService service:");
        stringBuilder.append(iOplusCustomizePhoneManagerService);
        Slog.d("OplusCustomizePhoneManager", stringBuilder.toString());
        iOplusCustomizePhoneManagerService.endCall(paramComponentName);
      } else {
        Slog.d("OplusCustomizePhoneManager", "mdm service IOplusCustomizePhoneManagerService service is null");
      } 
    } catch (RemoteException remoteException) {
      Slog.i("OplusCustomizePhoneManager", "endCall error!");
      remoteException.printStackTrace();
    } 
  }
  
  public boolean setVoiceIncomingDisabledforSlot1(ComponentName paramComponentName, boolean paramBoolean) {
    boolean bool1 = false, bool2 = false;
    try {
      IOplusCustomizePhoneManagerService iOplusCustomizePhoneManagerService = getOplusCustomizePhoneManagerService();
      if (iOplusCustomizePhoneManagerService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("IOplusCustomizePhoneManagerService, service:");
        stringBuilder.append(iOplusCustomizePhoneManagerService);
        Slog.d("OplusCustomizePhoneManager", stringBuilder.toString());
        paramBoolean = iOplusCustomizePhoneManagerService.setVoiceIncomingDisabledforSlot1(paramComponentName, paramBoolean);
      } else {
        Slog.d("OplusCustomizePhoneManager", "IOplusCustomizePhoneManagerService service is null");
        paramBoolean = bool2;
      } 
    } catch (RemoteException remoteException) {
      Slog.i("OplusCustomizePhoneManager", "setVoiceIncomingDisabledforSlot1 error!");
      remoteException.printStackTrace();
      paramBoolean = bool1;
    } 
    return paramBoolean;
  }
  
  public boolean setVoiceOutgoingDisabledforSlot1(ComponentName paramComponentName, boolean paramBoolean) {
    boolean bool1 = false, bool2 = false;
    try {
      IOplusCustomizePhoneManagerService iOplusCustomizePhoneManagerService = getOplusCustomizePhoneManagerService();
      if (iOplusCustomizePhoneManagerService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("IOplusCustomizePhoneManagerService, service:");
        stringBuilder.append(iOplusCustomizePhoneManagerService);
        Slog.d("OplusCustomizePhoneManager", stringBuilder.toString());
        paramBoolean = iOplusCustomizePhoneManagerService.setVoiceOutgoingDisabledforSlot1(paramComponentName, paramBoolean);
      } else {
        Slog.d("OplusCustomizePhoneManager", "IOplusCustomizePhoneManagerService service is null");
        paramBoolean = bool2;
      } 
    } catch (RemoteException remoteException) {
      Slog.i("OplusCustomizePhoneManager", "setVoiceIncomingDisabledforSlot1 error!");
      remoteException.printStackTrace();
      paramBoolean = bool1;
    } 
    return paramBoolean;
  }
  
  public boolean setVoiceIncomingDisabledforSlot2(ComponentName paramComponentName, boolean paramBoolean) {
    boolean bool1 = false, bool2 = false;
    try {
      IOplusCustomizePhoneManagerService iOplusCustomizePhoneManagerService = getOplusCustomizePhoneManagerService();
      if (iOplusCustomizePhoneManagerService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("IOplusCustomizePhoneManagerService, service:");
        stringBuilder.append(iOplusCustomizePhoneManagerService);
        Slog.d("OplusCustomizePhoneManager", stringBuilder.toString());
        paramBoolean = iOplusCustomizePhoneManagerService.setVoiceIncomingDisabledforSlot2(paramComponentName, paramBoolean);
      } else {
        Slog.d("OplusCustomizePhoneManager", "IOplusCustomizePhoneManagerService service is null");
        paramBoolean = bool2;
      } 
    } catch (RemoteException remoteException) {
      Slog.i("OplusCustomizePhoneManager", "setVoiceIncomingDisabledforSlot2 error!");
      remoteException.printStackTrace();
      paramBoolean = bool1;
    } 
    return paramBoolean;
  }
  
  public boolean setVoiceOutgoingDisabledforSlot2(ComponentName paramComponentName, boolean paramBoolean) {
    boolean bool1 = false, bool2 = false;
    try {
      IOplusCustomizePhoneManagerService iOplusCustomizePhoneManagerService = getOplusCustomizePhoneManagerService();
      if (iOplusCustomizePhoneManagerService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("IOplusCustomizePhoneManagerService, service:");
        stringBuilder.append(iOplusCustomizePhoneManagerService);
        Slog.d("OplusCustomizePhoneManager", stringBuilder.toString());
        paramBoolean = iOplusCustomizePhoneManagerService.setVoiceOutgoingDisabledforSlot2(paramComponentName, paramBoolean);
      } else {
        Slog.d("OplusCustomizePhoneManager", "IOplusCustomizePhoneManagerService service is null");
        paramBoolean = bool2;
      } 
    } catch (RemoteException remoteException) {
      Slog.i("OplusCustomizePhoneManager", "setVoiceOutgoingDisabledforSlot2 error!");
      remoteException.printStackTrace();
      paramBoolean = bool1;
    } 
    return paramBoolean;
  }
  
  public boolean setRoamingCallDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    boolean bool1 = false, bool2 = false;
    try {
      IOplusCustomizePhoneManagerService iOplusCustomizePhoneManagerService = getOplusCustomizePhoneManagerService();
      if (iOplusCustomizePhoneManagerService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("IOplusCustomizePhoneManagerService, service:");
        stringBuilder.append(iOplusCustomizePhoneManagerService);
        Slog.d("OplusCustomizePhoneManager", stringBuilder.toString());
        paramBoolean = iOplusCustomizePhoneManagerService.setRoamingCallDisabled(paramComponentName, paramBoolean);
      } else {
        Slog.d("OplusCustomizePhoneManager", "IOplusCustomizePhoneManagerService service is null");
        paramBoolean = bool2;
      } 
    } catch (RemoteException remoteException) {
      Slog.i("OplusCustomizePhoneManager", "setRoamingCallDisabled error!");
      remoteException.printStackTrace();
      paramBoolean = bool1;
    } 
    return paramBoolean;
  }
  
  public boolean isRoamingCallDisabled(ComponentName paramComponentName) {
    boolean bool1 = false, bool2 = false;
    try {
      IOplusCustomizePhoneManagerService iOplusCustomizePhoneManagerService = getOplusCustomizePhoneManagerService();
      if (iOplusCustomizePhoneManagerService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("mdm service IOplusCustomizePhoneManagerService service:");
        stringBuilder.append(iOplusCustomizePhoneManagerService);
        Slog.d("OplusCustomizePhoneManager", stringBuilder.toString());
        bool1 = bool2 = iOplusCustomizePhoneManagerService.isRoamingCallDisabled(paramComponentName);
      } else {
        Slog.d("OplusCustomizePhoneManager", "mdm service IOplusCustomizePhoneManagerService manager is null");
        bool1 = bool2;
      } 
    } catch (RemoteException remoteException) {
      Slog.i("OplusCustomizePhoneManager", "isRoamingCallDisabled error : RemoteException");
    } 
    return bool1;
  }
  
  public boolean setIncomingThirdCallDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    boolean bool1 = false, bool2 = false;
    try {
      IOplusCustomizePhoneManagerService iOplusCustomizePhoneManagerService = getOplusCustomizePhoneManagerService();
      if (iOplusCustomizePhoneManagerService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("mdm service IOplusCustomizePhoneManagerService service:");
        stringBuilder.append(iOplusCustomizePhoneManagerService);
        Slog.d("OplusCustomizePhoneManager", stringBuilder.toString());
        paramBoolean = iOplusCustomizePhoneManagerService.setIncomingThirdCallDisabled(paramComponentName, paramBoolean);
      } else {
        Slog.d("OplusCustomizePhoneManager", "mdm service IOplusCustomizePhoneManagerService service is null");
        paramBoolean = bool2;
      } 
    } catch (RemoteException remoteException) {
      Slog.i("OplusCustomizePhoneManager", "setIncomingThirdCallDisabled error!");
      remoteException.printStackTrace();
      paramBoolean = bool1;
    } 
    return paramBoolean;
  }
  
  public boolean isInComingThirdCallDisabled(ComponentName paramComponentName) {
    boolean bool1 = false, bool2 = false;
    try {
      IOplusCustomizePhoneManagerService iOplusCustomizePhoneManagerService = getOplusCustomizePhoneManagerService();
      if (iOplusCustomizePhoneManagerService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("mdm service IOplusCustomizePhoneManagerService service:");
        stringBuilder.append(iOplusCustomizePhoneManagerService);
        Slog.d("OplusCustomizePhoneManager", stringBuilder.toString());
        bool1 = bool2 = iOplusCustomizePhoneManagerService.isInComingThirdCallDisabled(paramComponentName);
      } else {
        Slog.d("OplusCustomizePhoneManager", "mdm service IOplusCustomizePhoneManagerService service is null");
        bool1 = bool2;
      } 
    } catch (RemoteException remoteException) {
      Slog.i("OplusCustomizePhoneManager", "isInComingThirdCallDisabled error!");
      remoteException.printStackTrace();
    } 
    return bool1;
  }
  
  public boolean setDefaultVoiceCard(ComponentName paramComponentName, int paramInt, Message paramMessage) {
    boolean bool1 = false, bool2 = false;
    boolean bool3 = bool1;
    try {
      IOplusCustomizePhoneManagerService iOplusCustomizePhoneManagerService = getOplusCustomizePhoneManagerService();
      if (iOplusCustomizePhoneManagerService != null) {
        bool3 = bool1;
        StringBuilder stringBuilder = new StringBuilder();
        bool3 = bool1;
        this();
        bool3 = bool1;
        stringBuilder.append("mdm service IDeviceRestrictionManager service:");
        bool3 = bool1;
        stringBuilder.append(iOplusCustomizePhoneManagerService);
        bool3 = bool1;
        Slog.d("OplusCustomizePhoneManager", stringBuilder.toString());
        bool3 = bool1;
        Bundle bundle = iOplusCustomizePhoneManagerService.setDefaultVoiceCard(paramComponentName, paramInt);
        if (bundle != null) {
          bool3 = bool1;
          paramMessage.setData(bundle);
          bool3 = bool1;
          bool2 = bundle.getBoolean("RESULT");
          bool3 = bool2;
          StringBuilder stringBuilder1 = new StringBuilder();
          bool3 = bool2;
          this();
          bool3 = bool2;
          stringBuilder1.append("mdm setDefaultVoiceCard result: ");
          bool3 = bool2;
          stringBuilder1.append(bundle.getBoolean("RESULT"));
          bool3 = bool2;
          stringBuilder1.append(", exception : ");
          bool3 = bool2;
          stringBuilder1.append(bundle.getString("EXCEPTION"));
          bool3 = bool2;
          Slog.d("OplusCustomizePhoneManager", stringBuilder1.toString());
          bool3 = bool2;
        } else {
          bool3 = bool1;
          Bundle bundle1 = new Bundle();
          bool3 = bool1;
          this();
          bool3 = bool1;
          bundle1.putBoolean("RESULT", false);
          bool3 = bool1;
          bundle1.putString("EXCEPTION", "GENERIC_FAILURE");
          bool3 = bool1;
          paramMessage.setData(bundle1);
          bool3 = bool2;
        } 
      } else {
        bool3 = bool1;
        Slog.d("OplusCustomizePhoneManager", "mdm service IDeviceRestrictionManager manager is null");
        bool3 = bool2;
      } 
    } catch (RemoteException remoteException) {
      Slog.i("OplusCustomizePhoneManager", "setDefaultVoiceCard RemoteException error!");
      Bundle bundle = new Bundle();
      bundle.putBoolean("RESULT", false);
      bundle.putString("EXCEPTION", "GENERIC_FAILURE");
      paramMessage.setData(bundle);
    } 
    return bool3;
  }
  
  public int getDefaultVoiceCard(ComponentName paramComponentName) {
    int i = -1;
    try {
      IOplusCustomizePhoneManagerService iOplusCustomizePhoneManagerService = getOplusCustomizePhoneManagerService();
      if (iOplusCustomizePhoneManagerService != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("mdm service IOplusCustomizePhoneManagerService service:");
        stringBuilder.append(iOplusCustomizePhoneManagerService);
        Slog.d("OplusCustomizePhoneManager", stringBuilder.toString());
        int j = iOplusCustomizePhoneManagerService.getDefaultVoiceCard(paramComponentName);
      } else {
        Slog.d("OplusCustomizePhoneManager", "mdm service IOplusCustomizePhoneManagerService service is null");
      } 
    } catch (RemoteException remoteException) {
      Slog.i("OplusCustomizePhoneManager", "getDefaultVoiceCard RemoteException error!");
    } 
    return i;
  }
  
  public void setSlotTwoDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizePhoneManagerService().setSlotTwoDisabled(paramBoolean);
      return;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSlotTwoDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
      return;
    } 
  }
  
  public boolean isSlotTwoDisabled() {
    try {
      return getOplusCustomizePhoneManagerService().isSlotTwoDisabled();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isSlotTwoDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public void answerRingingCall() {
    try {
      this.mOplusCustomizePhoneManagerService.answerRingingCall();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizePhoneManager", "answerRingingCall Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("answerRingingCall Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizePhoneManager", stringBuilder.toString());
    } 
  }
}
