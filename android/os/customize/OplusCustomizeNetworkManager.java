package android.os.customize;

import android.content.Context;
import android.os.RemoteException;
import android.util.Slog;
import java.util.List;

public class OplusCustomizeNetworkManager {
  private static final String SERVICE_NAME = "OplusCustomizeNetworkManagerService";
  
  private static final String TAG = "OplusCustomizeNetworkManager";
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizeNetworkManager sInstance;
  
  private IOplusCustomizeNetworkManagerService mOplusCustomizeNetworkManagerService;
  
  private OplusCustomizeNetworkManager() {
    geOplusCustomizeNetworkManagerService();
  }
  
  public static final OplusCustomizeNetworkManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizeNetworkManager oplusCustomizeNetworkManager = new OplusCustomizeNetworkManager();
          this();
          sInstance = oplusCustomizeNetworkManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusCustomizeNetworkManagerService geOplusCustomizeNetworkManagerService() {
    synchronized (mServiceLock) {
      if (this.mOplusCustomizeNetworkManagerService == null)
        this.mOplusCustomizeNetworkManagerService = IOplusCustomizeNetworkManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizeNetworkManagerService")); 
      return this.mOplusCustomizeNetworkManagerService;
    } 
  }
  
  public void setNetworkRestriction(int paramInt) {
    try {
      geOplusCustomizeNetworkManagerService().setNetworkRestriction(paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeNetworkManager", "setNetworkRestriction Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setNetworkRestriction Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeNetworkManager", stringBuilder.toString());
    } 
  }
  
  public void addNetworkRestriction(int paramInt, List<String> paramList) {
    try {
      geOplusCustomizeNetworkManagerService().addNetworkRestriction(paramInt, paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeNetworkManager", "addNetworkRestriction Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addNetworkRestriction Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeNetworkManager", stringBuilder.toString());
    } 
  }
  
  public void removeNetworkRestriction(int paramInt, List<String> paramList) {
    try {
      geOplusCustomizeNetworkManagerService().removeNetworkRestriction(paramInt, paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeNetworkManager", "removeNetworkRestriction Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeNetworkRestriction Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeNetworkManager", stringBuilder.toString());
    } 
  }
  
  public void removeNetworkRestrictionAll(int paramInt) {
    try {
      geOplusCustomizeNetworkManagerService().removeNetworkRestrictionAll(paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeNetworkManager", "removeNetworkRestrictionAll Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeNetworkRestrictionAll Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeNetworkManager", stringBuilder.toString());
    } 
  }
  
  public List<String> getNetworkRestrictionList(int paramInt) {
    try {
      return geOplusCustomizeNetworkManagerService().getNetworkRestrictionList(paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeNetworkManager", "getNetworkRestrictionList Error");
      return null;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getNetworkRestrictionList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeNetworkManager", stringBuilder.toString());
      return null;
    } 
  }
  
  public void setUserApnMgrPolicies(int paramInt) {
    try {
      geOplusCustomizeNetworkManagerService().setUserApnMgrPolicies(paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeNetworkManager", "setUserApnMgrPolicies Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setUserApnMgrPolicies Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeNetworkManager", stringBuilder.toString());
    } 
  }
  
  public int getUserApnMgrPolicies() {
    try {
      return geOplusCustomizeNetworkManagerService().getUserApnMgrPolicies();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeNetworkManager", "getUserApnMgrPolicies Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getUserApnMgrPolicies Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeNetworkManager", stringBuilder.toString());
    } 
    return -1;
  }
  
  public void addAppMeteredDataBlackList(List<String> paramList) {
    try {
      geOplusCustomizeNetworkManagerService().addAppMeteredDataBlackList(paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeNetworkManager", "addAppMeteredDataBlackList Error");
    } catch (Exception exception) {
      Slog.e("OplusCustomizeNetworkManager", "addAppMeteredDataBlackList Error");
    } 
  }
  
  public void addAppWlanDataBlackList(List<String> paramList) {
    try {
      geOplusCustomizeNetworkManagerService().addAppWlanDataBlackList(paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeNetworkManager", "addAppWlanDataBlackList Error");
    } catch (Exception exception) {
      Slog.e("OplusCustomizeNetworkManager", "addAppWlanDataBlackList Error");
    } 
  }
  
  public void removeAppMeteredDataBlackList(List<String> paramList) {
    try {
      geOplusCustomizeNetworkManagerService().removeAppMeteredDataBlackList(paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeNetworkManager", "removeAppMeteredDataBlackList Error");
    } catch (Exception exception) {
      Slog.e("OplusCustomizeNetworkManager", "removeAppMeteredDataBlackList Error");
    } 
  }
  
  public void removeAppWlanDataBlackList(List<String> paramList) {
    try {
      geOplusCustomizeNetworkManagerService().removeAppWlanDataBlackList(paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeNetworkManager", "removeAppWlanDataBlackList Error");
    } catch (Exception exception) {
      Slog.e("OplusCustomizeNetworkManager", "removeAppWlanDataBlackList Error");
    } 
  }
  
  public List<String> getAppMeteredDataBlackList() {
    try {
      return geOplusCustomizeNetworkManagerService().getAppMeteredDataBlackList();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeNetworkManager", "getAppMeteredDataBlackList Error");
      return null;
    } catch (Exception exception) {
      Slog.e("OplusCustomizeNetworkManager", "getAppMeteredDataBlackList Error");
      return null;
    } 
  }
  
  public List<String> getAppWlanDataBlackList() {
    try {
      return geOplusCustomizeNetworkManagerService().getAppWlanDataBlackList();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeNetworkManager", "getAppWlanDataBlackList Error");
      return null;
    } catch (Exception exception) {
      Slog.e("OplusCustomizeNetworkManager", "getAppWlanDataBlackList Error");
      return null;
    } 
  }
}
