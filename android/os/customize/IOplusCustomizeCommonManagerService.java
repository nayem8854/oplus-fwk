package android.os.customize;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusCustomizeCommonManagerService extends IInterface {
  class Default implements IOplusCustomizeCommonManagerService {
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizeCommonManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizeCommonManagerService";
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizeCommonManagerService");
    }
    
    public static IOplusCustomizeCommonManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizeCommonManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizeCommonManagerService)
        return (IOplusCustomizeCommonManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      return null;
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902)
        return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
      param1Parcel2.writeString("android.os.customize.IOplusCustomizeCommonManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusCustomizeCommonManagerService {
      public static IOplusCustomizeCommonManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizeCommonManagerService";
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizeCommonManagerService param1IOplusCustomizeCommonManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizeCommonManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizeCommonManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizeCommonManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
