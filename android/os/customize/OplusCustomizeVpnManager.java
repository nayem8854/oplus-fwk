package android.os.customize;

import android.content.ComponentName;
import android.content.Context;
import android.os.RemoteException;
import android.util.Slog;
import java.util.List;

public class OplusCustomizeVpnManager {
  private static final String SERVICE_NAME = "OplusCustomizeVpnManagerService";
  
  private static final String TAG = "OplusCustomizeVpnManager";
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizeVpnManager sInstance;
  
  private IOplusCustomizeVpnManagerService mOplusCustomizeVpnManagerService;
  
  private OplusCustomizeVpnManager() {
    getOplusCustomizeVpnManagerService();
  }
  
  public static final OplusCustomizeVpnManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizeVpnManager oplusCustomizeVpnManager = new OplusCustomizeVpnManager();
          this();
          sInstance = oplusCustomizeVpnManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusCustomizeVpnManagerService getOplusCustomizeVpnManagerService() {
    synchronized (mServiceLock) {
      if (this.mOplusCustomizeVpnManagerService == null)
        this.mOplusCustomizeVpnManagerService = IOplusCustomizeVpnManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizeVpnManagerService")); 
      return this.mOplusCustomizeVpnManagerService;
    } 
  }
  
  public int getVpnServiceState() {
    int i = 0;
    boolean bool = false;
    try {
      int j = getOplusCustomizeVpnManagerService().getVpnServiceState();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeVpnManager", "getVpnServiceState fail!", (Throwable)remoteException);
      i = bool;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getVpnServiceState Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeVpnManager", stringBuilder.toString());
    } 
    return i;
  }
  
  public void setVpnDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeVpnManagerService().setVpnDisabled(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeVpnManager", "setVpnDisabled Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setVpnDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeVpnManager", stringBuilder.toString());
    } 
  }
  
  public boolean isVpnDisabled(ComponentName paramComponentName) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      boolean bool2 = getOplusCustomizeVpnManagerService().isVpnDisabled(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeVpnManager", "isVpnDisabled Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isVpnDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeVpnManager", stringBuilder.toString());
      bool1 = bool;
    } 
    return bool1;
  }
  
  public void setAlwaysOnVpnPackage(ComponentName paramComponentName, String paramString, boolean paramBoolean) {
    try {
      getOplusCustomizeVpnManagerService().setAlwaysOnVpnPackage(paramComponentName, paramString, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeVpnManager", "setAlwaysOnVpnPackage fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setAlwaysOnVpnPackage Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeVpnManager", stringBuilder.toString());
    } 
  }
  
  public List<String> getVpnList(ComponentName paramComponentName) {
    StringBuilder stringBuilder1, stringBuilder2 = null;
    RemoteException remoteException = null;
    try {
      List<String> list = getOplusCustomizeVpnManagerService().getVpnList(paramComponentName);
    } catch (RemoteException remoteException1) {
      Slog.e("OplusCustomizeVpnManager", "getVpnList fail!", (Throwable)remoteException1);
      remoteException1 = remoteException;
    } catch (Exception exception) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("getVpnList Error");
      stringBuilder1.append(exception);
      Slog.e("OplusCustomizeVpnManager", stringBuilder1.toString());
      stringBuilder1 = stringBuilder2;
    } 
    return (List<String>)stringBuilder1;
  }
  
  public boolean deleteVpnProfile(ComponentName paramComponentName, String paramString) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      boolean bool2 = getOplusCustomizeVpnManagerService().deleteVpnProfile(paramComponentName, paramString);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeVpnManager", "deleteVpnProfile fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("deleteVpnProfile Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeVpnManager", stringBuilder.toString());
      bool1 = bool;
    } 
    return bool1;
  }
  
  public int disestablishVpnConnection(ComponentName paramComponentName) {
    int i = -1;
    try {
      int j = getOplusCustomizeVpnManagerService().disestablishVpnConnection(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeVpnManager", "disestablishVpnConnection fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("disestablishVpnConnection Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeVpnManager", stringBuilder.toString());
    } 
    return i;
  }
  
  public String getAlwaysOnVpnPackage(ComponentName paramComponentName) {
    Exception exception2 = null;
    RemoteException remoteException = null;
    try {
      String str = getOplusCustomizeVpnManagerService().getAlwaysOnVpnPackage(paramComponentName);
    } catch (RemoteException remoteException1) {
      Slog.e("OplusCustomizeVpnManager", "getAlwaysOnVpnPackage fail!", (Throwable)remoteException1);
      remoteException1 = remoteException;
    } catch (Exception exception1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAlwaysOnVpnPackage Error");
      stringBuilder.append(exception1);
      Slog.e("OplusCustomizeVpnManager", stringBuilder.toString());
      exception1 = exception2;
    } 
    return (String)exception1;
  }
  
  public void setVpnAlwaysOnPersis(boolean paramBoolean) {
    try {
      getOplusCustomizeVpnManagerService().setVpnAlwaysOnPersis(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeVpnManager", "setVpnAlwaysOnPersis fail!", (Throwable)remoteException);
    } 
  }
  
  public boolean getVpnAlwaysOnPersis(String paramString) {
    boolean bool2, bool1 = false;
    try {
      bool2 = getOplusCustomizeVpnManagerService().getVpnAlwaysOnPersis(paramString);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeVpnManager", "getVpnAlwaysOnPersis fail!", (Throwable)remoteException);
      bool2 = bool1;
    } 
    return bool2;
  }
}
