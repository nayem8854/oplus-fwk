package android.os.customize;

import android.content.Context;
import android.os.RemoteException;
import android.util.Slog;
import java.util.ArrayList;
import java.util.List;

public class OplusCustomizeApplicationManager {
  private static final String SERVICE_NAME = "OplusCustomizeApplicationManagerService";
  
  private static final String TAG = "OplusCustomizeApplicationManager";
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizeApplicationManager sInstance;
  
  private IOplusCustomizeApplicationManagerService mOplusCustomizeApplicationManagerService;
  
  private OplusCustomizeApplicationManager() {
    getOplusCustomizeApplicationManagerService();
  }
  
  public static final OplusCustomizeApplicationManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizeApplicationManager oplusCustomizeApplicationManager = new OplusCustomizeApplicationManager();
          this();
          sInstance = oplusCustomizeApplicationManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusCustomizeApplicationManagerService getOplusCustomizeApplicationManagerService() {
    synchronized (mServiceLock) {
      if (this.mOplusCustomizeApplicationManagerService == null)
        this.mOplusCustomizeApplicationManagerService = IOplusCustomizeApplicationManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizeApplicationManagerService")); 
      if (this.mOplusCustomizeApplicationManagerService == null)
        Slog.e("OplusCustomizeApplicationManager", "mOplusCustomizeApplicationManagerService is null"); 
      return this.mOplusCustomizeApplicationManagerService;
    } 
  }
  
  public boolean setDisabledAppList(List<String> paramList, int paramInt) {
    boolean bool3;
    if (paramList == null)
      return false; 
    boolean bool1 = false, bool2 = false;
    try {
      bool3 = getOplusCustomizeApplicationManagerService().setDisabledAppList(paramList, paramInt);
      bool2 = bool3;
      bool1 = bool3;
      Slog.d("OplusCustomizeApplicationManager", "setDisabledAppList: succeeded");
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "setDisabledAppList fail!", (Throwable)remoteException);
      bool3 = bool1;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setDisabledAppList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
    bool2 = bool3;
    return bool2;
  }
  
  public List<String> getDisabledAppList() {
    List<String> list1 = null, list2 = null;
    try {
      List<String> list = getOplusCustomizeApplicationManagerService().getDisabledAppList();
      list2 = list;
      list1 = list;
      Slog.d("OplusCustomizeApplicationManager", "getDisabledAppList: succeeded");
      list1 = list;
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "getDisabledAppList fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getDisabledAppList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
    return (List<String>)exception;
  }
  
  public boolean forceStopPackage(List<String> paramList) {
    if (paramList == null)
      return false; 
    boolean bool1 = false, bool2 = false;
    try {
      boolean bool = getOplusCustomizeApplicationManagerService().forceStopPackage(paramList);
      bool2 = bool;
      bool1 = bool;
      Slog.d("OplusCustomizeApplicationManager", "forceStopPackage: succeeded");
      bool1 = bool;
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "forceStopPackage fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("forceStopPackage Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
    bool2 = bool1;
    return bool2;
  }
  
  public String getTopAppPackageName() {
    try {
      return getOplusCustomizeApplicationManagerService().getTopAppPackageName();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "getTopAppPackageName fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getTopAppPackageName Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
    return "";
  }
  
  public void killApplicationProcess(String paramString) {
    if (paramString == null || paramString.isEmpty())
      return; 
    try {
      getOplusCustomizeApplicationManagerService().killApplicationProcess(paramString);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "killApplicationProcess fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("killApplicationProcess Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
  }
  
  public void cleanBackgroundProcess() {
    try {
      getOplusCustomizeApplicationManagerService().cleanBackgroundProcess();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "cleanBackgroundProcess fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("cleanBackgroundProcess Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
  }
  
  public void addDisallowedRunningApp(List<String> paramList) {
    try {
      getOplusCustomizeApplicationManagerService().addDisallowedRunningApp(paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "addDisallowedRunningApp fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addDisallowedRunningApp Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
  }
  
  public void removeDisallowedRunningApp(List<String> paramList) {
    try {
      getOplusCustomizeApplicationManagerService().removeDisallowedRunningApp(paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "removeDisallowedRunningApp fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeDisallowedRunningApp Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
  }
  
  public List<String> getDisallowedRunningApp() {
    try {
      return getOplusCustomizeApplicationManagerService().getDisallowedRunningApp();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "getDisallowedRunningApp fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getDisallowedRunningApp Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public void addTrustedAppStore(String paramString) {
    try {
      getOplusCustomizeApplicationManagerService().addTrustedAppStore(paramString);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "addTrustedAppStore fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addTrustedAppStore Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
  }
  
  public void deleteTrustedAppStore(String paramString) {
    try {
      getOplusCustomizeApplicationManagerService().deleteTrustedAppStore(paramString);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "deleteTrustedAppStore fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("deleteTrustedAppStore Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
  }
  
  public void enableTrustedAppStore(boolean paramBoolean) {
    try {
      getOplusCustomizeApplicationManagerService().enableTrustedAppStore(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "enableTrustedAppStore fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("enableTrustedAppStore Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
  }
  
  public boolean isTrustedAppStoreEnabled() {
    try {
      return getOplusCustomizeApplicationManagerService().isTrustedAppStoreEnabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "isTrustedAppStoreEnabled Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isTrustedAppStoreEnabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public List<String> getTrustedAppStore() {
    try {
      return getOplusCustomizeApplicationManagerService().getTrustedAppStore();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "getTrustedAppStore fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getTrustedAppStore Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public void setAllowTrustedAppStore(boolean paramBoolean) {
    try {
      getOplusCustomizeApplicationManagerService().setAllowTrustedAppStore(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "setAllowTrustedAppStore: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setAllowTrustedAppStore Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
  }
  
  public boolean isAllowTrustedAppStore() {
    try {
      return getOplusCustomizeApplicationManagerService().isAllowTrustedAppStore();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "isAllowTrustedAppStore: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isAllowTrustedAppStore Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public void addAppAlarmWhiteList(List<String> paramList) {
    try {
      getOplusCustomizeApplicationManagerService().addAppAlarmWhiteList(paramList);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addAppAlarmWhiteList exception ");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
  }
  
  public List<String> getAppAlarmWhiteList() {
    try {
      return getOplusCustomizeApplicationManagerService().getAppAlarmWhiteList();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAppAlarmWhiteList exception ");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
      return null;
    } 
  }
  
  public boolean removeAppAlarmWhiteList(List<String> paramList) {
    try {
      return getOplusCustomizeApplicationManagerService().removeAppAlarmWhiteList(paramList);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeAppAlarmWhiteList exception ");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean removeAllAppAlarmWhiteList() {
    try {
      return getOplusCustomizeApplicationManagerService().removeAllAppAlarmWhiteList();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeAllAppAlarmWhiteList exception ");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public void setAllowControlAppRun(boolean paramBoolean) {
    try {
      getOplusCustomizeApplicationManagerService().setAllowControlAppRun(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "setAllowControlAppRun: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setAllowControlAppRun Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
  }
  
  public boolean isAllowControlAppRun() {
    try {
      return getOplusCustomizeApplicationManagerService().isAllowControlAppRun();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeApplicationManager", "isAllowControlAppRun: fail", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isAllowControlAppRun Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public void addPersistentApp(List<String> paramList) {
    try {
      getOplusCustomizeApplicationManagerService().addPersistentApp(paramList);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addPersistentApp exception ");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
  }
  
  public void removePersistentApp(List<String> paramList) {
    try {
      getOplusCustomizeApplicationManagerService().removePersistentApp(paramList);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removePersistentApp exception ");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
  }
  
  public List<String> getPersistentApp() {
    List<String> list = new ArrayList();
    try {
      List<String> list1 = getOplusCustomizeApplicationManagerService().getPersistentApp();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getPersistentApp exception ");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
    return list;
  }
  
  public void interceptStopLockTask(boolean paramBoolean) {
    try {
      getOplusCustomizeApplicationManagerService().interceptStopLockTask(paramBoolean);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("interceptStopLockTask exception ");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
    } 
  }
  
  public boolean getStopLockTaskAvailability() {
    try {
      return getOplusCustomizeApplicationManagerService().getStopLockTaskAvailability();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getStopLockTaskAvailability exception ");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeApplicationManager", stringBuilder.toString());
      return false;
    } 
  }
}
