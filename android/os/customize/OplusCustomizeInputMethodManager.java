package android.os.customize;

import android.content.Context;
import android.os.RemoteException;
import android.util.Slog;

public class OplusCustomizeInputMethodManager {
  private static final String SERVICE_NAME = "OplusCustomizeInputMethodManagerService";
  
  private static final String TAG = "OplusCustomizeInputMethodManager";
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizeInputMethodManager sInstance;
  
  private IOplusCustomizeInputMethodManagerService mIOplusCustomizeInputMethodManagerService;
  
  private OplusCustomizeInputMethodManager() {
    getOplusCustomizeInputMethodManagerService();
  }
  
  private IOplusCustomizeInputMethodManagerService getOplusCustomizeInputMethodManagerService() {
    synchronized (mServiceLock) {
      if (this.mIOplusCustomizeInputMethodManagerService == null)
        this.mIOplusCustomizeInputMethodManagerService = IOplusCustomizeInputMethodManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizeInputMethodManagerService")); 
      if (this.mIOplusCustomizeInputMethodManagerService == null)
        Slog.e("OplusCustomizeInputMethodManager", "mIOplusCustomizeInputMethodService is null"); 
      return this.mIOplusCustomizeInputMethodManagerService;
    } 
  }
  
  public static final OplusCustomizeInputMethodManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizeInputMethodManager oplusCustomizeInputMethodManager = new OplusCustomizeInputMethodManager();
          this();
          sInstance = oplusCustomizeInputMethodManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  public void setDefaultInputMethod(String paramString) {
    try {
      getOplusCustomizeInputMethodManagerService().setDefaultInputMethod(paramString);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeInputMethodManager", "setDefaultInputMethod: fail");
    } catch (Exception exception) {
      Slog.e("OplusCustomizeInputMethodManager", "setDefaultInputMethod Error");
    } 
  }
  
  public String getDefaultInputMethod() {
    try {
      return getOplusCustomizeInputMethodManagerService().getDefaultInputMethod();
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeInputMethodManager", "setDefaultInputMethod: fail");
    } catch (Exception exception) {
      Slog.e("OplusCustomizeInputMethodManager", "setDefaultInputMethod Error");
    } 
    return null;
  }
  
  public void clearDefaultInputMethod() {
    try {
      getOplusCustomizeInputMethodManagerService().clearDefaultInputMethod();
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeInputMethodManager", "clearDefaultInputMethod: fail");
    } catch (Exception exception) {
      Slog.e("OplusCustomizeInputMethodManager", "clearDefaultInputMethod Error");
    } 
  }
}
