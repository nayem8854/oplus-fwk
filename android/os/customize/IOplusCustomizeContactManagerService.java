package android.os.customize;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusCustomizeContactManagerService extends IInterface {
  int getContactBlockPattern() throws RemoteException;
  
  int getContactMatchPattern() throws RemoteException;
  
  int getContactNumberHideMode() throws RemoteException;
  
  int getContactNumberMaskEnable() throws RemoteException;
  
  int getContactOutgoOrIncomePattern() throws RemoteException;
  
  boolean isContactBlackListEnable() throws RemoteException;
  
  boolean isForbidCallLogEnable() throws RemoteException;
  
  boolean setContactBlackListEnable(boolean paramBoolean) throws RemoteException;
  
  boolean setContactBlockPattern(int paramInt) throws RemoteException;
  
  boolean setContactMatchPattern(int paramInt) throws RemoteException;
  
  boolean setContactNumberHideMode(int paramInt) throws RemoteException;
  
  boolean setContactNumberMaskEnable(int paramInt) throws RemoteException;
  
  boolean setContactOutgoOrIncomePattern(int paramInt) throws RemoteException;
  
  boolean setForbidCallLogEnable(int paramInt) throws RemoteException;
  
  class Default implements IOplusCustomizeContactManagerService {
    public boolean setContactBlackListEnable(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isContactBlackListEnable() throws RemoteException {
      return false;
    }
    
    public boolean setContactBlockPattern(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getContactBlockPattern() throws RemoteException {
      return 0;
    }
    
    public boolean setContactMatchPattern(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getContactMatchPattern() throws RemoteException {
      return 0;
    }
    
    public boolean setContactOutgoOrIncomePattern(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getContactOutgoOrIncomePattern() throws RemoteException {
      return 0;
    }
    
    public boolean setContactNumberHideMode(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getContactNumberHideMode() throws RemoteException {
      return 0;
    }
    
    public boolean setContactNumberMaskEnable(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getContactNumberMaskEnable() throws RemoteException {
      return 0;
    }
    
    public boolean setForbidCallLogEnable(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isForbidCallLogEnable() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizeContactManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizeContactManagerService";
    
    static final int TRANSACTION_getContactBlockPattern = 4;
    
    static final int TRANSACTION_getContactMatchPattern = 6;
    
    static final int TRANSACTION_getContactNumberHideMode = 10;
    
    static final int TRANSACTION_getContactNumberMaskEnable = 12;
    
    static final int TRANSACTION_getContactOutgoOrIncomePattern = 8;
    
    static final int TRANSACTION_isContactBlackListEnable = 2;
    
    static final int TRANSACTION_isForbidCallLogEnable = 14;
    
    static final int TRANSACTION_setContactBlackListEnable = 1;
    
    static final int TRANSACTION_setContactBlockPattern = 3;
    
    static final int TRANSACTION_setContactMatchPattern = 5;
    
    static final int TRANSACTION_setContactNumberHideMode = 9;
    
    static final int TRANSACTION_setContactNumberMaskEnable = 11;
    
    static final int TRANSACTION_setContactOutgoOrIncomePattern = 7;
    
    static final int TRANSACTION_setForbidCallLogEnable = 13;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizeContactManagerService");
    }
    
    public static IOplusCustomizeContactManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizeContactManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizeContactManagerService)
        return (IOplusCustomizeContactManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 14:
          return "isForbidCallLogEnable";
        case 13:
          return "setForbidCallLogEnable";
        case 12:
          return "getContactNumberMaskEnable";
        case 11:
          return "setContactNumberMaskEnable";
        case 10:
          return "getContactNumberHideMode";
        case 9:
          return "setContactNumberHideMode";
        case 8:
          return "getContactOutgoOrIncomePattern";
        case 7:
          return "setContactOutgoOrIncomePattern";
        case 6:
          return "getContactMatchPattern";
        case 5:
          return "setContactMatchPattern";
        case 4:
          return "getContactBlockPattern";
        case 3:
          return "setContactBlockPattern";
        case 2:
          return "isContactBlackListEnable";
        case 1:
          break;
      } 
      return "setContactBlackListEnable";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool7;
        int i1;
        boolean bool6;
        int n;
        boolean bool5;
        int m;
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 14:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
            bool7 = isForbidCallLogEnable();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 13:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
            i1 = param1Parcel1.readInt();
            bool6 = setForbidCallLogEnable(i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 12:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
            n = getContactNumberMaskEnable();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(n);
            return true;
          case 11:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
            n = param1Parcel1.readInt();
            bool5 = setContactNumberMaskEnable(n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
            m = getContactNumberHideMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
            m = param1Parcel1.readInt();
            bool4 = setContactNumberHideMode(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
            k = getContactOutgoOrIncomePattern();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
            k = param1Parcel1.readInt();
            bool3 = setContactOutgoOrIncomePattern(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
            j = getContactMatchPattern();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
            j = param1Parcel1.readInt();
            bool2 = setContactMatchPattern(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
            i = getContactBlockPattern();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
            i = param1Parcel1.readInt();
            bool1 = setContactBlockPattern(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
            bool1 = isContactBlackListEnable();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeContactManagerService");
        if (param1Parcel1.readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        boolean bool1 = setContactBlackListEnable(bool);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.os.customize.IOplusCustomizeContactManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusCustomizeContactManagerService {
      public static IOplusCustomizeContactManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizeContactManagerService";
      }
      
      public boolean setContactBlackListEnable(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeContactManagerService.Stub.getDefaultImpl().setContactBlackListEnable(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isContactBlackListEnable() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeContactManagerService.Stub.getDefaultImpl().isContactBlackListEnable();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setContactBlockPattern(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeContactManagerService.Stub.getDefaultImpl().setContactBlockPattern(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getContactBlockPattern() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeContactManagerService.Stub.getDefaultImpl().getContactBlockPattern(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setContactMatchPattern(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeContactManagerService.Stub.getDefaultImpl().setContactMatchPattern(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getContactMatchPattern() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeContactManagerService.Stub.getDefaultImpl().getContactMatchPattern(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setContactOutgoOrIncomePattern(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeContactManagerService.Stub.getDefaultImpl().setContactOutgoOrIncomePattern(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getContactOutgoOrIncomePattern() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeContactManagerService.Stub.getDefaultImpl().getContactOutgoOrIncomePattern(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setContactNumberHideMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeContactManagerService.Stub.getDefaultImpl().setContactNumberHideMode(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getContactNumberHideMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeContactManagerService.Stub.getDefaultImpl().getContactNumberHideMode(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setContactNumberMaskEnable(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeContactManagerService.Stub.getDefaultImpl().setContactNumberMaskEnable(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getContactNumberMaskEnable() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeContactManagerService.Stub.getDefaultImpl().getContactNumberMaskEnable(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setForbidCallLogEnable(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeContactManagerService.Stub.getDefaultImpl().setForbidCallLogEnable(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isForbidCallLogEnable() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeContactManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(14, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeContactManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeContactManagerService.Stub.getDefaultImpl().isForbidCallLogEnable();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizeContactManagerService param1IOplusCustomizeContactManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizeContactManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizeContactManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizeContactManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
