package android.os.customize;

import android.content.ComponentName;
import android.content.Context;
import android.os.RemoteException;
import android.util.Slog;
import java.util.ArrayList;
import java.util.List;

public class OplusCustomizeConnectivityManager {
  private static final String SERVICE_NAME = "OplusCustomizeConnectivityManagerService";
  
  private static final String TAG = "OplusCustomizeConnectivityManager";
  
  public static final int WLAN_POLICY_DEFAULT = 2;
  
  public static final int WLAN_POLICY_FORCE_OFF = 0;
  
  public static final int WLAN_POLICY_FORCE_ON = 5;
  
  public static final int WLAN_POLICY_OFF = 3;
  
  public static final int WLAN_POLICY_OFF_WITH_SCAN = 1;
  
  public static final int WLAN_POLICY_ON = 4;
  
  public static final String WLAN_POLICY_STRING_DEFAULT = "2";
  
  public static final String WLAN_POLICY_STRING_FORCE_OFF = "0";
  
  public static final String WLAN_POLICY_STRING_FORCE_ON = "5";
  
  public static final String WLAN_POLICY_STRING_OFF = "3";
  
  public static final String WLAN_POLICY_STRING_OFF_WITH_SCAN = "1";
  
  public static final String WLAN_POLICY_STRING_ON = "4";
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizeConnectivityManager sInstance;
  
  private IOplusCustomizeConnectivityManagerService mOplusCustomizeConnectivityManagerServiceService;
  
  private OplusCustomizeConnectivityManager() {
    getOplusCustomizeConnectivityManagerService();
  }
  
  public static final OplusCustomizeConnectivityManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizeConnectivityManager oplusCustomizeConnectivityManager = new OplusCustomizeConnectivityManager();
          this();
          sInstance = oplusCustomizeConnectivityManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusCustomizeConnectivityManagerService getOplusCustomizeConnectivityManagerService() {
    synchronized (mServiceLock) {
      if (this.mOplusCustomizeConnectivityManagerServiceService == null)
        this.mOplusCustomizeConnectivityManagerServiceService = IOplusCustomizeConnectivityManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizeConnectivityManagerService")); 
      return this.mOplusCustomizeConnectivityManagerServiceService;
    } 
  }
  
  public boolean setUserProfilesDisabled(boolean paramBoolean) {
    boolean bool1 = false, bool2 = false;
    try {
      paramBoolean = getOplusCustomizeConnectivityManagerService().setUserProfilesDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeConnectivityManager", "setUserProfilesDisabled", (Throwable)remoteException);
      paramBoolean = bool2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setUserProfilesDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      paramBoolean = bool1;
    } 
    return paramBoolean;
  }
  
  public boolean isUserProfilesDisabled() {
    boolean bool = false;
    boolean bool1 = false;
    try {
      boolean bool2 = getOplusCustomizeConnectivityManagerService().isUserProfilesDisabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeConnectivityManager", "isUserProfilesDisabled", (Throwable)remoteException);
      bool = bool1;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isUserProfilesDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
    } 
    return bool;
  }
  
  public int getBluetoothPolicies() {
    try {
      return getOplusCustomizeConnectivityManagerService().getBluetoothPolicies();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "getBluetoothPolicies fail!");
      return 2;
    } 
  }
  
  public boolean setBluetoothPolicies(int paramInt) {
    try {
      return getOplusCustomizeConnectivityManagerService().setBluetoothPolicies(paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "setBluetoothPolicies fail!");
      return false;
    } 
  }
  
  public boolean addBluetoothDevicesToWhiteLists(List<String> paramList) {
    try {
      return getOplusCustomizeConnectivityManagerService().addBluetoothDevicesToWhiteLists(paramList);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "addBluetoothDevicesToWhiteLists fail!");
      return false;
    } 
  }
  
  public boolean isWhiteListedDevice(String paramString) {
    try {
      return getOplusCustomizeConnectivityManagerService().isWhiteListedDevice(paramString);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "isWhiteListedDevice fail!");
      return false;
    } 
  }
  
  public List<String> getBluetoothDevicesFromWhiteLists() {
    try {
      return getOplusCustomizeConnectivityManagerService().getBluetoothDevicesFromWhiteLists();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "getBluetoothDevicesFromWhiteLists fail!");
      return new ArrayList<>();
    } 
  }
  
  public boolean removeBluetoothDevicesFromWhiteLists(List<String> paramList) {
    try {
      return getOplusCustomizeConnectivityManagerService().removeBluetoothDevicesFromWhiteLists(paramList);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "getBluetoothDevicesFromWhiteLists fail!");
      return false;
    } 
  }
  
  public boolean addBluetoothDevicesToBlackLists(List<String> paramList) {
    try {
      return getOplusCustomizeConnectivityManagerService().addBluetoothDevicesToBlackLists(paramList);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "addBluetoothDevicesToBlackLists fail!");
      return false;
    } 
  }
  
  public boolean isBlackListedDevice(String paramString) {
    try {
      return getOplusCustomizeConnectivityManagerService().isBlackListedDevice(paramString);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "isBlackListedDevice fail!");
      return false;
    } 
  }
  
  public List<String> getBluetoothDevicesFromBlackLists() {
    try {
      return getOplusCustomizeConnectivityManagerService().getBluetoothDevicesFromBlackLists();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "getBluetoothDevicesFromBlackLists fail!");
      return new ArrayList<>();
    } 
  }
  
  public boolean removeBluetoothDevicesFromBlackLists(List<String> paramList) {
    try {
      return getOplusCustomizeConnectivityManagerService().removeBluetoothDevicesFromBlackLists(paramList);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "removeBluetoothDevicesFromBlackLists fail!");
      return false;
    } 
  }
  
  public boolean setWifiEditDisabled(boolean paramBoolean) {
    boolean bool1 = false, bool2 = false;
    try {
      paramBoolean = getOplusCustomizeConnectivityManagerService().setWifiEditDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeConnectivityManager", "setWifiEditDisabled", (Throwable)remoteException);
      paramBoolean = bool2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setWifiEditDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      paramBoolean = bool1;
    } 
    return paramBoolean;
  }
  
  public boolean isWifiEditDisabled() {
    boolean bool = false;
    boolean bool1 = false;
    try {
      boolean bool2 = getOplusCustomizeConnectivityManagerService().isWifiEditDisabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeConnectivityManager", "isWifiEditDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isWifiEditDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      bool1 = bool;
    } 
    return bool1;
  }
  
  public boolean setWifiRestrictionList(List<String> paramList, String paramString) {
    try {
      return getOplusCustomizeConnectivityManagerService().setWifiRestrictionList(paramList, paramString);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setWifiRestrictionList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean removeFromRestrictionList(List<String> paramList, String paramString) {
    try {
      return getOplusCustomizeConnectivityManagerService().removeFromRestrictionList(paramList, paramString);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeFromRestrictionList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public List<String> getWifiRestrictionList(String paramString) {
    try {
      return getOplusCustomizeConnectivityManagerService().getWifiRestrictionList(paramString);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getWifiRestrictionList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      return null;
    } 
  }
  
  public boolean setWifiAutoConnectionDisabled(boolean paramBoolean) {
    try {
      return getOplusCustomizeConnectivityManagerService().setWifiAutoConnectionDisabled(paramBoolean);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setWifiAutoConnectionDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean isWifiAutoConnectionDisabled() {
    try {
      return getOplusCustomizeConnectivityManagerService().isWifiAutoConnectionDisabled();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isWifiAutoConnectionDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean setSecurityLevel(int paramInt) {
    try {
      return getOplusCustomizeConnectivityManagerService().setSecurityLevel(paramInt);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSecurityLevel Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public int getSecurityLevel() {
    try {
      return getOplusCustomizeConnectivityManagerService().getSecurityLevel();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getSecurityLevel Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      return 0;
    } 
  }
  
  public boolean setWifiApPolicies(int paramInt) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      boolean bool2 = getOplusCustomizeConnectivityManagerService().setWifiApPolicies(paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeConnectivityManager", "setWifiApPolicies", (Throwable)remoteException);
      bool = bool1;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setWifiApPolicies Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
    } 
    return bool;
  }
  
  public int getWifiApPolicies() {
    int i = 0;
    boolean bool = false;
    try {
      int j = getOplusCustomizeConnectivityManagerService().getWifiApPolicies();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeConnectivityManager", "getWifiApPolicies", (Throwable)remoteException);
      i = bool;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getWifiApPolicies Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
    } 
    return i;
  }
  
  public List<String> getWlanApClientWhiteList() {
    try {
      return getOplusCustomizeConnectivityManagerService().getWlanApClientWhiteList();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeConnectivityManager", "getWlanApWhiteList", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getWlanApWhiteList");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public List<String> getWlanApClientBlackList() {
    try {
      return getOplusCustomizeConnectivityManagerService().getWlanApClientBlackList();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeConnectivityManager", "getWlanApClientBlackList", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getWlanApClientBlackList");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public boolean setWlanApClientBlackList(List<String> paramList) {
    try {
      return getOplusCustomizeConnectivityManagerService().setWlanApClientBlackList(paramList);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setWlanApClientBlackList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean removeWlanApClientBlackList(List<String> paramList) {
    try {
      return getOplusCustomizeConnectivityManagerService().removeWlanApClientBlackList(paramList);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeWlanApClientBlackList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public void turnOnGPS(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeConnectivityManagerService().turnOnGPS(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeConnectivityManager", "setVoiceDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setVoiceDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
    } 
  }
  
  public boolean isGPSTurnOn(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeConnectivityManagerService().isGPSTurnOn(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeConnectivityManager", "setVoiceDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setVoiceDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public void setGPSDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeConnectivityManagerService().setGPSDisabled(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeConnectivityManager", "setVoiceDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setVoiceDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
    } 
  }
  
  public boolean isGPSDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeConnectivityManagerService().isGPSDisabled(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeConnectivityManager", "setVoiceDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setVoiceDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public String getDevicePosition(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeConnectivityManagerService().getDevicePosition(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeConnectivityManager", "setVoiceDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setVoiceDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public boolean setUnSecureSoftApDisabled(boolean paramBoolean) {
    boolean bool1 = false, bool2 = false;
    try {
      paramBoolean = getOplusCustomizeConnectivityManagerService().setUnSecureSoftApDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeConnectivityManager", "setUnSecureSoftApDisabled", (Throwable)remoteException);
      paramBoolean = bool2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setUnSecureSoftApDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      paramBoolean = bool1;
    } 
    return paramBoolean;
  }
  
  public boolean isUnSecureSoftApDisabled() {
    boolean bool3, bool1 = false, bool2 = false;
    try {
      bool3 = getOplusCustomizeConnectivityManagerService().isUnSecureSoftApDisabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeConnectivityManager", "isUnSecureSoftApDisabled", (Throwable)remoteException);
      bool3 = bool2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isUnSecureSoftApDisabled");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeConnectivityManager", stringBuilder.toString());
      bool3 = bool1;
    } 
    return bool3;
  }
  
  public boolean setWlanPolicies(ComponentName paramComponentName, int paramInt) {
    try {
      return getOplusCustomizeConnectivityManagerService().setWlanPolicies(paramComponentName, paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "setWlanPolicies fail!", exception);
      return false;
    } 
  }
  
  public int getWlanPolicies(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeConnectivityManagerService().getWlanPolicies(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "getWlanPolicies fail!", exception);
      return 2;
    } 
  }
  
  public boolean isWlanForceEnabled() {
    try {
      return getOplusCustomizeConnectivityManagerService().isWlanForceEnabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "isWlanForceEnabled fail!", exception);
      return false;
    } 
  }
  
  public boolean isWlanForceDisabled() {
    try {
      return getOplusCustomizeConnectivityManagerService().isWlanForceDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeConnectivityManager", "isWlanForceDisabled fail!", exception);
      return false;
    } 
  }
}
