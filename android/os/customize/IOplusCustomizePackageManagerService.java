package android.os.customize;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public interface IOplusCustomizePackageManagerService extends IInterface {
  void addDisabledDeactivateMdmPackages(List<String> paramList) throws RemoteException;
  
  void addDisallowedUninstallPackages(List<String> paramList) throws RemoteException;
  
  boolean clearAllSuperWhiteList() throws RemoteException;
  
  void clearAppData(String paramString) throws RemoteException;
  
  boolean clearSuperWhiteList(List<String> paramList) throws RemoteException;
  
  boolean getAdbInstallUninstallDisabled() throws RemoteException;
  
  List<String> getAllInstallSysAppList() throws RemoteException;
  
  List<String> getClearAppName() throws RemoteException;
  
  Map<String, String> getContainOplusCertificatePackages() throws RemoteException;
  
  List<String> getDetachableInstallSysAppList() throws RemoteException;
  
  List<String> getDisabledDeactivateMdmPackages() throws RemoteException;
  
  List<String> getDisallowUninstallPackageList() throws RemoteException;
  
  Bundle getInstallSysAppBundle() throws RemoteException;
  
  List<String> getPrivInstallSysAppList() throws RemoteException;
  
  List<String> getSuperWhiteList() throws RemoteException;
  
  boolean isDisabledDeactivateMdmPackage(String paramString) throws RemoteException;
  
  void removeAllDisabledDeactivateMdmPackages() throws RemoteException;
  
  void removeAllDisallowedUninstallPackages() throws RemoteException;
  
  void removeDisabledDeactivateMdmPackages(List<String> paramList) throws RemoteException;
  
  void removeDisallowedUninstallPackages(List<String> paramList) throws RemoteException;
  
  void setAdbInstallUninstallDisabled(boolean paramBoolean) throws RemoteException;
  
  void setInstallSysAppBundle(Bundle paramBundle) throws RemoteException;
  
  boolean setSuperWhiteList(List<String> paramList) throws RemoteException;
  
  class Default implements IOplusCustomizePackageManagerService {
    public void addDisabledDeactivateMdmPackages(List<String> param1List) throws RemoteException {}
    
    public void removeDisabledDeactivateMdmPackages(List<String> param1List) throws RemoteException {}
    
    public void removeAllDisabledDeactivateMdmPackages() throws RemoteException {}
    
    public List<String> getDisabledDeactivateMdmPackages() throws RemoteException {
      return null;
    }
    
    public boolean isDisabledDeactivateMdmPackage(String param1String) throws RemoteException {
      return false;
    }
    
    public void addDisallowedUninstallPackages(List<String> param1List) throws RemoteException {}
    
    public void removeDisallowedUninstallPackages(List<String> param1List) throws RemoteException {}
    
    public void removeAllDisallowedUninstallPackages() throws RemoteException {}
    
    public List<String> getDisallowUninstallPackageList() throws RemoteException {
      return null;
    }
    
    public void clearAppData(String param1String) throws RemoteException {}
    
    public List<String> getClearAppName() throws RemoteException {
      return null;
    }
    
    public void setAdbInstallUninstallDisabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean getAdbInstallUninstallDisabled() throws RemoteException {
      return false;
    }
    
    public void setInstallSysAppBundle(Bundle param1Bundle) throws RemoteException {}
    
    public Bundle getInstallSysAppBundle() throws RemoteException {
      return null;
    }
    
    public List<String> getPrivInstallSysAppList() throws RemoteException {
      return null;
    }
    
    public List<String> getDetachableInstallSysAppList() throws RemoteException {
      return null;
    }
    
    public List<String> getAllInstallSysAppList() throws RemoteException {
      return null;
    }
    
    public Map<String, String> getContainOplusCertificatePackages() throws RemoteException {
      return null;
    }
    
    public boolean setSuperWhiteList(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public List<String> getSuperWhiteList() throws RemoteException {
      return null;
    }
    
    public boolean clearSuperWhiteList(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public boolean clearAllSuperWhiteList() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizePackageManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizePackageManagerService";
    
    static final int TRANSACTION_addDisabledDeactivateMdmPackages = 1;
    
    static final int TRANSACTION_addDisallowedUninstallPackages = 6;
    
    static final int TRANSACTION_clearAllSuperWhiteList = 23;
    
    static final int TRANSACTION_clearAppData = 10;
    
    static final int TRANSACTION_clearSuperWhiteList = 22;
    
    static final int TRANSACTION_getAdbInstallUninstallDisabled = 13;
    
    static final int TRANSACTION_getAllInstallSysAppList = 18;
    
    static final int TRANSACTION_getClearAppName = 11;
    
    static final int TRANSACTION_getContainOplusCertificatePackages = 19;
    
    static final int TRANSACTION_getDetachableInstallSysAppList = 17;
    
    static final int TRANSACTION_getDisabledDeactivateMdmPackages = 4;
    
    static final int TRANSACTION_getDisallowUninstallPackageList = 9;
    
    static final int TRANSACTION_getInstallSysAppBundle = 15;
    
    static final int TRANSACTION_getPrivInstallSysAppList = 16;
    
    static final int TRANSACTION_getSuperWhiteList = 21;
    
    static final int TRANSACTION_isDisabledDeactivateMdmPackage = 5;
    
    static final int TRANSACTION_removeAllDisabledDeactivateMdmPackages = 3;
    
    static final int TRANSACTION_removeAllDisallowedUninstallPackages = 8;
    
    static final int TRANSACTION_removeDisabledDeactivateMdmPackages = 2;
    
    static final int TRANSACTION_removeDisallowedUninstallPackages = 7;
    
    static final int TRANSACTION_setAdbInstallUninstallDisabled = 12;
    
    static final int TRANSACTION_setInstallSysAppBundle = 14;
    
    static final int TRANSACTION_setSuperWhiteList = 20;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizePackageManagerService");
    }
    
    public static IOplusCustomizePackageManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizePackageManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizePackageManagerService)
        return (IOplusCustomizePackageManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 23:
          return "clearAllSuperWhiteList";
        case 22:
          return "clearSuperWhiteList";
        case 21:
          return "getSuperWhiteList";
        case 20:
          return "setSuperWhiteList";
        case 19:
          return "getContainOplusCertificatePackages";
        case 18:
          return "getAllInstallSysAppList";
        case 17:
          return "getDetachableInstallSysAppList";
        case 16:
          return "getPrivInstallSysAppList";
        case 15:
          return "getInstallSysAppBundle";
        case 14:
          return "setInstallSysAppBundle";
        case 13:
          return "getAdbInstallUninstallDisabled";
        case 12:
          return "setAdbInstallUninstallDisabled";
        case 11:
          return "getClearAppName";
        case 10:
          return "clearAppData";
        case 9:
          return "getDisallowUninstallPackageList";
        case 8:
          return "removeAllDisallowedUninstallPackages";
        case 7:
          return "removeDisallowedUninstallPackages";
        case 6:
          return "addDisallowedUninstallPackages";
        case 5:
          return "isDisabledDeactivateMdmPackage";
        case 4:
          return "getDisabledDeactivateMdmPackages";
        case 3:
          return "removeAllDisabledDeactivateMdmPackages";
        case 2:
          return "removeDisabledDeactivateMdmPackages";
        case 1:
          break;
      } 
      return "addDisabledDeactivateMdmPackages";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        ArrayList<String> arrayList;
        List<String> list5;
        Map<String, String> map;
        List<String> list4;
        Bundle bundle;
        List<String> list3;
        String str2;
        List<String> list2;
        String str1;
        boolean bool1 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 23:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            bool = clearAllSuperWhiteList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 22:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            arrayList = param1Parcel1.createStringArrayList();
            bool = clearSuperWhiteList(arrayList);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 21:
            arrayList.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            list5 = getSuperWhiteList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list5);
            return true;
          case 20:
            list5.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            list5 = list5.createStringArrayList();
            bool = setSuperWhiteList(list5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 19:
            list5.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            map = getContainOplusCertificatePackages();
            param1Parcel2.writeNoException();
            if (map == null) {
              param1Parcel2.writeInt(-1);
            } else {
              param1Parcel2.writeInt(map.size());
              map.forEach(new _$$Lambda$IOplusCustomizePackageManagerService$Stub$6u7CVUiV0seo5lfVdnLAKA_PFYI(param1Parcel2));
            } 
            return true;
          case 18:
            map.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            list4 = getAllInstallSysAppList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list4);
            return true;
          case 17:
            list4.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            list4 = getDetachableInstallSysAppList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list4);
            return true;
          case 16:
            list4.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            list4 = getPrivInstallSysAppList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list4);
            return true;
          case 15:
            list4.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            bundle = getInstallSysAppBundle();
            param1Parcel2.writeNoException();
            if (bundle != null) {
              param1Parcel2.writeInt(1);
              bundle.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 14:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            if (bundle.readInt() != 0) {
              bundle = Bundle.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              bundle = null;
            } 
            setInstallSysAppBundle(bundle);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            bool = getAdbInstallUninstallDisabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 12:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            if (bundle.readInt() != 0)
              bool1 = true; 
            setAdbInstallUninstallDisabled(bool1);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            bundle.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            list3 = getClearAppName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list3);
            return true;
          case 10:
            list3.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            str2 = list3.readString();
            clearAppData(str2);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str2.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            list2 = getDisallowUninstallPackageList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list2);
            return true;
          case 8:
            list2.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            removeAllDisallowedUninstallPackages();
            param1Parcel2.writeNoException();
            return true;
          case 7:
            list2.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            list2 = list2.createStringArrayList();
            removeDisallowedUninstallPackages(list2);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            list2.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            list2 = list2.createStringArrayList();
            addDisallowedUninstallPackages(list2);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            list2.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            str1 = list2.readString();
            bool = isDisabledDeactivateMdmPackage(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            str1.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            list1 = getDisabledDeactivateMdmPackages();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 3:
            list1.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            removeAllDisabledDeactivateMdmPackages();
            param1Parcel2.writeNoException();
            return true;
          case 2:
            list1.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
            list1 = list1.createStringArrayList();
            removeDisabledDeactivateMdmPackages(list1);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        list1.enforceInterface("android.os.customize.IOplusCustomizePackageManagerService");
        List<String> list1 = list1.createStringArrayList();
        addDisabledDeactivateMdmPackages(list1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.os.customize.IOplusCustomizePackageManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusCustomizePackageManagerService {
      public static IOplusCustomizePackageManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizePackageManagerService";
      }
      
      public void addDisabledDeactivateMdmPackages(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePackageManagerService.Stub.getDefaultImpl().addDisabledDeactivateMdmPackages(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeDisabledDeactivateMdmPackages(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePackageManagerService.Stub.getDefaultImpl().removeDisabledDeactivateMdmPackages(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeAllDisabledDeactivateMdmPackages() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePackageManagerService.Stub.getDefaultImpl().removeAllDisabledDeactivateMdmPackages();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getDisabledDeactivateMdmPackages() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePackageManagerService.Stub.getDefaultImpl().getDisabledDeactivateMdmPackages(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDisabledDeactivateMdmPackage(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizePackageManagerService.Stub.getDefaultImpl().isDisabledDeactivateMdmPackage(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addDisallowedUninstallPackages(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePackageManagerService.Stub.getDefaultImpl().addDisallowedUninstallPackages(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeDisallowedUninstallPackages(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePackageManagerService.Stub.getDefaultImpl().removeDisallowedUninstallPackages(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeAllDisallowedUninstallPackages() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePackageManagerService.Stub.getDefaultImpl().removeAllDisallowedUninstallPackages();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getDisallowUninstallPackageList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePackageManagerService.Stub.getDefaultImpl().getDisallowUninstallPackageList(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearAppData(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePackageManagerService.Stub.getDefaultImpl().clearAppData(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getClearAppName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePackageManagerService.Stub.getDefaultImpl().getClearAppName(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAdbInstallUninstallDisabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePackageManagerService.Stub.getDefaultImpl().setAdbInstallUninstallDisabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getAdbInstallUninstallDisabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizePackageManagerService.Stub.getDefaultImpl().getAdbInstallUninstallDisabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInstallSysAppBundle(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizePackageManagerService.Stub.getDefaultImpl().setInstallSysAppBundle(param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getInstallSysAppBundle() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Bundle bundle;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            bundle = IOplusCustomizePackageManagerService.Stub.getDefaultImpl().getInstallSysAppBundle();
            return bundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bundle = Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            bundle = null;
          } 
          return bundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getPrivInstallSysAppList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePackageManagerService.Stub.getDefaultImpl().getPrivInstallSysAppList(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getDetachableInstallSysAppList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePackageManagerService.Stub.getDefaultImpl().getDetachableInstallSysAppList(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAllInstallSysAppList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePackageManagerService.Stub.getDefaultImpl().getAllInstallSysAppList(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Map<String, String> getContainOplusCertificatePackages() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          HashMap<Object, Object> hashMap;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePackageManagerService.Stub.getDefaultImpl().getContainOplusCertificatePackages(); 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i < 0) {
            hashMap = null;
          } else {
            hashMap = new HashMap<>();
          } 
          IntStream intStream = IntStream.range(0, i);
          _$$Lambda$IOplusCustomizePackageManagerService$Stub$Proxy$yH_hHeEl5etiuCzUV8UZetxHgNY _$$Lambda$IOplusCustomizePackageManagerService$Stub$Proxy$yH_hHeEl5etiuCzUV8UZetxHgNY = new _$$Lambda$IOplusCustomizePackageManagerService$Stub$Proxy$yH_hHeEl5etiuCzUV8UZetxHgNY();
          this(parcel2, hashMap);
          intStream.forEach(_$$Lambda$IOplusCustomizePackageManagerService$Stub$Proxy$yH_hHeEl5etiuCzUV8UZetxHgNY);
          return (Map)hashMap;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSuperWhiteList(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(20, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizePackageManagerService.Stub.getDefaultImpl().setSuperWhiteList(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getSuperWhiteList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizePackageManagerService.Stub.getDefaultImpl().getSuperWhiteList(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean clearSuperWhiteList(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(22, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizePackageManagerService.Stub.getDefaultImpl().clearSuperWhiteList(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean clearAllSuperWhiteList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizePackageManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(23, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizePackageManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizePackageManagerService.Stub.getDefaultImpl().clearAllSuperWhiteList();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizePackageManagerService param1IOplusCustomizePackageManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizePackageManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizePackageManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizePackageManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
