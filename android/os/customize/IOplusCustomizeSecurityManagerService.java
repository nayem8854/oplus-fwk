package android.os.customize;

import android.content.ComponentName;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IOplusCustomizeSecurityManagerService extends IInterface {
  Bitmap captureFullScreen() throws RemoteException;
  
  void clearDeviceOwner(String paramString) throws RemoteException;
  
  boolean enableThirdRecord(boolean paramBoolean) throws RemoteException;
  
  String executeShellToSetIptables(String paramString) throws RemoteException;
  
  List<String> getDeviceInfo(ComponentName paramComponentName) throws RemoteException;
  
  ComponentName getDeviceOwner() throws RemoteException;
  
  List<ComponentName> getEmmAdmin() throws RemoteException;
  
  Bundle getMobileCommSettings(ComponentName paramComponentName, String paramString1, String paramString2) throws RemoteException;
  
  String getPhoneNumber(int paramInt) throws RemoteException;
  
  boolean isCustomDevicePolicyEnabled() throws RemoteException;
  
  boolean isEnableThirdRecord() throws RemoteException;
  
  boolean isTestEnvEnable() throws RemoteException;
  
  String[] listIccid() throws RemoteException;
  
  String[] listImei() throws RemoteException;
  
  boolean needLockDeadByMdm() throws RemoteException;
  
  void setCustomDevicePolicyEnabled(boolean paramBoolean) throws RemoteException;
  
  boolean setDeviceLocked(ComponentName paramComponentName) throws RemoteException;
  
  boolean setDeviceOwner(ComponentName paramComponentName) throws RemoteException;
  
  boolean setDeviceUnLocked(ComponentName paramComponentName) throws RemoteException;
  
  void setEmmAdmin(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setMobileCommSettings(ComponentName paramComponentName, String paramString, Bundle paramBundle) throws RemoteException;
  
  boolean setTestEnvEnable(boolean paramBoolean) throws RemoteException;
  
  class Default implements IOplusCustomizeSecurityManagerService {
    public void setEmmAdmin(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public List<ComponentName> getEmmAdmin() throws RemoteException {
      return null;
    }
    
    public boolean setDeviceOwner(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public ComponentName getDeviceOwner() throws RemoteException {
      return null;
    }
    
    public void clearDeviceOwner(String param1String) throws RemoteException {}
    
    public void setCustomDevicePolicyEnabled(boolean param1Boolean) throws RemoteException {}
    
    public boolean isCustomDevicePolicyEnabled() throws RemoteException {
      return false;
    }
    
    public List<String> getDeviceInfo(ComponentName param1ComponentName) throws RemoteException {
      return null;
    }
    
    public String executeShellToSetIptables(String param1String) throws RemoteException {
      return null;
    }
    
    public String getPhoneNumber(int param1Int) throws RemoteException {
      return null;
    }
    
    public Bundle getMobileCommSettings(ComponentName param1ComponentName, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public void setMobileCommSettings(ComponentName param1ComponentName, String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public Bitmap captureFullScreen() throws RemoteException {
      return null;
    }
    
    public boolean setTestEnvEnable(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isTestEnvEnable() throws RemoteException {
      return false;
    }
    
    public boolean setDeviceLocked(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setDeviceUnLocked(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean needLockDeadByMdm() throws RemoteException {
      return false;
    }
    
    public boolean enableThirdRecord(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean isEnableThirdRecord() throws RemoteException {
      return false;
    }
    
    public String[] listIccid() throws RemoteException {
      return null;
    }
    
    public String[] listImei() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizeSecurityManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizeSecurityManagerService";
    
    static final int TRANSACTION_captureFullScreen = 13;
    
    static final int TRANSACTION_clearDeviceOwner = 5;
    
    static final int TRANSACTION_enableThirdRecord = 19;
    
    static final int TRANSACTION_executeShellToSetIptables = 9;
    
    static final int TRANSACTION_getDeviceInfo = 8;
    
    static final int TRANSACTION_getDeviceOwner = 4;
    
    static final int TRANSACTION_getEmmAdmin = 2;
    
    static final int TRANSACTION_getMobileCommSettings = 11;
    
    static final int TRANSACTION_getPhoneNumber = 10;
    
    static final int TRANSACTION_isCustomDevicePolicyEnabled = 7;
    
    static final int TRANSACTION_isEnableThirdRecord = 20;
    
    static final int TRANSACTION_isTestEnvEnable = 15;
    
    static final int TRANSACTION_listIccid = 21;
    
    static final int TRANSACTION_listImei = 22;
    
    static final int TRANSACTION_needLockDeadByMdm = 18;
    
    static final int TRANSACTION_setCustomDevicePolicyEnabled = 6;
    
    static final int TRANSACTION_setDeviceLocked = 16;
    
    static final int TRANSACTION_setDeviceOwner = 3;
    
    static final int TRANSACTION_setDeviceUnLocked = 17;
    
    static final int TRANSACTION_setEmmAdmin = 1;
    
    static final int TRANSACTION_setMobileCommSettings = 12;
    
    static final int TRANSACTION_setTestEnvEnable = 14;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizeSecurityManagerService");
    }
    
    public static IOplusCustomizeSecurityManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizeSecurityManagerService)
        return (IOplusCustomizeSecurityManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 22:
          return "listImei";
        case 21:
          return "listIccid";
        case 20:
          return "isEnableThirdRecord";
        case 19:
          return "enableThirdRecord";
        case 18:
          return "needLockDeadByMdm";
        case 17:
          return "setDeviceUnLocked";
        case 16:
          return "setDeviceLocked";
        case 15:
          return "isTestEnvEnable";
        case 14:
          return "setTestEnvEnable";
        case 13:
          return "captureFullScreen";
        case 12:
          return "setMobileCommSettings";
        case 11:
          return "getMobileCommSettings";
        case 10:
          return "getPhoneNumber";
        case 9:
          return "executeShellToSetIptables";
        case 8:
          return "getDeviceInfo";
        case 7:
          return "isCustomDevicePolicyEnabled";
        case 6:
          return "setCustomDevicePolicyEnabled";
        case 5:
          return "clearDeviceOwner";
        case 4:
          return "getDeviceOwner";
        case 3:
          return "setDeviceOwner";
        case 2:
          return "getEmmAdmin";
        case 1:
          break;
      } 
      return "setEmmAdmin";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int i;
        boolean bool1;
        String[] arrayOfString;
        Bitmap bitmap;
        String str3;
        Bundle bundle;
        String str2;
        List<String> list1;
        String str1;
        ComponentName componentName1;
        List<ComponentName> list;
        ComponentName componentName2;
        String str4;
        boolean bool3 = false, bool4 = false, bool5 = false, bool6 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 22:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            arrayOfString = listImei();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 21:
            arrayOfString.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            arrayOfString = listIccid();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 20:
            arrayOfString.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            bool2 = isEnableThirdRecord();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 19:
            arrayOfString.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            if (arrayOfString.readInt() != 0)
              bool6 = true; 
            bool2 = enableThirdRecord(bool6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 18:
            arrayOfString.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            bool2 = needLockDeadByMdm();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 17:
            arrayOfString.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            if (arrayOfString.readInt() != 0) {
              ComponentName componentName = ComponentName.CREATOR.createFromParcel((Parcel)arrayOfString);
            } else {
              arrayOfString = null;
            } 
            bool2 = setDeviceUnLocked((ComponentName)arrayOfString);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 16:
            arrayOfString.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            if (arrayOfString.readInt() != 0) {
              ComponentName componentName = ComponentName.CREATOR.createFromParcel((Parcel)arrayOfString);
            } else {
              arrayOfString = null;
            } 
            bool2 = setDeviceLocked((ComponentName)arrayOfString);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 15:
            arrayOfString.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            bool2 = isTestEnvEnable();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 14:
            arrayOfString.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            bool6 = bool3;
            if (arrayOfString.readInt() != 0)
              bool6 = true; 
            bool2 = setTestEnvEnable(bool6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 13:
            arrayOfString.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            bitmap = captureFullScreen();
            param1Parcel2.writeNoException();
            if (bitmap != null) {
              param1Parcel2.writeInt(1);
              bitmap.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 12:
            bitmap.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            if (bitmap.readInt() != 0) {
              componentName2 = ComponentName.CREATOR.createFromParcel((Parcel)bitmap);
            } else {
              componentName2 = null;
            } 
            str4 = bitmap.readString();
            if (bitmap.readInt() != 0) {
              Bundle bundle1 = Bundle.CREATOR.createFromParcel((Parcel)bitmap);
            } else {
              bitmap = null;
            } 
            setMobileCommSettings(componentName2, str4, (Bundle)bitmap);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            bitmap.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            if (bitmap.readInt() != 0) {
              componentName2 = ComponentName.CREATOR.createFromParcel((Parcel)bitmap);
            } else {
              componentName2 = null;
            } 
            str4 = bitmap.readString();
            str3 = bitmap.readString();
            bundle = getMobileCommSettings(componentName2, str4, str3);
            param1Parcel2.writeNoException();
            if (bundle != null) {
              param1Parcel2.writeInt(1);
              bundle.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 10:
            bundle.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            i = bundle.readInt();
            str2 = getPhoneNumber(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 9:
            str2.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            str2 = str2.readString();
            str2 = executeShellToSetIptables(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str2);
            return true;
          case 8:
            str2.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            if (str2.readInt() != 0) {
              ComponentName componentName = ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            list1 = getDeviceInfo((ComponentName)str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 7:
            list1.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            bool1 = isCustomDevicePolicyEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 6:
            list1.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            bool6 = bool4;
            if (list1.readInt() != 0)
              bool6 = true; 
            setCustomDevicePolicyEnabled(bool6);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            list1.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            str1 = list1.readString();
            clearDeviceOwner(str1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            componentName1 = getDeviceOwner();
            param1Parcel2.writeNoException();
            if (componentName1 != null) {
              param1Parcel2.writeInt(1);
              componentName1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            componentName1.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            if (componentName1.readInt() != 0) {
              componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)componentName1);
            } else {
              componentName1 = null;
            } 
            bool1 = setDeviceOwner(componentName1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            componentName1.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
            list = getEmmAdmin();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 1:
            break;
        } 
        list.enforceInterface("android.os.customize.IOplusCustomizeSecurityManagerService");
        if (list.readInt() != 0) {
          componentName2 = ComponentName.CREATOR.createFromParcel((Parcel)list);
        } else {
          componentName2 = null;
        } 
        bool6 = bool5;
        if (list.readInt() != 0)
          bool6 = true; 
        setEmmAdmin(componentName2, bool6);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.os.customize.IOplusCustomizeSecurityManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusCustomizeSecurityManagerService {
      public static IOplusCustomizeSecurityManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizeSecurityManagerService";
      }
      
      public void setEmmAdmin(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().setEmmAdmin(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ComponentName> getEmmAdmin() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().getEmmAdmin(); 
          parcel2.readException();
          return (List)parcel2.createTypedArrayList(ComponentName.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDeviceOwner(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().setDeviceOwner(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getDeviceOwner() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ComponentName componentName;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            componentName = IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().getDeviceOwner();
            return componentName;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            componentName = ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            componentName = null;
          } 
          return componentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearDeviceOwner(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().clearDeviceOwner(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCustomDevicePolicyEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().setCustomDevicePolicyEnabled(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCustomDevicePolicyEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().isCustomDevicePolicyEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getDeviceInfo(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().getDeviceInfo(param2ComponentName); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String executeShellToSetIptables(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            param2String = IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().executeShellToSetIptables(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getPhoneNumber(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().getPhoneNumber(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getMobileCommSettings(ComponentName param2ComponentName, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().getMobileCommSettings(param2ComponentName, param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bundle bundle = Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2ComponentName = null;
          } 
          return (Bundle)param2ComponentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMobileCommSettings(ComponentName param2ComponentName, String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().setMobileCommSettings(param2ComponentName, param2String, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bitmap captureFullScreen() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Bitmap bitmap;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            bitmap = IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().captureFullScreen();
            return bitmap;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bitmap = Bitmap.CREATOR.createFromParcel(parcel2);
          } else {
            bitmap = null;
          } 
          return bitmap;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setTestEnvEnable(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().setTestEnvEnable(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTestEnvEnable() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(15, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().isTestEnvEnable();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDeviceLocked(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().setDeviceLocked(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDeviceUnLocked(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().setDeviceUnLocked(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean needLockDeadByMdm() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(18, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().needLockDeadByMdm();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enableThirdRecord(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().enableThirdRecord(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isEnableThirdRecord() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(20, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().isEnableThirdRecord();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] listIccid() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().listIccid(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] listImei() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeSecurityManagerService");
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeSecurityManagerService.Stub.getDefaultImpl().listImei(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizeSecurityManagerService param1IOplusCustomizeSecurityManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizeSecurityManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizeSecurityManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizeSecurityManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
