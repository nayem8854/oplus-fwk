package android.os.customize;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IOplusCustomizeStateManagerService extends IInterface {
  List<String> getAppRuntimeExceptionInfo() throws RemoteException;
  
  String[] getDeviceState() throws RemoteException;
  
  List<String> getRunningApplication() throws RemoteException;
  
  boolean getScreenOnStatus() throws RemoteException;
  
  boolean getSystemIntegrity() throws RemoteException;
  
  void setScreenOnStatus(boolean paramBoolean) throws RemoteException;
  
  class Default implements IOplusCustomizeStateManagerService {
    public List<String> getAppRuntimeExceptionInfo() throws RemoteException {
      return null;
    }
    
    public List<String> getRunningApplication() throws RemoteException {
      return null;
    }
    
    public String[] getDeviceState() throws RemoteException {
      return null;
    }
    
    public boolean getSystemIntegrity() throws RemoteException {
      return false;
    }
    
    public void setScreenOnStatus(boolean param1Boolean) throws RemoteException {}
    
    public boolean getScreenOnStatus() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizeStateManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizeStateManagerService";
    
    static final int TRANSACTION_getAppRuntimeExceptionInfo = 1;
    
    static final int TRANSACTION_getDeviceState = 3;
    
    static final int TRANSACTION_getRunningApplication = 2;
    
    static final int TRANSACTION_getScreenOnStatus = 6;
    
    static final int TRANSACTION_getSystemIntegrity = 4;
    
    static final int TRANSACTION_setScreenOnStatus = 5;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizeStateManagerService");
    }
    
    public static IOplusCustomizeStateManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizeStateManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizeStateManagerService)
        return (IOplusCustomizeStateManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "getScreenOnStatus";
        case 5:
          return "setScreenOnStatus";
        case 4:
          return "getSystemIntegrity";
        case 3:
          return "getDeviceState";
        case 2:
          return "getRunningApplication";
        case 1:
          break;
      } 
      return "getAppRuntimeExceptionInfo";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        String[] arrayOfString;
        boolean bool1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeStateManagerService");
            bool = getScreenOnStatus();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeStateManagerService");
            if (param1Parcel1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            setScreenOnStatus(bool1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeStateManagerService");
            bool = getSystemIntegrity();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeStateManagerService");
            arrayOfString = getDeviceState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 2:
            arrayOfString.enforceInterface("android.os.customize.IOplusCustomizeStateManagerService");
            list = getRunningApplication();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list);
            return true;
          case 1:
            break;
        } 
        list.enforceInterface("android.os.customize.IOplusCustomizeStateManagerService");
        List<String> list = getAppRuntimeExceptionInfo();
        param1Parcel2.writeNoException();
        param1Parcel2.writeStringList(list);
        return true;
      } 
      param1Parcel2.writeString("android.os.customize.IOplusCustomizeStateManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusCustomizeStateManagerService {
      public static IOplusCustomizeStateManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizeStateManagerService";
      }
      
      public List<String> getAppRuntimeExceptionInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeStateManagerService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeStateManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeStateManagerService.Stub.getDefaultImpl().getAppRuntimeExceptionInfo(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getRunningApplication() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeStateManagerService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeStateManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeStateManagerService.Stub.getDefaultImpl().getRunningApplication(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getDeviceState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeStateManagerService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeStateManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeStateManagerService.Stub.getDefaultImpl().getDeviceState(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getSystemIntegrity() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeStateManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeStateManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeStateManagerService.Stub.getDefaultImpl().getSystemIntegrity();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setScreenOnStatus(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeStateManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeStateManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeStateManagerService.Stub.getDefaultImpl().setScreenOnStatus(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getScreenOnStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeStateManagerService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeStateManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeStateManagerService.Stub.getDefaultImpl().getScreenOnStatus();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizeStateManagerService param1IOplusCustomizeStateManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizeStateManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizeStateManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizeStateManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
