package android.os.customize;

import android.content.ComponentName;
import android.content.Context;
import android.os.RemoteException;
import android.util.Slog;
import java.util.ArrayList;
import java.util.List;

public class OplusCustomizeControlerManager {
  private static final String SERVICE_NAME = "OplusCustomizeControlerManagerService";
  
  private static final String TAG = "OplusCustomizeControlerManager";
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizeControlerManager sInstance;
  
  private IOplusCustomizeControlerManagerService mOplusCustomizeControlerManagerService;
  
  private OplusCustomizeControlerManager() {
    getOplusCustomizeControlerManagerService();
  }
  
  public static final OplusCustomizeControlerManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizeControlerManager oplusCustomizeControlerManager = new OplusCustomizeControlerManager();
          this();
          sInstance = oplusCustomizeControlerManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusCustomizeControlerManagerService getOplusCustomizeControlerManagerService() {
    synchronized (mServiceLock) {
      if (this.mOplusCustomizeControlerManagerService == null)
        this.mOplusCustomizeControlerManagerService = IOplusCustomizeControlerManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizeControlerManagerService")); 
      return this.mOplusCustomizeControlerManagerService;
    } 
  }
  
  public void shutdownDevice() {
    try {
      getOplusCustomizeControlerManagerService().shutdownDevice();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "shutdownDevice Error");
    } catch (Exception exception) {
      Slog.e("OplusCustomizeControlerManager", "shutdownDevice Error exception: ", exception);
    } 
  }
  
  public void rebootDevice() {
    try {
      getOplusCustomizeControlerManagerService().rebootDevice();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "rebootDevice Error");
    } catch (Exception exception) {
      Slog.e("OplusCustomizeControlerManager", "rebootDevice Error exception", exception);
    } 
  }
  
  public void wipeDeviceData() {
    try {
      getOplusCustomizeControlerManagerService().wipeDeviceData();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "wipeDeviceData Error");
    } catch (Exception exception) {
      Slog.e("OplusCustomizeControlerManager", "wipeDeviceData Error exception", exception);
    } 
  }
  
  public boolean formatSDCard(String paramString) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      boolean bool2 = getOplusCustomizeControlerManagerService().formatSDCard(paramString);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "formatSDCard Error");
      bool = bool1;
    } catch (Exception exception) {
      Slog.e("OplusCustomizeControlerManager", "formatSDCard Error exception: ", exception);
    } 
    return bool;
  }
  
  public void enableAccessibilityService(ComponentName paramComponentName) {
    try {
      getOplusCustomizeControlerManagerService().enableAccessibilityService(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "enableAccessibilityService fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("enableAccessibilityService Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeControlerManager", stringBuilder.toString());
    } 
  }
  
  public void disableAccessibilityService(ComponentName paramComponentName) {
    try {
      getOplusCustomizeControlerManagerService().disableAccessibilityService(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "disableAccessibilityService fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("disableAccessibilityService Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeControlerManager", stringBuilder.toString());
    } 
  }
  
  public List<ComponentName> getAccessibilityService() {
    List<ComponentName> list = new ArrayList();
    try {
      List<ComponentName> list1 = getOplusCustomizeControlerManagerService().getAccessibilityService();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "getAccessibilityService fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAccessibilityService Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeControlerManager", stringBuilder.toString());
    } 
    return list;
  }
  
  public boolean isAccessibilityServiceEnabled() {
    boolean bool = false;
    boolean bool1 = false;
    try {
      boolean bool2 = getOplusCustomizeControlerManagerService().isAccessibilityServiceEnabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "isAccessibilityServiceEnabled fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isAccessibilityServiceEnabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeControlerManager", stringBuilder.toString());
      bool1 = bool;
    } 
    return bool1;
  }
  
  public String getEnabledAccessibilityServicesName() {
    String str = "";
    try {
      String str1 = getOplusCustomizeControlerManagerService().getEnabledAccessibilityServicesName();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "getEnabledAccessibilityServicesName fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getEnabledAccessibilityServicesName Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeControlerManager", stringBuilder.toString());
    } 
    return str;
  }
  
  public void addAccessibilityServiceToWhiteList(List<String> paramList) {
    try {
      getOplusCustomizeControlerManagerService().addAccessibilityServiceToWhiteList(paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "addAccessibilityServiceToWhiteList fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addAccessibilityServiceToWhiteList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeControlerManager", stringBuilder.toString());
    } 
  }
  
  public void removeAccessibilityServiceFromWhiteList(List<String> paramList) {
    try {
      getOplusCustomizeControlerManagerService().removeAccessibilityServiceFromWhiteList(paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "removeAccessibilityServiceFromWhiteList fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeAccessibilityServiceFromWhiteList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeControlerManager", stringBuilder.toString());
    } 
  }
  
  public List<String> getAccessibilityServiceWhiteList() {
    try {
      return getOplusCustomizeControlerManagerService().getAccessibilityServiceWhiteList();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "getAccessibilityServiceWhiteList fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAccessibilityServiceWhiteList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeControlerManager", stringBuilder.toString());
    } 
    return null;
  }
  
  public void deleteAccessibilityServiceWhiteList() {
    try {
      getOplusCustomizeControlerManagerService().deleteAccessibilityServiceWhiteList();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "deleteAccessibilityServiceWhiteList fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("deleteAccessibilityServiceWhiteList Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeControlerManager", stringBuilder.toString());
    } 
  }
  
  public boolean setDisableKeyguardForgetPassword(boolean paramBoolean) {
    try {
      IOplusCustomizeControlerManagerService iOplusCustomizeControlerManagerService = getOplusCustomizeControlerManagerService();
      Slog.d("OplusCustomizeControlerManager", "setDisableKeyguardForgetPassword: succeeded");
      return iOplusCustomizeControlerManagerService.setDisableKeyguardForgetPassword(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "setDisableKeyguardForgetPassword fail!", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean isDisableKeyguardForgetPassword() {
    try {
      IOplusCustomizeControlerManagerService iOplusCustomizeControlerManagerService = getOplusCustomizeControlerManagerService();
      Slog.d("OplusCustomizeControlerManager", "isDisableKeyguardForgetPassword: succeeded");
      return iOplusCustomizeControlerManagerService.isDisableKeyguardForgetPassword();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "isDisableKeyguardForgetPassword fail!", (Throwable)remoteException);
      return false;
    } 
  }
  
  public void setDisabledKeyguardPolicy(boolean paramBoolean, String paramString) {
    try {
      IOplusCustomizeControlerManagerService iOplusCustomizeControlerManagerService = getOplusCustomizeControlerManagerService();
      Slog.d("OplusCustomizeControlerManager", "setDisabledKeyguardPolicy: succeeded");
      iOplusCustomizeControlerManagerService.setDisabledKeyguardPolicy(paramBoolean, paramString);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "setDisabledKeyguardPolicy fail!", (Throwable)remoteException);
    } 
  }
  
  public boolean isDisabledKeyguardPolicy(String paramString) {
    try {
      IOplusCustomizeControlerManagerService iOplusCustomizeControlerManagerService = getOplusCustomizeControlerManagerService();
      Slog.d("OplusCustomizeControlerManager", "isDisabledKeyguardPolicy: succeeded");
      return iOplusCustomizeControlerManagerService.isDisabledKeyguardPolicy(paramString);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "isDisabledKeyguardPolicy fail!", (Throwable)remoteException);
      return false;
    } 
  }
  
  public void setAirplaneMode(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeControlerManagerService().setAirplaneMode(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setAirplaneMode error! ");
      stringBuilder.append(remoteException);
      Slog.e("OplusCustomizeControlerManager", stringBuilder.toString());
    } 
  }
  
  public boolean getAirplaneMode(ComponentName paramComponentName) {
    boolean bool = false;
    try {
      boolean bool1 = getOplusCustomizeControlerManagerService().getAirplaneMode(paramComponentName);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAirplaneMode error! e=");
      stringBuilder.append(remoteException);
      Slog.e("OplusCustomizeControlerManager", stringBuilder.toString());
    } 
    return bool;
  }
  
  public boolean setSysTime(ComponentName paramComponentName, long paramLong) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      boolean bool2 = getOplusCustomizeControlerManagerService().setSysTime(paramComponentName, paramLong);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "setSysTime fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSysTime Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeControlerManager", stringBuilder.toString());
      bool1 = bool;
    } 
    return bool1;
  }
  
  public void setCustomSettingsMenu(ComponentName paramComponentName, List<String> paramList) {
    try {
      getOplusCustomizeControlerManagerService().setCustomSettingsMenu(paramComponentName, paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeControlerManager", "setCustomSettingsMenu fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setCustomSettingsMenu Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeControlerManager", stringBuilder.toString());
    } 
  }
}
