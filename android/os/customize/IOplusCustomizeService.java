package android.os.customize;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusCustomizeService extends IInterface {
  void checkPermission() throws RemoteException;
  
  IBinder getDeviceManagerServiceByName(String paramString) throws RemoteException;
  
  boolean isPlatformSigned(int paramInt) throws RemoteException;
  
  class Default implements IOplusCustomizeService {
    public IBinder getDeviceManagerServiceByName(String param1String) throws RemoteException {
      return null;
    }
    
    public void checkPermission() throws RemoteException {}
    
    public boolean isPlatformSigned(int param1Int) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizeService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizeService";
    
    static final int TRANSACTION_checkPermission = 2;
    
    static final int TRANSACTION_getDeviceManagerServiceByName = 1;
    
    static final int TRANSACTION_isPlatformSigned = 3;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizeService");
    }
    
    public static IOplusCustomizeService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizeService");
      if (iInterface != null && iInterface instanceof IOplusCustomizeService)
        return (IOplusCustomizeService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "isPlatformSigned";
        } 
        return "checkPermission";
      } 
      return "getDeviceManagerServiceByName";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.os.customize.IOplusCustomizeService");
            return true;
          } 
          param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeService");
          param1Int1 = param1Parcel1.readInt();
          boolean bool = isPlatformSigned(param1Int1);
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(bool);
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeService");
        checkPermission();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeService");
      String str = param1Parcel1.readString();
      IBinder iBinder = getDeviceManagerServiceByName(str);
      param1Parcel2.writeNoException();
      param1Parcel2.writeStrongBinder(iBinder);
      return true;
    }
    
    private static class Proxy implements IOplusCustomizeService {
      public static IOplusCustomizeService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizeService";
      }
      
      public IBinder getDeviceManagerServiceByName(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeService.Stub.getDefaultImpl().getDeviceManagerServiceByName(param2String); 
          parcel2.readException();
          return parcel2.readStrongBinder();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void checkPermission() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeService.Stub.getDefaultImpl().checkPermission();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPlatformSigned(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeService.Stub.getDefaultImpl().isPlatformSigned(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizeService param1IOplusCustomizeService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizeService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizeService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizeService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
