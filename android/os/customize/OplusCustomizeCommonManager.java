package android.os.customize;

import android.content.Context;

public class OplusCustomizeCommonManager {
  private static final String SERVICE_NAME = "OplusCustomizeCommonManagerService";
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizeCommonManager sInstance;
  
  private IOplusCustomizeCommonManagerService mOplusCustomizeCommonManagerService;
  
  private OplusCustomizeCommonManager() {
    getOplusCustomizeCommonManagerService();
  }
  
  public static final OplusCustomizeCommonManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizeCommonManager oplusCustomizeCommonManager = new OplusCustomizeCommonManager();
          this();
          sInstance = oplusCustomizeCommonManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusCustomizeCommonManagerService getOplusCustomizeCommonManagerService() {
    synchronized (mServiceLock) {
      if (this.mOplusCustomizeCommonManagerService == null)
        this.mOplusCustomizeCommonManagerService = IOplusCustomizeCommonManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizeCommonManagerService")); 
      return this.mOplusCustomizeCommonManagerService;
    } 
  }
}
