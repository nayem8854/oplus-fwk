package android.os.customize;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IOplusCustomizeVpnManagerService extends IInterface {
  boolean deleteVpnProfile(ComponentName paramComponentName, String paramString) throws RemoteException;
  
  int disestablishVpnConnection(ComponentName paramComponentName) throws RemoteException;
  
  String getAlwaysOnVpnPackage(ComponentName paramComponentName) throws RemoteException;
  
  boolean getVpnAlwaysOnPersis(String paramString) throws RemoteException;
  
  List<String> getVpnList(ComponentName paramComponentName) throws RemoteException;
  
  int getVpnServiceState() throws RemoteException;
  
  boolean isVpnDisabled(ComponentName paramComponentName) throws RemoteException;
  
  boolean setAlwaysOnVpnPackage(ComponentName paramComponentName, String paramString, boolean paramBoolean) throws RemoteException;
  
  void setVpnAlwaysOnPersis(boolean paramBoolean) throws RemoteException;
  
  void setVpnDisabled(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  class Default implements IOplusCustomizeVpnManagerService {
    public int getVpnServiceState() throws RemoteException {
      return 0;
    }
    
    public List<String> getVpnList(ComponentName param1ComponentName) throws RemoteException {
      return null;
    }
    
    public boolean deleteVpnProfile(ComponentName param1ComponentName, String param1String) throws RemoteException {
      return false;
    }
    
    public int disestablishVpnConnection(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public void setVpnDisabled(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public boolean isVpnDisabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean setAlwaysOnVpnPackage(ComponentName param1ComponentName, String param1String, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public String getAlwaysOnVpnPackage(ComponentName param1ComponentName) throws RemoteException {
      return null;
    }
    
    public void setVpnAlwaysOnPersis(boolean param1Boolean) throws RemoteException {}
    
    public boolean getVpnAlwaysOnPersis(String param1String) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizeVpnManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizeVpnManagerService";
    
    static final int TRANSACTION_deleteVpnProfile = 3;
    
    static final int TRANSACTION_disestablishVpnConnection = 4;
    
    static final int TRANSACTION_getAlwaysOnVpnPackage = 8;
    
    static final int TRANSACTION_getVpnAlwaysOnPersis = 10;
    
    static final int TRANSACTION_getVpnList = 2;
    
    static final int TRANSACTION_getVpnServiceState = 1;
    
    static final int TRANSACTION_isVpnDisabled = 6;
    
    static final int TRANSACTION_setAlwaysOnVpnPackage = 7;
    
    static final int TRANSACTION_setVpnAlwaysOnPersis = 9;
    
    static final int TRANSACTION_setVpnDisabled = 5;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizeVpnManagerService");
    }
    
    public static IOplusCustomizeVpnManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizeVpnManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizeVpnManagerService)
        return (IOplusCustomizeVpnManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "getVpnAlwaysOnPersis";
        case 9:
          return "setVpnAlwaysOnPersis";
        case 8:
          return "getAlwaysOnVpnPackage";
        case 7:
          return "setAlwaysOnVpnPackage";
        case 6:
          return "isVpnDisabled";
        case 5:
          return "setVpnDisabled";
        case 4:
          return "disestablishVpnConnection";
        case 3:
          return "deleteVpnProfile";
        case 2:
          return "getVpnList";
        case 1:
          break;
      } 
      return "getVpnServiceState";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        String str1;
        List<String> list;
        ComponentName componentName;
        String str2;
        boolean bool3 = false, bool4 = false, bool5 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeVpnManagerService");
            str1 = param1Parcel1.readString();
            bool2 = getVpnAlwaysOnPersis(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 9:
            str1.enforceInterface("android.os.customize.IOplusCustomizeVpnManagerService");
            if (str1.readInt() != 0)
              bool5 = true; 
            setVpnAlwaysOnPersis(bool5);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str1.enforceInterface("android.os.customize.IOplusCustomizeVpnManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            str1 = getAlwaysOnVpnPackage((ComponentName)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 7:
            str1.enforceInterface("android.os.customize.IOplusCustomizeVpnManagerService");
            if (str1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              componentName = null;
            } 
            str2 = str1.readString();
            bool5 = bool3;
            if (str1.readInt() != 0)
              bool5 = true; 
            bool2 = setAlwaysOnVpnPackage(componentName, str2, bool5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 6:
            str1.enforceInterface("android.os.customize.IOplusCustomizeVpnManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            bool2 = isVpnDisabled((ComponentName)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 5:
            str1.enforceInterface("android.os.customize.IOplusCustomizeVpnManagerService");
            if (str1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              componentName = null;
            } 
            bool5 = bool4;
            if (str1.readInt() != 0)
              bool5 = true; 
            setVpnDisabled(componentName, bool5);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("android.os.customize.IOplusCustomizeVpnManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            j = disestablishVpnConnection((ComponentName)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 3:
            str1.enforceInterface("android.os.customize.IOplusCustomizeVpnManagerService");
            if (str1.readInt() != 0) {
              componentName = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              componentName = null;
            } 
            str1 = str1.readString();
            bool1 = deleteVpnProfile(componentName, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            str1.enforceInterface("android.os.customize.IOplusCustomizeVpnManagerService");
            if (str1.readInt() != 0) {
              ComponentName componentName1 = ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            list = getVpnList((ComponentName)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list);
            return true;
          case 1:
            break;
        } 
        list.enforceInterface("android.os.customize.IOplusCustomizeVpnManagerService");
        int i = getVpnServiceState();
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("android.os.customize.IOplusCustomizeVpnManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusCustomizeVpnManagerService {
      public static IOplusCustomizeVpnManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizeVpnManagerService";
      }
      
      public int getVpnServiceState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeVpnManagerService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeVpnManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeVpnManagerService.Stub.getDefaultImpl().getVpnServiceState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getVpnList(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeVpnManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeVpnManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeVpnManagerService.Stub.getDefaultImpl().getVpnList(param2ComponentName); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean deleteVpnProfile(ComponentName param2ComponentName, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeVpnManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeVpnManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeVpnManagerService.Stub.getDefaultImpl().deleteVpnProfile(param2ComponentName, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int disestablishVpnConnection(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeVpnManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeVpnManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeVpnManagerService.Stub.getDefaultImpl().disestablishVpnConnection(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVpnDisabled(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeVpnManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeVpnManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeVpnManagerService.Stub.getDefaultImpl().setVpnDisabled(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVpnDisabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeVpnManagerService");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeVpnManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeVpnManagerService.Stub.getDefaultImpl().isVpnDisabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAlwaysOnVpnPackage(ComponentName param2ComponentName, String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeVpnManagerService");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeVpnManagerService.Stub.getDefaultImpl() != null) {
            param2Boolean = IOplusCustomizeVpnManagerService.Stub.getDefaultImpl().setAlwaysOnVpnPackage(param2ComponentName, param2String, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getAlwaysOnVpnPackage(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeVpnManagerService");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeVpnManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeVpnManagerService.Stub.getDefaultImpl().getAlwaysOnVpnPackage(param2ComponentName); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVpnAlwaysOnPersis(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeVpnManagerService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool1 && IOplusCustomizeVpnManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeVpnManagerService.Stub.getDefaultImpl().setVpnAlwaysOnPersis(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getVpnAlwaysOnPersis(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeVpnManagerService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IOplusCustomizeVpnManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusCustomizeVpnManagerService.Stub.getDefaultImpl().getVpnAlwaysOnPersis(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizeVpnManagerService param1IOplusCustomizeVpnManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizeVpnManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizeVpnManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizeVpnManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
