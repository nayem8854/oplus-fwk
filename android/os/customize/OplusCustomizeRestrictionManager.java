package android.os.customize;

import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Slog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OplusCustomizeRestrictionManager {
  private IOplusCustomizeRestrictionManagerService mOplusCustomizeRestrictionManagerService = null;
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizeRestrictionManager sInstance = null;
  
  public static final int AIRPLANE_POLICY_NO_RESTRICTIONS = 2;
  
  public static final int AIRPLANE_POLICY_OFF = 3;
  
  public static final int AIRPLANE_POLICY_OFF_FORCE = 0;
  
  public static final int AIRPLANE_POLICY_ON = 4;
  
  public static final int AIRPLANE_POLICY_ON_FORCE = 1;
  
  private static final String SERVICE_NAME = "OplusCustomizeRestrictionManagerService";
  
  private static final String TAG = "OplusCustomizeRestrictionManager";
  
  public static final String mDefaultCustomizeListFilePath = "/system/etc/oppo_customize_whitelist.xml";
  
  private OplusCustomizeRestrictionManager() {
    getOplusCustomizeRestrictionManagerService();
  }
  
  public static final OplusCustomizeRestrictionManager getInstance(Context paramContext) {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          Slog.e("OplusCustomizeRestrictionManager", "sInstance is null, start a new sInstance");
          OplusCustomizeRestrictionManager oplusCustomizeRestrictionManager = new OplusCustomizeRestrictionManager();
          this();
          sInstance = oplusCustomizeRestrictionManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusCustomizeRestrictionManagerService getOplusCustomizeRestrictionManagerService() {
    synchronized (mServiceLock) {
      if (this.mOplusCustomizeRestrictionManagerService == null)
        this.mOplusCustomizeRestrictionManagerService = IOplusCustomizeRestrictionManagerService.Stub.asInterface(OplusCustomizeManager.getInstance().getDeviceManagerServiceByName("OplusCustomizeRestrictionManagerService")); 
      if (this.mOplusCustomizeRestrictionManagerService == null)
        Slog.e("OplusCustomizeRestrictionManager", "mOplusCustomizeRestrictionManagerService is null"); 
      return this.mOplusCustomizeRestrictionManagerService;
    } 
  }
  
  public int getDefaultDataCard(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().getDefaultDataCard(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getDefaultDataCard fail!", exception);
      return 0;
    } 
  }
  
  public Bundle setDefaultDataCard(ComponentName paramComponentName, int paramInt) {
    try {
      return getOplusCustomizeRestrictionManagerService().setDefaultDataCard(paramComponentName, paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setDefaultDataCard fail!", exception);
      return null;
    } 
  }
  
  public int getSlot1DataConnectivityDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().getSlot1DataConnectivityDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getSlot1DataConnectivityDisabled fail!", exception);
      return -1;
    } 
  }
  
  public int getSlot2DataConnectivityDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().getSlot2DataConnectivityDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getSlot2DataConnectivityDisabled fail!", exception);
      return -1;
    } 
  }
  
  public void setClipboardEnabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setClipboardEnabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setClipboardEnabled Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setClipboardEnabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
  }
  
  public boolean getClipboardStatus() {
    try {
      return getOplusCustomizeRestrictionManagerService().getClipboardStatus();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "getClipboardStatus Error");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getClipboardStatus Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
    return true;
  }
  
  public void setAppInstallRestrictionPolicies(int paramInt) {
    try {
      getOplusCustomizeRestrictionManagerService().setAppInstallRestrictionPolicies(paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "addInstallPackageBlacklist", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "addInstallPackageBlacklist exception: ", exception);
    } 
  }
  
  public int getAppInstallRestrictionPolicies() {
    try {
      return getOplusCustomizeRestrictionManagerService().getAppInstallRestrictionPolicies();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "addInstallPackageBlacklist", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "addInstallPackageBlacklist exception: ", exception);
      return 0;
    } finally {
      Exception exception;
    } 
    return 0;
  }
  
  public List<String> getAppInstallPackageList(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    try {
      return getOplusCustomizeRestrictionManagerService().getAppInstallPackageList(paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "getAppInstalledPackageList", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getAppInstalledPackageList exception: ", exception);
      return arrayList;
    } finally {
      Exception exception;
    } 
    return arrayList;
  }
  
  public void addAppInstallPackageBlacklist(int paramInt, List<String> paramList) {
    try {
      getOplusCustomizeRestrictionManagerService().addAppInstallPackageBlacklist(paramInt, paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "addAppInstallPackageBlacklist", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "addAppInstallPackageBlacklist exception: ", exception);
    } 
  }
  
  public void addAppInstallPackageWhitelist(int paramInt, List<String> paramList) {
    try {
      getOplusCustomizeRestrictionManagerService().addAppInstallPackageWhitelist(paramInt, paramList);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "addAppInstallPackageWhitelist", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "addAppInstallPackageWhitelist exception: ", exception);
    } 
  }
  
  public void setUSBDataDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setUSBDataDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setUSBDataDisabled Error", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setUSBDataDisabled Error exception: ", exception);
    } 
  }
  
  public boolean isUSBDataDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isUSBDataDisabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "isUSBDataDisabled Error", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isUSBDataDisabled Error exception: ", exception);
    } 
    return false;
  }
  
  public void setUSBFileTransferDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setUSBFileTransferDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setUSBFileTransferDisabled Error", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setUSBFileTransferDisabled Error exception: ", exception);
    } 
  }
  
  public boolean isUSBFileTransferDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isUSBFileTransferDisabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "isUSBFileTransferDisabled Error", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isUSBFileTransferDisabled Error exception: ", exception);
    } 
    return false;
  }
  
  public void setSafeModeDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setSafeModeDisabled(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setSafeModeDisabled error: ", exception);
    } 
  }
  
  public boolean isSafeModeDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isSafeModeDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isSafeModeDisabled error: ", exception);
      return false;
    } 
  }
  
  public void setUSBOtgDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setUSBOtgDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setUSBOtgDisabled Error", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setUSBOtgDisabled Error exception: ", exception);
    } 
  }
  
  public boolean isUSBOtgDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isUSBOtgDisabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "isUSBOtgDisabled Error", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isUSBOtgDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public void setBiometricDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setBiometricDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setBiometricDisabled Error", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setBiometricDisabled Error exception: ", exception);
    } 
  }
  
  public boolean isBiometricDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isBiometricDisabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "isBiometricDisabled Error", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isBiometricDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public void setExternalStorageDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setExternalStorageDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setExternalStorageDisabled Error", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setExternalStorageDisabled Error exception: ", exception);
    } 
  }
  
  public boolean isExternalStorageDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isExternalStorageDisabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "isExternalStorageDisabled Error", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isExternalStorageDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public void setUsbTetheringDisable(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setUsbTetheringDisable(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "setUsbTetheringDisable", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setUsbTetheringDisable Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
  }
  
  public boolean isUsbTetheringDisabled() {
    boolean bool3, bool1 = false, bool2 = false;
    try {
      bool3 = getOplusCustomizeRestrictionManagerService().isUsbTetheringDisabled();
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "isUsbTetheringDisabled", (Throwable)remoteException);
      bool3 = bool2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isUsbTetheringDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
      bool3 = bool1;
    } 
    return bool3;
  }
  
  public void setRequiredStrongAuthTime(ComponentName paramComponentName, long paramLong) {
    try {
      getOplusCustomizeRestrictionManagerService().setRequiredStrongAuthTime(paramComponentName, paramLong);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "setRequiredStrongAuthTime", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setRequiredStrongAuthTime Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
  }
  
  public long getRequiredStrongAuthTime(ComponentName paramComponentName) {
    long l = 0L;
    try {
      long l1 = getOplusCustomizeRestrictionManagerService().getRequiredStrongAuthTime(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "getRequiredStrongAuthTime", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getRequiredStrongAuthTime Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
    return l;
  }
  
  public boolean setScreenCaptureDisabled(boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setScreenCaptureDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "enableForbidRecordScreen Error");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("enableForbidRecordScreen Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean getForbidRecordScreenState() {
    try {
      return getOplusCustomizeRestrictionManagerService().getForbidRecordScreenState();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "getForbidRecordScreenState Error");
      return false;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getForbidRecordScreenState Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public void setAppUninstallationPolicies(int paramInt, List<String> paramList) {
    try {
      getOplusCustomizeRestrictionManagerService().setAppUninstallationPolicies(paramInt, paramList);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "setAppUninstallationPolicies", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setAppUninstallationPolicies Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
  }
  
  public List<String> getAppUninstallationPackageList(int paramInt) {
    List<String> list = new ArrayList();
    try {
      List<String> list1 = getOplusCustomizeRestrictionManagerService().getAppUninstallationPackageList(paramInt);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "getAppUninstallationPolicies", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAppUninstallationPolicies Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
    return list;
  }
  
  public int getAppUninstallationPolicies() {
    boolean bool = false;
    int i = 0;
    try {
      int j = getOplusCustomizeRestrictionManagerService().getAppUninstallationPolicies();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "getAppUninstallationPolicies", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getAppUninstallationPolicies exception: ", exception);
      i = bool;
    } 
    return i;
  }
  
  public void setSlot1DataConnectivityDisabled(ComponentName paramComponentName, String paramString) {
    try {
      getOplusCustomizeRestrictionManagerService().setSlot1DataConnectivityDisabled(paramComponentName, paramString);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "setSlot1DataConnectivityDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSlot1DataConnectivityDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
  }
  
  public void setSlot2DataConnectivityDisabled(ComponentName paramComponentName, String paramString) {
    try {
      getOplusCustomizeRestrictionManagerService().setSlot2DataConnectivityDisabled(paramComponentName, paramString);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "setSlot2DataConnectivityDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setSlot2DataConnectivityDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
  }
  
  public boolean isVoiceOutgoingDisabled(ComponentName paramComponentName, int paramInt) {
    boolean bool3, bool1 = false, bool2 = false;
    try {
      bool3 = getOplusCustomizeRestrictionManagerService().isVoiceOutgoingDisabled(paramComponentName, paramInt);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "isVoiceOutgoingDisabled", (Throwable)remoteException);
      bool3 = bool2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isVoiceOutgoingDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
      bool3 = bool1;
    } 
    return bool3;
  }
  
  public boolean isVoiceIncomingDisabled(ComponentName paramComponentName, int paramInt) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      boolean bool2 = getOplusCustomizeRestrictionManagerService().isVoiceIncomingDisabled(paramComponentName, paramInt);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "isVoiceIncomingDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isVoiceIncomingDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
      bool1 = bool;
    } 
    return bool1;
  }
  
  public void setVoiceOutgoingDisable(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setVoiceOutgoingDisable(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "setVoiceOutgoingDisable", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setVoiceOutgoingDisable Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
  }
  
  public void setVoiceIncomingDisable(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setVoiceIncomingDisable(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "setVoiceIncomingDisable", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setVoiceIncomingDisable Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
  }
  
  public boolean isVoiceDisabled(ComponentName paramComponentName) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      boolean bool2 = getOplusCustomizeRestrictionManagerService().isVoiceDisabled(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "setVoiceIncomingDisable", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setVoiceIncomingDisable Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
      bool1 = bool;
    } 
    return bool1;
  }
  
  public void setVoiceDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setVoiceDisabled(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "setVoiceDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setVoiceDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
  }
  
  public boolean setFloatTaskDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setFloatTaskDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setTaskButtonDisabled error!", exception);
      return false;
    } 
  }
  
  public boolean isFloatTaskDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().isFloatTaskDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isTaskButtonDisabled error!", exception);
      return false;
    } 
  }
  
  public void setPowerSavingModeDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setPowerSavingModeDisabled(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setPowerSavingModeDisabled fail!", exception);
    } 
  }
  
  public boolean isPowerSavingModeDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().isPowerSavingModeDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isPowerSavingModeDisabled fail!", exception);
      return false;
    } 
  }
  
  public boolean setDataRoamingDisabled(boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setDataRoamingDisabled(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setDataRoamingDisabled fail!", exception);
      return false;
    } 
  }
  
  public boolean isDataRoamingDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isDataRoamingDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isDataRoamingDisabled fail!", exception);
      return false;
    } 
  }
  
  public void applyQSRestriction(String paramString, int paramInt) {
    try {
      getOplusCustomizeRestrictionManagerService().applyQSRestriction(paramString, paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "applyQSRestriction Error ", exception);
    } 
  }
  
  public void disableQSRestriction(String paramString, int paramInt) {
    try {
      getOplusCustomizeRestrictionManagerService().disableQSRestriction(paramString, paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "applyQSRestriction Error ", exception);
    } 
  }
  
  public int getQSRestrictionValue(String paramString) {
    try {
      return getOplusCustomizeRestrictionManagerService().getQSRestrictionValue(paramString);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getQSRestrictionValue Error", exception);
      return 1;
    } 
  }
  
  public boolean getQSRestrictionState(String paramString, int paramInt) {
    try {
      return getOplusCustomizeRestrictionManagerService().getQSRestrictionState(paramString, paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getQSRestrictionState Error", exception);
      return false;
    } 
  }
  
  public void setBluetoothDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setBluetoothDisabled(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setBluetoothDisabled fail!");
    } 
  }
  
  public boolean isBluetoothDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isBluetoothDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isBluetoothDisabled fail!");
      return false;
    } 
  }
  
  public void setBluetoothEnabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setBluetoothEnabled(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setBluetoothDisabled fail!");
    } 
  }
  
  public boolean isBluetoothEnabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isBluetoothEnabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isBluetoothDisabled fail!");
      return false;
    } 
  }
  
  public boolean setDiscoverableDisabled(boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setDiscoverableDisabled(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setDiscoverableDisabled fail!");
      return false;
    } 
  }
  
  public boolean isDiscoverableDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isDiscoverableDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isDiscoverableDisabled fail!");
      return false;
    } 
  }
  
  public boolean setLimitedDiscoverableDisable(boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setLimitedDiscoverableDisable(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setLimitedDiscoverableDisable fail!");
      return false;
    } 
  }
  
  public boolean isLimitedDiscoverableDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isLimitedDiscoverableDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isLimitedDiscoverableDisabled fail!");
      return false;
    } 
  }
  
  public boolean setBluetoothPairingDisable(boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setBluetoothPairingDisable(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setBluetoothPairingDisable fail!");
      return false;
    } 
  }
  
  public boolean isBluetoothPairingDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isBluetoothPairingDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isBluetoothPairingDisabled fail!");
      return false;
    } 
  }
  
  public boolean setBluetoothOutGoingCallDisable(boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setBluetoothOutGoingCallDisable(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setBluetoothOutGoingCallDisable fail!");
      return false;
    } 
  }
  
  public boolean isBluetoothOutGoingCallDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isBluetoothOutGoingCallDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isBluetoothOutGoingCallDisabled fail!");
      return false;
    } 
  }
  
  public boolean setBluetoothDataTransferDisable(boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setBluetoothDataTransferDisable(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setBluetoothDataTransferDisable fail!");
      return false;
    } 
  }
  
  public boolean isBluetoothDataTransferDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isBluetoothDataTransferDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isBluetoothDataTransferDisable fail!");
      return false;
    } 
  }
  
  public boolean setBluetoothTetheringDisable(boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setBluetoothTetheringDisable(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setBluetoothTetheringDisable fail!");
      return false;
    } 
  }
  
  public boolean isBluetoothTetheringDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isBluetoothTetheringDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isBluetoothTetheringDisabled fail!");
      return false;
    } 
  }
  
  public boolean setBluetoothDisabledProfiles(List<String> paramList) {
    try {
      return getOplusCustomizeRestrictionManagerService().setBluetoothDisabledProfiles(paramList);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setBluetoothDisabledProfiles fail!");
      return false;
    } 
  }
  
  public List<String> getBluetoothDisabledProfiles() {
    try {
      return getOplusCustomizeRestrictionManagerService().getBluetoothDisabledProfiles();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getBluetoothDisabledProfiles fail!");
      return new ArrayList<>();
    } 
  }
  
  public void setNFCDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setNFCDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setNFCDisabled fail!", exception);
    } 
  }
  
  public boolean isNFCDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().isNFCDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isNFCDisabled fail!", exception);
      return false;
    } 
  }
  
  public void openCloseNFC(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().openCloseNFC(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "openCloseNFC fail!", exception);
    } 
  }
  
  public boolean isNFCTurnOn(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().isNFCTurnOn(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isNFCTurnOn fail!", exception);
      return false;
    } 
  }
  
  public boolean setAndroidBeamDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setAndroidBeamDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setAndroidBeamDisabled fail!", exception);
      return false;
    } 
  }
  
  public boolean isAndroidBeamDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().isAndroidBeamDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isAndroidBeamDisabled fail!", exception);
      return false;
    } 
  }
  
  public boolean setNfcPolicies(ComponentName paramComponentName, int paramInt) {
    try {
      return getOplusCustomizeRestrictionManagerService().setNfcPolicies(paramComponentName, paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setNfcPolicies fail!", exception);
      return false;
    } 
  }
  
  public int getNfcPolicies(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().getNfcPolicies(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getNfcPolicies fail!", exception);
      return 2;
    } 
  }
  
  public void setMobileDataMode(ComponentName paramComponentName, int paramInt) {
    try {
      getOplusCustomizeRestrictionManagerService().setMobileDataMode(paramComponentName, paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setMobileDataMode error!");
    } 
  }
  
  public int getMobileDataMode(ComponentName paramComponentName) {
    int i = -1;
    try {
      int j = getOplusCustomizeRestrictionManagerService().getMobileDataMode(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "getMobileDataMode error!");
    } 
    return i;
  }
  
  public boolean allowWifiCellularNetwork(ComponentName paramComponentName, String paramString) {
    boolean bool = false;
    try {
      boolean bool1 = getOplusCustomizeRestrictionManagerService().allowWifiCellularNetwork(paramComponentName, paramString);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "allowWifiCellularNetwork error!");
    } 
    return bool;
  }
  
  public boolean setCameraPolicies(int paramInt) {
    try {
      return getOplusCustomizeRestrictionManagerService().setCameraPolicies(paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setCameraPolicies fail!", exception);
      return false;
    } 
  }
  
  public int getCameraPolicies() {
    try {
      return getOplusCustomizeRestrictionManagerService().getCameraPolicies();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getCameraPolicies fail!", exception);
      return 0;
    } 
  }
  
  public boolean setTorchPolicies(int paramInt) {
    try {
      return getOplusCustomizeRestrictionManagerService().setTorchPolicies(paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setTorchPolicies fail!", exception);
      return false;
    } 
  }
  
  public int getTorchPolicies() {
    try {
      return getOplusCustomizeRestrictionManagerService().getTorchPolicies();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getTorchPolicies fail!", exception);
      return 0;
    } 
  }
  
  public void setChangeWallpaperDisable(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setChangeWallpaperDisable(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setChangeWallpaperDisable fail!", exception);
    } 
  }
  
  public boolean isChangeWallpaperDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().isChangeWallpaperDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isChangeWallpaperDisabled fail!", exception);
      return false;
    } 
  }
  
  public void setSettingsApplicationDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setSettingsApplicationDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setSettingsApplicationDisabled fail!", exception);
    } 
  }
  
  public boolean isSettingsApplicationDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().isSettingsApplicationDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isSettingsApplicationDisabled fail!", exception);
      return false;
    } 
  }
  
  public void setApplicationDisabledInLauncherOrRecentTask(List<String> paramList, int paramInt) {
    try {
      getOplusCustomizeRestrictionManagerService().setApplicationDisabledInLauncherOrRecentTask(paramList, paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setApplicationDisabledInLauncherOrRecentTask fail!", exception);
    } 
  }
  
  public List<String> getApplicationDisabledInLauncherOrRecentTask(int paramInt) {
    try {
      return getOplusCustomizeRestrictionManagerService().getApplicationDisabledInLauncherOrRecentTask(paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getApplicationDisabledInLauncherOrRecentTask fail!", exception);
      return Collections.emptyList();
    } 
  }
  
  public boolean isMultiAppSupport() {
    try {
      return getOplusCustomizeRestrictionManagerService().isMultiAppSupport();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "isMultiAppSupport Error", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isMultiAppSupport Error exception: ", exception);
    } 
    return false;
  }
  
  public void setMultiAppSupport(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setMultiAppSupport(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setMultiAppSupport Error", (Throwable)remoteException);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setMultiAppSupport Error exception: ", exception);
    } 
  }
  
  public boolean setAirplanePolices(ComponentName paramComponentName, int paramInt) {
    try {
      return getOplusCustomizeRestrictionManagerService().setAirplanePolices(paramComponentName, paramInt);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setAirplanePolices error!", (Throwable)remoteException);
      return false;
    } 
  }
  
  public int getAirplanePolices(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().getAirplanePolices(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "getAirplanePolices error!", (Throwable)remoteException);
      return -1;
    } 
  }
  
  public boolean setSplitScreenDisable(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setSplitScreenDisable(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setSplitScreenDisable error!", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean getSplitScreenDisable(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().getSplitScreenDisable(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "getSplitScreenDisable error!", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean setUserPasswordPolicies(ComponentName paramComponentName, int paramInt) {
    try {
      return getOplusCustomizeRestrictionManagerService().setUserPasswordPolicies(paramComponentName, paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setUserPasswordPolicies fail!", exception);
      return false;
    } 
  }
  
  public int getUserPasswordPolicies(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().getUserPasswordPolicies(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getUserPasswordPolicies fail!", exception);
      return 0;
    } 
  }
  
  public boolean setUnlockByFingerprintPolicies(ComponentName paramComponentName, int paramInt) {
    try {
      return getOplusCustomizeRestrictionManagerService().setUnlockByFingerprintPolicies(paramComponentName, paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setUnlockByFingerprintPolicies fail!", exception);
      return false;
    } 
  }
  
  public int getUnlockByFingerprintPolicies(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().getUnlockByFingerprintPolicies(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getUnlockByFingerprintPolicies fail!", exception);
      return 0;
    } 
  }
  
  public boolean setUnlockByFacePolicies(ComponentName paramComponentName, int paramInt) {
    try {
      return getOplusCustomizeRestrictionManagerService().setUnlockByFacePolicies(paramComponentName, paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setUnlockByFacePolicies fail!", exception);
      return false;
    } 
  }
  
  public int getUnlockByFacePolicies(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().getUnlockByFacePolicies(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getUnlockByFacePolicies fail!", exception);
      return 0;
    } 
  }
  
  public boolean setUnlockByFingerprintDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setUnlockByFingerprintDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setUnlockByFingerprintDisabled fail!", exception);
      return false;
    } 
  }
  
  public boolean isUnlockByFingerprintDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().isUnlockByFingerprintDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isUnlockByFingerprintDisabled fail!", exception);
      return false;
    } 
  }
  
  public boolean setUnlockByFaceDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setUnlockByFaceDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setUnlockByFaceDisabled fail!", exception);
      return false;
    } 
  }
  
  public boolean isUnlockByFaceDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().isUnlockByFaceDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isUnlockByFaceDisabled fail!", exception);
      return false;
    } 
  }
  
  public void setTaskButtonDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setTaskButtonDisabled(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setTaskButtonDisabled error: ", exception);
    } 
  }
  
  public boolean isTaskButtonDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isTaskButtonDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isTaskButtonDisabled error: ", exception);
      return false;
    } 
  }
  
  public void setHomeButtonDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setHomeButtonDisabled(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setHomeButtonDisabled error: ", exception);
    } 
  }
  
  public boolean isHomeButtonDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isHomeButtonDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isHomeButtonDisabled error: ", exception);
      return false;
    } 
  }
  
  public void setBackButtonDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setBackButtonDisabled(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setBackButtonDisabled error: ", exception);
    } 
  }
  
  public boolean isBackButtonDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isBackButtonDisabled();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isBackButtonDisabled error: ", exception);
      return false;
    } 
  }
  
  public void setPowerDisable(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setPowerDisable(paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setPowerDisable error: ", exception);
    } 
  }
  
  public boolean getPowerDisable() {
    try {
      return getOplusCustomizeRestrictionManagerService().getPowerDisable();
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getPowerDisable error: ", exception);
      return false;
    } 
  }
  
  public boolean setGpsPolicies(ComponentName paramComponentName, int paramInt) {
    try {
      return getOplusCustomizeRestrictionManagerService().setGpsPolicies(paramComponentName, paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setGpsPolicies Error", exception);
      return false;
    } 
  }
  
  public int getGpsPolicies(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().getGpsPolicies(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getQSRestrictionState Error", exception);
      return 0;
    } 
  }
  
  public void setLanguageChangeDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setLanguageChangeDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.i("OplusCustomizeRestrictionManager", "setLanguageChangeDisabled error!");
      exception.printStackTrace();
    } 
  }
  
  public boolean isLanguageChangeDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().isLanguageChangeDisabled(paramComponentName);
    } catch (RemoteException remoteException) {
      Slog.i("OplusCustomizeRestrictionManager", "setLanguageChangeDisabled error!");
      remoteException.printStackTrace();
      return false;
    } 
  }
  
  public boolean isWifiOpen(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().isWifiOpen(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isWifiOpen fail!", exception);
      return false;
    } 
  }
  
  public boolean setWifiInBackground(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      return getOplusCustomizeRestrictionManagerService().setWifiInBackground(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setWifiInBackground error!");
      return false;
    } 
  }
  
  public boolean isWifiDisabled(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().isWifiDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isWifiDisabled fail!", exception);
      return false;
    } 
  }
  
  public void setWifiDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setWifiDisabled(paramComponentName, paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setWifiDisabled error!");
    } 
  }
  
  public boolean setWifiApDisabled(boolean paramBoolean) {
    boolean bool1 = false, bool2 = false;
    try {
      paramBoolean = getOplusCustomizeRestrictionManagerService().setWifiApDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setWifiApDisabled", (Throwable)remoteException);
      paramBoolean = bool2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setWifiApDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
      paramBoolean = bool1;
    } 
    return paramBoolean;
  }
  
  public boolean isWifiApDisabled() {
    boolean bool = false;
    boolean bool1 = false;
    try {
      boolean bool2 = getOplusCustomizeRestrictionManagerService().isWifiApDisabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "isWifiApDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isWifiApDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
      bool1 = bool;
    } 
    return bool1;
  }
  
  public boolean setWifiP2pDisabled(boolean paramBoolean) {
    boolean bool1 = false, bool2 = false;
    try {
      paramBoolean = getOplusCustomizeRestrictionManagerService().setWifiP2pDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "setWifiP2pDisabled", (Throwable)remoteException);
      paramBoolean = bool2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setWifiP2pDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
      paramBoolean = bool1;
    } 
    return paramBoolean;
  }
  
  public boolean isWifiP2pDisabled() {
    boolean bool3, bool1 = false, bool2 = false;
    try {
      bool3 = getOplusCustomizeRestrictionManagerService().isWifiP2pDisabled();
    } catch (RemoteException remoteException) {
      Slog.e("OplusCustomizeRestrictionManager", "isWifiP2pDisabled", (Throwable)remoteException);
      bool3 = bool2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isWifiP2pDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
      bool3 = bool1;
    } 
    return bool3;
  }
  
  public void setMmsDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setMmsDisabled(paramBoolean);
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "setMmsDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setMmsDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
  }
  
  public boolean isMmsDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isMmsDisabled();
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "isMmsDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isMmsDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public boolean isSmsReceiveDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isSmsReceiveDisabled();
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "isSmsReceiveDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isSmsReceiveDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public boolean isSmsSendDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isSmsSendDisabled();
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "isSmsSendDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isSmsSendDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public boolean isMmsSendReceiveDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isMmsSendReceiveDisabled();
    } catch (RemoteException remoteException) {
      Slog.d("OplusCustomizeRestrictionManager", "isMmsSendReceiveDisabled", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isMmsSendReceiveDisabled Error");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
    return false;
  }
  
  public void setUsbDebugSwitchDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setUsbDebugSwitchDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setUsbDebugSwitchDisabled fail!", exception);
    } 
  }
  
  public boolean isUsbDebugSwitchDisabled(ComponentName paramComponentName) {
    boolean bool2, bool1 = false;
    try {
      bool2 = getOplusCustomizeRestrictionManagerService().isUsbDebugSwitchDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isUsbDebugSwitchDisabled fail!", exception);
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public void setAdbDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setAdbDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setAdbDisabled fail!", exception);
    } 
  }
  
  public boolean isAdbDisabled(ComponentName paramComponentName) {
    boolean bool = false;
    try {
      boolean bool1 = getOplusCustomizeRestrictionManagerService().isAdbDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isAdbDisabled fail!", exception);
    } 
    return bool;
  }
  
  public void setNavigationBarDisabled(boolean paramBoolean) {
    try {
      getOplusCustomizeRestrictionManagerService().setNavigationBarDisabled(paramBoolean);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setNavigationBarDisabled: ");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
    } 
  }
  
  public boolean isNavigationBarDisabled() {
    try {
      return getOplusCustomizeRestrictionManagerService().isNavigationBarDisabled();
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isNavigationBarDisabled error: ");
      stringBuilder.append(exception);
      Slog.e("OplusCustomizeRestrictionManager", stringBuilder.toString());
      return false;
    } 
  }
  
  public boolean setSystemUpdatePolicies(ComponentName paramComponentName, int paramInt) {
    boolean bool = false;
    try {
      boolean bool1 = getOplusCustomizeRestrictionManagerService().setSystemUpdatePolicies(paramComponentName, paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setSystemUpdatePolicies fail!", exception);
    } 
    return bool;
  }
  
  public int getSystemUpdatePolicies(ComponentName paramComponentName) {
    byte b2, b1 = -1;
    try {
      b2 = getOplusCustomizeRestrictionManagerService().getSystemUpdatePolicies(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getSystemUpdatePolicies fail!", exception);
      b2 = b1;
    } 
    return b2;
  }
  
  public boolean setUnknownSourceAppInstallDisabled(ComponentName paramComponentName, boolean paramBoolean) {
    boolean bool = false;
    try {
      paramBoolean = getOplusCustomizeRestrictionManagerService().setUnknownSourceAppInstallDisabled(paramComponentName, paramBoolean);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setUnknownSourceAppInstallDisabled fail!", exception);
      paramBoolean = bool;
    } 
    return paramBoolean;
  }
  
  public boolean isUnknownSourceAppInstallDisabled(ComponentName paramComponentName) {
    boolean bool2, bool1 = false;
    try {
      bool2 = getOplusCustomizeRestrictionManagerService().isUnknownSourceAppInstallDisabled(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "isUnknownSourceAppInstallDisabled fail!", exception);
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public boolean setSideBarPolicies(ComponentName paramComponentName, int paramInt) {
    try {
      return getOplusCustomizeRestrictionManagerService().setSideBarPolicies(paramComponentName, paramInt);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "setSideBarPolicies Error", exception);
      return false;
    } 
  }
  
  public int getSideBarPolicies(ComponentName paramComponentName) {
    try {
      return getOplusCustomizeRestrictionManagerService().getSideBarPolicies(paramComponentName);
    } catch (Exception exception) {
      Slog.e("OplusCustomizeRestrictionManager", "getSideBarPolicies Error", exception);
      return 0;
    } 
  }
}
