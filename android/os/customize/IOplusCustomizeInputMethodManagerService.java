package android.os.customize;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusCustomizeInputMethodManagerService extends IInterface {
  void clearDefaultInputMethod() throws RemoteException;
  
  String getDefaultInputMethod() throws RemoteException;
  
  void setDefaultInputMethod(String paramString) throws RemoteException;
  
  class Default implements IOplusCustomizeInputMethodManagerService {
    public void setDefaultInputMethod(String param1String) throws RemoteException {}
    
    public String getDefaultInputMethod() throws RemoteException {
      return null;
    }
    
    public void clearDefaultInputMethod() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusCustomizeInputMethodManagerService {
    private static final String DESCRIPTOR = "android.os.customize.IOplusCustomizeInputMethodManagerService";
    
    static final int TRANSACTION_clearDefaultInputMethod = 3;
    
    static final int TRANSACTION_getDefaultInputMethod = 2;
    
    static final int TRANSACTION_setDefaultInputMethod = 1;
    
    public Stub() {
      attachInterface(this, "android.os.customize.IOplusCustomizeInputMethodManagerService");
    }
    
    public static IOplusCustomizeInputMethodManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.customize.IOplusCustomizeInputMethodManagerService");
      if (iInterface != null && iInterface instanceof IOplusCustomizeInputMethodManagerService)
        return (IOplusCustomizeInputMethodManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "clearDefaultInputMethod";
        } 
        return "getDefaultInputMethod";
      } 
      return "setDefaultInputMethod";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.os.customize.IOplusCustomizeInputMethodManagerService");
            return true;
          } 
          param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeInputMethodManagerService");
          clearDefaultInputMethod();
          param1Parcel2.writeNoException();
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.customize.IOplusCustomizeInputMethodManagerService");
        str = getDefaultInputMethod();
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str);
        return true;
      } 
      str.enforceInterface("android.os.customize.IOplusCustomizeInputMethodManagerService");
      String str = str.readString();
      setDefaultInputMethod(str);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOplusCustomizeInputMethodManagerService {
      public static IOplusCustomizeInputMethodManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.customize.IOplusCustomizeInputMethodManagerService";
      }
      
      public void setDefaultInputMethod(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeInputMethodManagerService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeInputMethodManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeInputMethodManagerService.Stub.getDefaultImpl().setDefaultInputMethod(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDefaultInputMethod() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeInputMethodManagerService");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeInputMethodManagerService.Stub.getDefaultImpl() != null)
            return IOplusCustomizeInputMethodManagerService.Stub.getDefaultImpl().getDefaultInputMethod(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearDefaultInputMethod() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.customize.IOplusCustomizeInputMethodManagerService");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusCustomizeInputMethodManagerService.Stub.getDefaultImpl() != null) {
            IOplusCustomizeInputMethodManagerService.Stub.getDefaultImpl().clearDefaultInputMethod();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusCustomizeInputMethodManagerService param1IOplusCustomizeInputMethodManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusCustomizeInputMethodManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusCustomizeInputMethodManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusCustomizeInputMethodManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
