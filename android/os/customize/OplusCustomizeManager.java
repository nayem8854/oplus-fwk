package android.os.customize;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.ArrayMap;
import android.util.Log;

public class OplusCustomizeManager {
  public static final String SERVICE_NAME = "opluscustomize";
  
  private static final String TAG = "OplusCustomizeManager";
  
  private static final ArrayMap<String, IBinder> mDeviceManagerCache;
  
  private static final Object mLock = new Object();
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusCustomizeManager sInstance;
  
  private IOplusCustomizeService mCustService;
  
  static {
    mDeviceManagerCache = new ArrayMap();
  }
  
  public static final OplusCustomizeManager getInstance() {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusCustomizeManager oplusCustomizeManager = new OplusCustomizeManager();
          this();
          sInstance = oplusCustomizeManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  public boolean isPlatformSigned(int paramInt) {
    boolean bool = false;
    getOplusCustomizeManagerService();
    IOplusCustomizeService iOplusCustomizeService = this.mCustService;
    if (iOplusCustomizeService != null) {
      try {
        boolean bool1 = iOplusCustomizeService.isPlatformSigned(paramInt);
      } catch (RemoteException remoteException) {
        Log.i("OplusCustomizeManager", "getOplusMdmManager error!");
        remoteException.printStackTrace();
      } 
      return bool;
    } 
    throw new SecurityException("OplusCustomizeService is not ready");
  }
  
  public void checkPermission() throws SecurityException {
    getOplusCustomizeManagerService();
    IOplusCustomizeService iOplusCustomizeService = this.mCustService;
    if (iOplusCustomizeService != null) {
      try {
        iOplusCustomizeService.checkPermission();
      } catch (RemoteException remoteException) {
        Log.i("OplusCustomizeManager", "getOplusMdmManager error!");
        remoteException.printStackTrace();
      } 
      return;
    } 
    throw new SecurityException("OplusCustomizeService is not ready");
  }
  
  private IOplusCustomizeService getOplusCustomizeManagerService() {
    synchronized (mServiceLock) {
      if (this.mCustService == null)
        this.mCustService = IOplusCustomizeService.Stub.asInterface(ServiceManager.getService("opluscustomize")); 
      return this.mCustService;
    } 
  }
  
  protected final IBinder getDeviceManagerServiceByName(String paramString) {
    synchronized (mDeviceManagerCache) {
      getOplusCustomizeManagerService();
      IOplusCustomizeService iOplusCustomizeService = this.mCustService;
      if (iOplusCustomizeService != null)
        try {
          IBinder iBinder1;
          if (mDeviceManagerCache.containsKey(paramString)) {
            iBinder1 = (IBinder)mDeviceManagerCache.get(paramString);
            return iBinder1;
          } 
          IBinder iBinder2 = this.mCustService.getDeviceManagerServiceByName((String)iBinder1);
          if (iBinder2 != null)
            mDeviceManagerCache.put(iBinder1, iBinder2); 
          return iBinder2;
        } catch (RemoteException remoteException) {
          Log.i("OplusCustomizeManager", "getOplusMdmManager error!");
          remoteException.printStackTrace();
        }  
      return null;
    } 
  }
}
