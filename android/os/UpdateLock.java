package android.os;

import android.util.Log;

public class UpdateLock {
  private static void checkService() {
    if (sService == null) {
      IBinder iBinder = ServiceManager.getService("updatelock");
      sService = IUpdateLock.Stub.asInterface(iBinder);
    } 
  }
  
  int mCount = 0;
  
  boolean mRefCounted = true;
  
  boolean mHeld = false;
  
  private static final boolean DEBUG = false;
  
  public static final String NOW_IS_CONVENIENT = "nowisconvenient";
  
  private static final String TAG = "UpdateLock";
  
  public static final String TIMESTAMP = "timestamp";
  
  public static final String UPDATE_LOCK_CHANGED = "android.os.UpdateLock.UPDATE_LOCK_CHANGED";
  
  private static IUpdateLock sService;
  
  final String mTag;
  
  IBinder mToken;
  
  public UpdateLock(String paramString) {
    this.mTag = paramString;
    this.mToken = new Binder();
  }
  
  public void setReferenceCounted(boolean paramBoolean) {
    this.mRefCounted = paramBoolean;
  }
  
  public boolean isHeld() {
    synchronized (this.mToken) {
      return this.mHeld;
    } 
  }
  
  public void acquire() {
    checkService();
    synchronized (this.mToken) {
      acquireLocked();
      return;
    } 
  }
  
  private void acquireLocked() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mRefCounted : Z
    //   4: ifeq -> 23
    //   7: aload_0
    //   8: getfield mCount : I
    //   11: istore_1
    //   12: aload_0
    //   13: iload_1
    //   14: iconst_1
    //   15: iadd
    //   16: putfield mCount : I
    //   19: iload_1
    //   20: ifne -> 62
    //   23: getstatic android/os/UpdateLock.sService : Landroid/os/IUpdateLock;
    //   26: astore_2
    //   27: aload_2
    //   28: ifnull -> 57
    //   31: aload_2
    //   32: aload_0
    //   33: getfield mToken : Landroid/os/IBinder;
    //   36: aload_0
    //   37: getfield mTag : Ljava/lang/String;
    //   40: invokeinterface acquireUpdateLock : (Landroid/os/IBinder;Ljava/lang/String;)V
    //   45: goto -> 57
    //   48: astore_2
    //   49: ldc 'UpdateLock'
    //   51: ldc 'Unable to contact service to acquire'
    //   53: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   56: pop
    //   57: aload_0
    //   58: iconst_1
    //   59: putfield mHeld : Z
    //   62: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #119	-> 0
    //   #120	-> 23
    //   #122	-> 31
    //   #125	-> 45
    //   #123	-> 48
    //   #124	-> 49
    //   #127	-> 57
    //   #129	-> 62
    // Exception table:
    //   from	to	target	type
    //   31	45	48	android/os/RemoteException
  }
  
  public void release() {
    checkService();
    synchronized (this.mToken) {
      releaseLocked();
      return;
    } 
  }
  
  private void releaseLocked() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mRefCounted : Z
    //   4: ifeq -> 23
    //   7: aload_0
    //   8: getfield mCount : I
    //   11: iconst_1
    //   12: isub
    //   13: istore_1
    //   14: aload_0
    //   15: iload_1
    //   16: putfield mCount : I
    //   19: iload_1
    //   20: ifne -> 58
    //   23: getstatic android/os/UpdateLock.sService : Landroid/os/IUpdateLock;
    //   26: astore_2
    //   27: aload_2
    //   28: ifnull -> 53
    //   31: aload_2
    //   32: aload_0
    //   33: getfield mToken : Landroid/os/IBinder;
    //   36: invokeinterface releaseUpdateLock : (Landroid/os/IBinder;)V
    //   41: goto -> 53
    //   44: astore_2
    //   45: ldc 'UpdateLock'
    //   47: ldc 'Unable to contact service to release'
    //   49: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   52: pop
    //   53: aload_0
    //   54: iconst_0
    //   55: putfield mHeld : Z
    //   58: aload_0
    //   59: getfield mCount : I
    //   62: iflt -> 66
    //   65: return
    //   66: new java/lang/RuntimeException
    //   69: dup
    //   70: ldc 'UpdateLock under-locked'
    //   72: invokespecial <init> : (Ljava/lang/String;)V
    //   75: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #144	-> 0
    //   #145	-> 23
    //   #147	-> 31
    //   #150	-> 41
    //   #148	-> 44
    //   #149	-> 45
    //   #152	-> 53
    //   #154	-> 58
    //   #157	-> 65
    //   #155	-> 66
    // Exception table:
    //   from	to	target	type
    //   31	41	44	android/os/RemoteException
  }
  
  protected void finalize() throws Throwable {
    synchronized (this.mToken) {
      if (this.mHeld) {
        Log.wtf("UpdateLock", "UpdateLock finalized while still held");
        try {
          sService.releaseUpdateLock(this.mToken);
        } catch (RemoteException remoteException) {
          Log.e("UpdateLock", "Unable to contact service to release");
        } 
      } 
      return;
    } 
  }
}
