package android.os;

import android.util.Log;
import vendor.oplus.hardware.stability.oplus_project.V1_0.IOplusProject;

public class ProjectManager {
  private static final String TAG = "ProjectManager";
  
  private static IOplusProject sProjectService = null;
  
  private static IOplusProject getProjectService() {
    if (sProjectService == null)
      try {
        sProjectService = IOplusProject.getService();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("GetProject() : ");
        stringBuilder.append(getProject());
        stringBuilder.append(", GetPcbVersion() : ");
        stringBuilder.append(getPcbVersion());
        stringBuilder.append(", GetSerialID() : ");
        stringBuilder.append(getSerialID());
        stringBuilder.append(", GetOperatorName() : ");
        stringBuilder.append(getOperatorName());
        stringBuilder.append(", GetRFType() : ");
        stringBuilder.append(getRFType());
        stringBuilder.append(", GetEngVersion() : ");
        stringBuilder.append(getEngVersion());
        String str = stringBuilder.toString();
        Log.i("ProjectManager", str);
      } catch (Exception exception) {
        Log.e("ProjectManager", "Failed to get oplus project hal service", exception);
      }  
    return sProjectService;
  }
  
  public static int getProject() {
    try {
      IOplusProject iOplusProject = getProjectService();
      if (iOplusProject != null)
        return iOplusProject.get_project(); 
    } catch (RemoteException remoteException) {
      Log.e("ProjectManager", "get_project() failed.", (Throwable)remoteException);
    } 
    return 0;
  }
  
  public static int getPcbVersion() {
    try {
      IOplusProject iOplusProject = getProjectService();
      if (iOplusProject != null)
        return iOplusProject.get_pcb_version(); 
    } catch (RemoteException remoteException) {
      Log.e("ProjectManager", "get_pcb_version() failed.", (Throwable)remoteException);
    } 
    return 0;
  }
  
  public static String getSerialID() {
    try {
      IOplusProject iOplusProject = getProjectService();
      if (iOplusProject != null)
        return iOplusProject.get_serial_ID(); 
    } catch (RemoteException remoteException) {
      Log.e("ProjectManager", "get_serial_ID() failed.", (Throwable)remoteException);
    } 
    return null;
  }
  
  public static int getOperatorName() {
    try {
      IOplusProject iOplusProject = getProjectService();
      if (iOplusProject != null)
        return iOplusProject.get_operator_name(); 
    } catch (RemoteException remoteException) {
      Log.e("ProjectManager", "get_operator_name() failed.", (Throwable)remoteException);
    } 
    return 0;
  }
  
  public static int getRFType() {
    try {
      IOplusProject iOplusProject = getProjectService();
      if (iOplusProject != null)
        return iOplusProject.get_rf_type(); 
    } catch (RemoteException remoteException) {
      Log.e("ProjectManager", "get_rf_type() failed.", (Throwable)remoteException);
    } 
    return 0;
  }
  
  public static int getEngVersion() {
    try {
      IOplusProject iOplusProject = getProjectService();
      if (iOplusProject != null)
        return iOplusProject.get_eng_version(); 
    } catch (RemoteException remoteException) {
      Log.e("ProjectManager", "get_eng_version() failed.", (Throwable)remoteException);
    } 
    return 0;
  }
}
