package android.os;

import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefMethod;

public class OplusMirrorLinearMotorVibratorManager {
  public static Class<?> TYPE = RefClass.load(OplusMirrorLinearMotorVibratorManager.class, "android.os.LinearMotorVibratorManager");
  
  public static RefMethod<Void> turnOffLinearMotorVibrator;
  
  public static RefMethod<Void> turnOnLinearmotorVibrator;
}
