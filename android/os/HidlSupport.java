package android.os;

import android.annotation.SystemApi;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@SystemApi
public class HidlSupport {
  @SystemApi
  public static boolean deepEquals(Object paramObject1, Object paramObject2) {
    boolean bool = true;
    if (paramObject1 == paramObject2)
      return true; 
    if (paramObject1 == null || paramObject2 == null)
      return false; 
    Class<?> clazz1 = paramObject1.getClass();
    Class<?> clazz2 = paramObject2.getClass();
    if (clazz1 != clazz2)
      return false; 
    if (clazz1.isArray()) {
      clazz1 = clazz1.getComponentType();
      if (clazz1 != clazz2.getComponentType())
        return false; 
      if (clazz1.isPrimitive())
        return Objects.deepEquals(paramObject1, paramObject2); 
      paramObject1 = paramObject1;
      paramObject2 = paramObject2;
      int i = paramObject1.length;
      if (paramObject1.length != paramObject2.length || 
        !IntStream.range(0, i).allMatch(new _$$Lambda$HidlSupport$4ktYtLCfMafhYI23iSXUQOH_hxo((Object[])paramObject1, (Object[])paramObject2)))
        bool = false; 
      return bool;
    } 
    if (paramObject1 instanceof List) {
      paramObject1 = paramObject1;
      paramObject2 = paramObject2;
      if (paramObject1.size() != paramObject2.size())
        return false; 
      paramObject1 = paramObject1.iterator();
      paramObject2 = paramObject2.stream();
      paramObject1 = new _$$Lambda$HidlSupport$oV2DlGQSAfcavBj7TK20nYhwS0U((Iterator)paramObject1);
      return 
        paramObject2.allMatch((Predicate)paramObject1);
    } 
    throwErrorIfUnsupportedType(paramObject1);
    return paramObject1.equals(paramObject2);
  }
  
  public static final class Mutable<E> {
    public E value;
    
    public Mutable() {
      this.value = null;
    }
    
    public Mutable(E param1E) {
      this.value = param1E;
    }
  }
  
  @SystemApi
  public static int deepHashCode(Object paramObject) {
    if (paramObject == null)
      return 0; 
    Class<?> clazz = paramObject.getClass();
    if (clazz.isArray()) {
      clazz = clazz.getComponentType();
      if (clazz.isPrimitive())
        return primitiveArrayHashCode(paramObject); 
      Stream stream = Arrays.stream((Object[])paramObject);
      paramObject = _$$Lambda$HidlSupport$GHxmwrIWiKN83tl6aMQt_nV5hiw.INSTANCE;
      paramObject = stream.mapToInt((ToIntFunction)paramObject);
      paramObject = paramObject.toArray();
      return Arrays.hashCode((int[])paramObject);
    } 
    if (paramObject instanceof List) {
      Stream stream = ((List)paramObject).stream();
      paramObject = _$$Lambda$HidlSupport$CwwfmHPEvZaybUxpLzKdwrpQRfA.INSTANCE;
      paramObject = stream.mapToInt((ToIntFunction)paramObject);
      paramObject = paramObject.toArray();
      return Arrays.hashCode((int[])paramObject);
    } 
    throwErrorIfUnsupportedType(paramObject);
    return paramObject.hashCode();
  }
  
  private static void throwErrorIfUnsupportedType(Object paramObject) {
    if (!(paramObject instanceof java.util.Collection) || paramObject instanceof List) {
      if (!(paramObject instanceof java.util.Map))
        return; 
      throw new UnsupportedOperationException("Cannot check equality on maps");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Cannot check equality on collections other than lists: ");
    stringBuilder.append(paramObject.getClass().getName());
    throw new UnsupportedOperationException(stringBuilder.toString());
  }
  
  private static int primitiveArrayHashCode(Object paramObject) {
    Class<?> clazz = paramObject.getClass().getComponentType();
    if (clazz == boolean.class)
      return Arrays.hashCode((boolean[])paramObject); 
    if (clazz == byte.class)
      return Arrays.hashCode((byte[])paramObject); 
    if (clazz == char.class)
      return Arrays.hashCode((char[])paramObject); 
    if (clazz == double.class)
      return Arrays.hashCode((double[])paramObject); 
    if (clazz == float.class)
      return Arrays.hashCode((float[])paramObject); 
    if (clazz == int.class)
      return Arrays.hashCode((int[])paramObject); 
    if (clazz == long.class)
      return Arrays.hashCode((long[])paramObject); 
    if (clazz == short.class)
      return Arrays.hashCode((short[])paramObject); 
    throw new UnsupportedOperationException();
  }
  
  @SystemApi
  public static boolean interfacesEqual(IHwInterface paramIHwInterface, Object paramObject) {
    if (paramIHwInterface == paramObject)
      return true; 
    if (paramIHwInterface == null || paramObject == null)
      return false; 
    if (!(paramObject instanceof IHwInterface))
      return false; 
    return Objects.equals(paramIHwInterface.asBinder(), ((IHwInterface)paramObject).asBinder());
  }
  
  @SystemApi
  public static native int getPidIfSharable();
}
