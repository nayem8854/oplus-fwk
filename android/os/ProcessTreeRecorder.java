package android.os;

import android.app.OplusActivityManager;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

class ProcessTreeRecorder {
  private static final String CMDLINE_UNDEFINED = "<Unknown>";
  
  private final OplusActivityManager mAm;
  
  private final int mPid;
  
  private final Map<Integer, String> mThreadMap;
  
  ProcessTreeRecorder(int paramInt) {
    this.mAm = new OplusActivityManager();
    this.mPid = paramInt;
    this.mThreadMap = new ConcurrentHashMap<>();
  }
  
  boolean attachThread(int paramInt, String paramString) {
    String str = paramString;
    if (paramString == null)
      str = "<Unknown>"; 
    this.mThreadMap.put(Integer.valueOf(paramInt), str);
    return true;
  }
  
  int getPid() {
    return this.mPid;
  }
  
  String getProcessName() {
    String str = "<Unknown>";
    try {
      String str1 = this.mAm.getProcCmdline(new int[] { this.mPid }).get(0);
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
    } 
    return str;
  }
  
  Map<Integer, String> getThreadMap() {
    return new HashMap<>(this.mThreadMap);
  }
  
  int getHashCode() {
    return this.mThreadMap.hashCode();
  }
}
