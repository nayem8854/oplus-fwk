package android.os;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public interface Parcelable {
  public static final int CONTENTS_FILE_DESCRIPTOR = 1;
  
  public static final int PARCELABLE_ELIDE_DUPLICATES = 2;
  
  public static final int PARCELABLE_WRITE_RETURN_VALUE = 1;
  
  int describeContents();
  
  void writeToParcel(Parcel paramParcel, int paramInt);
  
  class ClassLoaderCreator<T> implements Creator<T> {
    public abstract T createFromParcel(Parcel param1Parcel, ClassLoader param1ClassLoader);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ContentsFlags {}
  
  public static interface Creator<T> {
    T createFromParcel(Parcel param1Parcel);
    
    T[] newArray(int param1Int);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface WriteFlags {}
}
