package android.os;

public interface IUserRestrictionsListener extends IInterface {
  void onUserRestrictionsChanged(int paramInt, Bundle paramBundle1, Bundle paramBundle2) throws RemoteException;
  
  class Default implements IUserRestrictionsListener {
    public void onUserRestrictionsChanged(int param1Int, Bundle param1Bundle1, Bundle param1Bundle2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IUserRestrictionsListener {
    private static final String DESCRIPTOR = "android.os.IUserRestrictionsListener";
    
    static final int TRANSACTION_onUserRestrictionsChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IUserRestrictionsListener");
    }
    
    public static IUserRestrictionsListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IUserRestrictionsListener");
      if (iInterface != null && iInterface instanceof IUserRestrictionsListener)
        return (IUserRestrictionsListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onUserRestrictionsChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.IUserRestrictionsListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IUserRestrictionsListener");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onUserRestrictionsChanged(param1Int1, (Bundle)param1Parcel2, (Bundle)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IUserRestrictionsListener {
      public static IUserRestrictionsListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IUserRestrictionsListener";
      }
      
      public void onUserRestrictionsChanged(int param2Int, Bundle param2Bundle1, Bundle param2Bundle2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IUserRestrictionsListener");
          parcel.writeInt(param2Int);
          if (param2Bundle1 != null) {
            parcel.writeInt(1);
            param2Bundle1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Bundle2 != null) {
            parcel.writeInt(1);
            param2Bundle2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IUserRestrictionsListener.Stub.getDefaultImpl() != null) {
            IUserRestrictionsListener.Stub.getDefaultImpl().onUserRestrictionsChanged(param2Int, param2Bundle1, param2Bundle2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IUserRestrictionsListener param1IUserRestrictionsListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IUserRestrictionsListener != null) {
          Proxy.sDefaultImpl = param1IUserRestrictionsListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IUserRestrictionsListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
