package android.os;

public abstract class CountDownTimer {
  private static final int MSG = 1;
  
  private boolean mCancelled = false;
  
  private final long mCountdownInterval;
  
  private Handler mHandler;
  
  private final long mMillisInFuture;
  
  private long mStopTimeInFuture;
  
  public final void cancel() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_1
    //   4: putfield mCancelled : Z
    //   7: aload_0
    //   8: getfield mHandler : Landroid/os/Handler;
    //   11: iconst_1
    //   12: invokevirtual removeMessages : (I)V
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_1
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #79	-> 2
    //   #80	-> 7
    //   #81	-> 15
    //   #78	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	7	18	finally
    //   7	15	18	finally
  }
  
  public final CountDownTimer start() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_0
    //   4: putfield mCancelled : Z
    //   7: aload_0
    //   8: getfield mMillisInFuture : J
    //   11: lconst_0
    //   12: lcmp
    //   13: ifgt -> 24
    //   16: aload_0
    //   17: invokevirtual onFinish : ()V
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_0
    //   23: areturn
    //   24: aload_0
    //   25: invokestatic elapsedRealtime : ()J
    //   28: aload_0
    //   29: getfield mMillisInFuture : J
    //   32: ladd
    //   33: putfield mStopTimeInFuture : J
    //   36: aload_0
    //   37: getfield mHandler : Landroid/os/Handler;
    //   40: aload_0
    //   41: getfield mHandler : Landroid/os/Handler;
    //   44: iconst_1
    //   45: invokevirtual obtainMessage : (I)Landroid/os/Message;
    //   48: invokevirtual sendMessage : (Landroid/os/Message;)Z
    //   51: pop
    //   52: aload_0
    //   53: monitorexit
    //   54: aload_0
    //   55: areturn
    //   56: astore_1
    //   57: aload_0
    //   58: monitorexit
    //   59: aload_1
    //   60: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #87	-> 2
    //   #88	-> 7
    //   #89	-> 16
    //   #90	-> 20
    //   #92	-> 24
    //   #93	-> 36
    //   #94	-> 52
    //   #86	-> 56
    // Exception table:
    //   from	to	target	type
    //   2	7	56	finally
    //   7	16	56	finally
    //   16	20	56	finally
    //   24	36	56	finally
    //   36	52	56	finally
  }
  
  public CountDownTimer(long paramLong1, long paramLong2) {
    this.mHandler = (Handler)new Object(this);
    this.mMillisInFuture = paramLong1;
    this.mCountdownInterval = paramLong2;
  }
  
  public abstract void onFinish();
  
  public abstract void onTick(long paramLong);
}
