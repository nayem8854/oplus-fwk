package android.os.strictmode;

public final class IncorrectContextUseViolation extends Violation {
  public IncorrectContextUseViolation(String paramString, Throwable paramThrowable) {
    super(paramString);
    initCause(paramThrowable);
  }
}
