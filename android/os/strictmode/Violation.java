package android.os.strictmode;

public abstract class Violation extends Throwable {
  private int mHashCode;
  
  private boolean mHashCodeValid;
  
  Violation(String paramString) {
    super(paramString);
  }
  
  public int hashCode() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mHashCodeValid : Z
    //   6: ifeq -> 18
    //   9: aload_0
    //   10: getfield mHashCode : I
    //   13: istore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: iload_1
    //   17: ireturn
    //   18: aload_0
    //   19: invokevirtual getMessage : ()Ljava/lang/String;
    //   22: astore_2
    //   23: aload_0
    //   24: invokevirtual getCause : ()Ljava/lang/Throwable;
    //   27: astore_3
    //   28: aload_2
    //   29: ifnull -> 40
    //   32: aload_2
    //   33: invokevirtual hashCode : ()I
    //   36: istore_1
    //   37: goto -> 48
    //   40: aload_0
    //   41: invokevirtual getClass : ()Ljava/lang/Class;
    //   44: invokevirtual hashCode : ()I
    //   47: istore_1
    //   48: aload_0
    //   49: invokevirtual getStackTrace : ()[Ljava/lang/StackTraceElement;
    //   52: invokestatic calcStackTraceHashCode : ([Ljava/lang/StackTraceElement;)I
    //   55: istore #4
    //   57: aload_3
    //   58: ifnull -> 73
    //   61: aload_3
    //   62: invokevirtual toString : ()Ljava/lang/String;
    //   65: invokevirtual hashCode : ()I
    //   68: istore #5
    //   70: goto -> 76
    //   73: iconst_0
    //   74: istore #5
    //   76: iload_1
    //   77: bipush #37
    //   79: imul
    //   80: iload #4
    //   82: iadd
    //   83: bipush #37
    //   85: imul
    //   86: iload #5
    //   88: iadd
    //   89: istore_1
    //   90: aload_0
    //   91: iconst_1
    //   92: putfield mHashCodeValid : Z
    //   95: aload_0
    //   96: iload_1
    //   97: putfield mHashCode : I
    //   100: aload_0
    //   101: monitorexit
    //   102: iload_1
    //   103: ireturn
    //   104: astore_2
    //   105: aload_0
    //   106: monitorexit
    //   107: aload_2
    //   108: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #30	-> 0
    //   #31	-> 2
    //   #32	-> 9
    //   #34	-> 18
    //   #35	-> 23
    //   #36	-> 28
    //   #37	-> 48
    //   #38	-> 57
    //   #39	-> 90
    //   #40	-> 95
    //   #41	-> 104
    // Exception table:
    //   from	to	target	type
    //   2	9	104	finally
    //   9	16	104	finally
    //   18	23	104	finally
    //   23	28	104	finally
    //   32	37	104	finally
    //   40	48	104	finally
    //   48	57	104	finally
    //   61	70	104	finally
    //   90	95	104	finally
    //   95	102	104	finally
    //   105	107	104	finally
  }
  
  public Throwable initCause(Throwable paramThrowable) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_0
    //   4: putfield mHashCodeValid : Z
    //   7: aload_0
    //   8: aload_1
    //   9: invokespecial initCause : (Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   12: astore_1
    //   13: aload_0
    //   14: monitorexit
    //   15: aload_1
    //   16: areturn
    //   17: astore_1
    //   18: aload_0
    //   19: monitorexit
    //   20: aload_1
    //   21: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #46	-> 2
    //   #47	-> 7
    //   #45	-> 17
    // Exception table:
    //   from	to	target	type
    //   2	7	17	finally
    //   7	13	17	finally
  }
  
  public void setStackTrace(StackTraceElement[] paramArrayOfStackTraceElement) {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial setStackTrace : ([Ljava/lang/StackTraceElement;)V
    //   5: aload_0
    //   6: monitorenter
    //   7: aload_0
    //   8: iconst_0
    //   9: putfield mHashCodeValid : Z
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #52	-> 0
    //   #53	-> 5
    //   #54	-> 7
    //   #55	-> 12
    //   #56	-> 14
    //   #55	-> 15
    // Exception table:
    //   from	to	target	type
    //   7	12	15	finally
    //   12	14	15	finally
    //   16	18	15	finally
  }
  
  public Throwable fillInStackTrace() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_0
    //   4: putfield mHashCodeValid : Z
    //   7: aload_0
    //   8: invokespecial fillInStackTrace : ()Ljava/lang/Throwable;
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: areturn
    //   16: astore_1
    //   17: aload_0
    //   18: monitorexit
    //   19: aload_1
    //   20: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #60	-> 2
    //   #61	-> 7
    //   #59	-> 16
    // Exception table:
    //   from	to	target	type
    //   2	7	16	finally
    //   7	12	16	finally
  }
  
  private static int calcStackTraceHashCode(StackTraceElement[] paramArrayOfStackTraceElement) {
    int i = 17;
    int j = i;
    if (paramArrayOfStackTraceElement != null) {
      byte b = 0;
      while (true) {
        j = i;
        if (b < paramArrayOfStackTraceElement.length) {
          j = i;
          if (paramArrayOfStackTraceElement[b] != null)
            j = i * 37 + paramArrayOfStackTraceElement[b].hashCode(); 
          b++;
          i = j;
          continue;
        } 
        break;
      } 
    } 
    return j;
  }
}
