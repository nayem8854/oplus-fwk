package android.os.strictmode;

public final class CredentialProtectedWhileLockedViolation extends Violation {
  public CredentialProtectedWhileLockedViolation(String paramString) {
    super(paramString);
  }
}
