package android.os.strictmode;

public class InstanceCountViolation extends Violation {
  private static final StackTraceElement[] FAKE_STACK = new StackTraceElement[] { new StackTraceElement("android.os.StrictMode", "setClassInstanceLimit", "StrictMode.java", 1) };
  
  private final long mInstances;
  
  public InstanceCountViolation(Class paramClass, long paramLong, int paramInt) {
    super(stringBuilder.toString());
    setStackTrace(FAKE_STACK);
    this.mInstances = paramLong;
  }
  
  public long getNumberOfInstances() {
    return this.mInstances;
  }
}
