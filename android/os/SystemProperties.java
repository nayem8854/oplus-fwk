package android.os;

import android.annotation.SystemApi;
import android.util.MutableInt;
import dalvik.annotation.optimization.CriticalNative;
import dalvik.annotation.optimization.FastNative;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import libcore.util.HexEncoding;

@SystemApi
public class SystemProperties {
  public static final int PROP_NAME_MAX = 2147483647;
  
  public static final int PROP_VALUE_MAX = 91;
  
  private static final String TAG = "SystemProperties";
  
  private static final boolean TRACK_KEY_ACCESS = false;
  
  private static final ArrayList<Runnable> sChangeCallbacks = new ArrayList<>();
  
  private static final HashMap<String, MutableInt> sRoReads = null;
  
  private static void onKeyAccess(String paramString) {}
  
  private static String native_get(String paramString) {
    return native_get(paramString, "");
  }
  
  @SystemApi
  public static String get(String paramString) {
    return native_get(paramString);
  }
  
  @SystemApi
  public static String get(String paramString1, String paramString2) {
    return native_get(paramString1, paramString2);
  }
  
  @SystemApi
  public static int getInt(String paramString, int paramInt) {
    return native_get_int(paramString, paramInt);
  }
  
  @SystemApi
  public static long getLong(String paramString, long paramLong) {
    return native_get_long(paramString, paramLong);
  }
  
  @SystemApi
  public static boolean getBoolean(String paramString, boolean paramBoolean) {
    return native_get_boolean(paramString, paramBoolean);
  }
  
  public static void set(String paramString1, String paramString2) {
    if (paramString2 == null || paramString2.startsWith("ro.") || paramString2.length() <= 91) {
      native_set(paramString1, paramString2);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("value of system property '");
    stringBuilder.append(paramString1);
    stringBuilder.append("' is longer than ");
    stringBuilder.append(91);
    stringBuilder.append(" characters: ");
    stringBuilder.append(paramString2);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static void addChangeCallback(Runnable paramRunnable) {
    synchronized (sChangeCallbacks) {
      if (sChangeCallbacks.size() == 0)
        native_add_change_callback(); 
      sChangeCallbacks.add(paramRunnable);
      return;
    } 
  }
  
  public static void removeChangeCallback(Runnable paramRunnable) {
    synchronized (sChangeCallbacks) {
      if (sChangeCallbacks.contains(paramRunnable))
        sChangeCallbacks.remove(paramRunnable); 
      return;
    } 
  }
  
  private static void callChangeCallbacks() {
    synchronized (sChangeCallbacks) {
      if (sChangeCallbacks.size() == 0)
        return; 
      null = new ArrayList();
      this((Collection)sChangeCallbacks);
      long l = Binder.clearCallingIdentity();
      byte b = 0;
      try {
        while (true) {
          int i = null.size();
          if (b < i) {
            try {
              ((Runnable)null.get(b)).run();
            } finally {
              null = null;
            } 
            continue;
          } 
          break;
        } 
        return;
      } finally {
        Binder.restoreCallingIdentity(l);
      } 
    } 
  }
  
  public static void reportSyspropChanged() {
    native_report_sysprop_change();
  }
  
  public static String digestOf(String... paramVarArgs) {
    Arrays.sort((Object[])paramVarArgs);
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
      int i;
      byte b;
      for (i = paramVarArgs.length, b = 0; b < i; ) {
        String str = paramVarArgs[b];
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(str);
        stringBuilder.append("=");
        stringBuilder.append(get(str));
        stringBuilder.append("\n");
        str = stringBuilder.toString();
        messageDigest.update(str.getBytes(StandardCharsets.UTF_8));
        b++;
      } 
      return HexEncoding.encodeToString(messageDigest.digest()).toLowerCase();
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      throw new RuntimeException(noSuchAlgorithmException);
    } 
  }
  
  public static Handle find(String paramString) {
    long l = native_find(paramString);
    if (l == 0L)
      return null; 
    return new Handle(l);
  }
  
  private static native void native_add_change_callback();
  
  @FastNative
  private static native long native_find(String paramString);
  
  @FastNative
  private static native String native_get(long paramLong);
  
  @FastNative
  private static native String native_get(String paramString1, String paramString2);
  
  @CriticalNative
  private static native boolean native_get_boolean(long paramLong, boolean paramBoolean);
  
  @FastNative
  private static native boolean native_get_boolean(String paramString, boolean paramBoolean);
  
  @CriticalNative
  private static native int native_get_int(long paramLong, int paramInt);
  
  @FastNative
  private static native int native_get_int(String paramString, int paramInt);
  
  @CriticalNative
  private static native long native_get_long(long paramLong1, long paramLong2);
  
  @FastNative
  private static native long native_get_long(String paramString, long paramLong);
  
  private static native void native_report_sysprop_change();
  
  private static native void native_set(String paramString1, String paramString2);
  
  public static final class Handle {
    private final long mNativeHandle;
    
    public String get() {
      return SystemProperties.native_get(this.mNativeHandle);
    }
    
    public int getInt(int param1Int) {
      return SystemProperties.native_get_int(this.mNativeHandle, param1Int);
    }
    
    public long getLong(long param1Long) {
      return SystemProperties.native_get_long(this.mNativeHandle, param1Long);
    }
    
    public boolean getBoolean(boolean param1Boolean) {
      return SystemProperties.native_get_boolean(this.mNativeHandle, param1Boolean);
    }
    
    private Handle(long param1Long) {
      this.mNativeHandle = param1Long;
    }
  }
}
