package android.os;

import java.util.Map;

public interface IPowerManager extends IInterface {
  void acquireWakeLock(IBinder paramIBinder, int paramInt, String paramString1, String paramString2, WorkSource paramWorkSource, String paramString3) throws RemoteException;
  
  void acquireWakeLockWithUid(IBinder paramIBinder, int paramInt1, String paramString1, String paramString2, int paramInt2) throws RemoteException;
  
  void boostScreenBrightness(long paramLong) throws RemoteException;
  
  void crash(String paramString) throws RemoteException;
  
  boolean forceSuspend() throws RemoteException;
  
  float getBrightnessConstraint(int paramInt) throws RemoteException;
  
  int getCurrentChargeStateForSale() throws RemoteException;
  
  int getDefaultScreenBrightnessSetting() throws RemoteException;
  
  boolean getDisplayAodStatus() throws RemoteException;
  
  long getFrameworksBlockedTime() throws RemoteException;
  
  int getLastShutdownReason() throws RemoteException;
  
  int getLastSleepReason() throws RemoteException;
  
  int getMaximumScreenBrightnessSetting() throws RemoteException;
  
  int getMinimumScreenBrightnessSetting() throws RemoteException;
  
  int getPowerSaveModeTrigger() throws RemoteException;
  
  PowerSaveState getPowerSaveState(int paramInt) throws RemoteException;
  
  int getScreenState() throws RemoteException;
  
  Map getTopAppBlocked(int paramInt) throws RemoteException;
  
  void goToSleep(long paramLong, int paramInt1, int paramInt2) throws RemoteException;
  
  boolean isAmbientDisplayAvailable() throws RemoteException;
  
  boolean isAmbientDisplaySuppressed() throws RemoteException;
  
  boolean isAmbientDisplaySuppressedForToken(String paramString) throws RemoteException;
  
  boolean isDeviceIdleMode() throws RemoteException;
  
  boolean isInteractive() throws RemoteException;
  
  boolean isLightDeviceIdleMode() throws RemoteException;
  
  boolean isPowerSaveMode() throws RemoteException;
  
  boolean isScreenBrightnessBoosted() throws RemoteException;
  
  boolean isWakeLockLevelSupported(int paramInt) throws RemoteException;
  
  void nap(long paramLong) throws RemoteException;
  
  void powerHint(int paramInt1, int paramInt2) throws RemoteException;
  
  void reboot(boolean paramBoolean1, String paramString, boolean paramBoolean2) throws RemoteException;
  
  void rebootSafeMode(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void releaseWakeLock(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void resumeChargeForSale() throws RemoteException;
  
  boolean setAdaptivePowerSaveEnabled(boolean paramBoolean) throws RemoteException;
  
  boolean setAdaptivePowerSavePolicy(BatterySaverPolicyConfig paramBatterySaverPolicyConfig) throws RemoteException;
  
  void setAttentionLight(boolean paramBoolean, int paramInt) throws RemoteException;
  
  void setDozeAfterScreenOff(boolean paramBoolean) throws RemoteException;
  
  void setDozeOverride(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean setDynamicPowerSaveHint(boolean paramBoolean, int paramInt) throws RemoteException;
  
  void setPowerBoost(int paramInt1, int paramInt2) throws RemoteException;
  
  void setPowerMode(int paramInt, boolean paramBoolean) throws RemoteException;
  
  boolean setPowerModeChecked(int paramInt, boolean paramBoolean) throws RemoteException;
  
  boolean setPowerSaveModeEnabled(boolean paramBoolean) throws RemoteException;
  
  void setStayOnSetting(int paramInt) throws RemoteException;
  
  void shutdown(boolean paramBoolean1, String paramString, boolean paramBoolean2) throws RemoteException;
  
  void stopChargeForSale() throws RemoteException;
  
  void suppressAmbientDisplay(String paramString, boolean paramBoolean) throws RemoteException;
  
  void updateWakeLockUids(IBinder paramIBinder, int[] paramArrayOfint) throws RemoteException;
  
  void updateWakeLockWorkSource(IBinder paramIBinder, WorkSource paramWorkSource, String paramString) throws RemoteException;
  
  void userActivity(long paramLong, int paramInt1, int paramInt2) throws RemoteException;
  
  void wakeUp(long paramLong, int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  class Default implements IPowerManager {
    public void acquireWakeLock(IBinder param1IBinder, int param1Int, String param1String1, String param1String2, WorkSource param1WorkSource, String param1String3) throws RemoteException {}
    
    public void acquireWakeLockWithUid(IBinder param1IBinder, int param1Int1, String param1String1, String param1String2, int param1Int2) throws RemoteException {}
    
    public void releaseWakeLock(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void updateWakeLockUids(IBinder param1IBinder, int[] param1ArrayOfint) throws RemoteException {}
    
    public void powerHint(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setPowerBoost(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setPowerMode(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean setPowerModeChecked(int param1Int, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public void updateWakeLockWorkSource(IBinder param1IBinder, WorkSource param1WorkSource, String param1String) throws RemoteException {}
    
    public boolean isWakeLockLevelSupported(int param1Int) throws RemoteException {
      return false;
    }
    
    public void userActivity(long param1Long, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void wakeUp(long param1Long, int param1Int, String param1String1, String param1String2) throws RemoteException {}
    
    public void goToSleep(long param1Long, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void nap(long param1Long) throws RemoteException {}
    
    public float getBrightnessConstraint(int param1Int) throws RemoteException {
      return 0.0F;
    }
    
    public boolean isInteractive() throws RemoteException {
      return false;
    }
    
    public boolean isPowerSaveMode() throws RemoteException {
      return false;
    }
    
    public PowerSaveState getPowerSaveState(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean setPowerSaveModeEnabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean setDynamicPowerSaveHint(boolean param1Boolean, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean setAdaptivePowerSavePolicy(BatterySaverPolicyConfig param1BatterySaverPolicyConfig) throws RemoteException {
      return false;
    }
    
    public boolean setAdaptivePowerSaveEnabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public int getPowerSaveModeTrigger() throws RemoteException {
      return 0;
    }
    
    public boolean isDeviceIdleMode() throws RemoteException {
      return false;
    }
    
    public boolean isLightDeviceIdleMode() throws RemoteException {
      return false;
    }
    
    public void reboot(boolean param1Boolean1, String param1String, boolean param1Boolean2) throws RemoteException {}
    
    public void rebootSafeMode(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void shutdown(boolean param1Boolean1, String param1String, boolean param1Boolean2) throws RemoteException {}
    
    public void crash(String param1String) throws RemoteException {}
    
    public int getLastShutdownReason() throws RemoteException {
      return 0;
    }
    
    public int getLastSleepReason() throws RemoteException {
      return 0;
    }
    
    public void setStayOnSetting(int param1Int) throws RemoteException {}
    
    public void boostScreenBrightness(long param1Long) throws RemoteException {}
    
    public long getFrameworksBlockedTime() throws RemoteException {
      return 0L;
    }
    
    public Map getTopAppBlocked(int param1Int) throws RemoteException {
      return null;
    }
    
    public int getScreenState() throws RemoteException {
      return 0;
    }
    
    public boolean isScreenBrightnessBoosted() throws RemoteException {
      return false;
    }
    
    public void setAttentionLight(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void setDozeAfterScreenOff(boolean param1Boolean) throws RemoteException {}
    
    public boolean isAmbientDisplayAvailable() throws RemoteException {
      return false;
    }
    
    public void suppressAmbientDisplay(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public boolean isAmbientDisplaySuppressedForToken(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean isAmbientDisplaySuppressed() throws RemoteException {
      return false;
    }
    
    public boolean forceSuspend() throws RemoteException {
      return false;
    }
    
    public void setDozeOverride(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void stopChargeForSale() throws RemoteException {}
    
    public void resumeChargeForSale() throws RemoteException {}
    
    public int getCurrentChargeStateForSale() throws RemoteException {
      return 0;
    }
    
    public boolean getDisplayAodStatus() throws RemoteException {
      return false;
    }
    
    public int getMinimumScreenBrightnessSetting() throws RemoteException {
      return 0;
    }
    
    public int getMaximumScreenBrightnessSetting() throws RemoteException {
      return 0;
    }
    
    public int getDefaultScreenBrightnessSetting() throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPowerManager {
    private static final String DESCRIPTOR = "android.os.IPowerManager";
    
    static final int TRANSACTION_acquireWakeLock = 1;
    
    static final int TRANSACTION_acquireWakeLockWithUid = 2;
    
    static final int TRANSACTION_boostScreenBrightness = 33;
    
    static final int TRANSACTION_crash = 29;
    
    static final int TRANSACTION_forceSuspend = 44;
    
    static final int TRANSACTION_getBrightnessConstraint = 15;
    
    static final int TRANSACTION_getCurrentChargeStateForSale = 48;
    
    static final int TRANSACTION_getDefaultScreenBrightnessSetting = 52;
    
    static final int TRANSACTION_getDisplayAodStatus = 49;
    
    static final int TRANSACTION_getFrameworksBlockedTime = 34;
    
    static final int TRANSACTION_getLastShutdownReason = 30;
    
    static final int TRANSACTION_getLastSleepReason = 31;
    
    static final int TRANSACTION_getMaximumScreenBrightnessSetting = 51;
    
    static final int TRANSACTION_getMinimumScreenBrightnessSetting = 50;
    
    static final int TRANSACTION_getPowerSaveModeTrigger = 23;
    
    static final int TRANSACTION_getPowerSaveState = 18;
    
    static final int TRANSACTION_getScreenState = 36;
    
    static final int TRANSACTION_getTopAppBlocked = 35;
    
    static final int TRANSACTION_goToSleep = 13;
    
    static final int TRANSACTION_isAmbientDisplayAvailable = 40;
    
    static final int TRANSACTION_isAmbientDisplaySuppressed = 43;
    
    static final int TRANSACTION_isAmbientDisplaySuppressedForToken = 42;
    
    static final int TRANSACTION_isDeviceIdleMode = 24;
    
    static final int TRANSACTION_isInteractive = 16;
    
    static final int TRANSACTION_isLightDeviceIdleMode = 25;
    
    static final int TRANSACTION_isPowerSaveMode = 17;
    
    static final int TRANSACTION_isScreenBrightnessBoosted = 37;
    
    static final int TRANSACTION_isWakeLockLevelSupported = 10;
    
    static final int TRANSACTION_nap = 14;
    
    static final int TRANSACTION_powerHint = 5;
    
    static final int TRANSACTION_reboot = 26;
    
    static final int TRANSACTION_rebootSafeMode = 27;
    
    static final int TRANSACTION_releaseWakeLock = 3;
    
    static final int TRANSACTION_resumeChargeForSale = 47;
    
    static final int TRANSACTION_setAdaptivePowerSaveEnabled = 22;
    
    static final int TRANSACTION_setAdaptivePowerSavePolicy = 21;
    
    static final int TRANSACTION_setAttentionLight = 38;
    
    static final int TRANSACTION_setDozeAfterScreenOff = 39;
    
    static final int TRANSACTION_setDozeOverride = 45;
    
    static final int TRANSACTION_setDynamicPowerSaveHint = 20;
    
    static final int TRANSACTION_setPowerBoost = 6;
    
    static final int TRANSACTION_setPowerMode = 7;
    
    static final int TRANSACTION_setPowerModeChecked = 8;
    
    static final int TRANSACTION_setPowerSaveModeEnabled = 19;
    
    static final int TRANSACTION_setStayOnSetting = 32;
    
    static final int TRANSACTION_shutdown = 28;
    
    static final int TRANSACTION_stopChargeForSale = 46;
    
    static final int TRANSACTION_suppressAmbientDisplay = 41;
    
    static final int TRANSACTION_updateWakeLockUids = 4;
    
    static final int TRANSACTION_updateWakeLockWorkSource = 9;
    
    static final int TRANSACTION_userActivity = 11;
    
    static final int TRANSACTION_wakeUp = 12;
    
    public Stub() {
      attachInterface(this, "android.os.IPowerManager");
    }
    
    public static IPowerManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IPowerManager");
      if (iInterface != null && iInterface instanceof IPowerManager)
        return (IPowerManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 52:
          return "getDefaultScreenBrightnessSetting";
        case 51:
          return "getMaximumScreenBrightnessSetting";
        case 50:
          return "getMinimumScreenBrightnessSetting";
        case 49:
          return "getDisplayAodStatus";
        case 48:
          return "getCurrentChargeStateForSale";
        case 47:
          return "resumeChargeForSale";
        case 46:
          return "stopChargeForSale";
        case 45:
          return "setDozeOverride";
        case 44:
          return "forceSuspend";
        case 43:
          return "isAmbientDisplaySuppressed";
        case 42:
          return "isAmbientDisplaySuppressedForToken";
        case 41:
          return "suppressAmbientDisplay";
        case 40:
          return "isAmbientDisplayAvailable";
        case 39:
          return "setDozeAfterScreenOff";
        case 38:
          return "setAttentionLight";
        case 37:
          return "isScreenBrightnessBoosted";
        case 36:
          return "getScreenState";
        case 35:
          return "getTopAppBlocked";
        case 34:
          return "getFrameworksBlockedTime";
        case 33:
          return "boostScreenBrightness";
        case 32:
          return "setStayOnSetting";
        case 31:
          return "getLastSleepReason";
        case 30:
          return "getLastShutdownReason";
        case 29:
          return "crash";
        case 28:
          return "shutdown";
        case 27:
          return "rebootSafeMode";
        case 26:
          return "reboot";
        case 25:
          return "isLightDeviceIdleMode";
        case 24:
          return "isDeviceIdleMode";
        case 23:
          return "getPowerSaveModeTrigger";
        case 22:
          return "setAdaptivePowerSaveEnabled";
        case 21:
          return "setAdaptivePowerSavePolicy";
        case 20:
          return "setDynamicPowerSaveHint";
        case 19:
          return "setPowerSaveModeEnabled";
        case 18:
          return "getPowerSaveState";
        case 17:
          return "isPowerSaveMode";
        case 16:
          return "isInteractive";
        case 15:
          return "getBrightnessConstraint";
        case 14:
          return "nap";
        case 13:
          return "goToSleep";
        case 12:
          return "wakeUp";
        case 11:
          return "userActivity";
        case 10:
          return "isWakeLockLevelSupported";
        case 9:
          return "updateWakeLockWorkSource";
        case 8:
          return "setPowerModeChecked";
        case 7:
          return "setPowerMode";
        case 6:
          return "setPowerBoost";
        case 5:
          return "powerHint";
        case 4:
          return "updateWakeLockUids";
        case 3:
          return "releaseWakeLock";
        case 2:
          return "acquireWakeLockWithUid";
        case 1:
          break;
      } 
      return "acquireWakeLock";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool9;
        int i4;
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        String str4;
        Map map;
        String str3;
        PowerSaveState powerSaveState;
        String str2;
        int[] arrayOfInt;
        String str6;
        IBinder iBinder1;
        String str5;
        long l;
        float f;
        IBinder iBinder2;
        String str8;
        boolean bool10 = false, bool11 = false, bool12 = false, bool13 = false, bool14 = false, bool15 = false, bool16 = false, bool17 = false, bool18 = false, bool19 = false, bool20 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 52:
            param1Parcel1.enforceInterface("android.os.IPowerManager");
            param1Int1 = getDefaultScreenBrightnessSetting();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 51:
            param1Parcel1.enforceInterface("android.os.IPowerManager");
            param1Int1 = getMaximumScreenBrightnessSetting();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 50:
            param1Parcel1.enforceInterface("android.os.IPowerManager");
            param1Int1 = getMinimumScreenBrightnessSetting();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 49:
            param1Parcel1.enforceInterface("android.os.IPowerManager");
            bool9 = getDisplayAodStatus();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 48:
            param1Parcel1.enforceInterface("android.os.IPowerManager");
            i4 = getCurrentChargeStateForSale();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i4);
            return true;
          case 47:
            param1Parcel1.enforceInterface("android.os.IPowerManager");
            resumeChargeForSale();
            param1Parcel2.writeNoException();
            return true;
          case 46:
            param1Parcel1.enforceInterface("android.os.IPowerManager");
            stopChargeForSale();
            param1Parcel2.writeNoException();
            return true;
          case 45:
            param1Parcel1.enforceInterface("android.os.IPowerManager");
            i4 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            setDozeOverride(i4, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 44:
            param1Parcel1.enforceInterface("android.os.IPowerManager");
            bool8 = forceSuspend();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 43:
            param1Parcel1.enforceInterface("android.os.IPowerManager");
            bool8 = isAmbientDisplaySuppressed();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 42:
            param1Parcel1.enforceInterface("android.os.IPowerManager");
            str4 = param1Parcel1.readString();
            bool8 = isAmbientDisplaySuppressedForToken(str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 41:
            str4.enforceInterface("android.os.IPowerManager");
            str6 = str4.readString();
            bool10 = bool20;
            if (str4.readInt() != 0)
              bool10 = true; 
            suppressAmbientDisplay(str6, bool10);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            str4.enforceInterface("android.os.IPowerManager");
            bool8 = isAmbientDisplayAvailable();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 39:
            str4.enforceInterface("android.os.IPowerManager");
            if (str4.readInt() != 0)
              bool10 = true; 
            setDozeAfterScreenOff(bool10);
            param1Parcel2.writeNoException();
            return true;
          case 38:
            str4.enforceInterface("android.os.IPowerManager");
            bool10 = bool11;
            if (str4.readInt() != 0)
              bool10 = true; 
            i3 = str4.readInt();
            setAttentionLight(bool10, i3);
            param1Parcel2.writeNoException();
            return true;
          case 37:
            str4.enforceInterface("android.os.IPowerManager");
            bool7 = isScreenBrightnessBoosted();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 36:
            str4.enforceInterface("android.os.IPowerManager");
            i2 = getScreenState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 35:
            str4.enforceInterface("android.os.IPowerManager");
            i2 = str4.readInt();
            map = getTopAppBlocked(i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeMap(map);
            return true;
          case 34:
            map.enforceInterface("android.os.IPowerManager");
            l = getFrameworksBlockedTime();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 33:
            map.enforceInterface("android.os.IPowerManager");
            l = map.readLong();
            boostScreenBrightness(l);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            map.enforceInterface("android.os.IPowerManager");
            i2 = map.readInt();
            setStayOnSetting(i2);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            map.enforceInterface("android.os.IPowerManager");
            i2 = getLastSleepReason();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 30:
            map.enforceInterface("android.os.IPowerManager");
            i2 = getLastShutdownReason();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 29:
            map.enforceInterface("android.os.IPowerManager");
            str3 = map.readString();
            crash(str3);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            str3.enforceInterface("android.os.IPowerManager");
            if (str3.readInt() != 0) {
              bool10 = true;
            } else {
              bool10 = false;
            } 
            str6 = str3.readString();
            if (str3.readInt() != 0)
              bool12 = true; 
            shutdown(bool10, str6, bool12);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            str3.enforceInterface("android.os.IPowerManager");
            if (str3.readInt() != 0) {
              bool10 = true;
            } else {
              bool10 = false;
            } 
            bool12 = bool13;
            if (str3.readInt() != 0)
              bool12 = true; 
            rebootSafeMode(bool10, bool12);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            str3.enforceInterface("android.os.IPowerManager");
            if (str3.readInt() != 0) {
              bool10 = true;
            } else {
              bool10 = false;
            } 
            str6 = str3.readString();
            bool12 = bool14;
            if (str3.readInt() != 0)
              bool12 = true; 
            reboot(bool10, str6, bool12);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str3.enforceInterface("android.os.IPowerManager");
            bool6 = isLightDeviceIdleMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 24:
            str3.enforceInterface("android.os.IPowerManager");
            bool6 = isDeviceIdleMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 23:
            str3.enforceInterface("android.os.IPowerManager");
            i1 = getPowerSaveModeTrigger();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i1);
            return true;
          case 22:
            str3.enforceInterface("android.os.IPowerManager");
            bool10 = bool15;
            if (str3.readInt() != 0)
              bool10 = true; 
            bool5 = setAdaptivePowerSaveEnabled(bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 21:
            str3.enforceInterface("android.os.IPowerManager");
            if (str3.readInt() != 0) {
              BatterySaverPolicyConfig batterySaverPolicyConfig = BatterySaverPolicyConfig.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            bool5 = setAdaptivePowerSavePolicy((BatterySaverPolicyConfig)str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 20:
            str3.enforceInterface("android.os.IPowerManager");
            bool10 = bool16;
            if (str3.readInt() != 0)
              bool10 = true; 
            n = str3.readInt();
            bool4 = setDynamicPowerSaveHint(bool10, n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 19:
            str3.enforceInterface("android.os.IPowerManager");
            bool10 = bool17;
            if (str3.readInt() != 0)
              bool10 = true; 
            bool4 = setPowerSaveModeEnabled(bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 18:
            str3.enforceInterface("android.os.IPowerManager");
            m = str3.readInt();
            powerSaveState = getPowerSaveState(m);
            param1Parcel2.writeNoException();
            if (powerSaveState != null) {
              param1Parcel2.writeInt(1);
              powerSaveState.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 17:
            powerSaveState.enforceInterface("android.os.IPowerManager");
            bool3 = isPowerSaveMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 16:
            powerSaveState.enforceInterface("android.os.IPowerManager");
            bool3 = isInteractive();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 15:
            powerSaveState.enforceInterface("android.os.IPowerManager");
            k = powerSaveState.readInt();
            f = getBrightnessConstraint(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeFloat(f);
            return true;
          case 14:
            powerSaveState.enforceInterface("android.os.IPowerManager");
            l = powerSaveState.readLong();
            nap(l);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            powerSaveState.enforceInterface("android.os.IPowerManager");
            l = powerSaveState.readLong();
            k = powerSaveState.readInt();
            param1Int2 = powerSaveState.readInt();
            goToSleep(l, k, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            powerSaveState.enforceInterface("android.os.IPowerManager");
            l = powerSaveState.readLong();
            k = powerSaveState.readInt();
            str6 = powerSaveState.readString();
            str2 = powerSaveState.readString();
            wakeUp(l, k, str6, str2);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str2.enforceInterface("android.os.IPowerManager");
            l = str2.readLong();
            param1Int2 = str2.readInt();
            k = str2.readInt();
            userActivity(l, param1Int2, k);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            str2.enforceInterface("android.os.IPowerManager");
            k = str2.readInt();
            bool2 = isWakeLockLevelSupported(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 9:
            str2.enforceInterface("android.os.IPowerManager");
            iBinder2 = str2.readStrongBinder();
            if (str2.readInt() != 0) {
              WorkSource workSource = WorkSource.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str6 = null;
            } 
            str2 = str2.readString();
            updateWakeLockWorkSource(iBinder2, (WorkSource)str6, str2);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str2.enforceInterface("android.os.IPowerManager");
            j = str2.readInt();
            bool10 = bool18;
            if (str2.readInt() != 0)
              bool10 = true; 
            bool1 = setPowerModeChecked(j, bool10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 7:
            str2.enforceInterface("android.os.IPowerManager");
            i = str2.readInt();
            bool10 = bool19;
            if (str2.readInt() != 0)
              bool10 = true; 
            setPowerMode(i, bool10);
            return true;
          case 6:
            str2.enforceInterface("android.os.IPowerManager");
            i = str2.readInt();
            param1Int2 = str2.readInt();
            setPowerBoost(i, param1Int2);
            return true;
          case 5:
            str2.enforceInterface("android.os.IPowerManager");
            param1Int2 = str2.readInt();
            i = str2.readInt();
            powerHint(param1Int2, i);
            return true;
          case 4:
            str2.enforceInterface("android.os.IPowerManager");
            iBinder1 = str2.readStrongBinder();
            arrayOfInt = str2.createIntArray();
            updateWakeLockUids(iBinder1, arrayOfInt);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            arrayOfInt.enforceInterface("android.os.IPowerManager");
            iBinder1 = arrayOfInt.readStrongBinder();
            i = arrayOfInt.readInt();
            releaseWakeLock(iBinder1, i);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            arrayOfInt.enforceInterface("android.os.IPowerManager");
            iBinder2 = arrayOfInt.readStrongBinder();
            param1Int2 = arrayOfInt.readInt();
            str8 = arrayOfInt.readString();
            str5 = arrayOfInt.readString();
            i = arrayOfInt.readInt();
            acquireWakeLockWithUid(iBinder2, param1Int2, str8, str5, i);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfInt.enforceInterface("android.os.IPowerManager");
        IBinder iBinder3 = arrayOfInt.readStrongBinder();
        int i = arrayOfInt.readInt();
        String str9 = arrayOfInt.readString();
        String str7 = arrayOfInt.readString();
        if (arrayOfInt.readInt() != 0) {
          WorkSource workSource = WorkSource.CREATOR.createFromParcel((Parcel)arrayOfInt);
        } else {
          str5 = null;
        } 
        String str1 = arrayOfInt.readString();
        acquireWakeLock(iBinder3, i, str9, str7, (WorkSource)str5, str1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.os.IPowerManager");
      return true;
    }
    
    private static class Proxy implements IPowerManager {
      public static IPowerManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IPowerManager";
      }
      
      public void acquireWakeLock(IBinder param2IBinder, int param2Int, String param2String1, String param2String2, WorkSource param2WorkSource, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeInt(param2Int);
              try {
                parcel1.writeString(param2String1);
                try {
                  parcel1.writeString(param2String2);
                  if (param2WorkSource != null) {
                    parcel1.writeInt(1);
                    param2WorkSource.writeToParcel(parcel1, 0);
                  } else {
                    parcel1.writeInt(0);
                  } 
                  try {
                    parcel1.writeString(param2String3);
                    boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
                    if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
                      IPowerManager.Stub.getDefaultImpl().acquireWakeLock(param2IBinder, param2Int, param2String1, param2String2, param2WorkSource, param2String3);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void acquireWakeLockWithUid(IBinder param2IBinder, int param2Int1, String param2String1, String param2String2, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().acquireWakeLockWithUid(param2IBinder, param2Int1, param2String1, param2String2, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseWakeLock(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().releaseWakeLock(param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateWakeLockUids(IBinder param2IBinder, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().updateWakeLockUids(param2IBinder, param2ArrayOfint);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void powerHint(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IPowerManager");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().powerHint(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPowerBoost(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IPowerManager");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().setPowerBoost(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPowerMode(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.os.IPowerManager");
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool1 && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().setPowerMode(param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean setPowerModeChecked(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IPowerManager.Stub.getDefaultImpl().setPowerModeChecked(param2Int, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateWakeLockWorkSource(IBinder param2IBinder, WorkSource param2WorkSource, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().updateWakeLockWorkSource(param2IBinder, param2WorkSource, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isWakeLockLevelSupported(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IPowerManager.Stub.getDefaultImpl() != null) {
            bool1 = IPowerManager.Stub.getDefaultImpl().isWakeLockLevelSupported(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void userActivity(long param2Long, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().userActivity(param2Long, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void wakeUp(long param2Long, int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().wakeUp(param2Long, param2Int, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void goToSleep(long param2Long, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().goToSleep(param2Long, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void nap(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().nap(param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public float getBrightnessConstraint(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null)
            return IPowerManager.Stub.getDefaultImpl().getBrightnessConstraint(param2Int); 
          parcel2.readException();
          return parcel2.readFloat();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInteractive() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && IPowerManager.Stub.getDefaultImpl() != null) {
            bool1 = IPowerManager.Stub.getDefaultImpl().isInteractive();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPowerSaveMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IPowerManager.Stub.getDefaultImpl() != null) {
            bool1 = IPowerManager.Stub.getDefaultImpl().isPowerSaveMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PowerSaveState getPowerSaveState(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          PowerSaveState powerSaveState;
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            powerSaveState = IPowerManager.Stub.getDefaultImpl().getPowerSaveState(param2Int);
            return powerSaveState;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            powerSaveState = PowerSaveState.CREATOR.createFromParcel(parcel2);
          } else {
            powerSaveState = null;
          } 
          return powerSaveState;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setPowerSaveModeEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool1 && IPowerManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IPowerManager.Stub.getDefaultImpl().setPowerSaveModeEnabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDynamicPowerSaveHint(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IPowerManager.Stub.getDefaultImpl().setDynamicPowerSaveHint(param2Boolean, param2Int);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAdaptivePowerSavePolicy(BatterySaverPolicyConfig param2BatterySaverPolicyConfig) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool1 = true;
          if (param2BatterySaverPolicyConfig != null) {
            parcel1.writeInt(1);
            param2BatterySaverPolicyConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool2 && IPowerManager.Stub.getDefaultImpl() != null) {
            bool1 = IPowerManager.Stub.getDefaultImpl().setAdaptivePowerSavePolicy(param2BatterySaverPolicyConfig);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAdaptivePowerSaveEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool1 && IPowerManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IPowerManager.Stub.getDefaultImpl().setAdaptivePowerSaveEnabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPowerSaveModeTrigger() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null)
            return IPowerManager.Stub.getDefaultImpl().getPowerSaveModeTrigger(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDeviceIdleMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(24, parcel1, parcel2, 0);
          if (!bool2 && IPowerManager.Stub.getDefaultImpl() != null) {
            bool1 = IPowerManager.Stub.getDefaultImpl().isDeviceIdleMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isLightDeviceIdleMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(25, parcel1, parcel2, 0);
          if (!bool2 && IPowerManager.Stub.getDefaultImpl() != null) {
            bool1 = IPowerManager.Stub.getDefaultImpl().isLightDeviceIdleMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reboot(boolean param2Boolean1, String param2String, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeString(param2String);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().reboot(param2Boolean1, param2String, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void rebootSafeMode(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().rebootSafeMode(param2Boolean1, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void shutdown(boolean param2Boolean1, String param2String, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeString(param2String);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().shutdown(param2Boolean1, param2String, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void crash(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().crash(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLastShutdownReason() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null)
            return IPowerManager.Stub.getDefaultImpl().getLastShutdownReason(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLastSleepReason() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null)
            return IPowerManager.Stub.getDefaultImpl().getLastSleepReason(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setStayOnSetting(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().setStayOnSetting(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void boostScreenBrightness(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().boostScreenBrightness(param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getFrameworksBlockedTime() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null)
            return IPowerManager.Stub.getDefaultImpl().getFrameworksBlockedTime(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Map getTopAppBlocked(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null)
            return IPowerManager.Stub.getDefaultImpl().getTopAppBlocked(param2Int); 
          parcel2.readException();
          ClassLoader classLoader = getClass().getClassLoader();
          return parcel2.readHashMap(classLoader);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getScreenState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null)
            return IPowerManager.Stub.getDefaultImpl().getScreenState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isScreenBrightnessBoosted() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(37, parcel1, parcel2, 0);
          if (!bool2 && IPowerManager.Stub.getDefaultImpl() != null) {
            bool1 = IPowerManager.Stub.getDefaultImpl().isScreenBrightnessBoosted();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAttentionLight(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool1 && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().setAttentionLight(param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDozeAfterScreenOff(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool1 && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().setDozeAfterScreenOff(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAmbientDisplayAvailable() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(40, parcel1, parcel2, 0);
          if (!bool2 && IPowerManager.Stub.getDefaultImpl() != null) {
            bool1 = IPowerManager.Stub.getDefaultImpl().isAmbientDisplayAvailable();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void suppressAmbientDisplay(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool1 && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().suppressAmbientDisplay(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAmbientDisplaySuppressedForToken(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(42, parcel1, parcel2, 0);
          if (!bool2 && IPowerManager.Stub.getDefaultImpl() != null) {
            bool1 = IPowerManager.Stub.getDefaultImpl().isAmbientDisplaySuppressedForToken(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAmbientDisplaySuppressed() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(43, parcel1, parcel2, 0);
          if (!bool2 && IPowerManager.Stub.getDefaultImpl() != null) {
            bool1 = IPowerManager.Stub.getDefaultImpl().isAmbientDisplaySuppressed();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean forceSuspend() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(44, parcel1, parcel2, 0);
          if (!bool2 && IPowerManager.Stub.getDefaultImpl() != null) {
            bool1 = IPowerManager.Stub.getDefaultImpl().forceSuspend();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDozeOverride(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().setDozeOverride(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopChargeForSale() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().stopChargeForSale();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resumeChargeForSale() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null) {
            IPowerManager.Stub.getDefaultImpl().resumeChargeForSale();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getCurrentChargeStateForSale() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null)
            return IPowerManager.Stub.getDefaultImpl().getCurrentChargeStateForSale(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getDisplayAodStatus() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(49, parcel1, parcel2, 0);
          if (!bool2 && IPowerManager.Stub.getDefaultImpl() != null) {
            bool1 = IPowerManager.Stub.getDefaultImpl().getDisplayAodStatus();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMinimumScreenBrightnessSetting() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null)
            return IPowerManager.Stub.getDefaultImpl().getMinimumScreenBrightnessSetting(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMaximumScreenBrightnessSetting() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null)
            return IPowerManager.Stub.getDefaultImpl().getMaximumScreenBrightnessSetting(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDefaultScreenBrightnessSetting() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IPowerManager");
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && IPowerManager.Stub.getDefaultImpl() != null)
            return IPowerManager.Stub.getDefaultImpl().getDefaultScreenBrightnessSetting(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPowerManager param1IPowerManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPowerManager != null) {
          Proxy.sDefaultImpl = param1IPowerManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPowerManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
