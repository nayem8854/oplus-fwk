package android.os;

import com.oplus.os.IOplusScreenStatusListener;

public class OplusCommonPowerManager implements IOplusCommonPowerManager {
  private final IBinder mRemote;
  
  public OplusCommonPowerManager(IBinder paramIBinder) {
    this.mRemote = paramIBinder;
  }
  
  public void registerScreenStatusListener(IOplusScreenStatusListener paramIOplusScreenStatusListener) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.os.IPowerManager");
      if (paramIOplusScreenStatusListener != null) {
        parcel1.writeInt(1);
        parcel1.writeStrongBinder(paramIOplusScreenStatusListener.asBinder());
      } else {
        parcel1.writeInt(0);
      } 
      transact(OplusCommonPowerTransaction.REGISTER_SCREEN_STATUS_LISTENER, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void unregisterScreenStatusListener(IOplusScreenStatusListener paramIOplusScreenStatusListener) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.os.IPowerManager");
      if (paramIOplusScreenStatusListener != null) {
        parcel1.writeInt(1);
        parcel1.writeStrongBinder(paramIOplusScreenStatusListener.asBinder());
      } else {
        parcel1.writeInt(0);
      } 
      transact(OplusCommonPowerTransaction.UNREGISTER_SCREEN_STATUS_LISTENER, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  private void transact(OplusCommonPowerTransaction paramOplusCommonPowerTransaction, Parcel paramParcel1, Parcel paramParcel2, int paramInt) throws RemoteException {
    this.mRemote.transact(paramOplusCommonPowerTransaction.ordinal() + 10001, paramParcel1, paramParcel2, paramInt);
  }
}
