package android.os;

public interface IVoldListener extends IInterface {
  void onDiskCreated(String paramString, int paramInt) throws RemoteException;
  
  void onDiskDestroyed(String paramString) throws RemoteException;
  
  void onDiskMetadataChanged(String paramString1, long paramLong, String paramString2, String paramString3) throws RemoteException;
  
  void onDiskScanned(String paramString) throws RemoteException;
  
  void onDiskStateChanged(String paramString) throws RemoteException;
  
  void onVolumeChecked(String paramString1, int paramInt, String paramString2, String paramString3) throws RemoteException;
  
  void onVolumeCreated(String paramString1, int paramInt1, String paramString2, String paramString3, int paramInt2) throws RemoteException;
  
  void onVolumeDestroyed(String paramString) throws RemoteException;
  
  void onVolumeInternalPathChanged(String paramString1, String paramString2) throws RemoteException;
  
  void onVolumeMetadataChanged(String paramString1, String paramString2, String paramString3, String paramString4) throws RemoteException;
  
  void onVolumePathChanged(String paramString1, String paramString2) throws RemoteException;
  
  void onVolumeStateChanged(String paramString, int paramInt) throws RemoteException;
  
  class Default implements IVoldListener {
    public void onDiskCreated(String param1String, int param1Int) throws RemoteException {}
    
    public void onDiskScanned(String param1String) throws RemoteException {}
    
    public void onDiskMetadataChanged(String param1String1, long param1Long, String param1String2, String param1String3) throws RemoteException {}
    
    public void onDiskDestroyed(String param1String) throws RemoteException {}
    
    public void onVolumeCreated(String param1String1, int param1Int1, String param1String2, String param1String3, int param1Int2) throws RemoteException {}
    
    public void onVolumeStateChanged(String param1String, int param1Int) throws RemoteException {}
    
    public void onVolumeMetadataChanged(String param1String1, String param1String2, String param1String3, String param1String4) throws RemoteException {}
    
    public void onVolumePathChanged(String param1String1, String param1String2) throws RemoteException {}
    
    public void onVolumeInternalPathChanged(String param1String1, String param1String2) throws RemoteException {}
    
    public void onVolumeDestroyed(String param1String) throws RemoteException {}
    
    public void onDiskStateChanged(String param1String) throws RemoteException {}
    
    public void onVolumeChecked(String param1String1, int param1Int, String param1String2, String param1String3) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVoldListener {
    private static final String DESCRIPTOR = "android.os.IVoldListener";
    
    static final int TRANSACTION_onDiskCreated = 1;
    
    static final int TRANSACTION_onDiskDestroyed = 4;
    
    static final int TRANSACTION_onDiskMetadataChanged = 3;
    
    static final int TRANSACTION_onDiskScanned = 2;
    
    static final int TRANSACTION_onDiskStateChanged = 11;
    
    static final int TRANSACTION_onVolumeChecked = 12;
    
    static final int TRANSACTION_onVolumeCreated = 5;
    
    static final int TRANSACTION_onVolumeDestroyed = 10;
    
    static final int TRANSACTION_onVolumeInternalPathChanged = 9;
    
    static final int TRANSACTION_onVolumeMetadataChanged = 7;
    
    static final int TRANSACTION_onVolumePathChanged = 8;
    
    static final int TRANSACTION_onVolumeStateChanged = 6;
    
    public Stub() {
      attachInterface(this, "android.os.IVoldListener");
    }
    
    public static IVoldListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IVoldListener");
      if (iInterface != null && iInterface instanceof IVoldListener)
        return (IVoldListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 12:
          return "onVolumeChecked";
        case 11:
          return "onDiskStateChanged";
        case 10:
          return "onVolumeDestroyed";
        case 9:
          return "onVolumeInternalPathChanged";
        case 8:
          return "onVolumePathChanged";
        case 7:
          return "onVolumeMetadataChanged";
        case 6:
          return "onVolumeStateChanged";
        case 5:
          return "onVolumeCreated";
        case 4:
          return "onDiskDestroyed";
        case 3:
          return "onDiskMetadataChanged";
        case 2:
          return "onDiskScanned";
        case 1:
          break;
      } 
      return "onDiskCreated";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str1, str2, str3;
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 12:
            param1Parcel1.enforceInterface("android.os.IVoldListener");
            str2 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            str = param1Parcel1.readString();
            str1 = param1Parcel1.readString();
            onVolumeChecked(str2, param1Int1, str, str1);
            return true;
          case 11:
            str1.enforceInterface("android.os.IVoldListener");
            str1 = str1.readString();
            onDiskStateChanged(str1);
            return true;
          case 10:
            str1.enforceInterface("android.os.IVoldListener");
            str1 = str1.readString();
            onVolumeDestroyed(str1);
            return true;
          case 9:
            str1.enforceInterface("android.os.IVoldListener");
            str = str1.readString();
            str1 = str1.readString();
            onVolumeInternalPathChanged(str, str1);
            return true;
          case 8:
            str1.enforceInterface("android.os.IVoldListener");
            str = str1.readString();
            str1 = str1.readString();
            onVolumePathChanged(str, str1);
            return true;
          case 7:
            str1.enforceInterface("android.os.IVoldListener");
            str3 = str1.readString();
            str2 = str1.readString();
            str = str1.readString();
            str1 = str1.readString();
            onVolumeMetadataChanged(str3, str2, str, str1);
            return true;
          case 6:
            str1.enforceInterface("android.os.IVoldListener");
            str = str1.readString();
            param1Int1 = str1.readInt();
            onVolumeStateChanged(str, param1Int1);
            return true;
          case 5:
            str1.enforceInterface("android.os.IVoldListener");
            str = str1.readString();
            param1Int2 = str1.readInt();
            str3 = str1.readString();
            str2 = str1.readString();
            param1Int1 = str1.readInt();
            onVolumeCreated(str, param1Int2, str3, str2, param1Int1);
            return true;
          case 4:
            str1.enforceInterface("android.os.IVoldListener");
            str1 = str1.readString();
            onDiskDestroyed(str1);
            return true;
          case 3:
            str1.enforceInterface("android.os.IVoldListener");
            str = str1.readString();
            l = str1.readLong();
            str2 = str1.readString();
            str1 = str1.readString();
            onDiskMetadataChanged(str, l, str2, str1);
            return true;
          case 2:
            str1.enforceInterface("android.os.IVoldListener");
            str1 = str1.readString();
            onDiskScanned(str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.os.IVoldListener");
        str = str1.readString();
        param1Int1 = str1.readInt();
        onDiskCreated(str, param1Int1);
        return true;
      } 
      str.writeString("android.os.IVoldListener");
      return true;
    }
    
    private static class Proxy implements IVoldListener {
      public static IVoldListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IVoldListener";
      }
      
      public void onDiskCreated(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldListener");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IVoldListener.Stub.getDefaultImpl() != null) {
            IVoldListener.Stub.getDefaultImpl().onDiskCreated(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDiskScanned(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IVoldListener.Stub.getDefaultImpl() != null) {
            IVoldListener.Stub.getDefaultImpl().onDiskScanned(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDiskMetadataChanged(String param2String1, long param2Long, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldListener");
          parcel.writeString(param2String1);
          parcel.writeLong(param2Long);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IVoldListener.Stub.getDefaultImpl() != null) {
            IVoldListener.Stub.getDefaultImpl().onDiskMetadataChanged(param2String1, param2Long, param2String2, param2String3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDiskDestroyed(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IVoldListener.Stub.getDefaultImpl() != null) {
            IVoldListener.Stub.getDefaultImpl().onDiskDestroyed(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVolumeCreated(String param2String1, int param2Int1, String param2String2, String param2String3, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldListener");
          parcel.writeString(param2String1);
          parcel.writeInt(param2Int1);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IVoldListener.Stub.getDefaultImpl() != null) {
            IVoldListener.Stub.getDefaultImpl().onVolumeCreated(param2String1, param2Int1, param2String2, param2String3, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVolumeStateChanged(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldListener");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IVoldListener.Stub.getDefaultImpl() != null) {
            IVoldListener.Stub.getDefaultImpl().onVolumeStateChanged(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVolumeMetadataChanged(String param2String1, String param2String2, String param2String3, String param2String4) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldListener");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          parcel.writeString(param2String4);
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IVoldListener.Stub.getDefaultImpl() != null) {
            IVoldListener.Stub.getDefaultImpl().onVolumeMetadataChanged(param2String1, param2String2, param2String3, param2String4);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVolumePathChanged(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldListener");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && IVoldListener.Stub.getDefaultImpl() != null) {
            IVoldListener.Stub.getDefaultImpl().onVolumePathChanged(param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVolumeInternalPathChanged(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldListener");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && IVoldListener.Stub.getDefaultImpl() != null) {
            IVoldListener.Stub.getDefaultImpl().onVolumeInternalPathChanged(param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVolumeDestroyed(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool && IVoldListener.Stub.getDefaultImpl() != null) {
            IVoldListener.Stub.getDefaultImpl().onVolumeDestroyed(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDiskStateChanged(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldListener");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && IVoldListener.Stub.getDefaultImpl() != null) {
            IVoldListener.Stub.getDefaultImpl().onDiskStateChanged(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVolumeChecked(String param2String1, int param2Int, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldListener");
          parcel.writeString(param2String1);
          parcel.writeInt(param2Int);
          parcel.writeString(param2String2);
          parcel.writeString(param2String3);
          boolean bool = this.mRemote.transact(12, parcel, (Parcel)null, 1);
          if (!bool && IVoldListener.Stub.getDefaultImpl() != null) {
            IVoldListener.Stub.getDefaultImpl().onVolumeChecked(param2String1, param2Int, param2String2, param2String3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVoldListener param1IVoldListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVoldListener != null) {
          Proxy.sDefaultImpl = param1IVoldListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVoldListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
