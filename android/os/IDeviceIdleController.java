package android.os;

import java.util.ArrayList;
import java.util.List;

public interface IDeviceIdleController extends IInterface {
  void addPowerSaveTempWhitelistApp(String paramString1, long paramLong, int paramInt, String paramString2) throws RemoteException;
  
  long addPowerSaveTempWhitelistAppForMms(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  long addPowerSaveTempWhitelistAppForSms(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  void addPowerSaveWhitelistApp(String paramString) throws RemoteException;
  
  int addPowerSaveWhitelistApps(List<String> paramList) throws RemoteException;
  
  void exitIdle(String paramString) throws RemoteException;
  
  int[] getAppIdTempWhitelist() throws RemoteException;
  
  int[] getAppIdUserWhitelist() throws RemoteException;
  
  int[] getAppIdWhitelist() throws RemoteException;
  
  int[] getAppIdWhitelistExceptIdle() throws RemoteException;
  
  String[] getFullPowerWhitelist() throws RemoteException;
  
  String[] getFullPowerWhitelistExceptIdle() throws RemoteException;
  
  String[] getRemovedSystemPowerWhitelistApps() throws RemoteException;
  
  String[] getSystemPowerWhitelist() throws RemoteException;
  
  String[] getSystemPowerWhitelistExceptIdle() throws RemoteException;
  
  String[] getUserPowerWhitelist() throws RemoteException;
  
  boolean isPowerSaveWhitelistApp(String paramString) throws RemoteException;
  
  boolean isPowerSaveWhitelistExceptIdleApp(String paramString) throws RemoteException;
  
  void removePowerSaveWhitelistApp(String paramString) throws RemoteException;
  
  void removeSystemPowerWhitelistApp(String paramString) throws RemoteException;
  
  void resetPreIdleTimeoutMode() throws RemoteException;
  
  void restoreSystemPowerWhitelistApp(String paramString) throws RemoteException;
  
  int setPreIdleTimeoutMode(int paramInt) throws RemoteException;
  
  long whitelistAppTemporarily(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  class Default implements IDeviceIdleController {
    public void addPowerSaveWhitelistApp(String param1String) throws RemoteException {}
    
    public int addPowerSaveWhitelistApps(List<String> param1List) throws RemoteException {
      return 0;
    }
    
    public void removePowerSaveWhitelistApp(String param1String) throws RemoteException {}
    
    public void removeSystemPowerWhitelistApp(String param1String) throws RemoteException {}
    
    public void restoreSystemPowerWhitelistApp(String param1String) throws RemoteException {}
    
    public String[] getRemovedSystemPowerWhitelistApps() throws RemoteException {
      return null;
    }
    
    public String[] getSystemPowerWhitelistExceptIdle() throws RemoteException {
      return null;
    }
    
    public String[] getSystemPowerWhitelist() throws RemoteException {
      return null;
    }
    
    public String[] getUserPowerWhitelist() throws RemoteException {
      return null;
    }
    
    public String[] getFullPowerWhitelistExceptIdle() throws RemoteException {
      return null;
    }
    
    public String[] getFullPowerWhitelist() throws RemoteException {
      return null;
    }
    
    public int[] getAppIdWhitelistExceptIdle() throws RemoteException {
      return null;
    }
    
    public int[] getAppIdWhitelist() throws RemoteException {
      return null;
    }
    
    public int[] getAppIdUserWhitelist() throws RemoteException {
      return null;
    }
    
    public int[] getAppIdTempWhitelist() throws RemoteException {
      return null;
    }
    
    public boolean isPowerSaveWhitelistExceptIdleApp(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean isPowerSaveWhitelistApp(String param1String) throws RemoteException {
      return false;
    }
    
    public void addPowerSaveTempWhitelistApp(String param1String1, long param1Long, int param1Int, String param1String2) throws RemoteException {}
    
    public long addPowerSaveTempWhitelistAppForMms(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return 0L;
    }
    
    public long addPowerSaveTempWhitelistAppForSms(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return 0L;
    }
    
    public long whitelistAppTemporarily(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return 0L;
    }
    
    public void exitIdle(String param1String) throws RemoteException {}
    
    public int setPreIdleTimeoutMode(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void resetPreIdleTimeoutMode() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDeviceIdleController {
    private static final String DESCRIPTOR = "android.os.IDeviceIdleController";
    
    static final int TRANSACTION_addPowerSaveTempWhitelistApp = 18;
    
    static final int TRANSACTION_addPowerSaveTempWhitelistAppForMms = 19;
    
    static final int TRANSACTION_addPowerSaveTempWhitelistAppForSms = 20;
    
    static final int TRANSACTION_addPowerSaveWhitelistApp = 1;
    
    static final int TRANSACTION_addPowerSaveWhitelistApps = 2;
    
    static final int TRANSACTION_exitIdle = 22;
    
    static final int TRANSACTION_getAppIdTempWhitelist = 15;
    
    static final int TRANSACTION_getAppIdUserWhitelist = 14;
    
    static final int TRANSACTION_getAppIdWhitelist = 13;
    
    static final int TRANSACTION_getAppIdWhitelistExceptIdle = 12;
    
    static final int TRANSACTION_getFullPowerWhitelist = 11;
    
    static final int TRANSACTION_getFullPowerWhitelistExceptIdle = 10;
    
    static final int TRANSACTION_getRemovedSystemPowerWhitelistApps = 6;
    
    static final int TRANSACTION_getSystemPowerWhitelist = 8;
    
    static final int TRANSACTION_getSystemPowerWhitelistExceptIdle = 7;
    
    static final int TRANSACTION_getUserPowerWhitelist = 9;
    
    static final int TRANSACTION_isPowerSaveWhitelistApp = 17;
    
    static final int TRANSACTION_isPowerSaveWhitelistExceptIdleApp = 16;
    
    static final int TRANSACTION_removePowerSaveWhitelistApp = 3;
    
    static final int TRANSACTION_removeSystemPowerWhitelistApp = 4;
    
    static final int TRANSACTION_resetPreIdleTimeoutMode = 24;
    
    static final int TRANSACTION_restoreSystemPowerWhitelistApp = 5;
    
    static final int TRANSACTION_setPreIdleTimeoutMode = 23;
    
    static final int TRANSACTION_whitelistAppTemporarily = 21;
    
    public Stub() {
      attachInterface(this, "android.os.IDeviceIdleController");
    }
    
    public static IDeviceIdleController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IDeviceIdleController");
      if (iInterface != null && iInterface instanceof IDeviceIdleController)
        return (IDeviceIdleController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 24:
          return "resetPreIdleTimeoutMode";
        case 23:
          return "setPreIdleTimeoutMode";
        case 22:
          return "exitIdle";
        case 21:
          return "whitelistAppTemporarily";
        case 20:
          return "addPowerSaveTempWhitelistAppForSms";
        case 19:
          return "addPowerSaveTempWhitelistAppForMms";
        case 18:
          return "addPowerSaveTempWhitelistApp";
        case 17:
          return "isPowerSaveWhitelistApp";
        case 16:
          return "isPowerSaveWhitelistExceptIdleApp";
        case 15:
          return "getAppIdTempWhitelist";
        case 14:
          return "getAppIdUserWhitelist";
        case 13:
          return "getAppIdWhitelist";
        case 12:
          return "getAppIdWhitelistExceptIdle";
        case 11:
          return "getFullPowerWhitelist";
        case 10:
          return "getFullPowerWhitelistExceptIdle";
        case 9:
          return "getUserPowerWhitelist";
        case 8:
          return "getSystemPowerWhitelist";
        case 7:
          return "getSystemPowerWhitelistExceptIdle";
        case 6:
          return "getRemovedSystemPowerWhitelistApps";
        case 5:
          return "restoreSystemPowerWhitelistApp";
        case 4:
          return "removeSystemPowerWhitelistApp";
        case 3:
          return "removePowerSaveWhitelistApp";
        case 2:
          return "addPowerSaveWhitelistApps";
        case 1:
          break;
      } 
      return "addPowerSaveWhitelistApp";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        int i;
        String str3;
        int[] arrayOfInt;
        String arrayOfString[], str2;
        ArrayList<String> arrayList;
        String str4;
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 24:
            param1Parcel1.enforceInterface("android.os.IDeviceIdleController");
            resetPreIdleTimeoutMode();
            param1Parcel2.writeNoException();
            return true;
          case 23:
            param1Parcel1.enforceInterface("android.os.IDeviceIdleController");
            param1Int1 = param1Parcel1.readInt();
            param1Int1 = setPreIdleTimeoutMode(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 22:
            param1Parcel1.enforceInterface("android.os.IDeviceIdleController");
            str3 = param1Parcel1.readString();
            exitIdle(str3);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            str3.enforceInterface("android.os.IDeviceIdleController");
            str4 = str3.readString();
            param1Int1 = str3.readInt();
            str3 = str3.readString();
            l = whitelistAppTemporarily(str4, param1Int1, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 20:
            str3.enforceInterface("android.os.IDeviceIdleController");
            str4 = str3.readString();
            param1Int1 = str3.readInt();
            str3 = str3.readString();
            l = addPowerSaveTempWhitelistAppForSms(str4, param1Int1, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 19:
            str3.enforceInterface("android.os.IDeviceIdleController");
            str4 = str3.readString();
            param1Int1 = str3.readInt();
            str3 = str3.readString();
            l = addPowerSaveTempWhitelistAppForMms(str4, param1Int1, str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 18:
            str3.enforceInterface("android.os.IDeviceIdleController");
            str4 = str3.readString();
            l = str3.readLong();
            param1Int1 = str3.readInt();
            str3 = str3.readString();
            addPowerSaveTempWhitelistApp(str4, l, param1Int1, str3);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            str3.enforceInterface("android.os.IDeviceIdleController");
            str3 = str3.readString();
            bool = isPowerSaveWhitelistApp(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 16:
            str3.enforceInterface("android.os.IDeviceIdleController");
            str3 = str3.readString();
            bool = isPowerSaveWhitelistExceptIdleApp(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 15:
            str3.enforceInterface("android.os.IDeviceIdleController");
            arrayOfInt = getAppIdTempWhitelist();
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 14:
            arrayOfInt.enforceInterface("android.os.IDeviceIdleController");
            arrayOfInt = getAppIdUserWhitelist();
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 13:
            arrayOfInt.enforceInterface("android.os.IDeviceIdleController");
            arrayOfInt = getAppIdWhitelist();
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 12:
            arrayOfInt.enforceInterface("android.os.IDeviceIdleController");
            arrayOfInt = getAppIdWhitelistExceptIdle();
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 11:
            arrayOfInt.enforceInterface("android.os.IDeviceIdleController");
            arrayOfString = getFullPowerWhitelist();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 10:
            arrayOfString.enforceInterface("android.os.IDeviceIdleController");
            arrayOfString = getFullPowerWhitelistExceptIdle();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 9:
            arrayOfString.enforceInterface("android.os.IDeviceIdleController");
            arrayOfString = getUserPowerWhitelist();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 8:
            arrayOfString.enforceInterface("android.os.IDeviceIdleController");
            arrayOfString = getSystemPowerWhitelist();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 7:
            arrayOfString.enforceInterface("android.os.IDeviceIdleController");
            arrayOfString = getSystemPowerWhitelistExceptIdle();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 6:
            arrayOfString.enforceInterface("android.os.IDeviceIdleController");
            arrayOfString = getRemovedSystemPowerWhitelistApps();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 5:
            arrayOfString.enforceInterface("android.os.IDeviceIdleController");
            str2 = arrayOfString.readString();
            restoreSystemPowerWhitelistApp(str2);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str2.enforceInterface("android.os.IDeviceIdleController");
            str2 = str2.readString();
            removeSystemPowerWhitelistApp(str2);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str2.enforceInterface("android.os.IDeviceIdleController");
            str2 = str2.readString();
            removePowerSaveWhitelistApp(str2);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str2.enforceInterface("android.os.IDeviceIdleController");
            arrayList = str2.createStringArrayList();
            i = addPowerSaveWhitelistApps(arrayList);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 1:
            break;
        } 
        arrayList.enforceInterface("android.os.IDeviceIdleController");
        String str1 = arrayList.readString();
        addPowerSaveWhitelistApp(str1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.os.IDeviceIdleController");
      return true;
    }
    
    private static class Proxy implements IDeviceIdleController {
      public static IDeviceIdleController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IDeviceIdleController";
      }
      
      public void addPowerSaveWhitelistApp(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null) {
            IDeviceIdleController.Stub.getDefaultImpl().addPowerSaveWhitelistApp(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int addPowerSaveWhitelistApps(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().addPowerSaveWhitelistApps(param2List); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removePowerSaveWhitelistApp(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null) {
            IDeviceIdleController.Stub.getDefaultImpl().removePowerSaveWhitelistApp(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeSystemPowerWhitelistApp(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null) {
            IDeviceIdleController.Stub.getDefaultImpl().removeSystemPowerWhitelistApp(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void restoreSystemPowerWhitelistApp(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null) {
            IDeviceIdleController.Stub.getDefaultImpl().restoreSystemPowerWhitelistApp(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getRemovedSystemPowerWhitelistApps() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().getRemovedSystemPowerWhitelistApps(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getSystemPowerWhitelistExceptIdle() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().getSystemPowerWhitelistExceptIdle(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getSystemPowerWhitelist() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().getSystemPowerWhitelist(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getUserPowerWhitelist() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().getUserPowerWhitelist(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getFullPowerWhitelistExceptIdle() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().getFullPowerWhitelistExceptIdle(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getFullPowerWhitelist() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().getFullPowerWhitelist(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getAppIdWhitelistExceptIdle() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().getAppIdWhitelistExceptIdle(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getAppIdWhitelist() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().getAppIdWhitelist(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getAppIdUserWhitelist() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().getAppIdUserWhitelist(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getAppIdTempWhitelist() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().getAppIdTempWhitelist(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPowerSaveWhitelistExceptIdleApp(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && IDeviceIdleController.Stub.getDefaultImpl() != null) {
            bool1 = IDeviceIdleController.Stub.getDefaultImpl().isPowerSaveWhitelistExceptIdleApp(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPowerSaveWhitelistApp(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IDeviceIdleController.Stub.getDefaultImpl() != null) {
            bool1 = IDeviceIdleController.Stub.getDefaultImpl().isPowerSaveWhitelistApp(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addPowerSaveTempWhitelistApp(String param2String1, long param2Long, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          parcel1.writeString(param2String1);
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null) {
            IDeviceIdleController.Stub.getDefaultImpl().addPowerSaveTempWhitelistApp(param2String1, param2Long, param2Int, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long addPowerSaveTempWhitelistAppForMms(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().addPowerSaveTempWhitelistAppForMms(param2String1, param2Int, param2String2); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long addPowerSaveTempWhitelistAppForSms(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().addPowerSaveTempWhitelistAppForSms(param2String1, param2Int, param2String2); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long whitelistAppTemporarily(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null)
            return IDeviceIdleController.Stub.getDefaultImpl().whitelistAppTemporarily(param2String1, param2Int, param2String2); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void exitIdle(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null) {
            IDeviceIdleController.Stub.getDefaultImpl().exitIdle(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setPreIdleTimeoutMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null) {
            param2Int = IDeviceIdleController.Stub.getDefaultImpl().setPreIdleTimeoutMode(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetPreIdleTimeoutMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdleController");
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IDeviceIdleController.Stub.getDefaultImpl() != null) {
            IDeviceIdleController.Stub.getDefaultImpl().resetPreIdleTimeoutMode();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDeviceIdleController param1IDeviceIdleController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDeviceIdleController != null) {
          Proxy.sDefaultImpl = param1IDeviceIdleController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDeviceIdleController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
