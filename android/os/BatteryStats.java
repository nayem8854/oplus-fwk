package android.os;

import android.app.ActivityManager;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.telephony.CellSignalStrength;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.ArrayMap;
import android.util.LongSparseArray;
import android.util.MutableBoolean;
import android.util.Pair;
import android.util.Printer;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.TimeUtils;
import android.util.proto.ProtoOutputStream;
import com.android.internal.os.BatterySipper;
import com.android.internal.os.BatteryStatsHelper;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class BatteryStats extends OplusBaseBatteryStats implements Parcelable {
  private static final String AGGREGATED_WAKELOCK_DATA = "awl";
  
  public static final int AGGREGATED_WAKE_TYPE_PARTIAL = 20;
  
  private static final String APK_DATA = "apk";
  
  private static final String AUDIO_DATA = "aud";
  
  public static final int AUDIO_TURNED_ON = 15;
  
  private static final String BATTERY_DATA = "bt";
  
  private static final String BATTERY_DISCHARGE_DATA = "dc";
  
  private static final String BATTERY_LEVEL_DATA = "lv";
  
  private static final int BATTERY_STATS_CHECKIN_VERSION = 9;
  
  private static final String BLUETOOTH_CONTROLLER_DATA = "ble";
  
  private static final String BLUETOOTH_MISC_DATA = "blem";
  
  public static final int BLUETOOTH_SCAN_ON = 19;
  
  public static final int BLUETOOTH_UNOPTIMIZED_SCAN_ON = 21;
  
  private static final long BYTES_PER_GB = 1073741824L;
  
  private static final long BYTES_PER_KB = 1024L;
  
  private static final long BYTES_PER_MB = 1048576L;
  
  private static final String CAMERA_DATA = "cam";
  
  public static final int CAMERA_TURNED_ON = 17;
  
  private static final String CELLULAR_CONTROLLER_NAME = "Cellular";
  
  private static final String CHARGE_STEP_DATA = "csd";
  
  private static final String CHARGE_TIME_REMAIN_DATA = "ctr";
  
  static final int CHECKIN_VERSION = 35;
  
  private static final String CPU_DATA = "cpu";
  
  private static final String CPU_TIMES_AT_FREQ_DATA = "ctf";
  
  private static final String DATA_CONNECTION_COUNT_DATA = "dcc";
  
  public static final int DATA_CONNECTION_EMERGENCY_SERVICE;
  
  static final String[] DATA_CONNECTION_NAMES;
  
  public static final int DATA_CONNECTION_OTHER;
  
  public static final int DATA_CONNECTION_OUT_OF_SERVICE = 0;
  
  private static final String DATA_CONNECTION_TIME_DATA = "dct";
  
  public static final int DEVICE_IDLE_MODE_DEEP = 2;
  
  public static final int DEVICE_IDLE_MODE_LIGHT = 1;
  
  public static final int DEVICE_IDLE_MODE_OFF = 0;
  
  private static final String DISCHARGE_STEP_DATA = "dsd";
  
  private static final String DISCHARGE_TIME_REMAIN_DATA = "dtr";
  
  public static final int DUMP_CHARGED_ONLY = 2;
  
  public static final int DUMP_DAILY_ONLY = 4;
  
  public static final int DUMP_DEVICE_WIFI_ONLY = 64;
  
  public static final int DUMP_HISTORY_ONLY = 8;
  
  public static final int DUMP_INCLUDE_HISTORY = 16;
  
  public static final int DUMP_VERBOSE = 32;
  
  private static final String FLASHLIGHT_DATA = "fla";
  
  public static final int FLASHLIGHT_TURNED_ON = 16;
  
  public static final int FOREGROUND_ACTIVITY = 10;
  
  private static final String FOREGROUND_ACTIVITY_DATA = "fg";
  
  public static final int FOREGROUND_SERVICE = 22;
  
  private static final String FOREGROUND_SERVICE_DATA = "fgs";
  
  public static final int FULL_WIFI_LOCK = 5;
  
  private static final String GLOBAL_BLUETOOTH_CONTROLLER_DATA = "gble";
  
  private static final String GLOBAL_CPU_FREQ_DATA = "gcf";
  
  private static final String GLOBAL_MODEM_CONTROLLER_DATA = "gmcd";
  
  private static final String GLOBAL_NETWORK_DATA = "gn";
  
  private static final String GLOBAL_WIFI_CONTROLLER_DATA = "gwfcd";
  
  private static final String GLOBAL_WIFI_DATA = "gwfl";
  
  private static final String HISTORY_DATA = "h";
  
  public static final String[] HISTORY_EVENT_CHECKIN_NAMES;
  
  public static final IntToString[] HISTORY_EVENT_INT_FORMATTERS;
  
  public static final String[] HISTORY_EVENT_NAMES;
  
  public static final BitDescription[] HISTORY_STATE2_DESCRIPTIONS;
  
  public static final BitDescription[] HISTORY_STATE_DESCRIPTIONS;
  
  private static final String HISTORY_STRING_POOL = "hsp";
  
  public static final int JOB = 14;
  
  private static final String JOBS_DEFERRED_DATA = "jbd";
  
  private static final String JOB_COMPLETION_DATA = "jbc";
  
  private static final String JOB_DATA = "jb";
  
  public static final long[] JOB_FRESHNESS_BUCKETS;
  
  private static final String KERNEL_WAKELOCK_DATA = "kwl";
  
  private static final boolean LOCAL_LOGV = false;
  
  public static final int MAX_TRACKED_SCREEN_STATE = 4;
  
  public static final double MILLISECONDS_IN_HOUR = 3600000.0D;
  
  private static final String MISC_DATA = "m";
  
  private static final String MODEM_CONTROLLER_DATA = "mcd";
  
  public static final int NETWORK_BT_RX_DATA = 4;
  
  public static final int NETWORK_BT_TX_DATA = 5;
  
  private static final String NETWORK_DATA = "nt";
  
  public static final int NETWORK_MOBILE_BG_RX_DATA = 6;
  
  public static final int NETWORK_MOBILE_BG_TX_DATA = 7;
  
  public static final int NETWORK_MOBILE_RX_DATA = 0;
  
  public static final int NETWORK_MOBILE_TX_DATA = 1;
  
  public static final int NETWORK_WIFI_BG_RX_DATA = 8;
  
  public static final int NETWORK_WIFI_BG_TX_DATA = 9;
  
  public static final int NETWORK_WIFI_RX_DATA = 2;
  
  public static final int NETWORK_WIFI_TX_DATA = 3;
  
  public static final int NUM_DATA_CONNECTION_TYPES;
  
  public static final int NUM_NETWORK_ACTIVITY_TYPES = 10;
  
  public static final int NUM_SCREEN_BRIGHTNESS_BINS = 5;
  
  public static final int NUM_WIFI_SIGNAL_STRENGTH_BINS = 5;
  
  private static final String POWER_USE_ITEM_DATA = "pwi";
  
  private static final String POWER_USE_SUMMARY_DATA = "pws";
  
  private static final String PROCESS_DATA = "pr";
  
  public static final int PROCESS_STATE = 12;
  
  private static final String RESOURCE_POWER_MANAGER_DATA = "rpm";
  
  public static final String RESULT_RECEIVER_CONTROLLER_KEY = "controller_activity";
  
  public static final int SCREEN_BRIGHTNESS = 200;
  
  public static final int SCREEN_BRIGHTNESS_BRIGHT = 4;
  
  public static final int SCREEN_BRIGHTNESS_DARK = 0;
  
  private static final String SCREEN_BRIGHTNESS_DATA = "br";
  
  public static final int SCREEN_BRIGHTNESS_DIM = 1;
  
  public static final int SCREEN_BRIGHTNESS_LIGHT = 3;
  
  public static final int SCREEN_BRIGHTNESS_MEDIUM = 2;
  
  static final String[] SCREEN_BRIGHTNESS_NAMES;
  
  static final String[] SCREEN_BRIGHTNESS_SHORT_NAMES;
  
  protected static final boolean SCREEN_OFF_RPM_STATS_ENABLED = false;
  
  public static final int SENSOR = 3;
  
  private static final String SENSOR_DATA = "sr";
  
  public static final String SERVICE_NAME = "batterystats";
  
  private static final String SIGNAL_SCANNING_TIME_DATA = "sst";
  
  private static final String SIGNAL_STRENGTH_COUNT_DATA = "sgc";
  
  private static final String SIGNAL_STRENGTH_TIME_DATA = "sgt";
  
  private static final String STATE_TIME_DATA = "st";
  
  @Deprecated
  public static final int STATS_CURRENT = 1;
  
  public static final int STATS_SINCE_CHARGED = 0;
  
  @Deprecated
  public static final int STATS_SINCE_UNPLUGGED = 2;
  
  private static final String[] STAT_NAMES = new String[] { "l", "c", "u" };
  
  public static final long STEP_LEVEL_INITIAL_MODE_MASK = 71776119061217280L;
  
  public static final int STEP_LEVEL_INITIAL_MODE_SHIFT = 48;
  
  public static final long STEP_LEVEL_LEVEL_MASK = 280375465082880L;
  
  public static final int STEP_LEVEL_LEVEL_SHIFT = 40;
  
  public static final int[] STEP_LEVEL_MODES_OF_INTEREST;
  
  public static final int STEP_LEVEL_MODE_DEVICE_IDLE = 8;
  
  public static final String[] STEP_LEVEL_MODE_LABELS;
  
  public static final int STEP_LEVEL_MODE_POWER_SAVE = 4;
  
  public static final int STEP_LEVEL_MODE_SCREEN_STATE = 3;
  
  public static final int[] STEP_LEVEL_MODE_VALUES;
  
  public static final long STEP_LEVEL_MODIFIED_MODE_MASK = -72057594037927936L;
  
  public static final int STEP_LEVEL_MODIFIED_MODE_SHIFT = 56;
  
  public static final long STEP_LEVEL_TIME_MASK = 1099511627775L;
  
  public static final int SYNC = 13;
  
  private static final String SYNC_DATA = "sy";
  
  private static final String TAG = "BatteryStats";
  
  private static final String UID_DATA = "uid";
  
  public static final String UID_TIMES_TYPE_ALL = "A";
  
  private static final String USER_ACTIVITY_DATA = "ua";
  
  private static final String VERSION_DATA = "vers";
  
  private static final String VIBRATOR_DATA = "vib";
  
  public static final int VIBRATOR_ON = 9;
  
  private static final String VIDEO_DATA = "vid";
  
  public static final int VIDEO_TURNED_ON = 8;
  
  private static final String WAKELOCK_DATA = "wl";
  
  private static final String WAKEUP_ALARM_DATA = "wua";
  
  private static final String WAKEUP_REASON_DATA = "wr";
  
  public static final int WAKE_TYPE_DRAW = 18;
  
  public static final int WAKE_TYPE_FULL = 1;
  
  public static final int WAKE_TYPE_PARTIAL = 0;
  
  public static final int WAKE_TYPE_WINDOW = 2;
  
  public static final int WIFI_AGGREGATE_MULTICAST_ENABLED = 23;
  
  public static final int WIFI_BATCHED_SCAN = 11;
  
  private static final String WIFI_CONTROLLER_DATA = "wfcd";
  
  private static final String WIFI_CONTROLLER_NAME = "WiFi";
  
  private static final String WIFI_DATA = "wfl";
  
  private static final String WIFI_MULTICAST_DATA = "wmc";
  
  public static final int WIFI_MULTICAST_ENABLED = 7;
  
  private static final String WIFI_MULTICAST_TOTAL_DATA = "wmct";
  
  public static final int WIFI_RUNNING = 4;
  
  public static final int WIFI_SCAN = 6;
  
  private static final String WIFI_SIGNAL_STRENGTH_COUNT_DATA = "wsgc";
  
  private static final String WIFI_SIGNAL_STRENGTH_TIME_DATA = "wsgt";
  
  private static final String WIFI_STATE_COUNT_DATA = "wsc";
  
  static final String[] WIFI_STATE_NAMES;
  
  private static final String WIFI_STATE_TIME_DATA = "wst";
  
  private static final String WIFI_SUPPL_STATE_COUNT_DATA = "wssc";
  
  static final String[] WIFI_SUPPL_STATE_NAMES;
  
  static final String[] WIFI_SUPPL_STATE_SHORT_NAMES;
  
  private static final String WIFI_SUPPL_STATE_TIME_DATA = "wsst";
  
  private static final IntToString sIntToString;
  
  private static final IntToString sUidToString;
  
  private final StringBuilder mFormatBuilder = new StringBuilder(32);
  
  private final Formatter mFormatter = new Formatter(this.mFormatBuilder);
  
  static {
    JOB_FRESHNESS_BUCKETS = new long[] { 3600000L, 7200000L, 14400000L, 28800000L, Long.MAX_VALUE };
    SCREEN_BRIGHTNESS_NAMES = new String[] { "dark", "dim", "medium", "light", "bright" };
    SCREEN_BRIGHTNESS_SHORT_NAMES = new String[] { "0", "1", "2", "3", "4" };
    int i = (TelephonyManager.getAllNetworkTypes()).length + 1;
    DATA_CONNECTION_OTHER = ++i;
    DATA_CONNECTION_NAMES = new String[] { 
        "oos", "gprs", "edge", "umts", "cdma", "evdo_0", "evdo_A", "1xrtt", "hsdpa", "hsupa", 
        "hspa", "iden", "evdo_b", "lte", "ehrpd", "hspap", "gsm", "td_scdma", "iwlan", "lte_ca", 
        "nr", "emngcy", "other" };
    NUM_DATA_CONNECTION_TYPES = i + 1;
    WIFI_SUPPL_STATE_NAMES = new String[] { 
        "invalid", "disconn", "disabled", "inactive", "scanning", "authenticating", "associating", "associated", "4-way-handshake", "group-handshake", 
        "completed", "dormant", "uninit" };
    WIFI_SUPPL_STATE_SHORT_NAMES = new String[] { 
        "inv", "dsc", "dis", "inact", "scan", "auth", "ascing", "asced", "4-way", "group", 
        "compl", "dorm", "uninit" };
    BitDescription bitDescription1 = new BitDescription(-2147483648, "running", "r"), bitDescription2 = new BitDescription(1073741824, "wake_lock", "w"), bitDescription3 = new BitDescription(8388608, "sensor", "s"), bitDescription4 = new BitDescription(536870912, "gps", "g"), bitDescription5 = new BitDescription(268435456, "wifi_full_lock", "Wl"), bitDescription6 = new BitDescription(134217728, "wifi_scan", "Ws"), bitDescription7 = new BitDescription(65536, "wifi_multicast", "Wm"), bitDescription8 = new BitDescription(67108864, "wifi_radio", "Wr"), bitDescription9 = new BitDescription(33554432, "mobile_radio", "Pr"), bitDescription10 = new BitDescription(2097152, "phone_scanning", "Psc"), bitDescription11 = new BitDescription(4194304, "audio", "a"), bitDescription12 = new BitDescription(1048576, "screen", "S"), bitDescription13 = new BitDescription(524288, "plugged", "BP"), bitDescription14 = new BitDescription(262144, "screen_doze", "Sd");
    String[] arrayOfString = DATA_CONNECTION_NAMES;
    HISTORY_STATE_DESCRIPTIONS = new BitDescription[] { 
        bitDescription1, bitDescription2, bitDescription3, bitDescription4, bitDescription5, bitDescription6, bitDescription7, bitDescription8, bitDescription9, bitDescription10, 
        bitDescription11, bitDescription12, bitDescription13, bitDescription14, new BitDescription(15872, 9, "data_conn", "Pcn", arrayOfString, arrayOfString), new BitDescription(448, 6, "phone_state", "Pst", new String[] { "in", "out", "emergency", "off" }, new String[] { "in", "out", "em", "off" }), new BitDescription(56, 3, "phone_signal_strength", "Pss", new String[] { "none", "poor", "moderate", "good", "great" }, new String[] { "0", "1", "2", "3", "4" }), new BitDescription(7, 0, "brightness", "Sb", SCREEN_BRIGHTNESS_NAMES, SCREEN_BRIGHTNESS_SHORT_NAMES) };
    HISTORY_STATE2_DESCRIPTIONS = new BitDescription[] { 
        new BitDescription(-2147483648, "power_save", "ps"), new BitDescription(1073741824, "video", "v"), new BitDescription(536870912, "wifi_running", "Ww"), new BitDescription(268435456, "wifi", "W"), new BitDescription(134217728, "flashlight", "fl"), new BitDescription(100663296, 25, "device_idle", "di", new String[] { "off", "light", "full", "???" }, new String[] { "off", "light", "full", "???" }), new BitDescription(16777216, "charging", "ch"), new BitDescription(262144, "usb_data", "Ud"), new BitDescription(8388608, "phone_in_call", "Pcl"), new BitDescription(4194304, "bluetooth", "b"), 
        new BitDescription(112, 4, "wifi_signal_strength", "Wss", new String[] { "0", "1", "2", "3", "4" }, new String[] { "0", "1", "2", "3", "4" }), new BitDescription(15, 0, "wifi_suppl", "Wsp", WIFI_SUPPL_STATE_NAMES, WIFI_SUPPL_STATE_SHORT_NAMES), new BitDescription(2097152, "camera", "ca"), new BitDescription(1048576, "ble_scan", "bles"), new BitDescription(524288, "cellular_high_tx_power", "Chtp"), new BitDescription(128, 7, "gps_signal_quality", "Gss", new String[] { "poor", "good" }, new String[] { "poor", "good" }) };
    HISTORY_EVENT_NAMES = new String[] { 
        "null", "proc", "fg", "top", "sync", "wake_lock_in", "job", "user", "userfg", "conn", 
        "active", "pkginst", "pkgunin", "alarm", "stats", "pkginactive", "pkgactive", "tmpwhitelist", "screenwake", "wakeupap", 
        "longwake", "est_capacity" };
    HISTORY_EVENT_CHECKIN_NAMES = new String[] { 
        "Enl", "Epr", "Efg", "Etp", "Esy", "Ewl", "Ejb", "Eur", "Euf", "Ecn", 
        "Eac", "Epi", "Epu", "Eal", "Est", "Eai", "Eaa", "Etw", "Esw", "Ewa", 
        "Elw", "Eec" };
    sUidToString = (IntToString)_$$Lambda$IyvVQC_0mKtsfXbnO0kDL64hrk0.INSTANCE;
    -$.Lambda.q1UvBdLgHRZVzc68BxdksTmbuCw q1UvBdLgHRZVzc68BxdksTmbuCw = _$$Lambda$q1UvBdLgHRZVzc68BxdksTmbuCw.INSTANCE;
    IntToString intToString = sUidToString;
    HISTORY_EVENT_INT_FORMATTERS = new IntToString[] { 
        intToString, intToString, intToString, intToString, intToString, intToString, intToString, intToString, intToString, intToString, 
        intToString, (IntToString)q1UvBdLgHRZVzc68BxdksTmbuCw, intToString, intToString, intToString, intToString, intToString, intToString, intToString, intToString, 
        intToString, (IntToString)q1UvBdLgHRZVzc68BxdksTmbuCw };
    WIFI_STATE_NAMES = new String[] { "off", "scanning", "no_net", "disconn", "sta", "p2p", "sta_p2p", "soft_ap" };
    STEP_LEVEL_MODES_OF_INTEREST = new int[] { 7, 15, 11, 7, 7, 7, 7, 7, 15, 11 };
    STEP_LEVEL_MODE_VALUES = new int[] { 0, 4, 8, 1, 5, 2, 6, 3, 7, 11 };
    STEP_LEVEL_MODE_LABELS = new String[] { "screen off", "screen off power save", "screen off device idle", "screen on", "screen on power save", "screen doze", "screen doze power save", "screen doze-suspend", "screen doze-suspend power save", "screen doze-suspend device idle" };
  }
  
  class Counter {
    public abstract int getCountLocked(int param1Int);
    
    public abstract void logState(Printer param1Printer, String param1String);
  }
  
  class LongCounter {
    public abstract long getCountLocked(int param1Int);
    
    public abstract void logState(Printer param1Printer, String param1String);
  }
  
  class LongCounterArray {
    public abstract long[] getCountsLocked(int param1Int);
    
    public abstract void logState(Printer param1Printer, String param1String);
  }
  
  class ControllerActivityCounter {
    public abstract BatteryStats.LongCounter getIdleTimeCounter();
    
    public abstract BatteryStats.LongCounter getMonitoredRailChargeConsumedMaMs();
    
    public abstract BatteryStats.LongCounter getPowerCounter();
    
    public abstract BatteryStats.LongCounter getRxTimeCounter();
    
    public abstract BatteryStats.LongCounter getScanTimeCounter();
    
    public abstract BatteryStats.LongCounter getSleepTimeCounter();
    
    public abstract BatteryStats.LongCounter[] getTxTimeCounters();
  }
  
  class Timer {
    public long getMaxDurationMsLocked(long param1Long) {
      return -1L;
    }
    
    public long getCurrentDurationMsLocked(long param1Long) {
      return -1L;
    }
    
    public long getTotalDurationMsLocked(long param1Long) {
      return -1L;
    }
    
    public Timer getSubTimer() {
      return null;
    }
    
    public boolean isRunningLocked() {
      return false;
    }
    
    public abstract int getCountLocked(int param1Int);
    
    public abstract long getTimeSinceMarkLocked(long param1Long);
    
    public abstract long getTotalTimeLocked(long param1Long, int param1Int);
    
    public abstract void logState(Printer param1Printer, String param1String);
  }
  
  public static int mapToInternalProcessState(int paramInt) {
    if (paramInt == 20)
      return 20; 
    if (paramInt == 2)
      return 0; 
    if (ActivityManager.isForegroundService(paramInt))
      return 1; 
    if (paramInt <= 6)
      return 2; 
    if (paramInt <= 11)
      return 3; 
    if (paramInt <= 12)
      return 4; 
    if (paramInt <= 13)
      return 5; 
    return 6;
  }
  
  class Uid {
    public static final int[] CRITICAL_PROC_STATES = new int[] { 0, 3, 1, 2 };
    
    public static final int NUM_PROCESS_STATE = 7;
    
    public static final int NUM_USER_ACTIVITY_TYPES;
    
    public static final int NUM_WIFI_BATCHED_SCAN_BINS = 5;
    
    public static final int PROCESS_STATE_BACKGROUND = 3;
    
    public static final int PROCESS_STATE_CACHED = 6;
    
    public static final int PROCESS_STATE_FOREGROUND = 2;
    
    public static final int PROCESS_STATE_FOREGROUND_SERVICE = 1;
    
    public static final int PROCESS_STATE_HEAVY_WEIGHT = 5;
    
    public static abstract class Wakelock {
      public abstract BatteryStats.Timer getWakeTime(int param2Int);
    }
    
    static final String[] PROCESS_STATE_NAMES = new String[] { "Top", "Fg Service", "Foreground", "Background", "Top Sleeping", "Heavy Weight", "Cached" };
    
    public static final int PROCESS_STATE_TOP = 0;
    
    public static final int PROCESS_STATE_TOP_SLEEPING = 4;
    
    public static final String[] UID_PROCESS_TYPES = new String[] { "T", "FS", "F", "B", "TS", "HW", "C" };
    
    static final String[] USER_ACTIVITY_TYPES;
    
    static {
      String[] arrayOfString = new String[5];
      arrayOfString[0] = "other";
      arrayOfString[1] = "button";
      arrayOfString[2] = "touch";
      arrayOfString[3] = "accessibility";
      arrayOfString[4] = "attention";
      USER_ACTIVITY_TYPES = arrayOfString;
      NUM_USER_ACTIVITY_TYPES = arrayOfString.length;
    }
    
    public abstract BatteryStats.Timer getAggregatedPartialWakelockTimer();
    
    public abstract BatteryStats.Timer getAudioTurnedOnTimer();
    
    public abstract BatteryStats.ControllerActivityCounter getBluetoothControllerActivity();
    
    public abstract BatteryStats.Timer getBluetoothScanBackgroundTimer();
    
    public abstract BatteryStats.Counter getBluetoothScanResultBgCounter();
    
    public abstract BatteryStats.Counter getBluetoothScanResultCounter();
    
    public abstract BatteryStats.Timer getBluetoothScanTimer();
    
    public abstract BatteryStats.Timer getBluetoothUnoptimizedScanBackgroundTimer();
    
    public abstract BatteryStats.Timer getBluetoothUnoptimizedScanTimer();
    
    public abstract BatteryStats.Timer getCameraTurnedOnTimer();
    
    public abstract long getCpuActiveTime();
    
    public abstract long[] getCpuClusterTimes();
    
    public abstract long[] getCpuFreqTimes(int param1Int);
    
    public abstract long[] getCpuFreqTimes(int param1Int1, int param1Int2);
    
    public abstract void getDeferredJobsCheckinLineLocked(StringBuilder param1StringBuilder, int param1Int);
    
    public abstract void getDeferredJobsLineLocked(StringBuilder param1StringBuilder, int param1Int);
    
    public abstract BatteryStats.Timer getFlashlightTurnedOnTimer();
    
    public abstract BatteryStats.Timer getForegroundActivityTimer();
    
    public abstract BatteryStats.Timer getForegroundServiceTimer();
    
    public abstract long getFullWifiLockTime(long param1Long, int param1Int);
    
    public abstract ArrayMap<String, SparseIntArray> getJobCompletionStats();
    
    public abstract ArrayMap<String, ? extends BatteryStats.Timer> getJobStats();
    
    public abstract int getMobileRadioActiveCount(int param1Int);
    
    public abstract long getMobileRadioActiveTime(int param1Int);
    
    public abstract long getMobileRadioApWakeupCount(int param1Int);
    
    public abstract BatteryStats.ControllerActivityCounter getModemControllerActivity();
    
    public abstract BatteryStats.Timer getMulticastWakelockStats();
    
    public abstract long getNetworkActivityBytes(int param1Int1, int param1Int2);
    
    public abstract long getNetworkActivityPackets(int param1Int1, int param1Int2);
    
    public abstract ArrayMap<String, ? extends Pkg> getPackageStats();
    
    public abstract SparseArray<? extends Pid> getPidStats();
    
    public abstract long getProcessStateTime(int param1Int1, long param1Long, int param1Int2);
    
    public abstract BatteryStats.Timer getProcessStateTimer(int param1Int);
    
    public abstract ArrayMap<String, ? extends Proc> getProcessStats();
    
    public abstract long[] getScreenOffCpuFreqTimes(int param1Int);
    
    public abstract long[] getScreenOffCpuFreqTimes(int param1Int1, int param1Int2);
    
    public abstract SparseArray<? extends Sensor> getSensorStats();
    
    public abstract ArrayMap<String, ? extends BatteryStats.Timer> getSyncStats();
    
    public abstract long getSystemCpuTimeUs(int param1Int);
    
    public abstract long getTimeAtCpuSpeed(int param1Int1, int param1Int2, int param1Int3);
    
    public abstract int getUid();
    
    public abstract int getUserActivityCount(int param1Int1, int param1Int2);
    
    public abstract long getUserCpuTimeUs(int param1Int);
    
    public abstract BatteryStats.Timer getVibratorOnTimer();
    
    public abstract BatteryStats.Timer getVideoTurnedOnTimer();
    
    public abstract ArrayMap<String, ? extends Wakelock> getWakelockStats();
    
    public abstract int getWifiBatchedScanCount(int param1Int1, int param1Int2);
    
    public abstract long getWifiBatchedScanTime(int param1Int1, long param1Long, int param1Int2);
    
    public abstract BatteryStats.ControllerActivityCounter getWifiControllerActivity();
    
    public abstract long getWifiMulticastTime(long param1Long, int param1Int);
    
    public abstract long getWifiRadioApWakeupCount(int param1Int);
    
    public abstract long getWifiRunningTime(long param1Long, int param1Int);
    
    public abstract long getWifiScanActualTime(long param1Long);
    
    public abstract int getWifiScanBackgroundCount(int param1Int);
    
    public abstract long getWifiScanBackgroundTime(long param1Long);
    
    public abstract BatteryStats.Timer getWifiScanBackgroundTimer();
    
    public abstract int getWifiScanCount(int param1Int);
    
    public abstract long getWifiScanTime(long param1Long, int param1Int);
    
    public abstract BatteryStats.Timer getWifiScanTimer();
    
    public abstract boolean hasNetworkActivity();
    
    public abstract boolean hasUserActivity();
    
    public abstract void noteActivityPausedLocked(long param1Long);
    
    public abstract void noteActivityResumedLocked(long param1Long);
    
    public abstract void noteFullWifiLockAcquiredLocked(long param1Long);
    
    public abstract void noteFullWifiLockReleasedLocked(long param1Long);
    
    public abstract void noteUserActivityLocked(int param1Int);
    
    public abstract void noteWifiBatchedScanStartedLocked(int param1Int, long param1Long);
    
    public abstract void noteWifiBatchedScanStoppedLocked(long param1Long);
    
    public abstract void noteWifiMulticastDisabledLocked(long param1Long);
    
    public abstract void noteWifiMulticastEnabledLocked(long param1Long);
    
    public abstract void noteWifiRunningLocked(long param1Long);
    
    public abstract void noteWifiScanStartedLocked(long param1Long);
    
    public abstract void noteWifiScanStoppedLocked(long param1Long);
    
    public abstract void noteWifiStoppedLocked(long param1Long);
    
    public static abstract class Sensor {
      public static final int GPS = -10000;
      
      public abstract int getHandle();
      
      public abstract BatteryStats.Timer getSensorBackgroundTime();
      
      public abstract BatteryStats.Timer getSensorTime();
    }
    
    public class Pid {
      public int mWakeNesting;
      
      public long mWakeStartMs;
      
      public long mWakeSumMs;
      
      final BatteryStats.Uid this$0;
    }
    
    public static abstract class Proc {
      public abstract int countExcessivePowers();
      
      public abstract ExcessivePower getExcessivePower(int param2Int);
      
      public abstract long getForegroundTime(int param2Int);
      
      public abstract int getNumAnrs(int param2Int);
      
      public abstract int getNumCrashes(int param2Int);
      
      public abstract int getStarts(int param2Int);
      
      public abstract long getSystemTime(int param2Int);
      
      public abstract long getUserTime(int param2Int);
      
      public abstract boolean isActive();
      
      public static class ExcessivePower {
        public static final int TYPE_CPU = 2;
        
        public static final int TYPE_WAKE = 1;
        
        public long overTime;
        
        public int type;
        
        public long usedTime;
      }
    }
    
    public static class ExcessivePower {
      public static final int TYPE_CPU = 2;
      
      public static final int TYPE_WAKE = 1;
      
      public long overTime;
      
      public int type;
      
      public long usedTime;
    }
    
    public static abstract class Pkg {
      public abstract ArrayMap<String, ? extends Serv> getServiceStats();
      
      public abstract ArrayMap<String, ? extends BatteryStats.Counter> getWakeupAlarmStats();
      
      public static abstract class Serv {
        public abstract int getLaunches(int param3Int);
        
        public abstract long getStartTime(long param3Long, int param3Int);
        
        public abstract int getStarts(int param3Int);
      }
    }
    
    public static abstract class Serv {
      public abstract int getLaunches(int param2Int);
      
      public abstract long getStartTime(long param2Long, int param2Int);
      
      public abstract int getStarts(int param2Int);
    }
  }
  
  class LevelStepTracker {
    public long mLastStepTime = -1L;
    
    public int mNumStepDurations;
    
    public final long[] mStepDurations;
    
    public LevelStepTracker(BatteryStats this$0) {
      this.mStepDurations = new long[this$0];
    }
    
    public LevelStepTracker(BatteryStats this$0, long[] param1ArrayOflong) {
      this.mNumStepDurations = this$0;
      long[] arrayOfLong = new long[this$0];
      System.arraycopy(param1ArrayOflong, 0, arrayOfLong, 0, this$0);
    }
    
    public long getDurationAt(int param1Int) {
      return this.mStepDurations[param1Int] & 0xFFFFFFFFFFL;
    }
    
    public int getLevelAt(int param1Int) {
      return (int)((this.mStepDurations[param1Int] & 0xFF0000000000L) >> 40L);
    }
    
    public int getInitModeAt(int param1Int) {
      return (int)((this.mStepDurations[param1Int] & 0xFF000000000000L) >> 48L);
    }
    
    public int getModModeAt(int param1Int) {
      return (int)((this.mStepDurations[param1Int] & 0xFF00000000000000L) >> 56L);
    }
    
    private void appendHex(long param1Long, int param1Int, StringBuilder param1StringBuilder) {
      int i = 0, j = param1Int;
      param1Int = i;
      while (j >= 0) {
        i = (int)(param1Long >> j & 0xFL);
        j -= 4;
        if (param1Int == 0 && i == 0)
          continue; 
        param1Int = 1;
        if (i >= 0 && i <= 9) {
          param1StringBuilder.append((char)(i + 48));
          continue;
        } 
        param1StringBuilder.append((char)(i + 97 - 10));
      } 
    }
    
    public void encodeEntryAt(int param1Int, StringBuilder param1StringBuilder) {
      long l = this.mStepDurations[param1Int];
      int i = (int)((0xFF0000000000L & l) >> 40L);
      int j = (int)((0xFF000000000000L & l) >> 48L);
      param1Int = (int)((0xFF00000000000000L & l) >> 56L);
      int k = (j & 0x3) + 1;
      if (k != 1) {
        if (k != 2) {
          if (k != 3) {
            if (k == 4)
              param1StringBuilder.append('z'); 
          } else {
            param1StringBuilder.append('d');
          } 
        } else {
          param1StringBuilder.append('o');
        } 
      } else {
        param1StringBuilder.append('f');
      } 
      if ((j & 0x4) != 0)
        param1StringBuilder.append('p'); 
      if ((j & 0x8) != 0)
        param1StringBuilder.append('i'); 
      k = (param1Int & 0x3) + 1;
      if (k != 1) {
        if (k != 2) {
          if (k != 3) {
            if (k == 4)
              param1StringBuilder.append('Z'); 
          } else {
            param1StringBuilder.append('D');
          } 
        } else {
          param1StringBuilder.append('O');
        } 
      } else {
        param1StringBuilder.append('F');
      } 
      if ((param1Int & 0x4) != 0)
        param1StringBuilder.append('P'); 
      if ((param1Int & 0x8) != 0)
        param1StringBuilder.append('I'); 
      param1StringBuilder.append('-');
      appendHex(i, 4, param1StringBuilder);
      param1StringBuilder.append('-');
      appendHex(0xFFFFFFFFFFL & l, 36, param1StringBuilder);
    }
    
    public void decodeEntryAt(int param1Int, String param1String) {
      int i = param1String.length();
      int j = 0;
      long l1 = 0L;
      while (j < i) {
        char c = param1String.charAt(j);
        if (c != '-') {
          long l;
          j++;
          if (c != 'D') {
            if (c != 'F') {
              if (c != 'I') {
                if (c != 'Z') {
                  if (c != 'd') {
                    if (c != 'f') {
                      if (c != 'i') {
                        if (c != 'z') {
                          if (c != 'O') {
                            if (c != 'P') {
                              if (c != 'o') {
                                if (c != 'p') {
                                  l = l1;
                                } else {
                                  l = l1 | 0x4000000000000L;
                                } 
                              } else {
                                l = l1 | 0x1000000000000L;
                              } 
                            } else {
                              l = l1 | 0x400000000000000L;
                            } 
                          } else {
                            l = l1 | 0x100000000000000L;
                          } 
                        } else {
                          l = l1 | 0x3000000000000L;
                        } 
                      } else {
                        l = l1 | 0x8000000000000L;
                      } 
                    } else {
                      l = l1 | 0x0L;
                    } 
                  } else {
                    l = l1 | 0x2000000000000L;
                  } 
                } else {
                  l = l1 | 0x300000000000000L;
                } 
              } else {
                l = l1 | 0x800000000000000L;
              } 
            } else {
              l = l1 | 0x0L;
            } 
          } else {
            l = l1 | 0x200000000000000L;
          } 
          l1 = l;
        } 
      } 
      j++;
      long l2 = 0L;
      while (j < i) {
        char c = param1String.charAt(j);
        if (c != '-') {
          int k = j + 1;
          long l = l2 << 4L;
          if (c >= '0' && c <= '9') {
            l2 = l + (c - 48);
            j = k;
            continue;
          } 
          if (c >= 'a' && c <= 'f') {
            l2 = l + (c - 97 + 10);
            j = k;
            continue;
          } 
          j = k;
          l2 = l;
          if (c >= 'A') {
            j = k;
            l2 = l;
            if (c <= 'F') {
              l2 = l + (c - 65 + 10);
              j = k;
            } 
          } 
        } 
      } 
      j++;
      long l3 = 0L;
      while (j < i) {
        char c = param1String.charAt(j);
        if (c != '-') {
          j++;
          l3 <<= 4L;
          if (c >= '0' && c <= '9') {
            l3 += (c - 48);
            continue;
          } 
          if (c >= 'a' && c <= 'f') {
            l3 += (c - 97 + 10);
            continue;
          } 
          if (c >= 'A' && c <= 'F')
            l3 += (c - 65 + 10); 
        } 
      } 
      this.mStepDurations[param1Int] = 0xFFFFFFFFFFL & l3 | l1 | l2 << 40L & 0xFF0000000000L;
    }
    
    public void init() {
      this.mLastStepTime = -1L;
      this.mNumStepDurations = 0;
    }
    
    public void clearTime() {
      this.mLastStepTime = -1L;
    }
    
    public long computeTimePerLevel() {
      long[] arrayOfLong = this.mStepDurations;
      int i = this.mNumStepDurations;
      if (i <= 0)
        return -1L; 
      long l = 0L;
      for (byte b = 0; b < i; b++)
        l += arrayOfLong[b] & 0xFFFFFFFFFFL; 
      return l / i;
    }
    
    public long computeTimeEstimate(long param1Long1, long param1Long2, int[] param1ArrayOfint) {
      long[] arrayOfLong = this.mStepDurations;
      int i = this.mNumStepDurations;
      if (i <= 0)
        return -1L; 
      long l = 0L;
      int j = 0;
      for (byte b = 0; b < i; b++, l = l3, j = k) {
        long l1 = arrayOfLong[b];
        long l2 = arrayOfLong[b];
        long l3 = l;
        int k = j;
        if (((l2 & 0xFF00000000000000L) >> 56L & param1Long1) == 0L) {
          l3 = l;
          k = j;
          if (((l1 & 0xFF000000000000L) >> 48L & param1Long1) == param1Long2) {
            k = j + 1;
            l3 = l + (arrayOfLong[b] & 0xFFFFFFFFFFL);
          } 
        } 
      } 
      if (j <= 0)
        return -1L; 
      if (param1ArrayOfint != null)
        param1ArrayOfint[0] = j; 
      return l / j * 100L;
    }
    
    public void addLevelSteps(int param1Int, long param1Long1, long param1Long2) {
      int i = this.mNumStepDurations;
      long l = this.mLastStepTime;
      int j = i;
      if (l >= 0L) {
        j = i;
        if (param1Int > 0) {
          long[] arrayOfLong = this.mStepDurations;
          l = param1Long2 - l;
          for (j = 0; j < param1Int; j++, l = l2) {
            System.arraycopy(arrayOfLong, 0, arrayOfLong, 1, arrayOfLong.length - 1);
            long l1 = l / (param1Int - j);
            long l2 = l - l1;
            l = l1;
            if (l1 > 1099511627775L)
              l = 1099511627775L; 
            arrayOfLong[0] = l | param1Long1;
          } 
          param1Int = i + param1Int;
          j = param1Int;
          if (param1Int > arrayOfLong.length)
            j = arrayOfLong.length; 
        } 
      } 
      this.mNumStepDurations = j;
      this.mLastStepTime = param1Long2;
    }
    
    public void readFromParcel(Parcel param1Parcel) {
      int i = param1Parcel.readInt();
      if (i <= this.mStepDurations.length) {
        this.mNumStepDurations = i;
        for (byte b = 0; b < i; b++)
          this.mStepDurations[b] = param1Parcel.readLong(); 
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("more step durations than available: ");
      stringBuilder.append(i);
      throw new ParcelFormatException(stringBuilder.toString());
    }
    
    public void writeToParcel(Parcel param1Parcel) {
      int i = this.mNumStepDurations;
      param1Parcel.writeInt(i);
      for (byte b = 0; b < i; b++)
        param1Parcel.writeLong(this.mStepDurations[b]); 
    }
  }
  
  class PackageChange {
    public String mPackageName;
    
    public boolean mUpdate;
    
    public long mVersionCode;
  }
  
  class DailyItem {
    public BatteryStats.LevelStepTracker mChargeSteps;
    
    public BatteryStats.LevelStepTracker mDischargeSteps;
    
    public long mEndTime;
    
    public ArrayList<BatteryStats.PackageChange> mPackageChanges;
    
    public long mStartTime;
  }
  
  class HistoryTag {
    public int poolIdx;
    
    public String string;
    
    public int uid;
    
    public void setTo(HistoryTag param1HistoryTag) {
      this.string = param1HistoryTag.string;
      this.uid = param1HistoryTag.uid;
      this.poolIdx = param1HistoryTag.poolIdx;
    }
    
    public void setTo(String param1String, int param1Int) {
      this.string = param1String;
      this.uid = param1Int;
      this.poolIdx = -1;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(this.string);
      param1Parcel.writeInt(this.uid);
    }
    
    public void readFromParcel(Parcel param1Parcel) {
      this.string = param1Parcel.readString();
      this.uid = param1Parcel.readInt();
      this.poolIdx = -1;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.uid != ((HistoryTag)param1Object).uid)
        return false; 
      if (!this.string.equals(((HistoryTag)param1Object).string))
        return false; 
      return true;
    }
    
    public int hashCode() {
      int i = this.string.hashCode();
      int j = this.uid;
      return i * 31 + j;
    }
  }
  
  class HistoryStepDetails {
    public int appCpuSTime1;
    
    public int appCpuSTime2;
    
    public int appCpuSTime3;
    
    public int appCpuUTime1;
    
    public int appCpuUTime2;
    
    public int appCpuUTime3;
    
    public int appCpuUid1;
    
    public int appCpuUid2;
    
    public int appCpuUid3;
    
    public int statIOWaitTime;
    
    public int statIdlTime;
    
    public int statIrqTime;
    
    public String statPlatformIdleState;
    
    public int statSoftIrqTime;
    
    public String statSubsystemPowerState;
    
    public int statSystemTime;
    
    public int statUserTime;
    
    public int systemTime;
    
    public int userTime;
    
    public HistoryStepDetails() {
      clear();
    }
    
    public void clear() {
      this.systemTime = 0;
      this.userTime = 0;
      this.appCpuUid3 = -1;
      this.appCpuUid2 = -1;
      this.appCpuUid1 = -1;
      this.appCpuSTime3 = 0;
      this.appCpuUTime3 = 0;
      this.appCpuSTime2 = 0;
      this.appCpuUTime2 = 0;
      this.appCpuSTime1 = 0;
      this.appCpuUTime1 = 0;
    }
    
    public void writeToParcel(Parcel param1Parcel) {
      param1Parcel.writeInt(this.userTime);
      param1Parcel.writeInt(this.systemTime);
      param1Parcel.writeInt(this.appCpuUid1);
      param1Parcel.writeInt(this.appCpuUTime1);
      param1Parcel.writeInt(this.appCpuSTime1);
      param1Parcel.writeInt(this.appCpuUid2);
      param1Parcel.writeInt(this.appCpuUTime2);
      param1Parcel.writeInt(this.appCpuSTime2);
      param1Parcel.writeInt(this.appCpuUid3);
      param1Parcel.writeInt(this.appCpuUTime3);
      param1Parcel.writeInt(this.appCpuSTime3);
      param1Parcel.writeInt(this.statUserTime);
      param1Parcel.writeInt(this.statSystemTime);
      param1Parcel.writeInt(this.statIOWaitTime);
      param1Parcel.writeInt(this.statIrqTime);
      param1Parcel.writeInt(this.statSoftIrqTime);
      param1Parcel.writeInt(this.statIdlTime);
      param1Parcel.writeString(this.statPlatformIdleState);
      param1Parcel.writeString(this.statSubsystemPowerState);
    }
    
    public void readFromParcel(Parcel param1Parcel) {
      this.userTime = param1Parcel.readInt();
      this.systemTime = param1Parcel.readInt();
      this.appCpuUid1 = param1Parcel.readInt();
      this.appCpuUTime1 = param1Parcel.readInt();
      this.appCpuSTime1 = param1Parcel.readInt();
      this.appCpuUid2 = param1Parcel.readInt();
      this.appCpuUTime2 = param1Parcel.readInt();
      this.appCpuSTime2 = param1Parcel.readInt();
      this.appCpuUid3 = param1Parcel.readInt();
      this.appCpuUTime3 = param1Parcel.readInt();
      this.appCpuSTime3 = param1Parcel.readInt();
      this.statUserTime = param1Parcel.readInt();
      this.statSystemTime = param1Parcel.readInt();
      this.statIOWaitTime = param1Parcel.readInt();
      this.statIrqTime = param1Parcel.readInt();
      this.statSoftIrqTime = param1Parcel.readInt();
      this.statIdlTime = param1Parcel.readInt();
      this.statPlatformIdleState = param1Parcel.readString();
      this.statSubsystemPowerState = param1Parcel.readString();
    }
  }
  
  class HistoryItem {
    public byte cmd = -1;
    
    public boolean isDeltaData() {
      boolean bool;
      if (this.cmd == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public final BatteryStats.HistoryTag localWakelockTag = new BatteryStats.HistoryTag();
    
    public final BatteryStats.HistoryTag localWakeReasonTag = new BatteryStats.HistoryTag();
    
    public final BatteryStats.HistoryTag localEventTag = new BatteryStats.HistoryTag();
    
    public static final byte CMD_CURRENT_TIME = 5;
    
    public static final byte CMD_NULL = -1;
    
    public static final byte CMD_OVERFLOW = 6;
    
    public static final byte CMD_RESET = 7;
    
    public static final byte CMD_SHUTDOWN = 8;
    
    public static final byte CMD_START = 4;
    
    public static final byte CMD_UPDATE = 0;
    
    public static final int EVENT_ACTIVE = 10;
    
    public static final int EVENT_ALARM = 13;
    
    public static final int EVENT_ALARM_FINISH = 16397;
    
    public static final int EVENT_ALARM_START = 32781;
    
    public static final int EVENT_COLLECT_EXTERNAL_STATS = 14;
    
    public static final int EVENT_CONNECTIVITY_CHANGED = 9;
    
    public static final int EVENT_COUNT = 22;
    
    public static final int EVENT_FLAG_FINISH = 16384;
    
    public static final int EVENT_FLAG_START = 32768;
    
    public static final int EVENT_FOREGROUND = 2;
    
    public static final int EVENT_FOREGROUND_FINISH = 16386;
    
    public static final int EVENT_FOREGROUND_START = 32770;
    
    public static final int EVENT_JOB = 6;
    
    public static final int EVENT_JOB_FINISH = 16390;
    
    public static final int EVENT_JOB_START = 32774;
    
    public static final int EVENT_LONG_WAKE_LOCK = 20;
    
    public static final int EVENT_LONG_WAKE_LOCK_FINISH = 16404;
    
    public static final int EVENT_LONG_WAKE_LOCK_START = 32788;
    
    public static final int EVENT_NONE = 0;
    
    public static final int EVENT_PACKAGE_ACTIVE = 16;
    
    public static final int EVENT_PACKAGE_INACTIVE = 15;
    
    public static final int EVENT_PACKAGE_INSTALLED = 11;
    
    public static final int EVENT_PACKAGE_UNINSTALLED = 12;
    
    public static final int EVENT_PROC = 1;
    
    public static final int EVENT_PROC_FINISH = 16385;
    
    public static final int EVENT_PROC_START = 32769;
    
    public static final int EVENT_SCREEN_WAKE_UP = 18;
    
    public static final int EVENT_SYNC = 4;
    
    public static final int EVENT_SYNC_FINISH = 16388;
    
    public static final int EVENT_SYNC_START = 32772;
    
    public static final int EVENT_TEMP_WHITELIST = 17;
    
    public static final int EVENT_TEMP_WHITELIST_FINISH = 16401;
    
    public static final int EVENT_TEMP_WHITELIST_START = 32785;
    
    public static final int EVENT_TOP = 3;
    
    public static final int EVENT_TOP_FINISH = 16387;
    
    public static final int EVENT_TOP_START = 32771;
    
    public static final int EVENT_TYPE_MASK = -49153;
    
    public static final int EVENT_USER_FOREGROUND = 8;
    
    public static final int EVENT_USER_FOREGROUND_FINISH = 16392;
    
    public static final int EVENT_USER_FOREGROUND_START = 32776;
    
    public static final int EVENT_USER_RUNNING = 7;
    
    public static final int EVENT_USER_RUNNING_FINISH = 16391;
    
    public static final int EVENT_USER_RUNNING_START = 32775;
    
    public static final int EVENT_WAKEUP_AP = 19;
    
    public static final int EVENT_WAKE_LOCK = 5;
    
    public static final int EVENT_WAKE_LOCK_FINISH = 16389;
    
    public static final int EVENT_WAKE_LOCK_START = 32773;
    
    public static final int MOST_INTERESTING_STATES = 1835008;
    
    public static final int MOST_INTERESTING_STATES2 = -1749024768;
    
    public static final int SETTLE_TO_ZERO_STATES = -1900544;
    
    public static final int SETTLE_TO_ZERO_STATES2 = 1748959232;
    
    public static final int STATE2_BLUETOOTH_ON_FLAG = 4194304;
    
    public static final int STATE2_BLUETOOTH_SCAN_FLAG = 1048576;
    
    public static final int STATE2_CAMERA_FLAG = 2097152;
    
    public static final int STATE2_CELLULAR_HIGH_TX_POWER_FLAG = 524288;
    
    public static final int STATE2_CHARGING_FLAG = 16777216;
    
    public static final int STATE2_DEVICE_IDLE_MASK = 100663296;
    
    public static final int STATE2_DEVICE_IDLE_SHIFT = 25;
    
    public static final int STATE2_FLASHLIGHT_FLAG = 134217728;
    
    public static final int STATE2_GPS_SIGNAL_QUALITY_MASK = 128;
    
    public static final int STATE2_GPS_SIGNAL_QUALITY_SHIFT = 7;
    
    public static final int STATE2_PHONE_IN_CALL_FLAG = 8388608;
    
    public static final int STATE2_POWER_SAVE_FLAG = -2147483648;
    
    public static final int STATE2_USB_DATA_LINK_FLAG = 262144;
    
    public static final int STATE2_VIDEO_ON_FLAG = 1073741824;
    
    public static final int STATE2_WIFI_ON_FLAG = 268435456;
    
    public static final int STATE2_WIFI_RUNNING_FLAG = 536870912;
    
    public static final int STATE2_WIFI_SIGNAL_STRENGTH_MASK = 112;
    
    public static final int STATE2_WIFI_SIGNAL_STRENGTH_SHIFT = 4;
    
    public static final int STATE2_WIFI_SUPPL_STATE_MASK = 15;
    
    public static final int STATE2_WIFI_SUPPL_STATE_SHIFT = 0;
    
    public static final int STATE_AUDIO_ON_FLAG = 4194304;
    
    public static final int STATE_BATTERY_PLUGGED_FLAG = 524288;
    
    public static final int STATE_BRIGHTNESS_MASK = 7;
    
    public static final int STATE_BRIGHTNESS_SHIFT = 0;
    
    public static final int STATE_CPU_RUNNING_FLAG = -2147483648;
    
    public static final int STATE_DATA_CONNECTION_MASK = 15872;
    
    public static final int STATE_DATA_CONNECTION_SHIFT = 9;
    
    public static final int STATE_GPS_ON_FLAG = 536870912;
    
    public static final int STATE_MOBILE_RADIO_ACTIVE_FLAG = 33554432;
    
    public static final int STATE_PHONE_SCANNING_FLAG = 2097152;
    
    public static final int STATE_PHONE_SIGNAL_STRENGTH_MASK = 56;
    
    public static final int STATE_PHONE_SIGNAL_STRENGTH_SHIFT = 3;
    
    public static final int STATE_PHONE_STATE_MASK = 448;
    
    public static final int STATE_PHONE_STATE_SHIFT = 6;
    
    private static final int STATE_RESERVED_0 = 16777216;
    
    public static final int STATE_SCREEN_DOZE_FLAG = 262144;
    
    public static final int STATE_SCREEN_ON_FLAG = 1048576;
    
    public static final int STATE_SENSOR_ON_FLAG = 8388608;
    
    public static final int STATE_WAKE_LOCK_FLAG = 1073741824;
    
    public static final int STATE_WIFI_FULL_LOCK_FLAG = 268435456;
    
    public static final int STATE_WIFI_MULTICAST_ON_FLAG = 65536;
    
    public static final int STATE_WIFI_RADIO_ACTIVE_FLAG = 67108864;
    
    public static final int STATE_WIFI_SCAN_FLAG = 134217728;
    
    public int batteryChargeUAh;
    
    public byte batteryHealth;
    
    public byte batteryLevel;
    
    public byte batteryPlugType;
    
    public byte batteryStatus;
    
    public short batteryTemperature;
    
    public char batteryVoltage;
    
    public long currentTime;
    
    public int eventCode;
    
    public BatteryStats.HistoryTag eventTag;
    
    public double modemRailChargeMah;
    
    public HistoryItem next;
    
    public int numReadInts;
    
    public int states;
    
    public int states2;
    
    public BatteryStats.HistoryStepDetails stepDetails;
    
    public long time;
    
    public BatteryStats.HistoryTag wakeReasonTag;
    
    public BatteryStats.HistoryTag wakelockTag;
    
    public double wifiRailChargeMah;
    
    public HistoryItem(BatteryStats this$0) {
      readFromParcel((Parcel)this$0);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeLong(this.time);
      byte b1 = this.cmd, b2 = this.batteryLevel, b3 = this.batteryStatus, b4 = this.batteryHealth, b5 = this.batteryPlugType;
      BatteryStats.HistoryTag historyTag = this.wakelockTag;
      int i = 0;
      if (historyTag != null) {
        j = 268435456;
      } else {
        j = 0;
      } 
      if (this.wakeReasonTag != null) {
        c = '\000';
      } else {
        c = Character.MIN_VALUE;
      } 
      if (this.eventCode != 0)
        i = 1073741824; 
      param1Parcel.writeInt(b1 & 0xFF | b2 << 8 & 0xFF00 | b3 << 16 & 0xF0000 | b4 << 20 & 0xF00000 | b5 << 24 & 0xF000000 | j | c | i);
      int j = this.batteryTemperature;
      char c = this.batteryVoltage;
      param1Parcel.writeInt(j & 0xFFFF | c << 16 & 0xFFFF0000);
      param1Parcel.writeInt(this.batteryChargeUAh);
      param1Parcel.writeDouble(this.modemRailChargeMah);
      param1Parcel.writeDouble(this.wifiRailChargeMah);
      param1Parcel.writeInt(this.states);
      param1Parcel.writeInt(this.states2);
      historyTag = this.wakelockTag;
      if (historyTag != null)
        historyTag.writeToParcel(param1Parcel, param1Int); 
      historyTag = this.wakeReasonTag;
      if (historyTag != null)
        historyTag.writeToParcel(param1Parcel, param1Int); 
      j = this.eventCode;
      if (j != 0) {
        param1Parcel.writeInt(j);
        this.eventTag.writeToParcel(param1Parcel, param1Int);
      } 
      param1Int = this.cmd;
      if (param1Int == 5 || param1Int == 7)
        param1Parcel.writeLong(this.currentTime); 
    }
    
    public void readFromParcel(Parcel param1Parcel) {
      int i = param1Parcel.dataPosition();
      this.time = param1Parcel.readLong();
      int j = param1Parcel.readInt();
      this.cmd = (byte)(j & 0xFF);
      this.batteryLevel = (byte)(j >> 8 & 0xFF);
      this.batteryStatus = (byte)(j >> 16 & 0xF);
      this.batteryHealth = (byte)(j >> 20 & 0xF);
      this.batteryPlugType = (byte)(j >> 24 & 0xF);
      int k = param1Parcel.readInt();
      this.batteryTemperature = (short)(k & 0xFFFF);
      this.batteryVoltage = (char)(0xFFFF & k >> 16);
      this.batteryChargeUAh = param1Parcel.readInt();
      this.modemRailChargeMah = param1Parcel.readDouble();
      this.wifiRailChargeMah = param1Parcel.readDouble();
      this.states = param1Parcel.readInt();
      this.states2 = param1Parcel.readInt();
      if ((0x10000000 & j) != 0) {
        BatteryStats.HistoryTag historyTag = this.localWakelockTag;
        historyTag.readFromParcel(param1Parcel);
      } else {
        this.wakelockTag = null;
      } 
      if ((0x20000000 & j) != 0) {
        BatteryStats.HistoryTag historyTag = this.localWakeReasonTag;
        historyTag.readFromParcel(param1Parcel);
      } else {
        this.wakeReasonTag = null;
      } 
      if ((0x40000000 & j) != 0) {
        this.eventCode = param1Parcel.readInt();
        BatteryStats.HistoryTag historyTag = this.localEventTag;
        historyTag.readFromParcel(param1Parcel);
      } else {
        this.eventCode = 0;
        this.eventTag = null;
      } 
      k = this.cmd;
      if (k == 5 || k == 7) {
        this.currentTime = param1Parcel.readLong();
      } else {
        this.currentTime = 0L;
      } 
      this.numReadInts += (param1Parcel.dataPosition() - i) / 4;
    }
    
    public void clear() {
      this.time = 0L;
      this.cmd = -1;
      this.batteryLevel = 0;
      this.batteryStatus = 0;
      this.batteryHealth = 0;
      this.batteryPlugType = 0;
      this.batteryTemperature = 0;
      this.batteryVoltage = Character.MIN_VALUE;
      this.batteryChargeUAh = 0;
      this.modemRailChargeMah = 0.0D;
      this.wifiRailChargeMah = 0.0D;
      this.states = 0;
      this.states2 = 0;
      this.wakelockTag = null;
      this.wakeReasonTag = null;
      this.eventCode = 0;
      this.eventTag = null;
    }
    
    public void setTo(HistoryItem param1HistoryItem) {
      this.time = param1HistoryItem.time;
      this.cmd = param1HistoryItem.cmd;
      setToCommon(param1HistoryItem);
    }
    
    public void setTo(long param1Long, byte param1Byte, HistoryItem param1HistoryItem) {
      this.time = param1Long;
      this.cmd = param1Byte;
      setToCommon(param1HistoryItem);
    }
    
    private void setToCommon(HistoryItem param1HistoryItem) {
      this.batteryLevel = param1HistoryItem.batteryLevel;
      this.batteryStatus = param1HistoryItem.batteryStatus;
      this.batteryHealth = param1HistoryItem.batteryHealth;
      this.batteryPlugType = param1HistoryItem.batteryPlugType;
      this.batteryTemperature = param1HistoryItem.batteryTemperature;
      this.batteryVoltage = param1HistoryItem.batteryVoltage;
      this.batteryChargeUAh = param1HistoryItem.batteryChargeUAh;
      this.modemRailChargeMah = param1HistoryItem.modemRailChargeMah;
      this.wifiRailChargeMah = param1HistoryItem.wifiRailChargeMah;
      this.states = param1HistoryItem.states;
      this.states2 = param1HistoryItem.states2;
      if (param1HistoryItem.wakelockTag != null) {
        BatteryStats.HistoryTag historyTag = this.localWakelockTag;
        historyTag.setTo(param1HistoryItem.wakelockTag);
      } else {
        this.wakelockTag = null;
      } 
      if (param1HistoryItem.wakeReasonTag != null) {
        BatteryStats.HistoryTag historyTag = this.localWakeReasonTag;
        historyTag.setTo(param1HistoryItem.wakeReasonTag);
      } else {
        this.wakeReasonTag = null;
      } 
      this.eventCode = param1HistoryItem.eventCode;
      if (param1HistoryItem.eventTag != null) {
        BatteryStats.HistoryTag historyTag = this.localEventTag;
        historyTag.setTo(param1HistoryItem.eventTag);
      } else {
        this.eventTag = null;
      } 
      this.currentTime = param1HistoryItem.currentTime;
    }
    
    public boolean sameNonEvent(HistoryItem param1HistoryItem) {
      boolean bool;
      if (this.batteryLevel == param1HistoryItem.batteryLevel && this.batteryStatus == param1HistoryItem.batteryStatus && this.batteryHealth == param1HistoryItem.batteryHealth && this.batteryPlugType == param1HistoryItem.batteryPlugType && this.batteryTemperature == param1HistoryItem.batteryTemperature && this.batteryVoltage == param1HistoryItem.batteryVoltage && this.batteryChargeUAh == param1HistoryItem.batteryChargeUAh && this.modemRailChargeMah == param1HistoryItem.modemRailChargeMah && this.wifiRailChargeMah == param1HistoryItem.wifiRailChargeMah && this.states == param1HistoryItem.states && this.states2 == param1HistoryItem.states2 && this.currentTime == param1HistoryItem.currentTime) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean same(HistoryItem param1HistoryItem) {
      if (!sameNonEvent(param1HistoryItem) || this.eventCode != param1HistoryItem.eventCode)
        return false; 
      BatteryStats.HistoryTag historyTag2 = this.wakelockTag, historyTag3 = param1HistoryItem.wakelockTag;
      if (historyTag2 != historyTag3) {
        if (historyTag2 == null || historyTag3 == null)
          return false; 
        if (!historyTag2.equals(historyTag3))
          return false; 
      } 
      historyTag2 = this.wakeReasonTag;
      historyTag3 = param1HistoryItem.wakeReasonTag;
      if (historyTag2 != historyTag3) {
        if (historyTag2 == null || historyTag3 == null)
          return false; 
        if (!historyTag2.equals(historyTag3))
          return false; 
      } 
      historyTag2 = this.eventTag;
      BatteryStats.HistoryTag historyTag1 = param1HistoryItem.eventTag;
      if (historyTag2 != historyTag1) {
        if (historyTag2 == null || historyTag1 == null)
          return false; 
        if (!historyTag2.equals(historyTag1))
          return false; 
      } 
      return true;
    }
    
    public HistoryItem() {}
  }
  
  class HistoryEventTracker {
    private final HashMap<String, SparseIntArray>[] mActiveEvents = (HashMap<String, SparseIntArray>[])new HashMap[22];
    
    public boolean updateState(int param1Int1, String param1String, int param1Int2, int param1Int3) {
      if ((0x8000 & param1Int1) != 0) {
        param1Int1 &= 0xFFFF3FFF;
        HashMap<String, SparseIntArray> hashMap1 = this.mActiveEvents[param1Int1];
        HashMap<String, SparseIntArray> hashMap2 = hashMap1;
        if (hashMap1 == null) {
          hashMap2 = new HashMap<>();
          this.mActiveEvents[param1Int1] = hashMap2;
        } 
        SparseIntArray sparseIntArray2 = hashMap2.get(param1String);
        SparseIntArray sparseIntArray1 = sparseIntArray2;
        if (sparseIntArray2 == null) {
          sparseIntArray1 = new SparseIntArray();
          hashMap2.put(param1String, sparseIntArray1);
        } 
        if (sparseIntArray1.indexOfKey(param1Int2) >= 0)
          return false; 
        sparseIntArray1.put(param1Int2, param1Int3);
      } else if ((param1Int1 & 0x4000) != 0) {
        HashMap<String, SparseIntArray> hashMap = this.mActiveEvents[param1Int1 & 0xFFFF3FFF];
        if (hashMap == null)
          return false; 
        SparseIntArray sparseIntArray = hashMap.get(param1String);
        if (sparseIntArray == null)
          return false; 
        param1Int1 = sparseIntArray.indexOfKey(param1Int2);
        if (param1Int1 < 0)
          return false; 
        sparseIntArray.removeAt(param1Int1);
        if (sparseIntArray.size() <= 0)
          hashMap.remove(param1String); 
      } 
      return true;
    }
    
    public void removeEvents(int param1Int) {
      this.mActiveEvents[0xFFFF3FFF & param1Int] = null;
    }
    
    public HashMap<String, SparseIntArray> getStateForEvent(int param1Int) {
      return this.mActiveEvents[param1Int];
    }
  }
  
  class BitDescription {
    public final int mask;
    
    public final String name;
    
    public final int shift;
    
    public final String shortName;
    
    public final String[] shortValues;
    
    public final String[] values;
    
    public BitDescription(BatteryStats this$0, String param1String1, String param1String2) {
      this.mask = this$0;
      this.shift = -1;
      this.name = param1String1;
      this.shortName = param1String2;
      this.values = null;
      this.shortValues = null;
    }
    
    public BitDescription(BatteryStats this$0, int param1Int1, String param1String1, String param1String2, String[] param1ArrayOfString1, String[] param1ArrayOfString2) {
      this.mask = this$0;
      this.shift = param1Int1;
      this.name = param1String1;
      this.shortName = param1String2;
      this.values = param1ArrayOfString1;
      this.shortValues = param1ArrayOfString2;
    }
  }
  
  private static final void formatTimeRaw(StringBuilder paramStringBuilder, long paramLong) {
    long l1 = paramLong / 86400L;
    if (l1 != 0L) {
      paramStringBuilder.append(l1);
      paramStringBuilder.append("d ");
    } 
    l1 = l1 * 60L * 60L * 24L;
    long l2 = (paramLong - l1) / 3600L;
    if (l2 != 0L || l1 != 0L) {
      paramStringBuilder.append(l2);
      paramStringBuilder.append("h ");
    } 
    l1 += l2 * 60L * 60L;
    l2 = (paramLong - l1) / 60L;
    if (l2 != 0L || l1 != 0L) {
      paramStringBuilder.append(l2);
      paramStringBuilder.append("m ");
    } 
    l1 += 60L * l2;
    if (paramLong != 0L || l1 != 0L) {
      paramStringBuilder.append(paramLong - l1);
      paramStringBuilder.append("s ");
    } 
  }
  
  public static final void formatTimeMs(StringBuilder paramStringBuilder, long paramLong) {
    long l = paramLong / 1000L;
    formatTimeRaw(paramStringBuilder, l);
    paramStringBuilder.append(paramLong - 1000L * l);
    paramStringBuilder.append("ms ");
  }
  
  public static final void formatTimeMsNoSpace(StringBuilder paramStringBuilder, long paramLong) {
    long l = paramLong / 1000L;
    formatTimeRaw(paramStringBuilder, l);
    paramStringBuilder.append(paramLong - 1000L * l);
    paramStringBuilder.append("ms");
  }
  
  public final String formatRatioLocked(long paramLong1, long paramLong2) {
    if (paramLong2 == 0L)
      return "--%"; 
    float f = (float)paramLong1 / (float)paramLong2;
    this.mFormatBuilder.setLength(0);
    this.mFormatter.format("%.1f%%", new Object[] { Float.valueOf(f * 100.0F) });
    return this.mFormatBuilder.toString();
  }
  
  final String formatBytesLocked(long paramLong) {
    this.mFormatBuilder.setLength(0);
    if (paramLong < 1024L) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramLong);
      stringBuilder.append("B");
      return stringBuilder.toString();
    } 
    if (paramLong < 1048576L) {
      this.mFormatter.format("%.2fKB", new Object[] { Double.valueOf(paramLong / 1024.0D) });
      return this.mFormatBuilder.toString();
    } 
    if (paramLong < 1073741824L) {
      this.mFormatter.format("%.2fMB", new Object[] { Double.valueOf(paramLong / 1048576.0D) });
      return this.mFormatBuilder.toString();
    } 
    this.mFormatter.format("%.2fGB", new Object[] { Double.valueOf(paramLong / 1.073741824E9D) });
    return this.mFormatBuilder.toString();
  }
  
  private static long roundUsToMs(long paramLong) {
    return (500L + paramLong) / 1000L;
  }
  
  private static long computeWakeLock(Timer paramTimer, long paramLong, int paramInt) {
    if (paramTimer != null) {
      paramLong = paramTimer.getTotalTimeLocked(paramLong, paramInt);
      paramLong = (500L + paramLong) / 1000L;
      return paramLong;
    } 
    return 0L;
  }
  
  private static final String printWakeLock(StringBuilder paramStringBuilder, Timer paramTimer, long paramLong, String paramString1, int paramInt, String paramString2) {
    if (paramTimer != null) {
      long l = computeWakeLock(paramTimer, paramLong, paramInt);
      paramInt = paramTimer.getCountLocked(paramInt);
      if (l != 0L) {
        paramStringBuilder.append(paramString2);
        formatTimeMs(paramStringBuilder, l);
        if (paramString1 != null) {
          paramStringBuilder.append(paramString1);
          paramStringBuilder.append(' ');
        } 
        paramStringBuilder.append('(');
        paramStringBuilder.append(paramInt);
        paramStringBuilder.append(" times)");
        long l1 = paramTimer.getMaxDurationMsLocked(paramLong / 1000L);
        if (l1 >= 0L) {
          paramStringBuilder.append(" max=");
          paramStringBuilder.append(l1);
        } 
        l1 = paramTimer.getTotalDurationMsLocked(paramLong / 1000L);
        if (l1 > l) {
          paramStringBuilder.append(" actual=");
          paramStringBuilder.append(l1);
        } 
        if (paramTimer.isRunningLocked()) {
          paramLong = paramTimer.getCurrentDurationMsLocked(paramLong / 1000L);
          if (paramLong >= 0L) {
            paramStringBuilder.append(" (running for ");
            paramStringBuilder.append(paramLong);
            paramStringBuilder.append("ms)");
          } else {
            paramStringBuilder.append(" (running)");
          } 
        } 
        return ", ";
      } 
    } 
    return paramString2;
  }
  
  private static final boolean printTimer(PrintWriter paramPrintWriter, StringBuilder paramStringBuilder, Timer paramTimer, long paramLong, int paramInt, String paramString1, String paramString2) {
    if (paramTimer != null) {
      long l = (paramTimer.getTotalTimeLocked(paramLong, paramInt) + 500L) / 1000L;
      paramInt = paramTimer.getCountLocked(paramInt);
      if (l != 0L) {
        paramStringBuilder.setLength(0);
        paramStringBuilder.append(paramString1);
        paramStringBuilder.append("    ");
        paramStringBuilder.append(paramString2);
        paramStringBuilder.append(": ");
        formatTimeMs(paramStringBuilder, l);
        paramStringBuilder.append("realtime (");
        paramStringBuilder.append(paramInt);
        paramStringBuilder.append(" times)");
        l = paramTimer.getMaxDurationMsLocked(paramLong / 1000L);
        if (l >= 0L) {
          paramStringBuilder.append(" max=");
          paramStringBuilder.append(l);
        } 
        if (paramTimer.isRunningLocked()) {
          paramLong = paramTimer.getCurrentDurationMsLocked(paramLong / 1000L);
          if (paramLong >= 0L) {
            paramStringBuilder.append(" (running for ");
            paramStringBuilder.append(paramLong);
            paramStringBuilder.append("ms)");
          } else {
            paramStringBuilder.append(" (running)");
          } 
        } 
        paramPrintWriter.println(paramStringBuilder.toString());
        return true;
      } 
    } 
    return false;
  }
  
  private static final String printWakeLockCheckin(StringBuilder paramStringBuilder, Timer paramTimer, long paramLong, String paramString1, int paramInt, String paramString2) {
    String str;
    long l1 = 0L;
    int i = 0;
    long l2 = 0L;
    long l3 = 0L;
    long l4 = 0L;
    if (paramTimer != null) {
      l1 = paramTimer.getTotalTimeLocked(paramLong, paramInt);
      i = paramTimer.getCountLocked(paramInt);
      l3 = paramTimer.getCurrentDurationMsLocked(paramLong / 1000L);
      l2 = paramTimer.getMaxDurationMsLocked(paramLong / 1000L);
      l4 = paramTimer.getTotalDurationMsLocked(paramLong / 1000L);
    } 
    paramStringBuilder.append(paramString2);
    paramStringBuilder.append((l1 + 500L) / 1000L);
    paramStringBuilder.append(',');
    if (paramString1 != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append(",");
      str = stringBuilder.toString();
    } else {
      str = "";
    } 
    paramStringBuilder.append(str);
    paramStringBuilder.append(i);
    paramStringBuilder.append(',');
    paramStringBuilder.append(l3);
    paramStringBuilder.append(',');
    paramStringBuilder.append(l2);
    if (paramString1 != null) {
      paramStringBuilder.append(',');
      paramStringBuilder.append(l4);
    } 
    return ",";
  }
  
  private static final void dumpLineHeader(PrintWriter paramPrintWriter, int paramInt, String paramString1, String paramString2) {
    paramPrintWriter.print(9);
    paramPrintWriter.print(',');
    paramPrintWriter.print(paramInt);
    paramPrintWriter.print(',');
    paramPrintWriter.print(paramString1);
    paramPrintWriter.print(',');
    paramPrintWriter.print(paramString2);
  }
  
  private static final void dumpLine(PrintWriter paramPrintWriter, int paramInt, String paramString1, String paramString2, Object... paramVarArgs) {
    dumpLineHeader(paramPrintWriter, paramInt, paramString1, paramString2);
    for (int i = paramVarArgs.length; paramInt < i; ) {
      Object object = paramVarArgs[paramInt];
      paramPrintWriter.print(',');
      paramPrintWriter.print(object);
      paramInt++;
    } 
    paramPrintWriter.println();
  }
  
  private static final void dumpTimer(PrintWriter paramPrintWriter, int paramInt1, String paramString1, String paramString2, Timer paramTimer, long paramLong, int paramInt2) {
    if (paramTimer != null) {
      paramLong = roundUsToMs(paramTimer.getTotalTimeLocked(paramLong, paramInt2));
      paramInt2 = paramTimer.getCountLocked(paramInt2);
      if (paramLong != 0L || paramInt2 != 0)
        dumpLine(paramPrintWriter, paramInt1, paramString1, paramString2, new Object[] { Long.valueOf(paramLong), Integer.valueOf(paramInt2) }); 
    } 
  }
  
  private static void dumpTimer(ProtoOutputStream paramProtoOutputStream, long paramLong1, Timer paramTimer, long paramLong2, int paramInt) {
    if (paramTimer == null)
      return; 
    long l1 = roundUsToMs(paramTimer.getTotalTimeLocked(paramLong2, paramInt));
    paramInt = paramTimer.getCountLocked(paramInt);
    long l2 = paramTimer.getMaxDurationMsLocked(paramLong2 / 1000L);
    long l3 = paramTimer.getCurrentDurationMsLocked(paramLong2 / 1000L);
    paramLong2 = paramTimer.getTotalDurationMsLocked(paramLong2 / 1000L);
    if (l1 != 0L || paramInt != 0 || l2 != -1L || l3 != -1L || paramLong2 != -1L) {
      paramLong1 = paramProtoOutputStream.start(paramLong1);
      paramProtoOutputStream.write(1112396529665L, l1);
      paramProtoOutputStream.write(1112396529666L, paramInt);
      if (l2 != -1L)
        paramProtoOutputStream.write(1112396529667L, l2); 
      if (l3 != -1L)
        paramProtoOutputStream.write(1112396529668L, l3); 
      if (paramLong2 != -1L)
        paramProtoOutputStream.write(1112396529669L, paramLong2); 
      paramProtoOutputStream.end(paramLong1);
    } 
  }
  
  private static boolean controllerActivityHasData(ControllerActivityCounter paramControllerActivityCounter, int paramInt) {
    if (paramControllerActivityCounter == null)
      return false; 
    if (paramControllerActivityCounter.getIdleTimeCounter().getCountLocked(paramInt) != 0L || 
      paramControllerActivityCounter.getRxTimeCounter().getCountLocked(paramInt) != 0L || 
      paramControllerActivityCounter.getPowerCounter().getCountLocked(paramInt) != 0L || 
      paramControllerActivityCounter.getMonitoredRailChargeConsumedMaMs().getCountLocked(paramInt) != 0L)
      return true; 
    for (LongCounter longCounter : paramControllerActivityCounter.getTxTimeCounters()) {
      if (longCounter.getCountLocked(paramInt) != 0L)
        return true; 
    } 
    return false;
  }
  
  private static final void dumpControllerActivityLine(PrintWriter paramPrintWriter, int paramInt1, String paramString1, String paramString2, ControllerActivityCounter paramControllerActivityCounter, int paramInt2) {
    if (!controllerActivityHasData(paramControllerActivityCounter, paramInt2))
      return; 
    dumpLineHeader(paramPrintWriter, paramInt1, paramString1, paramString2);
    paramPrintWriter.print(",");
    paramPrintWriter.print(paramControllerActivityCounter.getIdleTimeCounter().getCountLocked(paramInt2));
    paramPrintWriter.print(",");
    paramPrintWriter.print(paramControllerActivityCounter.getRxTimeCounter().getCountLocked(paramInt2));
    paramPrintWriter.print(",");
    paramPrintWriter.print(paramControllerActivityCounter.getPowerCounter().getCountLocked(paramInt2) / 3600000.0D);
    paramPrintWriter.print(",");
    paramPrintWriter.print(paramControllerActivityCounter.getMonitoredRailChargeConsumedMaMs().getCountLocked(paramInt2) / 3600000.0D);
    LongCounter[] arrayOfLongCounter;
    int i;
    for (arrayOfLongCounter = paramControllerActivityCounter.getTxTimeCounters(), i = arrayOfLongCounter.length, paramInt1 = 0; paramInt1 < i; ) {
      LongCounter longCounter = arrayOfLongCounter[paramInt1];
      paramPrintWriter.print(",");
      paramPrintWriter.print(longCounter.getCountLocked(paramInt2));
      paramInt1++;
    } 
    paramPrintWriter.println();
  }
  
  private static void dumpControllerActivityProto(ProtoOutputStream paramProtoOutputStream, long paramLong, ControllerActivityCounter paramControllerActivityCounter, int paramInt) {
    if (!controllerActivityHasData(paramControllerActivityCounter, paramInt))
      return; 
    paramLong = paramProtoOutputStream.start(paramLong);
    long l = paramControllerActivityCounter.getIdleTimeCounter().getCountLocked(paramInt);
    paramProtoOutputStream.write(1112396529665L, l);
    l = paramControllerActivityCounter.getRxTimeCounter().getCountLocked(paramInt);
    paramProtoOutputStream.write(1112396529666L, l);
    double d = paramControllerActivityCounter.getPowerCounter().getCountLocked(paramInt) / 3600000.0D;
    paramProtoOutputStream.write(1112396529667L, d);
    d = paramControllerActivityCounter.getMonitoredRailChargeConsumedMaMs().getCountLocked(paramInt) / 3600000.0D;
    paramProtoOutputStream.write(1103806595077L, d);
    LongCounter[] arrayOfLongCounter = paramControllerActivityCounter.getTxTimeCounters();
    for (byte b = 0; b < arrayOfLongCounter.length; b++) {
      LongCounter longCounter = arrayOfLongCounter[b];
      l = paramProtoOutputStream.start(2246267895812L);
      paramProtoOutputStream.write(1120986464257L, b);
      paramProtoOutputStream.write(1112396529666L, longCounter.getCountLocked(paramInt));
      paramProtoOutputStream.end(l);
    } 
    paramProtoOutputStream.end(paramLong);
  }
  
  private final void printControllerActivityIfInteresting(PrintWriter paramPrintWriter, StringBuilder paramStringBuilder, String paramString1, String paramString2, ControllerActivityCounter paramControllerActivityCounter, int paramInt) {
    if (controllerActivityHasData(paramControllerActivityCounter, paramInt))
      printControllerActivity(paramPrintWriter, paramStringBuilder, paramString1, paramString2, paramControllerActivityCounter, paramInt); 
  }
  
  private final void printControllerActivity(PrintWriter paramPrintWriter, StringBuilder paramStringBuilder, String paramString1, String paramString2, ControllerActivityCounter paramControllerActivityCounter, int paramInt) {
    byte b;
    String[] arrayOfString;
    long l1 = paramControllerActivityCounter.getIdleTimeCounter().getCountLocked(paramInt);
    long l2 = paramControllerActivityCounter.getRxTimeCounter().getCountLocked(paramInt);
    long l3 = paramControllerActivityCounter.getPowerCounter().getCountLocked(paramInt);
    long l4 = paramControllerActivityCounter.getMonitoredRailChargeConsumedMaMs().getCountLocked(paramInt);
    long l5 = computeBatteryRealtime(SystemClock.elapsedRealtime() * 1000L, paramInt) / 1000L;
    long l6 = 0L;
    for (LongCounter longCounter : paramControllerActivityCounter.getTxTimeCounters())
      l6 += longCounter.getCountLocked(paramInt); 
    if (paramString2.equals("WiFi")) {
      long l = paramControllerActivityCounter.getScanTimeCounter().getCountLocked(paramInt);
      paramStringBuilder.setLength(0);
      paramStringBuilder.append(paramString1);
      paramStringBuilder.append("     ");
      paramStringBuilder.append(paramString2);
      paramStringBuilder.append(" Scan time:  ");
      formatTimeMs(paramStringBuilder, l);
      paramStringBuilder.append("(");
      paramStringBuilder.append(formatRatioLocked(l, l5));
      paramStringBuilder.append(")");
      paramPrintWriter.println(paramStringBuilder.toString());
      l6 = l5 - l1 + l2 + l6;
      paramStringBuilder.setLength(0);
      paramStringBuilder.append(paramString1);
      paramStringBuilder.append("     ");
      paramStringBuilder.append(paramString2);
      paramStringBuilder.append(" Sleep time:  ");
      formatTimeMs(paramStringBuilder, l6);
      paramStringBuilder.append("(");
      paramStringBuilder.append(formatRatioLocked(l6, l5));
      paramStringBuilder.append(")");
      paramPrintWriter.println(paramStringBuilder.toString());
    } 
    if (paramString2.equals("Cellular")) {
      l6 = paramControllerActivityCounter.getSleepTimeCounter().getCountLocked(paramInt);
      paramStringBuilder.setLength(0);
      paramStringBuilder.append(paramString1);
      paramStringBuilder.append("     ");
      paramStringBuilder.append(paramString2);
      paramStringBuilder.append(" Sleep time:  ");
      formatTimeMs(paramStringBuilder, l6);
      paramStringBuilder.append("(");
      paramStringBuilder.append(formatRatioLocked(l6, l5));
      paramStringBuilder.append(")");
      paramPrintWriter.println(paramStringBuilder.toString());
    } 
    paramStringBuilder.setLength(0);
    paramStringBuilder.append(paramString1);
    paramStringBuilder.append("     ");
    paramStringBuilder.append(paramString2);
    paramStringBuilder.append(" Idle time:   ");
    formatTimeMs(paramStringBuilder, l1);
    paramStringBuilder.append("(");
    paramStringBuilder.append(formatRatioLocked(l1, l5));
    paramStringBuilder.append(")");
    paramPrintWriter.println(paramStringBuilder.toString());
    paramStringBuilder.setLength(0);
    paramStringBuilder.append(paramString1);
    paramStringBuilder.append("     ");
    paramStringBuilder.append(paramString2);
    paramStringBuilder.append(" Rx time:     ");
    formatTimeMs(paramStringBuilder, l2);
    paramStringBuilder.append("(");
    paramStringBuilder.append(formatRatioLocked(l2, l5));
    paramStringBuilder.append(")");
    paramPrintWriter.println(paramStringBuilder.toString());
    paramStringBuilder.setLength(0);
    paramStringBuilder.append(paramString1);
    paramStringBuilder.append("     ");
    paramStringBuilder.append(paramString2);
    paramStringBuilder.append(" Tx time:     ");
    if (paramString2.hashCode() == -851952246 && paramString2.equals("Cellular")) {
      b = 0;
    } else {
      b = -1;
    } 
    if (b != 0) {
      arrayOfString = new String[] { "[0]", "[1]", "[2]", "[3]", "[4]" };
    } else {
      arrayOfString = new String[] { "   less than 0dBm: ", "   0dBm to 8dBm: ", "   8dBm to 15dBm: ", "   15dBm to 20dBm: ", "   above 20dBm: " };
    } 
    int i = Math.min((paramControllerActivityCounter.getTxTimeCounters()).length, arrayOfString.length);
    if (i > 1) {
      paramPrintWriter.println(paramStringBuilder.toString());
      for (b = 0; b < i; b++) {
        l6 = paramControllerActivityCounter.getTxTimeCounters()[b].getCountLocked(paramInt);
        paramStringBuilder.setLength(0);
        paramStringBuilder.append(paramString1);
        paramStringBuilder.append("    ");
        paramStringBuilder.append(arrayOfString[b]);
        paramStringBuilder.append(" ");
        formatTimeMs(paramStringBuilder, l6);
        paramStringBuilder.append("(");
        paramStringBuilder.append(formatRatioLocked(l6, l5));
        paramStringBuilder.append(")");
        paramPrintWriter.println(paramStringBuilder.toString());
      } 
    } else {
      l6 = paramControllerActivityCounter.getTxTimeCounters()[0].getCountLocked(paramInt);
      formatTimeMs(paramStringBuilder, l6);
      paramStringBuilder.append("(");
      paramStringBuilder.append(formatRatioLocked(l6, l5));
      paramStringBuilder.append(")");
      paramPrintWriter.println(paramStringBuilder.toString());
    } 
    if (l3 > 0L) {
      paramStringBuilder.setLength(0);
      paramStringBuilder.append(paramString1);
      paramStringBuilder.append("     ");
      paramStringBuilder.append(paramString2);
      paramStringBuilder.append(" Battery drain: ");
      double d = l3 / 3600000.0D;
      String str = BatteryStatsHelper.makemAh(d);
      paramStringBuilder.append(str);
      paramStringBuilder.append("mAh");
      paramPrintWriter.println(paramStringBuilder.toString());
    } 
    if (l4 > 0L) {
      paramStringBuilder.setLength(0);
      paramStringBuilder.append(paramString1);
      paramStringBuilder.append("     ");
      paramStringBuilder.append(paramString2);
      paramStringBuilder.append(" Monitored rail energy drain: ");
      DecimalFormat decimalFormat = new DecimalFormat("#.##");
      double d = l4 / 3600000.0D;
      String str = decimalFormat.format(d);
      paramStringBuilder.append(str);
      paramStringBuilder.append(" mAh");
      paramPrintWriter.println(paramStringBuilder.toString());
    } 
  }
  
  public final void dumpCheckinLocked(Context paramContext, PrintWriter paramPrintWriter, int paramInt1, int paramInt2) {
    dumpCheckinLocked(paramContext, paramPrintWriter, paramInt1, paramInt2, BatteryStatsHelper.checkWifiOnly(paramContext));
  }
  
  public final void dumpCheckinLocked(Context paramContext, PrintWriter paramPrintWriter, int paramInt1, int paramInt2, boolean paramBoolean) {
    StringBuilder stringBuilder1;
    String str4, str3;
    StringBuilder stringBuilder3;
    Integer integer = Integer.valueOf(0);
    if (paramInt1 != 0) {
      str1 = STAT_NAMES[paramInt1];
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("ERROR: BatteryStats.dumpCheckin called for which type ");
      stringBuilder1.append(paramInt1);
      stringBuilder1.append(" but only STATS_SINCE_CHARGED is supported.");
      dumpLine(paramPrintWriter, 0, str1, "err", new Object[] { stringBuilder1.toString() });
      return;
    } 
    long l1 = SystemClock.uptimeMillis() * 1000L;
    long l2 = SystemClock.elapsedRealtime();
    long l3 = l2 * 1000L;
    long l4 = getBatteryUptime(l1);
    long l5 = computeBatteryUptime(l1, paramInt1);
    long l6 = computeBatteryRealtime(l3, paramInt1);
    long l7 = computeBatteryScreenOffUptime(l1, paramInt1);
    long l8 = computeBatteryScreenOffRealtime(l3, paramInt1);
    long l9 = computeRealtime(l3, paramInt1);
    long l10 = computeUptime(l1, paramInt1);
    long l11 = getScreenOnTime(l3, paramInt1);
    l1 = getScreenDozeTime(l3, paramInt1);
    long l12 = getInteractiveTime(l3, paramInt1);
    long l13 = getPowerSaveModeEnabledTime(l3, paramInt1);
    long l14 = getDeviceIdleModeTime(1, l3, paramInt1);
    long l15 = getDeviceIdleModeTime(2, l3, paramInt1);
    long l16 = getDeviceIdlingTime(1, l3, paramInt1);
    long l17 = getDeviceIdlingTime(2, l3, paramInt1);
    int i = getNumConnectivityChange(paramInt1);
    long l18 = getPhoneOnTime(l3, paramInt1);
    long l19 = getUahDischarge(paramInt1);
    long l20 = getUahDischargeScreenOff(paramInt1);
    long l21 = getUahDischargeScreenDoze(paramInt1);
    long l22 = getUahDischargeLightDoze(paramInt1);
    long l23 = getUahDischargeDeepDoze(paramInt1);
    StringBuilder stringBuilder2 = new StringBuilder(128);
    SparseArray<? extends Uid> sparseArray1 = getUidStats();
    int j = sparseArray1.size();
    String str2 = STAT_NAMES[paramInt1];
    if (paramInt1 == 0) {
      Integer integer1 = Integer.valueOf(getStartCount());
    } else {
      str4 = "N/A";
    } 
    l6 /= 1000L;
    l5 /= 1000L;
    l9 /= 1000L;
    long l24 = l10 / 1000L;
    l10 = getStartClockTime();
    l8 /= 1000L;
    l7 /= 1000L;
    int k = getEstimatedBatteryCapacity();
    int m = getMinLearnedBatteryCapacity(), n = getMaxLearnedBatteryCapacity();
    l1 /= 1000L;
    dumpLine(paramPrintWriter, 0, str2, "bt", new Object[] { 
          str4, Long.valueOf(l6), Long.valueOf(l5), Long.valueOf(l9), Long.valueOf(l24), Long.valueOf(l10), Long.valueOf(l8), Long.valueOf(l7), Integer.valueOf(k), Integer.valueOf(m), 
          Integer.valueOf(n), Long.valueOf(l1) });
    SparseArray<? extends Uid> sparseArray2;
    for (m = 0, l1 = 0L, l8 = 0L, sparseArray2 = sparseArray1; m < j; m++) {
      Uid uid = (Uid)sparseArray2.valueAt(m);
      ArrayMap<String, ? extends Uid.Wakelock> arrayMap = uid.getWakelockStats();
      for (n = arrayMap.size() - 1; n >= 0; n--, l8 = l7, l1 = l6) {
        Uid.Wakelock wakelock = (Uid.Wakelock)arrayMap.valueAt(n);
        Timer timer = wakelock.getWakeTime(1);
        l7 = l8;
        if (timer != null)
          l7 = l8 + timer.getTotalTimeLocked(l3, paramInt1); 
        timer = wakelock.getWakeTime(0);
        l6 = l1;
        if (timer != null)
          l6 = l1 + timer.getTotalTimeLocked(l3, paramInt1); 
      } 
    } 
    m = j;
    l9 = getNetworkActivityBytes(0, paramInt1);
    l10 = getNetworkActivityBytes(1, paramInt1);
    long l25 = getNetworkActivityBytes(2, paramInt1);
    l7 = getNetworkActivityBytes(3, paramInt1);
    long l26 = getNetworkActivityPackets(0, paramInt1);
    l24 = getNetworkActivityPackets(1, paramInt1);
    l6 = getNetworkActivityPackets(2, paramInt1);
    long l27 = getNetworkActivityPackets(3, paramInt1);
    long l28 = getNetworkActivityBytes(4, paramInt1);
    l5 = getNetworkActivityBytes(5, paramInt1);
    dumpLine(paramPrintWriter, 0, str2, "gn", new Object[] { Long.valueOf(l9), Long.valueOf(l10), Long.valueOf(l25), Long.valueOf(l7), Long.valueOf(l26), Long.valueOf(l24), Long.valueOf(l6), Long.valueOf(l27), Long.valueOf(l28), Long.valueOf(l5) });
    ControllerActivityCounter controllerActivityCounter1 = getModemControllerActivity();
    l6 = l4;
    SparseArray<? extends Uid> sparseArray3 = sparseArray2;
    l7 = l3;
    dumpControllerActivityLine(paramPrintWriter, 0, str2, "gmcd", controllerActivityCounter1, paramInt1);
    l3 = getWifiOnTime(l7, paramInt1);
    l4 = getGlobalWifiRunningTime(l7, paramInt1);
    l3 /= 1000L;
    l4 /= 1000L;
    dumpLine(paramPrintWriter, 0, str2, "gwfl", new Object[] { Long.valueOf(l3), Long.valueOf(l4), stringBuilder1, stringBuilder1, stringBuilder1 });
    ControllerActivityCounter controllerActivityCounter2 = getWifiControllerActivity();
    dumpControllerActivityLine(paramPrintWriter, 0, str2, "gwfcd", controllerActivityCounter2, paramInt1);
    controllerActivityCounter2 = getBluetoothControllerActivity();
    dumpControllerActivityLine(paramPrintWriter, 0, str2, "gble", controllerActivityCounter2, paramInt1);
    l3 = l11 / 1000L;
    l4 = l18 / 1000L;
    l8 /= 1000L;
    l18 = l1 / 1000L;
    l1 = getMobileRadioActiveTime(l7, paramInt1) / 1000L;
    l11 = getMobileRadioActiveAdjustedTime(paramInt1) / 1000L;
    l12 /= 1000L;
    l13 /= 1000L;
    l15 /= 1000L;
    int i1 = getDeviceIdleModeCount(2, paramInt1);
    l5 = l17 / 1000L;
    j = getDeviceIdlingCount(2, paramInt1);
    int i2 = getMobileRadioActiveCount(paramInt1);
    l17 = getMobileRadioActiveUnknownTime(paramInt1) / 1000L;
    l14 /= 1000L;
    n = getDeviceIdleModeCount(1, paramInt1);
    l16 /= 1000L;
    k = getDeviceIdlingCount(1, paramInt1);
    l10 = getLongestDeviceIdleModeTime(1);
    l9 = getLongestDeviceIdleModeTime(2);
    dumpLine(paramPrintWriter, 0, str2, "m", new Object[] { 
          Long.valueOf(l3), Long.valueOf(l4), Long.valueOf(l8), Long.valueOf(l18), Long.valueOf(l1), Long.valueOf(l11), Long.valueOf(l12), Long.valueOf(l13), Integer.valueOf(i), Long.valueOf(l15), 
          Integer.valueOf(i1), Long.valueOf(l5), Integer.valueOf(j), Integer.valueOf(i2), Long.valueOf(l17), Long.valueOf(l14), Integer.valueOf(n), Long.valueOf(l16), Integer.valueOf(k), Long.valueOf(l10), 
          Long.valueOf(l9) });
    Object[] arrayOfObject2 = new Object[5];
    for (j = 0; j < 5; j++)
      arrayOfObject2[j] = Long.valueOf(getScreenBrightnessTime(j, l7, paramInt1) / 1000L); 
    dumpLine(paramPrintWriter, 0, str2, "br", arrayOfObject2);
    arrayOfObject2 = new Object[CellSignalStrength.getNumSignalStrengthLevels()];
    for (j = 0; j < CellSignalStrength.getNumSignalStrengthLevels(); j++)
      arrayOfObject2[j] = Long.valueOf(getPhoneSignalStrengthTime(j, l7, paramInt1) / 1000L); 
    dumpLine(paramPrintWriter, 0, str2, "sgt", arrayOfObject2);
    l1 = getPhoneSignalScanningTime(l7, paramInt1) / 1000L;
    dumpLine(paramPrintWriter, 0, str2, "sst", new Object[] { Long.valueOf(l1) });
    for (j = 0; j < CellSignalStrength.getNumSignalStrengthLevels(); j++)
      arrayOfObject2[j] = Integer.valueOf(getPhoneSignalStrengthCount(j, paramInt1)); 
    dumpLine(paramPrintWriter, 0, str2, "sgc", arrayOfObject2);
    arrayOfObject2 = new Object[NUM_DATA_CONNECTION_TYPES];
    for (j = 0; j < NUM_DATA_CONNECTION_TYPES; j++)
      arrayOfObject2[j] = Long.valueOf(getPhoneDataConnectionTime(j, l7, paramInt1) / 1000L); 
    dumpLine(paramPrintWriter, 0, str2, "dct", arrayOfObject2);
    for (j = 0; j < NUM_DATA_CONNECTION_TYPES; j++)
      arrayOfObject2[j] = Integer.valueOf(getPhoneDataConnectionCount(j, paramInt1)); 
    dumpLine(paramPrintWriter, 0, str2, "dcc", arrayOfObject2);
    arrayOfObject2 = new Object[8];
    for (j = 0; j < 8; j++)
      arrayOfObject2[j] = Long.valueOf(getWifiStateTime(j, l7, paramInt1) / 1000L); 
    dumpLine(paramPrintWriter, 0, str2, "wst", arrayOfObject2);
    for (j = 0; j < 8; j++)
      arrayOfObject2[j] = Integer.valueOf(getWifiStateCount(j, paramInt1)); 
    dumpLine(paramPrintWriter, 0, str2, "wsc", arrayOfObject2);
    arrayOfObject2 = new Object[13];
    for (j = 0; j < 13; j++)
      arrayOfObject2[j] = Long.valueOf(getWifiSupplStateTime(j, l7, paramInt1) / 1000L); 
    dumpLine(paramPrintWriter, 0, str2, "wsst", arrayOfObject2);
    for (j = 0; j < 13; j++)
      arrayOfObject2[j] = Integer.valueOf(getWifiSupplStateCount(j, paramInt1)); 
    dumpLine(paramPrintWriter, 0, str2, "wssc", arrayOfObject2);
    Object[] arrayOfObject1 = new Object[5];
    for (j = 0; j < 5; j++)
      arrayOfObject1[j] = Long.valueOf(getWifiSignalStrengthTime(j, l7, paramInt1) / 1000L); 
    dumpLine(paramPrintWriter, 0, str2, "wsgt", arrayOfObject1);
    for (j = 0; j < 5; j++)
      arrayOfObject1[j] = Integer.valueOf(getWifiSignalStrengthCount(j, paramInt1)); 
    dumpLine(paramPrintWriter, 0, str2, "wsgc", arrayOfObject1);
    l1 = getWifiMulticastWakelockTime(l7, paramInt1);
    j = getWifiMulticastWakelockCount(paramInt1);
    l1 /= 1000L;
    dumpLine(paramPrintWriter, 0, str2, "wmct", new Object[] { Long.valueOf(l1), Integer.valueOf(j) });
    k = getLowDischargeAmountSinceCharge();
    j = getHighDischargeAmountSinceCharge();
    n = getDischargeAmountScreenOnSinceCharge();
    i = getDischargeAmountScreenOffSinceCharge();
    l1 = l19 / 1000L;
    l8 = l20 / 1000L;
    i2 = getDischargeAmountScreenDozeSinceCharge();
    l3 = l21 / 1000L;
    l4 = l22 / 1000L;
    l23 /= 1000L;
    dumpLine(paramPrintWriter, 0, str2, "dc", new Object[] { Integer.valueOf(k), Integer.valueOf(j), Integer.valueOf(n), Integer.valueOf(i), Long.valueOf(l1), Long.valueOf(l8), Integer.valueOf(i2), Long.valueOf(l3), Long.valueOf(l4), Long.valueOf(l23) });
    String str5 = "\"";
    if (paramInt2 < 0) {
      Map<String, ? extends Timer> map4 = getKernelWakelockStats();
      if (map4.size() > 0) {
        String str;
        StringBuilder stringBuilder5;
        for (Iterator<Map.Entry> iterator = map4.entrySet().iterator(); iterator.hasNext(); ) {
          Map.Entry entry = iterator.next();
          stringBuilder2.setLength(0);
          printWakeLockCheckin(stringBuilder2, (Timer)entry.getValue(), l7, null, paramInt1, "");
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(str);
          stringBuilder.append((String)entry.getKey());
          stringBuilder.append(str);
          String str8 = stringBuilder.toString(), str7 = stringBuilder2.toString();
          dumpLine(paramPrintWriter, 0, str2, "kwl", new Object[] { str8, str7 });
        } 
        StringBuilder stringBuilder4 = stringBuilder5;
        str3 = str;
        stringBuilder1 = stringBuilder4;
      } else {
        str3 = "\"";
      } 
      Map<String, ? extends Timer> map5 = getWakeupReasonStats();
      if (map5.size() > 0) {
        for (Map.Entry<String, ? extends Timer> entry : map5.entrySet()) {
          l1 = ((Timer)entry.getValue()).getTotalTimeLocked(l7, paramInt1);
          j = ((Timer)entry.getValue()).getCountLocked(paramInt1);
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(str3);
          stringBuilder.append((String)entry.getKey());
          stringBuilder.append(str3);
          String str = stringBuilder.toString();
          l1 = (l1 + 500L) / 1000L;
          dumpLine(paramPrintWriter, 0, str2, "wr", new Object[] { str, Long.valueOf(l1), Integer.valueOf(j) });
        } 
        stringBuilder3 = stringBuilder1;
      } else {
        stringBuilder3 = stringBuilder1;
      } 
    } else {
      str3 = "\"";
      stringBuilder3 = stringBuilder1;
    } 
    n = paramInt1;
    Map<String, ? extends Timer> map3 = getRpmStats();
    Map<String, ? extends Timer> map2 = getScreenOffRpmStats();
    if (map3.size() > 0)
      for (Map.Entry<String, ? extends Timer> entry : map3.entrySet()) {
        stringBuilder2.setLength(0);
        Timer timer = (Timer)entry.getValue();
        l1 = (timer.getTotalTimeLocked(l7, n) + 500L) / 1000L;
        j = timer.getCountLocked(n);
        timer = map2.get(entry.getKey());
        if (timer != null)
          l8 = (timer.getTotalTimeLocked(l7, n) + 500L) / 1000L; 
        if (timer != null)
          timer.getCountLocked(n); 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str3);
        stringBuilder.append((String)entry.getKey());
        stringBuilder.append(str3);
        String str = stringBuilder.toString();
        dumpLine(paramPrintWriter, 0, str2, "rpm", new Object[] { str, Long.valueOf(l1), Integer.valueOf(j) });
      }  
    BatteryStatsHelper batteryStatsHelper = new BatteryStatsHelper((Context)str1, false, paramBoolean);
    batteryStatsHelper.create(this);
    batteryStatsHelper.refreshStats(n, -1);
    List<BatterySipper> list = batteryStatsHelper.getUsageList();
    if (list != null && list.size() > 0) {
      String str7 = BatteryStatsHelper.makemAh(batteryStatsHelper.getPowerProfile().getBatteryCapacity());
      str1 = BatteryStatsHelper.makemAh(batteryStatsHelper.getComputedPower());
      str5 = BatteryStatsHelper.makemAh(batteryStatsHelper.getMinDrainedPower());
      String str8 = BatteryStatsHelper.makemAh(batteryStatsHelper.getMaxDrainedPower());
      dumpLine(paramPrintWriter, 0, str2, "pws", new Object[] { str7, str1, str5, str8 });
      j = 0;
      for (i = 0; i < list.size(); i++) {
        BatterySipper batterySipper = list.get(i);
        switch (null.$SwitchMap$com$android$internal$os$BatterySipper$DrainType[batterySipper.drainType.ordinal()]) {
          default:
            str1 = "???";
            break;
          case 14:
            str1 = "memory";
            break;
          case 13:
            str1 = "camera";
            break;
          case 12:
            str1 = "over";
            break;
          case 11:
            str1 = "unacc";
            break;
          case 10:
            j = UserHandle.getUid(batterySipper.userId, 0);
            str1 = "user";
            break;
          case 9:
            j = batterySipper.uidObj.getUid();
            str1 = "uid";
            break;
          case 8:
            str1 = "flashlight";
            break;
          case 7:
            str1 = "scrn";
            break;
          case 6:
            str1 = "blue";
            break;
          case 5:
            str1 = "wifi";
            break;
          case 4:
            str1 = "phone";
            break;
          case 3:
            str1 = "cell";
            break;
          case 2:
            str1 = "idle";
            break;
          case 1:
            str1 = "ambi";
            break;
        } 
        double d = batterySipper.totalPowerMah;
        str8 = BatteryStatsHelper.makemAh(d);
        boolean bool = batterySipper.shouldHide;
        d = batterySipper.screenPowerMah;
        str7 = BatteryStatsHelper.makemAh(d);
        d = batterySipper.proportionalSmearMah;
        String str = BatteryStatsHelper.makemAh(d);
        dumpLine(paramPrintWriter, j, str2, "pwi", new Object[] { str1, str8, Integer.valueOf(bool), str7, str });
      } 
    } 
    long[] arrayOfLong2 = getCpuFreqs();
    String str6 = ",";
    if (arrayOfLong2 != null) {
      stringBuilder2.setLength(0);
      for (j = 0; j < arrayOfLong2.length; j++) {
        StringBuilder stringBuilder = new StringBuilder();
        if (j == 0) {
          str1 = "";
        } else {
          str1 = ",";
        } 
        stringBuilder.append(str1);
        stringBuilder.append(arrayOfLong2[j]);
        stringBuilder2.append(stringBuilder.toString());
      } 
      dumpLine(paramPrintWriter, 0, str2, "gcf", new Object[] { stringBuilder2.toString() });
    } 
    String str1;
    Map<String, ? extends Timer> map1;
    long[] arrayOfLong1;
    for (i = 0, str1 = str2, j = n, map1 = map2, str2 = str6, n = i, arrayOfLong1 = arrayOfLong2; n < m; n++, str6 = str7, l1 = l6, str2 = str10, stringBuilder4 = stringBuilder6, str8 = str6, l6 = l2, l2 = l1) {
      StringBuilder stringBuilder5;
      String str7;
      StringBuilder stringBuilder4;
      String str9;
      StringBuilder stringBuilder6;
      String str8, str10;
      int i3 = sparseArray3.keyAt(n);
      if (paramInt2 >= 0 && i3 != paramInt2) {
        Map<String, ? extends Timer> map = map1;
        StringBuilder stringBuilder = stringBuilder2;
        l1 = l6;
        l6 = l2;
        str10 = str2;
        str9 = str1;
        l2 = l1;
        stringBuilder5 = stringBuilder;
      } else {
        String str16;
        Uid uid = (Uid)sparseArray3.valueAt(n);
        l24 = uid.getNetworkActivityBytes(0, j);
        l15 = uid.getNetworkActivityBytes(1, j);
        l19 = uid.getNetworkActivityBytes(2, j);
        l16 = uid.getNetworkActivityBytes(3, j);
        l22 = uid.getNetworkActivityPackets(0, j);
        l1 = uid.getNetworkActivityPackets(1, j);
        l12 = uid.getMobileRadioActiveTime(j);
        i = uid.getMobileRadioActiveCount(j);
        l13 = uid.getMobileRadioApWakeupCount(j);
        l8 = uid.getNetworkActivityPackets(2, j);
        l10 = uid.getNetworkActivityPackets(3, j);
        l18 = uid.getWifiRadioApWakeupCount(j);
        l9 = uid.getNetworkActivityBytes(4, j);
        l23 = uid.getNetworkActivityBytes(5, j);
        l5 = uid.getNetworkActivityBytes(6, j);
        l14 = uid.getNetworkActivityBytes(7, j);
        l4 = uid.getNetworkActivityBytes(8, j);
        l20 = uid.getNetworkActivityBytes(9, j);
        l11 = uid.getNetworkActivityPackets(6, j);
        l3 = uid.getNetworkActivityPackets(7, j);
        l17 = uid.getNetworkActivityPackets(8, j);
        l21 = uid.getNetworkActivityPackets(9, j);
        if (l24 > 0L || l15 > 0L || l19 > 0L || l16 > 0L || l22 > 0L || l1 > 0L || l8 > 0L || l10 > 0L || l12 > 0L || i > 0 || l9 > 0L || l23 > 0L || l13 > 0L || l18 > 0L || l5 > 0L || l14 > 0L || l4 > 0L || l20 > 0L || l11 > 0L || l3 > 0L || l17 > 0L || l21 > 0L)
          dumpLine(paramPrintWriter, i3, (String)stringBuilder5, "nt", new Object[] { 
                Long.valueOf(l24), Long.valueOf(l15), Long.valueOf(l19), Long.valueOf(l16), Long.valueOf(l22), Long.valueOf(l1), Long.valueOf(l8), Long.valueOf(l10), Long.valueOf(l12), Integer.valueOf(i), 
                Long.valueOf(l9), Long.valueOf(l23), Long.valueOf(l13), Long.valueOf(l18), Long.valueOf(l5), Long.valueOf(l14), Long.valueOf(l4), Long.valueOf(l20), Long.valueOf(l11), Long.valueOf(l3), 
                Long.valueOf(l17), Long.valueOf(l21) }); 
        ControllerActivityCounter controllerActivityCounter4 = uid.getModemControllerActivity();
        dumpControllerActivityLine(paramPrintWriter, i3, (String)stringBuilder5, "mcd", controllerActivityCounter4, paramInt1);
        l8 = uid.getFullWifiLockTime(l7, j);
        l3 = uid.getWifiScanTime(l7, j);
        i = uid.getWifiScanCount(j);
        k = uid.getWifiScanBackgroundCount(j);
        l4 = (uid.getWifiScanActualTime(l7) + 500L) / 1000L;
        l23 = (uid.getWifiScanBackgroundTime(l7) + 500L) / 1000L;
        l1 = uid.getWifiRunningTime(l7, j);
        if (l8 != 0L || l3 != 0L || i != 0 || k != 0 || l4 != 0L || l23 != 0L || l1 != 0L)
          dumpLine(paramPrintWriter, i3, (String)stringBuilder5, "wfl", new Object[] { Long.valueOf(l8), Long.valueOf(l3), Long.valueOf(l1), Integer.valueOf(i), stringBuilder3, stringBuilder3, stringBuilder3, Integer.valueOf(k), Long.valueOf(l4), Long.valueOf(l23) }); 
        controllerActivityCounter4 = uid.getWifiControllerActivity();
        dumpControllerActivityLine(paramPrintWriter, i3, (String)stringBuilder5, "wfcd", controllerActivityCounter4, paramInt1);
        Timer timer3 = uid.getBluetoothScanTimer();
        if (timer3 != null) {
          l22 = (timer3.getTotalTimeLocked(l7, j) + 500L) / 1000L;
          if (l22 != 0L) {
            i1 = timer3.getCountLocked(j);
            Timer timer = uid.getBluetoothScanBackgroundTimer();
            if (timer != null) {
              i = timer.getCountLocked(j);
            } else {
              i = 0;
            } 
            l21 = timer3.getTotalDurationMsLocked(l2);
            if (timer != null) {
              l1 = timer.getTotalDurationMsLocked(l2);
            } else {
              l1 = 0L;
            } 
            if (uid.getBluetoothScanResultCounter() != null) {
              k = uid.getBluetoothScanResultCounter().getCountLocked(j);
            } else {
              k = 0;
            } 
            if (uid.getBluetoothScanResultBgCounter() != null) {
              i2 = uid.getBluetoothScanResultBgCounter().getCountLocked(j);
            } else {
              i2 = 0;
            } 
            timer = uid.getBluetoothUnoptimizedScanTimer();
            if (timer != null) {
              l8 = timer.getTotalDurationMsLocked(l2);
            } else {
              l8 = 0L;
            } 
            if (timer != null) {
              l3 = timer.getMaxDurationMsLocked(l2);
            } else {
              l3 = 0L;
            } 
            timer = uid.getBluetoothUnoptimizedScanBackgroundTimer();
            if (timer != null) {
              l4 = timer.getTotalDurationMsLocked(l2);
            } else {
              l4 = 0L;
            } 
            if (timer != null) {
              l23 = timer.getMaxDurationMsLocked(l2);
            } else {
              l23 = 0L;
            } 
            dumpLine(paramPrintWriter, i3, (String)stringBuilder5, "blem", new Object[] { 
                  Long.valueOf(l22), Integer.valueOf(i1), Integer.valueOf(i), Long.valueOf(l21), Long.valueOf(l1), Integer.valueOf(k), Integer.valueOf(i2), Long.valueOf(l8), Long.valueOf(l4), Long.valueOf(l3), 
                  Long.valueOf(l23) });
          } 
        } 
        String str17 = str3;
        ControllerActivityCounter controllerActivityCounter3 = uid.getBluetoothControllerActivity();
        dumpControllerActivityLine(paramPrintWriter, i3, (String)stringBuilder5, "ble", controllerActivityCounter3, paramInt1);
        if (uid.hasUserActivity()) {
          Object[] arrayOfObject = new Object[Uid.NUM_USER_ACTIVITY_TYPES];
          k = 0;
          for (i = 0; i < Uid.NUM_USER_ACTIVITY_TYPES; i++) {
            i2 = uid.getUserActivityCount(i, j);
            arrayOfObject[i] = Integer.valueOf(i2);
            if (i2 != 0)
              k = 1; 
          } 
          if (k != 0)
            dumpLine(paramPrintWriter, i3, (String)stringBuilder5, "ua", arrayOfObject); 
        } 
        if (uid.getAggregatedPartialWakelockTimer() != null) {
          Timer timer = uid.getAggregatedPartialWakelockTimer();
          l8 = timer.getTotalDurationMsLocked(l2);
          timer = timer.getSubTimer();
          if (timer != null) {
            l1 = timer.getTotalDurationMsLocked(l2);
          } else {
            l1 = 0L;
          } 
          dumpLine(paramPrintWriter, i3, (String)stringBuilder5, "awl", new Object[] { Long.valueOf(l8), Long.valueOf(l1) });
        } 
        ArrayMap<String, ? extends Uid.Wakelock> arrayMap4 = uid.getWakelockStats();
        String str14;
        for (j = arrayMap4.size() - 1, str14 = str2; j >= 0; j--) {
          Uid.Wakelock wakelock = (Uid.Wakelock)arrayMap4.valueAt(j);
          str9.setLength(0);
          String str19 = printWakeLockCheckin((StringBuilder)str9, wakelock.getWakeTime(1), l7, "f", paramInt1, "");
          Timer timer = wakelock.getWakeTime(0);
          str19 = printWakeLockCheckin((StringBuilder)str9, timer, l7, "p", paramInt1, str19);
          if (timer != null) {
            timer = timer.getSubTimer();
          } else {
            timer = null;
          } 
          String str18 = printWakeLockCheckin((StringBuilder)str9, timer, l7, "bp", paramInt1, str19);
          printWakeLockCheckin((StringBuilder)str9, wakelock.getWakeTime(2), l7, "w", paramInt1, str18);
          if (str9.length() > 0) {
            String str = (String)arrayMap4.keyAt(j);
            str18 = str;
            if (str.indexOf(',') >= 0)
              str18 = str.replace(',', '_'); 
            str = str18;
            if (str18.indexOf('\n') >= 0)
              str = str18.replace('\n', '_'); 
            str18 = str;
            if (str.indexOf('\r') >= 0)
              str18 = str.replace('\r', '_'); 
            dumpLine(paramPrintWriter, i3, (String)stringBuilder5, "wl", new Object[] { str18, str9.toString() });
          } 
        } 
        timer3 = uid.getMulticastWakelockStats();
        if (timer3 != null) {
          l1 = timer3.getTotalTimeLocked(l7, paramInt1) / 1000L;
          j = timer3.getCountLocked(paramInt1);
          if (l1 > 0L)
            dumpLine(paramPrintWriter, i3, (String)stringBuilder5, "wmc", new Object[] { Long.valueOf(l1), Integer.valueOf(j) }); 
        } 
        i = paramInt1;
        l1 = l7;
        ArrayMap<String, ? extends Timer> arrayMap5 = uid.getSyncStats();
        ArrayMap<String, ? extends Timer> arrayMap1;
        String str15;
        ArrayMap<String, ? extends Uid.Wakelock> arrayMap3;
        for (j = arrayMap5.size() - 1, l7 = l2, str15 = str17, arrayMap3 = arrayMap4, str2 = str9, arrayMap1 = arrayMap5; j >= 0; j--) {
          Timer timer = (Timer)arrayMap1.valueAt(j);
          l8 = (timer.getTotalTimeLocked(l1, i) + 500L) / 1000L;
          i2 = timer.getCountLocked(i);
          timer = timer.getSubTimer();
          if (timer != null) {
            l2 = timer.getTotalDurationMsLocked(l7);
          } else {
            l2 = -1L;
          } 
          if (timer != null) {
            k = timer.getCountLocked(i);
          } else {
            k = -1;
          } 
          if (l8 != 0L) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(str15);
            stringBuilder.append((String)arrayMap1.keyAt(j));
            stringBuilder.append(str15);
            String str = stringBuilder.toString();
            dumpLine(paramPrintWriter, i3, (String)stringBuilder5, "sy", new Object[] { str, Long.valueOf(l8), Integer.valueOf(i2), Long.valueOf(l2), Integer.valueOf(k) });
          } 
        } 
        l2 = l7;
        arrayMap1 = uid.getJobStats();
        for (j = arrayMap1.size() - 1, l7 = l1; j >= 0; j--) {
          Timer timer = (Timer)arrayMap1.valueAt(j);
          l8 = (timer.getTotalTimeLocked(l7, i) + 500L) / 1000L;
          i2 = timer.getCountLocked(i);
          timer = timer.getSubTimer();
          if (timer != null) {
            l1 = timer.getTotalDurationMsLocked(l2);
          } else {
            l1 = -1L;
          } 
          if (timer != null) {
            k = timer.getCountLocked(i);
          } else {
            k = -1;
          } 
          if (l8 != 0L) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(str15);
            stringBuilder.append((String)arrayMap1.keyAt(j));
            stringBuilder.append(str15);
            str16 = stringBuilder.toString();
            dumpLine(paramPrintWriter, i3, (String)stringBuilder5, "jb", new Object[] { str16, Long.valueOf(l8), Integer.valueOf(i2), Long.valueOf(l1), Integer.valueOf(k) });
          } 
        } 
        int[] arrayOfInt = JobParameters.getJobStopReasonCodes();
        Object[] arrayOfObject4 = new Object[arrayOfInt.length + 1];
        arrayMap1 = (ArrayMap)uid.getJobCompletionStats();
        for (j = arrayMap1.size() - 1; j >= 0; j--) {
          SparseIntArray sparseIntArray = (SparseIntArray)arrayMap1.valueAt(j);
          if (sparseIntArray != null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(str15);
            stringBuilder.append((String)arrayMap1.keyAt(j));
            stringBuilder.append(str15);
            arrayOfObject4[0] = stringBuilder.toString();
            for (k = 0; k < arrayOfInt.length; k++)
              arrayOfObject4[k + 1] = Integer.valueOf(sparseIntArray.get(arrayOfInt[k], 0)); 
            dumpLine(paramPrintWriter, i3, (String)stringBuilder5, "jbc", arrayOfObject4);
          } 
        } 
        uid.getDeferredJobsCheckinLineLocked((StringBuilder)str2, i);
        if (str2.length() > 0)
          dumpLine(paramPrintWriter, i3, (String)stringBuilder5, "jbd", new Object[] { str2.toString() }); 
        Timer timer2 = uid.getFlashlightTurnedOnTimer();
        Map<String, ? extends Timer> map = map1;
        dumpTimer(paramPrintWriter, i3, (String)stringBuilder5, "fla", timer2, l7, paramInt1);
        dumpTimer(paramPrintWriter, i3, (String)stringBuilder5, "cam", uid.getCameraTurnedOnTimer(), l7, paramInt1);
        dumpTimer(paramPrintWriter, i3, (String)stringBuilder5, "vid", uid.getVideoTurnedOnTimer(), l7, paramInt1);
        dumpTimer(paramPrintWriter, i3, (String)stringBuilder5, "aud", uid.getAudioTurnedOnTimer(), l7, paramInt1);
        SparseArray<? extends Uid.Sensor> sparseArray = uid.getSensorStats();
        i2 = sparseArray.size();
        for (j = 0; j < i2; j++) {
          Uid.Sensor sensor = (Uid.Sensor)sparseArray.valueAt(j);
          int i4 = sparseArray.keyAt(j);
          timer2 = sensor.getSensorTime();
          if (timer2 != null) {
            l3 = (timer2.getTotalTimeLocked(l7, i) + 500L) / 1000L;
            if (l3 != 0L) {
              i1 = timer2.getCountLocked(i);
              Timer timer = sensor.getSensorBackgroundTime();
              if (timer != null) {
                k = timer.getCountLocked(i);
              } else {
                k = 0;
              } 
              l8 = timer2.getTotalDurationMsLocked(l2);
              if (timer != null) {
                l1 = timer.getTotalDurationMsLocked(l2);
              } else {
                l1 = 0L;
              } 
              dumpLine(paramPrintWriter, i3, (String)stringBuilder5, "sr", new Object[] { Integer.valueOf(i4), Long.valueOf(l3), Integer.valueOf(i1), Integer.valueOf(k), Long.valueOf(l8), Long.valueOf(l1) });
            } 
          } 
        } 
        stringBuilder6 = stringBuilder5;
        Timer timer1 = uid.getVibratorOnTimer();
        j = i2;
        dumpTimer(paramPrintWriter, i3, (String)stringBuilder6, "vib", timer1, l7, paramInt1);
        dumpTimer(paramPrintWriter, i3, (String)stringBuilder6, "fg", uid.getForegroundActivityTimer(), l7, paramInt1);
        dumpTimer(paramPrintWriter, i3, (String)stringBuilder6, "fgs", uid.getForegroundServiceTimer(), l7, paramInt1);
        Object[] arrayOfObject3 = new Object[7];
        l1 = 0L;
        for (k = 0; k < 7; k++) {
          l8 = uid.getProcessStateTime(k, l7, i);
          l1 += l8;
          arrayOfObject3[k] = Long.valueOf((l8 + 500L) / 1000L);
        } 
        if (l1 > 0L)
          dumpLine(paramPrintWriter, i3, (String)stringBuilder6, "st", arrayOfObject3); 
        l8 = uid.getUserCpuTimeUs(i);
        l1 = uid.getSystemCpuTimeUs(i);
        if (l8 > 0L || l1 > 0L) {
          l4 = l8 / 1000L;
          l3 = l1 / 1000L;
          dumpLine(paramPrintWriter, i3, (String)stringBuilder6, "cpu", new Object[] { Long.valueOf(l4), Long.valueOf(l3), stringBuilder3 });
        } 
        if (arrayOfLong1 != null) {
          long[] arrayOfLong = uid.getCpuFreqTimes(i);
          if (arrayOfLong != null && arrayOfLong.length == arrayOfLong1.length) {
            str2.setLength(0);
            for (k = 0; k < arrayOfLong.length; k++) {
              String str18;
              StringBuilder stringBuilder = new StringBuilder();
              if (k == 0) {
                str18 = "";
              } else {
                str18 = str14;
              } 
              stringBuilder.append(str18);
              stringBuilder.append(arrayOfLong[k]);
              str2.append(stringBuilder.toString());
            } 
            long[] arrayOfLong5 = uid.getScreenOffCpuFreqTimes(i);
            if (arrayOfLong5 != null) {
              for (k = 0; k < arrayOfLong5.length; k++) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(str14);
                stringBuilder.append(arrayOfLong5[k]);
                str2.append(stringBuilder.toString());
              } 
            } else {
              l1 = l7;
              k = 0;
              while (true) {
                l7 = l1;
                if (k < arrayOfLong.length) {
                  str2.append(",0");
                  k++;
                  continue;
                } 
                break;
              } 
            } 
            k = arrayOfLong.length;
            String str = str2.toString();
            dumpLine(paramPrintWriter, i3, (String)stringBuilder6, "ctf", new Object[] { "A", Integer.valueOf(k), str });
          } 
          Uid uid1;
          for (k = 0, uid1 = uid; k < 7; k++) {
            arrayOfLong = uid1.getCpuFreqTimes(i, k);
            if (arrayOfLong != null && arrayOfLong.length == arrayOfLong1.length) {
              str2.setLength(0);
              for (i2 = 0; i2 < arrayOfLong.length; i2++) {
                String str;
                StringBuilder stringBuilder = new StringBuilder();
                if (i2 == 0) {
                  str = "";
                } else {
                  str = str14;
                } 
                stringBuilder.append(str);
                stringBuilder.append(arrayOfLong[i2]);
                str2.append(stringBuilder.toString());
              } 
              long[] arrayOfLong5 = uid1.getScreenOffCpuFreqTimes(i, k);
              if (arrayOfLong5 != null) {
                for (i2 = 0; i2 < arrayOfLong5.length; i2++) {
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append(str14);
                  stringBuilder.append(arrayOfLong5[i2]);
                  str2.append(stringBuilder.toString());
                } 
              } else {
                i2 = j;
                Uid uid3 = uid1;
                i1 = 0;
                while (true) {
                  uid1 = uid3;
                  j = i2;
                  if (i1 < arrayOfLong.length) {
                    str2.append(",0");
                    i1++;
                    continue;
                  } 
                  break;
                } 
              } 
              str10 = Uid.UID_PROCESS_TYPES[k];
              i2 = arrayOfLong.length;
              str16 = str2.toString();
              dumpLine(paramPrintWriter, i3, (String)stringBuilder6, "ctf", new Object[] { str10, Integer.valueOf(i2), str16 });
            } 
          } 
          Uid uid2 = uid1;
        } else {
          str16 = str10;
        } 
        str10 = str14;
        long[] arrayOfLong4 = arrayOfLong1;
        String str11 = str2;
        ArrayMap<String, ? extends Uid.Proc> arrayMap = str16.getProcessStats();
        for (j = arrayMap.size() - 1, str7 = str15; j >= 0; j--) {
          Uid.Proc proc = (Uid.Proc)arrayMap.valueAt(j);
          l8 = proc.getUserTime(i);
          l1 = proc.getSystemTime(i);
          l3 = proc.getForegroundTime(i);
          i2 = proc.getStarts(i);
          k = proc.getNumCrashes(i);
          i1 = proc.getNumAnrs(i);
          if (l8 != 0L || l1 != 0L || l3 != 0L || i2 != 0 || i1 != 0 || k != 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(str7);
            stringBuilder.append((String)arrayMap.keyAt(j));
            stringBuilder.append(str7);
            String str = stringBuilder.toString();
            dumpLine(paramPrintWriter, i3, (String)stringBuilder6, "pr", new Object[] { str, Long.valueOf(l8), Long.valueOf(l1), Long.valueOf(l3), Integer.valueOf(i2), Integer.valueOf(i1), Integer.valueOf(k) });
          } 
        } 
        String str12 = str11;
        long[] arrayOfLong3 = arrayOfLong4;
        ArrayMap<String, ? extends Uid.Pkg> arrayMap2 = str16.getPackageStats();
        for (j = arrayMap2.size() - 1; j >= 0; j--) {
          Uid.Pkg pkg = (Uid.Pkg)arrayMap2.valueAt(j);
          k = 0;
          ArrayMap<String, ? extends Counter> arrayMap6 = pkg.getWakeupAlarmStats();
          for (i2 = arrayMap6.size() - 1; i2 >= 0; i2--) {
            i1 = ((Counter)arrayMap6.valueAt(i2)).getCountLocked(i);
            k += i1;
            String str = ((String)arrayMap6.keyAt(i2)).replace(',', '_');
            dumpLine(paramPrintWriter, i3, (String)stringBuilder6, "wua", new Object[] { str, Integer.valueOf(i1) });
          } 
          ArrayMap<String, ? extends Uid.Pkg.Serv> arrayMap7 = pkg.getServiceStats();
          for (i2 = arrayMap7.size() - 1; i2 >= 0; i2--) {
            Uid.Pkg.Serv serv = (Uid.Pkg.Serv)arrayMap7.valueAt(i2);
            l1 = serv.getStartTime(l6, i);
            int i4 = serv.getStarts(i);
            i1 = serv.getLaunches(i);
            if (l1 != 0L || i4 != 0 || i1 != 0) {
              Object object2 = arrayMap2.keyAt(j);
              Object object1 = arrayMap7.keyAt(i2);
              l1 /= 1000L;
              dumpLine(paramPrintWriter, i3, (String)stringBuilder6, "apk", new Object[] { Integer.valueOf(k), object2, object1, Long.valueOf(l1), Integer.valueOf(i4), Integer.valueOf(i1) });
            } 
          } 
        } 
        String str13 = str7;
        l1 = l6;
        l6 = l2;
        str7 = str12;
        l2 = l1;
        j = i;
      } 
    } 
  }
  
  class TimerEntry {
    final int mId;
    
    final String mName;
    
    final long mTime;
    
    final BatteryStats.Timer mTimer;
    
    TimerEntry(BatteryStats this$0, int param1Int, BatteryStats.Timer param1Timer, long param1Long) {
      this.mName = (String)this$0;
      this.mId = param1Int;
      this.mTimer = param1Timer;
      this.mTime = param1Long;
    }
  }
  
  private void printmAh(PrintWriter paramPrintWriter, double paramDouble) {
    paramPrintWriter.print(BatteryStatsHelper.makemAh(paramDouble));
  }
  
  private void printmAh(StringBuilder paramStringBuilder, double paramDouble) {
    paramStringBuilder.append(BatteryStatsHelper.makemAh(paramDouble));
  }
  
  public final void dumpLocked(Context paramContext, PrintWriter paramPrintWriter, String paramString, int paramInt1, int paramInt2) {
    dumpLocked(paramContext, paramPrintWriter, paramString, paramInt1, paramInt2, BatteryStatsHelper.checkWifiOnly(paramContext));
  }
  
  public final void dumpLocked(Context paramContext, PrintWriter paramPrintWriter, String paramString, int paramInt1, int paramInt2, boolean paramBoolean) {
    // Byte code:
    //   0: aload_3
    //   1: astore #7
    //   3: iload #4
    //   5: ifeq -> 48
    //   8: new java/lang/StringBuilder
    //   11: dup
    //   12: invokespecial <init> : ()V
    //   15: astore_1
    //   16: aload_1
    //   17: ldc_w 'ERROR: BatteryStats.dump called for which type '
    //   20: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   23: pop
    //   24: aload_1
    //   25: iload #4
    //   27: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   30: pop
    //   31: aload_1
    //   32: ldc_w ' but only STATS_SINCE_CHARGED is supported'
    //   35: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   38: pop
    //   39: aload_2
    //   40: aload_1
    //   41: invokevirtual toString : ()Ljava/lang/String;
    //   44: invokevirtual println : (Ljava/lang/String;)V
    //   47: return
    //   48: invokestatic uptimeMillis : ()J
    //   51: ldc2_w 1000
    //   54: lmul
    //   55: lstore #8
    //   57: invokestatic elapsedRealtime : ()J
    //   60: ldc2_w 1000
    //   63: lmul
    //   64: lstore #10
    //   66: lload #10
    //   68: ldc2_w 500
    //   71: ladd
    //   72: ldc2_w 1000
    //   75: ldiv
    //   76: lstore #12
    //   78: aload_0
    //   79: lload #8
    //   81: invokevirtual getBatteryUptime : (J)J
    //   84: lstore #14
    //   86: aload_0
    //   87: lload #8
    //   89: iload #4
    //   91: invokevirtual computeBatteryUptime : (JI)J
    //   94: lstore #16
    //   96: aload_0
    //   97: lload #10
    //   99: iload #4
    //   101: invokevirtual computeBatteryRealtime : (JI)J
    //   104: lstore #18
    //   106: aload_0
    //   107: lload #10
    //   109: iload #4
    //   111: invokevirtual computeRealtime : (JI)J
    //   114: lstore #20
    //   116: aload_0
    //   117: lload #8
    //   119: iload #4
    //   121: invokevirtual computeUptime : (JI)J
    //   124: lstore #22
    //   126: aload_0
    //   127: lload #8
    //   129: iload #4
    //   131: invokevirtual computeBatteryScreenOffUptime : (JI)J
    //   134: lstore #24
    //   136: aload_0
    //   137: lload #10
    //   139: iload #4
    //   141: invokevirtual computeBatteryScreenOffRealtime : (JI)J
    //   144: lstore #26
    //   146: aload_0
    //   147: lload #10
    //   149: invokevirtual computeBatteryTimeRemaining : (J)J
    //   152: lstore #28
    //   154: aload_0
    //   155: lload #10
    //   157: invokevirtual computeChargeTimeRemaining : (J)J
    //   160: lstore #30
    //   162: aload_0
    //   163: lload #10
    //   165: iload #4
    //   167: invokevirtual getScreenDozeTime : (JI)J
    //   170: lstore #32
    //   172: new java/lang/StringBuilder
    //   175: dup
    //   176: sipush #128
    //   179: invokespecial <init> : (I)V
    //   182: astore #34
    //   184: aload_0
    //   185: invokevirtual getUidStats : ()Landroid/util/SparseArray;
    //   188: astore #35
    //   190: aload #35
    //   192: invokevirtual size : ()I
    //   195: istore #36
    //   197: aload_0
    //   198: invokevirtual getEstimatedBatteryCapacity : ()I
    //   201: istore #37
    //   203: iload #37
    //   205: ifle -> 264
    //   208: aload #34
    //   210: iconst_0
    //   211: invokevirtual setLength : (I)V
    //   214: aload #34
    //   216: aload #7
    //   218: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   221: pop
    //   222: aload #34
    //   224: ldc_w '  Estimated battery capacity: '
    //   227: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   230: pop
    //   231: aload #34
    //   233: iload #37
    //   235: i2d
    //   236: invokestatic makemAh : (D)Ljava/lang/String;
    //   239: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   242: pop
    //   243: aload #34
    //   245: ldc_w ' mAh'
    //   248: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   251: pop
    //   252: aload_2
    //   253: aload #34
    //   255: invokevirtual toString : ()Ljava/lang/String;
    //   258: invokevirtual println : (Ljava/lang/String;)V
    //   261: goto -> 264
    //   264: aload_0
    //   265: invokevirtual getMinLearnedBatteryCapacity : ()I
    //   268: istore #37
    //   270: iload #37
    //   272: ifle -> 332
    //   275: aload #34
    //   277: iconst_0
    //   278: invokevirtual setLength : (I)V
    //   281: aload #34
    //   283: aload #7
    //   285: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   288: pop
    //   289: aload #34
    //   291: ldc_w '  Min learned battery capacity: '
    //   294: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   297: pop
    //   298: aload #34
    //   300: iload #37
    //   302: sipush #1000
    //   305: idiv
    //   306: i2d
    //   307: invokestatic makemAh : (D)Ljava/lang/String;
    //   310: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   313: pop
    //   314: aload #34
    //   316: ldc_w ' mAh'
    //   319: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   322: pop
    //   323: aload_2
    //   324: aload #34
    //   326: invokevirtual toString : ()Ljava/lang/String;
    //   329: invokevirtual println : (Ljava/lang/String;)V
    //   332: aload_0
    //   333: invokevirtual getMaxLearnedBatteryCapacity : ()I
    //   336: istore #37
    //   338: iload #37
    //   340: ifle -> 403
    //   343: aload #34
    //   345: iconst_0
    //   346: invokevirtual setLength : (I)V
    //   349: aload #34
    //   351: aload #7
    //   353: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   356: pop
    //   357: aload #34
    //   359: ldc_w '  Max learned battery capacity: '
    //   362: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   365: pop
    //   366: aload #34
    //   368: iload #37
    //   370: sipush #1000
    //   373: idiv
    //   374: i2d
    //   375: invokestatic makemAh : (D)Ljava/lang/String;
    //   378: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   381: pop
    //   382: aload #34
    //   384: ldc_w ' mAh'
    //   387: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   390: pop
    //   391: aload_2
    //   392: aload #34
    //   394: invokevirtual toString : ()Ljava/lang/String;
    //   397: invokevirtual println : (Ljava/lang/String;)V
    //   400: goto -> 403
    //   403: aload #34
    //   405: iconst_0
    //   406: invokevirtual setLength : (I)V
    //   409: aload #34
    //   411: aload #7
    //   413: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   416: pop
    //   417: aload #34
    //   419: ldc_w '  Time on battery: '
    //   422: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   425: pop
    //   426: aload #34
    //   428: lload #18
    //   430: ldc2_w 1000
    //   433: ldiv
    //   434: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   437: aload #34
    //   439: ldc_w '('
    //   442: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   445: pop
    //   446: aload #34
    //   448: aload_0
    //   449: lload #18
    //   451: lload #20
    //   453: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   456: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   459: pop
    //   460: aload #34
    //   462: ldc_w ') realtime, '
    //   465: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   468: pop
    //   469: aload #34
    //   471: lload #16
    //   473: ldc2_w 1000
    //   476: ldiv
    //   477: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   480: aload #34
    //   482: ldc_w '('
    //   485: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   488: pop
    //   489: aload #34
    //   491: aload_0
    //   492: lload #16
    //   494: lload #18
    //   496: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   499: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   502: pop
    //   503: aload #34
    //   505: ldc_w ') uptime'
    //   508: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   511: pop
    //   512: aload_2
    //   513: aload #34
    //   515: invokevirtual toString : ()Ljava/lang/String;
    //   518: invokevirtual println : (Ljava/lang/String;)V
    //   521: aload #34
    //   523: iconst_0
    //   524: invokevirtual setLength : (I)V
    //   527: aload #34
    //   529: aload #7
    //   531: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   534: pop
    //   535: aload #34
    //   537: ldc_w '  Time on battery screen off: '
    //   540: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   543: pop
    //   544: aload #34
    //   546: lload #26
    //   548: ldc2_w 1000
    //   551: ldiv
    //   552: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   555: aload #34
    //   557: ldc_w '('
    //   560: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   563: pop
    //   564: aload #34
    //   566: aload_0
    //   567: lload #26
    //   569: lload #18
    //   571: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   574: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   577: pop
    //   578: aload #34
    //   580: ldc_w ') realtime, '
    //   583: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   586: pop
    //   587: aload #34
    //   589: lload #24
    //   591: ldc2_w 1000
    //   594: ldiv
    //   595: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   598: aload #34
    //   600: ldc_w '('
    //   603: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   606: pop
    //   607: aload #34
    //   609: aload_0
    //   610: lload #24
    //   612: lload #18
    //   614: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   617: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   620: pop
    //   621: aload #34
    //   623: ldc_w ') uptime'
    //   626: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   629: pop
    //   630: aload_2
    //   631: aload #34
    //   633: invokevirtual toString : ()Ljava/lang/String;
    //   636: invokevirtual println : (Ljava/lang/String;)V
    //   639: aload #34
    //   641: iconst_0
    //   642: invokevirtual setLength : (I)V
    //   645: aload #34
    //   647: aload #7
    //   649: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   652: pop
    //   653: aload #34
    //   655: ldc_w '  Time on battery screen doze: '
    //   658: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   661: pop
    //   662: aload #34
    //   664: lload #32
    //   666: ldc2_w 1000
    //   669: ldiv
    //   670: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   673: aload #34
    //   675: ldc_w '('
    //   678: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   681: pop
    //   682: aload #34
    //   684: aload_0
    //   685: lload #32
    //   687: lload #18
    //   689: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   692: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   695: pop
    //   696: aload #34
    //   698: ldc_w ')'
    //   701: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   704: pop
    //   705: aload_2
    //   706: aload #34
    //   708: invokevirtual toString : ()Ljava/lang/String;
    //   711: invokevirtual println : (Ljava/lang/String;)V
    //   714: aload #34
    //   716: iconst_0
    //   717: invokevirtual setLength : (I)V
    //   720: aload #34
    //   722: aload #7
    //   724: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   727: pop
    //   728: aload #34
    //   730: ldc_w '  Total run time: '
    //   733: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   736: pop
    //   737: aload #34
    //   739: lload #20
    //   741: ldc2_w 1000
    //   744: ldiv
    //   745: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   748: aload #34
    //   750: ldc_w 'realtime, '
    //   753: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   756: pop
    //   757: aload #34
    //   759: lload #22
    //   761: ldc2_w 1000
    //   764: ldiv
    //   765: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   768: aload #34
    //   770: ldc_w 'uptime'
    //   773: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   776: pop
    //   777: aload_2
    //   778: aload #34
    //   780: invokevirtual toString : ()Ljava/lang/String;
    //   783: invokevirtual println : (Ljava/lang/String;)V
    //   786: lload #28
    //   788: lconst_0
    //   789: lcmp
    //   790: iflt -> 836
    //   793: aload #34
    //   795: iconst_0
    //   796: invokevirtual setLength : (I)V
    //   799: aload #34
    //   801: aload #7
    //   803: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   806: pop
    //   807: aload #34
    //   809: ldc_w '  Battery time remaining: '
    //   812: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   815: pop
    //   816: aload #34
    //   818: lload #28
    //   820: ldc2_w 1000
    //   823: ldiv
    //   824: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   827: aload_2
    //   828: aload #34
    //   830: invokevirtual toString : ()Ljava/lang/String;
    //   833: invokevirtual println : (Ljava/lang/String;)V
    //   836: lload #30
    //   838: lconst_0
    //   839: lcmp
    //   840: iflt -> 886
    //   843: aload #34
    //   845: iconst_0
    //   846: invokevirtual setLength : (I)V
    //   849: aload #34
    //   851: aload #7
    //   853: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   856: pop
    //   857: aload #34
    //   859: ldc_w '  Charge time remaining: '
    //   862: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   865: pop
    //   866: aload #34
    //   868: lload #30
    //   870: ldc2_w 1000
    //   873: ldiv
    //   874: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   877: aload_2
    //   878: aload #34
    //   880: invokevirtual toString : ()Ljava/lang/String;
    //   883: invokevirtual println : (Ljava/lang/String;)V
    //   886: aload_0
    //   887: iload #4
    //   889: invokevirtual getUahDischarge : (I)J
    //   892: lstore #24
    //   894: lload #24
    //   896: lconst_0
    //   897: lcmp
    //   898: iflt -> 961
    //   901: aload #34
    //   903: iconst_0
    //   904: invokevirtual setLength : (I)V
    //   907: aload #34
    //   909: aload #7
    //   911: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   914: pop
    //   915: aload #34
    //   917: ldc_w '  Discharge: '
    //   920: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   923: pop
    //   924: aload #34
    //   926: lload #24
    //   928: l2d
    //   929: ldc2_w 1000.0
    //   932: ddiv
    //   933: invokestatic makemAh : (D)Ljava/lang/String;
    //   936: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   939: pop
    //   940: aload #34
    //   942: ldc_w ' mAh'
    //   945: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   948: pop
    //   949: aload_2
    //   950: aload #34
    //   952: invokevirtual toString : ()Ljava/lang/String;
    //   955: invokevirtual println : (Ljava/lang/String;)V
    //   958: goto -> 961
    //   961: aload_0
    //   962: iload #4
    //   964: invokevirtual getUahDischargeScreenOff : (I)J
    //   967: lstore #28
    //   969: lload #28
    //   971: lconst_0
    //   972: lcmp
    //   973: iflt -> 1036
    //   976: aload #34
    //   978: iconst_0
    //   979: invokevirtual setLength : (I)V
    //   982: aload #34
    //   984: aload #7
    //   986: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   989: pop
    //   990: aload #34
    //   992: ldc_w '  Screen off discharge: '
    //   995: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   998: pop
    //   999: aload #34
    //   1001: lload #28
    //   1003: l2d
    //   1004: ldc2_w 1000.0
    //   1007: ddiv
    //   1008: invokestatic makemAh : (D)Ljava/lang/String;
    //   1011: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1014: pop
    //   1015: aload #34
    //   1017: ldc_w ' mAh'
    //   1020: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1023: pop
    //   1024: aload_2
    //   1025: aload #34
    //   1027: invokevirtual toString : ()Ljava/lang/String;
    //   1030: invokevirtual println : (Ljava/lang/String;)V
    //   1033: goto -> 1036
    //   1036: aload_0
    //   1037: iload #4
    //   1039: invokevirtual getUahDischargeScreenDoze : (I)J
    //   1042: lstore #22
    //   1044: lload #22
    //   1046: lconst_0
    //   1047: lcmp
    //   1048: iflt -> 1111
    //   1051: aload #34
    //   1053: iconst_0
    //   1054: invokevirtual setLength : (I)V
    //   1057: aload #34
    //   1059: aload #7
    //   1061: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1064: pop
    //   1065: aload #34
    //   1067: ldc_w '  Screen doze discharge: '
    //   1070: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1073: pop
    //   1074: aload #34
    //   1076: lload #22
    //   1078: l2d
    //   1079: ldc2_w 1000.0
    //   1082: ddiv
    //   1083: invokestatic makemAh : (D)Ljava/lang/String;
    //   1086: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1089: pop
    //   1090: aload #34
    //   1092: ldc_w ' mAh'
    //   1095: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1098: pop
    //   1099: aload_2
    //   1100: aload #34
    //   1102: invokevirtual toString : ()Ljava/lang/String;
    //   1105: invokevirtual println : (Ljava/lang/String;)V
    //   1108: goto -> 1111
    //   1111: lload #24
    //   1113: lload #28
    //   1115: lsub
    //   1116: lstore #24
    //   1118: lload #24
    //   1120: lconst_0
    //   1121: lcmp
    //   1122: iflt -> 1185
    //   1125: aload #34
    //   1127: iconst_0
    //   1128: invokevirtual setLength : (I)V
    //   1131: aload #34
    //   1133: aload #7
    //   1135: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1138: pop
    //   1139: aload #34
    //   1141: ldc_w '  Screen on discharge: '
    //   1144: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1147: pop
    //   1148: aload #34
    //   1150: lload #24
    //   1152: l2d
    //   1153: ldc2_w 1000.0
    //   1156: ddiv
    //   1157: invokestatic makemAh : (D)Ljava/lang/String;
    //   1160: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1163: pop
    //   1164: aload #34
    //   1166: ldc_w ' mAh'
    //   1169: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1172: pop
    //   1173: aload_2
    //   1174: aload #34
    //   1176: invokevirtual toString : ()Ljava/lang/String;
    //   1179: invokevirtual println : (Ljava/lang/String;)V
    //   1182: goto -> 1185
    //   1185: aload_0
    //   1186: iload #4
    //   1188: invokevirtual getUahDischargeLightDoze : (I)J
    //   1191: lstore #24
    //   1193: lload #24
    //   1195: lconst_0
    //   1196: lcmp
    //   1197: iflt -> 1260
    //   1200: aload #34
    //   1202: iconst_0
    //   1203: invokevirtual setLength : (I)V
    //   1206: aload #34
    //   1208: aload #7
    //   1210: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1213: pop
    //   1214: aload #34
    //   1216: ldc_w '  Device light doze discharge: '
    //   1219: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1222: pop
    //   1223: aload #34
    //   1225: lload #24
    //   1227: l2d
    //   1228: ldc2_w 1000.0
    //   1231: ddiv
    //   1232: invokestatic makemAh : (D)Ljava/lang/String;
    //   1235: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1238: pop
    //   1239: aload #34
    //   1241: ldc_w ' mAh'
    //   1244: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1247: pop
    //   1248: aload_2
    //   1249: aload #34
    //   1251: invokevirtual toString : ()Ljava/lang/String;
    //   1254: invokevirtual println : (Ljava/lang/String;)V
    //   1257: goto -> 1260
    //   1260: aload_0
    //   1261: iload #4
    //   1263: invokevirtual getUahDischargeDeepDoze : (I)J
    //   1266: lstore #24
    //   1268: lload #24
    //   1270: lconst_0
    //   1271: lcmp
    //   1272: iflt -> 1335
    //   1275: aload #34
    //   1277: iconst_0
    //   1278: invokevirtual setLength : (I)V
    //   1281: aload #34
    //   1283: aload #7
    //   1285: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1288: pop
    //   1289: aload #34
    //   1291: ldc_w '  Device deep doze discharge: '
    //   1294: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1297: pop
    //   1298: aload #34
    //   1300: lload #24
    //   1302: l2d
    //   1303: ldc2_w 1000.0
    //   1306: ddiv
    //   1307: invokestatic makemAh : (D)Ljava/lang/String;
    //   1310: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1313: pop
    //   1314: aload #34
    //   1316: ldc_w ' mAh'
    //   1319: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1322: pop
    //   1323: aload_2
    //   1324: aload #34
    //   1326: invokevirtual toString : ()Ljava/lang/String;
    //   1329: invokevirtual println : (Ljava/lang/String;)V
    //   1332: goto -> 1335
    //   1335: aload_2
    //   1336: ldc_w '  Start clock time: '
    //   1339: invokevirtual print : (Ljava/lang/String;)V
    //   1342: aload_2
    //   1343: ldc_w 'yyyy-MM-dd-HH-mm-ss'
    //   1346: aload_0
    //   1347: invokevirtual getStartClockTime : ()J
    //   1350: invokestatic format : (Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;
    //   1353: invokeinterface toString : ()Ljava/lang/String;
    //   1358: invokevirtual println : (Ljava/lang/String;)V
    //   1361: aload_0
    //   1362: lload #10
    //   1364: iload #4
    //   1366: invokevirtual getScreenOnTime : (JI)J
    //   1369: lstore #24
    //   1371: aload_0
    //   1372: lload #10
    //   1374: iload #4
    //   1376: invokevirtual getInteractiveTime : (JI)J
    //   1379: lstore #38
    //   1381: aload_0
    //   1382: lload #10
    //   1384: iload #4
    //   1386: invokevirtual getPowerSaveModeEnabledTime : (JI)J
    //   1389: lstore #30
    //   1391: aload_0
    //   1392: iconst_1
    //   1393: lload #10
    //   1395: iload #4
    //   1397: invokevirtual getDeviceIdleModeTime : (IJI)J
    //   1400: lstore #32
    //   1402: aload_0
    //   1403: iconst_2
    //   1404: lload #10
    //   1406: iload #4
    //   1408: invokevirtual getDeviceIdleModeTime : (IJI)J
    //   1411: lstore #16
    //   1413: aload_0
    //   1414: iconst_1
    //   1415: lload #10
    //   1417: iload #4
    //   1419: invokevirtual getDeviceIdlingTime : (IJI)J
    //   1422: lstore #22
    //   1424: aload_0
    //   1425: iconst_2
    //   1426: lload #10
    //   1428: iload #4
    //   1430: invokevirtual getDeviceIdlingTime : (IJI)J
    //   1433: lstore #20
    //   1435: aload_0
    //   1436: lload #10
    //   1438: iload #4
    //   1440: invokevirtual getPhoneOnTime : (JI)J
    //   1443: lstore #40
    //   1445: aload_0
    //   1446: lload #10
    //   1448: iload #4
    //   1450: invokevirtual getGlobalWifiRunningTime : (JI)J
    //   1453: pop2
    //   1454: aload_0
    //   1455: lload #10
    //   1457: iload #4
    //   1459: invokevirtual getWifiOnTime : (JI)J
    //   1462: pop2
    //   1463: aload #34
    //   1465: iconst_0
    //   1466: invokevirtual setLength : (I)V
    //   1469: aload #34
    //   1471: aload #7
    //   1473: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1476: pop
    //   1477: aload #34
    //   1479: ldc_w '  Screen on: '
    //   1482: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1485: pop
    //   1486: aload #34
    //   1488: lload #24
    //   1490: ldc2_w 1000
    //   1493: ldiv
    //   1494: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   1497: ldc_w '('
    //   1500: astore #42
    //   1502: aload #34
    //   1504: ldc_w '('
    //   1507: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1510: pop
    //   1511: aload #34
    //   1513: aload_0
    //   1514: lload #24
    //   1516: lload #18
    //   1518: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   1521: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1524: pop
    //   1525: aload #34
    //   1527: ldc_w ') '
    //   1530: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1533: pop
    //   1534: aload #34
    //   1536: aload_0
    //   1537: iload #4
    //   1539: invokevirtual getScreenOnCount : (I)I
    //   1542: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1545: pop
    //   1546: aload #34
    //   1548: ldc_w 'x, Interactive: '
    //   1551: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1554: pop
    //   1555: aload #34
    //   1557: lload #38
    //   1559: ldc2_w 1000
    //   1562: ldiv
    //   1563: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   1566: aload #34
    //   1568: ldc_w '('
    //   1571: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1574: pop
    //   1575: aload #34
    //   1577: aload_0
    //   1578: lload #38
    //   1580: lload #18
    //   1582: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   1585: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1588: pop
    //   1589: ldc_w ')'
    //   1592: astore #43
    //   1594: aload #34
    //   1596: ldc_w ')'
    //   1599: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1602: pop
    //   1603: aload_2
    //   1604: aload #34
    //   1606: invokevirtual toString : ()Ljava/lang/String;
    //   1609: invokevirtual println : (Ljava/lang/String;)V
    //   1612: aload #34
    //   1614: iconst_0
    //   1615: invokevirtual setLength : (I)V
    //   1618: aload #34
    //   1620: aload #7
    //   1622: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1625: pop
    //   1626: aload #34
    //   1628: ldc_w '  Screen brightnesses:'
    //   1631: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1634: pop
    //   1635: iconst_0
    //   1636: istore #37
    //   1638: iconst_0
    //   1639: istore #44
    //   1641: iload #44
    //   1643: iconst_5
    //   1644: if_icmpge -> 1758
    //   1647: aload_0
    //   1648: iload #44
    //   1650: lload #10
    //   1652: iload #4
    //   1654: invokevirtual getScreenBrightnessTime : (IJI)J
    //   1657: lstore #38
    //   1659: lload #38
    //   1661: lconst_0
    //   1662: lcmp
    //   1663: ifne -> 1669
    //   1666: goto -> 1752
    //   1669: aload #34
    //   1671: ldc_w '\\n    '
    //   1674: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1677: pop
    //   1678: aload #34
    //   1680: aload #7
    //   1682: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1685: pop
    //   1686: aload #34
    //   1688: getstatic android/os/BatteryStats.SCREEN_BRIGHTNESS_NAMES : [Ljava/lang/String;
    //   1691: iload #44
    //   1693: aaload
    //   1694: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1697: pop
    //   1698: aload #34
    //   1700: ldc_w ' '
    //   1703: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1706: pop
    //   1707: aload #34
    //   1709: lload #38
    //   1711: ldc2_w 1000
    //   1714: ldiv
    //   1715: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   1718: aload #34
    //   1720: ldc_w '('
    //   1723: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1726: pop
    //   1727: aload #34
    //   1729: aload_0
    //   1730: lload #38
    //   1732: lload #24
    //   1734: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   1737: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1740: pop
    //   1741: aload #34
    //   1743: aload #43
    //   1745: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1748: pop
    //   1749: iconst_1
    //   1750: istore #37
    //   1752: iinc #44, 1
    //   1755: goto -> 1641
    //   1758: aload #43
    //   1760: astore #45
    //   1762: iload #37
    //   1764: ifne -> 1776
    //   1767: aload #34
    //   1769: ldc_w ' (no activity)'
    //   1772: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1775: pop
    //   1776: aload_2
    //   1777: aload #34
    //   1779: invokevirtual toString : ()Ljava/lang/String;
    //   1782: invokevirtual println : (Ljava/lang/String;)V
    //   1785: lload #30
    //   1787: lconst_0
    //   1788: lcmp
    //   1789: ifeq -> 1869
    //   1792: aload #34
    //   1794: iconst_0
    //   1795: invokevirtual setLength : (I)V
    //   1798: aload #34
    //   1800: aload #7
    //   1802: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1805: pop
    //   1806: aload #34
    //   1808: ldc_w '  Power save mode enabled: '
    //   1811: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1814: pop
    //   1815: aload #34
    //   1817: lload #30
    //   1819: ldc2_w 1000
    //   1822: ldiv
    //   1823: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   1826: aload #34
    //   1828: ldc_w '('
    //   1831: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1834: pop
    //   1835: aload #34
    //   1837: aload_0
    //   1838: lload #30
    //   1840: lload #18
    //   1842: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   1845: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1848: pop
    //   1849: aload #34
    //   1851: aload #45
    //   1853: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1856: pop
    //   1857: aload_2
    //   1858: aload #34
    //   1860: invokevirtual toString : ()Ljava/lang/String;
    //   1863: invokevirtual println : (Ljava/lang/String;)V
    //   1866: goto -> 1869
    //   1869: ldc_w 'x'
    //   1872: astore #46
    //   1874: lload #22
    //   1876: lconst_0
    //   1877: lcmp
    //   1878: ifeq -> 1981
    //   1881: aload #34
    //   1883: iconst_0
    //   1884: invokevirtual setLength : (I)V
    //   1887: aload #34
    //   1889: aload #7
    //   1891: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1894: pop
    //   1895: aload #34
    //   1897: ldc_w '  Device light idling: '
    //   1900: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1903: pop
    //   1904: aload #34
    //   1906: lload #22
    //   1908: ldc2_w 1000
    //   1911: ldiv
    //   1912: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   1915: aload #34
    //   1917: ldc_w '('
    //   1920: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1923: pop
    //   1924: aload #34
    //   1926: aload_0
    //   1927: lload #22
    //   1929: lload #18
    //   1931: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   1934: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1937: pop
    //   1938: aload #34
    //   1940: ldc_w ') '
    //   1943: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1946: pop
    //   1947: aload #34
    //   1949: aload_0
    //   1950: iconst_1
    //   1951: iload #4
    //   1953: invokevirtual getDeviceIdlingCount : (II)I
    //   1956: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1959: pop
    //   1960: aload #34
    //   1962: ldc_w 'x'
    //   1965: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1968: pop
    //   1969: aload_2
    //   1970: aload #34
    //   1972: invokevirtual toString : ()Ljava/lang/String;
    //   1975: invokevirtual println : (Ljava/lang/String;)V
    //   1978: goto -> 1981
    //   1981: lload #32
    //   1983: lconst_0
    //   1984: lcmp
    //   1985: ifeq -> 2107
    //   1988: aload #34
    //   1990: iconst_0
    //   1991: invokevirtual setLength : (I)V
    //   1994: aload #34
    //   1996: aload #7
    //   1998: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2001: pop
    //   2002: aload #34
    //   2004: ldc_w '  Idle mode light time: '
    //   2007: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2010: pop
    //   2011: aload #34
    //   2013: lload #32
    //   2015: ldc2_w 1000
    //   2018: ldiv
    //   2019: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   2022: aload #34
    //   2024: ldc_w '('
    //   2027: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2030: pop
    //   2031: aload #34
    //   2033: aload_0
    //   2034: lload #32
    //   2036: lload #18
    //   2038: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   2041: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2044: pop
    //   2045: aload #34
    //   2047: ldc_w ') '
    //   2050: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2053: pop
    //   2054: aload #34
    //   2056: aload_0
    //   2057: iconst_1
    //   2058: iload #4
    //   2060: invokevirtual getDeviceIdleModeCount : (II)I
    //   2063: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2066: pop
    //   2067: aload #34
    //   2069: ldc_w 'x'
    //   2072: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2075: pop
    //   2076: aload #34
    //   2078: ldc_w ' -- longest '
    //   2081: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2084: pop
    //   2085: aload #34
    //   2087: aload_0
    //   2088: iconst_1
    //   2089: invokevirtual getLongestDeviceIdleModeTime : (I)J
    //   2092: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   2095: aload_2
    //   2096: aload #34
    //   2098: invokevirtual toString : ()Ljava/lang/String;
    //   2101: invokevirtual println : (Ljava/lang/String;)V
    //   2104: goto -> 2107
    //   2107: lload #20
    //   2109: lconst_0
    //   2110: lcmp
    //   2111: ifeq -> 2211
    //   2114: aload #34
    //   2116: iconst_0
    //   2117: invokevirtual setLength : (I)V
    //   2120: aload #34
    //   2122: aload #7
    //   2124: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2127: pop
    //   2128: aload #34
    //   2130: ldc_w '  Device full idling: '
    //   2133: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2136: pop
    //   2137: aload #34
    //   2139: lload #20
    //   2141: ldc2_w 1000
    //   2144: ldiv
    //   2145: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   2148: aload #34
    //   2150: ldc_w '('
    //   2153: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2156: pop
    //   2157: aload #34
    //   2159: aload_0
    //   2160: lload #20
    //   2162: lload #18
    //   2164: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   2167: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2170: pop
    //   2171: aload #34
    //   2173: ldc_w ') '
    //   2176: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2179: pop
    //   2180: aload #34
    //   2182: aload_0
    //   2183: iconst_2
    //   2184: iload #4
    //   2186: invokevirtual getDeviceIdlingCount : (II)I
    //   2189: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2192: pop
    //   2193: aload #34
    //   2195: ldc_w 'x'
    //   2198: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2201: pop
    //   2202: aload_2
    //   2203: aload #34
    //   2205: invokevirtual toString : ()Ljava/lang/String;
    //   2208: invokevirtual println : (Ljava/lang/String;)V
    //   2211: lload #16
    //   2213: lconst_0
    //   2214: lcmp
    //   2215: ifeq -> 2334
    //   2218: aload #34
    //   2220: iconst_0
    //   2221: invokevirtual setLength : (I)V
    //   2224: aload #34
    //   2226: aload #7
    //   2228: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2231: pop
    //   2232: aload #34
    //   2234: ldc_w '  Idle mode full time: '
    //   2237: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2240: pop
    //   2241: aload #34
    //   2243: lload #16
    //   2245: ldc2_w 1000
    //   2248: ldiv
    //   2249: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   2252: aload #34
    //   2254: ldc_w '('
    //   2257: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2260: pop
    //   2261: aload #34
    //   2263: aload_0
    //   2264: lload #16
    //   2266: lload #18
    //   2268: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   2271: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2274: pop
    //   2275: aload #34
    //   2277: ldc_w ') '
    //   2280: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2283: pop
    //   2284: aload #34
    //   2286: aload_0
    //   2287: iconst_2
    //   2288: iload #4
    //   2290: invokevirtual getDeviceIdleModeCount : (II)I
    //   2293: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2296: pop
    //   2297: aload #34
    //   2299: ldc_w 'x'
    //   2302: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2305: pop
    //   2306: aload #34
    //   2308: ldc_w ' -- longest '
    //   2311: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2314: pop
    //   2315: aload #34
    //   2317: aload_0
    //   2318: iconst_2
    //   2319: invokevirtual getLongestDeviceIdleModeTime : (I)J
    //   2322: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   2325: aload_2
    //   2326: aload #34
    //   2328: invokevirtual toString : ()Ljava/lang/String;
    //   2331: invokevirtual println : (Ljava/lang/String;)V
    //   2334: lload #40
    //   2336: lconst_0
    //   2337: lcmp
    //   2338: ifeq -> 2431
    //   2341: aload #34
    //   2343: iconst_0
    //   2344: invokevirtual setLength : (I)V
    //   2347: aload #34
    //   2349: aload #7
    //   2351: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2354: pop
    //   2355: aload #34
    //   2357: ldc_w '  Active phone call: '
    //   2360: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2363: pop
    //   2364: aload #34
    //   2366: lload #40
    //   2368: ldc2_w 1000
    //   2371: ldiv
    //   2372: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   2375: aload #34
    //   2377: ldc_w '('
    //   2380: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2383: pop
    //   2384: aload #34
    //   2386: aload_0
    //   2387: lload #40
    //   2389: lload #18
    //   2391: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   2394: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2397: pop
    //   2398: aload #34
    //   2400: ldc_w ') '
    //   2403: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2406: pop
    //   2407: aload #34
    //   2409: aload_0
    //   2410: iload #4
    //   2412: invokevirtual getPhoneOnCount : (I)I
    //   2415: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2418: pop
    //   2419: aload #34
    //   2421: ldc_w 'x'
    //   2424: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2427: pop
    //   2428: goto -> 2431
    //   2431: aload_0
    //   2432: iload #4
    //   2434: invokevirtual getNumConnectivityChange : (I)I
    //   2437: istore #37
    //   2439: iload #37
    //   2441: ifeq -> 2465
    //   2444: aload_2
    //   2445: aload_3
    //   2446: invokevirtual print : (Ljava/lang/String;)V
    //   2449: aload_2
    //   2450: ldc_w '  Connectivity changes: '
    //   2453: invokevirtual print : (Ljava/lang/String;)V
    //   2456: aload_2
    //   2457: iload #37
    //   2459: invokevirtual println : (I)V
    //   2462: goto -> 2465
    //   2465: lconst_0
    //   2466: lstore #22
    //   2468: lconst_0
    //   2469: lstore #24
    //   2471: new java/util/ArrayList
    //   2474: dup
    //   2475: invokespecial <init> : ()V
    //   2478: astore #47
    //   2480: iconst_0
    //   2481: istore #44
    //   2483: aload #42
    //   2485: astore #43
    //   2487: iload #44
    //   2489: iload #36
    //   2491: if_icmpge -> 2689
    //   2494: iload #36
    //   2496: istore #48
    //   2498: aload #35
    //   2500: iload #44
    //   2502: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   2505: checkcast android/os/BatteryStats$Uid
    //   2508: astore #49
    //   2510: aload #49
    //   2512: invokevirtual getWakelockStats : ()Landroid/util/ArrayMap;
    //   2515: astore #42
    //   2517: aload #42
    //   2519: invokevirtual size : ()I
    //   2522: iconst_1
    //   2523: isub
    //   2524: istore #36
    //   2526: lload #22
    //   2528: lstore #16
    //   2530: iload #36
    //   2532: iflt -> 2675
    //   2535: aload #42
    //   2537: iload #36
    //   2539: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   2542: checkcast android/os/BatteryStats$Uid$Wakelock
    //   2545: astore #50
    //   2547: aload #50
    //   2549: iconst_1
    //   2550: invokevirtual getWakeTime : (I)Landroid/os/BatteryStats$Timer;
    //   2553: astore #51
    //   2555: lload #16
    //   2557: lstore #22
    //   2559: aload #51
    //   2561: ifnull -> 2578
    //   2564: lload #16
    //   2566: aload #51
    //   2568: lload #10
    //   2570: iload #4
    //   2572: invokevirtual getTotalTimeLocked : (JI)J
    //   2575: ladd
    //   2576: lstore #22
    //   2578: aload #50
    //   2580: iconst_0
    //   2581: invokevirtual getWakeTime : (I)Landroid/os/BatteryStats$Timer;
    //   2584: astore #50
    //   2586: lload #24
    //   2588: lstore #20
    //   2590: aload #50
    //   2592: ifnull -> 2661
    //   2595: aload #50
    //   2597: lload #10
    //   2599: iload #4
    //   2601: invokevirtual getTotalTimeLocked : (JI)J
    //   2604: lstore #16
    //   2606: lload #24
    //   2608: lstore #20
    //   2610: lload #16
    //   2612: lconst_0
    //   2613: lcmp
    //   2614: ifle -> 2661
    //   2617: iload #5
    //   2619: ifge -> 2654
    //   2622: aload #47
    //   2624: new android/os/BatteryStats$TimerEntry
    //   2627: dup
    //   2628: aload #42
    //   2630: iload #36
    //   2632: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   2635: checkcast java/lang/String
    //   2638: aload #49
    //   2640: invokevirtual getUid : ()I
    //   2643: aload #50
    //   2645: lload #16
    //   2647: invokespecial <init> : (Ljava/lang/String;ILandroid/os/BatteryStats$Timer;J)V
    //   2650: invokevirtual add : (Ljava/lang/Object;)Z
    //   2653: pop
    //   2654: lload #24
    //   2656: lload #16
    //   2658: ladd
    //   2659: lstore #20
    //   2661: iinc #36, -1
    //   2664: lload #22
    //   2666: lstore #16
    //   2668: lload #20
    //   2670: lstore #24
    //   2672: goto -> 2530
    //   2675: iinc #44, 1
    //   2678: iload #48
    //   2680: istore #36
    //   2682: lload #16
    //   2684: lstore #22
    //   2686: goto -> 2487
    //   2689: iload #37
    //   2691: istore #48
    //   2693: aload_0
    //   2694: iconst_0
    //   2695: iload #4
    //   2697: invokevirtual getNetworkActivityBytes : (II)J
    //   2700: lstore #20
    //   2702: aload_0
    //   2703: iconst_1
    //   2704: iload #4
    //   2706: invokevirtual getNetworkActivityBytes : (II)J
    //   2709: lstore #16
    //   2711: aload_0
    //   2712: iconst_2
    //   2713: iload #4
    //   2715: invokevirtual getNetworkActivityBytes : (II)J
    //   2718: lstore #32
    //   2720: aload_0
    //   2721: iconst_3
    //   2722: iload #4
    //   2724: invokevirtual getNetworkActivityBytes : (II)J
    //   2727: lstore #52
    //   2729: aload_0
    //   2730: iconst_0
    //   2731: iload #4
    //   2733: invokevirtual getNetworkActivityPackets : (II)J
    //   2736: lstore #40
    //   2738: aload_0
    //   2739: iconst_1
    //   2740: iload #4
    //   2742: invokevirtual getNetworkActivityPackets : (II)J
    //   2745: lstore #30
    //   2747: aload_0
    //   2748: iconst_2
    //   2749: iload #4
    //   2751: invokevirtual getNetworkActivityPackets : (II)J
    //   2754: lstore #38
    //   2756: aload_0
    //   2757: iconst_3
    //   2758: iload #4
    //   2760: invokevirtual getNetworkActivityPackets : (II)J
    //   2763: lstore #54
    //   2765: aload_0
    //   2766: iconst_4
    //   2767: iload #4
    //   2769: invokevirtual getNetworkActivityBytes : (II)J
    //   2772: lstore #56
    //   2774: aload_0
    //   2775: iconst_5
    //   2776: iload #4
    //   2778: invokevirtual getNetworkActivityBytes : (II)J
    //   2781: lstore #58
    //   2783: lload #22
    //   2785: lconst_0
    //   2786: lcmp
    //   2787: ifeq -> 2840
    //   2790: aload #34
    //   2792: iconst_0
    //   2793: invokevirtual setLength : (I)V
    //   2796: aload #34
    //   2798: aload #7
    //   2800: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2803: pop
    //   2804: aload #34
    //   2806: ldc_w '  Total full wakelock time: '
    //   2809: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2812: pop
    //   2813: aload #34
    //   2815: lload #22
    //   2817: ldc2_w 500
    //   2820: ladd
    //   2821: ldc2_w 1000
    //   2824: ldiv
    //   2825: invokestatic formatTimeMsNoSpace : (Ljava/lang/StringBuilder;J)V
    //   2828: aload_2
    //   2829: aload #34
    //   2831: invokevirtual toString : ()Ljava/lang/String;
    //   2834: invokevirtual println : (Ljava/lang/String;)V
    //   2837: goto -> 2840
    //   2840: lload #24
    //   2842: lconst_0
    //   2843: lcmp
    //   2844: ifeq -> 2894
    //   2847: aload #34
    //   2849: iconst_0
    //   2850: invokevirtual setLength : (I)V
    //   2853: aload #34
    //   2855: aload #7
    //   2857: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2860: pop
    //   2861: aload #34
    //   2863: ldc_w '  Total partial wakelock time: '
    //   2866: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2869: pop
    //   2870: aload #34
    //   2872: lload #24
    //   2874: ldc2_w 500
    //   2877: ladd
    //   2878: ldc2_w 1000
    //   2881: ldiv
    //   2882: invokestatic formatTimeMsNoSpace : (Ljava/lang/StringBuilder;J)V
    //   2885: aload_2
    //   2886: aload #34
    //   2888: invokevirtual toString : ()Ljava/lang/String;
    //   2891: invokevirtual println : (Ljava/lang/String;)V
    //   2894: aload_0
    //   2895: lload #10
    //   2897: iload #4
    //   2899: invokevirtual getWifiMulticastWakelockTime : (JI)J
    //   2902: lstore #24
    //   2904: aload_0
    //   2905: iload #4
    //   2907: invokevirtual getWifiMulticastWakelockCount : (I)I
    //   2910: istore #37
    //   2912: lload #24
    //   2914: lconst_0
    //   2915: lcmp
    //   2916: ifeq -> 3006
    //   2919: aload #34
    //   2921: iconst_0
    //   2922: invokevirtual setLength : (I)V
    //   2925: aload #34
    //   2927: aload #7
    //   2929: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2932: pop
    //   2933: aload #34
    //   2935: ldc_w '  Total WiFi Multicast wakelock Count: '
    //   2938: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2941: pop
    //   2942: aload #34
    //   2944: iload #37
    //   2946: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2949: pop
    //   2950: aload_2
    //   2951: aload #34
    //   2953: invokevirtual toString : ()Ljava/lang/String;
    //   2956: invokevirtual println : (Ljava/lang/String;)V
    //   2959: aload #34
    //   2961: iconst_0
    //   2962: invokevirtual setLength : (I)V
    //   2965: aload #34
    //   2967: aload #7
    //   2969: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2972: pop
    //   2973: aload #34
    //   2975: ldc_w '  Total WiFi Multicast wakelock time: '
    //   2978: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2981: pop
    //   2982: aload #34
    //   2984: lload #24
    //   2986: ldc2_w 500
    //   2989: ladd
    //   2990: ldc2_w 1000
    //   2993: ldiv
    //   2994: invokestatic formatTimeMsNoSpace : (Ljava/lang/StringBuilder;J)V
    //   2997: aload_2
    //   2998: aload #34
    //   3000: invokevirtual toString : ()Ljava/lang/String;
    //   3003: invokevirtual println : (Ljava/lang/String;)V
    //   3006: aload_2
    //   3007: ldc_w ''
    //   3010: invokevirtual println : (Ljava/lang/String;)V
    //   3013: aload_2
    //   3014: aload_3
    //   3015: invokevirtual print : (Ljava/lang/String;)V
    //   3018: aload #34
    //   3020: iconst_0
    //   3021: invokevirtual setLength : (I)V
    //   3024: aload #34
    //   3026: aload #7
    //   3028: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3031: pop
    //   3032: aload #34
    //   3034: ldc_w '  CONNECTIVITY POWER SUMMARY START'
    //   3037: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3040: pop
    //   3041: aload_2
    //   3042: aload #34
    //   3044: invokevirtual toString : ()Ljava/lang/String;
    //   3047: invokevirtual println : (Ljava/lang/String;)V
    //   3050: aload_2
    //   3051: aload_3
    //   3052: invokevirtual print : (Ljava/lang/String;)V
    //   3055: aload #34
    //   3057: iconst_0
    //   3058: invokevirtual setLength : (I)V
    //   3061: aload #34
    //   3063: aload #7
    //   3065: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3068: pop
    //   3069: aload #34
    //   3071: ldc_w '  Logging duration for connectivity statistics: '
    //   3074: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3077: pop
    //   3078: aload #34
    //   3080: lload #18
    //   3082: ldc2_w 1000
    //   3085: ldiv
    //   3086: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   3089: aload_2
    //   3090: aload #34
    //   3092: invokevirtual toString : ()Ljava/lang/String;
    //   3095: invokevirtual println : (Ljava/lang/String;)V
    //   3098: aload #34
    //   3100: iconst_0
    //   3101: invokevirtual setLength : (I)V
    //   3104: aload #34
    //   3106: aload #7
    //   3108: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3111: pop
    //   3112: aload #34
    //   3114: ldc_w '  Cellular Statistics:'
    //   3117: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3120: pop
    //   3121: aload_2
    //   3122: aload #34
    //   3124: invokevirtual toString : ()Ljava/lang/String;
    //   3127: invokevirtual println : (Ljava/lang/String;)V
    //   3130: aload_2
    //   3131: aload_3
    //   3132: invokevirtual print : (Ljava/lang/String;)V
    //   3135: aload #34
    //   3137: iconst_0
    //   3138: invokevirtual setLength : (I)V
    //   3141: aload #34
    //   3143: aload #7
    //   3145: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3148: pop
    //   3149: aload #34
    //   3151: ldc_w '     Cellular kernel active time: '
    //   3154: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3157: pop
    //   3158: aload_0
    //   3159: lload #10
    //   3161: iload #4
    //   3163: invokevirtual getMobileRadioActiveTime : (JI)J
    //   3166: lstore #22
    //   3168: aload #34
    //   3170: lload #22
    //   3172: ldc2_w 1000
    //   3175: ldiv
    //   3176: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   3179: aload #34
    //   3181: aload #43
    //   3183: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3186: pop
    //   3187: aload #34
    //   3189: aload_0
    //   3190: lload #22
    //   3192: lload #18
    //   3194: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   3197: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3200: pop
    //   3201: aload #34
    //   3203: aload #45
    //   3205: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3208: pop
    //   3209: aload_2
    //   3210: aload #34
    //   3212: invokevirtual toString : ()Ljava/lang/String;
    //   3215: invokevirtual println : (Ljava/lang/String;)V
    //   3218: aload_0
    //   3219: invokevirtual getModemControllerActivity : ()Landroid/os/BatteryStats$ControllerActivityCounter;
    //   3222: astore #42
    //   3224: ldc_w ') '
    //   3227: astore #49
    //   3229: ldc_w ' '
    //   3232: astore #50
    //   3234: lload #20
    //   3236: lstore #24
    //   3238: iload #36
    //   3240: istore #44
    //   3242: lload #10
    //   3244: lstore #20
    //   3246: aload_0
    //   3247: aload_2
    //   3248: aload #34
    //   3250: aload_3
    //   3251: ldc 'Cellular'
    //   3253: aload #42
    //   3255: iload #4
    //   3257: invokespecial printControllerActivity : (Ljava/io/PrintWriter;Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$ControllerActivityCounter;I)V
    //   3260: aload_2
    //   3261: ldc_w '     Cellular data received: '
    //   3264: invokevirtual print : (Ljava/lang/String;)V
    //   3267: aload_2
    //   3268: aload_0
    //   3269: lload #24
    //   3271: invokevirtual formatBytesLocked : (J)Ljava/lang/String;
    //   3274: invokevirtual println : (Ljava/lang/String;)V
    //   3277: aload_2
    //   3278: ldc_w '     Cellular data sent: '
    //   3281: invokevirtual print : (Ljava/lang/String;)V
    //   3284: aload_2
    //   3285: aload_0
    //   3286: lload #16
    //   3288: invokevirtual formatBytesLocked : (J)Ljava/lang/String;
    //   3291: invokevirtual println : (Ljava/lang/String;)V
    //   3294: aload_2
    //   3295: ldc_w '     Cellular packets received: '
    //   3298: invokevirtual print : (Ljava/lang/String;)V
    //   3301: lload #40
    //   3303: lstore #10
    //   3305: aload_2
    //   3306: lload #10
    //   3308: invokevirtual println : (J)V
    //   3311: aload_2
    //   3312: ldc_w '     Cellular packets sent: '
    //   3315: invokevirtual print : (Ljava/lang/String;)V
    //   3318: aload_2
    //   3319: lload #30
    //   3321: invokevirtual println : (J)V
    //   3324: aload #34
    //   3326: iconst_0
    //   3327: invokevirtual setLength : (I)V
    //   3330: aload #34
    //   3332: aload #7
    //   3334: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3337: pop
    //   3338: aload #34
    //   3340: ldc_w '     Cellular Radio Access Technology:'
    //   3343: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3346: pop
    //   3347: iconst_0
    //   3348: istore #36
    //   3350: iconst_0
    //   3351: istore #37
    //   3353: aload #43
    //   3355: astore #42
    //   3357: aload #49
    //   3359: astore #43
    //   3361: iload #37
    //   3363: getstatic android/os/BatteryStats.NUM_DATA_CONNECTION_TYPES : I
    //   3366: if_icmpge -> 3503
    //   3369: aload_0
    //   3370: iload #37
    //   3372: lload #20
    //   3374: iload #4
    //   3376: invokevirtual getPhoneDataConnectionTime : (IJI)J
    //   3379: lstore #40
    //   3381: lload #40
    //   3383: lconst_0
    //   3384: lcmp
    //   3385: ifne -> 3391
    //   3388: goto -> 3497
    //   3391: aload #34
    //   3393: ldc_w '\\n       '
    //   3396: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3399: pop
    //   3400: aload #34
    //   3402: aload #7
    //   3404: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3407: pop
    //   3408: iconst_1
    //   3409: istore #36
    //   3411: getstatic android/os/BatteryStats.DATA_CONNECTION_NAMES : [Ljava/lang/String;
    //   3414: astore #49
    //   3416: iload #37
    //   3418: aload #49
    //   3420: arraylength
    //   3421: if_icmpge -> 3434
    //   3424: aload #49
    //   3426: iload #37
    //   3428: aaload
    //   3429: astore #49
    //   3431: goto -> 3439
    //   3434: ldc_w 'ERROR'
    //   3437: astore #49
    //   3439: aload #34
    //   3441: aload #49
    //   3443: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3446: pop
    //   3447: aload #34
    //   3449: ldc_w ' '
    //   3452: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3455: pop
    //   3456: aload #34
    //   3458: lload #40
    //   3460: ldc2_w 1000
    //   3463: ldiv
    //   3464: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   3467: aload #34
    //   3469: aload #42
    //   3471: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3474: pop
    //   3475: aload #34
    //   3477: aload_0
    //   3478: lload #40
    //   3480: lload #18
    //   3482: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   3485: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3488: pop
    //   3489: aload #34
    //   3491: aload #43
    //   3493: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3496: pop
    //   3497: iinc #37, 1
    //   3500: goto -> 3361
    //   3503: lload #30
    //   3505: lstore #40
    //   3507: lload #10
    //   3509: lstore #30
    //   3511: lload #16
    //   3513: lstore #10
    //   3515: aload #43
    //   3517: astore #49
    //   3519: lload #24
    //   3521: lstore #16
    //   3523: iload #36
    //   3525: ifne -> 3540
    //   3528: aload #34
    //   3530: ldc_w ' (no activity)'
    //   3533: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3536: pop
    //   3537: goto -> 3540
    //   3540: aload_2
    //   3541: aload #34
    //   3543: invokevirtual toString : ()Ljava/lang/String;
    //   3546: invokevirtual println : (Ljava/lang/String;)V
    //   3549: aload #34
    //   3551: iconst_0
    //   3552: invokevirtual setLength : (I)V
    //   3555: aload #34
    //   3557: aload #7
    //   3559: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3562: pop
    //   3563: aload #34
    //   3565: ldc_w '     Cellular Rx signal strength (RSRP):'
    //   3568: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3571: pop
    //   3572: iconst_5
    //   3573: anewarray java/lang/String
    //   3576: astore #51
    //   3578: aload #51
    //   3580: iconst_0
    //   3581: ldc_w 'very poor (less than -128dBm): '
    //   3584: aastore
    //   3585: aload #51
    //   3587: iconst_1
    //   3588: ldc_w 'poor (-128dBm to -118dBm): '
    //   3591: aastore
    //   3592: aload #51
    //   3594: iconst_2
    //   3595: ldc_w 'moderate (-118dBm to -108dBm): '
    //   3598: aastore
    //   3599: aload #51
    //   3601: iconst_3
    //   3602: ldc_w 'good (-108dBm to -98dBm): '
    //   3605: aastore
    //   3606: aload #51
    //   3608: iconst_4
    //   3609: ldc_w 'great (greater than -98dBm): '
    //   3612: aastore
    //   3613: invokestatic getNumSignalStrengthLevels : ()I
    //   3616: aload #51
    //   3618: arraylength
    //   3619: invokestatic min : (II)I
    //   3622: istore #60
    //   3624: iconst_0
    //   3625: istore #37
    //   3627: iconst_0
    //   3628: istore #36
    //   3630: lload #20
    //   3632: lstore #24
    //   3634: aload #50
    //   3636: astore #43
    //   3638: iload #37
    //   3640: iload #60
    //   3642: if_icmpge -> 3753
    //   3645: aload_0
    //   3646: iload #37
    //   3648: lload #24
    //   3650: iload #4
    //   3652: invokevirtual getPhoneSignalStrengthTime : (IJI)J
    //   3655: lstore #20
    //   3657: lload #20
    //   3659: lconst_0
    //   3660: lcmp
    //   3661: ifne -> 3667
    //   3664: goto -> 3747
    //   3667: aload #34
    //   3669: ldc_w '\\n       '
    //   3672: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3675: pop
    //   3676: aload #34
    //   3678: aload #7
    //   3680: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3683: pop
    //   3684: iconst_1
    //   3685: istore #36
    //   3687: aload #34
    //   3689: aload #51
    //   3691: iload #37
    //   3693: aaload
    //   3694: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3697: pop
    //   3698: aload #34
    //   3700: aload #43
    //   3702: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3705: pop
    //   3706: aload #34
    //   3708: lload #20
    //   3710: ldc2_w 1000
    //   3713: ldiv
    //   3714: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   3717: aload #34
    //   3719: aload #42
    //   3721: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3724: pop
    //   3725: aload #34
    //   3727: aload_0
    //   3728: lload #20
    //   3730: lload #18
    //   3732: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   3735: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3738: pop
    //   3739: aload #34
    //   3741: aload #49
    //   3743: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3746: pop
    //   3747: iinc #37, 1
    //   3750: goto -> 3638
    //   3753: iload #36
    //   3755: ifne -> 3770
    //   3758: aload #34
    //   3760: ldc_w ' (no activity)'
    //   3763: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3766: pop
    //   3767: goto -> 3770
    //   3770: aload_2
    //   3771: aload #34
    //   3773: invokevirtual toString : ()Ljava/lang/String;
    //   3776: invokevirtual println : (Ljava/lang/String;)V
    //   3779: aload_2
    //   3780: aload_3
    //   3781: invokevirtual print : (Ljava/lang/String;)V
    //   3784: aload #34
    //   3786: iconst_0
    //   3787: invokevirtual setLength : (I)V
    //   3790: aload #34
    //   3792: aload #7
    //   3794: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3797: pop
    //   3798: aload #34
    //   3800: ldc_w '  Wifi Statistics:'
    //   3803: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3806: pop
    //   3807: aload_2
    //   3808: aload #34
    //   3810: invokevirtual toString : ()Ljava/lang/String;
    //   3813: invokevirtual println : (Ljava/lang/String;)V
    //   3816: aload_2
    //   3817: aload_3
    //   3818: invokevirtual print : (Ljava/lang/String;)V
    //   3821: aload #34
    //   3823: iconst_0
    //   3824: invokevirtual setLength : (I)V
    //   3827: aload #34
    //   3829: aload #7
    //   3831: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3834: pop
    //   3835: aload #34
    //   3837: ldc_w '     Wifi kernel active time: '
    //   3840: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3843: pop
    //   3844: aload_0
    //   3845: lload #24
    //   3847: iload #4
    //   3849: invokevirtual getWifiActiveTime : (JI)J
    //   3852: lstore #20
    //   3854: aload #34
    //   3856: lload #20
    //   3858: ldc2_w 1000
    //   3861: ldiv
    //   3862: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   3865: aload #34
    //   3867: aload #42
    //   3869: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3872: pop
    //   3873: aload #34
    //   3875: aload_0
    //   3876: lload #20
    //   3878: lload #18
    //   3880: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   3883: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3886: pop
    //   3887: aload #34
    //   3889: aload #45
    //   3891: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   3894: pop
    //   3895: aload_2
    //   3896: aload #34
    //   3898: invokevirtual toString : ()Ljava/lang/String;
    //   3901: invokevirtual println : (Ljava/lang/String;)V
    //   3904: aload_0
    //   3905: invokevirtual getWifiControllerActivity : ()Landroid/os/BatteryStats$ControllerActivityCounter;
    //   3908: astore #50
    //   3910: lload #24
    //   3912: lstore #61
    //   3914: iload #60
    //   3916: istore #36
    //   3918: aload_0
    //   3919: aload_2
    //   3920: aload #34
    //   3922: aload_3
    //   3923: ldc_w 'WiFi'
    //   3926: aload #50
    //   3928: iload #4
    //   3930: invokespecial printControllerActivity : (Ljava/io/PrintWriter;Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$ControllerActivityCounter;I)V
    //   3933: aload_2
    //   3934: ldc_w '     Wifi data received: '
    //   3937: invokevirtual print : (Ljava/lang/String;)V
    //   3940: aload_2
    //   3941: aload_0
    //   3942: lload #32
    //   3944: invokevirtual formatBytesLocked : (J)Ljava/lang/String;
    //   3947: invokevirtual println : (Ljava/lang/String;)V
    //   3950: aload_2
    //   3951: ldc_w '     Wifi data sent: '
    //   3954: invokevirtual print : (Ljava/lang/String;)V
    //   3957: lload #52
    //   3959: lstore #20
    //   3961: aload_2
    //   3962: aload_0
    //   3963: lload #20
    //   3965: invokevirtual formatBytesLocked : (J)Ljava/lang/String;
    //   3968: invokevirtual println : (Ljava/lang/String;)V
    //   3971: aload_2
    //   3972: ldc_w '     Wifi packets received: '
    //   3975: invokevirtual print : (Ljava/lang/String;)V
    //   3978: aload_2
    //   3979: lload #38
    //   3981: invokevirtual println : (J)V
    //   3984: aload_2
    //   3985: ldc_w '     Wifi packets sent: '
    //   3988: invokevirtual print : (Ljava/lang/String;)V
    //   3991: aload_2
    //   3992: lload #54
    //   3994: invokevirtual println : (J)V
    //   3997: aload #34
    //   3999: iconst_0
    //   4000: invokevirtual setLength : (I)V
    //   4003: aload #34
    //   4005: aload #7
    //   4007: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4010: pop
    //   4011: aload #34
    //   4013: ldc_w '     Wifi states:'
    //   4016: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4019: pop
    //   4020: iconst_0
    //   4021: istore #60
    //   4023: iconst_0
    //   4024: istore #37
    //   4026: lload #18
    //   4028: lstore #24
    //   4030: lload #54
    //   4032: lstore #18
    //   4034: iload #37
    //   4036: bipush #8
    //   4038: if_icmpge -> 4142
    //   4041: aload_0
    //   4042: iload #37
    //   4044: lload #61
    //   4046: iload #4
    //   4048: invokevirtual getWifiStateTime : (IJI)J
    //   4051: lstore #54
    //   4053: lload #54
    //   4055: lconst_0
    //   4056: lcmp
    //   4057: ifne -> 4063
    //   4060: goto -> 4136
    //   4063: aload #34
    //   4065: ldc_w '\\n       '
    //   4068: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4071: pop
    //   4072: aload #34
    //   4074: getstatic android/os/BatteryStats.WIFI_STATE_NAMES : [Ljava/lang/String;
    //   4077: iload #37
    //   4079: aaload
    //   4080: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4083: pop
    //   4084: aload #34
    //   4086: aload #43
    //   4088: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4091: pop
    //   4092: aload #34
    //   4094: lload #54
    //   4096: ldc2_w 1000
    //   4099: ldiv
    //   4100: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   4103: aload #34
    //   4105: aload #42
    //   4107: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4110: pop
    //   4111: aload #34
    //   4113: aload_0
    //   4114: lload #54
    //   4116: lload #24
    //   4118: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   4121: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4124: pop
    //   4125: aload #34
    //   4127: aload #49
    //   4129: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4132: pop
    //   4133: iconst_1
    //   4134: istore #60
    //   4136: iinc #37, 1
    //   4139: goto -> 4034
    //   4142: lload #18
    //   4144: lstore #54
    //   4146: aload #43
    //   4148: astore #51
    //   4150: iload #60
    //   4152: ifne -> 4167
    //   4155: aload #34
    //   4157: ldc_w ' (no activity)'
    //   4160: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4163: pop
    //   4164: goto -> 4167
    //   4167: ldc_w ' (no activity)'
    //   4170: astore #43
    //   4172: aload_2
    //   4173: aload #34
    //   4175: invokevirtual toString : ()Ljava/lang/String;
    //   4178: invokevirtual println : (Ljava/lang/String;)V
    //   4181: aload #34
    //   4183: iconst_0
    //   4184: invokevirtual setLength : (I)V
    //   4187: aload #34
    //   4189: aload #7
    //   4191: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4194: pop
    //   4195: aload #34
    //   4197: ldc_w '     Wifi supplicant states:'
    //   4200: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4203: pop
    //   4204: iconst_0
    //   4205: istore #37
    //   4207: iconst_0
    //   4208: istore #60
    //   4210: lload #61
    //   4212: lstore #18
    //   4214: iload #60
    //   4216: bipush #13
    //   4218: if_icmpge -> 4322
    //   4221: aload_0
    //   4222: iload #60
    //   4224: lload #18
    //   4226: iload #4
    //   4228: invokevirtual getWifiSupplStateTime : (IJI)J
    //   4231: lstore #61
    //   4233: lload #61
    //   4235: lconst_0
    //   4236: lcmp
    //   4237: ifne -> 4243
    //   4240: goto -> 4316
    //   4243: aload #34
    //   4245: ldc_w '\\n       '
    //   4248: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4251: pop
    //   4252: aload #34
    //   4254: getstatic android/os/BatteryStats.WIFI_SUPPL_STATE_NAMES : [Ljava/lang/String;
    //   4257: iload #60
    //   4259: aaload
    //   4260: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4263: pop
    //   4264: aload #34
    //   4266: aload #51
    //   4268: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4271: pop
    //   4272: aload #34
    //   4274: lload #61
    //   4276: ldc2_w 1000
    //   4279: ldiv
    //   4280: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   4283: aload #34
    //   4285: aload #42
    //   4287: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4290: pop
    //   4291: aload #34
    //   4293: aload_0
    //   4294: lload #61
    //   4296: lload #24
    //   4298: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   4301: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4304: pop
    //   4305: aload #34
    //   4307: aload #49
    //   4309: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4312: pop
    //   4313: iconst_1
    //   4314: istore #37
    //   4316: iinc #60, 1
    //   4319: goto -> 4214
    //   4322: iload #37
    //   4324: ifne -> 4338
    //   4327: aload #34
    //   4329: aload #43
    //   4331: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4334: pop
    //   4335: goto -> 4338
    //   4338: aload #43
    //   4340: astore #63
    //   4342: aload_2
    //   4343: aload #34
    //   4345: invokevirtual toString : ()Ljava/lang/String;
    //   4348: invokevirtual println : (Ljava/lang/String;)V
    //   4351: aload #34
    //   4353: iconst_0
    //   4354: invokevirtual setLength : (I)V
    //   4357: aload #34
    //   4359: aload #7
    //   4361: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4364: pop
    //   4365: aload #34
    //   4367: ldc_w '     Wifi Rx signal strength (RSSI):'
    //   4370: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4373: pop
    //   4374: iconst_5
    //   4375: anewarray java/lang/String
    //   4378: dup
    //   4379: iconst_0
    //   4380: ldc_w 'very poor (less than -88.75dBm): '
    //   4383: aastore
    //   4384: dup
    //   4385: iconst_1
    //   4386: ldc_w 'poor (-88.75 to -77.5dBm): '
    //   4389: aastore
    //   4390: dup
    //   4391: iconst_2
    //   4392: ldc_w 'moderate (-77.5dBm to -66.25dBm): '
    //   4395: aastore
    //   4396: dup
    //   4397: iconst_3
    //   4398: ldc_w 'good (-66.25dBm to -55dBm): '
    //   4401: aastore
    //   4402: dup
    //   4403: iconst_4
    //   4404: ldc_w 'great (greater than -55dBm): '
    //   4407: aastore
    //   4408: astore #50
    //   4410: iconst_5
    //   4411: aload #50
    //   4413: arraylength
    //   4414: invokestatic min : (II)I
    //   4417: istore #37
    //   4419: iconst_0
    //   4420: istore #60
    //   4422: iconst_0
    //   4423: istore #64
    //   4425: aload #51
    //   4427: astore #43
    //   4429: aload #63
    //   4431: astore #51
    //   4433: iload #60
    //   4435: iload #37
    //   4437: if_icmpge -> 4549
    //   4440: aload_0
    //   4441: iload #60
    //   4443: lload #18
    //   4445: iload #4
    //   4447: invokevirtual getWifiSignalStrengthTime : (IJI)J
    //   4450: lstore #61
    //   4452: lload #61
    //   4454: lconst_0
    //   4455: lcmp
    //   4456: ifne -> 4462
    //   4459: goto -> 4543
    //   4462: aload #34
    //   4464: ldc_w '\\n    '
    //   4467: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4470: pop
    //   4471: aload #34
    //   4473: aload #7
    //   4475: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4478: pop
    //   4479: iconst_1
    //   4480: istore #64
    //   4482: aload #34
    //   4484: ldc_w '     '
    //   4487: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4490: pop
    //   4491: aload #34
    //   4493: aload #50
    //   4495: iload #60
    //   4497: aaload
    //   4498: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4501: pop
    //   4502: aload #34
    //   4504: lload #61
    //   4506: ldc2_w 1000
    //   4509: ldiv
    //   4510: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   4513: aload #34
    //   4515: aload #42
    //   4517: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4520: pop
    //   4521: aload #34
    //   4523: aload_0
    //   4524: lload #61
    //   4526: lload #24
    //   4528: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   4531: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4534: pop
    //   4535: aload #34
    //   4537: aload #49
    //   4539: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4542: pop
    //   4543: iinc #60, 1
    //   4546: goto -> 4433
    //   4549: iload #37
    //   4551: istore #60
    //   4553: aload #50
    //   4555: astore #65
    //   4557: iload #64
    //   4559: ifne -> 4570
    //   4562: aload #34
    //   4564: aload #51
    //   4566: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4569: pop
    //   4570: aload_2
    //   4571: aload #34
    //   4573: invokevirtual toString : ()Ljava/lang/String;
    //   4576: invokevirtual println : (Ljava/lang/String;)V
    //   4579: aload_2
    //   4580: aload_3
    //   4581: invokevirtual print : (Ljava/lang/String;)V
    //   4584: aload #34
    //   4586: iconst_0
    //   4587: invokevirtual setLength : (I)V
    //   4590: aload #34
    //   4592: aload #7
    //   4594: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4597: pop
    //   4598: aload #34
    //   4600: ldc_w '  GPS Statistics:'
    //   4603: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4606: pop
    //   4607: aload_2
    //   4608: aload #34
    //   4610: invokevirtual toString : ()Ljava/lang/String;
    //   4613: invokevirtual println : (Ljava/lang/String;)V
    //   4616: aload #34
    //   4618: iconst_0
    //   4619: invokevirtual setLength : (I)V
    //   4622: aload #34
    //   4624: aload #7
    //   4626: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4629: pop
    //   4630: aload #34
    //   4632: ldc_w '     GPS signal quality (Top 4 Average CN0):'
    //   4635: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4638: pop
    //   4639: iconst_2
    //   4640: anewarray java/lang/String
    //   4643: dup
    //   4644: iconst_0
    //   4645: ldc_w 'poor (less than 20 dBHz): '
    //   4648: aastore
    //   4649: dup
    //   4650: iconst_1
    //   4651: ldc_w 'good (greater than 20 dBHz): '
    //   4654: aastore
    //   4655: astore #50
    //   4657: iconst_2
    //   4658: aload #50
    //   4660: arraylength
    //   4661: invokestatic min : (II)I
    //   4664: istore #37
    //   4666: iconst_0
    //   4667: istore #64
    //   4669: iload #64
    //   4671: iload #37
    //   4673: if_icmpge -> 4772
    //   4676: aload_0
    //   4677: iload #64
    //   4679: lload #18
    //   4681: iload #4
    //   4683: invokevirtual getGpsSignalQualityTime : (IJI)J
    //   4686: lstore #61
    //   4688: aload #34
    //   4690: ldc_w '\\n    '
    //   4693: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4696: pop
    //   4697: aload #34
    //   4699: aload #7
    //   4701: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4704: pop
    //   4705: aload #34
    //   4707: ldc_w '  '
    //   4710: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4713: pop
    //   4714: aload #34
    //   4716: aload #50
    //   4718: iload #64
    //   4720: aaload
    //   4721: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4724: pop
    //   4725: aload #34
    //   4727: lload #61
    //   4729: ldc2_w 1000
    //   4732: ldiv
    //   4733: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   4736: aload #34
    //   4738: aload #42
    //   4740: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4743: pop
    //   4744: aload #34
    //   4746: aload_0
    //   4747: lload #61
    //   4749: lload #24
    //   4751: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   4754: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4757: pop
    //   4758: aload #34
    //   4760: aload #49
    //   4762: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4765: pop
    //   4766: iinc #64, 1
    //   4769: goto -> 4669
    //   4772: aload #50
    //   4774: astore #66
    //   4776: aload_2
    //   4777: aload #34
    //   4779: invokevirtual toString : ()Ljava/lang/String;
    //   4782: invokevirtual println : (Ljava/lang/String;)V
    //   4785: aload_0
    //   4786: invokevirtual getGpsBatteryDrainMaMs : ()J
    //   4789: lstore #61
    //   4791: lload #61
    //   4793: lconst_0
    //   4794: lcmp
    //   4795: ifle -> 4870
    //   4798: aload_2
    //   4799: aload_3
    //   4800: invokevirtual print : (Ljava/lang/String;)V
    //   4803: aload #34
    //   4805: iconst_0
    //   4806: invokevirtual setLength : (I)V
    //   4809: aload #34
    //   4811: aload #7
    //   4813: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4816: pop
    //   4817: aload #34
    //   4819: ldc_w '     GPS Battery Drain: '
    //   4822: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4825: pop
    //   4826: aload #34
    //   4828: new java/text/DecimalFormat
    //   4831: dup
    //   4832: ldc_w '#.##'
    //   4835: invokespecial <init> : (Ljava/lang/String;)V
    //   4838: lload #61
    //   4840: l2d
    //   4841: ldc2_w 3600000.0
    //   4844: ddiv
    //   4845: invokevirtual format : (D)Ljava/lang/String;
    //   4848: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4851: pop
    //   4852: aload #34
    //   4854: ldc_w 'mAh'
    //   4857: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4860: pop
    //   4861: aload_2
    //   4862: aload #34
    //   4864: invokevirtual toString : ()Ljava/lang/String;
    //   4867: invokevirtual println : (Ljava/lang/String;)V
    //   4870: aload_2
    //   4871: aload_3
    //   4872: invokevirtual print : (Ljava/lang/String;)V
    //   4875: aload #34
    //   4877: iconst_0
    //   4878: invokevirtual setLength : (I)V
    //   4881: aload #34
    //   4883: aload #7
    //   4885: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4888: pop
    //   4889: aload #34
    //   4891: ldc_w '  CONNECTIVITY POWER SUMMARY END'
    //   4894: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4897: pop
    //   4898: aload_2
    //   4899: aload #34
    //   4901: invokevirtual toString : ()Ljava/lang/String;
    //   4904: invokevirtual println : (Ljava/lang/String;)V
    //   4907: aload_2
    //   4908: ldc_w ''
    //   4911: invokevirtual println : (Ljava/lang/String;)V
    //   4914: aload_2
    //   4915: aload_3
    //   4916: invokevirtual print : (Ljava/lang/String;)V
    //   4919: aload_2
    //   4920: ldc_w '  Bluetooth total received: '
    //   4923: invokevirtual print : (Ljava/lang/String;)V
    //   4926: aload_2
    //   4927: aload_0
    //   4928: lload #56
    //   4930: invokevirtual formatBytesLocked : (J)Ljava/lang/String;
    //   4933: invokevirtual print : (Ljava/lang/String;)V
    //   4936: aload_2
    //   4937: ldc_w ', sent: '
    //   4940: invokevirtual print : (Ljava/lang/String;)V
    //   4943: aload_2
    //   4944: aload_0
    //   4945: lload #58
    //   4947: invokevirtual formatBytesLocked : (J)Ljava/lang/String;
    //   4950: invokevirtual println : (Ljava/lang/String;)V
    //   4953: aload_0
    //   4954: lload #18
    //   4956: iload #4
    //   4958: invokevirtual getBluetoothScanTime : (JI)J
    //   4961: lstore #52
    //   4963: lload #52
    //   4965: ldc2_w 1000
    //   4968: ldiv
    //   4969: lstore #52
    //   4971: aload #34
    //   4973: iconst_0
    //   4974: invokevirtual setLength : (I)V
    //   4977: aload #34
    //   4979: aload #7
    //   4981: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4984: pop
    //   4985: aload #34
    //   4987: ldc_w '  Bluetooth scan time: '
    //   4990: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   4993: pop
    //   4994: aload #34
    //   4996: lload #52
    //   4998: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   5001: aload_2
    //   5002: aload #34
    //   5004: invokevirtual toString : ()Ljava/lang/String;
    //   5007: invokevirtual println : (Ljava/lang/String;)V
    //   5010: aload_0
    //   5011: invokevirtual getBluetoothControllerActivity : ()Landroid/os/BatteryStats$ControllerActivityCounter;
    //   5014: astore #50
    //   5016: aload_0
    //   5017: aload_2
    //   5018: aload #34
    //   5020: aload_3
    //   5021: ldc_w 'Bluetooth'
    //   5024: aload #50
    //   5026: iload #4
    //   5028: invokespecial printControllerActivity : (Ljava/io/PrintWriter;Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$ControllerActivityCounter;I)V
    //   5031: aload_2
    //   5032: invokevirtual println : ()V
    //   5035: aload_2
    //   5036: aload_3
    //   5037: invokevirtual print : (Ljava/lang/String;)V
    //   5040: aload_2
    //   5041: ldc_w '  Device battery use since last full charge'
    //   5044: invokevirtual println : (Ljava/lang/String;)V
    //   5047: aload_2
    //   5048: aload_3
    //   5049: invokevirtual print : (Ljava/lang/String;)V
    //   5052: aload_2
    //   5053: ldc_w '    Amount discharged (lower bound): '
    //   5056: invokevirtual print : (Ljava/lang/String;)V
    //   5059: aload_2
    //   5060: aload_0
    //   5061: invokevirtual getLowDischargeAmountSinceCharge : ()I
    //   5064: invokevirtual println : (I)V
    //   5067: aload_2
    //   5068: aload_3
    //   5069: invokevirtual print : (Ljava/lang/String;)V
    //   5072: aload_2
    //   5073: ldc_w '    Amount discharged (upper bound): '
    //   5076: invokevirtual print : (Ljava/lang/String;)V
    //   5079: aload_2
    //   5080: aload_0
    //   5081: invokevirtual getHighDischargeAmountSinceCharge : ()I
    //   5084: invokevirtual println : (I)V
    //   5087: aload_2
    //   5088: aload_3
    //   5089: invokevirtual print : (Ljava/lang/String;)V
    //   5092: aload_2
    //   5093: ldc_w '    Amount discharged while screen on: '
    //   5096: invokevirtual print : (Ljava/lang/String;)V
    //   5099: aload_2
    //   5100: aload_0
    //   5101: invokevirtual getDischargeAmountScreenOnSinceCharge : ()I
    //   5104: invokevirtual println : (I)V
    //   5107: aload_2
    //   5108: aload_3
    //   5109: invokevirtual print : (Ljava/lang/String;)V
    //   5112: aload_2
    //   5113: ldc_w '    Amount discharged while screen off: '
    //   5116: invokevirtual print : (Ljava/lang/String;)V
    //   5119: aload_2
    //   5120: aload_0
    //   5121: invokevirtual getDischargeAmountScreenOffSinceCharge : ()I
    //   5124: invokevirtual println : (I)V
    //   5127: aload_2
    //   5128: aload_3
    //   5129: invokevirtual print : (Ljava/lang/String;)V
    //   5132: aload_2
    //   5133: ldc_w '    Amount discharged while screen doze: '
    //   5136: invokevirtual print : (Ljava/lang/String;)V
    //   5139: aload_2
    //   5140: aload_0
    //   5141: invokevirtual getDischargeAmountScreenDozeSinceCharge : ()I
    //   5144: invokevirtual println : (I)V
    //   5147: aload_2
    //   5148: invokevirtual println : ()V
    //   5151: new com/android/internal/os/BatteryStatsHelper
    //   5154: dup
    //   5155: aload_1
    //   5156: iconst_0
    //   5157: iload #6
    //   5159: invokespecial <init> : (Landroid/content/Context;ZZ)V
    //   5162: astore_1
    //   5163: aload_1
    //   5164: aload_0
    //   5165: invokevirtual create : (Landroid/os/BatteryStats;)V
    //   5168: aload_1
    //   5169: iload #4
    //   5171: iconst_m1
    //   5172: invokevirtual refreshStats : (II)V
    //   5175: aload_1
    //   5176: invokevirtual getUsageList : ()Ljava/util/List;
    //   5179: astore #50
    //   5181: ldc_w ' ('
    //   5184: astore #51
    //   5186: ldc_w ': '
    //   5189: astore #63
    //   5191: aload #50
    //   5193: ifnull -> 6031
    //   5196: aload #50
    //   5198: invokeinterface size : ()I
    //   5203: ifle -> 6031
    //   5206: aload_2
    //   5207: aload_3
    //   5208: invokevirtual print : (Ljava/lang/String;)V
    //   5211: aload_2
    //   5212: ldc_w '  Estimated power use (mAh):'
    //   5215: invokevirtual println : (Ljava/lang/String;)V
    //   5218: aload_2
    //   5219: aload_3
    //   5220: invokevirtual print : (Ljava/lang/String;)V
    //   5223: aload_2
    //   5224: ldc_w '    Capacity: '
    //   5227: invokevirtual print : (Ljava/lang/String;)V
    //   5230: aload_0
    //   5231: aload_2
    //   5232: aload_1
    //   5233: invokevirtual getPowerProfile : ()Lcom/android/internal/os/PowerProfile;
    //   5236: invokevirtual getBatteryCapacity : ()D
    //   5239: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5242: aload_2
    //   5243: ldc_w ', Computed drain: '
    //   5246: invokevirtual print : (Ljava/lang/String;)V
    //   5249: aload_0
    //   5250: aload_2
    //   5251: aload_1
    //   5252: invokevirtual getComputedPower : ()D
    //   5255: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5258: aload_2
    //   5259: ldc_w ', actual drain: '
    //   5262: invokevirtual print : (Ljava/lang/String;)V
    //   5265: aload_0
    //   5266: aload_2
    //   5267: aload_1
    //   5268: invokevirtual getMinDrainedPower : ()D
    //   5271: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5274: aload_1
    //   5275: invokevirtual getMinDrainedPower : ()D
    //   5278: aload_1
    //   5279: invokevirtual getMaxDrainedPower : ()D
    //   5282: dcmpl
    //   5283: ifeq -> 5302
    //   5286: aload_2
    //   5287: ldc_w '-'
    //   5290: invokevirtual print : (Ljava/lang/String;)V
    //   5293: aload_0
    //   5294: aload_2
    //   5295: aload_1
    //   5296: invokevirtual getMaxDrainedPower : ()D
    //   5299: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5302: aload_2
    //   5303: invokevirtual println : ()V
    //   5306: iconst_0
    //   5307: istore #37
    //   5309: iload #37
    //   5311: aload #50
    //   5313: invokeinterface size : ()I
    //   5318: if_icmpge -> 6024
    //   5321: aload #50
    //   5323: iload #37
    //   5325: invokeinterface get : (I)Ljava/lang/Object;
    //   5330: checkcast com/android/internal/os/BatterySipper
    //   5333: astore #67
    //   5335: aload_2
    //   5336: aload_3
    //   5337: invokevirtual print : (Ljava/lang/String;)V
    //   5340: getstatic android/os/BatteryStats$2.$SwitchMap$com$android$internal$os$BatterySipper$DrainType : [I
    //   5343: aload #67
    //   5345: getfield drainType : Lcom/android/internal/os/BatterySipper$DrainType;
    //   5348: invokevirtual ordinal : ()I
    //   5351: iaload
    //   5352: tableswitch default -> 5420, 1 -> 5585, 2 -> 5575, 3 -> 5565, 4 -> 5555, 5 -> 5545, 6 -> 5535, 7 -> 5525, 8 -> 5515, 9 -> 5486, 10 -> 5460, 11 -> 5450, 12 -> 5440, 13 -> 5430
    //   5420: aload_2
    //   5421: ldc_w '    ???: '
    //   5424: invokevirtual print : (Ljava/lang/String;)V
    //   5427: goto -> 5592
    //   5430: aload_2
    //   5431: ldc_w '    Camera: '
    //   5434: invokevirtual print : (Ljava/lang/String;)V
    //   5437: goto -> 5592
    //   5440: aload_2
    //   5441: ldc_w '    Over-counted: '
    //   5444: invokevirtual print : (Ljava/lang/String;)V
    //   5447: goto -> 5592
    //   5450: aload_2
    //   5451: ldc_w '    Unaccounted: '
    //   5454: invokevirtual print : (Ljava/lang/String;)V
    //   5457: goto -> 5592
    //   5460: aload_2
    //   5461: ldc_w '    User '
    //   5464: invokevirtual print : (Ljava/lang/String;)V
    //   5467: aload_2
    //   5468: aload #67
    //   5470: getfield userId : I
    //   5473: invokevirtual print : (I)V
    //   5476: aload_2
    //   5477: ldc_w ': '
    //   5480: invokevirtual print : (Ljava/lang/String;)V
    //   5483: goto -> 5592
    //   5486: aload_2
    //   5487: ldc_w '    Uid '
    //   5490: invokevirtual print : (Ljava/lang/String;)V
    //   5493: aload_2
    //   5494: aload #67
    //   5496: getfield uidObj : Landroid/os/BatteryStats$Uid;
    //   5499: invokevirtual getUid : ()I
    //   5502: invokestatic formatUid : (Ljava/io/PrintWriter;I)V
    //   5505: aload_2
    //   5506: ldc_w ': '
    //   5509: invokevirtual print : (Ljava/lang/String;)V
    //   5512: goto -> 5592
    //   5515: aload_2
    //   5516: ldc_w '    Flashlight: '
    //   5519: invokevirtual print : (Ljava/lang/String;)V
    //   5522: goto -> 5592
    //   5525: aload_2
    //   5526: ldc_w '    Screen: '
    //   5529: invokevirtual print : (Ljava/lang/String;)V
    //   5532: goto -> 5592
    //   5535: aload_2
    //   5536: ldc_w '    Bluetooth: '
    //   5539: invokevirtual print : (Ljava/lang/String;)V
    //   5542: goto -> 5592
    //   5545: aload_2
    //   5546: ldc_w '    Wifi: '
    //   5549: invokevirtual print : (Ljava/lang/String;)V
    //   5552: goto -> 5592
    //   5555: aload_2
    //   5556: ldc_w '    Phone calls: '
    //   5559: invokevirtual print : (Ljava/lang/String;)V
    //   5562: goto -> 5592
    //   5565: aload_2
    //   5566: ldc_w '    Cell standby: '
    //   5569: invokevirtual print : (Ljava/lang/String;)V
    //   5572: goto -> 5592
    //   5575: aload_2
    //   5576: ldc_w '    Idle: '
    //   5579: invokevirtual print : (Ljava/lang/String;)V
    //   5582: goto -> 5592
    //   5585: aload_2
    //   5586: ldc_w '    Ambient display: '
    //   5589: invokevirtual print : (Ljava/lang/String;)V
    //   5592: aload_0
    //   5593: aload_2
    //   5594: aload #67
    //   5596: getfield totalPowerMah : D
    //   5599: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5602: aload #67
    //   5604: getfield usagePowerMah : D
    //   5607: aload #67
    //   5609: getfield totalPowerMah : D
    //   5612: dcmpl
    //   5613: ifeq -> 5900
    //   5616: aload_2
    //   5617: ldc_w ' ('
    //   5620: invokevirtual print : (Ljava/lang/String;)V
    //   5623: aload #67
    //   5625: getfield usagePowerMah : D
    //   5628: dconst_0
    //   5629: dcmpl
    //   5630: ifeq -> 5650
    //   5633: aload_2
    //   5634: ldc_w ' usage='
    //   5637: invokevirtual print : (Ljava/lang/String;)V
    //   5640: aload_0
    //   5641: aload_2
    //   5642: aload #67
    //   5644: getfield usagePowerMah : D
    //   5647: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5650: aload #67
    //   5652: getfield cpuPowerMah : D
    //   5655: dconst_0
    //   5656: dcmpl
    //   5657: ifeq -> 5677
    //   5660: aload_2
    //   5661: ldc_w ' cpu='
    //   5664: invokevirtual print : (Ljava/lang/String;)V
    //   5667: aload_0
    //   5668: aload_2
    //   5669: aload #67
    //   5671: getfield cpuPowerMah : D
    //   5674: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5677: aload #67
    //   5679: getfield wakeLockPowerMah : D
    //   5682: dconst_0
    //   5683: dcmpl
    //   5684: ifeq -> 5704
    //   5687: aload_2
    //   5688: ldc_w ' wake='
    //   5691: invokevirtual print : (Ljava/lang/String;)V
    //   5694: aload_0
    //   5695: aload_2
    //   5696: aload #67
    //   5698: getfield wakeLockPowerMah : D
    //   5701: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5704: aload #67
    //   5706: getfield mobileRadioPowerMah : D
    //   5709: dconst_0
    //   5710: dcmpl
    //   5711: ifeq -> 5731
    //   5714: aload_2
    //   5715: ldc_w ' radio='
    //   5718: invokevirtual print : (Ljava/lang/String;)V
    //   5721: aload_0
    //   5722: aload_2
    //   5723: aload #67
    //   5725: getfield mobileRadioPowerMah : D
    //   5728: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5731: aload #67
    //   5733: getfield wifiPowerMah : D
    //   5736: dconst_0
    //   5737: dcmpl
    //   5738: ifeq -> 5758
    //   5741: aload_2
    //   5742: ldc_w ' wifi='
    //   5745: invokevirtual print : (Ljava/lang/String;)V
    //   5748: aload_0
    //   5749: aload_2
    //   5750: aload #67
    //   5752: getfield wifiPowerMah : D
    //   5755: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5758: aload #67
    //   5760: getfield bluetoothPowerMah : D
    //   5763: dconst_0
    //   5764: dcmpl
    //   5765: ifeq -> 5785
    //   5768: aload_2
    //   5769: ldc_w ' bt='
    //   5772: invokevirtual print : (Ljava/lang/String;)V
    //   5775: aload_0
    //   5776: aload_2
    //   5777: aload #67
    //   5779: getfield bluetoothPowerMah : D
    //   5782: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5785: aload #67
    //   5787: getfield gpsPowerMah : D
    //   5790: dconst_0
    //   5791: dcmpl
    //   5792: ifeq -> 5812
    //   5795: aload_2
    //   5796: ldc_w ' gps='
    //   5799: invokevirtual print : (Ljava/lang/String;)V
    //   5802: aload_0
    //   5803: aload_2
    //   5804: aload #67
    //   5806: getfield gpsPowerMah : D
    //   5809: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5812: aload #67
    //   5814: getfield sensorPowerMah : D
    //   5817: dconst_0
    //   5818: dcmpl
    //   5819: ifeq -> 5839
    //   5822: aload_2
    //   5823: ldc_w ' sensor='
    //   5826: invokevirtual print : (Ljava/lang/String;)V
    //   5829: aload_0
    //   5830: aload_2
    //   5831: aload #67
    //   5833: getfield sensorPowerMah : D
    //   5836: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5839: aload #67
    //   5841: getfield cameraPowerMah : D
    //   5844: dconst_0
    //   5845: dcmpl
    //   5846: ifeq -> 5866
    //   5849: aload_2
    //   5850: ldc_w ' camera='
    //   5853: invokevirtual print : (Ljava/lang/String;)V
    //   5856: aload_0
    //   5857: aload_2
    //   5858: aload #67
    //   5860: getfield cameraPowerMah : D
    //   5863: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5866: aload #67
    //   5868: getfield flashlightPowerMah : D
    //   5871: dconst_0
    //   5872: dcmpl
    //   5873: ifeq -> 5893
    //   5876: aload_2
    //   5877: ldc_w ' flash='
    //   5880: invokevirtual print : (Ljava/lang/String;)V
    //   5883: aload_0
    //   5884: aload_2
    //   5885: aload #67
    //   5887: getfield flashlightPowerMah : D
    //   5890: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5893: aload_2
    //   5894: ldc_w ' )'
    //   5897: invokevirtual print : (Ljava/lang/String;)V
    //   5900: aload #67
    //   5902: getfield totalSmearedPowerMah : D
    //   5905: aload #67
    //   5907: getfield totalPowerMah : D
    //   5910: dcmpl
    //   5911: ifeq -> 5999
    //   5914: aload_2
    //   5915: ldc_w ' Including smearing: '
    //   5918: invokevirtual print : (Ljava/lang/String;)V
    //   5921: aload_0
    //   5922: aload_2
    //   5923: aload #67
    //   5925: getfield totalSmearedPowerMah : D
    //   5928: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5931: aload_2
    //   5932: ldc_w ' ('
    //   5935: invokevirtual print : (Ljava/lang/String;)V
    //   5938: aload #67
    //   5940: getfield screenPowerMah : D
    //   5943: dconst_0
    //   5944: dcmpl
    //   5945: ifeq -> 5965
    //   5948: aload_2
    //   5949: ldc_w ' screen='
    //   5952: invokevirtual print : (Ljava/lang/String;)V
    //   5955: aload_0
    //   5956: aload_2
    //   5957: aload #67
    //   5959: getfield screenPowerMah : D
    //   5962: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5965: aload #67
    //   5967: getfield proportionalSmearMah : D
    //   5970: dconst_0
    //   5971: dcmpl
    //   5972: ifeq -> 5992
    //   5975: aload_2
    //   5976: ldc_w ' proportional='
    //   5979: invokevirtual print : (Ljava/lang/String;)V
    //   5982: aload_0
    //   5983: aload_2
    //   5984: aload #67
    //   5986: getfield proportionalSmearMah : D
    //   5989: invokespecial printmAh : (Ljava/io/PrintWriter;D)V
    //   5992: aload_2
    //   5993: ldc_w ' )'
    //   5996: invokevirtual print : (Ljava/lang/String;)V
    //   5999: aload #67
    //   6001: getfield shouldHide : Z
    //   6004: ifeq -> 6014
    //   6007: aload_2
    //   6008: ldc_w ' Excluded from smearing'
    //   6011: invokevirtual print : (Ljava/lang/String;)V
    //   6014: aload_2
    //   6015: invokevirtual println : ()V
    //   6018: iinc #37, 1
    //   6021: goto -> 5309
    //   6024: aload_2
    //   6025: invokevirtual println : ()V
    //   6028: goto -> 6031
    //   6031: aload_1
    //   6032: invokevirtual getMobilemsppList : ()Ljava/util/List;
    //   6035: astore #68
    //   6037: aload #68
    //   6039: ifnull -> 6341
    //   6042: aload #68
    //   6044: invokeinterface size : ()I
    //   6049: ifle -> 6341
    //   6052: aload_2
    //   6053: aload_3
    //   6054: invokevirtual print : (Ljava/lang/String;)V
    //   6057: aload_2
    //   6058: ldc_w '  Per-app mobile ms per packet:'
    //   6061: invokevirtual println : (Ljava/lang/String;)V
    //   6064: lconst_0
    //   6065: lstore #69
    //   6067: iconst_0
    //   6068: istore #37
    //   6070: aload #51
    //   6072: astore #50
    //   6074: iload #37
    //   6076: aload #68
    //   6078: invokeinterface size : ()I
    //   6083: if_icmpge -> 6255
    //   6086: aload #68
    //   6088: iload #37
    //   6090: invokeinterface get : (I)Ljava/lang/Object;
    //   6095: checkcast com/android/internal/os/BatterySipper
    //   6098: astore #51
    //   6100: aload #34
    //   6102: iconst_0
    //   6103: invokevirtual setLength : (I)V
    //   6106: aload #34
    //   6108: aload #7
    //   6110: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6113: pop
    //   6114: aload #34
    //   6116: ldc_w '    Uid '
    //   6119: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6122: pop
    //   6123: aload #34
    //   6125: aload #51
    //   6127: getfield uidObj : Landroid/os/BatteryStats$Uid;
    //   6130: invokevirtual getUid : ()I
    //   6133: invokestatic formatUid : (Ljava/lang/StringBuilder;I)V
    //   6136: aload #34
    //   6138: ldc_w ': '
    //   6141: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6144: pop
    //   6145: aload #34
    //   6147: aload #51
    //   6149: getfield mobilemspp : D
    //   6152: invokestatic makemAh : (D)Ljava/lang/String;
    //   6155: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6158: pop
    //   6159: aload #34
    //   6161: aload #50
    //   6163: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6166: pop
    //   6167: aload #34
    //   6169: aload #51
    //   6171: getfield mobileRxPackets : J
    //   6174: aload #51
    //   6176: getfield mobileTxPackets : J
    //   6179: ladd
    //   6180: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   6183: pop
    //   6184: aload #34
    //   6186: ldc_w ' packets over '
    //   6189: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6192: pop
    //   6193: aload #34
    //   6195: aload #51
    //   6197: getfield mobileActive : J
    //   6200: invokestatic formatTimeMsNoSpace : (Ljava/lang/StringBuilder;J)V
    //   6203: aload #34
    //   6205: aload #49
    //   6207: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6210: pop
    //   6211: aload #34
    //   6213: aload #51
    //   6215: getfield mobileActiveCount : I
    //   6218: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   6221: pop
    //   6222: aload #34
    //   6224: aload #46
    //   6226: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6229: pop
    //   6230: aload_2
    //   6231: aload #34
    //   6233: invokevirtual toString : ()Ljava/lang/String;
    //   6236: invokevirtual println : (Ljava/lang/String;)V
    //   6239: lload #69
    //   6241: aload #51
    //   6243: getfield mobileActive : J
    //   6246: ladd
    //   6247: lstore #69
    //   6249: iinc #37, 1
    //   6252: goto -> 6074
    //   6255: aload #50
    //   6257: astore #51
    //   6259: aload #34
    //   6261: iconst_0
    //   6262: invokevirtual setLength : (I)V
    //   6265: aload #34
    //   6267: aload #7
    //   6269: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6272: pop
    //   6273: aload #34
    //   6275: ldc_w '    TOTAL TIME: '
    //   6278: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6281: pop
    //   6282: aload #34
    //   6284: lload #69
    //   6286: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   6289: aload #34
    //   6291: aload #42
    //   6293: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6296: pop
    //   6297: aload #34
    //   6299: aload_0
    //   6300: lload #69
    //   6302: lload #24
    //   6304: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   6307: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6310: pop
    //   6311: aload #34
    //   6313: aload #45
    //   6315: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6318: pop
    //   6319: aload_2
    //   6320: aload #34
    //   6322: invokevirtual toString : ()Ljava/lang/String;
    //   6325: invokevirtual println : (Ljava/lang/String;)V
    //   6328: aload_2
    //   6329: invokevirtual println : ()V
    //   6332: aload_1
    //   6333: astore #50
    //   6335: aload #51
    //   6337: astore_1
    //   6338: goto -> 6352
    //   6341: ldc_w ' ('
    //   6344: astore #51
    //   6346: aload_1
    //   6347: astore #50
    //   6349: aload #51
    //   6351: astore_1
    //   6352: new android/os/BatteryStats$1
    //   6355: dup
    //   6356: aload_0
    //   6357: invokespecial <init> : (Landroid/os/BatteryStats;)V
    //   6360: astore #51
    //   6362: ldc_w ' realtime'
    //   6365: astore #71
    //   6367: iload #5
    //   6369: ifge -> 7229
    //   6372: aload_0
    //   6373: invokevirtual getKernelWakelockStats : ()Ljava/util/Map;
    //   6376: astore #67
    //   6378: aload #67
    //   6380: invokeinterface size : ()I
    //   6385: ifle -> 6733
    //   6388: new java/util/ArrayList
    //   6391: dup
    //   6392: invokespecial <init> : ()V
    //   6395: astore #72
    //   6397: aload #67
    //   6399: invokeinterface entrySet : ()Ljava/util/Set;
    //   6404: invokeinterface iterator : ()Ljava/util/Iterator;
    //   6409: astore #67
    //   6411: aload #67
    //   6413: invokeinterface hasNext : ()Z
    //   6418: ifeq -> 6497
    //   6421: aload #67
    //   6423: invokeinterface next : ()Ljava/lang/Object;
    //   6428: checkcast java/util/Map$Entry
    //   6431: astore #73
    //   6433: aload #73
    //   6435: invokeinterface getValue : ()Ljava/lang/Object;
    //   6440: checkcast android/os/BatteryStats$Timer
    //   6443: astore #74
    //   6445: aload #74
    //   6447: lload #18
    //   6449: iload #4
    //   6451: invokestatic computeWakeLock : (Landroid/os/BatteryStats$Timer;JI)J
    //   6454: lstore #69
    //   6456: lload #69
    //   6458: lconst_0
    //   6459: lcmp
    //   6460: ifle -> 6494
    //   6463: aload #72
    //   6465: new android/os/BatteryStats$TimerEntry
    //   6468: dup
    //   6469: aload #73
    //   6471: invokeinterface getKey : ()Ljava/lang/Object;
    //   6476: checkcast java/lang/String
    //   6479: iconst_0
    //   6480: aload #74
    //   6482: lload #69
    //   6484: invokespecial <init> : (Ljava/lang/String;ILandroid/os/BatteryStats$Timer;J)V
    //   6487: invokevirtual add : (Ljava/lang/Object;)Z
    //   6490: pop
    //   6491: goto -> 6494
    //   6494: goto -> 6411
    //   6497: aload #45
    //   6499: astore #67
    //   6501: iload #4
    //   6503: istore #37
    //   6505: aload #42
    //   6507: astore #74
    //   6509: aload #46
    //   6511: astore #73
    //   6513: aload #72
    //   6515: invokevirtual size : ()I
    //   6518: ifle -> 6704
    //   6521: aload #72
    //   6523: aload #51
    //   6525: invokestatic sort : (Ljava/util/List;Ljava/util/Comparator;)V
    //   6528: aload_2
    //   6529: aload_3
    //   6530: invokevirtual print : (Ljava/lang/String;)V
    //   6533: aload_2
    //   6534: ldc_w '  All kernel wake locks:'
    //   6537: invokevirtual println : (Ljava/lang/String;)V
    //   6540: iconst_0
    //   6541: istore #64
    //   6543: aload #63
    //   6545: astore #45
    //   6547: aload #67
    //   6549: astore #46
    //   6551: aload #71
    //   6553: astore #42
    //   6555: aload #72
    //   6557: astore #63
    //   6559: iload #64
    //   6561: aload #63
    //   6563: invokevirtual size : ()I
    //   6566: if_icmpge -> 6671
    //   6569: aload #63
    //   6571: iload #64
    //   6573: invokevirtual get : (I)Ljava/lang/Object;
    //   6576: checkcast android/os/BatteryStats$TimerEntry
    //   6579: astore #67
    //   6581: aload #34
    //   6583: iconst_0
    //   6584: invokevirtual setLength : (I)V
    //   6587: aload #34
    //   6589: aload #7
    //   6591: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6594: pop
    //   6595: aload #34
    //   6597: ldc_w '  Kernel Wake lock '
    //   6600: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6603: pop
    //   6604: aload #34
    //   6606: aload #67
    //   6608: getfield mName : Ljava/lang/String;
    //   6611: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6614: pop
    //   6615: aload #34
    //   6617: aload #67
    //   6619: getfield mTimer : Landroid/os/BatteryStats$Timer;
    //   6622: lload #18
    //   6624: aconst_null
    //   6625: iload #4
    //   6627: ldc_w ': '
    //   6630: invokestatic printWakeLock : (Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    //   6633: astore #67
    //   6635: aload #67
    //   6637: aload #45
    //   6639: invokevirtual equals : (Ljava/lang/Object;)Z
    //   6642: ifne -> 6665
    //   6645: aload #34
    //   6647: aload #42
    //   6649: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6652: pop
    //   6653: aload_2
    //   6654: aload #34
    //   6656: invokevirtual toString : ()Ljava/lang/String;
    //   6659: invokevirtual println : (Ljava/lang/String;)V
    //   6662: goto -> 6665
    //   6665: iinc #64, 1
    //   6668: goto -> 6559
    //   6671: aload_1
    //   6672: astore #50
    //   6674: aload #46
    //   6676: astore #67
    //   6678: aload #45
    //   6680: astore_1
    //   6681: aload_2
    //   6682: invokevirtual println : ()V
    //   6685: aload #51
    //   6687: astore #46
    //   6689: aload #73
    //   6691: astore #63
    //   6693: aload #74
    //   6695: astore #51
    //   6697: aload #67
    //   6699: astore #45
    //   6701: goto -> 6768
    //   6704: aload #51
    //   6706: astore #46
    //   6708: aload_1
    //   6709: astore #50
    //   6711: aload #67
    //   6713: astore #45
    //   6715: aload #63
    //   6717: astore_1
    //   6718: aload #71
    //   6720: astore #42
    //   6722: aload #73
    //   6724: astore #63
    //   6726: aload #74
    //   6728: astore #51
    //   6730: goto -> 6768
    //   6733: aload #51
    //   6735: astore #67
    //   6737: iload #4
    //   6739: istore #37
    //   6741: ldc_w ': '
    //   6744: astore #74
    //   6746: aload_1
    //   6747: astore #50
    //   6749: aload #42
    //   6751: astore #51
    //   6753: aload #46
    //   6755: astore #63
    //   6757: aload #67
    //   6759: astore #46
    //   6761: aload #74
    //   6763: astore_1
    //   6764: aload #71
    //   6766: astore #42
    //   6768: aload #47
    //   6770: invokevirtual size : ()I
    //   6773: ifle -> 6936
    //   6776: aload #47
    //   6778: astore #67
    //   6780: aload #67
    //   6782: aload #46
    //   6784: invokestatic sort : (Ljava/util/List;Ljava/util/Comparator;)V
    //   6787: aload_2
    //   6788: aload_3
    //   6789: invokevirtual print : (Ljava/lang/String;)V
    //   6792: aload_2
    //   6793: ldc_w '  All partial wake locks:'
    //   6796: invokevirtual println : (Ljava/lang/String;)V
    //   6799: iconst_0
    //   6800: istore #64
    //   6802: iload #64
    //   6804: aload #67
    //   6806: invokevirtual size : ()I
    //   6809: if_icmpge -> 6910
    //   6812: aload #67
    //   6814: iload #64
    //   6816: invokevirtual get : (I)Ljava/lang/Object;
    //   6819: checkcast android/os/BatteryStats$TimerEntry
    //   6822: astore #71
    //   6824: aload #34
    //   6826: iconst_0
    //   6827: invokevirtual setLength : (I)V
    //   6830: aload #34
    //   6832: ldc_w '  Wake lock '
    //   6835: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6838: pop
    //   6839: aload #34
    //   6841: aload #71
    //   6843: getfield mId : I
    //   6846: invokestatic formatUid : (Ljava/lang/StringBuilder;I)V
    //   6849: aload #34
    //   6851: aload #43
    //   6853: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6856: pop
    //   6857: aload #34
    //   6859: aload #71
    //   6861: getfield mName : Ljava/lang/String;
    //   6864: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6867: pop
    //   6868: aload #34
    //   6870: aload #71
    //   6872: getfield mTimer : Landroid/os/BatteryStats$Timer;
    //   6875: lload #18
    //   6877: aconst_null
    //   6878: iload #4
    //   6880: ldc_w ': '
    //   6883: invokestatic printWakeLock : (Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    //   6886: pop
    //   6887: aload #34
    //   6889: aload #42
    //   6891: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   6894: pop
    //   6895: aload_2
    //   6896: aload #34
    //   6898: invokevirtual toString : ()Ljava/lang/String;
    //   6901: invokevirtual println : (Ljava/lang/String;)V
    //   6904: iinc #64, 1
    //   6907: goto -> 6802
    //   6910: aload_1
    //   6911: astore #71
    //   6913: aload #42
    //   6915: astore_1
    //   6916: aload #67
    //   6918: invokevirtual clear : ()V
    //   6921: aload_2
    //   6922: invokevirtual println : ()V
    //   6925: aload #43
    //   6927: astore #42
    //   6929: aload #71
    //   6931: astore #67
    //   6933: goto -> 6946
    //   6936: aload_1
    //   6937: astore #67
    //   6939: aload #42
    //   6941: astore_1
    //   6942: aload #43
    //   6944: astore #42
    //   6946: aload_0
    //   6947: invokevirtual getWakeupReasonStats : ()Ljava/util/Map;
    //   6950: astore #43
    //   6952: aload #43
    //   6954: invokeinterface size : ()I
    //   6959: ifle -> 7210
    //   6962: aload_2
    //   6963: aload_3
    //   6964: invokevirtual print : (Ljava/lang/String;)V
    //   6967: aload_2
    //   6968: ldc_w '  All wakeup reasons:'
    //   6971: invokevirtual println : (Ljava/lang/String;)V
    //   6974: new java/util/ArrayList
    //   6977: dup
    //   6978: invokespecial <init> : ()V
    //   6981: astore #71
    //   6983: aload #43
    //   6985: invokeinterface entrySet : ()Ljava/util/Set;
    //   6990: invokeinterface iterator : ()Ljava/util/Iterator;
    //   6995: astore #43
    //   6997: aload #43
    //   6999: invokeinterface hasNext : ()Z
    //   7004: ifeq -> 7076
    //   7007: aload #43
    //   7009: invokeinterface next : ()Ljava/lang/Object;
    //   7014: checkcast java/util/Map$Entry
    //   7017: astore #73
    //   7019: aload #73
    //   7021: invokeinterface getValue : ()Ljava/lang/Object;
    //   7026: checkcast android/os/BatteryStats$Timer
    //   7029: astore #74
    //   7031: aload #73
    //   7033: invokeinterface getKey : ()Ljava/lang/Object;
    //   7038: checkcast java/lang/String
    //   7041: astore #73
    //   7043: new android/os/BatteryStats$TimerEntry
    //   7046: dup
    //   7047: aload #73
    //   7049: iconst_0
    //   7050: aload #74
    //   7052: aload #74
    //   7054: iload #37
    //   7056: invokevirtual getCountLocked : (I)I
    //   7059: i2l
    //   7060: invokespecial <init> : (Ljava/lang/String;ILandroid/os/BatteryStats$Timer;J)V
    //   7063: astore #74
    //   7065: aload #71
    //   7067: aload #74
    //   7069: invokevirtual add : (Ljava/lang/Object;)Z
    //   7072: pop
    //   7073: goto -> 6997
    //   7076: aload #71
    //   7078: aload #46
    //   7080: invokestatic sort : (Ljava/util/List;Ljava/util/Comparator;)V
    //   7083: iconst_0
    //   7084: istore #64
    //   7086: aload #71
    //   7088: astore #43
    //   7090: iload #64
    //   7092: aload #43
    //   7094: invokevirtual size : ()I
    //   7097: if_icmpge -> 7187
    //   7100: aload #43
    //   7102: iload #64
    //   7104: invokevirtual get : (I)Ljava/lang/Object;
    //   7107: checkcast android/os/BatteryStats$TimerEntry
    //   7110: astore #71
    //   7112: aload #34
    //   7114: iconst_0
    //   7115: invokevirtual setLength : (I)V
    //   7118: aload #34
    //   7120: aload #7
    //   7122: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7125: pop
    //   7126: aload #34
    //   7128: ldc_w '  Wakeup reason '
    //   7131: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7134: pop
    //   7135: aload #34
    //   7137: aload #71
    //   7139: getfield mName : Ljava/lang/String;
    //   7142: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7145: pop
    //   7146: aload #34
    //   7148: aload #71
    //   7150: getfield mTimer : Landroid/os/BatteryStats$Timer;
    //   7153: lload #18
    //   7155: aconst_null
    //   7156: iload #4
    //   7158: ldc_w ': '
    //   7161: invokestatic printWakeLock : (Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    //   7164: pop
    //   7165: aload #34
    //   7167: aload_1
    //   7168: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7171: pop
    //   7172: aload_2
    //   7173: aload #34
    //   7175: invokevirtual toString : ()Ljava/lang/String;
    //   7178: invokevirtual println : (Ljava/lang/String;)V
    //   7181: iinc #64, 1
    //   7184: goto -> 7090
    //   7187: aload_2
    //   7188: invokevirtual println : ()V
    //   7191: aload #63
    //   7193: astore #43
    //   7195: aload #51
    //   7197: astore #46
    //   7199: aload #45
    //   7201: astore #51
    //   7203: aload #67
    //   7205: astore #45
    //   7207: goto -> 7271
    //   7210: aload #63
    //   7212: astore #43
    //   7214: aload #51
    //   7216: astore #46
    //   7218: aload #45
    //   7220: astore #51
    //   7222: aload #67
    //   7224: astore #45
    //   7226: goto -> 7271
    //   7229: ldc_w ': '
    //   7232: astore #67
    //   7234: aload #45
    //   7236: astore #51
    //   7238: ldc_w ' realtime'
    //   7241: astore #63
    //   7243: aload_1
    //   7244: astore #50
    //   7246: aload #43
    //   7248: astore_1
    //   7249: iload #4
    //   7251: istore #37
    //   7253: aload #46
    //   7255: astore #43
    //   7257: aload #67
    //   7259: astore #45
    //   7261: aload #42
    //   7263: astore #46
    //   7265: aload_1
    //   7266: astore #42
    //   7268: aload #63
    //   7270: astore_1
    //   7271: aload_0
    //   7272: invokevirtual getKernelMemoryStats : ()Landroid/util/LongSparseArray;
    //   7275: astore #63
    //   7277: aload #63
    //   7279: invokevirtual size : ()I
    //   7282: ifle -> 7387
    //   7285: aload_2
    //   7286: ldc_w '  Memory Stats'
    //   7289: invokevirtual println : (Ljava/lang/String;)V
    //   7292: iconst_0
    //   7293: istore #64
    //   7295: iload #64
    //   7297: aload #63
    //   7299: invokevirtual size : ()I
    //   7302: if_icmpge -> 7380
    //   7305: aload #34
    //   7307: iconst_0
    //   7308: invokevirtual setLength : (I)V
    //   7311: aload #34
    //   7313: ldc_w '  Bandwidth '
    //   7316: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7319: pop
    //   7320: aload #34
    //   7322: aload #63
    //   7324: iload #64
    //   7326: invokevirtual keyAt : (I)J
    //   7329: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   7332: pop
    //   7333: aload #34
    //   7335: ldc_w ' Time '
    //   7338: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7341: pop
    //   7342: aload #34
    //   7344: aload #63
    //   7346: iload #64
    //   7348: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   7351: checkcast android/os/BatteryStats$Timer
    //   7354: lload #18
    //   7356: iload #37
    //   7358: invokevirtual getTotalTimeLocked : (JI)J
    //   7361: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   7364: pop
    //   7365: aload_2
    //   7366: aload #34
    //   7368: invokevirtual toString : ()Ljava/lang/String;
    //   7371: invokevirtual println : (Ljava/lang/String;)V
    //   7374: iinc #64, 1
    //   7377: goto -> 7295
    //   7380: aload_2
    //   7381: invokevirtual println : ()V
    //   7384: goto -> 7387
    //   7387: aload_0
    //   7388: invokevirtual getRpmStats : ()Ljava/util/Map;
    //   7391: astore #67
    //   7393: aload #67
    //   7395: invokeinterface size : ()I
    //   7400: ifle -> 7657
    //   7403: aload_2
    //   7404: aload_3
    //   7405: invokevirtual print : (Ljava/lang/String;)V
    //   7408: aload_2
    //   7409: ldc_w '  Resource Power Manager Stats'
    //   7412: invokevirtual println : (Ljava/lang/String;)V
    //   7415: aload #67
    //   7417: invokeinterface size : ()I
    //   7422: ifle -> 7585
    //   7425: aload #67
    //   7427: invokeinterface entrySet : ()Ljava/util/Set;
    //   7432: invokeinterface iterator : ()Ljava/util/Iterator;
    //   7437: astore #71
    //   7439: aload #68
    //   7441: astore #67
    //   7443: aload #71
    //   7445: invokeinterface hasNext : ()Z
    //   7450: ifeq -> 7512
    //   7453: aload #71
    //   7455: invokeinterface next : ()Ljava/lang/Object;
    //   7460: checkcast java/util/Map$Entry
    //   7463: astore #73
    //   7465: aload #73
    //   7467: invokeinterface getKey : ()Ljava/lang/Object;
    //   7472: checkcast java/lang/String
    //   7475: astore #74
    //   7477: aload #73
    //   7479: invokeinterface getValue : ()Ljava/lang/Object;
    //   7484: checkcast android/os/BatteryStats$Timer
    //   7487: astore #73
    //   7489: aload_2
    //   7490: aload #34
    //   7492: aload #73
    //   7494: lload #18
    //   7496: iload #4
    //   7498: aload_3
    //   7499: aload #74
    //   7501: invokestatic printTimer : (Ljava/io/PrintWriter;Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JILjava/lang/String;Ljava/lang/String;)Z
    //   7504: pop
    //   7505: iload #4
    //   7507: istore #37
    //   7509: goto -> 7443
    //   7512: aload #34
    //   7514: astore #67
    //   7516: aload #7
    //   7518: astore #34
    //   7520: aload #49
    //   7522: astore #7
    //   7524: aload #42
    //   7526: astore #65
    //   7528: aload #46
    //   7530: astore #49
    //   7532: lload #24
    //   7534: lstore #20
    //   7536: lload #22
    //   7538: lstore #24
    //   7540: aload #63
    //   7542: astore #46
    //   7544: iload #37
    //   7546: istore #36
    //   7548: aload #67
    //   7550: astore #42
    //   7552: aload #35
    //   7554: astore #63
    //   7556: aload #65
    //   7558: astore #67
    //   7560: aload #46
    //   7562: astore #65
    //   7564: lload #18
    //   7566: lstore #22
    //   7568: aload #43
    //   7570: astore #46
    //   7572: aload_1
    //   7573: astore #35
    //   7575: lload #20
    //   7577: lstore #18
    //   7579: aload #7
    //   7581: astore_1
    //   7582: goto -> 7643
    //   7585: aload_1
    //   7586: astore #67
    //   7588: aload #49
    //   7590: astore_1
    //   7591: aload #46
    //   7593: astore #49
    //   7595: lload #22
    //   7597: lstore #20
    //   7599: lload #18
    //   7601: lstore #22
    //   7603: aload #63
    //   7605: astore #65
    //   7607: aload #35
    //   7609: astore #63
    //   7611: lload #24
    //   7613: lstore #18
    //   7615: aload #67
    //   7617: astore #35
    //   7619: lload #20
    //   7621: lstore #24
    //   7623: aload #43
    //   7625: astore #46
    //   7627: aload #42
    //   7629: astore #67
    //   7631: aload #34
    //   7633: astore #42
    //   7635: iload #37
    //   7637: istore #36
    //   7639: aload #7
    //   7641: astore #34
    //   7643: aload_2
    //   7644: invokevirtual println : ()V
    //   7647: aload #42
    //   7649: astore #43
    //   7651: aload_1
    //   7652: astore #42
    //   7654: goto -> 7720
    //   7657: aload #49
    //   7659: astore #65
    //   7661: aload #42
    //   7663: astore #67
    //   7665: aload #46
    //   7667: astore #49
    //   7669: lload #18
    //   7671: lstore #20
    //   7673: aload #63
    //   7675: astore #66
    //   7677: aload #35
    //   7679: astore #63
    //   7681: aload #65
    //   7683: astore #42
    //   7685: lload #24
    //   7687: lstore #18
    //   7689: aload_1
    //   7690: astore #35
    //   7692: lload #22
    //   7694: lstore #24
    //   7696: aload #43
    //   7698: astore #46
    //   7700: lload #20
    //   7702: lstore #22
    //   7704: aload #66
    //   7706: astore #65
    //   7708: aload #34
    //   7710: astore #43
    //   7712: iload #37
    //   7714: istore #36
    //   7716: aload #7
    //   7718: astore #34
    //   7720: aload #51
    //   7722: astore_1
    //   7723: aload_0
    //   7724: invokevirtual getCpuFreqs : ()[J
    //   7727: astore #66
    //   7729: aload #66
    //   7731: ifnull -> 7821
    //   7734: aload #43
    //   7736: iconst_0
    //   7737: invokevirtual setLength : (I)V
    //   7740: aload #43
    //   7742: ldc_w '  CPU freqs:'
    //   7745: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7748: pop
    //   7749: iconst_0
    //   7750: istore #37
    //   7752: iload #37
    //   7754: aload #66
    //   7756: arraylength
    //   7757: if_icmpge -> 7805
    //   7760: new java/lang/StringBuilder
    //   7763: dup
    //   7764: invokespecial <init> : ()V
    //   7767: astore #7
    //   7769: aload #7
    //   7771: aload #67
    //   7773: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7776: pop
    //   7777: aload #7
    //   7779: aload #66
    //   7781: iload #37
    //   7783: laload
    //   7784: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   7787: pop
    //   7788: aload #43
    //   7790: aload #7
    //   7792: invokevirtual toString : ()Ljava/lang/String;
    //   7795: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   7798: pop
    //   7799: iinc #37, 1
    //   7802: goto -> 7752
    //   7805: aload_2
    //   7806: aload #43
    //   7808: invokevirtual toString : ()Ljava/lang/String;
    //   7811: invokevirtual println : (Ljava/lang/String;)V
    //   7814: aload_2
    //   7815: invokevirtual println : ()V
    //   7818: goto -> 7821
    //   7821: aload_2
    //   7822: astore #71
    //   7824: iconst_0
    //   7825: istore #60
    //   7827: lload #18
    //   7829: lstore #8
    //   7831: aload #45
    //   7833: astore #7
    //   7835: aload #35
    //   7837: astore #51
    //   7839: aload #50
    //   7841: astore #35
    //   7843: lload #24
    //   7845: lstore #30
    //   7847: iload #44
    //   7849: istore #37
    //   7851: lload #12
    //   7853: lstore #18
    //   7855: lload #14
    //   7857: lstore #24
    //   7859: aload #71
    //   7861: astore #50
    //   7863: aload #67
    //   7865: astore #45
    //   7867: iload #60
    //   7869: iload #37
    //   7871: if_icmpge -> 14628
    //   7874: aload #63
    //   7876: iload #60
    //   7878: invokevirtual keyAt : (I)I
    //   7881: istore #44
    //   7883: iload #5
    //   7885: iflt -> 7977
    //   7888: iload #44
    //   7890: iload #5
    //   7892: if_icmpeq -> 7977
    //   7895: iload #44
    //   7897: sipush #1000
    //   7900: if_icmpeq -> 7977
    //   7903: aload #45
    //   7905: astore #67
    //   7907: iload #37
    //   7909: istore #44
    //   7911: lload #18
    //   7913: lstore #16
    //   7915: lload #22
    //   7917: lstore #18
    //   7919: aload #46
    //   7921: astore #45
    //   7923: aload #42
    //   7925: astore #45
    //   7927: aload_1
    //   7928: astore #71
    //   7930: iload #36
    //   7932: istore #37
    //   7934: aload #43
    //   7936: astore_1
    //   7937: aload #50
    //   7939: astore_1
    //   7940: aload #7
    //   7942: astore #50
    //   7944: aload_1
    //   7945: astore #7
    //   7947: lload #24
    //   7949: lstore #20
    //   7951: aload #49
    //   7953: astore_1
    //   7954: aload #35
    //   7956: astore #45
    //   7958: aload #71
    //   7960: astore #49
    //   7962: iload #44
    //   7964: istore #36
    //   7966: lload #16
    //   7968: lstore #24
    //   7970: aload #67
    //   7972: astore #35
    //   7974: goto -> 14548
    //   7977: aload #63
    //   7979: iload #60
    //   7981: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   7984: checkcast android/os/BatteryStats$Uid
    //   7987: astore #34
    //   7989: aload_2
    //   7990: aload_3
    //   7991: invokevirtual print : (Ljava/lang/String;)V
    //   7994: aload #50
    //   7996: ldc_w '  '
    //   7999: invokevirtual print : (Ljava/lang/String;)V
    //   8002: aload #50
    //   8004: iload #44
    //   8006: invokestatic formatUid : (Ljava/io/PrintWriter;I)V
    //   8009: aload #50
    //   8011: ldc_w ':'
    //   8014: invokevirtual println : (Ljava/lang/String;)V
    //   8017: aload #34
    //   8019: iconst_0
    //   8020: iload #36
    //   8022: invokevirtual getNetworkActivityBytes : (II)J
    //   8025: lstore #69
    //   8027: aload #34
    //   8029: iconst_1
    //   8030: iload #36
    //   8032: invokevirtual getNetworkActivityBytes : (II)J
    //   8035: lstore #14
    //   8037: aload #34
    //   8039: iconst_2
    //   8040: iload #36
    //   8042: invokevirtual getNetworkActivityBytes : (II)J
    //   8045: lstore #61
    //   8047: aload #34
    //   8049: iconst_3
    //   8050: iload #36
    //   8052: invokevirtual getNetworkActivityBytes : (II)J
    //   8055: lstore #52
    //   8057: aload #34
    //   8059: iconst_4
    //   8060: iload #36
    //   8062: invokevirtual getNetworkActivityBytes : (II)J
    //   8065: lstore #12
    //   8067: aload #34
    //   8069: iconst_5
    //   8070: iload #36
    //   8072: invokevirtual getNetworkActivityBytes : (II)J
    //   8075: lstore #28
    //   8077: aload #34
    //   8079: iconst_0
    //   8080: iload #36
    //   8082: invokevirtual getNetworkActivityPackets : (II)J
    //   8085: lstore #75
    //   8087: aload #34
    //   8089: iconst_1
    //   8090: iload #36
    //   8092: invokevirtual getNetworkActivityPackets : (II)J
    //   8095: lstore #77
    //   8097: aload #34
    //   8099: iconst_2
    //   8100: iload #36
    //   8102: invokevirtual getNetworkActivityPackets : (II)J
    //   8105: lstore #10
    //   8107: aload #34
    //   8109: iconst_3
    //   8110: iload #36
    //   8112: invokevirtual getNetworkActivityPackets : (II)J
    //   8115: lstore #54
    //   8117: aload #34
    //   8119: iload #36
    //   8121: invokevirtual getMobileRadioActiveTime : (I)J
    //   8124: lstore #58
    //   8126: aload #34
    //   8128: iload #36
    //   8130: invokevirtual getMobileRadioActiveCount : (I)I
    //   8133: istore #64
    //   8135: aload #34
    //   8137: lload #22
    //   8139: iload #36
    //   8141: invokevirtual getFullWifiLockTime : (JI)J
    //   8144: lstore #38
    //   8146: aload #34
    //   8148: lload #22
    //   8150: iload #36
    //   8152: invokevirtual getWifiScanTime : (JI)J
    //   8155: lstore #40
    //   8157: aload #34
    //   8159: iload #36
    //   8161: invokevirtual getWifiScanCount : (I)I
    //   8164: istore #44
    //   8166: aload #34
    //   8168: iload #36
    //   8170: invokevirtual getWifiScanBackgroundCount : (I)I
    //   8173: istore #48
    //   8175: aload #34
    //   8177: lload #22
    //   8179: invokevirtual getWifiScanActualTime : (J)J
    //   8182: lstore #32
    //   8184: aload #34
    //   8186: lload #22
    //   8188: invokevirtual getWifiScanBackgroundTime : (J)J
    //   8191: lstore #20
    //   8193: aload #34
    //   8195: lload #22
    //   8197: iload #36
    //   8199: invokevirtual getWifiRunningTime : (JI)J
    //   8202: lstore #16
    //   8204: aload #34
    //   8206: iload #36
    //   8208: invokevirtual getMobileRadioApWakeupCount : (I)J
    //   8211: lstore #56
    //   8213: aload #34
    //   8215: iload #36
    //   8217: invokevirtual getWifiRadioApWakeupCount : (I)J
    //   8220: lstore #26
    //   8222: lload #69
    //   8224: lconst_0
    //   8225: lcmp
    //   8226: ifgt -> 8256
    //   8229: lload #14
    //   8231: lconst_0
    //   8232: lcmp
    //   8233: ifgt -> 8256
    //   8236: lload #75
    //   8238: lconst_0
    //   8239: lcmp
    //   8240: ifgt -> 8256
    //   8243: lload #77
    //   8245: lconst_0
    //   8246: lcmp
    //   8247: ifle -> 8253
    //   8250: goto -> 8256
    //   8253: goto -> 8328
    //   8256: aload_2
    //   8257: aload_3
    //   8258: invokevirtual print : (Ljava/lang/String;)V
    //   8261: aload_2
    //   8262: ldc_w '    Mobile network: '
    //   8265: invokevirtual print : (Ljava/lang/String;)V
    //   8268: aload_2
    //   8269: aload_0
    //   8270: lload #69
    //   8272: invokevirtual formatBytesLocked : (J)Ljava/lang/String;
    //   8275: invokevirtual print : (Ljava/lang/String;)V
    //   8278: aload_2
    //   8279: ldc_w ' received, '
    //   8282: invokevirtual print : (Ljava/lang/String;)V
    //   8285: aload_2
    //   8286: aload_0
    //   8287: lload #14
    //   8289: invokevirtual formatBytesLocked : (J)Ljava/lang/String;
    //   8292: invokevirtual print : (Ljava/lang/String;)V
    //   8295: aload_2
    //   8296: ldc_w ' sent (packets '
    //   8299: invokevirtual print : (Ljava/lang/String;)V
    //   8302: aload_2
    //   8303: lload #75
    //   8305: invokevirtual print : (J)V
    //   8308: aload_2
    //   8309: ldc_w ' received, '
    //   8312: invokevirtual print : (Ljava/lang/String;)V
    //   8315: aload_2
    //   8316: lload #77
    //   8318: invokevirtual print : (J)V
    //   8321: aload_2
    //   8322: ldc_w ' sent)'
    //   8325: invokevirtual println : (Ljava/lang/String;)V
    //   8328: lload #58
    //   8330: lconst_0
    //   8331: lcmp
    //   8332: ifgt -> 8346
    //   8335: iload #64
    //   8337: ifle -> 8343
    //   8340: goto -> 8346
    //   8343: goto -> 8492
    //   8346: aload #43
    //   8348: iconst_0
    //   8349: invokevirtual setLength : (I)V
    //   8352: aload #43
    //   8354: aload_3
    //   8355: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8358: pop
    //   8359: aload #43
    //   8361: ldc_w '    Mobile radio active: '
    //   8364: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8367: pop
    //   8368: aload #43
    //   8370: lload #58
    //   8372: ldc2_w 1000
    //   8375: ldiv
    //   8376: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   8379: aload #43
    //   8381: aload #49
    //   8383: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8386: pop
    //   8387: aload #43
    //   8389: aload_0
    //   8390: lload #58
    //   8392: lload #30
    //   8394: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   8397: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8400: pop
    //   8401: aload #43
    //   8403: aload #42
    //   8405: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8408: pop
    //   8409: aload #43
    //   8411: iload #64
    //   8413: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   8416: pop
    //   8417: aload #43
    //   8419: aload #46
    //   8421: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8424: pop
    //   8425: lload #75
    //   8427: lload #77
    //   8429: ladd
    //   8430: lstore #14
    //   8432: lload #14
    //   8434: lconst_0
    //   8435: lcmp
    //   8436: ifne -> 8445
    //   8439: lconst_1
    //   8440: lstore #14
    //   8442: goto -> 8445
    //   8445: aload #43
    //   8447: ldc_w ' @ '
    //   8450: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8453: pop
    //   8454: aload #43
    //   8456: lload #58
    //   8458: ldc2_w 1000
    //   8461: ldiv
    //   8462: l2d
    //   8463: lload #14
    //   8465: l2d
    //   8466: ddiv
    //   8467: invokestatic makemAh : (D)Ljava/lang/String;
    //   8470: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8473: pop
    //   8474: aload #43
    //   8476: ldc_w ' mspp'
    //   8479: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8482: pop
    //   8483: aload_2
    //   8484: aload #43
    //   8486: invokevirtual toString : ()Ljava/lang/String;
    //   8489: invokevirtual println : (Ljava/lang/String;)V
    //   8492: aload #34
    //   8494: astore #50
    //   8496: lload #56
    //   8498: lconst_0
    //   8499: lcmp
    //   8500: ifle -> 8545
    //   8503: aload #43
    //   8505: iconst_0
    //   8506: invokevirtual setLength : (I)V
    //   8509: aload #43
    //   8511: aload_3
    //   8512: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8515: pop
    //   8516: aload #43
    //   8518: ldc_w '    Mobile radio AP wakeups: '
    //   8521: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8524: pop
    //   8525: aload #43
    //   8527: lload #56
    //   8529: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   8532: pop
    //   8533: aload_2
    //   8534: aload #43
    //   8536: invokevirtual toString : ()Ljava/lang/String;
    //   8539: invokevirtual println : (Ljava/lang/String;)V
    //   8542: goto -> 8545
    //   8545: new java/lang/StringBuilder
    //   8548: dup
    //   8549: invokespecial <init> : ()V
    //   8552: astore #34
    //   8554: aload #34
    //   8556: aload_3
    //   8557: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8560: pop
    //   8561: aload #34
    //   8563: ldc_w '  '
    //   8566: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8569: pop
    //   8570: aload #34
    //   8572: invokevirtual toString : ()Ljava/lang/String;
    //   8575: astore #67
    //   8577: aload #50
    //   8579: invokevirtual getModemControllerActivity : ()Landroid/os/BatteryStats$ControllerActivityCounter;
    //   8582: astore #34
    //   8584: aload #43
    //   8586: astore #79
    //   8588: aload_0
    //   8589: aload_2
    //   8590: aload #43
    //   8592: aload #67
    //   8594: ldc 'Cellular'
    //   8596: aload #34
    //   8598: iload #4
    //   8600: invokespecial printControllerActivityIfInteresting : (Ljava/io/PrintWriter;Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$ControllerActivityCounter;I)V
    //   8603: lload #61
    //   8605: lconst_0
    //   8606: lcmp
    //   8607: ifgt -> 8641
    //   8610: lload #52
    //   8612: lconst_0
    //   8613: lcmp
    //   8614: ifgt -> 8641
    //   8617: lload #10
    //   8619: lconst_0
    //   8620: lcmp
    //   8621: ifgt -> 8641
    //   8624: lload #54
    //   8626: lconst_0
    //   8627: lcmp
    //   8628: ifle -> 8634
    //   8631: goto -> 8641
    //   8634: lload #10
    //   8636: lstore #14
    //   8638: goto -> 8717
    //   8641: aload_2
    //   8642: aload_3
    //   8643: invokevirtual print : (Ljava/lang/String;)V
    //   8646: aload_2
    //   8647: ldc_w '    Wi-Fi network: '
    //   8650: invokevirtual print : (Ljava/lang/String;)V
    //   8653: aload_2
    //   8654: aload_0
    //   8655: lload #61
    //   8657: invokevirtual formatBytesLocked : (J)Ljava/lang/String;
    //   8660: invokevirtual print : (Ljava/lang/String;)V
    //   8663: aload_2
    //   8664: ldc_w ' received, '
    //   8667: invokevirtual print : (Ljava/lang/String;)V
    //   8670: aload_2
    //   8671: aload_0
    //   8672: lload #52
    //   8674: invokevirtual formatBytesLocked : (J)Ljava/lang/String;
    //   8677: invokevirtual print : (Ljava/lang/String;)V
    //   8680: aload_2
    //   8681: ldc_w ' sent (packets '
    //   8684: invokevirtual print : (Ljava/lang/String;)V
    //   8687: aload_2
    //   8688: lload #10
    //   8690: invokevirtual print : (J)V
    //   8693: aload_2
    //   8694: ldc_w ' received, '
    //   8697: invokevirtual print : (Ljava/lang/String;)V
    //   8700: lload #10
    //   8702: lstore #14
    //   8704: aload_2
    //   8705: lload #54
    //   8707: invokevirtual print : (J)V
    //   8710: aload_2
    //   8711: ldc_w ' sent)'
    //   8714: invokevirtual println : (Ljava/lang/String;)V
    //   8717: lload #38
    //   8719: lconst_0
    //   8720: lcmp
    //   8721: ifne -> 8785
    //   8724: lload #40
    //   8726: lconst_0
    //   8727: lcmp
    //   8728: ifne -> 8785
    //   8731: iload #44
    //   8733: ifne -> 8785
    //   8736: iload #48
    //   8738: ifne -> 8785
    //   8741: lload #32
    //   8743: lconst_0
    //   8744: lcmp
    //   8745: ifne -> 8782
    //   8748: lload #20
    //   8750: lstore #10
    //   8752: lload #10
    //   8754: lconst_0
    //   8755: lcmp
    //   8756: ifne -> 8779
    //   8759: lload #16
    //   8761: lconst_0
    //   8762: lcmp
    //   8763: ifeq -> 8769
    //   8766: goto -> 8785
    //   8769: lload #16
    //   8771: lstore #10
    //   8773: aload_2
    //   8774: astore #34
    //   8776: goto -> 9171
    //   8779: goto -> 8785
    //   8782: goto -> 8785
    //   8785: aload #79
    //   8787: iconst_0
    //   8788: invokevirtual setLength : (I)V
    //   8791: aload #79
    //   8793: aload_3
    //   8794: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8797: pop
    //   8798: aload #79
    //   8800: ldc_w '    Wifi Running: '
    //   8803: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8806: pop
    //   8807: aload #79
    //   8809: lload #16
    //   8811: ldc2_w 1000
    //   8814: ldiv
    //   8815: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   8818: aload #79
    //   8820: aload #49
    //   8822: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8825: pop
    //   8826: aload #79
    //   8828: aload_0
    //   8829: lload #16
    //   8831: lload #8
    //   8833: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   8836: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8839: pop
    //   8840: aload #79
    //   8842: ldc_w ')\\n'
    //   8845: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8848: pop
    //   8849: aload #79
    //   8851: aload_3
    //   8852: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8855: pop
    //   8856: aload #79
    //   8858: ldc_w '    Full Wifi Lock: '
    //   8861: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8864: pop
    //   8865: lload #16
    //   8867: lstore #10
    //   8869: aload #79
    //   8871: lload #38
    //   8873: ldc2_w 1000
    //   8876: ldiv
    //   8877: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   8880: aload #79
    //   8882: aload #49
    //   8884: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8887: pop
    //   8888: aload #79
    //   8890: aload_0
    //   8891: lload #38
    //   8893: lload #8
    //   8895: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   8898: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8901: pop
    //   8902: aload #79
    //   8904: ldc_w ')\\n'
    //   8907: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8910: pop
    //   8911: aload #79
    //   8913: aload_3
    //   8914: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8917: pop
    //   8918: aload #79
    //   8920: ldc_w '    Wifi Scan (blamed): '
    //   8923: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8926: pop
    //   8927: aload #79
    //   8929: lload #40
    //   8931: ldc2_w 1000
    //   8934: ldiv
    //   8935: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   8938: aload #79
    //   8940: aload #49
    //   8942: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8945: pop
    //   8946: aload #79
    //   8948: aload_0
    //   8949: lload #40
    //   8951: lload #8
    //   8953: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   8956: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8959: pop
    //   8960: aload #79
    //   8962: aload #42
    //   8964: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8967: pop
    //   8968: aload #79
    //   8970: iload #44
    //   8972: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   8975: pop
    //   8976: aload #79
    //   8978: ldc_w 'x\\n'
    //   8981: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8984: pop
    //   8985: aload #79
    //   8987: aload_3
    //   8988: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   8991: pop
    //   8992: aload #79
    //   8994: ldc_w '    Wifi Scan (actual): '
    //   8997: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9000: pop
    //   9001: aload #79
    //   9003: lload #32
    //   9005: ldc2_w 1000
    //   9008: ldiv
    //   9009: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   9012: aload #79
    //   9014: aload #49
    //   9016: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9019: pop
    //   9020: aload_0
    //   9021: lload #22
    //   9023: iconst_0
    //   9024: invokevirtual computeBatteryRealtime : (JI)J
    //   9027: lstore #10
    //   9029: aload #79
    //   9031: aload_0
    //   9032: lload #32
    //   9034: lload #10
    //   9036: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   9039: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9042: pop
    //   9043: aload #79
    //   9045: aload #42
    //   9047: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9050: pop
    //   9051: aload #79
    //   9053: iload #44
    //   9055: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   9058: pop
    //   9059: aload #79
    //   9061: ldc_w 'x\\n'
    //   9064: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9067: pop
    //   9068: aload #79
    //   9070: aload_3
    //   9071: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9074: pop
    //   9075: aload #79
    //   9077: ldc_w '    Background Wifi Scan: '
    //   9080: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9083: pop
    //   9084: aload #79
    //   9086: lload #20
    //   9088: ldc2_w 1000
    //   9091: ldiv
    //   9092: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   9095: aload #79
    //   9097: aload #49
    //   9099: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9102: pop
    //   9103: aload_0
    //   9104: lload #22
    //   9106: iconst_0
    //   9107: invokevirtual computeBatteryRealtime : (JI)J
    //   9110: lstore #14
    //   9112: lload #20
    //   9114: lstore #10
    //   9116: aload #79
    //   9118: aload_0
    //   9119: lload #10
    //   9121: lload #14
    //   9123: invokevirtual formatRatioLocked : (JJ)Ljava/lang/String;
    //   9126: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9129: pop
    //   9130: aload #79
    //   9132: aload #42
    //   9134: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9137: pop
    //   9138: aload #79
    //   9140: iload #48
    //   9142: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   9145: pop
    //   9146: aload #79
    //   9148: aload #46
    //   9150: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9153: pop
    //   9154: aload #79
    //   9156: invokevirtual toString : ()Ljava/lang/String;
    //   9159: astore #67
    //   9161: aload_2
    //   9162: astore #34
    //   9164: aload #34
    //   9166: aload #67
    //   9168: invokevirtual println : (Ljava/lang/String;)V
    //   9171: aload_2
    //   9172: astore #71
    //   9174: lload #26
    //   9176: lconst_0
    //   9177: lcmp
    //   9178: ifle -> 9221
    //   9181: aload #79
    //   9183: iconst_0
    //   9184: invokevirtual setLength : (I)V
    //   9187: aload #79
    //   9189: aload_3
    //   9190: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9193: pop
    //   9194: aload #79
    //   9196: ldc_w '    WiFi AP wakeups: '
    //   9199: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9202: pop
    //   9203: aload #79
    //   9205: lload #26
    //   9207: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   9210: pop
    //   9211: aload #71
    //   9213: aload #79
    //   9215: invokevirtual toString : ()Ljava/lang/String;
    //   9218: invokevirtual println : (Ljava/lang/String;)V
    //   9221: new java/lang/StringBuilder
    //   9224: dup
    //   9225: invokespecial <init> : ()V
    //   9228: astore #34
    //   9230: aload #34
    //   9232: aload_3
    //   9233: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9236: pop
    //   9237: aload #34
    //   9239: ldc_w '  '
    //   9242: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9245: pop
    //   9246: aload #34
    //   9248: invokevirtual toString : ()Ljava/lang/String;
    //   9251: astore #34
    //   9253: aload #50
    //   9255: invokevirtual getWifiControllerActivity : ()Landroid/os/BatteryStats$ControllerActivityCounter;
    //   9258: astore #67
    //   9260: aload_0
    //   9261: aload_2
    //   9262: aload #79
    //   9264: aload #34
    //   9266: ldc_w 'WiFi'
    //   9269: aload #67
    //   9271: iload #4
    //   9273: invokespecial printControllerActivityIfInteresting : (Ljava/io/PrintWriter;Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$ControllerActivityCounter;I)V
    //   9276: lload #12
    //   9278: lconst_0
    //   9279: lcmp
    //   9280: ifgt -> 9296
    //   9283: lload #28
    //   9285: lconst_0
    //   9286: lcmp
    //   9287: ifle -> 9293
    //   9290: goto -> 9296
    //   9293: goto -> 9347
    //   9296: aload_2
    //   9297: aload_3
    //   9298: invokevirtual print : (Ljava/lang/String;)V
    //   9301: aload #71
    //   9303: ldc_w '    Bluetooth network: '
    //   9306: invokevirtual print : (Ljava/lang/String;)V
    //   9309: aload #71
    //   9311: aload_0
    //   9312: lload #12
    //   9314: invokevirtual formatBytesLocked : (J)Ljava/lang/String;
    //   9317: invokevirtual print : (Ljava/lang/String;)V
    //   9320: aload #71
    //   9322: ldc_w ' received, '
    //   9325: invokevirtual print : (Ljava/lang/String;)V
    //   9328: aload #71
    //   9330: aload_0
    //   9331: lload #28
    //   9333: invokevirtual formatBytesLocked : (J)Ljava/lang/String;
    //   9336: invokevirtual print : (Ljava/lang/String;)V
    //   9339: aload #71
    //   9341: ldc_w ' sent'
    //   9344: invokevirtual println : (Ljava/lang/String;)V
    //   9347: aload #50
    //   9349: invokevirtual getBluetoothScanTimer : ()Landroid/os/BatteryStats$Timer;
    //   9352: astore #34
    //   9354: ldc_w '\\n'
    //   9357: astore #47
    //   9359: aload #34
    //   9361: ifnull -> 10134
    //   9364: aload #34
    //   9366: lload #22
    //   9368: iload #4
    //   9370: invokevirtual getTotalTimeLocked : (JI)J
    //   9373: ldc2_w 500
    //   9376: ladd
    //   9377: ldc2_w 1000
    //   9380: ldiv
    //   9381: lstore #28
    //   9383: lload #28
    //   9385: lconst_0
    //   9386: lcmp
    //   9387: ifeq -> 10131
    //   9390: aload #34
    //   9392: iload #4
    //   9394: invokevirtual getCountLocked : (I)I
    //   9397: istore #64
    //   9399: aload #50
    //   9401: invokevirtual getBluetoothScanBackgroundTimer : ()Landroid/os/BatteryStats$Timer;
    //   9404: astore #74
    //   9406: aload #74
    //   9408: ifnull -> 9423
    //   9411: aload #74
    //   9413: iload #4
    //   9415: invokevirtual getCountLocked : (I)I
    //   9418: istore #36
    //   9420: goto -> 9426
    //   9423: iconst_0
    //   9424: istore #36
    //   9426: aload #34
    //   9428: lload #18
    //   9430: invokevirtual getTotalDurationMsLocked : (J)J
    //   9433: lstore #26
    //   9435: aload #74
    //   9437: ifnull -> 9452
    //   9440: aload #74
    //   9442: lload #18
    //   9444: invokevirtual getTotalDurationMsLocked : (J)J
    //   9447: lstore #20
    //   9449: goto -> 9455
    //   9452: lconst_0
    //   9453: lstore #20
    //   9455: aload #50
    //   9457: invokevirtual getBluetoothScanResultCounter : ()Landroid/os/BatteryStats$Counter;
    //   9460: ifnull -> 9478
    //   9463: aload #50
    //   9465: invokevirtual getBluetoothScanResultCounter : ()Landroid/os/BatteryStats$Counter;
    //   9468: iload #4
    //   9470: invokevirtual getCountLocked : (I)I
    //   9473: istore #44
    //   9475: goto -> 9481
    //   9478: iconst_0
    //   9479: istore #44
    //   9481: aload #50
    //   9483: invokevirtual getBluetoothScanResultBgCounter : ()Landroid/os/BatteryStats$Counter;
    //   9486: ifnull -> 9504
    //   9489: aload #50
    //   9491: invokevirtual getBluetoothScanResultBgCounter : ()Landroid/os/BatteryStats$Counter;
    //   9494: iload #4
    //   9496: invokevirtual getCountLocked : (I)I
    //   9499: istore #48
    //   9501: goto -> 9507
    //   9504: iconst_0
    //   9505: istore #48
    //   9507: aload #50
    //   9509: invokevirtual getBluetoothUnoptimizedScanTimer : ()Landroid/os/BatteryStats$Timer;
    //   9512: astore #67
    //   9514: aload #67
    //   9516: ifnull -> 9531
    //   9519: aload #67
    //   9521: lload #18
    //   9523: invokevirtual getTotalDurationMsLocked : (J)J
    //   9526: lstore #16
    //   9528: goto -> 9534
    //   9531: lconst_0
    //   9532: lstore #16
    //   9534: aload #67
    //   9536: ifnull -> 9551
    //   9539: aload #67
    //   9541: lload #18
    //   9543: invokevirtual getMaxDurationMsLocked : (J)J
    //   9546: lstore #10
    //   9548: goto -> 9554
    //   9551: lconst_0
    //   9552: lstore #10
    //   9554: aload #50
    //   9556: invokevirtual getBluetoothUnoptimizedScanBackgroundTimer : ()Landroid/os/BatteryStats$Timer;
    //   9559: astore #71
    //   9561: aload #71
    //   9563: ifnull -> 9578
    //   9566: aload #71
    //   9568: lload #18
    //   9570: invokevirtual getTotalDurationMsLocked : (J)J
    //   9573: lstore #14
    //   9575: goto -> 9581
    //   9578: lconst_0
    //   9579: lstore #14
    //   9581: aload #71
    //   9583: ifnull -> 9598
    //   9586: aload #71
    //   9588: lload #18
    //   9590: invokevirtual getMaxDurationMsLocked : (J)J
    //   9593: lstore #12
    //   9595: goto -> 9601
    //   9598: lconst_0
    //   9599: lstore #12
    //   9601: aload #79
    //   9603: iconst_0
    //   9604: invokevirtual setLength : (I)V
    //   9607: lload #26
    //   9609: lload #28
    //   9611: lcmp
    //   9612: ifeq -> 9692
    //   9615: aload #79
    //   9617: aload_3
    //   9618: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9621: pop
    //   9622: aload #79
    //   9624: ldc_w '    Bluetooth Scan (total blamed realtime): '
    //   9627: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9630: pop
    //   9631: aload #79
    //   9633: lload #28
    //   9635: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   9638: aload #79
    //   9640: aload #35
    //   9642: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9645: pop
    //   9646: aload #79
    //   9648: iload #64
    //   9650: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   9653: pop
    //   9654: aload #79
    //   9656: ldc_w ' times)'
    //   9659: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9662: pop
    //   9663: aload #34
    //   9665: invokevirtual isRunningLocked : ()Z
    //   9668: ifeq -> 9680
    //   9671: aload #79
    //   9673: ldc_w ' (currently running)'
    //   9676: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9679: pop
    //   9680: aload #79
    //   9682: ldc_w '\\n'
    //   9685: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9688: pop
    //   9689: goto -> 9692
    //   9692: aload #79
    //   9694: aload_3
    //   9695: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9698: pop
    //   9699: aload #79
    //   9701: ldc_w '    Bluetooth Scan (total actual realtime): '
    //   9704: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9707: pop
    //   9708: aload #79
    //   9710: lload #26
    //   9712: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   9715: aload #79
    //   9717: aload #35
    //   9719: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9722: pop
    //   9723: aload #79
    //   9725: iload #64
    //   9727: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   9730: pop
    //   9731: aload #79
    //   9733: ldc_w ' times)'
    //   9736: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9739: pop
    //   9740: aload #34
    //   9742: invokevirtual isRunningLocked : ()Z
    //   9745: ifeq -> 9757
    //   9748: aload #79
    //   9750: ldc_w ' (currently running)'
    //   9753: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9756: pop
    //   9757: aload #79
    //   9759: ldc_w '\\n'
    //   9762: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9765: pop
    //   9766: lload #20
    //   9768: lconst_0
    //   9769: lcmp
    //   9770: ifgt -> 9784
    //   9773: iload #36
    //   9775: ifle -> 9781
    //   9778: goto -> 9784
    //   9781: goto -> 9863
    //   9784: aload #79
    //   9786: aload_3
    //   9787: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9790: pop
    //   9791: aload #79
    //   9793: ldc_w '    Bluetooth Scan (background realtime): '
    //   9796: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9799: pop
    //   9800: aload #79
    //   9802: lload #20
    //   9804: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   9807: aload #79
    //   9809: aload #35
    //   9811: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9814: pop
    //   9815: aload #79
    //   9817: iload #36
    //   9819: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   9822: pop
    //   9823: aload #79
    //   9825: ldc_w ' times)'
    //   9828: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9831: pop
    //   9832: aload #74
    //   9834: ifnull -> 9854
    //   9837: aload #74
    //   9839: invokevirtual isRunningLocked : ()Z
    //   9842: ifeq -> 9854
    //   9845: aload #79
    //   9847: ldc_w ' (currently running in background)'
    //   9850: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9853: pop
    //   9854: aload #79
    //   9856: ldc_w '\\n'
    //   9859: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9862: pop
    //   9863: aload #79
    //   9865: aload_3
    //   9866: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9869: pop
    //   9870: aload #79
    //   9872: ldc_w '    Bluetooth Scan Results: '
    //   9875: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9878: pop
    //   9879: aload #79
    //   9881: iload #44
    //   9883: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   9886: pop
    //   9887: aload #79
    //   9889: aload #35
    //   9891: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9894: pop
    //   9895: aload #79
    //   9897: iload #48
    //   9899: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   9902: pop
    //   9903: aload #79
    //   9905: ldc_w ' in background)'
    //   9908: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9911: pop
    //   9912: lload #16
    //   9914: lconst_0
    //   9915: lcmp
    //   9916: ifgt -> 9932
    //   9919: lload #14
    //   9921: lconst_0
    //   9922: lcmp
    //   9923: ifle -> 9929
    //   9926: goto -> 9932
    //   9929: goto -> 10105
    //   9932: aload #79
    //   9934: ldc_w '\\n'
    //   9937: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9940: pop
    //   9941: aload #79
    //   9943: aload_3
    //   9944: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9947: pop
    //   9948: aload #79
    //   9950: ldc_w '    Unoptimized Bluetooth Scan (realtime): '
    //   9953: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9956: pop
    //   9957: aload #79
    //   9959: lload #16
    //   9961: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   9964: aload #79
    //   9966: ldc_w ' (max '
    //   9969: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9972: pop
    //   9973: aload #79
    //   9975: lload #10
    //   9977: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   9980: aload #79
    //   9982: aload_1
    //   9983: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   9986: pop
    //   9987: aload #67
    //   9989: ifnull -> 10015
    //   9992: aload #67
    //   9994: invokevirtual isRunningLocked : ()Z
    //   9997: ifeq -> 10012
    //   10000: aload #79
    //   10002: ldc_w ' (currently running unoptimized)'
    //   10005: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10008: pop
    //   10009: goto -> 10015
    //   10012: goto -> 10015
    //   10015: aload #71
    //   10017: ifnull -> 10105
    //   10020: lload #14
    //   10022: lconst_0
    //   10023: lcmp
    //   10024: ifle -> 10105
    //   10027: aload #79
    //   10029: ldc_w '\\n'
    //   10032: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10035: pop
    //   10036: aload #79
    //   10038: aload_3
    //   10039: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10042: pop
    //   10043: aload #79
    //   10045: ldc_w '    Unoptimized Bluetooth Scan (background realtime): '
    //   10048: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10051: pop
    //   10052: aload #79
    //   10054: lload #14
    //   10056: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   10059: aload #79
    //   10061: ldc_w ' (max '
    //   10064: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10067: pop
    //   10068: aload #79
    //   10070: lload #12
    //   10072: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   10075: aload #79
    //   10077: aload_1
    //   10078: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10081: pop
    //   10082: aload #71
    //   10084: invokevirtual isRunningLocked : ()Z
    //   10087: ifeq -> 10102
    //   10090: aload #79
    //   10092: ldc_w ' (currently running unoptimized in background)'
    //   10095: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10098: pop
    //   10099: goto -> 10105
    //   10102: goto -> 10105
    //   10105: aload #79
    //   10107: invokevirtual toString : ()Ljava/lang/String;
    //   10110: astore #34
    //   10112: aload_2
    //   10113: astore #71
    //   10115: aload #71
    //   10117: aload #34
    //   10119: invokevirtual println : (Ljava/lang/String;)V
    //   10122: iconst_1
    //   10123: istore #36
    //   10125: aload_1
    //   10126: astore #34
    //   10128: goto -> 10140
    //   10131: goto -> 10134
    //   10134: iconst_0
    //   10135: istore #36
    //   10137: aload_1
    //   10138: astore #34
    //   10140: lload #18
    //   10142: lstore #12
    //   10144: ldc_w ' times)'
    //   10147: astore #67
    //   10149: aload #49
    //   10151: astore #74
    //   10153: aload #42
    //   10155: astore_1
    //   10156: aload #35
    //   10158: astore #49
    //   10160: aload #50
    //   10162: invokevirtual hasUserActivity : ()Z
    //   10165: istore #6
    //   10167: ldc_w ', '
    //   10170: astore #68
    //   10172: iload #6
    //   10174: ifeq -> 10311
    //   10177: iconst_0
    //   10178: istore #44
    //   10180: iconst_0
    //   10181: istore #48
    //   10183: iload #48
    //   10185: getstatic android/os/BatteryStats$Uid.NUM_USER_ACTIVITY_TYPES : I
    //   10188: if_icmpge -> 10279
    //   10191: aload #50
    //   10193: iload #48
    //   10195: iload #4
    //   10197: invokevirtual getUserActivityCount : (II)I
    //   10200: istore #64
    //   10202: iload #64
    //   10204: ifeq -> 10273
    //   10207: iload #44
    //   10209: ifne -> 10233
    //   10212: aload #79
    //   10214: iconst_0
    //   10215: invokevirtual setLength : (I)V
    //   10218: aload #79
    //   10220: ldc_w '    User activity: '
    //   10223: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10226: pop
    //   10227: iconst_1
    //   10228: istore #44
    //   10230: goto -> 10242
    //   10233: aload #79
    //   10235: ldc_w ', '
    //   10238: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10241: pop
    //   10242: aload #79
    //   10244: iload #64
    //   10246: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   10249: pop
    //   10250: aload #79
    //   10252: aload #45
    //   10254: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10257: pop
    //   10258: aload #79
    //   10260: getstatic android/os/BatteryStats$Uid.USER_ACTIVITY_TYPES : [Ljava/lang/String;
    //   10263: iload #48
    //   10265: aaload
    //   10266: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10269: pop
    //   10270: goto -> 10273
    //   10273: iinc #48, 1
    //   10276: goto -> 10183
    //   10279: aload #50
    //   10281: astore_1
    //   10282: aload #45
    //   10284: astore #35
    //   10286: iload #44
    //   10288: ifeq -> 10318
    //   10291: aload #71
    //   10293: aload #79
    //   10295: invokevirtual toString : ()Ljava/lang/String;
    //   10298: invokevirtual println : (Ljava/lang/String;)V
    //   10301: aload #50
    //   10303: astore_1
    //   10304: aload #45
    //   10306: astore #35
    //   10308: goto -> 10318
    //   10311: aload #45
    //   10313: astore #35
    //   10315: aload #50
    //   10317: astore_1
    //   10318: aload_1
    //   10319: invokevirtual getWakelockStats : ()Landroid/util/ArrayMap;
    //   10322: astore #72
    //   10324: aload #72
    //   10326: invokevirtual size : ()I
    //   10329: istore #44
    //   10331: aload #35
    //   10333: astore #73
    //   10335: lconst_0
    //   10336: lstore #20
    //   10338: lconst_0
    //   10339: lstore #14
    //   10341: iconst_0
    //   10342: istore #48
    //   10344: iinc #44, -1
    //   10347: lconst_0
    //   10348: lstore #16
    //   10350: lconst_0
    //   10351: lstore #10
    //   10353: aload #67
    //   10355: astore #45
    //   10357: aload #34
    //   10359: astore #50
    //   10361: aload #68
    //   10363: astore #35
    //   10365: aload #72
    //   10367: astore #67
    //   10369: aload #47
    //   10371: astore #34
    //   10373: iload #44
    //   10375: iflt -> 10672
    //   10378: aload #67
    //   10380: iload #44
    //   10382: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   10385: checkcast android/os/BatteryStats$Uid$Wakelock
    //   10388: astore #68
    //   10390: aload #79
    //   10392: iconst_0
    //   10393: invokevirtual setLength : (I)V
    //   10396: aload #79
    //   10398: aload_3
    //   10399: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10402: pop
    //   10403: aload #79
    //   10405: ldc_w '    Wake lock '
    //   10408: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10411: pop
    //   10412: aload #79
    //   10414: aload #67
    //   10416: iload #44
    //   10418: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   10421: checkcast java/lang/String
    //   10424: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10427: pop
    //   10428: aload #79
    //   10430: aload #68
    //   10432: iconst_1
    //   10433: invokevirtual getWakeTime : (I)Landroid/os/BatteryStats$Timer;
    //   10436: lload #22
    //   10438: ldc_w 'full'
    //   10441: iload #4
    //   10443: ldc_w ': '
    //   10446: invokestatic printWakeLock : (Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    //   10449: astore #72
    //   10451: aload #68
    //   10453: iconst_0
    //   10454: invokevirtual getWakeTime : (I)Landroid/os/BatteryStats$Timer;
    //   10457: astore #47
    //   10459: aload #79
    //   10461: aload #47
    //   10463: lload #22
    //   10465: ldc_w 'partial'
    //   10468: iload #4
    //   10470: aload #72
    //   10472: invokestatic printWakeLock : (Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    //   10475: astore #72
    //   10477: aload #47
    //   10479: ifnull -> 10492
    //   10482: aload #47
    //   10484: invokevirtual getSubTimer : ()Landroid/os/BatteryStats$Timer;
    //   10487: astore #47
    //   10489: goto -> 10495
    //   10492: aconst_null
    //   10493: astore #47
    //   10495: aload #79
    //   10497: aload #47
    //   10499: lload #22
    //   10501: ldc_w 'background partial'
    //   10504: iload #4
    //   10506: aload #72
    //   10508: invokestatic printWakeLock : (Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    //   10511: astore #47
    //   10513: aload #79
    //   10515: aload #68
    //   10517: iconst_2
    //   10518: invokevirtual getWakeTime : (I)Landroid/os/BatteryStats$Timer;
    //   10521: lload #22
    //   10523: ldc_w 'window'
    //   10526: iload #4
    //   10528: aload #47
    //   10530: invokestatic printWakeLock : (Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    //   10533: astore #47
    //   10535: aload #79
    //   10537: aload #68
    //   10539: bipush #18
    //   10541: invokevirtual getWakeTime : (I)Landroid/os/BatteryStats$Timer;
    //   10544: lload #22
    //   10546: ldc_w 'draw'
    //   10549: iload #4
    //   10551: aload #47
    //   10553: invokestatic printWakeLock : (Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    //   10556: pop
    //   10557: aload #79
    //   10559: aload #51
    //   10561: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10564: pop
    //   10565: aload #71
    //   10567: aload #79
    //   10569: invokevirtual toString : ()Ljava/lang/String;
    //   10572: invokevirtual println : (Ljava/lang/String;)V
    //   10575: iconst_1
    //   10576: istore #36
    //   10578: iinc #48, 1
    //   10581: aload #68
    //   10583: iconst_1
    //   10584: invokevirtual getWakeTime : (I)Landroid/os/BatteryStats$Timer;
    //   10587: lload #22
    //   10589: iload #4
    //   10591: invokestatic computeWakeLock : (Landroid/os/BatteryStats$Timer;JI)J
    //   10594: lstore #26
    //   10596: aload #68
    //   10598: iconst_0
    //   10599: invokevirtual getWakeTime : (I)Landroid/os/BatteryStats$Timer;
    //   10602: lload #22
    //   10604: iload #4
    //   10606: invokestatic computeWakeLock : (Landroid/os/BatteryStats$Timer;JI)J
    //   10609: lstore #32
    //   10611: aload #68
    //   10613: iconst_2
    //   10614: invokevirtual getWakeTime : (I)Landroid/os/BatteryStats$Timer;
    //   10617: lload #22
    //   10619: iload #4
    //   10621: invokestatic computeWakeLock : (Landroid/os/BatteryStats$Timer;JI)J
    //   10624: lstore #28
    //   10626: lload #14
    //   10628: aload #68
    //   10630: bipush #18
    //   10632: invokevirtual getWakeTime : (I)Landroid/os/BatteryStats$Timer;
    //   10635: lload #22
    //   10637: iload #4
    //   10639: invokestatic computeWakeLock : (Landroid/os/BatteryStats$Timer;JI)J
    //   10642: ladd
    //   10643: lstore #14
    //   10645: iinc #44, -1
    //   10648: lload #32
    //   10650: lload #16
    //   10652: ladd
    //   10653: lstore #16
    //   10655: lload #20
    //   10657: lload #28
    //   10659: ladd
    //   10660: lstore #20
    //   10662: lload #26
    //   10664: lload #10
    //   10666: ladd
    //   10667: lstore #10
    //   10669: goto -> 10373
    //   10672: lload #16
    //   10674: lstore #26
    //   10676: aload #50
    //   10678: astore #67
    //   10680: iload #48
    //   10682: iconst_1
    //   10683: if_icmple -> 11079
    //   10686: aload_1
    //   10687: invokevirtual getAggregatedPartialWakelockTimer : ()Landroid/os/BatteryStats$Timer;
    //   10690: ifnull -> 10742
    //   10693: aload_1
    //   10694: invokevirtual getAggregatedPartialWakelockTimer : ()Landroid/os/BatteryStats$Timer;
    //   10697: astore #50
    //   10699: aload #50
    //   10701: lload #12
    //   10703: invokevirtual getTotalDurationMsLocked : (J)J
    //   10706: lstore #28
    //   10708: aload #50
    //   10710: invokevirtual getSubTimer : ()Landroid/os/BatteryStats$Timer;
    //   10713: astore #50
    //   10715: aload #50
    //   10717: ifnull -> 10732
    //   10720: aload #50
    //   10722: lload #12
    //   10724: invokevirtual getTotalDurationMsLocked : (J)J
    //   10727: lstore #16
    //   10729: goto -> 10735
    //   10732: lconst_0
    //   10733: lstore #16
    //   10735: lload #28
    //   10737: lstore #12
    //   10739: goto -> 10748
    //   10742: lconst_0
    //   10743: lstore #12
    //   10745: lconst_0
    //   10746: lstore #16
    //   10748: lload #18
    //   10750: lstore #28
    //   10752: lload #12
    //   10754: lconst_0
    //   10755: lcmp
    //   10756: ifne -> 10793
    //   10759: lload #16
    //   10761: lconst_0
    //   10762: lcmp
    //   10763: ifne -> 10793
    //   10766: lload #10
    //   10768: lconst_0
    //   10769: lcmp
    //   10770: ifne -> 10793
    //   10773: lload #26
    //   10775: lconst_0
    //   10776: lcmp
    //   10777: ifne -> 10793
    //   10780: lload #20
    //   10782: lconst_0
    //   10783: lcmp
    //   10784: ifeq -> 10790
    //   10787: goto -> 10793
    //   10790: goto -> 11079
    //   10793: aload #79
    //   10795: iconst_0
    //   10796: invokevirtual setLength : (I)V
    //   10799: aload #79
    //   10801: aload_3
    //   10802: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10805: pop
    //   10806: aload #79
    //   10808: ldc_w '    TOTAL wake: '
    //   10811: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10814: pop
    //   10815: iconst_0
    //   10816: istore #44
    //   10818: lload #10
    //   10820: lconst_0
    //   10821: lcmp
    //   10822: ifeq -> 10844
    //   10825: aload #79
    //   10827: lload #10
    //   10829: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   10832: aload #79
    //   10834: ldc_w 'full'
    //   10837: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10840: pop
    //   10841: iconst_1
    //   10842: istore #44
    //   10844: lload #26
    //   10846: lconst_0
    //   10847: lcmp
    //   10848: ifeq -> 10889
    //   10851: iload #44
    //   10853: ifeq -> 10867
    //   10856: aload #79
    //   10858: aload #35
    //   10860: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10863: pop
    //   10864: goto -> 10867
    //   10867: iconst_1
    //   10868: istore #48
    //   10870: aload #79
    //   10872: lload #26
    //   10874: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   10877: aload #79
    //   10879: ldc_w 'blamed partial'
    //   10882: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10885: pop
    //   10886: goto -> 10893
    //   10889: iload #44
    //   10891: istore #48
    //   10893: iload #48
    //   10895: istore #44
    //   10897: lload #12
    //   10899: lconst_0
    //   10900: lcmp
    //   10901: ifeq -> 10936
    //   10904: iload #48
    //   10906: ifeq -> 10917
    //   10909: aload #79
    //   10911: aload #35
    //   10913: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10916: pop
    //   10917: iconst_1
    //   10918: istore #44
    //   10920: aload #79
    //   10922: lload #12
    //   10924: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   10927: aload #79
    //   10929: ldc_w 'actual partial'
    //   10932: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10935: pop
    //   10936: iload #44
    //   10938: istore #48
    //   10940: lload #16
    //   10942: lconst_0
    //   10943: lcmp
    //   10944: ifeq -> 10979
    //   10947: iload #44
    //   10949: ifeq -> 10960
    //   10952: aload #79
    //   10954: aload #35
    //   10956: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10959: pop
    //   10960: iconst_1
    //   10961: istore #48
    //   10963: aload #79
    //   10965: lload #16
    //   10967: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   10970: aload #79
    //   10972: ldc_w 'actual background partial'
    //   10975: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10978: pop
    //   10979: lload #20
    //   10981: lconst_0
    //   10982: lcmp
    //   10983: ifeq -> 11021
    //   10986: iload #48
    //   10988: ifeq -> 10999
    //   10991: aload #79
    //   10993: aload #35
    //   10995: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   10998: pop
    //   10999: iconst_1
    //   11000: istore #48
    //   11002: aload #79
    //   11004: lload #20
    //   11006: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   11009: aload #79
    //   11011: ldc_w 'window'
    //   11014: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11017: pop
    //   11018: goto -> 11021
    //   11021: lload #14
    //   11023: lconst_0
    //   11024: lcmp
    //   11025: ifeq -> 11058
    //   11028: iload #48
    //   11030: ifeq -> 11042
    //   11033: aload #79
    //   11035: ldc_w ','
    //   11038: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11041: pop
    //   11042: aload #79
    //   11044: lload #14
    //   11046: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   11049: aload #79
    //   11051: ldc_w 'draw'
    //   11054: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11057: pop
    //   11058: aload #79
    //   11060: aload #51
    //   11062: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11065: pop
    //   11066: aload #71
    //   11068: aload #79
    //   11070: invokevirtual toString : ()Ljava/lang/String;
    //   11073: invokevirtual println : (Ljava/lang/String;)V
    //   11076: goto -> 11079
    //   11079: aload_1
    //   11080: invokevirtual getMulticastWakelockStats : ()Landroid/os/BatteryStats$Timer;
    //   11083: astore #50
    //   11085: aload #50
    //   11087: ifnull -> 11196
    //   11090: aload #50
    //   11092: lload #22
    //   11094: iload #4
    //   11096: invokevirtual getTotalTimeLocked : (JI)J
    //   11099: lstore #16
    //   11101: aload #50
    //   11103: iload #4
    //   11105: invokevirtual getCountLocked : (I)I
    //   11108: istore #44
    //   11110: lload #16
    //   11112: lconst_0
    //   11113: lcmp
    //   11114: ifle -> 11193
    //   11117: aload #79
    //   11119: iconst_0
    //   11120: invokevirtual setLength : (I)V
    //   11123: aload #79
    //   11125: aload_3
    //   11126: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11129: pop
    //   11130: aload #79
    //   11132: ldc_w '    WiFi Multicast Wakelock'
    //   11135: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11138: pop
    //   11139: aload #79
    //   11141: ldc_w ' count = '
    //   11144: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11147: pop
    //   11148: aload #79
    //   11150: iload #44
    //   11152: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   11155: pop
    //   11156: aload #79
    //   11158: ldc_w ' time = '
    //   11161: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11164: pop
    //   11165: aload #79
    //   11167: lload #16
    //   11169: ldc2_w 500
    //   11172: ladd
    //   11173: ldc2_w 1000
    //   11176: ldiv
    //   11177: invokestatic formatTimeMsNoSpace : (Ljava/lang/StringBuilder;J)V
    //   11180: aload #71
    //   11182: aload #79
    //   11184: invokevirtual toString : ()Ljava/lang/String;
    //   11187: invokevirtual println : (Ljava/lang/String;)V
    //   11190: goto -> 11196
    //   11193: goto -> 11196
    //   11196: aload_1
    //   11197: invokevirtual getSyncStats : ()Landroid/util/ArrayMap;
    //   11200: astore #47
    //   11202: aload #47
    //   11204: invokevirtual size : ()I
    //   11207: iconst_1
    //   11208: isub
    //   11209: istore #44
    //   11211: iload #44
    //   11213: iflt -> 11471
    //   11216: aload #47
    //   11218: iload #44
    //   11220: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   11223: checkcast android/os/BatteryStats$Timer
    //   11226: astore #68
    //   11228: aload #68
    //   11230: lload #22
    //   11232: iload #4
    //   11234: invokevirtual getTotalTimeLocked : (JI)J
    //   11237: ldc2_w 500
    //   11240: ladd
    //   11241: ldc2_w 1000
    //   11244: ldiv
    //   11245: lstore #10
    //   11247: aload #68
    //   11249: iload #4
    //   11251: invokevirtual getCountLocked : (I)I
    //   11254: istore #48
    //   11256: aload #68
    //   11258: invokevirtual getSubTimer : ()Landroid/os/BatteryStats$Timer;
    //   11261: astore #68
    //   11263: aload #68
    //   11265: ifnull -> 11280
    //   11268: aload #68
    //   11270: lload #18
    //   11272: invokevirtual getTotalDurationMsLocked : (J)J
    //   11275: lstore #16
    //   11277: goto -> 11285
    //   11280: ldc2_w -1
    //   11283: lstore #16
    //   11285: aload #68
    //   11287: ifnull -> 11302
    //   11290: aload #68
    //   11292: iload #4
    //   11294: invokevirtual getCountLocked : (I)I
    //   11297: istore #36
    //   11299: goto -> 11305
    //   11302: iconst_m1
    //   11303: istore #36
    //   11305: aload #79
    //   11307: iconst_0
    //   11308: invokevirtual setLength : (I)V
    //   11311: aload #79
    //   11313: aload_3
    //   11314: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11317: pop
    //   11318: aload #79
    //   11320: ldc_w '    Sync '
    //   11323: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11326: pop
    //   11327: aload #79
    //   11329: aload #47
    //   11331: iload #44
    //   11333: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   11336: checkcast java/lang/String
    //   11339: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11342: pop
    //   11343: aload #79
    //   11345: aload #7
    //   11347: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11350: pop
    //   11351: lload #10
    //   11353: lconst_0
    //   11354: lcmp
    //   11355: ifeq -> 11443
    //   11358: aload #79
    //   11360: lload #10
    //   11362: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   11365: aload #79
    //   11367: ldc_w 'realtime ('
    //   11370: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11373: pop
    //   11374: aload #79
    //   11376: iload #48
    //   11378: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   11381: pop
    //   11382: aload #79
    //   11384: aload #45
    //   11386: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11389: pop
    //   11390: lload #16
    //   11392: lconst_0
    //   11393: lcmp
    //   11394: ifle -> 11440
    //   11397: aload #79
    //   11399: aload #35
    //   11401: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11404: pop
    //   11405: aload #79
    //   11407: lload #16
    //   11409: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   11412: aload #79
    //   11414: ldc_w 'background ('
    //   11417: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11420: pop
    //   11421: aload #79
    //   11423: iload #36
    //   11425: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   11428: pop
    //   11429: aload #79
    //   11431: aload #45
    //   11433: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11436: pop
    //   11437: goto -> 11452
    //   11440: goto -> 11452
    //   11443: aload #79
    //   11445: ldc_w '(not used)'
    //   11448: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11451: pop
    //   11452: aload #71
    //   11454: aload #79
    //   11456: invokevirtual toString : ()Ljava/lang/String;
    //   11459: invokevirtual println : (Ljava/lang/String;)V
    //   11462: iconst_1
    //   11463: istore #36
    //   11465: iinc #44, -1
    //   11468: goto -> 11211
    //   11471: aload #7
    //   11473: astore #47
    //   11475: aload_1
    //   11476: invokevirtual getJobStats : ()Landroid/util/ArrayMap;
    //   11479: astore #7
    //   11481: aload #7
    //   11483: invokevirtual size : ()I
    //   11486: iconst_1
    //   11487: isub
    //   11488: istore #44
    //   11490: iload #44
    //   11492: iflt -> 11750
    //   11495: aload #7
    //   11497: iload #44
    //   11499: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   11502: checkcast android/os/BatteryStats$Timer
    //   11505: astore #50
    //   11507: aload #50
    //   11509: lload #22
    //   11511: iload #4
    //   11513: invokevirtual getTotalTimeLocked : (JI)J
    //   11516: ldc2_w 500
    //   11519: ladd
    //   11520: ldc2_w 1000
    //   11523: ldiv
    //   11524: lstore #16
    //   11526: aload #50
    //   11528: iload #4
    //   11530: invokevirtual getCountLocked : (I)I
    //   11533: istore #48
    //   11535: aload #50
    //   11537: invokevirtual getSubTimer : ()Landroid/os/BatteryStats$Timer;
    //   11540: astore #50
    //   11542: aload #50
    //   11544: ifnull -> 11559
    //   11547: aload #50
    //   11549: lload #18
    //   11551: invokevirtual getTotalDurationMsLocked : (J)J
    //   11554: lstore #20
    //   11556: goto -> 11564
    //   11559: ldc2_w -1
    //   11562: lstore #20
    //   11564: aload #50
    //   11566: ifnull -> 11581
    //   11569: aload #50
    //   11571: iload #4
    //   11573: invokevirtual getCountLocked : (I)I
    //   11576: istore #36
    //   11578: goto -> 11584
    //   11581: iconst_m1
    //   11582: istore #36
    //   11584: aload #79
    //   11586: iconst_0
    //   11587: invokevirtual setLength : (I)V
    //   11590: aload #79
    //   11592: aload_3
    //   11593: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11596: pop
    //   11597: aload #79
    //   11599: ldc_w '    Job '
    //   11602: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11605: pop
    //   11606: aload #79
    //   11608: aload #7
    //   11610: iload #44
    //   11612: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   11615: checkcast java/lang/String
    //   11618: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11621: pop
    //   11622: aload #79
    //   11624: aload #47
    //   11626: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11629: pop
    //   11630: lload #16
    //   11632: lconst_0
    //   11633: lcmp
    //   11634: ifeq -> 11722
    //   11637: aload #79
    //   11639: lload #16
    //   11641: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   11644: aload #79
    //   11646: ldc_w 'realtime ('
    //   11649: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11652: pop
    //   11653: aload #79
    //   11655: iload #48
    //   11657: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   11660: pop
    //   11661: aload #79
    //   11663: aload #45
    //   11665: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11668: pop
    //   11669: lload #20
    //   11671: lconst_0
    //   11672: lcmp
    //   11673: ifle -> 11719
    //   11676: aload #79
    //   11678: aload #35
    //   11680: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11683: pop
    //   11684: aload #79
    //   11686: lload #20
    //   11688: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   11691: aload #79
    //   11693: ldc_w 'background ('
    //   11696: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11699: pop
    //   11700: aload #79
    //   11702: iload #36
    //   11704: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   11707: pop
    //   11708: aload #79
    //   11710: aload #45
    //   11712: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11715: pop
    //   11716: goto -> 11731
    //   11719: goto -> 11731
    //   11722: aload #79
    //   11724: ldc_w '(not used)'
    //   11727: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   11730: pop
    //   11731: aload #71
    //   11733: aload #79
    //   11735: invokevirtual toString : ()Ljava/lang/String;
    //   11738: invokevirtual println : (Ljava/lang/String;)V
    //   11741: iconst_1
    //   11742: istore #36
    //   11744: iinc #44, -1
    //   11747: goto -> 11490
    //   11750: aload_1
    //   11751: invokevirtual getJobCompletionStats : ()Landroid/util/ArrayMap;
    //   11754: astore #68
    //   11756: aload #68
    //   11758: invokevirtual size : ()I
    //   11761: iconst_1
    //   11762: isub
    //   11763: istore #44
    //   11765: aload #74
    //   11767: astore #50
    //   11769: aload #73
    //   11771: astore #7
    //   11773: iload #44
    //   11775: iflt -> 11912
    //   11778: aload #68
    //   11780: iload #44
    //   11782: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   11785: checkcast android/util/SparseIntArray
    //   11788: astore #74
    //   11790: aload #74
    //   11792: ifnull -> 11906
    //   11795: aload_2
    //   11796: aload_3
    //   11797: invokevirtual print : (Ljava/lang/String;)V
    //   11800: aload #71
    //   11802: ldc_w '    Job Completions '
    //   11805: invokevirtual print : (Ljava/lang/String;)V
    //   11808: aload #71
    //   11810: aload #68
    //   11812: iload #44
    //   11814: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   11817: checkcast java/lang/String
    //   11820: invokevirtual print : (Ljava/lang/String;)V
    //   11823: aload #71
    //   11825: ldc_w ':'
    //   11828: invokevirtual print : (Ljava/lang/String;)V
    //   11831: iconst_0
    //   11832: istore #48
    //   11834: iload #48
    //   11836: aload #74
    //   11838: invokevirtual size : ()I
    //   11841: if_icmpge -> 11899
    //   11844: aload #71
    //   11846: aload #7
    //   11848: invokevirtual print : (Ljava/lang/String;)V
    //   11851: aload #71
    //   11853: aload #74
    //   11855: iload #48
    //   11857: invokevirtual keyAt : (I)I
    //   11860: invokestatic getReasonCodeDescription : (I)Ljava/lang/String;
    //   11863: invokevirtual print : (Ljava/lang/String;)V
    //   11866: aload #71
    //   11868: aload #50
    //   11870: invokevirtual print : (Ljava/lang/String;)V
    //   11873: aload #71
    //   11875: aload #74
    //   11877: iload #48
    //   11879: invokevirtual valueAt : (I)I
    //   11882: invokevirtual print : (I)V
    //   11885: aload #71
    //   11887: ldc_w 'x)'
    //   11890: invokevirtual print : (Ljava/lang/String;)V
    //   11893: iinc #48, 1
    //   11896: goto -> 11834
    //   11899: aload_2
    //   11900: invokevirtual println : ()V
    //   11903: goto -> 11906
    //   11906: iinc #44, -1
    //   11909: goto -> 11773
    //   11912: aload_1
    //   11913: astore #68
    //   11915: aload #68
    //   11917: aload #79
    //   11919: iload #4
    //   11921: invokevirtual getDeferredJobsLineLocked : (Ljava/lang/StringBuilder;I)V
    //   11924: aload #79
    //   11926: invokevirtual length : ()I
    //   11929: ifle -> 11950
    //   11932: aload #71
    //   11934: ldc_w '    Jobs deferred on launch '
    //   11937: invokevirtual print : (Ljava/lang/String;)V
    //   11940: aload #71
    //   11942: aload #79
    //   11944: invokevirtual toString : ()Ljava/lang/String;
    //   11947: invokevirtual println : (Ljava/lang/String;)V
    //   11950: aload #68
    //   11952: invokevirtual getFlashlightTurnedOnTimer : ()Landroid/os/BatteryStats$Timer;
    //   11955: astore #74
    //   11957: lload #18
    //   11959: lstore #20
    //   11961: aload #46
    //   11963: astore_1
    //   11964: aload #50
    //   11966: astore #71
    //   11968: lload #22
    //   11970: lstore #18
    //   11972: aload #35
    //   11974: astore_1
    //   11975: aload_2
    //   11976: aload #79
    //   11978: aload #74
    //   11980: lload #22
    //   11982: iload #4
    //   11984: aload_3
    //   11985: ldc_w 'Flashlight'
    //   11988: invokestatic printTimer : (Ljava/io/PrintWriter;Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JILjava/lang/String;Ljava/lang/String;)Z
    //   11991: istore #6
    //   11993: aload_2
    //   11994: aload #79
    //   11996: aload #68
    //   11998: invokevirtual getCameraTurnedOnTimer : ()Landroid/os/BatteryStats$Timer;
    //   12001: lload #18
    //   12003: iload #4
    //   12005: aload_3
    //   12006: ldc_w 'Camera'
    //   12009: invokestatic printTimer : (Ljava/io/PrintWriter;Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JILjava/lang/String;Ljava/lang/String;)Z
    //   12012: istore #80
    //   12014: aload_2
    //   12015: aload #79
    //   12017: aload #68
    //   12019: invokevirtual getVideoTurnedOnTimer : ()Landroid/os/BatteryStats$Timer;
    //   12022: lload #18
    //   12024: iload #4
    //   12026: aload_3
    //   12027: ldc_w 'Video'
    //   12030: invokestatic printTimer : (Ljava/io/PrintWriter;Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JILjava/lang/String;Ljava/lang/String;)Z
    //   12033: istore #81
    //   12035: aload_2
    //   12036: aload #79
    //   12038: aload #68
    //   12040: invokevirtual getAudioTurnedOnTimer : ()Landroid/os/BatteryStats$Timer;
    //   12043: lload #18
    //   12045: iload #4
    //   12047: aload_3
    //   12048: ldc_w 'Audio'
    //   12051: invokestatic printTimer : (Ljava/io/PrintWriter;Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JILjava/lang/String;Ljava/lang/String;)Z
    //   12054: istore #82
    //   12056: aload #68
    //   12058: invokevirtual getSensorStats : ()Landroid/util/SparseArray;
    //   12061: astore #35
    //   12063: aload #35
    //   12065: invokevirtual size : ()I
    //   12068: istore #64
    //   12070: iconst_0
    //   12071: istore #44
    //   12073: iload #36
    //   12075: iload #6
    //   12077: ior
    //   12078: iload #80
    //   12080: ior
    //   12081: iload #81
    //   12083: ior
    //   12084: iload #82
    //   12086: ior
    //   12087: istore #48
    //   12089: aload #45
    //   12091: astore #50
    //   12093: aload #35
    //   12095: astore #45
    //   12097: iload #64
    //   12099: istore #36
    //   12101: aload #47
    //   12103: astore #35
    //   12105: lload #20
    //   12107: lstore #22
    //   12109: iload #44
    //   12111: iload #36
    //   12113: if_icmpge -> 12462
    //   12116: aload #45
    //   12118: iload #44
    //   12120: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   12123: checkcast android/os/BatteryStats$Uid$Sensor
    //   12126: astore #73
    //   12128: aload #45
    //   12130: iload #44
    //   12132: invokevirtual keyAt : (I)I
    //   12135: pop
    //   12136: aload #79
    //   12138: iconst_0
    //   12139: invokevirtual setLength : (I)V
    //   12142: aload #79
    //   12144: aload_3
    //   12145: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12148: pop
    //   12149: aload #79
    //   12151: ldc_w '    Sensor '
    //   12154: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12157: pop
    //   12158: aload #73
    //   12160: invokevirtual getHandle : ()I
    //   12163: istore #48
    //   12165: iload #48
    //   12167: sipush #-10000
    //   12170: if_icmpne -> 12185
    //   12173: aload #79
    //   12175: ldc_w 'GPS'
    //   12178: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12181: pop
    //   12182: goto -> 12193
    //   12185: aload #79
    //   12187: iload #48
    //   12189: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   12192: pop
    //   12193: aload #79
    //   12195: aload #35
    //   12197: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12200: pop
    //   12201: aload #73
    //   12203: invokevirtual getSensorTime : ()Landroid/os/BatteryStats$Timer;
    //   12206: astore #74
    //   12208: aload #74
    //   12210: ifnull -> 12435
    //   12213: aload #74
    //   12215: lload #18
    //   12217: iload #4
    //   12219: invokevirtual getTotalTimeLocked : (JI)J
    //   12222: ldc2_w 500
    //   12225: ladd
    //   12226: ldc2_w 1000
    //   12229: ldiv
    //   12230: lstore #10
    //   12232: aload #74
    //   12234: iload #4
    //   12236: invokevirtual getCountLocked : (I)I
    //   12239: istore #64
    //   12241: aload #73
    //   12243: invokevirtual getSensorBackgroundTime : ()Landroid/os/BatteryStats$Timer;
    //   12246: astore #73
    //   12248: aload #73
    //   12250: ifnull -> 12265
    //   12253: aload #73
    //   12255: iload #4
    //   12257: invokevirtual getCountLocked : (I)I
    //   12260: istore #48
    //   12262: goto -> 12268
    //   12265: iconst_0
    //   12266: istore #48
    //   12268: aload #74
    //   12270: lload #22
    //   12272: invokevirtual getTotalDurationMsLocked : (J)J
    //   12275: lstore #16
    //   12277: aload #73
    //   12279: ifnull -> 12294
    //   12282: aload #73
    //   12284: lload #22
    //   12286: invokevirtual getTotalDurationMsLocked : (J)J
    //   12289: lstore #20
    //   12291: goto -> 12297
    //   12294: lconst_0
    //   12295: lstore #20
    //   12297: lload #10
    //   12299: lconst_0
    //   12300: lcmp
    //   12301: ifeq -> 12423
    //   12304: lload #16
    //   12306: lload #10
    //   12308: lcmp
    //   12309: ifeq -> 12331
    //   12312: aload #79
    //   12314: lload #10
    //   12316: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   12319: aload #79
    //   12321: ldc_w 'blamed realtime, '
    //   12324: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12327: pop
    //   12328: goto -> 12331
    //   12331: aload #79
    //   12333: lload #16
    //   12335: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   12338: aload #79
    //   12340: ldc_w 'realtime ('
    //   12343: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12346: pop
    //   12347: aload #79
    //   12349: iload #64
    //   12351: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   12354: pop
    //   12355: aload #79
    //   12357: aload #50
    //   12359: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12362: pop
    //   12363: lload #20
    //   12365: lconst_0
    //   12366: lcmp
    //   12367: ifne -> 12381
    //   12370: iload #48
    //   12372: ifle -> 12378
    //   12375: goto -> 12381
    //   12378: goto -> 12432
    //   12381: aload #79
    //   12383: aload_1
    //   12384: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12387: pop
    //   12388: aload #79
    //   12390: lload #20
    //   12392: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   12395: aload #79
    //   12397: ldc_w 'background ('
    //   12400: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12403: pop
    //   12404: aload #79
    //   12406: iload #48
    //   12408: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   12411: pop
    //   12412: aload #79
    //   12414: aload #50
    //   12416: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12419: pop
    //   12420: goto -> 12432
    //   12423: aload #79
    //   12425: ldc_w '(not used)'
    //   12428: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12431: pop
    //   12432: goto -> 12444
    //   12435: aload #79
    //   12437: ldc_w '(not used)'
    //   12440: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12443: pop
    //   12444: aload_2
    //   12445: aload #79
    //   12447: invokevirtual toString : ()Ljava/lang/String;
    //   12450: invokevirtual println : (Ljava/lang/String;)V
    //   12453: iconst_1
    //   12454: istore #48
    //   12456: iinc #44, 1
    //   12459: goto -> 12109
    //   12462: lload #22
    //   12464: lstore #16
    //   12466: aload #49
    //   12468: astore #74
    //   12470: iload #37
    //   12472: istore #44
    //   12474: aload #35
    //   12476: astore #49
    //   12478: aload #68
    //   12480: invokevirtual getVibratorOnTimer : ()Landroid/os/BatteryStats$Timer;
    //   12483: astore #35
    //   12485: aload_3
    //   12486: astore #73
    //   12488: aload_2
    //   12489: astore #47
    //   12491: aload_2
    //   12492: aload #79
    //   12494: aload #35
    //   12496: lload #18
    //   12498: iload #4
    //   12500: aload_3
    //   12501: ldc_w 'Vibrator'
    //   12504: invokestatic printTimer : (Ljava/io/PrintWriter;Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JILjava/lang/String;Ljava/lang/String;)Z
    //   12507: istore #80
    //   12509: aload_2
    //   12510: aload #79
    //   12512: aload #68
    //   12514: invokevirtual getForegroundActivityTimer : ()Landroid/os/BatteryStats$Timer;
    //   12517: lload #18
    //   12519: iload #4
    //   12521: aload_3
    //   12522: ldc_w 'Foreground activities'
    //   12525: invokestatic printTimer : (Ljava/io/PrintWriter;Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JILjava/lang/String;Ljava/lang/String;)Z
    //   12528: istore #6
    //   12530: iload #48
    //   12532: iload #80
    //   12534: ior
    //   12535: iload #6
    //   12537: ior
    //   12538: aload_2
    //   12539: aload #79
    //   12541: aload #68
    //   12543: invokevirtual getForegroundServiceTimer : ()Landroid/os/BatteryStats$Timer;
    //   12546: lload #18
    //   12548: iload #4
    //   12550: aload_3
    //   12551: ldc_w 'Foreground services'
    //   12554: invokestatic printTimer : (Ljava/io/PrintWriter;Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JILjava/lang/String;Ljava/lang/String;)Z
    //   12557: ior
    //   12558: istore #37
    //   12560: lconst_0
    //   12561: lstore #22
    //   12563: iconst_0
    //   12564: istore #36
    //   12566: iload #36
    //   12568: bipush #7
    //   12570: if_icmpge -> 12686
    //   12573: aload #68
    //   12575: iload #36
    //   12577: lload #18
    //   12579: iload #4
    //   12581: invokevirtual getProcessStateTime : (IJI)J
    //   12584: lstore #10
    //   12586: lload #22
    //   12588: lstore #20
    //   12590: lload #10
    //   12592: lconst_0
    //   12593: lcmp
    //   12594: ifle -> 12676
    //   12597: lload #22
    //   12599: lload #10
    //   12601: ladd
    //   12602: lstore #20
    //   12604: aload #79
    //   12606: iconst_0
    //   12607: invokevirtual setLength : (I)V
    //   12610: aload #79
    //   12612: aload #73
    //   12614: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12617: pop
    //   12618: aload #79
    //   12620: ldc_w '    '
    //   12623: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12626: pop
    //   12627: aload #79
    //   12629: getstatic android/os/BatteryStats$Uid.PROCESS_STATE_NAMES : [Ljava/lang/String;
    //   12632: iload #36
    //   12634: aaload
    //   12635: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12638: pop
    //   12639: aload #79
    //   12641: ldc_w ' for: '
    //   12644: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12647: pop
    //   12648: aload #79
    //   12650: lload #10
    //   12652: ldc2_w 500
    //   12655: ladd
    //   12656: ldc2_w 1000
    //   12659: ldiv
    //   12660: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   12663: aload #47
    //   12665: aload #79
    //   12667: invokevirtual toString : ()Ljava/lang/String;
    //   12670: invokevirtual println : (Ljava/lang/String;)V
    //   12673: iconst_1
    //   12674: istore #37
    //   12676: iinc #36, 1
    //   12679: lload #20
    //   12681: lstore #22
    //   12683: goto -> 12566
    //   12686: lload #22
    //   12688: lconst_0
    //   12689: lcmp
    //   12690: ifle -> 12741
    //   12693: aload #79
    //   12695: iconst_0
    //   12696: invokevirtual setLength : (I)V
    //   12699: aload #79
    //   12701: aload #73
    //   12703: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12706: pop
    //   12707: aload #79
    //   12709: ldc_w '    Total running: '
    //   12712: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12715: pop
    //   12716: aload #79
    //   12718: lload #22
    //   12720: ldc2_w 500
    //   12723: ladd
    //   12724: ldc2_w 1000
    //   12727: ldiv
    //   12728: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   12731: aload #47
    //   12733: aload #79
    //   12735: invokevirtual toString : ()Ljava/lang/String;
    //   12738: invokevirtual println : (Ljava/lang/String;)V
    //   12741: aload #68
    //   12743: iload #4
    //   12745: invokevirtual getUserCpuTimeUs : (I)J
    //   12748: lstore #10
    //   12750: aload #68
    //   12752: iload #4
    //   12754: invokevirtual getSystemCpuTimeUs : (I)J
    //   12757: lstore #20
    //   12759: lload #10
    //   12761: lconst_0
    //   12762: lcmp
    //   12763: ifgt -> 12773
    //   12766: lload #20
    //   12768: lconst_0
    //   12769: lcmp
    //   12770: ifle -> 12837
    //   12773: aload #79
    //   12775: iconst_0
    //   12776: invokevirtual setLength : (I)V
    //   12779: aload #79
    //   12781: aload #73
    //   12783: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12786: pop
    //   12787: aload #79
    //   12789: ldc_w '    Total cpu time: u='
    //   12792: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12795: pop
    //   12796: aload #79
    //   12798: lload #10
    //   12800: ldc2_w 1000
    //   12803: ldiv
    //   12804: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   12807: aload #79
    //   12809: ldc_w 's='
    //   12812: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12815: pop
    //   12816: aload #79
    //   12818: lload #20
    //   12820: ldc2_w 1000
    //   12823: ldiv
    //   12824: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   12827: aload #47
    //   12829: aload #79
    //   12831: invokevirtual toString : ()Ljava/lang/String;
    //   12834: invokevirtual println : (Ljava/lang/String;)V
    //   12837: aload #68
    //   12839: iload #4
    //   12841: invokevirtual getCpuFreqTimes : (I)[J
    //   12844: astore #45
    //   12846: aload #45
    //   12848: ifnull -> 12939
    //   12851: aload #79
    //   12853: iconst_0
    //   12854: invokevirtual setLength : (I)V
    //   12857: aload #79
    //   12859: ldc_w '    Total cpu time per freq:'
    //   12862: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12865: pop
    //   12866: iconst_0
    //   12867: istore #36
    //   12869: iload #36
    //   12871: aload #45
    //   12873: arraylength
    //   12874: if_icmpge -> 12922
    //   12877: new java/lang/StringBuilder
    //   12880: dup
    //   12881: invokespecial <init> : ()V
    //   12884: astore #35
    //   12886: aload #35
    //   12888: aload #7
    //   12890: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12893: pop
    //   12894: aload #35
    //   12896: aload #45
    //   12898: iload #36
    //   12900: laload
    //   12901: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   12904: pop
    //   12905: aload #79
    //   12907: aload #35
    //   12909: invokevirtual toString : ()Ljava/lang/String;
    //   12912: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12915: pop
    //   12916: iinc #36, 1
    //   12919: goto -> 12869
    //   12922: aload #47
    //   12924: aload #79
    //   12926: invokevirtual toString : ()Ljava/lang/String;
    //   12929: invokevirtual println : (Ljava/lang/String;)V
    //   12932: lload #18
    //   12934: lstore #22
    //   12936: goto -> 12943
    //   12939: lload #18
    //   12941: lstore #22
    //   12943: aload #7
    //   12945: astore #35
    //   12947: aload #68
    //   12949: iload #4
    //   12951: invokevirtual getScreenOffCpuFreqTimes : (I)[J
    //   12954: astore #50
    //   12956: aload #50
    //   12958: ifnull -> 13049
    //   12961: aload #79
    //   12963: iconst_0
    //   12964: invokevirtual setLength : (I)V
    //   12967: aload #79
    //   12969: ldc_w '    Total screen-off cpu time per freq:'
    //   12972: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12975: pop
    //   12976: iconst_0
    //   12977: istore #36
    //   12979: lload #10
    //   12981: lstore #18
    //   12983: iload #36
    //   12985: aload #50
    //   12987: arraylength
    //   12988: if_icmpge -> 13036
    //   12991: new java/lang/StringBuilder
    //   12994: dup
    //   12995: invokespecial <init> : ()V
    //   12998: astore #7
    //   13000: aload #7
    //   13002: aload #35
    //   13004: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13007: pop
    //   13008: aload #7
    //   13010: aload #50
    //   13012: iload #36
    //   13014: laload
    //   13015: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   13018: pop
    //   13019: aload #79
    //   13021: aload #7
    //   13023: invokevirtual toString : ()Ljava/lang/String;
    //   13026: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13029: pop
    //   13030: iinc #36, 1
    //   13033: goto -> 12983
    //   13036: aload #47
    //   13038: aload #79
    //   13040: invokevirtual toString : ()Ljava/lang/String;
    //   13043: invokevirtual println : (Ljava/lang/String;)V
    //   13046: goto -> 13049
    //   13049: iconst_0
    //   13050: istore #36
    //   13052: lload #20
    //   13054: lstore #18
    //   13056: iload #36
    //   13058: bipush #7
    //   13060: if_icmpge -> 13348
    //   13063: aload #68
    //   13065: iload #4
    //   13067: iload #36
    //   13069: invokevirtual getCpuFreqTimes : (II)[J
    //   13072: astore #7
    //   13074: aload #7
    //   13076: ifnull -> 13204
    //   13079: aload #79
    //   13081: iconst_0
    //   13082: invokevirtual setLength : (I)V
    //   13085: new java/lang/StringBuilder
    //   13088: dup
    //   13089: invokespecial <init> : ()V
    //   13092: astore #72
    //   13094: aload #72
    //   13096: ldc_w '    Cpu times per freq at state '
    //   13099: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13102: pop
    //   13103: aload #72
    //   13105: getstatic android/os/BatteryStats$Uid.PROCESS_STATE_NAMES : [Ljava/lang/String;
    //   13108: iload #36
    //   13110: aaload
    //   13111: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13114: pop
    //   13115: aload #72
    //   13117: ldc_w ':'
    //   13120: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13123: pop
    //   13124: aload #79
    //   13126: aload #72
    //   13128: invokevirtual toString : ()Ljava/lang/String;
    //   13131: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13134: pop
    //   13135: iconst_0
    //   13136: istore #48
    //   13138: iload #48
    //   13140: aload #7
    //   13142: arraylength
    //   13143: if_icmpge -> 13191
    //   13146: new java/lang/StringBuilder
    //   13149: dup
    //   13150: invokespecial <init> : ()V
    //   13153: astore #72
    //   13155: aload #72
    //   13157: aload #35
    //   13159: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13162: pop
    //   13163: aload #72
    //   13165: aload #7
    //   13167: iload #48
    //   13169: laload
    //   13170: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   13173: pop
    //   13174: aload #79
    //   13176: aload #72
    //   13178: invokevirtual toString : ()Ljava/lang/String;
    //   13181: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13184: pop
    //   13185: iinc #48, 1
    //   13188: goto -> 13138
    //   13191: aload #47
    //   13193: aload #79
    //   13195: invokevirtual toString : ()Ljava/lang/String;
    //   13198: invokevirtual println : (Ljava/lang/String;)V
    //   13201: goto -> 13204
    //   13204: aload #68
    //   13206: iload #4
    //   13208: iload #36
    //   13210: invokevirtual getScreenOffCpuFreqTimes : (II)[J
    //   13213: astore #7
    //   13215: aload #7
    //   13217: ifnull -> 13342
    //   13220: aload #79
    //   13222: iconst_0
    //   13223: invokevirtual setLength : (I)V
    //   13226: new java/lang/StringBuilder
    //   13229: dup
    //   13230: invokespecial <init> : ()V
    //   13233: astore #72
    //   13235: aload #72
    //   13237: ldc_w '   Screen-off cpu times per freq at state '
    //   13240: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13243: pop
    //   13244: aload #72
    //   13246: getstatic android/os/BatteryStats$Uid.PROCESS_STATE_NAMES : [Ljava/lang/String;
    //   13249: iload #36
    //   13251: aaload
    //   13252: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13255: pop
    //   13256: aload #72
    //   13258: ldc_w ':'
    //   13261: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13264: pop
    //   13265: aload #79
    //   13267: aload #72
    //   13269: invokevirtual toString : ()Ljava/lang/String;
    //   13272: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13275: pop
    //   13276: iconst_0
    //   13277: istore #48
    //   13279: iload #48
    //   13281: aload #7
    //   13283: arraylength
    //   13284: if_icmpge -> 13332
    //   13287: new java/lang/StringBuilder
    //   13290: dup
    //   13291: invokespecial <init> : ()V
    //   13294: astore #72
    //   13296: aload #72
    //   13298: aload #35
    //   13300: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13303: pop
    //   13304: aload #72
    //   13306: aload #7
    //   13308: iload #48
    //   13310: laload
    //   13311: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   13314: pop
    //   13315: aload #79
    //   13317: aload #72
    //   13319: invokevirtual toString : ()Ljava/lang/String;
    //   13322: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13325: pop
    //   13326: iinc #48, 1
    //   13329: goto -> 13279
    //   13332: aload #47
    //   13334: aload #79
    //   13336: invokevirtual toString : ()Ljava/lang/String;
    //   13339: invokevirtual println : (Ljava/lang/String;)V
    //   13342: iinc #36, 1
    //   13345: goto -> 13056
    //   13348: aload #68
    //   13350: invokevirtual getProcessStats : ()Landroid/util/ArrayMap;
    //   13353: astore #72
    //   13355: aload #72
    //   13357: invokevirtual size : ()I
    //   13360: iconst_1
    //   13361: isub
    //   13362: istore #36
    //   13364: aload #34
    //   13366: astore #7
    //   13368: aload #45
    //   13370: astore #34
    //   13372: aload #72
    //   13374: astore #45
    //   13376: iload #36
    //   13378: iflt -> 13992
    //   13381: aload #45
    //   13383: iload #36
    //   13385: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   13388: checkcast android/os/BatteryStats$Uid$Proc
    //   13391: astore #72
    //   13393: aload #72
    //   13395: iload #4
    //   13397: invokevirtual getUserTime : (I)J
    //   13400: lstore #18
    //   13402: aload #72
    //   13404: iload #4
    //   13406: invokevirtual getSystemTime : (I)J
    //   13409: lstore #20
    //   13411: aload #72
    //   13413: iload #4
    //   13415: invokevirtual getForegroundTime : (I)J
    //   13418: lstore #10
    //   13420: aload #72
    //   13422: iload #4
    //   13424: invokevirtual getStarts : (I)I
    //   13427: istore #83
    //   13429: aload #72
    //   13431: iload #4
    //   13433: invokevirtual getNumCrashes : (I)I
    //   13436: istore #84
    //   13438: aload #72
    //   13440: iload #4
    //   13442: invokevirtual getNumAnrs : (I)I
    //   13445: istore #64
    //   13447: iload #4
    //   13449: ifne -> 13462
    //   13452: aload #72
    //   13454: invokevirtual countExcessivePowers : ()I
    //   13457: istore #48
    //   13459: goto -> 13465
    //   13462: iconst_0
    //   13463: istore #48
    //   13465: lload #18
    //   13467: lconst_0
    //   13468: lcmp
    //   13469: ifne -> 13522
    //   13472: lload #20
    //   13474: lconst_0
    //   13475: lcmp
    //   13476: ifne -> 13522
    //   13479: lload #10
    //   13481: lconst_0
    //   13482: lcmp
    //   13483: ifne -> 13522
    //   13486: iload #83
    //   13488: ifne -> 13522
    //   13491: iload #48
    //   13493: ifne -> 13522
    //   13496: iload #84
    //   13498: ifne -> 13522
    //   13501: iload #64
    //   13503: ifeq -> 13509
    //   13506: goto -> 13522
    //   13509: aload #7
    //   13511: astore #72
    //   13513: aload_1
    //   13514: astore #7
    //   13516: aload #72
    //   13518: astore_1
    //   13519: goto -> 13976
    //   13522: aload #79
    //   13524: iconst_0
    //   13525: invokevirtual setLength : (I)V
    //   13528: aload #79
    //   13530: aload #73
    //   13532: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13535: pop
    //   13536: aload #79
    //   13538: ldc_w '    Proc '
    //   13541: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13544: pop
    //   13545: aload #79
    //   13547: aload #45
    //   13549: iload #36
    //   13551: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   13554: checkcast java/lang/String
    //   13557: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13560: pop
    //   13561: aload #79
    //   13563: ldc_w ':\\n'
    //   13566: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13569: pop
    //   13570: aload #79
    //   13572: aload #73
    //   13574: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13577: pop
    //   13578: aload #79
    //   13580: ldc_w '      CPU: '
    //   13583: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13586: pop
    //   13587: aload #79
    //   13589: lload #18
    //   13591: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   13594: aload #79
    //   13596: ldc_w 'usr + '
    //   13599: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13602: pop
    //   13603: aload #79
    //   13605: lload #20
    //   13607: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   13610: aload #79
    //   13612: ldc_w 'krn ; '
    //   13615: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13618: pop
    //   13619: aload #79
    //   13621: lload #10
    //   13623: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   13626: aload #79
    //   13628: ldc 'fg'
    //   13630: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13633: pop
    //   13634: iload #83
    //   13636: ifne -> 13655
    //   13639: iload #84
    //   13641: ifne -> 13655
    //   13644: iload #64
    //   13646: ifeq -> 13652
    //   13649: goto -> 13655
    //   13652: goto -> 13788
    //   13655: aload #79
    //   13657: aload #7
    //   13659: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13662: pop
    //   13663: aload #79
    //   13665: aload #73
    //   13667: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13670: pop
    //   13671: aload #79
    //   13673: ldc_w '      '
    //   13676: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13679: pop
    //   13680: iconst_0
    //   13681: istore #37
    //   13683: iload #83
    //   13685: ifeq -> 13711
    //   13688: iconst_1
    //   13689: istore #37
    //   13691: aload #79
    //   13693: iload #83
    //   13695: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   13698: pop
    //   13699: aload #79
    //   13701: ldc_w ' starts'
    //   13704: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13707: pop
    //   13708: goto -> 13711
    //   13711: iload #84
    //   13713: ifeq -> 13754
    //   13716: iload #37
    //   13718: ifeq -> 13731
    //   13721: aload #79
    //   13723: aload_1
    //   13724: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13727: pop
    //   13728: goto -> 13731
    //   13731: iconst_1
    //   13732: istore #37
    //   13734: aload #79
    //   13736: iload #84
    //   13738: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   13741: pop
    //   13742: aload #79
    //   13744: ldc_w ' crashes'
    //   13747: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13750: pop
    //   13751: goto -> 13754
    //   13754: iload #64
    //   13756: ifeq -> 13788
    //   13759: iload #37
    //   13761: ifeq -> 13771
    //   13764: aload #79
    //   13766: aload_1
    //   13767: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13770: pop
    //   13771: aload #79
    //   13773: iload #64
    //   13775: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   13778: pop
    //   13779: aload #79
    //   13781: ldc_w ' anrs'
    //   13784: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   13787: pop
    //   13788: aload #7
    //   13790: astore #85
    //   13792: aload_1
    //   13793: astore #7
    //   13795: aload #47
    //   13797: aload #79
    //   13799: invokevirtual toString : ()Ljava/lang/String;
    //   13802: invokevirtual println : (Ljava/lang/String;)V
    //   13805: iconst_0
    //   13806: istore #84
    //   13808: aload #85
    //   13810: astore_1
    //   13811: iload #64
    //   13813: istore #37
    //   13815: iload #84
    //   13817: istore #64
    //   13819: iload #64
    //   13821: iload #48
    //   13823: if_icmpge -> 13973
    //   13826: aload #72
    //   13828: iload #64
    //   13830: invokevirtual getExcessivePower : (I)Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;
    //   13833: astore #85
    //   13835: aload #85
    //   13837: ifnull -> 13967
    //   13840: aload_2
    //   13841: aload_3
    //   13842: invokevirtual print : (Ljava/lang/String;)V
    //   13845: aload #47
    //   13847: ldc_w '      * Killed for '
    //   13850: invokevirtual print : (Ljava/lang/String;)V
    //   13853: aload #85
    //   13855: getfield type : I
    //   13858: iconst_2
    //   13859: if_icmpne -> 13872
    //   13862: aload #47
    //   13864: ldc 'cpu'
    //   13866: invokevirtual print : (Ljava/lang/String;)V
    //   13869: goto -> 13880
    //   13872: aload #47
    //   13874: ldc_w 'unknown'
    //   13877: invokevirtual print : (Ljava/lang/String;)V
    //   13880: aload #47
    //   13882: ldc_w ' use: '
    //   13885: invokevirtual print : (Ljava/lang/String;)V
    //   13888: aload #85
    //   13890: getfield usedTime : J
    //   13893: aload #47
    //   13895: invokestatic formatDuration : (JLjava/io/PrintWriter;)V
    //   13898: aload #47
    //   13900: ldc_w ' over '
    //   13903: invokevirtual print : (Ljava/lang/String;)V
    //   13906: aload #85
    //   13908: getfield overTime : J
    //   13911: aload #47
    //   13913: invokestatic formatDuration : (JLjava/io/PrintWriter;)V
    //   13916: aload #85
    //   13918: getfield overTime : J
    //   13921: lconst_0
    //   13922: lcmp
    //   13923: ifeq -> 13964
    //   13926: aload #47
    //   13928: aload #74
    //   13930: invokevirtual print : (Ljava/lang/String;)V
    //   13933: aload #47
    //   13935: aload #85
    //   13937: getfield usedTime : J
    //   13940: ldc2_w 100
    //   13943: lmul
    //   13944: aload #85
    //   13946: getfield overTime : J
    //   13949: ldiv
    //   13950: invokevirtual print : (J)V
    //   13953: aload #47
    //   13955: ldc_w '%)'
    //   13958: invokevirtual println : (Ljava/lang/String;)V
    //   13961: goto -> 13967
    //   13964: goto -> 13967
    //   13967: iinc #64, 1
    //   13970: goto -> 13819
    //   13973: iconst_1
    //   13974: istore #37
    //   13976: iinc #36, -1
    //   13979: aload #7
    //   13981: astore #72
    //   13983: aload_1
    //   13984: astore #7
    //   13986: aload #72
    //   13988: astore_1
    //   13989: goto -> 13376
    //   13992: aload #35
    //   13994: astore #72
    //   13996: aload #68
    //   13998: invokevirtual getPackageStats : ()Landroid/util/ArrayMap;
    //   14001: astore #7
    //   14003: aload #7
    //   14005: invokevirtual size : ()I
    //   14008: iconst_1
    //   14009: isub
    //   14010: istore #36
    //   14012: aload #49
    //   14014: astore_1
    //   14015: iload #37
    //   14017: istore #48
    //   14019: iload #36
    //   14021: iflt -> 14425
    //   14024: aload_2
    //   14025: aload_3
    //   14026: invokevirtual print : (Ljava/lang/String;)V
    //   14029: aload #47
    //   14031: ldc_w '    Apk '
    //   14034: invokevirtual print : (Ljava/lang/String;)V
    //   14037: aload #47
    //   14039: aload #7
    //   14041: iload #36
    //   14043: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   14046: checkcast java/lang/String
    //   14049: invokevirtual print : (Ljava/lang/String;)V
    //   14052: aload #47
    //   14054: ldc_w ':'
    //   14057: invokevirtual println : (Ljava/lang/String;)V
    //   14060: iconst_0
    //   14061: istore #37
    //   14063: aload #7
    //   14065: iload #36
    //   14067: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   14070: checkcast android/os/BatteryStats$Uid$Pkg
    //   14073: astore #49
    //   14075: aload #49
    //   14077: invokevirtual getWakeupAlarmStats : ()Landroid/util/ArrayMap;
    //   14080: astore #35
    //   14082: aload #35
    //   14084: invokevirtual size : ()I
    //   14087: iconst_1
    //   14088: isub
    //   14089: istore #48
    //   14091: iload #48
    //   14093: iflt -> 14167
    //   14096: aload_2
    //   14097: aload_3
    //   14098: invokevirtual print : (Ljava/lang/String;)V
    //   14101: aload #47
    //   14103: ldc_w '      Wakeup alarm '
    //   14106: invokevirtual print : (Ljava/lang/String;)V
    //   14109: aload #47
    //   14111: aload #35
    //   14113: iload #48
    //   14115: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   14118: checkcast java/lang/String
    //   14121: invokevirtual print : (Ljava/lang/String;)V
    //   14124: aload #47
    //   14126: aload_1
    //   14127: invokevirtual print : (Ljava/lang/String;)V
    //   14130: aload #47
    //   14132: aload #35
    //   14134: iload #48
    //   14136: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   14139: checkcast android/os/BatteryStats$Counter
    //   14142: iload #4
    //   14144: invokevirtual getCountLocked : (I)I
    //   14147: invokevirtual print : (I)V
    //   14150: aload #47
    //   14152: ldc_w ' times'
    //   14155: invokevirtual println : (Ljava/lang/String;)V
    //   14158: iconst_1
    //   14159: istore #37
    //   14161: iinc #48, -1
    //   14164: goto -> 14091
    //   14167: aload #49
    //   14169: invokevirtual getServiceStats : ()Landroid/util/ArrayMap;
    //   14172: astore #35
    //   14174: aload #35
    //   14176: invokevirtual size : ()I
    //   14179: iconst_1
    //   14180: isub
    //   14181: istore #48
    //   14183: iload #48
    //   14185: iflt -> 14398
    //   14188: aload #35
    //   14190: iload #48
    //   14192: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   14195: checkcast android/os/BatteryStats$Uid$Pkg$Serv
    //   14198: astore #45
    //   14200: aload #45
    //   14202: lload #24
    //   14204: iload #4
    //   14206: invokevirtual getStartTime : (JI)J
    //   14209: lstore #18
    //   14211: aload #45
    //   14213: iload #4
    //   14215: invokevirtual getStarts : (I)I
    //   14218: istore #84
    //   14220: aload #45
    //   14222: iload #4
    //   14224: invokevirtual getLaunches : (I)I
    //   14227: istore #64
    //   14229: lload #18
    //   14231: lconst_0
    //   14232: lcmp
    //   14233: ifne -> 14252
    //   14236: iload #84
    //   14238: ifne -> 14252
    //   14241: iload #64
    //   14243: ifeq -> 14249
    //   14246: goto -> 14252
    //   14249: goto -> 14392
    //   14252: aload #79
    //   14254: iconst_0
    //   14255: invokevirtual setLength : (I)V
    //   14258: aload #79
    //   14260: aload #73
    //   14262: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   14265: pop
    //   14266: aload #79
    //   14268: ldc_w '      Service '
    //   14271: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   14274: pop
    //   14275: aload #79
    //   14277: aload #35
    //   14279: iload #48
    //   14281: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   14284: checkcast java/lang/String
    //   14287: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   14290: pop
    //   14291: aload #79
    //   14293: ldc_w ':\\n'
    //   14296: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   14299: pop
    //   14300: aload #79
    //   14302: aload #73
    //   14304: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   14307: pop
    //   14308: aload #79
    //   14310: ldc_w '        Created for: '
    //   14313: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   14316: pop
    //   14317: aload #79
    //   14319: lload #18
    //   14321: ldc2_w 1000
    //   14324: ldiv
    //   14325: invokestatic formatTimeMs : (Ljava/lang/StringBuilder;J)V
    //   14328: aload #79
    //   14330: ldc_w 'uptime\\n'
    //   14333: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   14336: pop
    //   14337: aload #79
    //   14339: aload #73
    //   14341: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   14344: pop
    //   14345: aload #79
    //   14347: ldc_w '        Starts: '
    //   14350: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   14353: pop
    //   14354: aload #79
    //   14356: iload #84
    //   14358: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   14361: pop
    //   14362: aload #79
    //   14364: ldc_w ', launches: '
    //   14367: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   14370: pop
    //   14371: aload #79
    //   14373: iload #64
    //   14375: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   14378: pop
    //   14379: aload #47
    //   14381: aload #79
    //   14383: invokevirtual toString : ()Ljava/lang/String;
    //   14386: invokevirtual println : (Ljava/lang/String;)V
    //   14389: iconst_1
    //   14390: istore #37
    //   14392: iinc #48, -1
    //   14395: goto -> 14183
    //   14398: iload #37
    //   14400: ifne -> 14416
    //   14403: aload_2
    //   14404: aload_3
    //   14405: invokevirtual print : (Ljava/lang/String;)V
    //   14408: aload #47
    //   14410: ldc_w '      (nothing executed)'
    //   14413: invokevirtual println : (Ljava/lang/String;)V
    //   14416: iconst_1
    //   14417: istore #48
    //   14419: iinc #36, -1
    //   14422: goto -> 14019
    //   14425: iload #4
    //   14427: istore #64
    //   14429: lload #24
    //   14431: lstore #10
    //   14433: aload_1
    //   14434: astore #68
    //   14436: aload #73
    //   14438: astore #34
    //   14440: aload #68
    //   14442: astore #50
    //   14444: aload #47
    //   14446: astore #7
    //   14448: iload #64
    //   14450: istore #37
    //   14452: lload #10
    //   14454: lstore #20
    //   14456: aload #71
    //   14458: astore_1
    //   14459: aload #74
    //   14461: astore #45
    //   14463: aload #67
    //   14465: astore #49
    //   14467: iload #44
    //   14469: istore #36
    //   14471: lload #22
    //   14473: lstore #18
    //   14475: lload #16
    //   14477: lstore #24
    //   14479: aload #72
    //   14481: astore #35
    //   14483: iload #48
    //   14485: ifne -> 14548
    //   14488: aload_2
    //   14489: aload_3
    //   14490: invokevirtual print : (Ljava/lang/String;)V
    //   14493: aload #47
    //   14495: ldc_w '    (nothing executed)'
    //   14498: invokevirtual println : (Ljava/lang/String;)V
    //   14501: aload #72
    //   14503: astore #35
    //   14505: lload #16
    //   14507: lstore #24
    //   14509: lload #22
    //   14511: lstore #18
    //   14513: iload #44
    //   14515: istore #36
    //   14517: aload #67
    //   14519: astore #49
    //   14521: aload #74
    //   14523: astore #45
    //   14525: aload #71
    //   14527: astore_1
    //   14528: lload #10
    //   14530: lstore #20
    //   14532: iload #64
    //   14534: istore #37
    //   14536: aload #47
    //   14538: astore #7
    //   14540: aload #68
    //   14542: astore #50
    //   14544: aload #73
    //   14546: astore #34
    //   14548: aload #42
    //   14550: astore #71
    //   14552: aload #50
    //   14554: astore #42
    //   14556: iload #37
    //   14558: istore #44
    //   14560: aload #51
    //   14562: astore #74
    //   14564: aload #45
    //   14566: astore #51
    //   14568: aload #49
    //   14570: astore #67
    //   14572: iload #36
    //   14574: istore #37
    //   14576: lload #18
    //   14578: lstore #22
    //   14580: lload #24
    //   14582: lstore #18
    //   14584: iinc #60, 1
    //   14587: aload_1
    //   14588: astore #49
    //   14590: iload #44
    //   14592: istore #36
    //   14594: aload #35
    //   14596: astore #45
    //   14598: aload #7
    //   14600: astore #50
    //   14602: lload #20
    //   14604: lstore #24
    //   14606: aload #51
    //   14608: astore #35
    //   14610: aload #74
    //   14612: astore #51
    //   14614: aload #42
    //   14616: astore #7
    //   14618: aload #71
    //   14620: astore #42
    //   14622: aload #67
    //   14624: astore_1
    //   14625: goto -> 7867
    //   14628: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4489	-> 0
    //   #4490	-> 8
    //   #4492	-> 47
    //   #4495	-> 48
    //   #4496	-> 57
    //   #4497	-> 66
    //   #4498	-> 78
    //   #4500	-> 86
    //   #4501	-> 96
    //   #4502	-> 106
    //   #4503	-> 116
    //   #4504	-> 126
    //   #4505	-> 136
    //   #4507	-> 146
    //   #4508	-> 154
    //   #4509	-> 162
    //   #4511	-> 172
    //   #4513	-> 184
    //   #4514	-> 190
    //   #4516	-> 197
    //   #4517	-> 203
    //   #4518	-> 208
    //   #4519	-> 214
    //   #4520	-> 222
    //   #4521	-> 231
    //   #4522	-> 243
    //   #4523	-> 252
    //   #4517	-> 264
    //   #4526	-> 264
    //   #4527	-> 270
    //   #4528	-> 275
    //   #4529	-> 281
    //   #4530	-> 289
    //   #4531	-> 298
    //   #4532	-> 314
    //   #4533	-> 323
    //   #4535	-> 332
    //   #4536	-> 338
    //   #4537	-> 343
    //   #4538	-> 349
    //   #4539	-> 357
    //   #4540	-> 366
    //   #4541	-> 382
    //   #4542	-> 391
    //   #4536	-> 403
    //   #4545	-> 403
    //   #4546	-> 409
    //   #4547	-> 417
    //   #4548	-> 426
    //   #4549	-> 446
    //   #4550	-> 460
    //   #4551	-> 469
    //   #4552	-> 480
    //   #4553	-> 503
    //   #4554	-> 512
    //   #4556	-> 521
    //   #4557	-> 527
    //   #4558	-> 535
    //   #4559	-> 544
    //   #4560	-> 564
    //   #4561	-> 578
    //   #4562	-> 587
    //   #4563	-> 598
    //   #4564	-> 607
    //   #4565	-> 621
    //   #4566	-> 630
    //   #4568	-> 639
    //   #4569	-> 645
    //   #4570	-> 653
    //   #4571	-> 662
    //   #4572	-> 682
    //   #4573	-> 696
    //   #4574	-> 705
    //   #4576	-> 714
    //   #4577	-> 720
    //   #4578	-> 728
    //   #4579	-> 737
    //   #4580	-> 748
    //   #4581	-> 757
    //   #4582	-> 768
    //   #4583	-> 777
    //   #4584	-> 786
    //   #4585	-> 793
    //   #4586	-> 799
    //   #4587	-> 807
    //   #4588	-> 816
    //   #4589	-> 827
    //   #4591	-> 836
    //   #4592	-> 843
    //   #4593	-> 849
    //   #4594	-> 857
    //   #4595	-> 866
    //   #4596	-> 877
    //   #4599	-> 886
    //   #4600	-> 894
    //   #4601	-> 901
    //   #4602	-> 907
    //   #4603	-> 915
    //   #4604	-> 924
    //   #4605	-> 940
    //   #4606	-> 949
    //   #4600	-> 961
    //   #4609	-> 961
    //   #4610	-> 969
    //   #4611	-> 976
    //   #4612	-> 982
    //   #4613	-> 990
    //   #4614	-> 999
    //   #4615	-> 1015
    //   #4616	-> 1024
    //   #4610	-> 1036
    //   #4619	-> 1036
    //   #4620	-> 1044
    //   #4621	-> 1051
    //   #4622	-> 1057
    //   #4623	-> 1065
    //   #4624	-> 1074
    //   #4625	-> 1090
    //   #4626	-> 1099
    //   #4620	-> 1111
    //   #4629	-> 1111
    //   #4630	-> 1118
    //   #4631	-> 1125
    //   #4632	-> 1131
    //   #4633	-> 1139
    //   #4634	-> 1148
    //   #4635	-> 1164
    //   #4636	-> 1173
    //   #4630	-> 1185
    //   #4639	-> 1185
    //   #4640	-> 1193
    //   #4641	-> 1200
    //   #4642	-> 1206
    //   #4643	-> 1214
    //   #4644	-> 1223
    //   #4645	-> 1239
    //   #4646	-> 1248
    //   #4640	-> 1260
    //   #4649	-> 1260
    //   #4650	-> 1268
    //   #4651	-> 1275
    //   #4652	-> 1281
    //   #4653	-> 1289
    //   #4654	-> 1298
    //   #4655	-> 1314
    //   #4656	-> 1323
    //   #4650	-> 1335
    //   #4659	-> 1335
    //   #4660	-> 1342
    //   #4662	-> 1361
    //   #4663	-> 1371
    //   #4664	-> 1381
    //   #4665	-> 1391
    //   #4667	-> 1402
    //   #4669	-> 1413
    //   #4671	-> 1424
    //   #4673	-> 1435
    //   #4674	-> 1445
    //   #4675	-> 1454
    //   #4676	-> 1463
    //   #4677	-> 1469
    //   #4678	-> 1477
    //   #4679	-> 1497
    //   #4680	-> 1525
    //   #4681	-> 1546
    //   #4682	-> 1566
    //   #4683	-> 1589
    //   #4684	-> 1603
    //   #4685	-> 1612
    //   #4686	-> 1618
    //   #4687	-> 1626
    //   #4688	-> 1635
    //   #4689	-> 1638
    //   #4690	-> 1647
    //   #4691	-> 1659
    //   #4692	-> 1666
    //   #4694	-> 1669
    //   #4695	-> 1678
    //   #4696	-> 1686
    //   #4697	-> 1686
    //   #4698	-> 1698
    //   #4699	-> 1707
    //   #4700	-> 1718
    //   #4701	-> 1727
    //   #4702	-> 1741
    //   #4689	-> 1752
    //   #4704	-> 1762
    //   #4705	-> 1776
    //   #4706	-> 1785
    //   #4707	-> 1792
    //   #4708	-> 1798
    //   #4709	-> 1806
    //   #4710	-> 1815
    //   #4711	-> 1826
    //   #4712	-> 1835
    //   #4713	-> 1849
    //   #4714	-> 1857
    //   #4706	-> 1869
    //   #4716	-> 1869
    //   #4717	-> 1881
    //   #4718	-> 1887
    //   #4719	-> 1895
    //   #4720	-> 1904
    //   #4721	-> 1915
    //   #4722	-> 1924
    //   #4723	-> 1938
    //   #4724	-> 1960
    //   #4725	-> 1969
    //   #4716	-> 1981
    //   #4727	-> 1981
    //   #4728	-> 1988
    //   #4729	-> 1994
    //   #4730	-> 2002
    //   #4731	-> 2011
    //   #4732	-> 2022
    //   #4733	-> 2031
    //   #4734	-> 2045
    //   #4735	-> 2054
    //   #4736	-> 2067
    //   #4737	-> 2076
    //   #4738	-> 2085
    //   #4739	-> 2095
    //   #4727	-> 2107
    //   #4741	-> 2107
    //   #4742	-> 2114
    //   #4743	-> 2120
    //   #4744	-> 2128
    //   #4745	-> 2137
    //   #4746	-> 2148
    //   #4747	-> 2157
    //   #4748	-> 2171
    //   #4749	-> 2193
    //   #4750	-> 2202
    //   #4752	-> 2211
    //   #4753	-> 2218
    //   #4754	-> 2224
    //   #4755	-> 2232
    //   #4756	-> 2241
    //   #4757	-> 2252
    //   #4758	-> 2261
    //   #4759	-> 2275
    //   #4760	-> 2284
    //   #4761	-> 2297
    //   #4762	-> 2306
    //   #4763	-> 2315
    //   #4764	-> 2325
    //   #4766	-> 2334
    //   #4767	-> 2341
    //   #4768	-> 2347
    //   #4769	-> 2355
    //   #4770	-> 2375
    //   #4771	-> 2398
    //   #4766	-> 2431
    //   #4773	-> 2431
    //   #4774	-> 2439
    //   #4775	-> 2444
    //   #4776	-> 2449
    //   #4774	-> 2465
    //   #4780	-> 2465
    //   #4781	-> 2468
    //   #4783	-> 2471
    //   #4785	-> 2480
    //   #4786	-> 2494
    //   #4788	-> 2510
    //   #4789	-> 2510
    //   #4790	-> 2517
    //   #4791	-> 2535
    //   #4793	-> 2547
    //   #4794	-> 2555
    //   #4795	-> 2564
    //   #4799	-> 2578
    //   #4800	-> 2586
    //   #4801	-> 2595
    //   #4803	-> 2606
    //   #4804	-> 2617
    //   #4808	-> 2622
    //   #4811	-> 2654
    //   #4790	-> 2661
    //   #4785	-> 2675
    //   #4817	-> 2693
    //   #4818	-> 2702
    //   #4819	-> 2711
    //   #4820	-> 2720
    //   #4821	-> 2729
    //   #4822	-> 2738
    //   #4823	-> 2747
    //   #4824	-> 2756
    //   #4825	-> 2765
    //   #4826	-> 2774
    //   #4828	-> 2783
    //   #4829	-> 2790
    //   #4830	-> 2796
    //   #4831	-> 2804
    //   #4833	-> 2828
    //   #4828	-> 2840
    //   #4836	-> 2840
    //   #4837	-> 2847
    //   #4838	-> 2853
    //   #4839	-> 2861
    //   #4841	-> 2885
    //   #4844	-> 2894
    //   #4845	-> 2894
    //   #4846	-> 2904
    //   #4847	-> 2912
    //   #4848	-> 2919
    //   #4849	-> 2925
    //   #4850	-> 2933
    //   #4851	-> 2942
    //   #4852	-> 2950
    //   #4854	-> 2959
    //   #4855	-> 2965
    //   #4856	-> 2973
    //   #4857	-> 2982
    //   #4858	-> 2997
    //   #4861	-> 3006
    //   #4862	-> 3013
    //   #4863	-> 3018
    //   #4864	-> 3024
    //   #4865	-> 3032
    //   #4866	-> 3041
    //   #4868	-> 3050
    //   #4869	-> 3055
    //   #4870	-> 3061
    //   #4871	-> 3069
    //   #4872	-> 3078
    //   #4873	-> 3089
    //   #4875	-> 3098
    //   #4876	-> 3104
    //   #4877	-> 3112
    //   #4878	-> 3121
    //   #4880	-> 3130
    //   #4881	-> 3135
    //   #4882	-> 3141
    //   #4883	-> 3149
    //   #4884	-> 3158
    //   #4885	-> 3168
    //   #4886	-> 3179
    //   #4887	-> 3201
    //   #4888	-> 3209
    //   #4890	-> 3218
    //   #4891	-> 3218
    //   #4890	-> 3224
    //   #4893	-> 3260
    //   #4894	-> 3277
    //   #4895	-> 3294
    //   #4896	-> 3311
    //   #4898	-> 3324
    //   #4899	-> 3330
    //   #4900	-> 3338
    //   #4901	-> 3347
    //   #4902	-> 3350
    //   #4903	-> 3369
    //   #4904	-> 3381
    //   #4905	-> 3388
    //   #4907	-> 3391
    //   #4908	-> 3400
    //   #4909	-> 3408
    //   #4910	-> 3411
    //   #4911	-> 3447
    //   #4912	-> 3456
    //   #4913	-> 3467
    //   #4914	-> 3475
    //   #4915	-> 3489
    //   #4902	-> 3497
    //   #4917	-> 3523
    //   #4918	-> 3540
    //   #4920	-> 3549
    //   #4921	-> 3555
    //   #4922	-> 3563
    //   #4923	-> 3572
    //   #4929	-> 3613
    //   #4930	-> 3613
    //   #4932	-> 3624
    //   #4933	-> 3645
    //   #4934	-> 3657
    //   #4935	-> 3664
    //   #4937	-> 3667
    //   #4938	-> 3676
    //   #4939	-> 3684
    //   #4940	-> 3684
    //   #4941	-> 3698
    //   #4942	-> 3706
    //   #4943	-> 3717
    //   #4944	-> 3725
    //   #4945	-> 3739
    //   #4932	-> 3747
    //   #4947	-> 3753
    //   #4948	-> 3770
    //   #4950	-> 3779
    //   #4951	-> 3784
    //   #4952	-> 3790
    //   #4953	-> 3798
    //   #4954	-> 3807
    //   #4956	-> 3816
    //   #4957	-> 3821
    //   #4958	-> 3827
    //   #4959	-> 3835
    //   #4960	-> 3844
    //   #4961	-> 3854
    //   #4962	-> 3865
    //   #4963	-> 3887
    //   #4964	-> 3895
    //   #4966	-> 3904
    //   #4967	-> 3904
    //   #4966	-> 3910
    //   #4969	-> 3933
    //   #4970	-> 3950
    //   #4971	-> 3971
    //   #4972	-> 3984
    //   #4974	-> 3997
    //   #4975	-> 4003
    //   #4976	-> 4011
    //   #4977	-> 4020
    //   #4978	-> 4023
    //   #4979	-> 4041
    //   #4980	-> 4053
    //   #4981	-> 4060
    //   #4983	-> 4063
    //   #4984	-> 4072
    //   #4985	-> 4072
    //   #4986	-> 4084
    //   #4987	-> 4092
    //   #4988	-> 4103
    //   #4989	-> 4111
    //   #4990	-> 4125
    //   #4978	-> 4136
    //   #4992	-> 4150
    //   #4993	-> 4167
    //   #4995	-> 4181
    //   #4996	-> 4187
    //   #4997	-> 4195
    //   #4998	-> 4204
    //   #4999	-> 4207
    //   #5000	-> 4221
    //   #5001	-> 4233
    //   #5002	-> 4240
    //   #5004	-> 4243
    //   #5005	-> 4252
    //   #5006	-> 4252
    //   #5007	-> 4264
    //   #5008	-> 4272
    //   #5009	-> 4283
    //   #5010	-> 4291
    //   #5011	-> 4305
    //   #4999	-> 4316
    //   #5013	-> 4322
    //   #5014	-> 4338
    //   #5016	-> 4351
    //   #5017	-> 4357
    //   #5018	-> 4365
    //   #5019	-> 4374
    //   #5025	-> 4410
    //   #5026	-> 4410
    //   #5028	-> 4419
    //   #5029	-> 4440
    //   #5030	-> 4452
    //   #5031	-> 4459
    //   #5033	-> 4462
    //   #5034	-> 4471
    //   #5035	-> 4479
    //   #5036	-> 4479
    //   #5037	-> 4491
    //   #5038	-> 4502
    //   #5039	-> 4513
    //   #5040	-> 4521
    //   #5041	-> 4535
    //   #5028	-> 4543
    //   #5043	-> 4557
    //   #5044	-> 4570
    //   #5046	-> 4579
    //   #5047	-> 4584
    //   #5048	-> 4590
    //   #5049	-> 4598
    //   #5050	-> 4607
    //   #5052	-> 4616
    //   #5053	-> 4622
    //   #5054	-> 4630
    //   #5055	-> 4639
    //   #5058	-> 4657
    //   #5060	-> 4666
    //   #5061	-> 4676
    //   #5062	-> 4688
    //   #5063	-> 4697
    //   #5064	-> 4705
    //   #5065	-> 4714
    //   #5066	-> 4725
    //   #5067	-> 4736
    //   #5068	-> 4744
    //   #5069	-> 4758
    //   #5060	-> 4766
    //   #5071	-> 4776
    //   #5073	-> 4785
    //   #5074	-> 4791
    //   #5075	-> 4798
    //   #5076	-> 4803
    //   #5077	-> 4809
    //   #5078	-> 4817
    //   #5079	-> 4826
    //   #5081	-> 4852
    //   #5082	-> 4861
    //   #5085	-> 4870
    //   #5086	-> 4875
    //   #5087	-> 4881
    //   #5088	-> 4889
    //   #5089	-> 4898
    //   #5090	-> 4907
    //   #5092	-> 4914
    //   #5093	-> 4919
    //   #5094	-> 4936
    //   #5096	-> 4953
    //   #5097	-> 4971
    //   #5098	-> 4977
    //   #5099	-> 4985
    //   #5100	-> 5001
    //   #5102	-> 5010
    //   #5105	-> 5031
    //   #5107	-> 5035
    //   #5108	-> 5047
    //   #5109	-> 5059
    //   #5110	-> 5067
    //   #5111	-> 5079
    //   #5112	-> 5087
    //   #5113	-> 5099
    //   #5114	-> 5107
    //   #5115	-> 5119
    //   #5116	-> 5127
    //   #5117	-> 5139
    //   #5118	-> 5147
    //   #5120	-> 5151
    //   #5121	-> 5163
    //   #5122	-> 5168
    //   #5123	-> 5175
    //   #5124	-> 5181
    //   #5125	-> 5206
    //   #5126	-> 5218
    //   #5127	-> 5230
    //   #5128	-> 5242
    //   #5129	-> 5258
    //   #5130	-> 5274
    //   #5131	-> 5286
    //   #5133	-> 5302
    //   #5134	-> 5306
    //   #5135	-> 5321
    //   #5136	-> 5335
    //   #5137	-> 5340
    //   #5181	-> 5420
    //   #5178	-> 5430
    //   #5179	-> 5437
    //   #5175	-> 5440
    //   #5176	-> 5447
    //   #5172	-> 5450
    //   #5173	-> 5457
    //   #5168	-> 5460
    //   #5169	-> 5476
    //   #5170	-> 5483
    //   #5163	-> 5486
    //   #5164	-> 5493
    //   #5165	-> 5505
    //   #5166	-> 5512
    //   #5160	-> 5515
    //   #5161	-> 5522
    //   #5157	-> 5525
    //   #5158	-> 5532
    //   #5154	-> 5535
    //   #5155	-> 5542
    //   #5151	-> 5545
    //   #5152	-> 5552
    //   #5148	-> 5555
    //   #5149	-> 5562
    //   #5145	-> 5565
    //   #5146	-> 5572
    //   #5142	-> 5575
    //   #5143	-> 5582
    //   #5139	-> 5585
    //   #5140	-> 5592
    //   #5184	-> 5592
    //   #5186	-> 5602
    //   #5190	-> 5616
    //   #5191	-> 5623
    //   #5192	-> 5633
    //   #5193	-> 5640
    //   #5195	-> 5650
    //   #5196	-> 5660
    //   #5197	-> 5667
    //   #5199	-> 5677
    //   #5200	-> 5687
    //   #5201	-> 5694
    //   #5203	-> 5704
    //   #5204	-> 5714
    //   #5205	-> 5721
    //   #5207	-> 5731
    //   #5208	-> 5741
    //   #5209	-> 5748
    //   #5211	-> 5758
    //   #5212	-> 5768
    //   #5213	-> 5775
    //   #5215	-> 5785
    //   #5216	-> 5795
    //   #5217	-> 5802
    //   #5219	-> 5812
    //   #5220	-> 5822
    //   #5221	-> 5829
    //   #5223	-> 5839
    //   #5224	-> 5849
    //   #5225	-> 5856
    //   #5227	-> 5866
    //   #5228	-> 5876
    //   #5229	-> 5883
    //   #5231	-> 5893
    //   #5235	-> 5900
    //   #5236	-> 5914
    //   #5237	-> 5921
    //   #5238	-> 5931
    //   #5239	-> 5938
    //   #5240	-> 5948
    //   #5241	-> 5955
    //   #5243	-> 5965
    //   #5244	-> 5975
    //   #5245	-> 5982
    //   #5247	-> 5992
    //   #5249	-> 5999
    //   #5250	-> 6007
    //   #5253	-> 6014
    //   #5134	-> 6018
    //   #5255	-> 6024
    //   #5124	-> 6031
    //   #5258	-> 6031
    //   #5259	-> 6037
    //   #5260	-> 6052
    //   #5261	-> 6064
    //   #5262	-> 6067
    //   #5263	-> 6086
    //   #5264	-> 6100
    //   #5265	-> 6106
    //   #5266	-> 6123
    //   #5267	-> 6136
    //   #5268	-> 6159
    //   #5269	-> 6184
    //   #5270	-> 6203
    //   #5271	-> 6230
    //   #5272	-> 6239
    //   #5262	-> 6249
    //   #5274	-> 6259
    //   #5275	-> 6265
    //   #5276	-> 6273
    //   #5277	-> 6282
    //   #5278	-> 6289
    //   #5279	-> 6311
    //   #5280	-> 6319
    //   #5281	-> 6328
    //   #5259	-> 6341
    //   #5284	-> 6352
    //   #5299	-> 6362
    //   #5300	-> 6372
    //   #5301	-> 6372
    //   #5302	-> 6378
    //   #5303	-> 6388
    //   #5305	-> 6397
    //   #5306	-> 6433
    //   #5307	-> 6445
    //   #5308	-> 6456
    //   #5309	-> 6463
    //   #5308	-> 6494
    //   #5311	-> 6494
    //   #5312	-> 6497
    //   #5313	-> 6521
    //   #5314	-> 6528
    //   #5315	-> 6540
    //   #5316	-> 6569
    //   #5317	-> 6581
    //   #5318	-> 6581
    //   #5319	-> 6587
    //   #5320	-> 6595
    //   #5321	-> 6604
    //   #5322	-> 6615
    //   #5324	-> 6635
    //   #5325	-> 6645
    //   #5327	-> 6653
    //   #5324	-> 6665
    //   #5315	-> 6665
    //   #5330	-> 6681
    //   #5312	-> 6704
    //   #5302	-> 6733
    //   #5334	-> 6768
    //   #5335	-> 6776
    //   #5336	-> 6787
    //   #5337	-> 6799
    //   #5338	-> 6812
    //   #5339	-> 6824
    //   #5340	-> 6830
    //   #5341	-> 6839
    //   #5342	-> 6849
    //   #5343	-> 6857
    //   #5344	-> 6868
    //   #5345	-> 6887
    //   #5346	-> 6895
    //   #5337	-> 6904
    //   #5348	-> 6916
    //   #5349	-> 6921
    //   #5334	-> 6936
    //   #5352	-> 6946
    //   #5353	-> 6952
    //   #5354	-> 6962
    //   #5355	-> 6974
    //   #5356	-> 6983
    //   #5357	-> 7019
    //   #5358	-> 7031
    //   #5359	-> 7043
    //   #5358	-> 7065
    //   #5360	-> 7073
    //   #5361	-> 7076
    //   #5362	-> 7083
    //   #5363	-> 7100
    //   #5364	-> 7112
    //   #5365	-> 7112
    //   #5366	-> 7118
    //   #5367	-> 7126
    //   #5368	-> 7135
    //   #5369	-> 7146
    //   #5370	-> 7165
    //   #5371	-> 7172
    //   #5362	-> 7181
    //   #5373	-> 7187
    //   #5353	-> 7210
    //   #5299	-> 7229
    //   #5377	-> 7271
    //   #5378	-> 7277
    //   #5379	-> 7285
    //   #5380	-> 7292
    //   #5381	-> 7305
    //   #5382	-> 7311
    //   #5383	-> 7320
    //   #5384	-> 7333
    //   #5385	-> 7342
    //   #5386	-> 7365
    //   #5380	-> 7374
    //   #5388	-> 7380
    //   #5378	-> 7387
    //   #5391	-> 7387
    //   #5392	-> 7393
    //   #5393	-> 7403
    //   #5394	-> 7415
    //   #5395	-> 7425
    //   #5396	-> 7465
    //   #5397	-> 7477
    //   #5398	-> 7489
    //   #5399	-> 7505
    //   #5395	-> 7512
    //   #5394	-> 7585
    //   #5401	-> 7643
    //   #5392	-> 7657
    //   #5419	-> 7720
    //   #5420	-> 7729
    //   #5421	-> 7734
    //   #5422	-> 7740
    //   #5423	-> 7749
    //   #5424	-> 7760
    //   #5423	-> 7799
    //   #5426	-> 7805
    //   #5427	-> 7814
    //   #5420	-> 7821
    //   #5430	-> 7821
    //   #5431	-> 7874
    //   #5432	-> 7883
    //   #5433	-> 7903
    //   #5436	-> 7977
    //   #5438	-> 7989
    //   #5439	-> 7994
    //   #5440	-> 8002
    //   #5441	-> 8009
    //   #5442	-> 8017
    //   #5444	-> 8017
    //   #5445	-> 8027
    //   #5446	-> 8037
    //   #5447	-> 8047
    //   #5448	-> 8057
    //   #5449	-> 8067
    //   #5451	-> 8077
    //   #5452	-> 8087
    //   #5453	-> 8097
    //   #5454	-> 8107
    //   #5456	-> 8117
    //   #5457	-> 8126
    //   #5459	-> 8135
    //   #5460	-> 8146
    //   #5461	-> 8157
    //   #5462	-> 8166
    //   #5464	-> 8175
    //   #5465	-> 8184
    //   #5466	-> 8193
    //   #5468	-> 8204
    //   #5469	-> 8213
    //   #5471	-> 8222
    //   #5473	-> 8256
    //   #5474	-> 8268
    //   #5475	-> 8285
    //   #5476	-> 8295
    //   #5477	-> 8308
    //   #5479	-> 8328
    //   #5480	-> 8346
    //   #5481	-> 8352
    //   #5482	-> 8368
    //   #5483	-> 8379
    //   #5484	-> 8387
    //   #5485	-> 8401
    //   #5486	-> 8425
    //   #5487	-> 8432
    //   #5488	-> 8439
    //   #5487	-> 8445
    //   #5490	-> 8445
    //   #5491	-> 8454
    //   #5492	-> 8474
    //   #5493	-> 8483
    //   #5496	-> 8492
    //   #5497	-> 8503
    //   #5498	-> 8509
    //   #5499	-> 8516
    //   #5500	-> 8525
    //   #5501	-> 8533
    //   #5496	-> 8545
    //   #5504	-> 8545
    //   #5505	-> 8577
    //   #5504	-> 8584
    //   #5507	-> 8603
    //   #5508	-> 8641
    //   #5509	-> 8653
    //   #5510	-> 8670
    //   #5511	-> 8680
    //   #5512	-> 8693
    //   #5515	-> 8717
    //   #5518	-> 8785
    //   #5519	-> 8791
    //   #5520	-> 8807
    //   #5521	-> 8818
    //   #5522	-> 8840
    //   #5523	-> 8849
    //   #5524	-> 8865
    //   #5525	-> 8880
    //   #5526	-> 8902
    //   #5527	-> 8911
    //   #5528	-> 8927
    //   #5529	-> 8938
    //   #5530	-> 8960
    //   #5531	-> 8968
    //   #5532	-> 8976
    //   #5534	-> 8985
    //   #5535	-> 9001
    //   #5536	-> 9012
    //   #5537	-> 9020
    //   #5536	-> 9029
    //   #5538	-> 9043
    //   #5539	-> 9051
    //   #5540	-> 9059
    //   #5541	-> 9068
    //   #5542	-> 9084
    //   #5543	-> 9095
    //   #5544	-> 9103
    //   #5543	-> 9112
    //   #5545	-> 9130
    //   #5546	-> 9138
    //   #5547	-> 9146
    //   #5548	-> 9154
    //   #5551	-> 9171
    //   #5552	-> 9181
    //   #5553	-> 9187
    //   #5554	-> 9194
    //   #5555	-> 9203
    //   #5556	-> 9211
    //   #5559	-> 9221
    //   #5560	-> 9253
    //   #5559	-> 9260
    //   #5562	-> 9276
    //   #5563	-> 9296
    //   #5564	-> 9309
    //   #5565	-> 9328
    //   #5566	-> 9339
    //   #5569	-> 9347
    //   #5570	-> 9354
    //   #5572	-> 9364
    //   #5574	-> 9383
    //   #5575	-> 9390
    //   #5576	-> 9399
    //   #5577	-> 9406
    //   #5579	-> 9426
    //   #5580	-> 9435
    //   #5581	-> 9440
    //   #5583	-> 9455
    //   #5584	-> 9463
    //   #5585	-> 9481
    //   #5586	-> 9489
    //   #5588	-> 9507
    //   #5589	-> 9514
    //   #5590	-> 9519
    //   #5591	-> 9534
    //   #5592	-> 9539
    //   #5594	-> 9554
    //   #5595	-> 9554
    //   #5596	-> 9561
    //   #5597	-> 9566
    //   #5598	-> 9581
    //   #5599	-> 9586
    //   #5601	-> 9601
    //   #5602	-> 9607
    //   #5603	-> 9615
    //   #5604	-> 9622
    //   #5605	-> 9631
    //   #5606	-> 9638
    //   #5607	-> 9646
    //   #5608	-> 9654
    //   #5609	-> 9663
    //   #5610	-> 9671
    //   #5612	-> 9680
    //   #5602	-> 9692
    //   #5615	-> 9692
    //   #5616	-> 9699
    //   #5617	-> 9708
    //   #5618	-> 9715
    //   #5619	-> 9723
    //   #5620	-> 9731
    //   #5621	-> 9740
    //   #5622	-> 9748
    //   #5624	-> 9757
    //   #5625	-> 9766
    //   #5626	-> 9784
    //   #5627	-> 9791
    //   #5628	-> 9800
    //   #5629	-> 9807
    //   #5630	-> 9815
    //   #5631	-> 9823
    //   #5632	-> 9832
    //   #5633	-> 9845
    //   #5635	-> 9854
    //   #5638	-> 9863
    //   #5639	-> 9870
    //   #5640	-> 9879
    //   #5641	-> 9887
    //   #5642	-> 9895
    //   #5643	-> 9903
    //   #5645	-> 9912
    //   #5646	-> 9932
    //   #5647	-> 9941
    //   #5648	-> 9948
    //   #5649	-> 9957
    //   #5650	-> 9964
    //   #5651	-> 9973
    //   #5652	-> 9980
    //   #5653	-> 9987
    //   #5654	-> 9992
    //   #5655	-> 10000
    //   #5654	-> 10012
    //   #5653	-> 10015
    //   #5657	-> 10015
    //   #5658	-> 10027
    //   #5659	-> 10036
    //   #5660	-> 10043
    //   #5661	-> 10052
    //   #5662	-> 10059
    //   #5663	-> 10068
    //   #5664	-> 10075
    //   #5665	-> 10082
    //   #5666	-> 10090
    //   #5665	-> 10102
    //   #5657	-> 10105
    //   #5670	-> 10105
    //   #5671	-> 10122
    //   #5574	-> 10131
    //   #5570	-> 10134
    //   #5677	-> 10134
    //   #5678	-> 10177
    //   #5679	-> 10180
    //   #5680	-> 10191
    //   #5681	-> 10202
    //   #5682	-> 10207
    //   #5683	-> 10212
    //   #5684	-> 10218
    //   #5685	-> 10227
    //   #5687	-> 10233
    //   #5689	-> 10242
    //   #5690	-> 10250
    //   #5691	-> 10258
    //   #5681	-> 10273
    //   #5679	-> 10273
    //   #5694	-> 10279
    //   #5695	-> 10291
    //   #5677	-> 10311
    //   #5699	-> 10318
    //   #5700	-> 10318
    //   #5701	-> 10324
    //   #5702	-> 10324
    //   #5703	-> 10324
    //   #5704	-> 10324
    //   #5705	-> 10378
    //   #5706	-> 10390
    //   #5707	-> 10390
    //   #5708	-> 10396
    //   #5709	-> 10403
    //   #5710	-> 10412
    //   #5711	-> 10428
    //   #5713	-> 10451
    //   #5714	-> 10459
    //   #5716	-> 10477
    //   #5718	-> 10513
    //   #5720	-> 10535
    //   #5722	-> 10557
    //   #5723	-> 10565
    //   #5724	-> 10575
    //   #5725	-> 10578
    //   #5727	-> 10581
    //   #5729	-> 10596
    //   #5731	-> 10611
    //   #5733	-> 10626
    //   #5704	-> 10645
    //   #5736	-> 10680
    //   #5739	-> 10686
    //   #5740	-> 10686
    //   #5741	-> 10686
    //   #5742	-> 10693
    //   #5744	-> 10699
    //   #5745	-> 10699
    //   #5746	-> 10708
    //   #5747	-> 10715
    //   #5748	-> 10720
    //   #5741	-> 10742
    //   #5751	-> 10748
    //   #5754	-> 10793
    //   #5755	-> 10799
    //   #5756	-> 10806
    //   #5757	-> 10815
    //   #5758	-> 10818
    //   #5759	-> 10825
    //   #5760	-> 10825
    //   #5761	-> 10832
    //   #5763	-> 10844
    //   #5764	-> 10851
    //   #5765	-> 10856
    //   #5764	-> 10867
    //   #5767	-> 10867
    //   #5768	-> 10870
    //   #5769	-> 10877
    //   #5763	-> 10889
    //   #5771	-> 10893
    //   #5772	-> 10904
    //   #5773	-> 10909
    //   #5775	-> 10917
    //   #5776	-> 10920
    //   #5777	-> 10927
    //   #5779	-> 10936
    //   #5780	-> 10947
    //   #5781	-> 10952
    //   #5783	-> 10960
    //   #5784	-> 10963
    //   #5785	-> 10970
    //   #5787	-> 10979
    //   #5788	-> 10986
    //   #5789	-> 10991
    //   #5791	-> 10999
    //   #5792	-> 11002
    //   #5793	-> 11009
    //   #5787	-> 11021
    //   #5795	-> 11021
    //   #5796	-> 11028
    //   #5797	-> 11033
    //   #5799	-> 11042
    //   #5800	-> 11042
    //   #5801	-> 11049
    //   #5803	-> 11058
    //   #5804	-> 11066
    //   #5736	-> 11079
    //   #5809	-> 11079
    //   #5810	-> 11085
    //   #5811	-> 11090
    //   #5812	-> 11101
    //   #5814	-> 11110
    //   #5815	-> 11117
    //   #5816	-> 11123
    //   #5817	-> 11130
    //   #5818	-> 11139
    //   #5819	-> 11148
    //   #5820	-> 11156
    //   #5821	-> 11165
    //   #5822	-> 11180
    //   #5814	-> 11193
    //   #5810	-> 11196
    //   #5826	-> 11196
    //   #5827	-> 11202
    //   #5828	-> 11216
    //   #5830	-> 11228
    //   #5831	-> 11247
    //   #5832	-> 11256
    //   #5833	-> 11263
    //   #5834	-> 11268
    //   #5835	-> 11285
    //   #5836	-> 11305
    //   #5837	-> 11311
    //   #5838	-> 11318
    //   #5839	-> 11327
    //   #5840	-> 11343
    //   #5841	-> 11351
    //   #5842	-> 11358
    //   #5843	-> 11365
    //   #5844	-> 11374
    //   #5845	-> 11382
    //   #5846	-> 11390
    //   #5847	-> 11397
    //   #5848	-> 11405
    //   #5849	-> 11412
    //   #5850	-> 11421
    //   #5851	-> 11429
    //   #5846	-> 11440
    //   #5854	-> 11443
    //   #5856	-> 11452
    //   #5857	-> 11462
    //   #5827	-> 11465
    //   #5860	-> 11475
    //   #5861	-> 11481
    //   #5862	-> 11495
    //   #5864	-> 11507
    //   #5865	-> 11526
    //   #5866	-> 11535
    //   #5867	-> 11542
    //   #5868	-> 11547
    //   #5869	-> 11564
    //   #5870	-> 11584
    //   #5871	-> 11590
    //   #5872	-> 11597
    //   #5873	-> 11606
    //   #5874	-> 11622
    //   #5875	-> 11630
    //   #5876	-> 11637
    //   #5877	-> 11644
    //   #5878	-> 11653
    //   #5879	-> 11661
    //   #5880	-> 11669
    //   #5881	-> 11676
    //   #5882	-> 11684
    //   #5883	-> 11691
    //   #5884	-> 11700
    //   #5885	-> 11708
    //   #5880	-> 11719
    //   #5888	-> 11722
    //   #5890	-> 11731
    //   #5891	-> 11741
    //   #5861	-> 11744
    //   #5894	-> 11750
    //   #5895	-> 11756
    //   #5896	-> 11778
    //   #5897	-> 11790
    //   #5898	-> 11795
    //   #5899	-> 11800
    //   #5900	-> 11808
    //   #5901	-> 11823
    //   #5902	-> 11831
    //   #5903	-> 11844
    //   #5904	-> 11851
    //   #5905	-> 11866
    //   #5906	-> 11873
    //   #5907	-> 11885
    //   #5902	-> 11893
    //   #5909	-> 11899
    //   #5897	-> 11906
    //   #5895	-> 11906
    //   #5913	-> 11912
    //   #5914	-> 11924
    //   #5915	-> 11932
    //   #5918	-> 11950
    //   #5920	-> 11993
    //   #5922	-> 12014
    //   #5924	-> 12035
    //   #5927	-> 12056
    //   #5928	-> 12063
    //   #5929	-> 12070
    //   #5930	-> 12116
    //   #5931	-> 12128
    //   #5932	-> 12136
    //   #5933	-> 12142
    //   #5934	-> 12149
    //   #5935	-> 12158
    //   #5936	-> 12165
    //   #5937	-> 12173
    //   #5939	-> 12185
    //   #5941	-> 12193
    //   #5943	-> 12201
    //   #5944	-> 12208
    //   #5946	-> 12213
    //   #5948	-> 12232
    //   #5949	-> 12241
    //   #5950	-> 12248
    //   #5952	-> 12268
    //   #5953	-> 12277
    //   #5954	-> 12282
    //   #5957	-> 12297
    //   #5958	-> 12304
    //   #5959	-> 12312
    //   #5960	-> 12319
    //   #5958	-> 12331
    //   #5963	-> 12331
    //   #5964	-> 12338
    //   #5965	-> 12347
    //   #5966	-> 12355
    //   #5968	-> 12363
    //   #5969	-> 12381
    //   #5970	-> 12388
    //   #5971	-> 12395
    //   #5972	-> 12404
    //   #5973	-> 12412
    //   #5976	-> 12423
    //   #5978	-> 12432
    //   #5979	-> 12435
    //   #5982	-> 12444
    //   #5983	-> 12453
    //   #5929	-> 12456
    //   #5986	-> 12478
    //   #5988	-> 12509
    //   #5990	-> 12530
    //   #5993	-> 12560
    //   #5994	-> 12563
    //   #5995	-> 12573
    //   #5996	-> 12586
    //   #5997	-> 12597
    //   #5998	-> 12604
    //   #5999	-> 12610
    //   #6000	-> 12618
    //   #6001	-> 12627
    //   #6002	-> 12639
    //   #6003	-> 12648
    //   #6004	-> 12663
    //   #6005	-> 12673
    //   #5994	-> 12676
    //   #6008	-> 12686
    //   #6009	-> 12693
    //   #6010	-> 12699
    //   #6011	-> 12707
    //   #6012	-> 12716
    //   #6013	-> 12731
    //   #6016	-> 12741
    //   #6017	-> 12750
    //   #6018	-> 12759
    //   #6019	-> 12773
    //   #6020	-> 12779
    //   #6021	-> 12787
    //   #6022	-> 12796
    //   #6023	-> 12807
    //   #6024	-> 12816
    //   #6025	-> 12827
    //   #6028	-> 12837
    //   #6029	-> 12846
    //   #6030	-> 12851
    //   #6031	-> 12857
    //   #6032	-> 12866
    //   #6033	-> 12877
    //   #6032	-> 12916
    //   #6035	-> 12922
    //   #6029	-> 12939
    //   #6037	-> 12943
    //   #6038	-> 12956
    //   #6039	-> 12961
    //   #6040	-> 12967
    //   #6041	-> 12976
    //   #6042	-> 12991
    //   #6041	-> 13030
    //   #6044	-> 13036
    //   #6038	-> 13049
    //   #6047	-> 13049
    //   #6048	-> 13063
    //   #6049	-> 13074
    //   #6050	-> 13079
    //   #6051	-> 13085
    //   #6053	-> 13135
    //   #6054	-> 13146
    //   #6053	-> 13185
    //   #6056	-> 13191
    //   #6049	-> 13204
    //   #6059	-> 13204
    //   #6060	-> 13215
    //   #6061	-> 13220
    //   #6062	-> 13226
    //   #6064	-> 13276
    //   #6065	-> 13287
    //   #6064	-> 13326
    //   #6067	-> 13332
    //   #6047	-> 13342
    //   #6071	-> 13348
    //   #6072	-> 13348
    //   #6073	-> 13355
    //   #6074	-> 13381
    //   #6081	-> 13393
    //   #6082	-> 13402
    //   #6083	-> 13411
    //   #6084	-> 13420
    //   #6085	-> 13429
    //   #6086	-> 13438
    //   #6087	-> 13447
    //   #6088	-> 13452
    //   #6090	-> 13465
    //   #6092	-> 13522
    //   #6093	-> 13528
    //   #6094	-> 13545
    //   #6095	-> 13570
    //   #6096	-> 13587
    //   #6097	-> 13603
    //   #6098	-> 13619
    //   #6099	-> 13634
    //   #6100	-> 13655
    //   #6101	-> 13680
    //   #6102	-> 13683
    //   #6103	-> 13688
    //   #6104	-> 13691
    //   #6102	-> 13711
    //   #6106	-> 13711
    //   #6107	-> 13716
    //   #6108	-> 13721
    //   #6107	-> 13731
    //   #6110	-> 13731
    //   #6111	-> 13734
    //   #6106	-> 13754
    //   #6113	-> 13754
    //   #6114	-> 13759
    //   #6115	-> 13764
    //   #6117	-> 13771
    //   #6120	-> 13788
    //   #6121	-> 13805
    //   #6122	-> 13826
    //   #6123	-> 13835
    //   #6124	-> 13840
    //   #6125	-> 13853
    //   #6126	-> 13862
    //   #6128	-> 13872
    //   #6130	-> 13880
    //   #6131	-> 13888
    //   #6132	-> 13898
    //   #6133	-> 13906
    //   #6134	-> 13916
    //   #6135	-> 13926
    //   #6136	-> 13933
    //   #6137	-> 13953
    //   #6134	-> 13964
    //   #6123	-> 13967
    //   #6121	-> 13967
    //   #6141	-> 13973
    //   #6073	-> 13976
    //   #6145	-> 13996
    //   #6146	-> 13996
    //   #6147	-> 14003
    //   #6148	-> 14024
    //   #6149	-> 14052
    //   #6150	-> 14060
    //   #6151	-> 14063
    //   #6152	-> 14075
    //   #6153	-> 14082
    //   #6154	-> 14096
    //   #6155	-> 14109
    //   #6156	-> 14130
    //   #6157	-> 14150
    //   #6158	-> 14158
    //   #6153	-> 14161
    //   #6160	-> 14167
    //   #6161	-> 14174
    //   #6162	-> 14188
    //   #6163	-> 14200
    //   #6164	-> 14211
    //   #6165	-> 14220
    //   #6166	-> 14229
    //   #6167	-> 14252
    //   #6168	-> 14258
    //   #6169	-> 14275
    //   #6170	-> 14300
    //   #6171	-> 14317
    //   #6172	-> 14328
    //   #6173	-> 14337
    //   #6174	-> 14354
    //   #6175	-> 14362
    //   #6176	-> 14379
    //   #6177	-> 14389
    //   #6161	-> 14392
    //   #6180	-> 14398
    //   #6181	-> 14403
    //   #6183	-> 14416
    //   #6147	-> 14419
    //   #6185	-> 14436
    //   #6186	-> 14488
    //   #5430	-> 14548
    //   #6189	-> 14628
  }
  
  static void printBitDescriptions(StringBuilder paramStringBuilder, int paramInt1, int paramInt2, HistoryTag paramHistoryTag, BitDescription[] paramArrayOfBitDescription, boolean paramBoolean) {
    int i = paramInt1 ^ paramInt2;
    if (i == 0)
      return; 
    int j = 0;
    for (paramInt1 = 0; paramInt1 < paramArrayOfBitDescription.length; paramInt1++, j = k) {
      BitDescription bitDescription = paramArrayOfBitDescription[paramInt1];
      int k = j;
      if ((bitDescription.mask & i) != 0) {
        String str;
        if (paramBoolean) {
          str = " ";
        } else {
          str = ",";
        } 
        paramStringBuilder.append(str);
        if (bitDescription.shift < 0) {
          if ((bitDescription.mask & paramInt2) != 0) {
            str = "+";
          } else {
            str = "-";
          } 
          paramStringBuilder.append(str);
          if (paramBoolean) {
            str = bitDescription.name;
          } else {
            str = bitDescription.shortName;
          } 
          paramStringBuilder.append(str);
          k = j;
          if (bitDescription.mask == 1073741824) {
            k = j;
            if (paramHistoryTag != null) {
              k = 1;
              paramStringBuilder.append("=");
              if (paramBoolean) {
                UserHandle.formatUid(paramStringBuilder, paramHistoryTag.uid);
                paramStringBuilder.append(":\"");
                paramStringBuilder.append(paramHistoryTag.string);
                paramStringBuilder.append("\"");
              } else {
                paramStringBuilder.append(paramHistoryTag.poolIdx);
              } 
            } 
          } 
        } else {
          if (paramBoolean) {
            str = bitDescription.name;
          } else {
            str = bitDescription.shortName;
          } 
          paramStringBuilder.append(str);
          paramStringBuilder.append("=");
          k = (bitDescription.mask & paramInt2) >> bitDescription.shift;
          if (bitDescription.values != null && k >= 0 && k < bitDescription.values.length) {
            if (paramBoolean) {
              str = bitDescription.values[k];
            } else {
              str = bitDescription.shortValues[k];
            } 
            paramStringBuilder.append(str);
            k = j;
          } else {
            paramStringBuilder.append(k);
            k = j;
          } 
        } 
      } 
    } 
    if (j == 0 && paramHistoryTag != null) {
      String str;
      if (paramBoolean) {
        str = " wake_lock=";
      } else {
        str = ",w=";
      } 
      paramStringBuilder.append(str);
      if (paramBoolean) {
        UserHandle.formatUid(paramStringBuilder, paramHistoryTag.uid);
        paramStringBuilder.append(":\"");
        paramStringBuilder.append(paramHistoryTag.string);
        paramStringBuilder.append("\"");
      } else {
        paramStringBuilder.append(paramHistoryTag.poolIdx);
      } 
    } 
  }
  
  public void prepareForDumpLocked() {}
  
  class HistoryPrinter {
    long lastTime;
    
    int oldChargeMAh;
    
    int oldHealth;
    
    int oldLevel;
    
    double oldModemRailChargeMah;
    
    int oldPlug;
    
    int oldState;
    
    int oldState2;
    
    int oldStatus;
    
    int oldTemp;
    
    int oldVolt;
    
    double oldWifiRailChargeMah;
    
    public HistoryPrinter() {
      this.oldState = 0;
      this.oldState2 = 0;
      this.oldLevel = -1;
      this.oldStatus = -1;
      this.oldHealth = -1;
      this.oldPlug = -1;
      this.oldTemp = -1;
      this.oldVolt = -1;
      this.oldChargeMAh = -1;
      this.oldModemRailChargeMah = -1.0D;
      this.oldWifiRailChargeMah = -1.0D;
      this.lastTime = -1L;
    }
    
    void reset() {
      this.oldState2 = 0;
      this.oldState = 0;
      this.oldLevel = -1;
      this.oldStatus = -1;
      this.oldHealth = -1;
      this.oldPlug = -1;
      this.oldTemp = -1;
      this.oldVolt = -1;
      this.oldChargeMAh = -1;
      this.oldModemRailChargeMah = -1.0D;
      this.oldWifiRailChargeMah = -1.0D;
    }
    
    public void printNextItem(PrintWriter param1PrintWriter, BatteryStats.HistoryItem param1HistoryItem, long param1Long, boolean param1Boolean1, boolean param1Boolean2) {
      param1PrintWriter.print(printNextItem(param1HistoryItem, param1Long, param1Boolean1, param1Boolean2));
    }
    
    public void printNextItem(ProtoOutputStream param1ProtoOutputStream, BatteryStats.HistoryItem param1HistoryItem, long param1Long, boolean param1Boolean) {
      str = printNextItem(param1HistoryItem, param1Long, true, param1Boolean);
      for (String str : str.split("\n"))
        param1ProtoOutputStream.write(2237677961222L, str); 
    }
    
    private String printNextItem(BatteryStats.HistoryItem param1HistoryItem, long param1Long, boolean param1Boolean1, boolean param1Boolean2) {
      StringBuilder stringBuilder = new StringBuilder();
      if (!param1Boolean1) {
        stringBuilder.append("  ");
        TimeUtils.formatDuration(param1HistoryItem.time - param1Long, stringBuilder, 19);
        stringBuilder.append(" (");
        stringBuilder.append(param1HistoryItem.numReadInts);
        stringBuilder.append(") ");
      } else {
        stringBuilder.append(9);
        stringBuilder.append(',');
        stringBuilder.append("h");
        stringBuilder.append(',');
        if (this.lastTime < 0L) {
          stringBuilder.append(param1HistoryItem.time - param1Long);
        } else {
          stringBuilder.append(param1HistoryItem.time - this.lastTime);
        } 
        this.lastTime = param1HistoryItem.time;
      } 
      if (param1HistoryItem.cmd == 4) {
        if (param1Boolean1)
          stringBuilder.append(":"); 
        stringBuilder.append("START\n");
        reset();
      } else {
        CharSequence charSequence;
        byte b = param1HistoryItem.cmd;
        String str = " ";
        if (b == 5 || param1HistoryItem.cmd == 7) {
          if (param1Boolean1)
            stringBuilder.append(":"); 
          if (param1HistoryItem.cmd == 7) {
            stringBuilder.append("RESET:");
            reset();
          } 
          stringBuilder.append("TIME:");
          if (param1Boolean1) {
            stringBuilder.append(param1HistoryItem.currentTime);
            stringBuilder.append("\n");
          } else {
            stringBuilder.append(" ");
            charSequence = DateFormat.format("yyyy-MM-dd-HH-mm-ss", param1HistoryItem.currentTime);
            charSequence = charSequence.toString();
            stringBuilder.append((String)charSequence);
            stringBuilder.append("\n");
          } 
          return stringBuilder.toString();
        } 
        if (((BatteryStats.HistoryItem)charSequence).cmd == 8) {
          if (param1Boolean1)
            stringBuilder.append(":"); 
          stringBuilder.append("SHUTDOWN\n");
        } else if (((BatteryStats.HistoryItem)charSequence).cmd == 6) {
          if (param1Boolean1)
            stringBuilder.append(":"); 
          stringBuilder.append("*OVERFLOW*\n");
        } else {
          if (!param1Boolean1) {
            if (((BatteryStats.HistoryItem)charSequence).batteryLevel < 10) {
              stringBuilder.append("00");
            } else if (((BatteryStats.HistoryItem)charSequence).batteryLevel < 100) {
              stringBuilder.append("0");
            } 
            stringBuilder.append(((BatteryStats.HistoryItem)charSequence).batteryLevel);
            if (param1Boolean2) {
              stringBuilder.append(" ");
              if (((BatteryStats.HistoryItem)charSequence).states >= 0)
                if (((BatteryStats.HistoryItem)charSequence).states < 16) {
                  stringBuilder.append("0000000");
                } else if (((BatteryStats.HistoryItem)charSequence).states < 256) {
                  stringBuilder.append("000000");
                } else if (((BatteryStats.HistoryItem)charSequence).states < 4096) {
                  stringBuilder.append("00000");
                } else if (((BatteryStats.HistoryItem)charSequence).states < 65536) {
                  stringBuilder.append("0000");
                } else if (((BatteryStats.HistoryItem)charSequence).states < 1048576) {
                  stringBuilder.append("000");
                } else if (((BatteryStats.HistoryItem)charSequence).states < 16777216) {
                  stringBuilder.append("00");
                } else if (((BatteryStats.HistoryItem)charSequence).states < 268435456) {
                  stringBuilder.append("0");
                }  
              stringBuilder.append(Integer.toHexString(((BatteryStats.HistoryItem)charSequence).states));
            } 
          } else if (this.oldLevel != ((BatteryStats.HistoryItem)charSequence).batteryLevel) {
            this.oldLevel = ((BatteryStats.HistoryItem)charSequence).batteryLevel;
            stringBuilder.append(",Bl=");
            stringBuilder.append(((BatteryStats.HistoryItem)charSequence).batteryLevel);
          } 
          int i = this.oldStatus;
          byte b1 = ((BatteryStats.HistoryItem)charSequence).batteryStatus;
          String str1 = "f", str2 = "n", str3 = "d", str4 = "c";
          if (i != b1) {
            String str5;
            this.oldStatus = ((BatteryStats.HistoryItem)charSequence).batteryStatus;
            if (param1Boolean1) {
              str5 = ",Bs=";
            } else {
              str5 = " status=";
            } 
            stringBuilder.append(str5);
            i = this.oldStatus;
            if (i != 1) {
              if (i != 2) {
                if (i != 3) {
                  if (i != 4) {
                    if (i != 5) {
                      stringBuilder.append(i);
                    } else {
                      if (param1Boolean1) {
                        str5 = "f";
                      } else {
                        str5 = "full";
                      } 
                      stringBuilder.append(str5);
                    } 
                  } else {
                    if (param1Boolean1) {
                      str5 = "n";
                    } else {
                      str5 = "not-charging";
                    } 
                    stringBuilder.append(str5);
                  } 
                } else {
                  if (param1Boolean1) {
                    str5 = "d";
                  } else {
                    str5 = "discharging";
                  } 
                  stringBuilder.append(str5);
                } 
              } else {
                if (param1Boolean1) {
                  str5 = "c";
                } else {
                  str5 = "charging";
                } 
                stringBuilder.append(str5);
              } 
            } else {
              if (param1Boolean1) {
                str5 = "?";
              } else {
                str5 = "unknown";
              } 
              stringBuilder.append(str5);
            } 
          } 
          if (this.oldHealth != ((BatteryStats.HistoryItem)charSequence).batteryHealth) {
            String str5;
            this.oldHealth = ((BatteryStats.HistoryItem)charSequence).batteryHealth;
            if (param1Boolean1) {
              str5 = ",Bh=";
            } else {
              str5 = " health=";
            } 
            stringBuilder.append(str5);
            i = this.oldHealth;
            switch (i) {
              default:
                stringBuilder.append(i);
                break;
              case 7:
                if (param1Boolean1) {
                  str5 = str4;
                } else {
                  str5 = "cold";
                } 
                stringBuilder.append(str5);
                break;
              case 6:
                if (param1Boolean1) {
                  str5 = str1;
                } else {
                  str5 = "failure";
                } 
                stringBuilder.append(str5);
                break;
              case 5:
                if (param1Boolean1) {
                  str5 = "v";
                } else {
                  str5 = "over-voltage";
                } 
                stringBuilder.append(str5);
                break;
              case 4:
                if (param1Boolean1) {
                  str5 = str3;
                } else {
                  str5 = "dead";
                } 
                stringBuilder.append(str5);
                break;
              case 3:
                if (param1Boolean1) {
                  str5 = "h";
                } else {
                  str5 = "overheat";
                } 
                stringBuilder.append(str5);
                break;
              case 2:
                if (param1Boolean1) {
                  str5 = "g";
                } else {
                  str5 = "good";
                } 
                stringBuilder.append(str5);
                break;
              case 1:
                if (param1Boolean1) {
                  str5 = "?";
                } else {
                  str5 = "unknown";
                } 
                stringBuilder.append(str5);
                break;
            } 
          } 
          if (this.oldPlug != ((BatteryStats.HistoryItem)charSequence).batteryPlugType) {
            String str5;
            this.oldPlug = ((BatteryStats.HistoryItem)charSequence).batteryPlugType;
            if (param1Boolean1) {
              str5 = ",Bp=";
            } else {
              str5 = " plug=";
            } 
            stringBuilder.append(str5);
            i = this.oldPlug;
            if (i != 0) {
              if (i != 1) {
                if (i != 2) {
                  if (i != 4) {
                    stringBuilder.append(i);
                  } else {
                    if (param1Boolean1) {
                      str5 = "w";
                    } else {
                      str5 = "wireless";
                    } 
                    stringBuilder.append(str5);
                  } 
                } else {
                  if (param1Boolean1) {
                    str5 = "u";
                  } else {
                    str5 = "usb";
                  } 
                  stringBuilder.append(str5);
                } 
              } else {
                if (param1Boolean1) {
                  str5 = "a";
                } else {
                  str5 = "ac";
                } 
                stringBuilder.append(str5);
              } 
            } else {
              if (param1Boolean1) {
                str5 = str2;
              } else {
                str5 = "none";
              } 
              stringBuilder.append(str5);
            } 
          } 
          if (this.oldTemp != ((BatteryStats.HistoryItem)charSequence).batteryTemperature) {
            String str5;
            this.oldTemp = ((BatteryStats.HistoryItem)charSequence).batteryTemperature;
            if (param1Boolean1) {
              str5 = ",Bt=";
            } else {
              str5 = " temp=";
            } 
            stringBuilder.append(str5);
            stringBuilder.append(this.oldTemp);
          } 
          if (this.oldVolt != ((BatteryStats.HistoryItem)charSequence).batteryVoltage) {
            String str5;
            this.oldVolt = ((BatteryStats.HistoryItem)charSequence).batteryVoltage;
            if (param1Boolean1) {
              str5 = ",Bv=";
            } else {
              str5 = " volt=";
            } 
            stringBuilder.append(str5);
            stringBuilder.append(this.oldVolt);
          } 
          i = ((BatteryStats.HistoryItem)charSequence).batteryChargeUAh / 1000;
          if (this.oldChargeMAh != i) {
            String str5;
            this.oldChargeMAh = i;
            if (param1Boolean1) {
              str5 = ",Bcc=";
            } else {
              str5 = " charge=";
            } 
            stringBuilder.append(str5);
            stringBuilder.append(this.oldChargeMAh);
          } 
          if (this.oldModemRailChargeMah != ((BatteryStats.HistoryItem)charSequence).modemRailChargeMah) {
            String str5;
            this.oldModemRailChargeMah = ((BatteryStats.HistoryItem)charSequence).modemRailChargeMah;
            if (param1Boolean1) {
              str5 = ",Mrc=";
            } else {
              str5 = " modemRailChargemAh=";
            } 
            stringBuilder.append(str5);
            stringBuilder.append((new DecimalFormat("#.##")).format(this.oldModemRailChargeMah));
          } 
          if (this.oldWifiRailChargeMah != ((BatteryStats.HistoryItem)charSequence).wifiRailChargeMah) {
            String str5;
            this.oldWifiRailChargeMah = ((BatteryStats.HistoryItem)charSequence).wifiRailChargeMah;
            if (param1Boolean1) {
              str5 = ",Wrc=";
            } else {
              str5 = " wifiRailChargemAh=";
            } 
            stringBuilder.append(str5);
            stringBuilder.append((new DecimalFormat("#.##")).format(this.oldWifiRailChargeMah));
          } 
          BatteryStats.printBitDescriptions(stringBuilder, this.oldState, ((BatteryStats.HistoryItem)charSequence).states, ((BatteryStats.HistoryItem)charSequence).wakelockTag, BatteryStats.HISTORY_STATE_DESCRIPTIONS, param1Boolean1 ^ true);
          BatteryStats.printBitDescriptions(stringBuilder, this.oldState2, ((BatteryStats.HistoryItem)charSequence).states2, null, BatteryStats.HISTORY_STATE2_DESCRIPTIONS, param1Boolean1 ^ true);
          if (((BatteryStats.HistoryItem)charSequence).wakeReasonTag != null)
            if (param1Boolean1) {
              stringBuilder.append(",wr=");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).wakeReasonTag.poolIdx);
            } else {
              stringBuilder.append(" wake_reason=");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).wakeReasonTag.uid);
              stringBuilder.append(":\"");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).wakeReasonTag.string);
              stringBuilder.append("\"");
            }  
          if (((BatteryStats.HistoryItem)charSequence).eventCode != 0) {
            String arrayOfString[], str5 = str;
            if (param1Boolean1)
              str5 = ","; 
            stringBuilder.append(str5);
            if ((((BatteryStats.HistoryItem)charSequence).eventCode & 0x8000) != 0) {
              stringBuilder.append("+");
            } else if ((((BatteryStats.HistoryItem)charSequence).eventCode & 0x4000) != 0) {
              stringBuilder.append("-");
            } 
            if (param1Boolean1) {
              arrayOfString = BatteryStats.HISTORY_EVENT_CHECKIN_NAMES;
            } else {
              arrayOfString = BatteryStats.HISTORY_EVENT_NAMES;
            } 
            i = ((BatteryStats.HistoryItem)charSequence).eventCode & 0xFFFF3FFF;
            if (i >= 0 && i < arrayOfString.length) {
              stringBuilder.append(arrayOfString[i]);
            } else {
              String str6;
              if (param1Boolean1) {
                str6 = "Ev";
              } else {
                str6 = "event";
              } 
              stringBuilder.append(str6);
              stringBuilder.append(i);
            } 
            stringBuilder.append("=");
            if (param1Boolean1) {
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).eventTag.poolIdx);
            } else {
              BatteryStats.IntToString intToString = BatteryStats.HISTORY_EVENT_INT_FORMATTERS[i];
              i = ((BatteryStats.HistoryItem)charSequence).eventTag.uid;
              String str6 = intToString.applyAsString(i);
              stringBuilder.append(str6);
              stringBuilder.append(":\"");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).eventTag.string);
              stringBuilder.append("\"");
            } 
          } 
          stringBuilder.append("\n");
          if (((BatteryStats.HistoryItem)charSequence).stepDetails != null)
            if (!param1Boolean1) {
              stringBuilder.append("                 Details: cpu=");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.userTime);
              stringBuilder.append("u+");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.systemTime);
              stringBuilder.append("s");
              if (((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUid1 >= 0) {
                stringBuilder.append(" (");
                printStepCpuUidDetails(stringBuilder, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUid1, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUTime1, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuSTime1);
                if (((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUid2 >= 0) {
                  stringBuilder.append(", ");
                  printStepCpuUidDetails(stringBuilder, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUid2, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUTime2, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuSTime2);
                } 
                if (((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUid3 >= 0) {
                  stringBuilder.append(", ");
                  printStepCpuUidDetails(stringBuilder, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUid3, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUTime3, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuSTime3);
                } 
                stringBuilder.append(')');
              } 
              stringBuilder.append("\n");
              stringBuilder.append("                          /proc/stat=");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statUserTime);
              stringBuilder.append(" usr, ");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statSystemTime);
              stringBuilder.append(" sys, ");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statIOWaitTime);
              stringBuilder.append(" io, ");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statIrqTime);
              stringBuilder.append(" irq, ");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statSoftIrqTime);
              stringBuilder.append(" sirq, ");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statIdlTime);
              stringBuilder.append(" idle");
              int j = ((BatteryStats.HistoryItem)charSequence).stepDetails.statUserTime + ((BatteryStats.HistoryItem)charSequence).stepDetails.statSystemTime + ((BatteryStats.HistoryItem)charSequence).stepDetails.statIOWaitTime + ((BatteryStats.HistoryItem)charSequence).stepDetails.statIrqTime + ((BatteryStats.HistoryItem)charSequence).stepDetails.statSoftIrqTime;
              i = ((BatteryStats.HistoryItem)charSequence).stepDetails.statIdlTime + j;
              if (i > 0) {
                stringBuilder.append(" (");
                float f = j / i;
                stringBuilder.append(String.format("%.1f%%", new Object[] { Float.valueOf(f * 100.0F) }));
                stringBuilder.append(" of ");
                StringBuilder stringBuilder1 = new StringBuilder(64);
                BatteryStats.formatTimeMsNoSpace(stringBuilder1, (i * 10));
                stringBuilder.append(stringBuilder1);
                stringBuilder.append(")");
              } 
              stringBuilder.append(", PlatformIdleStat ");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statPlatformIdleState);
              stringBuilder.append("\n");
              stringBuilder.append(", SubsystemPowerState ");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statSubsystemPowerState);
              stringBuilder.append("\n");
            } else {
              stringBuilder.append(9);
              stringBuilder.append(',');
              stringBuilder.append("h");
              stringBuilder.append(",0,Dcpu=");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.userTime);
              stringBuilder.append(":");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.systemTime);
              if (((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUid1 >= 0) {
                printStepCpuUidCheckinDetails(stringBuilder, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUid1, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUTime1, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuSTime1);
                if (((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUid2 >= 0)
                  printStepCpuUidCheckinDetails(stringBuilder, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUid2, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUTime2, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuSTime2); 
                if (((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUid3 >= 0)
                  printStepCpuUidCheckinDetails(stringBuilder, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUid3, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuUTime3, ((BatteryStats.HistoryItem)charSequence).stepDetails.appCpuSTime3); 
              } 
              stringBuilder.append("\n");
              stringBuilder.append(9);
              stringBuilder.append(',');
              stringBuilder.append("h");
              stringBuilder.append(",0,Dpst=");
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statUserTime);
              stringBuilder.append(',');
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statSystemTime);
              stringBuilder.append(',');
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statIOWaitTime);
              stringBuilder.append(',');
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statIrqTime);
              stringBuilder.append(',');
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statSoftIrqTime);
              stringBuilder.append(',');
              stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statIdlTime);
              stringBuilder.append(',');
              if (((BatteryStats.HistoryItem)charSequence).stepDetails.statPlatformIdleState != null) {
                stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statPlatformIdleState);
                if (((BatteryStats.HistoryItem)charSequence).stepDetails.statSubsystemPowerState != null)
                  stringBuilder.append(','); 
              } 
              if (((BatteryStats.HistoryItem)charSequence).stepDetails.statSubsystemPowerState != null)
                stringBuilder.append(((BatteryStats.HistoryItem)charSequence).stepDetails.statSubsystemPowerState); 
              stringBuilder.append("\n");
            }  
          this.oldState = ((BatteryStats.HistoryItem)charSequence).states;
          this.oldState2 = ((BatteryStats.HistoryItem)charSequence).states2;
          if ((((BatteryStats.HistoryItem)charSequence).states2 & 0x80000) != 0)
            ((BatteryStats.HistoryItem)charSequence).states2 &= 0xFFF7FFFF; 
        } 
      } 
      return stringBuilder.toString();
    }
    
    private void printStepCpuUidDetails(StringBuilder param1StringBuilder, int param1Int1, int param1Int2, int param1Int3) {
      UserHandle.formatUid(param1StringBuilder, param1Int1);
      param1StringBuilder.append("=");
      param1StringBuilder.append(param1Int2);
      param1StringBuilder.append("u+");
      param1StringBuilder.append(param1Int3);
      param1StringBuilder.append("s");
    }
    
    private void printStepCpuUidCheckinDetails(StringBuilder param1StringBuilder, int param1Int1, int param1Int2, int param1Int3) {
      param1StringBuilder.append('/');
      param1StringBuilder.append(param1Int1);
      param1StringBuilder.append(":");
      param1StringBuilder.append(param1Int2);
      param1StringBuilder.append(":");
      param1StringBuilder.append(param1Int3);
    }
  }
  
  private void printSizeValue(PrintWriter paramPrintWriter, long paramLong) {
    float f1 = (float)paramLong;
    String str = "";
    float f2 = f1;
    if (f1 >= 10240.0F) {
      str = "KB";
      f2 = f1 / 1024.0F;
    } 
    f1 = f2;
    if (f2 >= 10240.0F) {
      str = "MB";
      f1 = f2 / 1024.0F;
    } 
    f2 = f1;
    if (f1 >= 10240.0F) {
      str = "GB";
      f2 = f1 / 1024.0F;
    } 
    f1 = f2;
    if (f2 >= 10240.0F) {
      str = "TB";
      f1 = f2 / 1024.0F;
    } 
    f2 = f1;
    if (f1 >= 10240.0F) {
      str = "PB";
      f2 = f1 / 1024.0F;
    } 
    paramPrintWriter.print((int)f2);
    paramPrintWriter.print(str);
  }
  
  private static boolean dumpTimeEstimate(PrintWriter paramPrintWriter, String paramString1, String paramString2, String paramString3, long paramLong) {
    if (paramLong < 0L)
      return false; 
    paramPrintWriter.print(paramString1);
    paramPrintWriter.print(paramString2);
    paramPrintWriter.print(paramString3);
    StringBuilder stringBuilder = new StringBuilder(64);
    formatTimeMs(stringBuilder, paramLong);
    paramPrintWriter.print(stringBuilder);
    paramPrintWriter.println();
    return true;
  }
  
  private static boolean dumpDurationSteps(PrintWriter paramPrintWriter, String paramString1, String paramString2, LevelStepTracker paramLevelStepTracker, boolean paramBoolean) {
    if (paramLevelStepTracker == null)
      return false; 
    int i = paramLevelStepTracker.mNumStepDurations;
    if (i <= 0)
      return false; 
    if (!paramBoolean)
      paramPrintWriter.println(paramString2); 
    String[] arrayOfString = new String[5];
    for (byte b = 0; b < i; b++) {
      long l1 = paramLevelStepTracker.getDurationAt(b);
      int j = paramLevelStepTracker.getLevelAt(b);
      long l2 = paramLevelStepTracker.getInitModeAt(b);
      long l3 = paramLevelStepTracker.getModModeAt(b);
      if (paramBoolean) {
        arrayOfString[0] = Long.toString(l1);
        arrayOfString[1] = Integer.toString(j);
        if ((l3 & 0x3L) == 0L) {
          j = (int)(l2 & 0x3L) + 1;
          if (j != 1) {
            if (j != 2) {
              if (j != 3) {
                if (j != 4) {
                  arrayOfString[2] = "?";
                } else {
                  arrayOfString[2] = "sds";
                } 
              } else {
                arrayOfString[2] = "sd";
              } 
            } else {
              arrayOfString[2] = "s+";
            } 
          } else {
            arrayOfString[2] = "s-";
          } 
        } else {
          arrayOfString[2] = "";
        } 
        if ((l3 & 0x4L) == 0L) {
          String str;
          if ((l2 & 0x4L) != 0L) {
            str = "p+";
          } else {
            str = "p-";
          } 
          arrayOfString[3] = str;
        } else {
          arrayOfString[3] = "";
        } 
        if ((l3 & 0x8L) == 0L) {
          String str;
          if ((0x8L & l2) != 0L) {
            str = "i+";
          } else {
            str = "i-";
          } 
          arrayOfString[4] = str;
        } else {
          arrayOfString[4] = "";
        } 
        dumpLine(paramPrintWriter, 0, "i", paramString2, (Object[])arrayOfString);
      } else {
        paramPrintWriter.print(paramString1);
        paramPrintWriter.print("#");
        paramPrintWriter.print(b);
        paramPrintWriter.print(": ");
        TimeUtils.formatDuration(l1, paramPrintWriter);
        paramPrintWriter.print(" to ");
        paramPrintWriter.print(j);
        int k = 0;
        String str = " (";
        if ((l3 & 0x3L) == 0L) {
          paramPrintWriter.print(" (");
          j = (int)(l2 & 0x3L) + 1;
          if (j != 1) {
            if (j != 2) {
              if (j != 3) {
                if (j != 4) {
                  paramPrintWriter.print("screen-?");
                } else {
                  paramPrintWriter.print("screen-doze-suspend");
                } 
              } else {
                paramPrintWriter.print("screen-doze");
              } 
            } else {
              paramPrintWriter.print("screen-on");
            } 
          } else {
            paramPrintWriter.print("screen-off");
          } 
          k = 1;
        } 
        j = k;
        if ((l3 & 0x4L) == 0L) {
          String str1;
          if (k) {
            str1 = ", ";
          } else {
            str1 = " (";
          } 
          paramPrintWriter.print(str1);
          if ((l2 & 0x4L) != 0L) {
            str1 = "power-save-on";
          } else {
            str1 = "power-save-off";
          } 
          paramPrintWriter.print(str1);
          j = 1;
        } 
        k = j;
        if ((l3 & 0x8L) == 0L) {
          String str1 = str;
          if (j != 0)
            str1 = ", "; 
          paramPrintWriter.print(str1);
          if ((0x8L & l2) != 0L) {
            str1 = "device-idle-on";
          } else {
            str1 = "device-idle-off";
          } 
          paramPrintWriter.print(str1);
          k = 1;
        } 
        if (k != 0)
          paramPrintWriter.print(")"); 
        paramPrintWriter.println();
      } 
    } 
    return true;
  }
  
  private static void dumpDurationSteps(ProtoOutputStream paramProtoOutputStream, long paramLong, LevelStepTracker paramLevelStepTracker) {
    if (paramLevelStepTracker == null)
      return; 
    int i = paramLevelStepTracker.mNumStepDurations;
    for (byte b = 0; b < i; b++) {
      long l1 = paramProtoOutputStream.start(paramLong);
      paramProtoOutputStream.write(1112396529665L, paramLevelStepTracker.getDurationAt(b));
      paramProtoOutputStream.write(1120986464258L, paramLevelStepTracker.getLevelAt(b));
      long l2 = paramLevelStepTracker.getInitModeAt(b);
      long l3 = paramLevelStepTracker.getModModeAt(b);
      int j = 0;
      boolean bool = true;
      if ((l3 & 0x3L) == 0L) {
        j = (int)(0x3L & l2) + 1;
        if (j != 1) {
          if (j != 2) {
            if (j != 3) {
              if (j != 4) {
                j = 5;
              } else {
                j = 4;
              } 
            } else {
              j = 3;
            } 
          } else {
            j = 1;
          } 
        } else {
          j = 2;
        } 
      } 
      paramProtoOutputStream.write(1159641169923L, j);
      j = 0;
      if ((l3 & 0x4L) == 0L)
        if ((0x4L & l2) != 0L) {
          j = bool;
        } else {
          j = 2;
        }  
      paramProtoOutputStream.write(1159641169924L, j);
      j = 0;
      if ((l3 & 0x8L) == 0L)
        if ((0x8L & l2) != 0L) {
          j = 2;
        } else {
          j = 3;
        }  
      paramProtoOutputStream.write(1159641169925L, j);
      paramProtoOutputStream.end(l1);
    } 
  }
  
  private void dumpHistoryLocked(PrintWriter paramPrintWriter, int paramInt, long paramLong, boolean paramBoolean) {
    HistoryPrinter historyPrinter = new HistoryPrinter();
    HistoryItem historyItem = new HistoryItem();
    long l1 = -1L;
    long l2 = -1L;
    byte b = 0;
    HistoryTag historyTag = null;
    while (getNextHistoryLocked(historyItem)) {
      l1 = historyItem.time;
      if (l2 < 0L)
        l2 = l1; 
      if (historyItem.time >= paramLong) {
        boolean bool;
        HistoryTag historyTag1;
        byte b1;
        if (paramLong >= 0L && !b) {
          if (historyItem.cmd == 5 || historyItem.cmd == 7 || historyItem.cmd == 4 || historyItem.cmd == 8) {
            b = 1;
            if ((paramInt & 0x20) != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            historyPrinter.printNextItem(paramPrintWriter, historyItem, l2, paramBoolean, bool);
            historyItem.cmd = 0;
          } else if (historyItem.currentTime != 0L) {
            b = 1;
            byte b2 = historyItem.cmd;
            historyItem.cmd = 5;
            if ((paramInt & 0x20) != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            historyPrinter.printNextItem(paramPrintWriter, historyItem, l2, paramBoolean, bool);
            historyItem.cmd = b2;
          } 
          historyTag1 = historyTag;
          b1 = b;
          if (historyTag != null) {
            if (historyItem.cmd != 0) {
              if ((paramInt & 0x20) != 0) {
                bool = true;
              } else {
                bool = false;
              } 
              historyPrinter.printNextItem(paramPrintWriter, historyItem, l2, paramBoolean, bool);
              historyItem.cmd = 0;
            } 
            int i = historyItem.eventCode;
            historyTag1 = historyItem.eventTag;
            historyItem.eventTag = new HistoryTag();
            for (b1 = 0; b1 < 22; b1++) {
              HashMap<String, SparseIntArray> hashMap = historyTag.getStateForEvent(b1);
              if (hashMap != null)
                for (Map.Entry<String, SparseIntArray> entry : hashMap.entrySet()) {
                  SparseIntArray sparseIntArray2 = (SparseIntArray)entry.getValue();
                  SparseIntArray sparseIntArray1;
                  HistoryTag historyTag3;
                  byte b2;
                  for (b2 = 0, historyTag3 = historyTag1, sparseIntArray1 = sparseIntArray2; b2 < sparseIntArray1.size(); b2++) {
                    historyItem.eventCode = b1;
                    historyItem.eventTag.string = (String)entry.getKey();
                    historyItem.eventTag.uid = sparseIntArray1.keyAt(b2);
                    historyItem.eventTag.poolIdx = sparseIntArray1.valueAt(b2);
                    if ((paramInt & 0x20) != 0) {
                      bool = true;
                    } else {
                      bool = false;
                    } 
                    historyPrinter.printNextItem(paramPrintWriter, historyItem, l2, paramBoolean, bool);
                    historyItem.wakeReasonTag = null;
                    historyItem.wakelockTag = null;
                  } 
                  historyTag2 = historyTag3;
                }  
            } 
            historyItem.eventCode = i;
            historyItem.eventTag = historyTag2;
            HistoryTag historyTag2 = null;
            b1 = b;
          } 
        } else {
          b1 = b;
          historyTag1 = historyTag;
        } 
        if ((paramInt & 0x20) != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        historyPrinter.printNextItem(paramPrintWriter, historyItem, l2, paramBoolean, bool);
        b = b1;
        historyTag = historyTag1;
      } 
    } 
    if (paramLong >= 0L) {
      String str;
      commitCurrentHistoryBatchLocked();
      if (paramBoolean) {
        str = "NEXT: ";
      } else {
        str = "  NEXT: ";
      } 
      paramPrintWriter.print(str);
      paramPrintWriter.println(1L + l1);
    } 
  }
  
  private void dumpDailyLevelStepSummary(PrintWriter paramPrintWriter, String paramString1, String paramString2, LevelStepTracker paramLevelStepTracker, StringBuilder paramStringBuilder, int[] paramArrayOfint) {
    if (paramLevelStepTracker == null)
      return; 
    long l = paramLevelStepTracker.computeTimeEstimate(0L, 0L, paramArrayOfint);
    if (l >= 0L) {
      paramPrintWriter.print(paramString1);
      paramPrintWriter.print(paramString2);
      paramPrintWriter.print(" total time: ");
      paramStringBuilder.setLength(0);
      formatTimeMs(paramStringBuilder, l);
      paramPrintWriter.print(paramStringBuilder);
      paramPrintWriter.print(" (from ");
      paramPrintWriter.print(paramArrayOfint[0]);
      paramPrintWriter.println(" steps)");
    } 
    byte b = 0;
    while (true) {
      int[] arrayOfInt = STEP_LEVEL_MODES_OF_INTEREST;
      if (b < arrayOfInt.length) {
        l = paramLevelStepTracker.computeTimeEstimate(arrayOfInt[b], STEP_LEVEL_MODE_VALUES[b], paramArrayOfint);
        if (l > 0L) {
          paramPrintWriter.print(paramString1);
          paramPrintWriter.print(paramString2);
          paramPrintWriter.print(" ");
          paramPrintWriter.print(STEP_LEVEL_MODE_LABELS[b]);
          paramPrintWriter.print(" time: ");
          paramStringBuilder.setLength(0);
          formatTimeMs(paramStringBuilder, l);
          paramPrintWriter.print(paramStringBuilder);
          paramPrintWriter.print(" (from ");
          paramPrintWriter.print(paramArrayOfint[0]);
          paramPrintWriter.println(" steps)");
        } 
        b++;
        continue;
      } 
      break;
    } 
  }
  
  private void dumpDailyPackageChanges(PrintWriter paramPrintWriter, String paramString, ArrayList<PackageChange> paramArrayList) {
    if (paramArrayList == null)
      return; 
    paramPrintWriter.print(paramString);
    paramPrintWriter.println("Package changes:");
    for (byte b = 0; b < paramArrayList.size(); b++) {
      PackageChange packageChange = paramArrayList.get(b);
      if (packageChange.mUpdate) {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("  Update ");
        paramPrintWriter.print(packageChange.mPackageName);
        paramPrintWriter.print(" vers=");
        paramPrintWriter.println(packageChange.mVersionCode);
      } else {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("  Uninstall ");
        paramPrintWriter.println(packageChange.mPackageName);
      } 
    } 
  }
  
  public void dumpLocked(Context paramContext, PrintWriter paramPrintWriter, int paramInt1, int paramInt2, long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual prepareForDumpLocked : ()V
    //   4: iload_3
    //   5: bipush #14
    //   7: iand
    //   8: ifeq -> 17
    //   11: iconst_1
    //   12: istore #7
    //   14: goto -> 20
    //   17: iconst_0
    //   18: istore #7
    //   20: iload_3
    //   21: bipush #8
    //   23: iand
    //   24: ifne -> 32
    //   27: iload #7
    //   29: ifne -> 286
    //   32: aload_0
    //   33: invokevirtual getHistoryTotalSize : ()I
    //   36: i2l
    //   37: lstore #8
    //   39: aload_0
    //   40: invokevirtual getHistoryUsedSize : ()I
    //   43: i2l
    //   44: lstore #10
    //   46: aload_0
    //   47: invokevirtual startIteratingHistoryLocked : ()Z
    //   50: ifeq -> 171
    //   53: aload_2
    //   54: ldc_w 'Battery History ('
    //   57: invokevirtual print : (Ljava/lang/String;)V
    //   60: aload_2
    //   61: ldc2_w 100
    //   64: lload #10
    //   66: lmul
    //   67: lload #8
    //   69: ldiv
    //   70: invokevirtual print : (J)V
    //   73: aload_2
    //   74: ldc_w '% used, '
    //   77: invokevirtual print : (Ljava/lang/String;)V
    //   80: aload_0
    //   81: aload_2
    //   82: lload #10
    //   84: invokespecial printSizeValue : (Ljava/io/PrintWriter;J)V
    //   87: aload_2
    //   88: ldc_w ' used of '
    //   91: invokevirtual print : (Ljava/lang/String;)V
    //   94: aload_0
    //   95: aload_2
    //   96: lload #8
    //   98: invokespecial printSizeValue : (Ljava/io/PrintWriter;J)V
    //   101: aload_2
    //   102: ldc_w ', '
    //   105: invokevirtual print : (Ljava/lang/String;)V
    //   108: aload_2
    //   109: aload_0
    //   110: invokevirtual getHistoryStringPoolSize : ()I
    //   113: invokevirtual print : (I)V
    //   116: aload_2
    //   117: ldc_w ' strings using '
    //   120: invokevirtual print : (Ljava/lang/String;)V
    //   123: aload_0
    //   124: aload_2
    //   125: aload_0
    //   126: invokevirtual getHistoryStringPoolBytes : ()I
    //   129: i2l
    //   130: invokespecial printSizeValue : (Ljava/io/PrintWriter;J)V
    //   133: aload_2
    //   134: ldc_w '):'
    //   137: invokevirtual println : (Ljava/lang/String;)V
    //   140: aload_0
    //   141: aload_2
    //   142: iload_3
    //   143: lload #5
    //   145: iconst_0
    //   146: invokespecial dumpHistoryLocked : (Ljava/io/PrintWriter;IJZ)V
    //   149: aload_2
    //   150: invokevirtual println : ()V
    //   153: aload_0
    //   154: invokevirtual finishIteratingHistoryLocked : ()V
    //   157: goto -> 171
    //   160: astore_1
    //   161: goto -> 165
    //   164: astore_1
    //   165: aload_0
    //   166: invokevirtual finishIteratingHistoryLocked : ()V
    //   169: aload_1
    //   170: athrow
    //   171: aload_0
    //   172: invokevirtual startIteratingOldHistoryLocked : ()Z
    //   175: ifeq -> 286
    //   178: new android/os/BatteryStats$HistoryItem
    //   181: astore #12
    //   183: aload #12
    //   185: invokespecial <init> : ()V
    //   188: aload_2
    //   189: ldc_w 'Old battery History:'
    //   192: invokevirtual println : (Ljava/lang/String;)V
    //   195: new android/os/BatteryStats$HistoryPrinter
    //   198: astore #13
    //   200: aload #13
    //   202: invokespecial <init> : ()V
    //   205: ldc2_w -1
    //   208: lstore #5
    //   210: aload_0
    //   211: aload #12
    //   213: invokevirtual getNextOldHistoryLocked : (Landroid/os/BatteryStats$HistoryItem;)Z
    //   216: ifeq -> 268
    //   219: lload #5
    //   221: lconst_0
    //   222: lcmp
    //   223: ifge -> 236
    //   226: aload #12
    //   228: getfield time : J
    //   231: lstore #5
    //   233: goto -> 236
    //   236: iload_3
    //   237: bipush #32
    //   239: iand
    //   240: ifeq -> 249
    //   243: iconst_1
    //   244: istore #14
    //   246: goto -> 252
    //   249: iconst_0
    //   250: istore #14
    //   252: aload #13
    //   254: aload_2
    //   255: aload #12
    //   257: lload #5
    //   259: iconst_0
    //   260: iload #14
    //   262: invokevirtual printNextItem : (Ljava/io/PrintWriter;Landroid/os/BatteryStats$HistoryItem;JZZ)V
    //   265: goto -> 210
    //   268: aload_2
    //   269: invokevirtual println : ()V
    //   272: aload_0
    //   273: invokevirtual finishIteratingOldHistoryLocked : ()V
    //   276: goto -> 286
    //   279: astore_1
    //   280: aload_0
    //   281: invokevirtual finishIteratingOldHistoryLocked : ()V
    //   284: aload_1
    //   285: athrow
    //   286: iload #7
    //   288: ifeq -> 299
    //   291: iload_3
    //   292: bipush #6
    //   294: iand
    //   295: ifne -> 299
    //   298: return
    //   299: iload #7
    //   301: ifne -> 512
    //   304: aload_0
    //   305: invokevirtual getUidStats : ()Landroid/util/SparseArray;
    //   308: astore #13
    //   310: aload #13
    //   312: invokevirtual size : ()I
    //   315: istore #15
    //   317: iconst_0
    //   318: istore #16
    //   320: invokestatic elapsedRealtime : ()J
    //   323: lstore #8
    //   325: iconst_0
    //   326: istore #17
    //   328: iload #17
    //   330: iload #15
    //   332: if_icmpge -> 503
    //   335: aload #13
    //   337: iload #17
    //   339: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   342: checkcast android/os/BatteryStats$Uid
    //   345: astore #12
    //   347: aload #12
    //   349: invokevirtual getPidStats : ()Landroid/util/SparseArray;
    //   352: astore #12
    //   354: iload #16
    //   356: istore #18
    //   358: aload #12
    //   360: ifnull -> 493
    //   363: iconst_0
    //   364: istore #19
    //   366: iload #16
    //   368: istore #18
    //   370: iload #19
    //   372: aload #12
    //   374: invokevirtual size : ()I
    //   377: if_icmpge -> 493
    //   380: aload #12
    //   382: iload #19
    //   384: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   387: checkcast android/os/BatteryStats$Uid$Pid
    //   390: astore #20
    //   392: iload #16
    //   394: istore #18
    //   396: iload #16
    //   398: ifne -> 411
    //   401: aload_2
    //   402: ldc_w 'Per-PID Stats:'
    //   405: invokevirtual println : (Ljava/lang/String;)V
    //   408: iconst_1
    //   409: istore #18
    //   411: aload #20
    //   413: getfield mWakeSumMs : J
    //   416: lstore #10
    //   418: aload #20
    //   420: getfield mWakeNesting : I
    //   423: ifle -> 439
    //   426: lload #8
    //   428: aload #20
    //   430: getfield mWakeStartMs : J
    //   433: lsub
    //   434: lstore #5
    //   436: goto -> 442
    //   439: lconst_0
    //   440: lstore #5
    //   442: aload_2
    //   443: ldc_w '  PID '
    //   446: invokevirtual print : (Ljava/lang/String;)V
    //   449: aload_2
    //   450: aload #12
    //   452: iload #19
    //   454: invokevirtual keyAt : (I)I
    //   457: invokevirtual print : (I)V
    //   460: aload_2
    //   461: ldc_w ' wake time: '
    //   464: invokevirtual print : (Ljava/lang/String;)V
    //   467: lload #10
    //   469: lload #5
    //   471: ladd
    //   472: aload_2
    //   473: invokestatic formatDuration : (JLjava/io/PrintWriter;)V
    //   476: aload_2
    //   477: ldc_w ''
    //   480: invokevirtual println : (Ljava/lang/String;)V
    //   483: iinc #19, 1
    //   486: iload #18
    //   488: istore #16
    //   490: goto -> 366
    //   493: iinc #17, 1
    //   496: iload #18
    //   498: istore #16
    //   500: goto -> 328
    //   503: iload #16
    //   505: ifeq -> 512
    //   508: aload_2
    //   509: invokevirtual println : ()V
    //   512: iload #7
    //   514: ifeq -> 529
    //   517: iload_3
    //   518: iconst_2
    //   519: iand
    //   520: ifeq -> 526
    //   523: goto -> 529
    //   526: goto -> 751
    //   529: aload_0
    //   530: invokevirtual getDischargeLevelStepTracker : ()Landroid/os/BatteryStats$LevelStepTracker;
    //   533: astore #13
    //   535: aload_2
    //   536: ldc_w '  '
    //   539: ldc_w 'Discharge step durations:'
    //   542: aload #13
    //   544: iconst_0
    //   545: invokestatic dumpDurationSteps : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Z)Z
    //   548: ifeq -> 680
    //   551: invokestatic elapsedRealtime : ()J
    //   554: lstore #5
    //   556: aload_0
    //   557: lload #5
    //   559: ldc2_w 1000
    //   562: lmul
    //   563: invokevirtual computeBatteryTimeRemaining : (J)J
    //   566: lstore #5
    //   568: lload #5
    //   570: lconst_0
    //   571: lcmp
    //   572: iflt -> 596
    //   575: aload_2
    //   576: ldc_w '  Estimated discharge time remaining: '
    //   579: invokevirtual print : (Ljava/lang/String;)V
    //   582: lload #5
    //   584: ldc2_w 1000
    //   587: ldiv
    //   588: aload_2
    //   589: invokestatic formatDuration : (JLjava/io/PrintWriter;)V
    //   592: aload_2
    //   593: invokevirtual println : ()V
    //   596: aload_0
    //   597: invokevirtual getDischargeLevelStepTracker : ()Landroid/os/BatteryStats$LevelStepTracker;
    //   600: astore #13
    //   602: iconst_0
    //   603: istore #16
    //   605: getstatic android/os/BatteryStats.STEP_LEVEL_MODES_OF_INTEREST : [I
    //   608: astore #12
    //   610: iload #16
    //   612: aload #12
    //   614: arraylength
    //   615: if_icmpge -> 676
    //   618: getstatic android/os/BatteryStats.STEP_LEVEL_MODE_LABELS : [Ljava/lang/String;
    //   621: iload #16
    //   623: aaload
    //   624: astore #20
    //   626: aload #12
    //   628: iload #16
    //   630: iaload
    //   631: i2l
    //   632: lstore #5
    //   634: getstatic android/os/BatteryStats.STEP_LEVEL_MODE_VALUES : [I
    //   637: iload #16
    //   639: iaload
    //   640: i2l
    //   641: lstore #10
    //   643: aload #13
    //   645: lload #5
    //   647: lload #10
    //   649: aconst_null
    //   650: invokevirtual computeTimeEstimate : (JJ[I)J
    //   653: lstore #5
    //   655: aload_2
    //   656: ldc_w '  Estimated '
    //   659: aload #20
    //   661: ldc_w ' time: '
    //   664: lload #5
    //   666: invokestatic dumpTimeEstimate : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Z
    //   669: pop
    //   670: iinc #16, 1
    //   673: goto -> 605
    //   676: aload_2
    //   677: invokevirtual println : ()V
    //   680: aload_0
    //   681: invokevirtual getChargeLevelStepTracker : ()Landroid/os/BatteryStats$LevelStepTracker;
    //   684: astore #13
    //   686: aload_2
    //   687: ldc_w '  '
    //   690: ldc_w 'Charge step durations:'
    //   693: aload #13
    //   695: iconst_0
    //   696: invokestatic dumpDurationSteps : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Z)Z
    //   699: ifeq -> 751
    //   702: invokestatic elapsedRealtime : ()J
    //   705: lstore #5
    //   707: aload_0
    //   708: lload #5
    //   710: ldc2_w 1000
    //   713: lmul
    //   714: invokevirtual computeChargeTimeRemaining : (J)J
    //   717: lstore #5
    //   719: lload #5
    //   721: lconst_0
    //   722: lcmp
    //   723: iflt -> 747
    //   726: aload_2
    //   727: ldc_w '  Estimated charge time remaining: '
    //   730: invokevirtual print : (Ljava/lang/String;)V
    //   733: lload #5
    //   735: ldc2_w 1000
    //   738: ldiv
    //   739: aload_2
    //   740: invokestatic formatDuration : (JLjava/io/PrintWriter;)V
    //   743: aload_2
    //   744: invokevirtual println : ()V
    //   747: aload_2
    //   748: invokevirtual println : ()V
    //   751: iload #7
    //   753: ifeq -> 771
    //   756: iload_3
    //   757: iconst_4
    //   758: iand
    //   759: ifeq -> 765
    //   762: goto -> 771
    //   765: iconst_0
    //   766: istore #14
    //   768: goto -> 1344
    //   771: aload_2
    //   772: ldc_w 'Daily stats:'
    //   775: invokevirtual println : (Ljava/lang/String;)V
    //   778: aload_2
    //   779: ldc_w '  Current start time: '
    //   782: invokevirtual print : (Ljava/lang/String;)V
    //   785: aload_0
    //   786: invokevirtual getCurrentDailyStartTime : ()J
    //   789: lstore #5
    //   791: ldc_w 'yyyy-MM-dd-HH-mm-ss'
    //   794: lload #5
    //   796: invokestatic format : (Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;
    //   799: astore #13
    //   801: aload #13
    //   803: invokeinterface toString : ()Ljava/lang/String;
    //   808: astore #13
    //   810: aload_2
    //   811: aload #13
    //   813: invokevirtual println : (Ljava/lang/String;)V
    //   816: aload_2
    //   817: ldc_w '  Next min deadline: '
    //   820: invokevirtual print : (Ljava/lang/String;)V
    //   823: aload_0
    //   824: invokevirtual getNextMinDailyDeadline : ()J
    //   827: lstore #5
    //   829: ldc_w 'yyyy-MM-dd-HH-mm-ss'
    //   832: lload #5
    //   834: invokestatic format : (Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;
    //   837: astore #13
    //   839: aload #13
    //   841: invokeinterface toString : ()Ljava/lang/String;
    //   846: astore #13
    //   848: aload_2
    //   849: aload #13
    //   851: invokevirtual println : (Ljava/lang/String;)V
    //   854: aload_2
    //   855: ldc_w '  Next max deadline: '
    //   858: invokevirtual print : (Ljava/lang/String;)V
    //   861: aload_0
    //   862: invokevirtual getNextMaxDailyDeadline : ()J
    //   865: lstore #5
    //   867: ldc_w 'yyyy-MM-dd-HH-mm-ss'
    //   870: lload #5
    //   872: invokestatic format : (Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;
    //   875: astore #13
    //   877: aload #13
    //   879: invokeinterface toString : ()Ljava/lang/String;
    //   884: astore #13
    //   886: aload_2
    //   887: aload #13
    //   889: invokevirtual println : (Ljava/lang/String;)V
    //   892: new java/lang/StringBuilder
    //   895: dup
    //   896: bipush #64
    //   898: invokespecial <init> : (I)V
    //   901: astore #20
    //   903: iconst_1
    //   904: newarray int
    //   906: astore #21
    //   908: aload_0
    //   909: invokevirtual getDailyDischargeLevelStepTracker : ()Landroid/os/BatteryStats$LevelStepTracker;
    //   912: astore #13
    //   914: aload_0
    //   915: invokevirtual getDailyChargeLevelStepTracker : ()Landroid/os/BatteryStats$LevelStepTracker;
    //   918: astore #12
    //   920: aload_0
    //   921: invokevirtual getDailyPackageChanges : ()Ljava/util/ArrayList;
    //   924: astore #22
    //   926: aload #13
    //   928: getfield mNumStepDurations : I
    //   931: ifgt -> 953
    //   934: aload #12
    //   936: getfield mNumStepDurations : I
    //   939: ifgt -> 953
    //   942: aload #22
    //   944: ifnull -> 950
    //   947: goto -> 953
    //   950: goto -> 1087
    //   953: iload_3
    //   954: iconst_4
    //   955: iand
    //   956: ifne -> 1011
    //   959: iload #7
    //   961: ifne -> 967
    //   964: goto -> 1011
    //   967: aload_2
    //   968: ldc_w '  Current daily steps:'
    //   971: invokevirtual println : (Ljava/lang/String;)V
    //   974: aload_0
    //   975: aload_2
    //   976: ldc_w '    '
    //   979: ldc_w 'Discharge'
    //   982: aload #13
    //   984: aload #20
    //   986: aload #21
    //   988: invokespecial dumpDailyLevelStepSummary : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Ljava/lang/StringBuilder;[I)V
    //   991: aload_0
    //   992: aload_2
    //   993: ldc_w '    '
    //   996: ldc_w 'Charge'
    //   999: aload #12
    //   1001: aload #20
    //   1003: aload #21
    //   1005: invokespecial dumpDailyLevelStepSummary : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Ljava/lang/StringBuilder;[I)V
    //   1008: goto -> 1087
    //   1011: aload_2
    //   1012: ldc_w '    '
    //   1015: ldc_w '  Current daily discharge step durations:'
    //   1018: aload #13
    //   1020: iconst_0
    //   1021: invokestatic dumpDurationSteps : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Z)Z
    //   1024: ifeq -> 1044
    //   1027: aload_0
    //   1028: aload_2
    //   1029: ldc_w '      '
    //   1032: ldc_w 'Discharge'
    //   1035: aload #13
    //   1037: aload #20
    //   1039: aload #21
    //   1041: invokespecial dumpDailyLevelStepSummary : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Ljava/lang/StringBuilder;[I)V
    //   1044: aload_2
    //   1045: ldc_w '    '
    //   1048: ldc_w '  Current daily charge step durations:'
    //   1051: aload #12
    //   1053: iconst_0
    //   1054: invokestatic dumpDurationSteps : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Z)Z
    //   1057: ifeq -> 1077
    //   1060: aload_0
    //   1061: aload_2
    //   1062: ldc_w '      '
    //   1065: ldc_w 'Charge'
    //   1068: aload #12
    //   1070: aload #20
    //   1072: aload #21
    //   1074: invokespecial dumpDailyLevelStepSummary : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Ljava/lang/StringBuilder;[I)V
    //   1077: aload_0
    //   1078: aload_2
    //   1079: ldc_w '    '
    //   1082: aload #22
    //   1084: invokespecial dumpDailyPackageChanges : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/util/ArrayList;)V
    //   1087: ldc_w 'yyyy-MM-dd-HH-mm-ss'
    //   1090: astore #12
    //   1092: iconst_0
    //   1093: istore #16
    //   1095: aload_0
    //   1096: iload #16
    //   1098: invokevirtual getDailyItemLocked : (I)Landroid/os/BatteryStats$DailyItem;
    //   1101: astore #22
    //   1103: aload #22
    //   1105: ifnull -> 1337
    //   1108: iload_3
    //   1109: iconst_4
    //   1110: iand
    //   1111: ifeq -> 1118
    //   1114: aload_2
    //   1115: invokevirtual println : ()V
    //   1118: aload_2
    //   1119: ldc_w '  Daily from '
    //   1122: invokevirtual print : (Ljava/lang/String;)V
    //   1125: aload_2
    //   1126: aload #12
    //   1128: aload #22
    //   1130: getfield mStartTime : J
    //   1133: invokestatic format : (Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;
    //   1136: invokeinterface toString : ()Ljava/lang/String;
    //   1141: invokevirtual print : (Ljava/lang/String;)V
    //   1144: aload_2
    //   1145: ldc_w ' to '
    //   1148: invokevirtual print : (Ljava/lang/String;)V
    //   1151: aload_2
    //   1152: aload #12
    //   1154: aload #22
    //   1156: getfield mEndTime : J
    //   1159: invokestatic format : (Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;
    //   1162: invokeinterface toString : ()Ljava/lang/String;
    //   1167: invokevirtual print : (Ljava/lang/String;)V
    //   1170: aload_2
    //   1171: ldc_w ':'
    //   1174: invokevirtual println : (Ljava/lang/String;)V
    //   1177: iload_3
    //   1178: iconst_4
    //   1179: iand
    //   1180: ifne -> 1234
    //   1183: iload #7
    //   1185: ifne -> 1191
    //   1188: goto -> 1234
    //   1191: aload_0
    //   1192: aload_2
    //   1193: ldc_w '    '
    //   1196: ldc_w 'Discharge'
    //   1199: aload #22
    //   1201: getfield mDischargeSteps : Landroid/os/BatteryStats$LevelStepTracker;
    //   1204: aload #20
    //   1206: aload #21
    //   1208: invokespecial dumpDailyLevelStepSummary : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Ljava/lang/StringBuilder;[I)V
    //   1211: aload_0
    //   1212: aload_2
    //   1213: ldc_w '    '
    //   1216: ldc_w 'Charge'
    //   1219: aload #22
    //   1221: getfield mChargeSteps : Landroid/os/BatteryStats$LevelStepTracker;
    //   1224: aload #20
    //   1226: aload #21
    //   1228: invokespecial dumpDailyLevelStepSummary : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Ljava/lang/StringBuilder;[I)V
    //   1231: goto -> 1331
    //   1234: aload_2
    //   1235: ldc_w '      '
    //   1238: ldc_w '    Discharge step durations:'
    //   1241: aload #22
    //   1243: getfield mDischargeSteps : Landroid/os/BatteryStats$LevelStepTracker;
    //   1246: iconst_0
    //   1247: invokestatic dumpDurationSteps : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Z)Z
    //   1250: ifeq -> 1276
    //   1253: aload_0
    //   1254: aload_2
    //   1255: ldc_w '        '
    //   1258: ldc_w 'Discharge'
    //   1261: aload #22
    //   1263: getfield mDischargeSteps : Landroid/os/BatteryStats$LevelStepTracker;
    //   1266: aload #20
    //   1268: aload #21
    //   1270: invokespecial dumpDailyLevelStepSummary : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Ljava/lang/StringBuilder;[I)V
    //   1273: goto -> 1276
    //   1276: aload_2
    //   1277: ldc_w '      '
    //   1280: ldc_w '    Charge step durations:'
    //   1283: aload #22
    //   1285: getfield mChargeSteps : Landroid/os/BatteryStats$LevelStepTracker;
    //   1288: iconst_0
    //   1289: invokestatic dumpDurationSteps : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Z)Z
    //   1292: ifeq -> 1318
    //   1295: aload_0
    //   1296: aload_2
    //   1297: ldc_w '        '
    //   1300: ldc_w 'Charge'
    //   1303: aload #22
    //   1305: getfield mChargeSteps : Landroid/os/BatteryStats$LevelStepTracker;
    //   1308: aload #20
    //   1310: aload #21
    //   1312: invokespecial dumpDailyLevelStepSummary : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Landroid/os/BatteryStats$LevelStepTracker;Ljava/lang/StringBuilder;[I)V
    //   1315: goto -> 1318
    //   1318: aload_0
    //   1319: aload_2
    //   1320: ldc_w '    '
    //   1323: aload #22
    //   1325: getfield mPackageChanges : Ljava/util/ArrayList;
    //   1328: invokespecial dumpDailyPackageChanges : (Ljava/io/PrintWriter;Ljava/lang/String;Ljava/util/ArrayList;)V
    //   1331: iinc #16, 1
    //   1334: goto -> 1095
    //   1337: iconst_0
    //   1338: istore #14
    //   1340: aload_2
    //   1341: invokevirtual println : ()V
    //   1344: iload #7
    //   1346: ifeq -> 1355
    //   1349: iload_3
    //   1350: iconst_2
    //   1351: iand
    //   1352: ifeq -> 1453
    //   1355: aload_2
    //   1356: ldc_w 'Statistics since last charge:'
    //   1359: invokevirtual println : (Ljava/lang/String;)V
    //   1362: new java/lang/StringBuilder
    //   1365: dup
    //   1366: invokespecial <init> : ()V
    //   1369: astore #13
    //   1371: aload #13
    //   1373: ldc_w '  System starts: '
    //   1376: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1379: pop
    //   1380: aload #13
    //   1382: aload_0
    //   1383: invokevirtual getStartCount : ()I
    //   1386: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1389: pop
    //   1390: aload #13
    //   1392: ldc_w ', currently on battery: '
    //   1395: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1398: pop
    //   1399: aload #13
    //   1401: aload_0
    //   1402: invokevirtual getIsOnBattery : ()Z
    //   1405: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   1408: pop
    //   1409: aload #13
    //   1411: invokevirtual toString : ()Ljava/lang/String;
    //   1414: astore #13
    //   1416: aload_2
    //   1417: aload #13
    //   1419: invokevirtual println : (Ljava/lang/String;)V
    //   1422: iload_3
    //   1423: bipush #64
    //   1425: iand
    //   1426: ifeq -> 1435
    //   1429: iconst_1
    //   1430: istore #14
    //   1432: goto -> 1435
    //   1435: aload_0
    //   1436: aload_1
    //   1437: aload_2
    //   1438: ldc_w ''
    //   1441: iconst_0
    //   1442: iload #4
    //   1444: iload #14
    //   1446: invokevirtual dumpLocked : (Landroid/content/Context;Ljava/io/PrintWriter;Ljava/lang/String;IIZ)V
    //   1449: aload_2
    //   1450: invokevirtual println : ()V
    //   1453: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #6966	-> 0
    //   #6968	-> 4
    //   #6971	-> 20
    //   #6972	-> 32
    //   #6973	-> 39
    //   #6974	-> 46
    //   #6976	-> 53
    //   #6977	-> 60
    //   #6978	-> 73
    //   #6979	-> 80
    //   #6980	-> 87
    //   #6981	-> 94
    //   #6982	-> 101
    //   #6983	-> 108
    //   #6984	-> 116
    //   #6985	-> 123
    //   #6986	-> 133
    //   #6987	-> 140
    //   #6988	-> 149
    //   #6990	-> 153
    //   #6991	-> 157
    //   #6990	-> 160
    //   #6991	-> 169
    //   #6974	-> 171
    //   #6994	-> 171
    //   #6996	-> 178
    //   #6997	-> 188
    //   #6998	-> 195
    //   #6999	-> 205
    //   #7000	-> 210
    //   #7001	-> 219
    //   #7002	-> 226
    //   #7001	-> 236
    //   #7004	-> 236
    //   #7006	-> 268
    //   #7008	-> 272
    //   #7009	-> 276
    //   #7008	-> 279
    //   #7009	-> 284
    //   #7013	-> 286
    //   #7014	-> 298
    //   #7017	-> 299
    //   #7018	-> 304
    //   #7019	-> 310
    //   #7020	-> 317
    //   #7021	-> 320
    //   #7022	-> 325
    //   #7023	-> 335
    //   #7024	-> 347
    //   #7025	-> 354
    //   #7026	-> 363
    //   #7027	-> 380
    //   #7028	-> 392
    //   #7029	-> 401
    //   #7030	-> 408
    //   #7032	-> 411
    //   #7033	-> 426
    //   #7034	-> 442
    //   #7035	-> 460
    //   #7036	-> 467
    //   #7037	-> 476
    //   #7026	-> 483
    //   #7022	-> 493
    //   #7041	-> 503
    //   #7042	-> 508
    //   #7046	-> 512
    //   #7047	-> 529
    //   #7048	-> 529
    //   #7047	-> 535
    //   #7049	-> 551
    //   #7050	-> 551
    //   #7049	-> 556
    //   #7051	-> 568
    //   #7052	-> 575
    //   #7053	-> 582
    //   #7054	-> 592
    //   #7056	-> 596
    //   #7057	-> 602
    //   #7058	-> 618
    //   #7059	-> 643
    //   #7058	-> 655
    //   #7057	-> 670
    //   #7062	-> 676
    //   #7064	-> 680
    //   #7065	-> 680
    //   #7064	-> 686
    //   #7066	-> 702
    //   #7067	-> 702
    //   #7066	-> 707
    //   #7068	-> 719
    //   #7069	-> 726
    //   #7070	-> 733
    //   #7071	-> 743
    //   #7073	-> 747
    //   #7076	-> 751
    //   #7077	-> 771
    //   #7078	-> 778
    //   #7079	-> 785
    //   #7080	-> 785
    //   #7079	-> 791
    //   #7080	-> 801
    //   #7079	-> 810
    //   #7081	-> 816
    //   #7082	-> 823
    //   #7083	-> 823
    //   #7082	-> 829
    //   #7083	-> 839
    //   #7082	-> 848
    //   #7084	-> 854
    //   #7085	-> 861
    //   #7086	-> 861
    //   #7085	-> 867
    //   #7086	-> 877
    //   #7085	-> 886
    //   #7087	-> 892
    //   #7088	-> 903
    //   #7089	-> 908
    //   #7090	-> 914
    //   #7091	-> 920
    //   #7092	-> 926
    //   #7093	-> 953
    //   #7106	-> 967
    //   #7107	-> 974
    //   #7109	-> 991
    //   #7093	-> 1011
    //   #7094	-> 1011
    //   #7096	-> 1027
    //   #7099	-> 1044
    //   #7101	-> 1060
    //   #7104	-> 1077
    //   #7114	-> 1087
    //   #7115	-> 1095
    //   #7116	-> 1108
    //   #7117	-> 1108
    //   #7118	-> 1114
    //   #7120	-> 1118
    //   #7121	-> 1125
    //   #7122	-> 1144
    //   #7123	-> 1151
    //   #7124	-> 1170
    //   #7125	-> 1177
    //   #7138	-> 1191
    //   #7140	-> 1211
    //   #7125	-> 1234
    //   #7126	-> 1234
    //   #7128	-> 1253
    //   #7126	-> 1276
    //   #7131	-> 1276
    //   #7133	-> 1295
    //   #7131	-> 1318
    //   #7136	-> 1318
    //   #7115	-> 1331
    //   #7144	-> 1337
    //   #7146	-> 1344
    //   #7147	-> 1355
    //   #7148	-> 1362
    //   #7149	-> 1399
    //   #7148	-> 1416
    //   #7150	-> 1422
    //   #7152	-> 1449
    //   #7154	-> 1453
    // Exception table:
    //   from	to	target	type
    //   53	60	164	finally
    //   60	73	164	finally
    //   73	80	164	finally
    //   80	87	164	finally
    //   87	94	164	finally
    //   94	101	164	finally
    //   101	108	164	finally
    //   108	116	164	finally
    //   116	123	164	finally
    //   123	133	164	finally
    //   133	140	164	finally
    //   140	149	160	finally
    //   149	153	160	finally
    //   178	188	279	finally
    //   188	195	279	finally
    //   195	205	279	finally
    //   210	219	279	finally
    //   226	233	279	finally
    //   252	265	279	finally
    //   268	272	279	finally
  }
  
  public void dumpCheckinLocked(Context paramContext, PrintWriter paramPrintWriter, List<ApplicationInfo> paramList, int paramInt, long paramLong) {
    prepareForDumpLocked();
    int i = getParcelVersion();
    String str1 = getStartPlatformVersion();
    String str2 = getEndPlatformVersion();
    dumpLine(paramPrintWriter, 0, "i", "vers", new Object[] { Integer.valueOf(35), Integer.valueOf(i), str1, str2 });
    getHistoryBaseTime();
    SystemClock.elapsedRealtime();
    if ((paramInt & 0x18) != 0 && 
      startIteratingHistoryLocked()) {
      i = 0;
      try {
        for (; i < getHistoryStringPoolSize(); i++) {
          paramPrintWriter.print(9);
          paramPrintWriter.print(',');
          paramPrintWriter.print("hsp");
          paramPrintWriter.print(',');
          paramPrintWriter.print(i);
          paramPrintWriter.print(",");
          paramPrintWriter.print(getHistoryTagPoolUid(i));
          paramPrintWriter.print(",\"");
          str1 = getHistoryTagPoolString(i);
          str1 = str1.replace("\\", "\\\\");
          str1 = str1.replace("\"", "\\\"");
          paramPrintWriter.print(str1);
          paramPrintWriter.print("\"");
          paramPrintWriter.println();
        } 
        dumpHistoryLocked(paramPrintWriter, paramInt, paramLong, true);
      } finally {
        finishIteratingHistoryLocked();
      } 
    } 
    if ((paramInt & 0x8) != 0)
      return; 
    if (paramList != null) {
      SparseArray sparseArray2 = new SparseArray();
      for (i = 0; i < paramList.size(); i++) {
        ApplicationInfo applicationInfo = paramList.get(i);
        int k = applicationInfo.uid;
        k = UserHandle.getAppId(k);
        Pair pair2 = (Pair)sparseArray2.get(k);
        Pair pair1 = pair2;
        if (pair2 == null) {
          pair1 = new Pair(new ArrayList(), new MutableBoolean(false));
          sparseArray2.put(UserHandle.getAppId(applicationInfo.uid), pair1);
        } 
        ((ArrayList<String>)pair1.first).add(applicationInfo.packageName);
      } 
      SparseArray<? extends Uid> sparseArray = getUidStats();
      int j = sparseArray.size();
      String[] arrayOfString = new String[2];
      SparseArray sparseArray1;
      for (i = 0, sparseArray1 = sparseArray2; i < j; i++) {
        int k = UserHandle.getAppId(sparseArray.keyAt(i));
        Pair pair = (Pair)sparseArray1.get(k);
        if (pair != null && !((MutableBoolean)pair.second).value) {
          ((MutableBoolean)pair.second).value = true;
          for (byte b = 0; b < ((ArrayList)pair.first).size(); b++) {
            arrayOfString[0] = Integer.toString(k);
            arrayOfString[1] = ((ArrayList<String>)pair.first).get(b);
            dumpLine(paramPrintWriter, 0, "i", "uid", (Object[])arrayOfString);
          } 
        } 
      } 
    } 
    if ((paramInt & 0x4) == 0) {
      boolean bool;
      dumpDurationSteps(paramPrintWriter, "", "dsd", getDischargeLevelStepTracker(), true);
      String[] arrayOfString = new String[1];
      paramLong = computeBatteryTimeRemaining(SystemClock.elapsedRealtime() * 1000L);
      if (paramLong >= 0L) {
        arrayOfString[0] = Long.toString(paramLong);
        dumpLine(paramPrintWriter, 0, "i", "dtr", (Object[])arrayOfString);
      } 
      dumpDurationSteps(paramPrintWriter, "", "csd", getChargeLevelStepTracker(), true);
      paramLong = computeChargeTimeRemaining(1000L * SystemClock.elapsedRealtime());
      if (paramLong >= 0L) {
        arrayOfString[0] = Long.toString(paramLong);
        dumpLine(paramPrintWriter, 0, "i", "ctr", (Object[])arrayOfString);
      } 
      if ((paramInt & 0x40) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      dumpCheckinLocked(paramContext, paramPrintWriter, 0, -1, bool);
    } 
  }
  
  public void dumpProtoLocked(Context paramContext, FileDescriptor paramFileDescriptor, List<ApplicationInfo> paramList, int paramInt, long paramLong) {
    ProtoOutputStream protoOutputStream = new ProtoOutputStream(paramFileDescriptor);
    prepareForDumpLocked();
    if ((paramInt & 0x18) != 0) {
      dumpProtoHistoryLocked(protoOutputStream, paramInt, paramLong);
      protoOutputStream.flush();
      return;
    } 
    paramLong = protoOutputStream.start(1146756268033L);
    protoOutputStream.write(1120986464257L, 35);
    protoOutputStream.write(1112396529666L, getParcelVersion());
    protoOutputStream.write(1138166333443L, getStartPlatformVersion());
    protoOutputStream.write(1138166333444L, getEndPlatformVersion());
    if ((paramInt & 0x4) == 0) {
      boolean bool;
      if ((paramInt & 0x40) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      BatteryStatsHelper batteryStatsHelper = new BatteryStatsHelper(paramContext, false, bool);
      batteryStatsHelper.create(this);
      batteryStatsHelper.refreshStats(0, -1);
      dumpProtoAppsLocked(protoOutputStream, batteryStatsHelper, paramList);
      dumpProtoSystemLocked(protoOutputStream, batteryStatsHelper);
    } 
    protoOutputStream.end(paramLong);
    protoOutputStream.flush();
  }
  
  private void dumpProtoAppsLocked(ProtoOutputStream paramProtoOutputStream, BatteryStatsHelper paramBatteryStatsHelper, List<ApplicationInfo> paramList) {
    boolean bool = false;
    long l1 = SystemClock.uptimeMillis() * 1000L;
    long l2 = SystemClock.elapsedRealtime();
    long l3 = l2 * 1000L;
    long l4 = getBatteryUptime(l1);
    SparseArray sparseArray2 = new SparseArray();
    if (paramList != null)
      for (byte b1 = 0; b1 < paramList.size(); b1++) {
        ApplicationInfo applicationInfo = paramList.get(b1);
        int j = UserHandle.getAppId(applicationInfo.uid);
        ArrayList<String> arrayList = (ArrayList)sparseArray2.get(j);
        if (arrayList == null) {
          arrayList = new ArrayList();
          sparseArray2.put(j, arrayList);
        } 
        arrayList.add(applicationInfo.packageName);
      }  
    SparseArray<? extends Uid.Sensor> sparseArray = new SparseArray();
    List<BatterySipper> list1 = paramBatteryStatsHelper.getUsageList();
    if (list1 != null)
      for (byte b1 = 0; b1 < list1.size(); b1++) {
        BatterySipper batterySipper = list1.get(b1);
        if (batterySipper.drainType == BatterySipper.DrainType.APP)
          sparseArray.put(batterySipper.uidObj.getUid(), batterySipper); 
      }  
    SparseArray<? extends Uid> sparseArray3 = getUidStats();
    int i = sparseArray3.size();
    SparseArray sparseArray1;
    byte b;
    List<BatterySipper> list2;
    for (b = 0, list2 = list1, sparseArray1 = sparseArray2; b < i; b++, l3 = l6, uid2 = uid1) {
      ArrayMap<String, ? extends Uid.Pkg> arrayMap2;
      Uid uid2;
      Timer timer4;
      long l5 = paramProtoOutputStream.start(2246267895813L);
      Uid uid4 = (Uid)sparseArray3.valueAt(b);
      int k = sparseArray3.keyAt(b);
      paramProtoOutputStream.write(1120986464257L, k);
      int j = UserHandle.getAppId(k);
      ArrayList arrayList = (ArrayList)sparseArray1.get(j);
      if (arrayList == null)
        arrayList = new ArrayList(); 
      ArrayMap<String, ? extends Uid.Pkg> arrayMap1 = uid4.getPackageStats();
      for (j = arrayMap1.size() - 1; j >= 0; j--) {
        String str = (String)arrayMap1.keyAt(j);
        ArrayMap<String, ? extends Uid.Pkg.Serv> arrayMap = ((Uid.Pkg)arrayMap1.valueAt(j)).getServiceStats();
        if (arrayMap.size() != 0) {
          long l = paramProtoOutputStream.start(2246267895810L);
          paramProtoOutputStream.write(1138166333441L, str);
          arrayList.remove(str);
          for (int m = arrayMap.size() - 1; m >= 0; m--) {
            Uid.Pkg.Serv serv = (Uid.Pkg.Serv)arrayMap.valueAt(m);
            long l8 = roundUsToMs(serv.getStartTime(l4, 0));
            int n = serv.getStarts(0);
            int i1 = serv.getLaunches(0);
            if (l8 != 0L || n != 0 || i1 != 0) {
              long l9 = paramProtoOutputStream.start(2246267895810L);
              paramProtoOutputStream.write(1138166333441L, (String)arrayMap.keyAt(m));
              paramProtoOutputStream.write(1112396529666L, l8);
              paramProtoOutputStream.write(1120986464259L, n);
              paramProtoOutputStream.write(1120986464260L, i1);
              paramProtoOutputStream.end(l9);
            } 
          } 
          paramProtoOutputStream.end(l);
        } 
      } 
      long l6 = l3;
      for (String str : arrayList) {
        l3 = paramProtoOutputStream.start(2246267895810L);
        paramProtoOutputStream.write(1138166333441L, str);
        paramProtoOutputStream.end(l3);
      } 
      if (uid4.getAggregatedPartialWakelockTimer() != null) {
        Timer timer = uid4.getAggregatedPartialWakelockTimer();
        long l8 = timer.getTotalDurationMsLocked(l2);
        timer = timer.getSubTimer();
        if (timer != null) {
          l3 = timer.getTotalDurationMsLocked(l2);
        } else {
          l3 = 0L;
        } 
        long l9 = paramProtoOutputStream.start(1146756268056L);
        paramProtoOutputStream.write(1112396529665L, l8);
        paramProtoOutputStream.write(1112396529666L, l3);
        paramProtoOutputStream.end(l9);
      } 
      Timer timer1 = uid4.getAudioTurnedOnTimer();
      l3 = l5;
      SparseArray<? extends Uid.Sensor> sparseArray7 = sparseArray;
      dumpTimer(paramProtoOutputStream, 1146756268040L, timer1, l6, 0);
      ControllerActivityCounter controllerActivityCounter3 = uid4.getBluetoothControllerActivity();
      dumpControllerActivityProto(paramProtoOutputStream, 1146756268035L, controllerActivityCounter3, 0);
      Timer timer3 = uid4.getBluetoothScanTimer();
      if (timer3 != null) {
        l5 = paramProtoOutputStream.start(1146756268038L);
        dumpTimer(paramProtoOutputStream, 1146756268033L, timer3, l6, 0);
        timer1 = uid4.getBluetoothScanBackgroundTimer();
        dumpTimer(paramProtoOutputStream, 1146756268034L, timer1, l6, 0);
        timer1 = uid4.getBluetoothUnoptimizedScanTimer();
        dumpTimer(paramProtoOutputStream, 1146756268035L, timer1, l6, 0);
        timer1 = uid4.getBluetoothUnoptimizedScanBackgroundTimer();
        dumpTimer(paramProtoOutputStream, 1146756268036L, timer1, l6, 0);
        if (uid4.getBluetoothScanResultCounter() != null) {
          j = uid4.getBluetoothScanResultCounter().getCountLocked(0);
        } else {
          j = 0;
        } 
        paramProtoOutputStream.write(1120986464261L, j);
        if (uid4.getBluetoothScanResultBgCounter() != null) {
          j = uid4.getBluetoothScanResultBgCounter().getCountLocked(0);
        } else {
          j = 0;
        } 
        paramProtoOutputStream.write(1120986464262L, j);
        paramProtoOutputStream.end(l5);
      } 
      dumpTimer(paramProtoOutputStream, 1146756268041L, uid4.getCameraTurnedOnTimer(), l6, 0);
      l5 = paramProtoOutputStream.start(1146756268039L);
      paramProtoOutputStream.write(1112396529665L, roundUsToMs(uid4.getUserCpuTimeUs(0)));
      paramProtoOutputStream.write(1112396529666L, roundUsToMs(uid4.getSystemCpuTimeUs(0)));
      long[] arrayOfLong = getCpuFreqs();
      if (arrayOfLong != null) {
        long[] arrayOfLong1 = uid4.getCpuFreqTimes(0);
        if (arrayOfLong1 != null && arrayOfLong1.length == arrayOfLong.length) {
          long[] arrayOfLong3 = uid4.getScreenOffCpuFreqTimes(0);
          long[] arrayOfLong2 = arrayOfLong3;
          if (arrayOfLong3 == null)
            arrayOfLong2 = new long[arrayOfLong1.length]; 
          for (j = 0; j < arrayOfLong1.length; j++) {
            long l = paramProtoOutputStream.start(2246267895811L);
            paramProtoOutputStream.write(1120986464257L, j + 1);
            paramProtoOutputStream.write(1112396529666L, arrayOfLong1[j]);
            paramProtoOutputStream.write(1112396529667L, arrayOfLong2[j]);
            paramProtoOutputStream.end(l);
          } 
          timer4 = timer3;
          arrayMap2 = arrayMap1;
        } else {
          timer4 = timer3;
          arrayMap2 = arrayMap1;
        } 
      } else {
        arrayMap2 = arrayMap1;
        timer4 = timer3;
      } 
      SparseArray<? extends Uid.Sensor> sparseArray4;
      Uid uid1, uid3;
      SparseArray<? extends Uid.Sensor> sparseArray6;
      for (j = 0, uid3 = uid4, sparseArray4 = sparseArray7; j < 7; j++, timer8 = timer7, arrayOfLong2 = arrayOfLong1, sparseArray9 = sparseArray8, uid1 = uid6, sparseArray6 = sparseArray9, timer5 = timer8) {
        Uid uid5;
        SparseArray<? extends Uid.Sensor> sparseArray8;
        Timer timer6;
        long[] arrayOfLong1;
        SparseArray<? extends Uid.Sensor> sparseArray9;
        Timer timer5;
        SparseArray<? extends Uid.Sensor> sparseArray10;
        Uid uid6;
        long[] arrayOfLong3;
        Timer timer7;
        long[] arrayOfLong2;
        Timer timer8;
        long[] arrayOfLong4 = uid3.getCpuFreqTimes(0, j);
        if (arrayOfLong4 != null && arrayOfLong4.length == arrayOfLong.length) {
          long[] arrayOfLong6 = uid3.getScreenOffCpuFreqTimes(0, j);
          long[] arrayOfLong7 = arrayOfLong6;
          if (arrayOfLong6 == null)
            arrayOfLong7 = new long[arrayOfLong4.length]; 
          long l = paramProtoOutputStream.start(2246267895812L);
          paramProtoOutputStream.write(1159641169921L, j);
          for (byte b1 = 0; b1 < arrayOfLong4.length; b1++) {
            long l8 = paramProtoOutputStream.start(2246267895810L);
            paramProtoOutputStream.write(1120986464257L, b1 + 1);
            paramProtoOutputStream.write(1112396529666L, arrayOfLong4[b1]);
            paramProtoOutputStream.write(1112396529667L, arrayOfLong7[b1]);
            paramProtoOutputStream.end(l8);
          } 
          SparseArray<? extends Uid.Sensor> sparseArray11 = sparseArray4;
          uid5 = uid3;
          long[] arrayOfLong5 = arrayOfLong;
          timer6 = timer4;
          paramProtoOutputStream.end(l);
          arrayOfLong3 = arrayOfLong5;
          sparseArray10 = sparseArray11;
        } else {
          Uid uid = uid5;
          sparseArray8 = sparseArray10;
          Timer timer = timer6;
          arrayOfLong1 = arrayOfLong3;
          uid6 = uid;
          timer7 = timer;
        } 
      } 
      paramProtoOutputStream.end(l5);
      Timer timer2 = sparseArray6.getFlashlightTurnedOnTimer();
      dumpTimer(paramProtoOutputStream, 1146756268042L, timer2, l6, 0);
      dumpTimer(paramProtoOutputStream, 1146756268043L, sparseArray6.getForegroundActivityTimer(), l6, 0);
      dumpTimer(paramProtoOutputStream, 1146756268044L, sparseArray6.getForegroundServiceTimer(), l6, 0);
      ArrayMap<String, SparseIntArray> arrayMap6 = sparseArray6.getJobCompletionStats();
      for (j = 0; j < arrayMap6.size(); j++) {
        SparseIntArray sparseIntArray = (SparseIntArray)arrayMap6.valueAt(j);
        if (sparseIntArray != null) {
          long l = paramProtoOutputStream.start(2246267895824L);
          paramProtoOutputStream.write(1138166333441L, (String)arrayMap6.keyAt(j));
          for (int m : JobParameters.getJobStopReasonCodes()) {
            long l8 = paramProtoOutputStream.start(2246267895810L);
            paramProtoOutputStream.write(1159641169921L, m);
            paramProtoOutputStream.write(1120986464258L, sparseIntArray.get(m, 0));
            paramProtoOutputStream.end(l8);
          } 
          paramProtoOutputStream.end(l);
        } 
      } 
      ArrayMap<String, ? extends Timer> arrayMap4 = sparseArray6.getJobStats();
      for (j = arrayMap4.size() - 1; j >= 0; j--) {
        Timer timer6 = (Timer)arrayMap4.valueAt(j);
        Timer timer5 = timer6.getSubTimer();
        l5 = paramProtoOutputStream.start(2246267895823L);
        paramProtoOutputStream.write(1138166333441L, (String)arrayMap4.keyAt(j));
        dumpTimer(paramProtoOutputStream, 1146756268034L, timer6, l6, 0);
        dumpTimer(paramProtoOutputStream, 1146756268035L, timer5, l6, 0);
        paramProtoOutputStream.end(l5);
      } 
      ControllerActivityCounter controllerActivityCounter1 = sparseArray6.getModemControllerActivity();
      dumpControllerActivityProto(paramProtoOutputStream, 1146756268036L, controllerActivityCounter1, 0);
      l5 = paramProtoOutputStream.start(1146756268049L);
      long l7 = sparseArray6.getNetworkActivityBytes(0, 0);
      paramProtoOutputStream.write(1112396529665L, l7);
      l7 = sparseArray6.getNetworkActivityBytes(1, 0);
      paramProtoOutputStream.write(1112396529666L, l7);
      l7 = sparseArray6.getNetworkActivityBytes(2, 0);
      paramProtoOutputStream.write(1112396529667L, l7);
      l7 = sparseArray6.getNetworkActivityBytes(3, 0);
      paramProtoOutputStream.write(1112396529668L, l7);
      l7 = sparseArray6.getNetworkActivityBytes(4, 0);
      paramProtoOutputStream.write(1112396529669L, l7);
      l7 = sparseArray6.getNetworkActivityBytes(5, 0);
      paramProtoOutputStream.write(1112396529670L, l7);
      l7 = sparseArray6.getNetworkActivityPackets(0, 0);
      paramProtoOutputStream.write(1112396529671L, l7);
      l7 = sparseArray6.getNetworkActivityPackets(1, 0);
      paramProtoOutputStream.write(1112396529672L, l7);
      l7 = sparseArray6.getNetworkActivityPackets(2, 0);
      paramProtoOutputStream.write(1112396529673L, l7);
      l7 = sparseArray6.getNetworkActivityPackets(3, 0);
      paramProtoOutputStream.write(1112396529674L, l7);
      l7 = roundUsToMs(sparseArray6.getMobileRadioActiveTime(0));
      paramProtoOutputStream.write(1112396529675L, l7);
      j = sparseArray6.getMobileRadioActiveCount(0);
      paramProtoOutputStream.write(1120986464268L, j);
      l7 = sparseArray6.getMobileRadioApWakeupCount(0);
      paramProtoOutputStream.write(1120986464269L, l7);
      l7 = sparseArray6.getWifiRadioApWakeupCount(0);
      paramProtoOutputStream.write(1120986464270L, l7);
      l7 = sparseArray6.getNetworkActivityBytes(6, 0);
      paramProtoOutputStream.write(1112396529679L, l7);
      l7 = sparseArray6.getNetworkActivityBytes(7, 0);
      paramProtoOutputStream.write(1112396529680L, l7);
      l7 = sparseArray6.getNetworkActivityBytes(8, 0);
      paramProtoOutputStream.write(1112396529681L, l7);
      l7 = sparseArray6.getNetworkActivityBytes(9, 0);
      paramProtoOutputStream.write(1112396529682L, l7);
      l7 = sparseArray6.getNetworkActivityPackets(6, 0);
      paramProtoOutputStream.write(1112396529683L, l7);
      l7 = sparseArray6.getNetworkActivityPackets(7, 0);
      paramProtoOutputStream.write(1112396529684L, l7);
      l7 = sparseArray6.getNetworkActivityPackets(8, 0);
      paramProtoOutputStream.write(1112396529685L, l7);
      l7 = sparseArray6.getNetworkActivityPackets(9, 0);
      paramProtoOutputStream.write(1112396529686L, l7);
      paramProtoOutputStream.end(l5);
      BatterySipper batterySipper = (BatterySipper)uid1.get(k);
      if (batterySipper != null) {
        l7 = paramProtoOutputStream.start(1146756268050L);
        paramProtoOutputStream.write(1103806595073L, batterySipper.totalPowerMah);
        paramProtoOutputStream.write(1133871366146L, batterySipper.shouldHide);
        paramProtoOutputStream.write(1103806595075L, batterySipper.screenPowerMah);
        paramProtoOutputStream.write(1103806595076L, batterySipper.proportionalSmearMah);
        paramProtoOutputStream.end(l7);
      } 
      arrayMap6 = (ArrayMap)sparseArray6.getProcessStats();
      for (j = arrayMap6.size() - 1; j >= 0; j--) {
        Uid.Proc proc = (Uid.Proc)arrayMap6.valueAt(j);
        l7 = paramProtoOutputStream.start(2246267895827L);
        paramProtoOutputStream.write(1138166333441L, (String)arrayMap6.keyAt(j));
        paramProtoOutputStream.write(1112396529666L, proc.getUserTime(0));
        paramProtoOutputStream.write(1112396529667L, proc.getSystemTime(0));
        paramProtoOutputStream.write(1112396529668L, proc.getForegroundTime(0));
        paramProtoOutputStream.write(1120986464261L, proc.getStarts(0));
        paramProtoOutputStream.write(1120986464262L, proc.getNumAnrs(0));
        paramProtoOutputStream.write(1120986464263L, proc.getNumCrashes(0));
        paramProtoOutputStream.end(l7);
      } 
      sparseArray7 = sparseArray6.getSensorStats();
      ArrayMap<String, SparseIntArray> arrayMap3;
      for (j = 0, arrayMap3 = arrayMap6; j < sparseArray7.size(); j++) {
        Uid.Sensor sensor = (Uid.Sensor)sparseArray7.valueAt(j);
        Timer timer = sensor.getSensorTime();
        if (timer != null) {
          Timer timer5 = sensor.getSensorBackgroundTime();
          int m = sparseArray7.keyAt(j);
          l7 = paramProtoOutputStream.start(2246267895829L);
          paramProtoOutputStream.write(1120986464257L, m);
          dumpTimer(paramProtoOutputStream, 1146756268034L, timer, l6, 0);
          dumpTimer(paramProtoOutputStream, 1146756268035L, timer5, l6, 0);
          paramProtoOutputStream.end(l7);
        } 
      } 
      SparseArray<? extends Uid.Sensor> sparseArray5;
      for (j = 0, sparseArray5 = sparseArray6; j < 7; j++) {
        long l = roundUsToMs(sparseArray5.getProcessStateTime(j, l6, 0));
        if (l != 0L) {
          l7 = paramProtoOutputStream.start(2246267895828L);
          paramProtoOutputStream.write(1159641169921L, j);
          paramProtoOutputStream.write(1112396529666L, l);
          paramProtoOutputStream.end(l7);
        } 
      } 
      ArrayMap<String, ? extends Timer> arrayMap5 = sparseArray5.getSyncStats();
      for (j = arrayMap5.size() - 1; j >= 0; j--) {
        Timer timer6 = (Timer)arrayMap5.valueAt(j);
        Timer timer5 = timer6.getSubTimer();
        l7 = paramProtoOutputStream.start(2246267895830L);
        paramProtoOutputStream.write(1138166333441L, (String)arrayMap5.keyAt(j));
        dumpTimer(paramProtoOutputStream, 1146756268034L, timer6, l6, 0);
        dumpTimer(paramProtoOutputStream, 1146756268035L, timer5, l6, 0);
        paramProtoOutputStream.end(l7);
      } 
      if (sparseArray5.hasUserActivity())
        for (j = 0; j < Uid.NUM_USER_ACTIVITY_TYPES; j++) {
          int m = sparseArray5.getUserActivityCount(j, 0);
          if (m != 0) {
            l7 = paramProtoOutputStream.start(2246267895831L);
            paramProtoOutputStream.write(1159641169921L, j);
            paramProtoOutputStream.write(1120986464258L, m);
            paramProtoOutputStream.end(l7);
          } 
        }  
      dumpTimer(paramProtoOutputStream, 1146756268045L, sparseArray5.getVibratorOnTimer(), l6, 0);
      dumpTimer(paramProtoOutputStream, 1146756268046L, sparseArray5.getVideoTurnedOnTimer(), l6, 0);
      arrayMap5 = (ArrayMap)sparseArray5.getWakelockStats();
      for (j = arrayMap5.size() - 1; j >= 0; j--) {
        Uid.Wakelock wakelock = (Uid.Wakelock)arrayMap5.valueAt(j);
        l5 = paramProtoOutputStream.start(2246267895833L);
        paramProtoOutputStream.write(1138166333441L, (String)arrayMap5.keyAt(j));
        dumpTimer(paramProtoOutputStream, 1146756268034L, wakelock.getWakeTime(1), l6, 0);
        Timer timer = wakelock.getWakeTime(0);
        if (timer != null) {
          dumpTimer(paramProtoOutputStream, 1146756268035L, timer, l6, 0);
          dumpTimer(paramProtoOutputStream, 1146756268036L, timer.getSubTimer(), l6, 0);
        } 
        dumpTimer(paramProtoOutputStream, 1146756268037L, wakelock.getWakeTime(2), l6, 0);
        paramProtoOutputStream.end(l5);
      } 
      dumpTimer(paramProtoOutputStream, 1146756268060L, sparseArray5.getMulticastWakelockStats(), l6, 0);
      for (j = arrayMap2.size() - 1; j >= 0; j--, arrayMap7 = arrayMap8) {
        ArrayMap<String, ? extends Uid.Pkg> arrayMap7, arrayMap8 = arrayMap2;
        Uid.Pkg pkg = (Uid.Pkg)arrayMap8.valueAt(j);
        arrayMap6 = (ArrayMap)pkg.getWakeupAlarmStats();
        for (int m = arrayMap6.size() - 1; m >= 0; m--) {
          l5 = paramProtoOutputStream.start(2246267895834L);
          paramProtoOutputStream.write(1138166333441L, (String)arrayMap6.keyAt(m));
          k = ((Counter)arrayMap6.valueAt(m)).getCountLocked(0);
          paramProtoOutputStream.write(1120986464258L, k);
          paramProtoOutputStream.end(l5);
        } 
      } 
      ControllerActivityCounter controllerActivityCounter2 = sparseArray5.getWifiControllerActivity();
      dumpControllerActivityProto(paramProtoOutputStream, 1146756268037L, controllerActivityCounter2, 0);
      l5 = paramProtoOutputStream.start(1146756268059L);
      l7 = roundUsToMs(sparseArray5.getFullWifiLockTime(l6, 0));
      paramProtoOutputStream.write(1112396529665L, l7);
      dumpTimer(paramProtoOutputStream, 1146756268035L, sparseArray5.getWifiScanTimer(), l6, 0);
      l7 = roundUsToMs(sparseArray5.getWifiRunningTime(l6, 0));
      paramProtoOutputStream.write(1112396529666L, l7);
      dumpTimer(paramProtoOutputStream, 1146756268036L, sparseArray5.getWifiScanBackgroundTimer(), l6, 0);
      paramProtoOutputStream.end(l5);
      paramProtoOutputStream.end(l3);
    } 
  }
  
  private void dumpProtoHistoryLocked(ProtoOutputStream paramProtoOutputStream, int paramInt, long paramLong) {
    if (!startIteratingHistoryLocked())
      return; 
    paramProtoOutputStream.write(1120986464257L, 35);
    paramProtoOutputStream.write(1112396529666L, getParcelVersion());
    String str = getStartPlatformVersion();
    paramProtoOutputStream.write(1138166333443L, str);
    str = getEndPlatformVersion();
    paramProtoOutputStream.write(1138166333444L, str);
    byte b = 0;
    try {
      for (; b < getHistoryStringPoolSize(); b++) {
        long l = paramProtoOutputStream.start(2246267895813L);
        paramProtoOutputStream.write(1120986464257L, b);
        paramProtoOutputStream.write(1120986464258L, getHistoryTagPoolUid(b));
        str = getHistoryTagPoolString(b);
        paramProtoOutputStream.write(1138166333443L, str);
        paramProtoOutputStream.end(l);
      } 
      HistoryPrinter historyPrinter = new HistoryPrinter();
      this();
      HistoryItem historyItem = new HistoryItem();
      this();
      long l2 = -1L;
      long l1 = -1L;
      b = 0;
      String str1 = null;
      while (getNextHistoryLocked(historyItem)) {
        l2 = historyItem.time;
        if (l1 < 0L)
          l1 = l2; 
        if (historyItem.time >= paramLong) {
          boolean bool;
          byte b1;
          if (paramLong >= 0L && b == 0) {
            if (historyItem.cmd == 5 || historyItem.cmd == 7 || historyItem.cmd == 4 || historyItem.cmd == 8) {
              b = 1;
              if ((paramInt & 0x20) != 0) {
                bool = true;
              } else {
                bool = false;
              } 
              historyPrinter.printNextItem(paramProtoOutputStream, historyItem, l1, bool);
              historyItem.cmd = 0;
            } else if (historyItem.currentTime != 0L) {
              b = 1;
              byte b2 = historyItem.cmd;
              historyItem.cmd = 5;
              if ((paramInt & 0x20) != 0) {
                bool = true;
              } else {
                bool = false;
              } 
              historyPrinter.printNextItem(paramProtoOutputStream, historyItem, l1, bool);
              historyItem.cmd = b2;
            } 
            str = str1;
            b1 = b;
            if (str1 != null) {
              if (historyItem.cmd != 0) {
                if ((paramInt & 0x20) != 0) {
                  bool = true;
                } else {
                  bool = false;
                } 
                historyPrinter.printNextItem(paramProtoOutputStream, historyItem, l1, bool);
                historyItem.cmd = 0;
              } 
              int i = historyItem.eventCode;
              HistoryTag historyTag1 = historyItem.eventTag;
              HistoryTag historyTag2 = new HistoryTag();
              this();
              historyItem.eventTag = historyTag2;
              for (b1 = 0; b1 < 22; b1++) {
                HashMap<String, SparseIntArray> hashMap = str1.getStateForEvent(b1);
                if (hashMap != null)
                  for (Map.Entry<String, SparseIntArray> entry : hashMap.entrySet()) {
                    SparseIntArray sparseIntArray2 = (SparseIntArray)entry.getValue();
                    SparseIntArray sparseIntArray1;
                    HistoryTag historyTag;
                    byte b2;
                    for (b2 = 0, historyTag = historyTag1, sparseIntArray1 = sparseIntArray2; b2 < sparseIntArray1.size(); b2++) {
                      historyItem.eventCode = b1;
                      historyItem.eventTag.string = (String)entry.getKey();
                      historyItem.eventTag.uid = sparseIntArray1.keyAt(b2);
                      historyItem.eventTag.poolIdx = sparseIntArray1.valueAt(b2);
                      if ((paramInt & 0x20) != 0) {
                        bool = true;
                      } else {
                        bool = false;
                      } 
                      historyPrinter.printNextItem(paramProtoOutputStream, historyItem, l1, bool);
                      historyItem.wakeReasonTag = null;
                      historyItem.wakelockTag = null;
                    } 
                    historyTag1 = historyTag;
                  }  
              } 
              historyItem.eventCode = i;
              historyItem.eventTag = historyTag1;
              historyTag1 = null;
              b1 = b;
            } 
          } else {
            b1 = b;
            str = str1;
          } 
          if ((paramInt & 0x20) != 0) {
            bool = true;
          } else {
            bool = false;
          } 
          historyPrinter.printNextItem(paramProtoOutputStream, historyItem, l1, bool);
          b = b1;
          str1 = str;
        } 
      } 
      if (paramLong >= 0L) {
        commitCurrentHistoryBatchLocked();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("NEXT: ");
        stringBuilder.append(1L + l2);
        paramProtoOutputStream.write(2237677961222L, stringBuilder.toString());
      } 
      return;
    } finally {
      finishIteratingHistoryLocked();
    } 
  }
  
  private void dumpProtoSystemLocked(ProtoOutputStream paramProtoOutputStream, BatteryStatsHelper paramBatteryStatsHelper) {
    long l1 = paramProtoOutputStream.start(1146756268038L);
    long l2 = SystemClock.uptimeMillis() * 1000L;
    long l3 = SystemClock.elapsedRealtime();
    long l4 = l3 * 1000L;
    l3 = paramProtoOutputStream.start(1146756268033L);
    paramProtoOutputStream.write(1112396529665L, getStartClockTime());
    paramProtoOutputStream.write(1112396529666L, getStartCount());
    long l5 = computeRealtime(l4, 0) / 1000L;
    paramProtoOutputStream.write(1112396529667L, l5);
    l5 = computeUptime(l2, 0) / 1000L;
    paramProtoOutputStream.write(1112396529668L, l5);
    l5 = computeBatteryRealtime(l4, 0) / 1000L;
    paramProtoOutputStream.write(1112396529669L, l5);
    l5 = computeBatteryUptime(l2, 0) / 1000L;
    paramProtoOutputStream.write(1112396529670L, l5);
    l5 = computeBatteryScreenOffRealtime(l4, 0) / 1000L;
    paramProtoOutputStream.write(1112396529671L, l5);
    l2 = computeBatteryScreenOffUptime(l2, 0) / 1000L;
    paramProtoOutputStream.write(1112396529672L, l2);
    l2 = getScreenDozeTime(l4, 0) / 1000L;
    paramProtoOutputStream.write(1112396529673L, l2);
    int i = getEstimatedBatteryCapacity();
    paramProtoOutputStream.write(1112396529674L, i);
    i = getMinLearnedBatteryCapacity();
    paramProtoOutputStream.write(1112396529675L, i);
    i = getMaxLearnedBatteryCapacity();
    paramProtoOutputStream.write(1112396529676L, i);
    paramProtoOutputStream.end(l3);
    l5 = paramProtoOutputStream.start(1146756268034L);
    i = getLowDischargeAmountSinceCharge();
    paramProtoOutputStream.write(1120986464257L, i);
    i = getHighDischargeAmountSinceCharge();
    paramProtoOutputStream.write(1120986464258L, i);
    i = getDischargeAmountScreenOnSinceCharge();
    paramProtoOutputStream.write(1120986464259L, i);
    i = getDischargeAmountScreenOffSinceCharge();
    paramProtoOutputStream.write(1120986464260L, i);
    i = getDischargeAmountScreenDozeSinceCharge();
    paramProtoOutputStream.write(1120986464261L, i);
    l3 = getUahDischarge(0) / 1000L;
    paramProtoOutputStream.write(1112396529670L, l3);
    l3 = getUahDischargeScreenOff(0) / 1000L;
    paramProtoOutputStream.write(1112396529671L, l3);
    l3 = getUahDischargeScreenDoze(0) / 1000L;
    paramProtoOutputStream.write(1112396529672L, l3);
    l3 = getUahDischargeLightDoze(0) / 1000L;
    paramProtoOutputStream.write(1112396529673L, l3);
    l3 = getUahDischargeDeepDoze(0) / 1000L;
    paramProtoOutputStream.write(1112396529674L, l3);
    paramProtoOutputStream.end(l5);
    l3 = computeChargeTimeRemaining(l4);
    if (l3 >= 0L) {
      paramProtoOutputStream.write(1112396529667L, l3 / 1000L);
    } else {
      l3 = computeBatteryTimeRemaining(l4);
      if (l3 >= 0L) {
        paramProtoOutputStream.write(1112396529668L, l3 / 1000L);
      } else {
        paramProtoOutputStream.write(1112396529668L, -1);
      } 
    } 
    dumpDurationSteps(paramProtoOutputStream, 2246267895813L, getChargeLevelStepTracker());
    i = 0;
    l2 = l3;
    l3 = l4;
    while (true) {
      int k = NUM_DATA_CONNECTION_TYPES;
      boolean bool = true;
      if (i < k) {
        if (i != 0)
          bool = false; 
        if (i == DATA_CONNECTION_OTHER || i == DATA_CONNECTION_EMERGENCY_SERVICE) {
          k = 0;
        } else {
          k = i;
        } 
        l4 = paramProtoOutputStream.start(2246267895816L);
        if (bool) {
          paramProtoOutputStream.write(1133871366146L, bool);
        } else {
          paramProtoOutputStream.write(1159641169921L, k);
        } 
        Timer timer = getPhoneDataConnectionTimer(i);
        dumpTimer(paramProtoOutputStream, 1146756268035L, timer, l3, 0);
        paramProtoOutputStream.end(l4);
        i++;
        continue;
      } 
      break;
    } 
    dumpDurationSteps(paramProtoOutputStream, 2246267895814L, getDischargeLevelStepTracker());
    long[] arrayOfLong = getCpuFreqs();
    if (arrayOfLong != null)
      for (int k = arrayOfLong.length; i < k; ) {
        l2 = arrayOfLong[i];
        paramProtoOutputStream.write(2211908157447L, l2);
        i++;
      }  
    ControllerActivityCounter controllerActivityCounter = getBluetoothControllerActivity();
    dumpControllerActivityProto(paramProtoOutputStream, 1146756268041L, controllerActivityCounter, 0);
    controllerActivityCounter = getModemControllerActivity();
    dumpControllerActivityProto(paramProtoOutputStream, 1146756268042L, controllerActivityCounter, 0);
    l2 = paramProtoOutputStream.start(1146756268044L);
    l4 = getNetworkActivityBytes(0, 0);
    paramProtoOutputStream.write(1112396529665L, l4);
    l4 = getNetworkActivityBytes(1, 0);
    paramProtoOutputStream.write(1112396529666L, l4);
    l4 = getNetworkActivityPackets(0, 0);
    paramProtoOutputStream.write(1112396529669L, l4);
    l4 = getNetworkActivityPackets(1, 0);
    paramProtoOutputStream.write(1112396529670L, l4);
    l4 = getNetworkActivityBytes(2, 0);
    paramProtoOutputStream.write(1112396529667L, l4);
    l4 = getNetworkActivityBytes(3, 0);
    paramProtoOutputStream.write(1112396529668L, l4);
    l4 = getNetworkActivityPackets(2, 0);
    paramProtoOutputStream.write(1112396529671L, l4);
    l4 = getNetworkActivityPackets(3, 0);
    paramProtoOutputStream.write(1112396529672L, l4);
    l4 = getNetworkActivityBytes(4, 0);
    paramProtoOutputStream.write(1112396529673L, l4);
    l4 = getNetworkActivityBytes(5, 0);
    paramProtoOutputStream.write(1112396529674L, l4);
    paramProtoOutputStream.end(l2);
    controllerActivityCounter = getWifiControllerActivity();
    dumpControllerActivityProto(paramProtoOutputStream, 1146756268043L, controllerActivityCounter, 0);
    l4 = paramProtoOutputStream.start(1146756268045L);
    l2 = getWifiOnTime(l3, 0) / 1000L;
    paramProtoOutputStream.write(1112396529665L, l2);
    l2 = getGlobalWifiRunningTime(l3, 0) / 1000L;
    paramProtoOutputStream.write(1112396529666L, l2);
    paramProtoOutputStream.end(l4);
    Map<String, ? extends Timer> map3 = getKernelWakelockStats();
    for (Iterator<Map.Entry> iterator = map3.entrySet().iterator(); iterator.hasNext(); ) {
      Map.Entry entry = iterator.next();
      l4 = paramProtoOutputStream.start(2246267895822L);
      paramProtoOutputStream.write(1138166333441L, (String)entry.getKey());
      dumpTimer(paramProtoOutputStream, 1146756268034L, (Timer)entry.getValue(), l3, 0);
      paramProtoOutputStream.end(l4);
    } 
    SparseArray<? extends Uid> sparseArray = getUidStats();
    for (i = 0, l5 = 0L, l2 = 0L; i < sparseArray.size(); i++) {
      Uid uid = (Uid)sparseArray.valueAt(i);
      ArrayMap<String, ? extends Uid.Wakelock> arrayMap = uid.getWakelockStats();
      for (int k = arrayMap.size() - 1; k >= 0; k--, l2 = l4) {
        Uid.Wakelock wakelock = (Uid.Wakelock)arrayMap.valueAt(k);
        Timer timer = wakelock.getWakeTime(1);
        if (timer != null)
          l5 += timer.getTotalTimeLocked(l3, 0); 
        timer = wakelock.getWakeTime(0);
        l4 = l2;
        if (timer != null)
          l4 = l2 + timer.getTotalTimeLocked(l3, 0); 
      } 
    } 
    l4 = paramProtoOutputStream.start(1146756268047L);
    long l6 = getScreenOnTime(l3, 0) / 1000L;
    paramProtoOutputStream.write(1112396529665L, l6);
    l6 = getPhoneOnTime(l3, 0) / 1000L;
    paramProtoOutputStream.write(1112396529666L, l6);
    paramProtoOutputStream.write(1112396529667L, l5 / 1000L);
    paramProtoOutputStream.write(1112396529668L, l2 / 1000L);
    l2 = getMobileRadioActiveTime(l3, 0) / 1000L;
    paramProtoOutputStream.write(1112396529669L, l2);
    l2 = getMobileRadioActiveAdjustedTime(0) / 1000L;
    paramProtoOutputStream.write(1112396529670L, l2);
    i = getMobileRadioActiveCount(0);
    paramProtoOutputStream.write(1120986464263L, i);
    l2 = getMobileRadioActiveUnknownTime(0) / 1000L;
    paramProtoOutputStream.write(1120986464264L, l2);
    l2 = getInteractiveTime(l3, 0) / 1000L;
    paramProtoOutputStream.write(1112396529673L, l2);
    l2 = getPowerSaveModeEnabledTime(l3, 0) / 1000L;
    paramProtoOutputStream.write(1112396529674L, l2);
    i = getNumConnectivityChange(0);
    paramProtoOutputStream.write(1120986464267L, i);
    l2 = getDeviceIdleModeTime(2, l3, 0) / 1000L;
    paramProtoOutputStream.write(1112396529676L, l2);
    i = getDeviceIdleModeCount(2, 0);
    paramProtoOutputStream.write(1120986464269L, i);
    l2 = getDeviceIdlingTime(2, l3, 0) / 1000L;
    paramProtoOutputStream.write(1112396529678L, l2);
    i = getDeviceIdlingCount(2, 0);
    paramProtoOutputStream.write(1120986464271L, i);
    l2 = getLongestDeviceIdleModeTime(2);
    paramProtoOutputStream.write(1112396529680L, l2);
    l2 = getDeviceIdleModeTime(1, l3, 0) / 1000L;
    paramProtoOutputStream.write(1112396529681L, l2);
    i = getDeviceIdleModeCount(1, 0);
    paramProtoOutputStream.write(1120986464274L, i);
    l2 = getDeviceIdlingTime(1, l3, 0) / 1000L;
    paramProtoOutputStream.write(1112396529683L, l2);
    i = getDeviceIdlingCount(1, 0);
    paramProtoOutputStream.write(1120986464276L, i);
    l2 = getLongestDeviceIdleModeTime(1);
    paramProtoOutputStream.write(1112396529685L, l2);
    paramProtoOutputStream.end(l4);
    l5 = getWifiMulticastWakelockTime(l3, 0);
    int j = getWifiMulticastWakelockCount(0);
    l2 = paramProtoOutputStream.start(1146756268055L);
    paramProtoOutputStream.write(1112396529665L, l5 / 1000L);
    paramProtoOutputStream.write(1120986464258L, j);
    paramProtoOutputStream.end(l2);
    List<BatterySipper> list = paramBatteryStatsHelper.getUsageList();
    if (list != null)
      for (byte b = 0; b < list.size(); b++) {
        BatterySipper batterySipper = list.get(b);
        i = 0;
        int k = 0;
        switch (null.$SwitchMap$com$android$internal$os$BatterySipper$DrainType[batterySipper.drainType.ordinal()]) {
          default:
            l5 = paramProtoOutputStream.start(2246267895825L);
            paramProtoOutputStream.write(1159641169921L, i);
            paramProtoOutputStream.write(1120986464258L, k);
            paramProtoOutputStream.write(1103806595075L, batterySipper.totalPowerMah);
            paramProtoOutputStream.write(1133871366148L, batterySipper.shouldHide);
            paramProtoOutputStream.write(1103806595077L, batterySipper.screenPowerMah);
            paramProtoOutputStream.write(1103806595078L, batterySipper.proportionalSmearMah);
            paramProtoOutputStream.end(l5);
            break;
          case 14:
            i = 12;
          case 13:
            i = 11;
          case 12:
            i = 10;
          case 11:
            i = 9;
          case 10:
            i = 8;
            k = UserHandle.getUid(batterySipper.userId, 0);
          case 9:
            break;
          case 8:
            i = 6;
          case 7:
            i = 7;
          case 6:
            i = 5;
          case 5:
            i = 4;
          case 4:
            i = 3;
          case 3:
            i = 2;
          case 2:
            i = 1;
          case 1:
            i = 13;
        } 
      }  
    l2 = paramProtoOutputStream.start(1146756268050L);
    double d = paramBatteryStatsHelper.getPowerProfile().getBatteryCapacity();
    paramProtoOutputStream.write(1103806595073L, d);
    paramProtoOutputStream.write(1103806595074L, paramBatteryStatsHelper.getComputedPower());
    paramProtoOutputStream.write(1103806595075L, paramBatteryStatsHelper.getMinDrainedPower());
    paramProtoOutputStream.write(1103806595076L, paramBatteryStatsHelper.getMaxDrainedPower());
    paramProtoOutputStream.end(l2);
    Map<String, ? extends Timer> map2 = getRpmStats();
    Map<String, ? extends Timer> map1 = getScreenOffRpmStats();
    for (null = map2.entrySet().iterator(), i = j; null.hasNext(); ) {
      Map.Entry entry = null.next();
      l2 = paramProtoOutputStream.start(2246267895827L);
      paramProtoOutputStream.write(1138166333441L, (String)entry.getKey());
      Timer timer = (Timer)entry.getValue();
      dumpTimer(paramProtoOutputStream, 1146756268034L, timer, l3, 0);
      timer = map1.get(entry.getKey());
      dumpTimer(paramProtoOutputStream, 1146756268035L, timer, l3, 0);
      paramProtoOutputStream.end(l2);
    } 
    for (i = 0; i < 5; i++) {
      l2 = paramProtoOutputStream.start(2246267895828L);
      paramProtoOutputStream.write(1159641169921L, i);
      dumpTimer(paramProtoOutputStream, 1146756268034L, getScreenBrightnessTimer(i), l3, 0);
      paramProtoOutputStream.end(l2);
    } 
    dumpTimer(paramProtoOutputStream, 1146756268053L, getPhoneSignalScanningTimer(), l3, 0);
    for (i = 0; i < CellSignalStrength.getNumSignalStrengthLevels(); i++) {
      l2 = paramProtoOutputStream.start(2246267895824L);
      paramProtoOutputStream.write(1159641169921L, i);
      dumpTimer(paramProtoOutputStream, 1146756268034L, getPhoneSignalStrengthTimer(i), l3, 0);
      paramProtoOutputStream.end(l2);
    } 
    map1 = getWakeupReasonStats();
    for (Map.Entry<String, ? extends Timer> entry : map1.entrySet()) {
      l2 = paramProtoOutputStream.start(2246267895830L);
      paramProtoOutputStream.write(1138166333441L, (String)entry.getKey());
      dumpTimer(paramProtoOutputStream, 1146756268034L, (Timer)entry.getValue(), l3, 0);
      paramProtoOutputStream.end(l2);
    } 
    for (i = 0; i < 5; i++) {
      l2 = paramProtoOutputStream.start(2246267895832L);
      paramProtoOutputStream.write(1159641169921L, i);
      dumpTimer(paramProtoOutputStream, 1146756268034L, getWifiSignalStrengthTimer(i), l3, 0);
      paramProtoOutputStream.end(l2);
    } 
    for (i = 0; i < 8; i++) {
      l2 = paramProtoOutputStream.start(2246267895833L);
      paramProtoOutputStream.write(1159641169921L, i);
      dumpTimer(paramProtoOutputStream, 1146756268034L, getWifiStateTimer(i), l3, 0);
      paramProtoOutputStream.end(l2);
    } 
    for (i = 0; i < 13; i++) {
      l2 = paramProtoOutputStream.start(2246267895834L);
      paramProtoOutputStream.write(1159641169921L, i);
      dumpTimer(paramProtoOutputStream, 1146756268034L, getWifiSupplStateTimer(i), l3, 0);
      paramProtoOutputStream.end(l2);
    } 
    paramProtoOutputStream.end(l1);
  }
  
  public abstract void commitCurrentHistoryBatchLocked();
  
  public abstract long computeBatteryRealtime(long paramLong, int paramInt);
  
  public abstract long computeBatteryScreenOffRealtime(long paramLong, int paramInt);
  
  public abstract long computeBatteryScreenOffUptime(long paramLong, int paramInt);
  
  public abstract long computeBatteryTimeRemaining(long paramLong);
  
  public abstract long computeBatteryUptime(long paramLong, int paramInt);
  
  public abstract long computeChargeTimeRemaining(long paramLong);
  
  public abstract long computeRealtime(long paramLong, int paramInt);
  
  public abstract long computeUptime(long paramLong, int paramInt);
  
  public abstract void finishIteratingHistoryLocked();
  
  public abstract void finishIteratingOldHistoryLocked();
  
  public abstract long getBatteryRealtime(long paramLong);
  
  public abstract long getBatteryUptime(long paramLong);
  
  public abstract ControllerActivityCounter getBluetoothControllerActivity();
  
  public abstract long getBluetoothScanTime(long paramLong, int paramInt);
  
  public abstract long getCameraOnTime(long paramLong, int paramInt);
  
  public abstract LevelStepTracker getChargeLevelStepTracker();
  
  public abstract long[] getCpuFreqs();
  
  public abstract long getCurrentDailyStartTime();
  
  public abstract LevelStepTracker getDailyChargeLevelStepTracker();
  
  public abstract LevelStepTracker getDailyDischargeLevelStepTracker();
  
  public abstract DailyItem getDailyItemLocked(int paramInt);
  
  public abstract ArrayList<PackageChange> getDailyPackageChanges();
  
  public abstract int getDeviceIdleModeCount(int paramInt1, int paramInt2);
  
  public abstract long getDeviceIdleModeTime(int paramInt1, long paramLong, int paramInt2);
  
  public abstract int getDeviceIdlingCount(int paramInt1, int paramInt2);
  
  public abstract long getDeviceIdlingTime(int paramInt1, long paramLong, int paramInt2);
  
  public abstract int getDischargeAmount(int paramInt);
  
  public abstract int getDischargeAmountScreenDoze();
  
  public abstract int getDischargeAmountScreenDozeSinceCharge();
  
  public abstract int getDischargeAmountScreenOff();
  
  public abstract int getDischargeAmountScreenOffSinceCharge();
  
  public abstract int getDischargeAmountScreenOn();
  
  public abstract int getDischargeAmountScreenOnSinceCharge();
  
  public abstract int getDischargeCurrentLevel();
  
  public abstract LevelStepTracker getDischargeLevelStepTracker();
  
  public abstract int getDischargeStartLevel();
  
  public abstract String getEndPlatformVersion();
  
  public abstract int getEstimatedBatteryCapacity();
  
  public abstract long getFlashlightOnCount(int paramInt);
  
  public abstract long getFlashlightOnTime(long paramLong, int paramInt);
  
  public abstract long getGlobalWifiRunningTime(long paramLong, int paramInt);
  
  public abstract long getGpsBatteryDrainMaMs();
  
  public abstract long getGpsSignalQualityTime(int paramInt1, long paramLong, int paramInt2);
  
  public abstract int getHighDischargeAmountSinceCharge();
  
  public abstract long getHistoryBaseTime();
  
  public abstract int getHistoryStringPoolBytes();
  
  public abstract int getHistoryStringPoolSize();
  
  public abstract String getHistoryTagPoolString(int paramInt);
  
  public abstract int getHistoryTagPoolUid(int paramInt);
  
  public abstract int getHistoryTotalSize();
  
  public abstract int getHistoryUsedSize();
  
  public abstract long getInteractiveTime(long paramLong, int paramInt);
  
  public abstract boolean getIsOnBattery();
  
  public abstract LongSparseArray<? extends Timer> getKernelMemoryStats();
  
  public abstract Map<String, ? extends Timer> getKernelWakelockStats();
  
  public abstract long getLongestDeviceIdleModeTime(int paramInt);
  
  public abstract int getLowDischargeAmountSinceCharge();
  
  public abstract int getMaxLearnedBatteryCapacity();
  
  public abstract int getMinLearnedBatteryCapacity();
  
  public abstract long getMobileRadioActiveAdjustedTime(int paramInt);
  
  public abstract int getMobileRadioActiveCount(int paramInt);
  
  public abstract long getMobileRadioActiveTime(long paramLong, int paramInt);
  
  public abstract int getMobileRadioActiveUnknownCount(int paramInt);
  
  public abstract long getMobileRadioActiveUnknownTime(int paramInt);
  
  public abstract ControllerActivityCounter getModemControllerActivity();
  
  public abstract long getNetworkActivityBytes(int paramInt1, int paramInt2);
  
  public abstract long getNetworkActivityPackets(int paramInt1, int paramInt2);
  
  public abstract boolean getNextHistoryLocked(HistoryItem paramHistoryItem);
  
  public abstract long getNextMaxDailyDeadline();
  
  public abstract long getNextMinDailyDeadline();
  
  public abstract boolean getNextOldHistoryLocked(HistoryItem paramHistoryItem);
  
  public abstract int getNumConnectivityChange(int paramInt);
  
  public abstract int getParcelVersion();
  
  public abstract int getPhoneDataConnectionCount(int paramInt1, int paramInt2);
  
  public abstract long getPhoneDataConnectionTime(int paramInt1, long paramLong, int paramInt2);
  
  public abstract Timer getPhoneDataConnectionTimer(int paramInt);
  
  public abstract int getPhoneOnCount(int paramInt);
  
  public abstract long getPhoneOnTime(long paramLong, int paramInt);
  
  public abstract long getPhoneSignalScanningTime(long paramLong, int paramInt);
  
  public abstract Timer getPhoneSignalScanningTimer();
  
  public abstract int getPhoneSignalStrengthCount(int paramInt1, int paramInt2);
  
  public abstract long getPhoneSignalStrengthTime(int paramInt1, long paramLong, int paramInt2);
  
  protected abstract Timer getPhoneSignalStrengthTimer(int paramInt);
  
  public abstract int getPowerSaveModeEnabledCount(int paramInt);
  
  public abstract long getPowerSaveModeEnabledTime(long paramLong, int paramInt);
  
  public abstract Map<String, ? extends Timer> getRpmStats();
  
  public abstract long getScreenBrightnessTime(int paramInt1, long paramLong, int paramInt2);
  
  public abstract Timer getScreenBrightnessTimer(int paramInt);
  
  public abstract int getScreenDozeCount(int paramInt);
  
  public abstract long getScreenDozeTime(long paramLong, int paramInt);
  
  public abstract Map<String, ? extends Timer> getScreenOffRpmStats();
  
  public abstract int getScreenOnCount(int paramInt);
  
  public abstract long getScreenOnTime(long paramLong, int paramInt);
  
  public abstract long getStartClockTime();
  
  public abstract int getStartCount();
  
  public abstract String getStartPlatformVersion();
  
  public abstract long getUahDischarge(int paramInt);
  
  public abstract long getUahDischargeDeepDoze(int paramInt);
  
  public abstract long getUahDischargeLightDoze(int paramInt);
  
  public abstract long getUahDischargeScreenDoze(int paramInt);
  
  public abstract long getUahDischargeScreenOff(int paramInt);
  
  public abstract SparseArray<? extends Uid> getUidStats();
  
  public abstract Map<String, ? extends Timer> getWakeupReasonStats();
  
  public abstract long getWifiActiveTime(long paramLong, int paramInt);
  
  public abstract ControllerActivityCounter getWifiControllerActivity();
  
  public abstract int getWifiMulticastWakelockCount(int paramInt);
  
  public abstract long getWifiMulticastWakelockTime(long paramLong, int paramInt);
  
  public abstract long getWifiOnTime(long paramLong, int paramInt);
  
  public abstract int getWifiSignalStrengthCount(int paramInt1, int paramInt2);
  
  public abstract long getWifiSignalStrengthTime(int paramInt1, long paramLong, int paramInt2);
  
  public abstract Timer getWifiSignalStrengthTimer(int paramInt);
  
  public abstract int getWifiStateCount(int paramInt1, int paramInt2);
  
  public abstract long getWifiStateTime(int paramInt1, long paramLong, int paramInt2);
  
  public abstract Timer getWifiStateTimer(int paramInt);
  
  public abstract int getWifiSupplStateCount(int paramInt1, int paramInt2);
  
  public abstract long getWifiSupplStateTime(int paramInt1, long paramLong, int paramInt2);
  
  public abstract Timer getWifiSupplStateTimer(int paramInt);
  
  public abstract boolean hasBluetoothActivityReporting();
  
  public abstract boolean hasModemActivityReporting();
  
  public abstract boolean hasWifiActivityReporting();
  
  public abstract boolean startIteratingHistoryLocked();
  
  public abstract boolean startIteratingOldHistoryLocked();
  
  public abstract void writeToParcelWithoutUids(Parcel paramParcel, int paramInt);
  
  @FunctionalInterface
  class IntToString {
    public abstract String applyAsString(int param1Int);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class StatName implements Annotation {}
}
