package android.os;

import android.system.ErrnoException;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class MemoryFile {
  private static String TAG = "MemoryFile";
  
  private boolean mAllowPurging = false;
  
  private ByteBuffer mMapping;
  
  private SharedMemory mSharedMemory;
  
  public MemoryFile(String paramString, int paramInt) throws IOException {
    try {
      SharedMemory sharedMemory = SharedMemory.create(paramString, paramInt);
      this.mMapping = sharedMemory.mapReadWrite();
    } catch (ErrnoException errnoException) {
      errnoException.rethrowAsIOException();
    } 
  }
  
  public void close() {
    deactivate();
    this.mSharedMemory.close();
  }
  
  void deactivate() {
    ByteBuffer byteBuffer = this.mMapping;
    if (byteBuffer != null) {
      SharedMemory.unmap(byteBuffer);
      this.mMapping = null;
    } 
  }
  
  private void checkActive() throws IOException {
    if (this.mMapping != null)
      return; 
    throw new IOException("MemoryFile has been deactivated");
  }
  
  private void beginAccess() throws IOException {
    checkActive();
    if (!this.mAllowPurging || 
      !native_pin(this.mSharedMemory.getFileDescriptor(), true))
      return; 
    throw new IOException("MemoryFile has been purged");
  }
  
  private void endAccess() throws IOException {
    if (this.mAllowPurging)
      native_pin(this.mSharedMemory.getFileDescriptor(), false); 
  }
  
  public int length() {
    return this.mSharedMemory.getSize();
  }
  
  @Deprecated
  public boolean isPurgingAllowed() {
    return this.mAllowPurging;
  }
  
  @Deprecated
  public boolean allowPurging(boolean paramBoolean) throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mAllowPurging : Z
    //   6: istore_2
    //   7: iload_2
    //   8: iload_1
    //   9: if_icmpeq -> 45
    //   12: aload_0
    //   13: getfield mSharedMemory : Landroid/os/SharedMemory;
    //   16: invokevirtual getFileDescriptor : ()Ljava/io/FileDescriptor;
    //   19: astore_3
    //   20: iload_1
    //   21: ifne -> 30
    //   24: iconst_1
    //   25: istore #4
    //   27: goto -> 33
    //   30: iconst_0
    //   31: istore #4
    //   33: aload_3
    //   34: iload #4
    //   36: invokestatic native_pin : (Ljava/io/FileDescriptor;Z)Z
    //   39: pop
    //   40: aload_0
    //   41: iload_1
    //   42: putfield mAllowPurging : Z
    //   45: aload_0
    //   46: monitorexit
    //   47: iload_2
    //   48: ireturn
    //   49: astore_3
    //   50: aload_0
    //   51: monitorexit
    //   52: aload_3
    //   53: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #150	-> 2
    //   #151	-> 7
    //   #152	-> 12
    //   #153	-> 40
    //   #155	-> 45
    //   #149	-> 49
    // Exception table:
    //   from	to	target	type
    //   2	7	49	finally
    //   12	20	49	finally
    //   33	40	49	finally
    //   40	45	49	finally
  }
  
  public InputStream getInputStream() {
    return new MemoryInputStream();
  }
  
  public OutputStream getOutputStream() {
    return new MemoryOutputStream();
  }
  
  public int readBytes(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) throws IOException {
    beginAccess();
    try {
      this.mMapping.position(paramInt1);
      this.mMapping.get(paramArrayOfbyte, paramInt2, paramInt3);
      return paramInt3;
    } finally {
      endAccess();
    } 
  }
  
  public void writeBytes(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) throws IOException {
    beginAccess();
    try {
      this.mMapping.position(paramInt2);
      this.mMapping.put(paramArrayOfbyte, paramInt1, paramInt3);
      return;
    } finally {
      endAccess();
    } 
  }
  
  public FileDescriptor getFileDescriptor() throws IOException {
    return this.mSharedMemory.getFileDescriptor();
  }
  
  public static int getSize(FileDescriptor paramFileDescriptor) throws IOException {
    return native_get_size(paramFileDescriptor);
  }
  
  private static native int native_get_size(FileDescriptor paramFileDescriptor) throws IOException;
  
  private static native boolean native_pin(FileDescriptor paramFileDescriptor, boolean paramBoolean) throws IOException;
  
  private class MemoryInputStream extends InputStream {
    private int mMark = 0;
    
    private int mOffset = 0;
    
    private byte[] mSingleByte;
    
    final MemoryFile this$0;
    
    public int available() throws IOException {
      if (this.mOffset >= MemoryFile.this.mSharedMemory.getSize())
        return 0; 
      return MemoryFile.this.mSharedMemory.getSize() - this.mOffset;
    }
    
    public boolean markSupported() {
      return true;
    }
    
    public void mark(int param1Int) {
      this.mMark = this.mOffset;
    }
    
    public void reset() throws IOException {
      this.mOffset = this.mMark;
    }
    
    public int read() throws IOException {
      if (this.mSingleByte == null)
        this.mSingleByte = new byte[1]; 
      int i = read(this.mSingleByte, 0, 1);
      if (i != 1)
        return -1; 
      return this.mSingleByte[0];
    }
    
    public int read(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      if (param1Int1 >= 0 && param1Int2 >= 0 && param1Int1 + param1Int2 <= param1ArrayOfbyte.length) {
        param1Int2 = Math.min(param1Int2, available());
        if (param1Int2 < 1)
          return -1; 
        param1Int1 = MemoryFile.this.readBytes(param1ArrayOfbyte, this.mOffset, param1Int1, param1Int2);
        if (param1Int1 > 0)
          this.mOffset += param1Int1; 
        return param1Int1;
      } 
      throw new IndexOutOfBoundsException();
    }
    
    public long skip(long param1Long) throws IOException {
      long l = param1Long;
      if (this.mOffset + param1Long > MemoryFile.this.mSharedMemory.getSize())
        l = (MemoryFile.this.mSharedMemory.getSize() - this.mOffset); 
      this.mOffset = (int)(this.mOffset + l);
      return l;
    }
    
    private MemoryInputStream() {}
  }
  
  private class MemoryOutputStream extends OutputStream {
    private int mOffset;
    
    private byte[] mSingleByte;
    
    final MemoryFile this$0;
    
    private MemoryOutputStream() {
      this.mOffset = 0;
    }
    
    public void write(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws IOException {
      MemoryFile.this.writeBytes(param1ArrayOfbyte, param1Int1, this.mOffset, param1Int2);
      this.mOffset += param1Int2;
    }
    
    public void write(int param1Int) throws IOException {
      if (this.mSingleByte == null)
        this.mSingleByte = new byte[1]; 
      byte[] arrayOfByte = this.mSingleByte;
      arrayOfByte[0] = (byte)param1Int;
      write(arrayOfByte, 0, 1);
    }
  }
}
