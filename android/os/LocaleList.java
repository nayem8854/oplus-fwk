package android.os;

import android.icu.util.ULocale;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

public final class LocaleList implements Parcelable {
  public static final Parcelable.Creator<LocaleList> CREATOR;
  
  private static final Locale EN_LATN;
  
  private static final Locale LOCALE_AR_XB;
  
  private static final Locale LOCALE_EN_XA;
  
  private static final int NUM_PSEUDO_LOCALES = 2;
  
  private static final String STRING_AR_XB = "ar-XB";
  
  private static final String STRING_EN_XA = "en-XA";
  
  private static LocaleList sDefaultAdjustedLocaleList;
  
  private static LocaleList sDefaultLocaleList;
  
  private static final Locale[] sEmptyList = new Locale[0];
  
  private static final LocaleList sEmptyLocaleList = new LocaleList(new Locale[0]);
  
  private static Locale sLastDefaultLocale;
  
  private static LocaleList sLastExplicitlySetLocaleList;
  
  private static final Object sLock;
  
  private final Locale[] mList;
  
  private final String mStringRepresentation;
  
  public Locale get(int paramInt) {
    if (paramInt >= 0) {
      Locale[] arrayOfLocale = this.mList;
      if (paramInt < arrayOfLocale.length)
        return arrayOfLocale[paramInt]; 
    } 
    return null;
  }
  
  public boolean isEmpty() {
    boolean bool;
    if (this.mList.length == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int size() {
    return this.mList.length;
  }
  
  public int indexOf(Locale paramLocale) {
    byte b = 0;
    while (true) {
      Locale[] arrayOfLocale = this.mList;
      if (b < arrayOfLocale.length) {
        if (arrayOfLocale[b].equals(paramLocale))
          return b; 
        b++;
        continue;
      } 
      break;
    } 
    return -1;
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == this)
      return true; 
    if (!(paramObject instanceof LocaleList))
      return false; 
    Locale[] arrayOfLocale = ((LocaleList)paramObject).mList;
    if (this.mList.length != arrayOfLocale.length)
      return false; 
    byte b = 0;
    while (true) {
      paramObject = this.mList;
      if (b < paramObject.length) {
        if (!paramObject[b].equals(arrayOfLocale[b]))
          return false; 
        b++;
        continue;
      } 
      break;
    } 
    return true;
  }
  
  public int hashCode() {
    int i = 1;
    byte b = 0;
    while (true) {
      Locale[] arrayOfLocale = this.mList;
      if (b < arrayOfLocale.length) {
        i = i * 31 + arrayOfLocale[b].hashCode();
        b++;
        continue;
      } 
      break;
    } 
    return i;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    byte b = 0;
    while (true) {
      Locale[] arrayOfLocale = this.mList;
      if (b < arrayOfLocale.length) {
        stringBuilder.append(arrayOfLocale[b]);
        if (b < this.mList.length - 1)
          stringBuilder.append(','); 
        b++;
        continue;
      } 
      break;
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString8(this.mStringRepresentation);
  }
  
  public String toLanguageTags() {
    return this.mStringRepresentation;
  }
  
  public LocaleList(Locale... paramVarArgs) {
    if (paramVarArgs.length == 0) {
      this.mList = sEmptyList;
      this.mStringRepresentation = "";
    } else {
      ArrayList<Locale> arrayList = new ArrayList();
      HashSet<Locale> hashSet = new HashSet();
      StringBuilder stringBuilder = new StringBuilder();
      for (byte b = 0; b < paramVarArgs.length; ) {
        Locale locale = paramVarArgs[b];
        if (locale != null) {
          if (!hashSet.contains(locale)) {
            locale = (Locale)locale.clone();
            arrayList.add(locale);
            stringBuilder.append(locale.toLanguageTag());
            if (b < paramVarArgs.length - 1)
              stringBuilder.append(','); 
            hashSet.add(locale);
          } 
          b++;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("list[");
        stringBuilder1.append(b);
        stringBuilder1.append("] is null");
        throw new NullPointerException(stringBuilder1.toString());
      } 
      this.mList = arrayList.<Locale>toArray(new Locale[arrayList.size()]);
      this.mStringRepresentation = stringBuilder.toString();
    } 
  }
  
  public LocaleList(Locale paramLocale, LocaleList paramLocaleList) {
    if (paramLocale != null) {
      int i, m;
      if (paramLocaleList == null) {
        i = 0;
      } else {
        i = paramLocaleList.mList.length;
      } 
      int j = -1;
      int k = 0;
      while (true) {
        m = j;
        if (k < i) {
          if (paramLocale.equals(paramLocaleList.mList[k])) {
            m = k;
            break;
          } 
          k++;
          continue;
        } 
        break;
      } 
      if (m == -1) {
        k = 1;
      } else {
        k = 0;
      } 
      j = k + i;
      Locale[] arrayOfLocale = new Locale[j];
      arrayOfLocale[0] = (Locale)paramLocale.clone();
      if (m == -1) {
        for (k = 0; k < i; k++)
          arrayOfLocale[k + 1] = (Locale)paramLocaleList.mList[k].clone(); 
      } else {
        for (k = 0; k < m; k++)
          arrayOfLocale[k + 1] = (Locale)paramLocaleList.mList[k].clone(); 
        for (k = m + 1; k < i; k++)
          arrayOfLocale[k] = (Locale)paramLocaleList.mList[k].clone(); 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      for (k = 0; k < j; k++) {
        stringBuilder.append(arrayOfLocale[k].toLanguageTag());
        if (k < j - 1)
          stringBuilder.append(','); 
      } 
      this.mList = arrayOfLocale;
      this.mStringRepresentation = stringBuilder.toString();
      return;
    } 
    throw new NullPointerException("topLocale is null");
  }
  
  static {
    CREATOR = new Parcelable.Creator<LocaleList>() {
        public LocaleList createFromParcel(Parcel param1Parcel) {
          return LocaleList.forLanguageTags(param1Parcel.readString8());
        }
        
        public LocaleList[] newArray(int param1Int) {
          return new LocaleList[param1Int];
        }
      };
    LOCALE_EN_XA = new Locale("en", "XA");
    LOCALE_AR_XB = new Locale("ar", "XB");
    EN_LATN = Locale.forLanguageTag("en-Latn");
    sLock = new Object();
    sLastExplicitlySetLocaleList = null;
    sDefaultLocaleList = null;
    sDefaultAdjustedLocaleList = null;
    sLastDefaultLocale = null;
  }
  
  public static LocaleList getEmptyLocaleList() {
    return sEmptyLocaleList;
  }
  
  public static LocaleList forLanguageTags(String paramString) {
    if (paramString == null || paramString.equals(""))
      return getEmptyLocaleList(); 
    String[] arrayOfString = paramString.split(",");
    Locale[] arrayOfLocale = new Locale[arrayOfString.length];
    for (byte b = 0; b < arrayOfLocale.length; b++)
      arrayOfLocale[b] = Locale.forLanguageTag(arrayOfString[b]); 
    return new LocaleList(arrayOfLocale);
  }
  
  private static String getLikelyScript(Locale paramLocale) {
    String str = paramLocale.getScript();
    if (!str.isEmpty())
      return str; 
    return ULocale.addLikelySubtags(ULocale.forLocale(paramLocale)).getScript();
  }
  
  private static boolean isPseudoLocale(String paramString) {
    return ("en-XA".equals(paramString) || "ar-XB".equals(paramString));
  }
  
  public static boolean isPseudoLocale(Locale paramLocale) {
    return (LOCALE_EN_XA.equals(paramLocale) || LOCALE_AR_XB.equals(paramLocale));
  }
  
  public static boolean isPseudoLocale(ULocale paramULocale) {
    if (paramULocale != null) {
      Locale locale = paramULocale.toLocale();
    } else {
      paramULocale = null;
    } 
    return isPseudoLocale((Locale)paramULocale);
  }
  
  private static int matchScore(Locale paramLocale1, Locale paramLocale2) {
    boolean bool = paramLocale1.equals(paramLocale2);
    boolean bool1 = true;
    if (bool)
      return 1; 
    if (!paramLocale1.getLanguage().equals(paramLocale2.getLanguage()))
      return 0; 
    if (isPseudoLocale(paramLocale1) || isPseudoLocale(paramLocale2))
      return 0; 
    String str2 = getLikelyScript(paramLocale1);
    if (str2.isEmpty()) {
      String str = paramLocale1.getCountry();
      if (!str.isEmpty() && !str.equals(paramLocale2.getCountry()))
        bool1 = false; 
      return bool1;
    } 
    String str1 = getLikelyScript(paramLocale2);
    return str2.equals(str1);
  }
  
  private int findFirstMatchIndex(Locale paramLocale) {
    byte b = 0;
    while (true) {
      Locale[] arrayOfLocale = this.mList;
      if (b < arrayOfLocale.length) {
        int i = matchScore(paramLocale, arrayOfLocale[b]);
        if (i > 0)
          return b; 
        b++;
        continue;
      } 
      break;
    } 
    return Integer.MAX_VALUE;
  }
  
  private int computeFirstMatchIndex(Collection<String> paramCollection, boolean paramBoolean) {
    Locale[] arrayOfLocale = this.mList;
    if (arrayOfLocale.length == 1)
      return 0; 
    if (arrayOfLocale.length == 0)
      return -1; 
    int i = Integer.MAX_VALUE;
    int j = i;
    if (paramBoolean) {
      int m = findFirstMatchIndex(EN_LATN);
      if (m == 0)
        return 0; 
      j = i;
      if (m < Integer.MAX_VALUE)
        j = m; 
    } 
    int k;
    for (Iterator<String> iterator = paramCollection.iterator(); iterator.hasNext(); ) {
      String str = iterator.next();
      Locale locale = Locale.forLanguageTag(str);
      i = findFirstMatchIndex(locale);
      if (i == 0)
        return 0; 
      j = k;
      if (i < k)
        j = i; 
      k = j;
    } 
    if (k == Integer.MAX_VALUE)
      return 0; 
    return k;
  }
  
  private Locale computeFirstMatch(Collection<String> paramCollection, boolean paramBoolean) {
    Locale locale;
    int i = computeFirstMatchIndex(paramCollection, paramBoolean);
    if (i == -1) {
      paramCollection = null;
    } else {
      locale = this.mList[i];
    } 
    return locale;
  }
  
  public Locale getFirstMatch(String[] paramArrayOfString) {
    return computeFirstMatch(Arrays.asList(paramArrayOfString), false);
  }
  
  public int getFirstMatchIndex(String[] paramArrayOfString) {
    return computeFirstMatchIndex(Arrays.asList(paramArrayOfString), false);
  }
  
  public Locale getFirstMatchWithEnglishSupported(String[] paramArrayOfString) {
    return computeFirstMatch(Arrays.asList(paramArrayOfString), true);
  }
  
  public int getFirstMatchIndexWithEnglishSupported(Collection<String> paramCollection) {
    return computeFirstMatchIndex(paramCollection, true);
  }
  
  public int getFirstMatchIndexWithEnglishSupported(String[] paramArrayOfString) {
    return getFirstMatchIndexWithEnglishSupported(Arrays.asList(paramArrayOfString));
  }
  
  public static boolean isPseudoLocalesOnly(String[] paramArrayOfString) {
    if (paramArrayOfString == null)
      return true; 
    if (paramArrayOfString.length > 3)
      return false; 
    int i;
    byte b;
    for (i = paramArrayOfString.length, b = 0; b < i; ) {
      String str = paramArrayOfString[b];
      if (!str.isEmpty() && !isPseudoLocale(str))
        return false; 
      b++;
    } 
    return true;
  }
  
  public static LocaleList getDefault() {
    null = Locale.getDefault();
    synchronized (sLock) {
      if (!null.equals(sLastDefaultLocale)) {
        LocaleList localeList1;
        sLastDefaultLocale = null;
        if (sDefaultLocaleList != null) {
          LocaleList localeList = sDefaultLocaleList;
          if (null.equals(localeList.get(0))) {
            localeList1 = sDefaultLocaleList;
            return localeList1;
          } 
        } 
        LocaleList localeList2 = new LocaleList();
        this((Locale)localeList1, sLastExplicitlySetLocaleList);
        sDefaultLocaleList = localeList2;
        sDefaultAdjustedLocaleList = localeList2;
      } 
      return sDefaultLocaleList;
    } 
  }
  
  public static LocaleList getAdjustedDefault() {
    getDefault();
    synchronized (sLock) {
      return sDefaultAdjustedLocaleList;
    } 
  }
  
  public static void setDefault(LocaleList paramLocaleList) {
    setDefault(paramLocaleList, 0);
  }
  
  public static void setDefault(LocaleList paramLocaleList, int paramInt) {
    if (paramLocaleList != null) {
      if (!paramLocaleList.isEmpty())
        synchronized (sLock) {
          Locale locale = paramLocaleList.get(paramInt);
          Locale.setDefault(locale);
          sLastExplicitlySetLocaleList = paramLocaleList;
          sDefaultLocaleList = paramLocaleList;
          if (paramInt == 0) {
            sDefaultAdjustedLocaleList = paramLocaleList;
          } else {
            paramLocaleList = new LocaleList();
            this(sLastDefaultLocale, sDefaultLocaleList);
            sDefaultAdjustedLocaleList = paramLocaleList;
          } 
          return;
        }  
      throw new IllegalArgumentException("locales is empty");
    } 
    throw new NullPointerException("locales is null");
  }
}
