package android.os;

import android.util.ArrayMap;
import android.util.Size;
import android.util.SizeF;
import android.util.SparseArray;
import android.util.proto.ProtoOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class Bundle extends BaseBundle implements Cloneable, Parcelable {
  public static final Parcelable.Creator<Bundle> CREATOR;
  
  public static final Bundle EMPTY;
  
  static final int FLAG_ALLOW_FDS = 1024;
  
  static final int FLAG_HAS_FDS = 256;
  
  static final int FLAG_HAS_FDS_KNOWN = 512;
  
  public static final Bundle STRIPPED;
  
  static {
    Bundle bundle = new Bundle();
    bundle.mMap = ArrayMap.EMPTY;
    STRIPPED = bundle = new Bundle();
    bundle.putInt("STRIPPED", 1);
    CREATOR = new Parcelable.Creator<Bundle>() {
        public Bundle createFromParcel(Parcel param1Parcel) {
          return param1Parcel.readBundle();
        }
        
        public Bundle[] newArray(int param1Int) {
          return new Bundle[param1Int];
        }
      };
  }
  
  public Bundle() {
    this.mFlags = 1536;
  }
  
  public Bundle(Parcel paramParcel) {
    super(paramParcel);
    this.mFlags = 1024;
    maybePrefillHasFds();
  }
  
  public Bundle(Parcel paramParcel, int paramInt) {
    super(paramParcel, paramInt);
    this.mFlags = 1024;
    maybePrefillHasFds();
  }
  
  private void maybePrefillHasFds() {
    if (this.mParcelledData != null)
      if (this.mParcelledData.hasFileDescriptors()) {
        this.mFlags |= 0x300;
      } else {
        this.mFlags |= 0x200;
      }  
  }
  
  public Bundle(ClassLoader paramClassLoader) {
    super(paramClassLoader);
    this.mFlags = 1536;
  }
  
  public Bundle(int paramInt) {
    super(paramInt);
    this.mFlags = 1536;
  }
  
  public Bundle(Bundle paramBundle) {
    super(paramBundle);
    this.mFlags = paramBundle.mFlags;
  }
  
  public Bundle(PersistableBundle paramPersistableBundle) {
    super(paramPersistableBundle);
    this.mFlags = 1536;
  }
  
  Bundle(boolean paramBoolean) {
    super(paramBoolean);
  }
  
  public static Bundle forPair(String paramString1, String paramString2) {
    Bundle bundle = new Bundle(1);
    bundle.putString(paramString1, paramString2);
    return bundle;
  }
  
  public void setClassLoader(ClassLoader paramClassLoader) {
    super.setClassLoader(paramClassLoader);
  }
  
  public ClassLoader getClassLoader() {
    return super.getClassLoader();
  }
  
  public boolean setAllowFds(boolean paramBoolean) {
    boolean bool;
    if ((this.mFlags & 0x400) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramBoolean) {
      this.mFlags |= 0x400;
    } else {
      this.mFlags &= 0xFFFFFBFF;
    } 
    return bool;
  }
  
  public void setDefusable(boolean paramBoolean) {
    if (paramBoolean) {
      this.mFlags |= 0x1;
    } else {
      this.mFlags &= 0xFFFFFFFE;
    } 
  }
  
  public static Bundle setDefusable(Bundle paramBundle, boolean paramBoolean) {
    if (paramBundle != null)
      paramBundle.setDefusable(paramBoolean); 
    return paramBundle;
  }
  
  public Object clone() {
    return new Bundle(this);
  }
  
  public Bundle deepCopy() {
    Bundle bundle = new Bundle(false);
    bundle.copyInternal(this, true);
    return bundle;
  }
  
  public void clear() {
    super.clear();
    this.mFlags = 1536;
  }
  
  public void remove(String paramString) {
    super.remove(paramString);
    if ((this.mFlags & 0x100) != 0)
      this.mFlags &= 0xFFFFFDFF; 
  }
  
  public void putAll(Bundle paramBundle) {
    unparcel();
    paramBundle.unparcel();
    this.mMap.putAll(paramBundle.mMap);
    if ((paramBundle.mFlags & 0x100) != 0)
      this.mFlags |= 0x100; 
    if ((paramBundle.mFlags & 0x200) == 0)
      this.mFlags &= 0xFFFFFDFF; 
  }
  
  public int getSize() {
    if (this.mParcelledData != null)
      return this.mParcelledData.dataSize(); 
    return 0;
  }
  
  public boolean hasFileDescriptors() {
    int i = this.mFlags;
    boolean bool = false;
    if ((i & 0x200) == 0) {
      int j = 0;
      i = 0;
      if (this.mParcelledData != null) {
        if (this.mParcelledData.hasFileDescriptors())
          j = 1; 
      } else {
        int k = this.mMap.size() - 1;
        while (true) {
          j = i;
          if (k >= 0) {
            Object object = this.mMap.valueAt(k);
            if (object instanceof Parcelable) {
              j = i;
              if ((((Parcelable)object).describeContents() & 0x1) != 0) {
                j = 1;
                break;
              } 
            } else if (object instanceof Parcelable[]) {
              Parcelable[] arrayOfParcelable = (Parcelable[])object;
              int m = arrayOfParcelable.length - 1;
              while (true) {
                j = i;
                if (m >= 0) {
                  object = arrayOfParcelable[m];
                  if (object != null && (object.describeContents() & 0x1) != 0) {
                    j = 1;
                    break;
                  } 
                  m--;
                  continue;
                } 
                break;
              } 
            } else if (object instanceof SparseArray) {
              object = object;
              int m = object.size() - 1;
              while (true) {
                j = i;
                if (m >= 0) {
                  Parcelable parcelable = (Parcelable)object.valueAt(m);
                  if (parcelable != null && (parcelable.describeContents() & 0x1) != 0) {
                    j = 1;
                    break;
                  } 
                  m--;
                  continue;
                } 
                break;
              } 
            } else {
              j = i;
              if (object instanceof ArrayList) {
                object = object;
                j = i;
                if (!object.isEmpty()) {
                  j = i;
                  if (object.get(0) instanceof Parcelable) {
                    int m = object.size() - 1;
                    while (true) {
                      j = i;
                      if (m >= 0) {
                        Parcelable parcelable = object.get(m);
                        if (parcelable != null && (parcelable.describeContents() & 0x1) != 0) {
                          j = 1;
                          break;
                        } 
                        m--;
                        continue;
                      } 
                      break;
                    } 
                  } 
                } 
              } 
            } 
            k--;
            i = j;
            continue;
          } 
          break;
        } 
      } 
      if (j != 0) {
        this.mFlags |= 0x100;
      } else {
        this.mFlags &= 0xFFFFFEFF;
      } 
      this.mFlags |= 0x200;
    } 
    if ((this.mFlags & 0x100) != 0)
      bool = true; 
    return bool;
  }
  
  public Bundle filterValues() {
    unparcel();
    Bundle bundle1 = this;
    Bundle bundle2 = bundle1;
    if (this.mMap != null) {
      ArrayMap<String, Object> arrayMap = this.mMap;
      int i = arrayMap.size() - 1;
      while (true) {
        bundle2 = bundle1;
        if (i >= 0) {
          ArrayMap<String, Object> arrayMap1;
          Object object = arrayMap.valueAt(i);
          if (PersistableBundle.isValidType(object)) {
            bundle2 = bundle1;
            arrayMap1 = arrayMap;
          } else if (object instanceof Bundle) {
            Bundle bundle = ((Bundle)object).filterValues();
            bundle2 = bundle1;
            arrayMap1 = arrayMap;
            if (bundle != object) {
              arrayMap1 = arrayMap;
              if (arrayMap == this.mMap) {
                bundle1 = new Bundle(this);
                arrayMap1 = bundle1.mMap;
              } 
              arrayMap1.setValueAt(i, bundle);
              bundle2 = bundle1;
            } 
          } else if (object.getClass().getName().startsWith("android.")) {
            bundle2 = bundle1;
            arrayMap1 = arrayMap;
          } else {
            arrayMap1 = arrayMap;
            if (arrayMap == this.mMap) {
              bundle1 = new Bundle(this);
              arrayMap1 = bundle1.mMap;
            } 
            arrayMap1.removeAt(i);
            bundle2 = bundle1;
          } 
          i--;
          bundle1 = bundle2;
          arrayMap = arrayMap1;
          continue;
        } 
        break;
      } 
    } 
    this.mFlags |= 0x200;
    this.mFlags &= 0xFFFFFEFF;
    return bundle2;
  }
  
  public void putObject(String paramString, Object paramObject) {
    if (paramObject instanceof Byte) {
      putByte(paramString, ((Byte)paramObject).byteValue());
    } else if (paramObject instanceof Character) {
      putChar(paramString, ((Character)paramObject).charValue());
    } else if (paramObject instanceof Short) {
      putShort(paramString, ((Short)paramObject).shortValue());
    } else if (paramObject instanceof Float) {
      putFloat(paramString, ((Float)paramObject).floatValue());
    } else if (paramObject instanceof CharSequence) {
      putCharSequence(paramString, (CharSequence)paramObject);
    } else if (paramObject instanceof Parcelable) {
      putParcelable(paramString, (Parcelable)paramObject);
    } else if (paramObject instanceof Size) {
      putSize(paramString, (Size)paramObject);
    } else if (paramObject instanceof SizeF) {
      putSizeF(paramString, (SizeF)paramObject);
    } else if (paramObject instanceof Parcelable[]) {
      putParcelableArray(paramString, (Parcelable[])paramObject);
    } else if (paramObject instanceof ArrayList) {
      putParcelableArrayList(paramString, (ArrayList<? extends Parcelable>)paramObject);
    } else if (paramObject instanceof List) {
      putParcelableList(paramString, (List<? extends Parcelable>)paramObject);
    } else if (paramObject instanceof SparseArray) {
      putSparseParcelableArray(paramString, (SparseArray<? extends Parcelable>)paramObject);
    } else if (paramObject instanceof Serializable) {
      putSerializable(paramString, (Serializable)paramObject);
    } else if (paramObject instanceof byte[]) {
      putByteArray(paramString, (byte[])paramObject);
    } else if (paramObject instanceof short[]) {
      putShortArray(paramString, (short[])paramObject);
    } else if (paramObject instanceof char[]) {
      putCharArray(paramString, (char[])paramObject);
    } else if (paramObject instanceof float[]) {
      putFloatArray(paramString, (float[])paramObject);
    } else if (paramObject instanceof CharSequence[]) {
      putCharSequenceArray(paramString, (CharSequence[])paramObject);
    } else if (paramObject instanceof Bundle) {
      putBundle(paramString, (Bundle)paramObject);
    } else if (paramObject instanceof Binder) {
      putBinder(paramString, (Binder)paramObject);
    } else if (paramObject instanceof IBinder) {
      putIBinder(paramString, (IBinder)paramObject);
    } else {
      super.putObject(paramString, paramObject);
    } 
  }
  
  public void putByte(String paramString, byte paramByte) {
    super.putByte(paramString, paramByte);
  }
  
  public void putChar(String paramString, char paramChar) {
    super.putChar(paramString, paramChar);
  }
  
  public void putShort(String paramString, short paramShort) {
    super.putShort(paramString, paramShort);
  }
  
  public void putFloat(String paramString, float paramFloat) {
    super.putFloat(paramString, paramFloat);
  }
  
  public void putCharSequence(String paramString, CharSequence paramCharSequence) {
    super.putCharSequence(paramString, paramCharSequence);
  }
  
  public void putParcelable(String paramString, Parcelable paramParcelable) {
    unparcel();
    this.mMap.put(paramString, paramParcelable);
    this.mFlags &= 0xFFFFFDFF;
  }
  
  public void putSize(String paramString, Size paramSize) {
    unparcel();
    this.mMap.put(paramString, paramSize);
  }
  
  public void putSizeF(String paramString, SizeF paramSizeF) {
    unparcel();
    this.mMap.put(paramString, paramSizeF);
  }
  
  public void putParcelableArray(String paramString, Parcelable[] paramArrayOfParcelable) {
    unparcel();
    this.mMap.put(paramString, paramArrayOfParcelable);
    this.mFlags &= 0xFFFFFDFF;
  }
  
  public void putParcelableArrayList(String paramString, ArrayList<? extends Parcelable> paramArrayList) {
    unparcel();
    this.mMap.put(paramString, paramArrayList);
    this.mFlags &= 0xFFFFFDFF;
  }
  
  public void putParcelableList(String paramString, List<? extends Parcelable> paramList) {
    unparcel();
    this.mMap.put(paramString, paramList);
    this.mFlags &= 0xFFFFFDFF;
  }
  
  public void putSparseParcelableArray(String paramString, SparseArray<? extends Parcelable> paramSparseArray) {
    unparcel();
    this.mMap.put(paramString, paramSparseArray);
    this.mFlags &= 0xFFFFFDFF;
  }
  
  public void putIntegerArrayList(String paramString, ArrayList<Integer> paramArrayList) {
    super.putIntegerArrayList(paramString, paramArrayList);
  }
  
  public void putStringArrayList(String paramString, ArrayList<String> paramArrayList) {
    super.putStringArrayList(paramString, paramArrayList);
  }
  
  public void putCharSequenceArrayList(String paramString, ArrayList<CharSequence> paramArrayList) {
    super.putCharSequenceArrayList(paramString, paramArrayList);
  }
  
  public void putSerializable(String paramString, Serializable paramSerializable) {
    super.putSerializable(paramString, paramSerializable);
  }
  
  public void putByteArray(String paramString, byte[] paramArrayOfbyte) {
    super.putByteArray(paramString, paramArrayOfbyte);
  }
  
  public void putShortArray(String paramString, short[] paramArrayOfshort) {
    super.putShortArray(paramString, paramArrayOfshort);
  }
  
  public void putCharArray(String paramString, char[] paramArrayOfchar) {
    super.putCharArray(paramString, paramArrayOfchar);
  }
  
  public void putFloatArray(String paramString, float[] paramArrayOffloat) {
    super.putFloatArray(paramString, paramArrayOffloat);
  }
  
  public void putCharSequenceArray(String paramString, CharSequence[] paramArrayOfCharSequence) {
    super.putCharSequenceArray(paramString, paramArrayOfCharSequence);
  }
  
  public void putBundle(String paramString, Bundle paramBundle) {
    unparcel();
    this.mMap.put(paramString, paramBundle);
  }
  
  public void putBinder(String paramString, IBinder paramIBinder) {
    unparcel();
    this.mMap.put(paramString, paramIBinder);
  }
  
  @Deprecated
  public void putIBinder(String paramString, IBinder paramIBinder) {
    unparcel();
    this.mMap.put(paramString, paramIBinder);
  }
  
  public byte getByte(String paramString) {
    return super.getByte(paramString);
  }
  
  public Byte getByte(String paramString, byte paramByte) {
    return super.getByte(paramString, paramByte);
  }
  
  public char getChar(String paramString) {
    return super.getChar(paramString);
  }
  
  public char getChar(String paramString, char paramChar) {
    return super.getChar(paramString, paramChar);
  }
  
  public short getShort(String paramString) {
    return super.getShort(paramString);
  }
  
  public short getShort(String paramString, short paramShort) {
    return super.getShort(paramString, paramShort);
  }
  
  public float getFloat(String paramString) {
    return super.getFloat(paramString);
  }
  
  public float getFloat(String paramString, float paramFloat) {
    return super.getFloat(paramString, paramFloat);
  }
  
  public CharSequence getCharSequence(String paramString) {
    return super.getCharSequence(paramString);
  }
  
  public CharSequence getCharSequence(String paramString, CharSequence paramCharSequence) {
    return super.getCharSequence(paramString, paramCharSequence);
  }
  
  public Size getSize(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    try {
      return (Size)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Size", classCastException);
      return null;
    } 
  }
  
  public SizeF getSizeF(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    try {
      return (SizeF)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "SizeF", classCastException);
      return null;
    } 
  }
  
  public Bundle getBundle(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (Bundle)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Bundle", classCastException);
      return null;
    } 
  }
  
  public <T extends Parcelable> T getParcelable(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (T)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Parcelable", classCastException);
      return null;
    } 
  }
  
  public Parcelable[] getParcelableArray(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (Parcelable[])object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Parcelable[]", classCastException);
      return null;
    } 
  }
  
  public <T extends Parcelable> ArrayList<T> getParcelableArrayList(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (ArrayList)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "ArrayList", classCastException);
      return null;
    } 
  }
  
  public <T extends Parcelable> SparseArray<T> getSparseParcelableArray(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (SparseArray)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "SparseArray", classCastException);
      return null;
    } 
  }
  
  public Serializable getSerializable(String paramString) {
    return super.getSerializable(paramString);
  }
  
  public ArrayList<Integer> getIntegerArrayList(String paramString) {
    return super.getIntegerArrayList(paramString);
  }
  
  public ArrayList<String> getStringArrayList(String paramString) {
    return super.getStringArrayList(paramString);
  }
  
  public ArrayList<CharSequence> getCharSequenceArrayList(String paramString) {
    return super.getCharSequenceArrayList(paramString);
  }
  
  public byte[] getByteArray(String paramString) {
    return super.getByteArray(paramString);
  }
  
  public short[] getShortArray(String paramString) {
    return super.getShortArray(paramString);
  }
  
  public char[] getCharArray(String paramString) {
    return super.getCharArray(paramString);
  }
  
  public float[] getFloatArray(String paramString) {
    return super.getFloatArray(paramString);
  }
  
  public CharSequence[] getCharSequenceArray(String paramString) {
    return super.getCharSequenceArray(paramString);
  }
  
  public IBinder getBinder(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (IBinder)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "IBinder", classCastException);
      return null;
    } 
  }
  
  @Deprecated
  public IBinder getIBinder(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (IBinder)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "IBinder", classCastException);
      return null;
    } 
  }
  
  public int describeContents() {
    int i = 0;
    if (hasFileDescriptors())
      i = false | true; 
    return i;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if ((this.mFlags & 0x400) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    boolean bool = paramParcel.pushAllowFds(bool);
    try {
      writeToParcelInner(paramParcel, paramInt);
      return;
    } finally {
      paramParcel.restoreAllowFds(bool);
    } 
  }
  
  public void readFromParcel(Parcel paramParcel) {
    readFromParcelInner(paramParcel);
    this.mFlags = 1024;
    maybePrefillHasFds();
  }
  
  public String toString() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mParcelledData : Landroid/os/Parcel;
    //   6: ifnull -> 69
    //   9: aload_0
    //   10: invokevirtual isEmptyParcel : ()Z
    //   13: ifeq -> 22
    //   16: aload_0
    //   17: monitorexit
    //   18: ldc_w 'Bundle[EMPTY_PARCEL]'
    //   21: areturn
    //   22: new java/lang/StringBuilder
    //   25: astore_1
    //   26: aload_1
    //   27: invokespecial <init> : ()V
    //   30: aload_1
    //   31: ldc_w 'Bundle[mParcelledData.dataSize='
    //   34: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   37: pop
    //   38: aload_0
    //   39: getfield mParcelledData : Landroid/os/Parcel;
    //   42: astore_2
    //   43: aload_1
    //   44: aload_2
    //   45: invokevirtual dataSize : ()I
    //   48: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   51: pop
    //   52: aload_1
    //   53: ldc_w ']'
    //   56: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   59: pop
    //   60: aload_1
    //   61: invokevirtual toString : ()Ljava/lang/String;
    //   64: astore_1
    //   65: aload_0
    //   66: monitorexit
    //   67: aload_1
    //   68: areturn
    //   69: new java/lang/StringBuilder
    //   72: astore_1
    //   73: aload_1
    //   74: invokespecial <init> : ()V
    //   77: aload_1
    //   78: ldc_w 'Bundle['
    //   81: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   84: pop
    //   85: aload_1
    //   86: aload_0
    //   87: getfield mMap : Landroid/util/ArrayMap;
    //   90: invokevirtual toString : ()Ljava/lang/String;
    //   93: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   96: pop
    //   97: aload_1
    //   98: ldc_w ']'
    //   101: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   104: pop
    //   105: aload_1
    //   106: invokevirtual toString : ()Ljava/lang/String;
    //   109: astore_1
    //   110: aload_0
    //   111: monitorexit
    //   112: aload_1
    //   113: areturn
    //   114: astore_1
    //   115: aload_0
    //   116: monitorexit
    //   117: ldc_w 'Bundle[EMPTY_PARCEL]'
    //   120: areturn
    //   121: astore_1
    //   122: aload_0
    //   123: monitorexit
    //   124: aload_1
    //   125: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1322	-> 2
    //   #1323	-> 9
    //   #1324	-> 16
    //   #1326	-> 22
    //   #1327	-> 43
    //   #1326	-> 65
    //   #1336	-> 69
    //   #1338	-> 114
    //   #1340	-> 115
    //   #1321	-> 121
    // Exception table:
    //   from	to	target	type
    //   2	9	121	finally
    //   9	16	121	finally
    //   22	43	121	finally
    //   43	65	121	finally
    //   69	110	114	java/lang/ArrayIndexOutOfBoundsException
    //   69	110	121	finally
  }
  
  public String toShortString() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mParcelledData : Landroid/os/Parcel;
    //   6: ifnull -> 59
    //   9: aload_0
    //   10: invokevirtual isEmptyParcel : ()Z
    //   13: ifeq -> 22
    //   16: aload_0
    //   17: monitorexit
    //   18: ldc_w 'EMPTY_PARCEL'
    //   21: areturn
    //   22: new java/lang/StringBuilder
    //   25: astore_1
    //   26: aload_1
    //   27: invokespecial <init> : ()V
    //   30: aload_1
    //   31: ldc_w 'mParcelledData.dataSize='
    //   34: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   37: pop
    //   38: aload_1
    //   39: aload_0
    //   40: getfield mParcelledData : Landroid/os/Parcel;
    //   43: invokevirtual dataSize : ()I
    //   46: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: aload_1
    //   51: invokevirtual toString : ()Ljava/lang/String;
    //   54: astore_1
    //   55: aload_0
    //   56: monitorexit
    //   57: aload_1
    //   58: areturn
    //   59: aload_0
    //   60: getfield mMap : Landroid/util/ArrayMap;
    //   63: invokevirtual toString : ()Ljava/lang/String;
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: areturn
    //   71: astore_1
    //   72: aload_0
    //   73: monitorexit
    //   74: aload_1
    //   75: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1349	-> 2
    //   #1350	-> 9
    //   #1351	-> 16
    //   #1353	-> 22
    //   #1356	-> 59
    //   #1348	-> 71
    // Exception table:
    //   from	to	target	type
    //   2	9	71	finally
    //   9	16	71	finally
    //   22	55	71	finally
    //   59	67	71	finally
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    if (this.mParcelledData != null) {
      if (isEmptyParcel()) {
        paramProtoOutputStream.write(1120986464257L, 0);
      } else {
        paramProtoOutputStream.write(1120986464257L, this.mParcelledData.dataSize());
      } 
    } else {
      paramProtoOutputStream.write(1138166333442L, this.mMap.toString());
    } 
    paramProtoOutputStream.end(paramLong);
  }
}
