package android.os;

import android.graphics.Paint;
import android.util.Log;

public class OplusAssertTip {
  private static final boolean DEBUG = false;
  
  private static final String PROP_ASSERT_TIP_RUNNING = "persist.sys.assert.state";
  
  private static final String TAG = "OplusAssertTip";
  
  private static OplusAssertTip mOppoAssertTipInstance = null;
  
  public static OplusAssertTip getInstance() {
    if (mOppoAssertTipInstance == null)
      mOppoAssertTipInstance = new OplusAssertTip(); 
    return mOppoAssertTipInstance;
  }
  
  public void makeSureAssertTipServiceRunning() {
    boolean bool = SystemProperties.getBoolean("persist.sys.assert.state", false);
    if (!bool)
      SystemProperties.set("persist.sys.assert.state", "true"); 
  }
  
  public int requestShowAssertMessage(String paramString) {
    if (paramString == null || paramString.length() <= 0) {
      Log.w("OplusAssertTip", "requestShowAssertMessage:message is empty!");
      return -1;
    } 
    return showAssertMessage(paramString);
  }
  
  public int requestSetTipTextPaintAttr(int paramInt) {
    if (paramInt < 10) {
      Log.w("OplusAssertTip", "size is too small! set larger than 10.");
      return 0;
    } 
    Paint paint = new Paint();
    paint.setTextSize(paramInt);
    float f = paint.measureText(new char[] { 'W' }, 0, 1);
    return setTipTextPaintAttr(paramInt, (int)f);
  }
  
  public boolean isAssertTipShowed() {
    int i = getAssertMessageState();
    if (i == 1)
      return true; 
    return false;
  }
  
  private native int setTipTextPaintAttr(int paramInt1, int paramInt2);
  
  public native int getAssertMessageState();
  
  public native int hideAssertMessage();
  
  public native int showAssertMessage(String paramString);
  
  public native int testAddFunction(int paramInt1, int paramInt2);
}
