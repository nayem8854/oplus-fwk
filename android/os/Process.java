package android.os;

import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Pair;
import android.webkit.WebViewZygote;
import dalvik.system.VMRuntime;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class Process {
  public static final int AUDIOSERVER_UID = 1041;
  
  public static final int BLUETOOTH_UID = 1002;
  
  public static final int CAMERASERVER_UID = 1047;
  
  public static final int CLAT_UID = 1029;
  
  public static final int CREDSTORE_UID = 1076;
  
  public static final int DNS_TETHER_UID = 1052;
  
  public static final int DRM_UID = 1019;
  
  public static final int EXT_DATA_RW_GID = 1078;
  
  public static final int EXT_OBB_RW_GID = 1079;
  
  public static final int FIRST_APPLICATION_CACHE_GID = 20000;
  
  public static final int FIRST_APPLICATION_UID = 10000;
  
  public static final int FIRST_APP_ZYGOTE_ISOLATED_UID = 90000;
  
  public static final int FIRST_ISOLATED_UID = 99000;
  
  public static final int FIRST_SHARED_APPLICATION_GID = 50000;
  
  public static final int FSVERITY_CERT_UID = 1075;
  
  public static final int INCIDENTD_UID = 1067;
  
  public static final int INET_GID = 3003;
  
  public static final int INVALID_UID = -1;
  
  public static final int IORAPD_UID = 1071;
  
  public static final int KEYSTORE_UID = 1017;
  
  public static final int LAST_APPLICATION_CACHE_GID = 29999;
  
  public static final int LAST_APPLICATION_UID = 19999;
  
  public static final int LAST_APP_ZYGOTE_ISOLATED_UID = 98999;
  
  public static final int LAST_ISOLATED_UID = 99999;
  
  public static final int LAST_SHARED_APPLICATION_GID = 59999;
  
  private static final String LOG_TAG = "Process";
  
  public static final int LOG_UID = 1007;
  
  public static final int MEDIA_RW_GID = 1023;
  
  public static final int MEDIA_UID = 1013;
  
  public static final int NETWORK_STACK_UID = 1073;
  
  public static final int NFC_UID = 1027;
  
  public static final int NOBODY_UID = 9999;
  
  public static final int NUM_UIDS_PER_APP_ZYGOTE = 100;
  
  public static final int OTA_UPDATE_UID = 1061;
  
  public static final int PACKAGE_INFO_GID = 1032;
  
  public static final int PHONE_UID = 1001;
  
  private static final int PIDFD_SUPPORTED = 1;
  
  private static final int PIDFD_UNKNOWN = 0;
  
  private static final int PIDFD_UNSUPPORTED = 2;
  
  public static final int PROC_CHAR = 2048;
  
  public static final int PROC_COMBINE = 256;
  
  public static final int PROC_NEWLINE_TERM = 10;
  
  public static final int PROC_OUT_FLOAT = 16384;
  
  public static final int PROC_OUT_LONG = 8192;
  
  public static final int PROC_OUT_STRING = 4096;
  
  public static final int PROC_PARENS = 512;
  
  public static final int PROC_QUOTES = 1024;
  
  public static final int PROC_SPACE_TERM = 32;
  
  public static final int PROC_TAB_TERM = 9;
  
  public static final int PROC_TERM_MASK = 255;
  
  public static final int PROC_ZERO_TERM = 0;
  
  public static final int ROOT_UID = 0;
  
  public static final int SCHED_BATCH = 3;
  
  public static final int SCHED_FIFO = 1;
  
  public static final int SCHED_IDLE = 5;
  
  public static final int SCHED_OTHER = 0;
  
  public static final int SCHED_RESET_ON_FORK = 1073741824;
  
  public static final int SCHED_RR = 2;
  
  public static final int SDCARD_RW_GID = 1015;
  
  public static final int SE_UID = 1068;
  
  public static final int SHARED_RELRO_UID = 1037;
  
  public static final int SHARED_USER_GID = 9997;
  
  public static final int SHELL_UID = 2000;
  
  public static final int SIGNAL_KILL = 9;
  
  public static final int SIGNAL_QUIT = 3;
  
  public static final int SIGNAL_USR1 = 10;
  
  public static final int STATSD_UID = 1066;
  
  public static final int SYSTEM_UID = 1000;
  
  public static final int THREAD_GROUP_AUDIO_APP = 3;
  
  public static final int THREAD_GROUP_AUDIO_SYS = 4;
  
  public static final int THREAD_GROUP_BACKGROUND = 0;
  
  public static final int THREAD_GROUP_DEFAULT = -1;
  
  private static final int THREAD_GROUP_FOREGROUND = 1;
  
  public static final int THREAD_GROUP_RESTRICTED = 7;
  
  public static final int THREAD_GROUP_RT_APP = 6;
  
  public static final int THREAD_GROUP_SYSTEM = 2;
  
  public static final int THREAD_GROUP_TOP_APP = 5;
  
  public static final int THREAD_PRIORITY_AUDIO = -16;
  
  public static final int THREAD_PRIORITY_BACKGROUND = 10;
  
  public static final int THREAD_PRIORITY_DEFAULT = 0;
  
  public static final int THREAD_PRIORITY_DISPLAY = -4;
  
  public static final int THREAD_PRIORITY_FOREGROUND = -2;
  
  public static final int THREAD_PRIORITY_LESS_FAVORABLE = 1;
  
  public static final int THREAD_PRIORITY_LOWEST = 19;
  
  public static final int THREAD_PRIORITY_MORE_FAVORABLE = -1;
  
  public static final int THREAD_PRIORITY_URGENT_AUDIO = -19;
  
  public static final int THREAD_PRIORITY_URGENT_DISPLAY = -8;
  
  public static final int THREAD_PRIORITY_VIDEO = -10;
  
  public static final int VPN_UID = 1016;
  
  public static final int WEBVIEW_ZYGOTE_UID = 1053;
  
  public static final int WIFI_UID = 1010;
  
  public static final int ZYGOTE_POLICY_FLAG_BATCH_LAUNCH = 2;
  
  public static final int ZYGOTE_POLICY_FLAG_EMPTY = 0;
  
  public static final int ZYGOTE_POLICY_FLAG_LATENCY_SENSITIVE = 1;
  
  public static final int ZYGOTE_POLICY_FLAG_SYSTEM_PROCESS = 4;
  
  public static final ZygoteProcess ZYGOTE_PROCESS;
  
  private static int sPidFdSupported = 0;
  
  private static long sStartElapsedRealtime;
  
  private static long sStartUptimeMillis;
  
  static {
    ZYGOTE_PROCESS = new ZygoteProcess();
  }
  
  public static ProcessStartResult start(String paramString1, String paramString2, int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3, int paramInt4, int paramInt5, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, int paramInt6, boolean paramBoolean1, long[] paramArrayOflong, Map<String, Pair<String, Long>> paramMap1, Map<String, Pair<String, Long>> paramMap2, boolean paramBoolean2, boolean paramBoolean3, String[] paramArrayOfString) {
    return ZYGOTE_PROCESS.start(paramString1, paramString2, paramInt1, paramInt2, paramArrayOfint, paramInt3, paramInt4, paramInt5, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8, paramInt6, paramBoolean1, paramArrayOflong, paramMap1, paramMap2, paramBoolean2, paramBoolean3, paramArrayOfString);
  }
  
  public static ProcessStartResult startWebView(String paramString1, String paramString2, int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3, int paramInt4, int paramInt5, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, long[] paramArrayOflong, String[] paramArrayOfString) {
    return WebViewZygote.getProcess().start(paramString1, paramString2, paramInt1, paramInt2, paramArrayOfint, paramInt3, paramInt4, paramInt5, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8, 0, false, paramArrayOflong, null, null, false, false, paramArrayOfString);
  }
  
  public static final long getStartElapsedRealtime() {
    return sStartElapsedRealtime;
  }
  
  public static final long getStartUptimeMillis() {
    return sStartUptimeMillis;
  }
  
  public static final void setStartTimes(long paramLong1, long paramLong2) {
    sStartElapsedRealtime = paramLong1;
    sStartUptimeMillis = paramLong2;
  }
  
  public static final boolean is64Bit() {
    return VMRuntime.getRuntime().is64Bit();
  }
  
  public static final int myPid() {
    return Os.getpid();
  }
  
  public static final int myPpid() {
    return Os.getppid();
  }
  
  public static final int myTid() {
    return Os.gettid();
  }
  
  public static final int myUid() {
    return Os.getuid();
  }
  
  public static UserHandle myUserHandle() {
    return UserHandle.of(UserHandle.getUserId(myUid()));
  }
  
  public static boolean isCoreUid(int paramInt) {
    return UserHandle.isCore(paramInt);
  }
  
  public static boolean isApplicationUid(int paramInt) {
    return UserHandle.isApp(paramInt);
  }
  
  public static final boolean isIsolated() {
    return isIsolated(myUid());
  }
  
  public static final boolean isIsolated(int paramInt) {
    boolean bool;
    paramInt = UserHandle.getAppId(paramInt);
    if ((paramInt >= 99000 && paramInt <= 99999) || (paramInt >= 90000 && paramInt <= 98999)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static final int getUidForPid(int paramInt) {
    long[] arrayOfLong = new long[1];
    arrayOfLong[0] = -1L;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("/proc/");
    stringBuilder.append(paramInt);
    stringBuilder.append("/status");
    readProcLines(stringBuilder.toString(), new String[] { "Uid:" }, arrayOfLong);
    return (int)arrayOfLong[0];
  }
  
  public static final int getParentPid(int paramInt) {
    long[] arrayOfLong = new long[1];
    arrayOfLong[0] = -1L;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("/proc/");
    stringBuilder.append(paramInt);
    stringBuilder.append("/status");
    readProcLines(stringBuilder.toString(), new String[] { "PPid:" }, arrayOfLong);
    return (int)arrayOfLong[0];
  }
  
  public static final int getThreadGroupLeader(int paramInt) {
    long[] arrayOfLong = new long[1];
    arrayOfLong[0] = -1L;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("/proc/");
    stringBuilder.append(paramInt);
    stringBuilder.append("/status");
    readProcLines(stringBuilder.toString(), new String[] { "Tgid:" }, arrayOfLong);
    return (int)arrayOfLong[0];
  }
  
  @Deprecated
  public static final boolean supportsProcesses() {
    return true;
  }
  
  public static final void killProcess(int paramInt) {
    sendSignal(paramInt, 9);
  }
  
  public static final void killProcessQuiet(int paramInt) {
    sendSignalQuiet(paramInt, 9);
  }
  
  public static final class ProcessStartResult {
    public int pid;
    
    public boolean usingWrapper;
  }
  
  public static final boolean isThreadInProcess(int paramInt1, int paramInt2) {
    StrictMode.ThreadPolicy threadPolicy = StrictMode.allowThreadDiskReads();
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("/proc/");
      stringBuilder.append(paramInt1);
      stringBuilder.append("/task/");
      stringBuilder.append(paramInt2);
      boolean bool = Os.access(stringBuilder.toString(), OsConstants.F_OK);
      if (bool)
        return true; 
      return false;
    } catch (Exception exception) {
      return false;
    } finally {
      StrictMode.setThreadPolicy(threadPolicy);
    } 
  }
  
  public static void waitForProcessDeath(int paramInt1, int paramInt2) throws InterruptedException, TimeoutException {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: getstatic android/os/Process.sPidFdSupported : I
    //   5: istore_3
    //   6: iconst_1
    //   7: istore #4
    //   9: aload_2
    //   10: astore #5
    //   12: iload_3
    //   13: ifne -> 147
    //   16: iconst_m1
    //   17: istore_3
    //   18: iload_3
    //   19: istore #6
    //   21: iload_0
    //   22: iconst_0
    //   23: invokestatic nativePidFdOpen : (II)I
    //   26: istore #7
    //   28: iload #7
    //   30: istore #6
    //   32: iload #7
    //   34: istore_3
    //   35: iconst_1
    //   36: putstatic android/os/Process.sPidFdSupported : I
    //   39: aload_2
    //   40: astore #5
    //   42: iload #7
    //   44: iflt -> 147
    //   47: new java/io/FileDescriptor
    //   50: dup
    //   51: invokespecial <init> : ()V
    //   54: astore_2
    //   55: iload #7
    //   57: istore_3
    //   58: aload_2
    //   59: iload_3
    //   60: invokevirtual setInt$ : (I)V
    //   63: aload_2
    //   64: astore #5
    //   66: goto -> 147
    //   69: astore_2
    //   70: goto -> 124
    //   73: astore #8
    //   75: iload_3
    //   76: istore #6
    //   78: aload #8
    //   80: getfield errno : I
    //   83: getstatic android/system/OsConstants.ENOSYS : I
    //   86: if_icmpeq -> 95
    //   89: iconst_1
    //   90: istore #7
    //   92: goto -> 98
    //   95: iconst_2
    //   96: istore #7
    //   98: iload_3
    //   99: istore #6
    //   101: iload #7
    //   103: putstatic android/os/Process.sPidFdSupported : I
    //   106: aload_2
    //   107: astore #5
    //   109: iload_3
    //   110: iflt -> 147
    //   113: new java/io/FileDescriptor
    //   116: dup
    //   117: invokespecial <init> : ()V
    //   120: astore_2
    //   121: goto -> 58
    //   124: iload #6
    //   126: iflt -> 145
    //   129: new java/io/FileDescriptor
    //   132: dup
    //   133: invokespecial <init> : ()V
    //   136: astore #8
    //   138: aload #8
    //   140: iload #6
    //   142: invokevirtual setInt$ : (I)V
    //   145: aload_2
    //   146: athrow
    //   147: getstatic android/os/Process.sPidFdSupported : I
    //   150: iconst_2
    //   151: if_icmpne -> 160
    //   154: iconst_1
    //   155: istore #7
    //   157: goto -> 163
    //   160: iconst_0
    //   161: istore #7
    //   163: iload #7
    //   165: istore_3
    //   166: iload #7
    //   168: ifne -> 539
    //   171: aload #5
    //   173: astore #8
    //   175: iload #7
    //   177: istore #6
    //   179: aload #5
    //   181: ifnonnull -> 264
    //   184: aload #5
    //   186: astore #9
    //   188: aload #5
    //   190: astore_2
    //   191: iload_0
    //   192: iconst_0
    //   193: invokestatic nativePidFdOpen : (II)I
    //   196: istore_3
    //   197: iload_3
    //   198: iflt -> 245
    //   201: aload #5
    //   203: astore #9
    //   205: aload #5
    //   207: astore_2
    //   208: new java/io/FileDescriptor
    //   211: astore #8
    //   213: aload #5
    //   215: astore #9
    //   217: aload #5
    //   219: astore_2
    //   220: aload #8
    //   222: invokespecial <init> : ()V
    //   225: aload #8
    //   227: astore #9
    //   229: aload #8
    //   231: astore_2
    //   232: aload #8
    //   234: iload_3
    //   235: invokevirtual setInt$ : (I)V
    //   238: iload #7
    //   240: istore #6
    //   242: goto -> 264
    //   245: iconst_1
    //   246: istore #6
    //   248: aload #5
    //   250: astore #8
    //   252: goto -> 264
    //   255: astore_2
    //   256: goto -> 510
    //   259: astore #8
    //   261: goto -> 446
    //   264: aload #8
    //   266: ifnull -> 522
    //   269: aload #8
    //   271: astore #9
    //   273: aload #8
    //   275: astore_2
    //   276: iconst_1
    //   277: anewarray android/system/StructPollfd
    //   280: astore #5
    //   282: aload #8
    //   284: astore #9
    //   286: aload #8
    //   288: astore_2
    //   289: new android/system/StructPollfd
    //   292: astore #10
    //   294: aload #8
    //   296: astore #9
    //   298: aload #8
    //   300: astore_2
    //   301: aload #10
    //   303: invokespecial <init> : ()V
    //   306: aload #5
    //   308: iconst_0
    //   309: aload #10
    //   311: aastore
    //   312: aload #8
    //   314: astore #9
    //   316: aload #8
    //   318: astore_2
    //   319: aload #5
    //   321: iconst_0
    //   322: aaload
    //   323: aload #8
    //   325: putfield fd : Ljava/io/FileDescriptor;
    //   328: aload #8
    //   330: astore #9
    //   332: aload #8
    //   334: astore_2
    //   335: aload #5
    //   337: iconst_0
    //   338: aaload
    //   339: getstatic android/system/OsConstants.POLLIN : I
    //   342: i2s
    //   343: putfield events : S
    //   346: aload #8
    //   348: astore #9
    //   350: aload #8
    //   352: astore_2
    //   353: aload #5
    //   355: iconst_0
    //   356: aaload
    //   357: iconst_0
    //   358: putfield revents : S
    //   361: aload #8
    //   363: astore #9
    //   365: aload #8
    //   367: astore_2
    //   368: aload #5
    //   370: iconst_0
    //   371: aaload
    //   372: aconst_null
    //   373: putfield userData : Ljava/lang/Object;
    //   376: aload #8
    //   378: astore #9
    //   380: aload #8
    //   382: astore_2
    //   383: aload #5
    //   385: iload_1
    //   386: invokestatic poll : ([Landroid/system/StructPollfd;I)I
    //   389: istore_3
    //   390: iload_3
    //   391: ifle -> 405
    //   394: aload #8
    //   396: ifnull -> 404
    //   399: aload #8
    //   401: invokestatic closeQuietly : (Ljava/io/FileDescriptor;)V
    //   404: return
    //   405: iload_3
    //   406: ifeq -> 412
    //   409: goto -> 522
    //   412: aload #8
    //   414: astore #9
    //   416: aload #8
    //   418: astore_2
    //   419: new java/util/concurrent/TimeoutException
    //   422: astore #5
    //   424: aload #8
    //   426: astore #9
    //   428: aload #8
    //   430: astore_2
    //   431: aload #5
    //   433: invokespecial <init> : ()V
    //   436: aload #8
    //   438: astore #9
    //   440: aload #8
    //   442: astore_2
    //   443: aload #5
    //   445: athrow
    //   446: aload_2
    //   447: astore #9
    //   449: aload #8
    //   451: getfield errno : I
    //   454: istore #6
    //   456: aload_2
    //   457: astore #9
    //   459: getstatic android/system/OsConstants.EINTR : I
    //   462: istore_3
    //   463: iload #6
    //   465: iload_3
    //   466: if_icmpeq -> 488
    //   469: iconst_1
    //   470: istore_3
    //   471: iconst_1
    //   472: istore #6
    //   474: aload_2
    //   475: ifnull -> 539
    //   478: iload #6
    //   480: istore_3
    //   481: aload_2
    //   482: invokestatic closeQuietly : (Ljava/io/FileDescriptor;)V
    //   485: goto -> 539
    //   488: aload_2
    //   489: astore #9
    //   491: new java/lang/InterruptedException
    //   494: astore #8
    //   496: aload_2
    //   497: astore #9
    //   499: aload #8
    //   501: invokespecial <init> : ()V
    //   504: aload_2
    //   505: astore #9
    //   507: aload #8
    //   509: athrow
    //   510: aload #9
    //   512: ifnull -> 520
    //   515: aload #9
    //   517: invokestatic closeQuietly : (Ljava/io/FileDescriptor;)V
    //   520: aload_2
    //   521: athrow
    //   522: iload #6
    //   524: istore_3
    //   525: aload #8
    //   527: ifnull -> 539
    //   530: aload #8
    //   532: astore_2
    //   533: iload #6
    //   535: istore_3
    //   536: goto -> 481
    //   539: iload_3
    //   540: ifeq -> 615
    //   543: iload_1
    //   544: ifge -> 553
    //   547: iload #4
    //   549: istore_3
    //   550: goto -> 555
    //   553: iconst_0
    //   554: istore_3
    //   555: invokestatic currentTimeMillis : ()J
    //   558: lstore #11
    //   560: iload_1
    //   561: i2l
    //   562: lstore #13
    //   564: lload #11
    //   566: lstore #15
    //   568: iload_3
    //   569: ifne -> 583
    //   572: lload #15
    //   574: lload #13
    //   576: lload #11
    //   578: ladd
    //   579: lcmp
    //   580: ifge -> 615
    //   583: iload_0
    //   584: iconst_0
    //   585: invokestatic kill : (II)V
    //   588: goto -> 603
    //   591: astore_2
    //   592: aload_2
    //   593: getfield errno : I
    //   596: getstatic android/system/OsConstants.ESRCH : I
    //   599: if_icmpne -> 603
    //   602: return
    //   603: lconst_1
    //   604: invokestatic sleep : (J)V
    //   607: invokestatic currentTimeMillis : ()J
    //   610: lstore #15
    //   612: goto -> 568
    //   615: new java/util/concurrent/TimeoutException
    //   618: dup
    //   619: invokespecial <init> : ()V
    //   622: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1356	-> 0
    //   #1357	-> 2
    //   #1358	-> 16
    //   #1360	-> 18
    //   #1361	-> 28
    //   #1366	-> 39
    //   #1367	-> 47
    //   #1368	-> 58
    //   #1366	-> 69
    //   #1362	-> 73
    //   #1363	-> 75
    //   #1364	-> 89
    //   #1366	-> 106
    //   #1367	-> 113
    //   #1366	-> 124
    //   #1367	-> 129
    //   #1368	-> 138
    //   #1370	-> 145
    //   #1372	-> 147
    //   #1373	-> 163
    //   #1375	-> 171
    //   #1376	-> 184
    //   #1377	-> 197
    //   #1378	-> 201
    //   #1379	-> 225
    //   #1381	-> 245
    //   #1407	-> 255
    //   #1401	-> 259
    //   #1384	-> 264
    //   #1385	-> 269
    //   #1388	-> 312
    //   #1389	-> 328
    //   #1390	-> 346
    //   #1391	-> 361
    //   #1392	-> 376
    //   #1393	-> 390
    //   #1407	-> 394
    //   #1408	-> 399
    //   #1394	-> 404
    //   #1395	-> 405
    //   #1396	-> 412
    //   #1402	-> 446
    //   #1405	-> 469
    //   #1407	-> 474
    //   #1408	-> 481
    //   #1403	-> 488
    //   #1407	-> 510
    //   #1408	-> 515
    //   #1410	-> 520
    //   #1407	-> 522
    //   #1408	-> 530
    //   #1412	-> 539
    //   #1413	-> 543
    //   #1414	-> 555
    //   #1415	-> 560
    //   #1416	-> 568
    //   #1418	-> 583
    //   #1423	-> 588
    //   #1419	-> 591
    //   #1420	-> 592
    //   #1421	-> 602
    //   #1424	-> 603
    //   #1425	-> 607
    //   #1428	-> 615
    // Exception table:
    //   from	to	target	type
    //   21	28	73	android/system/ErrnoException
    //   21	28	69	finally
    //   35	39	73	android/system/ErrnoException
    //   35	39	69	finally
    //   78	89	69	finally
    //   101	106	69	finally
    //   191	197	259	android/system/ErrnoException
    //   191	197	255	finally
    //   208	213	259	android/system/ErrnoException
    //   208	213	255	finally
    //   220	225	259	android/system/ErrnoException
    //   220	225	255	finally
    //   232	238	259	android/system/ErrnoException
    //   232	238	255	finally
    //   276	282	259	android/system/ErrnoException
    //   276	282	255	finally
    //   289	294	259	android/system/ErrnoException
    //   289	294	255	finally
    //   301	306	259	android/system/ErrnoException
    //   301	306	255	finally
    //   319	328	259	android/system/ErrnoException
    //   319	328	255	finally
    //   335	346	259	android/system/ErrnoException
    //   335	346	255	finally
    //   353	361	259	android/system/ErrnoException
    //   353	361	255	finally
    //   368	376	259	android/system/ErrnoException
    //   368	376	255	finally
    //   383	390	259	android/system/ErrnoException
    //   383	390	255	finally
    //   419	424	259	android/system/ErrnoException
    //   419	424	255	finally
    //   431	436	259	android/system/ErrnoException
    //   431	436	255	finally
    //   443	446	259	android/system/ErrnoException
    //   443	446	255	finally
    //   449	456	255	finally
    //   459	463	255	finally
    //   491	496	255	finally
    //   499	504	255	finally
    //   507	510	255	finally
    //   583	588	591	android/system/ErrnoException
  }
  
  public static final native void enableFreezer(boolean paramBoolean);
  
  public static final native long getElapsedCpuTime();
  
  public static final native int[] getExclusiveCores();
  
  public static final native long getFreeMemory();
  
  public static final native int getGidForName(String paramString);
  
  public static final native int[] getPids(String paramString, int[] paramArrayOfint);
  
  public static final native int[] getPidsForCommands(String[] paramArrayOfString);
  
  public static final native int getProcessGroup(int paramInt) throws IllegalArgumentException, SecurityException;
  
  public static final native int getProcessState(int paramInt);
  
  public static final native long getPss(int paramInt);
  
  public static final native long[] getRss(int paramInt);
  
  public static final native int getThreadPriority(int paramInt) throws IllegalArgumentException;
  
  public static final native int getThreadScheduler(int paramInt) throws IllegalArgumentException;
  
  public static final native long getTotalMemory();
  
  public static final native int getUidForName(String paramString);
  
  public static final native int killProcessGroup(int paramInt1, int paramInt2);
  
  private static native int nativePidFdOpen(int paramInt1, int paramInt2) throws ErrnoException;
  
  public static final native boolean parseProcLine(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int[] paramArrayOfint, String[] paramArrayOfString, long[] paramArrayOflong, float[] paramArrayOffloat);
  
  public static final native boolean readProcFile(String paramString, int[] paramArrayOfint, String[] paramArrayOfString, long[] paramArrayOflong, float[] paramArrayOffloat);
  
  public static final native void readProcLines(String paramString, String[] paramArrayOfString, long[] paramArrayOflong);
  
  public static final native void removeAllProcessGroups();
  
  public static final native void sendSignal(int paramInt1, int paramInt2);
  
  public static final native void sendSignalQuiet(int paramInt1, int paramInt2);
  
  public static final native void setArgV0(String paramString);
  
  public static final native void setCanSelfBackground(boolean paramBoolean);
  
  public static final native void setCgroupProcsProcessGroup(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) throws IllegalArgumentException, SecurityException;
  
  public static final native int setGid(int paramInt);
  
  public static final native void setProcessFrozen(int paramInt1, int paramInt2, boolean paramBoolean);
  
  public static final native void setProcessGroup(int paramInt1, int paramInt2) throws IllegalArgumentException, SecurityException;
  
  public static final native boolean setSwappiness(int paramInt, boolean paramBoolean);
  
  public static final native void setThreadGroup(int paramInt1, int paramInt2) throws IllegalArgumentException, SecurityException;
  
  public static final native void setThreadGroupAndCpuset(int paramInt1, int paramInt2) throws IllegalArgumentException, SecurityException;
  
  public static final native void setThreadPriority(int paramInt) throws IllegalArgumentException, SecurityException;
  
  public static final native void setThreadPriority(int paramInt1, int paramInt2) throws IllegalArgumentException, SecurityException;
  
  public static final native void setThreadScheduler(int paramInt1, int paramInt2, int paramInt3) throws IllegalArgumentException;
  
  public static final native int setUid(int paramInt);
}
