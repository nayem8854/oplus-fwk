package android.os;

import android.content.Context;
import android.util.Slog;
import com.android.internal.app.IMotorManager;

public final class MotorManager {
  private static final boolean DEBUG = true;
  
  public static final int MOTORDOWNED = 0;
  
  public static final int MOTORDOWNING = -1;
  
  public static final int MOTORDOWNLOCKED = -5;
  
  public static final int MOTORERROR = -10;
  
  public static final int MOTORUPED = 10;
  
  public static final int MOTORUPING = 1;
  
  public static final int MOTORUPLOCKED = 5;
  
  public static final String TAG = "MotorManager";
  
  private static MotorStateChangedCallback sMotorStateChangedCallback;
  
  private Context mContext;
  
  private IMotorManager mService;
  
  private IBinder mToken = new Binder();
  
  public MotorManager(Context paramContext, IMotorManager paramIMotorManager) {
    this.mContext = paramContext;
    this.mService = paramIMotorManager;
    if (paramIMotorManager == null)
      Slog.v("MotorManager", "MotorManagerService was null"); 
  }
  
  public static void sendMotorStateChanged(int paramInt) {
    Slog.d("MotorManager", "sendMotorStateChanged");
    if (sMotorStateChangedCallback != null) {
      Slog.d("MotorManager", "callback keyguard StateChanged");
      sMotorStateChangedCallback.onMotorStateChanged(paramInt);
    } 
  }
  
  public void registerMotorStateChangedCallback(MotorStateChangedCallback paramMotorStateChangedCallback) {
    Slog.d("MotorManager", "registerMotorStateChangedCallback");
    sMotorStateChangedCallback = paramMotorStateChangedCallback;
  }
  
  public void unregisterMotorStateChangedCallback() {
    Slog.d("MotorManager", "unregisterMotorStateChangedCallback");
    sMotorStateChangedCallback = null;
  }
  
  public int getMotorStateBySystemApp() {
    Slog.d("MotorManager", "getMotorStateBySystemApp");
    IMotorManager iMotorManager = this.mService;
    if (iMotorManager != null)
      try {
        return iMotorManager.getMotorStateBySystemApp();
      } catch (RemoteException remoteException) {
        Slog.w("MotorManager", "Remote exception in motormanager: ", (Throwable)remoteException);
      }  
    return -10;
  }
  
  public int downMotorBySystemApp(String paramString) {
    Slog.d("MotorManager", "downMotorBySystemApp");
    if (this.mService != null && paramString != null)
      try {
        Slog.d("MotorManager", "call service downMotorBySystemApp");
        return this.mService.downMotorBySystemApp(paramString, this.mToken);
      } catch (RemoteException remoteException) {
        Slog.w("MotorManager", "Remote exception in motormanager: ", (Throwable)remoteException);
      }  
    return 0;
  }
  
  public int upMotorBySystemApp(String paramString) {
    Slog.d("MotorManager", "upMotorBySystemApp");
    if (this.mService != null && paramString != null)
      try {
        Slog.d("MotorManager", "call service upMotorBySystemApp");
        return this.mService.upMotorBySystemApp(paramString, this.mToken);
      } catch (RemoteException remoteException) {
        Slog.w("MotorManager", "Remote exception in motormanager: ", (Throwable)remoteException);
      }  
    return 0;
  }
  
  public int downMotorByPrivacyApp(String paramString, int paramInt) {
    Slog.d("MotorManager", "downMotorByPrivacyApp");
    if (this.mService != null && paramString != null)
      try {
        Slog.d("MotorManager", "call service downMotorByPrivacyApp");
        return this.mService.downMotorByPrivacyApp(paramString, paramInt, this.mToken);
      } catch (RemoteException remoteException) {
        Slog.w("MotorManager", "Remote exception in motormanager: ", (Throwable)remoteException);
      }  
    return 0;
  }
  
  public static abstract class MotorStateChangedCallback {
    public void onMotorStateChanged(int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onMotorStateChanged state: ");
      stringBuilder.append(Integer.toString(param1Int));
      Slog.d("MotorManager", stringBuilder.toString());
    }
  }
  
  public void breathLedLoopEffect(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("breathLedLoopEffect effect :");
    stringBuilder.append(paramInt);
    Slog.d("MotorManager", stringBuilder.toString());
    if (this.mService != null)
      try {
        Slog.d("MotorManager", "call service downMotorBybreathLedLoopEffect");
        this.mService.breathLedLoopEffect(paramInt);
      } catch (RemoteException remoteException) {
        Slog.w("MotorManager", "Remote exception in motormanager: ", (Throwable)remoteException);
      }  
  }
}
