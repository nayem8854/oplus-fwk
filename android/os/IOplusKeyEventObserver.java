package android.os;

import android.view.KeyEvent;

public interface IOplusKeyEventObserver extends IInterface {
  void onKeyEvent(KeyEvent paramKeyEvent) throws RemoteException;
  
  class Default implements IOplusKeyEventObserver {
    public void onKeyEvent(KeyEvent param1KeyEvent) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusKeyEventObserver {
    private static final String DESCRIPTOR = "android.os.IOplusKeyEventObserver";
    
    static final int TRANSACTION_onKeyEvent = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IOplusKeyEventObserver");
    }
    
    public static IOplusKeyEventObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IOplusKeyEventObserver");
      if (iInterface != null && iInterface instanceof IOplusKeyEventObserver)
        return (IOplusKeyEventObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onKeyEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.IOplusKeyEventObserver");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IOplusKeyEventObserver");
      if (param1Parcel1.readInt() != 0) {
        KeyEvent keyEvent = KeyEvent.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onKeyEvent((KeyEvent)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOplusKeyEventObserver {
      public static IOplusKeyEventObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IOplusKeyEventObserver";
      }
      
      public void onKeyEvent(KeyEvent param2KeyEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusKeyEventObserver");
          if (param2KeyEvent != null) {
            parcel.writeInt(1);
            param2KeyEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IOplusKeyEventObserver.Stub.getDefaultImpl() != null) {
            IOplusKeyEventObserver.Stub.getDefaultImpl().onKeyEvent(param2KeyEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusKeyEventObserver param1IOplusKeyEventObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusKeyEventObserver != null) {
          Proxy.sDefaultImpl = param1IOplusKeyEventObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusKeyEventObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
