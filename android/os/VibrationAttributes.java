package android.os;

import android.media.AudioAttributes;
import android.util.Slog;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

public final class VibrationAttributes implements Parcelable {
  private VibrationAttributes(int paramInt1, int paramInt2, AudioAttributes paramAudioAttributes) {
    this.mUsage = paramInt1;
    this.mFlags = paramInt2;
    this.mAudioAttributes = paramAudioAttributes;
  }
  
  public int getUsageClass() {
    return this.mUsage & 0xF;
  }
  
  public int getUsage() {
    return this.mUsage;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public boolean isFlagSet(int paramInt) {
    boolean bool;
    if ((this.mFlags & paramInt) > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @Deprecated
  public AudioAttributes getAudioAttributes() {
    return this.mAudioAttributes;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mUsage);
    paramParcel.writeInt(this.mFlags);
    paramParcel.writeParcelable(this.mAudioAttributes, paramInt);
  }
  
  private VibrationAttributes(Parcel paramParcel) {
    this.mUsage = paramParcel.readInt();
    this.mFlags = paramParcel.readInt();
    ClassLoader classLoader = AudioAttributes.class.getClassLoader();
    this.mAudioAttributes = paramParcel.<AudioAttributes>readParcelable(classLoader);
  }
  
  public static final Parcelable.Creator<VibrationAttributes> CREATOR = new Parcelable.Creator<VibrationAttributes>() {
      public VibrationAttributes createFromParcel(Parcel param1Parcel) {
        return new VibrationAttributes(param1Parcel);
      }
      
      public VibrationAttributes[] newArray(int param1Int) {
        return new VibrationAttributes[param1Int];
      }
    };
  
  public static final int FLAG_BYPASS_INTERRUPTION_POLICY = 1;
  
  private static final long MAX_HAPTIC_FEEDBACK_DURATION = 5000L;
  
  private static final String TAG = "VibrationAttributes";
  
  public static final int USAGE_ALARM = 17;
  
  public static final int USAGE_CLASS_ALARM = 1;
  
  public static final int USAGE_CLASS_FEEDBACK = 2;
  
  public static final int USAGE_CLASS_MASK = 15;
  
  public static final int USAGE_CLASS_UNKNOWN = 0;
  
  public static final int USAGE_COMMUNICATION_REQUEST = 65;
  
  public static final int USAGE_HARDWARE_FEEDBACK = 50;
  
  public static final int USAGE_NOTIFICATION = 49;
  
  public static final int USAGE_PHYSICAL_EMULATION = 34;
  
  public static final int USAGE_RINGTONE = 33;
  
  public static final int USAGE_TOUCH = 18;
  
  public static final int USAGE_UNKNOWN = 0;
  
  private final AudioAttributes mAudioAttributes;
  
  private final int mFlags;
  
  private final int mUsage;
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mUsage != ((VibrationAttributes)paramObject).mUsage || this.mFlags != ((VibrationAttributes)paramObject).mFlags)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mUsage), Integer.valueOf(this.mFlags) });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("VibrationAttributes: Usage=");
    stringBuilder.append(usageToString());
    stringBuilder.append(" Flags=");
    stringBuilder.append(this.mFlags);
    return stringBuilder.toString();
  }
  
  public String usageToString() {
    return usageToString(this.mUsage);
  }
  
  public String usageToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 65) {
        if (paramInt != 17) {
          if (paramInt != 18) {
            if (paramInt != 33) {
              if (paramInt != 34) {
                if (paramInt != 49) {
                  if (paramInt != 50) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("unknown usage ");
                    stringBuilder.append(paramInt);
                    return stringBuilder.toString();
                  } 
                  return "HARDWARE_FEEDBACK";
                } 
                return "NOTIFICATION";
              } 
              return "PHYSICAL_EMULATION";
            } 
            return "RIGNTONE";
          } 
          return "TOUCH";
        } 
        return "ALARM";
      } 
      return "COMMUNICATION_REQUEST";
    } 
    return "UNKNOWN";
  }
  
  class Builder {
    private int mUsage = 0;
    
    private int mFlags = 0;
    
    private AudioAttributes mAudioAttributes = (new AudioAttributes.Builder()).build();
    
    public Builder(VibrationAttributes this$0) {
      if (this$0 != null) {
        this.mUsage = this$0.mUsage;
        this.mFlags = this$0.mFlags;
        this.mAudioAttributes = this$0.mAudioAttributes;
      } 
    }
    
    public Builder(VibrationAttributes this$0, VibrationEffect param1VibrationEffect) {
      this.mAudioAttributes = (AudioAttributes)this$0;
      setUsage((AudioAttributes)this$0);
      setFlags((AudioAttributes)this$0);
      applyHapticFeedbackHeuristics(param1VibrationEffect);
    }
    
    private void applyHapticFeedbackHeuristics(VibrationEffect param1VibrationEffect) {
      if (param1VibrationEffect != null) {
        if (this.mUsage == 0 && param1VibrationEffect instanceof VibrationEffect.Prebaked) {
          VibrationEffect.Prebaked prebaked = (VibrationEffect.Prebaked)param1VibrationEffect;
          int i = prebaked.getId();
          if (i != 0 && i != 1 && i != 2 && i != 3 && i != 4 && i != 5 && i != 21) {
            Slog.w("VibrationAttributes", "Unknown prebaked vibration effect, assuming it isn't haptic feedback");
          } else {
            this.mUsage = 18;
          } 
        } 
        long l = param1VibrationEffect.getDuration();
        if (this.mUsage == 0 && l >= 0L && l < 5000L)
          this.mUsage = 18; 
      } 
    }
    
    private void setUsage(AudioAttributes param1AudioAttributes) {
      switch (param1AudioAttributes.getUsage()) {
        default:
          this.mUsage = 0;
          return;
        case 13:
          this.mUsage = 18;
          return;
        case 7:
        case 11:
          this.mUsage = 65;
          return;
        case 6:
          this.mUsage = 33;
          return;
        case 5:
        case 8:
        case 9:
        case 10:
          this.mUsage = 49;
          return;
        case 4:
          break;
      } 
      this.mUsage = 17;
    }
    
    private void setFlags(AudioAttributes param1AudioAttributes) {
      if ((param1AudioAttributes.getAllFlags() & 0x40) != 0)
        this.mFlags |= 0x1; 
    }
    
    public VibrationAttributes build() {
      return new VibrationAttributes(this.mUsage, this.mFlags, this.mAudioAttributes);
    }
    
    public Builder setUsage(int param1Int) {
      this.mUsage = param1Int;
      return this;
    }
    
    public Builder replaceFlags(int param1Int) {
      this.mFlags = param1Int;
      return this;
    }
    
    public Builder setFlags(int param1Int1, int param1Int2) {
      this.mFlags = this.mFlags & (param1Int2 ^ 0xFFFFFFFF) | param1Int1 & param1Int2;
      return this;
    }
    
    public Builder() {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Flag implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Usage implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class UsageClass implements Annotation {}
}
