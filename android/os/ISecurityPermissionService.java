package android.os;

import android.content.pm.PackagePermission;

public interface ISecurityPermissionService extends IInterface {
  void basicTypes(int paramInt, long paramLong, boolean paramBoolean, float paramFloat, double paramDouble, String paramString) throws RemoteException;
  
  boolean checkOplusPermission(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  PackagePermission queryPackagePermissionsAsUser(String paramString, int paramInt) throws RemoteException;
  
  int queryPermissionAsUser(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  void updateCachedPermission(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  class Default implements ISecurityPermissionService {
    public boolean checkOplusPermission(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void basicTypes(int param1Int, long param1Long, boolean param1Boolean, float param1Float, double param1Double, String param1String) throws RemoteException {}
    
    public int queryPermissionAsUser(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return 0;
    }
    
    public PackagePermission queryPackagePermissionsAsUser(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void updateCachedPermission(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISecurityPermissionService {
    private static final String DESCRIPTOR = "android.os.ISecurityPermissionService";
    
    static final int TRANSACTION_basicTypes = 2;
    
    static final int TRANSACTION_checkOplusPermission = 1;
    
    static final int TRANSACTION_queryPackagePermissionsAsUser = 4;
    
    static final int TRANSACTION_queryPermissionAsUser = 3;
    
    static final int TRANSACTION_updateCachedPermission = 5;
    
    public Stub() {
      attachInterface(this, "android.os.ISecurityPermissionService");
    }
    
    public static ISecurityPermissionService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.ISecurityPermissionService");
      if (iInterface != null && iInterface instanceof ISecurityPermissionService)
        return (ISecurityPermissionService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "updateCachedPermission";
            } 
            return "queryPackagePermissionsAsUser";
          } 
          return "queryPermissionAsUser";
        } 
        return "basicTypes";
      } 
      return "checkOplusPermission";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str1;
      if (param1Int1 != 1) {
        PackagePermission packagePermission;
        boolean bool1 = false;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.os.ISecurityPermissionService");
                return true;
              } 
              param1Parcel1.enforceInterface("android.os.ISecurityPermissionService");
              String str5 = param1Parcel1.readString();
              param1Int1 = param1Parcel1.readInt();
              if (param1Parcel1.readInt() != 0)
                bool1 = true; 
              updateCachedPermission(str5, param1Int1, bool1);
              param1Parcel2.writeNoException();
              return true;
            } 
            param1Parcel1.enforceInterface("android.os.ISecurityPermissionService");
            String str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            packagePermission = queryPackagePermissionsAsUser(str, param1Int1);
            param1Parcel2.writeNoException();
            if (packagePermission != null) {
              param1Parcel2.writeInt(1);
              packagePermission.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          } 
          packagePermission.enforceInterface("android.os.ISecurityPermissionService");
          String str4 = packagePermission.readString();
          String str3 = packagePermission.readString();
          param1Int1 = packagePermission.readInt();
          param1Int1 = queryPermissionAsUser(str4, str3, param1Int1);
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(param1Int1);
          return true;
        } 
        packagePermission.enforceInterface("android.os.ISecurityPermissionService");
        param1Int1 = packagePermission.readInt();
        long l = packagePermission.readLong();
        if (packagePermission.readInt() != 0) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        float f = packagePermission.readFloat();
        double d = packagePermission.readDouble();
        str1 = packagePermission.readString();
        basicTypes(param1Int1, l, bool1, f, d, str1);
        param1Parcel2.writeNoException();
        return true;
      } 
      str1.enforceInterface("android.os.ISecurityPermissionService");
      String str2 = str1.readString();
      param1Int1 = str1.readInt();
      param1Int2 = str1.readInt();
      boolean bool = checkOplusPermission(str2, param1Int1, param1Int2);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements ISecurityPermissionService {
      public static ISecurityPermissionService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.ISecurityPermissionService";
      }
      
      public boolean checkOplusPermission(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.ISecurityPermissionService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && ISecurityPermissionService.Stub.getDefaultImpl() != null) {
            bool1 = ISecurityPermissionService.Stub.getDefaultImpl().checkOplusPermission(param2String, param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void basicTypes(int param2Int, long param2Long, boolean param2Boolean, float param2Float, double param2Double, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.ISecurityPermissionService");
          try {
            parcel1.writeInt(param2Int);
            try {
              boolean bool;
              parcel1.writeLong(param2Long);
              if (param2Boolean) {
                bool = true;
              } else {
                bool = false;
              } 
              parcel1.writeInt(bool);
              try {
                parcel1.writeFloat(param2Float);
                parcel1.writeDouble(param2Double);
                parcel1.writeString(param2String);
                boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
                if (!bool1 && ISecurityPermissionService.Stub.getDefaultImpl() != null) {
                  ISecurityPermissionService.Stub.getDefaultImpl().basicTypes(param2Int, param2Long, param2Boolean, param2Float, param2Double, param2String);
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } 
                parcel2.readException();
                parcel2.recycle();
                parcel1.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public int queryPermissionAsUser(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.ISecurityPermissionService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ISecurityPermissionService.Stub.getDefaultImpl() != null) {
            param2Int = ISecurityPermissionService.Stub.getDefaultImpl().queryPermissionAsUser(param2String1, param2String2, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PackagePermission queryPackagePermissionsAsUser(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.ISecurityPermissionService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ISecurityPermissionService.Stub.getDefaultImpl() != null)
            return ISecurityPermissionService.Stub.getDefaultImpl().queryPackagePermissionsAsUser(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PackagePermission packagePermission = PackagePermission.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (PackagePermission)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateCachedPermission(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.ISecurityPermissionService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool1 && ISecurityPermissionService.Stub.getDefaultImpl() != null) {
            ISecurityPermissionService.Stub.getDefaultImpl().updateCachedPermission(param2String, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISecurityPermissionService param1ISecurityPermissionService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISecurityPermissionService != null) {
          Proxy.sDefaultImpl = param1ISecurityPermissionService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISecurityPermissionService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
