package android.os;

import android.content.Context;
import android.os.storage.StorageManager;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import libcore.io.IoUtils;

public class RevocableFileDescriptor {
  private static final boolean DEBUG = true;
  
  private static final String TAG = "RevocableFileDescriptor";
  
  public RevocableFileDescriptor(Context paramContext, File paramFile) throws IOException {
    try {
      init(paramContext, Os.open(paramFile.getAbsolutePath(), OsConstants.O_CREAT | OsConstants.O_RDWR, 448));
      return;
    } catch (ErrnoException errnoException) {
      throw errnoException.rethrowAsIOException();
    } 
  }
  
  public RevocableFileDescriptor(Context paramContext, FileDescriptor paramFileDescriptor) throws IOException {
    init(paramContext, paramFileDescriptor);
  }
  
  public void init(Context paramContext, FileDescriptor paramFileDescriptor) throws IOException {
    this.mInner = paramFileDescriptor;
    StorageManager storageManager = (StorageManager)paramContext.getSystemService(StorageManager.class);
    ProxyFileDescriptorCallback proxyFileDescriptorCallback = this.mCallback;
    this.mOuter = storageManager.openProxyFileDescriptor(805306368, proxyFileDescriptorCallback);
  }
  
  public ParcelFileDescriptor getRevocableFileDescriptor() {
    return this.mOuter;
  }
  
  public void revoke() {
    this.mRevoked = true;
    IoUtils.closeQuietly(this.mInner);
  }
  
  public void addOnCloseListener(ParcelFileDescriptor.OnCloseListener paramOnCloseListener) {
    this.mOnCloseListener = paramOnCloseListener;
  }
  
  public boolean isRevoked() {
    return this.mRevoked;
  }
  
  private final ProxyFileDescriptorCallback mCallback = (ProxyFileDescriptorCallback)new Object(this);
  
  private FileDescriptor mInner;
  
  private ParcelFileDescriptor.OnCloseListener mOnCloseListener;
  
  private ParcelFileDescriptor mOuter;
  
  private volatile boolean mRevoked;
  
  public RevocableFileDescriptor() {}
}
