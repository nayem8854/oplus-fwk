package android.os;

import android.content.Context;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class HardwarePropertiesManager {
  public static final int DEVICE_TEMPERATURE_BATTERY = 2;
  
  public static final int DEVICE_TEMPERATURE_CPU = 0;
  
  public static final int DEVICE_TEMPERATURE_GPU = 1;
  
  public static final int DEVICE_TEMPERATURE_SKIN = 3;
  
  private static final String TAG = HardwarePropertiesManager.class.getSimpleName();
  
  public static final int TEMPERATURE_CURRENT = 0;
  
  public static final int TEMPERATURE_SHUTDOWN = 2;
  
  public static final int TEMPERATURE_THROTTLING = 1;
  
  public static final int TEMPERATURE_THROTTLING_BELOW_VR_MIN = 3;
  
  public static final float UNDEFINED_TEMPERATURE = -3.4028235E38F;
  
  private final Context mContext;
  
  private final IHardwarePropertiesManager mService;
  
  public HardwarePropertiesManager(Context paramContext, IHardwarePropertiesManager paramIHardwarePropertiesManager) {
    this.mContext = paramContext;
    this.mService = paramIHardwarePropertiesManager;
  }
  
  public float[] getDeviceTemperatures(int paramInt1, int paramInt2) {
    if (paramInt1 != 0 && paramInt1 != 1 && paramInt1 != 2 && paramInt1 != 3) {
      Log.w(TAG, "Unknown device temperature type.");
      return new float[0];
    } 
    if (paramInt2 != 0 && paramInt2 != 1 && paramInt2 != 2 && paramInt2 != 3) {
      Log.w(TAG, "Unknown device temperature source.");
      return new float[0];
    } 
    try {
      return this.mService.getDeviceTemperatures(this.mContext.getOpPackageName(), paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public CpuUsageInfo[] getCpuUsages() {
    try {
      return this.mService.getCpuUsages(this.mContext.getOpPackageName());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public float[] getFanSpeeds() {
    try {
      return this.mService.getFanSpeeds(this.mContext.getOpPackageName());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DeviceTemperatureType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface TemperatureSource {}
}
