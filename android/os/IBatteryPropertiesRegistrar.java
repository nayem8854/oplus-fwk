package android.os;

public interface IBatteryPropertiesRegistrar extends IInterface {
  float getBeginDecimal() throws RemoteException;
  
  float getEndDecimal() throws RemoteException;
  
  int getProperty(int paramInt, BatteryProperty paramBatteryProperty) throws RemoteException;
  
  int getPsyBatteryHmac() throws RemoteException;
  
  int getPsyFastChgType() throws RemoteException;
  
  int getPsyOtgOnline() throws RemoteException;
  
  void scheduleUpdate() throws RemoteException;
  
  class Default implements IBatteryPropertiesRegistrar {
    public int getProperty(int param1Int, BatteryProperty param1BatteryProperty) throws RemoteException {
      return 0;
    }
    
    public void scheduleUpdate() throws RemoteException {}
    
    public int getPsyOtgOnline() throws RemoteException {
      return 0;
    }
    
    public int getPsyBatteryHmac() throws RemoteException {
      return 0;
    }
    
    public int getPsyFastChgType() throws RemoteException {
      return 0;
    }
    
    public float getBeginDecimal() throws RemoteException {
      return 0.0F;
    }
    
    public float getEndDecimal() throws RemoteException {
      return 0.0F;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBatteryPropertiesRegistrar {
    private static final String DESCRIPTOR = "android.os.IBatteryPropertiesRegistrar";
    
    static final int TRANSACTION_getBeginDecimal = 6;
    
    static final int TRANSACTION_getEndDecimal = 7;
    
    static final int TRANSACTION_getProperty = 1;
    
    static final int TRANSACTION_getPsyBatteryHmac = 4;
    
    static final int TRANSACTION_getPsyFastChgType = 5;
    
    static final int TRANSACTION_getPsyOtgOnline = 3;
    
    static final int TRANSACTION_scheduleUpdate = 2;
    
    public Stub() {
      attachInterface(this, "android.os.IBatteryPropertiesRegistrar");
    }
    
    public static IBatteryPropertiesRegistrar asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IBatteryPropertiesRegistrar");
      if (iInterface != null && iInterface instanceof IBatteryPropertiesRegistrar)
        return (IBatteryPropertiesRegistrar)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "getEndDecimal";
        case 6:
          return "getBeginDecimal";
        case 5:
          return "getPsyFastChgType";
        case 4:
          return "getPsyBatteryHmac";
        case 3:
          return "getPsyOtgOnline";
        case 2:
          return "scheduleUpdate";
        case 1:
          break;
      } 
      return "getProperty";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        float f;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.os.IBatteryPropertiesRegistrar");
            f = getEndDecimal();
            param1Parcel2.writeNoException();
            param1Parcel2.writeFloat(f);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.os.IBatteryPropertiesRegistrar");
            f = getBeginDecimal();
            param1Parcel2.writeNoException();
            param1Parcel2.writeFloat(f);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.os.IBatteryPropertiesRegistrar");
            param1Int1 = getPsyFastChgType();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.os.IBatteryPropertiesRegistrar");
            param1Int1 = getPsyBatteryHmac();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.os.IBatteryPropertiesRegistrar");
            param1Int1 = getPsyOtgOnline();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.os.IBatteryPropertiesRegistrar");
            scheduleUpdate();
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.os.IBatteryPropertiesRegistrar");
        param1Int1 = param1Parcel1.readInt();
        BatteryProperty batteryProperty = new BatteryProperty();
        param1Int1 = getProperty(param1Int1, batteryProperty);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(param1Int1);
        param1Parcel2.writeInt(1);
        batteryProperty.writeToParcel(param1Parcel2, 1);
        return true;
      } 
      param1Parcel2.writeString("android.os.IBatteryPropertiesRegistrar");
      return true;
    }
    
    private static class Proxy implements IBatteryPropertiesRegistrar {
      public static IBatteryPropertiesRegistrar sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IBatteryPropertiesRegistrar";
      }
      
      public int getProperty(int param2Int, BatteryProperty param2BatteryProperty) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IBatteryPropertiesRegistrar");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IBatteryPropertiesRegistrar.Stub.getDefaultImpl() != null) {
            param2Int = IBatteryPropertiesRegistrar.Stub.getDefaultImpl().getProperty(param2Int, param2BatteryProperty);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (parcel2.readInt() != 0)
            param2BatteryProperty.readFromParcel(parcel2); 
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void scheduleUpdate() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IBatteryPropertiesRegistrar");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IBatteryPropertiesRegistrar.Stub.getDefaultImpl() != null) {
            IBatteryPropertiesRegistrar.Stub.getDefaultImpl().scheduleUpdate();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public int getPsyOtgOnline() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IBatteryPropertiesRegistrar");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IBatteryPropertiesRegistrar.Stub.getDefaultImpl() != null)
            return IBatteryPropertiesRegistrar.Stub.getDefaultImpl().getPsyOtgOnline(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPsyBatteryHmac() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IBatteryPropertiesRegistrar");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IBatteryPropertiesRegistrar.Stub.getDefaultImpl() != null)
            return IBatteryPropertiesRegistrar.Stub.getDefaultImpl().getPsyBatteryHmac(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPsyFastChgType() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IBatteryPropertiesRegistrar");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IBatteryPropertiesRegistrar.Stub.getDefaultImpl() != null)
            return IBatteryPropertiesRegistrar.Stub.getDefaultImpl().getPsyFastChgType(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public float getBeginDecimal() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IBatteryPropertiesRegistrar");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IBatteryPropertiesRegistrar.Stub.getDefaultImpl() != null)
            return IBatteryPropertiesRegistrar.Stub.getDefaultImpl().getBeginDecimal(); 
          parcel2.readException();
          return parcel2.readFloat();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public float getEndDecimal() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IBatteryPropertiesRegistrar");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IBatteryPropertiesRegistrar.Stub.getDefaultImpl() != null)
            return IBatteryPropertiesRegistrar.Stub.getDefaultImpl().getEndDecimal(); 
          parcel2.readException();
          return parcel2.readFloat();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBatteryPropertiesRegistrar param1IBatteryPropertiesRegistrar) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBatteryPropertiesRegistrar != null) {
          Proxy.sDefaultImpl = param1IBatteryPropertiesRegistrar;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBatteryPropertiesRegistrar getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
