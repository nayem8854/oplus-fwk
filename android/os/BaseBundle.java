package android.os;

import android.util.ArrayMap;
import android.util.Log;
import android.util.SparseArray;
import com.android.internal.util.IndentingPrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;

public class BaseBundle {
  private static final int BUNDLE_MAGIC = 1279544898;
  
  private static final int BUNDLE_MAGIC_NATIVE = 1279544900;
  
  static final boolean DEBUG = false;
  
  static final int FLAG_DEFUSABLE = 1;
  
  private static final boolean LOG_DEFUSABLE = false;
  
  private static final String TAG = "Bundle";
  
  private static volatile boolean sShouldDefuse = false;
  
  private ClassLoader mClassLoader;
  
  public int mFlags;
  
  ArrayMap<String, Object> mMap;
  
  private boolean mParcelledByNative;
  
  Parcel mParcelledData;
  
  public static void setShouldDefuse(boolean paramBoolean) {
    sShouldDefuse = paramBoolean;
  }
  
  static final class NoImagePreloadHolder {
    public static final Parcel EMPTY_PARCEL = Parcel.obtain();
  }
  
  BaseBundle(ClassLoader paramClassLoader, int paramInt) {
    ArrayMap<String, Object> arrayMap;
    this.mMap = null;
    this.mParcelledData = null;
    if (paramInt > 0) {
      arrayMap = new ArrayMap(paramInt);
    } else {
      arrayMap = new ArrayMap();
    } 
    this.mMap = arrayMap;
    if (paramClassLoader == null)
      paramClassLoader = getClass().getClassLoader(); 
    this.mClassLoader = paramClassLoader;
  }
  
  BaseBundle() {
    this((ClassLoader)null, 0);
  }
  
  BaseBundle(Parcel paramParcel) {
    this.mMap = null;
    this.mParcelledData = null;
    readFromParcelInner(paramParcel);
  }
  
  BaseBundle(Parcel paramParcel, int paramInt) {
    this.mMap = null;
    this.mParcelledData = null;
    readFromParcelInner(paramParcel, paramInt);
  }
  
  BaseBundle(ClassLoader paramClassLoader) {
    this(paramClassLoader, 0);
  }
  
  BaseBundle(int paramInt) {
    this((ClassLoader)null, paramInt);
  }
  
  BaseBundle(BaseBundle paramBaseBundle) {
    this.mMap = null;
    this.mParcelledData = null;
    copyInternal(paramBaseBundle, false);
  }
  
  BaseBundle(boolean paramBoolean) {
    this.mMap = null;
    this.mParcelledData = null;
  }
  
  public String getPairValue() {
    unparcel();
    int i = this.mMap.size();
    if (i > 1)
      Log.w("Bundle", "getPairValue() used on Bundle with multiple pairs."); 
    if (i == 0)
      return null; 
    Object object = this.mMap.valueAt(0);
    try {
      return (String)object;
    } catch (ClassCastException classCastException) {
      typeWarning("getPairValue()", object, "String", classCastException);
      return null;
    } 
  }
  
  void setClassLoader(ClassLoader paramClassLoader) {
    this.mClassLoader = paramClassLoader;
  }
  
  ClassLoader getClassLoader() {
    return this.mClassLoader;
  }
  
  void unparcel() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mParcelledData : Landroid/os/Parcel;
    //   6: astore_1
    //   7: aload_1
    //   8: ifnull -> 21
    //   11: aload_0
    //   12: aload_1
    //   13: iconst_1
    //   14: aload_0
    //   15: getfield mParcelledByNative : Z
    //   18: invokespecial initializeFromParcelLocked : (Landroid/os/Parcel;ZZ)V
    //   21: aload_0
    //   22: monitorexit
    //   23: return
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #233	-> 0
    //   #234	-> 2
    //   #235	-> 7
    //   #236	-> 11
    //   #244	-> 21
    //   #245	-> 23
    //   #244	-> 24
    // Exception table:
    //   from	to	target	type
    //   2	7	24	finally
    //   11	21	24	finally
    //   21	23	24	finally
    //   25	27	24	finally
  }
  
  private void initializeFromParcelLocked(Parcel paramParcel, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic isEmptyParcel : (Landroid/os/Parcel;)Z
    //   4: ifeq -> 46
    //   7: aload_0
    //   8: getfield mMap : Landroid/util/ArrayMap;
    //   11: astore_1
    //   12: aload_1
    //   13: ifnonnull -> 31
    //   16: aload_0
    //   17: new android/util/ArrayMap
    //   20: dup
    //   21: iconst_1
    //   22: invokespecial <init> : (I)V
    //   25: putfield mMap : Landroid/util/ArrayMap;
    //   28: goto -> 35
    //   31: aload_1
    //   32: invokevirtual erase : ()V
    //   35: aload_0
    //   36: aconst_null
    //   37: putfield mParcelledData : Landroid/os/Parcel;
    //   40: aload_0
    //   41: iconst_0
    //   42: putfield mParcelledByNative : Z
    //   45: return
    //   46: aload_1
    //   47: invokevirtual readInt : ()I
    //   50: istore #4
    //   52: iload #4
    //   54: ifge -> 58
    //   57: return
    //   58: aload_0
    //   59: getfield mMap : Landroid/util/ArrayMap;
    //   62: astore #5
    //   64: aload #5
    //   66: ifnonnull -> 83
    //   69: new android/util/ArrayMap
    //   72: dup
    //   73: iload #4
    //   75: invokespecial <init> : (I)V
    //   78: astore #5
    //   80: goto -> 95
    //   83: aload #5
    //   85: invokevirtual erase : ()V
    //   88: aload #5
    //   90: iload #4
    //   92: invokevirtual ensureCapacity : (I)V
    //   95: iload_3
    //   96: ifeq -> 114
    //   99: aload_1
    //   100: aload #5
    //   102: iload #4
    //   104: aload_0
    //   105: getfield mClassLoader : Ljava/lang/ClassLoader;
    //   108: invokevirtual readArrayMapSafelyInternal : (Landroid/util/ArrayMap;ILjava/lang/ClassLoader;)V
    //   111: goto -> 126
    //   114: aload_1
    //   115: aload #5
    //   117: iload #4
    //   119: aload_0
    //   120: getfield mClassLoader : Ljava/lang/ClassLoader;
    //   123: invokevirtual readArrayMapInternal : (Landroid/util/ArrayMap;ILjava/lang/ClassLoader;)V
    //   126: aload_0
    //   127: aload #5
    //   129: putfield mMap : Landroid/util/ArrayMap;
    //   132: iload_2
    //   133: ifeq -> 140
    //   136: aload_1
    //   137: invokestatic recycleParcel : (Landroid/os/Parcel;)V
    //   140: aload_0
    //   141: aconst_null
    //   142: putfield mParcelledData : Landroid/os/Parcel;
    //   145: aload_0
    //   146: iconst_0
    //   147: putfield mParcelledByNative : Z
    //   150: goto -> 194
    //   153: astore #6
    //   155: goto -> 198
    //   158: astore #6
    //   160: getstatic android/os/BaseBundle.sShouldDefuse : Z
    //   163: ifeq -> 195
    //   166: ldc 'Bundle'
    //   168: ldc 'Failed to parse Bundle, but defusing quietly'
    //   170: aload #6
    //   172: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   175: pop
    //   176: aload #5
    //   178: invokevirtual erase : ()V
    //   181: aload_0
    //   182: aload #5
    //   184: putfield mMap : Landroid/util/ArrayMap;
    //   187: iload_2
    //   188: ifeq -> 140
    //   191: goto -> 136
    //   194: return
    //   195: aload #6
    //   197: athrow
    //   198: aload_0
    //   199: aload #5
    //   201: putfield mMap : Landroid/util/ArrayMap;
    //   204: iload_2
    //   205: ifeq -> 212
    //   208: aload_1
    //   209: invokestatic recycleParcel : (Landroid/os/Parcel;)V
    //   212: aload_0
    //   213: aconst_null
    //   214: putfield mParcelledData : Landroid/os/Parcel;
    //   217: aload_0
    //   218: iconst_0
    //   219: putfield mParcelledByNative : Z
    //   222: aload #6
    //   224: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #254	-> 0
    //   #259	-> 7
    //   #260	-> 16
    //   #262	-> 31
    //   #264	-> 35
    //   #265	-> 40
    //   #266	-> 45
    //   #269	-> 46
    //   #274	-> 52
    //   #275	-> 57
    //   #277	-> 58
    //   #278	-> 64
    //   #279	-> 69
    //   #281	-> 83
    //   #282	-> 88
    //   #285	-> 95
    //   #288	-> 99
    //   #292	-> 114
    //   #302	-> 126
    //   #303	-> 132
    //   #304	-> 136
    //   #306	-> 140
    //   #307	-> 145
    //   #308	-> 150
    //   #302	-> 153
    //   #294	-> 158
    //   #295	-> 160
    //   #296	-> 166
    //   #297	-> 176
    //   #302	-> 181
    //   #303	-> 187
    //   #304	-> 191
    //   #313	-> 194
    //   #299	-> 195
    //   #302	-> 198
    //   #303	-> 204
    //   #304	-> 208
    //   #306	-> 212
    //   #307	-> 217
    //   #308	-> 222
    // Exception table:
    //   from	to	target	type
    //   99	111	158	android/os/BadParcelableException
    //   99	111	153	finally
    //   114	126	158	android/os/BadParcelableException
    //   114	126	153	finally
    //   160	166	153	finally
    //   166	176	153	finally
    //   176	181	153	finally
    //   195	198	153	finally
  }
  
  public boolean isParcelled() {
    boolean bool;
    if (this.mParcelledData != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isEmptyParcel() {
    return isEmptyParcel(this.mParcelledData);
  }
  
  private static boolean isEmptyParcel(Parcel paramParcel) {
    boolean bool;
    if (paramParcel == NoImagePreloadHolder.EMPTY_PARCEL) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static void recycleParcel(Parcel paramParcel) {
    if (paramParcel != null && !isEmptyParcel(paramParcel))
      paramParcel.recycle(); 
  }
  
  ArrayMap<String, Object> getMap() {
    unparcel();
    return this.mMap;
  }
  
  public int size() {
    unparcel();
    return this.mMap.size();
  }
  
  public boolean isEmpty() {
    unparcel();
    return this.mMap.isEmpty();
  }
  
  public boolean isDefinitelyEmpty() {
    if (isParcelled())
      return isEmptyParcel(); 
    return isEmpty();
  }
  
  public static boolean kindofEquals(BaseBundle paramBaseBundle1, BaseBundle paramBaseBundle2) {
    return (paramBaseBundle1 == paramBaseBundle2 || (paramBaseBundle1 != null && paramBaseBundle1.kindofEquals(paramBaseBundle2)));
  }
  
  public boolean kindofEquals(BaseBundle paramBaseBundle) {
    boolean bool = false;
    if (paramBaseBundle == null)
      return false; 
    if (isDefinitelyEmpty() && paramBaseBundle.isDefinitelyEmpty())
      return true; 
    if (isParcelled() != paramBaseBundle.isParcelled())
      return false; 
    if (isParcelled()) {
      if (this.mParcelledData.compareData(paramBaseBundle.mParcelledData) == 0)
        bool = true; 
      return bool;
    } 
    return this.mMap.equals(paramBaseBundle.mMap);
  }
  
  public void clear() {
    unparcel();
    this.mMap.clear();
  }
  
  void copyInternal(BaseBundle paramBaseBundle, boolean paramBoolean) {
    // Byte code:
    //   0: aload_1
    //   1: monitorenter
    //   2: aload_1
    //   3: getfield mParcelledData : Landroid/os/Parcel;
    //   6: ifnull -> 87
    //   9: aload_1
    //   10: invokevirtual isEmptyParcel : ()Z
    //   13: ifeq -> 31
    //   16: aload_0
    //   17: getstatic android/os/BaseBundle$NoImagePreloadHolder.EMPTY_PARCEL : Landroid/os/Parcel;
    //   20: putfield mParcelledData : Landroid/os/Parcel;
    //   23: aload_0
    //   24: iconst_0
    //   25: putfield mParcelledByNative : Z
    //   28: goto -> 97
    //   31: invokestatic obtain : ()Landroid/os/Parcel;
    //   34: astore_3
    //   35: aload_0
    //   36: aload_3
    //   37: putfield mParcelledData : Landroid/os/Parcel;
    //   40: aload_1
    //   41: getfield mParcelledData : Landroid/os/Parcel;
    //   44: astore #4
    //   46: aload_1
    //   47: getfield mParcelledData : Landroid/os/Parcel;
    //   50: astore #5
    //   52: aload #5
    //   54: invokevirtual dataSize : ()I
    //   57: istore #6
    //   59: aload_3
    //   60: aload #4
    //   62: iconst_0
    //   63: iload #6
    //   65: invokevirtual appendFrom : (Landroid/os/Parcel;II)V
    //   68: aload_0
    //   69: getfield mParcelledData : Landroid/os/Parcel;
    //   72: iconst_0
    //   73: invokevirtual setDataPosition : (I)V
    //   76: aload_0
    //   77: aload_1
    //   78: getfield mParcelledByNative : Z
    //   81: putfield mParcelledByNative : Z
    //   84: goto -> 97
    //   87: aload_0
    //   88: aconst_null
    //   89: putfield mParcelledData : Landroid/os/Parcel;
    //   92: aload_0
    //   93: iconst_0
    //   94: putfield mParcelledByNative : Z
    //   97: aload_1
    //   98: getfield mMap : Landroid/util/ArrayMap;
    //   101: ifnull -> 203
    //   104: iload_2
    //   105: ifne -> 128
    //   108: new android/util/ArrayMap
    //   111: astore_3
    //   112: aload_3
    //   113: aload_1
    //   114: getfield mMap : Landroid/util/ArrayMap;
    //   117: invokespecial <init> : (Landroid/util/ArrayMap;)V
    //   120: aload_0
    //   121: aload_3
    //   122: putfield mMap : Landroid/util/ArrayMap;
    //   125: goto -> 208
    //   128: aload_1
    //   129: getfield mMap : Landroid/util/ArrayMap;
    //   132: astore #4
    //   134: aload #4
    //   136: invokevirtual size : ()I
    //   139: istore #7
    //   141: new android/util/ArrayMap
    //   144: astore_3
    //   145: aload_3
    //   146: iload #7
    //   148: invokespecial <init> : (I)V
    //   151: aload_0
    //   152: aload_3
    //   153: putfield mMap : Landroid/util/ArrayMap;
    //   156: iconst_0
    //   157: istore #6
    //   159: iload #6
    //   161: iload #7
    //   163: if_icmpge -> 200
    //   166: aload_0
    //   167: getfield mMap : Landroid/util/ArrayMap;
    //   170: aload #4
    //   172: iload #6
    //   174: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   177: checkcast java/lang/String
    //   180: aload_0
    //   181: aload #4
    //   183: iload #6
    //   185: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   188: invokevirtual deepCopyValue : (Ljava/lang/Object;)Ljava/lang/Object;
    //   191: invokevirtual append : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   194: iinc #6, 1
    //   197: goto -> 159
    //   200: goto -> 208
    //   203: aload_0
    //   204: aconst_null
    //   205: putfield mMap : Landroid/util/ArrayMap;
    //   208: aload_0
    //   209: aload_1
    //   210: getfield mClassLoader : Ljava/lang/ClassLoader;
    //   213: putfield mClassLoader : Ljava/lang/ClassLoader;
    //   216: aload_1
    //   217: monitorexit
    //   218: return
    //   219: astore_3
    //   220: aload_1
    //   221: monitorexit
    //   222: aload_3
    //   223: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #431	-> 0
    //   #432	-> 2
    //   #433	-> 9
    //   #434	-> 16
    //   #435	-> 23
    //   #437	-> 31
    //   #438	-> 40
    //   #439	-> 52
    //   #438	-> 59
    //   #440	-> 68
    //   #441	-> 76
    //   #444	-> 87
    //   #445	-> 92
    //   #448	-> 97
    //   #449	-> 104
    //   #450	-> 108
    //   #452	-> 128
    //   #453	-> 134
    //   #454	-> 141
    //   #455	-> 156
    //   #456	-> 166
    //   #455	-> 194
    //   #458	-> 200
    //   #460	-> 203
    //   #463	-> 208
    //   #464	-> 216
    //   #465	-> 218
    //   #464	-> 219
    // Exception table:
    //   from	to	target	type
    //   2	9	219	finally
    //   9	16	219	finally
    //   16	23	219	finally
    //   23	28	219	finally
    //   31	40	219	finally
    //   40	52	219	finally
    //   52	59	219	finally
    //   59	68	219	finally
    //   68	76	219	finally
    //   76	84	219	finally
    //   87	92	219	finally
    //   92	97	219	finally
    //   97	104	219	finally
    //   108	125	219	finally
    //   128	134	219	finally
    //   134	141	219	finally
    //   141	156	219	finally
    //   166	194	219	finally
    //   203	208	219	finally
    //   208	216	219	finally
    //   216	218	219	finally
    //   220	222	219	finally
  }
  
  Object deepCopyValue(Object paramObject) {
    if (paramObject == null)
      return null; 
    if (paramObject instanceof Bundle)
      return ((Bundle)paramObject).deepCopy(); 
    if (paramObject instanceof PersistableBundle)
      return ((PersistableBundle)paramObject).deepCopy(); 
    if (paramObject instanceof ArrayList)
      return deepcopyArrayList((ArrayList)paramObject); 
    if (paramObject.getClass().isArray()) {
      if (paramObject instanceof int[])
        return ((int[])paramObject).clone(); 
      if (paramObject instanceof long[])
        return ((long[])paramObject).clone(); 
      if (paramObject instanceof float[])
        return ((float[])paramObject).clone(); 
      if (paramObject instanceof double[])
        return ((double[])paramObject).clone(); 
      if (paramObject instanceof Object[])
        return ((Object[])paramObject).clone(); 
      if (paramObject instanceof byte[])
        return ((byte[])paramObject).clone(); 
      if (paramObject instanceof short[])
        return ((short[])paramObject).clone(); 
      if (paramObject instanceof char[])
        return ((char[])paramObject).clone(); 
    } 
    return paramObject;
  }
  
  ArrayList deepcopyArrayList(ArrayList paramArrayList) {
    int i = paramArrayList.size();
    ArrayList<Object> arrayList = new ArrayList(i);
    for (byte b = 0; b < i; b++)
      arrayList.add(deepCopyValue(paramArrayList.get(b))); 
    return arrayList;
  }
  
  public boolean containsKey(String paramString) {
    unparcel();
    return this.mMap.containsKey(paramString);
  }
  
  public Object get(String paramString) {
    unparcel();
    return this.mMap.get(paramString);
  }
  
  public void remove(String paramString) {
    unparcel();
    this.mMap.remove(paramString);
  }
  
  public void putAll(PersistableBundle paramPersistableBundle) {
    unparcel();
    paramPersistableBundle.unparcel();
    this.mMap.putAll(paramPersistableBundle.mMap);
  }
  
  void putAll(ArrayMap paramArrayMap) {
    unparcel();
    this.mMap.putAll(paramArrayMap);
  }
  
  public Set<String> keySet() {
    unparcel();
    return this.mMap.keySet();
  }
  
  public void putObject(String paramString, Object paramObject) {
    if (paramObject == null) {
      putString(paramString, null);
    } else if (paramObject instanceof Boolean) {
      putBoolean(paramString, ((Boolean)paramObject).booleanValue());
    } else if (paramObject instanceof Integer) {
      putInt(paramString, ((Integer)paramObject).intValue());
    } else if (paramObject instanceof Long) {
      putLong(paramString, ((Long)paramObject).longValue());
    } else if (paramObject instanceof Double) {
      putDouble(paramString, ((Double)paramObject).doubleValue());
    } else if (paramObject instanceof String) {
      putString(paramString, (String)paramObject);
    } else if (paramObject instanceof boolean[]) {
      putBooleanArray(paramString, (boolean[])paramObject);
    } else if (paramObject instanceof int[]) {
      putIntArray(paramString, (int[])paramObject);
    } else if (paramObject instanceof long[]) {
      putLongArray(paramString, (long[])paramObject);
    } else if (paramObject instanceof double[]) {
      putDoubleArray(paramString, (double[])paramObject);
    } else {
      if (paramObject instanceof String[]) {
        putStringArray(paramString, (String[])paramObject);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported type ");
      stringBuilder.append(paramObject.getClass());
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
  }
  
  public void putBoolean(String paramString, boolean paramBoolean) {
    unparcel();
    this.mMap.put(paramString, Boolean.valueOf(paramBoolean));
  }
  
  void putByte(String paramString, byte paramByte) {
    unparcel();
    this.mMap.put(paramString, Byte.valueOf(paramByte));
  }
  
  void putChar(String paramString, char paramChar) {
    unparcel();
    this.mMap.put(paramString, Character.valueOf(paramChar));
  }
  
  void putShort(String paramString, short paramShort) {
    unparcel();
    this.mMap.put(paramString, Short.valueOf(paramShort));
  }
  
  public void putInt(String paramString, int paramInt) {
    unparcel();
    this.mMap.put(paramString, Integer.valueOf(paramInt));
  }
  
  public void putLong(String paramString, long paramLong) {
    unparcel();
    this.mMap.put(paramString, Long.valueOf(paramLong));
  }
  
  void putFloat(String paramString, float paramFloat) {
    unparcel();
    this.mMap.put(paramString, Float.valueOf(paramFloat));
  }
  
  public void putDouble(String paramString, double paramDouble) {
    unparcel();
    this.mMap.put(paramString, Double.valueOf(paramDouble));
  }
  
  public void putString(String paramString1, String paramString2) {
    unparcel();
    this.mMap.put(paramString1, paramString2);
  }
  
  void putCharSequence(String paramString, CharSequence paramCharSequence) {
    unparcel();
    this.mMap.put(paramString, paramCharSequence);
  }
  
  void putIntegerArrayList(String paramString, ArrayList<Integer> paramArrayList) {
    unparcel();
    this.mMap.put(paramString, paramArrayList);
  }
  
  void putStringArrayList(String paramString, ArrayList<String> paramArrayList) {
    unparcel();
    this.mMap.put(paramString, paramArrayList);
  }
  
  void putCharSequenceArrayList(String paramString, ArrayList<CharSequence> paramArrayList) {
    unparcel();
    this.mMap.put(paramString, paramArrayList);
  }
  
  void putSerializable(String paramString, Serializable paramSerializable) {
    unparcel();
    this.mMap.put(paramString, paramSerializable);
  }
  
  public void putBooleanArray(String paramString, boolean[] paramArrayOfboolean) {
    unparcel();
    this.mMap.put(paramString, paramArrayOfboolean);
  }
  
  void putByteArray(String paramString, byte[] paramArrayOfbyte) {
    unparcel();
    this.mMap.put(paramString, paramArrayOfbyte);
  }
  
  void putShortArray(String paramString, short[] paramArrayOfshort) {
    unparcel();
    this.mMap.put(paramString, paramArrayOfshort);
  }
  
  void putCharArray(String paramString, char[] paramArrayOfchar) {
    unparcel();
    this.mMap.put(paramString, paramArrayOfchar);
  }
  
  public void putIntArray(String paramString, int[] paramArrayOfint) {
    unparcel();
    this.mMap.put(paramString, paramArrayOfint);
  }
  
  public void putLongArray(String paramString, long[] paramArrayOflong) {
    unparcel();
    this.mMap.put(paramString, paramArrayOflong);
  }
  
  void putFloatArray(String paramString, float[] paramArrayOffloat) {
    unparcel();
    this.mMap.put(paramString, paramArrayOffloat);
  }
  
  public void putDoubleArray(String paramString, double[] paramArrayOfdouble) {
    unparcel();
    this.mMap.put(paramString, paramArrayOfdouble);
  }
  
  public void putStringArray(String paramString, String[] paramArrayOfString) {
    unparcel();
    this.mMap.put(paramString, paramArrayOfString);
  }
  
  void putCharSequenceArray(String paramString, CharSequence[] paramArrayOfCharSequence) {
    unparcel();
    this.mMap.put(paramString, paramArrayOfCharSequence);
  }
  
  public boolean getBoolean(String paramString) {
    unparcel();
    return getBoolean(paramString, false);
  }
  
  void typeWarning(String paramString1, Object paramObject1, String paramString2, Object paramObject2, ClassCastException paramClassCastException) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Key ");
    stringBuilder.append(paramString1);
    stringBuilder.append(" expected ");
    stringBuilder.append(paramString2);
    stringBuilder.append(" but value was a ");
    stringBuilder.append(paramObject1.getClass().getName());
    stringBuilder.append(".  The default value ");
    stringBuilder.append(paramObject2);
    stringBuilder.append(" was returned.");
    Log.w("Bundle", stringBuilder.toString());
    Log.w("Bundle", "Attempt to cast generated internal exception:", paramClassCastException);
  }
  
  void typeWarning(String paramString1, Object paramObject, String paramString2, ClassCastException paramClassCastException) {
    typeWarning(paramString1, paramObject, paramString2, "<null>", paramClassCastException);
  }
  
  public boolean getBoolean(String paramString, boolean paramBoolean) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return paramBoolean; 
    try {
      return ((Boolean)object).booleanValue();
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Boolean", Boolean.valueOf(paramBoolean), classCastException);
      return paramBoolean;
    } 
  }
  
  byte getByte(String paramString) {
    unparcel();
    return getByte(paramString, (byte)0).byteValue();
  }
  
  Byte getByte(String paramString, byte paramByte) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return Byte.valueOf(paramByte); 
    try {
      return (Byte)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Byte", Byte.valueOf(paramByte), classCastException);
      return Byte.valueOf(paramByte);
    } 
  }
  
  char getChar(String paramString) {
    unparcel();
    return getChar(paramString, false);
  }
  
  char getChar(String paramString, char paramChar) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return paramChar; 
    try {
      return ((Character)object).charValue();
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Character", Character.valueOf(paramChar), classCastException);
      return paramChar;
    } 
  }
  
  short getShort(String paramString) {
    unparcel();
    return getShort(paramString, (short)0);
  }
  
  short getShort(String paramString, short paramShort) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return paramShort; 
    try {
      return ((Short)object).shortValue();
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Short", Short.valueOf(paramShort), classCastException);
      return paramShort;
    } 
  }
  
  public int getInt(String paramString) {
    unparcel();
    return getInt(paramString, 0);
  }
  
  public int getInt(String paramString, int paramInt) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return paramInt; 
    try {
      return ((Integer)object).intValue();
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Integer", Integer.valueOf(paramInt), classCastException);
      return paramInt;
    } 
  }
  
  public long getLong(String paramString) {
    unparcel();
    return getLong(paramString, 0L);
  }
  
  public long getLong(String paramString, long paramLong) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return paramLong; 
    try {
      return ((Long)object).longValue();
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Long", Long.valueOf(paramLong), classCastException);
      return paramLong;
    } 
  }
  
  float getFloat(String paramString) {
    unparcel();
    return getFloat(paramString, 0.0F);
  }
  
  float getFloat(String paramString, float paramFloat) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return paramFloat; 
    try {
      return ((Float)object).floatValue();
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Float", Float.valueOf(paramFloat), classCastException);
      return paramFloat;
    } 
  }
  
  public double getDouble(String paramString) {
    unparcel();
    return getDouble(paramString, 0.0D);
  }
  
  public double getDouble(String paramString, double paramDouble) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return paramDouble; 
    try {
      return ((Double)object).doubleValue();
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Double", Double.valueOf(paramDouble), classCastException);
      return paramDouble;
    } 
  }
  
  public String getString(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    try {
      return (String)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "String", classCastException);
      return null;
    } 
  }
  
  public String getString(String paramString1, String paramString2) {
    paramString1 = getString(paramString1);
    if (paramString1 == null)
      paramString1 = paramString2; 
    return paramString1;
  }
  
  CharSequence getCharSequence(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    try {
      return (CharSequence)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "CharSequence", classCastException);
      return null;
    } 
  }
  
  CharSequence getCharSequence(String paramString, CharSequence paramCharSequence) {
    CharSequence charSequence = getCharSequence(paramString);
    if (charSequence == null)
      charSequence = paramCharSequence; 
    return charSequence;
  }
  
  Serializable getSerializable(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (Serializable)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "Serializable", classCastException);
      return null;
    } 
  }
  
  ArrayList<Integer> getIntegerArrayList(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (ArrayList)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "ArrayList<Integer>", classCastException);
      return null;
    } 
  }
  
  ArrayList<String> getStringArrayList(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (ArrayList)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "ArrayList<String>", classCastException);
      return null;
    } 
  }
  
  ArrayList<CharSequence> getCharSequenceArrayList(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (ArrayList)object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "ArrayList<CharSequence>", classCastException);
      return null;
    } 
  }
  
  public boolean[] getBooleanArray(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (boolean[])object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "byte[]", classCastException);
      return null;
    } 
  }
  
  byte[] getByteArray(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (byte[])object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "byte[]", classCastException);
      return null;
    } 
  }
  
  short[] getShortArray(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (short[])object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "short[]", classCastException);
      return null;
    } 
  }
  
  char[] getCharArray(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (char[])object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "char[]", classCastException);
      return null;
    } 
  }
  
  public int[] getIntArray(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (int[])object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "int[]", classCastException);
      return null;
    } 
  }
  
  public long[] getLongArray(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (long[])object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "long[]", classCastException);
      return null;
    } 
  }
  
  float[] getFloatArray(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (float[])object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "float[]", classCastException);
      return null;
    } 
  }
  
  public double[] getDoubleArray(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (double[])object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "double[]", classCastException);
      return null;
    } 
  }
  
  public String[] getStringArray(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (String[])object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "String[]", classCastException);
      return null;
    } 
  }
  
  CharSequence[] getCharSequenceArray(String paramString) {
    unparcel();
    Object object = this.mMap.get(paramString);
    if (object == null)
      return null; 
    try {
      return (CharSequence[])object;
    } catch (ClassCastException classCastException) {
      typeWarning(paramString, object, "CharSequence[]", classCastException);
      return null;
    } 
  }
  
  void writeToParcelInner(Parcel paramParcel, int paramInt) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual hasReadWriteHelper : ()Z
    //   4: ifeq -> 11
    //   7: aload_0
    //   8: invokevirtual unparcel : ()V
    //   11: aload_0
    //   12: monitorenter
    //   13: aload_0
    //   14: getfield mParcelledData : Landroid/os/Parcel;
    //   17: astore_3
    //   18: ldc 1279544898
    //   20: istore_2
    //   21: aload_3
    //   22: ifnull -> 87
    //   25: aload_0
    //   26: getfield mParcelledData : Landroid/os/Parcel;
    //   29: getstatic android/os/BaseBundle$NoImagePreloadHolder.EMPTY_PARCEL : Landroid/os/Parcel;
    //   32: if_acmpne -> 43
    //   35: aload_1
    //   36: iconst_0
    //   37: invokevirtual writeInt : (I)V
    //   40: goto -> 84
    //   43: aload_0
    //   44: getfield mParcelledData : Landroid/os/Parcel;
    //   47: invokevirtual dataSize : ()I
    //   50: istore #4
    //   52: aload_1
    //   53: iload #4
    //   55: invokevirtual writeInt : (I)V
    //   58: aload_0
    //   59: getfield mParcelledByNative : Z
    //   62: ifeq -> 68
    //   65: ldc 1279544900
    //   67: istore_2
    //   68: aload_1
    //   69: iload_2
    //   70: invokevirtual writeInt : (I)V
    //   73: aload_1
    //   74: aload_0
    //   75: getfield mParcelledData : Landroid/os/Parcel;
    //   78: iconst_0
    //   79: iload #4
    //   81: invokevirtual appendFrom : (Landroid/os/Parcel;II)V
    //   84: aload_0
    //   85: monitorexit
    //   86: return
    //   87: aload_0
    //   88: getfield mMap : Landroid/util/ArrayMap;
    //   91: astore_3
    //   92: aload_0
    //   93: monitorexit
    //   94: aload_3
    //   95: ifnull -> 162
    //   98: aload_3
    //   99: invokevirtual size : ()I
    //   102: ifgt -> 108
    //   105: goto -> 162
    //   108: aload_1
    //   109: invokevirtual dataPosition : ()I
    //   112: istore #5
    //   114: aload_1
    //   115: iconst_m1
    //   116: invokevirtual writeInt : (I)V
    //   119: aload_1
    //   120: ldc 1279544898
    //   122: invokevirtual writeInt : (I)V
    //   125: aload_1
    //   126: invokevirtual dataPosition : ()I
    //   129: istore_2
    //   130: aload_1
    //   131: aload_3
    //   132: invokevirtual writeArrayMapInternal : (Landroid/util/ArrayMap;)V
    //   135: aload_1
    //   136: invokevirtual dataPosition : ()I
    //   139: istore #4
    //   141: aload_1
    //   142: iload #5
    //   144: invokevirtual setDataPosition : (I)V
    //   147: aload_1
    //   148: iload #4
    //   150: iload_2
    //   151: isub
    //   152: invokevirtual writeInt : (I)V
    //   155: aload_1
    //   156: iload #4
    //   158: invokevirtual setDataPosition : (I)V
    //   161: return
    //   162: aload_1
    //   163: iconst_0
    //   164: invokevirtual writeInt : (I)V
    //   167: return
    //   168: astore_1
    //   169: aload_0
    //   170: monitorexit
    //   171: aload_1
    //   172: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1587	-> 0
    //   #1588	-> 7
    //   #1593	-> 11
    //   #1596	-> 13
    //   #1597	-> 25
    //   #1598	-> 35
    //   #1600	-> 43
    //   #1601	-> 52
    //   #1602	-> 58
    //   #1603	-> 73
    //   #1605	-> 84
    //   #1607	-> 87
    //   #1608	-> 92
    //   #1611	-> 94
    //   #1615	-> 108
    //   #1616	-> 114
    //   #1617	-> 119
    //   #1619	-> 125
    //   #1620	-> 130
    //   #1621	-> 135
    //   #1624	-> 141
    //   #1625	-> 147
    //   #1626	-> 147
    //   #1627	-> 155
    //   #1628	-> 161
    //   #1612	-> 162
    //   #1613	-> 167
    //   #1608	-> 168
    // Exception table:
    //   from	to	target	type
    //   13	18	168	finally
    //   25	35	168	finally
    //   35	40	168	finally
    //   43	52	168	finally
    //   52	58	168	finally
    //   58	65	168	finally
    //   68	73	168	finally
    //   73	84	168	finally
    //   84	86	168	finally
    //   87	92	168	finally
    //   92	94	168	finally
    //   169	171	168	finally
  }
  
  void readFromParcelInner(Parcel paramParcel) {
    int i = paramParcel.readInt();
    readFromParcelInner(paramParcel, i);
  }
  
  private void readFromParcelInner(Parcel paramParcel, int paramInt) {
    // Byte code:
    //   0: iload_2
    //   1: iflt -> 230
    //   4: iload_2
    //   5: ifne -> 21
    //   8: aload_0
    //   9: getstatic android/os/BaseBundle$NoImagePreloadHolder.EMPTY_PARCEL : Landroid/os/Parcel;
    //   12: putfield mParcelledData : Landroid/os/Parcel;
    //   15: aload_0
    //   16: iconst_0
    //   17: putfield mParcelledByNative : Z
    //   20: return
    //   21: iload_2
    //   22: iconst_4
    //   23: irem
    //   24: ifne -> 197
    //   27: aload_1
    //   28: invokevirtual readInt : ()I
    //   31: istore_3
    //   32: iconst_1
    //   33: istore #4
    //   35: iload_3
    //   36: ldc 1279544898
    //   38: if_icmpne -> 47
    //   41: iconst_1
    //   42: istore #5
    //   44: goto -> 50
    //   47: iconst_0
    //   48: istore #5
    //   50: iload_3
    //   51: ldc 1279544900
    //   53: if_icmpne -> 59
    //   56: goto -> 62
    //   59: iconst_0
    //   60: istore #4
    //   62: iload #5
    //   64: ifne -> 111
    //   67: iload #4
    //   69: ifeq -> 75
    //   72: goto -> 111
    //   75: new java/lang/StringBuilder
    //   78: dup
    //   79: invokespecial <init> : ()V
    //   82: astore_1
    //   83: aload_1
    //   84: ldc 'Bad magic number for Bundle: 0x'
    //   86: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   89: pop
    //   90: aload_1
    //   91: iload_3
    //   92: invokestatic toHexString : (I)Ljava/lang/String;
    //   95: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   98: pop
    //   99: new java/lang/IllegalStateException
    //   102: dup
    //   103: aload_1
    //   104: invokevirtual toString : ()Ljava/lang/String;
    //   107: invokespecial <init> : (Ljava/lang/String;)V
    //   110: athrow
    //   111: aload_1
    //   112: invokevirtual hasReadWriteHelper : ()Z
    //   115: ifeq -> 136
    //   118: aload_0
    //   119: monitorenter
    //   120: aload_0
    //   121: aload_1
    //   122: iconst_0
    //   123: iload #4
    //   125: invokespecial initializeFromParcelLocked : (Landroid/os/Parcel;ZZ)V
    //   128: aload_0
    //   129: monitorexit
    //   130: return
    //   131: astore_1
    //   132: aload_0
    //   133: monitorexit
    //   134: aload_1
    //   135: athrow
    //   136: aload_1
    //   137: invokevirtual dataPosition : ()I
    //   140: istore #5
    //   142: aload_1
    //   143: iload #5
    //   145: iload_2
    //   146: invokestatic addOrThrow : (II)I
    //   149: invokevirtual setDataPosition : (I)V
    //   152: invokestatic obtain : ()Landroid/os/Parcel;
    //   155: astore #6
    //   157: aload #6
    //   159: iconst_0
    //   160: invokevirtual setDataPosition : (I)V
    //   163: aload #6
    //   165: aload_1
    //   166: iload #5
    //   168: iload_2
    //   169: invokevirtual appendFrom : (Landroid/os/Parcel;II)V
    //   172: aload #6
    //   174: aload_1
    //   175: invokevirtual adoptClassCookies : (Landroid/os/Parcel;)V
    //   178: aload #6
    //   180: iconst_0
    //   181: invokevirtual setDataPosition : (I)V
    //   184: aload_0
    //   185: aload #6
    //   187: putfield mParcelledData : Landroid/os/Parcel;
    //   190: aload_0
    //   191: iload #4
    //   193: putfield mParcelledByNative : Z
    //   196: return
    //   197: new java/lang/StringBuilder
    //   200: dup
    //   201: invokespecial <init> : ()V
    //   204: astore_1
    //   205: aload_1
    //   206: ldc 'Bundle length is not aligned by 4: '
    //   208: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   211: pop
    //   212: aload_1
    //   213: iload_2
    //   214: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   217: pop
    //   218: new java/lang/IllegalStateException
    //   221: dup
    //   222: aload_1
    //   223: invokevirtual toString : ()Ljava/lang/String;
    //   226: invokespecial <init> : (Ljava/lang/String;)V
    //   229: athrow
    //   230: new java/lang/StringBuilder
    //   233: dup
    //   234: invokespecial <init> : ()V
    //   237: astore_1
    //   238: aload_1
    //   239: ldc 'Bad length in parcel: '
    //   241: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   244: pop
    //   245: aload_1
    //   246: iload_2
    //   247: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   250: pop
    //   251: new java/lang/RuntimeException
    //   254: dup
    //   255: aload_1
    //   256: invokevirtual toString : ()Ljava/lang/String;
    //   259: invokespecial <init> : (Ljava/lang/String;)V
    //   262: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1643	-> 0
    //   #1645	-> 4
    //   #1647	-> 8
    //   #1648	-> 15
    //   #1649	-> 20
    //   #1650	-> 21
    //   #1654	-> 27
    //   #1655	-> 32
    //   #1656	-> 50
    //   #1657	-> 62
    //   #1658	-> 75
    //   #1659	-> 90
    //   #1662	-> 111
    //   #1665	-> 118
    //   #1666	-> 120
    //   #1667	-> 128
    //   #1668	-> 130
    //   #1667	-> 131
    //   #1672	-> 136
    //   #1673	-> 142
    //   #1675	-> 152
    //   #1676	-> 157
    //   #1677	-> 163
    //   #1678	-> 172
    //   #1681	-> 178
    //   #1683	-> 184
    //   #1684	-> 190
    //   #1685	-> 196
    //   #1651	-> 197
    //   #1644	-> 230
    // Exception table:
    //   from	to	target	type
    //   120	128	131	finally
    //   128	130	131	finally
    //   132	134	131	finally
  }
  
  public static void dumpStats(IndentingPrintWriter paramIndentingPrintWriter, String paramString, Object paramObject) {
    Parcel parcel = Parcel.obtain();
    parcel.writeValue(paramObject);
    int i = parcel.dataPosition();
    parcel.recycle();
    if (i > 1024) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(" [size=");
      stringBuilder.append(i);
      stringBuilder.append("]");
      paramIndentingPrintWriter.println(stringBuilder.toString());
      if (paramObject instanceof BaseBundle) {
        dumpStats(paramIndentingPrintWriter, (BaseBundle)paramObject);
      } else if (paramObject instanceof SparseArray) {
        dumpStats(paramIndentingPrintWriter, (SparseArray)paramObject);
      } 
    } 
  }
  
  public static void dumpStats(IndentingPrintWriter paramIndentingPrintWriter, SparseArray paramSparseArray) {
    paramIndentingPrintWriter.increaseIndent();
    if (paramSparseArray == null) {
      paramIndentingPrintWriter.println("[null]");
      return;
    } 
    for (byte b = 0; b < paramSparseArray.size(); b++) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString(paramSparseArray.keyAt(b)));
      dumpStats(paramIndentingPrintWriter, stringBuilder.toString(), paramSparseArray.valueAt(b));
    } 
    paramIndentingPrintWriter.decreaseIndent();
  }
  
  public static void dumpStats(IndentingPrintWriter paramIndentingPrintWriter, BaseBundle paramBaseBundle) {
    paramIndentingPrintWriter.increaseIndent();
    if (paramBaseBundle == null) {
      paramIndentingPrintWriter.println("[null]");
      return;
    } 
    ArrayMap<String, Object> arrayMap = paramBaseBundle.getMap();
    for (byte b = 0; b < arrayMap.size(); b++)
      dumpStats(paramIndentingPrintWriter, (String)arrayMap.keyAt(b), arrayMap.valueAt(b)); 
    paramIndentingPrintWriter.decreaseIndent();
  }
}
