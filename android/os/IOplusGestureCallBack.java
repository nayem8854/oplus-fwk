package android.os;

public interface IOplusGestureCallBack extends IInterface {
  void onDealGesture(int paramInt) throws RemoteException;
  
  class Default implements IOplusGestureCallBack {
    public void onDealGesture(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusGestureCallBack {
    private static final String DESCRIPTOR = "android.os.IOplusGestureCallBack";
    
    static final int TRANSACTION_onDealGesture = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IOplusGestureCallBack");
    }
    
    public static IOplusGestureCallBack asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IOplusGestureCallBack");
      if (iInterface != null && iInterface instanceof IOplusGestureCallBack)
        return (IOplusGestureCallBack)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onDealGesture";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.IOplusGestureCallBack");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IOplusGestureCallBack");
      param1Int1 = param1Parcel1.readInt();
      onDealGesture(param1Int1);
      return true;
    }
    
    private static class Proxy implements IOplusGestureCallBack {
      public static IOplusGestureCallBack sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IOplusGestureCallBack";
      }
      
      public void onDealGesture(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusGestureCallBack");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IOplusGestureCallBack.Stub.getDefaultImpl() != null) {
            IOplusGestureCallBack.Stub.getDefaultImpl().onDealGesture(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusGestureCallBack param1IOplusGestureCallBack) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusGestureCallBack != null) {
          Proxy.sDefaultImpl = param1IOplusGestureCallBack;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusGestureCallBack getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
