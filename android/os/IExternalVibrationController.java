package android.os;

public interface IExternalVibrationController extends IInterface {
  boolean mute() throws RemoteException;
  
  boolean unmute() throws RemoteException;
  
  class Default implements IExternalVibrationController {
    public boolean mute() throws RemoteException {
      return false;
    }
    
    public boolean unmute() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IExternalVibrationController {
    private static final String DESCRIPTOR = "android.os.IExternalVibrationController";
    
    static final int TRANSACTION_mute = 1;
    
    static final int TRANSACTION_unmute = 2;
    
    public Stub() {
      attachInterface(this, "android.os.IExternalVibrationController");
    }
    
    public static IExternalVibrationController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IExternalVibrationController");
      if (iInterface != null && iInterface instanceof IExternalVibrationController)
        return (IExternalVibrationController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "unmute";
      } 
      return "mute";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.os.IExternalVibrationController");
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.IExternalVibrationController");
        boolean bool1 = unmute();
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IExternalVibrationController");
      boolean bool = mute();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements IExternalVibrationController {
      public static IExternalVibrationController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IExternalVibrationController";
      }
      
      public boolean mute() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IExternalVibrationController");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IExternalVibrationController.Stub.getDefaultImpl() != null) {
            bool1 = IExternalVibrationController.Stub.getDefaultImpl().mute();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unmute() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IExternalVibrationController");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IExternalVibrationController.Stub.getDefaultImpl() != null) {
            bool1 = IExternalVibrationController.Stub.getDefaultImpl().unmute();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IExternalVibrationController param1IExternalVibrationController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IExternalVibrationController != null) {
          Proxy.sDefaultImpl = param1IExternalVibrationController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IExternalVibrationController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
