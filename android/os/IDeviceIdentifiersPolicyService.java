package android.os;

public interface IDeviceIdentifiersPolicyService extends IInterface {
  String getSerial() throws RemoteException;
  
  String getSerialForPackage(String paramString1, String paramString2) throws RemoteException;
  
  class Default implements IDeviceIdentifiersPolicyService {
    public String getSerial() throws RemoteException {
      return null;
    }
    
    public String getSerialForPackage(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDeviceIdentifiersPolicyService {
    private static final String DESCRIPTOR = "android.os.IDeviceIdentifiersPolicyService";
    
    static final int TRANSACTION_getSerial = 1;
    
    static final int TRANSACTION_getSerialForPackage = 2;
    
    public Stub() {
      attachInterface(this, "android.os.IDeviceIdentifiersPolicyService");
    }
    
    public static IDeviceIdentifiersPolicyService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IDeviceIdentifiersPolicyService");
      if (iInterface != null && iInterface instanceof IDeviceIdentifiersPolicyService)
        return (IDeviceIdentifiersPolicyService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "getSerialForPackage";
      } 
      return "getSerial";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.os.IDeviceIdentifiersPolicyService");
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.IDeviceIdentifiersPolicyService");
        String str1 = param1Parcel1.readString();
        str = param1Parcel1.readString();
        str = getSerialForPackage(str1, str);
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str);
        return true;
      } 
      str.enforceInterface("android.os.IDeviceIdentifiersPolicyService");
      String str = getSerial();
      param1Parcel2.writeNoException();
      param1Parcel2.writeString(str);
      return true;
    }
    
    private static class Proxy implements IDeviceIdentifiersPolicyService {
      public static IDeviceIdentifiersPolicyService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IDeviceIdentifiersPolicyService";
      }
      
      public String getSerial() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdentifiersPolicyService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IDeviceIdentifiersPolicyService.Stub.getDefaultImpl() != null)
            return IDeviceIdentifiersPolicyService.Stub.getDefaultImpl().getSerial(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSerialForPackage(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDeviceIdentifiersPolicyService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IDeviceIdentifiersPolicyService.Stub.getDefaultImpl() != null) {
            param2String1 = IDeviceIdentifiersPolicyService.Stub.getDefaultImpl().getSerialForPackage(param2String1, param2String2);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDeviceIdentifiersPolicyService param1IDeviceIdentifiersPolicyService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDeviceIdentifiersPolicyService != null) {
          Proxy.sDefaultImpl = param1IDeviceIdentifiersPolicyService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDeviceIdentifiersPolicyService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
