package android.os;

public interface IOplusExVibrator {
  public static final int OPLUS_CALL_TRANSACTION_INDEX = 10000;
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
  
  public static final int VIBRATE_NOT_CHECK_TRANSACTION = 10002;
  
  void vibrateNotCheck(int paramInt, String paramString, IBinder paramIBinder) throws RemoteException;
}
