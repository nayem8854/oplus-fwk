package android.os;

import android.os.incremental.IncrementalFileSystemControlParcel;
import java.io.FileDescriptor;

public interface IVold extends IInterface {
  public static final int ENCRYPTION_FLAG_NO_UI = 4;
  
  public static final int ENCRYPTION_STATE_ERROR_CORRUPT = -4;
  
  public static final int ENCRYPTION_STATE_ERROR_INCOMPLETE = -2;
  
  public static final int ENCRYPTION_STATE_ERROR_INCONSISTENT = -3;
  
  public static final int ENCRYPTION_STATE_ERROR_UNKNOWN = -1;
  
  public static final int ENCRYPTION_STATE_NONE = 1;
  
  public static final int ENCRYPTION_STATE_OK = 0;
  
  public static final int FSTRIM_FLAG_DEEP_TRIM = 1;
  
  public static final int MOUNT_FLAG_PRIMARY = 1;
  
  public static final int MOUNT_FLAG_VISIBLE = 2;
  
  public static final int PARTITION_TYPE_MIXED = 2;
  
  public static final int PARTITION_TYPE_PRIVATE = 1;
  
  public static final int PARTITION_TYPE_PUBLIC = 0;
  
  public static final int PASSWORD_TYPE_DEFAULT = 1;
  
  public static final int PASSWORD_TYPE_PASSWORD = 0;
  
  public static final int PASSWORD_TYPE_PATTERN = 2;
  
  public static final int PASSWORD_TYPE_PIN = 3;
  
  public static final int REMOUNT_MODE_ANDROID_WRITABLE = 8;
  
  public static final int REMOUNT_MODE_DEFAULT = 1;
  
  public static final int REMOUNT_MODE_FULL = 6;
  
  public static final int REMOUNT_MODE_INSTALLER = 5;
  
  public static final int REMOUNT_MODE_LEGACY = 4;
  
  public static final int REMOUNT_MODE_NONE = 0;
  
  public static final int REMOUNT_MODE_OPPO_ANDROID_WRITABLE = 9;
  
  public static final int REMOUNT_MODE_PASS_THROUGH = 7;
  
  public static final int REMOUNT_MODE_READ = 2;
  
  public static final int REMOUNT_MODE_WRITE = 3;
  
  public static final int STORAGE_FLAG_CE = 2;
  
  public static final int STORAGE_FLAG_DE = 1;
  
  public static final int VOLUME_STATE_BAD_REMOVAL = 8;
  
  public static final int VOLUME_STATE_CHECKING = 1;
  
  public static final int VOLUME_STATE_EJECTING = 5;
  
  public static final int VOLUME_STATE_FORMATTING = 4;
  
  public static final int VOLUME_STATE_MOUNTED = 2;
  
  public static final int VOLUME_STATE_MOUNTED_READ_ONLY = 3;
  
  public static final int VOLUME_STATE_REMOVED = 7;
  
  public static final int VOLUME_STATE_UNMOUNTABLE = 6;
  
  public static final int VOLUME_STATE_UNMOUNTED = 0;
  
  public static final int VOLUME_TYPE_ASEC = 3;
  
  public static final int VOLUME_TYPE_EMULATED = 2;
  
  public static final int VOLUME_TYPE_OBB = 4;
  
  public static final int VOLUME_TYPE_PRIVATE = 1;
  
  public static final int VOLUME_TYPE_PUBLIC = 0;
  
  public static final int VOLUME_TYPE_STUB = 5;
  
  void abortChanges(String paramString, boolean paramBoolean) throws RemoteException;
  
  void abortFuse() throws RemoteException;
  
  void abortIdleMaint(IVoldTaskListener paramIVoldTaskListener) throws RemoteException;
  
  void addAppIds(String[] paramArrayOfString, int[] paramArrayOfint) throws RemoteException;
  
  void addSandboxIds(int[] paramArrayOfint, String[] paramArrayOfString) throws RemoteException;
  
  void addUserKeyAuth(int paramInt1, int paramInt2, String paramString1, String paramString2) throws RemoteException;
  
  void benchmark(String paramString, IVoldTaskListener paramIVoldTaskListener) throws RemoteException;
  
  void bindMount(String paramString1, String paramString2) throws RemoteException;
  
  void checkBeforeMount(String paramString) throws RemoteException;
  
  void checkEncryption(String paramString) throws RemoteException;
  
  void clearUserKeyAuth(int paramInt1, int paramInt2, String paramString1, String paramString2) throws RemoteException;
  
  void commitChanges() throws RemoteException;
  
  String createObb(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  String createStubVolume(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt) throws RemoteException;
  
  void createUserKey(int paramInt1, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  void destroyObb(String paramString) throws RemoteException;
  
  void destroySandboxForApp(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  void destroyStubVolume(String paramString) throws RemoteException;
  
  void destroyUserKey(int paramInt) throws RemoteException;
  
  void destroyUserStorage(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void encryptFstab(String paramString1, String paramString2) throws RemoteException;
  
  void fbeEnable() throws RemoteException;
  
  void fdeChangePassword(int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  void fdeCheckPassword(String paramString) throws RemoteException;
  
  void fdeClearPassword() throws RemoteException;
  
  int fdeComplete() throws RemoteException;
  
  void fdeEnable(int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  String fdeGetField(String paramString) throws RemoteException;
  
  String fdeGetPassword() throws RemoteException;
  
  int fdeGetPasswordType() throws RemoteException;
  
  void fdeRestart() throws RemoteException;
  
  void fdeSetField(String paramString1, String paramString2) throws RemoteException;
  
  void fdeVerifyPassword(String paramString) throws RemoteException;
  
  void fixateNewestUserKeyAuth(int paramInt) throws RemoteException;
  
  void fixupAppDir(String paramString, int paramInt) throws RemoteException;
  
  void forgetPartition(String paramString1, String paramString2) throws RemoteException;
  
  void format(String paramString1, String paramString2) throws RemoteException;
  
  void fstrim(int paramInt, IVoldTaskListener paramIVoldTaskListener) throws RemoteException;
  
  boolean incFsEnabled() throws RemoteException;
  
  void initUser0() throws RemoteException;
  
  boolean isCheckpointing() throws RemoteException;
  
  boolean isConvertibleToFbe() throws RemoteException;
  
  void lockUserKey(int paramInt) throws RemoteException;
  
  void markBootAttempt() throws RemoteException;
  
  void monitor() throws RemoteException;
  
  void mount(String paramString, int paramInt1, int paramInt2, IVoldMountCallback paramIVoldMountCallback) throws RemoteException;
  
  FileDescriptor mountAppFuse(int paramInt1, int paramInt2) throws RemoteException;
  
  void mountDefaultEncrypted() throws RemoteException;
  
  void mountFstab(String paramString1, String paramString2) throws RemoteException;
  
  IncrementalFileSystemControlParcel mountIncFs(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  void moveStorage(String paramString1, String paramString2, IVoldTaskListener paramIVoldTaskListener) throws RemoteException;
  
  boolean needsCheckpoint() throws RemoteException;
  
  boolean needsRollback() throws RemoteException;
  
  void onSecureKeyguardStateChanged(boolean paramBoolean) throws RemoteException;
  
  void onUserAdded(int paramInt1, int paramInt2) throws RemoteException;
  
  void onUserRemoved(int paramInt) throws RemoteException;
  
  void onUserStarted(int paramInt) throws RemoteException;
  
  void onUserStopped(int paramInt) throws RemoteException;
  
  FileDescriptor openAppFuseFile(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void partition(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void prepareCheckpoint() throws RemoteException;
  
  void prepareSandboxForApp(String paramString1, int paramInt1, String paramString2, int paramInt2) throws RemoteException;
  
  void prepareUserStorage(String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void remountAppStorageDirs(int paramInt1, int paramInt2, String[] paramArrayOfString) throws RemoteException;
  
  void remountUid(int paramInt1, int paramInt2) throws RemoteException;
  
  void reset() throws RemoteException;
  
  void resetCheckpoint() throws RemoteException;
  
  void restoreCheckpoint(String paramString) throws RemoteException;
  
  void restoreCheckpointPart(String paramString, int paramInt) throws RemoteException;
  
  void runIdleMaint(IVoldTaskListener paramIVoldTaskListener) throws RemoteException;
  
  void setIncFsMountOptions(IncrementalFileSystemControlParcel paramIncrementalFileSystemControlParcel, boolean paramBoolean) throws RemoteException;
  
  void setListener(IVoldListener paramIVoldListener) throws RemoteException;
  
  void setupAppDir(String paramString, int paramInt) throws RemoteException;
  
  void shutdown() throws RemoteException;
  
  void startCheckpoint(int paramInt) throws RemoteException;
  
  boolean supportsBlockCheckpoint() throws RemoteException;
  
  boolean supportsCheckpoint() throws RemoteException;
  
  boolean supportsFileCheckpoint() throws RemoteException;
  
  void unlockUserKey(int paramInt1, int paramInt2, String paramString1, String paramString2) throws RemoteException;
  
  void unmount(String paramString) throws RemoteException;
  
  void unmountAppFuse(int paramInt1, int paramInt2) throws RemoteException;
  
  void unmountIncFs(String paramString) throws RemoteException;
  
  class Default implements IVold {
    public void setListener(IVoldListener param1IVoldListener) throws RemoteException {}
    
    public void abortFuse() throws RemoteException {}
    
    public void monitor() throws RemoteException {}
    
    public void reset() throws RemoteException {}
    
    public void shutdown() throws RemoteException {}
    
    public void onUserAdded(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onUserRemoved(int param1Int) throws RemoteException {}
    
    public void onUserStarted(int param1Int) throws RemoteException {}
    
    public void onUserStopped(int param1Int) throws RemoteException {}
    
    public void addAppIds(String[] param1ArrayOfString, int[] param1ArrayOfint) throws RemoteException {}
    
    public void addSandboxIds(int[] param1ArrayOfint, String[] param1ArrayOfString) throws RemoteException {}
    
    public void onSecureKeyguardStateChanged(boolean param1Boolean) throws RemoteException {}
    
    public void partition(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void forgetPartition(String param1String1, String param1String2) throws RemoteException {}
    
    public void mount(String param1String, int param1Int1, int param1Int2, IVoldMountCallback param1IVoldMountCallback) throws RemoteException {}
    
    public void unmount(String param1String) throws RemoteException {}
    
    public void format(String param1String1, String param1String2) throws RemoteException {}
    
    public void benchmark(String param1String, IVoldTaskListener param1IVoldTaskListener) throws RemoteException {}
    
    public void checkEncryption(String param1String) throws RemoteException {}
    
    public void checkBeforeMount(String param1String) throws RemoteException {}
    
    public void moveStorage(String param1String1, String param1String2, IVoldTaskListener param1IVoldTaskListener) throws RemoteException {}
    
    public void remountUid(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void remountAppStorageDirs(int param1Int1, int param1Int2, String[] param1ArrayOfString) throws RemoteException {}
    
    public void setupAppDir(String param1String, int param1Int) throws RemoteException {}
    
    public void fixupAppDir(String param1String, int param1Int) throws RemoteException {}
    
    public String createObb(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return null;
    }
    
    public void destroyObb(String param1String) throws RemoteException {}
    
    public void fstrim(int param1Int, IVoldTaskListener param1IVoldTaskListener) throws RemoteException {}
    
    public void runIdleMaint(IVoldTaskListener param1IVoldTaskListener) throws RemoteException {}
    
    public void abortIdleMaint(IVoldTaskListener param1IVoldTaskListener) throws RemoteException {}
    
    public FileDescriptor mountAppFuse(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public void unmountAppFuse(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void fdeCheckPassword(String param1String) throws RemoteException {}
    
    public void fdeRestart() throws RemoteException {}
    
    public int fdeComplete() throws RemoteException {
      return 0;
    }
    
    public void fdeEnable(int param1Int1, String param1String, int param1Int2) throws RemoteException {}
    
    public void fdeChangePassword(int param1Int, String param1String1, String param1String2) throws RemoteException {}
    
    public void fdeVerifyPassword(String param1String) throws RemoteException {}
    
    public String fdeGetField(String param1String) throws RemoteException {
      return null;
    }
    
    public void fdeSetField(String param1String1, String param1String2) throws RemoteException {}
    
    public int fdeGetPasswordType() throws RemoteException {
      return 0;
    }
    
    public String fdeGetPassword() throws RemoteException {
      return null;
    }
    
    public void fdeClearPassword() throws RemoteException {}
    
    public void fbeEnable() throws RemoteException {}
    
    public void mountDefaultEncrypted() throws RemoteException {}
    
    public void initUser0() throws RemoteException {}
    
    public boolean isConvertibleToFbe() throws RemoteException {
      return false;
    }
    
    public void mountFstab(String param1String1, String param1String2) throws RemoteException {}
    
    public void encryptFstab(String param1String1, String param1String2) throws RemoteException {}
    
    public void createUserKey(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public void destroyUserKey(int param1Int) throws RemoteException {}
    
    public void addUserKeyAuth(int param1Int1, int param1Int2, String param1String1, String param1String2) throws RemoteException {}
    
    public void clearUserKeyAuth(int param1Int1, int param1Int2, String param1String1, String param1String2) throws RemoteException {}
    
    public void fixateNewestUserKeyAuth(int param1Int) throws RemoteException {}
    
    public void unlockUserKey(int param1Int1, int param1Int2, String param1String1, String param1String2) throws RemoteException {}
    
    public void lockUserKey(int param1Int) throws RemoteException {}
    
    public void prepareUserStorage(String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void destroyUserStorage(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void prepareSandboxForApp(String param1String1, int param1Int1, String param1String2, int param1Int2) throws RemoteException {}
    
    public void destroySandboxForApp(String param1String1, String param1String2, int param1Int) throws RemoteException {}
    
    public void startCheckpoint(int param1Int) throws RemoteException {}
    
    public boolean needsCheckpoint() throws RemoteException {
      return false;
    }
    
    public boolean needsRollback() throws RemoteException {
      return false;
    }
    
    public boolean isCheckpointing() throws RemoteException {
      return false;
    }
    
    public void abortChanges(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void commitChanges() throws RemoteException {}
    
    public void prepareCheckpoint() throws RemoteException {}
    
    public void restoreCheckpoint(String param1String) throws RemoteException {}
    
    public void restoreCheckpointPart(String param1String, int param1Int) throws RemoteException {}
    
    public void markBootAttempt() throws RemoteException {}
    
    public boolean supportsCheckpoint() throws RemoteException {
      return false;
    }
    
    public boolean supportsBlockCheckpoint() throws RemoteException {
      return false;
    }
    
    public boolean supportsFileCheckpoint() throws RemoteException {
      return false;
    }
    
    public void resetCheckpoint() throws RemoteException {}
    
    public String createStubVolume(String param1String1, String param1String2, String param1String3, String param1String4, String param1String5, int param1Int) throws RemoteException {
      return null;
    }
    
    public void destroyStubVolume(String param1String) throws RemoteException {}
    
    public FileDescriptor openAppFuseFile(int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {
      return null;
    }
    
    public boolean incFsEnabled() throws RemoteException {
      return false;
    }
    
    public IncrementalFileSystemControlParcel mountIncFs(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return null;
    }
    
    public void unmountIncFs(String param1String) throws RemoteException {}
    
    public void setIncFsMountOptions(IncrementalFileSystemControlParcel param1IncrementalFileSystemControlParcel, boolean param1Boolean) throws RemoteException {}
    
    public void bindMount(String param1String1, String param1String2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVold {
    private static final String DESCRIPTOR = "android.os.IVold";
    
    static final int TRANSACTION_abortChanges = 65;
    
    static final int TRANSACTION_abortFuse = 2;
    
    static final int TRANSACTION_abortIdleMaint = 30;
    
    static final int TRANSACTION_addAppIds = 10;
    
    static final int TRANSACTION_addSandboxIds = 11;
    
    static final int TRANSACTION_addUserKeyAuth = 52;
    
    static final int TRANSACTION_benchmark = 18;
    
    static final int TRANSACTION_bindMount = 82;
    
    static final int TRANSACTION_checkBeforeMount = 20;
    
    static final int TRANSACTION_checkEncryption = 19;
    
    static final int TRANSACTION_clearUserKeyAuth = 53;
    
    static final int TRANSACTION_commitChanges = 66;
    
    static final int TRANSACTION_createObb = 26;
    
    static final int TRANSACTION_createStubVolume = 75;
    
    static final int TRANSACTION_createUserKey = 50;
    
    static final int TRANSACTION_destroyObb = 27;
    
    static final int TRANSACTION_destroySandboxForApp = 60;
    
    static final int TRANSACTION_destroyStubVolume = 76;
    
    static final int TRANSACTION_destroyUserKey = 51;
    
    static final int TRANSACTION_destroyUserStorage = 58;
    
    static final int TRANSACTION_encryptFstab = 49;
    
    static final int TRANSACTION_fbeEnable = 44;
    
    static final int TRANSACTION_fdeChangePassword = 37;
    
    static final int TRANSACTION_fdeCheckPassword = 33;
    
    static final int TRANSACTION_fdeClearPassword = 43;
    
    static final int TRANSACTION_fdeComplete = 35;
    
    static final int TRANSACTION_fdeEnable = 36;
    
    static final int TRANSACTION_fdeGetField = 39;
    
    static final int TRANSACTION_fdeGetPassword = 42;
    
    static final int TRANSACTION_fdeGetPasswordType = 41;
    
    static final int TRANSACTION_fdeRestart = 34;
    
    static final int TRANSACTION_fdeSetField = 40;
    
    static final int TRANSACTION_fdeVerifyPassword = 38;
    
    static final int TRANSACTION_fixateNewestUserKeyAuth = 54;
    
    static final int TRANSACTION_fixupAppDir = 25;
    
    static final int TRANSACTION_forgetPartition = 14;
    
    static final int TRANSACTION_format = 17;
    
    static final int TRANSACTION_fstrim = 28;
    
    static final int TRANSACTION_incFsEnabled = 78;
    
    static final int TRANSACTION_initUser0 = 46;
    
    static final int TRANSACTION_isCheckpointing = 64;
    
    static final int TRANSACTION_isConvertibleToFbe = 47;
    
    static final int TRANSACTION_lockUserKey = 56;
    
    static final int TRANSACTION_markBootAttempt = 70;
    
    static final int TRANSACTION_monitor = 3;
    
    static final int TRANSACTION_mount = 15;
    
    static final int TRANSACTION_mountAppFuse = 31;
    
    static final int TRANSACTION_mountDefaultEncrypted = 45;
    
    static final int TRANSACTION_mountFstab = 48;
    
    static final int TRANSACTION_mountIncFs = 79;
    
    static final int TRANSACTION_moveStorage = 21;
    
    static final int TRANSACTION_needsCheckpoint = 62;
    
    static final int TRANSACTION_needsRollback = 63;
    
    static final int TRANSACTION_onSecureKeyguardStateChanged = 12;
    
    static final int TRANSACTION_onUserAdded = 6;
    
    static final int TRANSACTION_onUserRemoved = 7;
    
    static final int TRANSACTION_onUserStarted = 8;
    
    static final int TRANSACTION_onUserStopped = 9;
    
    static final int TRANSACTION_openAppFuseFile = 77;
    
    static final int TRANSACTION_partition = 13;
    
    static final int TRANSACTION_prepareCheckpoint = 67;
    
    static final int TRANSACTION_prepareSandboxForApp = 59;
    
    static final int TRANSACTION_prepareUserStorage = 57;
    
    static final int TRANSACTION_remountAppStorageDirs = 23;
    
    static final int TRANSACTION_remountUid = 22;
    
    static final int TRANSACTION_reset = 4;
    
    static final int TRANSACTION_resetCheckpoint = 74;
    
    static final int TRANSACTION_restoreCheckpoint = 68;
    
    static final int TRANSACTION_restoreCheckpointPart = 69;
    
    static final int TRANSACTION_runIdleMaint = 29;
    
    static final int TRANSACTION_setIncFsMountOptions = 81;
    
    static final int TRANSACTION_setListener = 1;
    
    static final int TRANSACTION_setupAppDir = 24;
    
    static final int TRANSACTION_shutdown = 5;
    
    static final int TRANSACTION_startCheckpoint = 61;
    
    static final int TRANSACTION_supportsBlockCheckpoint = 72;
    
    static final int TRANSACTION_supportsCheckpoint = 71;
    
    static final int TRANSACTION_supportsFileCheckpoint = 73;
    
    static final int TRANSACTION_unlockUserKey = 55;
    
    static final int TRANSACTION_unmount = 16;
    
    static final int TRANSACTION_unmountAppFuse = 32;
    
    static final int TRANSACTION_unmountIncFs = 80;
    
    public Stub() {
      attachInterface(this, "android.os.IVold");
    }
    
    public static IVold asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IVold");
      if (iInterface != null && iInterface instanceof IVold)
        return (IVold)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 82:
          return "bindMount";
        case 81:
          return "setIncFsMountOptions";
        case 80:
          return "unmountIncFs";
        case 79:
          return "mountIncFs";
        case 78:
          return "incFsEnabled";
        case 77:
          return "openAppFuseFile";
        case 76:
          return "destroyStubVolume";
        case 75:
          return "createStubVolume";
        case 74:
          return "resetCheckpoint";
        case 73:
          return "supportsFileCheckpoint";
        case 72:
          return "supportsBlockCheckpoint";
        case 71:
          return "supportsCheckpoint";
        case 70:
          return "markBootAttempt";
        case 69:
          return "restoreCheckpointPart";
        case 68:
          return "restoreCheckpoint";
        case 67:
          return "prepareCheckpoint";
        case 66:
          return "commitChanges";
        case 65:
          return "abortChanges";
        case 64:
          return "isCheckpointing";
        case 63:
          return "needsRollback";
        case 62:
          return "needsCheckpoint";
        case 61:
          return "startCheckpoint";
        case 60:
          return "destroySandboxForApp";
        case 59:
          return "prepareSandboxForApp";
        case 58:
          return "destroyUserStorage";
        case 57:
          return "prepareUserStorage";
        case 56:
          return "lockUserKey";
        case 55:
          return "unlockUserKey";
        case 54:
          return "fixateNewestUserKeyAuth";
        case 53:
          return "clearUserKeyAuth";
        case 52:
          return "addUserKeyAuth";
        case 51:
          return "destroyUserKey";
        case 50:
          return "createUserKey";
        case 49:
          return "encryptFstab";
        case 48:
          return "mountFstab";
        case 47:
          return "isConvertibleToFbe";
        case 46:
          return "initUser0";
        case 45:
          return "mountDefaultEncrypted";
        case 44:
          return "fbeEnable";
        case 43:
          return "fdeClearPassword";
        case 42:
          return "fdeGetPassword";
        case 41:
          return "fdeGetPasswordType";
        case 40:
          return "fdeSetField";
        case 39:
          return "fdeGetField";
        case 38:
          return "fdeVerifyPassword";
        case 37:
          return "fdeChangePassword";
        case 36:
          return "fdeEnable";
        case 35:
          return "fdeComplete";
        case 34:
          return "fdeRestart";
        case 33:
          return "fdeCheckPassword";
        case 32:
          return "unmountAppFuse";
        case 31:
          return "mountAppFuse";
        case 30:
          return "abortIdleMaint";
        case 29:
          return "runIdleMaint";
        case 28:
          return "fstrim";
        case 27:
          return "destroyObb";
        case 26:
          return "createObb";
        case 25:
          return "fixupAppDir";
        case 24:
          return "setupAppDir";
        case 23:
          return "remountAppStorageDirs";
        case 22:
          return "remountUid";
        case 21:
          return "moveStorage";
        case 20:
          return "checkBeforeMount";
        case 19:
          return "checkEncryption";
        case 18:
          return "benchmark";
        case 17:
          return "format";
        case 16:
          return "unmount";
        case 15:
          return "mount";
        case 14:
          return "forgetPartition";
        case 13:
          return "partition";
        case 12:
          return "onSecureKeyguardStateChanged";
        case 11:
          return "addSandboxIds";
        case 10:
          return "addAppIds";
        case 9:
          return "onUserStopped";
        case 8:
          return "onUserStarted";
        case 7:
          return "onUserRemoved";
        case 6:
          return "onUserAdded";
        case 5:
          return "shutdown";
        case 4:
          return "reset";
        case 3:
          return "monitor";
        case 2:
          return "abortFuse";
        case 1:
          break;
      } 
      return "setListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        String str6;
        IncrementalFileSystemControlParcel incrementalFileSystemControlParcel;
        FileDescriptor fileDescriptor2;
        String str5;
        FileDescriptor fileDescriptor1;
        IVoldTaskListener iVoldTaskListener3;
        String str4, arrayOfString2[];
        IVoldTaskListener iVoldTaskListener2;
        String str3;
        IVoldTaskListener iVoldTaskListener1;
        String str2;
        IVoldMountCallback iVoldMountCallback;
        String str1, arrayOfString1[];
        int[] arrayOfInt1;
        String str7;
        int[] arrayOfInt2;
        String arrayOfString3[], str8;
        int n, i1;
        String str9, str10, str11;
        boolean bool5 = false, bool6 = false, bool7 = false, bool8 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 82:
            param1Parcel1.enforceInterface("android.os.IVold");
            str7 = param1Parcel1.readString();
            str6 = param1Parcel1.readString();
            bindMount(str7, str6);
            param1Parcel2.writeNoException();
            return true;
          case 81:
            str6.enforceInterface("android.os.IVold");
            if (str6.readInt() != 0) {
              IncrementalFileSystemControlParcel incrementalFileSystemControlParcel1 = IncrementalFileSystemControlParcel.CREATOR.createFromParcel((Parcel)str6);
            } else {
              str7 = null;
            } 
            bool5 = bool8;
            if (str6.readInt() != 0)
              bool5 = true; 
            setIncFsMountOptions((IncrementalFileSystemControlParcel)str7, bool5);
            param1Parcel2.writeNoException();
            return true;
          case 80:
            str6.enforceInterface("android.os.IVold");
            str6 = str6.readString();
            unmountIncFs(str6);
            param1Parcel2.writeNoException();
            return true;
          case 79:
            str6.enforceInterface("android.os.IVold");
            str8 = str6.readString();
            str7 = str6.readString();
            param1Int1 = str6.readInt();
            incrementalFileSystemControlParcel = mountIncFs(str8, str7, param1Int1);
            param1Parcel2.writeNoException();
            if (incrementalFileSystemControlParcel != null) {
              param1Parcel2.writeInt(1);
              incrementalFileSystemControlParcel.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 78:
            incrementalFileSystemControlParcel.enforceInterface("android.os.IVold");
            bool4 = incFsEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 77:
            incrementalFileSystemControlParcel.enforceInterface("android.os.IVold");
            param1Int2 = incrementalFileSystemControlParcel.readInt();
            n = incrementalFileSystemControlParcel.readInt();
            m = incrementalFileSystemControlParcel.readInt();
            i1 = incrementalFileSystemControlParcel.readInt();
            fileDescriptor2 = openAppFuseFile(param1Int2, n, m, i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeRawFileDescriptor(fileDescriptor2);
            return true;
          case 76:
            fileDescriptor2.enforceInterface("android.os.IVold");
            str5 = fileDescriptor2.readString();
            destroyStubVolume(str5);
            param1Parcel2.writeNoException();
            return true;
          case 75:
            str5.enforceInterface("android.os.IVold");
            str9 = str5.readString();
            str10 = str5.readString();
            str11 = str5.readString();
            str8 = str5.readString();
            str7 = str5.readString();
            m = str5.readInt();
            str5 = createStubVolume(str9, str10, str11, str8, str7, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str5);
            return true;
          case 74:
            str5.enforceInterface("android.os.IVold");
            resetCheckpoint();
            param1Parcel2.writeNoException();
            return true;
          case 73:
            str5.enforceInterface("android.os.IVold");
            bool3 = supportsFileCheckpoint();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 72:
            str5.enforceInterface("android.os.IVold");
            bool3 = supportsBlockCheckpoint();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 71:
            str5.enforceInterface("android.os.IVold");
            bool3 = supportsCheckpoint();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 70:
            str5.enforceInterface("android.os.IVold");
            markBootAttempt();
            param1Parcel2.writeNoException();
            return true;
          case 69:
            str5.enforceInterface("android.os.IVold");
            str7 = str5.readString();
            k = str5.readInt();
            restoreCheckpointPart(str7, k);
            param1Parcel2.writeNoException();
            return true;
          case 68:
            str5.enforceInterface("android.os.IVold");
            str5 = str5.readString();
            restoreCheckpoint(str5);
            param1Parcel2.writeNoException();
            return true;
          case 67:
            str5.enforceInterface("android.os.IVold");
            prepareCheckpoint();
            param1Parcel2.writeNoException();
            return true;
          case 66:
            str5.enforceInterface("android.os.IVold");
            commitChanges();
            param1Parcel2.writeNoException();
            return true;
          case 65:
            str5.enforceInterface("android.os.IVold");
            str7 = str5.readString();
            if (str5.readInt() != 0)
              bool5 = true; 
            abortChanges(str7, bool5);
            param1Parcel2.writeNoException();
            return true;
          case 64:
            str5.enforceInterface("android.os.IVold");
            bool2 = isCheckpointing();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 63:
            str5.enforceInterface("android.os.IVold");
            bool2 = needsRollback();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 62:
            str5.enforceInterface("android.os.IVold");
            bool2 = needsCheckpoint();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 61:
            str5.enforceInterface("android.os.IVold");
            j = str5.readInt();
            startCheckpoint(j);
            param1Parcel2.writeNoException();
            return true;
          case 60:
            str5.enforceInterface("android.os.IVold");
            str8 = str5.readString();
            str7 = str5.readString();
            j = str5.readInt();
            destroySandboxForApp(str8, str7, j);
            param1Parcel2.writeNoException();
            return true;
          case 59:
            str5.enforceInterface("android.os.IVold");
            str7 = str5.readString();
            param1Int2 = str5.readInt();
            str8 = str5.readString();
            j = str5.readInt();
            prepareSandboxForApp(str7, param1Int2, str8, j);
            param1Parcel2.writeNoException();
            return true;
          case 58:
            str5.enforceInterface("android.os.IVold");
            str7 = str5.readString();
            j = str5.readInt();
            param1Int2 = str5.readInt();
            destroyUserStorage(str7, j, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 57:
            str5.enforceInterface("android.os.IVold");
            str7 = str5.readString();
            j = str5.readInt();
            param1Int2 = str5.readInt();
            i1 = str5.readInt();
            prepareUserStorage(str7, j, param1Int2, i1);
            param1Parcel2.writeNoException();
            return true;
          case 56:
            str5.enforceInterface("android.os.IVold");
            j = str5.readInt();
            lockUserKey(j);
            param1Parcel2.writeNoException();
            return true;
          case 55:
            str5.enforceInterface("android.os.IVold");
            j = str5.readInt();
            param1Int2 = str5.readInt();
            str7 = str5.readString();
            str5 = str5.readString();
            unlockUserKey(j, param1Int2, str7, str5);
            param1Parcel2.writeNoException();
            return true;
          case 54:
            str5.enforceInterface("android.os.IVold");
            j = str5.readInt();
            fixateNewestUserKeyAuth(j);
            param1Parcel2.writeNoException();
            return true;
          case 53:
            str5.enforceInterface("android.os.IVold");
            param1Int2 = str5.readInt();
            j = str5.readInt();
            str7 = str5.readString();
            str5 = str5.readString();
            clearUserKeyAuth(param1Int2, j, str7, str5);
            param1Parcel2.writeNoException();
            return true;
          case 52:
            str5.enforceInterface("android.os.IVold");
            j = str5.readInt();
            param1Int2 = str5.readInt();
            str7 = str5.readString();
            str5 = str5.readString();
            addUserKeyAuth(j, param1Int2, str7, str5);
            param1Parcel2.writeNoException();
            return true;
          case 51:
            str5.enforceInterface("android.os.IVold");
            j = str5.readInt();
            destroyUserKey(j);
            param1Parcel2.writeNoException();
            return true;
          case 50:
            str5.enforceInterface("android.os.IVold");
            j = str5.readInt();
            param1Int2 = str5.readInt();
            bool5 = bool6;
            if (str5.readInt() != 0)
              bool5 = true; 
            createUserKey(j, param1Int2, bool5);
            param1Parcel2.writeNoException();
            return true;
          case 49:
            str5.enforceInterface("android.os.IVold");
            str7 = str5.readString();
            str5 = str5.readString();
            encryptFstab(str7, str5);
            param1Parcel2.writeNoException();
            return true;
          case 48:
            str5.enforceInterface("android.os.IVold");
            str7 = str5.readString();
            str5 = str5.readString();
            mountFstab(str7, str5);
            param1Parcel2.writeNoException();
            return true;
          case 47:
            str5.enforceInterface("android.os.IVold");
            bool1 = isConvertibleToFbe();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 46:
            str5.enforceInterface("android.os.IVold");
            initUser0();
            param1Parcel2.writeNoException();
            return true;
          case 45:
            str5.enforceInterface("android.os.IVold");
            mountDefaultEncrypted();
            param1Parcel2.writeNoException();
            return true;
          case 44:
            str5.enforceInterface("android.os.IVold");
            fbeEnable();
            param1Parcel2.writeNoException();
            return true;
          case 43:
            str5.enforceInterface("android.os.IVold");
            fdeClearPassword();
            param1Parcel2.writeNoException();
            return true;
          case 42:
            str5.enforceInterface("android.os.IVold");
            str5 = fdeGetPassword();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str5);
            return true;
          case 41:
            str5.enforceInterface("android.os.IVold");
            i = fdeGetPasswordType();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 40:
            str5.enforceInterface("android.os.IVold");
            str7 = str5.readString();
            str5 = str5.readString();
            fdeSetField(str7, str5);
            param1Parcel2.writeNoException();
            return true;
          case 39:
            str5.enforceInterface("android.os.IVold");
            str5 = str5.readString();
            str5 = fdeGetField(str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str5);
            return true;
          case 38:
            str5.enforceInterface("android.os.IVold");
            str5 = str5.readString();
            fdeVerifyPassword(str5);
            param1Parcel2.writeNoException();
            return true;
          case 37:
            str5.enforceInterface("android.os.IVold");
            i = str5.readInt();
            str7 = str5.readString();
            str5 = str5.readString();
            fdeChangePassword(i, str7, str5);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            str5.enforceInterface("android.os.IVold");
            param1Int2 = str5.readInt();
            str7 = str5.readString();
            i = str5.readInt();
            fdeEnable(param1Int2, str7, i);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            str5.enforceInterface("android.os.IVold");
            i = fdeComplete();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 34:
            str5.enforceInterface("android.os.IVold");
            fdeRestart();
            param1Parcel2.writeNoException();
            return true;
          case 33:
            str5.enforceInterface("android.os.IVold");
            str5 = str5.readString();
            fdeCheckPassword(str5);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            str5.enforceInterface("android.os.IVold");
            param1Int2 = str5.readInt();
            i = str5.readInt();
            unmountAppFuse(param1Int2, i);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            str5.enforceInterface("android.os.IVold");
            param1Int2 = str5.readInt();
            i = str5.readInt();
            fileDescriptor1 = mountAppFuse(param1Int2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeRawFileDescriptor(fileDescriptor1);
            return true;
          case 30:
            fileDescriptor1.enforceInterface("android.os.IVold");
            iVoldTaskListener3 = IVoldTaskListener.Stub.asInterface(fileDescriptor1.readStrongBinder());
            abortIdleMaint(iVoldTaskListener3);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            iVoldTaskListener3.enforceInterface("android.os.IVold");
            iVoldTaskListener3 = IVoldTaskListener.Stub.asInterface(iVoldTaskListener3.readStrongBinder());
            runIdleMaint(iVoldTaskListener3);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            iVoldTaskListener3.enforceInterface("android.os.IVold");
            i = iVoldTaskListener3.readInt();
            iVoldTaskListener3 = IVoldTaskListener.Stub.asInterface(iVoldTaskListener3.readStrongBinder());
            fstrim(i, iVoldTaskListener3);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            iVoldTaskListener3.enforceInterface("android.os.IVold");
            str4 = iVoldTaskListener3.readString();
            destroyObb(str4);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            str4.enforceInterface("android.os.IVold");
            str7 = str4.readString();
            str8 = str4.readString();
            i = str4.readInt();
            str4 = createObb(str7, str8, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str4);
            return true;
          case 25:
            str4.enforceInterface("android.os.IVold");
            str7 = str4.readString();
            i = str4.readInt();
            fixupAppDir(str7, i);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            str4.enforceInterface("android.os.IVold");
            str7 = str4.readString();
            i = str4.readInt();
            setupAppDir(str7, i);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str4.enforceInterface("android.os.IVold");
            param1Int2 = str4.readInt();
            i = str4.readInt();
            arrayOfString2 = str4.createStringArray();
            remountAppStorageDirs(param1Int2, i, arrayOfString2);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            arrayOfString2.enforceInterface("android.os.IVold");
            i = arrayOfString2.readInt();
            param1Int2 = arrayOfString2.readInt();
            remountUid(i, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            arrayOfString2.enforceInterface("android.os.IVold");
            str8 = arrayOfString2.readString();
            str7 = arrayOfString2.readString();
            iVoldTaskListener2 = IVoldTaskListener.Stub.asInterface(arrayOfString2.readStrongBinder());
            moveStorage(str8, str7, iVoldTaskListener2);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            iVoldTaskListener2.enforceInterface("android.os.IVold");
            str3 = iVoldTaskListener2.readString();
            checkBeforeMount(str3);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            str3.enforceInterface("android.os.IVold");
            str3 = str3.readString();
            checkEncryption(str3);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            str3.enforceInterface("android.os.IVold");
            str7 = str3.readString();
            iVoldTaskListener1 = IVoldTaskListener.Stub.asInterface(str3.readStrongBinder());
            benchmark(str7, iVoldTaskListener1);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            iVoldTaskListener1.enforceInterface("android.os.IVold");
            str7 = iVoldTaskListener1.readString();
            str2 = iVoldTaskListener1.readString();
            format(str7, str2);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            str2.enforceInterface("android.os.IVold");
            str2 = str2.readString();
            unmount(str2);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str2.enforceInterface("android.os.IVold");
            str7 = str2.readString();
            param1Int2 = str2.readInt();
            i = str2.readInt();
            iVoldMountCallback = IVoldMountCallback.Stub.asInterface(str2.readStrongBinder());
            mount(str7, param1Int2, i, iVoldMountCallback);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            iVoldMountCallback.enforceInterface("android.os.IVold");
            str7 = iVoldMountCallback.readString();
            str1 = iVoldMountCallback.readString();
            forgetPartition(str7, str1);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            str1.enforceInterface("android.os.IVold");
            str7 = str1.readString();
            param1Int2 = str1.readInt();
            i = str1.readInt();
            partition(str7, param1Int2, i);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str1.enforceInterface("android.os.IVold");
            bool5 = bool7;
            if (str1.readInt() != 0)
              bool5 = true; 
            onSecureKeyguardStateChanged(bool5);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str1.enforceInterface("android.os.IVold");
            arrayOfInt2 = str1.createIntArray();
            arrayOfString1 = str1.createStringArray();
            addSandboxIds(arrayOfInt2, arrayOfString1);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            arrayOfString1.enforceInterface("android.os.IVold");
            arrayOfString3 = arrayOfString1.createStringArray();
            arrayOfInt1 = arrayOfString1.createIntArray();
            addAppIds(arrayOfString3, arrayOfInt1);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            arrayOfInt1.enforceInterface("android.os.IVold");
            i = arrayOfInt1.readInt();
            onUserStopped(i);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            arrayOfInt1.enforceInterface("android.os.IVold");
            i = arrayOfInt1.readInt();
            onUserStarted(i);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            arrayOfInt1.enforceInterface("android.os.IVold");
            i = arrayOfInt1.readInt();
            onUserRemoved(i);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            arrayOfInt1.enforceInterface("android.os.IVold");
            param1Int2 = arrayOfInt1.readInt();
            i = arrayOfInt1.readInt();
            onUserAdded(param1Int2, i);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            arrayOfInt1.enforceInterface("android.os.IVold");
            shutdown();
            param1Parcel2.writeNoException();
            return true;
          case 4:
            arrayOfInt1.enforceInterface("android.os.IVold");
            reset();
            param1Parcel2.writeNoException();
            return true;
          case 3:
            arrayOfInt1.enforceInterface("android.os.IVold");
            monitor();
            param1Parcel2.writeNoException();
            return true;
          case 2:
            arrayOfInt1.enforceInterface("android.os.IVold");
            abortFuse();
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfInt1.enforceInterface("android.os.IVold");
        IVoldListener iVoldListener = IVoldListener.Stub.asInterface(arrayOfInt1.readStrongBinder());
        setListener(iVoldListener);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.os.IVold");
      return true;
    }
    
    private static class Proxy implements IVold {
      public static IVold sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IVold";
      }
      
      public void setListener(IVoldListener param2IVoldListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IVold");
          if (param2IVoldListener != null) {
            iBinder = param2IVoldListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().setListener(param2IVoldListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void abortFuse() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().abortFuse();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void monitor() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().monitor();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reset() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().reset();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void shutdown() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().shutdown();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onUserAdded(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().onUserAdded(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onUserRemoved(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().onUserRemoved(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onUserStarted(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().onUserStarted(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onUserStopped(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().onUserStopped(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addAppIds(String[] param2ArrayOfString, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().addAppIds(param2ArrayOfString, param2ArrayOfint);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addSandboxIds(int[] param2ArrayOfint, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeIntArray(param2ArrayOfint);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().addSandboxIds(param2ArrayOfint, param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onSecureKeyguardStateChanged(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.IVold");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool1 && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().onSecureKeyguardStateChanged(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void partition(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().partition(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void forgetPartition(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().forgetPartition(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void mount(String param2String, int param2Int1, int param2Int2, IVoldMountCallback param2IVoldMountCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2IVoldMountCallback != null) {
            iBinder = param2IVoldMountCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().mount(param2String, param2Int1, param2Int2, param2IVoldMountCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unmount(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().unmount(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void format(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().format(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void benchmark(String param2String, IVoldTaskListener param2IVoldTaskListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          if (param2IVoldTaskListener != null) {
            iBinder = param2IVoldTaskListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().benchmark(param2String, param2IVoldTaskListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void checkEncryption(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().checkEncryption(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void checkBeforeMount(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().checkBeforeMount(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void moveStorage(String param2String1, String param2String2, IVoldTaskListener param2IVoldTaskListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2IVoldTaskListener != null) {
            iBinder = param2IVoldTaskListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().moveStorage(param2String1, param2String2, param2IVoldTaskListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void remountUid(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().remountUid(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void remountAppStorageDirs(int param2Int1, int param2Int2, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().remountAppStorageDirs(param2Int1, param2Int2, param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setupAppDir(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().setupAppDir(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fixupAppDir(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().fixupAppDir(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String createObb(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            param2String1 = IVold.Stub.getDefaultImpl().createObb(param2String1, param2String2, param2Int);
            return param2String1;
          } 
          parcel2.readException();
          param2String1 = parcel2.readString();
          return param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void destroyObb(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().destroyObb(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fstrim(int param2Int, IVoldTaskListener param2IVoldTaskListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int);
          if (param2IVoldTaskListener != null) {
            iBinder = param2IVoldTaskListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().fstrim(param2Int, param2IVoldTaskListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void runIdleMaint(IVoldTaskListener param2IVoldTaskListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IVold");
          if (param2IVoldTaskListener != null) {
            iBinder = param2IVoldTaskListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().runIdleMaint(param2IVoldTaskListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void abortIdleMaint(IVoldTaskListener param2IVoldTaskListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.os.IVold");
          if (param2IVoldTaskListener != null) {
            iBinder = param2IVoldTaskListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().abortIdleMaint(param2IVoldTaskListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public FileDescriptor mountAppFuse(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null)
            return IVold.Stub.getDefaultImpl().mountAppFuse(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.readRawFileDescriptor();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unmountAppFuse(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().unmountAppFuse(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fdeCheckPassword(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().fdeCheckPassword(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fdeRestart() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().fdeRestart();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int fdeComplete() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null)
            return IVold.Stub.getDefaultImpl().fdeComplete(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fdeEnable(int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().fdeEnable(param2Int1, param2String, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fdeChangePassword(int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().fdeChangePassword(param2Int, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fdeVerifyPassword(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().fdeVerifyPassword(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String fdeGetField(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            param2String = IVold.Stub.getDefaultImpl().fdeGetField(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fdeSetField(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().fdeSetField(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int fdeGetPasswordType() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null)
            return IVold.Stub.getDefaultImpl().fdeGetPasswordType(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String fdeGetPassword() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null)
            return IVold.Stub.getDefaultImpl().fdeGetPassword(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fdeClearPassword() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().fdeClearPassword();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fbeEnable() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().fbeEnable();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void mountDefaultEncrypted() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().mountDefaultEncrypted();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void initUser0() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().initUser0();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isConvertibleToFbe() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(47, parcel1, parcel2, 0);
          if (!bool2 && IVold.Stub.getDefaultImpl() != null) {
            bool1 = IVold.Stub.getDefaultImpl().isConvertibleToFbe();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void mountFstab(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().mountFstab(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void encryptFstab(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().encryptFstab(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void createUserKey(int param2Int1, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool1 && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().createUserKey(param2Int1, param2Int2, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void destroyUserKey(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().destroyUserKey(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addUserKeyAuth(int param2Int1, int param2Int2, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().addUserKeyAuth(param2Int1, param2Int2, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearUserKeyAuth(int param2Int1, int param2Int2, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().clearUserKeyAuth(param2Int1, param2Int2, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fixateNewestUserKeyAuth(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(54, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().fixateNewestUserKeyAuth(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unlockUserKey(int param2Int1, int param2Int2, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().unlockUserKey(param2Int1, param2Int2, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void lockUserKey(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(56, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().lockUserKey(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void prepareUserStorage(String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(57, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().prepareUserStorage(param2String, param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void destroyUserStorage(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(58, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().destroyUserStorage(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void prepareSandboxForApp(String param2String1, int param2Int1, String param2String2, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().prepareSandboxForApp(param2String1, param2Int1, param2String2, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void destroySandboxForApp(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(60, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().destroySandboxForApp(param2String1, param2String2, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startCheckpoint(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().startCheckpoint(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean needsCheckpoint() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(62, parcel1, parcel2, 0);
          if (!bool2 && IVold.Stub.getDefaultImpl() != null) {
            bool1 = IVold.Stub.getDefaultImpl().needsCheckpoint();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean needsRollback() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(63, parcel1, parcel2, 0);
          if (!bool2 && IVold.Stub.getDefaultImpl() != null) {
            bool1 = IVold.Stub.getDefaultImpl().needsRollback();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isCheckpointing() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(64, parcel1, parcel2, 0);
          if (!bool2 && IVold.Stub.getDefaultImpl() != null) {
            bool1 = IVold.Stub.getDefaultImpl().isCheckpointing();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void abortChanges(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(65, parcel1, parcel2, 0);
          if (!bool1 && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().abortChanges(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void commitChanges() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(66, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().commitChanges();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void prepareCheckpoint() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(67, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().prepareCheckpoint();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void restoreCheckpoint(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(68, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().restoreCheckpoint(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void restoreCheckpointPart(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(69, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().restoreCheckpointPart(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void markBootAttempt() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(70, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().markBootAttempt();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean supportsCheckpoint() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(71, parcel1, parcel2, 0);
          if (!bool2 && IVold.Stub.getDefaultImpl() != null) {
            bool1 = IVold.Stub.getDefaultImpl().supportsCheckpoint();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean supportsBlockCheckpoint() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(72, parcel1, parcel2, 0);
          if (!bool2 && IVold.Stub.getDefaultImpl() != null) {
            bool1 = IVold.Stub.getDefaultImpl().supportsBlockCheckpoint();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean supportsFileCheckpoint() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(73, parcel1, parcel2, 0);
          if (!bool2 && IVold.Stub.getDefaultImpl() != null) {
            bool1 = IVold.Stub.getDefaultImpl().supportsFileCheckpoint();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetCheckpoint() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = this.mRemote.transact(74, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().resetCheckpoint();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String createStubVolume(String param2String1, String param2String2, String param2String3, String param2String4, String param2String5, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeString(param2String3);
                try {
                  parcel1.writeString(param2String4);
                  try {
                    parcel1.writeString(param2String5);
                    try {
                      parcel1.writeInt(param2Int);
                      boolean bool = this.mRemote.transact(75, parcel1, parcel2, 0);
                      if (!bool && IVold.Stub.getDefaultImpl() != null) {
                        param2String1 = IVold.Stub.getDefaultImpl().createStubVolume(param2String1, param2String2, param2String3, param2String4, param2String5, param2Int);
                        parcel2.recycle();
                        parcel1.recycle();
                        return param2String1;
                      } 
                      parcel2.readException();
                      param2String1 = parcel2.readString();
                      parcel2.recycle();
                      parcel1.recycle();
                      return param2String1;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void destroyStubVolume(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(76, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().destroyStubVolume(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public FileDescriptor openAppFuseFile(int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(77, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null)
            return IVold.Stub.getDefaultImpl().openAppFuseFile(param2Int1, param2Int2, param2Int3, param2Int4); 
          parcel2.readException();
          return parcel2.readRawFileDescriptor();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean incFsEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(78, parcel1, parcel2, 0);
          if (!bool2 && IVold.Stub.getDefaultImpl() != null) {
            bool1 = IVold.Stub.getDefaultImpl().incFsEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IncrementalFileSystemControlParcel mountIncFs(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(79, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null)
            return IVold.Stub.getDefaultImpl().mountIncFs(param2String1, param2String2, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            IncrementalFileSystemControlParcel incrementalFileSystemControlParcel = IncrementalFileSystemControlParcel.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (IncrementalFileSystemControlParcel)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unmountIncFs(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(80, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().unmountIncFs(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setIncFsMountOptions(IncrementalFileSystemControlParcel param2IncrementalFileSystemControlParcel, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          boolean bool = true;
          if (param2IncrementalFileSystemControlParcel != null) {
            parcel1.writeInt(1);
            param2IncrementalFileSystemControlParcel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(81, parcel1, parcel2, 0);
          if (!bool1 && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().setIncFsMountOptions(param2IncrementalFileSystemControlParcel, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void bindMount(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVold");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(82, parcel1, parcel2, 0);
          if (!bool && IVold.Stub.getDefaultImpl() != null) {
            IVold.Stub.getDefaultImpl().bindMount(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVold param1IVold) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVold != null) {
          Proxy.sDefaultImpl = param1IVold;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVold getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
