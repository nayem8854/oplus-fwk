package android.os;

import android.util.Log;
import java.util.ArrayDeque;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Deprecated
public abstract class AsyncTask<Params, Progress, Result> {
  private static final int BACKUP_POOL_SIZE = 5;
  
  private static final int CORE_POOL_SIZE = 1;
  
  private static final int KEEP_ALIVE_SECONDS = 3;
  
  private static final String LOG_TAG = "AsyncTask";
  
  private static final int MAXIMUM_POOL_SIZE = 20;
  
  private static final int MESSAGE_POST_PROGRESS = 2;
  
  private static final int MESSAGE_POST_RESULT = 1;
  
  @Deprecated
  public static final Executor SERIAL_EXECUTOR;
  
  @Deprecated
  public static final Executor THREAD_POOL_EXECUTOR;
  
  private static ThreadPoolExecutor sBackupExecutor;
  
  private static LinkedBlockingQueue<Runnable> sBackupExecutorQueue;
  
  private static volatile Executor sDefaultExecutor;
  
  private static InternalHandler sHandler;
  
  private static final RejectedExecutionHandler sRunOnSerialPolicy;
  
  private static final ThreadFactory sThreadFactory = new ThreadFactory() {
      private final AtomicInteger mCount = new AtomicInteger(1);
      
      public Thread newThread(Runnable param1Runnable) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("AsyncTask #");
        stringBuilder.append(this.mCount.getAndIncrement());
        return new Thread(param1Runnable, stringBuilder.toString());
      }
    };
  
  private final AtomicBoolean mCancelled;
  
  private final FutureTask<Result> mFuture;
  
  private final Handler mHandler;
  
  private volatile Status mStatus;
  
  private final AtomicBoolean mTaskInvoked;
  
  private final WorkerRunnable<Params, Result> mWorker;
  
  static {
    sRunOnSerialPolicy = new RejectedExecutionHandler() {
        public void rejectedExecution(Runnable param1Runnable, ThreadPoolExecutor param1ThreadPoolExecutor) {
          // Byte code:
          //   0: ldc 'AsyncTask'
          //   2: ldc 'Exceeded ThreadPoolExecutor pool size'
          //   4: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
          //   7: pop
          //   8: aload_0
          //   9: monitorenter
          //   10: invokestatic access$000 : ()Ljava/util/concurrent/ThreadPoolExecutor;
          //   13: ifnonnull -> 65
          //   16: new java/util/concurrent/LinkedBlockingQueue
          //   19: astore_2
          //   20: aload_2
          //   21: invokespecial <init> : ()V
          //   24: aload_2
          //   25: invokestatic access$102 : (Ljava/util/concurrent/LinkedBlockingQueue;)Ljava/util/concurrent/LinkedBlockingQueue;
          //   28: pop
          //   29: new java/util/concurrent/ThreadPoolExecutor
          //   32: astore_3
          //   33: getstatic java/util/concurrent/TimeUnit.SECONDS : Ljava/util/concurrent/TimeUnit;
          //   36: astore_2
          //   37: aload_3
          //   38: iconst_5
          //   39: iconst_5
          //   40: ldc2_w 3
          //   43: aload_2
          //   44: invokestatic access$100 : ()Ljava/util/concurrent/LinkedBlockingQueue;
          //   47: invokestatic access$200 : ()Ljava/util/concurrent/ThreadFactory;
          //   50: invokespecial <init> : (IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V
          //   53: aload_3
          //   54: invokestatic access$002 : (Ljava/util/concurrent/ThreadPoolExecutor;)Ljava/util/concurrent/ThreadPoolExecutor;
          //   57: pop
          //   58: invokestatic access$000 : ()Ljava/util/concurrent/ThreadPoolExecutor;
          //   61: iconst_1
          //   62: invokevirtual allowCoreThreadTimeOut : (Z)V
          //   65: aload_0
          //   66: monitorexit
          //   67: invokestatic access$000 : ()Ljava/util/concurrent/ThreadPoolExecutor;
          //   70: aload_1
          //   71: invokevirtual execute : (Ljava/lang/Runnable;)V
          //   74: return
          //   75: astore_1
          //   76: aload_0
          //   77: monitorexit
          //   78: aload_1
          //   79: athrow
          // Line number table:
          //   Java source line number -> byte code offset
          //   #232	-> 0
          //   #235	-> 8
          //   #236	-> 10
          //   #237	-> 16
          //   #238	-> 29
          //   #240	-> 37
          //   #238	-> 53
          //   #241	-> 58
          //   #243	-> 65
          //   #244	-> 67
          //   #245	-> 74
          //   #243	-> 75
          // Exception table:
          //   from	to	target	type
          //   10	16	75	finally
          //   16	29	75	finally
          //   29	37	75	finally
          //   37	53	75	finally
          //   53	58	75	finally
          //   58	65	75	finally
          //   65	67	75	finally
          //   76	78	75	finally
        }
      };
    ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 20, 3L, TimeUnit.SECONDS, new SynchronousQueue<>(), sThreadFactory);
    threadPoolExecutor.setRejectedExecutionHandler(sRunOnSerialPolicy);
    THREAD_POOL_EXECUTOR = threadPoolExecutor;
    SerialExecutor serialExecutor = new SerialExecutor();
    sDefaultExecutor = serialExecutor;
  }
  
  private static class SerialExecutor implements Executor {
    Runnable mActive;
    
    private SerialExecutor() {}
    
    final ArrayDeque<Runnable> mTasks = new ArrayDeque<>();
    
    public void execute(Runnable param1Runnable) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mTasks : Ljava/util/ArrayDeque;
      //   6: astore_2
      //   7: new android/os/AsyncTask$SerialExecutor$1
      //   10: astore_3
      //   11: aload_3
      //   12: aload_0
      //   13: aload_1
      //   14: invokespecial <init> : (Landroid/os/AsyncTask$SerialExecutor;Ljava/lang/Runnable;)V
      //   17: aload_2
      //   18: aload_3
      //   19: invokevirtual offer : (Ljava/lang/Object;)Z
      //   22: pop
      //   23: aload_0
      //   24: getfield mActive : Ljava/lang/Runnable;
      //   27: ifnonnull -> 34
      //   30: aload_0
      //   31: invokevirtual scheduleNext : ()V
      //   34: aload_0
      //   35: monitorexit
      //   36: return
      //   37: astore_1
      //   38: aload_0
      //   39: monitorexit
      //   40: aload_1
      //   41: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #302	-> 2
      //   #311	-> 23
      //   #312	-> 30
      //   #314	-> 34
      //   #301	-> 37
      // Exception table:
      //   from	to	target	type
      //   2	23	37	finally
      //   23	30	37	finally
      //   30	34	37	finally
    }
    
    protected void scheduleNext() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mTasks : Ljava/util/ArrayDeque;
      //   6: invokevirtual poll : ()Ljava/lang/Object;
      //   9: checkcast java/lang/Runnable
      //   12: astore_1
      //   13: aload_0
      //   14: aload_1
      //   15: putfield mActive : Ljava/lang/Runnable;
      //   18: aload_1
      //   19: ifnull -> 34
      //   22: getstatic android/os/AsyncTask.THREAD_POOL_EXECUTOR : Ljava/util/concurrent/Executor;
      //   25: aload_0
      //   26: getfield mActive : Ljava/lang/Runnable;
      //   29: invokeinterface execute : (Ljava/lang/Runnable;)V
      //   34: aload_0
      //   35: monitorexit
      //   36: return
      //   37: astore_1
      //   38: aload_0
      //   39: monitorexit
      //   40: aload_1
      //   41: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #317	-> 2
      //   #318	-> 22
      //   #320	-> 34
      //   #316	-> 37
      // Exception table:
      //   from	to	target	type
      //   2	18	37	finally
      //   22	34	37	finally
    }
  }
  
  class null implements Runnable {
    final AsyncTask.SerialExecutor this$0;
    
    final Runnable val$r;
    
    public void run() {
      try {
        r.run();
        return;
      } finally {
        this.this$0.scheduleNext();
      } 
    }
  }
  
  public enum Status {
    FINISHED, PENDING, RUNNING;
    
    private static final Status[] $VALUES;
    
    static {
      Status status = new Status("FINISHED", 2);
      $VALUES = new Status[] { PENDING, RUNNING, status };
    }
  }
  
  private static Handler getMainHandler() {
    // Byte code:
    //   0: ldc android/os/AsyncTask
    //   2: monitorenter
    //   3: getstatic android/os/AsyncTask.sHandler : Landroid/os/AsyncTask$InternalHandler;
    //   6: ifnonnull -> 24
    //   9: new android/os/AsyncTask$InternalHandler
    //   12: astore_0
    //   13: aload_0
    //   14: invokestatic getMainLooper : ()Landroid/os/Looper;
    //   17: invokespecial <init> : (Landroid/os/Looper;)V
    //   20: aload_0
    //   21: putstatic android/os/AsyncTask.sHandler : Landroid/os/AsyncTask$InternalHandler;
    //   24: getstatic android/os/AsyncTask.sHandler : Landroid/os/AsyncTask$InternalHandler;
    //   27: astore_0
    //   28: ldc android/os/AsyncTask
    //   30: monitorexit
    //   31: aload_0
    //   32: areturn
    //   33: astore_0
    //   34: ldc android/os/AsyncTask
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #343	-> 0
    //   #344	-> 3
    //   #345	-> 9
    //   #347	-> 24
    //   #348	-> 33
    // Exception table:
    //   from	to	target	type
    //   3	9	33	finally
    //   9	24	33	finally
    //   24	31	33	finally
    //   34	37	33	finally
  }
  
  private Handler getHandler() {
    return this.mHandler;
  }
  
  public static void setDefaultExecutor(Executor paramExecutor) {
    sDefaultExecutor = paramExecutor;
  }
  
  public AsyncTask() {
    this((Looper)null);
  }
  
  public AsyncTask(Handler paramHandler) {
    this((Looper)paramHandler);
  }
  
  public AsyncTask(Looper paramLooper) {
    Handler handler;
    this.mStatus = Status.PENDING;
    this.mCancelled = new AtomicBoolean();
    this.mTaskInvoked = new AtomicBoolean();
    if (paramLooper == null || paramLooper == Looper.getMainLooper()) {
      handler = getMainHandler();
    } else {
      handler = new Handler((Looper)handler);
    } 
    this.mHandler = handler;
    this.mWorker = (WorkerRunnable<Params, Result>)new Object(this);
    this.mFuture = new FutureTask<Result>(this.mWorker) {
        final AsyncTask this$0;
        
        protected void done() {
          try {
            AsyncTask.this.postResultIfNotInvoked(get());
          } catch (InterruptedException interruptedException) {
            Log.w("AsyncTask", interruptedException);
          } catch (ExecutionException executionException) {
            throw new RuntimeException("An error occurred while executing doInBackground()", executionException.getCause());
          } catch (CancellationException cancellationException) {
            AsyncTask.this.postResultIfNotInvoked(null);
          } 
        }
      };
  }
  
  private void postResultIfNotInvoked(Result paramResult) {
    boolean bool = this.mTaskInvoked.get();
    if (!bool)
      postResult(paramResult); 
  }
  
  private Result postResult(Result paramResult) {
    Message message = getHandler().obtainMessage(1, new AsyncTaskResult(this, new Object[] { paramResult }));
    message.sendToTarget();
    return paramResult;
  }
  
  public final Status getStatus() {
    return this.mStatus;
  }
  
  protected void onPreExecute() {}
  
  protected void onPostExecute(Result paramResult) {}
  
  protected void onProgressUpdate(Progress... paramVarArgs) {}
  
  protected void onCancelled(Result paramResult) {
    onCancelled();
  }
  
  protected void onCancelled() {}
  
  public final boolean isCancelled() {
    return this.mCancelled.get();
  }
  
  public final boolean cancel(boolean paramBoolean) {
    this.mCancelled.set(true);
    return this.mFuture.cancel(paramBoolean);
  }
  
  public final Result get() throws InterruptedException, ExecutionException {
    return this.mFuture.get();
  }
  
  public final Result get(long paramLong, TimeUnit paramTimeUnit) throws InterruptedException, ExecutionException, TimeoutException {
    return this.mFuture.get(paramLong, paramTimeUnit);
  }
  
  public final AsyncTask<Params, Progress, Result> execute(Params... paramVarArgs) {
    return executeOnExecutor(sDefaultExecutor, paramVarArgs);
  }
  
  public final AsyncTask<Params, Progress, Result> executeOnExecutor(Executor paramExecutor, Params... paramVarArgs) {
    if (this.mStatus != Status.PENDING) {
      int i = null.$SwitchMap$android$os$AsyncTask$Status[this.mStatus.ordinal()];
      if (i != 1) {
        if (i == 2)
          throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)"); 
      } else {
        throw new IllegalStateException("Cannot execute task: the task is already running.");
      } 
    } 
    this.mStatus = Status.RUNNING;
    onPreExecute();
    this.mWorker.mParams = paramVarArgs;
    paramExecutor.execute(this.mFuture);
    return this;
  }
  
  public static void execute(Runnable paramRunnable) {
    sDefaultExecutor.execute(paramRunnable);
  }
  
  protected final void publishProgress(Progress... paramVarArgs) {
    if (!isCancelled()) {
      Message message = getHandler().obtainMessage(2, new AsyncTaskResult<>(this, paramVarArgs));
      message.sendToTarget();
    } 
  }
  
  private void finish(Result paramResult) {
    if (isCancelled()) {
      onCancelled(paramResult);
    } else {
      onPostExecute(paramResult);
    } 
    this.mStatus = Status.FINISHED;
  }
  
  protected abstract Result doInBackground(Params... paramVarArgs);
  
  class InternalHandler extends Handler {
    public InternalHandler(AsyncTask this$0) {
      super((Looper)this$0);
    }
    
    public void handleMessage(Message param1Message) {
      AsyncTask.AsyncTaskResult asyncTaskResult = (AsyncTask.AsyncTaskResult)param1Message.obj;
      int i = param1Message.what;
      if (i != 1) {
        if (i == 2)
          asyncTaskResult.mTask.onProgressUpdate((Object[])asyncTaskResult.mData); 
      } else {
        asyncTaskResult.mTask.finish((Result)asyncTaskResult.mData[0]);
      } 
    }
  }
  
  private static abstract class WorkerRunnable<Params, Result> implements Callable<Result> {
    Params[] mParams;
    
    private WorkerRunnable() {}
  }
  
  private static class AsyncTaskResult<Data> {
    final Data[] mData;
    
    final AsyncTask mTask;
    
    AsyncTaskResult(AsyncTask param1AsyncTask, Data... param1VarArgs) {
      this.mTask = param1AsyncTask;
      this.mData = param1VarArgs;
    }
  }
}
