package android.os.oplusdevicepolicy;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IOplusDevicePolicyManagerService extends IInterface {
  boolean addList(String paramString, List paramList, int paramInt) throws RemoteException;
  
  boolean clearData(int paramInt) throws RemoteException;
  
  boolean clearList(int paramInt) throws RemoteException;
  
  String getData(String paramString, int paramInt) throws RemoteException;
  
  List<String> getList(String paramString, int paramInt) throws RemoteException;
  
  boolean registerOplusDevicePolicyObserver(String paramString, IOplusDevicePolicyObserver paramIOplusDevicePolicyObserver) throws RemoteException;
  
  boolean removeData(String paramString, int paramInt) throws RemoteException;
  
  boolean removeList(String paramString, int paramInt) throws RemoteException;
  
  boolean removePartListData(String paramString, List paramList, int paramInt) throws RemoteException;
  
  boolean setData(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  boolean setList(String paramString, List paramList, int paramInt) throws RemoteException;
  
  boolean unregisterOplusDevicePolicyObserver(IOplusDevicePolicyObserver paramIOplusDevicePolicyObserver) throws RemoteException;
  
  class Default implements IOplusDevicePolicyManagerService {
    public boolean setData(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean setList(String param1String, List param1List, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean addList(String param1String, List param1List, int param1Int) throws RemoteException {
      return false;
    }
    
    public String getData(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public List<String> getList(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean removeData(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean removeList(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean removePartListData(String param1String, List param1List, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean clearData(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean clearList(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean registerOplusDevicePolicyObserver(String param1String, IOplusDevicePolicyObserver param1IOplusDevicePolicyObserver) throws RemoteException {
      return false;
    }
    
    public boolean unregisterOplusDevicePolicyObserver(IOplusDevicePolicyObserver param1IOplusDevicePolicyObserver) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusDevicePolicyManagerService {
    private static final String DESCRIPTOR = "android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService";
    
    static final int TRANSACTION_addList = 3;
    
    static final int TRANSACTION_clearData = 9;
    
    static final int TRANSACTION_clearList = 10;
    
    static final int TRANSACTION_getData = 4;
    
    static final int TRANSACTION_getList = 5;
    
    static final int TRANSACTION_registerOplusDevicePolicyObserver = 11;
    
    static final int TRANSACTION_removeData = 6;
    
    static final int TRANSACTION_removeList = 7;
    
    static final int TRANSACTION_removePartListData = 8;
    
    static final int TRANSACTION_setData = 1;
    
    static final int TRANSACTION_setList = 2;
    
    static final int TRANSACTION_unregisterOplusDevicePolicyObserver = 12;
    
    public Stub() {
      attachInterface(this, "android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
    }
    
    public static IOplusDevicePolicyManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
      if (iInterface != null && iInterface instanceof IOplusDevicePolicyManagerService)
        return (IOplusDevicePolicyManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 12:
          return "unregisterOplusDevicePolicyObserver";
        case 11:
          return "registerOplusDevicePolicyObserver";
        case 10:
          return "clearList";
        case 9:
          return "clearData";
        case 8:
          return "removePartListData";
        case 7:
          return "removeList";
        case 6:
          return "removeData";
        case 5:
          return "getList";
        case 4:
          return "getData";
        case 3:
          return "addList";
        case 2:
          return "setList";
        case 1:
          break;
      } 
      return "setData";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool9;
        int i3;
        boolean bool8;
        int i2;
        boolean bool7;
        int i1;
        boolean bool6;
        int n;
        boolean bool5;
        int m;
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        IOplusDevicePolicyObserver iOplusDevicePolicyObserver;
        List<String> list;
        String str1;
        ClassLoader classLoader3;
        ArrayList arrayList3;
        ClassLoader classLoader2;
        ArrayList arrayList2;
        ClassLoader classLoader1;
        ArrayList arrayList1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 12:
            param1Parcel1.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
            iOplusDevicePolicyObserver = IOplusDevicePolicyObserver.Stub.asInterface(param1Parcel1.readStrongBinder());
            bool9 = unregisterOplusDevicePolicyObserver(iOplusDevicePolicyObserver);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 11:
            iOplusDevicePolicyObserver.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
            str2 = iOplusDevicePolicyObserver.readString();
            iOplusDevicePolicyObserver = IOplusDevicePolicyObserver.Stub.asInterface(iOplusDevicePolicyObserver.readStrongBinder());
            bool9 = registerOplusDevicePolicyObserver(str2, iOplusDevicePolicyObserver);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 10:
            iOplusDevicePolicyObserver.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
            i3 = iOplusDevicePolicyObserver.readInt();
            bool8 = clearList(i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 9:
            iOplusDevicePolicyObserver.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
            i2 = iOplusDevicePolicyObserver.readInt();
            bool7 = clearData(i2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 8:
            iOplusDevicePolicyObserver.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
            str2 = iOplusDevicePolicyObserver.readString();
            classLoader3 = getClass().getClassLoader();
            arrayList3 = iOplusDevicePolicyObserver.readArrayList(classLoader3);
            i1 = iOplusDevicePolicyObserver.readInt();
            bool6 = removePartListData(str2, arrayList3, i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 7:
            iOplusDevicePolicyObserver.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
            str2 = iOplusDevicePolicyObserver.readString();
            n = iOplusDevicePolicyObserver.readInt();
            bool5 = removeList(str2, n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 6:
            iOplusDevicePolicyObserver.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
            str2 = iOplusDevicePolicyObserver.readString();
            m = iOplusDevicePolicyObserver.readInt();
            bool4 = removeData(str2, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 5:
            iOplusDevicePolicyObserver.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
            str2 = iOplusDevicePolicyObserver.readString();
            k = iOplusDevicePolicyObserver.readInt();
            list = getList(str2, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list);
            return true;
          case 4:
            list.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
            str2 = list.readString();
            k = list.readInt();
            str1 = getData(str2, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 3:
            str1.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
            str2 = str1.readString();
            classLoader2 = getClass().getClassLoader();
            arrayList2 = str1.readArrayList(classLoader2);
            k = str1.readInt();
            bool3 = addList(str2, arrayList2, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 2:
            str1.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
            str2 = str1.readString();
            classLoader1 = getClass().getClassLoader();
            arrayList1 = str1.readArrayList(classLoader1);
            j = str1.readInt();
            bool2 = setList(str2, arrayList1, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
        String str2 = str1.readString();
        String str3 = str1.readString();
        int i = str1.readInt();
        boolean bool1 = setData(str2, str3, i);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
      return true;
    }
    
    private static class Proxy implements IOplusDevicePolicyManagerService {
      public static IOplusDevicePolicyManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService";
      }
      
      public boolean setData(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IOplusDevicePolicyManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusDevicePolicyManagerService.Stub.getDefaultImpl().setData(param2String1, param2String2, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setList(String param2String, List param2List, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
          parcel1.writeString(param2String);
          parcel1.writeList(param2List);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IOplusDevicePolicyManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusDevicePolicyManagerService.Stub.getDefaultImpl().setList(param2String, param2List, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addList(String param2String, List param2List, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
          parcel1.writeString(param2String);
          parcel1.writeList(param2List);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IOplusDevicePolicyManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusDevicePolicyManagerService.Stub.getDefaultImpl().addList(param2String, param2List, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getData(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IOplusDevicePolicyManagerService.Stub.getDefaultImpl() != null) {
            param2String = IOplusDevicePolicyManagerService.Stub.getDefaultImpl().getData(param2String, param2Int);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getList(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IOplusDevicePolicyManagerService.Stub.getDefaultImpl() != null)
            return IOplusDevicePolicyManagerService.Stub.getDefaultImpl().getList(param2String, param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeData(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusDevicePolicyManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusDevicePolicyManagerService.Stub.getDefaultImpl().removeData(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeList(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IOplusDevicePolicyManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusDevicePolicyManagerService.Stub.getDefaultImpl().removeList(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removePartListData(String param2String, List param2List, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
          parcel1.writeString(param2String);
          parcel1.writeList(param2List);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IOplusDevicePolicyManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusDevicePolicyManagerService.Stub.getDefaultImpl().removePartListData(param2String, param2List, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean clearData(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IOplusDevicePolicyManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusDevicePolicyManagerService.Stub.getDefaultImpl().clearData(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean clearList(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IOplusDevicePolicyManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusDevicePolicyManagerService.Stub.getDefaultImpl().clearList(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean registerOplusDevicePolicyObserver(String param2String, IOplusDevicePolicyObserver param2IOplusDevicePolicyObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
          parcel1.writeString(param2String);
          if (param2IOplusDevicePolicyObserver != null) {
            iBinder = param2IOplusDevicePolicyObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IOplusDevicePolicyManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusDevicePolicyManagerService.Stub.getDefaultImpl().registerOplusDevicePolicyObserver(param2String, param2IOplusDevicePolicyObserver);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unregisterOplusDevicePolicyObserver(IOplusDevicePolicyObserver param2IOplusDevicePolicyObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyManagerService");
          if (param2IOplusDevicePolicyObserver != null) {
            iBinder = param2IOplusDevicePolicyObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && IOplusDevicePolicyManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IOplusDevicePolicyManagerService.Stub.getDefaultImpl().unregisterOplusDevicePolicyObserver(param2IOplusDevicePolicyObserver);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusDevicePolicyManagerService param1IOplusDevicePolicyManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusDevicePolicyManagerService != null) {
          Proxy.sDefaultImpl = param1IOplusDevicePolicyManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusDevicePolicyManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
