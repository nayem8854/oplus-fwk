package android.os.oplusdevicepolicy;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IOplusDevicePolicyObserver extends IInterface {
  void onOplusDevicePolicyListUpdate(String paramString, List<String> paramList) throws RemoteException;
  
  void onOplusDevicePolicyValueUpdate(String paramString1, String paramString2) throws RemoteException;
  
  class Default implements IOplusDevicePolicyObserver {
    public void onOplusDevicePolicyListUpdate(String param1String, List<String> param1List) throws RemoteException {}
    
    public void onOplusDevicePolicyValueUpdate(String param1String1, String param1String2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusDevicePolicyObserver {
    private static final String DESCRIPTOR = "android.os.oplusdevicepolicy.IOplusDevicePolicyObserver";
    
    static final int TRANSACTION_onOplusDevicePolicyListUpdate = 1;
    
    static final int TRANSACTION_onOplusDevicePolicyValueUpdate = 2;
    
    public Stub() {
      attachInterface(this, "android.os.oplusdevicepolicy.IOplusDevicePolicyObserver");
    }
    
    public static IOplusDevicePolicyObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyObserver");
      if (iInterface != null && iInterface instanceof IOplusDevicePolicyObserver)
        return (IOplusDevicePolicyObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onOplusDevicePolicyValueUpdate";
      } 
      return "onOplusDevicePolicyListUpdate";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str1;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.os.oplusdevicepolicy.IOplusDevicePolicyObserver");
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyObserver");
        String str = param1Parcel1.readString();
        str1 = param1Parcel1.readString();
        onOplusDevicePolicyValueUpdate(str, str1);
        return true;
      } 
      str1.enforceInterface("android.os.oplusdevicepolicy.IOplusDevicePolicyObserver");
      String str2 = str1.readString();
      ArrayList<String> arrayList = str1.createStringArrayList();
      onOplusDevicePolicyListUpdate(str2, arrayList);
      return true;
    }
    
    private static class Proxy implements IOplusDevicePolicyObserver {
      public static IOplusDevicePolicyObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.oplusdevicepolicy.IOplusDevicePolicyObserver";
      }
      
      public void onOplusDevicePolicyListUpdate(String param2String, List<String> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyObserver");
          parcel.writeString(param2String);
          parcel.writeStringList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IOplusDevicePolicyObserver.Stub.getDefaultImpl() != null) {
            IOplusDevicePolicyObserver.Stub.getDefaultImpl().onOplusDevicePolicyListUpdate(param2String, param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onOplusDevicePolicyValueUpdate(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.oplusdevicepolicy.IOplusDevicePolicyObserver");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IOplusDevicePolicyObserver.Stub.getDefaultImpl() != null) {
            IOplusDevicePolicyObserver.Stub.getDefaultImpl().onOplusDevicePolicyValueUpdate(param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusDevicePolicyObserver param1IOplusDevicePolicyObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusDevicePolicyObserver != null) {
          Proxy.sDefaultImpl = param1IOplusDevicePolicyObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusDevicePolicyObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
