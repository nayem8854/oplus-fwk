package android.os.oplusdevicepolicy;

import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Slog;
import java.util.List;

public class OplusDevicepolicyManager {
  public static final int CUSTOMIZE_DATA_TYPE = 1;
  
  public static final String SERVICE_NAME = "oplusdevicepolicy";
  
  public static final int SYSTEM_DATA_TYPE = 0;
  
  private static final String TAG = "OplusDevicePolicyManager";
  
  private static final Object mLock;
  
  private static final Object mServiceLock = new Object();
  
  private static volatile OplusDevicepolicyManager sInstance;
  
  static {
    mLock = new Object();
    sInstance = null;
  }
  
  private final ArrayMap<OplusDevicePolicyObserver, IOplusDevicePolicyObserver> mOplusDevicePolicyObservers = new ArrayMap();
  
  private IOplusDevicePolicyManagerService mOplusDevicepolicyManagerService;
  
  private OplusDevicepolicyManager() {
    getOplusDevicepolicyManagerService();
  }
  
  public static final OplusDevicepolicyManager getInstance() {
    if (sInstance == null)
      synchronized (mLock) {
        if (sInstance == null) {
          OplusDevicepolicyManager oplusDevicepolicyManager = new OplusDevicepolicyManager();
          this();
          sInstance = oplusDevicepolicyManager;
        } 
        return sInstance;
      }  
    return sInstance;
  }
  
  private IOplusDevicePolicyManagerService getOplusDevicepolicyManagerService() {
    synchronized (mServiceLock) {
      if (this.mOplusDevicepolicyManagerService == null)
        this.mOplusDevicepolicyManagerService = IOplusDevicePolicyManagerService.Stub.asInterface(ServiceManager.getService("oplusdevicepolicy")); 
      return this.mOplusDevicepolicyManagerService;
    } 
  }
  
  public boolean setData(String paramString1, String paramString2, int paramInt) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      getOplusDevicepolicyManagerService();
      if (this.mOplusDevicepolicyManagerService != null) {
        boolean bool2 = this.mOplusDevicepolicyManagerService.setData(paramString1, paramString2, paramInt);
      } else {
        Slog.d("OplusDevicePolicyManager", "mOplusDevicepolicyManagerService is null");
        bool = bool1;
      } 
    } catch (RemoteException remoteException) {
      Slog.e("OplusDevicePolicyManager", "setData fail!", (Throwable)remoteException);
      bool = bool1;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setData Error");
      stringBuilder.append(exception);
      Slog.e("OplusDevicePolicyManager", stringBuilder.toString());
    } 
    return bool;
  }
  
  public String getData(String paramString, int paramInt) {
    StringBuilder stringBuilder1, stringBuilder2 = null;
    String str = null;
    try {
      getOplusDevicepolicyManagerService();
      if (this.mOplusDevicepolicyManagerService != null) {
        paramString = getOplusDevicepolicyManagerService().getData(paramString, paramInt);
      } else {
        Slog.d("OplusDevicePolicyManager", "mOplusDevicepolicyManagerService is null");
        paramString = str;
      } 
    } catch (RemoteException remoteException) {
      Slog.e("OplusDevicePolicyManager", "getData fail!", (Throwable)remoteException);
      String str1 = str;
    } catch (Exception exception) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("getData Error");
      stringBuilder1.append(exception);
      Slog.e("OplusDevicePolicyManager", stringBuilder1.toString());
      stringBuilder1 = stringBuilder2;
    } 
    return (String)stringBuilder1;
  }
  
  public boolean getBoolean(String paramString, int paramInt, boolean paramBoolean) {
    paramString = getData(paramString, paramInt);
    if (paramString == null)
      return paramBoolean; 
    return Boolean.valueOf(paramString).booleanValue();
  }
  
  public boolean setList(String paramString, List paramList, int paramInt) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      getOplusDevicepolicyManagerService();
      if (this.mOplusDevicepolicyManagerService != null) {
        boolean bool2 = getOplusDevicepolicyManagerService().setList(paramString, paramList, paramInt);
      } else {
        Slog.d("OplusDevicePolicyManager", "mOplusDevicepolicyManagerService is null");
        bool = bool1;
      } 
    } catch (RemoteException remoteException) {
      Slog.e("OplusDevicePolicyManager", "setList fail!", (Throwable)remoteException);
      bool = bool1;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setList Error");
      stringBuilder.append(exception);
      Slog.e("OplusDevicePolicyManager", stringBuilder.toString());
    } 
    return bool;
  }
  
  public boolean addList(String paramString, List paramList, int paramInt) {
    boolean bool3, bool1 = false, bool2 = false;
    try {
      getOplusDevicepolicyManagerService();
      if (this.mOplusDevicepolicyManagerService != null) {
        bool3 = getOplusDevicepolicyManagerService().addList(paramString, paramList, paramInt);
      } else {
        Slog.d("OplusDevicePolicyManager", "mOplusDevicepolicyManagerService is null");
        bool3 = bool2;
      } 
    } catch (RemoteException remoteException) {
      Slog.e("OplusDevicePolicyManager", "addList fail!", (Throwable)remoteException);
      bool3 = bool2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addList Error");
      stringBuilder.append(exception);
      Slog.e("OplusDevicePolicyManager", stringBuilder.toString());
      bool3 = bool1;
    } 
    return bool3;
  }
  
  public List<String> getList(String paramString, int paramInt) {
    StringBuilder stringBuilder1, stringBuilder2 = null;
    String str = null;
    try {
      getOplusDevicepolicyManagerService();
      if (this.mOplusDevicepolicyManagerService != null) {
        List<String> list = getOplusDevicepolicyManagerService().getList(paramString, paramInt);
      } else {
        Slog.d("OplusDevicePolicyManager", "mOplusDevicepolicyManagerService is null");
        paramString = str;
      } 
    } catch (RemoteException remoteException) {
      Slog.e("OplusDevicePolicyManager", "getList fail!", (Throwable)remoteException);
      String str1 = str;
    } catch (Exception exception) {
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("getList Error");
      stringBuilder1.append(exception);
      Slog.e("OplusDevicePolicyManager", stringBuilder1.toString());
      stringBuilder1 = stringBuilder2;
    } 
    return (List<String>)stringBuilder1;
  }
  
  public boolean removeData(String paramString, int paramInt) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      getOplusDevicepolicyManagerService();
      if (this.mOplusDevicepolicyManagerService != null) {
        boolean bool2 = getOplusDevicepolicyManagerService().removeData(paramString, paramInt);
      } else {
        Slog.d("OplusDevicePolicyManager", "mOplusDevicepolicyManagerService is null");
      } 
    } catch (RemoteException remoteException) {
      Slog.e("OplusDevicePolicyManager", "removeData fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeData Error");
      stringBuilder.append(exception);
      Slog.e("OplusDevicePolicyManager", stringBuilder.toString());
      bool1 = bool;
    } 
    return bool1;
  }
  
  public boolean removeList(String paramString, int paramInt) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      getOplusDevicepolicyManagerService();
      if (this.mOplusDevicepolicyManagerService != null) {
        boolean bool2 = getOplusDevicepolicyManagerService().removeList(paramString, paramInt);
      } else {
        Slog.d("OplusDevicePolicyManager", "mOplusDevicepolicyManagerService is null");
        bool = bool1;
      } 
    } catch (RemoteException remoteException) {
      Slog.e("OplusDevicePolicyManager", "removeList fail!", (Throwable)remoteException);
      bool = bool1;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeList Error");
      stringBuilder.append(exception);
      Slog.e("OplusDevicePolicyManager", stringBuilder.toString());
    } 
    return bool;
  }
  
  public boolean removePartListData(String paramString, List paramList, int paramInt) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      getOplusDevicepolicyManagerService();
      if (this.mOplusDevicepolicyManagerService != null) {
        boolean bool2 = getOplusDevicepolicyManagerService().removePartListData(paramString, paramList, paramInt);
      } else {
        Slog.d("OplusDevicePolicyManager", "mOplusDevicepolicyManagerService is null");
      } 
    } catch (RemoteException remoteException) {
      Slog.e("OplusDevicePolicyManager", "removePartListData fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removePartListData Error");
      stringBuilder.append(exception);
      Slog.e("OplusDevicePolicyManager", stringBuilder.toString());
      bool1 = bool;
    } 
    return bool1;
  }
  
  public boolean clearData(int paramInt) {
    boolean bool3, bool1 = false, bool2 = false;
    try {
      getOplusDevicepolicyManagerService();
      if (this.mOplusDevicepolicyManagerService != null) {
        bool3 = getOplusDevicepolicyManagerService().clearData(paramInt);
      } else {
        Slog.d("OplusDevicePolicyManager", "mOplusDevicepolicyManagerService is null");
        bool3 = bool2;
      } 
    } catch (RemoteException remoteException) {
      Slog.e("OplusDevicePolicyManager", "clearData fail!", (Throwable)remoteException);
      bool3 = bool2;
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("clearData Error");
      stringBuilder.append(exception);
      Slog.e("OplusDevicePolicyManager", stringBuilder.toString());
      bool3 = bool1;
    } 
    return bool3;
  }
  
  public boolean clearList(int paramInt) {
    boolean bool = false;
    boolean bool1 = false;
    try {
      getOplusDevicepolicyManagerService();
      if (this.mOplusDevicepolicyManagerService != null) {
        boolean bool2 = getOplusDevicepolicyManagerService().clearList(paramInt);
      } else {
        Slog.d("OplusDevicePolicyManager", "mOplusDevicepolicyManagerService is null");
      } 
    } catch (RemoteException remoteException) {
      Slog.e("OplusDevicePolicyManager", "clearList fail!", (Throwable)remoteException);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("clearList Error");
      stringBuilder.append(exception);
      Slog.e("OplusDevicePolicyManager", stringBuilder.toString());
      bool1 = bool;
    } 
    return bool1;
  }
  
  public boolean registerOplusDevicepolicyObserver(String paramString, OplusDevicePolicyObserver paramOplusDevicePolicyObserver) {
    boolean bool1 = false, bool2 = false;
    boolean bool = false;
    if (paramOplusDevicePolicyObserver == null) {
      Log.e("OplusDevicePolicyManager", " registerOplusDevicePolicyObserver null observer");
      return false;
    } 
    synchronized (this.mOplusDevicePolicyObservers) {
      if (this.mOplusDevicePolicyObservers.get(paramOplusDevicePolicyObserver) != null) {
        Log.e("OplusDevicePolicyManager", "already regiter before");
        return false;
      } 
      OplusDevicePolicyObserverDelegate oplusDevicePolicyObserverDelegate = new OplusDevicePolicyObserverDelegate();
      this(this, paramOplusDevicePolicyObserver);
      boolean bool3 = bool1, bool4 = bool2;
      try {
        getOplusDevicepolicyManagerService();
        bool3 = bool1;
        bool4 = bool2;
        if (this.mOplusDevicepolicyManagerService != null) {
          bool3 = bool1;
          bool4 = bool2;
          bool1 = getOplusDevicepolicyManagerService().registerOplusDevicePolicyObserver(paramString, oplusDevicePolicyObserverDelegate);
        } else {
          bool3 = bool1;
          bool4 = bool2;
          Slog.d("OplusDevicePolicyManager", "mOplusDevicepolicyManagerService is null");
          bool1 = bool;
        } 
        if (bool1) {
          bool3 = bool1;
          bool4 = bool1;
          this.mOplusDevicePolicyObservers.put(paramOplusDevicePolicyObserver, oplusDevicePolicyObserverDelegate);
        } 
      } catch (RemoteException remoteException) {
        Slog.e("OplusDevicePolicyManager", "registerOplusDevicePolicyObserver fail!", (Throwable)remoteException);
        bool1 = bool4;
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("registerOplusDevicePolicyObserver Error");
        stringBuilder.append(exception);
        Slog.e("OplusDevicePolicyManager", stringBuilder.toString());
        bool1 = bool3;
      } 
      return bool1;
    } 
  }
  
  public boolean unregisterOplusDevicePolicyObserver(OplusDevicePolicyObserver paramOplusDevicePolicyObserver) {
    boolean bool1 = false, bool2 = false, bool3 = false;
    boolean bool = false;
    if (paramOplusDevicePolicyObserver == null) {
      Log.i("OplusDevicePolicyManager", "unregisterOplusDevicepolicyObserver null observer");
      return false;
    } 
    synchronized (this.mOplusDevicePolicyObservers) {
      IOplusDevicePolicyObserver iOplusDevicePolicyObserver = (IOplusDevicePolicyObserver)this.mOplusDevicePolicyObservers.get(paramOplusDevicePolicyObserver);
      if (iOplusDevicePolicyObserver != null) {
        boolean bool4 = bool1, bool5 = bool2;
        try {
          getOplusDevicepolicyManagerService();
          bool4 = bool1;
          bool5 = bool2;
          if (this.mOplusDevicepolicyManagerService != null) {
            bool4 = bool1;
            bool5 = bool2;
            bool3 = getOplusDevicepolicyManagerService().unregisterOplusDevicePolicyObserver(iOplusDevicePolicyObserver);
          } else {
            bool4 = bool1;
            bool5 = bool2;
            Slog.d("OplusDevicePolicyManager", "mOplusDevicepolicyManagerService is null");
            bool3 = bool;
          } 
          if (bool3) {
            bool4 = bool3;
            bool5 = bool3;
            this.mOplusDevicePolicyObservers.remove(paramOplusDevicePolicyObserver);
          } 
        } catch (RemoteException remoteException) {
          Slog.e("OplusDevicePolicyManager", "unregisterOplusDevicePolicyObserver fail!", (Throwable)remoteException);
          bool3 = bool5;
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("unregisterOplusDevicePolicyObserver Error");
          stringBuilder.append(exception);
          Slog.e("OplusDevicePolicyManager", stringBuilder.toString());
          bool3 = bool4;
        } 
      } 
      return bool3;
    } 
  }
  
  public static interface OplusDevicePolicyObserver {
    void onOplusDevicePolicyUpdate(String param1String1, String param1String2);
    
    void onOplusDevicePolicyUpdate(String param1String, List<String> param1List);
  }
  
  class OplusDevicePolicyObserverDelegate extends IOplusDevicePolicyObserver.Stub {
    private final OplusDevicepolicyManager.OplusDevicePolicyObserver mObserver;
    
    final OplusDevicepolicyManager this$0;
    
    public OplusDevicePolicyObserverDelegate(OplusDevicepolicyManager.OplusDevicePolicyObserver param1OplusDevicePolicyObserver) {
      this.mObserver = param1OplusDevicePolicyObserver;
    }
    
    public void onOplusDevicePolicyListUpdate(String param1String, List<String> param1List) {
      this.mObserver.onOplusDevicePolicyUpdate(param1String, param1List);
    }
    
    public void onOplusDevicePolicyValueUpdate(String param1String1, String param1String2) {
      this.mObserver.onOplusDevicePolicyUpdate(param1String1, param1String2);
    }
  }
}
