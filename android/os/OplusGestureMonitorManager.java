package android.os;

import android.util.ArrayMap;
import android.util.Log;
import android.view.InputEvent;
import java.util.Map;

public class OplusGestureMonitorManager {
  public static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private IOplusExService mExService = null;
  
  private static volatile OplusGestureMonitorManager sInstance = null;
  
  private static final String TAG = "OplusGestureMonitorManager";
  
  private final Map<OnGestureObserver, IOplusGestureCallBack> mGestureObservers;
  
  private final Map<OnPointerEventObserver, IOplusExInputCallBack> mPointersEventObservers;
  
  public static OplusGestureMonitorManager getInstance() {
    // Byte code:
    //   0: getstatic android/os/OplusGestureMonitorManager.sInstance : Landroid/os/OplusGestureMonitorManager;
    //   3: ifnonnull -> 39
    //   6: ldc android/os/OplusGestureMonitorManager
    //   8: monitorenter
    //   9: getstatic android/os/OplusGestureMonitorManager.sInstance : Landroid/os/OplusGestureMonitorManager;
    //   12: ifnonnull -> 27
    //   15: new android/os/OplusGestureMonitorManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic android/os/OplusGestureMonitorManager.sInstance : Landroid/os/OplusGestureMonitorManager;
    //   27: ldc android/os/OplusGestureMonitorManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc android/os/OplusGestureMonitorManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic android/os/OplusGestureMonitorManager.sInstance : Landroid/os/OplusGestureMonitorManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #39	-> 0
    //   #40	-> 6
    //   #41	-> 9
    //   #42	-> 15
    //   #44	-> 27
    //   #46	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusGestureMonitorManager() {
    this.mPointersEventObservers = (Map<OnPointerEventObserver, IOplusExInputCallBack>)new ArrayMap();
    this.mGestureObservers = (Map<OnGestureObserver, IOplusGestureCallBack>)new ArrayMap();
    checkExService();
  }
  
  public boolean registerInputEvent(OnPointerEventObserver paramOnPointerEventObserver) {
    if (DEBUG)
      Log.i("OplusGestureMonitorManager", "start registerInputEvent"); 
    if (paramOnPointerEventObserver == null) {
      Log.e("OplusGestureMonitorManager", "observer is null, registerInputEvent failed");
      return false;
    } 
    synchronized (this.mPointersEventObservers) {
      if (this.mPointersEventObservers.get(paramOnPointerEventObserver) != null) {
        Log.e("OplusGestureMonitorManager", "OnPointerEventObserver already register before");
        return false;
      } 
      checkExService();
      OnPointerEventObserverDelegate onPointerEventObserverDelegate = new OnPointerEventObserverDelegate();
      this(this, paramOnPointerEventObserver);
      try {
        boolean bool = this.mExService.registerInputEvent(onPointerEventObserverDelegate);
        if (bool)
          this.mPointersEventObservers.put(paramOnPointerEventObserver, onPointerEventObserverDelegate); 
        return bool;
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("registerInputEvent failed, e: ");
        stringBuilder.append(remoteException);
        Log.e("OplusGestureMonitorManager", stringBuilder.toString());
        return false;
      } 
    } 
  }
  
  public boolean registerRawInputEvent(OnPointerEventObserver paramOnPointerEventObserver) {
    if (DEBUG)
      Log.i("OplusGestureMonitorManager", "start registerRawInputEvent"); 
    if (paramOnPointerEventObserver == null) {
      Log.e("OplusGestureMonitorManager", "observer is null, registerRawInputEvent failed");
      return false;
    } 
    synchronized (this.mPointersEventObservers) {
      if (this.mPointersEventObservers.get(paramOnPointerEventObserver) != null) {
        Log.e("OplusGestureMonitorManager", "raw OnPointerEventObserver already register before");
        return false;
      } 
      checkExService();
      OnPointerEventObserverDelegate onPointerEventObserverDelegate = new OnPointerEventObserverDelegate();
      this(this, paramOnPointerEventObserver);
      try {
        boolean bool = this.mExService.registerRawInputEvent(onPointerEventObserverDelegate);
        if (bool)
          this.mPointersEventObservers.put(paramOnPointerEventObserver, onPointerEventObserverDelegate); 
        return bool;
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("registerRawInputEvent failed, e: ");
        stringBuilder.append(remoteException);
        Log.e("OplusGestureMonitorManager", stringBuilder.toString());
        return false;
      } 
    } 
  }
  
  public void unregisterInputEvent(OnPointerEventObserver paramOnPointerEventObserver) {
    if (DEBUG)
      Log.i("OplusGestureMonitorManager", "start unregisterInputEvent"); 
    if (paramOnPointerEventObserver == null) {
      Log.e("OplusGestureMonitorManager", "observer is null, unregisterInputEvent failed");
      return;
    } 
    synchronized (this.mPointersEventObservers) {
      IOplusExInputCallBack iOplusExInputCallBack = this.mPointersEventObservers.get(paramOnPointerEventObserver);
      if (iOplusExInputCallBack != null) {
        checkExService();
        try {
          this.mExService.unregisterInputEvent(iOplusExInputCallBack);
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("unregisterInputEvent failed, e: ");
          stringBuilder.append(remoteException);
          Log.e("OplusGestureMonitorManager", stringBuilder.toString());
        } 
      } 
      return;
    } 
  }
  
  public void pauseExInputEvent() {
    checkExService();
    IOplusExService iOplusExService = this.mExService;
    if (iOplusExService != null)
      try {
        iOplusExService.pauseExInputEvent();
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("pauseExInputEvent failed, e: ");
        stringBuilder.append(remoteException);
        Log.e("OplusGestureMonitorManager", stringBuilder.toString());
      }  
  }
  
  public void resumeExInputEvent() {
    checkExService();
    IOplusExService iOplusExService = this.mExService;
    if (iOplusExService != null)
      try {
        iOplusExService.resumeExInputEvent();
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("resumeExInputEvent failed, e: ");
        stringBuilder.append(remoteException);
        Log.e("OplusGestureMonitorManager", stringBuilder.toString());
      }  
  }
  
  public boolean registerScreenoffGesture(OnGestureObserver paramOnGestureObserver) {
    if (DEBUG)
      Log.i("OplusGestureMonitorManager", "start registerScreenoffGesture"); 
    if (paramOnGestureObserver == null) {
      Log.e("OplusGestureMonitorManager", "observer is null, registerScreenoffGesture failed");
      return false;
    } 
    synchronized (this.mGestureObservers) {
      if (this.mGestureObservers.get(paramOnGestureObserver) != null) {
        Log.e("OplusGestureMonitorManager", "OnGestureObserver already register before");
        return false;
      } 
      checkExService();
      OnGestureObserverDelegate onGestureObserverDelegate = new OnGestureObserverDelegate();
      this(this, paramOnGestureObserver);
      try {
        boolean bool = this.mExService.registerScreenoffGesture(onGestureObserverDelegate);
        if (bool)
          this.mGestureObservers.put(paramOnGestureObserver, onGestureObserverDelegate); 
        return bool;
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("registerScreenoffGesture failed, e: ");
        stringBuilder.append(remoteException);
        Log.e("OplusGestureMonitorManager", stringBuilder.toString());
        return false;
      } 
    } 
  }
  
  public void unregisterScreenoffGesture(OnGestureObserver paramOnGestureObserver) {
    if (DEBUG)
      Log.i("OplusGestureMonitorManager", "start unregisterScreenoffGesture"); 
    if (paramOnGestureObserver == null) {
      Log.e("OplusGestureMonitorManager", "observer is null, unregisterScreenoffGesture failed");
      return;
    } 
    synchronized (this.mGestureObservers) {
      IOplusGestureCallBack iOplusGestureCallBack = this.mGestureObservers.get(paramOnGestureObserver);
      if (iOplusGestureCallBack != null) {
        checkExService();
        try {
          this.mExService.unregisterScreenoffGesture(iOplusGestureCallBack);
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("unregisterScreenoffGesture failed, e: ");
          stringBuilder.append(remoteException);
          Log.e("OplusGestureMonitorManager", stringBuilder.toString());
        } 
      } 
      return;
    } 
  }
  
  public void dealScreenoffGesture(int paramInt) {
    checkExService();
    IOplusExService iOplusExService = this.mExService;
    if (iOplusExService != null)
      try {
        iOplusExService.dealScreenoffGesture(paramInt);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("dealScreenoffGesture failed, e: ");
        stringBuilder.append(remoteException);
        Log.e("OplusGestureMonitorManager", stringBuilder.toString());
      }  
  }
  
  public void setGestureState(int paramInt, boolean paramBoolean) {
    checkExService();
    IOplusExService iOplusExService = this.mExService;
    if (iOplusExService != null)
      try {
        iOplusExService.setGestureState(paramInt, paramBoolean);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setGestureState failed, e: ");
        stringBuilder.append(remoteException);
        Log.e("OplusGestureMonitorManager", stringBuilder.toString());
      }  
  }
  
  public boolean getGestureState(int paramInt) {
    checkExService();
    IOplusExService iOplusExService = this.mExService;
    if (iOplusExService != null)
      try {
        return iOplusExService.getGestureState(paramInt);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getGestureState failed, e: ");
        stringBuilder.append(remoteException);
        Log.e("OplusGestureMonitorManager", stringBuilder.toString());
      }  
    return false;
  }
  
  public void pilferPointers() {
    checkExService();
    IOplusExService iOplusExService = this.mExService;
    if (iOplusExService != null)
      try {
        iOplusExService.pilferPointers();
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("pilferPointers failed, e: ");
        stringBuilder.append(remoteException);
        Log.e("OplusGestureMonitorManager", stringBuilder.toString());
      }  
  }
  
  private void checkExService() {
    if (this.mExService == null) {
      IBinder iBinder = ServiceManager.getService("OPPOExService");
      this.mExService = IOplusExService.Stub.asInterface(iBinder);
    } 
  }
  
  class OnPointerEventObserverDelegate extends IOplusExInputCallBack.Stub {
    private OplusGestureMonitorManager.OnPointerEventObserver mObserver;
    
    final OplusGestureMonitorManager this$0;
    
    public OnPointerEventObserverDelegate(OplusGestureMonitorManager.OnPointerEventObserver param1OnPointerEventObserver) {
      if (OplusGestureMonitorManager.DEBUG)
        Log.d("Binder", "new OnPointerEventObserverDelegate"); 
      this.mObserver = param1OnPointerEventObserver;
    }
    
    public void onInputEvent(InputEvent param1InputEvent) {
      this.mObserver.onInputEvent(param1InputEvent);
    }
  }
  
  public static interface OnPointerEventObserver {
    void onInputEvent(InputEvent param1InputEvent);
  }
  
  class OnGestureObserverDelegate extends IOplusGestureCallBack.Stub {
    private OplusGestureMonitorManager.OnGestureObserver mObserver;
    
    final OplusGestureMonitorManager this$0;
    
    public OnGestureObserverDelegate(OplusGestureMonitorManager.OnGestureObserver param1OnGestureObserver) {
      if (OplusGestureMonitorManager.DEBUG)
        Log.d("Binder", "new OnGestureObserverDelegate"); 
      this.mObserver = param1OnGestureObserver;
    }
    
    public void onDealGesture(int param1Int) {
      this.mObserver.onDealGesture(param1Int);
    }
  }
  
  public static interface OnGestureObserver {
    void onDealGesture(int param1Int);
  }
}
