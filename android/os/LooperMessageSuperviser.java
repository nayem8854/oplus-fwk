package android.os;

import android.app.ActivityThread;
import android.text.TextUtils;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class LooperMessageSuperviser {
  private static final String TAG = "LooperMessageSuperviser";
  
  private boolean isTracing = false;
  
  private String getPackageName() {
    String str = ActivityThread.currentPackageName();
    if (str == null)
      return "system_server"; 
    return str;
  }
  
  private boolean isForegroudApp(int paramInt) {
    String str1 = SystemProperties.get("debug.junk.process.pid");
    String str2 = null;
    try {
      String str = Integer.toString(paramInt);
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return str1.equals(str2);
  }
  
  private int getSchedGroup(int paramInt) {
    try {
      return Process.getProcessGroup(paramInt);
    } catch (Exception exception) {
      return Integer.MAX_VALUE;
    } 
  }
  
  private int getThreadSchedulePolicy(int paramInt) {
    try {
      return Process.getThreadScheduler(paramInt);
    } catch (Exception exception) {
      return 0;
    } 
  }
  
  private void uploadLongTimeMessage(long paramLong1, Message paramMessage, long paramLong2, String paramString, int paramInt) {
    StringBuilder stringBuilder2 = new StringBuilder();
    new StringBuilder();
    stringBuilder2.append("Package name: ");
    stringBuilder2.append(getPackageName());
    stringBuilder2.append(" [ ");
    stringBuilder2.append("schedGroup: ");
    stringBuilder2.append(getSchedGroup(paramInt));
    stringBuilder2.append(" schedPolicy: ");
    stringBuilder2.append(getThreadSchedulePolicy(paramInt));
    stringBuilder2.append(" ]");
    stringBuilder2.append(" process the message: ");
    stringBuilder2.append(getStringLiteOfMessage(paramMessage, paramLong1 + paramLong2, true));
    stringBuilder2.append(" took ");
    stringBuilder2.append(paramLong1);
    stringBuilder2.append(" ms");
    if (!TextUtils.isEmpty(paramString)) {
      stringBuilder2.append(" ( ");
      stringBuilder2.append(paramString);
      stringBuilder2.append(" )");
    } 
    stringBuilder2.append("\n");
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Blocked msg = ");
    stringBuilder1.append(stringBuilder2.toString());
    logP("Quality", stringBuilder1.toString());
  }
  
  public void beginLooperMessage(Message paramMessage, int paramInt) {
    if (OplusDebug.DEBUG_SYSTRACE_TAG)
      Trace.traceBegin(8L, paramMessage.target.getTraceName(paramMessage)); 
    boolean bool = isForegroudApp(paramInt);
    if (bool) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("/proc/");
      stringBuilder.append(paramInt);
      stringBuilder.append("/jank_info");
      String str = stringBuilder.toString();
      jankMonitorUtilsFunction_writeProcNode(str, "1");
      this.isTracing = true;
    } 
    OplusTheiaUIMonitor.getInstance().messageBegin(paramMessage, bool);
  }
  
  public void endLooperMessage(Message paramMessage, long paramLong, int paramInt) {
    if (OplusDebug.DEBUG_SYSTRACE_TAG)
      Trace.traceEnd(8L); 
    boolean bool = isForegroudApp(paramInt);
    if (bool) {
      long l = SystemClock.uptimeMillis() - paramLong;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("/proc/");
      stringBuilder.append(paramInt);
      stringBuilder.append("/jank_info");
      String str = stringBuilder.toString();
      if (l >= OplusDebug.LOOPER_DELAY) {
        String str1 = jankMonitorUtilsFunction_readProcNode(str);
        uploadLongTimeMessage(l, paramMessage, paramLong, str1, paramInt);
      } 
      if (this.isTracing)
        jankMonitorUtilsFunction_writeProcNode(str, "0"); 
    } 
    OplusTheiaUIMonitor.getInstance().messageEnd(paramMessage, bool);
  }
  
  private String jankMonitorUtilsFunction_readProcNode(String paramString) {
    return "";
  }
  
  private void jankMonitorUtilsFunction_writeProcNode(String paramString1, String paramString2) {}
  
  private String getStringLiteOfMessage(Message paramMessage, long paramLong, boolean paramBoolean) {
    Class<long> clazz = long.class;
    Class<boolean> clazz1 = boolean.class;
    Object object = callDeclaredMethod(paramMessage, "android.os.Message", "toStringLite", new Class[] { clazz, clazz1 }, new Object[] { Long.valueOf(paramLong), Boolean.valueOf(paramBoolean) });
    if (object != null && object instanceof String)
      return (String)object; 
    return null;
  }
  
  private void logP(String paramString1, String paramString2) {
    callDeclaredMethod(null, "android.util.Log", "p", new Class[] { String.class, String.class }, new Object[] { paramString1, paramString2 });
  }
  
  private Object callDeclaredMethod(Object paramObject, String paramString1, String paramString2, Class[] paramArrayOfClass, Object[] paramArrayOfObject) {
    SecurityException securityException2 = null;
    ClassNotFoundException classNotFoundException = null;
    try {
      Class<?> clazz = Class.forName(paramString1);
      Method method = clazz.getDeclaredMethod(paramString2, paramArrayOfClass);
      method.setAccessible(true);
      paramObject = method.invoke(paramObject, paramArrayOfObject);
    } catch (ClassNotFoundException classNotFoundException1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ClassNotFoundException : ");
      stringBuilder.append(classNotFoundException1.getMessage());
      Log.i("LooperMessageSuperviser", stringBuilder.toString());
      classNotFoundException1.printStackTrace();
      classNotFoundException1 = classNotFoundException;
    } catch (NoSuchMethodException noSuchMethodException) {
      paramObject = new StringBuilder();
      paramObject.append("NoSuchMethodException : ");
      paramObject.append(noSuchMethodException.getMessage());
      Log.i("LooperMessageSuperviser", paramObject.toString());
      noSuchMethodException.printStackTrace();
      paramObject = classNotFoundException;
    } catch (IllegalAccessException illegalAccessException) {
      illegalAccessException.printStackTrace();
      ClassNotFoundException classNotFoundException1 = classNotFoundException;
    } catch (InvocationTargetException invocationTargetException) {
      invocationTargetException.printStackTrace();
      ClassNotFoundException classNotFoundException1 = classNotFoundException;
    } catch (SecurityException securityException1) {
      securityException1.printStackTrace();
      securityException1 = securityException2;
    } 
    return securityException1;
  }
  
  private static native String readNativeProcNode(String paramString);
  
  private static native int writeNativeProcNode(String paramString1, String paramString2);
}
