package android.os;

import android.content.pm.ApplicationInfo;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.util.Log;
import android.util.Pair;
import android.util.Slog;
import com.android.internal.os.Zygote;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ZygoteProcess {
  public ZygoteProcess() {
    this.mZygoteSocketAddress = new LocalSocketAddress("zygote", LocalSocketAddress.Namespace.RESERVED);
    this.mZygoteSecondarySocketAddress = new LocalSocketAddress("zygote_secondary", LocalSocketAddress.Namespace.RESERVED);
    this.mUsapPoolSocketAddress = new LocalSocketAddress("usap_pool_primary", LocalSocketAddress.Namespace.RESERVED);
    this.mUsapPoolSecondarySocketAddress = new LocalSocketAddress("usap_pool_secondary", LocalSocketAddress.Namespace.RESERVED);
    this.mUsapPoolSupported = true;
  }
  
  public ZygoteProcess(LocalSocketAddress paramLocalSocketAddress1, LocalSocketAddress paramLocalSocketAddress2) {
    this.mZygoteSocketAddress = paramLocalSocketAddress1;
    this.mZygoteSecondarySocketAddress = paramLocalSocketAddress2;
    this.mUsapPoolSocketAddress = null;
    this.mUsapPoolSecondarySocketAddress = null;
    this.mUsapPoolSupported = false;
  }
  
  public LocalSocketAddress getPrimarySocketAddress() {
    return this.mZygoteSocketAddress;
  }
  
  private static class ZygoteState implements AutoCloseable {
    private final List<String> mAbiList;
    
    private boolean mClosed;
    
    final LocalSocketAddress mUsapSocketAddress;
    
    final DataInputStream mZygoteInputStream;
    
    final BufferedWriter mZygoteOutputWriter;
    
    private final LocalSocket mZygoteSessionSocket;
    
    final LocalSocketAddress mZygoteSocketAddress;
    
    private ZygoteState(LocalSocketAddress param1LocalSocketAddress1, LocalSocketAddress param1LocalSocketAddress2, LocalSocket param1LocalSocket, DataInputStream param1DataInputStream, BufferedWriter param1BufferedWriter, List<String> param1List) {
      this.mZygoteSocketAddress = param1LocalSocketAddress1;
      this.mUsapSocketAddress = param1LocalSocketAddress2;
      this.mZygoteSessionSocket = param1LocalSocket;
      this.mZygoteInputStream = param1DataInputStream;
      this.mZygoteOutputWriter = param1BufferedWriter;
      this.mAbiList = param1List;
    }
    
    static ZygoteState connect(LocalSocketAddress param1LocalSocketAddress1, LocalSocketAddress param1LocalSocketAddress2) throws IOException {
      LocalSocket localSocket = new LocalSocket();
      if (param1LocalSocketAddress1 != null)
        try {
          localSocket.connect(param1LocalSocketAddress1);
          DataInputStream dataInputStream = new DataInputStream();
          this(localSocket.getInputStream());
          OutputStreamWriter outputStreamWriter = new OutputStreamWriter();
          this(localSocket.getOutputStream());
          BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter, 256);
          return 
            
            new ZygoteState(param1LocalSocketAddress1, param1LocalSocketAddress2, localSocket, dataInputStream, bufferedWriter, ZygoteProcess.getAbiList(bufferedWriter, dataInputStream));
        } catch (IOException iOException) {
          try {
            localSocket.close();
          } catch (IOException iOException1) {}
          throw iOException;
        }  
      throw new IllegalArgumentException("zygoteSocketAddress can't be null");
    }
    
    LocalSocket getUsapSessionSocket() throws IOException {
      LocalSocket localSocket = new LocalSocket();
      localSocket.connect(this.mUsapSocketAddress);
      return localSocket;
    }
    
    boolean matches(String param1String) {
      return this.mAbiList.contains(param1String);
    }
    
    public void close() {
      try {
        this.mZygoteSessionSocket.close();
      } catch (IOException iOException) {
        Log.e("ZygoteProcess", "I/O exception on routine close", iOException);
      } 
      this.mClosed = true;
    }
    
    boolean isClosed() {
      return this.mClosed;
    }
  }
  
  private final Object mLock = new Object();
  
  private List<String> mApiBlacklistExemptions = Collections.emptyList();
  
  private boolean mUsapPoolEnabled = false;
  
  public final Process.ProcessStartResult start(String paramString1, String paramString2, int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3, int paramInt4, int paramInt5, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, int paramInt6, boolean paramBoolean1, long[] paramArrayOflong, Map<String, Pair<String, Long>> paramMap1, Map<String, Pair<String, Long>> paramMap2, boolean paramBoolean2, boolean paramBoolean3, String[] paramArrayOfString) {
    if (fetchUsapPoolEnabledPropWithMinInterval())
      informZygotesOfUsapPoolStatus(); 
    try {
      return startViaZygote(paramString1, paramString2, paramInt1, paramInt2, paramArrayOfint, paramInt3, paramInt4, paramInt5, paramString3, paramString4, paramString5, paramString6, paramString7, false, paramString8, paramInt6, paramBoolean1, paramArrayOflong, paramMap1, paramMap2, paramBoolean2, paramBoolean3, paramArrayOfString);
    } catch (ZygoteStartFailedEx zygoteStartFailedEx) {
      Log.e("ZygoteProcess", "Starting VM process through Zygote failed");
      throw new RuntimeException("Starting VM process through Zygote failed", zygoteStartFailedEx);
    } 
  }
  
  private static List<String> getAbiList(BufferedWriter paramBufferedWriter, DataInputStream paramDataInputStream) throws IOException {
    paramBufferedWriter.write("1");
    paramBufferedWriter.newLine();
    paramBufferedWriter.write("--query-abi-list");
    paramBufferedWriter.newLine();
    paramBufferedWriter.flush();
    int i = paramDataInputStream.readInt();
    byte[] arrayOfByte = new byte[i];
    paramDataInputStream.readFully(arrayOfByte);
    String str = new String(arrayOfByte, StandardCharsets.US_ASCII);
    return Arrays.asList(str.split(","));
  }
  
  private Process.ProcessStartResult zygoteSendArgsAndGetResult(ZygoteState paramZygoteState, int paramInt, ArrayList<String> paramArrayList) throws ZygoteStartFailedEx {
    for (String str1 : paramArrayList) {
      if (str1.indexOf('\n') < 0) {
        if (str1.indexOf('\r') < 0)
          continue; 
        throw new ZygoteStartFailedEx("Embedded carriage returns not allowed");
      } 
      throw new ZygoteStartFailedEx("Embedded newlines not allowed");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramArrayList.size());
    stringBuilder.append("\n");
    stringBuilder.append(String.join("\n", (Iterable)paramArrayList));
    stringBuilder.append("\n");
    String str = stringBuilder.toString();
    if (shouldAttemptUsapLaunch(paramInt, paramArrayList))
      try {
        return attemptUsapSendArgsAndGetResult(paramZygoteState, str);
      } catch (IOException iOException) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("IO Exception while communicating with USAP pool - ");
        stringBuilder1.append(iOException.getMessage());
        String str1 = stringBuilder1.toString();
        Log.e("ZygoteProcess", str1);
      }  
    return attemptZygoteSendArgsAndGetResult(paramZygoteState, str);
  }
  
  private Process.ProcessStartResult attemptZygoteSendArgsAndGetResult(ZygoteState paramZygoteState, String paramString) throws ZygoteStartFailedEx {
    try {
      BufferedWriter bufferedWriter = paramZygoteState.mZygoteOutputWriter;
      DataInputStream dataInputStream = paramZygoteState.mZygoteInputStream;
      bufferedWriter.write(paramString);
      bufferedWriter.flush();
      Process.ProcessStartResult processStartResult = new Process.ProcessStartResult();
      this();
      processStartResult.pid = dataInputStream.readInt();
      processStartResult.usingWrapper = dataInputStream.readBoolean();
      if (processStartResult.pid >= 0)
        return processStartResult; 
      ZygoteStartFailedEx zygoteStartFailedEx = new ZygoteStartFailedEx();
      this("fork() failed");
      throw zygoteStartFailedEx;
    } catch (IOException iOException) {
      paramZygoteState.close();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("IO Exception while communicating with Zygote - ");
      stringBuilder.append(iOException.toString());
      String str = stringBuilder.toString();
      Log.e("ZygoteProcess", str);
      throw new ZygoteStartFailedEx(iOException);
    } 
  }
  
  private Process.ProcessStartResult attemptUsapSendArgsAndGetResult(ZygoteState paramZygoteState, String paramString) throws ZygoteStartFailedEx, IOException {
    LocalSocket localSocket = paramZygoteState.getUsapSessionSocket();
    try {
      BufferedWriter bufferedWriter = new BufferedWriter();
      OutputStreamWriter outputStreamWriter = new OutputStreamWriter();
      this(localSocket.getOutputStream());
      this(outputStreamWriter, 256);
      DataInputStream dataInputStream = new DataInputStream();
      this(localSocket.getInputStream());
      bufferedWriter.write(paramString);
      bufferedWriter.flush();
      Process.ProcessStartResult processStartResult = new Process.ProcessStartResult();
      this();
      processStartResult.pid = dataInputStream.readInt();
      processStartResult.usingWrapper = false;
      int i = processStartResult.pid;
      if (i >= 0)
        return processStartResult; 
      ZygoteStartFailedEx zygoteStartFailedEx = new ZygoteStartFailedEx();
      this("USAP specialization failed");
      throw zygoteStartFailedEx;
    } finally {
      if (localSocket != null)
        try {
          localSocket.close();
        } finally {
          localSocket = null;
        }  
    } 
  }
  
  private boolean shouldAttemptUsapLaunch(int paramInt, ArrayList<String> paramArrayList) {
    if (this.mUsapPoolSupported && this.mUsapPoolEnabled)
      if (policySpecifiesUsapPoolLaunch(paramInt) && 
        commandSupportedByUsap(paramArrayList))
        return true;  
    return false;
  }
  
  private static boolean policySpecifiesUsapPoolLaunch(int paramInt) {
    boolean bool = true;
    if ((paramInt & 0x5) != 1)
      bool = false; 
    return bool;
  }
  
  private static final String[] INVALID_USAP_FLAGS = new String[] { "--query-abi-list", "--get-pid", "--preload-default", "--preload-package", "--preload-app", "--start-child-zygote", "--set-api-blacklist-exemptions", "--hidden-api-log-sampling-rate", "--hidden-api-statslog-sampling-rate", "--invoke-with" };
  
  private static boolean commandSupportedByUsap(ArrayList<String> paramArrayList) {
    for (String str : paramArrayList) {
      for (String str1 : INVALID_USAP_FLAGS) {
        if (str.startsWith(str1))
          return false; 
      } 
      if (str.startsWith("--nice-name="))
        if (Zygote.getWrapProperty(str.substring(12)) != null)
          return false;  
    } 
    return true;
  }
  
  private Process.ProcessStartResult startViaZygote(String paramString1, String paramString2, int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3, int paramInt4, int paramInt5, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, boolean paramBoolean1, String paramString8, int paramInt6, boolean paramBoolean2, long[] paramArrayOflong, Map<String, Pair<String, Long>> paramMap1, Map<String, Pair<String, Long>> paramMap2, boolean paramBoolean3, boolean paramBoolean4, String[] paramArrayOfString) throws ZygoteStartFailedEx {
    ArrayList<String> arrayList = new ArrayList();
    arrayList.add("--runtime-args");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("--setuid=");
    stringBuilder.append(paramInt1);
    arrayList.add(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("--setgid=");
    stringBuilder.append(paramInt2);
    arrayList.add(stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("--runtime-flags=");
    stringBuilder.append(paramInt3);
    arrayList.add(stringBuilder.toString());
    if (paramInt4 == 1) {
      arrayList.add("--mount-external-default");
    } else if (paramInt4 == 2) {
      arrayList.add("--mount-external-read");
    } else if (paramInt4 == 3) {
      arrayList.add("--mount-external-write");
    } else if (paramInt4 == 6) {
      arrayList.add("--mount-external-full");
    } else if (paramInt4 == 5) {
      arrayList.add("--mount-external-installer");
    } else if (paramInt4 == 4) {
      arrayList.add("--mount-external-legacy");
    } else if (paramInt4 == 7) {
      arrayList.add("--mount-external-pass-through");
    } else if (paramInt4 == 8) {
      arrayList.add("--mount-external-android-writable");
    } else if (paramInt4 == 9) {
      arrayList.add("--mount-external-oppo-android-writable");
    } 
    stringBuilder = new StringBuilder();
    stringBuilder.append("--target-sdk-version=");
    stringBuilder.append(paramInt5);
    arrayList.add(stringBuilder.toString());
    if (paramArrayOfint != null && paramArrayOfint.length > 0) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("--setgroups=");
      paramInt2 = paramArrayOfint.length;
      for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
        if (paramInt1 != 0)
          stringBuilder.append(','); 
        stringBuilder.append(paramArrayOfint[paramInt1]);
      } 
      arrayList.add(stringBuilder.toString());
    } 
    if (paramString2 != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("--nice-name=");
      stringBuilder1.append(paramString2);
      arrayList.add(stringBuilder1.toString());
    } 
    if (paramString3 != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("--seinfo=");
      stringBuilder1.append(paramString3);
      arrayList.add(stringBuilder1.toString());
    } 
    if (paramString5 != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("--instruction-set=");
      stringBuilder1.append(paramString5);
      arrayList.add(stringBuilder1.toString());
    } 
    if (paramString6 != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("--app-data-dir=");
      stringBuilder1.append(paramString6);
      arrayList.add(stringBuilder1.toString());
    } 
    if (paramString7 != null) {
      arrayList.add("--invoke-with");
      arrayList.add(paramString7);
    } 
    if (paramBoolean1)
      arrayList.add("--start-child-zygote"); 
    if (paramString8 != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("--package-name=");
      stringBuilder1.append(paramString8);
      arrayList.add(stringBuilder1.toString());
    } 
    if (paramBoolean2)
      arrayList.add("--is-top-app"); 
    if (paramMap1 != null && paramMap1.size() > 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("--pkg-data-info-map");
      stringBuilder1.append("=");
      paramInt1 = 0;
      for (Map.Entry<String, Pair<String, Long>> entry : paramMap1.entrySet()) {
        if (paramInt1 != 0)
          stringBuilder1.append(','); 
        paramInt1 = 1;
        stringBuilder1.append((String)entry.getKey());
        stringBuilder1.append(',');
        stringBuilder1.append((String)((Pair)entry.getValue()).first);
        stringBuilder1.append(',');
        stringBuilder1.append(((Pair)entry.getValue()).second);
      } 
      arrayList.add(stringBuilder1.toString());
    } 
    if (paramMap2 != null && paramMap2.size() > 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("--whitelisted-data-info-map");
      stringBuilder1.append("=");
      paramInt1 = 0;
      for (Map.Entry<String, Pair<String, Long>> entry : paramMap2.entrySet()) {
        if (paramInt1 != 0)
          stringBuilder1.append(','); 
        stringBuilder1.append((String)entry.getKey());
        stringBuilder1.append(',');
        stringBuilder1.append((String)((Pair)entry.getValue()).first);
        stringBuilder1.append(',');
        stringBuilder1.append(((Pair)entry.getValue()).second);
        paramInt1 = 1;
      } 
      arrayList.add(stringBuilder1.toString());
    } 
    if (paramBoolean4)
      arrayList.add("--bind-mount-storage-dirs"); 
    if (paramBoolean3)
      arrayList.add("--bind-mount-data-dirs"); 
    if (paramArrayOflong != null && paramArrayOflong.length > 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("--disabled-compat-changes=");
      paramInt2 = paramArrayOflong.length;
      for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
        if (paramInt1 != 0)
          stringBuilder1.append(','); 
        stringBuilder1.append(paramArrayOflong[paramInt1]);
      } 
      arrayList.add(stringBuilder1.toString());
    } 
    arrayList.add(paramString1);
    if (paramArrayOfString != null)
      Collections.addAll(arrayList, paramArrayOfString); 
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      ZygoteState zygoteState = openZygoteSocketIfNeeded(paramString4);
      try {
        Process.ProcessStartResult processStartResult = zygoteSendArgsAndGetResult(zygoteState, paramInt6, arrayList);
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        return processStartResult;
      } finally {}
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    throw paramString1;
  }
  
  private boolean fetchUsapPoolEnabledProp() {
    boolean bool1;
    boolean bool = this.mUsapPoolEnabled;
    String str = Zygote.getConfigurationProperty("usap_pool_enabled", "false");
    if (!str.isEmpty()) {
      bool1 = Boolean.parseBoolean("false");
      this.mUsapPoolEnabled = Zygote.getConfigurationPropertyBoolean("usap_pool_enabled", Boolean.valueOf(bool1));
    } 
    if (bool != this.mUsapPoolEnabled) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (bool1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("usapPoolEnabled = ");
      stringBuilder.append(this.mUsapPoolEnabled);
      Log.i("ZygoteProcess", stringBuilder.toString());
    } 
    return bool1;
  }
  
  private boolean mIsFirstPropCheck = true;
  
  private long mLastPropCheckTimestamp = 0L;
  
  private static final String LOG_TAG = "ZygoteProcess";
  
  private static final String USAP_POOL_ENABLED_DEFAULT = "false";
  
  private static final int ZYGOTE_CONNECT_RETRY_DELAY_MS = 50;
  
  private static final int ZYGOTE_CONNECT_TIMEOUT_MS = 20000;
  
  static final int ZYGOTE_RETRY_MILLIS = 500;
  
  private int mHiddenApiAccessLogSampleRate;
  
  private int mHiddenApiAccessStatslogSampleRate;
  
  private final LocalSocketAddress mUsapPoolSecondarySocketAddress;
  
  private final LocalSocketAddress mUsapPoolSocketAddress;
  
  private final boolean mUsapPoolSupported;
  
  private final LocalSocketAddress mZygoteSecondarySocketAddress;
  
  private final LocalSocketAddress mZygoteSocketAddress;
  
  private ZygoteState primaryZygoteState;
  
  private ZygoteState secondaryZygoteState;
  
  private boolean fetchUsapPoolEnabledPropWithMinInterval() {
    if (!this.mUsapPoolSupported)
      return false; 
    long l = SystemClock.elapsedRealtime();
    if (this.mIsFirstPropCheck || l - this.mLastPropCheckTimestamp >= 60000L) {
      this.mIsFirstPropCheck = false;
      this.mLastPropCheckTimestamp = l;
      return fetchUsapPoolEnabledProp();
    } 
    return false;
  }
  
  public void close() {
    ZygoteState zygoteState = this.primaryZygoteState;
    if (zygoteState != null)
      zygoteState.close(); 
    zygoteState = this.secondaryZygoteState;
    if (zygoteState != null)
      zygoteState.close(); 
  }
  
  public void establishZygoteConnectionForAbi(String paramString) {
    try {
      synchronized (this.mLock) {
        openZygoteSocketIfNeeded(paramString);
        return;
      } 
    } catch (ZygoteStartFailedEx zygoteStartFailedEx) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to connect to zygote for abi: ");
      stringBuilder.append(paramString);
      throw new RuntimeException(stringBuilder.toString(), zygoteStartFailedEx);
    } 
  }
  
  public int getZygotePid(String paramString) {
    try {
      synchronized (this.mLock) {
        ZygoteState zygoteState = openZygoteSocketIfNeeded(paramString);
        zygoteState.mZygoteOutputWriter.write("1");
        zygoteState.mZygoteOutputWriter.newLine();
        zygoteState.mZygoteOutputWriter.write("--get-pid");
        zygoteState.mZygoteOutputWriter.newLine();
        zygoteState.mZygoteOutputWriter.flush();
        int i = zygoteState.mZygoteInputStream.readInt();
        byte[] arrayOfByte = new byte[i];
        zygoteState.mZygoteInputStream.readFully(arrayOfByte);
        String str = new String();
        this(arrayOfByte, StandardCharsets.US_ASCII);
        i = Integer.parseInt(str);
        return i;
      } 
    } catch (Exception exception) {
      throw new RuntimeException("Failure retrieving pid", exception);
    } 
  }
  
  public void bootCompleted() {
    if (Build.SUPPORTED_32_BIT_ABIS.length > 0)
      bootCompleted(Build.SUPPORTED_32_BIT_ABIS[0]); 
    if (Build.SUPPORTED_64_BIT_ABIS.length > 0)
      bootCompleted(Build.SUPPORTED_64_BIT_ABIS[0]); 
  }
  
  private void bootCompleted(String paramString) {
    try {
      synchronized (this.mLock) {
        ZygoteState zygoteState = openZygoteSocketIfNeeded(paramString);
        zygoteState.mZygoteOutputWriter.write("1\n--boot-completed\n");
        zygoteState.mZygoteOutputWriter.flush();
        zygoteState.mZygoteInputStream.readInt();
        return;
      } 
    } catch (Exception exception) {
      throw new RuntimeException("Failed to inform zygote of boot_completed", exception);
    } 
  }
  
  public boolean setApiBlacklistExemptions(List<String> paramList) {
    synchronized (this.mLock) {
      this.mApiBlacklistExemptions = paramList;
      boolean bool1 = maybeSetApiBlacklistExemptions(this.primaryZygoteState, true);
      boolean bool2 = bool1;
      if (bool1)
        bool2 = maybeSetApiBlacklistExemptions(this.secondaryZygoteState, true); 
      return bool2;
    } 
  }
  
  public void setHiddenApiAccessLogSampleRate(int paramInt) {
    synchronized (this.mLock) {
      this.mHiddenApiAccessLogSampleRate = paramInt;
      maybeSetHiddenApiAccessLogSampleRate(this.primaryZygoteState);
      maybeSetHiddenApiAccessLogSampleRate(this.secondaryZygoteState);
      return;
    } 
  }
  
  public void setHiddenApiAccessStatslogSampleRate(int paramInt) {
    synchronized (this.mLock) {
      this.mHiddenApiAccessStatslogSampleRate = paramInt;
      maybeSetHiddenApiAccessStatslogSampleRate(this.primaryZygoteState);
      maybeSetHiddenApiAccessStatslogSampleRate(this.secondaryZygoteState);
      return;
    } 
  }
  
  private boolean maybeSetApiBlacklistExemptions(ZygoteState paramZygoteState, boolean paramBoolean) {
    if (paramZygoteState == null || paramZygoteState.isClosed()) {
      Slog.e("ZygoteProcess", "Can't set API blacklist exemptions: no zygote connection");
      return false;
    } 
    if (!paramBoolean && this.mApiBlacklistExemptions.isEmpty())
      return true; 
    try {
      paramZygoteState.mZygoteOutputWriter.write(Integer.toString(this.mApiBlacklistExemptions.size() + 1));
      paramZygoteState.mZygoteOutputWriter.newLine();
      paramZygoteState.mZygoteOutputWriter.write("--set-api-blacklist-exemptions");
      paramZygoteState.mZygoteOutputWriter.newLine();
      int i;
      for (i = 0; i < this.mApiBlacklistExemptions.size(); i++) {
        paramZygoteState.mZygoteOutputWriter.write(this.mApiBlacklistExemptions.get(i));
        paramZygoteState.mZygoteOutputWriter.newLine();
      } 
      paramZygoteState.mZygoteOutputWriter.flush();
      i = paramZygoteState.mZygoteInputStream.readInt();
      if (i != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to set API blacklist exemptions; status ");
        stringBuilder.append(i);
        Slog.e("ZygoteProcess", stringBuilder.toString());
      } 
      return true;
    } catch (IOException iOException) {
      Slog.e("ZygoteProcess", "Failed to set API blacklist exemptions", iOException);
      this.mApiBlacklistExemptions = Collections.emptyList();
      return false;
    } 
  }
  
  private void maybeSetHiddenApiAccessLogSampleRate(ZygoteState paramZygoteState) {
    if (paramZygoteState == null || paramZygoteState.isClosed() || this.mHiddenApiAccessLogSampleRate == -1)
      return; 
    try {
      paramZygoteState.mZygoteOutputWriter.write(Integer.toString(1));
      paramZygoteState.mZygoteOutputWriter.newLine();
      BufferedWriter bufferedWriter = paramZygoteState.mZygoteOutputWriter;
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("--hidden-api-log-sampling-rate=");
      stringBuilder.append(this.mHiddenApiAccessLogSampleRate);
      bufferedWriter.write(stringBuilder.toString());
      paramZygoteState.mZygoteOutputWriter.newLine();
      paramZygoteState.mZygoteOutputWriter.flush();
      int i = paramZygoteState.mZygoteInputStream.readInt();
      if (i != 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Failed to set hidden API log sampling rate; status ");
        stringBuilder1.append(i);
        Slog.e("ZygoteProcess", stringBuilder1.toString());
      } 
    } catch (IOException iOException) {
      Slog.e("ZygoteProcess", "Failed to set hidden API log sampling rate", iOException);
    } 
  }
  
  private void maybeSetHiddenApiAccessStatslogSampleRate(ZygoteState paramZygoteState) {
    if (paramZygoteState == null || paramZygoteState.isClosed() || this.mHiddenApiAccessStatslogSampleRate == -1)
      return; 
    try {
      paramZygoteState.mZygoteOutputWriter.write(Integer.toString(1));
      paramZygoteState.mZygoteOutputWriter.newLine();
      BufferedWriter bufferedWriter = paramZygoteState.mZygoteOutputWriter;
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("--hidden-api-statslog-sampling-rate=");
      stringBuilder.append(this.mHiddenApiAccessStatslogSampleRate);
      bufferedWriter.write(stringBuilder.toString());
      paramZygoteState.mZygoteOutputWriter.newLine();
      paramZygoteState.mZygoteOutputWriter.flush();
      int i = paramZygoteState.mZygoteInputStream.readInt();
      if (i != 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Failed to set hidden API statslog sampling rate; status ");
        stringBuilder1.append(i);
        Slog.e("ZygoteProcess", stringBuilder1.toString());
      } 
    } catch (IOException iOException) {
      Slog.e("ZygoteProcess", "Failed to set hidden API statslog sampling rate", iOException);
    } 
  }
  
  private void attemptConnectionToPrimaryZygote() throws IOException {
    ZygoteState zygoteState = this.primaryZygoteState;
    if (zygoteState == null || zygoteState.isClosed()) {
      LocalSocketAddress localSocketAddress1 = this.mZygoteSocketAddress, localSocketAddress2 = this.mUsapPoolSocketAddress;
      ZygoteState zygoteState1 = ZygoteState.connect(localSocketAddress1, localSocketAddress2);
      maybeSetApiBlacklistExemptions(zygoteState1, false);
      maybeSetHiddenApiAccessLogSampleRate(this.primaryZygoteState);
    } 
  }
  
  private void attemptConnectionToSecondaryZygote() throws IOException {
    ZygoteState zygoteState = this.secondaryZygoteState;
    if (zygoteState == null || zygoteState.isClosed()) {
      LocalSocketAddress localSocketAddress1 = this.mZygoteSecondarySocketAddress, localSocketAddress2 = this.mUsapPoolSecondarySocketAddress;
      ZygoteState zygoteState1 = ZygoteState.connect(localSocketAddress1, localSocketAddress2);
      maybeSetApiBlacklistExemptions(zygoteState1, false);
      maybeSetHiddenApiAccessLogSampleRate(this.secondaryZygoteState);
    } 
  }
  
  private ZygoteState openZygoteSocketIfNeeded(String paramString) throws ZygoteStartFailedEx {
    try {
      attemptConnectionToPrimaryZygote();
      if (this.primaryZygoteState.matches(paramString))
        return this.primaryZygoteState; 
      if (this.mZygoteSecondarySocketAddress != null) {
        attemptConnectionToSecondaryZygote();
        if (this.secondaryZygoteState.matches(paramString))
          return this.secondaryZygoteState; 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported zygote ABI: ");
      stringBuilder.append(paramString);
      throw new ZygoteStartFailedEx(stringBuilder.toString());
    } catch (IOException iOException) {
      throw new ZygoteStartFailedEx("Error connecting to zygote", iOException);
    } 
  }
  
  public boolean preloadApp(ApplicationInfo paramApplicationInfo, String paramString) throws ZygoteStartFailedEx, IOException {
    synchronized (this.mLock) {
      ZygoteState zygoteState = openZygoteSocketIfNeeded(paramString);
      zygoteState.mZygoteOutputWriter.write("2");
      zygoteState.mZygoteOutputWriter.newLine();
      zygoteState.mZygoteOutputWriter.write("--preload-app");
      zygoteState.mZygoteOutputWriter.newLine();
      Parcel parcel = Parcel.obtain();
      boolean bool = false;
      paramApplicationInfo.writeToParcel(parcel, 0);
      String str = Base64.getEncoder().encodeToString(parcel.marshall());
      parcel.recycle();
      zygoteState.mZygoteOutputWriter.write(str);
      zygoteState.mZygoteOutputWriter.newLine();
      zygoteState.mZygoteOutputWriter.flush();
      if (zygoteState.mZygoteInputStream.readInt() == 0)
        bool = true; 
      return bool;
    } 
  }
  
  public boolean preloadPackageForAbi(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5) throws ZygoteStartFailedEx, IOException {
    synchronized (this.mLock) {
      boolean bool;
      ZygoteState zygoteState = openZygoteSocketIfNeeded(paramString5);
      zygoteState.mZygoteOutputWriter.write("5");
      zygoteState.mZygoteOutputWriter.newLine();
      zygoteState.mZygoteOutputWriter.write("--preload-package");
      zygoteState.mZygoteOutputWriter.newLine();
      zygoteState.mZygoteOutputWriter.write(paramString1);
      zygoteState.mZygoteOutputWriter.newLine();
      zygoteState.mZygoteOutputWriter.write(paramString2);
      zygoteState.mZygoteOutputWriter.newLine();
      zygoteState.mZygoteOutputWriter.write(paramString3);
      zygoteState.mZygoteOutputWriter.newLine();
      zygoteState.mZygoteOutputWriter.write(paramString4);
      zygoteState.mZygoteOutputWriter.newLine();
      zygoteState.mZygoteOutputWriter.flush();
      if (zygoteState.mZygoteInputStream.readInt() == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
  }
  
  public boolean preloadDefault(String paramString) throws ZygoteStartFailedEx, IOException {
    synchronized (this.mLock) {
      boolean bool;
      ZygoteState zygoteState = openZygoteSocketIfNeeded(paramString);
      zygoteState.mZygoteOutputWriter.write("1");
      zygoteState.mZygoteOutputWriter.newLine();
      zygoteState.mZygoteOutputWriter.write("--preload-default");
      zygoteState.mZygoteOutputWriter.newLine();
      zygoteState.mZygoteOutputWriter.flush();
      if (zygoteState.mZygoteInputStream.readInt() == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
  }
  
  public static void waitForConnectionToZygote(String paramString) {
    LocalSocketAddress localSocketAddress = new LocalSocketAddress(paramString, LocalSocketAddress.Namespace.RESERVED);
    waitForConnectionToZygote(localSocketAddress);
  }
  
  public static void waitForConnectionToZygote(LocalSocketAddress paramLocalSocketAddress) {
    for (char c = 'Ɛ'; c >= '\000'; c--) {
      try {
        ZygoteState zygoteState = ZygoteState.connect(paramLocalSocketAddress, null);
        zygoteState.close();
        return;
      } catch (IOException iOException) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Got error connecting to zygote, retrying. msg= ");
        stringBuilder1.append(iOException.getMessage());
        String str1 = stringBuilder1.toString();
        Log.w("ZygoteProcess", str1);
        try {
          Thread.sleep(50L);
        } catch (InterruptedException interruptedException) {}
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to connect to Zygote through socket ");
    stringBuilder.append(paramLocalSocketAddress.getName());
    String str = stringBuilder.toString();
    Slog.wtf("ZygoteProcess", str);
  }
  
  private void informZygotesOfUsapPoolStatus() {
    Exception exception;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("1\n--usap-pool-enabled=");
    stringBuilder.append(this.mUsapPoolEnabled);
    stringBuilder.append("\n");
    String str = stringBuilder.toString();
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      attemptConnectionToPrimaryZygote();
      this.primaryZygoteState.mZygoteOutputWriter.write(str);
      this.primaryZygoteState.mZygoteOutputWriter.flush();
      LocalSocketAddress localSocketAddress = this.mZygoteSecondarySocketAddress;
      if (localSocketAddress != null)
        try {
          attemptConnectionToSecondaryZygote();
          try {
            this.secondaryZygoteState.mZygoteOutputWriter.write(str);
            this.secondaryZygoteState.mZygoteOutputWriter.flush();
            this.secondaryZygoteState.mZygoteInputStream.readInt();
          } catch (IOException iOException) {
            exception = new IllegalStateException();
            this("USAP pool state change cause an irrecoverable error", iOException);
            throw exception;
          } 
        } catch (IOException null) {} 
      try {
        this.primaryZygoteState.mZygoteInputStream.readInt();
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        return;
      } catch (IOException null) {
        IllegalStateException illegalStateException = new IllegalStateException();
        this("USAP pool state change cause an irrecoverable error", exception);
        throw illegalStateException;
      } 
    } catch (IOException iOException) {
      boolean bool;
      if (!this.mUsapPoolEnabled) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mUsapPoolEnabled = bool;
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append("Failed to inform zygotes of USAP pool status: ");
      stringBuilder1.append(iOException.getMessage());
      String str1 = stringBuilder1.toString();
      Log.w("ZygoteProcess", str1);
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      return;
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    throw exception;
  }
  
  public ChildZygoteProcess startChildZygote(String paramString1, String paramString2, int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3, String paramString3, String paramString4, String paramString5, String paramString6, int paramInt4, int paramInt5) {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString1);
    stringBuilder1.append("/");
    stringBuilder1.append(UUID.randomUUID().toString());
    LocalSocketAddress localSocketAddress = new LocalSocketAddress(stringBuilder1.toString());
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("--zygote-socket=");
    stringBuilder2.append(localSocketAddress.getName());
    String str1 = stringBuilder2.toString();
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append("--abi-list=");
    stringBuilder3.append(paramString5);
    paramString5 = stringBuilder3.toString();
    stringBuilder3 = new StringBuilder();
    stringBuilder3.append("--uid-range-start=");
    stringBuilder3.append(paramInt4);
    String str2 = stringBuilder3.toString();
    StringBuilder stringBuilder4 = new StringBuilder();
    stringBuilder4.append("--uid-range-end=");
    stringBuilder4.append(paramInt5);
    String str3 = stringBuilder4.toString();
    try {
      Process.ProcessStartResult processStartResult = startViaZygote(paramString1, paramString2, paramInt1, paramInt2, paramArrayOfint, paramInt3, 0, 0, paramString3, paramString4, paramString6, null, null, true, null, 4, false, null, null, null, true, false, new String[] { str1, paramString5, str2, str3 });
      return new ChildZygoteProcess(localSocketAddress, processStartResult.pid);
    } catch (ZygoteStartFailedEx zygoteStartFailedEx) {
      throw new RuntimeException("Starting child-zygote through Zygote failed", zygoteStartFailedEx);
    } 
  }
}
