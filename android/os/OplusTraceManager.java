package android.os;

import android.app.OplusActivityManager;
import android.system.ErrnoException;
import android.util.Log;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class OplusTraceManager extends IOplusFilterListener.Stub {
  private static final TraceBuffer ASYNC_TRACE_BUFFER;
  
  private static final ThreadLocal<Integer> ATTACHED_THREAD;
  
  private static final int BUFFER_SAFE_SIZE = 128;
  
  private static final long CACHE_EXPIRED_TIME_MILLIS = 30000L;
  
  private static final boolean DEBUG;
  
  private static final long FLUSH_PERIOD_DEFAULT_TIME_MILLIS;
  
  private static final long FLUSH_PERIOD_MIN_TIME_MILLIS;
  
  private static final OplusTraceManager INSTANCE;
  
  public static final String ONETRACE_SERVICE = "onetrace";
  
  private static final ScheduledExecutorService SCHEDULED_SINGLE_EXECUTOR;
  
  private static final String SYS_LOG_TAG = "persist.onetrace.logtag";
  
  private static final String TAG = "OplusTraceManager";
  
  private static final ThreadLocal<TraceBuffer> THREAD_BUFFER;
  
  private static final int THREAD_BUFFER_DEFAULT_CAPACITY = 8192;
  
  private static final int THREAD_BUFFER_MIN_CAPACITY = 4096;
  
  private static final String THREAD_BUFFER_SIZE_PROP = "persist.onetrace.thread.buffersize";
  
  private static final String THREAD_FLUSH_TIME_PROP = "persist.onetrace.thread.flushtime";
  
  public static final String TRACE_SWITCH_FLAG = "debug.onetrace.tag";
  
  private static final long UPDATE_FILTER_PERIOD_TIME_MILLIS = 20000L;
  
  private static ContentFilter sContentFilter;
  
  private static volatile IOplusTraceService sRemoteService;
  
  private ByteBufferOutputStream mByteBufferOutput;
  
  private SharedMemory mCurrentSharedMem;
  
  private Future<?> mFilterTaskFuture;
  
  private long mLastFlushCacheElapsedTime;
  
  private int mLastProcessTreeHash;
  
  private volatile ProcessTreeRecorder mProcessRecorder;
  
  private final Handler mWorkHandler;
  
  public static OplusTraceManager getInstance() {
    if (!INSTANCE.isValid())
      return null; 
    if (!binderService())
      return null; 
    return INSTANCE;
  }
  
  private OplusTraceManager() {
    Handler handler1 = null;
    this.mFilterTaskFuture = null;
    this.mLastProcessTreeHash = -1;
    this.mLastFlushCacheElapsedTime = 0L;
    boolean bool = false;
    HandlerThread handlerThread1 = null;
    HandlerThread handlerThread2 = handlerThread1;
    try {
      HandlerThread handlerThread = new HandlerThread();
      handlerThread2 = handlerThread1;
      this("OplusTraceManager");
      handlerThread2 = handlerThread;
      handlerThread.start();
      bool = true;
      handlerThread2 = handlerThread;
    } catch (InternalError internalError) {
      Log.w("OplusTraceManager", "Failed to start handler thread!", internalError);
      SystemProperties.set("debug.onetrace.tag", "0");
    } 
    Handler handler2 = handler1;
    if (bool)
      handler2 = new Handler(handlerThread2.getLooper()); 
    this.mWorkHandler = handler2;
  }
  
  private void init() {
    Future<?> future = this.mFilterTaskFuture;
    if (future != null)
      future.cancel(false); 
    this.mFilterTaskFuture = SCHEDULED_SINGLE_EXECUTOR.scheduleAtFixedRate(new _$$Lambda$OplusTraceManager$SejLcgfoVxlKEtSJtFYoUGMyCuo(this), 20000L, 20000L, TimeUnit.MILLISECONDS);
    this.mLastFlushCacheElapsedTime = SystemClock.elapsedRealtime();
  }
  
  public static void oneTraceBegin(String paramString) {
    if (((Integer)ATTACHED_THREAD.get()).intValue() >= 0)
      ((TraceBuffer)THREAD_BUFFER.get()).begin(paramString); 
  }
  
  public static void oneTraceEnd() {
    if (((Integer)ATTACHED_THREAD.get()).intValue() >= 0)
      ((TraceBuffer)THREAD_BUFFER.get()).end(sContentFilter); 
  }
  
  public static void oneTraceBeginAsync(String paramString, int paramInt) {
    int i = ((Integer)ATTACHED_THREAD.get()).intValue();
    if (i >= 0)
      ASYNC_TRACE_BUFFER.asyncBegin(SCHEDULED_SINGLE_EXECUTOR, paramString, paramInt); 
  }
  
  public static void oneTraceEndAsync(String paramString, int paramInt) {
    int i = ((Integer)ATTACHED_THREAD.get()).intValue();
    if (i >= 0)
      ASYNC_TRACE_BUFFER.asyncEnd(SCHEDULED_SINGLE_EXECUTOR, paramString, paramInt, i, sContentFilter); 
  }
  
  public void onFilterChanged(int paramInt, Map<String, Integer> paramMap) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Filter changed: hashCode=");
      stringBuilder.append(paramInt);
      stringBuilder.append(", pid=");
      stringBuilder.append(Process.myPid());
      Log.d("OplusTraceManager", stringBuilder.toString());
    } 
    if (paramMap == null || paramMap.isEmpty()) {
      sContentFilter = null;
      return;
    } 
    sContentFilter = new ContentFilter(paramInt, paramMap);
  }
  
  private boolean isValid() {
    boolean bool;
    if (this.mWorkHandler != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private static boolean binderService() {
    if (sRemoteService != null)
      return true; 
    IBinder iBinder = ServiceManager.getService("onetrace");
    sRemoteService = IOplusTraceService.Stub.asInterface(iBinder);
    if (sRemoteService == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("binder one trace service failed. pid = ");
      stringBuilder.append(Process.myPid());
      stringBuilder.append(", tid = ");
      stringBuilder.append(Process.myTid());
      String str = stringBuilder.toString();
      Log.w("OplusTraceManager", str);
      return false;
    } 
    INSTANCE.init();
    return true;
  }
  
  private void attachThread(int paramInt, String paramString) {
    if (this.mProcessRecorder == null)
      this.mProcessRecorder = new ProcessTreeRecorder(Process.myPid()); 
    this.mProcessRecorder.attachThread(paramInt, paramString);
  }
  
  private void insertSerializedDataAsQueue(byte[] paramArrayOfbyte) {
    this.mWorkHandler.post(new _$$Lambda$OplusTraceManager$n0rRPSCxd55aQ0srj7uD3eEI_s8(this, paramArrayOfbyte));
  }
  
  private void writeTraceData(byte[] paramArrayOfbyte) {
    String str;
    if (!checkActive()) {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to find active shared memory in client.Pid=");
        stringBuilder.append(Process.myPid());
        stringBuilder.append(". The data with len=");
        stringBuilder.append(paramArrayOfbyte.length);
        stringBuilder.append(" is loss.");
        str = stringBuilder.toString();
        Log.i("OplusTraceManager", str);
      } 
      return;
    } 
    ByteBufferOutputStream byteBufferOutputStream = this.mByteBufferOutput;
    Objects.requireNonNull(byteBufferOutputStream);
    int i = byteBufferOutputStream.getCapacity();
    int j = str.length;
    if (i < j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to write trace with size:");
      stringBuilder.append(j);
      stringBuilder.append(" into main buffer with capacity:");
      stringBuilder.append(i);
      stringBuilder.append(". Pid = ");
      stringBuilder.append(Process.myPid());
      str = stringBuilder.toString();
      Log.e("OplusTraceManager", str);
      return;
    } 
    int k = this.mByteBufferOutput.mOffset;
    int m = 1;
    if (i <= k + j + 128) {
      i = 1;
    } else {
      i = 0;
    } 
    if (SystemClock.elapsedRealtime() - this.mLastFlushCacheElapsedTime <= 30000L)
      m = 0; 
    if (k > 0 && (i != 0 || m)) {
      try {
        sRemoteService.handleTraceShmemBuffer(this.mCurrentSharedMem, k);
        this.mLastFlushCacheElapsedTime = SystemClock.elapsedRealtime();
        if (DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Send sharedMemory obj to service, Pid = ");
          stringBuilder.append(Process.myPid());
          Log.d("OplusTraceManager", stringBuilder.toString());
        } 
        i = this.mProcessRecorder.getHashCode();
        if (i != this.mLastProcessTreeHash) {
          IOplusTraceService iOplusTraceService = sRemoteService;
          m = this.mProcessRecorder.getPid();
          ProcessTreeRecorder processTreeRecorder1 = this.mProcessRecorder;
          String str1 = processTreeRecorder1.getProcessName();
          ProcessTreeRecorder processTreeRecorder2 = this.mProcessRecorder;
          Map<Integer, String> map = processTreeRecorder2.getThreadMap();
          iOplusTraceService.uploadProcessTree(m, str1, map);
          if (DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Upload process tree, Pid = ");
            stringBuilder.append(Process.myPid());
            Log.d("OplusTraceManager", stringBuilder.toString());
          } 
          this.mLastProcessTreeHash = i;
        } 
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      } 
      deactivate();
    } 
    if (!checkActive()) {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to find active shared memory in client. Pid=");
        stringBuilder.append(Process.myPid());
        stringBuilder.append(". The data with len=");
        stringBuilder.append(str.length);
        stringBuilder.append(" is loss.");
        str = stringBuilder.toString();
        Log.i("OplusTraceManager", str);
      } 
      return;
    } 
    try {
      this.mByteBufferOutput.write((byte[])str);
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Write trace to Process cache with size:");
        stringBuilder.append(j);
        Log.d("OplusTraceManager", stringBuilder.toString());
      } 
    } catch (IOException iOException) {
      Log.w("OplusTraceManager", "Failed to write trace to Process cache", iOException);
    } 
  }
  
  private boolean checkActive() {
    if (this.mCurrentSharedMem != null && this.mByteBufferOutput != null)
      return true; 
    deactivate();
    try {
      int i = getContentHashCode();
      IOplusTraceService iOplusTraceService = sRemoteService;
      String str = getProcessName(Process.myPid());
      SharedMemory sharedMemory = iOplusTraceService.obtainMemoryCache(str, i, (IOplusFilterListener)this);
      if (sharedMemory == null) {
        Log.e("OplusTraceManager", "Failed to obtain main shared memory from pool");
        return false;
      } 
      try {
        if (this.mByteBufferOutput != null)
          this.mByteBufferOutput.close(); 
        ByteBufferOutputStream byteBufferOutputStream = new ByteBufferOutputStream();
        this(this.mCurrentSharedMem.mapReadWrite());
        this.mByteBufferOutput = byteBufferOutputStream;
        return true;
      } catch (ErrnoException errnoException) {
        deactivate();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to call mmap from shared memory! Pid = ");
        stringBuilder.append(Process.myPid());
        String str1 = stringBuilder.toString();
        Log.e("OplusTraceManager", str1, (Throwable)errnoException);
        return false;
      } 
    } catch (RemoteException remoteException) {
      deactivate();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to obtain shared memory from remote service! Pid = ");
      stringBuilder.append(Process.myPid());
      String str = stringBuilder.toString();
      Log.e("OplusTraceManager", str, (Throwable)remoteException);
      return false;
    } 
  }
  
  private void deactivate() {
    ByteBufferOutputStream byteBufferOutputStream = this.mByteBufferOutput;
    if (byteBufferOutputStream != null) {
      byteBufferOutputStream.close();
      this.mByteBufferOutput = null;
    } 
    SharedMemory sharedMemory = this.mCurrentSharedMem;
    if (sharedMemory != null) {
      sharedMemory.close();
      this.mCurrentSharedMem = null;
    } 
  }
  
  private static int getTraceBufferCapacity() {
    int i = SystemProperties.getInt("persist.onetrace.thread.buffersize", 8192);
    return Math.max(i, 4096);
  }
  
  private static long getTraceBufferFlushPeriod() {
    long l = SystemProperties.getLong("persist.onetrace.thread.flushtime", FLUSH_PERIOD_DEFAULT_TIME_MILLIS);
    return Math.max(l, FLUSH_PERIOD_MIN_TIME_MILLIS);
  }
  
  private String getProcessName(int paramInt) {
    try {
      OplusActivityManager oplusActivityManager = new OplusActivityManager();
      this();
      return oplusActivityManager.getProcCmdline(new int[] { paramInt }).get(0);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to get process name with pid=");
      stringBuilder.append(Process.myPid());
      Log.w("OplusTraceManager", stringBuilder.toString(), exception);
      return "";
    } 
  }
  
  private int getContentHashCode() {
    int i;
    ContentFilter contentFilter = sContentFilter;
    if (contentFilter == null) {
      i = 0;
    } else {
      i = contentFilter.getHashCode();
    } 
    return i;
  }
  
  static {
    // Byte code:
    //   0: iconst_0
    //   1: istore_0
    //   2: ldc 'persist.sys.assert.panic'
    //   4: iconst_0
    //   5: invokestatic getBoolean : (Ljava/lang/String;Z)Z
    //   8: ifne -> 20
    //   11: ldc 'persist.onetrace.logtag'
    //   13: iconst_0
    //   14: invokestatic getBoolean : (Ljava/lang/String;Z)Z
    //   17: ifeq -> 22
    //   20: iconst_1
    //   21: istore_0
    //   22: iload_0
    //   23: putstatic android/os/OplusTraceManager.DEBUG : Z
    //   26: getstatic java/util/concurrent/TimeUnit.SECONDS : Ljava/util/concurrent/TimeUnit;
    //   29: astore_1
    //   30: aload_1
    //   31: ldc2_w 10
    //   34: invokevirtual toMillis : (J)J
    //   37: putstatic android/os/OplusTraceManager.FLUSH_PERIOD_DEFAULT_TIME_MILLIS : J
    //   40: getstatic java/util/concurrent/TimeUnit.SECONDS : Ljava/util/concurrent/TimeUnit;
    //   43: ldc2_w 5
    //   46: invokevirtual toMillis : (J)J
    //   49: putstatic android/os/OplusTraceManager.FLUSH_PERIOD_MIN_TIME_MILLIS : J
    //   52: getstatic android/os/_$$Lambda$OplusTraceManager$2eOmSMfY6U0bjztdMqqHgPjxUHc.INSTANCE : Landroid/os/-$$Lambda$OplusTraceManager$2eOmSMfY6U0bjztdMqqHgPjxUHc;
    //   55: astore_1
    //   56: aload_1
    //   57: invokestatic newSingleThreadScheduledExecutor : (Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;
    //   60: putstatic android/os/OplusTraceManager.SCHEDULED_SINGLE_EXECUTOR : Ljava/util/concurrent/ScheduledExecutorService;
    //   63: getstatic android/os/_$$Lambda$OplusTraceManager$g6OlJpo9jfaRpUzu0Y0tdNKlgng.INSTANCE : Landroid/os/-$$Lambda$OplusTraceManager$g6OlJpo9jfaRpUzu0Y0tdNKlgng;
    //   66: astore_1
    //   67: aload_1
    //   68: invokestatic withInitial : (Ljava/util/function/Supplier;)Ljava/lang/ThreadLocal;
    //   71: putstatic android/os/OplusTraceManager.THREAD_BUFFER : Ljava/lang/ThreadLocal;
    //   74: getstatic android/os/_$$Lambda$OplusTraceManager$uQeK9Nk5ohBn5kLHxhW_A4CjjtQ.INSTANCE : Landroid/os/-$$Lambda$OplusTraceManager$uQeK9Nk5ohBn5kLHxhW-A4CjjtQ;
    //   77: astore_1
    //   78: aload_1
    //   79: invokestatic withInitial : (Ljava/util/function/Supplier;)Ljava/lang/ThreadLocal;
    //   82: putstatic android/os/OplusTraceManager.ATTACHED_THREAD : Ljava/lang/ThreadLocal;
    //   85: new android/os/OplusTraceManager
    //   88: dup
    //   89: invokespecial <init> : ()V
    //   92: putstatic android/os/OplusTraceManager.INSTANCE : Landroid/os/OplusTraceManager;
    //   95: aconst_null
    //   96: putstatic android/os/OplusTraceManager.sContentFilter : Landroid/os/ContentFilter;
    //   99: new android/os/TraceBuffer
    //   102: dup
    //   103: invokestatic getTraceBufferFlushPeriod : ()J
    //   106: invokestatic getTraceBufferCapacity : ()I
    //   109: invokespecial <init> : (JI)V
    //   112: astore_1
    //   113: aload_1
    //   114: putstatic android/os/OplusTraceManager.ASYNC_TRACE_BUFFER : Landroid/os/TraceBuffer;
    //   117: aload_1
    //   118: getstatic android/os/_$$Lambda$OplusTraceManager$5k_VZ6FU_8YVMuLTLE0_c1D8I6s.INSTANCE : Landroid/os/-$$Lambda$OplusTraceManager$5k_VZ6FU_8YVMuLTLE0_c1D8I6s;
    //   121: invokevirtual setFlushActionObserver : (Landroid/os/TraceBuffer$FlushOutObserver;)V
    //   124: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #46	-> 0
    //   #48	-> 11
    //   #61	-> 26
    //   #62	-> 30
    //   #64	-> 40
    //   #66	-> 52
    //   #67	-> 56
    //   #70	-> 63
    //   #71	-> 67
    //   #83	-> 74
    //   #84	-> 78
    //   #94	-> 85
    //   #106	-> 95
    //   #111	-> 99
    //   #112	-> 99
    //   #113	-> 117
    //   #119	-> 124
  }
  
  private static final class ByteBufferOutputStream extends OutputStream {
    private final ByteBuffer mByteBuffer;
    
    private int mOffset = 0;
    
    private byte[] mSingleByte;
    
    public ByteBufferOutputStream(ByteBuffer param1ByteBuffer) {
      this.mByteBuffer = param1ByteBuffer;
    }
    
    public int getCapacity() {
      return this.mByteBuffer.limit();
    }
    
    public void write(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) {
      writeBytes(param1ArrayOfbyte, param1Int1, this.mOffset, param1Int2);
      this.mOffset += param1Int2;
    }
    
    public void write(int param1Int) {
      if (this.mSingleByte == null)
        this.mSingleByte = new byte[1]; 
      byte[] arrayOfByte = this.mSingleByte;
      arrayOfByte[0] = (byte)param1Int;
      write(arrayOfByte, 0, 1);
    }
    
    private void writeBytes(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2, int param1Int3) {
      this.mByteBuffer.put(param1ArrayOfbyte, param1Int1, param1Int3);
    }
    
    public void close() {
      SharedMemory.unmap(this.mByteBuffer);
    }
  }
}
