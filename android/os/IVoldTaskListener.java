package android.os;

public interface IVoldTaskListener extends IInterface {
  void onFinished(int paramInt, PersistableBundle paramPersistableBundle) throws RemoteException;
  
  void onStatus(int paramInt, PersistableBundle paramPersistableBundle) throws RemoteException;
  
  class Default implements IVoldTaskListener {
    public void onStatus(int param1Int, PersistableBundle param1PersistableBundle) throws RemoteException {}
    
    public void onFinished(int param1Int, PersistableBundle param1PersistableBundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVoldTaskListener {
    private static final String DESCRIPTOR = "android.os.IVoldTaskListener";
    
    static final int TRANSACTION_onFinished = 2;
    
    static final int TRANSACTION_onStatus = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IVoldTaskListener");
    }
    
    public static IVoldTaskListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IVoldTaskListener");
      if (iInterface != null && iInterface instanceof IVoldTaskListener)
        return (IVoldTaskListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onFinished";
      } 
      return "onStatus";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.os.IVoldTaskListener");
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.IVoldTaskListener");
        param1Int1 = param1Parcel1.readInt();
        if (param1Parcel1.readInt() != 0) {
          PersistableBundle persistableBundle = PersistableBundle.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onFinished(param1Int1, (PersistableBundle)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IVoldTaskListener");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        PersistableBundle persistableBundle = PersistableBundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onStatus(param1Int1, (PersistableBundle)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IVoldTaskListener {
      public static IVoldTaskListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IVoldTaskListener";
      }
      
      public void onStatus(int param2Int, PersistableBundle param2PersistableBundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldTaskListener");
          parcel.writeInt(param2Int);
          if (param2PersistableBundle != null) {
            parcel.writeInt(1);
            param2PersistableBundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IVoldTaskListener.Stub.getDefaultImpl() != null) {
            IVoldTaskListener.Stub.getDefaultImpl().onStatus(param2Int, param2PersistableBundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFinished(int param2Int, PersistableBundle param2PersistableBundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IVoldTaskListener");
          parcel.writeInt(param2Int);
          if (param2PersistableBundle != null) {
            parcel.writeInt(1);
            param2PersistableBundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IVoldTaskListener.Stub.getDefaultImpl() != null) {
            IVoldTaskListener.Stub.getDefaultImpl().onFinished(param2Int, param2PersistableBundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVoldTaskListener param1IVoldTaskListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVoldTaskListener != null) {
          Proxy.sDefaultImpl = param1IVoldTaskListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVoldTaskListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
