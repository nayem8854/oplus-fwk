package android.os;

import android.content.Context;

public class DeviceIdleManager {
  private final Context mContext;
  
  private final IDeviceIdleController mService;
  
  public DeviceIdleManager(Context paramContext, IDeviceIdleController paramIDeviceIdleController) {
    this.mContext = paramContext;
    this.mService = paramIDeviceIdleController;
  }
  
  IDeviceIdleController getService() {
    return this.mService;
  }
  
  public String[] getSystemPowerWhitelistExceptIdle() {
    try {
      return this.mService.getSystemPowerWhitelistExceptIdle();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String[] getSystemPowerWhitelist() {
    try {
      return this.mService.getSystemPowerWhitelist();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
