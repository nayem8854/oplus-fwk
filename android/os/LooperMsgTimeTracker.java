package android.os;

import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class LooperMsgTimeTracker {
  private static final long DISPATCH_TIMEOUT = 1500L;
  
  private static final String TAG = "ANR_LOG";
  
  private Message mCurrentMsg;
  
  private long mStartTime;
  
  public void start(Message paramMessage) {
    this.mStartTime = SystemClock.uptimeMillis();
    this.mCurrentMsg = paramMessage;
  }
  
  public void stop() {
    long l = SystemClock.uptimeMillis() - this.mStartTime;
    if (l >= 1500L)
      dumpMsgListWhenAnr(l); 
  }
  
  private void dumpMsgListWhenAnr(long paramLong) {
    try {
      String str1 = getStringLiteOfMessage(this.mCurrentMsg, this.mStartTime + paramLong, true);
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Blocked msg = ");
      stringBuilder.append(str1);
      stringBuilder.append(" , cost  = ");
      stringBuilder.append(paramLong);
      stringBuilder.append(" ms");
      String str2 = stringBuilder.toString();
      Log.e("ANR_LOG", ">>> msg's executing time is too long");
      Log.e("ANR_LOG", str2);
      byte b = 0;
      Log.e("ANR_LOG", ">>>Current msg List is:");
      MessageQueue messageQueue = (Looper.myLooper()).mQueue;
      Message message = messageQueue.mMessages;
      b++;
      for (; message != null && b <= 10; message = message.next) {
        str1 = getStringLiteOfMessage(message, this.mStartTime + paramLong, true);
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Current msg <");
        stringBuilder1.append(b);
        stringBuilder1.append("> = ");
        stringBuilder1.append(str1);
        str1 = stringBuilder1.toString();
        Log.e("ANR_LOG", str1);
      } 
      Log.e("ANR_LOG", ">>>CURRENT MSG DUMP OVER<<<");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failure log ANR msg.");
      stringBuilder.append(exception);
      Log.e("ANR_LOG", stringBuilder.toString());
    } 
  }
  
  private String getStringLiteOfMessage(Message paramMessage, long paramLong, boolean paramBoolean) {
    Class<long> clazz = long.class;
    Class<boolean> clazz1 = boolean.class;
    Object object = callDeclaredMethod(paramMessage, "android.os.Message", "toStringLite", new Class[] { clazz, clazz1 }, new Object[] { Long.valueOf(paramLong), Boolean.valueOf(paramBoolean) });
    if (object != null && object instanceof String)
      return (String)object; 
    return null;
  }
  
  private Object callDeclaredMethod(Object paramObject, String paramString1, String paramString2, Class[] paramArrayOfClass, Object[] paramArrayOfObject) {
    SecurityException securityException2 = null;
    NoSuchMethodException noSuchMethodException = null;
    try {
      Class<?> clazz = Class.forName(paramString1);
      Method method = clazz.getDeclaredMethod(paramString2, paramArrayOfClass);
      method.setAccessible(true);
      paramObject = method.invoke(paramObject, paramArrayOfObject);
    } catch (ClassNotFoundException classNotFoundException) {
      paramObject = new StringBuilder();
      paramObject.append("ClassNotFoundException : ");
      paramObject.append(classNotFoundException.getMessage());
      Log.i("ANR_LOG", paramObject.toString());
      classNotFoundException.printStackTrace();
      paramObject = noSuchMethodException;
    } catch (NoSuchMethodException noSuchMethodException1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("NoSuchMethodException : ");
      stringBuilder.append(noSuchMethodException1.getMessage());
      Log.i("ANR_LOG", stringBuilder.toString());
      noSuchMethodException1.printStackTrace();
      noSuchMethodException1 = noSuchMethodException;
    } catch (IllegalAccessException illegalAccessException) {
      illegalAccessException.printStackTrace();
      NoSuchMethodException noSuchMethodException1 = noSuchMethodException;
    } catch (InvocationTargetException invocationTargetException) {
      invocationTargetException.printStackTrace();
      NoSuchMethodException noSuchMethodException1 = noSuchMethodException;
    } catch (SecurityException securityException1) {
      securityException1.printStackTrace();
      securityException1 = securityException2;
    } 
    return securityException1;
  }
}
