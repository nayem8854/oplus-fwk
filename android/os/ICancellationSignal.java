package android.os;

public interface ICancellationSignal extends IInterface {
  void cancel() throws RemoteException;
  
  class Default implements ICancellationSignal {
    public void cancel() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICancellationSignal {
    private static final String DESCRIPTOR = "android.os.ICancellationSignal";
    
    static final int TRANSACTION_cancel = 1;
    
    public Stub() {
      attachInterface(this, "android.os.ICancellationSignal");
    }
    
    public static ICancellationSignal asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.ICancellationSignal");
      if (iInterface != null && iInterface instanceof ICancellationSignal)
        return (ICancellationSignal)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "cancel";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.ICancellationSignal");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.ICancellationSignal");
      cancel();
      return true;
    }
    
    private static class Proxy implements ICancellationSignal {
      public static ICancellationSignal sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.ICancellationSignal";
      }
      
      public void cancel() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.ICancellationSignal");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ICancellationSignal.Stub.getDefaultImpl() != null) {
            ICancellationSignal.Stub.getDefaultImpl().cancel();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICancellationSignal param1ICancellationSignal) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICancellationSignal != null) {
          Proxy.sDefaultImpl = param1ICancellationSignal;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICancellationSignal getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
