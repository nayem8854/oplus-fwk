package android.os;

import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public abstract class FileObserver {
  public static final int ACCESS = 1;
  
  public static final int ALL_EVENTS = 4095;
  
  public static final int ATTRIB = 4;
  
  public static final int CLOSE_NOWRITE = 16;
  
  public static final int CLOSE_WRITE = 8;
  
  public static final int CREATE = 256;
  
  public static final int DELETE = 512;
  
  public static final int DELETE_SELF = 1024;
  
  private static final String LOG_TAG = "FileObserver";
  
  public static final int MODIFY = 2;
  
  public static final int MOVED_FROM = 64;
  
  public static final int MOVED_TO = 128;
  
  public static final int MOVE_SELF = 2048;
  
  public static final int OPEN = 32;
  
  private static ObserverThread s_observerThread;
  
  private int[] mDescriptors;
  
  private final List<File> mFiles;
  
  private final int mMask;
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface NotifyEventType {}
  
  private static class ObserverThread extends Thread {
    private int m_fd;
    
    private HashMap<Integer, WeakReference> m_observers = new HashMap<>();
    
    public ObserverThread() {
      super("FileObserver");
      this.m_fd = init();
    }
    
    public void run() {
      observe(this.m_fd);
    }
    
    public int[] startWatching(List<File> param1List, int param1Int, FileObserver param1FileObserver) {
      int i = param1List.size();
      String[] arrayOfString = new String[i];
      int j;
      for (j = 0; j < i; j++)
        arrayOfString[j] = ((File)param1List.get(j)).getAbsolutePath(); 
      null = new int[i];
      Arrays.fill(null, -1);
      startWatching(this.m_fd, arrayOfString, param1Int, null);
      WeakReference<FileObserver> weakReference = new WeakReference<>(param1FileObserver);
      synchronized (this.m_observers) {
        for (j = null.length, param1Int = 0; param1Int < j; ) {
          i = null[param1Int];
          if (i >= 0)
            this.m_observers.put(Integer.valueOf(i), weakReference); 
          param1Int++;
        } 
        return null;
      } 
    }
    
    public void stopWatching(int[] param1ArrayOfint) {
      stopWatching(this.m_fd, param1ArrayOfint);
    }
    
    public void onEvent(int param1Int1, int param1Int2, String param1String) {
      FileObserver fileObserver = null;
      synchronized (this.m_observers) {
        WeakReference<FileObserver> weakReference = this.m_observers.get(Integer.valueOf(param1Int1));
        if (weakReference != null) {
          FileObserver fileObserver1 = weakReference.get();
          fileObserver = fileObserver1;
          if (fileObserver1 == null) {
            this.m_observers.remove(Integer.valueOf(param1Int1));
            fileObserver = fileObserver1;
          } 
        } 
        if (fileObserver != null)
          try {
            fileObserver.onEvent(param1Int2, param1String);
          } finally {
            param1String = null;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unhandled exception in FileObserver ");
            stringBuilder.append(fileObserver);
          }  
        return;
      } 
    }
    
    private native int init();
    
    private native void observe(int param1Int);
    
    private native void startWatching(int param1Int1, String[] param1ArrayOfString, int param1Int2, int[] param1ArrayOfint);
    
    private native void stopWatching(int param1Int, int[] param1ArrayOfint);
  }
  
  static {
    ObserverThread observerThread = new ObserverThread();
    observerThread.start();
  }
  
  @Deprecated
  public FileObserver(String paramString) {
    this(new File(paramString));
  }
  
  public FileObserver(File paramFile) {
    this(Arrays.asList(new File[] { paramFile }));
  }
  
  public FileObserver(List<File> paramList) {
    this(paramList, 4095);
  }
  
  @Deprecated
  public FileObserver(String paramString, int paramInt) {
    this(new File(paramString), paramInt);
  }
  
  public FileObserver(File paramFile, int paramInt) {
    this(Arrays.asList(new File[] { paramFile }, ), paramInt);
  }
  
  public FileObserver(List<File> paramList, int paramInt) {
    this.mFiles = paramList;
    this.mMask = paramInt;
  }
  
  protected void finalize() {
    stopWatching();
  }
  
  public void startWatching() {
    if (this.mDescriptors == null)
      this.mDescriptors = s_observerThread.startWatching(this.mFiles, this.mMask, this); 
  }
  
  public void stopWatching() {
    int[] arrayOfInt = this.mDescriptors;
    if (arrayOfInt != null) {
      s_observerThread.stopWatching(arrayOfInt);
      this.mDescriptors = null;
    } 
  }
  
  public abstract void onEvent(int paramInt, String paramString);
}
