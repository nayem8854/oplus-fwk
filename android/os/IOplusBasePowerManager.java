package android.os;

import com.oplus.util.IOplusBaseServiceManager;

public interface IOplusBasePowerManager extends IOplusBaseServiceManager {
  public static final String DESCRIPTOR = "android.os.IPowerManager";
  
  public static final int TRANSACTION_COMMON = 10001;
}
