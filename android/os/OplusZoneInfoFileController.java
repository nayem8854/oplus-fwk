package android.os;

import android.util.Log;

public final class OplusZoneInfoFileController {
  private static final boolean DEBUG = true;
  
  public static final String SERVICE_NAME = "OPPO";
  
  private static final String TAG = "OplusZoneInfoFileController";
  
  private static OplusZoneInfoFileController mInstance = null;
  
  private static IOplusService sService;
  
  private OplusZoneInfoFileController() {
    sService = IOplusService.Stub.asInterface(ServiceManager.getService("OPPO"));
  }
  
  public static OplusZoneInfoFileController getOplusZoneInfoFileController() {
    if (mInstance == null)
      mInstance = new OplusZoneInfoFileController(); 
    return mInstance;
  }
  
  public boolean copyFile(String paramString1, String paramString2) {
    boolean bool = false;
    try {
      if (sService != null)
        bool = sService.copyFile(paramString1, paramString2); 
      return bool;
    } catch (RemoteException remoteException) {
      Log.e("OplusZoneInfoFileController", "copyFile failed.", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean deleteFile(String paramString) {
    boolean bool = false;
    try {
      if (sService != null)
        bool = sService.deleteFile(paramString); 
      return bool;
    } catch (RemoteException remoteException) {
      Log.e("OplusZoneInfoFileController", "deleteFile failed.", (Throwable)remoteException);
      return false;
    } 
  }
}
