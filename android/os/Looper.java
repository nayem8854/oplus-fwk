package android.os;

import android.util.Printer;
import android.util.Slog;
import android.util.proto.ProtoOutputStream;

public final class Looper extends OplusBaseLooper {
  private static final String TAG = "Looper";
  
  private static Looper sMainLooper;
  
  private static Observer sObserver;
  
  static final ThreadLocal<Looper> sThreadLocal = new ThreadLocal<>();
  
  private boolean mInLoop;
  
  private Printer mLogging;
  
  final MessageQueue mQueue;
  
  private long mSlowDeliveryThresholdMs;
  
  private long mSlowDispatchThresholdMs;
  
  final Thread mThread;
  
  private long mTraceTag;
  
  public static void prepare() {
    prepare(true);
  }
  
  private static void prepare(boolean paramBoolean) {
    if (sThreadLocal.get() == null) {
      sThreadLocal.set(new Looper(paramBoolean));
      return;
    } 
    throw new RuntimeException("Only one Looper may be created per thread");
  }
  
  @Deprecated
  public static void prepareMainLooper() {
    // Byte code:
    //   0: iconst_0
    //   1: invokestatic prepare : (Z)V
    //   4: ldc android/os/Looper
    //   6: monitorenter
    //   7: getstatic android/os/Looper.sMainLooper : Landroid/os/Looper;
    //   10: ifnonnull -> 23
    //   13: invokestatic myLooper : ()Landroid/os/Looper;
    //   16: putstatic android/os/Looper.sMainLooper : Landroid/os/Looper;
    //   19: ldc android/os/Looper
    //   21: monitorexit
    //   22: return
    //   23: new java/lang/IllegalStateException
    //   26: astore_0
    //   27: aload_0
    //   28: ldc_w 'The main Looper has already been prepared.'
    //   31: invokespecial <init> : (Ljava/lang/String;)V
    //   34: aload_0
    //   35: athrow
    //   36: astore_0
    //   37: ldc android/os/Looper
    //   39: monitorexit
    //   40: aload_0
    //   41: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #128	-> 0
    //   #129	-> 4
    //   #130	-> 7
    //   #133	-> 13
    //   #134	-> 19
    //   #135	-> 22
    //   #131	-> 23
    //   #134	-> 36
    // Exception table:
    //   from	to	target	type
    //   7	13	36	finally
    //   13	19	36	finally
    //   19	22	36	finally
    //   23	36	36	finally
    //   37	40	36	finally
  }
  
  public static Looper getMainLooper() {
    // Byte code:
    //   0: ldc android/os/Looper
    //   2: monitorenter
    //   3: getstatic android/os/Looper.sMainLooper : Landroid/os/Looper;
    //   6: astore_0
    //   7: ldc android/os/Looper
    //   9: monitorexit
    //   10: aload_0
    //   11: areturn
    //   12: astore_0
    //   13: ldc android/os/Looper
    //   15: monitorexit
    //   16: aload_0
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #141	-> 0
    //   #142	-> 3
    //   #143	-> 12
    // Exception table:
    //   from	to	target	type
    //   3	10	12	finally
    //   13	16	12	finally
  }
  
  public static void setObserver(Observer paramObserver) {
    sObserver = paramObserver;
  }
  
  public static void loop() {
    // Byte code:
    //   0: invokestatic myLooper : ()Landroid/os/Looper;
    //   3: astore_0
    //   4: aload_0
    //   5: ifnull -> 1118
    //   8: aload_0
    //   9: getfield mInLoop : Z
    //   12: ifeq -> 23
    //   15: ldc 'Looper'
    //   17: ldc 'Loop again would have the queued messages be executed before this one completed.'
    //   19: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   22: pop
    //   23: aload_0
    //   24: iconst_1
    //   25: putfield mInLoop : Z
    //   28: aload_0
    //   29: getfield mQueue : Landroid/os/MessageQueue;
    //   32: astore_1
    //   33: ldc 'main'
    //   35: invokestatic currentThread : ()Ljava/lang/Thread;
    //   38: invokevirtual getName : ()Ljava/lang/String;
    //   41: invokevirtual equals : (Ljava/lang/Object;)Z
    //   44: istore_2
    //   45: iload_2
    //   46: ifeq -> 69
    //   49: new android/os/LooperMsgTimeTracker
    //   52: dup
    //   53: invokespecial <init> : ()V
    //   56: astore_3
    //   57: new android/os/LooperMessageSuperviser
    //   60: dup
    //   61: invokespecial <init> : ()V
    //   64: astore #4
    //   66: goto -> 74
    //   69: aconst_null
    //   70: astore #4
    //   72: aconst_null
    //   73: astore_3
    //   74: invokestatic clearCallingIdentity : ()J
    //   77: pop2
    //   78: invokestatic clearCallingIdentity : ()J
    //   81: lstore #5
    //   83: new java/lang/StringBuilder
    //   86: dup
    //   87: invokespecial <init> : ()V
    //   90: astore #7
    //   92: aload #7
    //   94: ldc 'log.looper.'
    //   96: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   99: pop
    //   100: aload #7
    //   102: invokestatic myUid : ()I
    //   105: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   108: pop
    //   109: aload #7
    //   111: ldc '.'
    //   113: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   116: pop
    //   117: aload #7
    //   119: invokestatic currentThread : ()Ljava/lang/Thread;
    //   122: invokevirtual getName : ()Ljava/lang/String;
    //   125: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   128: pop
    //   129: aload #7
    //   131: ldc '.slow'
    //   133: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   136: pop
    //   137: aload #7
    //   139: invokevirtual toString : ()Ljava/lang/String;
    //   142: astore #7
    //   144: aload #7
    //   146: iconst_0
    //   147: invokestatic getInt : (Ljava/lang/String;I)I
    //   150: istore #8
    //   152: iconst_0
    //   153: istore #9
    //   155: aload_1
    //   156: invokevirtual next : ()Landroid/os/Message;
    //   159: astore #10
    //   161: aload #10
    //   163: ifnonnull -> 167
    //   166: return
    //   167: aload_0
    //   168: getfield mLogging : Landroid/util/Printer;
    //   171: astore #11
    //   173: aload #11
    //   175: ifnull -> 256
    //   178: new java/lang/StringBuilder
    //   181: dup
    //   182: invokespecial <init> : ()V
    //   185: astore #7
    //   187: aload #7
    //   189: ldc '>>>>> Dispatching to '
    //   191: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   194: pop
    //   195: aload #7
    //   197: aload #10
    //   199: getfield target : Landroid/os/Handler;
    //   202: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   205: pop
    //   206: aload #7
    //   208: ldc ' '
    //   210: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   213: pop
    //   214: aload #7
    //   216: aload #10
    //   218: getfield callback : Ljava/lang/Runnable;
    //   221: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   224: pop
    //   225: aload #7
    //   227: ldc ': '
    //   229: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   232: pop
    //   233: aload #7
    //   235: aload #10
    //   237: getfield what : I
    //   240: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   243: pop
    //   244: aload #11
    //   246: aload #7
    //   248: invokevirtual toString : ()Ljava/lang/String;
    //   251: invokeinterface println : (Ljava/lang/String;)V
    //   256: getstatic android/os/Looper.sObserver : Landroid/os/Looper$Observer;
    //   259: astore #12
    //   261: aload_0
    //   262: getfield mTraceTag : J
    //   265: lstore #13
    //   267: aload_0
    //   268: getfield mSlowDispatchThresholdMs : J
    //   271: lstore #15
    //   273: aload_0
    //   274: getfield mSlowDeliveryThresholdMs : J
    //   277: lstore #17
    //   279: iload #8
    //   281: ifle -> 297
    //   284: iload #8
    //   286: i2l
    //   287: lstore #15
    //   289: iload #8
    //   291: i2l
    //   292: lstore #17
    //   294: goto -> 297
    //   297: lload #17
    //   299: lconst_0
    //   300: lcmp
    //   301: ifle -> 320
    //   304: aload #10
    //   306: getfield when : J
    //   309: lconst_0
    //   310: lcmp
    //   311: ifle -> 320
    //   314: iconst_1
    //   315: istore #19
    //   317: goto -> 323
    //   320: iconst_0
    //   321: istore #19
    //   323: lload #15
    //   325: lconst_0
    //   326: lcmp
    //   327: ifle -> 336
    //   330: iconst_1
    //   331: istore #20
    //   333: goto -> 339
    //   336: iconst_0
    //   337: istore #20
    //   339: iload #19
    //   341: ifne -> 358
    //   344: iload #20
    //   346: ifeq -> 352
    //   349: goto -> 358
    //   352: iconst_0
    //   353: istore #21
    //   355: goto -> 361
    //   358: iconst_1
    //   359: istore #21
    //   361: lload #13
    //   363: lconst_0
    //   364: lcmp
    //   365: ifeq -> 391
    //   368: lload #13
    //   370: invokestatic isTagEnabled : (J)Z
    //   373: ifeq -> 391
    //   376: lload #13
    //   378: aload #10
    //   380: getfield target : Landroid/os/Handler;
    //   383: aload #10
    //   385: invokevirtual getTraceName : (Landroid/os/Message;)Ljava/lang/String;
    //   388: invokestatic traceBegin : (JLjava/lang/String;)V
    //   391: aload_3
    //   392: ifnull -> 401
    //   395: aload_3
    //   396: aload #10
    //   398: invokevirtual start : (Landroid/os/Message;)V
    //   401: invokestatic uptimeMillis : ()J
    //   404: lstore #22
    //   406: invokestatic myPid : ()I
    //   409: istore #24
    //   411: aload #4
    //   413: ifnull -> 425
    //   416: aload #4
    //   418: aload #10
    //   420: iload #24
    //   422: invokevirtual beginLooperMessage : (Landroid/os/Message;I)V
    //   425: iload #21
    //   427: ifeq -> 438
    //   430: invokestatic uptimeMillis : ()J
    //   433: lstore #25
    //   435: goto -> 441
    //   438: lconst_0
    //   439: lstore #25
    //   441: lconst_0
    //   442: lstore #27
    //   444: aload #12
    //   446: ifnull -> 461
    //   449: aload #12
    //   451: invokeinterface messageDispatchStarting : ()Ljava/lang/Object;
    //   456: astore #7
    //   458: goto -> 464
    //   461: aconst_null
    //   462: astore #7
    //   464: aload #10
    //   466: getfield workSourceUid : I
    //   469: invokestatic setUid : (I)J
    //   472: lstore #29
    //   474: aload #10
    //   476: getfield target : Landroid/os/Handler;
    //   479: astore #31
    //   481: aload #31
    //   483: aload #10
    //   485: invokevirtual dispatchMessage : (Landroid/os/Message;)V
    //   488: aload #12
    //   490: ifnull -> 504
    //   493: aload #12
    //   495: aload #7
    //   497: aload #10
    //   499: invokeinterface messageDispatched : (Ljava/lang/Object;Landroid/os/Message;)V
    //   504: iload #20
    //   506: ifeq -> 517
    //   509: invokestatic uptimeMillis : ()J
    //   512: lstore #32
    //   514: goto -> 520
    //   517: lconst_0
    //   518: lstore #32
    //   520: lload #29
    //   522: invokestatic restore : (J)V
    //   525: lload #13
    //   527: lconst_0
    //   528: lcmp
    //   529: ifeq -> 537
    //   532: lload #13
    //   534: invokestatic traceEnd : (J)V
    //   537: goto -> 698
    //   540: astore #31
    //   542: goto -> 611
    //   545: astore #7
    //   547: getstatic android/os/Looper.sThrowableObserver : Landroid/os/OplusBaseLooper$ThrowableObserver;
    //   550: astore #31
    //   552: aload #31
    //   554: ifnull -> 602
    //   557: getstatic android/os/Looper.sThrowableObserver : Landroid/os/OplusBaseLooper$ThrowableObserver;
    //   560: aload #7
    //   562: invokeinterface shouldCatchThrowable : (Ljava/lang/Throwable;)Z
    //   567: istore #34
    //   569: iload #34
    //   571: ifeq -> 602
    //   574: lload #29
    //   576: invokestatic restore : (J)V
    //   579: lload #27
    //   581: lstore #32
    //   583: lload #13
    //   585: lconst_0
    //   586: lcmp
    //   587: ifeq -> 599
    //   590: lload #27
    //   592: lstore #32
    //   594: lload #13
    //   596: invokestatic traceEnd : (J)V
    //   599: goto -> 698
    //   602: aload #7
    //   604: athrow
    //   605: astore_3
    //   606: goto -> 1099
    //   609: astore #31
    //   611: aload #12
    //   613: ifnull -> 636
    //   616: aload #12
    //   618: aload #7
    //   620: aload #10
    //   622: aload #31
    //   624: invokeinterface dispatchingThrewException : (Ljava/lang/Object;Landroid/os/Message;Ljava/lang/Exception;)V
    //   629: goto -> 636
    //   632: astore_3
    //   633: goto -> 1099
    //   636: getstatic android/os/Looper.sThrowableObserver : Landroid/os/OplusBaseLooper$ThrowableObserver;
    //   639: ifnull -> 1091
    //   642: getstatic android/os/Looper.sThrowableObserver : Landroid/os/OplusBaseLooper$ThrowableObserver;
    //   645: aload #31
    //   647: invokeinterface shouldCatchThrowable : (Ljava/lang/Throwable;)Z
    //   652: istore #34
    //   654: iload #34
    //   656: ifeq -> 1091
    //   659: iload #20
    //   661: ifeq -> 672
    //   664: invokestatic uptimeMillis : ()J
    //   667: lstore #27
    //   669: goto -> 675
    //   672: lconst_0
    //   673: lstore #27
    //   675: lload #29
    //   677: invokestatic restore : (J)V
    //   680: lload #27
    //   682: lstore #32
    //   684: lload #13
    //   686: lconst_0
    //   687: lcmp
    //   688: ifeq -> 599
    //   691: lload #27
    //   693: lstore #32
    //   695: goto -> 594
    //   698: aload_3
    //   699: ifnull -> 706
    //   702: aload_3
    //   703: invokevirtual stop : ()V
    //   706: aload #4
    //   708: ifnull -> 722
    //   711: aload #4
    //   713: aload #10
    //   715: lload #22
    //   717: iload #24
    //   719: invokevirtual endLooperMessage : (Landroid/os/Message;JI)V
    //   722: iload #19
    //   724: ifeq -> 791
    //   727: iload #9
    //   729: ifeq -> 765
    //   732: lload #25
    //   734: aload #10
    //   736: getfield when : J
    //   739: lsub
    //   740: ldc2_w 10
    //   743: lcmp
    //   744: ifgt -> 762
    //   747: ldc 'Looper'
    //   749: ldc_w 'Drained'
    //   752: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   755: pop
    //   756: iconst_0
    //   757: istore #9
    //   759: goto -> 791
    //   762: goto -> 791
    //   765: lload #17
    //   767: aload #10
    //   769: getfield when : J
    //   772: lload #25
    //   774: ldc_w 'delivery'
    //   777: aload #10
    //   779: invokestatic showSlowLog : (JJJLjava/lang/String;Landroid/os/Message;)Z
    //   782: ifeq -> 791
    //   785: iconst_1
    //   786: istore #9
    //   788: goto -> 791
    //   791: iload #20
    //   793: ifeq -> 872
    //   796: lload #15
    //   798: lload #25
    //   800: lload #32
    //   802: ldc_w 'dispatch'
    //   805: aload #10
    //   807: invokestatic showSlowLog : (JJJLjava/lang/String;Landroid/os/Message;)Z
    //   810: pop
    //   811: lload #32
    //   813: lload #25
    //   815: lsub
    //   816: ldc2_w 10000
    //   819: lcmp
    //   820: ifgt -> 859
    //   823: lload #32
    //   825: lload #25
    //   827: lsub
    //   828: ldc2_w 4000
    //   831: lcmp
    //   832: ifle -> 856
    //   835: aload #10
    //   837: getfield target : Landroid/os/Handler;
    //   840: astore #7
    //   842: ldc_w 'android.view.Choreographer$FrameHandler'
    //   845: aload #7
    //   847: invokevirtual equals : (Ljava/lang/Object;)Z
    //   850: ifeq -> 872
    //   853: goto -> 859
    //   856: goto -> 872
    //   859: ldc_w 'Quality'
    //   862: ldc_w '07 01 blocked'
    //   865: invokestatic p : (Ljava/lang/String;Ljava/lang/String;)I
    //   868: pop
    //   869: goto -> 872
    //   872: aload #11
    //   874: ifnull -> 937
    //   877: new java/lang/StringBuilder
    //   880: dup
    //   881: invokespecial <init> : ()V
    //   884: astore #7
    //   886: aload #7
    //   888: ldc_w '<<<<< Finished to '
    //   891: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   894: pop
    //   895: aload #7
    //   897: aload #10
    //   899: getfield target : Landroid/os/Handler;
    //   902: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   905: pop
    //   906: aload #7
    //   908: ldc ' '
    //   910: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   913: pop
    //   914: aload #7
    //   916: aload #10
    //   918: getfield callback : Ljava/lang/Runnable;
    //   921: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   924: pop
    //   925: aload #11
    //   927: aload #7
    //   929: invokevirtual toString : ()Ljava/lang/String;
    //   932: invokeinterface println : (Ljava/lang/String;)V
    //   937: invokestatic clearCallingIdentity : ()J
    //   940: lstore #32
    //   942: lload #5
    //   944: lload #32
    //   946: lcmp
    //   947: ifeq -> 1083
    //   950: new java/lang/StringBuilder
    //   953: dup
    //   954: invokespecial <init> : ()V
    //   957: astore #7
    //   959: aload #7
    //   961: ldc_w 'Thread identity changed from 0x'
    //   964: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   967: pop
    //   968: aload #7
    //   970: lload #5
    //   972: invokestatic toHexString : (J)Ljava/lang/String;
    //   975: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   978: pop
    //   979: aload #7
    //   981: ldc_w ' to 0x'
    //   984: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   987: pop
    //   988: aload #7
    //   990: lload #32
    //   992: invokestatic toHexString : (J)Ljava/lang/String;
    //   995: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   998: pop
    //   999: aload #7
    //   1001: ldc_w ' while dispatching to '
    //   1004: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1007: pop
    //   1008: aload #10
    //   1010: getfield target : Landroid/os/Handler;
    //   1013: astore #31
    //   1015: aload #7
    //   1017: aload #31
    //   1019: invokevirtual getClass : ()Ljava/lang/Class;
    //   1022: invokevirtual getName : ()Ljava/lang/String;
    //   1025: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1028: pop
    //   1029: aload #7
    //   1031: ldc ' '
    //   1033: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1036: pop
    //   1037: aload #7
    //   1039: aload #10
    //   1041: getfield callback : Ljava/lang/Runnable;
    //   1044: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1047: pop
    //   1048: aload #7
    //   1050: ldc_w ' what='
    //   1053: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1056: pop
    //   1057: aload #7
    //   1059: aload #10
    //   1061: getfield what : I
    //   1064: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1067: pop
    //   1068: aload #7
    //   1070: invokevirtual toString : ()Ljava/lang/String;
    //   1073: astore #7
    //   1075: ldc 'Looper'
    //   1077: aload #7
    //   1079: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;)I
    //   1082: pop
    //   1083: aload #10
    //   1085: invokevirtual recycleUnchecked : ()V
    //   1088: goto -> 155
    //   1091: aload #31
    //   1093: athrow
    //   1094: astore_3
    //   1095: goto -> 1099
    //   1098: astore_3
    //   1099: lload #29
    //   1101: invokestatic restore : (J)V
    //   1104: lload #13
    //   1106: lconst_0
    //   1107: lcmp
    //   1108: ifeq -> 1116
    //   1111: lload #13
    //   1113: invokestatic traceEnd : (J)V
    //   1116: aload_3
    //   1117: athrow
    //   1118: new java/lang/RuntimeException
    //   1121: dup
    //   1122: ldc_w 'No Looper; Looper.prepare() wasn't called on this thread.'
    //   1125: invokespecial <init> : (Ljava/lang/String;)V
    //   1128: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #160	-> 0
    //   #161	-> 4
    //   #164	-> 8
    //   #165	-> 15
    //   #169	-> 23
    //   #170	-> 28
    //   #173	-> 33
    //   #174	-> 45
    //   #175	-> 45
    //   #176	-> 45
    //   #177	-> 49
    //   #178	-> 57
    //   #176	-> 69
    //   #184	-> 74
    //   #185	-> 78
    //   #189	-> 83
    //   #191	-> 100
    //   #192	-> 117
    //   #190	-> 144
    //   #195	-> 152
    //   #198	-> 155
    //   #199	-> 161
    //   #201	-> 166
    //   #205	-> 167
    //   #206	-> 173
    //   #207	-> 178
    //   #211	-> 256
    //   #213	-> 261
    //   #214	-> 267
    //   #215	-> 273
    //   #216	-> 279
    //   #217	-> 284
    //   #218	-> 289
    //   #216	-> 297
    //   #220	-> 297
    //   #221	-> 323
    //   #223	-> 339
    //   #224	-> 361
    //   #226	-> 361
    //   #227	-> 376
    //   #232	-> 391
    //   #233	-> 395
    //   #235	-> 401
    //   #236	-> 406
    //   #237	-> 411
    //   #238	-> 416
    //   #241	-> 425
    //   #244	-> 441
    //   #248	-> 444
    //   #249	-> 444
    //   #250	-> 449
    //   #249	-> 461
    //   #252	-> 464
    //   #254	-> 474
    //   #255	-> 488
    //   #256	-> 493
    //   #258	-> 504
    //   #283	-> 520
    //   #284	-> 525
    //   #285	-> 532
    //   #290	-> 537
    //   #259	-> 540
    //   #276	-> 545
    //   #277	-> 547
    //   #283	-> 574
    //   #284	-> 579
    //   #285	-> 594
    //   #290	-> 599
    //   #277	-> 602
    //   #278	-> 602
    //   #283	-> 605
    //   #259	-> 609
    //   #260	-> 611
    //   #261	-> 616
    //   #283	-> 632
    //   #265	-> 636
    //   #268	-> 659
    //   #283	-> 675
    //   #284	-> 680
    //   #285	-> 691
    //   #290	-> 698
    //   #291	-> 702
    //   #293	-> 706
    //   #294	-> 711
    //   #297	-> 722
    //   #298	-> 727
    //   #299	-> 732
    //   #300	-> 747
    //   #301	-> 756
    //   #299	-> 762
    //   #304	-> 765
    //   #307	-> 785
    //   #297	-> 791
    //   #311	-> 791
    //   #312	-> 796
    //   #315	-> 811
    //   #316	-> 842
    //   #315	-> 856
    //   #317	-> 859
    //   #311	-> 872
    //   #321	-> 872
    //   #322	-> 877
    //   #327	-> 937
    //   #328	-> 942
    //   #329	-> 950
    //   #330	-> 968
    //   #331	-> 988
    //   #332	-> 1015
    //   #329	-> 1075
    //   #336	-> 1083
    //   #337	-> 1088
    //   #265	-> 1091
    //   #266	-> 1091
    //   #283	-> 1094
    //   #284	-> 1104
    //   #285	-> 1111
    //   #287	-> 1116
    //   #162	-> 1118
    // Exception table:
    //   from	to	target	type
    //   474	481	609	java/lang/Exception
    //   474	481	545	finally
    //   481	488	540	java/lang/Exception
    //   481	488	545	finally
    //   493	504	540	java/lang/Exception
    //   493	504	545	finally
    //   509	514	540	java/lang/Exception
    //   509	514	545	finally
    //   547	552	605	finally
    //   557	569	632	finally
    //   602	605	632	finally
    //   616	629	632	finally
    //   636	654	1098	finally
    //   664	669	632	finally
    //   1091	1094	1094	finally
  }
  
  private static boolean showSlowLog(long paramLong1, long paramLong2, long paramLong3, String paramString, Message paramMessage) {
    paramLong2 = paramLong3 - paramLong2;
    if (paramLong2 < paramLong1)
      return false; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Slow ");
    stringBuilder.append(paramString);
    stringBuilder.append(" took ");
    stringBuilder.append(paramLong2);
    stringBuilder.append("ms ");
    stringBuilder.append(Thread.currentThread().getName());
    stringBuilder.append(" h=");
    Handler handler = paramMessage.target;
    stringBuilder.append(handler.getClass().getName());
    stringBuilder.append(" c=");
    stringBuilder.append(paramMessage.callback);
    stringBuilder.append(" m=");
    stringBuilder.append(paramMessage.what);
    String str = stringBuilder.toString();
    Slog.w("Looper", str);
    return true;
  }
  
  public static Looper myLooper() {
    return sThreadLocal.get();
  }
  
  public static MessageQueue myQueue() {
    return (myLooper()).mQueue;
  }
  
  private Looper(boolean paramBoolean) {
    this.mQueue = new MessageQueue(paramBoolean);
    this.mThread = Thread.currentThread();
  }
  
  public boolean isCurrentThread() {
    boolean bool;
    if (Thread.currentThread() == this.mThread) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setMessageLogging(Printer paramPrinter) {
    this.mLogging = paramPrinter;
  }
  
  public void setTraceTag(long paramLong) {
    this.mTraceTag = paramLong;
  }
  
  public void setSlowLogThresholdMs(long paramLong1, long paramLong2) {
    this.mSlowDispatchThresholdMs = paramLong1;
    this.mSlowDeliveryThresholdMs = paramLong2;
  }
  
  public void quit() {
    this.mQueue.quit(false);
  }
  
  public void quitSafely() {
    this.mQueue.quit(true);
  }
  
  public Thread getThread() {
    return this.mThread;
  }
  
  public MessageQueue getQueue() {
    return this.mQueue;
  }
  
  public void dump(Printer paramPrinter, String paramString) {
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString);
    stringBuilder1.append(toString());
    paramPrinter.println(stringBuilder1.toString());
    MessageQueue messageQueue = this.mQueue;
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramString);
    stringBuilder2.append("  ");
    messageQueue.dump(paramPrinter, stringBuilder2.toString(), null);
  }
  
  public void dump(Printer paramPrinter, String paramString, Handler paramHandler) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(toString());
    paramPrinter.println(stringBuilder.toString());
    MessageQueue messageQueue = this.mQueue;
    stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("  ");
    messageQueue.dump(paramPrinter, stringBuilder.toString(), paramHandler);
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1138166333441L, this.mThread.getName());
    paramProtoOutputStream.write(1112396529666L, this.mThread.getId());
    MessageQueue messageQueue = this.mQueue;
    if (messageQueue != null)
      messageQueue.dumpDebug(paramProtoOutputStream, 1146756268035L); 
    paramProtoOutputStream.end(paramLong);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Looper (");
    stringBuilder.append(this.mThread.getName());
    stringBuilder.append(", tid ");
    stringBuilder.append(this.mThread.getId());
    stringBuilder.append(") {");
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  class Observer {
    public abstract void dispatchingThrewException(Object param1Object, Message param1Message, Exception param1Exception);
    
    public abstract Object messageDispatchStarting();
    
    public abstract void messageDispatched(Object param1Object, Message param1Message);
  }
}
