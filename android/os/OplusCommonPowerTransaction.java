package android.os;

public enum OplusCommonPowerTransaction {
  END, REGISTER_SCREEN_STATUS_LISTENER, UNREGISTER_SCREEN_STATUS_LISTENER;
  
  private static final OplusCommonPowerTransaction[] $VALUES;
  
  static {
    OplusCommonPowerTransaction oplusCommonPowerTransaction = new OplusCommonPowerTransaction("END", 2);
    $VALUES = new OplusCommonPowerTransaction[] { REGISTER_SCREEN_STATUS_LISTENER, UNREGISTER_SCREEN_STATUS_LISTENER, oplusCommonPowerTransaction };
  }
}
