package android.os;

import android.util.Log;
import java.time.Clock;
import java.time.DateTimeException;
import java.time.ZoneId;
import java.util.Arrays;

public class BestClock extends SimpleClock {
  private static final String TAG = "BestClock";
  
  private final Clock[] clocks;
  
  public BestClock(ZoneId paramZoneId, Clock... paramVarArgs) {
    super(paramZoneId);
    this.clocks = paramVarArgs;
  }
  
  public long millis() {
    for (Clock clock : this.clocks) {
      try {
        return clock.millis();
      } catch (DateTimeException dateTimeException) {
        Log.w("BestClock", dateTimeException.toString());
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("No clocks in ");
    Clock[] arrayOfClock = this.clocks;
    stringBuilder.append(Arrays.toString((Object[])arrayOfClock));
    stringBuilder.append(" were able to provide time");
    throw new DateTimeException(stringBuilder.toString());
  }
}
