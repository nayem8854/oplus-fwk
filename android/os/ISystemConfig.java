package android.os;

import java.util.List;
import java.util.Map;

public interface ISystemConfig extends IInterface {
  List<String> getDisabledUntilUsedPreinstalledCarrierApps() throws RemoteException;
  
  Map getDisabledUntilUsedPreinstalledCarrierAssociatedAppEntries() throws RemoteException;
  
  Map getDisabledUntilUsedPreinstalledCarrierAssociatedApps() throws RemoteException;
  
  class Default implements ISystemConfig {
    public List<String> getDisabledUntilUsedPreinstalledCarrierApps() throws RemoteException {
      return null;
    }
    
    public Map getDisabledUntilUsedPreinstalledCarrierAssociatedApps() throws RemoteException {
      return null;
    }
    
    public Map getDisabledUntilUsedPreinstalledCarrierAssociatedAppEntries() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISystemConfig {
    private static final String DESCRIPTOR = "android.os.ISystemConfig";
    
    static final int TRANSACTION_getDisabledUntilUsedPreinstalledCarrierApps = 1;
    
    static final int TRANSACTION_getDisabledUntilUsedPreinstalledCarrierAssociatedAppEntries = 3;
    
    static final int TRANSACTION_getDisabledUntilUsedPreinstalledCarrierAssociatedApps = 2;
    
    public Stub() {
      attachInterface(this, "android.os.ISystemConfig");
    }
    
    public static ISystemConfig asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.ISystemConfig");
      if (iInterface != null && iInterface instanceof ISystemConfig)
        return (ISystemConfig)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "getDisabledUntilUsedPreinstalledCarrierAssociatedAppEntries";
        } 
        return "getDisabledUntilUsedPreinstalledCarrierAssociatedApps";
      } 
      return "getDisabledUntilUsedPreinstalledCarrierApps";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      Map map;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.os.ISystemConfig");
            return true;
          } 
          param1Parcel1.enforceInterface("android.os.ISystemConfig");
          map = getDisabledUntilUsedPreinstalledCarrierAssociatedAppEntries();
          param1Parcel2.writeNoException();
          param1Parcel2.writeMap(map);
          return true;
        } 
        map.enforceInterface("android.os.ISystemConfig");
        map = getDisabledUntilUsedPreinstalledCarrierAssociatedApps();
        param1Parcel2.writeNoException();
        param1Parcel2.writeMap(map);
        return true;
      } 
      map.enforceInterface("android.os.ISystemConfig");
      List<String> list = getDisabledUntilUsedPreinstalledCarrierApps();
      param1Parcel2.writeNoException();
      param1Parcel2.writeStringList(list);
      return true;
    }
    
    private static class Proxy implements ISystemConfig {
      public static ISystemConfig sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.ISystemConfig";
      }
      
      public List<String> getDisabledUntilUsedPreinstalledCarrierApps() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.ISystemConfig");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISystemConfig.Stub.getDefaultImpl() != null)
            return ISystemConfig.Stub.getDefaultImpl().getDisabledUntilUsedPreinstalledCarrierApps(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Map getDisabledUntilUsedPreinstalledCarrierAssociatedApps() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.ISystemConfig");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISystemConfig.Stub.getDefaultImpl() != null)
            return ISystemConfig.Stub.getDefaultImpl().getDisabledUntilUsedPreinstalledCarrierAssociatedApps(); 
          parcel2.readException();
          ClassLoader classLoader = getClass().getClassLoader();
          return parcel2.readHashMap(classLoader);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Map getDisabledUntilUsedPreinstalledCarrierAssociatedAppEntries() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.ISystemConfig");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ISystemConfig.Stub.getDefaultImpl() != null)
            return ISystemConfig.Stub.getDefaultImpl().getDisabledUntilUsedPreinstalledCarrierAssociatedAppEntries(); 
          parcel2.readException();
          ClassLoader classLoader = getClass().getClassLoader();
          return parcel2.readHashMap(classLoader);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISystemConfig param1ISystemConfig) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISystemConfig != null) {
          Proxy.sDefaultImpl = param1ISystemConfig;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISystemConfig getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
