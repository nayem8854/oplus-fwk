package android.os;

import android.util.ArrayMap;
import android.util.Log;
import android.util.proto.ProtoOutputStream;
import java.util.ArrayDeque;
import java.util.Map;
import java.util.concurrent.Executor;

public class TraceBuffer {
  private final TraceHeap mHeap = new TraceHeap();
  
  private long mLastFlushTimeMillis = -1L;
  
  private FlushOutObserver mAction = null;
  
  private static final boolean DEBUG;
  
  private static final int DEFAULT_BUFFER_SIZE = 8192;
  
  private static final long DEFAULT_FLUSH_PERIOD_TIME_MILLIS;
  
  private static final String SYS_LOG_TAG = "persist.onetrace.logtag";
  
  private static final String TAG = "TraceBuffer";
  
  private final long mFlushPeriodTime;
  
  private final int mMaxCacheCapacity;
  
  private ProtoOutputStream mProto;
  
  public TraceBuffer() {
    this(DEFAULT_FLUSH_PERIOD_TIME_MILLIS);
  }
  
  public TraceBuffer(long paramLong) {
    this(paramLong, 8192);
  }
  
  public TraceBuffer(long paramLong, int paramInt) {
    this.mFlushPeriodTime = paramLong;
    this.mMaxCacheCapacity = paramInt;
  }
  
  public void setFlushActionObserver(FlushOutObserver paramFlushOutObserver) {
    this.mAction = paramFlushOutObserver;
  }
  
  public void begin(String paramString) {
    this.mHeap.beginMethod(paramString);
  }
  
  public void end(ContentFilter paramContentFilter) {
    TraceInfo traceInfo = this.mHeap.endMethod();
    if (traceInfo == null)
      return; 
    writeTraceContent(traceInfo, paramContentFilter);
  }
  
  public void asyncBegin(Executor paramExecutor, String paramString, int paramInt) {
    TraceInfo traceInfo = new TraceInfo(paramString, paramInt);
    paramExecutor.execute(new _$$Lambda$TraceBuffer$s0Q8VKo_0O4UUsVslycv_wj2m60(this, traceInfo));
  }
  
  public void asyncEnd(Executor paramExecutor, String paramString, int paramInt1, int paramInt2, ContentFilter paramContentFilter) {
    long l = SystemClock.currentTimeMicro();
    paramExecutor.execute(new _$$Lambda$TraceBuffer$8jHL0cx_i9SeC2yo0gOHVLT7iPY(this, paramString, paramInt1, paramInt2, l, paramContentFilter));
  }
  
  private void writeTraceContent(TraceInfo paramTraceInfo, ContentFilter paramContentFilter) {
    if (paramContentFilter != null)
      paramTraceInfo.contentId = paramContentFilter.matchContent(paramTraceInfo.content); 
    if (paramContentFilter == null || paramTraceInfo.contentId >= 0) {
      writeTraceInfo(paramTraceInfo);
      flushBufferIfNecessary();
    } 
  }
  
  private void writeTraceInfo(TraceInfo paramTraceInfo) {
    if (this.mProto == null)
      this.mProto = new ProtoOutputStream(this.mMaxCacheCapacity); 
    long l1 = this.mProto.start(2246267895809L);
    this.mProto.write(1116691496968L, paramTraceInfo.startTimeMicro);
    long l2 = this.mProto.start(1146756268432L);
    this.mProto.write(1172526071810L, paramTraceInfo.pid);
    this.mProto.write(1172526071811L, paramTraceInfo.tid);
    this.mProto.write(1116691496967L, paramTraceInfo.endTimeMicro);
    this.mProto.write(1172526071813L, paramTraceInfo.level);
    if (paramTraceInfo.isAsync) {
      this.mProto.write(1172526071814L, paramTraceInfo.cookie);
      this.mProto.write(1172526071817L, paramTraceInfo.endTid);
    } 
    if (paramTraceInfo.contentId >= 0) {
      this.mProto.write(1155346202632L, paramTraceInfo.contentId);
    } else {
      this.mProto.write(1138166333444L, paramTraceInfo.content);
    } 
    this.mProto.end(l2);
    this.mProto.end(l1);
  }
  
  private void flushBufferIfNecessary() {
    ProtoOutputStream protoOutputStream = this.mProto;
    if (protoOutputStream != null && protoOutputStream.getRawSize() > this.mMaxCacheCapacity) {
      flush();
      return;
    } 
    long l1 = SystemClock.elapsedRealtime();
    long l2 = this.mLastFlushTimeMillis;
    if (l2 < 0L) {
      this.mLastFlushTimeMillis = l1;
    } else if (l1 - l2 > this.mFlushPeriodTime) {
      flush(l1);
    } 
  }
  
  private void flush() {
    flush(SystemClock.elapsedRealtime());
  }
  
  private void flush(long paramLong) {
    this.mLastFlushTimeMillis = paramLong;
    FlushOutObserver flushOutObserver = this.mAction;
    if (flushOutObserver != null) {
      ProtoOutputStream protoOutputStream = this.mProto;
      if (protoOutputStream != null)
        flushOutObserver.flushOut(protoOutputStream.getBytes()); 
    } 
    this.mProto = null;
  }
  
  static {
    // Byte code:
    //   0: iconst_0
    //   1: istore_0
    //   2: ldc 'persist.sys.assert.panic'
    //   4: iconst_0
    //   5: invokestatic getBoolean : (Ljava/lang/String;Z)Z
    //   8: ifne -> 20
    //   11: ldc 'persist.onetrace.logtag'
    //   13: iconst_0
    //   14: invokestatic getBoolean : (Ljava/lang/String;Z)Z
    //   17: ifeq -> 22
    //   20: iconst_1
    //   21: istore_0
    //   22: iload_0
    //   23: putstatic android/os/TraceBuffer.DEBUG : Z
    //   26: getstatic java/util/concurrent/TimeUnit.SECONDS : Ljava/util/concurrent/TimeUnit;
    //   29: astore_1
    //   30: aload_1
    //   31: ldc2_w 5
    //   34: invokevirtual toMillis : (J)J
    //   37: putstatic android/os/TraceBuffer.DEFAULT_FLUSH_PERIOD_TIME_MILLIS : J
    //   40: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #35	-> 0
    //   #37	-> 11
    //   #40	-> 26
    //   #41	-> 30
    //   #40	-> 40
  }
  
  public static interface FlushOutObserver {
    void flushOut(byte[] param1ArrayOfbyte);
  }
  
  private static class TraceHeap {
    private final ArrayDeque<TraceBuffer.TraceInfo> mTraceDeque = new ArrayDeque<>();
    
    private final Map<String, TraceBuffer.TraceInfo> mAsyncTraceMap = (Map<String, TraceBuffer.TraceInfo>)new ArrayMap();
    
    private static final int LEVEL_UNDEFINED = -1;
    
    void beginMethod(String param1String) {
      TraceBuffer.TraceInfo traceInfo = new TraceBuffer.TraceInfo(param1String, false);
      traceInfo.level = this.mTraceDeque.size();
      this.mTraceDeque.push(traceInfo);
    }
    
    TraceBuffer.TraceInfo endMethod() {
      if (this.mTraceDeque.isEmpty())
        return null; 
      TraceBuffer.TraceInfo traceInfo = this.mTraceDeque.pop();
      traceInfo.endTimeMicro = SystemClock.currentTimeMicro();
      return traceInfo;
    }
    
    void beginMethodAsync(TraceBuffer.TraceInfo param1TraceInfo) {
      param1TraceInfo.level = -1;
      Map<String, TraceBuffer.TraceInfo> map = this.mAsyncTraceMap;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1TraceInfo.content);
      stringBuilder.append(param1TraceInfo.cookie);
      TraceBuffer.TraceInfo traceInfo = map.putIfAbsent(stringBuilder.toString(), param1TraceInfo);
      if (TraceBuffer.DEBUG && traceInfo != null && traceInfo != param1TraceInfo) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("The trace with id=");
        stringBuilder1.append(param1TraceInfo.content);
        stringBuilder1.append(" cookie=");
        stringBuilder1.append(param1TraceInfo.cookie);
        stringBuilder1.append(" is already exist!!! #beginMethodAsync() may have been calledrepeatedly without #endMethodAsync()");
        String str = stringBuilder1.toString();
        Log.w("TraceBuffer", str);
      } 
    }
    
    TraceBuffer.TraceInfo endMethodAsync(String param1String, int param1Int1, int param1Int2, long param1Long) {
      StringBuilder stringBuilder1;
      Map<String, TraceBuffer.TraceInfo> map = this.mAsyncTraceMap;
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(param1String);
      stringBuilder2.append(param1Int1);
      TraceBuffer.TraceInfo traceInfo = map.remove(stringBuilder2.toString());
      if (traceInfo == null) {
        if (TraceBuffer.DEBUG) {
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("The trace with id=");
          stringBuilder1.append(param1String);
          stringBuilder1.append(" cookie=");
          stringBuilder1.append(param1Int1);
          stringBuilder1.append(" is not exist!!! #beginMethodAsync() may have not been calledfor this method.");
          param1String = stringBuilder1.toString();
          Log.w("TraceBuffer", param1String);
        } 
        return null;
      } 
      ((TraceBuffer.TraceInfo)stringBuilder1).endTimeMicro = param1Long;
      ((TraceBuffer.TraceInfo)stringBuilder1).endTid = param1Int2;
      return (TraceBuffer.TraceInfo)stringBuilder1;
    }
    
    private TraceHeap() {}
  }
  
  private static class TraceInfo {
    private static final int UNDEFINED_ID = -1;
    
    public String content;
    
    public int contentId;
    
    public int cookie;
    
    public int endTid;
    
    public long endTimeMicro;
    
    public boolean isAsync;
    
    public int level;
    
    public int pid;
    
    public long startTimeMicro;
    
    public int tid;
    
    public TraceInfo(String param1String, boolean param1Boolean) {
      this.content = param1String;
      this.startTimeMicro = SystemClock.currentTimeMicro();
      this.pid = Process.myPid();
      this.tid = Process.myTid();
      this.isAsync = param1Boolean;
      this.contentId = -1;
    }
    
    public TraceInfo(String param1String, int param1Int) {
      this(param1String, true);
      this.cookie = param1Int;
    }
  }
}
