package android.os;

import android.annotation.SystemApi;
import libcore.util.NativeAllocationRegistry;

@SystemApi
public class HwBlob {
  private static final String TAG = "HwBlob";
  
  private static final NativeAllocationRegistry sNativeRegistry;
  
  private long mNativeContext;
  
  public HwBlob(int paramInt) {
    native_setup(paramInt);
    sNativeRegistry.registerNativeAllocation(this, this.mNativeContext);
  }
  
  public final void putHidlMemory(long paramLong, HidlMemory paramHidlMemory) {
    putNativeHandle(0L + paramLong, paramHidlMemory.getHandle());
    putInt64(16L + paramLong, paramHidlMemory.getSize());
    putString(24L + paramLong, paramHidlMemory.getName());
  }
  
  public static Boolean[] wrapArray(boolean[] paramArrayOfboolean) {
    int i = paramArrayOfboolean.length;
    Boolean[] arrayOfBoolean = new Boolean[i];
    for (byte b = 0; b < i; b++)
      arrayOfBoolean[b] = Boolean.valueOf(paramArrayOfboolean[b]); 
    return arrayOfBoolean;
  }
  
  public static Long[] wrapArray(long[] paramArrayOflong) {
    int i = paramArrayOflong.length;
    Long[] arrayOfLong = new Long[i];
    for (byte b = 0; b < i; b++)
      arrayOfLong[b] = Long.valueOf(paramArrayOflong[b]); 
    return arrayOfLong;
  }
  
  public static Byte[] wrapArray(byte[] paramArrayOfbyte) {
    int i = paramArrayOfbyte.length;
    Byte[] arrayOfByte = new Byte[i];
    for (byte b = 0; b < i; b++)
      arrayOfByte[b] = Byte.valueOf(paramArrayOfbyte[b]); 
    return arrayOfByte;
  }
  
  public static Short[] wrapArray(short[] paramArrayOfshort) {
    int i = paramArrayOfshort.length;
    Short[] arrayOfShort = new Short[i];
    for (byte b = 0; b < i; b++)
      arrayOfShort[b] = Short.valueOf(paramArrayOfshort[b]); 
    return arrayOfShort;
  }
  
  public static Integer[] wrapArray(int[] paramArrayOfint) {
    int i = paramArrayOfint.length;
    Integer[] arrayOfInteger = new Integer[i];
    for (byte b = 0; b < i; b++)
      arrayOfInteger[b] = Integer.valueOf(paramArrayOfint[b]); 
    return arrayOfInteger;
  }
  
  public static Float[] wrapArray(float[] paramArrayOffloat) {
    int i = paramArrayOffloat.length;
    Float[] arrayOfFloat = new Float[i];
    for (byte b = 0; b < i; b++)
      arrayOfFloat[b] = Float.valueOf(paramArrayOffloat[b]); 
    return arrayOfFloat;
  }
  
  public static Double[] wrapArray(double[] paramArrayOfdouble) {
    int i = paramArrayOfdouble.length;
    Double[] arrayOfDouble = new Double[i];
    for (byte b = 0; b < i; b++)
      arrayOfDouble[b] = Double.valueOf(paramArrayOfdouble[b]); 
    return arrayOfDouble;
  }
  
  static {
    long l = native_init();
    sNativeRegistry = new NativeAllocationRegistry(HwBlob.class.getClassLoader(), l, 128L);
  }
  
  private static final native long native_init();
  
  private final native void native_setup(int paramInt);
  
  public final native void copyToBoolArray(long paramLong, boolean[] paramArrayOfboolean, int paramInt);
  
  public final native void copyToDoubleArray(long paramLong, double[] paramArrayOfdouble, int paramInt);
  
  public final native void copyToFloatArray(long paramLong, float[] paramArrayOffloat, int paramInt);
  
  public final native void copyToInt16Array(long paramLong, short[] paramArrayOfshort, int paramInt);
  
  public final native void copyToInt32Array(long paramLong, int[] paramArrayOfint, int paramInt);
  
  public final native void copyToInt64Array(long paramLong, long[] paramArrayOflong, int paramInt);
  
  public final native void copyToInt8Array(long paramLong, byte[] paramArrayOfbyte, int paramInt);
  
  public final native boolean getBool(long paramLong);
  
  public final native double getDouble(long paramLong);
  
  public final native long getFieldHandle(long paramLong);
  
  public final native float getFloat(long paramLong);
  
  public final native short getInt16(long paramLong);
  
  public final native int getInt32(long paramLong);
  
  public final native long getInt64(long paramLong);
  
  public final native byte getInt8(long paramLong);
  
  public final native String getString(long paramLong);
  
  public final native long handle();
  
  public final native void putBlob(long paramLong, HwBlob paramHwBlob);
  
  public final native void putBool(long paramLong, boolean paramBoolean);
  
  public final native void putBoolArray(long paramLong, boolean[] paramArrayOfboolean);
  
  public final native void putDouble(long paramLong, double paramDouble);
  
  public final native void putDoubleArray(long paramLong, double[] paramArrayOfdouble);
  
  public final native void putFloat(long paramLong, float paramFloat);
  
  public final native void putFloatArray(long paramLong, float[] paramArrayOffloat);
  
  public final native void putInt16(long paramLong, short paramShort);
  
  public final native void putInt16Array(long paramLong, short[] paramArrayOfshort);
  
  public final native void putInt32(long paramLong, int paramInt);
  
  public final native void putInt32Array(long paramLong, int[] paramArrayOfint);
  
  public final native void putInt64(long paramLong1, long paramLong2);
  
  public final native void putInt64Array(long paramLong, long[] paramArrayOflong);
  
  public final native void putInt8(long paramLong, byte paramByte);
  
  public final native void putInt8Array(long paramLong, byte[] paramArrayOfbyte);
  
  public final native void putNativeHandle(long paramLong, NativeHandle paramNativeHandle);
  
  public final native void putString(long paramLong, String paramString);
}
