package android.os;

public interface IHardwarePropertiesManager extends IInterface {
  CpuUsageInfo[] getCpuUsages(String paramString) throws RemoteException;
  
  float[] getDeviceTemperatures(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  float[] getFanSpeeds(String paramString) throws RemoteException;
  
  class Default implements IHardwarePropertiesManager {
    public float[] getDeviceTemperatures(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public CpuUsageInfo[] getCpuUsages(String param1String) throws RemoteException {
      return null;
    }
    
    public float[] getFanSpeeds(String param1String) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IHardwarePropertiesManager {
    private static final String DESCRIPTOR = "android.os.IHardwarePropertiesManager";
    
    static final int TRANSACTION_getCpuUsages = 2;
    
    static final int TRANSACTION_getDeviceTemperatures = 1;
    
    static final int TRANSACTION_getFanSpeeds = 3;
    
    public Stub() {
      attachInterface(this, "android.os.IHardwarePropertiesManager");
    }
    
    public static IHardwarePropertiesManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IHardwarePropertiesManager");
      if (iInterface != null && iInterface instanceof IHardwarePropertiesManager)
        return (IHardwarePropertiesManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "getFanSpeeds";
        } 
        return "getCpuUsages";
      } 
      return "getDeviceTemperatures";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      CpuUsageInfo[] arrayOfCpuUsageInfo;
      if (param1Int1 != 1) {
        float[] arrayOfFloat1;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.os.IHardwarePropertiesManager");
            return true;
          } 
          param1Parcel1.enforceInterface("android.os.IHardwarePropertiesManager");
          String str2 = param1Parcel1.readString();
          arrayOfFloat1 = getFanSpeeds(str2);
          param1Parcel2.writeNoException();
          param1Parcel2.writeFloatArray(arrayOfFloat1);
          return true;
        } 
        arrayOfFloat1.enforceInterface("android.os.IHardwarePropertiesManager");
        String str1 = arrayOfFloat1.readString();
        arrayOfCpuUsageInfo = getCpuUsages(str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeTypedArray(arrayOfCpuUsageInfo, 1);
        return true;
      } 
      arrayOfCpuUsageInfo.enforceInterface("android.os.IHardwarePropertiesManager");
      String str = arrayOfCpuUsageInfo.readString();
      param1Int2 = arrayOfCpuUsageInfo.readInt();
      param1Int1 = arrayOfCpuUsageInfo.readInt();
      float[] arrayOfFloat = getDeviceTemperatures(str, param1Int2, param1Int1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeFloatArray(arrayOfFloat);
      return true;
    }
    
    private static class Proxy implements IHardwarePropertiesManager {
      public static IHardwarePropertiesManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IHardwarePropertiesManager";
      }
      
      public float[] getDeviceTemperatures(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IHardwarePropertiesManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IHardwarePropertiesManager.Stub.getDefaultImpl() != null)
            return IHardwarePropertiesManager.Stub.getDefaultImpl().getDeviceTemperatures(param2String, param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createFloatArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CpuUsageInfo[] getCpuUsages(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IHardwarePropertiesManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IHardwarePropertiesManager.Stub.getDefaultImpl() != null)
            return IHardwarePropertiesManager.Stub.getDefaultImpl().getCpuUsages(param2String); 
          parcel2.readException();
          return parcel2.<CpuUsageInfo>createTypedArray(CpuUsageInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public float[] getFanSpeeds(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IHardwarePropertiesManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IHardwarePropertiesManager.Stub.getDefaultImpl() != null)
            return IHardwarePropertiesManager.Stub.getDefaultImpl().getFanSpeeds(param2String); 
          parcel2.readException();
          return parcel2.createFloatArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IHardwarePropertiesManager param1IHardwarePropertiesManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IHardwarePropertiesManager != null) {
          Proxy.sDefaultImpl = param1IHardwarePropertiesManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IHardwarePropertiesManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
