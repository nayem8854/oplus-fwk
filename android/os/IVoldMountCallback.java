package android.os;

import java.io.FileDescriptor;

public interface IVoldMountCallback extends IInterface {
  boolean onVolumeChecking(FileDescriptor paramFileDescriptor, String paramString1, String paramString2) throws RemoteException;
  
  class Default implements IVoldMountCallback {
    public boolean onVolumeChecking(FileDescriptor param1FileDescriptor, String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVoldMountCallback {
    private static final String DESCRIPTOR = "android.os.IVoldMountCallback";
    
    static final int TRANSACTION_onVolumeChecking = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IVoldMountCallback");
    }
    
    public static IVoldMountCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IVoldMountCallback");
      if (iInterface != null && iInterface instanceof IVoldMountCallback)
        return (IVoldMountCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onVolumeChecking";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.IVoldMountCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IVoldMountCallback");
      FileDescriptor fileDescriptor = param1Parcel1.readRawFileDescriptor();
      String str2 = param1Parcel1.readString();
      String str1 = param1Parcel1.readString();
      boolean bool = onVolumeChecking(fileDescriptor, str2, str1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements IVoldMountCallback {
      public static IVoldMountCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IVoldMountCallback";
      }
      
      public boolean onVolumeChecking(FileDescriptor param2FileDescriptor, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IVoldMountCallback");
          parcel1.writeRawFileDescriptor(param2FileDescriptor);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IVoldMountCallback.Stub.getDefaultImpl() != null) {
            bool1 = IVoldMountCallback.Stub.getDefaultImpl().onVolumeChecking(param2FileDescriptor, param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVoldMountCallback param1IVoldMountCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVoldMountCallback != null) {
          Proxy.sDefaultImpl = param1IVoldMountCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVoldMountCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
