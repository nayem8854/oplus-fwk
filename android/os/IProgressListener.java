package android.os;

public interface IProgressListener extends IInterface {
  void onFinished(int paramInt, Bundle paramBundle) throws RemoteException;
  
  void onProgress(int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException;
  
  void onStarted(int paramInt, Bundle paramBundle) throws RemoteException;
  
  class Default implements IProgressListener {
    public void onStarted(int param1Int, Bundle param1Bundle) throws RemoteException {}
    
    public void onProgress(int param1Int1, int param1Int2, Bundle param1Bundle) throws RemoteException {}
    
    public void onFinished(int param1Int, Bundle param1Bundle) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IProgressListener {
    private static final String DESCRIPTOR = "android.os.IProgressListener";
    
    static final int TRANSACTION_onFinished = 3;
    
    static final int TRANSACTION_onProgress = 2;
    
    static final int TRANSACTION_onStarted = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IProgressListener");
    }
    
    public static IProgressListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IProgressListener");
      if (iInterface != null && iInterface instanceof IProgressListener)
        return (IProgressListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onFinished";
        } 
        return "onProgress";
      } 
      return "onStarted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.os.IProgressListener");
            return true;
          } 
          param1Parcel1.enforceInterface("android.os.IProgressListener");
          param1Int1 = param1Parcel1.readInt();
          if (param1Parcel1.readInt() != 0) {
            Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onFinished(param1Int1, (Bundle)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.IProgressListener");
        param1Int2 = param1Parcel1.readInt();
        param1Int1 = param1Parcel1.readInt();
        if (param1Parcel1.readInt() != 0) {
          Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onProgress(param1Int2, param1Int1, (Bundle)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IProgressListener");
      param1Int1 = param1Parcel1.readInt();
      if (param1Parcel1.readInt() != 0) {
        Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onStarted(param1Int1, (Bundle)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IProgressListener {
      public static IProgressListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IProgressListener";
      }
      
      public void onStarted(int param2Int, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IProgressListener");
          parcel.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IProgressListener.Stub.getDefaultImpl() != null) {
            IProgressListener.Stub.getDefaultImpl().onStarted(param2Int, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onProgress(int param2Int1, int param2Int2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IProgressListener");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IProgressListener.Stub.getDefaultImpl() != null) {
            IProgressListener.Stub.getDefaultImpl().onProgress(param2Int1, param2Int2, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFinished(int param2Int, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IProgressListener");
          parcel.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IProgressListener.Stub.getDefaultImpl() != null) {
            IProgressListener.Stub.getDefaultImpl().onFinished(param2Int, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IProgressListener param1IProgressListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IProgressListener != null) {
          Proxy.sDefaultImpl = param1IProgressListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IProgressListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
