package android.os;

import android.annotation.SystemApi;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import com.android.internal.util.Preconditions;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.Executor;
import libcore.io.IoUtils;

@SystemApi
public final class BugreportManager {
  private static final String INTENT_UI_INTENSIVE_BUGREPORT_DUMPS_FINISHED = "com.android.internal.intent.action.UI_INTENSIVE_BUGREPORT_DUMPS_FINISHED";
  
  private static final String TAG = "BugreportManager";
  
  private final IDumpstate mBinder;
  
  private final Context mContext;
  
  public BugreportManager(Context paramContext, IDumpstate paramIDumpstate) {
    this.mContext = paramContext;
    this.mBinder = paramIDumpstate;
  }
  
  public static abstract class BugreportCallback {
    public static final int BUGREPORT_ERROR_ANOTHER_REPORT_IN_PROGRESS = 5;
    
    public static final int BUGREPORT_ERROR_INVALID_INPUT = 1;
    
    public static final int BUGREPORT_ERROR_RUNTIME = 2;
    
    public static final int BUGREPORT_ERROR_USER_CONSENT_TIMED_OUT = 4;
    
    public static final int BUGREPORT_ERROR_USER_DENIED_CONSENT = 3;
    
    public void onProgress(float param1Float) {}
    
    public void onError(int param1Int) {}
    
    public void onFinished() {}
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface BugreportErrorCode {}
  }
  
  public void startBugreport(ParcelFileDescriptor paramParcelFileDescriptor1, ParcelFileDescriptor paramParcelFileDescriptor2, BugreportParams paramBugreportParams, Executor paramExecutor, BugreportCallback paramBugreportCallback) {
    ParcelFileDescriptor parcelFileDescriptor1 = paramParcelFileDescriptor2, parcelFileDescriptor2 = paramParcelFileDescriptor2, parcelFileDescriptor3 = paramParcelFileDescriptor2;
    try {
      boolean bool;
      Preconditions.checkNotNull(paramParcelFileDescriptor1);
      parcelFileDescriptor1 = paramParcelFileDescriptor2;
      parcelFileDescriptor2 = paramParcelFileDescriptor2;
      parcelFileDescriptor3 = paramParcelFileDescriptor2;
      Preconditions.checkNotNull(paramBugreportParams);
      parcelFileDescriptor1 = paramParcelFileDescriptor2;
      parcelFileDescriptor2 = paramParcelFileDescriptor2;
      parcelFileDescriptor3 = paramParcelFileDescriptor2;
      Preconditions.checkNotNull(paramExecutor);
      parcelFileDescriptor1 = paramParcelFileDescriptor2;
      parcelFileDescriptor2 = paramParcelFileDescriptor2;
      parcelFileDescriptor3 = paramParcelFileDescriptor2;
      Preconditions.checkNotNull(paramBugreportCallback);
      if (paramParcelFileDescriptor2 != null) {
        bool = true;
      } else {
        bool = false;
      } 
      ParcelFileDescriptor parcelFileDescriptor = paramParcelFileDescriptor2;
      if (paramParcelFileDescriptor2 == null) {
        parcelFileDescriptor1 = paramParcelFileDescriptor2;
        parcelFileDescriptor2 = paramParcelFileDescriptor2;
        parcelFileDescriptor3 = paramParcelFileDescriptor2;
        File file = new File();
        parcelFileDescriptor1 = paramParcelFileDescriptor2;
        parcelFileDescriptor2 = paramParcelFileDescriptor2;
        parcelFileDescriptor3 = paramParcelFileDescriptor2;
        this("/dev/null");
        parcelFileDescriptor1 = paramParcelFileDescriptor2;
        parcelFileDescriptor2 = paramParcelFileDescriptor2;
        parcelFileDescriptor3 = paramParcelFileDescriptor2;
        parcelFileDescriptor = ParcelFileDescriptor.open(file, 268435456);
      } 
      parcelFileDescriptor1 = parcelFileDescriptor;
      parcelFileDescriptor2 = parcelFileDescriptor;
      parcelFileDescriptor3 = parcelFileDescriptor;
      DumpstateListener dumpstateListener = new DumpstateListener();
      parcelFileDescriptor1 = parcelFileDescriptor;
      parcelFileDescriptor2 = parcelFileDescriptor;
      parcelFileDescriptor3 = parcelFileDescriptor;
      this(this, paramExecutor, paramBugreportCallback, bool);
      parcelFileDescriptor1 = parcelFileDescriptor;
      parcelFileDescriptor2 = parcelFileDescriptor;
      parcelFileDescriptor3 = parcelFileDescriptor;
      IDumpstate iDumpstate = this.mBinder;
      parcelFileDescriptor1 = parcelFileDescriptor;
      parcelFileDescriptor2 = parcelFileDescriptor;
      parcelFileDescriptor3 = parcelFileDescriptor;
      Context context = this.mContext;
      parcelFileDescriptor1 = parcelFileDescriptor;
      parcelFileDescriptor2 = parcelFileDescriptor;
      parcelFileDescriptor3 = parcelFileDescriptor;
      String str = context.getOpPackageName();
      parcelFileDescriptor1 = parcelFileDescriptor;
      parcelFileDescriptor2 = parcelFileDescriptor;
      parcelFileDescriptor3 = parcelFileDescriptor;
      FileDescriptor fileDescriptor1 = paramParcelFileDescriptor1.getFileDescriptor();
      parcelFileDescriptor1 = parcelFileDescriptor;
      parcelFileDescriptor2 = parcelFileDescriptor;
      parcelFileDescriptor3 = parcelFileDescriptor;
      FileDescriptor fileDescriptor2 = parcelFileDescriptor.getFileDescriptor();
      parcelFileDescriptor1 = parcelFileDescriptor;
      parcelFileDescriptor2 = parcelFileDescriptor;
      parcelFileDescriptor3 = parcelFileDescriptor;
      int i = paramBugreportParams.getMode();
      parcelFileDescriptor1 = parcelFileDescriptor;
      parcelFileDescriptor2 = parcelFileDescriptor;
      parcelFileDescriptor3 = parcelFileDescriptor;
      iDumpstate.startBugreport(-1, str, fileDescriptor1, fileDescriptor2, i, dumpstateListener, bool);
      IoUtils.closeQuietly(paramParcelFileDescriptor1);
      if (parcelFileDescriptor != null) {
        IoUtils.closeQuietly(parcelFileDescriptor);
        return;
      } 
      return;
    } catch (RemoteException remoteException) {
      parcelFileDescriptor1 = parcelFileDescriptor3;
      throw remoteException.rethrowFromSystemServer();
    } catch (FileNotFoundException fileNotFoundException) {
      ParcelFileDescriptor parcelFileDescriptor;
      parcelFileDescriptor1 = parcelFileDescriptor2;
      Log.wtf("BugreportManager", "Not able to find /dev/null file: ", fileNotFoundException);
      IoUtils.closeQuietly(paramParcelFileDescriptor1);
      if (parcelFileDescriptor2 != null) {
        parcelFileDescriptor = parcelFileDescriptor2;
      } else {
        return;
      } 
      IoUtils.closeQuietly(parcelFileDescriptor);
      return;
    } finally {}
    IoUtils.closeQuietly(paramParcelFileDescriptor1);
    if (parcelFileDescriptor1 != null)
      IoUtils.closeQuietly(parcelFileDescriptor1); 
    throw paramParcelFileDescriptor2;
  }
  
  public void cancelBugreport() {
    try {
      this.mBinder.cancelBugreport();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void requestBugreport(BugreportParams paramBugreportParams, CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    // Byte code:
    //   0: aconst_null
    //   1: astore #4
    //   3: aload_2
    //   4: ifnonnull -> 12
    //   7: aconst_null
    //   8: astore_2
    //   9: goto -> 19
    //   12: aload_2
    //   13: invokeinterface toString : ()Ljava/lang/String;
    //   18: astore_2
    //   19: aload_3
    //   20: ifnonnull -> 29
    //   23: aload #4
    //   25: astore_3
    //   26: goto -> 36
    //   29: aload_3
    //   30: invokeinterface toString : ()Ljava/lang/String;
    //   35: astore_3
    //   36: invokestatic getService : ()Landroid/app/IActivityManager;
    //   39: astore #4
    //   41: aload_1
    //   42: invokevirtual getMode : ()I
    //   45: istore #5
    //   47: aload #4
    //   49: aload_2
    //   50: aload_3
    //   51: iload #5
    //   53: invokeinterface requestBugReportWithDescription : (Ljava/lang/String;Ljava/lang/String;I)V
    //   58: return
    //   59: astore_1
    //   60: aload_1
    //   61: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   64: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #219	-> 0
    //   #220	-> 19
    //   #221	-> 36
    //   #222	-> 41
    //   #221	-> 47
    //   #225	-> 58
    //   #226	-> 58
    //   #223	-> 59
    //   #224	-> 60
    // Exception table:
    //   from	to	target	type
    //   12	19	59	android/os/RemoteException
    //   29	36	59	android/os/RemoteException
    //   36	41	59	android/os/RemoteException
    //   41	47	59	android/os/RemoteException
    //   47	58	59	android/os/RemoteException
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface BugreportErrorCode {}
  
  class DumpstateListener extends IDumpstateListener.Stub {
    private final BugreportManager.BugreportCallback mCallback;
    
    private final Executor mExecutor;
    
    private final boolean mIsScreenshotRequested;
    
    final BugreportManager this$0;
    
    DumpstateListener(Executor param1Executor, BugreportManager.BugreportCallback param1BugreportCallback, boolean param1Boolean) {
      this.mExecutor = param1Executor;
      this.mCallback = param1BugreportCallback;
      this.mIsScreenshotRequested = param1Boolean;
    }
    
    public void onProgress(int param1Int) throws RemoteException {
      long l = Binder.clearCallingIdentity();
      try {
        Executor executor = this.mExecutor;
        _$$Lambda$BugreportManager$DumpstateListener$Vi18nEKTiYzuC_I5Io1XCZxd88w _$$Lambda$BugreportManager$DumpstateListener$Vi18nEKTiYzuC_I5Io1XCZxd88w = new _$$Lambda$BugreportManager$DumpstateListener$Vi18nEKTiYzuC_I5Io1XCZxd88w();
        this(this, param1Int);
        executor.execute(_$$Lambda$BugreportManager$DumpstateListener$Vi18nEKTiYzuC_I5Io1XCZxd88w);
        return;
      } finally {
        Binder.restoreCallingIdentity(l);
      } 
    }
    
    public void onError(int param1Int) throws RemoteException {
      long l = Binder.clearCallingIdentity();
      try {
        Executor executor = this.mExecutor;
        _$$Lambda$BugreportManager$DumpstateListener$srBmWyEMI_89xDivmKB4DtiSQ2A _$$Lambda$BugreportManager$DumpstateListener$srBmWyEMI_89xDivmKB4DtiSQ2A = new _$$Lambda$BugreportManager$DumpstateListener$srBmWyEMI_89xDivmKB4DtiSQ2A();
        this(this, param1Int);
        executor.execute(_$$Lambda$BugreportManager$DumpstateListener$srBmWyEMI_89xDivmKB4DtiSQ2A);
        return;
      } finally {
        Binder.restoreCallingIdentity(l);
      } 
    }
    
    public void onFinished() throws RemoteException {
      long l = Binder.clearCallingIdentity();
      try {
        Executor executor = this.mExecutor;
        _$$Lambda$BugreportManager$DumpstateListener$XpZbAM9CYGe3tPOak0Nw_HdFQ8I _$$Lambda$BugreportManager$DumpstateListener$XpZbAM9CYGe3tPOak0Nw_HdFQ8I = new _$$Lambda$BugreportManager$DumpstateListener$XpZbAM9CYGe3tPOak0Nw_HdFQ8I();
        this(this);
        executor.execute(_$$Lambda$BugreportManager$DumpstateListener$XpZbAM9CYGe3tPOak0Nw_HdFQ8I);
        return;
      } finally {
        Binder.restoreCallingIdentity(l);
      } 
    }
    
    public void onScreenshotTaken(boolean param1Boolean) throws RemoteException {
      if (!this.mIsScreenshotRequested)
        return; 
      Handler handler = new Handler(Looper.getMainLooper());
      handler.post(new _$$Lambda$BugreportManager$DumpstateListener$25_fbbhpwKLVX23K6WDSFGsWmHM(this, param1Boolean));
    }
    
    public void onUiIntensiveBugreportDumpsFinished(String param1String) throws RemoteException {
      long l = Binder.clearCallingIdentity();
      try {
        Executor executor = this.mExecutor;
        _$$Lambda$BugreportManager$DumpstateListener$8jYsAwxtBYQkBW2vxfMUYgDXLK8 _$$Lambda$BugreportManager$DumpstateListener$8jYsAwxtBYQkBW2vxfMUYgDXLK8 = new _$$Lambda$BugreportManager$DumpstateListener$8jYsAwxtBYQkBW2vxfMUYgDXLK8();
        this(this, param1String);
        executor.execute(_$$Lambda$BugreportManager$DumpstateListener$8jYsAwxtBYQkBW2vxfMUYgDXLK8);
        return;
      } finally {
        Binder.restoreCallingIdentity(l);
      } 
    }
  }
}
