package android.os;

import android.app.ActivityThread;
import android.app.AppOpsManager;
import android.util.Log;
import android.util.SparseIntArray;
import com.android.internal.os.BinderInternal;
import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import libcore.util.NativeAllocationRegistry;

public final class BinderProxy implements IBinder {
  volatile boolean mWarnOnBlocking = Binder.sWarnOnBlocking;
  
  private final long mNativeData;
  
  private static volatile Binder.ProxyTransactListener sTransactListener = null;
  
  private static final ProxyMap sProxyMap;
  
  private static final int NATIVE_ALLOCATION_SIZE = 3000;
  
  public static void setTransactListener(Binder.ProxyTransactListener paramProxyTransactListener) {
    sTransactListener = paramProxyTransactListener;
  }
  
  class ProxyMap {
    private int mWarnBucketSize = 20;
    
    private static int hash(long param1Long) {
      return (int)(param1Long >> 2L ^ param1Long >> 10L) & 0xFF;
    }
    
    private int size() {
      int i = 0;
      ArrayList<WeakReference<BinderProxy>>[] arrayOfArrayList;
      int j;
      byte b;
      for (arrayOfArrayList = this.mMainIndexValues, j = arrayOfArrayList.length, b = 0; b < j; ) {
        ArrayList<WeakReference<BinderProxy>> arrayList = arrayOfArrayList[b];
        int k = i;
        if (arrayList != null)
          k = i + arrayList.size(); 
        b++;
        i = k;
      } 
      return i;
    }
    
    private int unclearedSize() {
      int i = 0;
      ArrayList<WeakReference<BinderProxy>>[] arrayOfArrayList;
      int j;
      byte b;
      for (arrayOfArrayList = this.mMainIndexValues, j = arrayOfArrayList.length, b = 0; b < j; ) {
        ArrayList<WeakReference<BinderProxy>> arrayList = arrayOfArrayList[b];
        int k = i;
        if (arrayList != null) {
          Iterator<WeakReference<BinderProxy>> iterator = arrayList.iterator();
          while (true) {
            k = i;
            if (iterator.hasNext()) {
              WeakReference weakReference = iterator.next();
              k = i;
              if (weakReference.get() != null)
                k = i + 1; 
              i = k;
              continue;
            } 
            break;
          } 
        } 
        b++;
        i = k;
      } 
      return i;
    }
    
    private void remove(int param1Int1, int param1Int2) {
      Long[] arrayOfLong = this.mMainIndexKeys[param1Int1];
      ArrayList<WeakReference<BinderProxy>> arrayList = this.mMainIndexValues[param1Int1];
      param1Int1 = arrayList.size();
      if (param1Int2 != param1Int1 - 1) {
        arrayOfLong[param1Int2] = arrayOfLong[param1Int1 - 1];
        arrayList.set(param1Int2, arrayList.get(param1Int1 - 1));
      } 
      arrayList.remove(param1Int1 - 1);
    }
    
    BinderProxy get(long param1Long) {
      int i = hash(param1Long);
      Long[] arrayOfLong = this.mMainIndexKeys[i];
      if (arrayOfLong == null)
        return null; 
      ArrayList<WeakReference<BinderProxy>> arrayList = this.mMainIndexValues[i];
      int j = arrayList.size();
      for (byte b = 0; b < j; b++) {
        long l = arrayOfLong[b].longValue();
        if (param1Long == l) {
          WeakReference<BinderProxy> weakReference = arrayList.get(b);
          BinderProxy binderProxy = weakReference.get();
          if (binderProxy != null)
            return binderProxy; 
          remove(i, b);
          return null;
        } 
      } 
      return null;
    }
    
    void set(long param1Long, BinderProxy param1BinderProxy) {
      Long[] arrayOfLong1;
      int i = hash(param1Long);
      ArrayList<WeakReference<BinderProxy>> arrayOfArrayList[] = this.mMainIndexValues, arrayList1 = arrayOfArrayList[i];
      ArrayList<WeakReference<BinderProxy>> arrayList2 = arrayList1;
      if (arrayList1 == null) {
        arrayList2 = new ArrayList<>();
        arrayOfArrayList[i] = arrayList2;
        this.mMainIndexKeys[i] = new Long[1];
      } 
      int j = arrayList2.size();
      WeakReference<BinderProxy> weakReference = new WeakReference<>(param1BinderProxy);
      int k;
      for (k = 0; k < j; k++) {
        if (((WeakReference)arrayList2.get(k)).get() == null) {
          arrayList2.set(k, weakReference);
          arrayOfLong1 = this.mMainIndexKeys[i];
          arrayOfLong1[k] = Long.valueOf(param1Long);
          if (k < j - 1) {
            int m = this.mRandom + 1;
            j = Math.floorMod(m, j - k + 1);
            if (((WeakReference)arrayList2.get(k + 1 + j)).get() == null)
              remove(i, k + 1 + j); 
          } 
          return;
        } 
      } 
      arrayList2.add(j, arrayOfLong1);
      Long[] arrayOfLong2 = this.mMainIndexKeys[i];
      if (arrayOfLong2.length == j) {
        arrayOfLong1 = new Long[j / 2 + j + 2];
        System.arraycopy(arrayOfLong2, 0, arrayOfLong1, 0, j);
        arrayOfLong1[j] = Long.valueOf(param1Long);
        this.mMainIndexKeys[i] = arrayOfLong1;
      } else {
        arrayOfLong2[j] = Long.valueOf(param1Long);
      } 
      if (j >= this.mWarnBucketSize) {
        k = size();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("BinderProxy map growth! bucket size = ");
        stringBuilder.append(j);
        stringBuilder.append(" total = ");
        stringBuilder.append(k);
        Log.v("Binder", stringBuilder.toString());
        this.mWarnBucketSize += 10;
        if (Build.IS_DEBUGGABLE && k >= 20000) {
          i = unclearedSize();
          if (i >= 20000) {
            dumpProxyInterfaceCounts();
            dumpPerUidProxyCounts();
            Runtime.getRuntime().gc();
            if (ActivityThread.isSystem()) {
              SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
              String str1 = simpleDateFormat.format(new Date());
              str1 = str1.replaceAll(" ", "_");
              String str2 = str1.replaceAll(":", "-");
              try {
                StringBuilder stringBuilder1 = new StringBuilder();
                this();
                stringBuilder1.append("/data/anr/");
                stringBuilder1.append(str2);
                stringBuilder1.append(".hprof");
                Debug.dumpHprofData(stringBuilder1.toString());
              } catch (IOException iOException) {
                Log.w("Binder", "system server heap dump failed ");
              } 
            } 
            stringBuilder = new StringBuilder();
            stringBuilder.append("Binder ProxyMap has too many entries: ");
            stringBuilder.append(k);
            stringBuilder.append(" (total), ");
            stringBuilder.append(i);
            stringBuilder.append(" (uncleared), ");
            stringBuilder.append(unclearedSize());
            stringBuilder.append(" (uncleared after GC). BinderProxy leak?");
            throw new AssertionError(stringBuilder.toString());
          } 
          if (k > i * 3 / 2) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("BinderProxy map has many cleared entries: ");
            stringBuilder.append(k - i);
            stringBuilder.append(" of ");
            stringBuilder.append(k);
            stringBuilder.append(" are cleared");
            Log.v("Binder", stringBuilder.toString());
          } 
        } 
      } 
    }
    
    private BinderProxy.InterfaceCount[] getSortedInterfaceCounts(int param1Int) {
      // Byte code:
      //   0: iload_1
      //   1: iflt -> 357
      //   4: new java/util/HashMap
      //   7: dup
      //   8: invokespecial <init> : ()V
      //   11: astore_2
      //   12: new java/util/ArrayList
      //   15: dup
      //   16: invokespecial <init> : ()V
      //   19: astore_3
      //   20: invokestatic access$000 : ()Landroid/os/BinderProxy$ProxyMap;
      //   23: astore #4
      //   25: aload #4
      //   27: monitorenter
      //   28: aload_0
      //   29: getfield mMainIndexValues : [Ljava/util/ArrayList;
      //   32: astore #5
      //   34: aload #5
      //   36: arraylength
      //   37: istore #6
      //   39: iconst_0
      //   40: istore #7
      //   42: iload #7
      //   44: iload #6
      //   46: if_icmpge -> 74
      //   49: aload #5
      //   51: iload #7
      //   53: aaload
      //   54: astore #8
      //   56: aload #8
      //   58: ifnull -> 68
      //   61: aload_3
      //   62: aload #8
      //   64: invokevirtual addAll : (Ljava/util/Collection;)Z
      //   67: pop
      //   68: iinc #7, 1
      //   71: goto -> 42
      //   74: aload #4
      //   76: monitorexit
      //   77: iconst_0
      //   78: invokestatic enableFreezer : (Z)V
      //   81: aload_3
      //   82: invokevirtual iterator : ()Ljava/util/Iterator;
      //   85: astore_3
      //   86: aload_3
      //   87: invokeinterface hasNext : ()Z
      //   92: ifeq -> 233
      //   95: aload_3
      //   96: invokeinterface next : ()Ljava/lang/Object;
      //   101: checkcast java/lang/ref/WeakReference
      //   104: astore #4
      //   106: aload #4
      //   108: invokevirtual get : ()Ljava/lang/Object;
      //   111: checkcast android/os/BinderProxy
      //   114: astore #8
      //   116: aload #8
      //   118: ifnonnull -> 128
      //   121: ldc '<cleared weak-ref>'
      //   123: astore #4
      //   125: goto -> 177
      //   128: aload #8
      //   130: invokevirtual getInterfaceDescriptor : ()Ljava/lang/String;
      //   133: astore #5
      //   135: aload #5
      //   137: ifnull -> 152
      //   140: aload #5
      //   142: astore #4
      //   144: aload #5
      //   146: invokevirtual isEmpty : ()Z
      //   149: ifeq -> 168
      //   152: aload #5
      //   154: astore #4
      //   156: aload #8
      //   158: invokevirtual isBinderAlive : ()Z
      //   161: ifne -> 168
      //   164: ldc '<proxy to dead node>'
      //   166: astore #4
      //   168: goto -> 177
      //   171: astore #4
      //   173: ldc '<exception during getDescriptor>'
      //   175: astore #4
      //   177: aload_2
      //   178: aload #4
      //   180: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   185: checkcast java/lang/Integer
      //   188: astore #5
      //   190: aload #5
      //   192: ifnonnull -> 211
      //   195: aload_2
      //   196: aload #4
      //   198: iconst_1
      //   199: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   202: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   207: pop
      //   208: goto -> 230
      //   211: aload_2
      //   212: aload #4
      //   214: aload #5
      //   216: invokevirtual intValue : ()I
      //   219: iconst_1
      //   220: iadd
      //   221: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   224: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   229: pop
      //   230: goto -> 86
      //   233: iconst_1
      //   234: invokestatic enableFreezer : (Z)V
      //   237: aload_2
      //   238: invokeinterface entrySet : ()Ljava/util/Set;
      //   243: astore #4
      //   245: aload_2
      //   246: invokeinterface size : ()I
      //   251: anewarray java/util/Map$Entry
      //   254: astore #5
      //   256: aload #4
      //   258: aload #5
      //   260: invokeinterface toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
      //   265: checkcast [Ljava/util/Map$Entry;
      //   268: astore #4
      //   270: aload #4
      //   272: getstatic android/os/_$$Lambda$BinderProxy$ProxyMap$aKNUVKkR8bNu2XRFxaO2PW1AFBA.INSTANCE : Landroid/os/-$$Lambda$BinderProxy$ProxyMap$aKNUVKkR8bNu2XRFxaO2PW1AFBA;
      //   275: invokestatic sort : ([Ljava/lang/Object;Ljava/util/Comparator;)V
      //   278: iload_1
      //   279: aload #4
      //   281: arraylength
      //   282: invokestatic min : (II)I
      //   285: istore #7
      //   287: iload #7
      //   289: anewarray android/os/BinderProxy$InterfaceCount
      //   292: astore #5
      //   294: iconst_0
      //   295: istore_1
      //   296: iload_1
      //   297: iload #7
      //   299: if_icmpge -> 346
      //   302: aload #5
      //   304: iload_1
      //   305: new android/os/BinderProxy$InterfaceCount
      //   308: dup
      //   309: aload #4
      //   311: iload_1
      //   312: aaload
      //   313: invokeinterface getKey : ()Ljava/lang/Object;
      //   318: checkcast java/lang/String
      //   321: aload #4
      //   323: iload_1
      //   324: aaload
      //   325: invokeinterface getValue : ()Ljava/lang/Object;
      //   330: checkcast java/lang/Integer
      //   333: invokevirtual intValue : ()I
      //   336: invokespecial <init> : (Ljava/lang/String;I)V
      //   339: aastore
      //   340: iinc #1, 1
      //   343: goto -> 296
      //   346: aload #5
      //   348: areturn
      //   349: astore #5
      //   351: aload #4
      //   353: monitorexit
      //   354: aload #5
      //   356: athrow
      //   357: new java/lang/IllegalArgumentException
      //   360: dup
      //   361: ldc 'negative interface count'
      //   363: invokespecial <init> : (Ljava/lang/String;)V
      //   366: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #255	-> 0
      //   #259	-> 4
      //   #260	-> 12
      //   #262	-> 20
      //   #263	-> 28
      //   #264	-> 56
      //   #265	-> 61
      //   #263	-> 68
      //   #268	-> 74
      //   #273	-> 77
      //   #274	-> 81
      //   #275	-> 106
      //   #277	-> 116
      //   #278	-> 121
      //   #281	-> 128
      //   #282	-> 135
      //   #283	-> 164
      //   #287	-> 168
      //   #285	-> 171
      //   #286	-> 173
      //   #289	-> 177
      //   #290	-> 190
      //   #291	-> 195
      //   #293	-> 211
      //   #295	-> 230
      //   #296	-> 233
      //   #297	-> 237
      //   #298	-> 245
      //   #297	-> 256
      //   #300	-> 270
      //   #303	-> 278
      //   #304	-> 287
      //   #305	-> 294
      //   #306	-> 302
      //   #305	-> 340
      //   #308	-> 346
      //   #268	-> 349
      //   #256	-> 357
      // Exception table:
      //   from	to	target	type
      //   28	39	349	finally
      //   61	68	349	finally
      //   74	77	349	finally
      //   128	135	171	finally
      //   144	152	171	finally
      //   156	164	171	finally
      //   351	354	349	finally
    }
    
    private void dumpProxyInterfaceCounts() {
      BinderProxy.InterfaceCount[] arrayOfInterfaceCount = getSortedInterfaceCounts(10);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("BinderProxy descriptor histogram (top ");
      stringBuilder.append(Integer.toString(10));
      stringBuilder.append("):");
      String str = stringBuilder.toString();
      Log.v("Binder", str);
      for (byte b = 0; b < arrayOfInterfaceCount.length; b++) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(" #");
        stringBuilder1.append(b + 1);
        stringBuilder1.append(": ");
        stringBuilder1.append(arrayOfInterfaceCount[b]);
        Log.v("Binder", stringBuilder1.toString());
      } 
    }
    
    private void dumpPerUidProxyCounts() {
      SparseIntArray sparseIntArray = BinderInternal.nGetBinderProxyPerUidCounts();
      if (sparseIntArray.size() == 0)
        return; 
      Log.d("Binder", "Per Uid Binder Proxy Counts:");
      for (byte b = 0; b < sparseIntArray.size(); b++) {
        int i = sparseIntArray.keyAt(b);
        int j = sparseIntArray.valueAt(b);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UID : ");
        stringBuilder.append(i);
        stringBuilder.append("  count = ");
        stringBuilder.append(j);
        Log.d("Binder", stringBuilder.toString());
      } 
    }
    
    private final Long[][] mMainIndexKeys = new Long[256][];
    
    private final ArrayList<WeakReference<BinderProxy>>[] mMainIndexValues = (ArrayList<WeakReference<BinderProxy>>[])new ArrayList[256];
    
    private static final int CRASH_AT_SIZE = 20000;
    
    private static final int LOG_MAIN_INDEX_SIZE = 8;
    
    private static final int MAIN_INDEX_MASK = 255;
    
    private static final int MAIN_INDEX_SIZE = 256;
    
    static final int MAX_NUM_INTERFACES_TO_DUMP = 10;
    
    private static final int WARN_INCREMENT = 10;
    
    private int mRandom;
    
    private ProxyMap() {}
  }
  
  static {
    sProxyMap = new ProxyMap();
  }
  
  class InterfaceCount {
    private final int mCount;
    
    private final String mInterfaceName;
    
    InterfaceCount(BinderProxy this$0, int param1Int) {
      this.mInterfaceName = (String)this$0;
      this.mCount = param1Int;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.mInterfaceName);
      stringBuilder.append(" x");
      stringBuilder.append(Integer.toString(this.mCount));
      return stringBuilder.toString();
    }
  }
  
  public static InterfaceCount[] getSortedInterfaceCounts(int paramInt) {
    return sProxyMap.getSortedInterfaceCounts(paramInt);
  }
  
  public static int getProxyCount() {
    synchronized (sProxyMap) {
      return sProxyMap.size();
    } 
  }
  
  public static void dumpProxyDebugInfo() {}
  
  private static BinderProxy getInstance(long paramLong1, long paramLong2) {
    // Byte code:
    //   0: getstatic android/os/BinderProxy.sProxyMap : Landroid/os/BinderProxy$ProxyMap;
    //   3: astore #4
    //   5: aload #4
    //   7: monitorenter
    //   8: getstatic android/os/BinderProxy.sProxyMap : Landroid/os/BinderProxy$ProxyMap;
    //   11: lload_2
    //   12: invokevirtual get : (J)Landroid/os/BinderProxy;
    //   15: astore #5
    //   17: aload #5
    //   19: ifnull -> 28
    //   22: aload #4
    //   24: monitorexit
    //   25: aload #5
    //   27: areturn
    //   28: new android/os/BinderProxy
    //   31: astore #5
    //   33: aload #5
    //   35: lload_0
    //   36: invokespecial <init> : (J)V
    //   39: getstatic android/os/BinderProxy$NoImagePreloadHolder.sRegistry : Llibcore/util/NativeAllocationRegistry;
    //   42: aload #5
    //   44: lload_0
    //   45: invokevirtual registerNativeAllocation : (Ljava/lang/Object;J)Ljava/lang/Runnable;
    //   48: pop
    //   49: getstatic android/os/BinderProxy.sProxyMap : Landroid/os/BinderProxy$ProxyMap;
    //   52: lload_2
    //   53: aload #5
    //   55: invokevirtual set : (JLandroid/os/BinderProxy;)V
    //   58: aload #4
    //   60: monitorexit
    //   61: aload #5
    //   63: areturn
    //   64: astore #5
    //   66: getstatic android/os/BinderProxy$NoImagePreloadHolder.sNativeFinalizer : J
    //   69: lload_0
    //   70: invokestatic applyFreeFunction : (JJ)V
    //   73: aload #5
    //   75: athrow
    //   76: astore #5
    //   78: aload #4
    //   80: monitorexit
    //   81: aload #5
    //   83: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #428	-> 0
    //   #430	-> 8
    //   #431	-> 17
    //   #432	-> 22
    //   #434	-> 28
    //   #440	-> 39
    //   #441	-> 39
    //   #443	-> 49
    //   #444	-> 58
    //   #445	-> 61
    //   #435	-> 64
    //   #437	-> 66
    //   #439	-> 73
    //   #444	-> 76
    // Exception table:
    //   from	to	target	type
    //   8	17	64	finally
    //   22	25	76	finally
    //   28	39	64	finally
    //   39	49	76	finally
    //   49	58	76	finally
    //   58	61	76	finally
    //   66	73	76	finally
    //   73	76	76	finally
    //   78	81	76	finally
  }
  
  private BinderProxy(long paramLong) {
    this.mNativeData = paramLong;
  }
  
  class NoImagePreloadHolder {
    public static final long sNativeFinalizer = BinderProxy.getNativeFinalizer();
    
    public static final NativeAllocationRegistry sRegistry = new NativeAllocationRegistry(BinderProxy.class.getClassLoader(), sNativeFinalizer, 3000L);
  }
  
  public IInterface queryLocalInterface(String paramString) {
    return null;
  }
  
  public boolean transact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2) throws RemoteException {
    Binder.checkParcel(this, paramInt1, paramParcel1, "Unreasonably large binder buffer");
    if (this.mWarnOnBlocking && (paramInt2 & 0x1) == 0) {
      ThreadLocal<Boolean> threadLocal = Binder.sWarnOnBlockingOnCurrentThread;
      if (((Boolean)threadLocal.get()).booleanValue()) {
        this.mWarnOnBlocking = false;
        if (Build.IS_USERDEBUG) {
          Log.wtf("Binder", "Outgoing transactions from this process must be FLAG_ONEWAY", new Throwable());
        } else {
          Log.w("Binder", "Outgoing transactions from this process must be FLAG_ONEWAY", new Throwable());
        } 
      } 
    } 
    boolean bool = Binder.isTracingEnabled();
    if (bool) {
      Throwable throwable = new Throwable();
      Binder.getTransactionTracker().addTrace(throwable);
      StackTraceElement stackTraceElement = throwable.getStackTrace()[1];
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(stackTraceElement.getClassName());
      stringBuilder.append(".");
      stringBuilder.append(stackTraceElement.getMethodName());
      String str = stringBuilder.toString();
      Trace.traceBegin(1L, str);
    } 
    Binder.ProxyTransactListener proxyTransactListener = sTransactListener;
    Object object = null;
    if (proxyTransactListener != null) {
      int j = Binder.getCallingWorkSourceUid();
      Object object1 = proxyTransactListener.onTransactStarted(this, paramInt1, paramInt2);
      int k = Binder.getCallingWorkSourceUid();
      object = object1;
      if (j != k) {
        paramParcel1.replaceCallingWorkSourceUid(k);
        object = object1;
      } 
    } 
    AppOpsManager.PausedNotedAppOpsCollection pausedNotedAppOpsCollection = AppOpsManager.pauseNotedAppOpsCollection();
    int i = paramInt2;
    if ((paramInt2 & 0x1) == 0) {
      i = paramInt2;
      if (AppOpsManager.isListeningForOpNoted())
        i = paramInt2 | 0x2; 
    } 
    try {
      return transactNative(paramInt1, paramParcel1, paramParcel2, i);
    } finally {
      AppOpsManager.resumeNotedAppOpsCollection(pausedNotedAppOpsCollection);
      if (proxyTransactListener != null)
        proxyTransactListener.onTransactEnded(object); 
      if (bool)
        Trace.traceEnd(1L); 
    } 
  }
  
  public void dump(FileDescriptor paramFileDescriptor, String[] paramArrayOfString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    parcel1.writeFileDescriptor(paramFileDescriptor);
    parcel1.writeStringArray(paramArrayOfString);
    try {
      transact(1598311760, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void dumpAsync(FileDescriptor paramFileDescriptor, String[] paramArrayOfString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    parcel1.writeFileDescriptor(paramFileDescriptor);
    parcel1.writeStringArray(paramArrayOfString);
    try {
      transact(1598311760, parcel1, parcel2, 1);
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void shellCommand(FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, FileDescriptor paramFileDescriptor3, String[] paramArrayOfString, ShellCallback paramShellCallback, ResultReceiver paramResultReceiver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    parcel1.writeFileDescriptor(paramFileDescriptor1);
    parcel1.writeFileDescriptor(paramFileDescriptor2);
    parcel1.writeFileDescriptor(paramFileDescriptor3);
    parcel1.writeStringArray(paramArrayOfString);
    ShellCallback.writeToParcel(paramShellCallback, parcel1);
    paramResultReceiver.writeToParcel(parcel1, 0);
    try {
      transact(1598246212, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  private static void sendDeathNotice(IBinder.DeathRecipient paramDeathRecipient, IBinder paramIBinder) {
    try {
      paramDeathRecipient.binderDied(paramIBinder);
    } catch (RuntimeException runtimeException) {
      Log.w("BinderNative", "Uncaught exception from death notification", runtimeException);
    } 
  }
  
  private static native long getNativeFinalizer();
  
  public native IBinder getExtension() throws RemoteException;
  
  public native String getInterfaceDescriptor() throws RemoteException;
  
  public native boolean isBinderAlive();
  
  public native void linkToDeath(IBinder.DeathRecipient paramDeathRecipient, int paramInt) throws RemoteException;
  
  public native boolean pingBinder();
  
  public native boolean transactNative(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2) throws RemoteException;
  
  public native boolean unlinkToDeath(IBinder.DeathRecipient paramDeathRecipient, int paramInt);
}
