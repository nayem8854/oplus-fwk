package android.os;

import java.io.FileDescriptor;

public interface IDumpstate extends IInterface {
  public static final int BUGREPORT_MODE_DEFAULT = 6;
  
  public static final int BUGREPORT_MODE_FULL = 0;
  
  public static final int BUGREPORT_MODE_INTERACTIVE = 1;
  
  public static final int BUGREPORT_MODE_REMOTE = 2;
  
  public static final int BUGREPORT_MODE_TELEPHONY = 4;
  
  public static final int BUGREPORT_MODE_WEAR = 3;
  
  public static final int BUGREPORT_MODE_WIFI = 5;
  
  void cancelBugreport() throws RemoteException;
  
  void startBugreport(int paramInt1, String paramString, FileDescriptor paramFileDescriptor1, FileDescriptor paramFileDescriptor2, int paramInt2, IDumpstateListener paramIDumpstateListener, boolean paramBoolean) throws RemoteException;
  
  class Default implements IDumpstate {
    public void startBugreport(int param1Int1, String param1String, FileDescriptor param1FileDescriptor1, FileDescriptor param1FileDescriptor2, int param1Int2, IDumpstateListener param1IDumpstateListener, boolean param1Boolean) throws RemoteException {}
    
    public void cancelBugreport() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDumpstate {
    private static final String DESCRIPTOR = "android.os.IDumpstate";
    
    static final int TRANSACTION_cancelBugreport = 2;
    
    static final int TRANSACTION_startBugreport = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IDumpstate");
    }
    
    public static IDumpstate asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IDumpstate");
      if (iInterface != null && iInterface instanceof IDumpstate)
        return (IDumpstate)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "cancelBugreport";
      } 
      return "startBugreport";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.os.IDumpstate");
          return true;
        } 
        param1Parcel1.enforceInterface("android.os.IDumpstate");
        cancelBugreport();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IDumpstate");
      param1Int2 = param1Parcel1.readInt();
      String str = param1Parcel1.readString();
      FileDescriptor fileDescriptor1 = param1Parcel1.readRawFileDescriptor();
      FileDescriptor fileDescriptor2 = param1Parcel1.readRawFileDescriptor();
      param1Int1 = param1Parcel1.readInt();
      IDumpstateListener iDumpstateListener = IDumpstateListener.Stub.asInterface(param1Parcel1.readStrongBinder());
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      startBugreport(param1Int2, str, fileDescriptor1, fileDescriptor2, param1Int1, iDumpstateListener, bool);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IDumpstate {
      public static IDumpstate sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IDumpstate";
      }
      
      public void startBugreport(int param2Int1, String param2String, FileDescriptor param2FileDescriptor1, FileDescriptor param2FileDescriptor2, int param2Int2, IDumpstateListener param2IDumpstateListener, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDumpstate");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String);
              try {
                parcel1.writeRawFileDescriptor(param2FileDescriptor1);
                try {
                  parcel1.writeRawFileDescriptor(param2FileDescriptor2);
                  try {
                    IBinder iBinder;
                    boolean bool;
                    parcel1.writeInt(param2Int2);
                    if (param2IDumpstateListener != null) {
                      iBinder = param2IDumpstateListener.asBinder();
                    } else {
                      iBinder = null;
                    } 
                    parcel1.writeStrongBinder(iBinder);
                    if (param2Boolean) {
                      bool = true;
                    } else {
                      bool = false;
                    } 
                    parcel1.writeInt(bool);
                    boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
                    if (!bool1 && IDumpstate.Stub.getDefaultImpl() != null) {
                      IDumpstate.Stub.getDefaultImpl().startBugreport(param2Int1, param2String, param2FileDescriptor1, param2FileDescriptor2, param2Int2, param2IDumpstateListener, param2Boolean);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void cancelBugreport() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IDumpstate");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IDumpstate.Stub.getDefaultImpl() != null) {
            IDumpstate.Stub.getDefaultImpl().cancelBugreport();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDumpstate param1IDumpstate) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDumpstate != null) {
          Proxy.sDefaultImpl = param1IDumpstate;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDumpstate getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
