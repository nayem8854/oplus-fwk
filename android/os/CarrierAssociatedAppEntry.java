package android.os;

public final class CarrierAssociatedAppEntry implements Parcelable {
  public CarrierAssociatedAppEntry(String paramString, int paramInt) {
    this.packageName = paramString;
    this.addedInSdk = paramInt;
  }
  
  public CarrierAssociatedAppEntry(Parcel paramParcel) {
    this.packageName = paramParcel.readString();
    this.addedInSdk = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.packageName);
    paramParcel.writeInt(this.addedInSdk);
  }
  
  public static final Parcelable.Creator<CarrierAssociatedAppEntry> CREATOR = new Parcelable.Creator<CarrierAssociatedAppEntry>() {
      public CarrierAssociatedAppEntry createFromParcel(Parcel param1Parcel) {
        return new CarrierAssociatedAppEntry(param1Parcel);
      }
      
      public CarrierAssociatedAppEntry[] newArray(int param1Int) {
        return new CarrierAssociatedAppEntry[param1Int];
      }
    };
  
  public static final int SDK_UNSPECIFIED = -1;
  
  public final int addedInSdk;
  
  public final String packageName;
}
