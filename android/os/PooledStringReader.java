package android.os;

public class PooledStringReader {
  private final Parcel mIn;
  
  private final String[] mPool;
  
  public PooledStringReader(Parcel paramParcel) {
    this.mIn = paramParcel;
    int i = paramParcel.readInt();
    this.mPool = new String[i];
  }
  
  public int getStringCount() {
    return this.mPool.length;
  }
  
  public String readString() {
    int i = this.mIn.readInt();
    if (i >= 0)
      return this.mPool[i]; 
    i = -i;
    String str = this.mIn.readString();
    this.mPool[i - 1] = str;
    return str;
  }
}
