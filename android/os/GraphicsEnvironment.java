package android.os;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.provider.Settings;
import android.util.Log;
import dalvik.system.VMRuntime;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GraphicsEnvironment {
  private static final String ACTION_ANGLE_FOR_ANDROID = "android.app.action.ANGLE_FOR_ANDROID";
  
  private static final String ACTION_ANGLE_FOR_ANDROID_TOAST_MESSAGE = "android.app.action.ANGLE_FOR_ANDROID_TOAST_MESSAGE";
  
  private static final String ANGLE_RULES_FILE = "a4a_rules.json";
  
  private static final String ANGLE_TEMP_RULES = "debug.angle.rules";
  
  private static final boolean DEBUG = false;
  
  private static final int GAME_DRIVER_GLOBAL_OPT_IN_DEFAULT = 0;
  
  private static final int GAME_DRIVER_GLOBAL_OPT_IN_GAME_DRIVER = 1;
  
  private static final int GAME_DRIVER_GLOBAL_OPT_IN_OFF = 3;
  
  private static final int GAME_DRIVER_GLOBAL_OPT_IN_PRERELEASE_DRIVER = 2;
  
  private static final String GAME_DRIVER_SPHAL_LIBRARIES_FILENAME = "sphal_libraries.txt";
  
  private static final String GAME_DRIVER_WHITELIST_ALL = "*";
  
  private static final String INTENT_KEY_A4A_TOAST_MESSAGE = "A4A Toast Message";
  
  private static final String METADATA_DEVELOPER_DRIVER_ENABLE = "com.android.graphics.developerdriver.enable";
  
  private static final String METADATA_DRIVER_BUILD_TIME = "com.android.gamedriver.build_time";
  
  private static final String METADATA_INJECT_LAYERS_ENABLE = "com.android.graphics.injectLayers.enable";
  
  private static final String PROPERTY_GFX_DRIVER = "ro.gfx.driver.0";
  
  private static final String PROPERTY_GFX_DRIVER_BUILD_TIME = "ro.gfx.driver_build_time";
  
  private static final String PROPERTY_GFX_DRIVER_PRERELEASE = "ro.gfx.driver.1";
  
  private static final String SYSTEM_DRIVER_NAME = "system";
  
  private static final long SYSTEM_DRIVER_VERSION_CODE = 0L;
  
  private static final String SYSTEM_DRIVER_VERSION_NAME = "";
  
  private static final String TAG = "GraphicsEnvironment";
  
  private static final int VULKAN_1_0 = 4194304;
  
  private static final int VULKAN_1_1 = 4198400;
  
  private static final Map<OpenGlDriverChoice, String> sDriverMap;
  
  private static final GraphicsEnvironment sInstance = new GraphicsEnvironment();
  
  private ClassLoader mClassLoader;
  
  private String mLibraryPermittedPaths;
  
  private String mLibrarySearchPaths;
  
  public static GraphicsEnvironment getInstance() {
    return sInstance;
  }
  
  public void setup(Context paramContext, Bundle paramBundle) {
    PackageManager packageManager = paramContext.getPackageManager();
    String str = paramContext.getPackageName();
    ApplicationInfo applicationInfo = getAppInfoWithMetadata(paramContext, packageManager, str);
    Trace.traceBegin(2L, "setupGpuLayers");
    setupGpuLayers(paramContext, paramBundle, packageManager, str, applicationInfo);
    Trace.traceEnd(2L);
    Trace.traceBegin(2L, "setupAngle");
    setupAngle(paramContext, paramBundle, packageManager, str);
    Trace.traceEnd(2L);
    Trace.traceBegin(2L, "chooseDriver");
    if (!chooseDriver(paramContext, paramBundle, packageManager, str, applicationInfo)) {
      long l = SystemProperties.getLong("ro.gfx.driver_build_time", 0L);
      int i = getVulkanVersion(packageManager);
      setGpuStats("system", "", 0L, l, str, i);
    } 
    Trace.traceEnd(2L);
  }
  
  public static boolean shouldUseAngle(Context paramContext, Bundle paramBundle, String paramString) {
    boolean bool;
    if (paramString.isEmpty()) {
      Log.v("GraphicsEnvironment", "No package name available yet, ANGLE should not be used");
      return false;
    } 
    String str = getDriverForPkg(paramContext, paramBundle, paramString);
    boolean bool1 = checkAngleWhitelist(paramContext, paramBundle, paramString);
    boolean bool2 = str.equals(sDriverMap.get(OpenGlDriverChoice.ANGLE));
    if (bool1 || bool2) {
      bool = true;
    } else {
      bool = false;
    } 
    if (!bool)
      return false; 
    if (bool1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ANGLE whitelist includes ");
      stringBuilder.append(paramString);
      Log.v("GraphicsEnvironment", stringBuilder.toString());
    } 
    if (bool2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ANGLE developer option for ");
      stringBuilder.append(paramString);
      stringBuilder.append(": ");
      stringBuilder.append(str);
      Log.v("GraphicsEnvironment", stringBuilder.toString());
    } 
    return true;
  }
  
  private static int getVulkanVersion(PackageManager paramPackageManager) {
    if (paramPackageManager.hasSystemFeature("android.hardware.vulkan.version", 4198400))
      return 4198400; 
    if (paramPackageManager.hasSystemFeature("android.hardware.vulkan.version", 4194304))
      return 4194304; 
    return 0;
  }
  
  private static boolean canInjectLayers(ApplicationInfo paramApplicationInfo) {
    boolean bool;
    if (paramApplicationInfo.metaData != null && paramApplicationInfo.metaData.getBoolean("com.android.graphics.injectLayers.enable") && 
      setInjectLayersPrSetDumpable()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setLayerPaths(ClassLoader paramClassLoader, String paramString1, String paramString2) {
    this.mClassLoader = paramClassLoader;
    this.mLibrarySearchPaths = paramString1;
    this.mLibraryPermittedPaths = paramString2;
  }
  
  public String getDebugLayerPathsFromSettings(Bundle paramBundle, IPackageManager paramIPackageManager, String paramString, ApplicationInfo paramApplicationInfo) {
    if (!debugLayerEnabled(paramBundle, paramString, paramApplicationInfo))
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("GPU debug layers enabled for ");
    stringBuilder.append(paramString);
    Log.i("GraphicsEnvironment", stringBuilder.toString());
    paramString = "";
    String str1 = paramBundle.getString("gpu_debug_layer_app", "");
    String str2 = paramString;
    if (!str1.isEmpty()) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("GPU debug layer apps: ");
      stringBuilder1.append(str1);
      Log.i("GraphicsEnvironment", stringBuilder1.toString());
      String[] arrayOfString = str1.split(":");
      byte b = 0;
      str1 = paramString;
      while (true) {
        str2 = str1;
        if (b < arrayOfString.length) {
          String str;
          str2 = getDebugLayerAppPaths(paramIPackageManager, arrayOfString[b]);
          paramString = str1;
          if (!str2.isEmpty()) {
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append(str1);
            stringBuilder2.append(str2);
            stringBuilder2.append(File.pathSeparator);
            str = stringBuilder2.toString();
          } 
          b++;
          str1 = str;
          continue;
        } 
        break;
      } 
    } 
    return str2;
  }
  
  private static String getDebugLayerAppPaths(IPackageManager paramIPackageManager, String paramString) {
    try {
      int i = UserHandle.myUserId();
      ApplicationInfo applicationInfo = paramIPackageManager.getApplicationInfo(paramString, 131072, i);
      if (applicationInfo == null) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Debug layer app '");
        stringBuilder1.append(paramString);
        stringBuilder1.append("' not installed");
        Log.w("GraphicsEnvironment", stringBuilder1.toString());
      } 
      String str2 = chooseAbi(applicationInfo);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(applicationInfo.nativeLibraryDir);
      String str3 = File.pathSeparator;
      stringBuilder.append(str3);
      String str1 = applicationInfo.sourceDir;
      stringBuilder.append(str1);
      stringBuilder.append("!/lib/");
      stringBuilder.append(str2);
      str1 = stringBuilder.toString();
      return str1;
    } catch (RemoteException remoteException) {
      return "";
    } 
  }
  
  private boolean debugLayerEnabled(Bundle paramBundle, String paramString, ApplicationInfo paramApplicationInfo) {
    if (!isDebuggable() && !canInjectLayers(paramApplicationInfo))
      return false; 
    int i = paramBundle.getInt("enable_gpu_debug_layers", 0);
    if (i == 0)
      return false; 
    String str = paramBundle.getString("gpu_debug_app", "");
    if (paramString == null || 
      str.isEmpty() || paramString.isEmpty() || 
      !str.equals(paramString))
      return false; 
    return true;
  }
  
  private void setupGpuLayers(Context paramContext, Bundle paramBundle, PackageManager paramPackageManager, String paramString, ApplicationInfo paramApplicationInfo) {
    boolean bool = debugLayerEnabled(paramBundle, paramString, paramApplicationInfo);
    String str = "";
    if (bool) {
      String str2 = this.mLibraryPermittedPaths;
      str = paramBundle.getString("gpu_debug_layers");
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Vulkan debug layer list: ");
      stringBuilder2.append(str);
      Log.i("GraphicsEnvironment", stringBuilder2.toString());
      if (str != null && !str.isEmpty())
        setDebugLayers(str); 
      String str1 = paramBundle.getString("gpu_debug_layers_gles");
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("GLES debug layer list: ");
      stringBuilder1.append(str1);
      Log.i("GraphicsEnvironment", stringBuilder1.toString());
      str = str2;
      if (str1 != null) {
        str = str2;
        if (!str1.isEmpty()) {
          setDebugLayersGLES(str1);
          str = str2;
        } 
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str);
    stringBuilder.append(this.mLibrarySearchPaths);
    str = stringBuilder.toString();
    setLayerPaths(this.mClassLoader, str);
  }
  
  enum OpenGlDriverChoice {
    ANGLE, DEFAULT, NATIVE;
    
    private static final OpenGlDriverChoice[] $VALUES;
    
    static {
      OpenGlDriverChoice openGlDriverChoice = new OpenGlDriverChoice("ANGLE", 2);
      $VALUES = new OpenGlDriverChoice[] { DEFAULT, NATIVE, openGlDriverChoice };
    }
  }
  
  static {
    sDriverMap = buildMap();
  }
  
  private static Map<OpenGlDriverChoice, String> buildMap() {
    HashMap<Object, Object> hashMap = new HashMap<>();
    hashMap.put(OpenGlDriverChoice.DEFAULT, "default");
    hashMap.put(OpenGlDriverChoice.ANGLE, "angle");
    hashMap.put(OpenGlDriverChoice.NATIVE, "native");
    return (Map)hashMap;
  }
  
  private static List<String> getGlobalSettingsString(ContentResolver paramContentResolver, Bundle paramBundle, String paramString) {
    String str;
    ArrayList<String> arrayList;
    if (paramBundle != null) {
      str = paramBundle.getString(paramString);
    } else {
      str = Settings.Global.getString((ContentResolver)str, paramString);
    } 
    if (str != null) {
      arrayList = new ArrayList(Arrays.asList((Object[])str.split(",")));
    } else {
      arrayList = new ArrayList();
    } 
    return arrayList;
  }
  
  private static int getGlobalSettingsPkgIndex(String paramString, List<String> paramList) {
    for (byte b = 0; b < paramList.size(); b++) {
      if (((String)paramList.get(b)).equals(paramString))
        return b; 
    } 
    return -1;
  }
  
  private static ApplicationInfo getAppInfoWithMetadata(Context paramContext, PackageManager paramPackageManager, String paramString) {
    ApplicationInfo applicationInfo;
    try {
      ApplicationInfo applicationInfo1 = paramPackageManager.getApplicationInfo(paramString, 128);
      applicationInfo = applicationInfo1;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      applicationInfo = applicationInfo.getApplicationInfo();
    } 
    return applicationInfo;
  }
  
  private static String getDriverForPkg(Context paramContext, Bundle paramBundle, String paramString) {
    String str1;
    StringBuilder stringBuilder;
    String str2;
    if (paramBundle != null) {
      str2 = paramBundle.getString("angle_gl_driver_all_angle");
    } else {
      ContentResolver contentResolver1 = paramContext.getContentResolver();
      str2 = Settings.Global.getString(contentResolver1, "angle_gl_driver_all_angle");
    } 
    if (str2 != null && str2.equals("1"))
      return sDriverMap.get(OpenGlDriverChoice.ANGLE); 
    ContentResolver contentResolver = paramContext.getContentResolver();
    List<String> list1 = getGlobalSettingsString(contentResolver, paramBundle, "angle_gl_driver_selection_pkgs");
    List<String> list2 = getGlobalSettingsString(contentResolver, paramBundle, "angle_gl_driver_selection_values");
    if (paramString == null || paramString.isEmpty())
      return sDriverMap.get(OpenGlDriverChoice.DEFAULT); 
    if (list1.size() != list2.size()) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Global.Settings values are invalid: globalSettingsDriverPkgs.size = ");
      stringBuilder.append(list1.size());
      stringBuilder.append(", globalSettingsDriverValues.size = ");
      stringBuilder.append(list2.size());
      str1 = stringBuilder.toString();
      Log.w("GraphicsEnvironment", str1);
      return sDriverMap.get(OpenGlDriverChoice.DEFAULT);
    } 
    int i = getGlobalSettingsPkgIndex((String)stringBuilder, (List<String>)str1);
    if (i < 0)
      return sDriverMap.get(OpenGlDriverChoice.DEFAULT); 
    return list2.get(i);
  }
  
  private String getAnglePackageName(PackageManager paramPackageManager) {
    Intent intent = new Intent("android.app.action.ANGLE_FOR_ANDROID");
    List list = paramPackageManager.queryIntentActivities(intent, 1048576);
    if (list.size() != 1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid number of ANGLE packages. Required: 1, Found: ");
      stringBuilder.append(list.size());
      String str = stringBuilder.toString();
      Log.e("GraphicsEnvironment", str);
      for (ResolveInfo resolveInfo : list) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Found ANGLE package: ");
        stringBuilder1.append(resolveInfo.activityInfo.packageName);
        Log.e("GraphicsEnvironment", stringBuilder1.toString());
      } 
      return "";
    } 
    return ((ResolveInfo)list.get(0)).activityInfo.packageName;
  }
  
  private String getAngleDebugPackage(Context paramContext, Bundle paramBundle) {
    if (isDebuggable()) {
      String str;
      if (paramBundle != null) {
        str = paramBundle.getString("angle_debug_package");
      } else {
        ContentResolver contentResolver = str.getContentResolver();
        str = Settings.Global.getString(contentResolver, "angle_debug_package");
      } 
      if (str != null && !str.isEmpty())
        return str; 
    } 
    return "";
  }
  
  private static boolean setupAngleWithTempRulesFile(Context paramContext, String paramString1, String paramString2, String paramString3) {
    if (!isDebuggable()) {
      Log.v("GraphicsEnvironment", "Skipping loading temporary rules file");
      return false;
    } 
    String str = SystemProperties.get("debug.angle.rules");
    if (str == null || str.isEmpty()) {
      Log.v("GraphicsEnvironment", "System property 'debug.angle.rules' is not set or is empty");
      return false;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Detected system property debug.angle.rules: ");
    stringBuilder.append(str);
    Log.i("GraphicsEnvironment", stringBuilder.toString());
    File file = new File(str);
    if (file.exists()) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str);
      stringBuilder1.append(" exists, loading file.");
      Log.i("GraphicsEnvironment", stringBuilder1.toString());
      try {
        FileInputStream fileInputStream = new FileInputStream();
        this(str);
        try {
          FileDescriptor fileDescriptor = fileInputStream.getFD();
          long l = fileInputStream.getChannel().size();
          StringBuilder stringBuilder2 = new StringBuilder();
          this();
          stringBuilder2.append("Loaded temporary ANGLE rules from ");
          stringBuilder2.append(str);
          Log.i("GraphicsEnvironment", stringBuilder2.toString());
          setAngleInfo(paramString2, paramString1, paramString3, fileDescriptor, 0L, l);
          fileInputStream.close();
          return true;
        } catch (IOException iOException) {
          StringBuilder stringBuilder2 = new StringBuilder();
          this();
          stringBuilder2.append("Hit IOException thrown by FileInputStream: ");
          stringBuilder2.append(iOException);
          Log.w("GraphicsEnvironment", stringBuilder2.toString());
        } 
      } catch (FileNotFoundException fileNotFoundException) {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Temp ANGLE rules file not found: ");
        stringBuilder2.append(fileNotFoundException);
        Log.w("GraphicsEnvironment", stringBuilder2.toString());
      } catch (SecurityException securityException) {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Temp ANGLE rules file not accessible: ");
        stringBuilder2.append(securityException);
        Log.w("GraphicsEnvironment", stringBuilder2.toString());
      } 
    } 
    return false;
  }
  
  private static boolean setupAngleRulesApk(String paramString1, ApplicationInfo paramApplicationInfo, PackageManager paramPackageManager, String paramString2, String paramString3, String paramString4) {
    try {
      AssetManager assetManager = paramPackageManager.getResourcesForApplication(paramApplicationInfo).getAssets();
      try {
        AssetFileDescriptor assetFileDescriptor = assetManager.openFd("a4a_rules.json");
        FileDescriptor fileDescriptor = assetFileDescriptor.getFileDescriptor();
        long l1 = assetFileDescriptor.getStartOffset(), l2 = assetFileDescriptor.getLength();
        setAngleInfo(paramString3, paramString2, paramString4, fileDescriptor, l1, l2);
        assetFileDescriptor.close();
        return true;
      } catch (IOException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to get AssetFileDescriptor for a4a_rules.json from '");
        stringBuilder.append(paramString1);
        stringBuilder.append("': ");
        stringBuilder.append(iOException);
        Log.w("GraphicsEnvironment", stringBuilder.toString());
      } 
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to get AssetManager for '");
      stringBuilder.append(paramString1);
      stringBuilder.append("': ");
      stringBuilder.append(nameNotFoundException);
      Log.w("GraphicsEnvironment", stringBuilder.toString());
    } 
    return false;
  }
  
  private static boolean checkAngleWhitelist(Context paramContext, Bundle paramBundle, String paramString) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    List<String> list = getGlobalSettingsString(contentResolver, paramBundle, "angle_whitelist");
    return list.contains(paramString);
  }
  
  public boolean setupAngle(Context paramContext, Bundle paramBundle, PackageManager paramPackageManager, String paramString) {
    StringBuilder stringBuilder1;
    ApplicationInfo applicationInfo;
    if (!shouldUseAngle(paramContext, paramBundle, paramString))
      return false; 
    StringBuilder stringBuilder2 = null;
    String str2 = getAngleDebugPackage(paramContext, paramBundle);
    if (!str2.isEmpty()) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("ANGLE debug package enabled: ");
      stringBuilder2.append(str2);
      Log.i("GraphicsEnvironment", stringBuilder2.toString());
      try {
        applicationInfo = paramPackageManager.getApplicationInfo(str2, 0);
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("ANGLE debug package '");
        stringBuilder1.append(str2);
        stringBuilder1.append("' not installed");
        Log.w("GraphicsEnvironment", stringBuilder1.toString());
        return false;
      } 
    } 
    if (applicationInfo == null) {
      str2 = getAnglePackageName(paramPackageManager);
      if (!str2.isEmpty()) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ANGLE package enabled: ");
        stringBuilder.append(str2);
        Log.i("GraphicsEnvironment", stringBuilder.toString());
        try {
          applicationInfo = paramPackageManager.getApplicationInfo(str2, 1048576);
        } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("ANGLE package '");
          stringBuilder1.append(str2);
          stringBuilder1.append("' not installed");
          Log.w("GraphicsEnvironment", stringBuilder1.toString());
          return false;
        } 
      } else {
        Log.e("GraphicsEnvironment", "Failed to find ANGLE package.");
        return false;
      } 
    } 
    String str3 = chooseAbi(applicationInfo);
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(applicationInfo.nativeLibraryDir);
    stringBuilder3.append(File.pathSeparator);
    stringBuilder3.append(applicationInfo.sourceDir);
    stringBuilder3.append("!/lib/");
    stringBuilder3.append(str3);
    String str4 = stringBuilder3.toString();
    String str1 = getDriverForPkg((Context)stringBuilder1, paramBundle, paramString);
    if (setupAngleWithTempRulesFile((Context)stringBuilder1, paramString, str4, str1))
      return true; 
    if (setupAngleRulesApk(str2, applicationInfo, paramPackageManager, paramString, str4, str1))
      return true; 
    return false;
  }
  
  private boolean shouldShowAngleInUseDialogBox(Context paramContext) {
    boolean bool = false;
    try {
      ContentResolver contentResolver = paramContext.getContentResolver();
      int i = Settings.Global.getInt(contentResolver, "show_angle_in_use_dialog_box");
      if (i == 1)
        bool = true; 
      return bool;
    } catch (android.provider.Settings.SettingNotFoundException|SecurityException settingNotFoundException) {
      return false;
    } 
  }
  
  private boolean setupAndUseAngle(Context paramContext, String paramString) {
    if (!setupAngle(paramContext, null, paramContext.getPackageManager(), paramString)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Package '");
      stringBuilder1.append(paramString);
      stringBuilder1.append("' should not use ANGLE");
      Log.v("GraphicsEnvironment", stringBuilder1.toString());
      return false;
    } 
    boolean bool = getShouldUseAngle(paramString);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Package '");
    stringBuilder.append(paramString);
    stringBuilder.append("' should use ANGLE = '");
    stringBuilder.append(bool);
    stringBuilder.append("'");
    Log.v("GraphicsEnvironment", stringBuilder.toString());
    return bool;
  }
  
  public void showAngleInUseDialogBox(Context paramContext) {
    String str = paramContext.getPackageName();
    if (shouldShowAngleInUseDialogBox(paramContext) && setupAndUseAngle(paramContext, str)) {
      Intent intent = new Intent("android.app.action.ANGLE_FOR_ANDROID_TOAST_MESSAGE");
      str = getAnglePackageName(paramContext.getPackageManager());
      intent.setPackage(str);
      paramContext.sendOrderedBroadcast(intent, null, (BroadcastReceiver)new Object(this), null, -1, null, null);
    } 
  }
  
  private static String chooseDriverInternal(Bundle paramBundle, ApplicationInfo paramApplicationInfo) {
    // Byte code:
    //   0: ldc 'ro.gfx.driver.0'
    //   2: invokestatic get : (Ljava/lang/String;)Ljava/lang/String;
    //   5: astore_2
    //   6: aload_2
    //   7: ifnull -> 22
    //   10: aload_2
    //   11: invokevirtual isEmpty : ()Z
    //   14: ifne -> 22
    //   17: iconst_1
    //   18: istore_3
    //   19: goto -> 24
    //   22: iconst_0
    //   23: istore_3
    //   24: ldc 'ro.gfx.driver.1'
    //   26: invokestatic get : (Ljava/lang/String;)Ljava/lang/String;
    //   29: astore #4
    //   31: aload #4
    //   33: ifnull -> 50
    //   36: aload #4
    //   38: invokevirtual isEmpty : ()Z
    //   41: ifne -> 50
    //   44: iconst_1
    //   45: istore #5
    //   47: goto -> 53
    //   50: iconst_0
    //   51: istore #5
    //   53: aconst_null
    //   54: astore #6
    //   56: aconst_null
    //   57: astore #7
    //   59: aconst_null
    //   60: astore #8
    //   62: iload_3
    //   63: ifne -> 73
    //   66: iload #5
    //   68: ifne -> 73
    //   71: aconst_null
    //   72: areturn
    //   73: aload_1
    //   74: invokevirtual isPrivilegedApp : ()Z
    //   77: ifne -> 361
    //   80: aload_1
    //   81: invokevirtual isSystemApp : ()Z
    //   84: ifeq -> 97
    //   87: aload_1
    //   88: invokevirtual isUpdatedSystemApp : ()Z
    //   91: ifne -> 97
    //   94: goto -> 361
    //   97: aload_1
    //   98: getfield metaData : Landroid/os/Bundle;
    //   101: ifnull -> 120
    //   104: aload_1
    //   105: getfield metaData : Landroid/os/Bundle;
    //   108: astore #9
    //   110: aload #9
    //   112: ldc 'com.android.graphics.developerdriver.enable'
    //   114: invokevirtual getBoolean : (Ljava/lang/String;)Z
    //   117: ifne -> 126
    //   120: invokestatic isDebuggable : ()Z
    //   123: ifeq -> 132
    //   126: iconst_1
    //   127: istore #10
    //   129: goto -> 135
    //   132: iconst_0
    //   133: istore #10
    //   135: aload_0
    //   136: ldc_w 'game_driver_all_apps'
    //   139: iconst_0
    //   140: invokevirtual getInt : (Ljava/lang/String;I)I
    //   143: istore #11
    //   145: iload #11
    //   147: iconst_1
    //   148: if_icmpeq -> 350
    //   151: iload #11
    //   153: iconst_2
    //   154: if_icmpeq -> 329
    //   157: iload #11
    //   159: iconst_3
    //   160: if_icmpeq -> 327
    //   163: aload_1
    //   164: getfield packageName : Ljava/lang/String;
    //   167: astore_1
    //   168: aconst_null
    //   169: aload_0
    //   170: ldc_w 'game_driver_opt_out_apps'
    //   173: invokestatic getGlobalSettingsString : (Landroid/content/ContentResolver;Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;
    //   176: astore #6
    //   178: aload #6
    //   180: aload_1
    //   181: invokeinterface contains : (Ljava/lang/Object;)Z
    //   186: ifeq -> 191
    //   189: aconst_null
    //   190: areturn
    //   191: aconst_null
    //   192: aload_0
    //   193: ldc_w 'game_driver_prerelease_opt_in_apps'
    //   196: invokestatic getGlobalSettingsString : (Landroid/content/ContentResolver;Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;
    //   199: astore #6
    //   201: aload #6
    //   203: aload_1
    //   204: invokeinterface contains : (Ljava/lang/Object;)Z
    //   209: ifeq -> 233
    //   212: aload #8
    //   214: astore_0
    //   215: iload #5
    //   217: ifeq -> 231
    //   220: aload #8
    //   222: astore_0
    //   223: iload #10
    //   225: ifeq -> 231
    //   228: aload #4
    //   230: astore_0
    //   231: aload_0
    //   232: areturn
    //   233: iload_3
    //   234: ifne -> 239
    //   237: aconst_null
    //   238: areturn
    //   239: aconst_null
    //   240: aload_0
    //   241: ldc_w 'game_driver_opt_in_apps'
    //   244: invokestatic getGlobalSettingsString : (Landroid/content/ContentResolver;Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;
    //   247: astore #4
    //   249: aload #4
    //   251: aload_1
    //   252: invokeinterface contains : (Ljava/lang/Object;)Z
    //   257: istore #12
    //   259: aconst_null
    //   260: aload_0
    //   261: ldc_w 'game_driver_whitelist'
    //   264: invokestatic getGlobalSettingsString : (Landroid/content/ContentResolver;Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;
    //   267: astore #4
    //   269: iload #12
    //   271: ifne -> 299
    //   274: aload #4
    //   276: ldc '*'
    //   278: invokeinterface indexOf : (Ljava/lang/Object;)I
    //   283: ifeq -> 299
    //   286: aload #4
    //   288: aload_1
    //   289: invokeinterface contains : (Ljava/lang/Object;)Z
    //   294: ifne -> 299
    //   297: aconst_null
    //   298: areturn
    //   299: iload #12
    //   301: ifne -> 325
    //   304: aconst_null
    //   305: aload_0
    //   306: ldc_w 'game_driver_blacklist'
    //   309: invokestatic getGlobalSettingsString : (Landroid/content/ContentResolver;Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;
    //   312: astore_0
    //   313: aload_0
    //   314: aload_1
    //   315: invokeinterface contains : (Ljava/lang/Object;)Z
    //   320: ifeq -> 325
    //   323: aconst_null
    //   324: areturn
    //   325: aload_2
    //   326: areturn
    //   327: aconst_null
    //   328: areturn
    //   329: aload #6
    //   331: astore_0
    //   332: iload #5
    //   334: ifeq -> 348
    //   337: aload #6
    //   339: astore_0
    //   340: iload #10
    //   342: ifeq -> 348
    //   345: aload #4
    //   347: astore_0
    //   348: aload_0
    //   349: areturn
    //   350: aload #7
    //   352: astore_0
    //   353: iload_3
    //   354: ifeq -> 359
    //   357: aload_2
    //   358: astore_0
    //   359: aload_0
    //   360: areturn
    //   361: aconst_null
    //   362: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #725	-> 0
    //   #726	-> 6
    //   #728	-> 24
    //   #729	-> 31
    //   #731	-> 53
    //   #733	-> 71
    //   #739	-> 73
    //   #744	-> 97
    //   #745	-> 110
    //   #746	-> 120
    //   #755	-> 135
    //   #770	-> 163
    //   #771	-> 168
    //   #772	-> 178
    //   #774	-> 189
    //   #777	-> 191
    //   #779	-> 201
    //   #781	-> 212
    //   #785	-> 233
    //   #787	-> 237
    //   #790	-> 239
    //   #791	-> 239
    //   #792	-> 249
    //   #793	-> 259
    //   #794	-> 259
    //   #795	-> 269
    //   #796	-> 286
    //   #798	-> 297
    //   #803	-> 299
    //   #804	-> 304
    //   #806	-> 313
    //   #808	-> 323
    //   #811	-> 325
    //   #758	-> 327
    //   #764	-> 329
    //   #761	-> 350
    //   #741	-> 361
  }
  
  private static boolean chooseDriver(Context paramContext, Bundle paramBundle, PackageManager paramPackageManager, String paramString, ApplicationInfo paramApplicationInfo) {
    String str = chooseDriverInternal(paramBundle, paramApplicationInfo);
    if (str == null)
      return false; 
    try {
      PackageInfo packageInfo = paramPackageManager.getPackageInfo(str, 1048704);
      ApplicationInfo applicationInfo = packageInfo.applicationInfo;
      if (applicationInfo.targetSdkVersion < 26)
        return false; 
      String str2 = chooseAbi(applicationInfo);
      if (str2 == null)
        return false; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(applicationInfo.nativeLibraryDir);
      String str3 = File.pathSeparator;
      stringBuilder.append(str3);
      stringBuilder.append(applicationInfo.sourceDir);
      stringBuilder.append("!/lib/");
      stringBuilder.append(str2);
      str3 = stringBuilder.toString();
      String str1 = getSphalLibraries(paramContext, str);
      setDriverPathAndSphalLibraries(str3, str1);
      if (applicationInfo.metaData != null) {
        str1 = applicationInfo.metaData.getString("com.android.gamedriver.build_time");
        if (str1 != null && !str1.isEmpty()) {
          String str4 = packageInfo.versionName;
          long l1 = applicationInfo.longVersionCode;
          long l2 = Long.parseLong(str1.substring(1));
          setGpuStats(str, str4, l1, l2, paramString, 0);
          return true;
        } 
        throw new IllegalArgumentException("com.android.gamedriver.build_time is not set");
      } 
      throw new NullPointerException("apk's meta-data cannot be null");
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("driver package '");
      stringBuilder.append(str);
      stringBuilder.append("' not installed");
      Log.w("GraphicsEnvironment", stringBuilder.toString());
      return false;
    } 
  }
  
  private static String chooseAbi(ApplicationInfo paramApplicationInfo) {
    String str = VMRuntime.getCurrentInstructionSet();
    if (paramApplicationInfo.primaryCpuAbi != null) {
      String str1 = paramApplicationInfo.primaryCpuAbi;
      if (str.equals(VMRuntime.getInstructionSet(str1)))
        return paramApplicationInfo.primaryCpuAbi; 
    } 
    if (paramApplicationInfo.secondaryCpuAbi != null) {
      String str1 = paramApplicationInfo.secondaryCpuAbi;
      if (str.equals(VMRuntime.getInstructionSet(str1)))
        return paramApplicationInfo.secondaryCpuAbi; 
    } 
    return null;
  }
  
  private static String getSphalLibraries(Context paramContext, String paramString) {
    try {
      Context context = paramContext.createPackageContext(paramString, 4);
      BufferedReader bufferedReader = new BufferedReader();
      InputStreamReader inputStreamReader = new InputStreamReader();
      this(context.getAssets().open("sphal_libraries.txt"));
      this(inputStreamReader);
      ArrayList<String> arrayList = new ArrayList();
      this();
      while (true) {
        String str = bufferedReader.readLine();
        if (str != null) {
          arrayList.add(str);
          continue;
        } 
        break;
      } 
      return String.join(":", (Iterable)arrayList);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
    
    } catch (IOException iOException) {}
    return "";
  }
  
  private static native boolean getShouldUseAngle(String paramString);
  
  public static native void hintActivityLaunch();
  
  private static native boolean isDebuggable();
  
  private static native void setAngleInfo(String paramString1, String paramString2, String paramString3, FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2);
  
  private static native void setDebugLayers(String paramString);
  
  private static native void setDebugLayersGLES(String paramString);
  
  private static native void setDriverPathAndSphalLibraries(String paramString1, String paramString2);
  
  private static native void setGpuStats(String paramString1, String paramString2, long paramLong1, long paramLong2, String paramString3, int paramInt);
  
  private static native boolean setInjectLayersPrSetDumpable();
  
  private static native void setLayerPaths(ClassLoader paramClassLoader, String paramString);
}
