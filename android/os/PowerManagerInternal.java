package android.os;

import java.util.function.Consumer;

public abstract class PowerManagerInternal implements IOplusPowerManagerInternalEx {
  public static final int BOOST_DISPLAY_UPDATE_IMMINENT = 1;
  
  public static final int BOOST_INTERACTION = 0;
  
  public static final int MODE_DEVICE_IDLE = 8;
  
  public static final int MODE_DISPLAY_INACTIVE = 9;
  
  public static final int MODE_DOUBLE_TAP_TO_WAKE = 0;
  
  public static final int MODE_EXPENSIVE_RENDERING = 6;
  
  public static final int MODE_FIXED_PERFORMANCE = 3;
  
  public static final int MODE_INTERACTIVE = 7;
  
  public static final int MODE_LAUNCH = 5;
  
  public static final int MODE_LOW_POWER = 1;
  
  public static final int MODE_SUSTAINED_PERFORMANCE = 2;
  
  public static final int MODE_VR = 4;
  
  public static final int WAKEFULNESS_ASLEEP = 0;
  
  public static final int WAKEFULNESS_AWAKE = 1;
  
  public static final int WAKEFULNESS_DOZING = 3;
  
  public static final int WAKEFULNESS_DREAMING = 2;
  
  public static String wakefulnessToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3)
            return Integer.toString(paramInt); 
          return "Dozing";
        } 
        return "Dreaming";
      } 
      return "Awake";
    } 
    return "Asleep";
  }
  
  public static int wakefulnessToProtoEnum(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3)
            return paramInt; 
          return 3;
        } 
        return 2;
      } 
      return 1;
    } 
    return 0;
  }
  
  public static boolean isInteractive(int paramInt) {
    boolean bool1 = true, bool2 = bool1;
    if (paramInt != 1)
      if (paramInt == 2) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    return bool2;
  }
  
  public void registerLowPowerModeObserver(final int serviceType, final Consumer<PowerSaveState> listener) {
    registerLowPowerModeObserver(new LowPowerModeListener() {
          final PowerManagerInternal this$0;
          
          final Consumer val$listener;
          
          final int val$serviceType;
          
          public int getServiceType() {
            return serviceType;
          }
          
          public void onLowPowerModeChanged(PowerSaveState param1PowerSaveState) {
            listener.accept(param1PowerSaveState);
          }
        });
  }
  
  public abstract void finishUidChanges();
  
  public abstract PowerManager.WakeData getLastWakeup();
  
  public abstract PowerSaveState getLowPowerState(int paramInt);
  
  public abstract void gotoSleepWhenScreenOnBlocked(String paramString);
  
  public abstract boolean isBiometricsWakeUpReason(String paramString);
  
  public abstract boolean isBlockedByFace();
  
  public abstract boolean isBlockedByFingerprint();
  
  public abstract boolean isFaceWakeUpReason(String paramString);
  
  public abstract boolean isFingerprintWakeUpReason(String paramString);
  
  public abstract boolean isStartGoToSleep();
  
  public abstract void notifyMotionGameAppForeground(String paramString, boolean paramBoolean);
  
  public abstract void powerHint(int paramInt1, int paramInt2);
  
  public abstract void registerLowPowerModeObserver(LowPowerModeListener paramLowPowerModeListener);
  
  public abstract boolean setDeviceIdleMode(boolean paramBoolean);
  
  public abstract void setDeviceIdleTempWhitelist(int[] paramArrayOfint);
  
  public abstract void setDeviceIdleWhitelist(int[] paramArrayOfint);
  
  public abstract void setDozeOverrideFromDreamManager(int paramInt1, int paramInt2);
  
  public abstract void setDrawWakeLockOverrideFromSidekick(boolean paramBoolean);
  
  public abstract boolean setLightDeviceIdleMode(boolean paramBoolean);
  
  public abstract void setMaximumScreenOffTimeoutFromDeviceAdmin(int paramInt, long paramLong);
  
  public abstract void setPowerBoost(int paramInt1, int paramInt2);
  
  public abstract void setPowerMode(int paramInt, boolean paramBoolean);
  
  public abstract void setScreenBrightnessOverrideFromWindowManager(float paramFloat);
  
  public abstract void setUserActivityTimeoutOverrideFromWindowManager(long paramLong);
  
  public abstract void setUserInactiveOverrideFromWindowManager();
  
  public abstract void startUidChanges();
  
  public abstract void uidActive(int paramInt);
  
  public abstract void uidGone(int paramInt);
  
  public abstract void uidIdle(int paramInt);
  
  public abstract void unblockScreenOn(String paramString);
  
  public abstract void updateUidProcState(int paramInt1, int paramInt2);
  
  public abstract void wakeUpAndBlockScreenOn(String paramString);
  
  public abstract boolean wasDeviceIdleFor(long paramLong);
  
  class LowPowerModeListener {
    public abstract int getServiceType();
    
    public abstract void onLowPowerModeChanged(PowerSaveState param1PowerSaveState);
  }
}
