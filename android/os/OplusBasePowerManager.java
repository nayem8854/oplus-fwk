package android.os;

import android.util.Log;

public class OplusBasePowerManager {
  private static final int BRIGHTNESS_ELEVEN_BITS = 2047;
  
  public static int BRIGHTNESS_MULTIBITS_ON = 255;
  
  private static final int BRIGHTNESS_TEN_BITS = 1023;
  
  public static final int GO_TO_SLEEP_REASON_FINGERPRINT = 12;
  
  public static final int GO_TO_SLEEP_REASON_PROXIMITY = 11;
  
  private static final int MAX_STACK_LINE = 5;
  
  public static final int WAKE_REASON_DOUBLE_HOME = 99;
  
  public static final int WAKE_REASON_DOUBLE_TAP_SCREEN = 100;
  
  public static final int WAKE_REASON_LIFT_HAND = 101;
  
  public static final int WAKE_REASON_PROXIMITY = 97;
  
  public static final int WAKE_REASON_SYSTEM_UI_CLEAN_UP = 103;
  
  public static final int WAKE_REASON_WINDOWMANAGER_TURN_SCREENON = 102;
  
  public static final String WAKE_UP_DUE_TO_ACTIVITYMANAGER_TURN_SCREENON = "android.server.am:TURN_ON:turnScreenOnFlag";
  
  public static final String WAKE_UP_DUE_TO_DOUBLE_HOME = "android.service.fingerprint:DOUBLE_HOME";
  
  public static final String WAKE_UP_DUE_TO_DOUBLE_TAP_SCREEN = "oppo.wakeup.gesture:DOUBLE_TAP_SCREEN";
  
  public static final String WAKE_UP_DUE_TO_FINGERPRINT = "android.service.fingerprint:WAKEUP";
  
  public static final String WAKE_UP_DUE_TO_LIFT_HAND = "oppo.wakeup.gesture:LIFT_HAND";
  
  public static final String WAKE_UP_DUE_TO_PROXIMITY = "android.service.power:proximity";
  
  public static final String WAKE_UP_DUE_TO_SYSTEM_UI_CLEAN_UP = "oppo.wakeup.systemui:clean up";
  
  public static final String WAKE_UP_DUE_TO_WINDOWMANAGER_TURN_SCREENON = "android.server.wm:SCREEN_ON_FLAG";
  
  public static final int WAKE_UP_REASON_FINGERPRINT = 98;
  
  void printStackTraceInfo(String paramString) {
    StackTraceElement[] arrayOfStackTraceElement = (new Throwable()).getStackTrace();
    if (arrayOfStackTraceElement != null)
      for (byte b = 0; b < 5 && b < arrayOfStackTraceElement.length; b++) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramString);
        stringBuilder.append("    |----");
        stringBuilder.append(arrayOfStackTraceElement[b].toString());
        Log.i("PowerManager", stringBuilder.toString());
      }  
  }
}
