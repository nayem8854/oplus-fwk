package android.os;

public class OplusBaseLooper {
  protected static ThrowableObserver sThrowableObserver;
  
  public static ThrowableObserver getThrowableObserver() {
    return sThrowableObserver;
  }
  
  public static void setThrowableObserver(ThrowableObserver paramThrowableObserver) {
    sThrowableObserver = paramThrowableObserver;
  }
  
  public static interface ThrowableObserver {
    boolean shouldCatchThrowable(Throwable param1Throwable);
  }
}
