package android.os;

public class PowerSaveState implements Parcelable {
  public PowerSaveState(Builder paramBuilder) {
    this.batterySaverEnabled = paramBuilder.mBatterySaverEnabled;
    this.locationMode = paramBuilder.mLocationMode;
    this.brightnessFactor = paramBuilder.mBrightnessFactor;
    this.globalBatterySaverEnabled = paramBuilder.mGlobalBatterySaverEnabled;
  }
  
  public PowerSaveState(Parcel paramParcel) {
    boolean bool2;
    byte b = paramParcel.readByte();
    boolean bool1 = true;
    if (b != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.batterySaverEnabled = bool2;
    if (paramParcel.readByte() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.globalBatterySaverEnabled = bool2;
    this.locationMode = paramParcel.readInt();
    this.brightnessFactor = paramParcel.readFloat();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByte((byte)this.batterySaverEnabled);
    paramParcel.writeByte((byte)this.globalBatterySaverEnabled);
    paramParcel.writeInt(this.locationMode);
    paramParcel.writeFloat(this.brightnessFactor);
  }
  
  class Builder {
    private boolean mBatterySaverEnabled = false;
    
    private boolean mGlobalBatterySaverEnabled = false;
    
    private int mLocationMode = 0;
    
    private float mBrightnessFactor = 0.5F;
    
    public Builder setBatterySaverEnabled(boolean param1Boolean) {
      this.mBatterySaverEnabled = param1Boolean;
      return this;
    }
    
    public Builder setGlobalBatterySaverEnabled(boolean param1Boolean) {
      this.mGlobalBatterySaverEnabled = param1Boolean;
      return this;
    }
    
    public Builder setLocationMode(int param1Int) {
      this.mLocationMode = param1Int;
      return this;
    }
    
    public Builder setBrightnessFactor(float param1Float) {
      this.mBrightnessFactor = param1Float;
      return this;
    }
    
    public PowerSaveState build() {
      return new PowerSaveState(this);
    }
  }
  
  public static final Parcelable.Creator<PowerSaveState> CREATOR = new Parcelable.Creator<PowerSaveState>() {
      public PowerSaveState createFromParcel(Parcel param1Parcel) {
        return new PowerSaveState(param1Parcel);
      }
      
      public PowerSaveState[] newArray(int param1Int) {
        return new PowerSaveState[param1Int];
      }
    };
  
  public final boolean batterySaverEnabled;
  
  public final float brightnessFactor;
  
  public final boolean globalBatterySaverEnabled;
  
  public final int locationMode;
}
