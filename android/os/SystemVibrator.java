package android.os;

import android.content.Context;
import android.media.AudioAttributes;
import android.util.ArrayMap;
import android.util.Log;
import java.util.Objects;
import java.util.concurrent.Executor;

public class SystemVibrator extends OplusBaseSystemVibrator {
  private final Binder mToken = new Binder();
  
  private final IVibratorService mService;
  
  private final ArrayMap<Vibrator.OnVibratorStateChangedListener, OnVibratorStateChangedListenerDelegate> mDelegates = new ArrayMap();
  
  private final Context mContext;
  
  private static final String TAG = "Vibrator";
  
  public SystemVibrator() {
    this.mContext = null;
    this.mService = IVibratorService.Stub.asInterface(ServiceManager.getService("vibrator"));
  }
  
  public SystemVibrator(Context paramContext) {
    super(paramContext);
    this.mContext = paramContext;
    this.mService = IVibratorService.Stub.asInterface(ServiceManager.getService("vibrator"));
  }
  
  public boolean hasVibrator() {
    IVibratorService iVibratorService = this.mService;
    if (iVibratorService == null) {
      Log.w("Vibrator", "Failed to vibrate; no vibrator service.");
      return false;
    } 
    try {
      return iVibratorService.hasVibrator();
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean isVibrating() {
    IVibratorService iVibratorService = this.mService;
    if (iVibratorService == null) {
      Log.w("Vibrator", "Failed to vibrate; no vibrator service.");
      return false;
    } 
    try {
      return iVibratorService.isVibrating();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return false;
    } 
  }
  
  class OnVibratorStateChangedListenerDelegate extends IVibratorStateListener.Stub {
    private final Executor mExecutor;
    
    private final Vibrator.OnVibratorStateChangedListener mListener;
    
    final SystemVibrator this$0;
    
    OnVibratorStateChangedListenerDelegate(Vibrator.OnVibratorStateChangedListener param1OnVibratorStateChangedListener, Executor param1Executor) {
      this.mExecutor = param1Executor;
      this.mListener = param1OnVibratorStateChangedListener;
    }
    
    public void onVibrating(boolean param1Boolean) {
      this.mExecutor.execute(new _$$Lambda$SystemVibrator$OnVibratorStateChangedListenerDelegate$TagAqaiuke_42S8UkSIiiGTN_8Y(this, param1Boolean));
    }
  }
  
  public void addVibratorStateListener(Executor paramExecutor, Vibrator.OnVibratorStateChangedListener paramOnVibratorStateChangedListener) {
    Objects.requireNonNull(paramOnVibratorStateChangedListener);
    Objects.requireNonNull(paramExecutor);
    if (this.mService == null) {
      Log.w("Vibrator", "Failed to add vibrate state listener; no vibrator service.");
      return;
    } 
    synchronized (this.mDelegates) {
      if (this.mDelegates.containsKey(paramOnVibratorStateChangedListener)) {
        Log.w("Vibrator", "Listener already registered.");
        return;
      } 
      try {
        OnVibratorStateChangedListenerDelegate onVibratorStateChangedListenerDelegate = new OnVibratorStateChangedListenerDelegate();
        this(this, paramOnVibratorStateChangedListener, paramExecutor);
        if (!this.mService.registerVibratorStateListener(onVibratorStateChangedListenerDelegate)) {
          Log.w("Vibrator", "Failed to register vibrate state listener");
          return;
        } 
        this.mDelegates.put(paramOnVibratorStateChangedListener, onVibratorStateChangedListenerDelegate);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  public void addVibratorStateListener(Vibrator.OnVibratorStateChangedListener paramOnVibratorStateChangedListener) {
    Objects.requireNonNull(paramOnVibratorStateChangedListener);
    Context context = this.mContext;
    if (context == null) {
      Log.w("Vibrator", "Failed to add vibrate state listener; no vibrator context.");
      return;
    } 
    addVibratorStateListener(context.getMainExecutor(), paramOnVibratorStateChangedListener);
  }
  
  public void removeVibratorStateListener(Vibrator.OnVibratorStateChangedListener paramOnVibratorStateChangedListener) {
    Objects.requireNonNull(paramOnVibratorStateChangedListener);
    if (this.mService == null) {
      Log.w("Vibrator", "Failed to remove vibrate state listener; no vibrator service.");
      return;
    } 
    synchronized (this.mDelegates) {
      if (this.mDelegates.containsKey(paramOnVibratorStateChangedListener)) {
        OnVibratorStateChangedListenerDelegate onVibratorStateChangedListenerDelegate = (OnVibratorStateChangedListenerDelegate)this.mDelegates.get(paramOnVibratorStateChangedListener);
        try {
          if (!this.mService.unregisterVibratorStateListener(onVibratorStateChangedListenerDelegate)) {
            Log.w("Vibrator", "Failed to unregister vibrate state listener");
            return;
          } 
          this.mDelegates.remove(paramOnVibratorStateChangedListener);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } 
      } 
      return;
    } 
  }
  
  public boolean hasAmplitudeControl() {
    IVibratorService iVibratorService = this.mService;
    if (iVibratorService == null) {
      Log.w("Vibrator", "Failed to check amplitude control; no vibrator service.");
      return false;
    } 
    try {
      return iVibratorService.hasAmplitudeControl();
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean setAlwaysOnEffect(int paramInt1, String paramString, int paramInt2, VibrationEffect paramVibrationEffect, AudioAttributes paramAudioAttributes) {
    if (this.mService == null) {
      Log.w("Vibrator", "Failed to set always-on effect; no vibrator service.");
      return false;
    } 
    try {
      VibrationAttributes.Builder builder = new VibrationAttributes.Builder();
      this(paramAudioAttributes, paramVibrationEffect);
      VibrationAttributes vibrationAttributes = builder.build();
      return this.mService.setAlwaysOnEffect(paramInt1, paramString, paramInt2, paramVibrationEffect, vibrationAttributes);
    } catch (RemoteException remoteException) {
      Log.w("Vibrator", "Failed to set always-on effect.", (Throwable)remoteException);
      return false;
    } 
  }
  
  public void vibrate(int paramInt, String paramString1, VibrationEffect paramVibrationEffect, String paramString2, AudioAttributes paramAudioAttributes) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mService : Landroid/os/IVibratorService;
    //   4: ifnonnull -> 16
    //   7: ldc 'Vibrator'
    //   9: ldc 'Failed to vibrate; no vibrator service.'
    //   11: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   14: pop
    //   15: return
    //   16: aload_0
    //   17: iload_1
    //   18: aload_2
    //   19: aload_3
    //   20: invokevirtual doVibrate : (ILjava/lang/String;Landroid/os/VibrationEffect;)Z
    //   23: ifeq -> 27
    //   26: return
    //   27: aload #5
    //   29: astore #6
    //   31: aload #5
    //   33: ifnonnull -> 53
    //   36: new android/media/AudioAttributes$Builder
    //   39: astore #5
    //   41: aload #5
    //   43: invokespecial <init> : ()V
    //   46: aload #5
    //   48: invokevirtual build : ()Landroid/media/AudioAttributes;
    //   51: astore #6
    //   53: new android/os/VibrationAttributes$Builder
    //   56: astore #5
    //   58: aload #5
    //   60: aload #6
    //   62: aload_3
    //   63: invokespecial <init> : (Landroid/media/AudioAttributes;Landroid/os/VibrationEffect;)V
    //   66: aload #5
    //   68: invokevirtual build : ()Landroid/os/VibrationAttributes;
    //   71: astore #5
    //   73: aload_0
    //   74: getfield mService : Landroid/os/IVibratorService;
    //   77: iload_1
    //   78: aload_2
    //   79: aload_3
    //   80: aload #5
    //   82: aload #4
    //   84: aload_0
    //   85: getfield mToken : Landroid/os/Binder;
    //   88: invokeinterface vibrate : (ILjava/lang/String;Landroid/os/VibrationEffect;Landroid/os/VibrationAttributes;Ljava/lang/String;Landroid/os/IBinder;)V
    //   93: goto -> 106
    //   96: astore_2
    //   97: ldc 'Vibrator'
    //   99: ldc 'Failed to vibrate.'
    //   101: aload_2
    //   102: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   105: pop
    //   106: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #231	-> 0
    //   #232	-> 7
    //   #233	-> 15
    //   #237	-> 16
    //   #238	-> 26
    //   #242	-> 27
    //   #243	-> 36
    //   #245	-> 53
    //   #246	-> 73
    //   #249	-> 93
    //   #247	-> 96
    //   #248	-> 97
    //   #250	-> 106
    // Exception table:
    //   from	to	target	type
    //   36	53	96	android/os/RemoteException
    //   53	73	96	android/os/RemoteException
    //   73	93	96	android/os/RemoteException
  }
  
  public int[] areEffectsSupported(int... paramVarArgs) {
    try {
      return this.mService.areEffectsSupported(paramVarArgs);
    } catch (RemoteException remoteException) {
      Log.w("Vibrator", "Failed to query effect support");
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public boolean[] arePrimitivesSupported(int... paramVarArgs) {
    try {
      return this.mService.arePrimitivesSupported(paramVarArgs);
    } catch (RemoteException remoteException) {
      Log.w("Vibrator", "Failed to query effect support");
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void cancel() {
    IVibratorService iVibratorService = this.mService;
    if (iVibratorService == null)
      return; 
    try {
      iVibratorService.cancelVibrate(this.mToken);
    } catch (RemoteException remoteException) {
      Log.w("Vibrator", "Failed to cancel vibration.", (Throwable)remoteException);
    } 
  }
  
  protected void vibrate(int paramInt, String paramString1, VibrationEffect paramVibrationEffect, String paramString2, AudioAttributes paramAudioAttributes, IBinder paramIBinder) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mService : Landroid/os/IVibratorService;
    //   4: ifnonnull -> 16
    //   7: ldc 'Vibrator'
    //   9: ldc 'Failed to vibrate; no vibrator service.'
    //   11: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   14: pop
    //   15: return
    //   16: aload #5
    //   18: astore #7
    //   20: aload #5
    //   22: ifnonnull -> 42
    //   25: new android/media/AudioAttributes$Builder
    //   28: astore #5
    //   30: aload #5
    //   32: invokespecial <init> : ()V
    //   35: aload #5
    //   37: invokevirtual build : ()Landroid/media/AudioAttributes;
    //   40: astore #7
    //   42: new android/os/VibrationAttributes$Builder
    //   45: astore #5
    //   47: aload #5
    //   49: aload #7
    //   51: aload_3
    //   52: invokespecial <init> : (Landroid/media/AudioAttributes;Landroid/os/VibrationEffect;)V
    //   55: aload #5
    //   57: aload #7
    //   59: invokevirtual getUsage : ()I
    //   62: invokevirtual setUsage : (I)Landroid/os/VibrationAttributes$Builder;
    //   65: invokevirtual build : ()Landroid/os/VibrationAttributes;
    //   68: astore #5
    //   70: aload_0
    //   71: getfield mService : Landroid/os/IVibratorService;
    //   74: iload_1
    //   75: aload_2
    //   76: aload_3
    //   77: aload #5
    //   79: aload #4
    //   81: aload #6
    //   83: invokeinterface vibrate : (ILjava/lang/String;Landroid/os/VibrationEffect;Landroid/os/VibrationAttributes;Ljava/lang/String;Landroid/os/IBinder;)V
    //   88: goto -> 101
    //   91: astore_2
    //   92: ldc 'Vibrator'
    //   94: ldc 'Failed to vibrate.'
    //   96: aload_2
    //   97: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   100: pop
    //   101: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #291	-> 0
    //   #292	-> 7
    //   #293	-> 15
    //   #297	-> 16
    //   #298	-> 25
    //   #301	-> 42
    //   #302	-> 55
    //   #303	-> 70
    //   #306	-> 88
    //   #304	-> 91
    //   #305	-> 92
    //   #307	-> 101
    // Exception table:
    //   from	to	target	type
    //   25	42	91	android/os/RemoteException
    //   42	55	91	android/os/RemoteException
    //   55	70	91	android/os/RemoteException
    //   70	88	91	android/os/RemoteException
  }
}
