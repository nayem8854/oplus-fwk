package android.os;

import android.content.IntentSender;

public interface IRecoverySystem extends IInterface {
  boolean clearBcb() throws RemoteException;
  
  boolean clearLskf() throws RemoteException;
  
  void rebootRecoveryWithCommand(String paramString) throws RemoteException;
  
  boolean rebootWithLskf(String paramString1, String paramString2) throws RemoteException;
  
  boolean requestLskf(String paramString, IntentSender paramIntentSender) throws RemoteException;
  
  void setWipeProperty(String paramString) throws RemoteException;
  
  boolean setupBcb(String paramString) throws RemoteException;
  
  boolean uncrypt(String paramString, IRecoverySystemProgressListener paramIRecoverySystemProgressListener) throws RemoteException;
  
  class Default implements IRecoverySystem {
    public boolean uncrypt(String param1String, IRecoverySystemProgressListener param1IRecoverySystemProgressListener) throws RemoteException {
      return false;
    }
    
    public boolean setupBcb(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean clearBcb() throws RemoteException {
      return false;
    }
    
    public void rebootRecoveryWithCommand(String param1String) throws RemoteException {}
    
    public boolean requestLskf(String param1String, IntentSender param1IntentSender) throws RemoteException {
      return false;
    }
    
    public boolean clearLskf() throws RemoteException {
      return false;
    }
    
    public boolean rebootWithLskf(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public void setWipeProperty(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRecoverySystem {
    private static final String DESCRIPTOR = "android.os.IRecoverySystem";
    
    static final int TRANSACTION_clearBcb = 3;
    
    static final int TRANSACTION_clearLskf = 6;
    
    static final int TRANSACTION_rebootRecoveryWithCommand = 4;
    
    static final int TRANSACTION_rebootWithLskf = 7;
    
    static final int TRANSACTION_requestLskf = 5;
    
    static final int TRANSACTION_setWipeProperty = 8;
    
    static final int TRANSACTION_setupBcb = 2;
    
    static final int TRANSACTION_uncrypt = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IRecoverySystem");
    }
    
    public static IRecoverySystem asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IRecoverySystem");
      if (iInterface != null && iInterface instanceof IRecoverySystem)
        return (IRecoverySystem)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "setWipeProperty";
        case 7:
          return "rebootWithLskf";
        case 6:
          return "clearLskf";
        case 5:
          return "requestLskf";
        case 4:
          return "rebootRecoveryWithCommand";
        case 3:
          return "clearBcb";
        case 2:
          return "setupBcb";
        case 1:
          break;
      } 
      return "uncrypt";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.os.IRecoverySystem");
            str1 = param1Parcel1.readString();
            setWipeProperty(str1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str1.enforceInterface("android.os.IRecoverySystem");
            str2 = str1.readString();
            str1 = str1.readString();
            bool = rebootWithLskf(str2, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 6:
            str1.enforceInterface("android.os.IRecoverySystem");
            bool = clearLskf();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 5:
            str1.enforceInterface("android.os.IRecoverySystem");
            str2 = str1.readString();
            if (str1.readInt() != 0) {
              IntentSender intentSender = IntentSender.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            bool = requestLskf(str2, (IntentSender)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            str1.enforceInterface("android.os.IRecoverySystem");
            str1 = str1.readString();
            rebootRecoveryWithCommand(str1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("android.os.IRecoverySystem");
            bool = clearBcb();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 2:
            str1.enforceInterface("android.os.IRecoverySystem");
            str1 = str1.readString();
            bool = setupBcb(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.os.IRecoverySystem");
        String str2 = str1.readString();
        IRecoverySystemProgressListener iRecoverySystemProgressListener = IRecoverySystemProgressListener.Stub.asInterface(str1.readStrongBinder());
        boolean bool = uncrypt(str2, iRecoverySystemProgressListener);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool);
        return true;
      } 
      param1Parcel2.writeString("android.os.IRecoverySystem");
      return true;
    }
    
    private static class Proxy implements IRecoverySystem {
      public static IRecoverySystem sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IRecoverySystem";
      }
      
      public boolean uncrypt(String param2String, IRecoverySystemProgressListener param2IRecoverySystemProgressListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IRecoverySystem");
          parcel1.writeString(param2String);
          if (param2IRecoverySystemProgressListener != null) {
            iBinder = param2IRecoverySystemProgressListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IRecoverySystem.Stub.getDefaultImpl() != null) {
            bool1 = IRecoverySystem.Stub.getDefaultImpl().uncrypt(param2String, param2IRecoverySystemProgressListener);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setupBcb(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IRecoverySystem");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IRecoverySystem.Stub.getDefaultImpl() != null) {
            bool1 = IRecoverySystem.Stub.getDefaultImpl().setupBcb(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean clearBcb() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IRecoverySystem");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IRecoverySystem.Stub.getDefaultImpl() != null) {
            bool1 = IRecoverySystem.Stub.getDefaultImpl().clearBcb();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void rebootRecoveryWithCommand(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IRecoverySystem");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IRecoverySystem.Stub.getDefaultImpl() != null) {
            IRecoverySystem.Stub.getDefaultImpl().rebootRecoveryWithCommand(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestLskf(String param2String, IntentSender param2IntentSender) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IRecoverySystem");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2IntentSender != null) {
            parcel1.writeInt(1);
            param2IntentSender.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IRecoverySystem.Stub.getDefaultImpl() != null) {
            bool1 = IRecoverySystem.Stub.getDefaultImpl().requestLskf(param2String, param2IntentSender);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean clearLskf() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IRecoverySystem");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IRecoverySystem.Stub.getDefaultImpl() != null) {
            bool1 = IRecoverySystem.Stub.getDefaultImpl().clearLskf();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean rebootWithLskf(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IRecoverySystem");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IRecoverySystem.Stub.getDefaultImpl() != null) {
            bool1 = IRecoverySystem.Stub.getDefaultImpl().rebootWithLskf(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setWipeProperty(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.os.IRecoverySystem");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IRecoverySystem.Stub.getDefaultImpl() != null) {
            IRecoverySystem.Stub.getDefaultImpl().setWipeProperty(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRecoverySystem param1IRecoverySystem) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRecoverySystem != null) {
          Proxy.sDefaultImpl = param1IRecoverySystem;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRecoverySystem getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
