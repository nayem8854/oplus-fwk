package android.os;

public interface IClientCallback extends IInterface {
  void onClients(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  class Default implements IClientCallback {
    public void onClients(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IClientCallback {
    private static final String DESCRIPTOR = "android.os.IClientCallback";
    
    static final int TRANSACTION_onClients = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IClientCallback");
    }
    
    public static IClientCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IClientCallback");
      if (iInterface != null && iInterface instanceof IClientCallback)
        return (IClientCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onClients";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.IClientCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IClientCallback");
      IBinder iBinder = param1Parcel1.readStrongBinder();
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      onClients(iBinder, bool);
      return true;
    }
    
    private static class Proxy implements IClientCallback {
      public static IClientCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IClientCallback";
      }
      
      public void onClients(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.os.IClientCallback");
          parcel.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && IClientCallback.Stub.getDefaultImpl() != null) {
            IClientCallback.Stub.getDefaultImpl().onClients(param2IBinder, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IClientCallback param1IClientCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IClientCallback != null) {
          Proxy.sDefaultImpl = param1IClientCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IClientCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
