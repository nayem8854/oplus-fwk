package android.os;

import java.util.ArrayList;

public class RegistrantList {
  ArrayList registrants = new ArrayList();
  
  public void add(Handler paramHandler, int paramInt, Object paramObject) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new android/os/Registrant
    //   5: astore #4
    //   7: aload #4
    //   9: aload_1
    //   10: iload_2
    //   11: aload_3
    //   12: invokespecial <init> : (Landroid/os/Handler;ILjava/lang/Object;)V
    //   15: aload_0
    //   16: aload #4
    //   18: invokevirtual add : (Landroid/os/Registrant;)V
    //   21: aload_0
    //   22: monitorexit
    //   23: return
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #36	-> 2
    //   #37	-> 21
    //   #35	-> 24
    // Exception table:
    //   from	to	target	type
    //   2	21	24	finally
  }
  
  public void addUnique(Handler paramHandler, int paramInt, Object paramObject) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual remove : (Landroid/os/Handler;)V
    //   7: new android/os/Registrant
    //   10: astore #4
    //   12: aload #4
    //   14: aload_1
    //   15: iload_2
    //   16: aload_3
    //   17: invokespecial <init> : (Landroid/os/Handler;ILjava/lang/Object;)V
    //   20: aload_0
    //   21: aload #4
    //   23: invokevirtual add : (Landroid/os/Registrant;)V
    //   26: aload_0
    //   27: monitorexit
    //   28: return
    //   29: astore_1
    //   30: aload_0
    //   31: monitorexit
    //   32: aload_1
    //   33: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #44	-> 2
    //   #45	-> 7
    //   #46	-> 26
    //   #43	-> 29
    // Exception table:
    //   from	to	target	type
    //   2	7	29	finally
    //   7	26	29	finally
  }
  
  public void add(Registrant paramRegistrant) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual removeCleared : ()V
    //   6: aload_0
    //   7: getfield registrants : Ljava/util/ArrayList;
    //   10: aload_1
    //   11: invokevirtual add : (Ljava/lang/Object;)Z
    //   14: pop
    //   15: aload_0
    //   16: monitorexit
    //   17: return
    //   18: astore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_1
    //   22: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #52	-> 2
    //   #53	-> 6
    //   #54	-> 15
    //   #51	-> 18
    // Exception table:
    //   from	to	target	type
    //   2	6	18	finally
    //   6	15	18	finally
  }
  
  public void removeCleared() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield registrants : Ljava/util/ArrayList;
    //   6: invokevirtual size : ()I
    //   9: iconst_1
    //   10: isub
    //   11: istore_1
    //   12: iload_1
    //   13: iflt -> 50
    //   16: aload_0
    //   17: getfield registrants : Ljava/util/ArrayList;
    //   20: iload_1
    //   21: invokevirtual get : (I)Ljava/lang/Object;
    //   24: checkcast android/os/Registrant
    //   27: astore_2
    //   28: aload_2
    //   29: getfield refH : Ljava/lang/ref/WeakReference;
    //   32: ifnonnull -> 44
    //   35: aload_0
    //   36: getfield registrants : Ljava/util/ArrayList;
    //   39: iload_1
    //   40: invokevirtual remove : (I)Ljava/lang/Object;
    //   43: pop
    //   44: iinc #1, -1
    //   47: goto -> 12
    //   50: aload_0
    //   51: monitorexit
    //   52: return
    //   53: astore_2
    //   54: aload_0
    //   55: monitorexit
    //   56: aload_2
    //   57: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #60	-> 2
    //   #61	-> 16
    //   #63	-> 28
    //   #64	-> 35
    //   #60	-> 44
    //   #67	-> 50
    //   #59	-> 53
    // Exception table:
    //   from	to	target	type
    //   2	12	53	finally
    //   16	28	53	finally
    //   28	35	53	finally
    //   35	44	53	finally
  }
  
  public void removeAll() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield registrants : Ljava/util/ArrayList;
    //   6: invokevirtual clear : ()V
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: astore_1
    //   13: aload_0
    //   14: monitorexit
    //   15: aload_1
    //   16: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #70	-> 2
    //   #71	-> 9
    //   #69	-> 12
    // Exception table:
    //   from	to	target	type
    //   2	9	12	finally
  }
  
  public int size() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield registrants : Ljava/util/ArrayList;
    //   6: invokevirtual size : ()I
    //   9: istore_1
    //   10: aload_0
    //   11: monitorexit
    //   12: iload_1
    //   13: ireturn
    //   14: astore_2
    //   15: aload_0
    //   16: monitorexit
    //   17: aload_2
    //   18: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #77	-> 2
    //   #77	-> 14
    // Exception table:
    //   from	to	target	type
    //   2	10	14	finally
  }
  
  public Object get(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield registrants : Ljava/util/ArrayList;
    //   6: iload_1
    //   7: invokevirtual get : (I)Ljava/lang/Object;
    //   10: astore_2
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_2
    //   14: areturn
    //   15: astore_2
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_2
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #84	-> 2
    //   #84	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	11	15	finally
  }
  
  private void internalNotifyRegistrants(Object paramObject, Throwable paramThrowable) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iconst_0
    //   3: istore_3
    //   4: aload_0
    //   5: getfield registrants : Ljava/util/ArrayList;
    //   8: invokevirtual size : ()I
    //   11: istore #4
    //   13: iload_3
    //   14: iload #4
    //   16: if_icmpge -> 45
    //   19: aload_0
    //   20: getfield registrants : Ljava/util/ArrayList;
    //   23: iload_3
    //   24: invokevirtual get : (I)Ljava/lang/Object;
    //   27: checkcast android/os/Registrant
    //   30: astore #5
    //   32: aload #5
    //   34: aload_1
    //   35: aload_2
    //   36: invokevirtual internalNotifyRegistrant : (Ljava/lang/Object;Ljava/lang/Throwable;)V
    //   39: iinc #3, 1
    //   42: goto -> 13
    //   45: aload_0
    //   46: monitorexit
    //   47: return
    //   48: astore_1
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_1
    //   52: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #90	-> 2
    //   #91	-> 19
    //   #92	-> 32
    //   #90	-> 39
    //   #94	-> 45
    //   #89	-> 48
    // Exception table:
    //   from	to	target	type
    //   4	13	48	finally
    //   19	32	48	finally
    //   32	39	48	finally
  }
  
  public void notifyRegistrants() {
    internalNotifyRegistrants(null, null);
  }
  
  public void notifyException(Throwable paramThrowable) {
    internalNotifyRegistrants(null, paramThrowable);
  }
  
  public void notifyResult(Object paramObject) {
    internalNotifyRegistrants(paramObject, null);
  }
  
  public void notifyRegistrants(AsyncResult paramAsyncResult) {
    internalNotifyRegistrants(paramAsyncResult.result, paramAsyncResult.exception);
  }
  
  public void remove(Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iconst_0
    //   3: istore_2
    //   4: aload_0
    //   5: getfield registrants : Ljava/util/ArrayList;
    //   8: invokevirtual size : ()I
    //   11: istore_3
    //   12: iload_2
    //   13: iload_3
    //   14: if_icmpge -> 59
    //   17: aload_0
    //   18: getfield registrants : Ljava/util/ArrayList;
    //   21: iload_2
    //   22: invokevirtual get : (I)Ljava/lang/Object;
    //   25: checkcast android/os/Registrant
    //   28: astore #4
    //   30: aload #4
    //   32: invokevirtual getHandler : ()Landroid/os/Handler;
    //   35: astore #5
    //   37: aload #5
    //   39: ifnull -> 48
    //   42: aload #5
    //   44: aload_1
    //   45: if_acmpne -> 53
    //   48: aload #4
    //   50: invokevirtual clear : ()V
    //   53: iinc #2, 1
    //   56: goto -> 12
    //   59: aload_0
    //   60: invokevirtual removeCleared : ()V
    //   63: aload_0
    //   64: monitorexit
    //   65: return
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #128	-> 2
    //   #129	-> 17
    //   #132	-> 30
    //   #137	-> 37
    //   #138	-> 48
    //   #128	-> 53
    //   #142	-> 59
    //   #143	-> 63
    //   #127	-> 66
    // Exception table:
    //   from	to	target	type
    //   4	12	66	finally
    //   17	30	66	finally
    //   30	37	66	finally
    //   48	53	66	finally
    //   59	63	66	finally
  }
}
