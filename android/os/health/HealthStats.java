package android.os.health;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArrayMap;
import java.util.Arrays;
import java.util.Map;

public class HealthStats {
  private String mDataType;
  
  private int[] mMeasurementKeys;
  
  private long[] mMeasurementValues;
  
  private int[] mMeasurementsKeys;
  
  private ArrayMap<String, Long>[] mMeasurementsValues;
  
  private int[] mStatsKeys;
  
  private ArrayMap<String, HealthStats>[] mStatsValues;
  
  private int[] mTimerCounts;
  
  private int[] mTimerKeys;
  
  private long[] mTimerTimes;
  
  private int[] mTimersKeys;
  
  private ArrayMap<String, TimerStat>[] mTimersValues;
  
  private HealthStats() {
    throw new RuntimeException("unsupported");
  }
  
  public HealthStats(Parcel paramParcel) {
    this.mDataType = paramParcel.readString();
    int i = paramParcel.readInt();
    this.mTimerKeys = new int[i];
    this.mTimerCounts = new int[i];
    this.mTimerTimes = new long[i];
    byte b;
    for (b = 0; b < i; b++) {
      this.mTimerKeys[b] = paramParcel.readInt();
      this.mTimerCounts[b] = paramParcel.readInt();
      this.mTimerTimes[b] = paramParcel.readLong();
    } 
    i = paramParcel.readInt();
    this.mMeasurementKeys = new int[i];
    this.mMeasurementValues = new long[i];
    for (b = 0; b < i; b++) {
      this.mMeasurementKeys[b] = paramParcel.readInt();
      this.mMeasurementValues[b] = paramParcel.readLong();
    } 
    i = paramParcel.readInt();
    this.mStatsKeys = new int[i];
    this.mStatsValues = (ArrayMap<String, HealthStats>[])new ArrayMap[i];
    for (b = 0; b < i; b++) {
      this.mStatsKeys[b] = paramParcel.readInt();
      this.mStatsValues[b] = createHealthStatsMap(paramParcel);
    } 
    i = paramParcel.readInt();
    this.mTimersKeys = new int[i];
    this.mTimersValues = (ArrayMap<String, TimerStat>[])new ArrayMap[i];
    for (b = 0; b < i; b++) {
      this.mTimersKeys[b] = paramParcel.readInt();
      this.mTimersValues[b] = createParcelableMap(paramParcel, TimerStat.CREATOR);
    } 
    i = paramParcel.readInt();
    this.mMeasurementsKeys = new int[i];
    this.mMeasurementsValues = (ArrayMap<String, Long>[])new ArrayMap[i];
    for (b = 0; b < i; b++) {
      this.mMeasurementsKeys[b] = paramParcel.readInt();
      this.mMeasurementsValues[b] = createLongsMap(paramParcel);
    } 
  }
  
  public String getDataType() {
    return this.mDataType;
  }
  
  public boolean hasTimer(int paramInt) {
    boolean bool;
    if (getIndex(this.mTimerKeys, paramInt) >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public TimerStat getTimer(int paramInt) {
    int i = getIndex(this.mTimerKeys, paramInt);
    if (i >= 0)
      return new TimerStat(this.mTimerCounts[i], this.mTimerTimes[i]); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad timer key dataType=");
    stringBuilder.append(this.mDataType);
    stringBuilder.append(" key=");
    stringBuilder.append(paramInt);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public int getTimerCount(int paramInt) {
    int i = getIndex(this.mTimerKeys, paramInt);
    if (i >= 0)
      return this.mTimerCounts[i]; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad timer key dataType=");
    stringBuilder.append(this.mDataType);
    stringBuilder.append(" key=");
    stringBuilder.append(paramInt);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public long getTimerTime(int paramInt) {
    int i = getIndex(this.mTimerKeys, paramInt);
    if (i >= 0)
      return this.mTimerTimes[i]; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad timer key dataType=");
    stringBuilder.append(this.mDataType);
    stringBuilder.append(" key=");
    stringBuilder.append(paramInt);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public int getTimerKeyCount() {
    return this.mTimerKeys.length;
  }
  
  public int getTimerKeyAt(int paramInt) {
    return this.mTimerKeys[paramInt];
  }
  
  public boolean hasMeasurement(int paramInt) {
    boolean bool;
    if (getIndex(this.mMeasurementKeys, paramInt) >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public long getMeasurement(int paramInt) {
    int i = getIndex(this.mMeasurementKeys, paramInt);
    if (i >= 0)
      return this.mMeasurementValues[i]; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad measurement key dataType=");
    stringBuilder.append(this.mDataType);
    stringBuilder.append(" key=");
    stringBuilder.append(paramInt);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public int getMeasurementKeyCount() {
    return this.mMeasurementKeys.length;
  }
  
  public int getMeasurementKeyAt(int paramInt) {
    return this.mMeasurementKeys[paramInt];
  }
  
  public boolean hasStats(int paramInt) {
    boolean bool;
    if (getIndex(this.mStatsKeys, paramInt) >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Map<String, HealthStats> getStats(int paramInt) {
    int i = getIndex(this.mStatsKeys, paramInt);
    if (i >= 0)
      return (Map<String, HealthStats>)this.mStatsValues[i]; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad stats key dataType=");
    stringBuilder.append(this.mDataType);
    stringBuilder.append(" key=");
    stringBuilder.append(paramInt);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public int getStatsKeyCount() {
    return this.mStatsKeys.length;
  }
  
  public int getStatsKeyAt(int paramInt) {
    return this.mStatsKeys[paramInt];
  }
  
  public boolean hasTimers(int paramInt) {
    boolean bool;
    if (getIndex(this.mTimersKeys, paramInt) >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Map<String, TimerStat> getTimers(int paramInt) {
    int i = getIndex(this.mTimersKeys, paramInt);
    if (i >= 0)
      return (Map<String, TimerStat>)this.mTimersValues[i]; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad timers key dataType=");
    stringBuilder.append(this.mDataType);
    stringBuilder.append(" key=");
    stringBuilder.append(paramInt);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public int getTimersKeyCount() {
    return this.mTimersKeys.length;
  }
  
  public int getTimersKeyAt(int paramInt) {
    return this.mTimersKeys[paramInt];
  }
  
  public boolean hasMeasurements(int paramInt) {
    boolean bool;
    if (getIndex(this.mMeasurementsKeys, paramInt) >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Map<String, Long> getMeasurements(int paramInt) {
    int i = getIndex(this.mMeasurementsKeys, paramInt);
    if (i >= 0)
      return (Map<String, Long>)this.mMeasurementsValues[i]; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad measurements key dataType=");
    stringBuilder.append(this.mDataType);
    stringBuilder.append(" key=");
    stringBuilder.append(paramInt);
    throw new IndexOutOfBoundsException(stringBuilder.toString());
  }
  
  public int getMeasurementsKeyCount() {
    return this.mMeasurementsKeys.length;
  }
  
  public int getMeasurementsKeyAt(int paramInt) {
    return this.mMeasurementsKeys[paramInt];
  }
  
  private static int getIndex(int[] paramArrayOfint, int paramInt) {
    return Arrays.binarySearch(paramArrayOfint, paramInt);
  }
  
  private static ArrayMap<String, HealthStats> createHealthStatsMap(Parcel paramParcel) {
    int i = paramParcel.readInt();
    ArrayMap<String, HealthStats> arrayMap = new ArrayMap(i);
    for (byte b = 0; b < i; b++)
      arrayMap.put(paramParcel.readString(), new HealthStats(paramParcel)); 
    return arrayMap;
  }
  
  private static <T extends Parcelable> ArrayMap<String, T> createParcelableMap(Parcel paramParcel, Parcelable.Creator<T> paramCreator) {
    int i = paramParcel.readInt();
    ArrayMap<String, T> arrayMap = new ArrayMap(i);
    for (byte b = 0; b < i; b++)
      arrayMap.put(paramParcel.readString(), (Parcelable)paramCreator.createFromParcel(paramParcel)); 
    return arrayMap;
  }
  
  private static ArrayMap<String, Long> createLongsMap(Parcel paramParcel) {
    int i = paramParcel.readInt();
    ArrayMap<String, Long> arrayMap = new ArrayMap(i);
    for (byte b = 0; b < i; b++)
      arrayMap.put(paramParcel.readString(), Long.valueOf(paramParcel.readLong())); 
    return arrayMap;
  }
}
