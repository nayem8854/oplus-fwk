package android.os.health;

import android.content.Context;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import com.android.internal.app.IBatteryStats;

public class SystemHealthManager {
  private final IBatteryStats mBatteryStats;
  
  public SystemHealthManager() {
    this(IBatteryStats.Stub.asInterface(ServiceManager.getService("batterystats")));
  }
  
  public SystemHealthManager(IBatteryStats paramIBatteryStats) {
    this.mBatteryStats = paramIBatteryStats;
  }
  
  public static SystemHealthManager from(Context paramContext) {
    return (SystemHealthManager)paramContext.getSystemService("systemhealth");
  }
  
  public HealthStats takeUidSnapshot(int paramInt) {
    try {
      HealthStatsParceler healthStatsParceler = this.mBatteryStats.takeUidSnapshot(paramInt);
      return healthStatsParceler.getHealthStats();
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException);
    } 
  }
  
  public HealthStats takeMyUidSnapshot() {
    return takeUidSnapshot(Process.myUid());
  }
  
  public HealthStats[] takeUidSnapshots(int[] paramArrayOfint) {
    try {
      HealthStatsParceler[] arrayOfHealthStatsParceler = this.mBatteryStats.takeUidSnapshots(paramArrayOfint);
      HealthStats[] arrayOfHealthStats = new HealthStats[paramArrayOfint.length];
      int i = paramArrayOfint.length;
      for (byte b = 0; b < i; b++)
        arrayOfHealthStats[b] = arrayOfHealthStatsParceler[b].getHealthStats(); 
      return arrayOfHealthStats;
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException);
    } 
  }
}
