package android.os.health;

import android.os.Parcel;
import android.os.Parcelable;

public final class TimerStat implements Parcelable {
  public static final Parcelable.Creator<TimerStat> CREATOR = new Parcelable.Creator<TimerStat>() {
      public TimerStat createFromParcel(Parcel param1Parcel) {
        return new TimerStat(param1Parcel);
      }
      
      public TimerStat[] newArray(int param1Int) {
        return new TimerStat[param1Int];
      }
    };
  
  private int mCount;
  
  private long mTime;
  
  public TimerStat() {}
  
  public TimerStat(int paramInt, long paramLong) {
    this.mCount = paramInt;
    this.mTime = paramLong;
  }
  
  public TimerStat(Parcel paramParcel) {
    this.mCount = paramParcel.readInt();
    this.mTime = paramParcel.readLong();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mCount);
    paramParcel.writeLong(this.mTime);
  }
  
  public void setCount(int paramInt) {
    this.mCount = paramInt;
  }
  
  public int getCount() {
    return this.mCount;
  }
  
  public void setTime(long paramLong) {
    this.mTime = paramLong;
  }
  
  public long getTime() {
    return this.mTime;
  }
}
