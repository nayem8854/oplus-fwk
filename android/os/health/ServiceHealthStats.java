package android.os.health;

public final class ServiceHealthStats {
  public static final HealthKeys.Constants CONSTANTS = new HealthKeys.Constants(ServiceHealthStats.class);
  
  @Constant(type = 1)
  public static final int MEASUREMENT_LAUNCH_COUNT = 50002;
  
  @Constant(type = 1)
  public static final int MEASUREMENT_START_SERVICE_COUNT = 50001;
}
