package android.os.health;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.Arrays;

public class HealthKeys {
  public static final int BASE_PACKAGE = 40000;
  
  public static final int BASE_PID = 20000;
  
  public static final int BASE_PROCESS = 30000;
  
  public static final int BASE_SERVICE = 50000;
  
  public static final int BASE_UID = 10000;
  
  public static final int TYPE_COUNT = 5;
  
  public static final int TYPE_MEASUREMENT = 1;
  
  public static final int TYPE_MEASUREMENTS = 4;
  
  public static final int TYPE_STATS = 2;
  
  public static final int TYPE_TIMER = 0;
  
  public static final int TYPE_TIMERS = 3;
  
  public static final int UNKNOWN_KEY = 0;
  
  @Retention(RetentionPolicy.RUNTIME)
  @Target({ElementType.FIELD})
  public static @interface Constant {
    int type();
  }
  
  public static class Constants {
    private final String mDataType;
    
    private final int[][] mKeys;
    
    public Constants(Class param1Class) {
      StringBuilder stringBuilder;
      this.mKeys = new int[5][];
      this.mDataType = param1Class.getSimpleName();
      Field[] arrayOfField = param1Class.getDeclaredFields();
      int i = arrayOfField.length;
      HealthKeys.SortedIntArray[] arrayOfSortedIntArray = new HealthKeys.SortedIntArray[this.mKeys.length];
      byte b;
      for (b = 0; b < arrayOfSortedIntArray.length; b++)
        arrayOfSortedIntArray[b] = new HealthKeys.SortedIntArray(i); 
      for (b = 0; b < i; b++) {
        Field field = arrayOfField[b];
        HealthKeys.Constant constant = field.<HealthKeys.Constant>getAnnotation(HealthKeys.Constant.class);
        if (constant != null) {
          int j = constant.type();
          if (j < arrayOfSortedIntArray.length) {
            try {
              arrayOfSortedIntArray[j].addValue(field.getInt(null));
            } catch (IllegalAccessException illegalAccessException) {
              stringBuilder = new StringBuilder();
              stringBuilder.append("Can't read constant value type=");
              stringBuilder.append(j);
              stringBuilder.append(" field=");
              stringBuilder.append(field);
              throw new RuntimeException(stringBuilder.toString(), illegalAccessException);
            } 
          } else {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Unknown Constant type ");
            stringBuilder1.append(j);
            stringBuilder1.append(" on ");
            stringBuilder1.append(field);
            throw new RuntimeException(stringBuilder1.toString());
          } 
        } 
      } 
      for (b = 0; b < stringBuilder.length; b++)
        this.mKeys[b] = stringBuilder[b].getArray(); 
    }
    
    public String getDataType() {
      return this.mDataType;
    }
    
    public int getSize(int param1Int) {
      return (this.mKeys[param1Int]).length;
    }
    
    public int getIndex(int param1Int1, int param1Int2) {
      int i = Arrays.binarySearch(this.mKeys[param1Int1], param1Int2);
      if (i >= 0)
        return i; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown Constant ");
      stringBuilder.append(param1Int2);
      stringBuilder.append(" (of type ");
      stringBuilder.append(param1Int1);
      stringBuilder.append(" )");
      throw new RuntimeException(stringBuilder.toString());
    }
    
    public int[] getKeys(int param1Int) {
      return this.mKeys[param1Int];
    }
  }
  
  private static class SortedIntArray {
    int[] mArray;
    
    int mCount;
    
    SortedIntArray(int param1Int) {
      this.mArray = new int[param1Int];
    }
    
    void addValue(int param1Int) {
      int arrayOfInt[] = this.mArray, i = this.mCount;
      this.mCount = i + 1;
      arrayOfInt[i] = param1Int;
    }
    
    int[] getArray() {
      int i = this.mCount, arrayOfInt1[] = this.mArray;
      if (i == arrayOfInt1.length) {
        Arrays.sort(arrayOfInt1);
        return this.mArray;
      } 
      int[] arrayOfInt2 = new int[i];
      System.arraycopy(arrayOfInt1, 0, arrayOfInt2, 0, i);
      Arrays.sort(arrayOfInt2);
      return arrayOfInt2;
    }
  }
}
