package android.os.health;

import android.os.Parcel;
import android.os.Parcelable;

public class HealthStatsParceler implements Parcelable {
  public static final Parcelable.Creator<HealthStatsParceler> CREATOR = new Parcelable.Creator<HealthStatsParceler>() {
      public HealthStatsParceler createFromParcel(Parcel param1Parcel) {
        return new HealthStatsParceler(param1Parcel);
      }
      
      public HealthStatsParceler[] newArray(int param1Int) {
        return new HealthStatsParceler[param1Int];
      }
    };
  
  private HealthStats mHealthStats;
  
  private HealthStatsWriter mWriter;
  
  public HealthStatsParceler(HealthStatsWriter paramHealthStatsWriter) {
    this.mWriter = paramHealthStatsWriter;
  }
  
  public HealthStatsParceler(Parcel paramParcel) {
    this.mHealthStats = new HealthStats(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    HealthStatsWriter healthStatsWriter = this.mWriter;
    if (healthStatsWriter != null) {
      healthStatsWriter.flattenToParcel(paramParcel);
      return;
    } 
    throw new RuntimeException("Can not re-parcel HealthStatsParceler that was constructed from a Parcel");
  }
  
  public HealthStats getHealthStats() {
    if (this.mWriter != null) {
      Parcel parcel = Parcel.obtain();
      this.mWriter.flattenToParcel(parcel);
      parcel.setDataPosition(0);
      this.mHealthStats = new HealthStats(parcel);
      parcel.recycle();
    } 
    return this.mHealthStats;
  }
}
