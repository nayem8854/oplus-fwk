package android.os.health;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArrayMap;

public class HealthStatsWriter {
  private final HealthKeys.Constants mConstants;
  
  private final boolean[] mMeasurementFields;
  
  private final long[] mMeasurementValues;
  
  private final ArrayMap<String, Long>[] mMeasurementsValues;
  
  private final ArrayMap<String, HealthStatsWriter>[] mStatsValues;
  
  private final int[] mTimerCounts;
  
  private final boolean[] mTimerFields;
  
  private final long[] mTimerTimes;
  
  private final ArrayMap<String, TimerStat>[] mTimersValues;
  
  public HealthStatsWriter(HealthKeys.Constants paramConstants) {
    this.mConstants = paramConstants;
    int i = paramConstants.getSize(0);
    this.mTimerFields = new boolean[i];
    this.mTimerCounts = new int[i];
    this.mTimerTimes = new long[i];
    i = paramConstants.getSize(1);
    this.mMeasurementFields = new boolean[i];
    this.mMeasurementValues = new long[i];
    i = paramConstants.getSize(2);
    this.mStatsValues = (ArrayMap<String, HealthStatsWriter>[])new ArrayMap[i];
    i = paramConstants.getSize(3);
    this.mTimersValues = (ArrayMap<String, TimerStat>[])new ArrayMap[i];
    i = paramConstants.getSize(4);
    this.mMeasurementsValues = (ArrayMap<String, Long>[])new ArrayMap[i];
  }
  
  public void addTimer(int paramInt1, int paramInt2, long paramLong) {
    paramInt1 = this.mConstants.getIndex(0, paramInt1);
    this.mTimerFields[paramInt1] = true;
    this.mTimerCounts[paramInt1] = paramInt2;
    this.mTimerTimes[paramInt1] = paramLong;
  }
  
  public void addMeasurement(int paramInt, long paramLong) {
    paramInt = this.mConstants.getIndex(1, paramInt);
    this.mMeasurementFields[paramInt] = true;
    this.mMeasurementValues[paramInt] = paramLong;
  }
  
  public void addStats(int paramInt, String paramString, HealthStatsWriter paramHealthStatsWriter) {
    paramInt = this.mConstants.getIndex(2, paramInt);
    ArrayMap<String, HealthStatsWriter> arrayOfArrayMap[] = this.mStatsValues, arrayMap1 = arrayOfArrayMap[paramInt];
    ArrayMap<String, HealthStatsWriter> arrayMap2 = arrayMap1;
    if (arrayMap1 == null) {
      arrayMap2 = new ArrayMap(1);
      arrayOfArrayMap[paramInt] = arrayMap2;
    } 
    arrayMap2.put(paramString, paramHealthStatsWriter);
  }
  
  public void addTimers(int paramInt, String paramString, TimerStat paramTimerStat) {
    paramInt = this.mConstants.getIndex(3, paramInt);
    ArrayMap<String, TimerStat> arrayOfArrayMap[] = this.mTimersValues, arrayMap1 = arrayOfArrayMap[paramInt];
    ArrayMap<String, TimerStat> arrayMap2 = arrayMap1;
    if (arrayMap1 == null) {
      arrayMap2 = new ArrayMap(1);
      arrayOfArrayMap[paramInt] = arrayMap2;
    } 
    arrayMap2.put(paramString, paramTimerStat);
  }
  
  public void addMeasurements(int paramInt, String paramString, long paramLong) {
    paramInt = this.mConstants.getIndex(4, paramInt);
    ArrayMap<String, Long> arrayOfArrayMap[] = this.mMeasurementsValues, arrayMap1 = arrayOfArrayMap[paramInt];
    ArrayMap<String, Long> arrayMap2 = arrayMap1;
    if (arrayMap1 == null) {
      arrayMap2 = new ArrayMap(1);
      arrayOfArrayMap[paramInt] = arrayMap2;
    } 
    arrayMap2.put(paramString, Long.valueOf(paramLong));
  }
  
  public void flattenToParcel(Parcel paramParcel) {
    paramParcel.writeString(this.mConstants.getDataType());
    paramParcel.writeInt(countBooleanArray(this.mTimerFields));
    int[] arrayOfInt = this.mConstants.getKeys(0);
    byte b;
    for (b = 0; b < arrayOfInt.length; b++) {
      if (this.mTimerFields[b]) {
        paramParcel.writeInt(arrayOfInt[b]);
        paramParcel.writeInt(this.mTimerCounts[b]);
        paramParcel.writeLong(this.mTimerTimes[b]);
      } 
    } 
    paramParcel.writeInt(countBooleanArray(this.mMeasurementFields));
    arrayOfInt = this.mConstants.getKeys(1);
    for (b = 0; b < arrayOfInt.length; b++) {
      if (this.mMeasurementFields[b]) {
        paramParcel.writeInt(arrayOfInt[b]);
        paramParcel.writeLong(this.mMeasurementValues[b]);
      } 
    } 
    paramParcel.writeInt(countObjectArray(this.mStatsValues));
    arrayOfInt = this.mConstants.getKeys(2);
    for (b = 0; b < arrayOfInt.length; b++) {
      if (this.mStatsValues[b] != null) {
        paramParcel.writeInt(arrayOfInt[b]);
        writeHealthStatsWriterMap(paramParcel, this.mStatsValues[b]);
      } 
    } 
    paramParcel.writeInt(countObjectArray(this.mTimersValues));
    arrayOfInt = this.mConstants.getKeys(3);
    for (b = 0; b < arrayOfInt.length; b++) {
      if (this.mTimersValues[b] != null) {
        paramParcel.writeInt(arrayOfInt[b]);
        writeParcelableMap(paramParcel, this.mTimersValues[b]);
      } 
    } 
    paramParcel.writeInt(countObjectArray(this.mMeasurementsValues));
    arrayOfInt = this.mConstants.getKeys(4);
    for (b = 0; b < arrayOfInt.length; b++) {
      if (this.mMeasurementsValues[b] != null) {
        paramParcel.writeInt(arrayOfInt[b]);
        writeLongsMap(paramParcel, this.mMeasurementsValues[b]);
      } 
    } 
  }
  
  private static int countBooleanArray(boolean[] paramArrayOfboolean) {
    int i = 0;
    int j = paramArrayOfboolean.length;
    for (byte b = 0; b < j; b++, i = k) {
      int k = i;
      if (paramArrayOfboolean[b])
        k = i + 1; 
    } 
    return i;
  }
  
  private static <T> int countObjectArray(T[] paramArrayOfT) {
    int i = 0;
    int j = paramArrayOfT.length;
    for (byte b = 0; b < j; b++, i = k) {
      int k = i;
      if (paramArrayOfT[b] != null)
        k = i + 1; 
    } 
    return i;
  }
  
  private static void writeHealthStatsWriterMap(Parcel paramParcel, ArrayMap<String, HealthStatsWriter> paramArrayMap) {
    int i = paramArrayMap.size();
    paramParcel.writeInt(i);
    for (byte b = 0; b < i; b++) {
      paramParcel.writeString((String)paramArrayMap.keyAt(b));
      ((HealthStatsWriter)paramArrayMap.valueAt(b)).flattenToParcel(paramParcel);
    } 
  }
  
  private static <T extends Parcelable> void writeParcelableMap(Parcel paramParcel, ArrayMap<String, T> paramArrayMap) {
    int i = paramArrayMap.size();
    paramParcel.writeInt(i);
    for (byte b = 0; b < i; b++) {
      paramParcel.writeString((String)paramArrayMap.keyAt(b));
      ((Parcelable)paramArrayMap.valueAt(b)).writeToParcel(paramParcel, 0);
    } 
  }
  
  private static void writeLongsMap(Parcel paramParcel, ArrayMap<String, Long> paramArrayMap) {
    int i = paramArrayMap.size();
    paramParcel.writeInt(i);
    for (byte b = 0; b < i; b++) {
      paramParcel.writeString((String)paramArrayMap.keyAt(b));
      paramParcel.writeLong(((Long)paramArrayMap.valueAt(b)).longValue());
    } 
  }
}
