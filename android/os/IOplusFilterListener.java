package android.os;

import java.util.HashMap;
import java.util.Map;

public interface IOplusFilterListener extends IInterface {
  void onFilterChanged(int paramInt, Map paramMap) throws RemoteException;
  
  class Default implements IOplusFilterListener {
    public void onFilterChanged(int param1Int, Map param1Map) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusFilterListener {
    private static final String DESCRIPTOR = "android.os.IOplusFilterListener";
    
    static final int TRANSACTION_onFilterChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.os.IOplusFilterListener");
    }
    
    public static IOplusFilterListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.os.IOplusFilterListener");
      if (iInterface != null && iInterface instanceof IOplusFilterListener)
        return (IOplusFilterListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onFilterChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.os.IOplusFilterListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.os.IOplusFilterListener");
      param1Int1 = param1Parcel1.readInt();
      ClassLoader classLoader = getClass().getClassLoader();
      HashMap hashMap = param1Parcel1.readHashMap(classLoader);
      onFilterChanged(param1Int1, hashMap);
      return true;
    }
    
    private static class Proxy implements IOplusFilterListener {
      public static IOplusFilterListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.os.IOplusFilterListener";
      }
      
      public void onFilterChanged(int param2Int, Map param2Map) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.os.IOplusFilterListener");
          parcel.writeInt(param2Int);
          parcel.writeMap(param2Map);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IOplusFilterListener.Stub.getDefaultImpl() != null) {
            IOplusFilterListener.Stub.getDefaultImpl().onFilterChanged(param2Int, param2Map);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusFilterListener param1IOplusFilterListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusFilterListener != null) {
          Proxy.sDefaultImpl = param1IOplusFilterListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusFilterListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
