package android.os;

import android.annotation.SystemApi;
import android.app.AppGlobals;
import android.app.AppOpsManager;
import android.app.Application;
import android.compat.Compatibility;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Environment extends OplusBaseEnvironment {
  private static final long DEFAULT_SCOPED_STORAGE = 149924527L;
  
  public static String DIRECTORY_ALARMS;
  
  @Deprecated
  public static final String DIRECTORY_ANDROID = "Android";
  
  public static String DIRECTORY_AUDIOBOOKS;
  
  public static String DIRECTORY_DCIM;
  
  public static String DIRECTORY_DOCUMENTS;
  
  public static String DIRECTORY_DOWNLOADS;
  
  public static String DIRECTORY_MOVIES;
  
  public static String DIRECTORY_MUSIC;
  
  public static String DIRECTORY_NOTIFICATIONS;
  
  public static String DIRECTORY_PICTURES;
  
  public static String DIRECTORY_PODCASTS;
  
  public static String DIRECTORY_RINGTONES;
  
  public static String DIRECTORY_SCREENSHOTS;
  
  public static final String DIR_ANDROID = "Android";
  
  private static final File DIR_ANDROID_DATA;
  
  private static final File DIR_ANDROID_EXPAND;
  
  private static final File DIR_ANDROID_ROOT = getDirectory("ANDROID_ROOT", "/system");
  
  private static final File DIR_ANDROID_STORAGE;
  
  private static final File DIR_APEX_ROOT;
  
  private static final String DIR_CACHE = "cache";
  
  private static final String DIR_DATA = "data";
  
  private static final File DIR_DOWNLOAD_CACHE;
  
  private static final String DIR_FILES = "files";
  
  private static final String DIR_MEDIA = "media";
  
  private static final String DIR_OBB = "obb";
  
  private static final File DIR_ODM_ROOT;
  
  private static final File DIR_OEM_ROOT;
  
  private static final File DIR_PRODUCT_ROOT;
  
  private static final File DIR_SYSTEM_EXT_ROOT;
  
  private static final File DIR_VENDOR_ROOT;
  
  private static final String ENV_ANDROID_DATA = "ANDROID_DATA";
  
  private static final String ENV_ANDROID_EXPAND = "ANDROID_EXPAND";
  
  private static final String ENV_ANDROID_ROOT = "ANDROID_ROOT";
  
  private static final String ENV_ANDROID_STORAGE = "ANDROID_STORAGE";
  
  private static final String ENV_APEX_ROOT = "APEX_ROOT";
  
  private static final String ENV_DOWNLOAD_CACHE = "DOWNLOAD_CACHE";
  
  private static final String ENV_EXTERNAL_STORAGE = "EXTERNAL_STORAGE";
  
  private static final String ENV_ODM_ROOT = "ODM_ROOT";
  
  private static final String ENV_OEM_ROOT = "OEM_ROOT";
  
  private static final String ENV_PRODUCT_ROOT = "PRODUCT_ROOT";
  
  private static final String ENV_SYSTEM_EXT_ROOT = "SYSTEM_EXT_ROOT";
  
  private static final String ENV_VENDOR_ROOT = "VENDOR_ROOT";
  
  private static final long FORCE_ENABLE_SCOPED_STORAGE = 132649864L;
  
  public static final int HAS_ALARMS = 8;
  
  public static final int HAS_ANDROID = 65536;
  
  public static final int HAS_AUDIOBOOKS = 1024;
  
  public static final int HAS_DCIM = 256;
  
  public static final int HAS_DOCUMENTS = 512;
  
  public static final int HAS_DOWNLOADS = 128;
  
  public static final int HAS_MOVIES = 64;
  
  public static final int HAS_MUSIC = 1;
  
  public static final int HAS_NOTIFICATIONS = 16;
  
  public static final int HAS_OTHER = 131072;
  
  public static final int HAS_PICTURES = 32;
  
  public static final int HAS_PODCASTS = 2;
  
  public static final int HAS_RINGTONES = 4;
  
  public static final String MEDIA_BAD_REMOVAL = "bad_removal";
  
  public static final String MEDIA_CHECKING = "checking";
  
  public static final String MEDIA_EJECTING = "ejecting";
  
  public static final String MEDIA_MOUNTED = "mounted";
  
  public static final String MEDIA_MOUNTED_READ_ONLY = "mounted_ro";
  
  public static final String MEDIA_NOFS = "nofs";
  
  public static final String MEDIA_REMOVED = "removed";
  
  public static final String MEDIA_SHARED = "shared";
  
  public static final String MEDIA_UNKNOWN = "unknown";
  
  public static final String MEDIA_UNMOUNTABLE = "unmountable";
  
  public static final String MEDIA_UNMOUNTED = "unmounted";
  
  public static final String[] STANDARD_DIRECTORIES;
  
  private static final String TAG = "Environment";
  
  private static UserEnvironment sCurrentUser;
  
  private static boolean sUserRequired;
  
  static {
    DIR_ANDROID_DATA = getDirectory("ANDROID_DATA", "/data");
    DIR_ANDROID_EXPAND = getDirectory("ANDROID_EXPAND", "/mnt/expand");
    DIR_ANDROID_STORAGE = getDirectory("ANDROID_STORAGE", "/storage");
    DIR_DOWNLOAD_CACHE = getDirectory("DOWNLOAD_CACHE", "/cache");
    DIR_OEM_ROOT = getDirectory("OEM_ROOT", "/oem");
    DIR_ODM_ROOT = getDirectory("ODM_ROOT", "/odm");
    DIR_VENDOR_ROOT = getDirectory("VENDOR_ROOT", "/vendor");
    DIR_PRODUCT_ROOT = getDirectory("PRODUCT_ROOT", "/product");
    DIR_SYSTEM_EXT_ROOT = getDirectory("SYSTEM_EXT_ROOT", "/system_ext");
    DIR_APEX_ROOT = getDirectory("APEX_ROOT", "/apex");
    initForCurrentUser();
    DIRECTORY_MUSIC = "Music";
    DIRECTORY_PODCASTS = "Podcasts";
    DIRECTORY_RINGTONES = "Ringtones";
    DIRECTORY_ALARMS = "Alarms";
    DIRECTORY_NOTIFICATIONS = "Notifications";
    DIRECTORY_PICTURES = "Pictures";
    DIRECTORY_MOVIES = "Movies";
    DIRECTORY_DOWNLOADS = "Download";
    DIRECTORY_DCIM = "DCIM";
    DIRECTORY_DOCUMENTS = "Documents";
    DIRECTORY_SCREENSHOTS = "Screenshots";
    DIRECTORY_AUDIOBOOKS = "Audiobooks";
    STANDARD_DIRECTORIES = new String[] { 
        "Music", "Podcasts", "Ringtones", "Alarms", "Notifications", "Pictures", "Movies", "Download", "DCIM", "Documents", 
        "Audiobooks" };
  }
  
  public static void initForCurrentUser() {
    int i = UserHandle.myUserId();
    sCurrentUser = new UserEnvironment(i);
  }
  
  class UserEnvironment {
    private final int mUserId;
    
    public UserEnvironment(Environment this$0) {
      this.mUserId = this$0;
    }
    
    public File[] getExternalDirs() {
      StorageVolume[] arrayOfStorageVolume = StorageManager.getVolumeList(this.mUserId, 256);
      File[] arrayOfFile = new File[arrayOfStorageVolume.length];
      for (byte b = 0; b < arrayOfStorageVolume.length; b++)
        arrayOfFile[b] = arrayOfStorageVolume[b].getPathFile(); 
      return arrayOfFile;
    }
    
    @Deprecated
    public File getExternalStorageDirectory() {
      return getExternalDirs()[0];
    }
    
    @Deprecated
    public File getExternalStoragePublicDirectory(String param1String) {
      return buildExternalStoragePublicDirs(param1String)[0];
    }
    
    public File[] buildExternalStoragePublicDirs(String param1String) {
      return Environment.buildPaths(getExternalDirs(), new String[] { param1String });
    }
    
    public File[] buildExternalStorageAndroidDataDirs() {
      return Environment.buildPaths(getExternalDirs(), new String[] { "Android", "data" });
    }
    
    public File[] buildExternalStorageAndroidObbDirs() {
      return Environment.buildPaths(getExternalDirs(), new String[] { "Android", "obb" });
    }
    
    public File[] buildExternalStorageAppDataDirs(String param1String) {
      return Environment.buildPaths(getExternalDirs(), new String[] { "Android", "data", param1String });
    }
    
    public File[] buildExternalStorageAppMediaDirs(String param1String) {
      return Environment.buildPaths(getExternalDirs(), new String[] { "Android", "media", param1String });
    }
    
    public File[] buildExternalStorageAppObbDirs(String param1String) {
      return Environment.buildPaths(getExternalDirs(), new String[] { "Android", "obb", param1String });
    }
    
    public File[] buildExternalStorageAppFilesDirs(String param1String) {
      return Environment.buildPaths(getExternalDirs(), new String[] { "Android", "data", param1String, "files" });
    }
    
    public File[] buildExternalStorageAppCacheDirs(String param1String) {
      return Environment.buildPaths(getExternalDirs(), new String[] { "Android", "data", param1String, "cache" });
    }
  }
  
  public static File getRootDirectory() {
    return DIR_ANDROID_ROOT;
  }
  
  public static File getStorageDirectory() {
    return DIR_ANDROID_STORAGE;
  }
  
  @SystemApi
  public static File getOemDirectory() {
    return DIR_OEM_ROOT;
  }
  
  @SystemApi
  public static File getOdmDirectory() {
    return DIR_ODM_ROOT;
  }
  
  @SystemApi
  public static File getVendorDirectory() {
    return DIR_VENDOR_ROOT;
  }
  
  @SystemApi
  public static File getProductDirectory() {
    return DIR_PRODUCT_ROOT;
  }
  
  @SystemApi
  @Deprecated
  public static File getProductServicesDirectory() {
    return getDirectory("PRODUCT_SERVICES_ROOT", "/product_services");
  }
  
  @SystemApi
  public static File getSystemExtDirectory() {
    return DIR_SYSTEM_EXT_ROOT;
  }
  
  public static File getApexDirectory() {
    return DIR_APEX_ROOT;
  }
  
  @Deprecated
  public static File getUserSystemDirectory(int paramInt) {
    return new File(new File(getDataSystemDirectory(), "users"), Integer.toString(paramInt));
  }
  
  @Deprecated
  public static File getUserConfigDirectory(int paramInt) {
    return new File(new File(new File(getDataDirectory(), "misc"), "user"), Integer.toString(paramInt));
  }
  
  public static File getDataDirectory() {
    return DIR_ANDROID_DATA;
  }
  
  public static File getDataDirectory(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return DIR_ANDROID_DATA; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("/mnt/expand/");
    stringBuilder.append(paramString);
    return new File(stringBuilder.toString());
  }
  
  public static File getExpandDirectory() {
    return DIR_ANDROID_EXPAND;
  }
  
  public static File getDataSystemDirectory() {
    return new File(getDataDirectory(), "system");
  }
  
  public static File getDataSystemDeDirectory() {
    return buildPath(getDataDirectory(), new String[] { "system_de" });
  }
  
  public static File getDataSystemCeDirectory() {
    return buildPath(getDataDirectory(), new String[] { "system_ce" });
  }
  
  public static File getDataSystemCeDirectory(int paramInt) {
    return buildPath(getDataDirectory(), new String[] { "system_ce", String.valueOf(paramInt) });
  }
  
  public static File getDataSystemDeDirectory(int paramInt) {
    return buildPath(getDataDirectory(), new String[] { "system_de", String.valueOf(paramInt) });
  }
  
  public static File getDataMiscDirectory() {
    return new File(getDataDirectory(), "misc");
  }
  
  public static File getDataMiscCeDirectory() {
    return buildPath(getDataDirectory(), new String[] { "misc_ce" });
  }
  
  public static File getDataMiscCeDirectory(int paramInt) {
    return buildPath(getDataDirectory(), new String[] { "misc_ce", String.valueOf(paramInt) });
  }
  
  public static File getDataMiscDeDirectory(int paramInt) {
    return buildPath(getDataDirectory(), new String[] { "misc_de", String.valueOf(paramInt) });
  }
  
  private static File getDataProfilesDeDirectory(int paramInt) {
    return buildPath(getDataDirectory(), new String[] { "misc", "profiles", "cur", String.valueOf(paramInt) });
  }
  
  public static File getDataVendorCeDirectory(int paramInt) {
    return buildPath(getDataDirectory(), new String[] { "vendor_ce", String.valueOf(paramInt) });
  }
  
  public static File getDataVendorDeDirectory(int paramInt) {
    return buildPath(getDataDirectory(), new String[] { "vendor_de", String.valueOf(paramInt) });
  }
  
  public static File getDataRefProfilesDePackageDirectory(String paramString) {
    return buildPath(getDataDirectory(), new String[] { "misc", "profiles", "ref", paramString });
  }
  
  public static File getDataProfilesDePackageDirectory(int paramInt, String paramString) {
    return buildPath(getDataProfilesDeDirectory(paramInt), new String[] { paramString });
  }
  
  public static File getDataAppDirectory(String paramString) {
    return new File(getDataDirectory(paramString), "app");
  }
  
  public static File getDataStagingDirectory(String paramString) {
    return new File(getDataDirectory(paramString), "app-staging");
  }
  
  public static File getDataUserCeDirectory(String paramString) {
    return new File(getDataDirectory(paramString), "user");
  }
  
  public static File getDataUserCeDirectory(String paramString, int paramInt) {
    return new File(getDataUserCeDirectory(paramString), String.valueOf(paramInt));
  }
  
  public static File getDataUserCePackageDirectory(String paramString1, int paramInt, String paramString2) {
    return new File(getDataUserCeDirectory(paramString1, paramInt), paramString2);
  }
  
  public static File getDataUserDeDirectory(String paramString) {
    return new File(getDataDirectory(paramString), "user_de");
  }
  
  public static File getDataUserDeDirectory(String paramString, int paramInt) {
    return new File(getDataUserDeDirectory(paramString), String.valueOf(paramInt));
  }
  
  public static File getDataUserDePackageDirectory(String paramString1, int paramInt, String paramString2) {
    return new File(getDataUserDeDirectory(paramString1, paramInt), paramString2);
  }
  
  public static File getDataPreloadsDirectory() {
    return new File(getDataDirectory(), "preloads");
  }
  
  public static File getDataPreloadsDemoDirectory() {
    return new File(getDataPreloadsDirectory(), "demo");
  }
  
  public static File getDataPreloadsAppsDirectory() {
    return new File(getDataPreloadsDirectory(), "apps");
  }
  
  public static File getDataPreloadsMediaDirectory() {
    return new File(getDataPreloadsDirectory(), "media");
  }
  
  public static File getDataPreloadsFileCacheDirectory(String paramString) {
    return new File(getDataPreloadsFileCacheDirectory(), paramString);
  }
  
  public static File getDataPreloadsFileCacheDirectory() {
    return new File(getDataPreloadsDirectory(), "file_cache");
  }
  
  public static File getPackageCacheDirectory() {
    return new File(getDataSystemDirectory(), "package_cache");
  }
  
  @SystemApi
  public static Collection<File> getInternalMediaDirectories() {
    ArrayList<File> arrayList = new ArrayList();
    addCanonicalFile(arrayList, new File(getRootDirectory(), "media"));
    addCanonicalFile(arrayList, new File(getOemDirectory(), "media"));
    addCanonicalFile(arrayList, new File(getProductDirectory(), "media"));
    addCanonicalFile(arrayList, new File(OplusBaseEnvironment.getMyProductDirectory(), "media"));
    addCanonicalFile(arrayList, new File(OplusBaseEnvironment.getMyCountryDirectory(), "media"));
    addCanonicalFile(arrayList, new File(OplusBaseEnvironment.getMyOperatorDirectory(), "media"));
    addCanonicalFile(arrayList, new File(OplusBaseEnvironment.getMyCompanyDirectory(), "media"));
    addCanonicalFile(arrayList, new File(getSystemExtDirectory(), "media"));
    addCanonicalFile(arrayList, new File(new File("/data/theme"), "ring"));
    addCanonicalFile(arrayList, new File(new File("/data/theme"), "audio"));
    return arrayList;
  }
  
  private static void addCanonicalFile(List<File> paramList, File paramFile) {
    try {
      paramList.add(paramFile.getCanonicalFile());
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to resolve ");
      stringBuilder.append(paramFile);
      stringBuilder.append(": ");
      stringBuilder.append(iOException);
      Log.w("Environment", stringBuilder.toString());
      paramList.add(paramFile);
    } 
  }
  
  @Deprecated
  public static File getExternalStorageDirectory() {
    throwIfUserRequired();
    File[] arrayOfFile = sCurrentUser.getExternalDirs();
    if (arrayOfFile == null || arrayOfFile.length == 0) {
      Log.e("environment", "getExternalStorageState files.length == 0 ");
      return null;
    } 
    return arrayOfFile[0];
  }
  
  public static File getLegacyExternalStorageDirectory() {
    return new File(System.getenv("EXTERNAL_STORAGE"));
  }
  
  public static File getLegacyExternalStorageObbDirectory() {
    return buildPath(getLegacyExternalStorageDirectory(), new String[] { "Android", "obb" });
  }
  
  public static boolean isStandardDirectory(String paramString) {
    for (String str : STANDARD_DIRECTORIES) {
      if (str.equals(paramString))
        return true; 
    } 
    return false;
  }
  
  public static int classifyExternalStorageDirectory(File paramFile) {
    int i = 0;
    File[] arrayOfFile;
    int j;
    byte b;
    for (arrayOfFile = FileUtils.listFilesOrEmpty(paramFile), j = arrayOfFile.length, b = 0; b < j; ) {
      int k;
      File file = arrayOfFile[b];
      if (file.isFile() && isInterestingFile(file)) {
        k = i | 0x20000;
      } else {
        k = i;
        if (file.isDirectory()) {
          k = i;
          if (hasInterestingFiles(file)) {
            String str = file.getName();
            if (DIRECTORY_MUSIC.equals(str)) {
              k = i | 0x1;
            } else if (DIRECTORY_PODCASTS.equals(str)) {
              k = i | 0x2;
            } else if (DIRECTORY_RINGTONES.equals(str)) {
              k = i | 0x4;
            } else if (DIRECTORY_ALARMS.equals(str)) {
              k = i | 0x8;
            } else if (DIRECTORY_NOTIFICATIONS.equals(str)) {
              k = i | 0x10;
            } else if (DIRECTORY_PICTURES.equals(str)) {
              k = i | 0x20;
            } else if (DIRECTORY_MOVIES.equals(str)) {
              k = i | 0x40;
            } else if (DIRECTORY_DOWNLOADS.equals(str)) {
              k = i | 0x80;
            } else if (DIRECTORY_DCIM.equals(str)) {
              k = i | 0x100;
            } else if (DIRECTORY_DOCUMENTS.equals(str)) {
              k = i | 0x200;
            } else if (DIRECTORY_AUDIOBOOKS.equals(str)) {
              k = i | 0x400;
            } else if ("Android".equals(str)) {
              k = i | 0x10000;
            } else {
              k = i | 0x20000;
            } 
          } 
        } 
      } 
      b++;
      i = k;
    } 
    return i;
  }
  
  private static boolean hasInterestingFiles(File paramFile) {
    LinkedList<File> linkedList = new LinkedList();
    linkedList.add(paramFile);
    while (true) {
      boolean bool = linkedList.isEmpty();
      byte b = 0;
      if (!bool) {
        paramFile = linkedList.pop();
        File[] arrayOfFile;
        int i;
        for (arrayOfFile = FileUtils.listFilesOrEmpty(paramFile), i = arrayOfFile.length; b < i; ) {
          paramFile = arrayOfFile[b];
          if (isInterestingFile(paramFile))
            return true; 
          if (paramFile.isDirectory())
            linkedList.add(paramFile); 
          b++;
        } 
        continue;
      } 
      break;
    } 
    return false;
  }
  
  private static boolean isInterestingFile(File paramFile) {
    if (paramFile.isFile()) {
      String str = paramFile.getName().toLowerCase();
      if (str.endsWith(".exe") || str.equals("autorun.inf") || 
        str.equals("launchpad.zip") || str.equals(".nomedia"))
        return false; 
      return true;
    } 
    return false;
  }
  
  @Deprecated
  public static File getExternalStoragePublicDirectory(String paramString) {
    throwIfUserRequired();
    return sCurrentUser.buildExternalStoragePublicDirs(paramString)[0];
  }
  
  public static File[] buildExternalStorageAndroidDataDirs() {
    throwIfUserRequired();
    return sCurrentUser.buildExternalStorageAndroidDataDirs();
  }
  
  public static File[] buildExternalStorageAndroidObbDirs() {
    throwIfUserRequired();
    return sCurrentUser.buildExternalStorageAndroidObbDirs();
  }
  
  public static File[] buildExternalStorageAppDataDirs(String paramString) {
    throwIfUserRequired();
    return sCurrentUser.buildExternalStorageAppDataDirs(paramString);
  }
  
  public static File[] buildExternalStorageAppMediaDirs(String paramString) {
    throwIfUserRequired();
    return sCurrentUser.buildExternalStorageAppMediaDirs(paramString);
  }
  
  public static File[] buildExternalStorageAppObbDirs(String paramString) {
    throwIfUserRequired();
    return sCurrentUser.buildExternalStorageAppObbDirs(paramString);
  }
  
  public static File[] buildExternalStorageAppFilesDirs(String paramString) {
    throwIfUserRequired();
    return sCurrentUser.buildExternalStorageAppFilesDirs(paramString);
  }
  
  public static File[] buildExternalStorageAppCacheDirs(String paramString) {
    throwIfUserRequired();
    return sCurrentUser.buildExternalStorageAppCacheDirs(paramString);
  }
  
  public static File[] buildExternalStoragePublicDirs(String paramString) {
    throwIfUserRequired();
    return sCurrentUser.buildExternalStoragePublicDirs(paramString);
  }
  
  public static File getDownloadCacheDirectory() {
    return DIR_DOWNLOAD_CACHE;
  }
  
  public static String getExternalStorageState() {
    File[] arrayOfFile = sCurrentUser.getExternalDirs();
    if (arrayOfFile == null || arrayOfFile.length == 0) {
      Log.e("environment", "getExternalStorageState files.length == 0 ");
      return "unknown";
    } 
    File file = arrayOfFile[0];
    return getExternalStorageState(file);
  }
  
  @Deprecated
  public static String getStorageState(File paramFile) {
    return getExternalStorageState(paramFile);
  }
  
  public static String getExternalStorageState(File paramFile) {
    StorageVolume storageVolume = StorageManager.getStorageVolume(paramFile, UserHandle.myUserId());
    if (storageVolume != null)
      return storageVolume.getState(); 
    return "unknown";
  }
  
  public static boolean isExternalStorageRemovable() {
    File file = sCurrentUser.getExternalDirs()[0];
    return isExternalStorageRemovable(file);
  }
  
  public static boolean isExternalStorageRemovable(File paramFile) {
    StorageVolume storageVolume = StorageManager.getStorageVolume(paramFile, UserHandle.myUserId());
    if (storageVolume != null)
      return storageVolume.isRemovable(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to find storage device at ");
    stringBuilder.append(paramFile);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static boolean isExternalStorageEmulated() {
    File file = sCurrentUser.getExternalDirs()[0];
    return isExternalStorageEmulated(file);
  }
  
  public static boolean isExternalStorageEmulated(File paramFile) {
    StorageVolume storageVolume = StorageManager.getStorageVolume(paramFile, UserHandle.myUserId());
    if (storageVolume != null)
      return storageVolume.isEmulated(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to find storage device at ");
    stringBuilder.append(paramFile);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static boolean isExternalStorageLegacy() {
    File file = sCurrentUser.getExternalDirs()[0];
    return isExternalStorageLegacy(file);
  }
  
  public static boolean isExternalStorageLegacy(File paramFile) {
    Application application = AppGlobals.getInitialApplication();
    int i = (application.getApplicationInfo()).uid;
    boolean bool1 = Process.isIsolated(i);
    boolean bool = false;
    if (bool1)
      return false; 
    PackageManager packageManager = application.getPackageManager();
    if (packageManager.isInstantApp())
      return false; 
    bool1 = Compatibility.isChangeEnabled(149924527L);
    boolean bool2 = Compatibility.isChangeEnabled(132649864L);
    if (isScopedStorageEnforced(bool1, bool2))
      return false; 
    if (isScopedStorageDisabled(bool1, bool2))
      return true; 
    AppOpsManager appOpsManager = (AppOpsManager)application.getSystemService(AppOpsManager.class);
    String str = application.getOpPackageName();
    if (appOpsManager.checkOpNoThrow(87, i, str) == 0)
      bool = true; 
    return bool;
  }
  
  public static boolean isExternalStorageManager() {
    File file = sCurrentUser.getExternalDirs()[0];
    return isExternalStorageManager(file);
  }
  
  public static boolean isExternalStorageManager(File paramFile) {
    Application application = AppGlobals.getInitialApplication();
    Objects.requireNonNull(application);
    Context context = (Context)application;
    String str1 = context.getPackageName();
    Objects.requireNonNull(str1);
    String str2 = str1;
    int i = (context.getApplicationInfo()).uid;
    AppOpsManager appOpsManager = (AppOpsManager)context.getSystemService(AppOpsManager.class);
    int j = appOpsManager.checkOpNoThrow(92, i, str2);
    boolean bool = true;
    if (j != 0) {
      if (j != 1 && j != 2) {
        if (j == 3) {
          j = Process.myPid();
          if (context.checkPermission("android.permission.MANAGE_EXTERNAL_STORAGE", j, i) != 0)
            bool = false; 
          return bool;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown AppOpsManager mode ");
        stringBuilder.append(j);
        throw new IllegalStateException(stringBuilder.toString());
      } 
      return false;
    } 
    return true;
  }
  
  private static boolean isScopedStorageEnforced(boolean paramBoolean1, boolean paramBoolean2) {
    if (paramBoolean1 && paramBoolean2) {
      paramBoolean1 = true;
    } else {
      paramBoolean1 = false;
    } 
    return paramBoolean1;
  }
  
  private static boolean isScopedStorageDisabled(boolean paramBoolean1, boolean paramBoolean2) {
    if (!paramBoolean1 && !paramBoolean2) {
      paramBoolean1 = true;
    } else {
      paramBoolean1 = false;
    } 
    return paramBoolean1;
  }
  
  static File getDirectory(String paramString1, String paramString2) {
    String str = System.getenv(paramString1);
    File file = new File();
    if (str == null) {
      this(paramString2);
    } else {
      this(str);
    } 
    return file;
  }
  
  public static void setUserRequired(boolean paramBoolean) {
    sUserRequired = paramBoolean;
  }
  
  private static void throwIfUserRequired() {
    if (sUserRequired)
      Log.e("Environment", "Path requests must specify a user by using UserEnvironment", new Throwable()); 
  }
  
  public static File[] buildPaths(File[] paramArrayOfFile, String... paramVarArgs) {
    File[] arrayOfFile = new File[paramArrayOfFile.length];
    for (byte b = 0; b < paramArrayOfFile.length; b++)
      arrayOfFile[b] = buildPath(paramArrayOfFile[b], paramVarArgs); 
    return arrayOfFile;
  }
  
  public static File buildPath(File paramFile, String... paramVarArgs) {
    int i;
    byte b;
    for (i = paramVarArgs.length, b = 0; b < i; ) {
      String str = paramVarArgs[b];
      if (paramFile == null) {
        paramFile = new File(str);
      } else {
        paramFile = new File(paramFile, str);
      } 
      b++;
    } 
    return paramFile;
  }
  
  @Deprecated
  public static File maybeTranslateEmulatedPathToInternal(File paramFile) {
    return paramFile;
  }
}
