package android.se.omapi;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Semaphore;

public final class SEService {
  public static interface OnConnectedListener {
    void onConnected();
  }
  
  class SEListener extends ISecureElementListener.Stub {
    final SEService this$0;
    
    private SEListener() {}
    
    public SEService.OnConnectedListener mListener = null;
    
    public Executor mExecutor = null;
    
    public IBinder asBinder() {
      return this;
    }
    
    public void onConnected() {
      if (this.mListener != null) {
        Executor executor = this.mExecutor;
        if (executor != null)
          executor.execute(new Runnable() {
                final SEService.SEListener this$1;
                
                public void run() {
                  this.this$1.mListener.onConnected();
                }
              }); 
      } 
    }
  }
  
  class null implements Runnable {
    final SEService.SEListener this$1;
    
    public void run() {
      this.this$1.mListener.onConnected();
    }
  }
  
  private SEListener mSEListener = new SEListener();
  
  private final Object mLock = new Object();
  
  private final HashMap<String, Reader> mReaders = new HashMap<>();
  
  public static final int IO_ERROR = 1;
  
  public static final int NO_SUCH_ELEMENT_ERROR = 2;
  
  private static final String TAG = "OMAPI.SEService";
  
  private static final String UICC_TERMINAL = "SIM";
  
  private ServiceConnection mConnection;
  
  private final Context mContext;
  
  private volatile ISecureElementService mSecureElementService;
  
  public SEService(Context paramContext, Executor paramExecutor, OnConnectedListener paramOnConnectedListener) {
    if (paramContext != null && paramOnConnectedListener != null && paramExecutor != null) {
      this.mContext = paramContext;
      this.mSEListener.mListener = paramOnConnectedListener;
      this.mSEListener.mExecutor = paramExecutor;
      Semaphore semaphore = new Semaphore(1, true);
      this.mConnection = (ServiceConnection)new Object(this, semaphore);
      try {
        semaphore.acquire();
      } catch (Exception exception) {
        Log.e("OMAPI.SEService", exception.toString());
      } 
      Intent intent = new Intent(ISecureElementService.class.getName());
      intent.setClassName("com.android.se", "com.android.se.SecureElementService");
      Context context = this.mContext;
      ServiceConnection serviceConnection = this.mConnection;
      boolean bool = context.bindService(intent, serviceConnection, 1);
      if (bool)
        Log.i("OMAPI.SEService", "bindService successful"); 
      semaphore.release();
      return;
    } 
    throw new NullPointerException("Arguments must not be null");
  }
  
  public boolean isConnected() {
    boolean bool;
    if (this.mSecureElementService != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Reader[] getReaders() {
    loadReaders();
    return (Reader[])this.mReaders.values().toArray((Object[])new Reader[0]);
  }
  
  public Reader getUiccReader(int paramInt) {
    if (paramInt >= 1) {
      loadReaders();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("SIM");
      stringBuilder1.append(paramInt);
      String str = stringBuilder1.toString();
      Reader reader = this.mReaders.get(str);
      if (reader != null)
        return reader; 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Reader:");
      stringBuilder2.append(str);
      stringBuilder2.append(" doesn't exist");
      throw new IllegalArgumentException(stringBuilder2.toString());
    } 
    throw new IllegalArgumentException("slotNumber should be larger than 0");
  }
  
  public void shutdown() {
    synchronized (this.mLock) {
      if (this.mSecureElementService != null)
        for (Reader reader : this.mReaders.values()) {
          try {
            reader.closeSessions();
          } catch (Exception exception) {}
        }  
      try {
        this.mContext.unbindService(this.mConnection);
      } catch (IllegalArgumentException illegalArgumentException) {}
      this.mSecureElementService = null;
      return;
    } 
  }
  
  public String getVersion() {
    return "3.3";
  }
  
  ISecureElementListener getListener() {
    return this.mSEListener;
  }
  
  private ISecureElementReader getReader(String paramString) {
    try {
      return this.mSecureElementService.getReader(paramString);
    } catch (RemoteException remoteException) {
      throw new IllegalStateException(remoteException.getMessage());
    } 
  }
  
  private void loadReaders() {
    if (this.mSecureElementService != null)
      try {
        for (String str : this.mSecureElementService.getReaders()) {
          if (this.mReaders.get(str) == null)
            try {
              HashMap<String, Reader> hashMap = this.mReaders;
              Reader reader = new Reader();
              this(this, str, getReader(str));
              hashMap.put(str, reader);
            } catch (Exception exception) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Error adding Reader: ");
              stringBuilder.append(str);
              Log.e("OMAPI.SEService", stringBuilder.toString(), exception);
            }  
        } 
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      }  
    throw new IllegalStateException("service not connected to system");
  }
}
