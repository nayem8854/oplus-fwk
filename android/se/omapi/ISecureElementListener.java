package android.se.omapi;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISecureElementListener extends IInterface {
  class Default implements ISecureElementListener {
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISecureElementListener {
    private static final String DESCRIPTOR = "android.se.omapi.ISecureElementListener";
    
    public Stub() {
      attachInterface(this, "android.se.omapi.ISecureElementListener");
    }
    
    public static ISecureElementListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.se.omapi.ISecureElementListener");
      if (iInterface != null && iInterface instanceof ISecureElementListener)
        return (ISecureElementListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      return null;
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902)
        return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
      param1Parcel2.writeString("android.se.omapi.ISecureElementListener");
      return true;
    }
    
    private static class Proxy implements ISecureElementListener {
      public static ISecureElementListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.se.omapi.ISecureElementListener";
      }
    }
    
    public static boolean setDefaultImpl(ISecureElementListener param1ISecureElementListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISecureElementListener != null) {
          Proxy.sDefaultImpl = param1ISecureElementListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISecureElementListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
