package android.se.omapi;

import android.os.RemoteException;
import android.os.ServiceSpecificException;
import android.util.Log;
import java.io.IOException;
import java.nio.channels.Channel;

public final class Channel implements Channel {
  private static final String TAG = "OMAPI.Channel";
  
  private final ISecureElementChannel mChannel;
  
  private final Object mLock = new Object();
  
  private final SEService mService;
  
  private Session mSession;
  
  Channel(SEService paramSEService, Session paramSession, ISecureElementChannel paramISecureElementChannel) {
    if (paramSEService != null && paramSession != null && paramISecureElementChannel != null) {
      this.mService = paramSEService;
      this.mSession = paramSession;
      this.mChannel = paramISecureElementChannel;
      return;
    } 
    throw new IllegalArgumentException("Parameters cannot be null");
  }
  
  public void close() {
    if (isOpen()) {
      Object object = this.mLock;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      try {
        this.mChannel.close();
      } catch (Exception exception) {
        Log.e("OMAPI.Channel", "Error closing channel", exception);
      } finally {
        Exception exception;
      } 
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    } 
  }
  
  public boolean isOpen() {
    if (!this.mService.isConnected()) {
      Log.e("OMAPI.Channel", "service not connected to system");
      return false;
    } 
    try {
      boolean bool = this.mChannel.isClosed();
      return bool ^ true;
    } catch (RemoteException remoteException) {
      Log.e("OMAPI.Channel", "Exception in isClosed()");
      return false;
    } 
  }
  
  public boolean isBasicChannel() {
    if (this.mService.isConnected())
      try {
        return this.mChannel.isBasicChannel();
      } catch (RemoteException remoteException) {
        throw new IllegalStateException(remoteException.getMessage());
      }  
    throw new IllegalStateException("service not connected to system");
  }
  
  public byte[] transmit(byte[] paramArrayOfbyte) throws IOException {
    if (this.mService.isConnected()) {
      Object object = this.mLock;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      try {
        paramArrayOfbyte = this.mChannel.transmit(paramArrayOfbyte);
        if (paramArrayOfbyte != null) {
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return paramArrayOfbyte;
        } 
        IOException iOException = new IOException();
        this("Error in communicating with Secure Element");
        throw iOException;
      } catch (ServiceSpecificException serviceSpecificException) {
        IOException iOException = new IOException();
        this(serviceSpecificException.getMessage());
        throw iOException;
      } catch (RemoteException remoteException) {
        IllegalStateException illegalStateException = new IllegalStateException();
        this(remoteException.getMessage());
        throw illegalStateException;
      } finally {}
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      throw paramArrayOfbyte;
    } 
    throw new IllegalStateException("service not connected to system");
  }
  
  public Session getSession() {
    return this.mSession;
  }
  
  public byte[] getSelectResponse() {
    if (this.mService.isConnected())
      try {
        byte[] arrayOfByte1 = this.mChannel.getSelectResponse();
        byte[] arrayOfByte2 = arrayOfByte1;
        if (arrayOfByte1 != null) {
          arrayOfByte2 = arrayOfByte1;
          if (arrayOfByte1.length == 0)
            arrayOfByte2 = null; 
        } 
        return arrayOfByte2;
      } catch (RemoteException remoteException) {
        throw new IllegalStateException(remoteException.getMessage());
      }  
    throw new IllegalStateException("service not connected to system");
  }
  
  public boolean selectNext() throws IOException {
    if (this.mService.isConnected())
      try {
        synchronized (this.mLock) {
          return this.mChannel.selectNext();
        } 
      } catch (ServiceSpecificException serviceSpecificException) {
        throw new IOException(serviceSpecificException.getMessage());
      } catch (RemoteException remoteException) {
        throw new IllegalStateException(remoteException.getMessage());
      }  
    throw new IllegalStateException("service not connected to system");
  }
}
