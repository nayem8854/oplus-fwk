package android.se.omapi;

import android.os.RemoteException;
import android.os.ServiceSpecificException;
import android.util.Log;
import java.io.IOException;
import java.util.NoSuchElementException;

public final class Session {
  private static final String TAG = "OMAPI.Session";
  
  private final Object mLock = new Object();
  
  private final Reader mReader;
  
  private final SEService mService;
  
  private final ISecureElementSession mSession;
  
  Session(SEService paramSEService, ISecureElementSession paramISecureElementSession, Reader paramReader) {
    if (paramSEService != null && paramReader != null && paramISecureElementSession != null) {
      this.mService = paramSEService;
      this.mReader = paramReader;
      this.mSession = paramISecureElementSession;
      return;
    } 
    throw new IllegalArgumentException("Parameters cannot be null");
  }
  
  public Reader getReader() {
    return this.mReader;
  }
  
  public byte[] getATR() {
    if (this.mService.isConnected())
      try {
        return this.mSession.getAtr();
      } catch (RemoteException remoteException) {
        throw new IllegalStateException(remoteException.getMessage());
      }  
    throw new IllegalStateException("service not connected to system");
  }
  
  public void close() {
    if (!this.mService.isConnected()) {
      Log.e("OMAPI.Session", "service not connected to system");
      return;
    } 
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      this.mSession.close();
    } catch (RemoteException remoteException) {
      Log.e("OMAPI.Session", "Error closing session", (Throwable)remoteException);
    } finally {
      Exception exception;
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  public boolean isClosed() {
    try {
      return this.mSession.isClosed();
    } catch (RemoteException remoteException) {
      return true;
    } 
  }
  
  public void closeChannels() {
    if (!this.mService.isConnected()) {
      Log.e("OMAPI.Session", "service not connected to system");
      return;
    } 
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      this.mSession.closeChannels();
    } catch (RemoteException remoteException) {
      Log.e("OMAPI.Session", "Error closing channels", (Throwable)remoteException);
    } finally {
      Exception exception;
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  public Channel openBasicChannel(byte[] paramArrayOfbyte, byte paramByte) throws IOException {
    if (this.mService.isConnected()) {
      Object object = this.mLock;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      try {
        ISecureElementSession iSecureElementSession = this.mSession;
        Reader reader = this.mReader;
        ISecureElementListener iSecureElementListener = reader.getSEService().getListener();
        ISecureElementChannel iSecureElementChannel = iSecureElementSession.openBasicChannel(paramArrayOfbyte, paramByte, iSecureElementListener);
        if (iSecureElementChannel == null) {
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return null;
        } 
        Channel channel = new Channel();
        this(this.mService, this, iSecureElementChannel);
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        return channel;
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode != 1) {
          if (serviceSpecificException.errorCode == 2) {
            NoSuchElementException noSuchElementException = new NoSuchElementException();
            this(serviceSpecificException.getMessage());
            throw noSuchElementException;
          } 
          IllegalStateException illegalStateException = new IllegalStateException();
          this(serviceSpecificException.getMessage());
          throw illegalStateException;
        } 
        IOException iOException = new IOException();
        this(serviceSpecificException.getMessage());
        throw iOException;
      } catch (RemoteException remoteException) {
        IllegalStateException illegalStateException = new IllegalStateException();
        this(remoteException.getMessage());
        throw illegalStateException;
      } finally {}
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      throw paramArrayOfbyte;
    } 
    throw new IllegalStateException("service not connected to system");
  }
  
  public Channel openBasicChannel(byte[] paramArrayOfbyte) throws IOException {
    return openBasicChannel(paramArrayOfbyte, (byte)0);
  }
  
  public Channel openLogicalChannel(byte[] paramArrayOfbyte, byte paramByte) throws IOException {
    if (this.mService.isConnected()) {
      Object object = this.mLock;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      try {
        ISecureElementSession iSecureElementSession = this.mSession;
        Reader reader = this.mReader;
        ISecureElementListener iSecureElementListener = reader.getSEService().getListener();
        ISecureElementChannel iSecureElementChannel = iSecureElementSession.openLogicalChannel(paramArrayOfbyte, paramByte, iSecureElementListener);
        if (iSecureElementChannel == null) {
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return null;
        } 
        Channel channel = new Channel();
        this(this.mService, this, iSecureElementChannel);
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        return channel;
      } catch (ServiceSpecificException serviceSpecificException) {
        if (serviceSpecificException.errorCode != 1) {
          if (serviceSpecificException.errorCode == 2) {
            NoSuchElementException noSuchElementException = new NoSuchElementException();
            this(serviceSpecificException.getMessage());
            throw noSuchElementException;
          } 
          IllegalStateException illegalStateException = new IllegalStateException();
          this(serviceSpecificException.getMessage());
          throw illegalStateException;
        } 
        IOException iOException = new IOException();
        this(serviceSpecificException.getMessage());
        throw iOException;
      } catch (RemoteException remoteException) {
        IllegalStateException illegalStateException = new IllegalStateException();
        this(remoteException.getMessage());
        throw illegalStateException;
      } finally {}
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      throw paramArrayOfbyte;
    } 
    throw new IllegalStateException("service not connected to system");
  }
  
  public Channel openLogicalChannel(byte[] paramArrayOfbyte) throws IOException {
    return openLogicalChannel(paramArrayOfbyte, (byte)0);
  }
}
