package android.se.omapi;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISecureElementChannel extends IInterface {
  void close() throws RemoteException;
  
  byte[] getSelectResponse() throws RemoteException;
  
  boolean isBasicChannel() throws RemoteException;
  
  boolean isClosed() throws RemoteException;
  
  boolean selectNext() throws RemoteException;
  
  byte[] transmit(byte[] paramArrayOfbyte) throws RemoteException;
  
  class Default implements ISecureElementChannel {
    public void close() throws RemoteException {}
    
    public boolean isClosed() throws RemoteException {
      return false;
    }
    
    public boolean isBasicChannel() throws RemoteException {
      return false;
    }
    
    public byte[] getSelectResponse() throws RemoteException {
      return null;
    }
    
    public byte[] transmit(byte[] param1ArrayOfbyte) throws RemoteException {
      return null;
    }
    
    public boolean selectNext() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISecureElementChannel {
    private static final String DESCRIPTOR = "android.se.omapi.ISecureElementChannel";
    
    static final int TRANSACTION_close = 1;
    
    static final int TRANSACTION_getSelectResponse = 4;
    
    static final int TRANSACTION_isBasicChannel = 3;
    
    static final int TRANSACTION_isClosed = 2;
    
    static final int TRANSACTION_selectNext = 6;
    
    static final int TRANSACTION_transmit = 5;
    
    public Stub() {
      attachInterface(this, "android.se.omapi.ISecureElementChannel");
    }
    
    public static ISecureElementChannel asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.se.omapi.ISecureElementChannel");
      if (iInterface != null && iInterface instanceof ISecureElementChannel)
        return (ISecureElementChannel)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "selectNext";
        case 5:
          return "transmit";
        case 4:
          return "getSelectResponse";
        case 3:
          return "isBasicChannel";
        case 2:
          return "isClosed";
        case 1:
          break;
      } 
      return "close";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        byte[] arrayOfByte;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.se.omapi.ISecureElementChannel");
            bool = selectNext();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.se.omapi.ISecureElementChannel");
            arrayOfByte = param1Parcel1.createByteArray();
            arrayOfByte = transmit(arrayOfByte);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte);
            return true;
          case 4:
            arrayOfByte.enforceInterface("android.se.omapi.ISecureElementChannel");
            arrayOfByte = getSelectResponse();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte);
            return true;
          case 3:
            arrayOfByte.enforceInterface("android.se.omapi.ISecureElementChannel");
            bool = isBasicChannel();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 2:
            arrayOfByte.enforceInterface("android.se.omapi.ISecureElementChannel");
            bool = isClosed();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 1:
            break;
        } 
        arrayOfByte.enforceInterface("android.se.omapi.ISecureElementChannel");
        close();
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.se.omapi.ISecureElementChannel");
      return true;
    }
    
    private static class Proxy implements ISecureElementChannel {
      public static ISecureElementChannel sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.se.omapi.ISecureElementChannel";
      }
      
      public void close() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementChannel");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISecureElementChannel.Stub.getDefaultImpl() != null) {
            ISecureElementChannel.Stub.getDefaultImpl().close();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isClosed() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementChannel");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && ISecureElementChannel.Stub.getDefaultImpl() != null) {
            bool1 = ISecureElementChannel.Stub.getDefaultImpl().isClosed();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isBasicChannel() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementChannel");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && ISecureElementChannel.Stub.getDefaultImpl() != null) {
            bool1 = ISecureElementChannel.Stub.getDefaultImpl().isBasicChannel();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getSelectResponse() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementChannel");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ISecureElementChannel.Stub.getDefaultImpl() != null)
            return ISecureElementChannel.Stub.getDefaultImpl().getSelectResponse(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] transmit(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementChannel");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ISecureElementChannel.Stub.getDefaultImpl() != null) {
            param2ArrayOfbyte = ISecureElementChannel.Stub.getDefaultImpl().transmit(param2ArrayOfbyte);
            return param2ArrayOfbyte;
          } 
          parcel2.readException();
          param2ArrayOfbyte = parcel2.createByteArray();
          return param2ArrayOfbyte;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean selectNext() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementChannel");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && ISecureElementChannel.Stub.getDefaultImpl() != null) {
            bool1 = ISecureElementChannel.Stub.getDefaultImpl().selectNext();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISecureElementChannel param1ISecureElementChannel) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISecureElementChannel != null) {
          Proxy.sDefaultImpl = param1ISecureElementChannel;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISecureElementChannel getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
