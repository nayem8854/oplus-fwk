package android.se.omapi;

import android.annotation.SystemApi;
import android.os.RemoteException;
import android.os.ServiceSpecificException;
import android.util.Log;
import java.io.IOException;

public final class Reader {
  private static final String TAG = "OMAPI.Reader";
  
  private final Object mLock = new Object();
  
  private final String mName;
  
  private ISecureElementReader mReader;
  
  private final SEService mService;
  
  Reader(SEService paramSEService, String paramString, ISecureElementReader paramISecureElementReader) {
    if (paramISecureElementReader != null && paramSEService != null && paramString != null) {
      this.mName = paramString;
      this.mService = paramSEService;
      this.mReader = paramISecureElementReader;
      return;
    } 
    throw new IllegalArgumentException("Parameters cannot be null");
  }
  
  public String getName() {
    return this.mName;
  }
  
  public Session openSession() throws IOException {
    if (this.mService.isConnected()) {
      Exception exception;
      Object object = this.mLock;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      try {
        ISecureElementSession iSecureElementSession = this.mReader.openSession();
        if (iSecureElementSession != null) {
          Session session = new Session();
          this(this.mService, iSecureElementSession, this);
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return session;
        } 
        exception = new IOException();
        this("service session is null.");
        throw exception;
      } catch (ServiceSpecificException null) {
        IOException iOException = new IOException();
        this(exception.getMessage());
        throw iOException;
      } catch (RemoteException remoteException) {
        IllegalStateException illegalStateException = new IllegalStateException();
        this(remoteException.getMessage());
        throw illegalStateException;
      } finally {}
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      throw exception;
    } 
    throw new IllegalStateException("service is not connected");
  }
  
  public boolean isSecureElementPresent() {
    if (this.mService.isConnected())
      try {
        return this.mReader.isSecureElementPresent();
      } catch (RemoteException remoteException) {
        throw new IllegalStateException("Error in isSecureElementPresent()");
      }  
    throw new IllegalStateException("service is not connected");
  }
  
  public SEService getSEService() {
    return this.mService;
  }
  
  public void closeSessions() {
    if (!this.mService.isConnected()) {
      Log.e("OMAPI.Reader", "service is not connected");
      return;
    } 
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      this.mReader.closeSessions();
    } catch (RemoteException remoteException) {
    
    } finally {
      Exception exception;
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  @SystemApi
  public boolean reset() {
    Exception exception;
    if (!this.mService.isConnected()) {
      Log.e("OMAPI.Reader", "service is not connected");
      return false;
    } 
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      closeSessions();
      boolean bool = this.mReader.reset();
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      return bool;
    } catch (RemoteException remoteException) {
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      return false;
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    throw exception;
  }
}
