package android.se.omapi;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISecureElementReader extends IInterface {
  void closeSessions() throws RemoteException;
  
  boolean isSecureElementPresent() throws RemoteException;
  
  ISecureElementSession openSession() throws RemoteException;
  
  boolean reset() throws RemoteException;
  
  class Default implements ISecureElementReader {
    public boolean isSecureElementPresent() throws RemoteException {
      return false;
    }
    
    public ISecureElementSession openSession() throws RemoteException {
      return null;
    }
    
    public void closeSessions() throws RemoteException {}
    
    public boolean reset() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISecureElementReader {
    private static final String DESCRIPTOR = "android.se.omapi.ISecureElementReader";
    
    static final int TRANSACTION_closeSessions = 3;
    
    static final int TRANSACTION_isSecureElementPresent = 1;
    
    static final int TRANSACTION_openSession = 2;
    
    static final int TRANSACTION_reset = 4;
    
    public Stub() {
      attachInterface(this, "android.se.omapi.ISecureElementReader");
    }
    
    public static ISecureElementReader asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.se.omapi.ISecureElementReader");
      if (iInterface != null && iInterface instanceof ISecureElementReader)
        return (ISecureElementReader)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "reset";
          } 
          return "closeSessions";
        } 
        return "openSession";
      } 
      return "isSecureElementPresent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ISecureElementSession iSecureElementSession;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.se.omapi.ISecureElementReader");
              return true;
            } 
            param1Parcel1.enforceInterface("android.se.omapi.ISecureElementReader");
            boolean bool1 = reset();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.se.omapi.ISecureElementReader");
          closeSessions();
          param1Parcel2.writeNoException();
          return true;
        } 
        param1Parcel1.enforceInterface("android.se.omapi.ISecureElementReader");
        iSecureElementSession = openSession();
        param1Parcel2.writeNoException();
        if (iSecureElementSession != null) {
          IBinder iBinder = iSecureElementSession.asBinder();
        } else {
          iSecureElementSession = null;
        } 
        param1Parcel2.writeStrongBinder((IBinder)iSecureElementSession);
        return true;
      } 
      iSecureElementSession.enforceInterface("android.se.omapi.ISecureElementReader");
      boolean bool = isSecureElementPresent();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements ISecureElementReader {
      public static ISecureElementReader sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.se.omapi.ISecureElementReader";
      }
      
      public boolean isSecureElementPresent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementReader");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && ISecureElementReader.Stub.getDefaultImpl() != null) {
            bool1 = ISecureElementReader.Stub.getDefaultImpl().isSecureElementPresent();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ISecureElementSession openSession() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementReader");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISecureElementReader.Stub.getDefaultImpl() != null)
            return ISecureElementReader.Stub.getDefaultImpl().openSession(); 
          parcel2.readException();
          return ISecureElementSession.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void closeSessions() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementReader");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ISecureElementReader.Stub.getDefaultImpl() != null) {
            ISecureElementReader.Stub.getDefaultImpl().closeSessions();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean reset() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementReader");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && ISecureElementReader.Stub.getDefaultImpl() != null) {
            bool1 = ISecureElementReader.Stub.getDefaultImpl().reset();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISecureElementReader param1ISecureElementReader) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISecureElementReader != null) {
          Proxy.sDefaultImpl = param1ISecureElementReader;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISecureElementReader getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
