package android.se.omapi;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISecureElementSession extends IInterface {
  void close() throws RemoteException;
  
  void closeChannels() throws RemoteException;
  
  byte[] getAtr() throws RemoteException;
  
  boolean isClosed() throws RemoteException;
  
  ISecureElementChannel openBasicChannel(byte[] paramArrayOfbyte, byte paramByte, ISecureElementListener paramISecureElementListener) throws RemoteException;
  
  ISecureElementChannel openLogicalChannel(byte[] paramArrayOfbyte, byte paramByte, ISecureElementListener paramISecureElementListener) throws RemoteException;
  
  class Default implements ISecureElementSession {
    public byte[] getAtr() throws RemoteException {
      return null;
    }
    
    public void close() throws RemoteException {}
    
    public void closeChannels() throws RemoteException {}
    
    public boolean isClosed() throws RemoteException {
      return false;
    }
    
    public ISecureElementChannel openBasicChannel(byte[] param1ArrayOfbyte, byte param1Byte, ISecureElementListener param1ISecureElementListener) throws RemoteException {
      return null;
    }
    
    public ISecureElementChannel openLogicalChannel(byte[] param1ArrayOfbyte, byte param1Byte, ISecureElementListener param1ISecureElementListener) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISecureElementSession {
    private static final String DESCRIPTOR = "android.se.omapi.ISecureElementSession";
    
    static final int TRANSACTION_close = 2;
    
    static final int TRANSACTION_closeChannels = 3;
    
    static final int TRANSACTION_getAtr = 1;
    
    static final int TRANSACTION_isClosed = 4;
    
    static final int TRANSACTION_openBasicChannel = 5;
    
    static final int TRANSACTION_openLogicalChannel = 6;
    
    public Stub() {
      attachInterface(this, "android.se.omapi.ISecureElementSession");
    }
    
    public static ISecureElementSession asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.se.omapi.ISecureElementSession");
      if (iInterface != null && iInterface instanceof ISecureElementSession)
        return (ISecureElementSession)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "openLogicalChannel";
        case 5:
          return "openBasicChannel";
        case 4:
          return "isClosed";
        case 3:
          return "closeChannels";
        case 2:
          return "close";
        case 1:
          break;
      } 
      return "getAtr";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        ISecureElementListener iSecureElementListener2;
        IBinder iBinder2;
        ISecureElementListener iSecureElementListener1;
        ISecureElementChannel iSecureElementChannel1;
        IBinder iBinder1;
        ISecureElementChannel iSecureElementChannel2;
        byte[] arrayOfByte3;
        ISecureElementChannel iSecureElementChannel3;
        byte b, arrayOfByte2[] = null;
        ISecureElementListener iSecureElementListener3 = null;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.se.omapi.ISecureElementSession");
            arrayOfByte2 = param1Parcel1.createByteArray();
            b = param1Parcel1.readByte();
            iSecureElementListener2 = ISecureElementListener.Stub.asInterface(param1Parcel1.readStrongBinder());
            iSecureElementChannel2 = openLogicalChannel(arrayOfByte2, b, iSecureElementListener2);
            param1Parcel2.writeNoException();
            iSecureElementListener2 = iSecureElementListener3;
            if (iSecureElementChannel2 != null)
              iBinder2 = iSecureElementChannel2.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder2);
            return true;
          case 5:
            iBinder2.enforceInterface("android.se.omapi.ISecureElementSession");
            arrayOfByte3 = iBinder2.createByteArray();
            b = iBinder2.readByte();
            iSecureElementListener1 = ISecureElementListener.Stub.asInterface(iBinder2.readStrongBinder());
            iSecureElementChannel3 = openBasicChannel(arrayOfByte3, b, iSecureElementListener1);
            param1Parcel2.writeNoException();
            iSecureElementChannel1 = iSecureElementChannel2;
            if (iSecureElementChannel3 != null)
              iBinder1 = iSecureElementChannel3.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder1);
            return true;
          case 4:
            iBinder1.enforceInterface("android.se.omapi.ISecureElementSession");
            bool = isClosed();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 3:
            iBinder1.enforceInterface("android.se.omapi.ISecureElementSession");
            closeChannels();
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iBinder1.enforceInterface("android.se.omapi.ISecureElementSession");
            close();
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iBinder1.enforceInterface("android.se.omapi.ISecureElementSession");
        byte[] arrayOfByte1 = getAtr();
        param1Parcel2.writeNoException();
        param1Parcel2.writeByteArray(arrayOfByte1);
        return true;
      } 
      param1Parcel2.writeString("android.se.omapi.ISecureElementSession");
      return true;
    }
    
    private static class Proxy implements ISecureElementSession {
      public static ISecureElementSession sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.se.omapi.ISecureElementSession";
      }
      
      public byte[] getAtr() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementSession");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISecureElementSession.Stub.getDefaultImpl() != null)
            return ISecureElementSession.Stub.getDefaultImpl().getAtr(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void close() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementSession");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISecureElementSession.Stub.getDefaultImpl() != null) {
            ISecureElementSession.Stub.getDefaultImpl().close();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void closeChannels() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementSession");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ISecureElementSession.Stub.getDefaultImpl() != null) {
            ISecureElementSession.Stub.getDefaultImpl().closeChannels();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isClosed() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementSession");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && ISecureElementSession.Stub.getDefaultImpl() != null) {
            bool1 = ISecureElementSession.Stub.getDefaultImpl().isClosed();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ISecureElementChannel openBasicChannel(byte[] param2ArrayOfbyte, byte param2Byte, ISecureElementListener param2ISecureElementListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementSession");
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeByte(param2Byte);
          if (param2ISecureElementListener != null) {
            iBinder = param2ISecureElementListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ISecureElementSession.Stub.getDefaultImpl() != null)
            return ISecureElementSession.Stub.getDefaultImpl().openBasicChannel(param2ArrayOfbyte, param2Byte, param2ISecureElementListener); 
          parcel2.readException();
          return ISecureElementChannel.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ISecureElementChannel openLogicalChannel(byte[] param2ArrayOfbyte, byte param2Byte, ISecureElementListener param2ISecureElementListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementSession");
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeByte(param2Byte);
          if (param2ISecureElementListener != null) {
            iBinder = param2ISecureElementListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ISecureElementSession.Stub.getDefaultImpl() != null)
            return ISecureElementSession.Stub.getDefaultImpl().openLogicalChannel(param2ArrayOfbyte, param2Byte, param2ISecureElementListener); 
          parcel2.readException();
          return ISecureElementChannel.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISecureElementSession param1ISecureElementSession) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISecureElementSession != null) {
          Proxy.sDefaultImpl = param1ISecureElementSession;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISecureElementSession getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
