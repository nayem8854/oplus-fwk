package android.se.omapi;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISecureElementService extends IInterface {
  ISecureElementReader getReader(String paramString) throws RemoteException;
  
  String[] getReaders() throws RemoteException;
  
  boolean[] isNFCEventAllowed(String paramString, byte[] paramArrayOfbyte, String[] paramArrayOfString) throws RemoteException;
  
  class Default implements ISecureElementService {
    public String[] getReaders() throws RemoteException {
      return null;
    }
    
    public ISecureElementReader getReader(String param1String) throws RemoteException {
      return null;
    }
    
    public boolean[] isNFCEventAllowed(String param1String, byte[] param1ArrayOfbyte, String[] param1ArrayOfString) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISecureElementService {
    private static final String DESCRIPTOR = "android.se.omapi.ISecureElementService";
    
    static final int TRANSACTION_getReader = 2;
    
    static final int TRANSACTION_getReaders = 1;
    
    static final int TRANSACTION_isNFCEventAllowed = 3;
    
    public Stub() {
      attachInterface(this, "android.se.omapi.ISecureElementService");
    }
    
    public static ISecureElementService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.se.omapi.ISecureElementService");
      if (iInterface != null && iInterface instanceof ISecureElementService)
        return (ISecureElementService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "isNFCEventAllowed";
        } 
        return "getReader";
      } 
      return "getReaders";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ISecureElementReader iSecureElementReader;
      if (param1Int1 != 1) {
        boolean[] arrayOfBoolean;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.se.omapi.ISecureElementService");
            return true;
          } 
          param1Parcel1.enforceInterface("android.se.omapi.ISecureElementService");
          String str1 = param1Parcel1.readString();
          byte[] arrayOfByte = param1Parcel1.createByteArray();
          String[] arrayOfString1 = param1Parcel1.createStringArray();
          arrayOfBoolean = isNFCEventAllowed(str1, arrayOfByte, arrayOfString1);
          param1Parcel2.writeNoException();
          param1Parcel2.writeBooleanArray(arrayOfBoolean);
          return true;
        } 
        arrayOfBoolean.enforceInterface("android.se.omapi.ISecureElementService");
        String str = arrayOfBoolean.readString();
        iSecureElementReader = getReader(str);
        param1Parcel2.writeNoException();
        if (iSecureElementReader != null) {
          IBinder iBinder = iSecureElementReader.asBinder();
        } else {
          iSecureElementReader = null;
        } 
        param1Parcel2.writeStrongBinder((IBinder)iSecureElementReader);
        return true;
      } 
      iSecureElementReader.enforceInterface("android.se.omapi.ISecureElementService");
      String[] arrayOfString = getReaders();
      param1Parcel2.writeNoException();
      param1Parcel2.writeStringArray(arrayOfString);
      return true;
    }
    
    private static class Proxy implements ISecureElementService {
      public static ISecureElementService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.se.omapi.ISecureElementService";
      }
      
      public String[] getReaders() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISecureElementService.Stub.getDefaultImpl() != null)
            return ISecureElementService.Stub.getDefaultImpl().getReaders(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ISecureElementReader getReader(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISecureElementService.Stub.getDefaultImpl() != null)
            return ISecureElementService.Stub.getDefaultImpl().getReader(param2String); 
          parcel2.readException();
          return ISecureElementReader.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean[] isNFCEventAllowed(String param2String, byte[] param2ArrayOfbyte, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.se.omapi.ISecureElementService");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ISecureElementService.Stub.getDefaultImpl() != null)
            return ISecureElementService.Stub.getDefaultImpl().isNFCEventAllowed(param2String, param2ArrayOfbyte, param2ArrayOfString); 
          parcel2.readException();
          return parcel2.createBooleanArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISecureElementService param1ISecureElementService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISecureElementService != null) {
          Proxy.sDefaultImpl = param1ISecureElementService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISecureElementService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
