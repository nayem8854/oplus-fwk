package android.companion;

import android.annotation.NonNull;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.AnnotationValidations;
import java.util.Objects;

public class Association implements Parcelable {
  public Association(int paramInt, String paramString1, String paramString2) {
    this.userId = paramInt;
    this.deviceAddress = paramString1;
    AnnotationValidations.validate(NonNull.class, null, paramString1);
    this.companionAppPackage = paramString2;
    AnnotationValidations.validate(NonNull.class, null, paramString2);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Association { userId = ");
    stringBuilder.append(this.userId);
    stringBuilder.append(", deviceAddress = ");
    stringBuilder.append(this.deviceAddress);
    stringBuilder.append(", companionAppPackage = ");
    stringBuilder.append(this.companionAppPackage);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.userId == ((Association)paramObject).userId) {
      String str1 = this.deviceAddress, str2 = ((Association)paramObject).deviceAddress;
      if (Objects.equals(str1, str2)) {
        str2 = this.companionAppPackage;
        paramObject = ((Association)paramObject).companionAppPackage;
        if (Objects.equals(str2, paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = this.userId;
    int j = Objects.hashCode(this.deviceAddress);
    int k = Objects.hashCode(this.companionAppPackage);
    return ((1 * 31 + i) * 31 + j) * 31 + k;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.userId);
    paramParcel.writeString(this.deviceAddress);
    paramParcel.writeString(this.companionAppPackage);
  }
  
  public int describeContents() {
    return 0;
  }
  
  protected Association(Parcel paramParcel) {
    int i = paramParcel.readInt();
    String str2 = paramParcel.readString();
    String str1 = paramParcel.readString();
    this.userId = i;
    this.deviceAddress = str2;
    AnnotationValidations.validate(NonNull.class, null, str2);
    this.companionAppPackage = str1;
    AnnotationValidations.validate(NonNull.class, null, str1);
  }
  
  public static final Parcelable.Creator<Association> CREATOR = new Parcelable.Creator<Association>() {
      public Association[] newArray(int param1Int) {
        return new Association[param1Int];
      }
      
      public Association createFromParcel(Parcel param1Parcel) {
        return new Association(param1Parcel);
      }
    };
  
  public final String companionAppPackage;
  
  public final String deviceAddress;
  
  public final int userId;
  
  @Deprecated
  private void __metadata() {}
}
