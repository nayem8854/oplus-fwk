package android.companion;

import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.OneTimeUseBuilder;
import android.text.TextUtils;
import com.android.internal.util.ObjectUtils;
import com.android.internal.util.Preconditions;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Pattern;
import libcore.util.HexEncoding;

public final class BluetoothLeDeviceFilter implements DeviceFilter<ScanResult> {
  private BluetoothLeDeviceFilter(Pattern paramPattern, ScanFilter paramScanFilter, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean) {
    this.mNamePattern = paramPattern;
    this.mScanFilter = (ScanFilter)ObjectUtils.firstNotNull(paramScanFilter, ScanFilter.EMPTY);
    this.mRawDataFilter = paramArrayOfbyte1;
    this.mRawDataFilterMask = paramArrayOfbyte2;
    this.mRenamePrefix = paramString1;
    this.mRenameSuffix = paramString2;
    this.mRenameBytesFrom = paramInt1;
    this.mRenameBytesLength = paramInt2;
    this.mRenameNameFrom = paramInt3;
    this.mRenameNameLength = paramInt4;
    this.mRenameBytesReverseOrder = paramBoolean;
  }
  
  public Pattern getNamePattern() {
    return this.mNamePattern;
  }
  
  public ScanFilter getScanFilter() {
    return this.mScanFilter;
  }
  
  public byte[] getRawDataFilter() {
    return this.mRawDataFilter;
  }
  
  public byte[] getRawDataFilterMask() {
    return this.mRawDataFilterMask;
  }
  
  public String getRenamePrefix() {
    return this.mRenamePrefix;
  }
  
  public String getRenameSuffix() {
    return this.mRenameSuffix;
  }
  
  public int getRenameBytesFrom() {
    return this.mRenameBytesFrom;
  }
  
  public int getRenameBytesLength() {
    return this.mRenameBytesLength;
  }
  
  public boolean isRenameBytesReverseOrder() {
    return this.mRenameBytesReverseOrder;
  }
  
  public String getDeviceDisplayName(ScanResult paramScanResult) {
    byte[] arrayOfByte;
    if (this.mRenameBytesFrom < 0 && this.mRenameNameFrom < 0)
      return BluetoothDeviceFilterUtils.getDeviceDisplayNameInternal(paramScanResult.getDevice()); 
    StringBuilder stringBuilder = new StringBuilder(TextUtils.emptyIfNull(this.mRenamePrefix));
    if (this.mRenameBytesFrom >= 0) {
      int k;
      byte b;
      arrayOfByte = paramScanResult.getScanRecord().getBytes();
      int i = this.mRenameBytesFrom;
      int j = this.mRenameBytesFrom + this.mRenameBytesLength - 1;
      if (this.mRenameBytesReverseOrder) {
        k = j;
      } else {
        k = i;
      } 
      if (this.mRenameBytesReverseOrder) {
        b = -1;
      } else {
        b = 1;
      } 
      for (; i <= k && k <= j; k += b)
        stringBuilder.append(HexEncoding.encodeToString(arrayOfByte[k], true)); 
    } else {
      String str = BluetoothDeviceFilterUtils.getDeviceDisplayNameInternal(arrayOfByte.getDevice());
      int j = this.mRenameNameFrom, i = this.mRenameNameLength;
      str = str.substring(j, i + j);
      stringBuilder.append(str);
    } 
    stringBuilder.append(TextUtils.emptyIfNull(this.mRenameSuffix));
    return stringBuilder.toString();
  }
  
  public boolean matches(ScanResult paramScanResult) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getDevice : ()Landroid/bluetooth/BluetoothDevice;
    //   4: astore_2
    //   5: aload_0
    //   6: invokevirtual getScanFilter : ()Landroid/bluetooth/le/ScanFilter;
    //   9: aload_1
    //   10: invokevirtual matches : (Landroid/bluetooth/le/ScanResult;)Z
    //   13: ifeq -> 60
    //   16: aload_0
    //   17: invokevirtual getNamePattern : ()Ljava/util/regex/Pattern;
    //   20: aload_2
    //   21: invokestatic matchesName : (Ljava/util/regex/Pattern;Landroid/bluetooth/BluetoothDevice;)Z
    //   24: ifeq -> 60
    //   27: aload_0
    //   28: getfield mRawDataFilter : [B
    //   31: ifnull -> 55
    //   34: aload_1
    //   35: invokevirtual getScanRecord : ()Landroid/bluetooth/le/ScanRecord;
    //   38: invokevirtual getBytes : ()[B
    //   41: aload_0
    //   42: getfield mRawDataFilter : [B
    //   45: aload_0
    //   46: getfield mRawDataFilterMask : [B
    //   49: invokestatic maskedEquals : ([B[B[B)Z
    //   52: ifeq -> 60
    //   55: iconst_1
    //   56: istore_3
    //   57: goto -> 62
    //   60: iconst_0
    //   61: istore_3
    //   62: iload_3
    //   63: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #169	-> 0
    //   #170	-> 5
    //   #171	-> 16
    //   #173	-> 34
    //   #177	-> 62
  }
  
  public int getMediumType() {
    return 1;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mRenameBytesFrom == ((BluetoothLeDeviceFilter)paramObject).mRenameBytesFrom && this.mRenameBytesLength == ((BluetoothLeDeviceFilter)paramObject).mRenameBytesLength && this.mRenameNameFrom == ((BluetoothLeDeviceFilter)paramObject).mRenameNameFrom && this.mRenameNameLength == ((BluetoothLeDeviceFilter)paramObject).mRenameNameLength && this.mRenameBytesReverseOrder == ((BluetoothLeDeviceFilter)paramObject).mRenameBytesReverseOrder) {
      Pattern pattern1 = this.mNamePattern, pattern2 = ((BluetoothLeDeviceFilter)paramObject).mNamePattern;
      if (Objects.equals(pattern1, pattern2)) {
        ScanFilter scanFilter2 = this.mScanFilter, scanFilter1 = ((BluetoothLeDeviceFilter)paramObject).mScanFilter;
        if (Objects.equals(scanFilter2, scanFilter1)) {
          byte[] arrayOfByte1 = this.mRawDataFilter, arrayOfByte2 = ((BluetoothLeDeviceFilter)paramObject).mRawDataFilter;
          if (Arrays.equals(arrayOfByte1, arrayOfByte2)) {
            arrayOfByte1 = this.mRawDataFilterMask;
            arrayOfByte2 = ((BluetoothLeDeviceFilter)paramObject).mRawDataFilterMask;
            if (Arrays.equals(arrayOfByte1, arrayOfByte2)) {
              String str1 = this.mRenamePrefix, str2 = ((BluetoothLeDeviceFilter)paramObject).mRenamePrefix;
              if (Objects.equals(str1, str2)) {
                str1 = this.mRenameSuffix;
                paramObject = ((BluetoothLeDeviceFilter)paramObject).mRenameSuffix;
                if (Objects.equals(str1, paramObject))
                  return null; 
              } 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    Pattern pattern = this.mNamePattern;
    ScanFilter scanFilter = this.mScanFilter;
    byte[] arrayOfByte1 = this.mRawDataFilter, arrayOfByte2 = this.mRawDataFilterMask;
    String str1 = this.mRenamePrefix, str2 = this.mRenameSuffix;
    int i = this.mRenameBytesFrom;
    int j = this.mRenameBytesLength, k = this.mRenameNameFrom;
    int m = this.mRenameNameLength;
    boolean bool = this.mRenameBytesReverseOrder;
    return Objects.hash(new Object[] { 
          pattern, scanFilter, arrayOfByte1, arrayOfByte2, str1, str2, Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), 
          Boolean.valueOf(bool) });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(BluetoothDeviceFilterUtils.patternToString(getNamePattern()));
    paramParcel.writeParcelable(this.mScanFilter, paramInt);
    paramParcel.writeByteArray(this.mRawDataFilter);
    paramParcel.writeByteArray(this.mRawDataFilterMask);
    paramParcel.writeString(this.mRenamePrefix);
    paramParcel.writeString(this.mRenameSuffix);
    paramParcel.writeInt(this.mRenameBytesFrom);
    paramParcel.writeInt(this.mRenameBytesLength);
    paramParcel.writeInt(this.mRenameNameFrom);
    paramParcel.writeInt(this.mRenameNameLength);
    paramParcel.writeBoolean(this.mRenameBytesReverseOrder);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("BluetoothLEDeviceFilter{mNamePattern=");
    stringBuilder.append(this.mNamePattern);
    stringBuilder.append(", mScanFilter=");
    stringBuilder.append(this.mScanFilter);
    stringBuilder.append(", mRawDataFilter=");
    byte[] arrayOfByte = this.mRawDataFilter;
    stringBuilder.append(Arrays.toString(arrayOfByte));
    stringBuilder.append(", mRawDataFilterMask=");
    arrayOfByte = this.mRawDataFilterMask;
    stringBuilder.append(Arrays.toString(arrayOfByte));
    stringBuilder.append(", mRenamePrefix='");
    stringBuilder.append(this.mRenamePrefix);
    stringBuilder.append('\'');
    stringBuilder.append(", mRenameSuffix='");
    stringBuilder.append(this.mRenameSuffix);
    stringBuilder.append('\'');
    stringBuilder.append(", mRenameBytesFrom=");
    stringBuilder.append(this.mRenameBytesFrom);
    stringBuilder.append(", mRenameBytesLength=");
    stringBuilder.append(this.mRenameBytesLength);
    stringBuilder.append(", mRenameNameFrom=");
    stringBuilder.append(this.mRenameNameFrom);
    stringBuilder.append(", mRenameNameLength=");
    stringBuilder.append(this.mRenameNameLength);
    stringBuilder.append(", mRenameBytesReverseOrder=");
    stringBuilder.append(this.mRenameBytesReverseOrder);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<BluetoothLeDeviceFilter> CREATOR = (Parcelable.Creator<BluetoothLeDeviceFilter>)new Object();
  
  private static final boolean DEBUG = false;
  
  private static final String LOG_TAG = "BluetoothLeDeviceFilter";
  
  private static final int RENAME_PREFIX_LENGTH_LIMIT = 10;
  
  private final Pattern mNamePattern;
  
  private final byte[] mRawDataFilter;
  
  private final byte[] mRawDataFilterMask;
  
  private final int mRenameBytesFrom;
  
  private final int mRenameBytesLength;
  
  private final boolean mRenameBytesReverseOrder;
  
  private final int mRenameNameFrom;
  
  private final int mRenameNameLength;
  
  private final String mRenamePrefix;
  
  private final String mRenameSuffix;
  
  private final ScanFilter mScanFilter;
  
  public static int getRenamePrefixLengthLimit() {
    return 10;
  }
  
  class Builder extends OneTimeUseBuilder<BluetoothLeDeviceFilter> {
    private int mRenameBytesFrom = -1;
    
    private int mRenameNameFrom = -1;
    
    private boolean mRenameBytesReverseOrder = false;
    
    private Pattern mNamePattern;
    
    private byte[] mRawDataFilter;
    
    private byte[] mRawDataFilterMask;
    
    private int mRenameBytesLength;
    
    private int mRenameNameLength;
    
    private String mRenamePrefix;
    
    private String mRenameSuffix;
    
    private ScanFilter mScanFilter;
    
    public Builder setNamePattern(Pattern param1Pattern) {
      checkNotUsed();
      this.mNamePattern = param1Pattern;
      return this;
    }
    
    public Builder setScanFilter(ScanFilter param1ScanFilter) {
      checkNotUsed();
      this.mScanFilter = param1ScanFilter;
      return this;
    }
    
    public Builder setRawDataFilter(byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      checkNotUsed();
      Objects.requireNonNull(param1ArrayOfbyte1);
      if (param1ArrayOfbyte2 == null || param1ArrayOfbyte1.length == param1ArrayOfbyte2.length) {
        boolean bool1 = true;
        Preconditions.checkArgument(bool1, "Mask and filter should be the same length");
        this.mRawDataFilter = param1ArrayOfbyte1;
        this.mRawDataFilterMask = param1ArrayOfbyte2;
        return this;
      } 
      boolean bool = false;
      Preconditions.checkArgument(bool, "Mask and filter should be the same length");
      this.mRawDataFilter = param1ArrayOfbyte1;
      this.mRawDataFilterMask = param1ArrayOfbyte2;
      return this;
    }
    
    public Builder setRenameFromBytes(String param1String1, String param1String2, int param1Int1, int param1Int2, ByteOrder param1ByteOrder) {
      boolean bool;
      checkRenameNotSet();
      checkRangeNotEmpty(param1Int2);
      this.mRenameBytesFrom = param1Int1;
      this.mRenameBytesLength = param1Int2;
      if (param1ByteOrder == ByteOrder.LITTLE_ENDIAN) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mRenameBytesReverseOrder = bool;
      return setRename(param1String1, param1String2);
    }
    
    public Builder setRenameFromName(String param1String1, String param1String2, int param1Int1, int param1Int2) {
      checkRenameNotSet();
      checkRangeNotEmpty(param1Int2);
      this.mRenameNameFrom = param1Int1;
      this.mRenameNameLength = param1Int2;
      this.mRenameBytesReverseOrder = false;
      return setRename(param1String1, param1String2);
    }
    
    private void checkRenameNotSet() {
      boolean bool;
      if (this.mRenamePrefix == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool, "Renaming rule can only be set once");
    }
    
    private void checkRangeNotEmpty(int param1Int) {
      boolean bool;
      if (param1Int > 0) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool, "Range must be non-empty");
    }
    
    private Builder setRename(String param1String1, String param1String2) {
      boolean bool;
      checkNotUsed();
      if (TextUtils.length(param1String1) <= BluetoothLeDeviceFilter.getRenamePrefixLengthLimit()) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool, "Prefix is too long");
      this.mRenamePrefix = param1String1;
      this.mRenameSuffix = param1String2;
      return this;
    }
    
    public BluetoothLeDeviceFilter build() {
      markUsed();
      return new BluetoothLeDeviceFilter(this.mNamePattern, this.mScanFilter, this.mRawDataFilter, this.mRawDataFilterMask, this.mRenamePrefix, this.mRenameSuffix, this.mRenameBytesFrom, this.mRenameBytesLength, this.mRenameNameFrom, this.mRenameNameLength, this.mRenameBytesReverseOrder);
    }
  }
}
