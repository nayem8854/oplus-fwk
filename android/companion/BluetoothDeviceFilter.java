package android.companion;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;
import android.provider.OneTimeUseBuilder;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.CollectionUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

public final class BluetoothDeviceFilter implements DeviceFilter<BluetoothDevice> {
  private BluetoothDeviceFilter(Pattern paramPattern, String paramString, List<ParcelUuid> paramList1, List<ParcelUuid> paramList2) {
    this.mNamePattern = paramPattern;
    this.mAddress = paramString;
    this.mServiceUuids = CollectionUtils.emptyIfNull(paramList1);
    this.mServiceUuidMasks = CollectionUtils.emptyIfNull(paramList2);
  }
  
  private BluetoothDeviceFilter(Parcel paramParcel) {
    this(pattern, str, list2, list1);
  }
  
  private static List<ParcelUuid> readUuids(Parcel paramParcel) {
    return paramParcel.readParcelableList(new ArrayList(), ParcelUuid.class.getClassLoader());
  }
  
  public boolean matches(BluetoothDevice paramBluetoothDevice) {
    if (BluetoothDeviceFilterUtils.matchesAddress(this.mAddress, paramBluetoothDevice)) {
      List<ParcelUuid> list1 = this.mServiceUuids, list2 = this.mServiceUuidMasks;
      if (BluetoothDeviceFilterUtils.matchesServiceUuids(list1, list2, paramBluetoothDevice) && 
        BluetoothDeviceFilterUtils.matchesName(getNamePattern(), paramBluetoothDevice))
        return true; 
    } 
    return false;
  }
  
  public String getDeviceDisplayName(BluetoothDevice paramBluetoothDevice) {
    return BluetoothDeviceFilterUtils.getDeviceDisplayNameInternal(paramBluetoothDevice);
  }
  
  public int getMediumType() {
    return 0;
  }
  
  public Pattern getNamePattern() {
    return this.mNamePattern;
  }
  
  public String getAddress() {
    return this.mAddress;
  }
  
  public List<ParcelUuid> getServiceUuids() {
    return this.mServiceUuids;
  }
  
  public List<ParcelUuid> getServiceUuidMasks() {
    return this.mServiceUuidMasks;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(BluetoothDeviceFilterUtils.patternToString(getNamePattern()));
    paramParcel.writeString(this.mAddress);
    paramParcel.writeParcelableList(this.mServiceUuids, paramInt);
    paramParcel.writeParcelableList(this.mServiceUuidMasks, paramInt);
  }
  
  public boolean equals(Object<ParcelUuid> paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mNamePattern, ((BluetoothDeviceFilter)paramObject).mNamePattern)) {
      String str1 = this.mAddress, str2 = ((BluetoothDeviceFilter)paramObject).mAddress;
      if (Objects.equals(str1, str2)) {
        List<ParcelUuid> list2 = this.mServiceUuids, list1 = ((BluetoothDeviceFilter)paramObject).mServiceUuids;
        if (Objects.equals(list2, list1)) {
          list1 = this.mServiceUuidMasks;
          paramObject = (Object<ParcelUuid>)((BluetoothDeviceFilter)paramObject).mServiceUuidMasks;
          if (Objects.equals(list1, paramObject))
            return null; 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mNamePattern, this.mAddress, this.mServiceUuids, this.mServiceUuidMasks });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("BluetoothDeviceFilter{mNamePattern=");
    stringBuilder.append(this.mNamePattern);
    stringBuilder.append(", mAddress='");
    stringBuilder.append(this.mAddress);
    stringBuilder.append('\'');
    stringBuilder.append(", mServiceUuids=");
    stringBuilder.append(this.mServiceUuids);
    stringBuilder.append(", mServiceUuidMasks=");
    stringBuilder.append(this.mServiceUuidMasks);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<BluetoothDeviceFilter> CREATOR = (Parcelable.Creator<BluetoothDeviceFilter>)new Object();
  
  private final String mAddress;
  
  private final Pattern mNamePattern;
  
  private final List<ParcelUuid> mServiceUuidMasks;
  
  private final List<ParcelUuid> mServiceUuids;
  
  class Builder extends OneTimeUseBuilder<BluetoothDeviceFilter> {
    private String mAddress;
    
    private Pattern mNamePattern;
    
    private ArrayList<ParcelUuid> mServiceUuid;
    
    private ArrayList<ParcelUuid> mServiceUuidMask;
    
    public Builder setNamePattern(Pattern param1Pattern) {
      checkNotUsed();
      this.mNamePattern = param1Pattern;
      return this;
    }
    
    public Builder setAddress(String param1String) {
      checkNotUsed();
      this.mAddress = param1String;
      return this;
    }
    
    public Builder addServiceUuid(ParcelUuid param1ParcelUuid1, ParcelUuid param1ParcelUuid2) {
      checkNotUsed();
      this.mServiceUuid = ArrayUtils.add(this.mServiceUuid, param1ParcelUuid1);
      this.mServiceUuidMask = ArrayUtils.add(this.mServiceUuidMask, param1ParcelUuid2);
      return this;
    }
    
    public BluetoothDeviceFilter build() {
      markUsed();
      return new BluetoothDeviceFilter(this.mNamePattern, this.mAddress, this.mServiceUuid, this.mServiceUuidMask);
    }
  }
}
