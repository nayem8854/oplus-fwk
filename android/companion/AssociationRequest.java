package android.companion;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.OneTimeUseBuilder;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.CollectionUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class AssociationRequest implements Parcelable {
  private AssociationRequest(boolean paramBoolean, List<DeviceFilter<?>> paramList) {
    this.mSingleDevice = paramBoolean;
    this.mDeviceFilters = CollectionUtils.emptyIfNull(paramList);
  }
  
  private AssociationRequest(Parcel paramParcel) {
    this(bool, list);
  }
  
  public boolean isSingleDevice() {
    return this.mSingleDevice;
  }
  
  public List<DeviceFilter<?>> getDeviceFilters() {
    return this.mDeviceFilters;
  }
  
  public boolean equals(Object<DeviceFilter<?>> paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    AssociationRequest associationRequest = (AssociationRequest)paramObject;
    if (this.mSingleDevice == associationRequest.mSingleDevice) {
      paramObject = (Object<DeviceFilter<?>>)this.mDeviceFilters;
      List<DeviceFilter<?>> list = associationRequest.mDeviceFilters;
      if (Objects.equals(paramObject, list))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Boolean.valueOf(this.mSingleDevice), this.mDeviceFilters });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("AssociationRequest{mSingleDevice=");
    stringBuilder.append(this.mSingleDevice);
    stringBuilder.append(", mDeviceFilters=");
    stringBuilder.append(this.mDeviceFilters);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByte((byte)this.mSingleDevice);
    paramParcel.writeParcelableList(this.mDeviceFilters, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<AssociationRequest> CREATOR = new Parcelable.Creator<AssociationRequest>() {
      public AssociationRequest createFromParcel(Parcel param1Parcel) {
        return new AssociationRequest(param1Parcel);
      }
      
      public AssociationRequest[] newArray(int param1Int) {
        return new AssociationRequest[param1Int];
      }
    };
  
  private final List<DeviceFilter<?>> mDeviceFilters;
  
  private final boolean mSingleDevice;
  
  public static final class Builder extends OneTimeUseBuilder<AssociationRequest> {
    private boolean mSingleDevice = false;
    
    private ArrayList<DeviceFilter<?>> mDeviceFilters = null;
    
    public Builder setSingleDevice(boolean param1Boolean) {
      checkNotUsed();
      this.mSingleDevice = param1Boolean;
      return this;
    }
    
    public Builder addDeviceFilter(DeviceFilter<?> param1DeviceFilter) {
      checkNotUsed();
      if (param1DeviceFilter != null)
        this.mDeviceFilters = ArrayUtils.add(this.mDeviceFilters, param1DeviceFilter); 
      return this;
    }
    
    public AssociationRequest build() {
      markUsed();
      return new AssociationRequest(this.mSingleDevice, this.mDeviceFilters);
    }
  }
}
