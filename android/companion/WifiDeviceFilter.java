package android.companion;

import android.annotation.NonNull;
import android.net.MacAddress;
import android.net.wifi.ScanResult;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.AnnotationValidations;
import com.android.internal.util.Parcelling;
import java.util.Objects;
import java.util.regex.Pattern;

public final class WifiDeviceFilter implements DeviceFilter<ScanResult> {
  public boolean matches(ScanResult paramScanResult) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getNamePattern : ()Ljava/util/regex/Pattern;
    //   4: aload_1
    //   5: invokestatic matchesName : (Ljava/util/regex/Pattern;Landroid/net/wifi/ScanResult;)Z
    //   8: ifeq -> 46
    //   11: aload_0
    //   12: getfield mBssid : Landroid/net/MacAddress;
    //   15: ifnull -> 41
    //   18: aload_1
    //   19: getfield BSSID : Ljava/lang/String;
    //   22: astore_1
    //   23: aload_1
    //   24: invokestatic fromString : (Ljava/lang/String;)Landroid/net/MacAddress;
    //   27: aload_0
    //   28: getfield mBssid : Landroid/net/MacAddress;
    //   31: aload_0
    //   32: getfield mBssidMask : Landroid/net/MacAddress;
    //   35: invokevirtual matches : (Landroid/net/MacAddress;Landroid/net/MacAddress;)Z
    //   38: ifeq -> 46
    //   41: iconst_1
    //   42: istore_2
    //   43: goto -> 48
    //   46: iconst_0
    //   47: istore_2
    //   48: iload_2
    //   49: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #71	-> 0
    //   #73	-> 23
    //   #71	-> 48
  }
  
  public String getDeviceDisplayName(ScanResult paramScanResult) {
    return BluetoothDeviceFilterUtils.getDeviceDisplayNameInternal(paramScanResult);
  }
  
  public int getMediumType() {
    return 2;
  }
  
  WifiDeviceFilter(Pattern paramPattern, MacAddress paramMacAddress1, MacAddress paramMacAddress2) {
    this.mNamePattern = null;
    this.mBssid = null;
    this.mBssidMask = MacAddress.BROADCAST_ADDRESS;
    this.mNamePattern = paramPattern;
    this.mBssid = paramMacAddress1;
    this.mBssidMask = paramMacAddress2;
    AnnotationValidations.validate(NonNull.class, null, paramMacAddress2);
  }
  
  public Pattern getNamePattern() {
    return this.mNamePattern;
  }
  
  public MacAddress getBssid() {
    return this.mBssid;
  }
  
  public MacAddress getBssidMask() {
    return this.mBssidMask;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    Pattern pattern1 = this.mNamePattern, pattern2 = ((WifiDeviceFilter)paramObject).mNamePattern;
    if (Objects.equals(pattern1, pattern2)) {
      MacAddress macAddress2 = this.mBssid, macAddress1 = ((WifiDeviceFilter)paramObject).mBssid;
      if (Objects.equals(macAddress2, macAddress1)) {
        macAddress1 = this.mBssidMask;
        paramObject = ((WifiDeviceFilter)paramObject).mBssidMask;
        if (Objects.equals(macAddress1, paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mNamePattern);
    int j = Objects.hashCode(this.mBssid);
    int k = Objects.hashCode(this.mBssidMask);
    return ((1 * 31 + i) * 31 + j) * 31 + k;
  }
  
  static {
    Parcelling<Pattern> parcelling = Parcelling.Cache.get(Parcelling.BuiltIn.ForPattern.class);
    if (parcelling == null)
      sParcellingForNamePattern = Parcelling.Cache.put((Parcelling)new Parcelling.BuiltIn.ForPattern()); 
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    byte b1 = 0;
    if (this.mNamePattern != null)
      b1 = (byte)(false | true); 
    byte b2 = b1;
    if (this.mBssid != null)
      b2 = (byte)(b1 | 0x2); 
    paramParcel.writeByte(b2);
    sParcellingForNamePattern.parcel(this.mNamePattern, paramParcel, paramInt);
    MacAddress macAddress = this.mBssid;
    if (macAddress != null)
      paramParcel.writeTypedObject((Parcelable)macAddress, paramInt); 
    paramParcel.writeTypedObject((Parcelable)this.mBssidMask, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  WifiDeviceFilter(Parcel paramParcel) {
    MacAddress macAddress2;
    this.mNamePattern = null;
    this.mBssid = null;
    this.mBssidMask = MacAddress.BROADCAST_ADDRESS;
    byte b = paramParcel.readByte();
    Pattern pattern = (Pattern)sParcellingForNamePattern.unparcel(paramParcel);
    if ((b & 0x2) == 0) {
      macAddress2 = null;
    } else {
      macAddress2 = (MacAddress)paramParcel.readTypedObject(MacAddress.CREATOR);
    } 
    MacAddress macAddress1 = (MacAddress)paramParcel.readTypedObject(MacAddress.CREATOR);
    this.mNamePattern = pattern;
    this.mBssid = macAddress2;
    this.mBssidMask = macAddress1;
    AnnotationValidations.validate(NonNull.class, null, macAddress1);
  }
  
  public static final Parcelable.Creator<WifiDeviceFilter> CREATOR = (Parcelable.Creator<WifiDeviceFilter>)new Object();
  
  static Parcelling<Pattern> sParcellingForNamePattern;
  
  private MacAddress mBssid;
  
  private MacAddress mBssidMask;
  
  private Pattern mNamePattern;
  
  class Builder {
    private MacAddress mBssid;
    
    private MacAddress mBssidMask;
    
    private long mBuilderFieldsSet = 0L;
    
    private Pattern mNamePattern;
    
    public Builder setNamePattern(Pattern param1Pattern) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x1L;
      this.mNamePattern = param1Pattern;
      return this;
    }
    
    public Builder setBssid(MacAddress param1MacAddress) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x2L;
      this.mBssid = param1MacAddress;
      return this;
    }
    
    public Builder setBssidMask(MacAddress param1MacAddress) {
      checkNotUsed();
      this.mBuilderFieldsSet |= 0x4L;
      this.mBssidMask = param1MacAddress;
      return this;
    }
    
    public WifiDeviceFilter build() {
      checkNotUsed();
      long l = this.mBuilderFieldsSet | 0x8L;
      if ((l & 0x1L) == 0L)
        this.mNamePattern = null; 
      if ((this.mBuilderFieldsSet & 0x2L) == 0L)
        this.mBssid = null; 
      if ((this.mBuilderFieldsSet & 0x4L) == 0L)
        this.mBssidMask = MacAddress.BROADCAST_ADDRESS; 
      return new WifiDeviceFilter(this.mNamePattern, this.mBssid, this.mBssidMask);
    }
    
    private void checkNotUsed() {
      if ((this.mBuilderFieldsSet & 0x8L) == 0L)
        return; 
      throw new IllegalStateException("This Builder should not be reused. Use a new Builder instance instead");
    }
  }
  
  @Deprecated
  private void __metadata() {}
}
