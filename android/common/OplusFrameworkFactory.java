package android.common;

import android.widget.IOplusFloatingToolbarUtil;
import java.lang.reflect.Constructor;

public class OplusFrameworkFactory implements IOplusCommonFactory {
  public static final String OPLUS_FRAMEWORK_FACTORY_IMPL_NAME = "oplus.android.OplusFrameworkFactoryImpl";
  
  private static final String TAG = "OplusFrameworkFactory";
  
  private static volatile OplusFrameworkFactory sInstance = null;
  
  public static OplusFrameworkFactory getInstance() {
    // Byte code:
    //   0: getstatic android/common/OplusFrameworkFactory.sInstance : Landroid/common/OplusFrameworkFactory;
    //   3: ifnonnull -> 90
    //   6: ldc android/common/OplusFrameworkFactory
    //   8: monitorenter
    //   9: getstatic android/common/OplusFrameworkFactory.sInstance : Landroid/common/OplusFrameworkFactory;
    //   12: astore_0
    //   13: aload_0
    //   14: ifnonnull -> 78
    //   17: ldc 'oplus.android.OplusFrameworkFactoryImpl'
    //   19: invokestatic newInstance : (Ljava/lang/String;)Ljava/lang/Object;
    //   22: checkcast android/common/OplusFrameworkFactory
    //   25: putstatic android/common/OplusFrameworkFactory.sInstance : Landroid/common/OplusFrameworkFactory;
    //   28: goto -> 78
    //   31: astore_0
    //   32: new java/lang/StringBuilder
    //   35: astore_1
    //   36: aload_1
    //   37: invokespecial <init> : ()V
    //   40: aload_1
    //   41: ldc ' Reflect exception getInstance: '
    //   43: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   46: pop
    //   47: aload_1
    //   48: aload_0
    //   49: invokevirtual toString : ()Ljava/lang/String;
    //   52: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: ldc 'OplusFrameworkFactory'
    //   58: aload_1
    //   59: invokevirtual toString : ()Ljava/lang/String;
    //   62: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   65: pop
    //   66: new android/common/OplusFrameworkFactory
    //   69: astore_0
    //   70: aload_0
    //   71: invokespecial <init> : ()V
    //   74: aload_0
    //   75: putstatic android/common/OplusFrameworkFactory.sInstance : Landroid/common/OplusFrameworkFactory;
    //   78: ldc android/common/OplusFrameworkFactory
    //   80: monitorexit
    //   81: goto -> 90
    //   84: astore_0
    //   85: ldc android/common/OplusFrameworkFactory
    //   87: monitorexit
    //   88: aload_0
    //   89: athrow
    //   90: getstatic android/common/OplusFrameworkFactory.sInstance : Landroid/common/OplusFrameworkFactory;
    //   93: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #36	-> 0
    //   #37	-> 6
    //   #38	-> 9
    //   #40	-> 17
    //   #44	-> 28
    //   #41	-> 31
    //   #42	-> 32
    //   #43	-> 66
    //   #46	-> 78
    //   #48	-> 90
    // Exception table:
    //   from	to	target	type
    //   9	13	84	finally
    //   17	28	31	java/lang/Exception
    //   17	28	84	finally
    //   32	66	84	finally
    //   66	78	84	finally
    //   78	81	84	finally
    //   85	88	84	finally
  }
  
  public boolean isValid(int paramInt) {
    // Byte code:
    //   0: getstatic android/common/OplusFeatureList$OplusIndex.EndOplusFrameworkFactory : Landroid/common/OplusFeatureList$OplusIndex;
    //   3: invokevirtual ordinal : ()I
    //   6: istore_2
    //   7: iconst_1
    //   8: istore_3
    //   9: iload_1
    //   10: iload_2
    //   11: if_icmpge -> 33
    //   14: getstatic android/common/OplusFeatureList$OplusIndex.StartOplusFrameworkFactory : Landroid/common/OplusFeatureList$OplusIndex;
    //   17: astore #4
    //   19: iload_1
    //   20: aload #4
    //   22: invokevirtual ordinal : ()I
    //   25: if_icmple -> 33
    //   28: iconst_1
    //   29: istore_2
    //   30: goto -> 35
    //   33: iconst_0
    //   34: istore_2
    //   35: iload_1
    //   36: getstatic android/common/OplusFeatureList$OplusIndex.EndColorFrameworkFactory : Landroid/common/OplusFeatureList$OplusIndex;
    //   39: invokevirtual ordinal : ()I
    //   42: if_icmpge -> 64
    //   45: getstatic android/common/OplusFeatureList$OplusIndex.StartColorFrameworkFactory : Landroid/common/OplusFeatureList$OplusIndex;
    //   48: astore #4
    //   50: iload_1
    //   51: aload #4
    //   53: invokevirtual ordinal : ()I
    //   56: if_icmple -> 64
    //   59: iconst_1
    //   60: istore_1
    //   61: goto -> 66
    //   64: iconst_0
    //   65: istore_1
    //   66: iload_3
    //   67: istore #5
    //   69: iload_1
    //   70: ifne -> 86
    //   73: iload_2
    //   74: ifeq -> 83
    //   77: iload_3
    //   78: istore #5
    //   80: goto -> 86
    //   83: iconst_0
    //   84: istore #5
    //   86: iload #5
    //   88: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #53	-> 0
    //   #54	-> 19
    //   #55	-> 35
    //   #56	-> 50
    //   #58	-> 66
    //   #59	-> 86
  }
  
  static Object newInstance(String paramString) throws Exception {
    Class<?> clazz = Class.forName(paramString);
    Constructor<?> constructor = clazz.getConstructor(new Class[0]);
    return constructor.newInstance(new Object[0]);
  }
  
  public IOplusFloatingToolbarUtil getOplusFloatingToolbarUtil() {
    return IOplusFloatingToolbarUtil.DEFAULT;
  }
}
