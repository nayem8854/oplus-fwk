package android.common;

public class OplusFeatureList {
  public enum OplusIndex {
    End, EndColorFrameworkFactory, EndColorInternalServiceFactory, EndColorServiceFactory, EndOplusFrameworkFactory, EndOplusJobSchedulerServiceFactory, EndOplusServiceFactory, IBatteryIdleController, IColorAIBrightManager, IColorAbnormalComponentManager, IColorAccessControlLocalManager, IColorActivityManagerServiceEx, IColorActivityTaskManagerServiceEx, IColorAlarmAlignment, IColorAlarmManagerHelper, IColorAlarmManagerServiceEx, IColorAlarmManagerServiceHelper, IColorAlarmTempWhitelist, IColorAlarmWakeupDetection, IColorAppCrashClearManager, IColorAppDetectManager, IColorAppInstallProgressManager, IColorAppListInterceptManager, IColorAppQuickFreezeManager, IColorAppStartupManager, IColorAppStoreTraffic, IColorAppSwitchManager, IColorAthenaAmManager, IColorAthenaManager, IColorBatterySaveExtend, IColorBreenoManager, IColorBroadcastManager, IColorBroadcastStaticRegisterWhitelistManager, IColorChildrenModeInstallManager, IColorClearDataProtectManager, IColorCommonListManager, IColorConfineModeManager, IColorDataFreeManager, IColorDeepSleepHelper, IColorDefaultAppPolicyManager, IColorDeviceIdleControllerEx, IColorDeviceIdleHelper, IColorDexMetadataManager, IColorDisplayManagerServiceEx, IColorDisplayPolicyEx, IColorDozeNetworkOptimization, IColorDynamicFeatureManager, IColorDynamicLogManager, IColorEapManager, IColorEdgeTouchManager, IColorEyeProtectManager, IColorFastAppManager, IColorFixupDataManager, IColorForbidHideOrDisableManager, IColorForbidUninstallAppManager, IColorFreeformManager, IColorFullmodeManager, IColorGameSpaceManager, IColorGoogleAlarmRestrict, IColorGoogleDozeRestrict, IColorGuardElfFeature, IColorHansManager, IColorIconCachesManager, IColorInputMethodManagerServiceEx, IColorInstallThreadsControlManager, IColorJobScheduleManager, IColorKeyEventManager, IColorKeyLayoutManager, IColorLockTaskController, IColorMergedProcessSplitManager, IColorMultiAppManager, IColorNetworkPolicyManagerServiceEx, IColorOtaDataManager, IColorOverlayManagerServiceEx, IColorPackageInstallInterceptManager, IColorPackageInstallStatisticManager, IColorPackageManagerServiceEx, IColorPerfManager, IColorPermSupportedFunctionManager, IColorPermissionManagerServiceEx, IColorPkgStartInfoManager, IColorPmsSupportedFunctionManager, IColorPowerManagerServiceEx, IColorRemovableAppManager, IColorResourcePreloadManager, IColorRuntimePermGrantPolicyManager, IColorSaveSurfaceManager, IColorScreenOffOptimization, IColorSecurePayManager, IColorSecurityPermissionManager, IColorSellModeManager, IColorSemlessAnimationManager, IColorSensitivePermGrantPolicyManager, IColorSilentRebootManager, IColorSmartDozeHelper, IColorSplitWindowManager, IColorStartingWindowManager, IColorStorageAllFileAccessManager, IColorStrictModeManager, IColorSysStateManager, IColorSystemAppProtectManager, IColorSystemServerEx, IColorThirdPartyAppSignCheckManager, IColorTouchNodeManager, IColorVFXScreenEffectFeature, IColorWakeLockCheck, IColorWatermarkManager, IColorWindowAnimationManager, IColorWindowManagerServiceEx, IColorZoomWindowManager, ICompatibilityHelper, IFreezeManagerHelp, IFreezeManagerService, IJobCountPolicy, IOPPODnsSelfrecoveryEngine, IOplusAccidentallyTouchHelper, IOplusActivityManagerDynamicLogConfigFeature, IOplusActivityManagerServiceEx, IOplusActivityTaskManagerServiceEx, IOplusAlertControllerEuclidManager, IOplusAmsUtilsFeatrue, IOplusAppConfigManager, IOplusAppDynamicFeatureManager, IOplusAppOpsManager, IOplusArmyControllerFeatrue, IOplusArmyServiceFeatrue, IOplusBackgroundTaskManagerService, IOplusBootPressureHolder, IOplusBurmeseZgHooks, IOplusCoarseToFine, IOplusCommonInjector, IOplusCpuExceptionMonitor, IOplusDarkModeManager, IOplusDarkModeMaskManager, IOplusDarkModeServiceManager, IOplusDeepThinkerExService, IOplusDeepThinkerManager, IOplusDexOptimizeManager, IOplusDirectViewHelper, IOplusDisplayPowerControllerFeature, IOplusDragTextShadowHelper, IOplusDrmDecoderFeature, IOplusDualHeadPhoneFeature, IOplusEngineerService, IOplusFastNetworkLocation, IOplusFavoriteManager, IOplusFeatureAOD, IOplusFeatureBrightness, IOplusFeatureColorMode, IOplusFeatureConfigManagerInternal, IOplusFeatureDCBacklight, IOplusFeatureHDREnhanceBrightness, IOplusFeatureReduceBrightness, IOplusFloatingToolbarUtil, IOplusFontManager, IOplusFtHooks, IOplusFullScreenDisplayManager, IOplusGestureUpBarPostionHelper, IOplusGlobalDragAndDropManager, IOplusGnssDiagnosticTool, IOplusGnssDuration, IOplusGnssWhiteListProxy, IOplusGradientHooks, IOplusHeadsetFadeIn, IOplusHighFrequencyLocationBlacklist, IOplusIconPackManager, IOplusInputMethodServiceUtils, IOplusJobSchedulerServiceEx, IOplusJobSchedulerSystemServerEx, IOplusJobTest, IOplusKeepAliveManager, IOplusLBSMainClass, IOplusLanguageEnableManager, IOplusLanguageManager, IOplusLbsCustomize, IOplusLbsRepairer, IOplusLbsRomUpdateUtil, IOplusListHooks, IOplusLocationBlacklistUtil, IOplusLocationCache, IOplusLocationStatistics, IOplusLocationStatusMonitor, IOplusMagnifierHooks, IOplusMirageDisplayManager, IOplusNavigationStatusController, IOplusNewNetworkTimeUpdateServiceFeature, IOplusNlpProxy, IOplusNotificationManagerServiceEx, IOplusNotificationUiManager, IOplusNwPowerManager, IOplusOptimizingProgressManager, IOplusOverScrollerHelper, IOplusPackageManagerNativeEx, IOplusPackageManagerServiceEx, IOplusPowerManagerServiceEx, IOplusPowerManagerServiceFeature, IOplusRedPacketManager, IOplusResolverManager, IOplusResolverStyle, IOplusRotationOptimization, IOplusScreenCastContentManager, IOplusScreenFrozenBooster, IOplusScreenFrozenManager, IOplusScreenModeFeature, IOplusScreenOffTorchHelper, IOplusScreenShotEuclidManager, IOplusSecurityAnalysisBroadCastSender, IOplusShutdownFeature, IOplusSuplController, IOplusSystemServerEx, IOplusTextViewRTLUtilForUG, IOplusThemeManager, IOplusThemeStyle, IOplusUIFirstManager, IOplusVerificationCodeController, IOplusViewConfigHelper, IOplusViewHooks, IOplusViewRootUtil, IOplusWifiGlobalMethod, IOplusWifiNetworkConfig, IOplusWindowManagerServiceEx, IOplusZenModeFeature, IOppoAutoConCaptivePortalControl, IOppoBatteryServiceFeature, IOppoBrightness, IOppoConnectivityServiceHelperUtils, IOppoDnsManagerHelper, IOppoListManager, IOppoNetworkNotificationManager, IOppoScreenModeManagerFeature, IOppoStorageManagerFeature, IOppoUsbDeviceFeature, IOppoVibratorFeature, IOppoVpnHelper, IOppoWifiDisplayUsageHelper, IOpusJobSchedulerServiceEx, ITextJustificationHooks, IWifiRomUpdateHelper, StartColorFrameworkFactory, StartColorInternalServiceFactory, StartColorServiceFactory, StartOplusFrameworkFactory, StartOplusJobSchedulerServiceFactory, StartOplusServiceFactory;
    
    private static final OplusIndex[] $VALUES;
    
    static {
      IColorActivityManagerServiceEx = new OplusIndex("IColorActivityManagerServiceEx", 2);
      IColorActivityTaskManagerServiceEx = new OplusIndex("IColorActivityTaskManagerServiceEx", 3);
      IColorPackageManagerServiceEx = new OplusIndex("IColorPackageManagerServiceEx", 4);
      IColorPermissionManagerServiceEx = new OplusIndex("IColorPermissionManagerServiceEx", 5);
      IColorWindowManagerServiceEx = new OplusIndex("IColorWindowManagerServiceEx", 6);
      IColorPowerManagerServiceEx = new OplusIndex("IColorPowerManagerServiceEx", 7);
      IColorAlarmManagerServiceEx = new OplusIndex("IColorAlarmManagerServiceEx", 8);
      IOpusJobSchedulerServiceEx = new OplusIndex("IOpusJobSchedulerServiceEx", 9);
      IOplusNotificationManagerServiceEx = new OplusIndex("IOplusNotificationManagerServiceEx", 10);
      IColorOverlayManagerServiceEx = new OplusIndex("IColorOverlayManagerServiceEx", 11);
      IColorInputMethodManagerServiceEx = new OplusIndex("IColorInputMethodManagerServiceEx", 12);
      IColorDisplayManagerServiceEx = new OplusIndex("IColorDisplayManagerServiceEx", 13);
      IColorNetworkPolicyManagerServiceEx = new OplusIndex("IColorNetworkPolicyManagerServiceEx", 14);
      IColorDisplayPolicyEx = new OplusIndex("IColorDisplayPolicyEx", 15);
      IColorDataFreeManager = new OplusIndex("IColorDataFreeManager", 16);
      IColorFullmodeManager = new OplusIndex("IColorFullmodeManager", 17);
      IColorIconCachesManager = new OplusIndex("IColorIconCachesManager", 18);
      IColorAppStartupManager = new OplusIndex("IColorAppStartupManager", 19);
      IColorChildrenModeInstallManager = new OplusIndex("IColorChildrenModeInstallManager", 20);
      IColorAppStoreTraffic = new OplusIndex("IColorAppStoreTraffic", 21);
      IColorDynamicFeatureManager = new OplusIndex("IColorDynamicFeatureManager", 22);
      IColorAppCrashClearManager = new OplusIndex("IColorAppCrashClearManager", 23);
      IColorAppSwitchManager = new OplusIndex("IColorAppSwitchManager", 24);
      IColorBroadcastStaticRegisterWhitelistManager = new OplusIndex("IColorBroadcastStaticRegisterWhitelistManager", 25);
      IColorSellModeManager = new OplusIndex("IColorSellModeManager", 26);
      IColorAppInstallProgressManager = new OplusIndex("IColorAppInstallProgressManager", 27);
      IColorGameSpaceManager = new OplusIndex("IColorGameSpaceManager", 28);
      IOplusAppConfigManager = new OplusIndex("IOplusAppConfigManager", 29);
      IColorScreenOffOptimization = new OplusIndex("IColorScreenOffOptimization", 30);
      IColorSplitWindowManager = new OplusIndex("IColorSplitWindowManager", 31);
      IColorMultiAppManager = new OplusIndex("IColorMultiAppManager", 32);
      IColorBatterySaveExtend = new OplusIndex("IColorBatterySaveExtend", 33);
      IColorGuardElfFeature = new OplusIndex("IColorGuardElfFeature", 34);
      IColorAppListInterceptManager = new OplusIndex("IColorAppListInterceptManager", 35);
      IColorAccessControlLocalManager = new OplusIndex("IColorAccessControlLocalManager", 36);
      IColorLockTaskController = new OplusIndex("IColorLockTaskController", 37);
      IColorDeepSleepHelper = new OplusIndex("IColorDeepSleepHelper", 38);
      IColorAppQuickFreezeManager = new OplusIndex("IColorAppQuickFreezeManager", 39);
      IColorRuntimePermGrantPolicyManager = new OplusIndex("IColorRuntimePermGrantPolicyManager", 40);
      IColorDefaultAppPolicyManager = new OplusIndex("IColorDefaultAppPolicyManager", 41);
      IColorDozeNetworkOptimization = new OplusIndex("IColorDozeNetworkOptimization", 42);
      IColorClearDataProtectManager = new OplusIndex("IColorClearDataProtectManager", 43);
      IColorGoogleAlarmRestrict = new OplusIndex("IColorGoogleAlarmRestrict", 44);
      IColorPackageInstallStatisticManager = new OplusIndex("IColorPackageInstallStatisticManager", 45);
      IColorAlarmManagerHelper = new OplusIndex("IColorAlarmManagerHelper", 46);
      IColorSecurePayManager = new OplusIndex("IColorSecurePayManager", 47);
      IColorPackageInstallInterceptManager = new OplusIndex("IColorPackageInstallInterceptManager", 48);
      IColorSensitivePermGrantPolicyManager = new OplusIndex("IColorSensitivePermGrantPolicyManager", 49);
      IColorInstallThreadsControlManager = new OplusIndex("IColorInstallThreadsControlManager", 50);
      IColorThirdPartyAppSignCheckManager = new OplusIndex("IColorThirdPartyAppSignCheckManager", 51);
      IColorBroadcastManager = new OplusIndex("IColorBroadcastManager", 52);
      IColorForbidUninstallAppManager = new OplusIndex("IColorForbidUninstallAppManager", 53);
      IColorStrictModeManager = new OplusIndex("IColorStrictModeManager", 54);
      IColorFastAppManager = new OplusIndex("IColorFastAppManager", 55);
      IColorAlarmAlignment = new OplusIndex("IColorAlarmAlignment", 56);
      IColorSecurityPermissionManager = new OplusIndex("IColorSecurityPermissionManager", 57);
      IColorAlarmTempWhitelist = new OplusIndex("IColorAlarmTempWhitelist", 58);
      IColorDynamicLogManager = new OplusIndex("IColorDynamicLogManager", 59);
      IColorAIBrightManager = new OplusIndex("IColorAIBrightManager", 60);
      IOplusLanguageManager = new OplusIndex("IOplusLanguageManager", 61);
      IColorForbidHideOrDisableManager = new OplusIndex("IColorForbidHideOrDisableManager", 62);
      IOplusLanguageEnableManager = new OplusIndex("IOplusLanguageEnableManager", 63);
      IColorPkgStartInfoManager = new OplusIndex("IColorPkgStartInfoManager", 64);
      IColorFixupDataManager = new OplusIndex("IColorFixupDataManager", 65);
      IColorRemovableAppManager = new OplusIndex("IColorRemovableAppManager", 66);
      IColorSystemAppProtectManager = new OplusIndex("IColorSystemAppProtectManager", 67);
      IColorSilentRebootManager = new OplusIndex("IColorSilentRebootManager", 68);
      IColorWakeLockCheck = new OplusIndex("IColorWakeLockCheck", 69);
      IColorFreeformManager = new OplusIndex("IColorFreeformManager", 70);
      IColorZoomWindowManager = new OplusIndex("IColorZoomWindowManager", 71);
      IColorPerfManager = new OplusIndex("IColorPerfManager", 72);
      IColorAthenaAmManager = new OplusIndex("IColorAthenaAmManager", 73);
      IColorAthenaManager = new OplusIndex("IColorAthenaManager", 74);
      IColorWatermarkManager = new OplusIndex("IColorWatermarkManager", 75);
      IColorOtaDataManager = new OplusIndex("IColorOtaDataManager", 76);
      IColorBreenoManager = new OplusIndex("IColorBreenoManager", 77);
      IOplusFullScreenDisplayManager = new OplusIndex("IOplusFullScreenDisplayManager", 78);
      IColorEapManager = new OplusIndex("IColorEapManager", 79);
      IBatteryIdleController = new OplusIndex("IBatteryIdleController", 80);
      IColorStartingWindowManager = new OplusIndex("IColorStartingWindowManager", 81);
      IOplusDarkModeMaskManager = new OplusIndex("IOplusDarkModeMaskManager", 82);
      IOplusDarkModeServiceManager = new OplusIndex("IOplusDarkModeServiceManager", 83);
      IColorEdgeTouchManager = new OplusIndex("IColorEdgeTouchManager", 84);
      IColorConfineModeManager = new OplusIndex("IColorConfineModeManager", 85);
      IColorKeyLayoutManager = new OplusIndex("IColorKeyLayoutManager", 86);
      IColorPmsSupportedFunctionManager = new OplusIndex("IColorPmsSupportedFunctionManager", 87);
      IColorPermSupportedFunctionManager = new OplusIndex("IColorPermSupportedFunctionManager", 88);
      IColorStorageAllFileAccessManager = new OplusIndex("IColorStorageAllFileAccessManager", 89);
      IColorSaveSurfaceManager = new OplusIndex("IColorSaveSurfaceManager", 90);
      IColorKeyEventManager = new OplusIndex("IColorKeyEventManager", 91);
      IColorWindowAnimationManager = new OplusIndex("IColorWindowAnimationManager", 92);
      IColorMergedProcessSplitManager = new OplusIndex("IColorMergedProcessSplitManager", 93);
      IOplusMirageDisplayManager = new OplusIndex("IOplusMirageDisplayManager", 94);
      IColorSemlessAnimationManager = new OplusIndex("IColorSemlessAnimationManager", 95);
      IColorTouchNodeManager = new OplusIndex("IColorTouchNodeManager", 96);
      IColorResourcePreloadManager = new OplusIndex("IColorResourcePreloadManager", 97);
      IColorAppDetectManager = new OplusIndex("IColorAppDetectManager", 98);
      IOplusGlobalDragAndDropManager = new OplusIndex("IOplusGlobalDragAndDropManager", 99);
      IOplusAppOpsManager = new OplusIndex("IOplusAppOpsManager", 100);
      IColorVFXScreenEffectFeature = new OplusIndex("IColorVFXScreenEffectFeature", 101);
      IOplusDeepThinkerExService = new OplusIndex("IOplusDeepThinkerExService", 102);
      IOplusScreenFrozenManager = new OplusIndex("IOplusScreenFrozenManager", 103);
      IOplusBootPressureHolder = new OplusIndex("IOplusBootPressureHolder", 104);
      IColorAbnormalComponentManager = new OplusIndex("IColorAbnormalComponentManager", 105);
      IOplusVerificationCodeController = new OplusIndex("IOplusVerificationCodeController", 106);
      EndColorServiceFactory = new OplusIndex("EndColorServiceFactory", 107);
      StartColorFrameworkFactory = new OplusIndex("StartColorFrameworkFactory", 108);
      IOplusViewRootUtil = new OplusIndex("IOplusViewRootUtil", 109);
      IOplusFontManager = new OplusIndex("IOplusFontManager", 110);
      IOplusFavoriteManager = new OplusIndex("IOplusFavoriteManager", 111);
      IOplusDarkModeManager = new OplusIndex("IOplusDarkModeManager", 112);
      IOplusDirectViewHelper = new OplusIndex("IOplusDirectViewHelper", 113);
      IOplusCommonInjector = new OplusIndex("IOplusCommonInjector", 114);
      IOplusTextViewRTLUtilForUG = new OplusIndex("IOplusTextViewRTLUtilForUG", 115);
      IOplusViewHooks = new OplusIndex("IOplusViewHooks", 116);
      IOplusScreenShotEuclidManager = new OplusIndex("IOplusScreenShotEuclidManager", 117);
      IOplusInputMethodServiceUtils = new OplusIndex("IOplusInputMethodServiceUtils", 118);
      IOplusResolverManager = new OplusIndex("IOplusResolverManager", 119);
      IOplusThemeManager = new OplusIndex("IOplusThemeManager", 120);
      IOplusAccidentallyTouchHelper = new OplusIndex("IOplusAccidentallyTouchHelper", 121);
      IOplusOverScrollerHelper = new OplusIndex("IOplusOverScrollerHelper", 122);
      IOplusListHooks = new OplusIndex("IOplusListHooks", 123);
      IOplusGradientHooks = new OplusIndex("IOplusGradientHooks", 124);
      ITextJustificationHooks = new OplusIndex("ITextJustificationHooks", 125);
      IOplusFtHooks = new OplusIndex("IOplusFtHooks", 126);
      IOplusMagnifierHooks = new OplusIndex("IOplusMagnifierHooks", 127);
      IOplusNotificationUiManager = new OplusIndex("IOplusNotificationUiManager", 128);
      IOplusAlertControllerEuclidManager = new OplusIndex("IOplusAlertControllerEuclidManager", 129);
      IOplusDeepThinkerManager = new OplusIndex("IOplusDeepThinkerManager", 130);
      IOplusBurmeseZgHooks = new OplusIndex("IOplusBurmeseZgHooks", 131);
      IOplusViewConfigHelper = new OplusIndex("IOplusViewConfigHelper", 132);
      IColorDexMetadataManager = new OplusIndex("IColorDexMetadataManager", 133);
      IOplusThemeStyle = new OplusIndex("IOplusThemeStyle", 134);
      IOplusIconPackManager = new OplusIndex("IOplusIconPackManager", 135);
      IColorEyeProtectManager = new OplusIndex("IColorEyeProtectManager", 136);
      IOplusAppDynamicFeatureManager = new OplusIndex("IOplusAppDynamicFeatureManager", 137);
      IOppoListManager = new OplusIndex("IOppoListManager", 138);
      IOplusFloatingToolbarUtil = new OplusIndex("IOplusFloatingToolbarUtil", 139);
      IOplusDragTextShadowHelper = new OplusIndex("IOplusDragTextShadowHelper", 140);
      EndColorFrameworkFactory = new OplusIndex("EndColorFrameworkFactory", 141);
      StartOplusServiceFactory = new OplusIndex("StartOplusServiceFactory", 142);
      IOplusLocationBlacklistUtil = new OplusIndex("IOplusLocationBlacklistUtil", 143);
      IOplusNavigationStatusController = new OplusIndex("IOplusNavigationStatusController", 144);
      IOplusGnssWhiteListProxy = new OplusIndex("IOplusGnssWhiteListProxy", 145);
      IOplusNlpProxy = new OplusIndex("IOplusNlpProxy", 146);
      IOplusSuplController = new OplusIndex("IOplusSuplController", 147);
      IOplusGnssDiagnosticTool = new OplusIndex("IOplusGnssDiagnosticTool", 148);
      IOplusLbsCustomize = new OplusIndex("IOplusLbsCustomize", 149);
      IOplusLbsRepairer = new OplusIndex("IOplusLbsRepairer", 150);
      IOplusLbsRomUpdateUtil = new OplusIndex("IOplusLbsRomUpdateUtil", 151);
      IOplusGnssDuration = new OplusIndex("IOplusGnssDuration", 152);
      IOplusFastNetworkLocation = new OplusIndex("IOplusFastNetworkLocation", 153);
      IOplusCoarseToFine = new OplusIndex("IOplusCoarseToFine", 154);
      IOplusLocationStatistics = new OplusIndex("IOplusLocationStatistics", 155);
      IOplusLocationStatusMonitor = new OplusIndex("IOplusLocationStatusMonitor", 156);
      IOplusLBSMainClass = new OplusIndex("IOplusLBSMainClass", 157);
      IOplusLocationCache = new OplusIndex("IOplusLocationCache", 158);
      IOplusHighFrequencyLocationBlacklist = new OplusIndex("IOplusHighFrequencyLocationBlacklist", 159);
      IOplusSystemServerEx = new OplusIndex("IOplusSystemServerEx", 160);
      IOplusActivityManagerServiceEx = new OplusIndex("IOplusActivityManagerServiceEx", 161);
      IOplusActivityTaskManagerServiceEx = new OplusIndex("IOplusActivityTaskManagerServiceEx", 162);
      IOplusPackageManagerServiceEx = new OplusIndex("IOplusPackageManagerServiceEx", 163);
      IOplusPackageManagerNativeEx = new OplusIndex("IOplusPackageManagerNativeEx", 164);
      IOplusWindowManagerServiceEx = new OplusIndex("IOplusWindowManagerServiceEx", 165);
      IOplusPowerManagerServiceEx = new OplusIndex("IOplusPowerManagerServiceEx", 166);
      IOplusDisplayPowerControllerFeature = new OplusIndex("IOplusDisplayPowerControllerFeature", 167);
      IOplusFeatureAOD = new OplusIndex("IOplusFeatureAOD", 168);
      IOplusHeadsetFadeIn = new OplusIndex("IOplusHeadsetFadeIn", 169);
      IOPPODnsSelfrecoveryEngine = new OplusIndex("IOPPODnsSelfrecoveryEngine", 170);
      IWifiRomUpdateHelper = new OplusIndex("IWifiRomUpdateHelper", 171);
      IOplusWifiGlobalMethod = new OplusIndex("IOplusWifiGlobalMethod", 172);
      IOppoConnectivityServiceHelperUtils = new OplusIndex("IOppoConnectivityServiceHelperUtils", 173);
      IOppoWifiDisplayUsageHelper = new OplusIndex("IOppoWifiDisplayUsageHelper", 174);
      IOppoDnsManagerHelper = new OplusIndex("IOppoDnsManagerHelper", 175);
      IOppoAutoConCaptivePortalControl = new OplusIndex("IOppoAutoConCaptivePortalControl", 176);
      IOppoNetworkNotificationManager = new OplusIndex("IOppoNetworkNotificationManager", 177);
      IOplusWifiNetworkConfig = new OplusIndex("IOplusWifiNetworkConfig", 178);
      IOppoVpnHelper = new OplusIndex("IOppoVpnHelper", 179);
      IOplusShutdownFeature = new OplusIndex("IOplusShutdownFeature", 180);
      IOppoBrightness = new OplusIndex("IOppoBrightness", 181);
      IOplusFeatureReduceBrightness = new OplusIndex("IOplusFeatureReduceBrightness", 182);
      IOplusFeatureBrightness = new OplusIndex("IOplusFeatureBrightness", 183);
      IOplusFeatureColorMode = new OplusIndex("IOplusFeatureColorMode", 184);
      IOplusFeatureDCBacklight = new OplusIndex("IOplusFeatureDCBacklight", 185);
      IOplusFeatureHDREnhanceBrightness = new OplusIndex("IOplusFeatureHDREnhanceBrightness", 186);
      IOppoUsbDeviceFeature = new OplusIndex("IOppoUsbDeviceFeature", 187);
      IOppoVibratorFeature = new OplusIndex("IOppoVibratorFeature", 188);
      IOppoStorageManagerFeature = new OplusIndex("IOppoStorageManagerFeature", 189);
      IOppoBatteryServiceFeature = new OplusIndex("IOppoBatteryServiceFeature", 190);
      IOplusNewNetworkTimeUpdateServiceFeature = new OplusIndex("IOplusNewNetworkTimeUpdateServiceFeature", 191);
      IOplusActivityManagerDynamicLogConfigFeature = new OplusIndex("IOplusActivityManagerDynamicLogConfigFeature", 192);
      IOplusArmyControllerFeatrue = new OplusIndex("IOplusArmyControllerFeatrue", 193);
      IOplusArmyServiceFeatrue = new OplusIndex("IOplusArmyServiceFeatrue", 194);
      IOplusAmsUtilsFeatrue = new OplusIndex("IOplusAmsUtilsFeatrue", 195);
      IOppoScreenModeManagerFeature = new OplusIndex("IOppoScreenModeManagerFeature", 196);
      ICompatibilityHelper = new OplusIndex("ICompatibilityHelper", 197);
      IOplusPowerManagerServiceFeature = new OplusIndex("IOplusPowerManagerServiceFeature", 198);
      IOplusFeatureConfigManagerInternal = new OplusIndex("IOplusFeatureConfigManagerInternal", 199);
      IOplusEngineerService = new OplusIndex("IOplusEngineerService", 200);
      IOplusScreenCastContentManager = new OplusIndex("IOplusScreenCastContentManager", 201);
      IOplusScreenFrozenBooster = new OplusIndex("IOplusScreenFrozenBooster", 202);
      IOplusKeepAliveManager = new OplusIndex("IOplusKeepAliveManager", 203);
      IOplusDualHeadPhoneFeature = new OplusIndex("IOplusDualHeadPhoneFeature", 204);
      IOplusDexOptimizeManager = new OplusIndex("IOplusDexOptimizeManager", 205);
      IOplusOptimizingProgressManager = new OplusIndex("IOplusOptimizingProgressManager", 206);
      IOplusScreenOffTorchHelper = new OplusIndex("IOplusScreenOffTorchHelper", 207);
      IOplusBackgroundTaskManagerService = new OplusIndex("IOplusBackgroundTaskManagerService", 208);
      IFreezeManagerHelp = new OplusIndex("IFreezeManagerHelp", 209);
      IFreezeManagerService = new OplusIndex("IFreezeManagerService", 210);
      IOplusSecurityAnalysisBroadCastSender = new OplusIndex("IOplusSecurityAnalysisBroadCastSender", 211);
      IOplusGestureUpBarPostionHelper = new OplusIndex("IOplusGestureUpBarPostionHelper", 212);
      EndOplusServiceFactory = new OplusIndex("EndOplusServiceFactory", 213);
      StartOplusFrameworkFactory = new OplusIndex("StartOplusFrameworkFactory", 214);
      IOplusZenModeFeature = new OplusIndex("IOplusZenModeFeature", 215);
      IOplusScreenModeFeature = new OplusIndex("IOplusScreenModeFeature", 216);
      IOplusDrmDecoderFeature = new OplusIndex("IOplusDrmDecoderFeature", 217);
      IOplusCpuExceptionMonitor = new OplusIndex("IOplusCpuExceptionMonitor", 218);
      IOplusRotationOptimization = new OplusIndex("IOplusRotationOptimization", 219);
      IOplusRedPacketManager = new OplusIndex("IOplusRedPacketManager", 220);
      IOplusUIFirstManager = new OplusIndex("IOplusUIFirstManager", 221);
      IOplusNwPowerManager = new OplusIndex("IOplusNwPowerManager", 222);
      EndOplusFrameworkFactory = new OplusIndex("EndOplusFrameworkFactory", 223);
      StartOplusJobSchedulerServiceFactory = new OplusIndex("StartOplusJobSchedulerServiceFactory", 224);
      IOplusJobTest = new OplusIndex("IOplusJobTest", 225);
      IOplusJobSchedulerSystemServerEx = new OplusIndex("IOplusJobSchedulerSystemServerEx", 226);
      IOplusJobSchedulerServiceEx = new OplusIndex("IOplusJobSchedulerServiceEx", 227);
      IJobCountPolicy = new OplusIndex("IJobCountPolicy", 228);
      IColorJobScheduleManager = new OplusIndex("IColorJobScheduleManager", 229);
      IColorDeviceIdleHelper = new OplusIndex("IColorDeviceIdleHelper", 230);
      IColorGoogleDozeRestrict = new OplusIndex("IColorGoogleDozeRestrict", 231);
      IColorDeviceIdleControllerEx = new OplusIndex("IColorDeviceIdleControllerEx", 232);
      IColorSmartDozeHelper = new OplusIndex("IColorSmartDozeHelper", 233);
      EndOplusJobSchedulerServiceFactory = new OplusIndex("EndOplusJobSchedulerServiceFactory", 234);
      StartColorInternalServiceFactory = new OplusIndex("StartColorInternalServiceFactory", 235);
      IColorHansManager = new OplusIndex("IColorHansManager", 236);
      IColorCommonListManager = new OplusIndex("IColorCommonListManager", 237);
      IColorSysStateManager = new OplusIndex("IColorSysStateManager", 238);
      IColorAlarmWakeupDetection = new OplusIndex("IColorAlarmWakeupDetection", 239);
      IColorAlarmManagerServiceHelper = new OplusIndex("IColorAlarmManagerServiceHelper", 240);
      EndColorInternalServiceFactory = new OplusIndex("EndColorInternalServiceFactory", 241);
      IOplusResolverStyle = new OplusIndex("IOplusResolverStyle", 242);
      OplusIndex oplusIndex = new OplusIndex("End", 243);
      $VALUES = new OplusIndex[] { 
          StartColorServiceFactory, IColorSystemServerEx, IColorActivityManagerServiceEx, IColorActivityTaskManagerServiceEx, IColorPackageManagerServiceEx, IColorPermissionManagerServiceEx, IColorWindowManagerServiceEx, IColorPowerManagerServiceEx, IColorAlarmManagerServiceEx, IOpusJobSchedulerServiceEx, 
          IOplusNotificationManagerServiceEx, IColorOverlayManagerServiceEx, IColorInputMethodManagerServiceEx, IColorDisplayManagerServiceEx, IColorNetworkPolicyManagerServiceEx, IColorDisplayPolicyEx, IColorDataFreeManager, IColorFullmodeManager, IColorIconCachesManager, IColorAppStartupManager, 
          IColorChildrenModeInstallManager, IColorAppStoreTraffic, IColorDynamicFeatureManager, IColorAppCrashClearManager, IColorAppSwitchManager, IColorBroadcastStaticRegisterWhitelistManager, IColorSellModeManager, IColorAppInstallProgressManager, IColorGameSpaceManager, IOplusAppConfigManager, 
          IColorScreenOffOptimization, IColorSplitWindowManager, IColorMultiAppManager, IColorBatterySaveExtend, IColorGuardElfFeature, IColorAppListInterceptManager, IColorAccessControlLocalManager, IColorLockTaskController, IColorDeepSleepHelper, IColorAppQuickFreezeManager, 
          IColorRuntimePermGrantPolicyManager, IColorDefaultAppPolicyManager, IColorDozeNetworkOptimization, IColorClearDataProtectManager, IColorGoogleAlarmRestrict, IColorPackageInstallStatisticManager, IColorAlarmManagerHelper, IColorSecurePayManager, IColorPackageInstallInterceptManager, IColorSensitivePermGrantPolicyManager, 
          IColorInstallThreadsControlManager, IColorThirdPartyAppSignCheckManager, IColorBroadcastManager, IColorForbidUninstallAppManager, IColorStrictModeManager, IColorFastAppManager, IColorAlarmAlignment, IColorSecurityPermissionManager, IColorAlarmTempWhitelist, IColorDynamicLogManager, 
          IColorAIBrightManager, IOplusLanguageManager, IColorForbidHideOrDisableManager, IOplusLanguageEnableManager, IColorPkgStartInfoManager, IColorFixupDataManager, IColorRemovableAppManager, IColorSystemAppProtectManager, IColorSilentRebootManager, IColorWakeLockCheck, 
          IColorFreeformManager, IColorZoomWindowManager, IColorPerfManager, IColorAthenaAmManager, IColorAthenaManager, IColorWatermarkManager, IColorOtaDataManager, IColorBreenoManager, IOplusFullScreenDisplayManager, IColorEapManager, 
          IBatteryIdleController, IColorStartingWindowManager, IOplusDarkModeMaskManager, IOplusDarkModeServiceManager, IColorEdgeTouchManager, IColorConfineModeManager, IColorKeyLayoutManager, IColorPmsSupportedFunctionManager, IColorPermSupportedFunctionManager, IColorStorageAllFileAccessManager, 
          IColorSaveSurfaceManager, IColorKeyEventManager, IColorWindowAnimationManager, IColorMergedProcessSplitManager, IOplusMirageDisplayManager, IColorSemlessAnimationManager, IColorTouchNodeManager, IColorResourcePreloadManager, IColorAppDetectManager, IOplusGlobalDragAndDropManager, 
          IOplusAppOpsManager, IColorVFXScreenEffectFeature, IOplusDeepThinkerExService, IOplusScreenFrozenManager, IOplusBootPressureHolder, IColorAbnormalComponentManager, IOplusVerificationCodeController, EndColorServiceFactory, StartColorFrameworkFactory, IOplusViewRootUtil, 
          IOplusFontManager, IOplusFavoriteManager, IOplusDarkModeManager, IOplusDirectViewHelper, IOplusCommonInjector, IOplusTextViewRTLUtilForUG, IOplusViewHooks, IOplusScreenShotEuclidManager, IOplusInputMethodServiceUtils, IOplusResolverManager, 
          IOplusThemeManager, IOplusAccidentallyTouchHelper, IOplusOverScrollerHelper, IOplusListHooks, IOplusGradientHooks, ITextJustificationHooks, IOplusFtHooks, IOplusMagnifierHooks, IOplusNotificationUiManager, IOplusAlertControllerEuclidManager, 
          IOplusDeepThinkerManager, IOplusBurmeseZgHooks, IOplusViewConfigHelper, IColorDexMetadataManager, IOplusThemeStyle, IOplusIconPackManager, IColorEyeProtectManager, IOplusAppDynamicFeatureManager, IOppoListManager, IOplusFloatingToolbarUtil, 
          IOplusDragTextShadowHelper, EndColorFrameworkFactory, StartOplusServiceFactory, IOplusLocationBlacklistUtil, IOplusNavigationStatusController, IOplusGnssWhiteListProxy, IOplusNlpProxy, IOplusSuplController, IOplusGnssDiagnosticTool, IOplusLbsCustomize, 
          IOplusLbsRepairer, IOplusLbsRomUpdateUtil, IOplusGnssDuration, IOplusFastNetworkLocation, IOplusCoarseToFine, IOplusLocationStatistics, IOplusLocationStatusMonitor, IOplusLBSMainClass, IOplusLocationCache, IOplusHighFrequencyLocationBlacklist, 
          IOplusSystemServerEx, IOplusActivityManagerServiceEx, IOplusActivityTaskManagerServiceEx, IOplusPackageManagerServiceEx, IOplusPackageManagerNativeEx, IOplusWindowManagerServiceEx, IOplusPowerManagerServiceEx, IOplusDisplayPowerControllerFeature, IOplusFeatureAOD, IOplusHeadsetFadeIn, 
          IOPPODnsSelfrecoveryEngine, IWifiRomUpdateHelper, IOplusWifiGlobalMethod, IOppoConnectivityServiceHelperUtils, IOppoWifiDisplayUsageHelper, IOppoDnsManagerHelper, IOppoAutoConCaptivePortalControl, IOppoNetworkNotificationManager, IOplusWifiNetworkConfig, IOppoVpnHelper, 
          IOplusShutdownFeature, IOppoBrightness, IOplusFeatureReduceBrightness, IOplusFeatureBrightness, IOplusFeatureColorMode, IOplusFeatureDCBacklight, IOplusFeatureHDREnhanceBrightness, IOppoUsbDeviceFeature, IOppoVibratorFeature, IOppoStorageManagerFeature, 
          IOppoBatteryServiceFeature, IOplusNewNetworkTimeUpdateServiceFeature, IOplusActivityManagerDynamicLogConfigFeature, IOplusArmyControllerFeatrue, IOplusArmyServiceFeatrue, IOplusAmsUtilsFeatrue, IOppoScreenModeManagerFeature, ICompatibilityHelper, IOplusPowerManagerServiceFeature, IOplusFeatureConfigManagerInternal, 
          IOplusEngineerService, IOplusScreenCastContentManager, IOplusScreenFrozenBooster, IOplusKeepAliveManager, IOplusDualHeadPhoneFeature, IOplusDexOptimizeManager, IOplusOptimizingProgressManager, IOplusScreenOffTorchHelper, IOplusBackgroundTaskManagerService, IFreezeManagerHelp, 
          IFreezeManagerService, IOplusSecurityAnalysisBroadCastSender, IOplusGestureUpBarPostionHelper, EndOplusServiceFactory, StartOplusFrameworkFactory, IOplusZenModeFeature, IOplusScreenModeFeature, IOplusDrmDecoderFeature, IOplusCpuExceptionMonitor, IOplusRotationOptimization, 
          IOplusRedPacketManager, IOplusUIFirstManager, IOplusNwPowerManager, EndOplusFrameworkFactory, StartOplusJobSchedulerServiceFactory, IOplusJobTest, IOplusJobSchedulerSystemServerEx, IOplusJobSchedulerServiceEx, IJobCountPolicy, IColorJobScheduleManager, 
          IColorDeviceIdleHelper, IColorGoogleDozeRestrict, IColorDeviceIdleControllerEx, IColorSmartDozeHelper, EndOplusJobSchedulerServiceFactory, StartColorInternalServiceFactory, IColorHansManager, IColorCommonListManager, IColorSysStateManager, IColorAlarmWakeupDetection, 
          IColorAlarmManagerServiceHelper, EndColorInternalServiceFactory, IOplusResolverStyle, oplusIndex };
    }
  }
}
