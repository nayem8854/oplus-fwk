package android.common;

public interface IOplusCommonFactory {
  <T extends IOplusCommonFeature> T getFeature(T paramT, Object... paramVarArgs) {
    verityParams(paramT);
    return paramT;
  }
  
  boolean isValid(int paramInt);
  
  default <T extends IOplusCommonFeature> void verityParams(T paramT) {
    if (paramT != null) {
      if (paramT.index() != OplusFeatureList.OplusIndex.End)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramT);
      stringBuilder.append("must override index() method");
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    throw new IllegalArgumentException("def can not be null");
  }
  
  void verityParamsType(String paramString, Object[] paramArrayOfObject, int paramInt, Class... paramVarArgs) {
    StringBuilder stringBuilder1;
    if (paramArrayOfObject != null && paramVarArgs != null && paramArrayOfObject.length == paramInt && paramVarArgs.length == paramInt) {
      for (byte b = 0; b < paramInt; ) {
        if (paramArrayOfObject[b] == null || paramVarArgs[b].isInstance(paramArrayOfObject[b])) {
          b++;
          continue;
        } 
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append(paramVarArgs[b].getName());
        stringBuilder1.append(" is not instance ");
        stringBuilder1.append(paramArrayOfObject[b]);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      return;
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append((String)stringBuilder1);
    stringBuilder2.append(" need +");
    stringBuilder2.append(paramInt);
    stringBuilder2.append(" params");
    throw new IllegalArgumentException(stringBuilder2.toString());
  }
}
