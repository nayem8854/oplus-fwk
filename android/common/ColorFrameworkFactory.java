package android.common;

import android.app.IOplusCommonInjector;
import android.content.Context;
import android.content.res.IOplusThemeManager;
import android.content.res.Resources;
import android.inputmethodservice.IOplusInputMethodServiceUtils;
import android.text.ITextJustificationHooks;
import android.util.Slog;
import android.view.IOplusAccidentallyTouchHelper;
import android.view.IOplusBurmeseZgHooks;
import android.view.IOplusDirectViewHelper;
import android.view.IOplusViewHooks;
import android.view.IOplusViewRootUtil;
import android.view.View;
import android.view.ViewRootImpl;
import android.widget.IOplusDragTextShadowHelper;
import android.widget.IOplusFtHooks;
import android.widget.IOplusListHooks;
import android.widget.IOplusMagnifierHooks;
import android.widget.IOplusOverScrollerHelper;
import android.widget.IOplusTextViewRTLUtilForUG;
import android.widget.OverScroller;
import com.android.internal.app.IOplusAlertControllerEuclidManager;
import com.android.internal.app.IOplusResolverManager;
import com.android.internal.app.IOplusResolverStyle;
import com.oplus.app.IOplusAppDynamicFeatureManager;
import com.oplus.darkmode.IOplusDarkModeManager;
import com.oplus.deepthinker.IOplusDeepThinkerManager;
import com.oplus.favorite.IOplusFavoriteManager;
import com.oplus.font.IOplusFontManager;
import com.oplus.multiapp.IOplusMultiApp;
import com.oplus.screenshot.IOplusScreenShotEuclidManager;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;

public class ColorFrameworkFactory extends OplusFrameworkFactory implements IOplusCommonFactory {
  public static final String COLOR_FRAMEWORK_FACTORY_IMPL_NAME = "oplus.android.OplusFrameworkFactoryImpl";
  
  private static final boolean DEBUG = true;
  
  private static final String TAG = "ColorFrameworkFactory";
  
  private static volatile ColorFrameworkFactory sInstance = null;
  
  public static ColorFrameworkFactory getInstance() {
    // Byte code:
    //   0: getstatic android/common/ColorFrameworkFactory.sInstance : Landroid/common/ColorFrameworkFactory;
    //   3: ifnonnull -> 90
    //   6: ldc android/common/ColorFrameworkFactory
    //   8: monitorenter
    //   9: getstatic android/common/ColorFrameworkFactory.sInstance : Landroid/common/ColorFrameworkFactory;
    //   12: astore_0
    //   13: aload_0
    //   14: ifnonnull -> 78
    //   17: ldc 'oplus.android.OplusFrameworkFactoryImpl'
    //   19: invokestatic newInstance : (Ljava/lang/String;)Ljava/lang/Object;
    //   22: checkcast android/common/ColorFrameworkFactory
    //   25: putstatic android/common/ColorFrameworkFactory.sInstance : Landroid/common/ColorFrameworkFactory;
    //   28: goto -> 78
    //   31: astore_1
    //   32: new java/lang/StringBuilder
    //   35: astore_0
    //   36: aload_0
    //   37: invokespecial <init> : ()V
    //   40: aload_0
    //   41: ldc ' Reflect exception getInstance: '
    //   43: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   46: pop
    //   47: aload_0
    //   48: aload_1
    //   49: invokevirtual toString : ()Ljava/lang/String;
    //   52: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: ldc 'ColorFrameworkFactory'
    //   58: aload_0
    //   59: invokevirtual toString : ()Ljava/lang/String;
    //   62: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   65: pop
    //   66: new android/common/ColorFrameworkFactory
    //   69: astore_0
    //   70: aload_0
    //   71: invokespecial <init> : ()V
    //   74: aload_0
    //   75: putstatic android/common/ColorFrameworkFactory.sInstance : Landroid/common/ColorFrameworkFactory;
    //   78: ldc android/common/ColorFrameworkFactory
    //   80: monitorexit
    //   81: goto -> 90
    //   84: astore_0
    //   85: ldc android/common/ColorFrameworkFactory
    //   87: monitorexit
    //   88: aload_0
    //   89: athrow
    //   90: getstatic android/common/ColorFrameworkFactory.sInstance : Landroid/common/ColorFrameworkFactory;
    //   93: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #81	-> 0
    //   #82	-> 6
    //   #83	-> 9
    //   #85	-> 17
    //   #89	-> 28
    //   #86	-> 31
    //   #87	-> 32
    //   #88	-> 66
    //   #91	-> 78
    //   #93	-> 90
    // Exception table:
    //   from	to	target	type
    //   9	13	84	finally
    //   17	28	31	java/lang/Exception
    //   17	28	84	finally
    //   32	66	84	finally
    //   66	78	84	finally
    //   78	81	84	finally
    //   85	88	84	finally
  }
  
  public boolean isValid(int paramInt) {
    // Byte code:
    //   0: getstatic android/common/OplusFeatureList$OplusIndex.EndOplusFrameworkFactory : Landroid/common/OplusFeatureList$OplusIndex;
    //   3: invokevirtual ordinal : ()I
    //   6: istore_2
    //   7: iconst_1
    //   8: istore_3
    //   9: iload_1
    //   10: iload_2
    //   11: if_icmpge -> 33
    //   14: getstatic android/common/OplusFeatureList$OplusIndex.StartOplusFrameworkFactory : Landroid/common/OplusFeatureList$OplusIndex;
    //   17: astore #4
    //   19: iload_1
    //   20: aload #4
    //   22: invokevirtual ordinal : ()I
    //   25: if_icmple -> 33
    //   28: iconst_1
    //   29: istore_2
    //   30: goto -> 35
    //   33: iconst_0
    //   34: istore_2
    //   35: iload_1
    //   36: getstatic android/common/OplusFeatureList$OplusIndex.EndColorFrameworkFactory : Landroid/common/OplusFeatureList$OplusIndex;
    //   39: invokevirtual ordinal : ()I
    //   42: if_icmpge -> 64
    //   45: getstatic android/common/OplusFeatureList$OplusIndex.StartColorFrameworkFactory : Landroid/common/OplusFeatureList$OplusIndex;
    //   48: astore #4
    //   50: iload_1
    //   51: aload #4
    //   53: invokevirtual ordinal : ()I
    //   56: if_icmple -> 64
    //   59: iconst_1
    //   60: istore_1
    //   61: goto -> 66
    //   64: iconst_0
    //   65: istore_1
    //   66: iload_3
    //   67: istore #5
    //   69: iload_1
    //   70: ifne -> 86
    //   73: iload_2
    //   74: ifeq -> 83
    //   77: iload_3
    //   78: istore #5
    //   80: goto -> 86
    //   83: iconst_0
    //   84: istore #5
    //   86: iload #5
    //   88: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #98	-> 0
    //   #99	-> 19
    //   #100	-> 35
    //   #101	-> 50
    //   #103	-> 66
    //   #104	-> 86
  }
  
  static Object newInstance(String paramString) throws Exception {
    Class<?> clazz = Class.forName(paramString);
    Constructor<?> constructor = clazz.getConstructor(new Class[0]);
    return constructor.newInstance(new Object[0]);
  }
  
  public IOplusViewRootUtil getOplusViewRootUtil() {
    warn("getOplusViewRootUtil dummy");
    return IOplusViewRootUtil.DEFAULT;
  }
  
  public IOplusFontManager getOplusFontManager() {
    warn("getOplusFontManager dummy");
    return IOplusFontManager.DEFAULT;
  }
  
  public IOplusFavoriteManager getOplusFavoriteManager() {
    warn("getOplusFavoriteManager dummy");
    return IOplusFavoriteManager.DEFAULT;
  }
  
  public IOplusDarkModeManager getOplusDarkModeManager() {
    warn("getOplusDarkModeManager dummy");
    return IOplusDarkModeManager.DEFAULT;
  }
  
  public IOplusDarkModeManager newOplusDarkModeManager() {
    warn("newOplusDarkModeManager dummy");
    return IOplusDarkModeManager.DEFAULT;
  }
  
  public IOplusDirectViewHelper getOplusDirectViewHelper(WeakReference<ViewRootImpl> paramWeakReference) {
    warn("getOplusDirectViewHelper dummy");
    return IOplusDirectViewHelper.DEFAULT;
  }
  
  public IOplusCommonInjector getOplusCommonInjector() {
    return IOplusCommonInjector.DEFAULT;
  }
  
  public IOplusTextViewRTLUtilForUG getOplusTextViewRTLUtilForUG() {
    return IOplusTextViewRTLUtilForUG.DEFAULT;
  }
  
  public IOplusViewHooks getOplusViewHooks(View paramView, Resources paramResources) {
    return IOplusViewHooks.DEFAULT;
  }
  
  public IOplusScreenShotEuclidManager getOplusScreenShotEuclidManager() {
    return IOplusScreenShotEuclidManager.DEFAULT;
  }
  
  public IOplusInputMethodServiceUtils getOplusInputMethodServiceUtils() {
    warn("getOplusViewRootUtil dummy");
    return IOplusInputMethodServiceUtils.DEFAULT;
  }
  
  public IOplusResolverManager getOplusResolverManager() {
    warn("getOplusResolverManager dummy");
    return IOplusResolverManager.DEFAULT;
  }
  
  public IOplusResolverStyle getOplusResolverStyle() {
    warn("getOplusResolverStyle dummy");
    return IOplusResolverStyle.DEFAULT;
  }
  
  public IOplusThemeManager getOplusThemeManager() {
    warn("getOplusThemeManager dummy");
    return IOplusThemeManager.DEFAULT;
  }
  
  public IOplusAccidentallyTouchHelper getOplusAccidentallyTouchHelper() {
    return IOplusAccidentallyTouchHelper.DEFAULT;
  }
  
  public IOplusOverScrollerHelper getOplusOverScrollerHelper(OverScroller paramOverScroller) {
    warn("getOplusOverScrollerHelper dummy");
    return IOplusOverScrollerHelper.DEFAULT;
  }
  
  public IOplusListHooks getOplusListHooks() {
    return IOplusListHooks.DEFAULT;
  }
  
  public ITextJustificationHooks getTextJustificationHooks() {
    return ITextJustificationHooks.DEFAULT;
  }
  
  public IOplusFtHooks getOplusFtHooks() {
    return IOplusFtHooks.DEFAULT;
  }
  
  public IOplusMagnifierHooks getColorMagnifierHooks() {
    return IOplusMagnifierHooks.DEFAULT;
  }
  
  public IOplusAlertControllerEuclidManager getOplusAlertControllerEuclidManger() {
    return IOplusAlertControllerEuclidManager.DEFAULT;
  }
  
  public IOplusDeepThinkerManager getOplusDeepThinkerManager(Context paramContext) {
    return IOplusDeepThinkerManager.DEFAULT;
  }
  
  public IOplusBurmeseZgHooks getOplusBurmeseZgFlagHooks() {
    return IOplusBurmeseZgHooks.DEFAULT;
  }
  
  public IOplusMultiApp getOplusMultiApp() {
    return IOplusMultiApp.DEFAULT;
  }
  
  public IOplusAppDynamicFeatureManager getOplusAppDynamicFeatureManager() {
    return IOplusAppDynamicFeatureManager.DEFAULT;
  }
  
  protected void warn(String paramString) {
    Slog.w("ColorFrameworkFactory", paramString);
  }
  
  public IOplusDragTextShadowHelper getDragTextShadowHelper() {
    return IOplusDragTextShadowHelper.DEFAULT;
  }
}
