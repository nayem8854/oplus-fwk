package android.common;

import android.content.Context;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;

public class OplusFeatureManager {
  private static final String TAG = "ColorFeatureManager";
  
  private static final boolean[] mFeatureDisable;
  
  private static final boolean[] mFeatureTraing;
  
  private static final HashMap<String, Boolean> sFeatureSwitchMap;
  
  private static final HashMap<String, Boolean> sFeatureTraceMap;
  
  private static OplusFeatureManager sInstance = null;
  
  static {
    sFeatureSwitchMap = new HashMap<>();
    sFeatureTraceMap = new HashMap<>();
    mFeatureDisable = new boolean[OplusFeatureList.OplusIndex.End.ordinal()];
    mFeatureTraing = new boolean[OplusFeatureList.OplusIndex.End.ordinal()];
  }
  
  public OplusFeatureManager getInstance() {
    // Byte code:
    //   0: getstatic android/common/OplusFeatureManager.sInstance : Landroid/common/OplusFeatureManager;
    //   3: ifnonnull -> 39
    //   6: ldc android/common/OplusFeatureManager
    //   8: monitorenter
    //   9: getstatic android/common/OplusFeatureManager.sInstance : Landroid/common/OplusFeatureManager;
    //   12: ifnonnull -> 27
    //   15: new android/common/OplusFeatureManager
    //   18: astore_1
    //   19: aload_1
    //   20: invokespecial <init> : ()V
    //   23: aload_1
    //   24: putstatic android/common/OplusFeatureManager.sInstance : Landroid/common/OplusFeatureManager;
    //   27: ldc android/common/OplusFeatureManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_1
    //   34: ldc android/common/OplusFeatureManager
    //   36: monitorexit
    //   37: aload_1
    //   38: athrow
    //   39: getstatic android/common/OplusFeatureManager.sInstance : Landroid/common/OplusFeatureManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #19	-> 0
    //   #20	-> 6
    //   #21	-> 9
    //   #22	-> 15
    //   #24	-> 27
    //   #26	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public static void init(Context paramContext) {}
  
  public static <T extends IOplusCommonFeature> boolean isSupport(T paramT) {
    int i = getIndex(paramT);
    boolean bool1 = mFeatureDisable[i];
    boolean bool2 = bool1;
    if (bool1)
      synchronized (paramT.getDefault()) {
        bool2 = mFeatureDisable[i];
      }  
    if (!bool2) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    return bool2;
  }
  
  public static <T extends IOplusCommonFeature> boolean isTracing(T paramT) {
    int i = getIndex(paramT);
    boolean bool1 = mFeatureTraing[i];
    boolean bool2 = bool1;
    if (bool1)
      synchronized (paramT.getDefault()) {
        bool2 = mFeatureTraing[i];
      }  
    return bool2;
  }
  
  public static boolean isSupport(String paramString) {
    synchronized (sFeatureSwitchMap) {
      return true;
    } 
  }
  
  public static boolean isTracing(String paramString) {
    synchronized (sFeatureTraceMap) {
      return false;
    } 
  }
  
  public static <T extends IOplusCommonFeature> T getTraceMonitor(T paramT) {
    if (paramT != null) {
      IOplusCommonFeature iOplusCommonFeature;
      OplusFeatureList.OplusIndex oplusIndex = paramT.index();
      if (isTracing(paramT)) {
        DynamicProxy dynamicProxy = new DynamicProxy(paramT, oplusIndex.name());
        Class clazz = (Class)paramT.getDefault().getClass().getGenericInterfaces()[0];
        iOplusCommonFeature = (IOplusCommonFeature)Proxy.newProxyInstance(dynamicProxy.getClass().getClassLoader(), new Class[] { clazz }, dynamicProxy);
        return (T)iOplusCommonFeature;
      } 
      return (T)iOplusCommonFeature;
    } 
    throw new IllegalArgumentException("params must not be null");
  }
  
  private static class DynamicProxy implements InvocationHandler {
    private String feature;
    
    private Object realObj;
    
    public DynamicProxy(Object param1Object, String param1String) {
      this.realObj = param1Object;
      this.feature = param1String;
    }
    
    public Object invoke(Object param1Object, Method param1Method, Object[] param1ArrayOfObject) throws Throwable {
      param1Object = param1Method.invoke(this.realObj, param1ArrayOfObject);
      return param1Object;
    }
  }
  
  private static <T extends IOplusCommonFeature> int getIndex(T paramT) {
    if (paramT != null) {
      int i = paramT.index().ordinal();
      if (i < mFeatureDisable.length)
        return i; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("index = ");
      stringBuilder.append(i);
      stringBuilder.append(" size = ");
      stringBuilder.append(mFeatureDisable.length);
      throw new IllegalAccessError(stringBuilder.toString());
    } 
    throw new IllegalArgumentException("dummy must not be null");
  }
}
