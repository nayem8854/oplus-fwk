package android.common;

import java.util.ArrayList;
import java.util.Iterator;

public class OplusFeatureCache {
  private static final String TAG = "OplusFeatureCache";
  
  private static ArrayList<IOplusCommonFactory> mFactoryCache;
  
  private static Object[] mFeatureCache = new Object[OplusFeatureList.OplusIndex.End.ordinal()];
  
  static {
    ArrayList<IOplusCommonFactory> arrayList = new ArrayList();
    arrayList.add(ColorFrameworkFactory.getInstance());
    mFactoryCache.add(OplusFrameworkFactory.getInstance());
  }
  
  public static <T extends IOplusCommonFeature> T get(T paramT) {
    IOplusCommonFeature iOplusCommonFeature1;
    int i = getIndex(paramT);
    IOplusCommonFeature iOplusCommonFeature2 = (IOplusCommonFeature)mFeatureCache[paramT.index().ordinal()];
    IOplusCommonFeature iOplusCommonFeature3 = iOplusCommonFeature2;
    if (iOplusCommonFeature2 == null)
      synchronized (paramT.getDefault()) {
        iOplusCommonFeature3 = (IOplusCommonFeature)mFeatureCache[i];
      }  
    if (iOplusCommonFeature3 != null)
      iOplusCommonFeature1 = iOplusCommonFeature3; 
    return (T)iOplusCommonFeature1;
  }
  
  public static <T extends IOplusCommonFeature> T getOrCreate(T paramT, Object... paramVarArgs) {
    IOplusCommonFactory iOplusCommonFactory1, iOplusCommonFactory2;
    int i = getIndex(paramT);
    IOplusCommonFeature iOplusCommonFeature1 = (IOplusCommonFeature)mFeatureCache[paramT.index().ordinal()];
    IOplusCommonFeature iOplusCommonFeature2 = iOplusCommonFeature1;
    if (iOplusCommonFeature1 == null)
      synchronized (paramT.getDefault()) {
        iOplusCommonFeature1 = (IOplusCommonFeature)mFeatureCache[i];
        iOplusCommonFeature2 = iOplusCommonFeature1;
        if (iOplusCommonFeature1 == null)
          synchronized (mFactoryCache) {
            for (Iterator<IOplusCommonFactory> iterator = mFactoryCache.iterator(); iterator.hasNext(); ) {
              IOplusCommonFactory iOplusCommonFactory = iterator.next();
              if (iOplusCommonFactory.isValid(i)) {
                iOplusCommonFactory = iOplusCommonFactory.getFeature((IOplusCommonFactory)paramT, paramVarArgs);
                iOplusCommonFactory2 = iOplusCommonFactory;
                if (iOplusCommonFactory != null) {
                  set(iOplusCommonFactory);
                  return (T)iOplusCommonFactory;
                } 
              } 
            } 
          }  
      }  
    if (iOplusCommonFactory2 != null)
      iOplusCommonFactory1 = iOplusCommonFactory2; 
    return (T)iOplusCommonFactory1;
  }
  
  public static <T extends IOplusCommonFeature> void set(T paramT) {
    int i = getIndex(paramT);
    synchronized (paramT.getDefault()) {
      mFeatureCache[i] = paramT;
      return;
    } 
  }
  
  public static <T extends IOplusCommonFactory> void addFactory(T paramT) {
    if (paramT != null)
      synchronized (mFactoryCache) {
        mFactoryCache.add((IOplusCommonFactory)paramT);
      }  
  }
  
  private static <T extends IOplusCommonFeature> int getIndex(T paramT) {
    if (paramT != null) {
      int i = paramT.index().ordinal();
      if (i < mFeatureCache.length)
        return i; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("index = ");
      stringBuilder.append(i);
      stringBuilder.append(" size = ");
      stringBuilder.append(mFeatureCache.length);
      throw new IllegalAccessError(stringBuilder.toString());
    } 
    throw new IllegalArgumentException("dummy must not be null");
  }
}
