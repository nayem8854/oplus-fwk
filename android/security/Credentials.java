package android.security;

import com.android.org.bouncycastle.util.io.pem.PemObject;
import com.android.org.bouncycastle.util.io.pem.PemObjectGenerator;
import com.android.org.bouncycastle.util.io.pem.PemReader;
import com.android.org.bouncycastle.util.io.pem.PemWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

public class Credentials {
  public static final String APP_SOURCE_CERTIFICATE = "FSV_";
  
  public static final String CA_CERTIFICATE = "CACERT_";
  
  public static final String CERTIFICATE_USAGE_APP_SOURCE = "appsrc";
  
  public static final String CERTIFICATE_USAGE_CA = "ca";
  
  public static final String CERTIFICATE_USAGE_USER = "user";
  
  public static final String CERTIFICATE_USAGE_WIFI = "wifi";
  
  public static final String EXTENSION_CER = ".cer";
  
  public static final String EXTENSION_CRT = ".crt";
  
  public static final String EXTENSION_P12 = ".p12";
  
  public static final String EXTENSION_PFX = ".pfx";
  
  public static final String EXTRA_CA_CERTIFICATES_DATA = "ca_certificates_data";
  
  public static final String EXTRA_CERTIFICATE_USAGE = "certificate_install_usage";
  
  public static final String EXTRA_INSTALL_AS_UID = "install_as_uid";
  
  public static final String EXTRA_PRIVATE_KEY = "PKEY";
  
  public static final String EXTRA_PUBLIC_KEY = "KEY";
  
  public static final String EXTRA_USER_CERTIFICATE_DATA = "user_certificate_data";
  
  public static final String EXTRA_USER_KEY_ALIAS = "user_key_pair_name";
  
  public static final String EXTRA_USER_PRIVATE_KEY_DATA = "user_private_key_data";
  
  public static final String INSTALL_ACTION = "android.credentials.INSTALL";
  
  public static final String INSTALL_AS_USER_ACTION = "android.credentials.INSTALL_AS_USER";
  
  public static final String LOCKDOWN_VPN = "LOCKDOWN_VPN";
  
  private static final String LOGTAG = "Credentials";
  
  public static final String PLATFORM_VPN = "PLATFORM_VPN_";
  
  public static final String USER_CERTIFICATE = "USRCERT_";
  
  public static final String USER_PRIVATE_KEY = "USRPKEY_";
  
  public static final String USER_SECRET_KEY = "USRSKEY_";
  
  public static final String VPN = "VPN_";
  
  public static final String WIFI = "WIFI_";
  
  public static byte[] convertToPem(Certificate... paramVarArgs) throws IOException, CertificateEncodingException {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(byteArrayOutputStream, StandardCharsets.US_ASCII);
    PemWriter pemWriter = new PemWriter(outputStreamWriter);
    int i;
    byte b;
    for (i = paramVarArgs.length, b = 0; b < i; ) {
      Certificate certificate = paramVarArgs[b];
      pemWriter.writeObject((PemObjectGenerator)new PemObject("CERTIFICATE", certificate.getEncoded()));
      b++;
    } 
    pemWriter.close();
    return byteArrayOutputStream.toByteArray();
  }
  
  public static List<X509Certificate> convertFromPem(byte[] paramArrayOfbyte) throws IOException, CertificateException {
    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(paramArrayOfbyte);
    InputStreamReader inputStreamReader = new InputStreamReader(byteArrayInputStream, StandardCharsets.US_ASCII);
    PemReader pemReader = new PemReader(inputStreamReader);
    try {
      StringBuilder stringBuilder;
      CertificateFactory certificateFactory = CertificateFactory.getInstance("X509");
      ArrayList<X509Certificate> arrayList = new ArrayList();
      this();
      while (true) {
        PemObject pemObject = pemReader.readPemObject();
        if (pemObject != null) {
          Certificate certificate;
          if (pemObject.getType().equals("CERTIFICATE")) {
            ByteArrayInputStream byteArrayInputStream1 = new ByteArrayInputStream();
            this(pemObject.getContent());
            certificate = certificateFactory.generateCertificate(byteArrayInputStream1);
            arrayList.add((X509Certificate)certificate);
            continue;
          } 
          IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Unknown type ");
          stringBuilder.append(certificate.getType());
          this(stringBuilder.toString());
          throw illegalArgumentException;
        } 
        break;
      } 
      return (List<X509Certificate>)stringBuilder;
    } finally {
      pemReader.close();
    } 
  }
  
  public static boolean deleteAllTypesForAlias(KeyStore paramKeyStore, String paramString) {
    return deleteAllTypesForAlias(paramKeyStore, paramString, -1);
  }
  
  public static boolean deleteAllTypesForAlias(KeyStore paramKeyStore, String paramString, int paramInt) {
    boolean bool1 = deleteUserKeyTypeForAlias(paramKeyStore, paramString, paramInt);
    boolean bool2 = deleteCertificateTypesForAlias(paramKeyStore, paramString, paramInt);
    return bool1 & bool2;
  }
  
  public static boolean deleteCertificateTypesForAlias(KeyStore paramKeyStore, String paramString) {
    return deleteCertificateTypesForAlias(paramKeyStore, paramString, -1);
  }
  
  public static boolean deleteCertificateTypesForAlias(KeyStore paramKeyStore, String paramString, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("USRCERT_");
    stringBuilder.append(paramString);
    boolean bool1 = paramKeyStore.delete(stringBuilder.toString(), paramInt);
    stringBuilder = new StringBuilder();
    stringBuilder.append("CACERT_");
    stringBuilder.append(paramString);
    paramString = stringBuilder.toString();
    boolean bool2 = paramKeyStore.delete(paramString, paramInt);
    return bool1 & bool2;
  }
  
  public static boolean deleteUserKeyTypeForAlias(KeyStore paramKeyStore, String paramString) {
    return deleteUserKeyTypeForAlias(paramKeyStore, paramString, -1);
  }
  
  public static boolean deleteUserKeyTypeForAlias(KeyStore paramKeyStore, String paramString, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("USRPKEY_");
    stringBuilder.append(paramString);
    int i = paramKeyStore.delete2(stringBuilder.toString(), paramInt);
    if (i == 7) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("USRSKEY_");
      stringBuilder.append(paramString);
      return paramKeyStore.delete(stringBuilder.toString(), paramInt);
    } 
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  public static boolean deleteLegacyKeyForAlias(KeyStore paramKeyStore, String paramString, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("USRSKEY_");
    stringBuilder.append(paramString);
    return paramKeyStore.delete(stringBuilder.toString(), paramInt);
  }
}
