package android.security.net.config;

import com.android.org.conscrypt.TrustedCertificateStore;
import java.io.File;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Set;

public class TrustedCertificateStoreAdapter extends TrustedCertificateStore {
  private final NetworkSecurityConfig mConfig;
  
  public TrustedCertificateStoreAdapter(NetworkSecurityConfig paramNetworkSecurityConfig) {
    this.mConfig = paramNetworkSecurityConfig;
  }
  
  public X509Certificate findIssuer(X509Certificate paramX509Certificate) {
    TrustAnchor trustAnchor = this.mConfig.findTrustAnchorByIssuerAndSignature(paramX509Certificate);
    if (trustAnchor == null)
      return null; 
    return trustAnchor.certificate;
  }
  
  public Set<X509Certificate> findAllIssuers(X509Certificate paramX509Certificate) {
    return this.mConfig.findAllCertificatesByIssuerAndSignature(paramX509Certificate);
  }
  
  public X509Certificate getTrustAnchor(X509Certificate paramX509Certificate) {
    TrustAnchor trustAnchor = this.mConfig.findTrustAnchorBySubjectAndPublicKey(paramX509Certificate);
    if (trustAnchor == null)
      return null; 
    return trustAnchor.certificate;
  }
  
  public boolean isUserAddedCertificate(X509Certificate paramX509Certificate) {
    TrustAnchor trustAnchor = this.mConfig.findTrustAnchorBySubjectAndPublicKey(paramX509Certificate);
    if (trustAnchor == null)
      return false; 
    return trustAnchor.overridesPins;
  }
  
  public File getCertificateFile(File paramFile, X509Certificate paramX509Certificate) {
    throw new UnsupportedOperationException();
  }
  
  public Certificate getCertificate(String paramString) {
    throw new UnsupportedOperationException();
  }
  
  public Certificate getCertificate(String paramString, boolean paramBoolean) {
    throw new UnsupportedOperationException();
  }
  
  public Date getCreationDate(String paramString) {
    throw new UnsupportedOperationException();
  }
  
  public Set<String> aliases() {
    throw new UnsupportedOperationException();
  }
  
  public Set<String> userAliases() {
    throw new UnsupportedOperationException();
  }
  
  public Set<String> allSystemAliases() {
    throw new UnsupportedOperationException();
  }
  
  public boolean containsAlias(String paramString) {
    throw new UnsupportedOperationException();
  }
  
  public String getCertificateAlias(Certificate paramCertificate) {
    throw new UnsupportedOperationException();
  }
  
  public String getCertificateAlias(Certificate paramCertificate, boolean paramBoolean) {
    throw new UnsupportedOperationException();
  }
}
