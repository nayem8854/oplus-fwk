package android.security.net.config;

import java.util.Locale;

public final class Domain {
  public final String hostname;
  
  public final boolean subdomainsIncluded;
  
  public Domain(String paramString, boolean paramBoolean) {
    if (paramString != null) {
      this.hostname = paramString.toLowerCase(Locale.US);
      this.subdomainsIncluded = paramBoolean;
      return;
    } 
    throw new NullPointerException("Hostname must not be null");
  }
  
  public int hashCode() {
    char c;
    int i = this.hostname.hashCode();
    if (this.subdomainsIncluded) {
      c = 'ӏ';
    } else {
      c = 'ӕ';
    } 
    return i ^ c;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (paramObject == this)
      return true; 
    if (!(paramObject instanceof Domain))
      return false; 
    paramObject = paramObject;
    if (((Domain)paramObject).subdomainsIncluded == this.subdomainsIncluded) {
      String str = ((Domain)paramObject).hostname;
      paramObject = this.hostname;
      if (str.equals(paramObject))
        return null; 
    } 
    return false;
  }
}
