package android.security.net.config;

import android.os.Environment;
import android.os.UserHandle;
import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Set;

public final class SystemCertificateSource extends DirectoryCertificateSource {
  private final File mUserRemovedCaDir;
  
  class NoPreloadHolder {
    private static final SystemCertificateSource INSTANCE = new SystemCertificateSource();
  }
  
  private SystemCertificateSource() {
    super(new File(stringBuilder.toString()));
    File file = Environment.getUserConfigDirectory(UserHandle.myUserId());
    this.mUserRemovedCaDir = new File(file, "cacerts-removed");
  }
  
  public static SystemCertificateSource getInstance() {
    return NoPreloadHolder.INSTANCE;
  }
  
  protected boolean isCertMarkedAsRemoved(String paramString) {
    return (new File(this.mUserRemovedCaDir, paramString)).exists();
  }
}
