package android.security.net.config;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.util.Pair;
import java.util.Set;

public class ManifestConfigSource implements ConfigSource {
  private static final boolean DBG = true;
  
  private static final String LOG_TAG = "NetworkSecurityConfig";
  
  private final ApplicationInfo mApplicationInfo;
  
  private ConfigSource mConfigSource;
  
  private final Context mContext;
  
  private final Object mLock = new Object();
  
  public ManifestConfigSource(Context paramContext) {
    this.mContext = paramContext;
    this.mApplicationInfo = new ApplicationInfo(paramContext.getApplicationInfo());
  }
  
  public Set<Pair<Domain, NetworkSecurityConfig>> getPerDomainConfigs() {
    return getConfigSource().getPerDomainConfigs();
  }
  
  public NetworkSecurityConfig getDefaultConfig() {
    return getConfigSource().getDefaultConfig();
  }
  
  private ConfigSource getConfigSource() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore_1
    //   5: aload_1
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mConfigSource : Landroid/security/net/config/ConfigSource;
    //   11: ifnull -> 23
    //   14: aload_0
    //   15: getfield mConfigSource : Landroid/security/net/config/ConfigSource;
    //   18: astore_2
    //   19: aload_1
    //   20: monitorexit
    //   21: aload_2
    //   22: areturn
    //   23: aload_0
    //   24: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   27: getfield networkSecurityConfigRes : I
    //   30: istore_3
    //   31: iconst_1
    //   32: istore #4
    //   34: iconst_1
    //   35: istore #5
    //   37: iload_3
    //   38: ifeq -> 144
    //   41: aload_0
    //   42: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   45: getfield flags : I
    //   48: iconst_2
    //   49: iand
    //   50: ifeq -> 56
    //   53: goto -> 59
    //   56: iconst_0
    //   57: istore #5
    //   59: new java/lang/StringBuilder
    //   62: astore_2
    //   63: aload_2
    //   64: invokespecial <init> : ()V
    //   67: aload_2
    //   68: ldc 'Using Network Security Config from resource '
    //   70: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: pop
    //   74: aload_0
    //   75: getfield mContext : Landroid/content/Context;
    //   78: astore #6
    //   80: aload #6
    //   82: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   85: astore #6
    //   87: aload_2
    //   88: aload #6
    //   90: iload_3
    //   91: invokevirtual getResourceEntryName : (I)Ljava/lang/String;
    //   94: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   97: pop
    //   98: aload_2
    //   99: ldc ' debugBuild: '
    //   101: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   104: pop
    //   105: aload_2
    //   106: iload #5
    //   108: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   111: pop
    //   112: aload_2
    //   113: invokevirtual toString : ()Ljava/lang/String;
    //   116: astore_2
    //   117: ldc 'NetworkSecurityConfig'
    //   119: aload_2
    //   120: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   123: pop
    //   124: new android/security/net/config/XmlConfigSource
    //   127: astore_2
    //   128: aload_2
    //   129: aload_0
    //   130: getfield mContext : Landroid/content/Context;
    //   133: iload_3
    //   134: aload_0
    //   135: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   138: invokespecial <init> : (Landroid/content/Context;ILandroid/content/pm/ApplicationInfo;)V
    //   141: goto -> 201
    //   144: ldc 'NetworkSecurityConfig'
    //   146: ldc 'No Network Security Config specified, using platform default'
    //   148: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   151: pop
    //   152: aload_0
    //   153: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   156: getfield flags : I
    //   159: ldc 134217728
    //   161: iand
    //   162: ifeq -> 184
    //   165: aload_0
    //   166: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   169: astore_2
    //   170: aload_2
    //   171: invokevirtual isInstantApp : ()Z
    //   174: ifne -> 184
    //   177: iload #4
    //   179: istore #5
    //   181: goto -> 187
    //   184: iconst_0
    //   185: istore #5
    //   187: new android/security/net/config/ManifestConfigSource$DefaultConfigSource
    //   190: dup
    //   191: iload #5
    //   193: aload_0
    //   194: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   197: invokespecial <init> : (ZLandroid/content/pm/ApplicationInfo;)V
    //   200: astore_2
    //   201: aload_0
    //   202: aload_2
    //   203: putfield mConfigSource : Landroid/security/net/config/ConfigSource;
    //   206: aload_1
    //   207: monitorexit
    //   208: aload_2
    //   209: areturn
    //   210: astore_2
    //   211: aload_1
    //   212: monitorexit
    //   213: aload_2
    //   214: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #54	-> 0
    //   #55	-> 7
    //   #56	-> 14
    //   #58	-> 23
    //   #60	-> 31
    //   #61	-> 41
    //   #64	-> 59
    //   #65	-> 80
    //   #66	-> 87
    //   #64	-> 117
    //   #69	-> 124
    //   #70	-> 141
    //   #72	-> 144
    //   #76	-> 152
    //   #78	-> 170
    //   #79	-> 187
    //   #81	-> 201
    //   #82	-> 206
    //   #83	-> 210
    // Exception table:
    //   from	to	target	type
    //   7	14	210	finally
    //   14	21	210	finally
    //   23	31	210	finally
    //   41	53	210	finally
    //   59	80	210	finally
    //   80	87	210	finally
    //   87	117	210	finally
    //   117	124	210	finally
    //   124	141	210	finally
    //   144	152	210	finally
    //   152	170	210	finally
    //   170	177	210	finally
    //   187	201	210	finally
    //   201	206	210	finally
    //   206	208	210	finally
    //   211	213	210	finally
  }
  
  private static final class DefaultConfigSource implements ConfigSource {
    private final NetworkSecurityConfig mDefaultConfig;
    
    DefaultConfigSource(boolean param1Boolean, ApplicationInfo param1ApplicationInfo) {
      NetworkSecurityConfig.Builder builder = NetworkSecurityConfig.getDefaultBuilder(param1ApplicationInfo);
      builder = builder.setCleartextTrafficPermitted(param1Boolean);
      this.mDefaultConfig = builder.build();
    }
    
    public NetworkSecurityConfig getDefaultConfig() {
      return this.mDefaultConfig;
    }
    
    public Set<Pair<Domain, NetworkSecurityConfig>> getPerDomainConfigs() {
      return null;
    }
  }
}
