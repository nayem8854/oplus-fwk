package android.security.net.config;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.ArraySet;
import android.util.Base64;
import android.util.Pair;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class XmlConfigSource implements ConfigSource {
  private static final int CONFIG_BASE = 0;
  
  private static final int CONFIG_DEBUG = 2;
  
  private static final int CONFIG_DOMAIN = 1;
  
  private final ApplicationInfo mApplicationInfo;
  
  private Context mContext;
  
  private final boolean mDebugBuild;
  
  private NetworkSecurityConfig mDefaultConfig;
  
  private Set<Pair<Domain, NetworkSecurityConfig>> mDomainMap;
  
  private boolean mInitialized;
  
  private final Object mLock;
  
  private final int mResourceId;
  
  public XmlConfigSource(Context paramContext, int paramInt, ApplicationInfo paramApplicationInfo) {
    boolean bool;
    this.mLock = new Object();
    this.mContext = paramContext;
    this.mResourceId = paramInt;
    ApplicationInfo applicationInfo = new ApplicationInfo(paramApplicationInfo);
    if ((applicationInfo.flags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mDebugBuild = bool;
  }
  
  public Set<Pair<Domain, NetworkSecurityConfig>> getPerDomainConfigs() {
    ensureInitialized();
    return this.mDomainMap;
  }
  
  public NetworkSecurityConfig getDefaultConfig() {
    ensureInitialized();
    return this.mDefaultConfig;
  }
  
  private static final String getConfigString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2)
          return "debug-overrides"; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown config type: ");
        stringBuilder.append(paramInt);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return "domain-config";
    } 
    return "base-config";
  }
  
  private void ensureInitialized() {
    synchronized (this.mLock) {
      if (this.mInitialized)
        return; 
      try {
        XmlResourceParser xmlResourceParser = this.mContext.getResources().getXml(this.mResourceId);
        try {
          parseNetworkSecurityConfig(xmlResourceParser);
          this.mContext = null;
          this.mInitialized = true;
          return;
        } finally {
          if (xmlResourceParser != null)
            try {
              xmlResourceParser.close();
            } finally {
              xmlResourceParser = null;
            }  
        } 
      } catch (android.content.res.Resources.NotFoundException|XmlPullParserException|IOException|ParserException notFoundException) {
        RuntimeException runtimeException = new RuntimeException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Failed to parse XML configuration from ");
        Context context = this.mContext;
        stringBuilder.append(context.getResources().getResourceEntryName(this.mResourceId));
        this(stringBuilder.toString(), (Throwable)notFoundException);
        throw runtimeException;
      } 
    } 
  }
  
  private Pin parsePin(XmlResourceParser paramXmlResourceParser) throws IOException, XmlPullParserException, ParserException {
    String str = paramXmlResourceParser.getAttributeValue(null, "digest");
    if (Pin.isSupportedDigestAlgorithm(str)) {
      if (paramXmlResourceParser.next() == 4) {
        String str1 = paramXmlResourceParser.getText().trim();
        try {
          byte[] arrayOfByte = Base64.decode(str1, 0);
          int i = Pin.getDigestLength(str);
          if (arrayOfByte.length == i) {
            if (paramXmlResourceParser.next() == 3)
              return new Pin(str, arrayOfByte); 
            throw new ParserException("pin contains additional elements");
          } 
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("digest length ");
          stringBuilder1.append(arrayOfByte.length);
          stringBuilder1.append(" does not match expected length for ");
          stringBuilder1.append(str);
          stringBuilder1.append(" of ");
          stringBuilder1.append(i);
          throw new ParserException(stringBuilder1.toString());
        } catch (IllegalArgumentException illegalArgumentException) {
          throw new ParserException("Invalid pin digest", illegalArgumentException);
        } 
      } 
      throw new ParserException("Missing pin digest");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported pin digest algorithm: ");
    stringBuilder.append((String)illegalArgumentException);
    throw new ParserException(stringBuilder.toString());
  }
  
  private PinSet parsePinSet(XmlResourceParser paramXmlResourceParser) throws IOException, XmlPullParserException, ParserException {
    String str = paramXmlResourceParser.getAttributeValue(null, "expiration");
    long l = Long.MAX_VALUE;
    if (str != null)
      try {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        this("yyyy-MM-dd");
        simpleDateFormat.setLenient(false);
        Date date = simpleDateFormat.parse(str);
        if (date != null) {
          l = date.getTime();
        } else {
          ParserException parserException = new ParserException();
          this((XmlPullParser)paramXmlResourceParser, "Invalid expiration date in pin-set");
          throw parserException;
        } 
      } catch (ParseException parseException) {
        throw new ParserException("Invalid expiration date in pin-set", parseException);
      }  
    int i = paramXmlResourceParser.getDepth();
    ArraySet<Pin> arraySet = new ArraySet();
    while (XmlUtils.nextElementWithin((XmlPullParser)paramXmlResourceParser, i)) {
      String str1 = paramXmlResourceParser.getName();
      if (str1.equals("pin")) {
        arraySet.add(parsePin(paramXmlResourceParser));
        continue;
      } 
      XmlUtils.skipCurrentTag((XmlPullParser)paramXmlResourceParser);
    } 
    return new PinSet((Set<Pin>)arraySet, l);
  }
  
  private Domain parseDomain(XmlResourceParser paramXmlResourceParser, Set<String> paramSet) throws IOException, XmlPullParserException, ParserException {
    boolean bool = paramXmlResourceParser.getAttributeBooleanValue(null, "includeSubdomains", false);
    if (paramXmlResourceParser.next() == 4) {
      String str = paramXmlResourceParser.getText().trim().toLowerCase(Locale.US);
      if (paramXmlResourceParser.next() == 3) {
        if (paramSet.add(str))
          return new Domain(str, bool); 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append(" has already been specified");
        throw new ParserException(stringBuilder.toString());
      } 
      throw new ParserException("domain contains additional elements");
    } 
    throw new ParserException("Domain name missing");
  }
  
  private CertificatesEntryRef parseCertificatesEntry(XmlResourceParser paramXmlResourceParser, boolean paramBoolean) throws IOException, XmlPullParserException, ParserException {
    paramBoolean = paramXmlResourceParser.getAttributeBooleanValue(null, "overridePins", paramBoolean);
    int i = paramXmlResourceParser.getAttributeResourceValue(null, "src", -1);
    String str = paramXmlResourceParser.getAttributeValue(null, "src");
    if (str != null) {
      ResourceCertificateSource resourceCertificateSource;
      WfaCertificateSource wfaCertificateSource;
      if (i != -1) {
        resourceCertificateSource = new ResourceCertificateSource(i, this.mContext);
      } else {
        SystemCertificateSource systemCertificateSource;
        if ("system".equals(resourceCertificateSource)) {
          systemCertificateSource = SystemCertificateSource.getInstance();
        } else {
          UserCertificateSource userCertificateSource;
          if ("user".equals(systemCertificateSource)) {
            userCertificateSource = UserCertificateSource.getInstance();
          } else {
            if ("wfa".equals(userCertificateSource)) {
              wfaCertificateSource = WfaCertificateSource.getInstance();
              XmlUtils.skipCurrentTag((XmlPullParser)paramXmlResourceParser);
              return new CertificatesEntryRef(wfaCertificateSource, paramBoolean);
            } 
            throw new ParserException("Unknown certificates src. Should be one of system|user|@resourceVal");
          } 
        } 
      } 
      XmlUtils.skipCurrentTag((XmlPullParser)paramXmlResourceParser);
      return new CertificatesEntryRef(wfaCertificateSource, paramBoolean);
    } 
    throw new ParserException("certificates element missing src attribute");
  }
  
  private Collection<CertificatesEntryRef> parseTrustAnchors(XmlResourceParser paramXmlResourceParser, boolean paramBoolean) throws IOException, XmlPullParserException, ParserException {
    int i = paramXmlResourceParser.getDepth();
    ArrayList<CertificatesEntryRef> arrayList = new ArrayList();
    while (XmlUtils.nextElementWithin((XmlPullParser)paramXmlResourceParser, i)) {
      String str = paramXmlResourceParser.getName();
      if (str.equals("certificates")) {
        arrayList.add(parseCertificatesEntry(paramXmlResourceParser, paramBoolean));
        continue;
      } 
      XmlUtils.skipCurrentTag((XmlPullParser)paramXmlResourceParser);
    } 
    return arrayList;
  }
  
  private List<Pair<NetworkSecurityConfig.Builder, Set<Domain>>> parseConfigEntry(XmlResourceParser paramXmlResourceParser, Set<String> paramSet, NetworkSecurityConfig.Builder paramBuilder, int paramInt) throws IOException, XmlPullParserException, ParserException {
    boolean bool3, bool4, bool5;
    ArrayList<Pair> arrayList = new ArrayList();
    NetworkSecurityConfig.Builder builder = new NetworkSecurityConfig.Builder();
    builder.setParent(paramBuilder);
    ArraySet<Domain> arraySet = new ArraySet();
    boolean bool1 = false;
    boolean bool2 = false;
    if (paramInt == 2) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    paramXmlResourceParser.getName();
    int i = paramXmlResourceParser.getDepth();
    arrayList.add(new Pair(builder, arraySet));
    byte b = 0;
    while (true) {
      bool4 = bool1;
      bool5 = bool2;
      if (b < paramXmlResourceParser.getAttributeCount()) {
        String str = paramXmlResourceParser.getAttributeName(b);
        if ("hstsEnforced".equals(str)) {
          boolean bool = paramXmlResourceParser.getAttributeBooleanValue(b, false);
          builder.setHstsEnforced(bool);
        } else if ("cleartextTrafficPermitted".equals(str)) {
          boolean bool = paramXmlResourceParser.getAttributeBooleanValue(b, true);
          builder.setCleartextTrafficPermitted(bool);
        } 
        b++;
        continue;
      } 
      break;
    } 
    while (XmlUtils.nextElementWithin((XmlPullParser)paramXmlResourceParser, i)) {
      StringBuilder stringBuilder;
      Domain domain;
      Collection<CertificatesEntryRef> collection;
      String str = paramXmlResourceParser.getName();
      if ("domain".equals(str)) {
        if (paramInt == 1) {
          domain = parseDomain(paramXmlResourceParser, paramSet);
          arraySet.add(domain);
          continue;
        } 
        stringBuilder = new StringBuilder();
        stringBuilder.append("domain element not allowed in ");
        stringBuilder.append(getConfigString(paramInt));
        throw new ParserException(stringBuilder.toString());
      } 
      if ("trust-anchors".equals(domain)) {
        if (!bool5) {
          collection = parseTrustAnchors(paramXmlResourceParser, bool3);
          builder.addCertificatesEntryRefs(collection);
          bool5 = true;
          continue;
        } 
        throw new ParserException("Multiple trust-anchor elements not allowed");
      } 
      if ("pin-set".equals(collection)) {
        if (paramInt == 1) {
          if (!bool4) {
            builder.setPinSet(parsePinSet(paramXmlResourceParser));
            bool4 = true;
            continue;
          } 
          throw new ParserException("Multiple pin-set elements not allowed");
        } 
        stringBuilder = new StringBuilder();
        stringBuilder.append("pin-set element not allowed in ");
        stringBuilder.append(getConfigString(paramInt));
        throw new ParserException(stringBuilder.toString());
      } 
      if ("domain-config".equals(collection)) {
        if (paramInt == 1) {
          arrayList.addAll((Collection)parseConfigEntry(paramXmlResourceParser, (Set<String>)stringBuilder, builder, paramInt));
          continue;
        } 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Nested domain-config not allowed in ");
        stringBuilder.append(getConfigString(paramInt));
        throw new ParserException(stringBuilder.toString());
      } 
      XmlUtils.skipCurrentTag((XmlPullParser)paramXmlResourceParser);
    } 
    if (paramInt != 1 || !arraySet.isEmpty())
      return (List)arrayList; 
    throw new ParserException("No domain elements in domain-config");
  }
  
  private void addDebugAnchorsIfNeeded(NetworkSecurityConfig.Builder paramBuilder1, NetworkSecurityConfig.Builder paramBuilder2) {
    if (paramBuilder1 == null || !paramBuilder1.hasCertificatesEntryRefs())
      return; 
    if (!paramBuilder2.hasCertificatesEntryRefs())
      return; 
    paramBuilder2.addCertificatesEntryRefs(paramBuilder1.getCertificatesEntryRefs());
  }
  
  private void parseNetworkSecurityConfig(XmlResourceParser paramXmlResourceParser) throws IOException, XmlPullParserException, ParserException {
    ArraySet arraySet1 = new ArraySet();
    ArrayList<Pair<NetworkSecurityConfig.Builder, Set<Domain>>> arrayList = new ArrayList();
    NetworkSecurityConfig.Builder builder2 = null;
    NetworkSecurityConfig.Builder builder4 = null;
    boolean bool1 = false;
    boolean bool2 = false;
    XmlUtils.beginDocument((XmlPullParser)paramXmlResourceParser, "network-security-config");
    int i = paramXmlResourceParser.getDepth();
    while (XmlUtils.nextElementWithin((XmlPullParser)paramXmlResourceParser, i)) {
      if ("base-config".equals(paramXmlResourceParser.getName())) {
        if (!bool2) {
          bool2 = true;
          builder2 = (NetworkSecurityConfig.Builder)((Pair)parseConfigEntry(paramXmlResourceParser, (Set<String>)arraySet1, null, 0).get(0)).first;
          continue;
        } 
        throw new ParserException("Only one base-config allowed");
      } 
      if ("domain-config".equals(paramXmlResourceParser.getName())) {
        List<Pair<NetworkSecurityConfig.Builder, Set<Domain>>> list = parseConfigEntry(paramXmlResourceParser, (Set<String>)arraySet1, builder2, 1);
        arrayList.addAll(list);
        continue;
      } 
      if ("debug-overrides".equals(paramXmlResourceParser.getName())) {
        if (!bool1) {
          if (this.mDebugBuild) {
            builder4 = (NetworkSecurityConfig.Builder)((Pair)parseConfigEntry(paramXmlResourceParser, null, null, 2).get(0)).first;
          } else {
            XmlUtils.skipCurrentTag((XmlPullParser)paramXmlResourceParser);
          } 
          bool1 = true;
          continue;
        } 
        throw new ParserException("Only one debug-overrides allowed");
      } 
      XmlUtils.skipCurrentTag((XmlPullParser)paramXmlResourceParser);
    } 
    NetworkSecurityConfig.Builder builder1 = builder4;
    if (this.mDebugBuild) {
      builder1 = builder4;
      if (builder4 == null)
        builder1 = parseDebugOverridesResource(); 
    } 
    ApplicationInfo applicationInfo = this.mApplicationInfo;
    NetworkSecurityConfig.Builder builder3 = NetworkSecurityConfig.getDefaultBuilder(applicationInfo);
    addDebugAnchorsIfNeeded(builder1, builder3);
    if (builder2 != null) {
      builder2.setParent(builder3);
      addDebugAnchorsIfNeeded(builder1, builder2);
    } else {
      builder2 = builder3;
    } 
    ArraySet<Pair> arraySet = new ArraySet();
    Iterator<Pair<NetworkSecurityConfig.Builder, Set<Domain>>> iterator;
    ArraySet arraySet2;
    for (iterator = arrayList.iterator(), arraySet2 = arraySet1; iterator.hasNext(); ) {
      Pair pair = iterator.next();
      NetworkSecurityConfig.Builder builder = (NetworkSecurityConfig.Builder)pair.first;
      Set set = (Set)pair.second;
      if (builder.getParent() == null)
        builder.setParent(builder2); 
      addDebugAnchorsIfNeeded(builder1, builder);
      NetworkSecurityConfig networkSecurityConfig = builder.build();
      for (Domain domain : set)
        arraySet.add(new Pair(domain, networkSecurityConfig)); 
    } 
    this.mDefaultConfig = builder2.build();
    this.mDomainMap = (Set)arraySet;
  }
  
  private NetworkSecurityConfig.Builder parseDebugOverridesResource() throws IOException, XmlPullParserException, ParserException {
    Resources resources = this.mContext.getResources();
    String str1 = resources.getResourcePackageName(this.mResourceId);
    String str2 = resources.getResourceEntryName(this.mResourceId);
    null = new StringBuilder();
    null.append(str2);
    null.append("_debug");
    int i = resources.getIdentifier(null.toString(), "xml", str1);
    if (i == 0)
      return null; 
    null = null;
    XmlResourceParser xmlResourceParser = resources.getXml(i);
    try {
      ParserException parserException;
      XmlUtils.beginDocument((XmlPullParser)xmlResourceParser, "network-security-config");
      int j = xmlResourceParser.getDepth();
      i = 0;
      while (XmlUtils.nextElementWithin((XmlPullParser)xmlResourceParser, j)) {
        if ("debug-overrides".equals(xmlResourceParser.getName())) {
          if (i == 0) {
            if (this.mDebugBuild) {
              NetworkSecurityConfig.Builder builder = (NetworkSecurityConfig.Builder)((Pair)parseConfigEntry(xmlResourceParser, null, null, 2).get(0)).first;
            } else {
              XmlUtils.skipCurrentTag((XmlPullParser)xmlResourceParser);
            } 
            i = 1;
            continue;
          } 
          parserException = new ParserException();
          this((XmlPullParser)xmlResourceParser, "Only one debug-overrides allowed");
          throw parserException;
        } 
        XmlUtils.skipCurrentTag((XmlPullParser)xmlResourceParser);
      } 
      return (NetworkSecurityConfig.Builder)parserException;
    } finally {
      if (xmlResourceParser != null)
        try {
          xmlResourceParser.close();
        } finally {
          xmlResourceParser = null;
        }  
    } 
  }
  
  class ParserException extends Exception {
    public ParserException(String param1String, Throwable param1Throwable) {
      super(stringBuilder.toString(), param1Throwable);
    }
    
    public ParserException(String param1String) {
      this((XmlPullParser)this$0, param1String, null);
    }
  }
}
