package android.security.net.config;

import android.util.ArraySet;
import java.security.cert.X509Certificate;
import java.util.Set;

public final class CertificatesEntryRef {
  private final boolean mOverridesPins;
  
  private final CertificateSource mSource;
  
  public CertificatesEntryRef(CertificateSource paramCertificateSource, boolean paramBoolean) {
    this.mSource = paramCertificateSource;
    this.mOverridesPins = paramBoolean;
  }
  
  boolean overridesPins() {
    return this.mOverridesPins;
  }
  
  public Set<TrustAnchor> getTrustAnchors() {
    ArraySet<TrustAnchor> arraySet = new ArraySet();
    for (X509Certificate x509Certificate : this.mSource.getCertificates())
      arraySet.add(new TrustAnchor(x509Certificate, this.mOverridesPins)); 
    return (Set<TrustAnchor>)arraySet;
  }
  
  public TrustAnchor findBySubjectAndPublicKey(X509Certificate paramX509Certificate) {
    paramX509Certificate = this.mSource.findBySubjectAndPublicKey(paramX509Certificate);
    if (paramX509Certificate == null)
      return null; 
    return new TrustAnchor(paramX509Certificate, this.mOverridesPins);
  }
  
  public TrustAnchor findByIssuerAndSignature(X509Certificate paramX509Certificate) {
    paramX509Certificate = this.mSource.findByIssuerAndSignature(paramX509Certificate);
    if (paramX509Certificate == null)
      return null; 
    return new TrustAnchor(paramX509Certificate, this.mOverridesPins);
  }
  
  public Set<X509Certificate> findAllCertificatesByIssuerAndSignature(X509Certificate paramX509Certificate) {
    return this.mSource.findAllByIssuerAndSignature(paramX509Certificate);
  }
  
  public void handleTrustStorageUpdate() {
    this.mSource.handleTrustStorageUpdate();
  }
}
