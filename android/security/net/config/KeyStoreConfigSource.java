package android.security.net.config;

import android.util.Pair;
import java.security.KeyStore;
import java.util.Set;

class KeyStoreConfigSource implements ConfigSource {
  private final NetworkSecurityConfig mConfig;
  
  public KeyStoreConfigSource(KeyStore paramKeyStore) {
    NetworkSecurityConfig.Builder builder2 = new NetworkSecurityConfig.Builder();
    CertificatesEntryRef certificatesEntryRef = new CertificatesEntryRef(new KeyStoreCertificateSource(paramKeyStore), false);
    NetworkSecurityConfig.Builder builder1 = builder2.addCertificatesEntryRef(certificatesEntryRef);
    this.mConfig = builder1.build();
  }
  
  public Set<Pair<Domain, NetworkSecurityConfig>> getPerDomainConfigs() {
    return null;
  }
  
  public NetworkSecurityConfig getDefaultConfig() {
    return this.mConfig;
  }
}
