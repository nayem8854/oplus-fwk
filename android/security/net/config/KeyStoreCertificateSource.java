package android.security.net.config;

import android.util.ArraySet;
import com.android.org.conscrypt.TrustedCertificateIndex;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Set;

class KeyStoreCertificateSource implements CertificateSource {
  private Set<X509Certificate> mCertificates;
  
  private TrustedCertificateIndex mIndex;
  
  private final KeyStore mKeyStore;
  
  private final Object mLock = new Object();
  
  public KeyStoreCertificateSource(KeyStore paramKeyStore) {
    this.mKeyStore = paramKeyStore;
  }
  
  public Set<X509Certificate> getCertificates() {
    ensureInitialized();
    return this.mCertificates;
  }
  
  private void ensureInitialized() {
    synchronized (this.mLock) {
      if (this.mCertificates != null)
        return; 
      try {
        TrustedCertificateIndex trustedCertificateIndex = new TrustedCertificateIndex();
        this();
        ArraySet<X509Certificate> arraySet = new ArraySet();
        this(this.mKeyStore.size());
        for (Enumeration<String> enumeration = this.mKeyStore.aliases(); enumeration.hasMoreElements(); ) {
          String str = enumeration.nextElement();
          X509Certificate x509Certificate = (X509Certificate)this.mKeyStore.getCertificate(str);
          if (x509Certificate != null) {
            arraySet.add(x509Certificate);
            trustedCertificateIndex.index(x509Certificate);
          } 
        } 
        this.mIndex = trustedCertificateIndex;
        this.mCertificates = (Set<X509Certificate>)arraySet;
        return;
      } catch (KeyStoreException keyStoreException) {
        RuntimeException runtimeException = new RuntimeException();
        this("Failed to load certificates from KeyStore", keyStoreException);
        throw runtimeException;
      } 
    } 
  }
  
  public X509Certificate findBySubjectAndPublicKey(X509Certificate paramX509Certificate) {
    ensureInitialized();
    TrustAnchor trustAnchor = this.mIndex.findBySubjectAndPublicKey(paramX509Certificate);
    if (trustAnchor == null)
      return null; 
    return trustAnchor.getTrustedCert();
  }
  
  public X509Certificate findByIssuerAndSignature(X509Certificate paramX509Certificate) {
    ensureInitialized();
    TrustAnchor trustAnchor = this.mIndex.findByIssuerAndSignature(paramX509Certificate);
    if (trustAnchor == null)
      return null; 
    return trustAnchor.getTrustedCert();
  }
  
  public Set<X509Certificate> findAllByIssuerAndSignature(X509Certificate paramX509Certificate) {
    ensureInitialized();
    Set set = this.mIndex.findAllByIssuerAndSignature(paramX509Certificate);
    if (set.isEmpty())
      return Collections.emptySet(); 
    ArraySet<X509Certificate> arraySet = new ArraySet(set.size());
    for (TrustAnchor trustAnchor : set)
      arraySet.add(trustAnchor.getTrustedCert()); 
    return (Set<X509Certificate>)arraySet;
  }
  
  public void handleTrustStorageUpdate() {}
}
