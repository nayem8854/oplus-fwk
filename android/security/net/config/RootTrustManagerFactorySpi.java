package android.security.net.config;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import javax.net.ssl.ManagerFactoryParameters;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactorySpi;

public class RootTrustManagerFactorySpi extends TrustManagerFactorySpi {
  private ApplicationConfig mApplicationConfig;
  
  private NetworkSecurityConfig mConfig;
  
  public void engineInit(ManagerFactoryParameters paramManagerFactoryParameters) throws InvalidAlgorithmParameterException {
    if (paramManagerFactoryParameters instanceof ApplicationConfigParameters) {
      this.mApplicationConfig = ((ApplicationConfigParameters)paramManagerFactoryParameters).config;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported spec: ");
    stringBuilder.append(paramManagerFactoryParameters);
    stringBuilder.append(". Only ");
    stringBuilder.append(ApplicationConfigParameters.class.getName());
    stringBuilder.append(" supported");
    throw new InvalidAlgorithmParameterException(stringBuilder.toString());
  }
  
  public void engineInit(KeyStore paramKeyStore) throws KeyStoreException {
    if (paramKeyStore != null) {
      this.mApplicationConfig = new ApplicationConfig(new KeyStoreConfigSource(paramKeyStore));
    } else {
      this.mApplicationConfig = ApplicationConfig.getDefaultInstance();
    } 
  }
  
  public TrustManager[] engineGetTrustManagers() {
    ApplicationConfig applicationConfig = this.mApplicationConfig;
    if (applicationConfig != null)
      return new TrustManager[] { applicationConfig.getTrustManager() }; 
    throw new IllegalStateException("TrustManagerFactory not initialized");
  }
  
  public static final class ApplicationConfigParameters implements ManagerFactoryParameters {
    public final ApplicationConfig config;
    
    public ApplicationConfigParameters(ApplicationConfig param1ApplicationConfig) {
      this.config = param1ApplicationConfig;
    }
  }
}
