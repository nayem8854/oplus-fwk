package android.security.net.config;

import java.util.Arrays;

public final class Pin {
  public final byte[] digest;
  
  public final String digestAlgorithm;
  
  private final int mHashCode;
  
  public Pin(String paramString, byte[] paramArrayOfbyte) {
    this.digestAlgorithm = paramString;
    this.digest = paramArrayOfbyte;
    this.mHashCode = Arrays.hashCode(paramArrayOfbyte) ^ paramString.hashCode();
  }
  
  public static boolean isSupportedDigestAlgorithm(String paramString) {
    return "SHA-256".equalsIgnoreCase(paramString);
  }
  
  public static int getDigestLength(String paramString) {
    if ("SHA-256".equalsIgnoreCase(paramString))
      return 32; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported digest algorithm: ");
    stringBuilder.append(paramString);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int hashCode() {
    return this.mHashCode;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof Pin))
      return false; 
    paramObject = paramObject;
    if (paramObject.hashCode() != this.mHashCode)
      return false; 
    if (!Arrays.equals(this.digest, ((Pin)paramObject).digest))
      return false; 
    if (!this.digestAlgorithm.equals(((Pin)paramObject).digestAlgorithm))
      return false; 
    return true;
  }
}
