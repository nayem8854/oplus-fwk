package android.security.net.config;

import android.util.Pair;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import javax.net.ssl.X509TrustManager;

public final class ApplicationConfig {
  private static ApplicationConfig sInstance;
  
  private static Object sLock = new Object();
  
  private ConfigSource mConfigSource;
  
  private Set<Pair<Domain, NetworkSecurityConfig>> mConfigs;
  
  private NetworkSecurityConfig mDefaultConfig;
  
  private boolean mInitialized;
  
  private final Object mLock = new Object();
  
  private X509TrustManager mTrustManager;
  
  public ApplicationConfig(ConfigSource paramConfigSource) {
    this.mConfigSource = paramConfigSource;
    this.mInitialized = false;
  }
  
  public boolean hasPerDomainConfigs() {
    boolean bool;
    ensureInitialized();
    Set<Pair<Domain, NetworkSecurityConfig>> set = this.mConfigs;
    if (set != null && !set.isEmpty()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public NetworkSecurityConfig getConfigForHostname(String paramString) {
    ensureInitialized();
    if (paramString == null || paramString.isEmpty() || this.mConfigs == null)
      return this.mDefaultConfig; 
    if (paramString.charAt(0) != '.') {
      Pair<Domain, NetworkSecurityConfig> pair;
      paramString = paramString.toLowerCase(Locale.US);
      String str = paramString;
      if (paramString.charAt(paramString.length() - 1) == '.')
        str = paramString.substring(0, paramString.length() - 1); 
      NetworkSecurityConfig networkSecurityConfig = null;
      for (Pair<Domain, NetworkSecurityConfig> pair2 : this.mConfigs) {
        Pair<Domain, NetworkSecurityConfig> pair1;
        Domain domain = (Domain)pair2.first;
        NetworkSecurityConfig networkSecurityConfig1 = (NetworkSecurityConfig)pair2.second;
        if (domain.hostname.equals(str))
          return networkSecurityConfig1; 
        networkSecurityConfig1 = networkSecurityConfig;
        if (domain.subdomainsIncluded) {
          String str1 = domain.hostname;
          networkSecurityConfig1 = networkSecurityConfig;
          if (str.endsWith(str1)) {
            networkSecurityConfig1 = networkSecurityConfig;
            if (str.charAt(str.length() - domain.hostname.length() - 1) == '.')
              if (networkSecurityConfig == null) {
                pair1 = pair2;
              } else {
                networkSecurityConfig1 = networkSecurityConfig;
                if (domain.hostname.length() > ((Domain)((Pair)networkSecurityConfig).first).hostname.length())
                  pair1 = pair2; 
              }  
          } 
        } 
        pair = pair1;
      } 
      if (pair != null)
        return (NetworkSecurityConfig)pair.second; 
      return this.mDefaultConfig;
    } 
    throw new IllegalArgumentException("hostname must not begin with a .");
  }
  
  public X509TrustManager getTrustManager() {
    ensureInitialized();
    return this.mTrustManager;
  }
  
  public boolean isCleartextTrafficPermitted() {
    ensureInitialized();
    Set<Pair<Domain, NetworkSecurityConfig>> set = this.mConfigs;
    if (set != null)
      for (Pair<Domain, NetworkSecurityConfig> pair : set) {
        if (!((NetworkSecurityConfig)pair.second).isCleartextTrafficPermitted())
          return false; 
      }  
    return this.mDefaultConfig.isCleartextTrafficPermitted();
  }
  
  public boolean isCleartextTrafficPermitted(String paramString) {
    return getConfigForHostname(paramString).isCleartextTrafficPermitted();
  }
  
  public void handleTrustStorageUpdate() {
    synchronized (this.mLock) {
      if (!this.mInitialized)
        return; 
      this.mDefaultConfig.handleTrustStorageUpdate();
      if (this.mConfigs != null) {
        HashSet<NetworkSecurityConfig> hashSet = new HashSet();
        Set<Pair<Domain, NetworkSecurityConfig>> set = this.mConfigs;
        this(set.size());
        for (Pair<Domain, NetworkSecurityConfig> pair : this.mConfigs) {
          if (hashSet.add((NetworkSecurityConfig)pair.second))
            ((NetworkSecurityConfig)pair.second).handleTrustStorageUpdate(); 
        } 
      } 
      return;
    } 
  }
  
  private void ensureInitialized() {
    synchronized (this.mLock) {
      if (this.mInitialized)
        return; 
      this.mConfigs = this.mConfigSource.getPerDomainConfigs();
      this.mDefaultConfig = this.mConfigSource.getDefaultConfig();
      this.mConfigSource = null;
      RootTrustManager rootTrustManager = new RootTrustManager();
      this(this);
      this.mTrustManager = rootTrustManager;
      this.mInitialized = true;
      return;
    } 
  }
  
  public static void setDefaultInstance(ApplicationConfig paramApplicationConfig) {
    synchronized (sLock) {
      sInstance = paramApplicationConfig;
      return;
    } 
  }
  
  public static ApplicationConfig getDefaultInstance() {
    synchronized (sLock) {
      return sInstance;
    } 
  }
}
