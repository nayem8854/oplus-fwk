package android.security.net.config;

import java.net.Socket;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509ExtendedTrustManager;

public class RootTrustManager extends X509ExtendedTrustManager {
  private final ApplicationConfig mConfig;
  
  public RootTrustManager(ApplicationConfig paramApplicationConfig) {
    if (paramApplicationConfig != null) {
      this.mConfig = paramApplicationConfig;
      return;
    } 
    throw new NullPointerException("config must not be null");
  }
  
  public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString) throws CertificateException {
    NetworkSecurityConfig networkSecurityConfig = this.mConfig.getConfigForHostname("");
    networkSecurityConfig.getTrustManager().checkClientTrusted(paramArrayOfX509Certificate, paramString);
  }
  
  public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString, Socket paramSocket) throws CertificateException {
    NetworkSecurityConfig networkSecurityConfig = this.mConfig.getConfigForHostname("");
    networkSecurityConfig.getTrustManager().checkClientTrusted(paramArrayOfX509Certificate, paramString, paramSocket);
  }
  
  public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString, SSLEngine paramSSLEngine) throws CertificateException {
    NetworkSecurityConfig networkSecurityConfig = this.mConfig.getConfigForHostname("");
    networkSecurityConfig.getTrustManager().checkClientTrusted(paramArrayOfX509Certificate, paramString, paramSSLEngine);
  }
  
  public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString, Socket paramSocket) throws CertificateException {
    if (paramSocket instanceof SSLSocket) {
      SSLSocket sSLSocket = (SSLSocket)paramSocket;
      SSLSession sSLSession = sSLSocket.getHandshakeSession();
      if (sSLSession != null) {
        String str = sSLSession.getPeerHost();
        NetworkSecurityConfig networkSecurityConfig = this.mConfig.getConfigForHostname(str);
        networkSecurityConfig.getTrustManager().checkServerTrusted(paramArrayOfX509Certificate, paramString, paramSocket);
      } else {
        throw new CertificateException("Not in handshake; no session available");
      } 
    } else {
      checkServerTrusted(paramArrayOfX509Certificate, paramString);
    } 
  }
  
  public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString, SSLEngine paramSSLEngine) throws CertificateException {
    SSLSession sSLSession = paramSSLEngine.getHandshakeSession();
    if (sSLSession != null) {
      String str = sSLSession.getPeerHost();
      NetworkSecurityConfig networkSecurityConfig = this.mConfig.getConfigForHostname(str);
      networkSecurityConfig.getTrustManager().checkServerTrusted(paramArrayOfX509Certificate, paramString, paramSSLEngine);
      return;
    } 
    throw new CertificateException("Not in handshake; no session available");
  }
  
  public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString) throws CertificateException {
    if (!this.mConfig.hasPerDomainConfigs()) {
      NetworkSecurityConfig networkSecurityConfig = this.mConfig.getConfigForHostname("");
      networkSecurityConfig.getTrustManager().checkServerTrusted(paramArrayOfX509Certificate, paramString);
      return;
    } 
    throw new CertificateException("Domain specific configurations require that hostname aware checkServerTrusted(X509Certificate[], String, String) is used");
  }
  
  public List<X509Certificate> checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString1, String paramString2) throws CertificateException {
    if (paramString2 != null || !this.mConfig.hasPerDomainConfigs()) {
      NetworkSecurityConfig networkSecurityConfig = this.mConfig.getConfigForHostname(paramString2);
      return networkSecurityConfig.getTrustManager().checkServerTrusted(paramArrayOfX509Certificate, paramString1, paramString2);
    } 
    throw new CertificateException("Domain specific configurations require that the hostname be provided");
  }
  
  public X509Certificate[] getAcceptedIssuers() {
    NetworkSecurityConfig networkSecurityConfig = this.mConfig.getConfigForHostname("");
    return networkSecurityConfig.getTrustManager().getAcceptedIssuers();
  }
  
  public boolean isSameTrustConfiguration(String paramString1, String paramString2) {
    NetworkSecurityConfig networkSecurityConfig = this.mConfig.getConfigForHostname(paramString1);
    ApplicationConfig applicationConfig = this.mConfig;
    return 
      networkSecurityConfig.equals(applicationConfig.getConfigForHostname(paramString2));
  }
}
