package android.security.net.config;

import android.util.ArraySet;
import java.util.Collections;
import java.util.Set;

public final class PinSet {
  public static final PinSet EMPTY_PINSET = new PinSet(Collections.emptySet(), Long.MAX_VALUE);
  
  public final long expirationTime;
  
  public final Set<Pin> pins;
  
  public PinSet(Set<Pin> paramSet, long paramLong) {
    if (paramSet != null) {
      this.pins = paramSet;
      this.expirationTime = paramLong;
      return;
    } 
    throw new NullPointerException("pins must not be null");
  }
  
  Set<String> getPinAlgorithms() {
    ArraySet<String> arraySet = new ArraySet();
    for (Pin pin : this.pins)
      arraySet.add(pin.digestAlgorithm); 
    return (Set<String>)arraySet;
  }
}
