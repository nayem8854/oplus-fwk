package android.security.net.config;

import libcore.net.NetworkSecurityPolicy;

public class ConfigNetworkSecurityPolicy extends NetworkSecurityPolicy {
  private final ApplicationConfig mConfig;
  
  public ConfigNetworkSecurityPolicy(ApplicationConfig paramApplicationConfig) {
    this.mConfig = paramApplicationConfig;
  }
  
  public boolean isCleartextTrafficPermitted() {
    return this.mConfig.isCleartextTrafficPermitted();
  }
  
  public boolean isCleartextTrafficPermitted(String paramString) {
    return this.mConfig.isCleartextTrafficPermitted(paramString);
  }
  
  public boolean isCertificateTransparencyVerificationRequired(String paramString) {
    return false;
  }
}
