package android.security.net.config;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Set;

public final class WfaCertificateSource extends DirectoryCertificateSource {
  private static final String CACERTS_WFA_PATH = "/apex/com.android.wifi/etc/security/cacerts_wfa";
  
  class NoPreloadHolder {
    private static final WfaCertificateSource INSTANCE = new WfaCertificateSource();
  }
  
  private WfaCertificateSource() {
    super(new File("/apex/com.android.wifi/etc/security/cacerts_wfa"));
  }
  
  public static WfaCertificateSource getInstance() {
    return NoPreloadHolder.INSTANCE;
  }
  
  protected boolean isCertMarkedAsRemoved(String paramString) {
    return false;
  }
}
