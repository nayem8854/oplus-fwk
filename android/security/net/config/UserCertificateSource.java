package android.security.net.config;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Set;

public final class UserCertificateSource extends DirectoryCertificateSource {
  class NoPreloadHolder {
    private static final UserCertificateSource INSTANCE = new UserCertificateSource();
  }
  
  private UserCertificateSource() {
    super(file);
  }
  
  public static UserCertificateSource getInstance() {
    return NoPreloadHolder.INSTANCE;
  }
  
  protected boolean isCertMarkedAsRemoved(String paramString) {
    return false;
  }
}
