package android.security.net.config;

import android.content.pm.ApplicationInfo;
import android.util.ArrayMap;
import android.util.ArraySet;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

public final class NetworkSecurityConfig {
  public static final boolean DEFAULT_CLEARTEXT_TRAFFIC_PERMITTED = true;
  
  public static final boolean DEFAULT_HSTS_ENFORCED = false;
  
  private Set<TrustAnchor> mAnchors;
  
  private final Object mAnchorsLock = new Object();
  
  private final List<CertificatesEntryRef> mCertificatesEntryRefs;
  
  private final boolean mCleartextTrafficPermitted;
  
  private final boolean mHstsEnforced;
  
  private final PinSet mPins;
  
  private NetworkSecurityTrustManager mTrustManager;
  
  private final Object mTrustManagerLock = new Object();
  
  private NetworkSecurityConfig(boolean paramBoolean1, boolean paramBoolean2, PinSet paramPinSet, List<CertificatesEntryRef> paramList) {
    this.mCleartextTrafficPermitted = paramBoolean1;
    this.mHstsEnforced = paramBoolean2;
    this.mPins = paramPinSet;
    this.mCertificatesEntryRefs = paramList;
    Collections.sort(paramList, new Comparator<CertificatesEntryRef>() {
          final NetworkSecurityConfig this$0;
          
          public int compare(CertificatesEntryRef param1CertificatesEntryRef1, CertificatesEntryRef param1CertificatesEntryRef2) {
            if (param1CertificatesEntryRef1.overridesPins()) {
              byte b;
              if (param1CertificatesEntryRef2.overridesPins()) {
                b = 0;
              } else {
                b = -1;
              } 
              return b;
            } 
            return param1CertificatesEntryRef2.overridesPins();
          }
        });
  }
  
  public Set<TrustAnchor> getTrustAnchors() {
    synchronized (this.mAnchorsLock) {
      if (this.mAnchors != null)
        return this.mAnchors; 
      ArrayMap<X509Certificate, TrustAnchor> arrayMap = new ArrayMap();
      this();
      for (CertificatesEntryRef certificatesEntryRef : this.mCertificatesEntryRefs) {
        Set<TrustAnchor> set = certificatesEntryRef.getTrustAnchors();
        for (TrustAnchor trustAnchor : set) {
          X509Certificate x509Certificate = trustAnchor.certificate;
          if (!arrayMap.containsKey(x509Certificate))
            arrayMap.put(x509Certificate, trustAnchor); 
        } 
      } 
      ArraySet arraySet = new ArraySet();
      this(arrayMap.size());
      arraySet.addAll(arrayMap.values());
      this.mAnchors = (Set<TrustAnchor>)arraySet;
      return (Set<TrustAnchor>)arraySet;
    } 
  }
  
  public boolean isCleartextTrafficPermitted() {
    return this.mCleartextTrafficPermitted;
  }
  
  public boolean isHstsEnforced() {
    return this.mHstsEnforced;
  }
  
  public PinSet getPins() {
    return this.mPins;
  }
  
  public NetworkSecurityTrustManager getTrustManager() {
    synchronized (this.mTrustManagerLock) {
      if (this.mTrustManager == null) {
        NetworkSecurityTrustManager networkSecurityTrustManager = new NetworkSecurityTrustManager();
        this(this);
        this.mTrustManager = networkSecurityTrustManager;
      } 
      return this.mTrustManager;
    } 
  }
  
  public TrustAnchor findTrustAnchorBySubjectAndPublicKey(X509Certificate paramX509Certificate) {
    for (CertificatesEntryRef certificatesEntryRef : this.mCertificatesEntryRefs) {
      TrustAnchor trustAnchor = certificatesEntryRef.findBySubjectAndPublicKey(paramX509Certificate);
      if (trustAnchor != null)
        return trustAnchor; 
    } 
    return null;
  }
  
  public TrustAnchor findTrustAnchorByIssuerAndSignature(X509Certificate paramX509Certificate) {
    for (CertificatesEntryRef certificatesEntryRef : this.mCertificatesEntryRefs) {
      TrustAnchor trustAnchor = certificatesEntryRef.findByIssuerAndSignature(paramX509Certificate);
      if (trustAnchor != null)
        return trustAnchor; 
    } 
    return null;
  }
  
  public Set<X509Certificate> findAllCertificatesByIssuerAndSignature(X509Certificate paramX509Certificate) {
    ArraySet<X509Certificate> arraySet = new ArraySet();
    for (CertificatesEntryRef certificatesEntryRef : this.mCertificatesEntryRefs)
      arraySet.addAll(certificatesEntryRef.findAllCertificatesByIssuerAndSignature(paramX509Certificate)); 
    return (Set<X509Certificate>)arraySet;
  }
  
  public void handleTrustStorageUpdate() {
    synchronized (this.mAnchorsLock) {
      this.mAnchors = null;
      for (CertificatesEntryRef certificatesEntryRef : this.mCertificatesEntryRefs)
        certificatesEntryRef.handleTrustStorageUpdate(); 
      getTrustManager().handleTrustStorageUpdate();
      return;
    } 
  }
  
  public static Builder getDefaultBuilder(ApplicationInfo paramApplicationInfo) {
    boolean bool;
    Builder builder2 = new Builder();
    Builder builder3 = builder2.setHstsEnforced(false);
    CertificatesEntryRef certificatesEntryRef = new CertificatesEntryRef(SystemCertificateSource.getInstance(), false);
    Builder builder1 = builder3.addCertificatesEntryRef(certificatesEntryRef);
    if (paramApplicationInfo.targetSdkVersion < 28 && 
      !paramApplicationInfo.isInstantApp()) {
      bool = true;
    } else {
      bool = false;
    } 
    builder1.setCleartextTrafficPermitted(bool);
    if (paramApplicationInfo.targetSdkVersion <= 23 && !paramApplicationInfo.isPrivilegedApp()) {
      CertificatesEntryRef certificatesEntryRef1 = new CertificatesEntryRef(UserCertificateSource.getInstance(), false);
      builder1.addCertificatesEntryRef(certificatesEntryRef1);
    } 
    return builder1;
  }
  
  public static final class Builder {
    private List<CertificatesEntryRef> mCertificatesEntryRefs;
    
    private boolean mCleartextTrafficPermitted;
    
    private boolean mCleartextTrafficPermittedSet;
    
    private boolean mHstsEnforced;
    
    private boolean mHstsEnforcedSet;
    
    private Builder mParentBuilder;
    
    private PinSet mPinSet;
    
    public Builder() {
      this.mCleartextTrafficPermitted = true;
      this.mHstsEnforced = false;
      this.mCleartextTrafficPermittedSet = false;
      this.mHstsEnforcedSet = false;
    }
    
    public Builder setParent(Builder param1Builder) {
      Builder builder = param1Builder;
      while (builder != null) {
        if (builder != this) {
          builder = builder.getParent();
          continue;
        } 
        throw new IllegalArgumentException("Loops are not allowed in Builder parents");
      } 
      this.mParentBuilder = param1Builder;
      return this;
    }
    
    public Builder getParent() {
      return this.mParentBuilder;
    }
    
    public Builder setPinSet(PinSet param1PinSet) {
      this.mPinSet = param1PinSet;
      return this;
    }
    
    private PinSet getEffectivePinSet() {
      PinSet pinSet = this.mPinSet;
      if (pinSet != null)
        return pinSet; 
      Builder builder = this.mParentBuilder;
      if (builder != null)
        return builder.getEffectivePinSet(); 
      return PinSet.EMPTY_PINSET;
    }
    
    public Builder setCleartextTrafficPermitted(boolean param1Boolean) {
      this.mCleartextTrafficPermitted = param1Boolean;
      this.mCleartextTrafficPermittedSet = true;
      return this;
    }
    
    private boolean getEffectiveCleartextTrafficPermitted() {
      if (this.mCleartextTrafficPermittedSet)
        return this.mCleartextTrafficPermitted; 
      Builder builder = this.mParentBuilder;
      if (builder != null)
        return builder.getEffectiveCleartextTrafficPermitted(); 
      return true;
    }
    
    public Builder setHstsEnforced(boolean param1Boolean) {
      this.mHstsEnforced = param1Boolean;
      this.mHstsEnforcedSet = true;
      return this;
    }
    
    private boolean getEffectiveHstsEnforced() {
      if (this.mHstsEnforcedSet)
        return this.mHstsEnforced; 
      Builder builder = this.mParentBuilder;
      if (builder != null)
        return builder.getEffectiveHstsEnforced(); 
      return false;
    }
    
    public Builder addCertificatesEntryRef(CertificatesEntryRef param1CertificatesEntryRef) {
      if (this.mCertificatesEntryRefs == null)
        this.mCertificatesEntryRefs = new ArrayList<>(); 
      this.mCertificatesEntryRefs.add(param1CertificatesEntryRef);
      return this;
    }
    
    public Builder addCertificatesEntryRefs(Collection<? extends CertificatesEntryRef> param1Collection) {
      if (this.mCertificatesEntryRefs == null)
        this.mCertificatesEntryRefs = new ArrayList<>(); 
      this.mCertificatesEntryRefs.addAll(param1Collection);
      return this;
    }
    
    private List<CertificatesEntryRef> getEffectiveCertificatesEntryRefs() {
      List<CertificatesEntryRef> list = this.mCertificatesEntryRefs;
      if (list != null)
        return list; 
      Builder builder = this.mParentBuilder;
      if (builder != null)
        return builder.getEffectiveCertificatesEntryRefs(); 
      return Collections.emptyList();
    }
    
    public boolean hasCertificatesEntryRefs() {
      boolean bool;
      if (this.mCertificatesEntryRefs != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    List<CertificatesEntryRef> getCertificatesEntryRefs() {
      return this.mCertificatesEntryRefs;
    }
    
    public NetworkSecurityConfig build() {
      boolean bool1 = getEffectiveCleartextTrafficPermitted();
      boolean bool2 = getEffectiveHstsEnforced();
      PinSet pinSet = getEffectivePinSet();
      List<CertificatesEntryRef> list = getEffectiveCertificatesEntryRefs();
      return new NetworkSecurityConfig(bool1, bool2, pinSet, list);
    }
  }
}
