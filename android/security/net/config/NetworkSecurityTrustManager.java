package android.security.net.config;

import android.util.ArrayMap;
import com.android.org.conscrypt.ConscryptCertStore;
import com.android.org.conscrypt.TrustManagerImpl;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Set;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.X509ExtendedTrustManager;

public class NetworkSecurityTrustManager extends X509ExtendedTrustManager {
  private final TrustManagerImpl mDelegate;
  
  private X509Certificate[] mIssuers;
  
  private final Object mIssuersLock = new Object();
  
  private final NetworkSecurityConfig mNetworkSecurityConfig;
  
  public NetworkSecurityTrustManager(NetworkSecurityConfig paramNetworkSecurityConfig) {
    if (paramNetworkSecurityConfig != null) {
      this.mNetworkSecurityConfig = paramNetworkSecurityConfig;
      try {
        TrustedCertificateStoreAdapter trustedCertificateStoreAdapter = new TrustedCertificateStoreAdapter();
        this(paramNetworkSecurityConfig);
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(null);
        TrustManagerImpl trustManagerImpl = new TrustManagerImpl();
        this(keyStore, null, (ConscryptCertStore)trustedCertificateStoreAdapter);
        this.mDelegate = trustManagerImpl;
        return;
      } catch (GeneralSecurityException|java.io.IOException generalSecurityException) {
        throw new RuntimeException(generalSecurityException);
      } 
    } 
    throw new NullPointerException("config must not be null");
  }
  
  public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString) throws CertificateException {
    this.mDelegate.checkClientTrusted(paramArrayOfX509Certificate, paramString);
  }
  
  public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString, Socket paramSocket) throws CertificateException {
    this.mDelegate.checkClientTrusted(paramArrayOfX509Certificate, paramString, paramSocket);
  }
  
  public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString, SSLEngine paramSSLEngine) throws CertificateException {
    this.mDelegate.checkClientTrusted(paramArrayOfX509Certificate, paramString, paramSSLEngine);
  }
  
  public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString) throws CertificateException {
    checkServerTrusted(paramArrayOfX509Certificate, paramString, (String)null);
  }
  
  public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString, Socket paramSocket) throws CertificateException {
    TrustManagerImpl trustManagerImpl = this.mDelegate;
    List<X509Certificate> list = trustManagerImpl.getTrustedChainForServer(paramArrayOfX509Certificate, paramString, paramSocket);
    checkPins(list);
  }
  
  public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString, SSLEngine paramSSLEngine) throws CertificateException {
    TrustManagerImpl trustManagerImpl = this.mDelegate;
    List<X509Certificate> list = trustManagerImpl.getTrustedChainForServer(paramArrayOfX509Certificate, paramString, paramSSLEngine);
    checkPins(list);
  }
  
  public List<X509Certificate> checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString1, String paramString2) throws CertificateException {
    List<X509Certificate> list = this.mDelegate.checkServerTrusted(paramArrayOfX509Certificate, paramString1, paramString2);
    checkPins(list);
    return list;
  }
  
  private void checkPins(List<X509Certificate> paramList) throws CertificateException {
    PinSet pinSet = this.mNetworkSecurityConfig.getPins();
    if (pinSet.pins.isEmpty() || 
      System.currentTimeMillis() > pinSet.expirationTime || 
      !isPinningEnforced(paramList))
      return; 
    Set<String> set = pinSet.getPinAlgorithms();
    ArrayMap<String, MessageDigest> arrayMap = new ArrayMap(set.size());
    for (int i = paramList.size() - 1; i >= 0; i--) {
      X509Certificate x509Certificate = paramList.get(i);
      byte[] arrayOfByte = x509Certificate.getPublicKey().getEncoded();
      for (String str : set) {
        MessageDigest messageDigest2 = (MessageDigest)arrayMap.get(str);
        MessageDigest messageDigest1 = messageDigest2;
        if (messageDigest2 == null)
          try {
            messageDigest1 = MessageDigest.getInstance(str);
            arrayMap.put(str, messageDigest1);
          } catch (GeneralSecurityException generalSecurityException) {
            throw new RuntimeException(generalSecurityException);
          }  
        if (pinSet.pins.contains(new Pin(str, messageDigest1.digest(arrayOfByte))))
          return; 
      } 
    } 
    throw new CertificateException("Pin verification failed");
  }
  
  private boolean isPinningEnforced(List<X509Certificate> paramList) throws CertificateException {
    if (paramList.isEmpty())
      return false; 
    X509Certificate x509Certificate = paramList.get(paramList.size() - 1);
    NetworkSecurityConfig networkSecurityConfig = this.mNetworkSecurityConfig;
    TrustAnchor trustAnchor = networkSecurityConfig.findTrustAnchorBySubjectAndPublicKey(x509Certificate);
    if (trustAnchor != null)
      return trustAnchor.overridesPins ^ true; 
    throw new CertificateException("Trusted chain does not end in a TrustAnchor");
  }
  
  public X509Certificate[] getAcceptedIssuers() {
    synchronized (this.mIssuersLock) {
      if (this.mIssuers == null) {
        Set<TrustAnchor> set = this.mNetworkSecurityConfig.getTrustAnchors();
        X509Certificate[] arrayOfX509Certificate = new X509Certificate[set.size()];
        byte b = 0;
        for (TrustAnchor trustAnchor : set) {
          arrayOfX509Certificate[b] = trustAnchor.certificate;
          b++;
        } 
        this.mIssuers = arrayOfX509Certificate;
      } 
      return (X509Certificate[])this.mIssuers.clone();
    } 
  }
  
  public void handleTrustStorageUpdate() {
    synchronized (this.mIssuersLock) {
      this.mIssuers = null;
      this.mDelegate.handleTrustStorageUpdate();
      return;
    } 
  }
}
