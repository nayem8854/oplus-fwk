package android.security.net.config;

import android.content.Context;
import android.util.ArraySet;
import com.android.org.conscrypt.TrustedCertificateIndex;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import libcore.io.IoUtils;

public class ResourceCertificateSource implements CertificateSource {
  private Set<X509Certificate> mCertificates;
  
  private Context mContext;
  
  private TrustedCertificateIndex mIndex;
  
  private final Object mLock = new Object();
  
  private final int mResourceId;
  
  public ResourceCertificateSource(int paramInt, Context paramContext) {
    this.mResourceId = paramInt;
    this.mContext = paramContext;
  }
  
  private void ensureInitialized() {
    synchronized (this.mLock) {
      Certificate certificate1;
      if (this.mCertificates != null)
        return; 
      ArraySet<X509Certificate> arraySet = new ArraySet();
      this();
      InputStream inputStream1 = null, inputStream2 = null;
      InputStream inputStream3 = inputStream2, inputStream4 = inputStream1;
      try {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        inputStream3 = inputStream2;
        inputStream4 = inputStream1;
        inputStream1 = this.mContext.getResources().openRawResource(this.mResourceId);
        inputStream3 = inputStream1;
        inputStream4 = inputStream1;
        Collection<? extends Certificate> collection = certificateFactory.generateCertificates(inputStream1);
        IoUtils.closeQuietly(inputStream1);
        TrustedCertificateIndex trustedCertificateIndex = new TrustedCertificateIndex();
        this();
        for (Certificate certificate2 : collection) {
          arraySet.add((X509Certificate)certificate2);
          trustedCertificateIndex.index((X509Certificate)certificate2);
        } 
        this.mCertificates = (Set<X509Certificate>)arraySet;
        this.mIndex = trustedCertificateIndex;
        this.mContext = null;
        return;
      } catch (CertificateException certificateException) {
        certificate1 = certificate2;
        RuntimeException runtimeException = new RuntimeException();
        certificate1 = certificate2;
        StringBuilder stringBuilder = new StringBuilder();
        certificate1 = certificate2;
        this();
        certificate1 = certificate2;
        stringBuilder.append("Failed to load trust anchors from id ");
        certificate1 = certificate2;
        stringBuilder.append(this.mResourceId);
        certificate1 = certificate2;
        this(stringBuilder.toString(), certificateException);
        certificate1 = certificate2;
        throw runtimeException;
      } finally {}
      IoUtils.closeQuietly((AutoCloseable)certificate1);
      throw certificate2;
    } 
  }
  
  public Set<X509Certificate> getCertificates() {
    ensureInitialized();
    return this.mCertificates;
  }
  
  public X509Certificate findBySubjectAndPublicKey(X509Certificate paramX509Certificate) {
    ensureInitialized();
    TrustAnchor trustAnchor = this.mIndex.findBySubjectAndPublicKey(paramX509Certificate);
    if (trustAnchor == null)
      return null; 
    return trustAnchor.getTrustedCert();
  }
  
  public X509Certificate findByIssuerAndSignature(X509Certificate paramX509Certificate) {
    ensureInitialized();
    TrustAnchor trustAnchor = this.mIndex.findByIssuerAndSignature(paramX509Certificate);
    if (trustAnchor == null)
      return null; 
    return trustAnchor.getTrustedCert();
  }
  
  public Set<X509Certificate> findAllByIssuerAndSignature(X509Certificate paramX509Certificate) {
    ensureInitialized();
    Set set = this.mIndex.findAllByIssuerAndSignature(paramX509Certificate);
    if (set.isEmpty())
      return Collections.emptySet(); 
    ArraySet<X509Certificate> arraySet = new ArraySet(set.size());
    for (TrustAnchor trustAnchor : set)
      arraySet.add(trustAnchor.getTrustedCert()); 
    return (Set<X509Certificate>)arraySet;
  }
  
  public void handleTrustStorageUpdate() {}
}
