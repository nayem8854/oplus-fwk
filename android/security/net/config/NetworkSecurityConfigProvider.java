package android.security.net.config;

import android.content.Context;
import android.util.Log;
import java.security.Provider;
import java.security.Security;
import libcore.net.NetworkSecurityPolicy;

public final class NetworkSecurityConfigProvider extends Provider {
  private static final String LOG_TAG = "nsconfig";
  
  private static final String PREFIX;
  
  static {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(NetworkSecurityConfigProvider.class.getPackage().getName());
    stringBuilder.append(".");
    PREFIX = stringBuilder.toString();
  }
  
  public NetworkSecurityConfigProvider() {
    super("AndroidNSSP", 1.0D, "Android Network Security Policy Provider");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(PREFIX);
    stringBuilder.append("RootTrustManagerFactorySpi");
    put("TrustManagerFactory.PKIX", stringBuilder.toString());
    put("Alg.Alias.TrustManagerFactory.X509", "PKIX");
  }
  
  public static void install(Context paramContext) {
    ApplicationConfig applicationConfig = new ApplicationConfig(new ManifestConfigSource(paramContext));
    ApplicationConfig.setDefaultInstance(applicationConfig);
    int i = Security.insertProviderAt(new NetworkSecurityConfigProvider(), 1);
    if (i == 1) {
      NetworkSecurityPolicy.setInstance(new ConfigNetworkSecurityPolicy(applicationConfig));
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to install provider as highest priority provider. Provider was installed at position ");
    stringBuilder.append(i);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public static void handleNewApplication(Context paramContext) {
    ApplicationConfig applicationConfig2 = new ApplicationConfig(new ManifestConfigSource(paramContext));
    ApplicationConfig applicationConfig3 = ApplicationConfig.getDefaultInstance();
    String str = (paramContext.getApplicationInfo()).processName;
    ApplicationConfig applicationConfig1 = applicationConfig2;
    if (applicationConfig3 != null) {
      boolean bool = applicationConfig3.isCleartextTrafficPermitted();
      applicationConfig1 = applicationConfig2;
      if (bool != applicationConfig2.isCleartextTrafficPermitted()) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append(": New config does not match the previously set config.");
        Log.w("nsconfig", stringBuilder.toString());
        if (!applicationConfig3.hasPerDomainConfigs() && 
          !applicationConfig2.hasPerDomainConfigs()) {
          if (applicationConfig3.isCleartextTrafficPermitted()) {
            ApplicationConfig applicationConfig = applicationConfig3;
          } else {
            applicationConfig1 = applicationConfig2;
          } 
        } else {
          throw new RuntimeException("Found multiple conflicting per-domain rules");
        } 
      } 
    } 
    ApplicationConfig.setDefaultInstance(applicationConfig1);
  }
}
