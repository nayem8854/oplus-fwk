package android.security.net.config;

import android.util.ArraySet;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Set;
import javax.security.auth.x500.X500Principal;
import libcore.io.IoUtils;

abstract class DirectoryCertificateSource implements CertificateSource {
  private final Object mLock = new Object();
  
  private final File mDir;
  
  private Set<X509Certificate> mCertificates;
  
  private final CertificateFactory mCertFactory;
  
  private static final String LOG_TAG = "DirectoryCertificateSrc";
  
  protected DirectoryCertificateSource(File paramFile) {
    this.mDir = paramFile;
    try {
      this.mCertFactory = CertificateFactory.getInstance("X.509");
      return;
    } catch (CertificateException certificateException) {
      throw new RuntimeException("Failed to obtain X.509 CertificateFactory", certificateException);
    } 
  }
  
  public Set<X509Certificate> getCertificates() {
    synchronized (this.mLock) {
      if (this.mCertificates != null)
        return this.mCertificates; 
      ArraySet<X509Certificate> arraySet = new ArraySet();
      this();
      if (this.mDir.isDirectory())
        for (String str : this.mDir.list()) {
          if (!isCertMarkedAsRemoved(str)) {
            X509Certificate x509Certificate = readCertificate(str);
            if (x509Certificate != null)
              arraySet.add(x509Certificate); 
          } 
        }  
      this.mCertificates = (Set<X509Certificate>)arraySet;
      return (Set<X509Certificate>)arraySet;
    } 
  }
  
  public X509Certificate findBySubjectAndPublicKey(final X509Certificate cert) {
    return findCert(cert.getSubjectX500Principal(), new CertSelector() {
          final DirectoryCertificateSource this$0;
          
          final X509Certificate val$cert;
          
          public boolean match(X509Certificate param1X509Certificate) {
            return param1X509Certificate.getPublicKey().equals(cert.getPublicKey());
          }
        });
  }
  
  public X509Certificate findByIssuerAndSignature(final X509Certificate cert) {
    return findCert(cert.getIssuerX500Principal(), new CertSelector() {
          final DirectoryCertificateSource this$0;
          
          final X509Certificate val$cert;
          
          public boolean match(X509Certificate param1X509Certificate) {
            try {
              cert.verify(param1X509Certificate.getPublicKey());
              return true;
            } catch (Exception exception) {
              return false;
            } 
          }
        });
  }
  
  public Set<X509Certificate> findAllByIssuerAndSignature(final X509Certificate cert) {
    return findCerts(cert.getIssuerX500Principal(), new CertSelector() {
          final DirectoryCertificateSource this$0;
          
          final X509Certificate val$cert;
          
          public boolean match(X509Certificate param1X509Certificate) {
            try {
              cert.verify(param1X509Certificate.getPublicKey());
              return true;
            } catch (Exception exception) {
              return false;
            } 
          }
        });
  }
  
  public void handleTrustStorageUpdate() {
    synchronized (this.mLock) {
      this.mCertificates = null;
      return;
    } 
  }
  
  private Set<X509Certificate> findCerts(X500Principal paramX500Principal, CertSelector paramCertSelector) {
    Set<?> set;
    String str1 = getHash(paramX500Principal);
    String str2 = null;
    byte b = 0;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str1);
    stringBuilder.append(".");
    stringBuilder.append(b);
    String str3 = stringBuilder.toString();
    ArraySet<X509Certificate> arraySet;
    for (; b && (new File(this.mDir, str3)).exists(); b++, arraySet = arraySet1) {
      ArraySet<X509Certificate> arraySet1;
      if (isCertMarkedAsRemoved(str3)) {
        str3 = str2;
      } else {
        X509Certificate x509Certificate = readCertificate(str3);
        if (x509Certificate == null) {
          str3 = str2;
        } else if (!paramX500Principal.equals(x509Certificate.getSubjectX500Principal())) {
          str3 = str2;
        } else {
          str3 = str2;
          if (paramCertSelector.match(x509Certificate)) {
            str3 = str2;
            if (str2 == null)
              arraySet1 = new ArraySet(); 
            arraySet1.add(x509Certificate);
          } 
        } 
      } 
    } 
    if (arraySet == null)
      set = Collections.emptySet(); 
    return (Set)set;
  }
  
  private X509Certificate findCert(X500Principal paramX500Principal, CertSelector paramCertSelector) {
    String str1 = getHash(paramX500Principal);
    byte b = 0;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str1);
    stringBuilder.append(".");
    stringBuilder.append(b);
    String str2 = stringBuilder.toString();
    for (; b && (new File(this.mDir, str2)).exists(); b++) {
      if (!isCertMarkedAsRemoved(str2)) {
        X509Certificate x509Certificate = readCertificate(str2);
        if (x509Certificate != null)
          if (paramX500Principal.equals(x509Certificate.getSubjectX500Principal()))
            if (paramCertSelector.match(x509Certificate))
              return x509Certificate;   
      } 
    } 
    return null;
  }
  
  private String getHash(X500Principal paramX500Principal) {
    int i = hashName(paramX500Principal);
    return intToHexString(i, 8);
  }
  
  private static final char[] DIGITS = new char[] { 
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
      'a', 'b', 'c', 'd', 'e', 'f' };
  
  private static String intToHexString(int paramInt1, int paramInt2) {
    int i;
    char[] arrayOfChar = new char[8];
    byte b = 8;
    do {
      arrayOfChar[--b] = DIGITS[paramInt1 & 0xF];
      paramInt1 = i = paramInt1 >>> 4;
    } while (i != 0 || 8 - b < paramInt2);
    return new String(arrayOfChar, b, 8 - b);
  }
  
  private static int hashName(X500Principal paramX500Principal) {
    try {
      byte[] arrayOfByte = MessageDigest.getInstance("MD5").digest(paramX500Principal.getEncoded());
      int i = 0 + 1;
      byte b1 = arrayOfByte[0];
      int j = i + 1;
      i = arrayOfByte[i];
      byte b2 = arrayOfByte[j];
      j = arrayOfByte[j + 1];
      return (b1 & 0xFF) << 0 | (i & 0xFF) << 8 | (b2 & 0xFF) << 16 | (j & 0xFF) << 24;
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      throw new AssertionError(noSuchAlgorithmException);
    } 
  }
  
  private X509Certificate readCertificate(String paramString) {
    BufferedInputStream bufferedInputStream1 = null, bufferedInputStream2 = null;
    BufferedInputStream bufferedInputStream3 = bufferedInputStream2, bufferedInputStream4 = bufferedInputStream1;
    try {
      BufferedInputStream bufferedInputStream = new BufferedInputStream();
      bufferedInputStream3 = bufferedInputStream2;
      bufferedInputStream4 = bufferedInputStream1;
      FileInputStream fileInputStream = new FileInputStream();
      bufferedInputStream3 = bufferedInputStream2;
      bufferedInputStream4 = bufferedInputStream1;
      File file = new File();
      bufferedInputStream3 = bufferedInputStream2;
      bufferedInputStream4 = bufferedInputStream1;
      this(this.mDir, paramString);
      bufferedInputStream3 = bufferedInputStream2;
      bufferedInputStream4 = bufferedInputStream1;
      this(file);
      bufferedInputStream3 = bufferedInputStream2;
      bufferedInputStream4 = bufferedInputStream1;
      this(fileInputStream);
      bufferedInputStream3 = bufferedInputStream;
      bufferedInputStream4 = bufferedInputStream;
      X509Certificate x509Certificate = (X509Certificate)this.mCertFactory.generateCertificate(bufferedInputStream);
      IoUtils.closeQuietly(bufferedInputStream);
      return x509Certificate;
    } catch (CertificateException|java.io.IOException certificateException) {
      bufferedInputStream3 = bufferedInputStream4;
      StringBuilder stringBuilder = new StringBuilder();
      bufferedInputStream3 = bufferedInputStream4;
      this();
      bufferedInputStream3 = bufferedInputStream4;
      stringBuilder.append("Failed to read certificate from ");
      bufferedInputStream3 = bufferedInputStream4;
      stringBuilder.append(paramString);
      bufferedInputStream3 = bufferedInputStream4;
      Log.e("DirectoryCertificateSrc", stringBuilder.toString(), certificateException);
      IoUtils.closeQuietly(bufferedInputStream4);
      return null;
    } finally {}
    IoUtils.closeQuietly(bufferedInputStream3);
    throw paramString;
  }
  
  protected abstract boolean isCertMarkedAsRemoved(String paramString);
  
  class CertSelector {
    public abstract boolean match(X509Certificate param1X509Certificate);
  }
}
