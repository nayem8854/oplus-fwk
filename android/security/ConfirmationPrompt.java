package android.security;

import android.content.ContentResolver;
import android.content.Context;
import android.os.IBinder;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import java.util.Locale;
import java.util.concurrent.Executor;

public class ConfirmationPrompt {
  private CharSequence mPromptText;
  
  private final KeyStore mKeyStore = KeyStore.getInstance();
  
  private byte[] mExtraData;
  
  private Executor mExecutor;
  
  private Context mContext;
  
  private void doCallback(int paramInt, byte[] paramArrayOfbyte, ConfirmationCallback paramConfirmationCallback) {
    StringBuilder stringBuilder;
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 5) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Unexpected responseCode=");
            stringBuilder.append(paramInt);
            stringBuilder.append(" from onConfirmtionPromptCompleted() callback.");
            paramConfirmationCallback.onError(new Exception(stringBuilder.toString()));
          } else {
            paramConfirmationCallback.onError(new Exception("System error returned by ConfirmationUI."));
          } 
        } else {
          paramConfirmationCallback.onCanceled();
        } 
      } else {
        paramConfirmationCallback.onDismissed();
      } 
    } else {
      paramConfirmationCallback.onConfirmed((byte[])stringBuilder);
    } 
  }
  
  private final IBinder mCallbackBinder = (IBinder)new Object(this);
  
  private ConfirmationCallback mCallback;
  
  private static final int UI_OPTION_ACCESSIBILITY_MAGNIFIED_FLAG = 2;
  
  private static final int UI_OPTION_ACCESSIBILITY_INVERTED_FLAG = 1;
  
  private static final String TAG = "ConfirmationPrompt";
  
  public static final class Builder {
    private Context mContext;
    
    private byte[] mExtraData;
    
    private CharSequence mPromptText;
    
    public Builder(Context param1Context) {
      this.mContext = param1Context;
    }
    
    public Builder setPromptText(CharSequence param1CharSequence) {
      this.mPromptText = param1CharSequence;
      return this;
    }
    
    public Builder setExtraData(byte[] param1ArrayOfbyte) {
      this.mExtraData = param1ArrayOfbyte;
      return this;
    }
    
    public ConfirmationPrompt build() {
      if (!TextUtils.isEmpty(this.mPromptText)) {
        byte[] arrayOfByte = this.mExtraData;
        if (arrayOfByte != null)
          return new ConfirmationPrompt(this.mContext, this.mPromptText, arrayOfByte); 
        throw new IllegalArgumentException("extraData must be set");
      } 
      throw new IllegalArgumentException("prompt text must be set and non-empty");
    }
  }
  
  private ConfirmationPrompt(Context paramContext, CharSequence paramCharSequence, byte[] paramArrayOfbyte) {
    this.mContext = paramContext;
    this.mPromptText = paramCharSequence;
    this.mExtraData = paramArrayOfbyte;
  }
  
  private int getUiOptionsAsFlags() {
    int i = 0;
    ContentResolver contentResolver = this.mContext.getContentResolver();
    int j = Settings.Secure.getInt(contentResolver, "accessibility_display_inversion_enabled", 0);
    if (j == 1)
      i = false | true; 
    float f = Settings.System.getFloat(contentResolver, "font_scale", 1.0F);
    j = i;
    if (f > 1.0D)
      j = i | 0x2; 
    return j;
  }
  
  private static boolean isAccessibilityServiceRunning(Context paramContext) {
    boolean bool1 = false, bool2 = false;
    try {
      ContentResolver contentResolver = paramContext.getContentResolver();
      int i = Settings.Secure.getInt(contentResolver, "accessibility_enabled");
      if (i == 1)
        bool2 = true; 
    } catch (android.provider.Settings.SettingNotFoundException settingNotFoundException) {
      Log.w("ConfirmationPrompt", "Unexpected SettingNotFoundException");
      settingNotFoundException.printStackTrace();
      bool2 = bool1;
    } 
    return bool2;
  }
  
  public void presentPrompt(Executor paramExecutor, ConfirmationCallback paramConfirmationCallback) throws ConfirmationAlreadyPresentingException, ConfirmationNotAvailableException {
    if (this.mCallback == null) {
      if (!isAccessibilityServiceRunning(this.mContext)) {
        this.mCallback = paramConfirmationCallback;
        this.mExecutor = paramExecutor;
        int i = getUiOptionsAsFlags();
        String str = Locale.getDefault().toLanguageTag();
        KeyStore keyStore = this.mKeyStore;
        IBinder iBinder = this.mCallbackBinder;
        CharSequence charSequence = this.mPromptText;
        charSequence = charSequence.toString();
        byte[] arrayOfByte = this.mExtraData;
        i = keyStore.presentConfirmationPrompt(iBinder, (String)charSequence, arrayOfByte, str, i);
        if (i != 0) {
          if (i != 3) {
            if (i != 6) {
              if (i != 65536) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Unexpected responseCode=");
                stringBuilder.append(i);
                stringBuilder.append(" from presentConfirmationPrompt() call.");
                Log.w("ConfirmationPrompt", stringBuilder.toString());
                throw new IllegalArgumentException();
              } 
              throw new IllegalArgumentException();
            } 
            throw new ConfirmationNotAvailableException();
          } 
          throw new ConfirmationAlreadyPresentingException();
        } 
        return;
      } 
      throw new ConfirmationNotAvailableException();
    } 
    throw new ConfirmationAlreadyPresentingException();
  }
  
  public void cancelPrompt() {
    int i = this.mKeyStore.cancelConfirmationPrompt(this.mCallbackBinder);
    if (i == 0)
      return; 
    if (i == 3)
      throw new IllegalStateException(); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unexpected responseCode=");
    stringBuilder.append(i);
    stringBuilder.append(" from cancelConfirmationPrompt() call.");
    Log.w("ConfirmationPrompt", stringBuilder.toString());
    throw new IllegalStateException();
  }
  
  public static boolean isSupported(Context paramContext) {
    if (isAccessibilityServiceRunning(paramContext))
      return false; 
    return KeyStore.getInstance().isConfirmationPromptSupported();
  }
}
