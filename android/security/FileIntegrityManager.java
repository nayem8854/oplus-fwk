package android.security;

import android.content.Context;
import android.os.RemoteException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

public final class FileIntegrityManager {
  private final Context mContext;
  
  private final IFileIntegrityService mService;
  
  public FileIntegrityManager(Context paramContext, IFileIntegrityService paramIFileIntegrityService) {
    this.mContext = paramContext;
    this.mService = paramIFileIntegrityService;
  }
  
  public boolean isApkVeritySupported() {
    try {
      return this.mService.isApkVeritySupported();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isAppSourceCertificateTrusted(X509Certificate paramX509Certificate) throws CertificateEncodingException {
    try {
      IFileIntegrityService iFileIntegrityService = this.mService;
      byte[] arrayOfByte = paramX509Certificate.getEncoded();
      String str = this.mContext.getOpPackageName();
      return iFileIntegrityService.isAppSourceCertificateTrusted(arrayOfByte, str);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
