package android.security;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Looper;
import android.os.Parcelable;
import android.os.Process;
import android.os.RemoteException;
import android.os.UserHandle;
import android.os.UserManager;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.security.KeyPair;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import javax.security.auth.x500.X500Principal;

public final class KeyChain {
  public static final String ACCOUNT_TYPE = "com.android.keychain";
  
  private static final String ACTION_CHOOSER = "com.android.keychain.CHOOSER";
  
  private static final String ACTION_INSTALL = "android.credentials.INSTALL";
  
  public static final String ACTION_KEYCHAIN_CHANGED = "android.security.action.KEYCHAIN_CHANGED";
  
  public static final String ACTION_KEY_ACCESS_CHANGED = "android.security.action.KEY_ACCESS_CHANGED";
  
  public static final String ACTION_STORAGE_CHANGED = "android.security.STORAGE_CHANGED";
  
  public static final String ACTION_TRUST_STORE_CHANGED = "android.security.action.TRUST_STORE_CHANGED";
  
  private static final String CERT_INSTALLER_PACKAGE = "com.android.certinstaller";
  
  public static final String EXTRA_ALIAS = "alias";
  
  public static final String EXTRA_CERTIFICATE = "CERT";
  
  public static final String EXTRA_ISSUERS = "issuers";
  
  public static final String EXTRA_KEY_ACCESSIBLE = "android.security.extra.KEY_ACCESSIBLE";
  
  public static final String EXTRA_KEY_ALIAS = "android.security.extra.KEY_ALIAS";
  
  public static final String EXTRA_KEY_TYPES = "key_types";
  
  public static final String EXTRA_NAME = "name";
  
  public static final String EXTRA_PKCS12 = "PKCS12";
  
  public static final String EXTRA_RESPONSE = "response";
  
  public static final String EXTRA_SENDER = "sender";
  
  public static final String EXTRA_URI = "uri";
  
  private static final String KEYCHAIN_PACKAGE = "com.android.keychain";
  
  public static final String KEY_ALIAS_SELECTION_DENIED = "android:alias-selection-denied";
  
  public static final int KEY_ATTESTATION_CANNOT_ATTEST_IDS = 3;
  
  public static final int KEY_ATTESTATION_CANNOT_COLLECT_DATA = 2;
  
  public static final int KEY_ATTESTATION_FAILURE = 4;
  
  public static final int KEY_ATTESTATION_MISSING_CHALLENGE = 1;
  
  public static final int KEY_ATTESTATION_SUCCESS = 0;
  
  public static final int KEY_GEN_FAILURE = 7;
  
  public static final int KEY_GEN_INVALID_ALGORITHM_PARAMETERS = 4;
  
  public static final int KEY_GEN_MISSING_ALIAS = 1;
  
  public static final int KEY_GEN_NO_KEYSTORE_PROVIDER = 5;
  
  public static final int KEY_GEN_NO_SUCH_ALGORITHM = 3;
  
  public static final int KEY_GEN_STRONGBOX_UNAVAILABLE = 6;
  
  public static final int KEY_GEN_SUCCESS = 0;
  
  public static final int KEY_GEN_SUPERFLUOUS_ATTESTATION_CHALLENGE = 2;
  
  public static Intent createInstallIntent() {
    Intent intent = new Intent("android.credentials.INSTALL");
    intent.setClassName("com.android.certinstaller", "com.android.certinstaller.CertInstallerMain");
    return intent;
  }
  
  public static void choosePrivateKeyAlias(Activity paramActivity, KeyChainAliasCallback paramKeyChainAliasCallback, String[] paramArrayOfString, Principal[] paramArrayOfPrincipal, String paramString1, int paramInt, String paramString2) {
    Uri uri;
    StringBuilder stringBuilder = null;
    if (paramString1 != null) {
      Uri.Builder builder2 = new Uri.Builder();
      stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      if (paramInt != -1) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(":");
        stringBuilder1.append(paramInt);
        String str = stringBuilder1.toString();
      } else {
        paramString1 = "";
      } 
      stringBuilder.append(paramString1);
      Uri.Builder builder1 = builder2.authority(stringBuilder.toString());
      uri = builder1.build();
    } 
    choosePrivateKeyAlias(paramActivity, paramKeyChainAliasCallback, paramArrayOfString, paramArrayOfPrincipal, uri, paramString2);
  }
  
  public static void choosePrivateKeyAlias(Activity paramActivity, KeyChainAliasCallback paramKeyChainAliasCallback, String[] paramArrayOfString, Principal[] paramArrayOfPrincipal, Uri paramUri, String paramString) {
    if (paramActivity != null) {
      if (paramKeyChainAliasCallback != null) {
        String str;
        Intent intent = new Intent("com.android.keychain.CHOOSER");
        intent.setPackage("com.android.keychain");
        intent.putExtra("response", new AliasResponse());
        intent.putExtra("uri", paramUri);
        intent.putExtra("alias", paramString);
        intent.putExtra("key_types", paramArrayOfString);
        ArrayList<byte[]> arrayList = new ArrayList();
        if (paramArrayOfPrincipal != null) {
          int i;
          byte b;
          for (i = paramArrayOfPrincipal.length, b = 0; b < i; ) {
            Principal principal = paramArrayOfPrincipal[b];
            if (principal instanceof X500Principal) {
              arrayList.add(((X500Principal)principal).getEncoded());
              b++;
            } 
            str = principal.toString();
            Class<?> clazz = principal.getClass();
            throw new IllegalArgumentException(String.format("Issuer %s is of type %s, not X500Principal", new Object[] { str, clazz }));
          } 
        } 
        intent.putExtra("issuers", arrayList);
        intent.putExtra("sender", (Parcelable)PendingIntent.getActivity((Context)str, 0, new Intent(), 0));
        str.startActivity(intent);
        return;
      } 
      throw new NullPointerException("response == null");
    } 
    throw new NullPointerException("activity == null");
  }
  
  class AliasResponse extends IKeyChainAliasCallback.Stub {
    private final KeyChainAliasCallback keyChainAliasResponse;
    
    private AliasResponse(KeyChain this$0) {
      this.keyChainAliasResponse = (KeyChainAliasCallback)this$0;
    }
    
    public void alias(String param1String) {
      this.keyChainAliasResponse.alias(param1String);
    }
  }
  
  public static PrivateKey getPrivateKey(Context paramContext, String paramString) throws KeyChainException, InterruptedException {
    KeyPair keyPair = getKeyPair(paramContext, paramString);
    if (keyPair != null)
      return keyPair.getPrivate(); 
    return null;
  }
  
  public static KeyPair getKeyPair(Context paramContext, String paramString) throws KeyChainException, InterruptedException {
    if (paramString != null) {
      if (paramContext != null)
        try {
          KeyChainConnection keyChainConnection = bind(paramContext.getApplicationContext());
          try {
            paramString = keyChainConnection.getService().requestPrivateKey(paramString);
            if (keyChainConnection != null)
              keyChainConnection.close(); 
            if (paramString == null)
              return null; 
          } finally {
            if (runtimeException != null)
              try {
                runtimeException.close();
              } finally {
                runtimeException = null;
              }  
          } 
        } catch (RemoteException remoteException) {
          throw new KeyChainException(remoteException);
        } catch (RuntimeException runtimeException) {
          throw new KeyChainException(runtimeException);
        }  
      throw new NullPointerException("context == null");
    } 
    throw new NullPointerException("alias == null");
  }
  
  public static X509Certificate[] getCertificateChain(Context paramContext, String paramString) throws KeyChainException, InterruptedException {
    if (paramString != null)
      try {
        KeyChainConnection keyChainConnection = bind(paramContext.getApplicationContext());
        try {
          IKeyChainService iKeyChainService = keyChainConnection.getService();
          byte[] arrayOfByte2 = iKeyChainService.getCertificate(paramString);
          if (arrayOfByte2 == null)
            return null; 
          byte[] arrayOfByte1 = iKeyChainService.getCaCertificates(paramString);
          if (keyChainConnection != null)
            keyChainConnection.close(); 
        } finally {
          if (certificateException != null)
            try {
              certificateException.close();
            } finally {
              certificateException = null;
            }  
        } 
      } catch (RemoteException remoteException) {
        throw new KeyChainException(remoteException);
      } catch (RuntimeException runtimeException) {
        throw new KeyChainException(runtimeException);
      }  
    throw new NullPointerException("alias == null");
  }
  
  public static boolean isKeyAlgorithmSupported(String paramString) {
    paramString = paramString.toUpperCase(Locale.US);
    return ("EC".equals(paramString) || 
      "RSA".equals(paramString));
  }
  
  @Deprecated
  public static boolean isBoundKeyAlgorithm(String paramString) {
    if (!isKeyAlgorithmSupported(paramString))
      return false; 
    return KeyStore.getInstance().isHardwareBacked(paramString);
  }
  
  public static X509Certificate toCertificate(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null)
      try {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
        this(paramArrayOfbyte);
        null = certificateFactory.generateCertificate(byteArrayInputStream);
        return (X509Certificate)null;
      } catch (CertificateException certificateException) {
        throw new AssertionError(certificateException);
      }  
    throw new IllegalArgumentException("bytes == null");
  }
  
  public static Collection<X509Certificate> toCertificates(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null)
      try {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
        this(paramArrayOfbyte);
        return (Collection)certificateFactory.generateCertificates(byteArrayInputStream);
      } catch (CertificateException certificateException) {
        throw new AssertionError(certificateException);
      }  
    throw new IllegalArgumentException("bytes == null");
  }
  
  public static class KeyChainConnection implements Closeable {
    private final Context mContext;
    
    private final IKeyChainService mService;
    
    private final ServiceConnection mServiceConnection;
    
    protected KeyChainConnection(Context param1Context, ServiceConnection param1ServiceConnection, IKeyChainService param1IKeyChainService) {
      this.mContext = param1Context;
      this.mServiceConnection = param1ServiceConnection;
      this.mService = param1IKeyChainService;
    }
    
    public void close() {
      this.mContext.unbindService(this.mServiceConnection);
    }
    
    public IKeyChainService getService() {
      return this.mService;
    }
  }
  
  public static KeyChainConnection bind(Context paramContext) throws InterruptedException {
    return bindAsUser(paramContext, Process.myUserHandle());
  }
  
  public static KeyChainConnection bindAsUser(Context paramContext, UserHandle paramUserHandle) throws InterruptedException {
    if (paramContext != null) {
      ensureNotOnMainThread(paramContext);
      if (UserManager.get(paramContext).isUserUnlocked(paramUserHandle)) {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        AtomicReference<IKeyChainService> atomicReference = new AtomicReference();
        Object object = new Object(atomicReference, countDownLatch);
        Intent intent = new Intent(IKeyChainService.class.getName());
        ComponentName componentName = intent.resolveSystemService(paramContext.getPackageManager(), 0);
        intent.setComponent(componentName);
        if (componentName != null && paramContext.bindServiceAsUser(intent, (ServiceConnection)object, 1, paramUserHandle)) {
          countDownLatch.await();
          IKeyChainService iKeyChainService = atomicReference.get();
          if (iKeyChainService != null)
            return new KeyChainConnection(paramContext, (ServiceConnection)object, iKeyChainService); 
          paramContext.unbindService((ServiceConnection)object);
          throw new AssertionError("KeyChainService died while binding");
        } 
        throw new AssertionError("could not bind to KeyChainService");
      } 
      throw new IllegalStateException("User must be unlocked");
    } 
    throw new NullPointerException("context == null");
  }
  
  private static void ensureNotOnMainThread(Context paramContext) {
    Looper looper = Looper.myLooper();
    if (looper == null || looper != paramContext.getMainLooper())
      return; 
    throw new IllegalStateException("calling this from your main thread can lead to deadlock");
  }
}
