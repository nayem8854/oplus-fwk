package android.security;

import android.content.pm.StringParceledListSlice;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.security.keymaster.KeymasterCertificateChain;
import android.security.keystore.ParcelableKeyGenParameterSpec;
import java.util.List;

public interface IKeyChainService extends IInterface {
  int attestKey(String paramString, byte[] paramArrayOfbyte, int[] paramArrayOfint, KeymasterCertificateChain paramKeymasterCertificateChain) throws RemoteException;
  
  boolean containsCaAlias(String paramString) throws RemoteException;
  
  boolean deleteCaCertificate(String paramString) throws RemoteException;
  
  int generateKeyPair(String paramString, ParcelableKeyGenParameterSpec paramParcelableKeyGenParameterSpec) throws RemoteException;
  
  List<String> getCaCertificateChainAliases(String paramString, boolean paramBoolean) throws RemoteException;
  
  byte[] getCaCertificates(String paramString) throws RemoteException;
  
  byte[] getCertificate(String paramString) throws RemoteException;
  
  byte[] getEncodedCaCertificate(String paramString, boolean paramBoolean) throws RemoteException;
  
  StringParceledListSlice getSystemCaAliases() throws RemoteException;
  
  StringParceledListSlice getUserCaAliases() throws RemoteException;
  
  boolean hasGrant(int paramInt, String paramString) throws RemoteException;
  
  String installCaCertificate(byte[] paramArrayOfbyte) throws RemoteException;
  
  boolean installKeyPair(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3, String paramString, int paramInt) throws RemoteException;
  
  boolean isUserSelectable(String paramString) throws RemoteException;
  
  boolean removeKeyPair(String paramString) throws RemoteException;
  
  String requestPrivateKey(String paramString) throws RemoteException;
  
  boolean reset() throws RemoteException;
  
  void setGrant(int paramInt, String paramString, boolean paramBoolean) throws RemoteException;
  
  boolean setKeyPairCertificate(String paramString, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws RemoteException;
  
  void setUserSelectable(String paramString, boolean paramBoolean) throws RemoteException;
  
  class Default implements IKeyChainService {
    public String requestPrivateKey(String param1String) throws RemoteException {
      return null;
    }
    
    public byte[] getCertificate(String param1String) throws RemoteException {
      return null;
    }
    
    public byte[] getCaCertificates(String param1String) throws RemoteException {
      return null;
    }
    
    public boolean isUserSelectable(String param1String) throws RemoteException {
      return false;
    }
    
    public void setUserSelectable(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public int generateKeyPair(String param1String, ParcelableKeyGenParameterSpec param1ParcelableKeyGenParameterSpec) throws RemoteException {
      return 0;
    }
    
    public int attestKey(String param1String, byte[] param1ArrayOfbyte, int[] param1ArrayOfint, KeymasterCertificateChain param1KeymasterCertificateChain) throws RemoteException {
      return 0;
    }
    
    public boolean setKeyPairCertificate(String param1String, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) throws RemoteException {
      return false;
    }
    
    public String installCaCertificate(byte[] param1ArrayOfbyte) throws RemoteException {
      return null;
    }
    
    public boolean installKeyPair(byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3, String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean removeKeyPair(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean deleteCaCertificate(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean reset() throws RemoteException {
      return false;
    }
    
    public StringParceledListSlice getUserCaAliases() throws RemoteException {
      return null;
    }
    
    public StringParceledListSlice getSystemCaAliases() throws RemoteException {
      return null;
    }
    
    public boolean containsCaAlias(String param1String) throws RemoteException {
      return false;
    }
    
    public byte[] getEncodedCaCertificate(String param1String, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public List<String> getCaCertificateChainAliases(String param1String, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public void setGrant(int param1Int, String param1String, boolean param1Boolean) throws RemoteException {}
    
    public boolean hasGrant(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IKeyChainService {
    private static final String DESCRIPTOR = "android.security.IKeyChainService";
    
    static final int TRANSACTION_attestKey = 7;
    
    static final int TRANSACTION_containsCaAlias = 16;
    
    static final int TRANSACTION_deleteCaCertificate = 12;
    
    static final int TRANSACTION_generateKeyPair = 6;
    
    static final int TRANSACTION_getCaCertificateChainAliases = 18;
    
    static final int TRANSACTION_getCaCertificates = 3;
    
    static final int TRANSACTION_getCertificate = 2;
    
    static final int TRANSACTION_getEncodedCaCertificate = 17;
    
    static final int TRANSACTION_getSystemCaAliases = 15;
    
    static final int TRANSACTION_getUserCaAliases = 14;
    
    static final int TRANSACTION_hasGrant = 20;
    
    static final int TRANSACTION_installCaCertificate = 9;
    
    static final int TRANSACTION_installKeyPair = 10;
    
    static final int TRANSACTION_isUserSelectable = 4;
    
    static final int TRANSACTION_removeKeyPair = 11;
    
    static final int TRANSACTION_requestPrivateKey = 1;
    
    static final int TRANSACTION_reset = 13;
    
    static final int TRANSACTION_setGrant = 19;
    
    static final int TRANSACTION_setKeyPairCertificate = 8;
    
    static final int TRANSACTION_setUserSelectable = 5;
    
    public Stub() {
      attachInterface(this, "android.security.IKeyChainService");
    }
    
    public static IKeyChainService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.IKeyChainService");
      if (iInterface != null && iInterface instanceof IKeyChainService)
        return (IKeyChainService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 20:
          return "hasGrant";
        case 19:
          return "setGrant";
        case 18:
          return "getCaCertificateChainAliases";
        case 17:
          return "getEncodedCaCertificate";
        case 16:
          return "containsCaAlias";
        case 15:
          return "getSystemCaAliases";
        case 14:
          return "getUserCaAliases";
        case 13:
          return "reset";
        case 12:
          return "deleteCaCertificate";
        case 11:
          return "removeKeyPair";
        case 10:
          return "installKeyPair";
        case 9:
          return "installCaCertificate";
        case 8:
          return "setKeyPairCertificate";
        case 7:
          return "attestKey";
        case 6:
          return "generateKeyPair";
        case 5:
          return "setUserSelectable";
        case 4:
          return "isUserSelectable";
        case 3:
          return "getCaCertificates";
        case 2:
          return "getCertificate";
        case 1:
          break;
      } 
      return "requestPrivateKey";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        boolean bool1;
        String str7;
        List<String> list;
        byte[] arrayOfByte5;
        String str6;
        StringParceledListSlice stringParceledListSlice;
        String str5;
        byte[] arrayOfByte4;
        String str4;
        byte[] arrayOfByte3;
        int[] arrayOfInt;
        String str3;
        byte[] arrayOfByte2;
        String str2;
        byte[] arrayOfByte1;
        String str10;
        byte[] arrayOfByte7;
        String str9;
        byte[] arrayOfByte6;
        String str8;
        byte[] arrayOfByte8;
        KeymasterCertificateChain keymasterCertificateChain;
        byte[] arrayOfByte9;
        String str12;
        byte[] arrayOfByte10;
        String str11;
        boolean bool5 = false, bool6 = false, bool7 = false, bool8 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 20:
            param1Parcel1.enforceInterface("android.security.IKeyChainService");
            param1Int1 = param1Parcel1.readInt();
            str7 = param1Parcel1.readString();
            bool4 = hasGrant(param1Int1, str7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 19:
            str7.enforceInterface("android.security.IKeyChainService");
            k = str7.readInt();
            str10 = str7.readString();
            bool6 = bool8;
            if (str7.readInt() != 0)
              bool6 = true; 
            setGrant(k, str10, bool6);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            str7.enforceInterface("android.security.IKeyChainService");
            str10 = str7.readString();
            bool6 = bool5;
            if (str7.readInt() != 0)
              bool6 = true; 
            list = getCaCertificateChainAliases(str10, bool6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list);
            return true;
          case 17:
            list.enforceInterface("android.security.IKeyChainService");
            str10 = list.readString();
            if (list.readInt() != 0)
              bool6 = true; 
            arrayOfByte5 = getEncodedCaCertificate(str10, bool6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte5);
            return true;
          case 16:
            arrayOfByte5.enforceInterface("android.security.IKeyChainService");
            str6 = arrayOfByte5.readString();
            bool3 = containsCaAlias(str6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 15:
            str6.enforceInterface("android.security.IKeyChainService");
            stringParceledListSlice = getSystemCaAliases();
            param1Parcel2.writeNoException();
            if (stringParceledListSlice != null) {
              param1Parcel2.writeInt(1);
              stringParceledListSlice.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 14:
            stringParceledListSlice.enforceInterface("android.security.IKeyChainService");
            stringParceledListSlice = getUserCaAliases();
            param1Parcel2.writeNoException();
            if (stringParceledListSlice != null) {
              param1Parcel2.writeInt(1);
              stringParceledListSlice.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 13:
            stringParceledListSlice.enforceInterface("android.security.IKeyChainService");
            bool3 = reset();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 12:
            stringParceledListSlice.enforceInterface("android.security.IKeyChainService");
            str5 = stringParceledListSlice.readString();
            bool3 = deleteCaCertificate(str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 11:
            str5.enforceInterface("android.security.IKeyChainService");
            str5 = str5.readString();
            bool3 = removeKeyPair(str5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 10:
            str5.enforceInterface("android.security.IKeyChainService");
            arrayOfByte8 = str5.createByteArray();
            arrayOfByte9 = str5.createByteArray();
            arrayOfByte7 = str5.createByteArray();
            str12 = str5.readString();
            j = str5.readInt();
            bool2 = installKeyPair(arrayOfByte8, arrayOfByte9, arrayOfByte7, str12, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 9:
            str5.enforceInterface("android.security.IKeyChainService");
            arrayOfByte4 = str5.createByteArray();
            str4 = installCaCertificate(arrayOfByte4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str4);
            return true;
          case 8:
            str4.enforceInterface("android.security.IKeyChainService");
            str9 = str4.readString();
            arrayOfByte10 = str4.createByteArray();
            arrayOfByte3 = str4.createByteArray();
            bool2 = setKeyPairCertificate(str9, arrayOfByte10, arrayOfByte3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 7:
            arrayOfByte3.enforceInterface("android.security.IKeyChainService");
            str11 = arrayOfByte3.readString();
            arrayOfByte6 = arrayOfByte3.createByteArray();
            arrayOfInt = arrayOfByte3.createIntArray();
            keymasterCertificateChain = new KeymasterCertificateChain();
            i = attestKey(str11, arrayOfByte6, arrayOfInt, keymasterCertificateChain);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            param1Parcel2.writeInt(1);
            keymasterCertificateChain.writeToParcel(param1Parcel2, 1);
            return true;
          case 6:
            arrayOfInt.enforceInterface("android.security.IKeyChainService");
            str8 = arrayOfInt.readString();
            if (arrayOfInt.readInt() != 0) {
              ParcelableKeyGenParameterSpec parcelableKeyGenParameterSpec = ParcelableKeyGenParameterSpec.CREATOR.createFromParcel((Parcel)arrayOfInt);
            } else {
              arrayOfInt = null;
            } 
            i = generateKeyPair(str8, (ParcelableKeyGenParameterSpec)arrayOfInt);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 5:
            arrayOfInt.enforceInterface("android.security.IKeyChainService");
            str8 = arrayOfInt.readString();
            bool6 = bool7;
            if (arrayOfInt.readInt() != 0)
              bool6 = true; 
            setUserSelectable(str8, bool6);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            arrayOfInt.enforceInterface("android.security.IKeyChainService");
            str3 = arrayOfInt.readString();
            bool1 = isUserSelectable(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            str3.enforceInterface("android.security.IKeyChainService");
            str3 = str3.readString();
            arrayOfByte2 = getCaCertificates(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 2:
            arrayOfByte2.enforceInterface("android.security.IKeyChainService");
            str2 = arrayOfByte2.readString();
            arrayOfByte1 = getCertificate(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte1);
            return true;
          case 1:
            break;
        } 
        arrayOfByte1.enforceInterface("android.security.IKeyChainService");
        String str1 = arrayOfByte1.readString();
        str1 = requestPrivateKey(str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str1);
        return true;
      } 
      param1Parcel2.writeString("android.security.IKeyChainService");
      return true;
    }
    
    private static class Proxy implements IKeyChainService {
      public static IKeyChainService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.IKeyChainService";
      }
      
      public String requestPrivateKey(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IKeyChainService.Stub.getDefaultImpl() != null) {
            param2String = IKeyChainService.Stub.getDefaultImpl().requestPrivateKey(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getCertificate(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IKeyChainService.Stub.getDefaultImpl() != null)
            return IKeyChainService.Stub.getDefaultImpl().getCertificate(param2String); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getCaCertificates(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IKeyChainService.Stub.getDefaultImpl() != null)
            return IKeyChainService.Stub.getDefaultImpl().getCaCertificates(param2String); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUserSelectable(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IKeyChainService.Stub.getDefaultImpl() != null) {
            bool1 = IKeyChainService.Stub.getDefaultImpl().isUserSelectable(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUserSelectable(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool1 && IKeyChainService.Stub.getDefaultImpl() != null) {
            IKeyChainService.Stub.getDefaultImpl().setUserSelectable(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int generateKeyPair(String param2String, ParcelableKeyGenParameterSpec param2ParcelableKeyGenParameterSpec) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeString(param2String);
          if (param2ParcelableKeyGenParameterSpec != null) {
            parcel1.writeInt(1);
            param2ParcelableKeyGenParameterSpec.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IKeyChainService.Stub.getDefaultImpl() != null)
            return IKeyChainService.Stub.getDefaultImpl().generateKeyPair(param2String, param2ParcelableKeyGenParameterSpec); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int attestKey(String param2String, byte[] param2ArrayOfbyte, int[] param2ArrayOfint, KeymasterCertificateChain param2KeymasterCertificateChain) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IKeyChainService.Stub.getDefaultImpl() != null)
            return IKeyChainService.Stub.getDefaultImpl().attestKey(param2String, param2ArrayOfbyte, param2ArrayOfint, param2KeymasterCertificateChain); 
          parcel2.readException();
          int i = parcel2.readInt();
          if (parcel2.readInt() != 0)
            param2KeymasterCertificateChain.readFromParcel(parcel2); 
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setKeyPairCertificate(String param2String, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte1);
          parcel1.writeByteArray(param2ArrayOfbyte2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IKeyChainService.Stub.getDefaultImpl() != null) {
            bool1 = IKeyChainService.Stub.getDefaultImpl().setKeyPairCertificate(param2String, param2ArrayOfbyte1, param2ArrayOfbyte2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String installCaCertificate(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IKeyChainService.Stub.getDefaultImpl() != null)
            return IKeyChainService.Stub.getDefaultImpl().installCaCertificate(param2ArrayOfbyte); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean installKeyPair(byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2, byte[] param2ArrayOfbyte3, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          try {
            parcel1.writeByteArray(param2ArrayOfbyte1);
            try {
              parcel1.writeByteArray(param2ArrayOfbyte2);
              try {
                parcel1.writeByteArray(param2ArrayOfbyte3);
                try {
                  parcel1.writeString(param2String);
                  try {
                    parcel1.writeInt(param2Int);
                    try {
                      IBinder iBinder = this.mRemote;
                      boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
                      if (!bool2 && IKeyChainService.Stub.getDefaultImpl() != null) {
                        bool1 = IKeyChainService.Stub.getDefaultImpl().installKeyPair(param2ArrayOfbyte1, param2ArrayOfbyte2, param2ArrayOfbyte3, param2String, param2Int);
                        parcel2.recycle();
                        parcel1.recycle();
                        return bool1;
                      } 
                      parcel2.readException();
                      param2Int = parcel2.readInt();
                      if (param2Int != 0)
                        bool1 = true; 
                      parcel2.recycle();
                      parcel1.recycle();
                      return bool1;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ArrayOfbyte1;
      }
      
      public boolean removeKeyPair(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IKeyChainService.Stub.getDefaultImpl() != null) {
            bool1 = IKeyChainService.Stub.getDefaultImpl().removeKeyPair(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean deleteCaCertificate(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && IKeyChainService.Stub.getDefaultImpl() != null) {
            bool1 = IKeyChainService.Stub.getDefaultImpl().deleteCaCertificate(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean reset() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IKeyChainService.Stub.getDefaultImpl() != null) {
            bool1 = IKeyChainService.Stub.getDefaultImpl().reset();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StringParceledListSlice getUserCaAliases() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          StringParceledListSlice stringParceledListSlice;
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IKeyChainService.Stub.getDefaultImpl() != null) {
            stringParceledListSlice = IKeyChainService.Stub.getDefaultImpl().getUserCaAliases();
            return stringParceledListSlice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            stringParceledListSlice = StringParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            stringParceledListSlice = null;
          } 
          return stringParceledListSlice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StringParceledListSlice getSystemCaAliases() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          StringParceledListSlice stringParceledListSlice;
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IKeyChainService.Stub.getDefaultImpl() != null) {
            stringParceledListSlice = IKeyChainService.Stub.getDefaultImpl().getSystemCaAliases();
            return stringParceledListSlice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            stringParceledListSlice = StringParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            stringParceledListSlice = null;
          } 
          return stringParceledListSlice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean containsCaAlias(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && IKeyChainService.Stub.getDefaultImpl() != null) {
            bool1 = IKeyChainService.Stub.getDefaultImpl().containsCaAlias(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getEncodedCaCertificate(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool1 && IKeyChainService.Stub.getDefaultImpl() != null)
            return IKeyChainService.Stub.getDefaultImpl().getEncodedCaCertificate(param2String, param2Boolean); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getCaCertificateChainAliases(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool1 && IKeyChainService.Stub.getDefaultImpl() != null)
            return IKeyChainService.Stub.getDefaultImpl().getCaCertificateChainAliases(param2String, param2Boolean); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setGrant(int param2Int, String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool1 && IKeyChainService.Stub.getDefaultImpl() != null) {
            IKeyChainService.Stub.getDefaultImpl().setGrant(param2Int, param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasGrant(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IKeyChainService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(20, parcel1, parcel2, 0);
          if (!bool2 && IKeyChainService.Stub.getDefaultImpl() != null) {
            bool1 = IKeyChainService.Stub.getDefaultImpl().hasGrant(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IKeyChainService param1IKeyChainService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IKeyChainService != null) {
          Proxy.sDefaultImpl = param1IKeyChainService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IKeyChainService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
