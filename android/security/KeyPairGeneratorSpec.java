package android.security;

import android.content.Context;
import android.security.keystore.KeyProperties;
import android.text.TextUtils;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Date;
import javax.security.auth.x500.X500Principal;

@Deprecated
public final class KeyPairGeneratorSpec implements AlgorithmParameterSpec {
  private final Context mContext;
  
  private final Date mEndDate;
  
  private final int mFlags;
  
  private final int mKeySize;
  
  private final String mKeyType;
  
  private final String mKeystoreAlias;
  
  private final BigInteger mSerialNumber;
  
  private final AlgorithmParameterSpec mSpec;
  
  private final Date mStartDate;
  
  private final X500Principal mSubjectDN;
  
  public KeyPairGeneratorSpec(Context paramContext, String paramString1, String paramString2, int paramInt1, AlgorithmParameterSpec paramAlgorithmParameterSpec, X500Principal paramX500Principal, BigInteger paramBigInteger, Date paramDate1, Date paramDate2, int paramInt2) {
    if (paramContext != null) {
      if (!TextUtils.isEmpty(paramString1)) {
        if (paramX500Principal != null) {
          if (paramBigInteger != null) {
            if (paramDate1 != null) {
              if (paramDate2 != null) {
                if (!paramDate2.before(paramDate1)) {
                  if (!paramDate2.before(paramDate1)) {
                    this.mContext = paramContext;
                    this.mKeystoreAlias = paramString1;
                    this.mKeyType = paramString2;
                    this.mKeySize = paramInt1;
                    this.mSpec = paramAlgorithmParameterSpec;
                    this.mSubjectDN = paramX500Principal;
                    this.mSerialNumber = paramBigInteger;
                    this.mStartDate = paramDate1;
                    this.mEndDate = paramDate2;
                    this.mFlags = paramInt2;
                    return;
                  } 
                  throw new IllegalArgumentException("endDate < startDate");
                } 
                throw new IllegalArgumentException("endDate < startDate");
              } 
              throw new IllegalArgumentException("endDate == null");
            } 
            throw new IllegalArgumentException("startDate == null");
          } 
          throw new IllegalArgumentException("serialNumber == null");
        } 
        throw new IllegalArgumentException("subjectDN == null");
      } 
      throw new IllegalArgumentException("keyStoreAlias must not be empty");
    } 
    throw new IllegalArgumentException("context == null");
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  public String getKeystoreAlias() {
    return this.mKeystoreAlias;
  }
  
  public String getKeyType() {
    return this.mKeyType;
  }
  
  public int getKeySize() {
    return this.mKeySize;
  }
  
  public AlgorithmParameterSpec getAlgorithmParameterSpec() {
    return this.mSpec;
  }
  
  public X500Principal getSubjectDN() {
    return this.mSubjectDN;
  }
  
  public BigInteger getSerialNumber() {
    return this.mSerialNumber;
  }
  
  public Date getStartDate() {
    return this.mStartDate;
  }
  
  public Date getEndDate() {
    return this.mEndDate;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public boolean isEncryptionRequired() {
    int i = this.mFlags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  @Deprecated
  public static final class Builder {
    private final Context mContext;
    
    private Date mEndDate;
    
    private int mFlags;
    
    private int mKeySize = -1;
    
    private String mKeyType;
    
    private String mKeystoreAlias;
    
    private BigInteger mSerialNumber;
    
    private AlgorithmParameterSpec mSpec;
    
    private Date mStartDate;
    
    private X500Principal mSubjectDN;
    
    public Builder(Context param1Context) {
      if (param1Context != null) {
        this.mContext = param1Context;
        return;
      } 
      throw new NullPointerException("context == null");
    }
    
    public Builder setAlias(String param1String) {
      if (param1String != null) {
        this.mKeystoreAlias = param1String;
        return this;
      } 
      throw new NullPointerException("alias == null");
    }
    
    public Builder setKeyType(String param1String) throws NoSuchAlgorithmException {
      if (param1String != null)
        try {
          KeyProperties.KeyAlgorithm.toKeymasterAsymmetricKeyAlgorithm(param1String);
          this.mKeyType = param1String;
          return this;
        } catch (IllegalArgumentException illegalArgumentException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unsupported key type: ");
          stringBuilder.append(param1String);
          throw new NoSuchAlgorithmException(stringBuilder.toString());
        }  
      throw new NullPointerException("keyType == null");
    }
    
    public Builder setKeySize(int param1Int) {
      if (param1Int >= 0) {
        this.mKeySize = param1Int;
        return this;
      } 
      throw new IllegalArgumentException("keySize < 0");
    }
    
    public Builder setAlgorithmParameterSpec(AlgorithmParameterSpec param1AlgorithmParameterSpec) {
      if (param1AlgorithmParameterSpec != null) {
        this.mSpec = param1AlgorithmParameterSpec;
        return this;
      } 
      throw new NullPointerException("spec == null");
    }
    
    public Builder setSubject(X500Principal param1X500Principal) {
      if (param1X500Principal != null) {
        this.mSubjectDN = param1X500Principal;
        return this;
      } 
      throw new NullPointerException("subject == null");
    }
    
    public Builder setSerialNumber(BigInteger param1BigInteger) {
      if (param1BigInteger != null) {
        this.mSerialNumber = param1BigInteger;
        return this;
      } 
      throw new NullPointerException("serialNumber == null");
    }
    
    public Builder setStartDate(Date param1Date) {
      if (param1Date != null) {
        this.mStartDate = param1Date;
        return this;
      } 
      throw new NullPointerException("startDate == null");
    }
    
    public Builder setEndDate(Date param1Date) {
      if (param1Date != null) {
        this.mEndDate = param1Date;
        return this;
      } 
      throw new NullPointerException("endDate == null");
    }
    
    public Builder setEncryptionRequired() {
      this.mFlags |= 0x1;
      return this;
    }
    
    public KeyPairGeneratorSpec build() {
      return new KeyPairGeneratorSpec(this.mContext, this.mKeystoreAlias, this.mKeyType, this.mKeySize, this.mSpec, this.mSubjectDN, this.mSerialNumber, this.mStartDate, this.mEndDate, this.mFlags);
    }
  }
}
