package android.security.keystore;

import java.util.Date;

abstract class Utils {
  static Date cloneIfNotNull(Date paramDate) {
    if (paramDate != null) {
      paramDate = (Date)paramDate.clone();
    } else {
      paramDate = null;
    } 
    return paramDate;
  }
  
  static byte[] cloneIfNotNull(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      paramArrayOfbyte = (byte[])paramArrayOfbyte.clone();
    } else {
      paramArrayOfbyte = null;
    } 
    return paramArrayOfbyte;
  }
}
