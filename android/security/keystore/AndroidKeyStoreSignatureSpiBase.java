package android.security.keystore;

import android.os.IBinder;
import android.security.KeyStore;
import android.security.KeyStoreException;
import android.security.keymaster.KeymasterArguments;
import android.security.keymaster.OperationResult;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.PrivateKey;
import java.security.ProviderException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.SignatureSpi;
import libcore.util.EmptyArray;

abstract class AndroidKeyStoreSignatureSpiBase extends SignatureSpi implements KeyStoreCryptoOperation {
  private Exception mCachedException;
  
  private AndroidKeyStoreKey mKey;
  
  private final KeyStore mKeyStore = KeyStore.getInstance();
  
  private KeyStoreCryptoOperationStreamer mMessageStreamer;
  
  private long mOperationHandle;
  
  private IBinder mOperationToken;
  
  private boolean mSigning;
  
  protected final void engineInitSign(PrivateKey paramPrivateKey) throws InvalidKeyException {
    engineInitSign(paramPrivateKey, null);
  }
  
  protected final void engineInitSign(PrivateKey paramPrivateKey, SecureRandom paramSecureRandom) throws InvalidKeyException {
    resetAll();
    if (paramPrivateKey != null)
      try {
        AndroidKeyStoreKey androidKeyStoreKey;
        if (paramPrivateKey instanceof AndroidKeyStorePrivateKey) {
          androidKeyStoreKey = (AndroidKeyStoreKey)paramPrivateKey;
          this.mSigning = true;
          initKey(androidKeyStoreKey);
          this.appRandom = paramSecureRandom;
          ensureKeystoreOperationInitialized();
          return;
        } 
        InvalidKeyException invalidKeyException1 = new InvalidKeyException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unsupported private key type: ");
        stringBuilder.append(androidKeyStoreKey);
        this(stringBuilder.toString());
        throw invalidKeyException1;
      } finally {
        if (!false)
          resetAll(); 
      }  
    InvalidKeyException invalidKeyException = new InvalidKeyException();
    this("Unsupported key: null");
    throw invalidKeyException;
  }
  
  protected final void engineInitVerify(PublicKey paramPublicKey) throws InvalidKeyException {
    resetAll();
    if (paramPublicKey != null)
      try {
        if (paramPublicKey instanceof AndroidKeyStorePublicKey) {
          paramPublicKey = paramPublicKey;
          this.mSigning = false;
          initKey((AndroidKeyStoreKey)paramPublicKey);
          this.appRandom = null;
          ensureKeystoreOperationInitialized();
          return;
        } 
        InvalidKeyException invalidKeyException1 = new InvalidKeyException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unsupported public key type: ");
        stringBuilder.append(paramPublicKey);
        this(stringBuilder.toString());
        throw invalidKeyException1;
      } finally {
        if (!false)
          resetAll(); 
      }  
    InvalidKeyException invalidKeyException = new InvalidKeyException();
    this("Unsupported key: null");
    throw invalidKeyException;
  }
  
  protected void initKey(AndroidKeyStoreKey paramAndroidKeyStoreKey) throws InvalidKeyException {
    this.mKey = paramAndroidKeyStoreKey;
  }
  
  protected void resetAll() {
    IBinder iBinder = this.mOperationToken;
    if (iBinder != null) {
      this.mOperationToken = null;
      this.mKeyStore.abort(iBinder);
    } 
    this.mSigning = false;
    this.mKey = null;
    this.appRandom = null;
    this.mOperationToken = null;
    this.mOperationHandle = 0L;
    this.mMessageStreamer = null;
    this.mCachedException = null;
  }
  
  protected void resetWhilePreservingInitState() {
    IBinder iBinder = this.mOperationToken;
    if (iBinder != null) {
      this.mOperationToken = null;
      this.mKeyStore.abort(iBinder);
    } 
    this.mOperationHandle = 0L;
    this.mMessageStreamer = null;
    this.mCachedException = null;
  }
  
  private void ensureKeystoreOperationInitialized() throws InvalidKeyException {
    if (this.mMessageStreamer != null)
      return; 
    if (this.mCachedException != null)
      return; 
    if (this.mKey != null) {
      byte b;
      KeymasterArguments keymasterArguments = new KeymasterArguments();
      addAlgorithmSpecificParametersToBegin(keymasterArguments);
      KeyStore keyStore = this.mKeyStore;
      AndroidKeyStoreKey androidKeyStoreKey = this.mKey;
      String str = androidKeyStoreKey.getAlias();
      if (this.mSigning) {
        b = 2;
      } else {
        b = 3;
      } 
      androidKeyStoreKey = this.mKey;
      int i = androidKeyStoreKey.getUid();
      OperationResult operationResult = keyStore.begin(str, b, true, keymasterArguments, null, i);
      if (operationResult != null) {
        this.mOperationToken = operationResult.token;
        this.mOperationHandle = operationResult.operationHandle;
        InvalidKeyException invalidKeyException = KeyStoreCryptoOperationUtils.getInvalidKeyExceptionForInit(this.mKeyStore, this.mKey, operationResult.resultCode);
        if (invalidKeyException == null) {
          if (this.mOperationToken != null) {
            if (this.mOperationHandle != 0L) {
              this.mMessageStreamer = createMainDataStreamer(this.mKeyStore, operationResult.token);
              return;
            } 
            throw new ProviderException("Keystore returned invalid operation handle");
          } 
          throw new ProviderException("Keystore returned null operation token");
        } 
        throw invalidKeyException;
      } 
      throw new KeyStoreConnectException();
    } 
    throw new IllegalStateException("Not initialized");
  }
  
  protected KeyStoreCryptoOperationStreamer createMainDataStreamer(KeyStore paramKeyStore, IBinder paramIBinder) {
    return new KeyStoreCryptoOperationChunkedStreamer(new KeyStoreCryptoOperationChunkedStreamer.MainDataStream(paramKeyStore, paramIBinder));
  }
  
  public final long getOperationHandle() {
    return this.mOperationHandle;
  }
  
  protected final void engineUpdate(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws SignatureException {
    if (this.mCachedException == null)
      try {
        ensureKeystoreOperationInitialized();
        if (paramInt2 == 0)
          return; 
        try {
          byte[] arrayOfByte = this.mMessageStreamer.update(paramArrayOfbyte, paramInt1, paramInt2);
          if (arrayOfByte.length == 0)
            return; 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Update operation unexpectedly produced output: ");
          stringBuilder.append(arrayOfByte.length);
          stringBuilder.append(" bytes");
          throw new ProviderException(stringBuilder.toString());
        } catch (KeyStoreException keyStoreException) {
          throw new SignatureException(keyStoreException);
        } 
      } catch (InvalidKeyException invalidKeyException) {
        throw new SignatureException(invalidKeyException);
      }  
    throw new SignatureException(this.mCachedException);
  }
  
  protected final void engineUpdate(byte paramByte) throws SignatureException {
    engineUpdate(new byte[] { paramByte }, 0, 1);
  }
  
  protected final void engineUpdate(ByteBuffer paramByteBuffer) {
    byte[] arrayOfByte;
    boolean bool;
    int i = paramByteBuffer.remaining();
    if (paramByteBuffer.hasArray()) {
      byte[] arrayOfByte1 = paramByteBuffer.array();
      bool = paramByteBuffer.arrayOffset() + paramByteBuffer.position();
      paramByteBuffer.position(paramByteBuffer.limit());
      arrayOfByte = arrayOfByte1;
    } else {
      byte[] arrayOfByte1 = new byte[i];
      bool = false;
      arrayOfByte.get(arrayOfByte1);
      arrayOfByte = arrayOfByte1;
    } 
    try {
      engineUpdate(arrayOfByte, bool, i);
    } catch (SignatureException signatureException) {
      this.mCachedException = signatureException;
    } 
  }
  
  protected final int engineSign(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws SignatureException {
    return super.engineSign(paramArrayOfbyte, paramInt1, paramInt2);
  }
  
  protected final byte[] engineSign() throws SignatureException {
    if (this.mCachedException == null)
      try {
        ensureKeystoreOperationInitialized();
        SecureRandom secureRandom = this.appRandom;
        int i = getAdditionalEntropyAmountForSign();
        byte[] arrayOfByte = KeyStoreCryptoOperationUtils.getRandomBytesToMixIntoKeystoreRng(secureRandom, i);
        arrayOfByte = this.mMessageStreamer.doFinal(EmptyArray.BYTE, 0, 0, null, arrayOfByte);
        resetWhilePreservingInitState();
        return arrayOfByte;
      } catch (InvalidKeyException|KeyStoreException invalidKeyException) {
        throw new SignatureException(invalidKeyException);
      }  
    throw new SignatureException(this.mCachedException);
  }
  
  protected final boolean engineVerify(byte[] paramArrayOfbyte) throws SignatureException {
    if (this.mCachedException == null)
      try {
        boolean bool;
        ensureKeystoreOperationInitialized();
        try {
          paramArrayOfbyte = this.mMessageStreamer.doFinal(EmptyArray.BYTE, 0, 0, paramArrayOfbyte, null);
          if (paramArrayOfbyte.length == 0) {
            bool = true;
          } else {
            ProviderException providerException = new ProviderException();
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Signature verification unexpected produced output: ");
            stringBuilder.append(paramArrayOfbyte.length);
            stringBuilder.append(" bytes");
            this(stringBuilder.toString());
            throw providerException;
          } 
        } catch (KeyStoreException keyStoreException) {
          if (keyStoreException.getErrorCode() == -30) {
            bool = false;
            resetWhilePreservingInitState();
            return bool;
          } 
        } 
        resetWhilePreservingInitState();
        return bool;
      } catch (InvalidKeyException invalidKeyException) {
        throw new SignatureException(invalidKeyException);
      }  
    throw new SignatureException(this.mCachedException);
  }
  
  protected final boolean engineVerify(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws SignatureException {
    return engineVerify(ArrayUtils.subarray(paramArrayOfbyte, paramInt1, paramInt2));
  }
  
  @Deprecated
  protected final Object engineGetParameter(String paramString) throws InvalidParameterException {
    throw new InvalidParameterException();
  }
  
  @Deprecated
  protected final void engineSetParameter(String paramString, Object paramObject) throws InvalidParameterException {
    throw new InvalidParameterException();
  }
  
  protected final KeyStore getKeyStore() {
    return this.mKeyStore;
  }
  
  protected final boolean isSigning() {
    return this.mSigning;
  }
  
  protected abstract void addAlgorithmSpecificParametersToBegin(KeymasterArguments paramKeymasterArguments);
  
  protected abstract int getAdditionalEntropyAmountForSign();
}
