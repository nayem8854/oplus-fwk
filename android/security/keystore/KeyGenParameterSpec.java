package android.security.keystore;

import android.annotation.SystemApi;
import android.text.TextUtils;
import java.math.BigInteger;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Date;
import javax.security.auth.x500.X500Principal;

public final class KeyGenParameterSpec implements AlgorithmParameterSpec, UserAuthArgs {
  private static final Date DEFAULT_CERT_NOT_AFTER;
  
  private static final Date DEFAULT_CERT_NOT_BEFORE;
  
  private static final BigInteger DEFAULT_CERT_SERIAL_NUMBER;
  
  private static final X500Principal DEFAULT_CERT_SUBJECT = new X500Principal("CN=fake");
  
  private final byte[] mAttestationChallenge;
  
  private final String[] mBlockModes;
  
  private final Date mCertificateNotAfter;
  
  private final Date mCertificateNotBefore;
  
  private final BigInteger mCertificateSerialNumber;
  
  private final X500Principal mCertificateSubject;
  
  private final boolean mCriticalToDeviceEncryption;
  
  private final String[] mDigests;
  
  private final String[] mEncryptionPaddings;
  
  private final boolean mInvalidatedByBiometricEnrollment;
  
  private final boolean mIsStrongBoxBacked;
  
  private final int mKeySize;
  
  private final Date mKeyValidityForConsumptionEnd;
  
  private final Date mKeyValidityForOriginationEnd;
  
  private final Date mKeyValidityStart;
  
  private final String mKeystoreAlias;
  
  private final int mPurposes;
  
  private final boolean mRandomizedEncryptionRequired;
  
  private final String[] mSignaturePaddings;
  
  private final AlgorithmParameterSpec mSpec;
  
  private final int mUid;
  
  private final boolean mUniqueIdIncluded;
  
  private final boolean mUnlockedDeviceRequired;
  
  private final boolean mUserAuthenticationRequired;
  
  private final int mUserAuthenticationType;
  
  private final boolean mUserAuthenticationValidWhileOnBody;
  
  private final int mUserAuthenticationValidityDurationSeconds;
  
  private final boolean mUserConfirmationRequired;
  
  private final boolean mUserPresenceRequired;
  
  static {
    DEFAULT_CERT_SERIAL_NUMBER = new BigInteger("1");
    DEFAULT_CERT_NOT_BEFORE = new Date(0L);
    DEFAULT_CERT_NOT_AFTER = new Date(2461449600000L);
  }
  
  public KeyGenParameterSpec(String paramString, int paramInt1, int paramInt2, AlgorithmParameterSpec paramAlgorithmParameterSpec, X500Principal paramX500Principal, BigInteger paramBigInteger, Date paramDate1, Date paramDate2, Date paramDate3, Date paramDate4, Date paramDate5, int paramInt3, String[] paramArrayOfString1, String[] paramArrayOfString2, String[] paramArrayOfString3, String[] paramArrayOfString4, boolean paramBoolean1, boolean paramBoolean2, int paramInt4, int paramInt5, boolean paramBoolean3, byte[] paramArrayOfbyte, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, boolean paramBoolean8, boolean paramBoolean9, boolean paramBoolean10) {
    if (!TextUtils.isEmpty(paramString)) {
      if (paramX500Principal == null)
        paramX500Principal = DEFAULT_CERT_SUBJECT; 
      if (paramDate1 == null)
        paramDate1 = DEFAULT_CERT_NOT_BEFORE; 
      if (paramDate2 == null)
        paramDate2 = DEFAULT_CERT_NOT_AFTER; 
      if (paramBigInteger == null)
        paramBigInteger = DEFAULT_CERT_SERIAL_NUMBER; 
      if (!paramDate2.before(paramDate1)) {
        this.mKeystoreAlias = paramString;
        this.mUid = paramInt1;
        this.mKeySize = paramInt2;
        this.mSpec = paramAlgorithmParameterSpec;
        this.mCertificateSubject = paramX500Principal;
        this.mCertificateSerialNumber = paramBigInteger;
        this.mCertificateNotBefore = Utils.cloneIfNotNull(paramDate1);
        this.mCertificateNotAfter = Utils.cloneIfNotNull(paramDate2);
        this.mKeyValidityStart = Utils.cloneIfNotNull(paramDate3);
        this.mKeyValidityForOriginationEnd = Utils.cloneIfNotNull(paramDate4);
        this.mKeyValidityForConsumptionEnd = Utils.cloneIfNotNull(paramDate5);
        this.mPurposes = paramInt3;
        this.mDigests = ArrayUtils.cloneIfNotEmpty(paramArrayOfString1);
        this.mEncryptionPaddings = ArrayUtils.cloneIfNotEmpty(ArrayUtils.nullToEmpty(paramArrayOfString2));
        this.mSignaturePaddings = ArrayUtils.cloneIfNotEmpty(ArrayUtils.nullToEmpty(paramArrayOfString3));
        this.mBlockModes = ArrayUtils.cloneIfNotEmpty(ArrayUtils.nullToEmpty(paramArrayOfString4));
        this.mRandomizedEncryptionRequired = paramBoolean1;
        this.mUserAuthenticationRequired = paramBoolean2;
        this.mUserPresenceRequired = paramBoolean3;
        this.mUserAuthenticationValidityDurationSeconds = paramInt4;
        this.mUserAuthenticationType = paramInt5;
        this.mAttestationChallenge = Utils.cloneIfNotNull(paramArrayOfbyte);
        this.mUniqueIdIncluded = paramBoolean4;
        this.mUserAuthenticationValidWhileOnBody = paramBoolean5;
        this.mInvalidatedByBiometricEnrollment = paramBoolean6;
        this.mIsStrongBoxBacked = paramBoolean7;
        this.mUserConfirmationRequired = paramBoolean8;
        this.mUnlockedDeviceRequired = paramBoolean9;
        this.mCriticalToDeviceEncryption = paramBoolean10;
        return;
      } 
      throw new IllegalArgumentException("certificateNotAfter < certificateNotBefore");
    } 
    throw new IllegalArgumentException("keyStoreAlias must not be empty");
  }
  
  public String getKeystoreAlias() {
    return this.mKeystoreAlias;
  }
  
  public int getUid() {
    return this.mUid;
  }
  
  public int getKeySize() {
    return this.mKeySize;
  }
  
  public AlgorithmParameterSpec getAlgorithmParameterSpec() {
    return this.mSpec;
  }
  
  public X500Principal getCertificateSubject() {
    return this.mCertificateSubject;
  }
  
  public BigInteger getCertificateSerialNumber() {
    return this.mCertificateSerialNumber;
  }
  
  public Date getCertificateNotBefore() {
    return Utils.cloneIfNotNull(this.mCertificateNotBefore);
  }
  
  public Date getCertificateNotAfter() {
    return Utils.cloneIfNotNull(this.mCertificateNotAfter);
  }
  
  public Date getKeyValidityStart() {
    return Utils.cloneIfNotNull(this.mKeyValidityStart);
  }
  
  public Date getKeyValidityForConsumptionEnd() {
    return Utils.cloneIfNotNull(this.mKeyValidityForConsumptionEnd);
  }
  
  public Date getKeyValidityForOriginationEnd() {
    return Utils.cloneIfNotNull(this.mKeyValidityForOriginationEnd);
  }
  
  public int getPurposes() {
    return this.mPurposes;
  }
  
  public String[] getDigests() {
    String[] arrayOfString = this.mDigests;
    if (arrayOfString != null)
      return ArrayUtils.cloneIfNotEmpty(arrayOfString); 
    throw new IllegalStateException("Digests not specified");
  }
  
  public boolean isDigestsSpecified() {
    boolean bool;
    if (this.mDigests != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String[] getEncryptionPaddings() {
    return ArrayUtils.cloneIfNotEmpty(this.mEncryptionPaddings);
  }
  
  public String[] getSignaturePaddings() {
    return ArrayUtils.cloneIfNotEmpty(this.mSignaturePaddings);
  }
  
  public String[] getBlockModes() {
    return ArrayUtils.cloneIfNotEmpty(this.mBlockModes);
  }
  
  public boolean isRandomizedEncryptionRequired() {
    return this.mRandomizedEncryptionRequired;
  }
  
  public boolean isUserAuthenticationRequired() {
    return this.mUserAuthenticationRequired;
  }
  
  public boolean isUserConfirmationRequired() {
    return this.mUserConfirmationRequired;
  }
  
  public int getUserAuthenticationValidityDurationSeconds() {
    return this.mUserAuthenticationValidityDurationSeconds;
  }
  
  public int getUserAuthenticationType() {
    return this.mUserAuthenticationType;
  }
  
  public boolean isUserPresenceRequired() {
    return this.mUserPresenceRequired;
  }
  
  public byte[] getAttestationChallenge() {
    return Utils.cloneIfNotNull(this.mAttestationChallenge);
  }
  
  public boolean isUniqueIdIncluded() {
    return this.mUniqueIdIncluded;
  }
  
  public boolean isUserAuthenticationValidWhileOnBody() {
    return this.mUserAuthenticationValidWhileOnBody;
  }
  
  public boolean isInvalidatedByBiometricEnrollment() {
    return this.mInvalidatedByBiometricEnrollment;
  }
  
  public boolean isStrongBoxBacked() {
    return this.mIsStrongBoxBacked;
  }
  
  public boolean isUnlockedDeviceRequired() {
    return this.mUnlockedDeviceRequired;
  }
  
  public long getBoundToSpecificSecureUserId() {
    return 0L;
  }
  
  public boolean isCriticalToDeviceEncryption() {
    return this.mCriticalToDeviceEncryption;
  }
  
  class Builder {
    private int mUid = -1;
    
    private int mKeySize = -1;
    
    private boolean mRandomizedEncryptionRequired = true;
    
    private int mUserAuthenticationValidityDurationSeconds = 0;
    
    private int mUserAuthenticationType = 2;
    
    private boolean mUserPresenceRequired = false;
    
    private byte[] mAttestationChallenge = null;
    
    private boolean mUniqueIdIncluded = false;
    
    private boolean mInvalidatedByBiometricEnrollment = true;
    
    private boolean mIsStrongBoxBacked = false;
    
    private boolean mUnlockedDeviceRequired = false;
    
    private boolean mCriticalToDeviceEncryption = false;
    
    private String[] mBlockModes;
    
    private Date mCertificateNotAfter;
    
    private Date mCertificateNotBefore;
    
    private BigInteger mCertificateSerialNumber;
    
    private X500Principal mCertificateSubject;
    
    private String[] mDigests;
    
    private String[] mEncryptionPaddings;
    
    private Date mKeyValidityForConsumptionEnd;
    
    private Date mKeyValidityForOriginationEnd;
    
    private Date mKeyValidityStart;
    
    private final String mKeystoreAlias;
    
    private int mPurposes;
    
    private String[] mSignaturePaddings;
    
    private AlgorithmParameterSpec mSpec;
    
    private boolean mUserAuthenticationRequired;
    
    private boolean mUserAuthenticationValidWhileOnBody;
    
    private boolean mUserConfirmationRequired;
    
    public Builder(int param1Int) {
      if (KeyGenParameterSpec.this != null) {
        if (!KeyGenParameterSpec.this.isEmpty()) {
          this.mKeystoreAlias = (String)KeyGenParameterSpec.this;
          this.mPurposes = param1Int;
          return;
        } 
        throw new IllegalArgumentException("keystoreAlias must not be empty");
      } 
      throw new NullPointerException("keystoreAlias == null");
    }
    
    public Builder() {
      this(KeyGenParameterSpec.this.getKeystoreAlias(), KeyGenParameterSpec.this.getPurposes());
      this.mUid = KeyGenParameterSpec.this.getUid();
      this.mKeySize = KeyGenParameterSpec.this.getKeySize();
      this.mSpec = KeyGenParameterSpec.this.getAlgorithmParameterSpec();
      this.mCertificateSubject = KeyGenParameterSpec.this.getCertificateSubject();
      this.mCertificateSerialNumber = KeyGenParameterSpec.this.getCertificateSerialNumber();
      this.mCertificateNotBefore = KeyGenParameterSpec.this.getCertificateNotBefore();
      this.mCertificateNotAfter = KeyGenParameterSpec.this.getCertificateNotAfter();
      this.mKeyValidityStart = KeyGenParameterSpec.this.getKeyValidityStart();
      this.mKeyValidityForOriginationEnd = KeyGenParameterSpec.this.getKeyValidityForOriginationEnd();
      this.mKeyValidityForConsumptionEnd = KeyGenParameterSpec.this.getKeyValidityForConsumptionEnd();
      this.mPurposes = KeyGenParameterSpec.this.getPurposes();
      if (KeyGenParameterSpec.this.isDigestsSpecified())
        this.mDigests = KeyGenParameterSpec.this.getDigests(); 
      this.mEncryptionPaddings = KeyGenParameterSpec.this.getEncryptionPaddings();
      this.mSignaturePaddings = KeyGenParameterSpec.this.getSignaturePaddings();
      this.mBlockModes = KeyGenParameterSpec.this.getBlockModes();
      this.mRandomizedEncryptionRequired = KeyGenParameterSpec.this.isRandomizedEncryptionRequired();
      this.mUserAuthenticationRequired = KeyGenParameterSpec.this.isUserAuthenticationRequired();
      this.mUserAuthenticationValidityDurationSeconds = KeyGenParameterSpec.this.getUserAuthenticationValidityDurationSeconds();
      this.mUserAuthenticationType = KeyGenParameterSpec.this.getUserAuthenticationType();
      this.mUserPresenceRequired = KeyGenParameterSpec.this.isUserPresenceRequired();
      this.mAttestationChallenge = KeyGenParameterSpec.this.getAttestationChallenge();
      this.mUniqueIdIncluded = KeyGenParameterSpec.this.isUniqueIdIncluded();
      this.mUserAuthenticationValidWhileOnBody = KeyGenParameterSpec.this.isUserAuthenticationValidWhileOnBody();
      this.mInvalidatedByBiometricEnrollment = KeyGenParameterSpec.this.isInvalidatedByBiometricEnrollment();
      this.mIsStrongBoxBacked = KeyGenParameterSpec.this.isStrongBoxBacked();
      this.mUserConfirmationRequired = KeyGenParameterSpec.this.isUserConfirmationRequired();
      this.mUnlockedDeviceRequired = KeyGenParameterSpec.this.isUnlockedDeviceRequired();
      this.mCriticalToDeviceEncryption = KeyGenParameterSpec.this.isCriticalToDeviceEncryption();
    }
    
    @SystemApi
    public Builder setUid(int param1Int) {
      this.mUid = param1Int;
      return this;
    }
    
    public Builder setKeySize(int param1Int) {
      if (param1Int >= 0) {
        this.mKeySize = param1Int;
        return this;
      } 
      throw new IllegalArgumentException("keySize < 0");
    }
    
    public Builder setAlgorithmParameterSpec(AlgorithmParameterSpec param1AlgorithmParameterSpec) {
      if (param1AlgorithmParameterSpec != null) {
        this.mSpec = param1AlgorithmParameterSpec;
        return this;
      } 
      throw new NullPointerException("spec == null");
    }
    
    public Builder setCertificateSubject(X500Principal param1X500Principal) {
      if (param1X500Principal != null) {
        this.mCertificateSubject = param1X500Principal;
        return this;
      } 
      throw new NullPointerException("subject == null");
    }
    
    public Builder setCertificateSerialNumber(BigInteger param1BigInteger) {
      if (param1BigInteger != null) {
        this.mCertificateSerialNumber = param1BigInteger;
        return this;
      } 
      throw new NullPointerException("serialNumber == null");
    }
    
    public Builder setCertificateNotBefore(Date param1Date) {
      if (param1Date != null) {
        this.mCertificateNotBefore = Utils.cloneIfNotNull(param1Date);
        return this;
      } 
      throw new NullPointerException("date == null");
    }
    
    public Builder setCertificateNotAfter(Date param1Date) {
      if (param1Date != null) {
        this.mCertificateNotAfter = Utils.cloneIfNotNull(param1Date);
        return this;
      } 
      throw new NullPointerException("date == null");
    }
    
    public Builder setKeyValidityStart(Date param1Date) {
      this.mKeyValidityStart = Utils.cloneIfNotNull(param1Date);
      return this;
    }
    
    public Builder setKeyValidityEnd(Date param1Date) {
      setKeyValidityForOriginationEnd(param1Date);
      setKeyValidityForConsumptionEnd(param1Date);
      return this;
    }
    
    public Builder setKeyValidityForOriginationEnd(Date param1Date) {
      this.mKeyValidityForOriginationEnd = Utils.cloneIfNotNull(param1Date);
      return this;
    }
    
    public Builder setKeyValidityForConsumptionEnd(Date param1Date) {
      this.mKeyValidityForConsumptionEnd = Utils.cloneIfNotNull(param1Date);
      return this;
    }
    
    public Builder setDigests(String... param1VarArgs) {
      this.mDigests = ArrayUtils.cloneIfNotEmpty(param1VarArgs);
      return this;
    }
    
    public Builder setEncryptionPaddings(String... param1VarArgs) {
      this.mEncryptionPaddings = ArrayUtils.cloneIfNotEmpty(param1VarArgs);
      return this;
    }
    
    public Builder setSignaturePaddings(String... param1VarArgs) {
      this.mSignaturePaddings = ArrayUtils.cloneIfNotEmpty(param1VarArgs);
      return this;
    }
    
    public Builder setBlockModes(String... param1VarArgs) {
      this.mBlockModes = ArrayUtils.cloneIfNotEmpty(param1VarArgs);
      return this;
    }
    
    public Builder setRandomizedEncryptionRequired(boolean param1Boolean) {
      this.mRandomizedEncryptionRequired = param1Boolean;
      return this;
    }
    
    public Builder setUserAuthenticationRequired(boolean param1Boolean) {
      this.mUserAuthenticationRequired = param1Boolean;
      return this;
    }
    
    public Builder setUserConfirmationRequired(boolean param1Boolean) {
      this.mUserConfirmationRequired = param1Boolean;
      return this;
    }
    
    @Deprecated
    public Builder setUserAuthenticationValidityDurationSeconds(int param1Int) {
      if (param1Int >= -1) {
        if (param1Int == -1)
          return setUserAuthenticationParameters(0, 2); 
        return setUserAuthenticationParameters(param1Int, 3);
      } 
      throw new IllegalArgumentException("seconds must be -1 or larger");
    }
    
    public Builder setUserAuthenticationParameters(int param1Int1, int param1Int2) {
      if (param1Int1 >= 0) {
        this.mUserAuthenticationValidityDurationSeconds = param1Int1;
        this.mUserAuthenticationType = param1Int2;
        return this;
      } 
      throw new IllegalArgumentException("timeout must be 0 or larger");
    }
    
    public Builder setUserPresenceRequired(boolean param1Boolean) {
      this.mUserPresenceRequired = param1Boolean;
      return this;
    }
    
    public Builder setAttestationChallenge(byte[] param1ArrayOfbyte) {
      this.mAttestationChallenge = param1ArrayOfbyte;
      return this;
    }
    
    public Builder setUniqueIdIncluded(boolean param1Boolean) {
      this.mUniqueIdIncluded = param1Boolean;
      return this;
    }
    
    public Builder setUserAuthenticationValidWhileOnBody(boolean param1Boolean) {
      this.mUserAuthenticationValidWhileOnBody = param1Boolean;
      return this;
    }
    
    public Builder setInvalidatedByBiometricEnrollment(boolean param1Boolean) {
      this.mInvalidatedByBiometricEnrollment = param1Boolean;
      return this;
    }
    
    public Builder setIsStrongBoxBacked(boolean param1Boolean) {
      this.mIsStrongBoxBacked = param1Boolean;
      return this;
    }
    
    public Builder setUnlockedDeviceRequired(boolean param1Boolean) {
      this.mUnlockedDeviceRequired = param1Boolean;
      return this;
    }
    
    public Builder setCriticalToDeviceEncryption(boolean param1Boolean) {
      this.mCriticalToDeviceEncryption = param1Boolean;
      return this;
    }
    
    public KeyGenParameterSpec build() {
      return new KeyGenParameterSpec(this.mKeystoreAlias, this.mUid, this.mKeySize, this.mSpec, this.mCertificateSubject, this.mCertificateSerialNumber, this.mCertificateNotBefore, this.mCertificateNotAfter, this.mKeyValidityStart, this.mKeyValidityForOriginationEnd, this.mKeyValidityForConsumptionEnd, this.mPurposes, this.mDigests, this.mEncryptionPaddings, this.mSignaturePaddings, this.mBlockModes, this.mRandomizedEncryptionRequired, this.mUserAuthenticationRequired, this.mUserAuthenticationValidityDurationSeconds, this.mUserAuthenticationType, this.mUserPresenceRequired, this.mAttestationChallenge, this.mUniqueIdIncluded, this.mUserAuthenticationValidWhileOnBody, this.mInvalidatedByBiometricEnrollment, this.mIsStrongBoxBacked, this.mUserConfirmationRequired, this.mUnlockedDeviceRequired, this.mCriticalToDeviceEncryption);
    }
  }
}
