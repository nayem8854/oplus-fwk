package android.security.keystore;

import android.security.Credentials;
import android.security.KeyStore;
import android.security.keymaster.KeyCharacteristics;
import android.security.keymaster.KeymasterArguments;
import java.security.InvalidAlgorithmParameterException;
import java.security.ProviderException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;
import java.util.Date;
import javax.crypto.KeyGeneratorSpi;
import javax.crypto.SecretKey;
import libcore.util.EmptyArray;

public abstract class AndroidKeyStoreKeyGeneratorSpi extends KeyGeneratorSpi {
  private final int mDefaultKeySizeBits;
  
  protected int mKeySizeBits;
  
  class AES extends AndroidKeyStoreKeyGeneratorSpi {
    public AES() {
      super(32, 128);
    }
    
    protected void engineInit(AlgorithmParameterSpec param1AlgorithmParameterSpec, SecureRandom param1SecureRandom) throws InvalidAlgorithmParameterException {
      super.engineInit(param1AlgorithmParameterSpec, param1SecureRandom);
      if (this.mKeySizeBits == 128 || this.mKeySizeBits == 192 || this.mKeySizeBits == 256)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported key size: ");
      stringBuilder.append(this.mKeySizeBits);
      stringBuilder.append(". Supported: 128, 192, 256.");
      throw new InvalidAlgorithmParameterException(stringBuilder.toString());
    }
  }
  
  class DESede extends AndroidKeyStoreKeyGeneratorSpi {
    public DESede() {
      super(33, 168);
    }
  }
  
  class HmacBase extends AndroidKeyStoreKeyGeneratorSpi {
    protected HmacBase(AndroidKeyStoreKeyGeneratorSpi this$0) {
      super(128, this$0, i);
    }
  }
  
  class HmacSHA1 extends HmacBase {
    public HmacSHA1() {
      super(2);
    }
  }
  
  class HmacSHA224 extends HmacBase {
    public HmacSHA224() {
      super(3);
    }
  }
  
  class HmacSHA256 extends HmacBase {
    public HmacSHA256() {
      super(4);
    }
  }
  
  class HmacSHA384 extends HmacBase {
    public HmacSHA384() {
      super(5);
    }
  }
  
  class HmacSHA512 extends HmacBase {
    public HmacSHA512() {
      super(6);
    }
  }
  
  private final KeyStore mKeyStore = KeyStore.getInstance();
  
  private final int mKeymasterAlgorithm;
  
  private int[] mKeymasterBlockModes;
  
  private final int mKeymasterDigest;
  
  private int[] mKeymasterDigests;
  
  private int[] mKeymasterPaddings;
  
  private int[] mKeymasterPurposes;
  
  private SecureRandom mRng;
  
  private KeyGenParameterSpec mSpec;
  
  protected AndroidKeyStoreKeyGeneratorSpi(int paramInt1, int paramInt2) {
    this(paramInt1, -1, paramInt2);
  }
  
  protected AndroidKeyStoreKeyGeneratorSpi(int paramInt1, int paramInt2, int paramInt3) {
    this.mKeymasterAlgorithm = paramInt1;
    this.mKeymasterDigest = paramInt2;
    this.mDefaultKeySizeBits = paramInt3;
    if (paramInt3 > 0) {
      if (paramInt1 != 128 || paramInt2 != -1)
        return; 
      throw new IllegalArgumentException("Digest algorithm must be specified for HMAC key");
    } 
    throw new IllegalArgumentException("Default key size must be positive");
  }
  
  protected void engineInit(SecureRandom paramSecureRandom) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Cannot initialize without a ");
    stringBuilder.append(KeyGenParameterSpec.class.getName());
    stringBuilder.append(" parameter");
    throw new UnsupportedOperationException(stringBuilder.toString());
  }
  
  protected void engineInit(int paramInt, SecureRandom paramSecureRandom) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Cannot initialize without a ");
    stringBuilder.append(KeyGenParameterSpec.class.getName());
    stringBuilder.append(" parameter");
    throw new UnsupportedOperationException(stringBuilder.toString());
  }
  
  protected void engineInit(AlgorithmParameterSpec paramAlgorithmParameterSpec, SecureRandom paramSecureRandom) throws InvalidAlgorithmParameterException {
    resetAll();
    if (paramAlgorithmParameterSpec != null)
      try {
        if (paramAlgorithmParameterSpec instanceof KeyGenParameterSpec) {
          paramAlgorithmParameterSpec = paramAlgorithmParameterSpec;
          if (paramAlgorithmParameterSpec.getKeystoreAlias() != null) {
            int i;
            this.mRng = paramSecureRandom;
            this.mSpec = (KeyGenParameterSpec)paramAlgorithmParameterSpec;
            if (paramAlgorithmParameterSpec.getKeySize() != -1) {
              i = paramAlgorithmParameterSpec.getKeySize();
            } else {
              i = this.mDefaultKeySizeBits;
            } 
            this.mKeySizeBits = i;
            if (i > 0) {
              if (i % 8 == 0)
                try {
                  this.mKeymasterPurposes = KeyProperties.Purpose.allToKeymaster(paramAlgorithmParameterSpec.getPurposes());
                  String[] arrayOfString = paramAlgorithmParameterSpec.getEncryptionPaddings();
                  this.mKeymasterPaddings = KeyProperties.EncryptionPadding.allToKeymaster(arrayOfString);
                  if ((paramAlgorithmParameterSpec.getSignaturePaddings()).length <= 0) {
                    StringBuilder stringBuilder4;
                    this.mKeymasterBlockModes = KeyProperties.BlockMode.allToKeymaster(paramAlgorithmParameterSpec.getBlockModes());
                    if ((paramAlgorithmParameterSpec.getPurposes() & 0x1) != 0 && 
                      paramAlgorithmParameterSpec.isRandomizedEncryptionRequired())
                      for (int arrayOfInt[] = this.mKeymasterBlockModes, j = arrayOfInt.length; i < j; ) {
                        int k = arrayOfInt[i];
                        if (KeymasterUtils.isKeymasterBlockModeIndCpaCompatibleWithSymmetricCrypto(k)) {
                          i++;
                          continue;
                        } 
                        InvalidAlgorithmParameterException invalidAlgorithmParameterException7 = new InvalidAlgorithmParameterException();
                        stringBuilder4 = new StringBuilder();
                        this();
                        stringBuilder4.append("Randomized encryption (IND-CPA) required but may be violated by block mode: ");
                        stringBuilder4.append(KeyProperties.BlockMode.fromKeymaster(k));
                        stringBuilder4.append(". See ");
                        stringBuilder4.append(KeyGenParameterSpec.class.getName());
                        stringBuilder4.append(" documentation.");
                        this(stringBuilder4.toString());
                        throw invalidAlgorithmParameterException7;
                      }  
                    if (this.mKeymasterAlgorithm != 33 || 
                      this.mKeySizeBits == 168) {
                      InvalidAlgorithmParameterException invalidAlgorithmParameterException7;
                      if (this.mKeymasterAlgorithm == 128) {
                        if (this.mKeySizeBits >= 64 && this.mKeySizeBits <= 512) {
                          this.mKeymasterDigests = new int[] { this.mKeymasterDigest };
                          if (stringBuilder4.isDigestsSpecified()) {
                            int[] arrayOfInt = KeyProperties.Digest.allToKeymaster(stringBuilder4.getDigests());
                            if (arrayOfInt.length != 1 || arrayOfInt[0] != this.mKeymasterDigest) {
                              InvalidAlgorithmParameterException invalidAlgorithmParameterException8 = new InvalidAlgorithmParameterException();
                              StringBuilder stringBuilder5 = new StringBuilder();
                              this();
                              stringBuilder5.append("Unsupported digests specification: ");
                              stringBuilder5.append(Arrays.asList(stringBuilder4.getDigests()));
                              stringBuilder5.append(". Only ");
                              i = this.mKeymasterDigest;
                              stringBuilder5.append(KeyProperties.Digest.fromKeymaster(i));
                              stringBuilder5.append(" supported for this HMAC key algorithm");
                              this(stringBuilder5.toString());
                              throw invalidAlgorithmParameterException8;
                            } 
                          } 
                        } else {
                          invalidAlgorithmParameterException7 = new InvalidAlgorithmParameterException();
                          this("HMAC key sizes must be within 64-512 bits, inclusive.");
                          throw invalidAlgorithmParameterException7;
                        } 
                      } else if (invalidAlgorithmParameterException7.isDigestsSpecified()) {
                        this.mKeymasterDigests = KeyProperties.Digest.allToKeymaster(invalidAlgorithmParameterException7.getDigests());
                      } else {
                        this.mKeymasterDigests = EmptyArray.INT;
                      } 
                      KeymasterArguments keymasterArguments = new KeymasterArguments();
                      this();
                      KeymasterUtils.addUserAuthArgs(keymasterArguments, (UserAuthArgs)invalidAlgorithmParameterException7);
                      return;
                    } 
                    InvalidAlgorithmParameterException invalidAlgorithmParameterException6 = new InvalidAlgorithmParameterException();
                    this("3DES key size must be 168 bits.");
                    throw invalidAlgorithmParameterException6;
                  } 
                  InvalidAlgorithmParameterException invalidAlgorithmParameterException5 = new InvalidAlgorithmParameterException();
                  this("Signature paddings not supported for symmetric key algorithms");
                  throw invalidAlgorithmParameterException5;
                } catch (IllegalStateException|IllegalArgumentException illegalStateException) {
                  InvalidAlgorithmParameterException invalidAlgorithmParameterException5 = new InvalidAlgorithmParameterException();
                  this(illegalStateException);
                  throw invalidAlgorithmParameterException5;
                }  
              InvalidAlgorithmParameterException invalidAlgorithmParameterException4 = new InvalidAlgorithmParameterException();
              StringBuilder stringBuilder3 = new StringBuilder();
              this();
              stringBuilder3.append("Key size must be a multiple of 8: ");
              stringBuilder3.append(this.mKeySizeBits);
              this(stringBuilder3.toString());
              throw invalidAlgorithmParameterException4;
            } 
            InvalidAlgorithmParameterException invalidAlgorithmParameterException3 = new InvalidAlgorithmParameterException();
            StringBuilder stringBuilder2 = new StringBuilder();
            this();
            stringBuilder2.append("Key size must be positive: ");
            stringBuilder2.append(this.mKeySizeBits);
            this(stringBuilder2.toString());
            throw invalidAlgorithmParameterException3;
          } 
          InvalidAlgorithmParameterException invalidAlgorithmParameterException2 = new InvalidAlgorithmParameterException();
          this("KeyStore entry alias not provided");
          throw invalidAlgorithmParameterException2;
        } 
        InvalidAlgorithmParameterException invalidAlgorithmParameterException1 = new InvalidAlgorithmParameterException();
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Cannot initialize without a ");
        stringBuilder1.append(KeyGenParameterSpec.class.getName());
        stringBuilder1.append(" parameter");
        this(stringBuilder1.toString());
        throw invalidAlgorithmParameterException1;
      } finally {
        if (!false)
          resetAll(); 
      }  
    InvalidAlgorithmParameterException invalidAlgorithmParameterException = new InvalidAlgorithmParameterException();
    StringBuilder stringBuilder = new StringBuilder();
    this();
    stringBuilder.append("Cannot initialize without a ");
    stringBuilder.append(KeyGenParameterSpec.class.getName());
    stringBuilder.append(" parameter");
    this(stringBuilder.toString());
    throw invalidAlgorithmParameterException;
  }
  
  private void resetAll() {
    this.mSpec = null;
    this.mRng = null;
    this.mKeySizeBits = -1;
    this.mKeymasterPurposes = null;
    this.mKeymasterPaddings = null;
    this.mKeymasterBlockModes = null;
  }
  
  protected SecretKey engineGenerateKey() {
    KeyGenParameterSpec keyGenParameterSpec = this.mSpec;
    if (keyGenParameterSpec != null) {
      KeymasterArguments keymasterArguments = new KeymasterArguments();
      keymasterArguments.addUnsignedInt(805306371, this.mKeySizeBits);
      keymasterArguments.addEnum(268435458, this.mKeymasterAlgorithm);
      keymasterArguments.addEnums(536870913, this.mKeymasterPurposes);
      keymasterArguments.addEnums(536870916, this.mKeymasterBlockModes);
      keymasterArguments.addEnums(536870918, this.mKeymasterPaddings);
      keymasterArguments.addEnums(536870917, this.mKeymasterDigests);
      KeymasterUtils.addUserAuthArgs(keymasterArguments, keyGenParameterSpec);
      KeymasterUtils.addMinMacLengthAuthorizationIfNecessary(keymasterArguments, this.mKeymasterAlgorithm, this.mKeymasterBlockModes, this.mKeymasterDigests);
      keymasterArguments.addDateIfNotNull(1610613136, keyGenParameterSpec.getKeyValidityStart());
      Date date = keyGenParameterSpec.getKeyValidityForOriginationEnd();
      keymasterArguments.addDateIfNotNull(1610613137, date);
      date = keyGenParameterSpec.getKeyValidityForConsumptionEnd();
      keymasterArguments.addDateIfNotNull(1610613138, date);
      if ((keyGenParameterSpec.getPurposes() & 0x1) != 0 && 
        !keyGenParameterSpec.isRandomizedEncryptionRequired())
        keymasterArguments.addBoolean(1879048199); 
      SecureRandom secureRandom = this.mRng;
      int i = (this.mKeySizeBits + 7) / 8;
      null = KeyStoreCryptoOperationUtils.getRandomBytesToMixIntoKeystoreRng(secureRandom, i);
      i = 0;
      if (keyGenParameterSpec.isStrongBoxBacked())
        i = 0x0 | 0x10; 
      if (keyGenParameterSpec.isCriticalToDeviceEncryption())
        i |= 0x8; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("USRPKEY_");
      stringBuilder.append(keyGenParameterSpec.getKeystoreAlias());
      String str = stringBuilder.toString();
      KeyCharacteristics keyCharacteristics = new KeyCharacteristics();
      try {
        Credentials.deleteAllTypesForAlias(this.mKeyStore, keyGenParameterSpec.getKeystoreAlias(), keyGenParameterSpec.getUid());
        KeyStore keyStore = this.mKeyStore;
        int j = keyGenParameterSpec.getUid();
        i = keyStore.generateKey(str, keymasterArguments, null, j, i, keyCharacteristics);
        if (i != 1) {
          if (i == -68) {
            StrongBoxUnavailableException strongBoxUnavailableException = new StrongBoxUnavailableException();
            this("Failed to generate key");
            throw strongBoxUnavailableException;
          } 
          ProviderException providerException = new ProviderException();
          this("Keystore operation failed", KeyStore.getKeyStoreException(i));
          throw providerException;
        } 
      } finally {
        if (!false) {
          KeyStore keyStore = this.mKeyStore;
          String str1 = keyGenParameterSpec.getKeystoreAlias();
          i = keyGenParameterSpec.getUid();
          Credentials.deleteAllTypesForAlias(keyStore, str1, i);
        } 
      } 
    } 
    throw new IllegalStateException("Not initialized");
  }
}
