package android.security.keystore;

import java.security.KeyStore;
import java.security.spec.AlgorithmParameterSpec;

public class WrappedKeyEntry implements KeyStore.Entry {
  private final AlgorithmParameterSpec mAlgorithmParameterSpec;
  
  private final String mTransformation;
  
  private final byte[] mWrappedKeyBytes;
  
  private final String mWrappingKeyAlias;
  
  public WrappedKeyEntry(byte[] paramArrayOfbyte, String paramString1, String paramString2, AlgorithmParameterSpec paramAlgorithmParameterSpec) {
    this.mWrappedKeyBytes = paramArrayOfbyte;
    this.mWrappingKeyAlias = paramString1;
    this.mTransformation = paramString2;
    this.mAlgorithmParameterSpec = paramAlgorithmParameterSpec;
  }
  
  public byte[] getWrappedKeyBytes() {
    return this.mWrappedKeyBytes;
  }
  
  public String getWrappingKeyAlias() {
    return this.mWrappingKeyAlias;
  }
  
  public String getTransformation() {
    return this.mTransformation;
  }
  
  public AlgorithmParameterSpec getAlgorithmParameterSpec() {
    return this.mAlgorithmParameterSpec;
  }
}
