package android.security.keystore;

import android.os.IBinder;
import android.security.KeyStore;
import android.security.KeyStoreException;
import android.security.keymaster.KeymasterArguments;
import android.security.keymaster.OperationResult;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.ProviderException;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.MacSpi;

public abstract class AndroidKeyStoreHmacSpi extends MacSpi implements KeyStoreCryptoOperation {
  private KeyStoreCryptoOperationChunkedStreamer mChunkedStreamer;
  
  private AndroidKeyStoreSecretKey mKey;
  
  class HmacSHA1 extends AndroidKeyStoreHmacSpi {
    public HmacSHA1() {
      super(2);
    }
  }
  
  class HmacSHA224 extends AndroidKeyStoreHmacSpi {
    public HmacSHA224() {
      super(3);
    }
  }
  
  class HmacSHA256 extends AndroidKeyStoreHmacSpi {
    public HmacSHA256() {
      super(4);
    }
  }
  
  class HmacSHA384 extends AndroidKeyStoreHmacSpi {
    public HmacSHA384() {
      super(5);
    }
  }
  
  class HmacSHA512 extends AndroidKeyStoreHmacSpi {
    public HmacSHA512() {
      super(6);
    }
  }
  
  private final KeyStore mKeyStore = KeyStore.getInstance();
  
  private final int mKeymasterDigest;
  
  private final int mMacSizeBits;
  
  private long mOperationHandle;
  
  private IBinder mOperationToken;
  
  protected AndroidKeyStoreHmacSpi(int paramInt) {
    this.mKeymasterDigest = paramInt;
    this.mMacSizeBits = KeymasterUtils.getDigestOutputSizeBits(paramInt);
  }
  
  protected int engineGetMacLength() {
    return (this.mMacSizeBits + 7) / 8;
  }
  
  protected void engineInit(Key paramKey, AlgorithmParameterSpec paramAlgorithmParameterSpec) throws InvalidKeyException, InvalidAlgorithmParameterException {
    resetAll();
    try {
      init(paramKey, paramAlgorithmParameterSpec);
      ensureKeystoreOperationInitialized();
      return;
    } finally {
      if (!false)
        resetAll(); 
    } 
  }
  
  private void init(Key paramKey, AlgorithmParameterSpec paramAlgorithmParameterSpec) throws InvalidKeyException, InvalidAlgorithmParameterException {
    if (paramKey != null) {
      StringBuilder stringBuilder1;
      if (paramKey instanceof AndroidKeyStoreSecretKey) {
        this.mKey = (AndroidKeyStoreSecretKey)paramKey;
        if (paramAlgorithmParameterSpec == null)
          return; 
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Unsupported algorithm parameters: ");
        stringBuilder1.append(paramAlgorithmParameterSpec);
        throw new InvalidAlgorithmParameterException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Only Android KeyStore secret keys supported. Key: ");
      stringBuilder2.append(stringBuilder1);
      throw new InvalidKeyException(stringBuilder2.toString());
    } 
    throw new InvalidKeyException("key == null");
  }
  
  private void resetAll() {
    this.mKey = null;
    IBinder iBinder = this.mOperationToken;
    if (iBinder != null)
      this.mKeyStore.abort(iBinder); 
    this.mOperationToken = null;
    this.mOperationHandle = 0L;
    this.mChunkedStreamer = null;
  }
  
  private void resetWhilePreservingInitState() {
    IBinder iBinder = this.mOperationToken;
    if (iBinder != null)
      this.mKeyStore.abort(iBinder); 
    this.mOperationToken = null;
    this.mOperationHandle = 0L;
    this.mChunkedStreamer = null;
  }
  
  protected void engineReset() {
    resetWhilePreservingInitState();
  }
  
  private void ensureKeystoreOperationInitialized() throws InvalidKeyException {
    if (this.mChunkedStreamer != null)
      return; 
    if (this.mKey != null) {
      KeymasterArguments keymasterArguments = new KeymasterArguments();
      keymasterArguments.addEnum(268435458, 128);
      keymasterArguments.addEnum(536870917, this.mKeymasterDigest);
      keymasterArguments.addUnsignedInt(805307371, this.mMacSizeBits);
      KeyStore keyStore = this.mKeyStore;
      AndroidKeyStoreSecretKey androidKeyStoreSecretKey = this.mKey;
      String str = androidKeyStoreSecretKey.getAlias();
      androidKeyStoreSecretKey = this.mKey;
      int i = androidKeyStoreSecretKey.getUid();
      OperationResult operationResult = keyStore.begin(str, 2, true, keymasterArguments, null, i);
      if (operationResult != null) {
        IBinder iBinder;
        this.mOperationToken = operationResult.token;
        this.mOperationHandle = operationResult.operationHandle;
        InvalidKeyException invalidKeyException = KeyStoreCryptoOperationUtils.getInvalidKeyExceptionForInit(this.mKeyStore, this.mKey, operationResult.resultCode);
        if (invalidKeyException == null) {
          iBinder = this.mOperationToken;
          if (iBinder != null) {
            if (this.mOperationHandle != 0L) {
              this.mChunkedStreamer = new KeyStoreCryptoOperationChunkedStreamer(new KeyStoreCryptoOperationChunkedStreamer.MainDataStream(this.mKeyStore, iBinder));
              return;
            } 
            throw new ProviderException("Keystore returned invalid operation handle");
          } 
          throw new ProviderException("Keystore returned null operation token");
        } 
        throw iBinder;
      } 
      throw new KeyStoreConnectException();
    } 
    throw new IllegalStateException("Not initialized");
  }
  
  protected void engineUpdate(byte paramByte) {
    engineUpdate(new byte[] { paramByte }, 0, 1);
  }
  
  protected void engineUpdate(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    try {
      ensureKeystoreOperationInitialized();
      try {
        paramArrayOfbyte = this.mChunkedStreamer.update(paramArrayOfbyte, paramInt1, paramInt2);
        if (paramArrayOfbyte == null || paramArrayOfbyte.length == 0)
          return; 
        throw new ProviderException("Update operation unexpectedly produced output");
      } catch (KeyStoreException keyStoreException) {
        throw new ProviderException("Keystore operation failed", keyStoreException);
      } 
    } catch (InvalidKeyException invalidKeyException) {
      throw new ProviderException("Failed to reinitialize MAC", invalidKeyException);
    } 
  }
  
  protected byte[] engineDoFinal() {
    try {
      ensureKeystoreOperationInitialized();
      try {
        byte[] arrayOfByte = this.mChunkedStreamer.doFinal(null, 0, 0, null, null);
        resetWhilePreservingInitState();
        return arrayOfByte;
      } catch (KeyStoreException keyStoreException) {
        throw new ProviderException("Keystore operation failed", keyStoreException);
      } 
    } catch (InvalidKeyException invalidKeyException) {
      throw new ProviderException("Failed to reinitialize MAC", invalidKeyException);
    } 
  }
  
  public void finalize() throws Throwable {
    try {
      IBinder iBinder = this.mOperationToken;
      if (iBinder != null)
        this.mKeyStore.abort(iBinder); 
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public long getOperationHandle() {
    return this.mOperationHandle;
  }
}
