package android.security.keystore;

import android.security.KeyStoreException;

interface KeyStoreCryptoOperationStreamer {
  byte[] doFinal(byte[] paramArrayOfbyte1, int paramInt1, int paramInt2, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3) throws KeyStoreException;
  
  long getConsumedInputSizeBytes();
  
  long getProducedOutputSizeBytes();
  
  byte[] update(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws KeyStoreException;
}
