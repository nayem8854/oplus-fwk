package android.security.keystore;

import android.os.IBinder;
import android.security.KeyStore;
import android.security.KeyStoreException;
import android.security.keymaster.OperationResult;
import libcore.util.EmptyArray;

class KeyStoreCryptoOperationChunkedStreamer implements KeyStoreCryptoOperationStreamer {
  private static final int DEFAULT_CHUNK_SIZE_MAX = 65536;
  
  private static final int DEFAULT_CHUNK_SIZE_THRESHOLD = 2048;
  
  private final byte[] mChunk;
  
  private int mChunkLength = 0;
  
  private final int mChunkSizeMax;
  
  private final int mChunkSizeThreshold;
  
  private long mConsumedInputSizeBytes;
  
  private final Stream mKeyStoreStream;
  
  private long mProducedOutputSizeBytes;
  
  KeyStoreCryptoOperationChunkedStreamer(Stream paramStream) {
    this(paramStream, 2048, 65536);
  }
  
  KeyStoreCryptoOperationChunkedStreamer(Stream paramStream, int paramInt) {
    this(paramStream, paramInt, 65536);
  }
  
  KeyStoreCryptoOperationChunkedStreamer(Stream paramStream, int paramInt1, int paramInt2) {
    this.mKeyStoreStream = paramStream;
    this.mChunkSizeMax = paramInt2;
    if (paramInt1 <= 0) {
      this.mChunkSizeThreshold = 1;
    } else if (paramInt1 > paramInt2) {
      this.mChunkSizeThreshold = paramInt2;
    } else {
      this.mChunkSizeThreshold = paramInt1;
    } 
    this.mChunk = new byte[this.mChunkSizeMax];
  }
  
  public byte[] update(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws KeyStoreException {
    if (paramInt2 == 0 || paramArrayOfbyte == null)
      return EmptyArray.BYTE; 
    if (paramInt2 >= 0 && paramInt1 >= 0 && paramInt1 + paramInt2 <= paramArrayOfbyte.length) {
      byte[] arrayOfByte = EmptyArray.BYTE;
      while (true) {
        if (paramInt2 > 0 || this.mChunkLength >= this.mChunkSizeThreshold) {
          int i = ArrayUtils.copy(paramArrayOfbyte, paramInt1, this.mChunk, this.mChunkLength, paramInt2);
          paramInt2 -= i;
          paramInt1 += i;
          int j = this.mChunkLength + i;
          this.mConsumedInputSizeBytes += i;
          if (j <= this.mChunkSizeMax) {
            byte[] arrayOfByte1 = arrayOfByte;
            if (j >= this.mChunkSizeThreshold) {
              Stream stream = this.mKeyStoreStream;
              byte[] arrayOfByte2 = this.mChunk;
              arrayOfByte2 = ArrayUtils.subarray(arrayOfByte2, 0, j);
              OperationResult operationResult = stream.update(arrayOfByte2);
              if (operationResult != null) {
                if (operationResult.resultCode == 1) {
                  if (operationResult.inputConsumed > 0) {
                    i = operationResult.inputConsumed;
                    j = this.mChunkLength;
                    if (i <= j) {
                      this.mChunkLength = i = j - operationResult.inputConsumed;
                      if (i > 0)
                        ArrayUtils.copy(this.mChunk, operationResult.inputConsumed, this.mChunk, 0, this.mChunkLength); 
                      arrayOfByte1 = arrayOfByte;
                      if (operationResult.output != null) {
                        arrayOfByte1 = arrayOfByte;
                        if (operationResult.output.length > 0) {
                          this.mProducedOutputSizeBytes += operationResult.output.length;
                          arrayOfByte1 = ArrayUtils.concat(arrayOfByte, operationResult.output);
                        } 
                      } 
                    } else {
                      StringBuilder stringBuilder1 = new StringBuilder();
                      stringBuilder1.append("Keystore consumed more input than provided. Provided: ");
                      stringBuilder1.append(this.mChunkLength);
                      stringBuilder1.append(", consumed: ");
                      stringBuilder1.append(operationResult.inputConsumed);
                      throw new KeyStoreException(-1000, stringBuilder1.toString());
                    } 
                  } else {
                    StringBuilder stringBuilder1 = new StringBuilder();
                    stringBuilder1.append("Keystore consumed 0 of ");
                    stringBuilder1.append(this.mChunkLength);
                    stringBuilder1.append(" bytes provided.");
                    throw new KeyStoreException(-21, stringBuilder1.toString());
                  } 
                } else {
                  throw KeyStore.getKeyStoreException(operationResult.resultCode);
                } 
              } else {
                throw new KeyStoreConnectException();
              } 
            } 
            arrayOfByte = arrayOfByte1;
            continue;
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Chunk size exceeded max chunk size. Max: ");
          stringBuilder.append(this.mChunkSizeMax);
          stringBuilder.append(" Actual: ");
          stringBuilder.append(this.mChunkLength);
          throw new KeyStoreException(-21, stringBuilder.toString());
        } 
        return arrayOfByte;
      } 
    } 
    throw new KeyStoreException(-1000, "Input offset and length out of bounds of input array");
  }
  
  public byte[] doFinal(byte[] paramArrayOfbyte1, int paramInt1, int paramInt2, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3) throws KeyStoreException {
    byte[] arrayOfByte = update(paramArrayOfbyte1, paramInt1, paramInt2);
    paramArrayOfbyte1 = ArrayUtils.subarray(this.mChunk, 0, this.mChunkLength);
    OperationResult operationResult = this.mKeyStoreStream.finish(paramArrayOfbyte1, paramArrayOfbyte2, paramArrayOfbyte3);
    if (operationResult != null) {
      if (operationResult.resultCode == 1) {
        this.mConsumedInputSizeBytes += paramArrayOfbyte1.length;
        paramArrayOfbyte1 = arrayOfByte;
        if (operationResult.output != null) {
          paramArrayOfbyte1 = arrayOfByte;
          if (operationResult.output.length > 0) {
            this.mProducedOutputSizeBytes += operationResult.output.length;
            paramArrayOfbyte1 = ArrayUtils.concat(arrayOfByte, operationResult.output);
          } 
        } 
        return paramArrayOfbyte1;
      } 
      throw KeyStore.getKeyStoreException(operationResult.resultCode);
    } 
    throw new KeyStoreConnectException();
  }
  
  public long getConsumedInputSizeBytes() {
    return this.mConsumedInputSizeBytes;
  }
  
  public long getProducedOutputSizeBytes() {
    return this.mProducedOutputSizeBytes;
  }
  
  public static class MainDataStream implements Stream {
    private final KeyStore mKeyStore;
    
    private final IBinder mOperationToken;
    
    public MainDataStream(KeyStore param1KeyStore, IBinder param1IBinder) {
      this.mKeyStore = param1KeyStore;
      this.mOperationToken = param1IBinder;
    }
    
    public OperationResult update(byte[] param1ArrayOfbyte) {
      return this.mKeyStore.update(this.mOperationToken, null, param1ArrayOfbyte);
    }
    
    public OperationResult finish(byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3) {
      return this.mKeyStore.finish(this.mOperationToken, null, param1ArrayOfbyte1, param1ArrayOfbyte2, param1ArrayOfbyte3);
    }
  }
  
  class Stream {
    public abstract OperationResult finish(byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3);
    
    public abstract OperationResult update(byte[] param1ArrayOfbyte);
  }
}
