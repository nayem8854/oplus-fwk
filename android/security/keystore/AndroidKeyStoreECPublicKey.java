package android.security.keystore;

import java.security.interfaces.ECPublicKey;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;

public class AndroidKeyStoreECPublicKey extends AndroidKeyStorePublicKey implements ECPublicKey {
  private final ECParameterSpec mParams;
  
  private final ECPoint mW;
  
  public AndroidKeyStoreECPublicKey(String paramString, int paramInt, byte[] paramArrayOfbyte, ECParameterSpec paramECParameterSpec, ECPoint paramECPoint) {
    super(paramString, paramInt, "EC", paramArrayOfbyte);
    this.mParams = paramECParameterSpec;
    this.mW = paramECPoint;
  }
  
  public AndroidKeyStoreECPublicKey(String paramString, int paramInt, ECPublicKey paramECPublicKey) {
    this(paramString, paramInt, paramECPublicKey.getEncoded(), paramECPublicKey.getParams(), paramECPublicKey.getW());
    if ("X.509".equalsIgnoreCase(paramECPublicKey.getFormat()))
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported key export format: ");
    stringBuilder.append(paramECPublicKey.getFormat());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public ECParameterSpec getParams() {
    return this.mParams;
  }
  
  public ECPoint getW() {
    return this.mW;
  }
}
