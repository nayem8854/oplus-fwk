package android.security.keystore;

import libcore.util.EmptyArray;

public abstract class ArrayUtils {
  public static String[] nullToEmpty(String[] paramArrayOfString) {
    if (paramArrayOfString == null)
      paramArrayOfString = EmptyArray.STRING; 
    return paramArrayOfString;
  }
  
  public static String[] cloneIfNotEmpty(String[] paramArrayOfString) {
    if (paramArrayOfString != null && paramArrayOfString.length > 0)
      paramArrayOfString = (String[])paramArrayOfString.clone(); 
    return paramArrayOfString;
  }
  
  public static byte[] cloneIfNotEmpty(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null && paramArrayOfbyte.length > 0)
      paramArrayOfbyte = (byte[])paramArrayOfbyte.clone(); 
    return paramArrayOfbyte;
  }
  
  public static byte[] concat(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) {
    boolean bool;
    int i = 0;
    if (paramArrayOfbyte1 != null) {
      bool = paramArrayOfbyte1.length;
    } else {
      bool = false;
    } 
    if (paramArrayOfbyte2 != null)
      i = paramArrayOfbyte2.length; 
    return concat(paramArrayOfbyte1, 0, bool, paramArrayOfbyte2, 0, i);
  }
  
  public static byte[] concat(byte[] paramArrayOfbyte1, int paramInt1, int paramInt2, byte[] paramArrayOfbyte2, int paramInt3, int paramInt4) {
    if (paramInt2 == 0)
      return subarray(paramArrayOfbyte2, paramInt3, paramInt4); 
    if (paramInt4 == 0)
      return subarray(paramArrayOfbyte1, paramInt1, paramInt2); 
    byte[] arrayOfByte = new byte[paramInt2 + paramInt4];
    System.arraycopy(paramArrayOfbyte1, paramInt1, arrayOfByte, 0, paramInt2);
    System.arraycopy(paramArrayOfbyte2, paramInt3, arrayOfByte, paramInt2, paramInt4);
    return arrayOfByte;
  }
  
  public static int copy(byte[] paramArrayOfbyte1, int paramInt1, byte[] paramArrayOfbyte2, int paramInt2, int paramInt3) {
    if (paramArrayOfbyte2 == null || paramArrayOfbyte1 == null)
      return 0; 
    int i = paramInt3;
    if (paramInt3 > paramArrayOfbyte2.length - paramInt2)
      i = paramArrayOfbyte2.length - paramInt2; 
    paramInt3 = i;
    if (i > paramArrayOfbyte1.length - paramInt1)
      paramInt3 = paramArrayOfbyte1.length - paramInt1; 
    if (paramInt3 <= 0)
      return 0; 
    System.arraycopy(paramArrayOfbyte1, paramInt1, paramArrayOfbyte2, paramInt2, paramInt3);
    return paramInt3;
  }
  
  public static byte[] subarray(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    if (paramInt2 == 0)
      return EmptyArray.BYTE; 
    if (paramInt1 == 0 && paramInt2 == paramArrayOfbyte.length)
      return paramArrayOfbyte; 
    byte[] arrayOfByte = new byte[paramInt2];
    System.arraycopy(paramArrayOfbyte, paramInt1, arrayOfByte, 0, paramInt2);
    return arrayOfByte;
  }
  
  public static int[] concat(int[] paramArrayOfint1, int[] paramArrayOfint2) {
    if (paramArrayOfint1 == null || paramArrayOfint1.length == 0)
      return paramArrayOfint2; 
    if (paramArrayOfint2 == null || paramArrayOfint2.length == 0)
      return paramArrayOfint1; 
    int[] arrayOfInt = new int[paramArrayOfint1.length + paramArrayOfint2.length];
    System.arraycopy(paramArrayOfint1, 0, arrayOfInt, 0, paramArrayOfint1.length);
    System.arraycopy(paramArrayOfint2, 0, arrayOfInt, paramArrayOfint1.length, paramArrayOfint2.length);
    return arrayOfInt;
  }
}
