package android.security.keystore;

import android.security.Credentials;
import android.security.KeyPairGeneratorSpec;
import android.security.KeyStore;
import android.security.keymaster.KeyCharacteristics;
import android.security.keymaster.KeymasterArguments;
import android.security.keymaster.KeymasterCertificateChain;
import com.android.internal.util.ArrayUtils;
import com.android.org.bouncycastle.asn1.ASN1Encodable;
import com.android.org.bouncycastle.asn1.ASN1EncodableVector;
import com.android.org.bouncycastle.asn1.ASN1InputStream;
import com.android.org.bouncycastle.asn1.ASN1Integer;
import com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier;
import com.android.org.bouncycastle.asn1.DERInteger;
import com.android.org.bouncycastle.asn1.DERNull;
import com.android.org.bouncycastle.asn1.DERSequence;
import com.android.org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import com.android.org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import com.android.org.bouncycastle.asn1.x509.Certificate;
import com.android.org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import com.android.org.bouncycastle.asn1.x509.TBSCertificate;
import com.android.org.bouncycastle.asn1.x509.Time;
import com.android.org.bouncycastle.asn1.x509.V3TBSCertificateGenerator;
import com.android.org.bouncycastle.asn1.x509.X509Name;
import com.android.org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import com.android.org.bouncycastle.jce.X509Principal;
import com.android.org.bouncycastle.jce.provider.X509CertificateObject;
import com.android.org.bouncycastle.x509.X509V3CertificateGenerator;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGeneratorSpi;
import java.security.PrivateKey;
import java.security.ProviderException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public abstract class AndroidKeyStoreKeyPairGeneratorSpi extends KeyPairGeneratorSpi {
  private static final int EC_DEFAULT_KEY_SIZE = 256;
  
  private static final int RSA_DEFAULT_KEY_SIZE = 2048;
  
  private static final int RSA_MAX_KEY_SIZE = 8192;
  
  private static final int RSA_MIN_KEY_SIZE = 512;
  
  private static final List<String> SUPPORTED_EC_NIST_CURVE_NAMES;
  
  class RSA extends AndroidKeyStoreKeyPairGeneratorSpi {
    public RSA() {
      super(1);
    }
  }
  
  class EC extends AndroidKeyStoreKeyPairGeneratorSpi {
    public EC() {
      super(3);
    }
  }
  
  private static final Map<String, Integer> SUPPORTED_EC_NIST_CURVE_NAME_TO_SIZE = new HashMap<>();
  
  private static final List<Integer> SUPPORTED_EC_NIST_CURVE_SIZES;
  
  private boolean mEncryptionAtRestRequired;
  
  private String mEntryAlias;
  
  private int mEntryUid;
  
  private String mJcaKeyAlgorithm;
  
  private int mKeySizeBits;
  
  private KeyStore mKeyStore;
  
  static {
    SUPPORTED_EC_NIST_CURVE_NAMES = new ArrayList<>();
    SUPPORTED_EC_NIST_CURVE_SIZES = new ArrayList<>();
    Map<String, Integer> map3 = SUPPORTED_EC_NIST_CURVE_NAME_TO_SIZE;
    Integer integer4 = Integer.valueOf(224);
    map3.put("p-224", integer4);
    SUPPORTED_EC_NIST_CURVE_NAME_TO_SIZE.put("secp224r1", integer4);
    Map<String, Integer> map5 = SUPPORTED_EC_NIST_CURVE_NAME_TO_SIZE;
    Integer integer2 = Integer.valueOf(256);
    map5.put("p-256", integer2);
    SUPPORTED_EC_NIST_CURVE_NAME_TO_SIZE.put("secp256r1", integer2);
    SUPPORTED_EC_NIST_CURVE_NAME_TO_SIZE.put("prime256v1", integer2);
    Map<String, Integer> map2 = SUPPORTED_EC_NIST_CURVE_NAME_TO_SIZE;
    Integer integer3 = Integer.valueOf(384);
    map2.put("p-384", integer3);
    SUPPORTED_EC_NIST_CURVE_NAME_TO_SIZE.put("secp384r1", integer3);
    Map<String, Integer> map4 = SUPPORTED_EC_NIST_CURVE_NAME_TO_SIZE;
    Integer integer1 = Integer.valueOf(521);
    map4.put("p-521", integer1);
    SUPPORTED_EC_NIST_CURVE_NAME_TO_SIZE.put("secp521r1", integer1);
    SUPPORTED_EC_NIST_CURVE_NAMES.addAll(SUPPORTED_EC_NIST_CURVE_NAME_TO_SIZE.keySet());
    Collections.sort(SUPPORTED_EC_NIST_CURVE_NAMES);
    List<Integer> list = SUPPORTED_EC_NIST_CURVE_SIZES;
    Map<String, Integer> map1 = SUPPORTED_EC_NIST_CURVE_NAME_TO_SIZE;
    HashSet<? extends Integer> hashSet = new HashSet(map1.values());
    list.addAll(hashSet);
    Collections.sort(SUPPORTED_EC_NIST_CURVE_SIZES);
  }
  
  private int mKeymasterAlgorithm = -1;
  
  private int[] mKeymasterBlockModes;
  
  private int[] mKeymasterDigests;
  
  private int[] mKeymasterEncryptionPaddings;
  
  private int[] mKeymasterPurposes;
  
  private int[] mKeymasterSignaturePaddings;
  
  private final int mOriginalKeymasterAlgorithm;
  
  private BigInteger mRSAPublicExponent;
  
  private SecureRandom mRng;
  
  private KeyGenParameterSpec mSpec;
  
  protected AndroidKeyStoreKeyPairGeneratorSpi(int paramInt) {
    this.mOriginalKeymasterAlgorithm = paramInt;
  }
  
  public void initialize(int paramInt, SecureRandom paramSecureRandom) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(KeyGenParameterSpec.class.getName());
    stringBuilder.append(" or ");
    stringBuilder.append(KeyPairGeneratorSpec.class.getName());
    stringBuilder.append(" required to initialize this KeyPairGenerator");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void initialize(AlgorithmParameterSpec paramAlgorithmParameterSpec, SecureRandom paramSecureRandom) throws InvalidAlgorithmParameterException {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial resetAll : ()V
    //   4: aload_1
    //   5: ifnull -> 885
    //   8: iconst_0
    //   9: istore_3
    //   10: aload_0
    //   11: getfield mOriginalKeymasterAlgorithm : I
    //   14: istore #4
    //   16: aload_1
    //   17: instanceof android/security/keystore/KeyGenParameterSpec
    //   20: istore #5
    //   22: iconst_0
    //   23: istore #6
    //   25: iload #5
    //   27: ifeq -> 38
    //   30: aload_1
    //   31: checkcast android/security/keystore/KeyGenParameterSpec
    //   34: astore_1
    //   35: goto -> 419
    //   38: aload_1
    //   39: instanceof android/security/KeyPairGeneratorSpec
    //   42: ifeq -> 799
    //   45: aload_1
    //   46: checkcast android/security/KeyPairGeneratorSpec
    //   49: astore #7
    //   51: aload #7
    //   53: invokevirtual getKeyType : ()Ljava/lang/String;
    //   56: astore_1
    //   57: aload_1
    //   58: ifnull -> 85
    //   61: aload_1
    //   62: invokestatic toKeymasterAsymmetricKeyAlgorithm : (Ljava/lang/String;)I
    //   65: istore #4
    //   67: goto -> 85
    //   70: astore_1
    //   71: new java/security/InvalidAlgorithmParameterException
    //   74: astore_2
    //   75: aload_2
    //   76: ldc_w 'Invalid key type in parameters'
    //   79: aload_1
    //   80: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   83: aload_2
    //   84: athrow
    //   85: iload #4
    //   87: iconst_1
    //   88: if_icmpeq -> 199
    //   91: iload #4
    //   93: iconst_3
    //   94: if_icmpne -> 161
    //   97: new android/security/keystore/KeyGenParameterSpec$Builder
    //   100: astore_1
    //   101: aload_1
    //   102: aload #7
    //   104: invokevirtual getKeystoreAlias : ()Ljava/lang/String;
    //   107: bipush #12
    //   109: invokespecial <init> : (Ljava/lang/String;I)V
    //   112: aload_1
    //   113: bipush #6
    //   115: anewarray java/lang/String
    //   118: dup
    //   119: iconst_0
    //   120: ldc_w 'NONE'
    //   123: aastore
    //   124: dup
    //   125: iconst_1
    //   126: ldc_w 'SHA-1'
    //   129: aastore
    //   130: dup
    //   131: iconst_2
    //   132: ldc_w 'SHA-224'
    //   135: aastore
    //   136: dup
    //   137: iconst_3
    //   138: ldc_w 'SHA-256'
    //   141: aastore
    //   142: dup
    //   143: iconst_4
    //   144: ldc_w 'SHA-384'
    //   147: aastore
    //   148: dup
    //   149: iconst_5
    //   150: ldc_w 'SHA-512'
    //   153: aastore
    //   154: invokevirtual setDigests : ([Ljava/lang/String;)Landroid/security/keystore/KeyGenParameterSpec$Builder;
    //   157: pop
    //   158: goto -> 321
    //   161: new java/security/ProviderException
    //   164: astore_1
    //   165: new java/lang/StringBuilder
    //   168: astore_2
    //   169: aload_2
    //   170: invokespecial <init> : ()V
    //   173: aload_2
    //   174: ldc 'Unsupported algorithm: '
    //   176: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   179: pop
    //   180: aload_2
    //   181: aload_0
    //   182: getfield mKeymasterAlgorithm : I
    //   185: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   188: pop
    //   189: aload_1
    //   190: aload_2
    //   191: invokevirtual toString : ()Ljava/lang/String;
    //   194: invokespecial <init> : (Ljava/lang/String;)V
    //   197: aload_1
    //   198: athrow
    //   199: new android/security/keystore/KeyGenParameterSpec$Builder
    //   202: astore_1
    //   203: aload_1
    //   204: aload #7
    //   206: invokevirtual getKeystoreAlias : ()Ljava/lang/String;
    //   209: bipush #15
    //   211: invokespecial <init> : (Ljava/lang/String;I)V
    //   214: aload_1
    //   215: bipush #7
    //   217: anewarray java/lang/String
    //   220: dup
    //   221: iconst_0
    //   222: ldc_w 'NONE'
    //   225: aastore
    //   226: dup
    //   227: iconst_1
    //   228: ldc_w 'MD5'
    //   231: aastore
    //   232: dup
    //   233: iconst_2
    //   234: ldc_w 'SHA-1'
    //   237: aastore
    //   238: dup
    //   239: iconst_3
    //   240: ldc_w 'SHA-224'
    //   243: aastore
    //   244: dup
    //   245: iconst_4
    //   246: ldc_w 'SHA-256'
    //   249: aastore
    //   250: dup
    //   251: iconst_5
    //   252: ldc_w 'SHA-384'
    //   255: aastore
    //   256: dup
    //   257: bipush #6
    //   259: ldc_w 'SHA-512'
    //   262: aastore
    //   263: invokevirtual setDigests : ([Ljava/lang/String;)Landroid/security/keystore/KeyGenParameterSpec$Builder;
    //   266: pop
    //   267: aload_1
    //   268: iconst_3
    //   269: anewarray java/lang/String
    //   272: dup
    //   273: iconst_0
    //   274: ldc_w 'NoPadding'
    //   277: aastore
    //   278: dup
    //   279: iconst_1
    //   280: ldc_w 'PKCS1Padding'
    //   283: aastore
    //   284: dup
    //   285: iconst_2
    //   286: ldc_w 'OAEPPadding'
    //   289: aastore
    //   290: invokevirtual setEncryptionPaddings : ([Ljava/lang/String;)Landroid/security/keystore/KeyGenParameterSpec$Builder;
    //   293: pop
    //   294: aload_1
    //   295: iconst_2
    //   296: anewarray java/lang/String
    //   299: dup
    //   300: iconst_0
    //   301: ldc_w 'PKCS1'
    //   304: aastore
    //   305: dup
    //   306: iconst_1
    //   307: ldc_w 'PSS'
    //   310: aastore
    //   311: invokevirtual setSignaturePaddings : ([Ljava/lang/String;)Landroid/security/keystore/KeyGenParameterSpec$Builder;
    //   314: pop
    //   315: aload_1
    //   316: iconst_0
    //   317: invokevirtual setRandomizedEncryptionRequired : (Z)Landroid/security/keystore/KeyGenParameterSpec$Builder;
    //   320: pop
    //   321: aload #7
    //   323: invokevirtual getKeySize : ()I
    //   326: iconst_m1
    //   327: if_icmpeq -> 340
    //   330: aload_1
    //   331: aload #7
    //   333: invokevirtual getKeySize : ()I
    //   336: invokevirtual setKeySize : (I)Landroid/security/keystore/KeyGenParameterSpec$Builder;
    //   339: pop
    //   340: aload #7
    //   342: invokevirtual getAlgorithmParameterSpec : ()Ljava/security/spec/AlgorithmParameterSpec;
    //   345: ifnull -> 362
    //   348: aload #7
    //   350: invokevirtual getAlgorithmParameterSpec : ()Ljava/security/spec/AlgorithmParameterSpec;
    //   353: astore #8
    //   355: aload_1
    //   356: aload #8
    //   358: invokevirtual setAlgorithmParameterSpec : (Ljava/security/spec/AlgorithmParameterSpec;)Landroid/security/keystore/KeyGenParameterSpec$Builder;
    //   361: pop
    //   362: aload_1
    //   363: aload #7
    //   365: invokevirtual getSubjectDN : ()Ljavax/security/auth/x500/X500Principal;
    //   368: invokevirtual setCertificateSubject : (Ljavax/security/auth/x500/X500Principal;)Landroid/security/keystore/KeyGenParameterSpec$Builder;
    //   371: pop
    //   372: aload_1
    //   373: aload #7
    //   375: invokevirtual getSerialNumber : ()Ljava/math/BigInteger;
    //   378: invokevirtual setCertificateSerialNumber : (Ljava/math/BigInteger;)Landroid/security/keystore/KeyGenParameterSpec$Builder;
    //   381: pop
    //   382: aload_1
    //   383: aload #7
    //   385: invokevirtual getStartDate : ()Ljava/util/Date;
    //   388: invokevirtual setCertificateNotBefore : (Ljava/util/Date;)Landroid/security/keystore/KeyGenParameterSpec$Builder;
    //   391: pop
    //   392: aload_1
    //   393: aload #7
    //   395: invokevirtual getEndDate : ()Ljava/util/Date;
    //   398: invokevirtual setCertificateNotAfter : (Ljava/util/Date;)Landroid/security/keystore/KeyGenParameterSpec$Builder;
    //   401: pop
    //   402: aload #7
    //   404: invokevirtual isEncryptionRequired : ()Z
    //   407: istore_3
    //   408: aload_1
    //   409: iconst_0
    //   410: invokevirtual setUserAuthenticationRequired : (Z)Landroid/security/keystore/KeyGenParameterSpec$Builder;
    //   413: pop
    //   414: aload_1
    //   415: invokevirtual build : ()Landroid/security/keystore/KeyGenParameterSpec;
    //   418: astore_1
    //   419: aload_0
    //   420: aload_1
    //   421: invokevirtual getKeystoreAlias : ()Ljava/lang/String;
    //   424: putfield mEntryAlias : Ljava/lang/String;
    //   427: aload_0
    //   428: aload_1
    //   429: invokevirtual getUid : ()I
    //   432: putfield mEntryUid : I
    //   435: aload_0
    //   436: aload_1
    //   437: putfield mSpec : Landroid/security/keystore/KeyGenParameterSpec;
    //   440: aload_0
    //   441: iload #4
    //   443: putfield mKeymasterAlgorithm : I
    //   446: aload_0
    //   447: iload_3
    //   448: putfield mEncryptionAtRestRequired : Z
    //   451: aload_0
    //   452: aload_1
    //   453: invokevirtual getKeySize : ()I
    //   456: putfield mKeySizeBits : I
    //   459: aload_0
    //   460: invokespecial initAlgorithmSpecificParameters : ()V
    //   463: aload_0
    //   464: getfield mKeySizeBits : I
    //   467: iconst_m1
    //   468: if_icmpne -> 480
    //   471: aload_0
    //   472: iload #4
    //   474: invokestatic getDefaultKeySize : (I)I
    //   477: putfield mKeySizeBits : I
    //   480: iload #4
    //   482: aload_0
    //   483: getfield mKeySizeBits : I
    //   486: aload_0
    //   487: getfield mSpec : Landroid/security/keystore/KeyGenParameterSpec;
    //   490: invokevirtual isStrongBoxBacked : ()Z
    //   493: invokestatic checkValidKeySize : (IIZ)V
    //   496: aload_1
    //   497: invokevirtual getKeystoreAlias : ()Ljava/lang/String;
    //   500: astore #7
    //   502: aload #7
    //   504: ifnull -> 774
    //   507: iload #4
    //   509: invokestatic fromKeymasterAsymmetricKeyAlgorithm : (I)Ljava/lang/String;
    //   512: astore #7
    //   514: aload_0
    //   515: aload_1
    //   516: invokevirtual getPurposes : ()I
    //   519: invokestatic allToKeymaster : (I)[I
    //   522: putfield mKeymasterPurposes : [I
    //   525: aload_0
    //   526: aload_1
    //   527: invokevirtual getBlockModes : ()[Ljava/lang/String;
    //   530: invokestatic allToKeymaster : ([Ljava/lang/String;)[I
    //   533: putfield mKeymasterBlockModes : [I
    //   536: aload_1
    //   537: invokevirtual getEncryptionPaddings : ()[Ljava/lang/String;
    //   540: astore #8
    //   542: aload_0
    //   543: aload #8
    //   545: invokestatic allToKeymaster : ([Ljava/lang/String;)[I
    //   548: putfield mKeymasterEncryptionPaddings : [I
    //   551: aload_1
    //   552: invokevirtual getPurposes : ()I
    //   555: iconst_1
    //   556: iand
    //   557: ifeq -> 676
    //   560: aload_1
    //   561: invokevirtual isRandomizedEncryptionRequired : ()Z
    //   564: ifeq -> 676
    //   567: aload_0
    //   568: getfield mKeymasterEncryptionPaddings : [I
    //   571: astore #8
    //   573: aload #8
    //   575: arraylength
    //   576: istore #9
    //   578: iload #6
    //   580: istore #4
    //   582: iload #4
    //   584: iload #9
    //   586: if_icmpge -> 676
    //   589: aload #8
    //   591: iload #4
    //   593: iaload
    //   594: istore #6
    //   596: iload #6
    //   598: invokestatic isKeymasterPaddingSchemeIndCpaCompatibleWithAsymmetricCrypto : (I)Z
    //   601: ifeq -> 610
    //   604: iinc #4, 1
    //   607: goto -> 582
    //   610: new java/security/InvalidAlgorithmParameterException
    //   613: astore_1
    //   614: new java/lang/StringBuilder
    //   617: astore_2
    //   618: aload_2
    //   619: invokespecial <init> : ()V
    //   622: aload_2
    //   623: ldc_w 'Randomized encryption (IND-CPA) required but may be violated by padding scheme: '
    //   626: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   629: pop
    //   630: aload_2
    //   631: iload #6
    //   633: invokestatic fromKeymaster : (I)Ljava/lang/String;
    //   636: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   639: pop
    //   640: aload_2
    //   641: ldc_w '. See '
    //   644: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   647: pop
    //   648: aload_2
    //   649: ldc android/security/keystore/KeyGenParameterSpec
    //   651: invokevirtual getName : ()Ljava/lang/String;
    //   654: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   657: pop
    //   658: aload_2
    //   659: ldc_w ' documentation.'
    //   662: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   665: pop
    //   666: aload_1
    //   667: aload_2
    //   668: invokevirtual toString : ()Ljava/lang/String;
    //   671: invokespecial <init> : (Ljava/lang/String;)V
    //   674: aload_1
    //   675: athrow
    //   676: aload_1
    //   677: invokevirtual getSignaturePaddings : ()[Ljava/lang/String;
    //   680: astore #8
    //   682: aload_0
    //   683: aload #8
    //   685: invokestatic allToKeymaster : ([Ljava/lang/String;)[I
    //   688: putfield mKeymasterSignaturePaddings : [I
    //   691: aload_1
    //   692: invokevirtual isDigestsSpecified : ()Z
    //   695: ifeq -> 712
    //   698: aload_0
    //   699: aload_1
    //   700: invokevirtual getDigests : ()[Ljava/lang/String;
    //   703: invokestatic allToKeymaster : ([Ljava/lang/String;)[I
    //   706: putfield mKeymasterDigests : [I
    //   709: goto -> 719
    //   712: aload_0
    //   713: getstatic libcore/util/EmptyArray.INT : [I
    //   716: putfield mKeymasterDigests : [I
    //   719: new android/security/keymaster/KeymasterArguments
    //   722: astore_1
    //   723: aload_1
    //   724: invokespecial <init> : ()V
    //   727: aload_1
    //   728: aload_0
    //   729: getfield mSpec : Landroid/security/keystore/KeyGenParameterSpec;
    //   732: invokestatic addUserAuthArgs : (Landroid/security/keymaster/KeymasterArguments;Landroid/security/keystore/UserAuthArgs;)V
    //   735: aload_0
    //   736: aload #7
    //   738: putfield mJcaKeyAlgorithm : Ljava/lang/String;
    //   741: aload_0
    //   742: aload_2
    //   743: putfield mRng : Ljava/security/SecureRandom;
    //   746: aload_0
    //   747: invokestatic getInstance : ()Landroid/security/KeyStore;
    //   750: putfield mKeyStore : Landroid/security/KeyStore;
    //   753: iconst_1
    //   754: ifne -> 761
    //   757: aload_0
    //   758: invokespecial resetAll : ()V
    //   761: return
    //   762: astore_1
    //   763: new java/security/InvalidAlgorithmParameterException
    //   766: astore_2
    //   767: aload_2
    //   768: aload_1
    //   769: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   772: aload_2
    //   773: athrow
    //   774: new java/security/InvalidAlgorithmParameterException
    //   777: astore_1
    //   778: aload_1
    //   779: ldc_w 'KeyStore entry alias not provided'
    //   782: invokespecial <init> : (Ljava/lang/String;)V
    //   785: aload_1
    //   786: athrow
    //   787: astore_2
    //   788: new java/security/InvalidAlgorithmParameterException
    //   791: astore_1
    //   792: aload_1
    //   793: aload_2
    //   794: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   797: aload_1
    //   798: athrow
    //   799: new java/security/InvalidAlgorithmParameterException
    //   802: astore #7
    //   804: new java/lang/StringBuilder
    //   807: astore_2
    //   808: aload_2
    //   809: invokespecial <init> : ()V
    //   812: aload_2
    //   813: ldc_w 'Unsupported params class: '
    //   816: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   819: pop
    //   820: aload_2
    //   821: aload_1
    //   822: invokevirtual getClass : ()Ljava/lang/Class;
    //   825: invokevirtual getName : ()Ljava/lang/String;
    //   828: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   831: pop
    //   832: aload_2
    //   833: ldc_w '. Supported: '
    //   836: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   839: pop
    //   840: aload_2
    //   841: ldc android/security/keystore/KeyGenParameterSpec
    //   843: invokevirtual getName : ()Ljava/lang/String;
    //   846: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   849: pop
    //   850: aload_2
    //   851: ldc_w ', '
    //   854: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   857: pop
    //   858: aload_2
    //   859: ldc_w android/security/KeyPairGeneratorSpec
    //   862: invokevirtual getName : ()Ljava/lang/String;
    //   865: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   868: pop
    //   869: aload #7
    //   871: aload_2
    //   872: invokevirtual toString : ()Ljava/lang/String;
    //   875: invokespecial <init> : (Ljava/lang/String;)V
    //   878: aload #7
    //   880: athrow
    //   881: astore_1
    //   882: goto -> 945
    //   885: new java/security/InvalidAlgorithmParameterException
    //   888: astore_2
    //   889: new java/lang/StringBuilder
    //   892: astore_1
    //   893: aload_1
    //   894: invokespecial <init> : ()V
    //   897: aload_1
    //   898: ldc_w 'Must supply params of type '
    //   901: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   904: pop
    //   905: aload_1
    //   906: ldc android/security/keystore/KeyGenParameterSpec
    //   908: invokevirtual getName : ()Ljava/lang/String;
    //   911: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   914: pop
    //   915: aload_1
    //   916: ldc_w ' or '
    //   919: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   922: pop
    //   923: aload_1
    //   924: ldc_w android/security/KeyPairGeneratorSpec
    //   927: invokevirtual getName : ()Ljava/lang/String;
    //   930: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   933: pop
    //   934: aload_2
    //   935: aload_1
    //   936: invokevirtual toString : ()Ljava/lang/String;
    //   939: invokespecial <init> : (Ljava/lang/String;)V
    //   942: aload_2
    //   943: athrow
    //   944: astore_1
    //   945: iconst_0
    //   946: ifne -> 953
    //   949: aload_0
    //   950: invokespecial resetAll : ()V
    //   953: aload_1
    //   954: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #187	-> 0
    //   #189	-> 4
    //   #191	-> 4
    //   #198	-> 8
    //   #199	-> 10
    //   #200	-> 16
    //   #201	-> 30
    //   #202	-> 38
    //   #204	-> 45
    //   #207	-> 51
    //   #208	-> 57
    //   #211	-> 61
    //   #212	-> 61
    //   #217	-> 67
    //   #214	-> 70
    //   #215	-> 71
    //   #219	-> 85
    //   #221	-> 97
    //   #222	-> 101
    //   #227	-> 112
    //   #234	-> 158
    //   #265	-> 161
    //   #236	-> 199
    //   #237	-> 203
    //   #243	-> 214
    //   #253	-> 267
    //   #257	-> 294
    //   #262	-> 315
    //   #263	-> 321
    //   #269	-> 321
    //   #270	-> 330
    //   #272	-> 340
    //   #273	-> 348
    //   #274	-> 348
    //   #273	-> 355
    //   #276	-> 362
    //   #277	-> 372
    //   #278	-> 382
    //   #279	-> 392
    //   #280	-> 402
    //   #281	-> 408
    //   #283	-> 414
    //   #286	-> 419
    //   #287	-> 419
    //   #294	-> 419
    //   #295	-> 427
    //   #296	-> 435
    //   #297	-> 440
    //   #298	-> 446
    //   #299	-> 451
    //   #300	-> 459
    //   #301	-> 463
    //   #302	-> 471
    //   #304	-> 480
    //   #306	-> 496
    //   #312	-> 507
    //   #314	-> 514
    //   #315	-> 525
    //   #316	-> 536
    //   #317	-> 536
    //   #316	-> 542
    //   #318	-> 551
    //   #319	-> 560
    //   #320	-> 567
    //   #321	-> 596
    //   #322	-> 596
    //   #320	-> 604
    //   #324	-> 610
    //   #327	-> 630
    //   #329	-> 648
    //   #334	-> 676
    //   #335	-> 676
    //   #334	-> 682
    //   #336	-> 691
    //   #337	-> 698
    //   #339	-> 712
    //   #345	-> 719
    //   #348	-> 735
    //   #350	-> 735
    //   #351	-> 741
    //   #352	-> 746
    //   #353	-> 753
    //   #355	-> 753
    //   #356	-> 757
    //   #359	-> 761
    //   #346	-> 762
    //   #347	-> 763
    //   #307	-> 774
    //   #284	-> 787
    //   #285	-> 788
    //   #288	-> 799
    //   #289	-> 820
    //   #290	-> 840
    //   #291	-> 858
    //   #355	-> 881
    //   #192	-> 885
    //   #193	-> 905
    //   #194	-> 923
    //   #355	-> 944
    //   #356	-> 949
    //   #358	-> 953
    // Exception table:
    //   from	to	target	type
    //   10	16	881	finally
    //   16	22	881	finally
    //   30	35	881	finally
    //   38	45	881	finally
    //   45	51	881	finally
    //   51	57	787	java/lang/NullPointerException
    //   51	57	787	java/lang/IllegalArgumentException
    //   51	57	881	finally
    //   61	67	70	java/lang/IllegalArgumentException
    //   61	67	787	java/lang/NullPointerException
    //   61	67	881	finally
    //   71	85	787	java/lang/NullPointerException
    //   71	85	787	java/lang/IllegalArgumentException
    //   71	85	881	finally
    //   97	101	787	java/lang/NullPointerException
    //   97	101	787	java/lang/IllegalArgumentException
    //   97	101	881	finally
    //   101	112	787	java/lang/NullPointerException
    //   101	112	787	java/lang/IllegalArgumentException
    //   101	112	881	finally
    //   112	158	787	java/lang/NullPointerException
    //   112	158	787	java/lang/IllegalArgumentException
    //   112	158	881	finally
    //   161	199	787	java/lang/NullPointerException
    //   161	199	787	java/lang/IllegalArgumentException
    //   161	199	881	finally
    //   199	203	787	java/lang/NullPointerException
    //   199	203	787	java/lang/IllegalArgumentException
    //   199	203	881	finally
    //   203	214	787	java/lang/NullPointerException
    //   203	214	787	java/lang/IllegalArgumentException
    //   203	214	881	finally
    //   214	267	787	java/lang/NullPointerException
    //   214	267	787	java/lang/IllegalArgumentException
    //   214	267	881	finally
    //   267	294	787	java/lang/NullPointerException
    //   267	294	787	java/lang/IllegalArgumentException
    //   267	294	881	finally
    //   294	315	787	java/lang/NullPointerException
    //   294	315	787	java/lang/IllegalArgumentException
    //   294	315	881	finally
    //   315	321	787	java/lang/NullPointerException
    //   315	321	787	java/lang/IllegalArgumentException
    //   315	321	881	finally
    //   321	330	787	java/lang/NullPointerException
    //   321	330	787	java/lang/IllegalArgumentException
    //   321	330	881	finally
    //   330	340	787	java/lang/NullPointerException
    //   330	340	787	java/lang/IllegalArgumentException
    //   330	340	881	finally
    //   340	348	787	java/lang/NullPointerException
    //   340	348	787	java/lang/IllegalArgumentException
    //   340	348	881	finally
    //   348	355	787	java/lang/NullPointerException
    //   348	355	787	java/lang/IllegalArgumentException
    //   348	355	881	finally
    //   355	362	787	java/lang/NullPointerException
    //   355	362	787	java/lang/IllegalArgumentException
    //   355	362	881	finally
    //   362	372	787	java/lang/NullPointerException
    //   362	372	787	java/lang/IllegalArgumentException
    //   362	372	881	finally
    //   372	382	787	java/lang/NullPointerException
    //   372	382	787	java/lang/IllegalArgumentException
    //   372	382	881	finally
    //   382	392	787	java/lang/NullPointerException
    //   382	392	787	java/lang/IllegalArgumentException
    //   382	392	881	finally
    //   392	402	787	java/lang/NullPointerException
    //   392	402	787	java/lang/IllegalArgumentException
    //   392	402	881	finally
    //   402	408	787	java/lang/NullPointerException
    //   402	408	787	java/lang/IllegalArgumentException
    //   402	408	881	finally
    //   408	414	787	java/lang/NullPointerException
    //   408	414	787	java/lang/IllegalArgumentException
    //   408	414	881	finally
    //   414	419	787	java/lang/NullPointerException
    //   414	419	787	java/lang/IllegalArgumentException
    //   414	419	881	finally
    //   419	427	881	finally
    //   427	435	881	finally
    //   435	440	881	finally
    //   440	446	881	finally
    //   446	451	881	finally
    //   451	459	881	finally
    //   459	463	881	finally
    //   463	471	881	finally
    //   471	480	881	finally
    //   480	496	881	finally
    //   496	502	881	finally
    //   507	514	762	java/lang/IllegalArgumentException
    //   507	514	762	java/lang/IllegalStateException
    //   507	514	881	finally
    //   514	525	762	java/lang/IllegalArgumentException
    //   514	525	762	java/lang/IllegalStateException
    //   514	525	881	finally
    //   525	536	762	java/lang/IllegalArgumentException
    //   525	536	762	java/lang/IllegalStateException
    //   525	536	881	finally
    //   536	542	762	java/lang/IllegalArgumentException
    //   536	542	762	java/lang/IllegalStateException
    //   536	542	881	finally
    //   542	551	762	java/lang/IllegalArgumentException
    //   542	551	762	java/lang/IllegalStateException
    //   542	551	881	finally
    //   551	560	762	java/lang/IllegalArgumentException
    //   551	560	762	java/lang/IllegalStateException
    //   551	560	881	finally
    //   560	567	762	java/lang/IllegalArgumentException
    //   560	567	762	java/lang/IllegalStateException
    //   560	567	881	finally
    //   567	578	762	java/lang/IllegalArgumentException
    //   567	578	762	java/lang/IllegalStateException
    //   567	578	881	finally
    //   596	604	762	java/lang/IllegalArgumentException
    //   596	604	762	java/lang/IllegalStateException
    //   596	604	881	finally
    //   610	630	762	java/lang/IllegalArgumentException
    //   610	630	762	java/lang/IllegalStateException
    //   610	630	881	finally
    //   630	648	762	java/lang/IllegalArgumentException
    //   630	648	762	java/lang/IllegalStateException
    //   630	648	881	finally
    //   648	676	762	java/lang/IllegalArgumentException
    //   648	676	762	java/lang/IllegalStateException
    //   648	676	881	finally
    //   676	682	762	java/lang/IllegalArgumentException
    //   676	682	762	java/lang/IllegalStateException
    //   676	682	881	finally
    //   682	691	762	java/lang/IllegalArgumentException
    //   682	691	762	java/lang/IllegalStateException
    //   682	691	881	finally
    //   691	698	762	java/lang/IllegalArgumentException
    //   691	698	762	java/lang/IllegalStateException
    //   691	698	881	finally
    //   698	709	762	java/lang/IllegalArgumentException
    //   698	709	762	java/lang/IllegalStateException
    //   698	709	881	finally
    //   712	719	762	java/lang/IllegalArgumentException
    //   712	719	762	java/lang/IllegalStateException
    //   712	719	881	finally
    //   719	735	762	java/lang/IllegalArgumentException
    //   719	735	762	java/lang/IllegalStateException
    //   719	735	881	finally
    //   735	741	881	finally
    //   741	746	944	finally
    //   746	753	944	finally
    //   763	774	944	finally
    //   774	787	944	finally
    //   788	799	944	finally
    //   799	820	944	finally
    //   820	840	944	finally
    //   840	858	944	finally
    //   858	881	944	finally
    //   885	905	944	finally
    //   905	923	944	finally
    //   923	944	944	finally
  }
  
  private void resetAll() {
    this.mEntryAlias = null;
    this.mEntryUid = -1;
    this.mJcaKeyAlgorithm = null;
    this.mKeymasterAlgorithm = -1;
    this.mKeymasterPurposes = null;
    this.mKeymasterBlockModes = null;
    this.mKeymasterEncryptionPaddings = null;
    this.mKeymasterSignaturePaddings = null;
    this.mKeymasterDigests = null;
    this.mKeySizeBits = 0;
    this.mSpec = null;
    this.mRSAPublicExponent = null;
    this.mEncryptionAtRestRequired = false;
    this.mRng = null;
    this.mKeyStore = null;
  }
  
  private void initAlgorithmSpecificParameters() throws InvalidAlgorithmParameterException {
    StringBuilder stringBuilder;
    AlgorithmParameterSpec algorithmParameterSpec = this.mSpec.getAlgorithmParameterSpec();
    int i = this.mKeymasterAlgorithm;
    if (i != 1) {
      if (i == 3) {
        if (algorithmParameterSpec instanceof ECGenParameterSpec) {
          StringBuilder stringBuilder1;
          ECGenParameterSpec eCGenParameterSpec = (ECGenParameterSpec)algorithmParameterSpec;
          String str1 = eCGenParameterSpec.getName();
          Map<String, Integer> map = SUPPORTED_EC_NIST_CURVE_NAME_TO_SIZE;
          Locale locale = Locale.US;
          String str2 = str1.toLowerCase(locale);
          Integer integer = map.get(str2);
          if (integer != null) {
            i = this.mKeySizeBits;
            if (i == -1) {
              this.mKeySizeBits = integer.intValue();
            } else if (i != integer.intValue()) {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("EC key size must match  between ");
              stringBuilder1.append(this.mSpec);
              stringBuilder1.append(" and ");
              stringBuilder1.append(algorithmParameterSpec);
              stringBuilder1.append(": ");
              stringBuilder1.append(this.mKeySizeBits);
              stringBuilder1.append(" vs ");
              stringBuilder1.append(integer);
              throw new InvalidAlgorithmParameterException(stringBuilder1.toString());
            } 
          } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Unsupported EC curve name: ");
            stringBuilder.append((String)stringBuilder1);
            stringBuilder.append(". Supported: ");
            stringBuilder.append(SUPPORTED_EC_NIST_CURVE_NAMES);
            throw new InvalidAlgorithmParameterException(stringBuilder.toString());
          } 
        } else if (stringBuilder != null) {
          throw new InvalidAlgorithmParameterException("EC may only use ECGenParameterSpec");
        } 
      } else {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Unsupported algorithm: ");
        stringBuilder1.append(this.mKeymasterAlgorithm);
        throw new ProviderException(stringBuilder1.toString());
      } 
    } else {
      BigInteger bigInteger2;
      RSAKeyGenParameterSpec rSAKeyGenParameterSpec = null;
      if (stringBuilder instanceof RSAKeyGenParameterSpec) {
        rSAKeyGenParameterSpec = (RSAKeyGenParameterSpec)stringBuilder;
        i = this.mKeySizeBits;
        if (i == -1) {
          this.mKeySizeBits = rSAKeyGenParameterSpec.getKeysize();
        } else if (i != rSAKeyGenParameterSpec.getKeysize()) {
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("RSA key size must match  between ");
          stringBuilder2.append(this.mSpec);
          stringBuilder2.append(" and ");
          stringBuilder2.append(stringBuilder);
          stringBuilder2.append(": ");
          stringBuilder2.append(this.mKeySizeBits);
          stringBuilder2.append(" vs ");
          stringBuilder2.append(rSAKeyGenParameterSpec.getKeysize());
          throw new InvalidAlgorithmParameterException(stringBuilder2.toString());
        } 
        bigInteger2 = rSAKeyGenParameterSpec.getPublicExponent();
      } else if (stringBuilder != null) {
        throw new InvalidAlgorithmParameterException("RSA may only use RSAKeyGenParameterSpec");
      } 
      BigInteger bigInteger1 = bigInteger2;
      if (bigInteger2 == null)
        bigInteger1 = RSAKeyGenParameterSpec.F4; 
      if (bigInteger1.compareTo(BigInteger.ZERO) >= 1) {
        if (bigInteger1.compareTo(KeymasterArguments.UINT64_MAX_VALUE) <= 0) {
          this.mRSAPublicExponent = bigInteger1;
          return;
        } 
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Unsupported RSA public exponent: ");
        stringBuilder2.append(bigInteger1);
        stringBuilder2.append(". Maximum supported value: ");
        stringBuilder2.append(KeymasterArguments.UINT64_MAX_VALUE);
        throw new InvalidAlgorithmParameterException(stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("RSA public exponent must be positive: ");
      stringBuilder1.append(bigInteger1);
      throw new InvalidAlgorithmParameterException(stringBuilder1.toString());
    } 
  }
  
  public KeyPair generateKeyPair() {
    KeyStore keyStore = this.mKeyStore;
    if (keyStore != null && this.mSpec != null) {
      boolean bool = this.mEncryptionAtRestRequired;
      if ((bool & true) == 0 || 
        keyStore.state() == KeyStore.State.UNLOCKED) {
        boolean bool1 = bool;
        if (this.mSpec.isStrongBoxBacked())
          j = bool | 0x10; 
        int i = j;
        if (this.mSpec.isCriticalToDeviceEncryption())
          i = j | 0x8; 
        SecureRandom secureRandom = this.mRng;
        int j = (this.mKeySizeBits + 7) / 8;
        byte[] arrayOfByte = KeyStoreCryptoOperationUtils.getRandomBytesToMixIntoKeystoreRng(secureRandom, j);
        Credentials.deleteAllTypesForAlias(this.mKeyStore, this.mEntryAlias, this.mEntryUid);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("USRPKEY_");
        stringBuilder.append(this.mEntryAlias);
        String str = stringBuilder.toString();
        try {
          KeymasterArguments keymasterArguments = constructKeyGenerationArguments();
          generateKeystoreKeyPair(str, keymasterArguments, arrayOfByte, i);
          KeyPair keyPair = loadKeystoreKeyPair(str);
          storeCertificateChain(i, createCertificateChain(str, keyPair));
          if (!true)
            Credentials.deleteAllTypesForAlias(this.mKeyStore, this.mEntryAlias, this.mEntryUid); 
          return keyPair;
        } catch (ProviderException providerException) {
          if ((this.mSpec.getPurposes() & 0x20) != 0) {
            SecureKeyImportUnavailableException secureKeyImportUnavailableException = new SecureKeyImportUnavailableException();
            this(providerException);
            throw secureKeyImportUnavailableException;
          } 
          throw providerException;
        } finally {}
        if (!false)
          Credentials.deleteAllTypesForAlias(this.mKeyStore, this.mEntryAlias, this.mEntryUid); 
        throw arrayOfByte;
      } 
      throw new IllegalStateException("Encryption at rest using secure lock screen credential requested for key pair, but the user has not yet entered the credential");
    } 
    throw new IllegalStateException("Not initialized");
  }
  
  private Iterable<byte[]> createCertificateChain(String paramString, KeyPair paramKeyPair) throws ProviderException {
    byte[] arrayOfByte = this.mSpec.getAttestationChallenge();
    if (arrayOfByte != null) {
      KeymasterArguments keymasterArguments = new KeymasterArguments();
      keymasterArguments.addBytes(-1879047484, arrayOfByte);
      return getAttestationChain(paramString, paramKeyPair, keymasterArguments);
    } 
    return (Iterable)Collections.singleton(generateSelfSignedCertificateBytes(paramKeyPair));
  }
  
  private void generateKeystoreKeyPair(String paramString, KeymasterArguments paramKeymasterArguments, byte[] paramArrayOfbyte, int paramInt) throws ProviderException {
    KeyCharacteristics keyCharacteristics = new KeyCharacteristics();
    paramInt = this.mKeyStore.generateKey(paramString, paramKeymasterArguments, paramArrayOfbyte, this.mEntryUid, paramInt, keyCharacteristics);
    if (paramInt != 1) {
      if (paramInt == -68)
        throw new StrongBoxUnavailableException("Failed to generate key pair"); 
      throw new ProviderException("Failed to generate key pair", KeyStore.getKeyStoreException(paramInt));
    } 
  }
  
  private KeyPair loadKeystoreKeyPair(String paramString) throws ProviderException {
    try {
      KeyPair keyPair = AndroidKeyStoreProvider.loadAndroidKeyStoreKeyPairFromKeystore(this.mKeyStore, paramString, this.mEntryUid);
      if (this.mJcaKeyAlgorithm.equalsIgnoreCase(keyPair.getPrivate().getAlgorithm()))
        return keyPair; 
      ProviderException providerException = new ProviderException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Generated key pair algorithm does not match requested algorithm: ");
      stringBuilder.append(keyPair.getPrivate().getAlgorithm());
      stringBuilder.append(" vs ");
      stringBuilder.append(this.mJcaKeyAlgorithm);
      this(stringBuilder.toString());
      throw providerException;
    } catch (UnrecoverableKeyException|KeyPermanentlyInvalidatedException unrecoverableKeyException) {
      throw new ProviderException("Failed to load generated key pair from keystore", unrecoverableKeyException);
    } 
  }
  
  private KeymasterArguments constructKeyGenerationArguments() {
    KeymasterArguments keymasterArguments = new KeymasterArguments();
    keymasterArguments.addUnsignedInt(805306371, this.mKeySizeBits);
    keymasterArguments.addEnum(268435458, this.mKeymasterAlgorithm);
    keymasterArguments.addEnums(536870913, this.mKeymasterPurposes);
    keymasterArguments.addEnums(536870916, this.mKeymasterBlockModes);
    keymasterArguments.addEnums(536870918, this.mKeymasterEncryptionPaddings);
    keymasterArguments.addEnums(536870918, this.mKeymasterSignaturePaddings);
    keymasterArguments.addEnums(536870917, this.mKeymasterDigests);
    KeymasterUtils.addUserAuthArgs(keymasterArguments, this.mSpec);
    keymasterArguments.addDateIfNotNull(1610613136, this.mSpec.getKeyValidityStart());
    KeyGenParameterSpec keyGenParameterSpec2 = this.mSpec;
    Date date2 = keyGenParameterSpec2.getKeyValidityForOriginationEnd();
    keymasterArguments.addDateIfNotNull(1610613137, date2);
    KeyGenParameterSpec keyGenParameterSpec1 = this.mSpec;
    Date date1 = keyGenParameterSpec1.getKeyValidityForConsumptionEnd();
    keymasterArguments.addDateIfNotNull(1610613138, date1);
    addAlgorithmSpecificParameters(keymasterArguments);
    if (this.mSpec.isUniqueIdIncluded())
      keymasterArguments.addBoolean(1879048394); 
    return keymasterArguments;
  }
  
  private void storeCertificateChain(int paramInt, Iterable<byte[]> paramIterable) throws ProviderException {
    Iterator<byte> iterator = paramIterable.iterator();
    byte[] arrayOfByte = (byte[])iterator.next();
    storeCertificate("USRCERT_", arrayOfByte, paramInt, "Failed to store certificate");
    if (!iterator.hasNext())
      return; 
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    while (iterator.hasNext()) {
      byte[] arrayOfByte1 = (byte[])iterator.next();
      byteArrayOutputStream.write(arrayOfByte1, 0, arrayOfByte1.length);
    } 
    storeCertificate("CACERT_", byteArrayOutputStream.toByteArray(), paramInt, "Failed to store attestation CA certificate");
  }
  
  private void storeCertificate(String paramString1, byte[] paramArrayOfbyte, int paramInt, String paramString2) throws ProviderException {
    KeyStore keyStore = this.mKeyStore;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append(this.mEntryAlias);
    paramInt = keyStore.insert(stringBuilder.toString(), paramArrayOfbyte, this.mEntryUid, paramInt);
    if (paramInt == 1)
      return; 
    throw new ProviderException(paramString2, KeyStore.getKeyStoreException(paramInt));
  }
  
  private byte[] generateSelfSignedCertificateBytes(KeyPair paramKeyPair) throws ProviderException {
    try {
      X509Certificate x509Certificate = generateSelfSignedCertificate(paramKeyPair.getPrivate(), paramKeyPair.getPublic());
      return 
        x509Certificate.getEncoded();
    } catch (IOException|CertificateParsingException iOException) {
      throw new ProviderException("Failed to generate self-signed certificate", iOException);
    } catch (CertificateEncodingException certificateEncodingException) {
      throw new ProviderException("Failed to obtain encoded form of self-signed certificate", certificateEncodingException);
    } 
  }
  
  private Iterable<byte[]> getAttestationChain(String paramString, KeyPair paramKeyPair, KeymasterArguments paramKeymasterArguments) throws ProviderException {
    KeymasterCertificateChain keymasterCertificateChain = new KeymasterCertificateChain();
    int i = this.mKeyStore.attestKey(paramString, paramKeymasterArguments, keymasterCertificateChain);
    if (i == 1) {
      List<byte[]> list = keymasterCertificateChain.getCertificates();
      if (list.size() >= 2)
        return (Iterable<byte[]>)list; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Attestation certificate chain contained ");
      stringBuilder.append(list.size());
      stringBuilder.append(" entries. At least two are required.");
      throw new ProviderException(stringBuilder.toString());
    } 
    throw new ProviderException("Failed to generate attestation certificate chain", KeyStore.getKeyStoreException(i));
  }
  
  private void addAlgorithmSpecificParameters(KeymasterArguments paramKeymasterArguments) {
    StringBuilder stringBuilder;
    int i = this.mKeymasterAlgorithm;
    if (i != 1) {
      if (i != 3) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported algorithm: ");
        stringBuilder.append(this.mKeymasterAlgorithm);
        throw new ProviderException(stringBuilder.toString());
      } 
    } else {
      stringBuilder.addUnsignedLong(1342177480, this.mRSAPublicExponent);
    } 
  }
  
  private X509Certificate generateSelfSignedCertificate(PrivateKey paramPrivateKey, PublicKey paramPublicKey) throws CertificateParsingException, IOException {
    int i = this.mKeymasterAlgorithm, j = this.mKeySizeBits;
    KeyGenParameterSpec keyGenParameterSpec = this.mSpec;
    String str = getCertificateSignatureAlgorithm(i, j, keyGenParameterSpec);
    if (str == null)
      return generateSelfSignedCertificateWithFakeSignature(paramPublicKey); 
    try {
      return generateSelfSignedCertificateWithValidSignature(paramPrivateKey, paramPublicKey, str);
    } catch (Exception exception) {
      return generateSelfSignedCertificateWithFakeSignature(paramPublicKey);
    } 
  }
  
  private X509Certificate generateSelfSignedCertificateWithValidSignature(PrivateKey paramPrivateKey, PublicKey paramPublicKey, String paramString) throws Exception {
    X509V3CertificateGenerator x509V3CertificateGenerator = new X509V3CertificateGenerator();
    x509V3CertificateGenerator.setPublicKey(paramPublicKey);
    x509V3CertificateGenerator.setSerialNumber(this.mSpec.getCertificateSerialNumber());
    x509V3CertificateGenerator.setSubjectDN(this.mSpec.getCertificateSubject());
    x509V3CertificateGenerator.setIssuerDN(this.mSpec.getCertificateSubject());
    x509V3CertificateGenerator.setNotBefore(this.mSpec.getCertificateNotBefore());
    x509V3CertificateGenerator.setNotAfter(this.mSpec.getCertificateNotAfter());
    x509V3CertificateGenerator.setSignatureAlgorithm(paramString);
    return x509V3CertificateGenerator.generate(paramPrivateKey);
  }
  
  private X509Certificate generateSelfSignedCertificateWithFakeSignature(PublicKey paramPublicKey) throws IOException, CertificateParsingException {
    StringBuilder stringBuilder;
    TBSCertificate tBSCertificate;
    AlgorithmIdentifier algorithmIdentifier;
    V3TBSCertificateGenerator v3TBSCertificateGenerator = new V3TBSCertificateGenerator();
    int i = this.mKeymasterAlgorithm;
    if (i != 1) {
      if (i == 3) {
        ASN1ObjectIdentifier aSN1ObjectIdentifier = X9ObjectIdentifiers.ecdsa_with_SHA256;
        algorithmIdentifier = new AlgorithmIdentifier(aSN1ObjectIdentifier);
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        aSN1EncodableVector.add((ASN1Encodable)new DERInteger(0L));
        aSN1EncodableVector.add((ASN1Encodable)new DERInteger(0L));
        null = (new DERSequence()).getEncoded();
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported key algorithm: ");
        stringBuilder.append(this.mKeymasterAlgorithm);
        throw new ProviderException(stringBuilder.toString());
      } 
    } else {
      ASN1ObjectIdentifier aSN1ObjectIdentifier = PKCSObjectIdentifiers.sha256WithRSAEncryption;
      algorithmIdentifier = new AlgorithmIdentifier(aSN1ObjectIdentifier, (ASN1Encodable)DERNull.INSTANCE);
      null = new byte[1];
    } 
    ASN1InputStream aSN1InputStream = new ASN1InputStream(stringBuilder.getEncoded());
    try {
      SubjectPublicKeyInfo subjectPublicKeyInfo = SubjectPublicKeyInfo.getInstance(aSN1InputStream.readObject());
      v3TBSCertificateGenerator.setSubjectPublicKeyInfo(subjectPublicKeyInfo);
      aSN1InputStream.close();
      v3TBSCertificateGenerator.setSerialNumber(new ASN1Integer(this.mSpec.getCertificateSerialNumber()));
      KeyGenParameterSpec keyGenParameterSpec = this.mSpec;
      X509Principal x509Principal = new X509Principal(keyGenParameterSpec.getCertificateSubject().getEncoded());
      v3TBSCertificateGenerator.setSubject((X509Name)x509Principal);
      v3TBSCertificateGenerator.setIssuer((X509Name)x509Principal);
      v3TBSCertificateGenerator.setStartDate(new Time(this.mSpec.getCertificateNotBefore()));
      v3TBSCertificateGenerator.setEndDate(new Time(this.mSpec.getCertificateNotAfter()));
      v3TBSCertificateGenerator.setSignature(algorithmIdentifier);
      tBSCertificate = v3TBSCertificateGenerator.generateTBSCertificate();
      ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
      aSN1EncodableVector.add((ASN1Encodable)tBSCertificate);
      aSN1EncodableVector.add((ASN1Encodable)algorithmIdentifier);
      return (X509Certificate)new X509CertificateObject(Certificate.getInstance(new DERSequence(aSN1EncodableVector)));
    } finally {
      try {
        tBSCertificate.close();
      } finally {
        tBSCertificate = null;
      } 
    } 
  }
  
  private static int getDefaultKeySize(int paramInt) {
    if (paramInt != 1) {
      if (paramInt == 3)
        return 256; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported algorithm: ");
      stringBuilder.append(paramInt);
      throw new ProviderException(stringBuilder.toString());
    } 
    return 2048;
  }
  
  private static void checkValidKeySize(int paramInt1, int paramInt2, boolean paramBoolean) throws InvalidAlgorithmParameterException {
    if (paramInt1 != 1) {
      if (paramInt1 == 3) {
        if (!paramBoolean || paramInt2 == 256) {
          if (!SUPPORTED_EC_NIST_CURVE_SIZES.contains(Integer.valueOf(paramInt2))) {
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Unsupported EC key size: ");
            stringBuilder2.append(paramInt2);
            stringBuilder2.append(" bits. Supported: ");
            stringBuilder2.append(SUPPORTED_EC_NIST_CURVE_SIZES);
            throw new InvalidAlgorithmParameterException(stringBuilder2.toString());
          } 
          return;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Unsupported StrongBox EC key size: ");
        stringBuilder1.append(paramInt2);
        stringBuilder1.append(" bits. Supported: 256");
        throw new InvalidAlgorithmParameterException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported algorithm: ");
      stringBuilder.append(paramInt1);
      throw new ProviderException(stringBuilder.toString());
    } 
    if (paramInt2 < 512 || paramInt2 > 8192)
      throw new InvalidAlgorithmParameterException("RSA key size must be >= 512 and <= 8192"); 
  }
  
  private static String getCertificateSignatureAlgorithm(int paramInt1, int paramInt2, KeyGenParameterSpec paramKeyGenParameterSpec) {
    StringBuilder stringBuilder2;
    if ((paramKeyGenParameterSpec.getPurposes() & 0x4) == 0)
      return null; 
    if (paramKeyGenParameterSpec.isUserAuthenticationRequired())
      return null; 
    if (!paramKeyGenParameterSpec.isDigestsSpecified())
      return null; 
    if (paramInt1 != 1) {
      if (paramInt1 == 3) {
        String[] arrayOfString5 = paramKeyGenParameterSpec.getDigests();
        String[] arrayOfString4 = AndroidKeyStoreBCWorkaroundProvider.getSupportedEcdsaSignatureDigests();
        Set<Integer> set1 = getAvailableKeymasterSignatureDigests(arrayOfString5, arrayOfString4);
        int k = -1;
        int m = -1;
        Iterator<Integer> iterator1 = set1.iterator();
        while (true) {
          paramInt1 = k;
          if (iterator1.hasNext()) {
            int i2, n = ((Integer)iterator1.next()).intValue();
            int i1 = KeymasterUtils.getDigestOutputSizeBits(n);
            if (i1 == paramInt2) {
              paramInt1 = n;
              break;
            } 
            if (k == -1) {
              paramInt1 = n;
              i2 = i1;
            } else if (m < paramInt2) {
              paramInt1 = k;
              i2 = m;
              if (i1 > m) {
                paramInt1 = n;
                i2 = i1;
              } 
            } else {
              paramInt1 = k;
              i2 = m;
              if (i1 < m) {
                paramInt1 = k;
                i2 = m;
                if (i1 >= paramInt2) {
                  paramInt1 = n;
                  i2 = i1;
                } 
              } 
            } 
            k = paramInt1;
            m = i2;
            continue;
          } 
          break;
        } 
        if (paramInt1 == -1)
          return null; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(KeyProperties.Digest.fromKeymasterToSignatureAlgorithmDigest(paramInt1));
        stringBuilder.append("WithECDSA");
        return stringBuilder.toString();
      } 
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Unsupported algorithm: ");
      stringBuilder2.append(paramInt1);
      throw new ProviderException(stringBuilder2.toString());
    } 
    String[] arrayOfString3 = stringBuilder2.getSignaturePaddings();
    int[] arrayOfInt = KeyProperties.SignaturePadding.allToKeymaster(arrayOfString3);
    boolean bool = ArrayUtils.contains(arrayOfInt, 5);
    if (!bool)
      return null; 
    String[] arrayOfString1 = stringBuilder2.getDigests();
    String[] arrayOfString2 = AndroidKeyStoreBCWorkaroundProvider.getSupportedEcdsaSignatureDigests();
    Set<Integer> set = getAvailableKeymasterSignatureDigests(arrayOfString1, arrayOfString2);
    int j = -1;
    int i = -1;
    for (Iterator<Integer> iterator = set.iterator(); iterator.hasNext(); ) {
      int k = ((Integer)iterator.next()).intValue();
      int m = KeymasterUtils.getDigestOutputSizeBits(k);
      if (m > paramInt2 - 240)
        continue; 
      if (j == -1) {
        j = k;
        paramInt1 = m;
      } else {
        paramInt1 = i;
        if (m > i) {
          j = k;
          paramInt1 = m;
        } 
      } 
      i = paramInt1;
    } 
    if (j == -1)
      return null; 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(KeyProperties.Digest.fromKeymasterToSignatureAlgorithmDigest(j));
    stringBuilder1.append("WithRSA");
    return stringBuilder1.toString();
  }
  
  private static Set<Integer> getAvailableKeymasterSignatureDigests(String[] paramArrayOfString1, String[] paramArrayOfString2) {
    HashSet<Integer> hashSet2 = new HashSet();
    int i, j, k;
    for (int[] arrayOfInt1 = KeyProperties.Digest.allToKeymaster(paramArrayOfString1); k < i; ) {
      int m = arrayOfInt1[k];
      hashSet2.add(Integer.valueOf(m));
      k++;
    } 
    HashSet<Integer> hashSet1 = new HashSet();
    for (int[] arrayOfInt2 = KeyProperties.Digest.allToKeymaster(paramArrayOfString2); k < i; ) {
      j = arrayOfInt2[k];
      hashSet1.add(Integer.valueOf(j));
      k++;
    } 
    hashSet1 = new HashSet<>(hashSet1);
    hashSet1.retainAll(hashSet2);
    return hashSet1;
  }
}
