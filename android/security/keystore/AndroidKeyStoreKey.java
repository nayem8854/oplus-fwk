package android.security.keystore;

import java.security.Key;

public class AndroidKeyStoreKey implements Key {
  private final String mAlgorithm;
  
  private final String mAlias;
  
  private final int mUid;
  
  public AndroidKeyStoreKey(String paramString1, int paramInt, String paramString2) {
    this.mAlias = paramString1;
    this.mUid = paramInt;
    this.mAlgorithm = paramString2;
  }
  
  String getAlias() {
    return this.mAlias;
  }
  
  int getUid() {
    return this.mUid;
  }
  
  public String getAlgorithm() {
    return this.mAlgorithm;
  }
  
  public String getFormat() {
    return null;
  }
  
  public byte[] getEncoded() {
    return null;
  }
  
  public int hashCode() {
    int j;
    String str = this.mAlgorithm;
    int i = 0;
    if (str == null) {
      j = 0;
    } else {
      j = str.hashCode();
    } 
    str = this.mAlias;
    if (str != null)
      i = str.hashCode(); 
    int k = this.mUid;
    return ((1 * 31 + j) * 31 + i) * 31 + k;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    String str = this.mAlgorithm;
    if (str == null) {
      if (((AndroidKeyStoreKey)paramObject).mAlgorithm != null)
        return false; 
    } else if (!str.equals(((AndroidKeyStoreKey)paramObject).mAlgorithm)) {
      return false;
    } 
    str = this.mAlias;
    if (str == null) {
      if (((AndroidKeyStoreKey)paramObject).mAlias != null)
        return false; 
    } else if (!str.equals(((AndroidKeyStoreKey)paramObject).mAlias)) {
      return false;
    } 
    if (this.mUid != ((AndroidKeyStoreKey)paramObject).mUid)
      return false; 
    return true;
  }
}
