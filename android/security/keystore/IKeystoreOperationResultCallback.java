package android.security.keystore;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.security.keymaster.OperationResult;

public interface IKeystoreOperationResultCallback extends IInterface {
  void onFinished(OperationResult paramOperationResult) throws RemoteException;
  
  class Default implements IKeystoreOperationResultCallback {
    public void onFinished(OperationResult param1OperationResult) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IKeystoreOperationResultCallback {
    private static final String DESCRIPTOR = "android.security.keystore.IKeystoreOperationResultCallback";
    
    static final int TRANSACTION_onFinished = 1;
    
    public Stub() {
      attachInterface(this, "android.security.keystore.IKeystoreOperationResultCallback");
    }
    
    public static IKeystoreOperationResultCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.keystore.IKeystoreOperationResultCallback");
      if (iInterface != null && iInterface instanceof IKeystoreOperationResultCallback)
        return (IKeystoreOperationResultCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onFinished";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.security.keystore.IKeystoreOperationResultCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.security.keystore.IKeystoreOperationResultCallback");
      if (param1Parcel1.readInt() != 0) {
        OperationResult operationResult = OperationResult.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onFinished((OperationResult)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IKeystoreOperationResultCallback {
      public static IKeystoreOperationResultCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.keystore.IKeystoreOperationResultCallback";
      }
      
      public void onFinished(OperationResult param2OperationResult) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.security.keystore.IKeystoreOperationResultCallback");
          if (param2OperationResult != null) {
            parcel.writeInt(1);
            param2OperationResult.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IKeystoreOperationResultCallback.Stub.getDefaultImpl() != null) {
            IKeystoreOperationResultCallback.Stub.getDefaultImpl().onFinished(param2OperationResult);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IKeystoreOperationResultCallback param1IKeystoreOperationResultCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IKeystoreOperationResultCallback != null) {
          Proxy.sDefaultImpl = param1IKeystoreOperationResultCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IKeystoreOperationResultCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
