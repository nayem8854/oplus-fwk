package android.security.keystore;

import java.security.KeyStore;

class AndroidKeyStoreLoadStoreParameter implements KeyStore.LoadStoreParameter {
  private final int mUid;
  
  AndroidKeyStoreLoadStoreParameter(int paramInt) {
    this.mUid = paramInt;
  }
  
  public KeyStore.ProtectionParameter getProtectionParameter() {
    return null;
  }
  
  int getUid() {
    return this.mUid;
  }
}
