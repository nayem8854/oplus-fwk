package android.security.keystore;

import android.os.Parcel;
import android.os.Parcelable;

public class KeystoreResponse implements Parcelable {
  public static final Parcelable.Creator<KeystoreResponse> CREATOR = new Parcelable.Creator<KeystoreResponse>() {
      public KeystoreResponse createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        String str = param1Parcel.readString();
        return new KeystoreResponse(i, str);
      }
      
      public KeystoreResponse[] newArray(int param1Int) {
        return new KeystoreResponse[param1Int];
      }
    };
  
  public final int error_code_;
  
  public final String error_msg_;
  
  protected KeystoreResponse(int paramInt, String paramString) {
    this.error_code_ = paramInt;
    this.error_msg_ = paramString;
  }
  
  public final int getErrorCode() {
    return this.error_code_;
  }
  
  public final String getErrorMessage() {
    return this.error_msg_;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.error_code_);
    paramParcel.writeString(this.error_msg_);
  }
}
