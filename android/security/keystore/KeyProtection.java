package android.security.keystore;

import java.security.KeyStore;
import java.util.Date;

public final class KeyProtection implements KeyStore.ProtectionParameter, UserAuthArgs {
  private final String[] mBlockModes;
  
  private final long mBoundToSecureUserId;
  
  private final boolean mCriticalToDeviceEncryption;
  
  private final String[] mDigests;
  
  private final String[] mEncryptionPaddings;
  
  private final boolean mInvalidatedByBiometricEnrollment;
  
  private final boolean mIsStrongBoxBacked;
  
  private final Date mKeyValidityForConsumptionEnd;
  
  private final Date mKeyValidityForOriginationEnd;
  
  private final Date mKeyValidityStart;
  
  private final int mPurposes;
  
  private final boolean mRandomizedEncryptionRequired;
  
  private final String[] mSignaturePaddings;
  
  private final boolean mUnlockedDeviceRequired;
  
  private final boolean mUserAuthenticationRequired;
  
  private final int mUserAuthenticationType;
  
  private final boolean mUserAuthenticationValidWhileOnBody;
  
  private final int mUserAuthenticationValidityDurationSeconds;
  
  private final boolean mUserConfirmationRequired;
  
  private final boolean mUserPresenceRequred;
  
  private KeyProtection(Date paramDate1, Date paramDate2, Date paramDate3, int paramInt1, String[] paramArrayOfString1, String[] paramArrayOfString2, String[] paramArrayOfString3, String[] paramArrayOfString4, boolean paramBoolean1, boolean paramBoolean2, int paramInt2, int paramInt3, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, long paramLong, boolean paramBoolean6, boolean paramBoolean7, boolean paramBoolean8, boolean paramBoolean9) {
    this.mKeyValidityStart = Utils.cloneIfNotNull(paramDate1);
    this.mKeyValidityForOriginationEnd = Utils.cloneIfNotNull(paramDate2);
    this.mKeyValidityForConsumptionEnd = Utils.cloneIfNotNull(paramDate3);
    this.mPurposes = paramInt1;
    this.mEncryptionPaddings = ArrayUtils.cloneIfNotEmpty(ArrayUtils.nullToEmpty(paramArrayOfString1));
    this.mSignaturePaddings = ArrayUtils.cloneIfNotEmpty(ArrayUtils.nullToEmpty(paramArrayOfString2));
    this.mDigests = ArrayUtils.cloneIfNotEmpty(paramArrayOfString3);
    this.mBlockModes = ArrayUtils.cloneIfNotEmpty(ArrayUtils.nullToEmpty(paramArrayOfString4));
    this.mRandomizedEncryptionRequired = paramBoolean1;
    this.mUserAuthenticationRequired = paramBoolean2;
    this.mUserAuthenticationType = paramInt2;
    this.mUserAuthenticationValidityDurationSeconds = paramInt3;
    this.mUserPresenceRequred = paramBoolean3;
    this.mUserAuthenticationValidWhileOnBody = paramBoolean4;
    this.mInvalidatedByBiometricEnrollment = paramBoolean5;
    this.mBoundToSecureUserId = paramLong;
    this.mCriticalToDeviceEncryption = paramBoolean6;
    this.mUserConfirmationRequired = paramBoolean7;
    this.mUnlockedDeviceRequired = paramBoolean8;
    this.mIsStrongBoxBacked = paramBoolean9;
  }
  
  public Date getKeyValidityStart() {
    return Utils.cloneIfNotNull(this.mKeyValidityStart);
  }
  
  public Date getKeyValidityForConsumptionEnd() {
    return Utils.cloneIfNotNull(this.mKeyValidityForConsumptionEnd);
  }
  
  public Date getKeyValidityForOriginationEnd() {
    return Utils.cloneIfNotNull(this.mKeyValidityForOriginationEnd);
  }
  
  public int getPurposes() {
    return this.mPurposes;
  }
  
  public String[] getEncryptionPaddings() {
    return ArrayUtils.cloneIfNotEmpty(this.mEncryptionPaddings);
  }
  
  public String[] getSignaturePaddings() {
    return ArrayUtils.cloneIfNotEmpty(this.mSignaturePaddings);
  }
  
  public String[] getDigests() {
    String[] arrayOfString = this.mDigests;
    if (arrayOfString != null)
      return ArrayUtils.cloneIfNotEmpty(arrayOfString); 
    throw new IllegalStateException("Digests not specified");
  }
  
  public boolean isDigestsSpecified() {
    boolean bool;
    if (this.mDigests != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String[] getBlockModes() {
    return ArrayUtils.cloneIfNotEmpty(this.mBlockModes);
  }
  
  public boolean isRandomizedEncryptionRequired() {
    return this.mRandomizedEncryptionRequired;
  }
  
  public boolean isUserAuthenticationRequired() {
    return this.mUserAuthenticationRequired;
  }
  
  public boolean isUserConfirmationRequired() {
    return this.mUserConfirmationRequired;
  }
  
  public int getUserAuthenticationType() {
    return this.mUserAuthenticationType;
  }
  
  public int getUserAuthenticationValidityDurationSeconds() {
    return this.mUserAuthenticationValidityDurationSeconds;
  }
  
  public boolean isUserPresenceRequired() {
    return this.mUserPresenceRequred;
  }
  
  public boolean isUserAuthenticationValidWhileOnBody() {
    return this.mUserAuthenticationValidWhileOnBody;
  }
  
  public boolean isInvalidatedByBiometricEnrollment() {
    return this.mInvalidatedByBiometricEnrollment;
  }
  
  public long getBoundToSpecificSecureUserId() {
    return this.mBoundToSecureUserId;
  }
  
  public boolean isCriticalToDeviceEncryption() {
    return this.mCriticalToDeviceEncryption;
  }
  
  public boolean isUnlockedDeviceRequired() {
    return this.mUnlockedDeviceRequired;
  }
  
  public boolean isStrongBoxBacked() {
    return this.mIsStrongBoxBacked;
  }
  
  class Builder {
    private boolean mRandomizedEncryptionRequired = true;
    
    private int mUserAuthenticationValidityDurationSeconds = 0;
    
    private int mUserAuthenticationType = 2;
    
    private boolean mUserPresenceRequired = false;
    
    private boolean mInvalidatedByBiometricEnrollment = true;
    
    private boolean mUnlockedDeviceRequired = false;
    
    private long mBoundToSecureUserId = 0L;
    
    private boolean mCriticalToDeviceEncryption = false;
    
    private boolean mIsStrongBoxBacked = false;
    
    private String[] mBlockModes;
    
    private String[] mDigests;
    
    private String[] mEncryptionPaddings;
    
    private Date mKeyValidityForConsumptionEnd;
    
    private Date mKeyValidityForOriginationEnd;
    
    private Date mKeyValidityStart;
    
    private int mPurposes;
    
    private String[] mSignaturePaddings;
    
    private boolean mUserAuthenticationRequired;
    
    private boolean mUserAuthenticationValidWhileOnBody;
    
    private boolean mUserConfirmationRequired;
    
    public Builder(KeyProtection this$0) {
      this.mPurposes = this$0;
    }
    
    public Builder setKeyValidityStart(Date param1Date) {
      this.mKeyValidityStart = Utils.cloneIfNotNull(param1Date);
      return this;
    }
    
    public Builder setKeyValidityEnd(Date param1Date) {
      setKeyValidityForOriginationEnd(param1Date);
      setKeyValidityForConsumptionEnd(param1Date);
      return this;
    }
    
    public Builder setKeyValidityForOriginationEnd(Date param1Date) {
      this.mKeyValidityForOriginationEnd = Utils.cloneIfNotNull(param1Date);
      return this;
    }
    
    public Builder setKeyValidityForConsumptionEnd(Date param1Date) {
      this.mKeyValidityForConsumptionEnd = Utils.cloneIfNotNull(param1Date);
      return this;
    }
    
    public Builder setEncryptionPaddings(String... param1VarArgs) {
      this.mEncryptionPaddings = ArrayUtils.cloneIfNotEmpty(param1VarArgs);
      return this;
    }
    
    public Builder setSignaturePaddings(String... param1VarArgs) {
      this.mSignaturePaddings = ArrayUtils.cloneIfNotEmpty(param1VarArgs);
      return this;
    }
    
    public Builder setDigests(String... param1VarArgs) {
      this.mDigests = ArrayUtils.cloneIfNotEmpty(param1VarArgs);
      return this;
    }
    
    public Builder setBlockModes(String... param1VarArgs) {
      this.mBlockModes = ArrayUtils.cloneIfNotEmpty(param1VarArgs);
      return this;
    }
    
    public Builder setRandomizedEncryptionRequired(boolean param1Boolean) {
      this.mRandomizedEncryptionRequired = param1Boolean;
      return this;
    }
    
    public Builder setUserAuthenticationRequired(boolean param1Boolean) {
      this.mUserAuthenticationRequired = param1Boolean;
      return this;
    }
    
    public Builder setUserConfirmationRequired(boolean param1Boolean) {
      this.mUserConfirmationRequired = param1Boolean;
      return this;
    }
    
    @Deprecated
    public Builder setUserAuthenticationValidityDurationSeconds(int param1Int) {
      if (param1Int >= -1) {
        if (param1Int == -1)
          return setUserAuthenticationParameters(0, 2); 
        return setUserAuthenticationParameters(param1Int, 3);
      } 
      throw new IllegalArgumentException("seconds must be -1 or larger");
    }
    
    public Builder setUserAuthenticationParameters(int param1Int1, int param1Int2) {
      if (param1Int1 >= 0) {
        this.mUserAuthenticationValidityDurationSeconds = param1Int1;
        this.mUserAuthenticationType = param1Int2;
        return this;
      } 
      throw new IllegalArgumentException("timeout must be 0 or larger");
    }
    
    public Builder setUserPresenceRequired(boolean param1Boolean) {
      this.mUserPresenceRequired = param1Boolean;
      return this;
    }
    
    public Builder setUserAuthenticationValidWhileOnBody(boolean param1Boolean) {
      this.mUserAuthenticationValidWhileOnBody = param1Boolean;
      return this;
    }
    
    public Builder setInvalidatedByBiometricEnrollment(boolean param1Boolean) {
      this.mInvalidatedByBiometricEnrollment = param1Boolean;
      return this;
    }
    
    public Builder setBoundToSpecificSecureUserId(long param1Long) {
      this.mBoundToSecureUserId = param1Long;
      return this;
    }
    
    public Builder setCriticalToDeviceEncryption(boolean param1Boolean) {
      this.mCriticalToDeviceEncryption = param1Boolean;
      return this;
    }
    
    public Builder setUnlockedDeviceRequired(boolean param1Boolean) {
      this.mUnlockedDeviceRequired = param1Boolean;
      return this;
    }
    
    public Builder setIsStrongBoxBacked(boolean param1Boolean) {
      this.mIsStrongBoxBacked = param1Boolean;
      return this;
    }
    
    public KeyProtection build() {
      return new KeyProtection(this.mKeyValidityStart, this.mKeyValidityForOriginationEnd, this.mKeyValidityForConsumptionEnd, this.mPurposes, this.mEncryptionPaddings, this.mSignaturePaddings, this.mDigests, this.mBlockModes, this.mRandomizedEncryptionRequired, this.mUserAuthenticationRequired, this.mUserAuthenticationType, this.mUserAuthenticationValidityDurationSeconds, this.mUserPresenceRequired, this.mUserAuthenticationValidWhileOnBody, this.mInvalidatedByBiometricEnrollment, this.mBoundToSecureUserId, this.mCriticalToDeviceEncryption, this.mUserConfirmationRequired, this.mUnlockedDeviceRequired, this.mIsStrongBoxBacked);
    }
  }
}
