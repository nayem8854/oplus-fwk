package android.security.keystore;

import android.os.IBinder;
import android.security.KeyStore;
import android.security.KeyStoreException;
import android.security.keymaster.KeymasterArguments;
import android.security.keymaster.OperationResult;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.ProviderException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidParameterSpecException;
import java.util.Arrays;
import javax.crypto.spec.GCMParameterSpec;
import libcore.util.EmptyArray;

abstract class AndroidKeyStoreAuthenticatedAESCipherSpi extends AndroidKeyStoreCipherSpiBase {
  private static final int BLOCK_SIZE_BYTES = 16;
  
  private byte[] mIv;
  
  private boolean mIvHasBeenUsed;
  
  private final int mKeymasterBlockMode;
  
  private final int mKeymasterPadding;
  
  class GCM extends AndroidKeyStoreAuthenticatedAESCipherSpi {
    private static final int DEFAULT_TAG_LENGTH_BITS = 128;
    
    private static final int IV_LENGTH_BYTES = 12;
    
    private static final int MAX_SUPPORTED_TAG_LENGTH_BITS = 128;
    
    static final int MIN_SUPPORTED_TAG_LENGTH_BITS = 96;
    
    private int mTagLengthBits = 128;
    
    GCM(AndroidKeyStoreAuthenticatedAESCipherSpi this$0) {
      super(32, this$0);
    }
    
    protected final void resetAll() {
      this.mTagLengthBits = 128;
      super.resetAll();
    }
    
    protected final void resetWhilePreservingInitState() {
      super.resetWhilePreservingInitState();
    }
    
    protected final void initAlgorithmSpecificParameters() throws InvalidKeyException {
      if (isEncrypting())
        return; 
      throw new InvalidKeyException("IV required when decrypting. Use IvParameterSpec or AlgorithmParameters to provide it.");
    }
    
    protected final void initAlgorithmSpecificParameters(AlgorithmParameterSpec param1AlgorithmParameterSpec) throws InvalidAlgorithmParameterException {
      if (param1AlgorithmParameterSpec == null) {
        if (isEncrypting())
          return; 
        throw new InvalidAlgorithmParameterException("GCMParameterSpec must be provided when decrypting");
      } 
      if (param1AlgorithmParameterSpec instanceof GCMParameterSpec) {
        GCMParameterSpec gCMParameterSpec = (GCMParameterSpec)param1AlgorithmParameterSpec;
        byte[] arrayOfByte = gCMParameterSpec.getIV();
        if (arrayOfByte != null) {
          StringBuilder stringBuilder1;
          if (arrayOfByte.length == 12) {
            int i = gCMParameterSpec.getTLen();
            if (i >= 96 && i <= 128 && i % 8 == 0) {
              setIv(arrayOfByte);
              this.mTagLengthBits = i;
              return;
            } 
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Unsupported tag length: ");
            stringBuilder1.append(i);
            stringBuilder1.append(" bits. Supported lengths: 96, 104, 112, 120, 128");
            throw new InvalidAlgorithmParameterException(stringBuilder1.toString());
          } 
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Unsupported IV length: ");
          stringBuilder2.append(stringBuilder1.length);
          stringBuilder2.append(" bytes. Only ");
          stringBuilder2.append(12);
          stringBuilder2.append(" bytes long IV supported");
          throw new InvalidAlgorithmParameterException(stringBuilder2.toString());
        } 
        throw new InvalidAlgorithmParameterException("Null IV in GCMParameterSpec");
      } 
      throw new InvalidAlgorithmParameterException("Only GCMParameterSpec supported");
    }
    
    protected final void initAlgorithmSpecificParameters(AlgorithmParameters param1AlgorithmParameters) throws InvalidAlgorithmParameterException {
      if (param1AlgorithmParameters == null) {
        if (isEncrypting())
          return; 
        throw new InvalidAlgorithmParameterException("IV required when decrypting. Use GCMParameterSpec or GCM AlgorithmParameters to provide it.");
      } 
      if ("GCM".equalsIgnoreCase(param1AlgorithmParameters.getAlgorithm()))
        try {
          GCMParameterSpec gCMParameterSpec = param1AlgorithmParameters.<GCMParameterSpec>getParameterSpec(GCMParameterSpec.class);
          initAlgorithmSpecificParameters(gCMParameterSpec);
          return;
        } catch (InvalidParameterSpecException invalidParameterSpecException) {
          if (isEncrypting()) {
            setIv((byte[])null);
            return;
          } 
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("IV and tag length required when decrypting, but not found in parameters: ");
          stringBuilder1.append(param1AlgorithmParameters);
          throw new InvalidAlgorithmParameterException(stringBuilder1.toString(), invalidParameterSpecException);
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported AlgorithmParameters algorithm: ");
      stringBuilder.append(param1AlgorithmParameters.getAlgorithm());
      stringBuilder.append(". Supported: GCM");
      throw new InvalidAlgorithmParameterException(stringBuilder.toString());
    }
    
    protected final AlgorithmParameters engineGetParameters() {
      byte[] arrayOfByte = getIv();
      if (arrayOfByte != null && arrayOfByte.length > 0)
        try {
          AlgorithmParameters algorithmParameters = AlgorithmParameters.getInstance("GCM");
          GCMParameterSpec gCMParameterSpec = new GCMParameterSpec();
          this(this.mTagLengthBits, arrayOfByte);
          algorithmParameters.init(gCMParameterSpec);
          return algorithmParameters;
        } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
          throw new ProviderException("Failed to obtain GCM AlgorithmParameters", noSuchAlgorithmException);
        } catch (InvalidParameterSpecException invalidParameterSpecException) {
          throw new ProviderException("Failed to initialize GCM AlgorithmParameters", invalidParameterSpecException);
        }  
      return null;
    }
    
    protected KeyStoreCryptoOperationStreamer createMainDataStreamer(KeyStore param1KeyStore, IBinder param1IBinder) {
      KeyStoreCryptoOperationChunkedStreamer keyStoreCryptoOperationChunkedStreamer = new KeyStoreCryptoOperationChunkedStreamer(new KeyStoreCryptoOperationChunkedStreamer.MainDataStream(param1KeyStore, param1IBinder), 0);
      if (isEncrypting())
        return keyStoreCryptoOperationChunkedStreamer; 
      return new AndroidKeyStoreAuthenticatedAESCipherSpi.BufferAllOutputUntilDoFinalStreamer();
    }
    
    protected final KeyStoreCryptoOperationStreamer createAdditionalAuthenticationDataStreamer(KeyStore param1KeyStore, IBinder param1IBinder) {
      return new KeyStoreCryptoOperationChunkedStreamer(new AndroidKeyStoreAuthenticatedAESCipherSpi.AdditionalAuthenticationDataStream(param1IBinder), 0);
    }
    
    protected final int getAdditionalEntropyAmountForBegin() {
      if (getIv() == null && isEncrypting())
        return 12; 
      return 0;
    }
    
    protected final int getAdditionalEntropyAmountForFinish() {
      return 0;
    }
    
    protected final void addAlgorithmSpecificParametersToBegin(KeymasterArguments param1KeymasterArguments) {
      super.addAlgorithmSpecificParametersToBegin(param1KeymasterArguments);
      param1KeymasterArguments.addUnsignedInt(805307371, this.mTagLengthBits);
    }
    
    protected final int getTagLengthBits() {
      return this.mTagLengthBits;
    }
    
    class NoPadding extends GCM {
      public NoPadding() {
        super(1);
      }
      
      protected final int engineGetOutputSize(int param2Int) {
        long l;
        int i = (getTagLengthBits() + 7) / 8;
        if (isEncrypting()) {
          l = getConsumedInputSizeBytes() - getProducedOutputSizeBytes() + param2Int + i;
        } else {
          l = getConsumedInputSizeBytes() - getProducedOutputSizeBytes() + param2Int - i;
        } 
        if (l < 0L)
          return 0; 
        if (l > 2147483647L)
          return Integer.MAX_VALUE; 
        return (int)l;
      }
    }
  }
  
  AndroidKeyStoreAuthenticatedAESCipherSpi(int paramInt1, int paramInt2) {
    this.mKeymasterBlockMode = paramInt1;
    this.mKeymasterPadding = paramInt2;
  }
  
  protected void resetAll() {
    this.mIv = null;
    this.mIvHasBeenUsed = false;
    super.resetAll();
  }
  
  protected final void initKey(int paramInt, Key paramKey) throws InvalidKeyException {
    String str;
    if (!(paramKey instanceof AndroidKeyStoreSecretKey)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Unsupported key: ");
      if (paramKey != null) {
        str = paramKey.getClass().getName();
      } else {
        str = "null";
      } 
      stringBuilder1.append(str);
      throw new InvalidKeyException(stringBuilder1.toString());
    } 
    if ("AES".equalsIgnoreCase(str.getAlgorithm())) {
      setKey((AndroidKeyStoreSecretKey)str);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported key algorithm: ");
    stringBuilder.append(str.getAlgorithm());
    stringBuilder.append(". Only ");
    stringBuilder.append("AES");
    stringBuilder.append(" supported");
    throw new InvalidKeyException(stringBuilder.toString());
  }
  
  protected void addAlgorithmSpecificParametersToBegin(KeymasterArguments paramKeymasterArguments) {
    if (!isEncrypting() || !this.mIvHasBeenUsed) {
      paramKeymasterArguments.addEnum(268435458, 32);
      paramKeymasterArguments.addEnum(536870916, this.mKeymasterBlockMode);
      paramKeymasterArguments.addEnum(536870918, this.mKeymasterPadding);
      byte[] arrayOfByte = this.mIv;
      if (arrayOfByte != null)
        paramKeymasterArguments.addBytes(-1879047191, arrayOfByte); 
      return;
    } 
    throw new IllegalStateException("IV has already been used. Reusing IV in encryption mode violates security best practices.");
  }
  
  protected final void loadAlgorithmSpecificParametersFromBeginResult(KeymasterArguments paramKeymasterArguments) {
    this.mIvHasBeenUsed = true;
    byte[] arrayOfByte2 = paramKeymasterArguments.getBytes(-1879047191, null);
    byte[] arrayOfByte1 = arrayOfByte2;
    if (arrayOfByte2 != null) {
      arrayOfByte1 = arrayOfByte2;
      if (arrayOfByte2.length == 0)
        arrayOfByte1 = null; 
    } 
    arrayOfByte2 = this.mIv;
    if (arrayOfByte2 == null) {
      this.mIv = arrayOfByte1;
    } else if (arrayOfByte1 != null && !Arrays.equals(arrayOfByte1, arrayOfByte2)) {
      throw new ProviderException("IV in use differs from provided IV");
    } 
  }
  
  protected final int engineGetBlockSize() {
    return 16;
  }
  
  protected final byte[] engineGetIV() {
    return ArrayUtils.cloneIfNotEmpty(this.mIv);
  }
  
  protected void setIv(byte[] paramArrayOfbyte) {
    this.mIv = paramArrayOfbyte;
  }
  
  protected byte[] getIv() {
    return this.mIv;
  }
  
  class BufferAllOutputUntilDoFinalStreamer implements KeyStoreCryptoOperationStreamer {
    private ByteArrayOutputStream mBufferedOutput = new ByteArrayOutputStream();
    
    private final KeyStoreCryptoOperationStreamer mDelegate;
    
    private long mProducedOutputSizeBytes;
    
    private BufferAllOutputUntilDoFinalStreamer(AndroidKeyStoreAuthenticatedAESCipherSpi this$0) {
      this.mDelegate = (KeyStoreCryptoOperationStreamer)this$0;
    }
    
    public byte[] update(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws KeyStoreException {
      param1ArrayOfbyte = this.mDelegate.update(param1ArrayOfbyte, param1Int1, param1Int2);
      if (param1ArrayOfbyte != null)
        try {
          this.mBufferedOutput.write(param1ArrayOfbyte);
        } catch (IOException iOException) {
          throw new ProviderException("Failed to buffer output", iOException);
        }  
      return EmptyArray.BYTE;
    }
    
    public byte[] doFinal(byte[] param1ArrayOfbyte1, int param1Int1, int param1Int2, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3) throws KeyStoreException {
      param1ArrayOfbyte1 = this.mDelegate.doFinal(param1ArrayOfbyte1, param1Int1, param1Int2, param1ArrayOfbyte2, param1ArrayOfbyte3);
      if (param1ArrayOfbyte1 != null)
        try {
          this.mBufferedOutput.write(param1ArrayOfbyte1);
        } catch (IOException iOException) {
          throw new ProviderException("Failed to buffer output", iOException);
        }  
      param1ArrayOfbyte1 = this.mBufferedOutput.toByteArray();
      this.mBufferedOutput.reset();
      this.mProducedOutputSizeBytes += param1ArrayOfbyte1.length;
      return param1ArrayOfbyte1;
    }
    
    public long getConsumedInputSizeBytes() {
      return this.mDelegate.getConsumedInputSizeBytes();
    }
    
    public long getProducedOutputSizeBytes() {
      return this.mProducedOutputSizeBytes;
    }
  }
  
  class AdditionalAuthenticationDataStream implements KeyStoreCryptoOperationChunkedStreamer.Stream {
    private final KeyStore mKeyStore;
    
    private final IBinder mOperationToken;
    
    private AdditionalAuthenticationDataStream(AndroidKeyStoreAuthenticatedAESCipherSpi this$0, IBinder param1IBinder) {
      this.mKeyStore = (KeyStore)this$0;
      this.mOperationToken = param1IBinder;
    }
    
    public OperationResult update(byte[] param1ArrayOfbyte) {
      KeymasterArguments keymasterArguments = new KeymasterArguments();
      keymasterArguments.addBytes(-1879047192, param1ArrayOfbyte);
      OperationResult operationResult2 = this.mKeyStore.update(this.mOperationToken, keymasterArguments, null);
      OperationResult operationResult1 = operationResult2;
      if (operationResult2.resultCode == 1)
        operationResult1 = new OperationResult(operationResult2.resultCode, operationResult2.token, operationResult2.operationHandle, param1ArrayOfbyte.length, operationResult2.output, operationResult2.outParams); 
      return operationResult1;
    }
    
    public OperationResult finish(byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3) {
      if (param1ArrayOfbyte3 == null || param1ArrayOfbyte3.length <= 0)
        return new OperationResult(1, this.mOperationToken, 0L, 0, EmptyArray.BYTE, new KeymasterArguments()); 
      throw new ProviderException("AAD stream does not support additional entropy");
    }
  }
}
