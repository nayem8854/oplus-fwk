package android.security.keystore;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICredstoreTokenCallback extends IInterface {
  void onFinished(boolean paramBoolean, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws RemoteException;
  
  class Default implements ICredstoreTokenCallback {
    public void onFinished(boolean param1Boolean, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICredstoreTokenCallback {
    private static final String DESCRIPTOR = "android.security.keystore.ICredstoreTokenCallback";
    
    static final int TRANSACTION_onFinished = 1;
    
    public Stub() {
      attachInterface(this, "android.security.keystore.ICredstoreTokenCallback");
    }
    
    public static ICredstoreTokenCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.keystore.ICredstoreTokenCallback");
      if (iInterface != null && iInterface instanceof ICredstoreTokenCallback)
        return (ICredstoreTokenCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onFinished";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.security.keystore.ICredstoreTokenCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.security.keystore.ICredstoreTokenCallback");
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      byte[] arrayOfByte2 = param1Parcel1.createByteArray();
      byte[] arrayOfByte1 = param1Parcel1.createByteArray();
      onFinished(bool, arrayOfByte2, arrayOfByte1);
      return true;
    }
    
    private static class Proxy implements ICredstoreTokenCallback {
      public static ICredstoreTokenCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.keystore.ICredstoreTokenCallback";
      }
      
      public void onFinished(boolean param2Boolean, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.security.keystore.ICredstoreTokenCallback");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          parcel.writeByteArray(param2ArrayOfbyte1);
          parcel.writeByteArray(param2ArrayOfbyte2);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && ICredstoreTokenCallback.Stub.getDefaultImpl() != null) {
            ICredstoreTokenCallback.Stub.getDefaultImpl().onFinished(param2Boolean, param2ArrayOfbyte1, param2ArrayOfbyte2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICredstoreTokenCallback param1ICredstoreTokenCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICredstoreTokenCallback != null) {
          Proxy.sDefaultImpl = param1ICredstoreTokenCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICredstoreTokenCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
