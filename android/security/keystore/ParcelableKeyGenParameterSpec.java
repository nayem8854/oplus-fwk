package android.security.keystore;

import android.os.Parcel;
import android.os.Parcelable;
import java.math.BigInteger;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.Date;
import javax.security.auth.x500.X500Principal;

public final class ParcelableKeyGenParameterSpec implements Parcelable {
  private static final int ALGORITHM_PARAMETER_SPEC_EC = 3;
  
  private static final int ALGORITHM_PARAMETER_SPEC_NONE = 1;
  
  private static final int ALGORITHM_PARAMETER_SPEC_RSA = 2;
  
  public ParcelableKeyGenParameterSpec(KeyGenParameterSpec paramKeyGenParameterSpec) {
    this.mSpec = paramKeyGenParameterSpec;
  }
  
  public int describeContents() {
    return 0;
  }
  
  private static void writeOptionalDate(Parcel paramParcel, Date paramDate) {
    if (paramDate != null) {
      paramParcel.writeBoolean(true);
      paramParcel.writeLong(paramDate.getTime());
    } else {
      paramParcel.writeBoolean(false);
    } 
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mSpec.getKeystoreAlias());
    paramParcel.writeInt(this.mSpec.getPurposes());
    paramParcel.writeInt(this.mSpec.getUid());
    paramParcel.writeInt(this.mSpec.getKeySize());
    AlgorithmParameterSpec algorithmParameterSpec = this.mSpec.getAlgorithmParameterSpec();
    if (algorithmParameterSpec == null) {
      paramParcel.writeInt(1);
    } else if (algorithmParameterSpec instanceof RSAKeyGenParameterSpec) {
      algorithmParameterSpec = algorithmParameterSpec;
      paramParcel.writeInt(2);
      paramParcel.writeInt(algorithmParameterSpec.getKeysize());
      paramParcel.writeByteArray(algorithmParameterSpec.getPublicExponent().toByteArray());
    } else if (algorithmParameterSpec instanceof ECGenParameterSpec) {
      algorithmParameterSpec = algorithmParameterSpec;
      paramParcel.writeInt(3);
      paramParcel.writeString(algorithmParameterSpec.getName());
    } else {
      throw new IllegalArgumentException(String.format("Unknown algorithm parameter spec: %s", new Object[] { algorithmParameterSpec.getClass() }));
    } 
    paramParcel.writeByteArray(this.mSpec.getCertificateSubject().getEncoded());
    paramParcel.writeByteArray(this.mSpec.getCertificateSerialNumber().toByteArray());
    paramParcel.writeLong(this.mSpec.getCertificateNotBefore().getTime());
    paramParcel.writeLong(this.mSpec.getCertificateNotAfter().getTime());
    writeOptionalDate(paramParcel, this.mSpec.getKeyValidityStart());
    writeOptionalDate(paramParcel, this.mSpec.getKeyValidityForOriginationEnd());
    writeOptionalDate(paramParcel, this.mSpec.getKeyValidityForConsumptionEnd());
    if (this.mSpec.isDigestsSpecified()) {
      paramParcel.writeStringArray(this.mSpec.getDigests());
    } else {
      paramParcel.writeStringArray(null);
    } 
    paramParcel.writeStringArray(this.mSpec.getEncryptionPaddings());
    paramParcel.writeStringArray(this.mSpec.getSignaturePaddings());
    paramParcel.writeStringArray(this.mSpec.getBlockModes());
    paramParcel.writeBoolean(this.mSpec.isRandomizedEncryptionRequired());
    paramParcel.writeBoolean(this.mSpec.isUserAuthenticationRequired());
    paramParcel.writeInt(this.mSpec.getUserAuthenticationValidityDurationSeconds());
    paramParcel.writeInt(this.mSpec.getUserAuthenticationType());
    paramParcel.writeBoolean(this.mSpec.isUserPresenceRequired());
    paramParcel.writeByteArray(this.mSpec.getAttestationChallenge());
    paramParcel.writeBoolean(this.mSpec.isUniqueIdIncluded());
    paramParcel.writeBoolean(this.mSpec.isUserAuthenticationValidWhileOnBody());
    paramParcel.writeBoolean(this.mSpec.isInvalidatedByBiometricEnrollment());
    paramParcel.writeBoolean(this.mSpec.isStrongBoxBacked());
    paramParcel.writeBoolean(this.mSpec.isUserConfirmationRequired());
    paramParcel.writeBoolean(this.mSpec.isUnlockedDeviceRequired());
    paramParcel.writeBoolean(this.mSpec.isCriticalToDeviceEncryption());
  }
  
  private static Date readDateOrNull(Parcel paramParcel) {
    boolean bool = paramParcel.readBoolean();
    if (bool)
      return new Date(paramParcel.readLong()); 
    return null;
  }
  
  private ParcelableKeyGenParameterSpec(Parcel paramParcel) {
    ECGenParameterSpec eCGenParameterSpec;
    String str = paramParcel.readString();
    int i = paramParcel.readInt();
    int j = paramParcel.readInt();
    int k = paramParcel.readInt();
    int m = paramParcel.readInt();
    if (m == 1) {
      eCGenParameterSpec = null;
    } else if (m == 2) {
      m = paramParcel.readInt();
      BigInteger bigInteger1 = new BigInteger(paramParcel.createByteArray());
      RSAKeyGenParameterSpec rSAKeyGenParameterSpec = new RSAKeyGenParameterSpec(m, bigInteger1);
    } else if (m == 3) {
      String str1 = paramParcel.readString();
      eCGenParameterSpec = new ECGenParameterSpec(str1);
    } else {
      throw new IllegalArgumentException(String.format("Unknown algorithm parameter spec: %d", new Object[] { Integer.valueOf(m) }));
    } 
    X500Principal x500Principal = new X500Principal(paramParcel.createByteArray());
    BigInteger bigInteger = new BigInteger(paramParcel.createByteArray());
    Date date1 = new Date(paramParcel.readLong());
    Date date2 = new Date(paramParcel.readLong());
    Date date3 = readDateOrNull(paramParcel);
    Date date4 = readDateOrNull(paramParcel);
    Date date5 = readDateOrNull(paramParcel);
    String[] arrayOfString1 = paramParcel.createStringArray();
    String[] arrayOfString2 = paramParcel.createStringArray();
    String[] arrayOfString3 = paramParcel.createStringArray();
    String[] arrayOfString4 = paramParcel.createStringArray();
    boolean bool1 = paramParcel.readBoolean();
    boolean bool2 = paramParcel.readBoolean();
    m = paramParcel.readInt();
    int n = paramParcel.readInt();
    boolean bool3 = paramParcel.readBoolean();
    byte[] arrayOfByte = paramParcel.createByteArray();
    boolean bool4 = paramParcel.readBoolean();
    boolean bool5 = paramParcel.readBoolean();
    boolean bool6 = paramParcel.readBoolean();
    boolean bool7 = paramParcel.readBoolean();
    boolean bool8 = paramParcel.readBoolean();
    boolean bool9 = paramParcel.readBoolean();
    boolean bool10 = paramParcel.readBoolean();
    this.mSpec = new KeyGenParameterSpec(str, j, k, eCGenParameterSpec, x500Principal, bigInteger, date1, date2, date3, date4, date5, i, arrayOfString1, arrayOfString2, arrayOfString3, arrayOfString4, bool1, bool2, m, n, bool3, arrayOfByte, bool4, bool5, bool6, bool7, bool8, bool9, bool10);
  }
  
  public static final Parcelable.Creator<ParcelableKeyGenParameterSpec> CREATOR = new Parcelable.Creator<ParcelableKeyGenParameterSpec>() {
      public ParcelableKeyGenParameterSpec createFromParcel(Parcel param1Parcel) {
        return new ParcelableKeyGenParameterSpec(param1Parcel);
      }
      
      public ParcelableKeyGenParameterSpec[] newArray(int param1Int) {
        return new ParcelableKeyGenParameterSpec[param1Int];
      }
    };
  
  private final KeyGenParameterSpec mSpec;
  
  public KeyGenParameterSpec getSpec() {
    return this.mSpec;
  }
}
