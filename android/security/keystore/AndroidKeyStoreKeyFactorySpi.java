package android.security.keystore;

import android.security.KeyStore;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactorySpi;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class AndroidKeyStoreKeyFactorySpi extends KeyFactorySpi {
  private final KeyStore mKeyStore = KeyStore.getInstance();
  
  protected <T extends KeySpec> T engineGetKeySpec(Key paramKey, Class<T> paramClass) throws InvalidKeySpecException {
    if (paramKey != null) {
      StringBuilder stringBuilder1;
      if (paramKey instanceof AndroidKeyStorePrivateKey || paramKey instanceof AndroidKeyStorePublicKey) {
        if (paramClass != null) {
          KeyInfo keyInfo;
          X509EncodedKeySpec x509EncodedKeySpec;
          RSAPublicKeySpec rSAPublicKeySpec;
          StringBuilder stringBuilder;
          if (KeyInfo.class.equals(paramClass)) {
            if (paramKey instanceof AndroidKeyStorePrivateKey) {
              AndroidKeyStorePrivateKey androidKeyStorePrivateKey = (AndroidKeyStorePrivateKey)paramKey;
              String str1 = androidKeyStorePrivateKey.getAlias();
              if (str1.startsWith("USRPKEY_")) {
                String str2 = str1.substring("USRPKEY_".length());
                KeyStore keyStore = this.mKeyStore;
                int i = androidKeyStorePrivateKey.getUid();
                keyInfo = AndroidKeyStoreSecretKeyFactorySpi.getKeyInfo(keyStore, str2, str1, i);
                return (T)keyInfo;
              } 
              StringBuilder stringBuilder3 = new StringBuilder();
              stringBuilder3.append("Invalid key alias: ");
              stringBuilder3.append((String)keyInfo);
              throw new InvalidKeySpecException(stringBuilder3.toString());
            } 
            stringBuilder = new StringBuilder();
            stringBuilder.append("Unsupported key type: ");
            stringBuilder.append(keyInfo.getClass().getName());
            stringBuilder.append(". KeyInfo can be obtained only for Android Keystore private keys");
            throw new InvalidKeySpecException(stringBuilder.toString());
          } 
          if (X509EncodedKeySpec.class.equals(stringBuilder)) {
            if (keyInfo instanceof AndroidKeyStorePublicKey) {
              x509EncodedKeySpec = new X509EncodedKeySpec(((AndroidKeyStorePublicKey)keyInfo).getEncoded());
              return (T)x509EncodedKeySpec;
            } 
            stringBuilder = new StringBuilder();
            stringBuilder.append("Unsupported key type: ");
            stringBuilder.append(x509EncodedKeySpec.getClass().getName());
            stringBuilder.append(". X509EncodedKeySpec can be obtained only for Android Keystore public keys");
            throw new InvalidKeySpecException(stringBuilder.toString());
          } 
          if (PKCS8EncodedKeySpec.class.equals(stringBuilder)) {
            if (x509EncodedKeySpec instanceof AndroidKeyStorePrivateKey)
              throw new InvalidKeySpecException("Key material export of Android Keystore private keys is not supported"); 
            throw new InvalidKeySpecException("Cannot export key material of public key in PKCS#8 format. Only X.509 format (X509EncodedKeySpec) supported for public keys.");
          } 
          boolean bool = RSAPublicKeySpec.class.equals(stringBuilder);
          String str = "private";
          if (bool) {
            if (x509EncodedKeySpec instanceof AndroidKeyStoreRSAPublicKey) {
              AndroidKeyStoreRSAPublicKey androidKeyStoreRSAPublicKey = (AndroidKeyStoreRSAPublicKey)x509EncodedKeySpec;
              rSAPublicKeySpec = new RSAPublicKeySpec(androidKeyStoreRSAPublicKey.getModulus(), androidKeyStoreRSAPublicKey.getPublicExponent());
              return (T)rSAPublicKeySpec;
            } 
            stringBuilder = new StringBuilder();
            stringBuilder.append("Obtaining RSAPublicKeySpec not supported for ");
            stringBuilder.append(rSAPublicKeySpec.getAlgorithm());
            stringBuilder.append(" ");
            if (!(rSAPublicKeySpec instanceof AndroidKeyStorePrivateKey))
              str = "public"; 
            stringBuilder.append(str);
            stringBuilder.append(" key");
            throw new InvalidKeySpecException(stringBuilder.toString());
          } 
          if (ECPublicKeySpec.class.equals(stringBuilder)) {
            ECPublicKeySpec eCPublicKeySpec;
            if (rSAPublicKeySpec instanceof AndroidKeyStoreECPublicKey) {
              AndroidKeyStoreECPublicKey androidKeyStoreECPublicKey = (AndroidKeyStoreECPublicKey)rSAPublicKeySpec;
              eCPublicKeySpec = new ECPublicKeySpec(androidKeyStoreECPublicKey.getW(), androidKeyStoreECPublicKey.getParams());
              return (T)eCPublicKeySpec;
            } 
            stringBuilder = new StringBuilder();
            stringBuilder.append("Obtaining ECPublicKeySpec not supported for ");
            stringBuilder.append(eCPublicKeySpec.getAlgorithm());
            stringBuilder.append(" ");
            if (!(eCPublicKeySpec instanceof AndroidKeyStorePrivateKey))
              str = "public"; 
            stringBuilder.append(str);
            stringBuilder.append(" key");
            throw new InvalidKeySpecException(stringBuilder.toString());
          } 
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Unsupported key spec: ");
          stringBuilder1.append(stringBuilder.getName());
          throw new InvalidKeySpecException(stringBuilder1.toString());
        } 
        throw new InvalidKeySpecException("keySpecClass == null");
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Unsupported key type: ");
      stringBuilder2.append(stringBuilder1.getClass().getName());
      stringBuilder2.append(". This KeyFactory supports only Android Keystore asymmetric keys");
      throw new InvalidKeySpecException(stringBuilder2.toString());
    } 
    throw new InvalidKeySpecException("key == null");
  }
  
  protected PrivateKey engineGeneratePrivate(KeySpec paramKeySpec) throws InvalidKeySpecException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("To generate a key pair in Android Keystore, use KeyPairGenerator initialized with ");
    stringBuilder.append(KeyGenParameterSpec.class.getName());
    throw new InvalidKeySpecException(stringBuilder.toString());
  }
  
  protected PublicKey engineGeneratePublic(KeySpec paramKeySpec) throws InvalidKeySpecException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("To generate a key pair in Android Keystore, use KeyPairGenerator initialized with ");
    stringBuilder.append(KeyGenParameterSpec.class.getName());
    throw new InvalidKeySpecException(stringBuilder.toString());
  }
  
  protected Key engineTranslateKey(Key paramKey) throws InvalidKeyException {
    if (paramKey != null) {
      if (paramKey instanceof AndroidKeyStorePrivateKey || paramKey instanceof AndroidKeyStorePublicKey)
        return paramKey; 
      throw new InvalidKeyException("To import a key into Android Keystore, use KeyStore.setEntry");
    } 
    throw new InvalidKeyException("key == null");
  }
}
