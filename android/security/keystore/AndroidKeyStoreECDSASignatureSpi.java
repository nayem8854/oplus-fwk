package android.security.keystore;

import android.os.IBinder;
import android.security.KeyStore;
import android.security.KeyStoreException;
import android.security.keymaster.KeyCharacteristics;
import android.security.keymaster.KeymasterArguments;
import java.io.ByteArrayOutputStream;
import java.security.InvalidKeyException;
import libcore.util.EmptyArray;

abstract class AndroidKeyStoreECDSASignatureSpi extends AndroidKeyStoreSignatureSpiBase {
  class NONE extends AndroidKeyStoreECDSASignatureSpi {
    public NONE() {
      super(0);
    }
    
    protected KeyStoreCryptoOperationStreamer createMainDataStreamer(KeyStore param1KeyStore, IBinder param1IBinder) {
      null = super.createMainDataStreamer(param1KeyStore, param1IBinder);
      return new TruncateToFieldSizeMessageStreamer(getGroupSizeBits());
    }
    
    class TruncateToFieldSizeMessageStreamer implements KeyStoreCryptoOperationStreamer {
      private long mConsumedInputSizeBytes;
      
      private final KeyStoreCryptoOperationStreamer mDelegate;
      
      private final int mGroupSizeBits;
      
      private final ByteArrayOutputStream mInputBuffer = new ByteArrayOutputStream();
      
      private TruncateToFieldSizeMessageStreamer(AndroidKeyStoreECDSASignatureSpi.NONE this$0, int param2Int) {
        this.mDelegate = (KeyStoreCryptoOperationStreamer)this$0;
        this.mGroupSizeBits = param2Int;
      }
      
      public byte[] update(byte[] param2ArrayOfbyte, int param2Int1, int param2Int2) throws KeyStoreException {
        if (param2Int2 > 0) {
          this.mInputBuffer.write(param2ArrayOfbyte, param2Int1, param2Int2);
          this.mConsumedInputSizeBytes += param2Int2;
        } 
        return EmptyArray.BYTE;
      }
      
      public byte[] doFinal(byte[] param2ArrayOfbyte1, int param2Int1, int param2Int2, byte[] param2ArrayOfbyte2, byte[] param2ArrayOfbyte3) throws KeyStoreException {
        if (param2Int2 > 0) {
          this.mConsumedInputSizeBytes += param2Int2;
          this.mInputBuffer.write(param2ArrayOfbyte1, param2Int1, param2Int2);
        } 
        param2ArrayOfbyte1 = this.mInputBuffer.toByteArray();
        this.mInputBuffer.reset();
        KeyStoreCryptoOperationStreamer keyStoreCryptoOperationStreamer = this.mDelegate;
        param2Int2 = param2ArrayOfbyte1.length;
        param2Int1 = (this.mGroupSizeBits + 7) / 8;
        param2Int1 = Math.min(param2Int2, param2Int1);
        return keyStoreCryptoOperationStreamer.doFinal(param2ArrayOfbyte1, 0, param2Int1, param2ArrayOfbyte2, param2ArrayOfbyte3);
      }
      
      public long getConsumedInputSizeBytes() {
        return this.mConsumedInputSizeBytes;
      }
      
      public long getProducedOutputSizeBytes() {
        return this.mDelegate.getProducedOutputSizeBytes();
      }
    }
  }
  
  class SHA1 extends AndroidKeyStoreECDSASignatureSpi {
    public SHA1() {
      super(2);
    }
  }
  
  class SHA224 extends AndroidKeyStoreECDSASignatureSpi {
    public SHA224() {
      super(3);
    }
  }
  
  class SHA256 extends AndroidKeyStoreECDSASignatureSpi {
    public SHA256() {
      super(4);
    }
  }
  
  class SHA384 extends AndroidKeyStoreECDSASignatureSpi {
    public SHA384() {
      super(5);
    }
  }
  
  class SHA512 extends AndroidKeyStoreECDSASignatureSpi {
    public SHA512() {
      super(6);
    }
  }
  
  private int mGroupSizeBits = -1;
  
  private final int mKeymasterDigest;
  
  AndroidKeyStoreECDSASignatureSpi(int paramInt) {
    this.mKeymasterDigest = paramInt;
  }
  
  protected final void initKey(AndroidKeyStoreKey paramAndroidKeyStoreKey) throws InvalidKeyException {
    StringBuilder stringBuilder1;
    if ("EC".equalsIgnoreCase(paramAndroidKeyStoreKey.getAlgorithm())) {
      KeyCharacteristics keyCharacteristics = new KeyCharacteristics();
      KeyStore keyStore = getKeyStore();
      String str = paramAndroidKeyStoreKey.getAlias();
      int i = paramAndroidKeyStoreKey.getUid();
      i = keyStore.getKeyCharacteristics(str, null, null, i, keyCharacteristics);
      if (i == 1) {
        long l = keyCharacteristics.getUnsignedInt(805306371, -1L);
        if (l != -1L) {
          if (l <= 2147483647L) {
            this.mGroupSizeBits = (int)l;
            super.initKey(paramAndroidKeyStoreKey);
            return;
          } 
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Key too large: ");
          stringBuilder1.append(l);
          stringBuilder1.append(" bits");
          throw new InvalidKeyException(stringBuilder1.toString());
        } 
        throw new InvalidKeyException("Size of key not known");
      } 
      throw getKeyStore().getInvalidKeyException(stringBuilder1.getAlias(), stringBuilder1.getUid(), i);
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Unsupported key algorithm: ");
    stringBuilder2.append(stringBuilder1.getAlgorithm());
    stringBuilder2.append(". Only");
    stringBuilder2.append("EC");
    stringBuilder2.append(" supported");
    throw new InvalidKeyException(stringBuilder2.toString());
  }
  
  protected final void resetAll() {
    this.mGroupSizeBits = -1;
    super.resetAll();
  }
  
  protected final void resetWhilePreservingInitState() {
    super.resetWhilePreservingInitState();
  }
  
  protected final void addAlgorithmSpecificParametersToBegin(KeymasterArguments paramKeymasterArguments) {
    paramKeymasterArguments.addEnum(268435458, 3);
    paramKeymasterArguments.addEnum(536870917, this.mKeymasterDigest);
  }
  
  protected final int getAdditionalEntropyAmountForSign() {
    return (this.mGroupSizeBits + 7) / 8;
  }
  
  protected final int getGroupSizeBits() {
    int i = this.mGroupSizeBits;
    if (i != -1)
      return i; 
    throw new IllegalStateException("Not initialized");
  }
}
