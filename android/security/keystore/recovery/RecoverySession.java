package android.security.keystore.recovery;

import android.annotation.SystemApi;
import android.os.RemoteException;
import android.os.ServiceSpecificException;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.util.ArrayMap;
import android.util.Log;
import com.android.internal.widget.ILockSettings;
import java.security.Key;
import java.security.SecureRandom;
import java.security.cert.CertPath;
import java.security.cert.CertificateException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import libcore.util.HexEncoding;

@SystemApi
public class RecoverySession implements AutoCloseable {
  private static final int SESSION_ID_LENGTH_BYTES = 16;
  
  private static final String TAG = "RecoverySession";
  
  private final RecoveryController mRecoveryController;
  
  private final String mSessionId;
  
  private RecoverySession(RecoveryController paramRecoveryController, String paramString) {
    this.mRecoveryController = paramRecoveryController;
    this.mSessionId = paramString;
  }
  
  static RecoverySession newInstance(RecoveryController paramRecoveryController) {
    return new RecoverySession(paramRecoveryController, newSessionId());
  }
  
  private static String newSessionId() {
    SecureRandom secureRandom = new SecureRandom();
    byte[] arrayOfByte = new byte[16];
    secureRandom.nextBytes(arrayOfByte);
    return HexEncoding.encodeToString(arrayOfByte, false);
  }
  
  public byte[] start(String paramString, CertPath paramCertPath, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, List<KeyChainProtectionParams> paramList) throws CertificateException, InternalRecoveryServiceException {
    RecoveryCertPath recoveryCertPath = RecoveryCertPath.createRecoveryCertPath(paramCertPath);
    try {
      RecoveryController recoveryController = this.mRecoveryController;
      return recoveryController.getBinder().startRecoverySessionWithCertPath(this.mSessionId, paramString, recoveryCertPath, paramArrayOfbyte1, paramArrayOfbyte2, paramList);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      if (serviceSpecificException.errorCode == 25 || serviceSpecificException.errorCode == 28)
        throw new CertificateException("Invalid certificate for recovery session", serviceSpecificException); 
      throw this.mRecoveryController.wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public Map<String, Key> recoverKeyChainSnapshot(byte[] paramArrayOfbyte, List<WrappedApplicationKey> paramList) throws SessionExpiredException, DecryptionFailedException, InternalRecoveryServiceException {
    try {
      RecoveryController recoveryController = this.mRecoveryController;
      ILockSettings iLockSettings = recoveryController.getBinder();
      String str = this.mSessionId;
      null = iLockSettings.recoverKeyChainSnapshot(str, paramArrayOfbyte, paramList);
      return getKeysFromGrants(null);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      if (serviceSpecificException.errorCode != 26) {
        if (serviceSpecificException.errorCode == 24)
          throw new SessionExpiredException(serviceSpecificException.getMessage()); 
        throw this.mRecoveryController.wrapUnexpectedServiceSpecificException(serviceSpecificException);
      } 
      throw new DecryptionFailedException(serviceSpecificException.getMessage());
    } 
  }
  
  private Map<String, Key> getKeysFromGrants(Map<String, String> paramMap) throws InternalRecoveryServiceException {
    ArrayMap arrayMap = new ArrayMap(paramMap.size());
    for (Iterator<String> iterator = paramMap.keySet().iterator(); iterator.hasNext(); ) {
      String str1 = iterator.next();
      String str2 = paramMap.get(str1);
      try {
        Key key = this.mRecoveryController.getKeyFromGrant(str2);
        arrayMap.put(str1, key);
      } catch (KeyPermanentlyInvalidatedException|java.security.UnrecoverableKeyException keyPermanentlyInvalidatedException) {
        Locale locale = Locale.US;
        throw new InternalRecoveryServiceException(String.format(locale, "Failed to get key '%s' from grant '%s'", new Object[] { str1, str2 }), keyPermanentlyInvalidatedException);
      } 
    } 
    return (Map<String, Key>)arrayMap;
  }
  
  String getSessionId() {
    return this.mSessionId;
  }
  
  public void close() {
    try {
      this.mRecoveryController.getBinder().closeSession(this.mSessionId);
    } catch (RemoteException|ServiceSpecificException remoteException) {
      Log.e("RecoverySession", "Unexpected error trying to close session", (Throwable)remoteException);
    } 
  }
}
