package android.security.keystore.recovery;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

@SystemApi
public final class KeyDerivationParams implements Parcelable {
  public static final int ALGORITHM_SCRYPT = 2;
  
  public static final int ALGORITHM_SHA256 = 1;
  
  public static KeyDerivationParams createSha256Params(byte[] paramArrayOfbyte) {
    return new KeyDerivationParams(1, paramArrayOfbyte);
  }
  
  public static KeyDerivationParams createScryptParams(byte[] paramArrayOfbyte, int paramInt) {
    return new KeyDerivationParams(2, paramArrayOfbyte, paramInt);
  }
  
  private KeyDerivationParams(int paramInt, byte[] paramArrayOfbyte) {
    this(paramInt, paramArrayOfbyte, -1);
  }
  
  private KeyDerivationParams(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) {
    this.mAlgorithm = paramInt1;
    Objects.requireNonNull(paramArrayOfbyte);
    this.mSalt = paramArrayOfbyte;
    this.mMemoryDifficulty = paramInt2;
  }
  
  public int getAlgorithm() {
    return this.mAlgorithm;
  }
  
  public byte[] getSalt() {
    return this.mSalt;
  }
  
  public int getMemoryDifficulty() {
    return this.mMemoryDifficulty;
  }
  
  public static final Parcelable.Creator<KeyDerivationParams> CREATOR = new Parcelable.Creator<KeyDerivationParams>() {
      public KeyDerivationParams createFromParcel(Parcel param1Parcel) {
        return new KeyDerivationParams(param1Parcel);
      }
      
      public KeyDerivationParams[] newArray(int param1Int) {
        return new KeyDerivationParams[param1Int];
      }
    };
  
  private final int mAlgorithm;
  
  private final int mMemoryDifficulty;
  
  private final byte[] mSalt;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mAlgorithm);
    paramParcel.writeByteArray(this.mSalt);
    paramParcel.writeInt(this.mMemoryDifficulty);
  }
  
  protected KeyDerivationParams(Parcel paramParcel) {
    this.mAlgorithm = paramParcel.readInt();
    this.mSalt = paramParcel.createByteArray();
    this.mMemoryDifficulty = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class KeyDerivationAlgorithm implements Annotation {}
}
