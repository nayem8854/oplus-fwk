package android.security.keystore.recovery;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayInputStream;
import java.security.cert.CertPath;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Objects;

public final class RecoveryCertPath implements Parcelable {
  private static final String CERT_PATH_ENCODING = "PkiPath";
  
  public static RecoveryCertPath createRecoveryCertPath(CertPath paramCertPath) throws CertificateException {
    try {
      return new RecoveryCertPath(encodeCertPath(paramCertPath));
    } catch (CertificateEncodingException certificateEncodingException) {
      throw new CertificateException("Failed to encode the given CertPath", certificateEncodingException);
    } 
  }
  
  public CertPath getCertPath() throws CertificateException {
    return decodeCertPath(this.mEncodedCertPath);
  }
  
  private RecoveryCertPath(byte[] paramArrayOfbyte) {
    Objects.requireNonNull(paramArrayOfbyte);
    this.mEncodedCertPath = paramArrayOfbyte;
  }
  
  private RecoveryCertPath(Parcel paramParcel) {
    this.mEncodedCertPath = paramParcel.createByteArray();
  }
  
  public static final Parcelable.Creator<RecoveryCertPath> CREATOR = new Parcelable.Creator<RecoveryCertPath>() {
      public RecoveryCertPath createFromParcel(Parcel param1Parcel) {
        return new RecoveryCertPath(param1Parcel);
      }
      
      public RecoveryCertPath[] newArray(int param1Int) {
        return new RecoveryCertPath[param1Int];
      }
    };
  
  private final byte[] mEncodedCertPath;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByteArray(this.mEncodedCertPath);
  }
  
  public int describeContents() {
    return 0;
  }
  
  private static byte[] encodeCertPath(CertPath paramCertPath) throws CertificateEncodingException {
    Objects.requireNonNull(paramCertPath);
    return paramCertPath.getEncoded("PkiPath");
  }
  
  private static CertPath decodeCertPath(byte[] paramArrayOfbyte) throws CertificateException {
    Objects.requireNonNull(paramArrayOfbyte);
    try {
      CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
      return certificateFactory.generateCertPath(new ByteArrayInputStream(paramArrayOfbyte), "PkiPath");
    } catch (CertificateException certificateException) {
      throw new RuntimeException(certificateException);
    } 
  }
}
