package android.security.keystore.recovery;

import android.annotation.SystemApi;
import android.os.BadParcelableException;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.security.cert.CertPath;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.Objects;

@SystemApi
public final class KeyChainSnapshot implements Parcelable {
  private int mSnapshotVersion;
  
  private byte[] mServerParams;
  
  private int mMaxAttempts = 10;
  
  private List<KeyChainProtectionParams> mKeyChainProtectionParams;
  
  private List<WrappedApplicationKey> mEntryRecoveryData;
  
  private byte[] mEncryptedRecoveryKeyBlob;
  
  private long mCounterId = 1L;
  
  private RecoveryCertPath mCertPath;
  
  private static final int DEFAULT_MAX_ATTEMPTS = 10;
  
  private static final long DEFAULT_COUNTER_ID = 1L;
  
  public int getSnapshotVersion() {
    return this.mSnapshotVersion;
  }
  
  public int getMaxAttempts() {
    return this.mMaxAttempts;
  }
  
  public long getCounterId() {
    return this.mCounterId;
  }
  
  public byte[] getServerParams() {
    return this.mServerParams;
  }
  
  public CertPath getTrustedHardwareCertPath() {
    try {
      return this.mCertPath.getCertPath();
    } catch (CertificateException certificateException) {
      throw new BadParcelableException(certificateException);
    } 
  }
  
  public List<KeyChainProtectionParams> getKeyChainProtectionParams() {
    return this.mKeyChainProtectionParams;
  }
  
  public List<WrappedApplicationKey> getWrappedApplicationKeys() {
    return this.mEntryRecoveryData;
  }
  
  public byte[] getEncryptedRecoveryKeyBlob() {
    return this.mEncryptedRecoveryKeyBlob;
  }
  
  public static final Parcelable.Creator<KeyChainSnapshot> CREATOR = new Parcelable.Creator<KeyChainSnapshot>() {
      public KeyChainSnapshot createFromParcel(Parcel param1Parcel) {
        return new KeyChainSnapshot(param1Parcel);
      }
      
      public KeyChainSnapshot[] newArray(int param1Int) {
        return new KeyChainSnapshot[param1Int];
      }
    };
  
  class Builder {
    private KeyChainSnapshot mInstance;
    
    public Builder() {
      this.mInstance = new KeyChainSnapshot();
    }
    
    public Builder setSnapshotVersion(int param1Int) {
      KeyChainSnapshot.access$102(this.mInstance, param1Int);
      return this;
    }
    
    public Builder setMaxAttempts(int param1Int) {
      KeyChainSnapshot.access$202(this.mInstance, param1Int);
      return this;
    }
    
    public Builder setCounterId(long param1Long) {
      KeyChainSnapshot.access$302(this.mInstance, param1Long);
      return this;
    }
    
    public Builder setServerParams(byte[] param1ArrayOfbyte) {
      KeyChainSnapshot.access$402(this.mInstance, param1ArrayOfbyte);
      return this;
    }
    
    public Builder setTrustedHardwareCertPath(CertPath param1CertPath) throws CertificateException {
      KeyChainSnapshot.access$502(this.mInstance, RecoveryCertPath.createRecoveryCertPath(param1CertPath));
      return this;
    }
    
    public Builder setKeyChainProtectionParams(List<KeyChainProtectionParams> param1List) {
      KeyChainSnapshot.access$602(this.mInstance, param1List);
      return this;
    }
    
    public Builder setWrappedApplicationKeys(List<WrappedApplicationKey> param1List) {
      KeyChainSnapshot.access$702(this.mInstance, param1List);
      return this;
    }
    
    public Builder setEncryptedRecoveryKeyBlob(byte[] param1ArrayOfbyte) {
      KeyChainSnapshot.access$802(this.mInstance, param1ArrayOfbyte);
      return this;
    }
    
    public KeyChainSnapshot build() {
      Preconditions.checkCollectionElementsNotNull(this.mInstance.mKeyChainProtectionParams, "keyChainProtectionParams");
      Preconditions.checkCollectionElementsNotNull(this.mInstance.mEntryRecoveryData, "entryRecoveryData");
      Objects.requireNonNull(this.mInstance.mEncryptedRecoveryKeyBlob);
      Objects.requireNonNull(this.mInstance.mServerParams);
      Objects.requireNonNull(this.mInstance.mCertPath);
      return this.mInstance;
    }
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSnapshotVersion);
    paramParcel.writeTypedList(this.mKeyChainProtectionParams);
    paramParcel.writeByteArray(this.mEncryptedRecoveryKeyBlob);
    paramParcel.writeTypedList(this.mEntryRecoveryData);
    paramParcel.writeInt(this.mMaxAttempts);
    paramParcel.writeLong(this.mCounterId);
    paramParcel.writeByteArray(this.mServerParams);
    paramParcel.writeTypedObject(this.mCertPath, 0);
  }
  
  protected KeyChainSnapshot(Parcel paramParcel) {
    this.mSnapshotVersion = paramParcel.readInt();
    this.mKeyChainProtectionParams = paramParcel.createTypedArrayList(KeyChainProtectionParams.CREATOR);
    this.mEncryptedRecoveryKeyBlob = paramParcel.createByteArray();
    this.mEntryRecoveryData = paramParcel.createTypedArrayList(WrappedApplicationKey.CREATOR);
    this.mMaxAttempts = paramParcel.readInt();
    this.mCounterId = paramParcel.readLong();
    this.mServerParams = paramParcel.createByteArray();
    this.mCertPath = paramParcel.<RecoveryCertPath>readTypedObject(RecoveryCertPath.CREATOR);
  }
  
  public int describeContents() {
    return 0;
  }
  
  private KeyChainSnapshot() {}
}
