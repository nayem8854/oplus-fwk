package android.security.keystore.recovery;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

public class X509CertificateParsingUtils {
  private static final String CERT_FORMAT = "X.509";
  
  public static X509Certificate decodeBase64Cert(String paramString) throws CertificateException {
    try {
      return decodeCert(decodeBase64(paramString));
    } catch (IllegalArgumentException illegalArgumentException) {
      throw new CertificateException(illegalArgumentException);
    } 
  }
  
  private static byte[] decodeBase64(String paramString) {
    return Base64.getDecoder().decode(paramString);
  }
  
  private static X509Certificate decodeCert(byte[] paramArrayOfbyte) throws CertificateException {
    return decodeCert(new ByteArrayInputStream(paramArrayOfbyte));
  }
  
  private static X509Certificate decodeCert(InputStream paramInputStream) throws CertificateException {
    try {
      CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
      return (X509Certificate)certificateFactory.generateCertificate(paramInputStream);
    } catch (CertificateException certificateException) {
      throw new RuntimeException(certificateException);
    } 
  }
}
