package android.security.keystore.recovery;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

@SystemApi
public final class WrappedApplicationKey implements Parcelable {
  class Builder {
    private WrappedApplicationKey mInstance = new WrappedApplicationKey();
    
    public Builder setAlias(String param1String) {
      WrappedApplicationKey.access$102(this.mInstance, param1String);
      return this;
    }
    
    public Builder setEncryptedKeyMaterial(byte[] param1ArrayOfbyte) {
      WrappedApplicationKey.access$202(this.mInstance, param1ArrayOfbyte);
      return this;
    }
    
    public Builder setMetadata(byte[] param1ArrayOfbyte) {
      WrappedApplicationKey.access$302(this.mInstance, param1ArrayOfbyte);
      return this;
    }
    
    public WrappedApplicationKey build() {
      Objects.requireNonNull(this.mInstance.mAlias);
      Objects.requireNonNull(this.mInstance.mEncryptedKeyMaterial);
      return this.mInstance;
    }
  }
  
  private WrappedApplicationKey() {}
  
  @Deprecated
  public WrappedApplicationKey(String paramString, byte[] paramArrayOfbyte) {
    Objects.requireNonNull(paramString);
    this.mAlias = paramString;
    Objects.requireNonNull(paramArrayOfbyte);
    this.mEncryptedKeyMaterial = paramArrayOfbyte;
  }
  
  public String getAlias() {
    return this.mAlias;
  }
  
  public byte[] getEncryptedKeyMaterial() {
    return this.mEncryptedKeyMaterial;
  }
  
  public byte[] getMetadata() {
    return this.mMetadata;
  }
  
  public static final Parcelable.Creator<WrappedApplicationKey> CREATOR = new Parcelable.Creator<WrappedApplicationKey>() {
      public WrappedApplicationKey createFromParcel(Parcel param1Parcel) {
        return new WrappedApplicationKey(param1Parcel);
      }
      
      public WrappedApplicationKey[] newArray(int param1Int) {
        return new WrappedApplicationKey[param1Int];
      }
    };
  
  private String mAlias;
  
  private byte[] mEncryptedKeyMaterial;
  
  private byte[] mMetadata;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mAlias);
    paramParcel.writeByteArray(this.mEncryptedKeyMaterial);
    paramParcel.writeByteArray(this.mMetadata);
  }
  
  protected WrappedApplicationKey(Parcel paramParcel) {
    this.mAlias = paramParcel.readString();
    this.mEncryptedKeyMaterial = paramParcel.createByteArray();
    if (paramParcel.dataAvail() > 0)
      this.mMetadata = paramParcel.createByteArray(); 
  }
  
  public int describeContents() {
    return 0;
  }
}
