package android.security.keystore.recovery;

import android.annotation.SystemApi;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.ServiceSpecificException;
import android.security.KeyStore;
import android.security.keystore.AndroidKeyStoreProvider;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import com.android.internal.widget.ILockSettings;
import java.security.Key;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SystemApi
public class RecoveryController {
  public static final int ERROR_BAD_CERTIFICATE_FORMAT = 25;
  
  public static final int ERROR_DECRYPTION_FAILED = 26;
  
  public static final int ERROR_DOWNGRADE_CERTIFICATE = 29;
  
  public static final int ERROR_INSECURE_USER = 23;
  
  public static final int ERROR_INVALID_CERTIFICATE = 28;
  
  public static final int ERROR_INVALID_KEY_FORMAT = 27;
  
  public static final int ERROR_NO_SNAPSHOT_PENDING = 21;
  
  public static final int ERROR_SERVICE_INTERNAL_ERROR = 22;
  
  public static final int ERROR_SESSION_EXPIRED = 24;
  
  public static final int RECOVERY_STATUS_PERMANENT_FAILURE = 3;
  
  public static final int RECOVERY_STATUS_SYNCED = 0;
  
  public static final int RECOVERY_STATUS_SYNC_IN_PROGRESS = 1;
  
  private static final String TAG = "RecoveryController";
  
  private final ILockSettings mBinder;
  
  private final KeyStore mKeyStore;
  
  private RecoveryController(ILockSettings paramILockSettings, KeyStore paramKeyStore) {
    this.mBinder = paramILockSettings;
    this.mKeyStore = paramKeyStore;
  }
  
  ILockSettings getBinder() {
    return this.mBinder;
  }
  
  public static RecoveryController getInstance(Context paramContext) {
    ILockSettings iLockSettings = ILockSettings.Stub.asInterface(ServiceManager.getService("lock_settings"));
    return new RecoveryController(iLockSettings, KeyStore.getInstance());
  }
  
  public static boolean isRecoverableKeyStoreEnabled(Context paramContext) {
    boolean bool;
    KeyguardManager keyguardManager = (KeyguardManager)paramContext.getSystemService(KeyguardManager.class);
    if (keyguardManager != null && keyguardManager.isDeviceSecure()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void initRecoveryService(String paramString, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws CertificateException, InternalRecoveryServiceException {
    try {
      this.mBinder.initRecoveryServiceWithSigFile(paramString, paramArrayOfbyte1, paramArrayOfbyte2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      if (serviceSpecificException.errorCode != 25 && serviceSpecificException.errorCode != 28) {
        if (serviceSpecificException.errorCode == 29)
          throw new CertificateException("Downgrading certificate serial version isn't supported.", serviceSpecificException); 
        throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
      } 
      throw new CertificateException("Invalid certificate for recovery service", serviceSpecificException);
    } 
  }
  
  public KeyChainSnapshot getKeyChainSnapshot() throws InternalRecoveryServiceException {
    try {
      return this.mBinder.getKeyChainSnapshot();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      if (serviceSpecificException.errorCode == 21)
        return null; 
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void setSnapshotCreatedPendingIntent(PendingIntent paramPendingIntent) throws InternalRecoveryServiceException {
    try {
      this.mBinder.setSnapshotCreatedPendingIntent(paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void setServerParams(byte[] paramArrayOfbyte) throws InternalRecoveryServiceException {
    try {
      this.mBinder.setServerParams(paramArrayOfbyte);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public List<String> getAliases() throws InternalRecoveryServiceException {
    try {
      Map map = this.mBinder.getRecoveryStatus();
      return new ArrayList(map.keySet());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void setRecoveryStatus(String paramString, int paramInt) throws InternalRecoveryServiceException {
    try {
      this.mBinder.setRecoveryStatus(paramString, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public int getRecoveryStatus(String paramString) throws InternalRecoveryServiceException {
    try {
      Map map = this.mBinder.getRecoveryStatus();
      Integer integer = (Integer)map.get(paramString);
      if (integer == null)
        return 3; 
      return integer.intValue();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public void setRecoverySecretTypes(int[] paramArrayOfint) throws InternalRecoveryServiceException {
    try {
      this.mBinder.setRecoverySecretTypes(paramArrayOfint);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public int[] getRecoverySecretTypes() throws InternalRecoveryServiceException {
    try {
      return this.mBinder.getRecoverySecretTypes();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  @Deprecated
  public Key generateKey(String paramString) throws InternalRecoveryServiceException, LockScreenRequiredException {
    try {
      paramString = this.mBinder.generateKey(paramString);
      if (paramString != null)
        return getKeyFromGrant(paramString); 
      InternalRecoveryServiceException internalRecoveryServiceException = new InternalRecoveryServiceException();
      this("null grant alias");
      throw internalRecoveryServiceException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (KeyPermanentlyInvalidatedException|UnrecoverableKeyException keyPermanentlyInvalidatedException) {
      throw new InternalRecoveryServiceException("Failed to get key from keystore", keyPermanentlyInvalidatedException);
    } catch (ServiceSpecificException serviceSpecificException) {
      if (serviceSpecificException.errorCode == 23)
        throw new LockScreenRequiredException(serviceSpecificException.getMessage()); 
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public Key generateKey(String paramString, byte[] paramArrayOfbyte) throws InternalRecoveryServiceException, LockScreenRequiredException {
    try {
      paramString = this.mBinder.generateKeyWithMetadata(paramString, paramArrayOfbyte);
      if (paramString != null)
        return getKeyFromGrant(paramString); 
      InternalRecoveryServiceException internalRecoveryServiceException = new InternalRecoveryServiceException();
      this("null grant alias");
      throw internalRecoveryServiceException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (KeyPermanentlyInvalidatedException|UnrecoverableKeyException keyPermanentlyInvalidatedException) {
      throw new InternalRecoveryServiceException("Failed to get key from keystore", keyPermanentlyInvalidatedException);
    } catch (ServiceSpecificException serviceSpecificException) {
      if (serviceSpecificException.errorCode == 23)
        throw new LockScreenRequiredException(serviceSpecificException.getMessage()); 
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  @Deprecated
  public Key importKey(String paramString, byte[] paramArrayOfbyte) throws InternalRecoveryServiceException, LockScreenRequiredException {
    try {
      paramString = this.mBinder.importKey(paramString, paramArrayOfbyte);
      if (paramString != null)
        return getKeyFromGrant(paramString); 
      InternalRecoveryServiceException internalRecoveryServiceException = new InternalRecoveryServiceException();
      this("Null grant alias");
      throw internalRecoveryServiceException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (KeyPermanentlyInvalidatedException|UnrecoverableKeyException keyPermanentlyInvalidatedException) {
      throw new InternalRecoveryServiceException("Failed to get key from keystore", keyPermanentlyInvalidatedException);
    } catch (ServiceSpecificException serviceSpecificException) {
      if (serviceSpecificException.errorCode == 23)
        throw new LockScreenRequiredException(serviceSpecificException.getMessage()); 
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public Key importKey(String paramString, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws InternalRecoveryServiceException, LockScreenRequiredException {
    try {
      paramString = this.mBinder.importKeyWithMetadata(paramString, paramArrayOfbyte1, paramArrayOfbyte2);
      if (paramString != null)
        return getKeyFromGrant(paramString); 
      InternalRecoveryServiceException internalRecoveryServiceException = new InternalRecoveryServiceException();
      this("Null grant alias");
      throw internalRecoveryServiceException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (KeyPermanentlyInvalidatedException|UnrecoverableKeyException keyPermanentlyInvalidatedException) {
      throw new InternalRecoveryServiceException("Failed to get key from keystore", keyPermanentlyInvalidatedException);
    } catch (ServiceSpecificException serviceSpecificException) {
      if (serviceSpecificException.errorCode == 23)
        throw new LockScreenRequiredException(serviceSpecificException.getMessage()); 
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public Key getKey(String paramString) throws InternalRecoveryServiceException, UnrecoverableKeyException {
    try {
      paramString = this.mBinder.getKey(paramString);
      if (paramString == null || "".equals(paramString))
        return null; 
      return getKeyFromGrant(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (KeyPermanentlyInvalidatedException|UnrecoverableKeyException keyPermanentlyInvalidatedException) {
      throw new UnrecoverableKeyException(keyPermanentlyInvalidatedException.getMessage());
    } catch (ServiceSpecificException serviceSpecificException) {
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  Key getKeyFromGrant(String paramString) throws UnrecoverableKeyException, KeyPermanentlyInvalidatedException {
    return AndroidKeyStoreProvider.loadAndroidKeyStoreKeyFromKeystore(this.mKeyStore, paramString, -1);
  }
  
  public void removeKey(String paramString) throws InternalRecoveryServiceException {
    try {
      this.mBinder.removeKey(paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } catch (ServiceSpecificException serviceSpecificException) {
      throw wrapUnexpectedServiceSpecificException(serviceSpecificException);
    } 
  }
  
  public RecoverySession createRecoverySession() {
    return RecoverySession.newInstance(this);
  }
  
  public Map<String, X509Certificate> getRootCertificates() {
    return TrustedRootCertificates.getRootCertificates();
  }
  
  InternalRecoveryServiceException wrapUnexpectedServiceSpecificException(ServiceSpecificException paramServiceSpecificException) {
    if (paramServiceSpecificException.errorCode == 22)
      return new InternalRecoveryServiceException(paramServiceSpecificException.getMessage()); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unexpected error code for method: ");
    stringBuilder.append(paramServiceSpecificException.errorCode);
    return new InternalRecoveryServiceException(stringBuilder.toString(), paramServiceSpecificException);
  }
}
