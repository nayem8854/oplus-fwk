package android.security.keystore.recovery;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.Objects;

@SystemApi
public final class KeyChainProtectionParams implements Parcelable {
  private KeyChainProtectionParams() {}
  
  public int getUserSecretType() {
    return this.mUserSecretType.intValue();
  }
  
  public int getLockScreenUiFormat() {
    return this.mLockScreenUiFormat.intValue();
  }
  
  public KeyDerivationParams getKeyDerivationParams() {
    return this.mKeyDerivationParams;
  }
  
  public byte[] getSecret() {
    return this.mSecret;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class UserSecretType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class LockScreenUiFormat implements Annotation {}
  
  class Builder {
    private KeyChainProtectionParams mInstance = new KeyChainProtectionParams();
    
    public Builder setUserSecretType(int param1Int) {
      KeyChainProtectionParams.access$102(this.mInstance, Integer.valueOf(param1Int));
      return this;
    }
    
    public Builder setLockScreenUiFormat(int param1Int) {
      KeyChainProtectionParams.access$202(this.mInstance, Integer.valueOf(param1Int));
      return this;
    }
    
    public Builder setKeyDerivationParams(KeyDerivationParams param1KeyDerivationParams) {
      KeyChainProtectionParams.access$302(this.mInstance, param1KeyDerivationParams);
      return this;
    }
    
    public Builder setSecret(byte[] param1ArrayOfbyte) {
      KeyChainProtectionParams.access$402(this.mInstance, param1ArrayOfbyte);
      return this;
    }
    
    public KeyChainProtectionParams build() {
      if (this.mInstance.mUserSecretType == null)
        KeyChainProtectionParams.access$102(this.mInstance, Integer.valueOf(100)); 
      Objects.requireNonNull(this.mInstance.mLockScreenUiFormat);
      Objects.requireNonNull(this.mInstance.mKeyDerivationParams);
      if (this.mInstance.mSecret == null)
        KeyChainProtectionParams.access$402(this.mInstance, new byte[0]); 
      return this.mInstance;
    }
  }
  
  public void clearSecret() {
    Arrays.fill(this.mSecret, (byte)0);
  }
  
  public static final Parcelable.Creator<KeyChainProtectionParams> CREATOR = new Parcelable.Creator<KeyChainProtectionParams>() {
      public KeyChainProtectionParams createFromParcel(Parcel param1Parcel) {
        return new KeyChainProtectionParams(param1Parcel);
      }
      
      public KeyChainProtectionParams[] newArray(int param1Int) {
        return new KeyChainProtectionParams[param1Int];
      }
    };
  
  public static final int TYPE_LOCKSCREEN = 100;
  
  public static final int UI_FORMAT_PASSWORD = 2;
  
  public static final int UI_FORMAT_PATTERN = 3;
  
  public static final int UI_FORMAT_PIN = 1;
  
  private KeyDerivationParams mKeyDerivationParams;
  
  private Integer mLockScreenUiFormat;
  
  private byte[] mSecret;
  
  private Integer mUserSecretType;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mUserSecretType.intValue());
    paramParcel.writeInt(this.mLockScreenUiFormat.intValue());
    paramParcel.writeTypedObject(this.mKeyDerivationParams, paramInt);
    paramParcel.writeByteArray(this.mSecret);
  }
  
  protected KeyChainProtectionParams(Parcel paramParcel) {
    this.mUserSecretType = Integer.valueOf(paramParcel.readInt());
    this.mLockScreenUiFormat = Integer.valueOf(paramParcel.readInt());
    this.mKeyDerivationParams = paramParcel.<KeyDerivationParams>readTypedObject(KeyDerivationParams.CREATOR);
    this.mSecret = paramParcel.createByteArray();
  }
  
  public int describeContents() {
    return 0;
  }
}
