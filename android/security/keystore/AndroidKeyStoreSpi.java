package android.security.keystore;

import android.security.Credentials;
import android.security.GateKeeper;
import android.security.KeyStore;
import android.security.keymaster.KeyCharacteristics;
import android.security.keymaster.KeymasterArguments;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.KeyStoreSpi;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.ProviderException;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.crypto.SecretKey;
import libcore.util.EmptyArray;

public class AndroidKeyStoreSpi extends KeyStoreSpi {
  public static final String NAME = "AndroidKeyStore";
  
  private KeyStore mKeyStore;
  
  private int mUid = -1;
  
  public Key engineGetKey(String paramString, char[] paramArrayOfchar) throws NoSuchAlgorithmException, UnrecoverableKeyException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("USRPKEY_");
    stringBuilder.append(paramString);
    String str2 = stringBuilder.toString();
    String str1 = str2;
    if (!this.mKeyStore.contains(str2, this.mUid)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("USRSKEY_");
      stringBuilder1.append(paramString);
      paramString = stringBuilder1.toString();
      str1 = paramString;
      if (!this.mKeyStore.contains(paramString, this.mUid))
        return null; 
    } 
    try {
      return AndroidKeyStoreProvider.loadAndroidKeyStoreKeyFromKeystore(this.mKeyStore, str1, this.mUid);
    } catch (KeyPermanentlyInvalidatedException keyPermanentlyInvalidatedException) {
      throw new UnrecoverableKeyException(keyPermanentlyInvalidatedException.getMessage());
    } 
  }
  
  public Certificate[] engineGetCertificateChain(String paramString) {
    if (paramString != null) {
      Certificate[] arrayOfCertificate;
      X509Certificate x509Certificate = (X509Certificate)engineGetCertificate(paramString);
      if (x509Certificate == null)
        return null; 
      KeyStore keyStore = this.mKeyStore;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("CACERT_");
      stringBuilder.append(paramString);
      byte[] arrayOfByte = keyStore.get(stringBuilder.toString(), this.mUid, true);
      if (arrayOfByte != null) {
        Collection<X509Certificate> collection = toCertificates(arrayOfByte);
        arrayOfCertificate = new Certificate[collection.size() + 1];
        Iterator<X509Certificate> iterator = collection.iterator();
        byte b = 1;
        while (iterator.hasNext()) {
          arrayOfCertificate[b] = iterator.next();
          b++;
        } 
      } else {
        arrayOfCertificate = new Certificate[1];
      } 
      arrayOfCertificate[0] = x509Certificate;
      return arrayOfCertificate;
    } 
    throw new NullPointerException("alias == null");
  }
  
  public Certificate engineGetCertificate(String paramString) {
    if (paramString != null) {
      KeyStore keyStore = this.mKeyStore;
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("USRCERT_");
      stringBuilder2.append(paramString);
      byte[] arrayOfByte2 = keyStore.get(stringBuilder2.toString(), this.mUid);
      if (arrayOfByte2 != null)
        return getCertificateForPrivateKeyEntry(paramString, arrayOfByte2); 
      keyStore = this.mKeyStore;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("CACERT_");
      stringBuilder1.append(paramString);
      byte[] arrayOfByte1 = keyStore.get(stringBuilder1.toString(), this.mUid);
      if (arrayOfByte1 != null)
        return getCertificateForTrustedCertificateEntry(arrayOfByte1); 
      return null;
    } 
    throw new NullPointerException("alias == null");
  }
  
  private Certificate getCertificateForTrustedCertificateEntry(byte[] paramArrayOfbyte) {
    return toCertificate(paramArrayOfbyte);
  }
  
  private Certificate getCertificateForPrivateKeyEntry(String paramString, byte[] paramArrayOfbyte) {
    X509Certificate x509Certificate = toCertificate(paramArrayOfbyte);
    if (x509Certificate == null)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("USRPKEY_");
    stringBuilder.append(paramString);
    paramString = stringBuilder.toString();
    if (this.mKeyStore.contains(paramString, this.mUid))
      return wrapIntoKeyStoreCertificate(paramString, this.mUid, x509Certificate); 
    return x509Certificate;
  }
  
  private static KeyStoreX509Certificate wrapIntoKeyStoreCertificate(String paramString, int paramInt, X509Certificate paramX509Certificate) {
    if (paramX509Certificate != null) {
      KeyStoreX509Certificate keyStoreX509Certificate = new KeyStoreX509Certificate(paramString, paramInt, paramX509Certificate);
    } else {
      paramString = null;
    } 
    return (KeyStoreX509Certificate)paramString;
  }
  
  private static X509Certificate toCertificate(byte[] paramArrayOfbyte) {
    try {
      CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
      ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
      this(paramArrayOfbyte);
      return (X509Certificate)certificateFactory.generateCertificate(byteArrayInputStream);
    } catch (CertificateException certificateException) {
      Log.w("AndroidKeyStore", "Couldn't parse certificate in keystore", certificateException);
      return null;
    } 
  }
  
  private static Collection<X509Certificate> toCertificates(byte[] paramArrayOfbyte) {
    try {
      CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
      ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
      this(paramArrayOfbyte);
      return (Collection)certificateFactory.generateCertificates(byteArrayInputStream);
    } catch (CertificateException certificateException) {
      Log.w("AndroidKeyStore", "Couldn't parse certificates in keystore", certificateException);
      return new ArrayList<>();
    } 
  }
  
  private Date getModificationDate(String paramString) {
    long l = this.mKeyStore.getmtime(paramString, this.mUid);
    if (l == -1L)
      return null; 
    return new Date(l);
  }
  
  public Date engineGetCreationDate(String paramString) {
    if (paramString != null) {
      StringBuilder stringBuilder4 = new StringBuilder();
      stringBuilder4.append("USRPKEY_");
      stringBuilder4.append(paramString);
      Date date3 = getModificationDate(stringBuilder4.toString());
      if (date3 != null)
        return date3; 
      StringBuilder stringBuilder3 = new StringBuilder();
      stringBuilder3.append("USRSKEY_");
      stringBuilder3.append(paramString);
      Date date2 = getModificationDate(stringBuilder3.toString());
      if (date2 != null)
        return date2; 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("USRCERT_");
      stringBuilder2.append(paramString);
      Date date1 = getModificationDate(stringBuilder2.toString());
      if (date1 != null)
        return date1; 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("CACERT_");
      stringBuilder1.append(paramString);
      return getModificationDate(stringBuilder1.toString());
    } 
    throw new NullPointerException("alias == null");
  }
  
  public void engineSetKeyEntry(String paramString, Key paramKey, char[] paramArrayOfchar, Certificate[] paramArrayOfCertificate) throws KeyStoreException {
    if (paramArrayOfchar == null || paramArrayOfchar.length <= 0) {
      if (paramKey instanceof PrivateKey) {
        setPrivateKeyEntry(paramString, (PrivateKey)paramKey, paramArrayOfCertificate, null);
      } else {
        if (paramKey instanceof SecretKey) {
          setSecretKeyEntry(paramString, (SecretKey)paramKey, null);
          return;
        } 
        throw new KeyStoreException("Only PrivateKey and SecretKey are supported");
      } 
      return;
    } 
    throw new KeyStoreException("entries cannot be protected with passwords");
  }
  
  private static KeyProtection getLegacyKeyProtectionParameter(PrivateKey paramPrivateKey) throws KeyStoreException {
    StringBuilder stringBuilder;
    String str = paramPrivateKey.getAlgorithm();
    if ("EC".equalsIgnoreCase(str)) {
      KeyProtection.Builder builder = new KeyProtection.Builder(12);
      builder.setDigests(new String[] { "NONE", "SHA-1", "SHA-224", "SHA-256", "SHA-384", "SHA-512" });
    } else {
      if ("RSA".equalsIgnoreCase(str)) {
        KeyProtection.Builder builder = new KeyProtection.Builder(15);
        builder.setDigests(new String[] { "NONE", "MD5", "SHA-1", "SHA-224", "SHA-256", "SHA-384", "SHA-512" });
        builder.setEncryptionPaddings(new String[] { "NoPadding", "PKCS1Padding", "OAEPPadding" });
        builder.setSignaturePaddings(new String[] { "PKCS1", "PSS" });
        builder.setRandomizedEncryptionRequired(false);
        builder.setUserAuthenticationRequired(false);
        return builder.build();
      } 
      stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported key algorithm: ");
      stringBuilder.append(str);
      throw new KeyStoreException(stringBuilder.toString());
    } 
    stringBuilder.setUserAuthenticationRequired(false);
    return stringBuilder.build();
  }
  
  private void setPrivateKeyEntry(String paramString, PrivateKey paramPrivateKey, Certificate[] paramArrayOfCertificate, KeyStore.ProtectionParameter paramProtectionParameter) throws KeyStoreException {
    // Byte code:
    //   0: iconst_0
    //   1: istore #5
    //   3: iconst_0
    //   4: istore #6
    //   6: aload #4
    //   8: ifnonnull -> 23
    //   11: aload_2
    //   12: invokestatic getLegacyKeyProtectionParameter : (Ljava/security/PrivateKey;)Landroid/security/keystore/KeyProtection;
    //   15: astore #4
    //   17: iconst_0
    //   18: istore #6
    //   20: goto -> 113
    //   23: aload #4
    //   25: instanceof android/security/KeyStoreParameter
    //   28: ifeq -> 62
    //   31: aload_2
    //   32: invokestatic getLegacyKeyProtectionParameter : (Ljava/security/PrivateKey;)Landroid/security/keystore/KeyProtection;
    //   35: astore #7
    //   37: aload #4
    //   39: checkcast android/security/KeyStoreParameter
    //   42: astore #4
    //   44: aload #4
    //   46: invokevirtual isEncryptionRequired : ()Z
    //   49: ifeq -> 55
    //   52: iconst_1
    //   53: istore #6
    //   55: aload #7
    //   57: astore #4
    //   59: goto -> 113
    //   62: aload #4
    //   64: instanceof android/security/keystore/KeyProtection
    //   67: ifeq -> 1391
    //   70: aload #4
    //   72: checkcast android/security/keystore/KeyProtection
    //   75: astore #4
    //   77: iload #5
    //   79: istore #6
    //   81: aload #4
    //   83: invokevirtual isCriticalToDeviceEncryption : ()Z
    //   86: ifeq -> 95
    //   89: iconst_0
    //   90: bipush #8
    //   92: ior
    //   93: istore #6
    //   95: aload #4
    //   97: invokevirtual isStrongBoxBacked : ()Z
    //   100: ifeq -> 113
    //   103: iload #6
    //   105: bipush #16
    //   107: ior
    //   108: istore #6
    //   110: goto -> 113
    //   113: aload_3
    //   114: ifnull -> 1380
    //   117: aload_3
    //   118: arraylength
    //   119: ifeq -> 1380
    //   122: aload_3
    //   123: arraylength
    //   124: anewarray java/security/cert/X509Certificate
    //   127: astore #7
    //   129: iconst_0
    //   130: istore #5
    //   132: iload #5
    //   134: aload_3
    //   135: arraylength
    //   136: if_icmpge -> 250
    //   139: ldc 'X.509'
    //   141: aload_3
    //   142: iload #5
    //   144: aaload
    //   145: invokevirtual getType : ()Ljava/lang/String;
    //   148: invokevirtual equals : (Ljava/lang/Object;)Z
    //   151: ifeq -> 216
    //   154: aload_3
    //   155: iload #5
    //   157: aaload
    //   158: instanceof java/security/cert/X509Certificate
    //   161: ifeq -> 182
    //   164: aload #7
    //   166: iload #5
    //   168: aload_3
    //   169: iload #5
    //   171: aaload
    //   172: checkcast java/security/cert/X509Certificate
    //   175: aastore
    //   176: iinc #5, 1
    //   179: goto -> 132
    //   182: new java/lang/StringBuilder
    //   185: dup
    //   186: invokespecial <init> : ()V
    //   189: astore_1
    //   190: aload_1
    //   191: ldc 'Certificates must be in X.509 format: invalid cert #'
    //   193: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: aload_1
    //   198: iload #5
    //   200: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   203: pop
    //   204: new java/security/KeyStoreException
    //   207: dup
    //   208: aload_1
    //   209: invokevirtual toString : ()Ljava/lang/String;
    //   212: invokespecial <init> : (Ljava/lang/String;)V
    //   215: athrow
    //   216: new java/lang/StringBuilder
    //   219: dup
    //   220: invokespecial <init> : ()V
    //   223: astore_1
    //   224: aload_1
    //   225: ldc 'Certificates must be in X.509 format: invalid cert #'
    //   227: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   230: pop
    //   231: aload_1
    //   232: iload #5
    //   234: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   237: pop
    //   238: new java/security/KeyStoreException
    //   241: dup
    //   242: aload_1
    //   243: invokevirtual toString : ()Ljava/lang/String;
    //   246: invokespecial <init> : (Ljava/lang/String;)V
    //   249: athrow
    //   250: aload #7
    //   252: iconst_0
    //   253: aaload
    //   254: invokevirtual getEncoded : ()[B
    //   257: astore #8
    //   259: aload_3
    //   260: arraylength
    //   261: iconst_1
    //   262: if_icmple -> 424
    //   265: aload #7
    //   267: arraylength
    //   268: iconst_1
    //   269: isub
    //   270: anewarray [B
    //   273: astore #9
    //   275: iconst_0
    //   276: istore #10
    //   278: iconst_0
    //   279: istore #5
    //   281: iload #5
    //   283: aload #9
    //   285: arraylength
    //   286: if_icmpge -> 361
    //   289: aload #9
    //   291: iload #5
    //   293: aload #7
    //   295: iload #5
    //   297: iconst_1
    //   298: iadd
    //   299: aaload
    //   300: invokevirtual getEncoded : ()[B
    //   303: aastore
    //   304: aload #9
    //   306: iload #5
    //   308: aaload
    //   309: arraylength
    //   310: istore #11
    //   312: iload #10
    //   314: iload #11
    //   316: iadd
    //   317: istore #10
    //   319: iinc #5, 1
    //   322: goto -> 281
    //   325: astore_1
    //   326: new java/lang/StringBuilder
    //   329: dup
    //   330: invokespecial <init> : ()V
    //   333: astore_2
    //   334: aload_2
    //   335: ldc 'Failed to encode certificate #'
    //   337: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   340: pop
    //   341: aload_2
    //   342: iload #5
    //   344: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   347: pop
    //   348: new java/security/KeyStoreException
    //   351: dup
    //   352: aload_2
    //   353: invokevirtual toString : ()Ljava/lang/String;
    //   356: aload_1
    //   357: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   360: athrow
    //   361: iload #10
    //   363: newarray byte
    //   365: astore_3
    //   366: iconst_0
    //   367: istore #10
    //   369: iconst_0
    //   370: istore #5
    //   372: iload #5
    //   374: aload #9
    //   376: arraylength
    //   377: if_icmpge -> 421
    //   380: aload #9
    //   382: iload #5
    //   384: aaload
    //   385: arraylength
    //   386: istore #11
    //   388: aload #9
    //   390: iload #5
    //   392: aaload
    //   393: iconst_0
    //   394: aload_3
    //   395: iload #10
    //   397: iload #11
    //   399: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   402: iload #10
    //   404: iload #11
    //   406: iadd
    //   407: istore #10
    //   409: aload #9
    //   411: iload #5
    //   413: aconst_null
    //   414: aastore
    //   415: iinc #5, 1
    //   418: goto -> 372
    //   421: goto -> 426
    //   424: aconst_null
    //   425: astore_3
    //   426: aload_2
    //   427: instanceof android/security/keystore/AndroidKeyStorePrivateKey
    //   430: ifeq -> 445
    //   433: aload_2
    //   434: checkcast android/security/keystore/AndroidKeyStoreKey
    //   437: invokevirtual getAlias : ()Ljava/lang/String;
    //   440: astore #7
    //   442: goto -> 448
    //   445: aconst_null
    //   446: astore #7
    //   448: aload #7
    //   450: ifnull -> 541
    //   453: aload #7
    //   455: ldc 'USRPKEY_'
    //   457: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   460: ifeq -> 541
    //   463: aload #7
    //   465: ldc 'USRPKEY_'
    //   467: invokevirtual length : ()I
    //   470: invokevirtual substring : (I)Ljava/lang/String;
    //   473: astore_2
    //   474: aload_1
    //   475: aload_2
    //   476: invokevirtual equals : (Ljava/lang/Object;)Z
    //   479: ifeq -> 493
    //   482: aconst_null
    //   483: astore_2
    //   484: iconst_0
    //   485: istore #5
    //   487: aconst_null
    //   488: astore #4
    //   490: goto -> 887
    //   493: new java/lang/StringBuilder
    //   496: dup
    //   497: invokespecial <init> : ()V
    //   500: astore_3
    //   501: aload_3
    //   502: ldc_w 'Can only replace keys with same alias: '
    //   505: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   508: pop
    //   509: aload_3
    //   510: aload_1
    //   511: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   514: pop
    //   515: aload_3
    //   516: ldc_w ' != '
    //   519: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   522: pop
    //   523: aload_3
    //   524: aload_2
    //   525: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   528: pop
    //   529: new java/security/KeyStoreException
    //   532: dup
    //   533: aload_3
    //   534: invokevirtual toString : ()Ljava/lang/String;
    //   537: invokespecial <init> : (Ljava/lang/String;)V
    //   540: athrow
    //   541: aload_2
    //   542: invokeinterface getFormat : ()Ljava/lang/String;
    //   547: astore #7
    //   549: aload #7
    //   551: ifnull -> 1324
    //   554: ldc_w 'PKCS#8'
    //   557: aload #7
    //   559: invokevirtual equals : (Ljava/lang/Object;)Z
    //   562: ifeq -> 1324
    //   565: aload_2
    //   566: invokeinterface getEncoded : ()[B
    //   571: astore #9
    //   573: aload #9
    //   575: ifnull -> 1313
    //   578: new android/security/keymaster/KeymasterArguments
    //   581: dup
    //   582: invokespecial <init> : ()V
    //   585: astore #7
    //   587: aload_2
    //   588: invokeinterface getAlgorithm : ()Ljava/lang/String;
    //   593: astore_2
    //   594: aload_2
    //   595: invokestatic toKeymasterAsymmetricKeyAlgorithm : (Ljava/lang/String;)I
    //   598: istore #5
    //   600: aload #7
    //   602: ldc_w 268435458
    //   605: iload #5
    //   607: invokevirtual addEnum : (II)V
    //   610: aload #4
    //   612: invokevirtual getPurposes : ()I
    //   615: istore #5
    //   617: iload #5
    //   619: invokestatic allToKeymaster : (I)[I
    //   622: astore_2
    //   623: aload #7
    //   625: ldc_w 536870913
    //   628: aload_2
    //   629: invokevirtual addEnums : (I[I)V
    //   632: aload #4
    //   634: invokevirtual isDigestsSpecified : ()Z
    //   637: istore #12
    //   639: iload #12
    //   641: ifeq -> 669
    //   644: aload #4
    //   646: invokevirtual getDigests : ()[Ljava/lang/String;
    //   649: invokestatic allToKeymaster : ([Ljava/lang/String;)[I
    //   652: astore_2
    //   653: aload #7
    //   655: ldc_w 536870917
    //   658: aload_2
    //   659: invokevirtual addEnums : (I[I)V
    //   662: goto -> 669
    //   665: astore_1
    //   666: goto -> 1304
    //   669: aload #4
    //   671: invokevirtual getBlockModes : ()[Ljava/lang/String;
    //   674: invokestatic allToKeymaster : ([Ljava/lang/String;)[I
    //   677: astore_2
    //   678: aload #7
    //   680: ldc_w 536870916
    //   683: aload_2
    //   684: invokevirtual addEnums : (I[I)V
    //   687: aload #4
    //   689: invokevirtual getEncryptionPaddings : ()[Ljava/lang/String;
    //   692: astore_2
    //   693: aload_2
    //   694: invokestatic allToKeymaster : ([Ljava/lang/String;)[I
    //   697: astore_2
    //   698: iload #5
    //   700: iconst_1
    //   701: iand
    //   702: ifeq -> 798
    //   705: aload #4
    //   707: invokevirtual isRandomizedEncryptionRequired : ()Z
    //   710: ifeq -> 795
    //   713: aload_2
    //   714: arraylength
    //   715: istore #10
    //   717: iconst_0
    //   718: istore #5
    //   720: iload #5
    //   722: iload #10
    //   724: if_icmpge -> 798
    //   727: aload_2
    //   728: iload #5
    //   730: iaload
    //   731: istore #11
    //   733: iload #11
    //   735: invokestatic isKeymasterPaddingSchemeIndCpaCompatibleWithAsymmetricCrypto : (I)Z
    //   738: ifeq -> 747
    //   741: iinc #5, 1
    //   744: goto -> 720
    //   747: new java/security/KeyStoreException
    //   750: astore_1
    //   751: new java/lang/StringBuilder
    //   754: astore_2
    //   755: aload_2
    //   756: invokespecial <init> : ()V
    //   759: aload_2
    //   760: ldc_w 'Randomized encryption (IND-CPA) required but is violated by encryption padding mode: '
    //   763: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   766: pop
    //   767: aload_2
    //   768: iload #11
    //   770: invokestatic fromKeymaster : (I)Ljava/lang/String;
    //   773: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   776: pop
    //   777: aload_2
    //   778: ldc_w '. See KeyProtection documentation.'
    //   781: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   784: pop
    //   785: aload_1
    //   786: aload_2
    //   787: invokevirtual toString : ()Ljava/lang/String;
    //   790: invokespecial <init> : (Ljava/lang/String;)V
    //   793: aload_1
    //   794: athrow
    //   795: goto -> 798
    //   798: aload #7
    //   800: ldc_w 536870918
    //   803: aload_2
    //   804: invokevirtual addEnums : (I[I)V
    //   807: aload #4
    //   809: invokevirtual getSignaturePaddings : ()[Ljava/lang/String;
    //   812: invokestatic allToKeymaster : ([Ljava/lang/String;)[I
    //   815: astore_2
    //   816: aload #7
    //   818: ldc_w 536870918
    //   821: aload_2
    //   822: invokevirtual addEnums : (I[I)V
    //   825: aload #7
    //   827: aload #4
    //   829: invokestatic addUserAuthArgs : (Landroid/security/keymaster/KeymasterArguments;Landroid/security/keystore/UserAuthArgs;)V
    //   832: aload #4
    //   834: invokevirtual getKeyValidityStart : ()Ljava/util/Date;
    //   837: astore_2
    //   838: aload #7
    //   840: ldc_w 1610613136
    //   843: aload_2
    //   844: invokevirtual addDateIfNotNull : (ILjava/util/Date;)V
    //   847: aload #4
    //   849: invokevirtual getKeyValidityForOriginationEnd : ()Ljava/util/Date;
    //   852: astore_2
    //   853: aload #7
    //   855: ldc_w 1610613137
    //   858: aload_2
    //   859: invokevirtual addDateIfNotNull : (ILjava/util/Date;)V
    //   862: aload #4
    //   864: invokevirtual getKeyValidityForConsumptionEnd : ()Ljava/util/Date;
    //   867: astore_2
    //   868: aload #7
    //   870: ldc_w 1610613138
    //   873: aload_2
    //   874: invokevirtual addDateIfNotNull : (ILjava/util/Date;)V
    //   877: iconst_1
    //   878: istore #5
    //   880: aload #9
    //   882: astore_2
    //   883: aload #7
    //   885: astore #4
    //   887: iload #5
    //   889: ifeq -> 1013
    //   892: aload_0
    //   893: getfield mKeyStore : Landroid/security/KeyStore;
    //   896: aload_1
    //   897: aload_0
    //   898: getfield mUid : I
    //   901: invokestatic deleteAllTypesForAlias : (Landroid/security/KeyStore;Ljava/lang/String;I)Z
    //   904: pop
    //   905: new android/security/keymaster/KeyCharacteristics
    //   908: astore #9
    //   910: aload #9
    //   912: invokespecial <init> : ()V
    //   915: aload_0
    //   916: getfield mKeyStore : Landroid/security/KeyStore;
    //   919: astore #7
    //   921: new java/lang/StringBuilder
    //   924: astore #13
    //   926: aload #13
    //   928: invokespecial <init> : ()V
    //   931: aload #13
    //   933: ldc 'USRPKEY_'
    //   935: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   938: pop
    //   939: aload #13
    //   941: aload_1
    //   942: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   945: pop
    //   946: aload #13
    //   948: invokevirtual toString : ()Ljava/lang/String;
    //   951: astore #13
    //   953: aload_0
    //   954: getfield mUid : I
    //   957: istore #10
    //   959: aload #7
    //   961: aload #13
    //   963: aload #4
    //   965: iconst_1
    //   966: aload_2
    //   967: iload #10
    //   969: iload #6
    //   971: aload #9
    //   973: invokevirtual importKey : (Ljava/lang/String;Landroid/security/keymaster/KeymasterArguments;I[BIILandroid/security/keymaster/KeyCharacteristics;)I
    //   976: istore #10
    //   978: iload #10
    //   980: iconst_1
    //   981: if_icmpne -> 987
    //   984: goto -> 1039
    //   987: new java/security/KeyStoreException
    //   990: astore_2
    //   991: aload_2
    //   992: ldc_w 'Failed to store private key'
    //   995: iload #10
    //   997: invokestatic getKeyStoreException : (I)Landroid/security/KeyStoreException;
    //   1000: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   1003: aload_2
    //   1004: athrow
    //   1005: astore_2
    //   1006: goto -> 1250
    //   1009: astore_2
    //   1010: goto -> 1250
    //   1013: aload_0
    //   1014: getfield mKeyStore : Landroid/security/KeyStore;
    //   1017: aload_1
    //   1018: aload_0
    //   1019: getfield mUid : I
    //   1022: invokestatic deleteCertificateTypesForAlias : (Landroid/security/KeyStore;Ljava/lang/String;I)Z
    //   1025: pop
    //   1026: aload_0
    //   1027: getfield mKeyStore : Landroid/security/KeyStore;
    //   1030: aload_1
    //   1031: aload_0
    //   1032: getfield mUid : I
    //   1035: invokestatic deleteLegacyKeyForAlias : (Landroid/security/KeyStore;Ljava/lang/String;I)Z
    //   1038: pop
    //   1039: aload_0
    //   1040: getfield mKeyStore : Landroid/security/KeyStore;
    //   1043: astore_2
    //   1044: new java/lang/StringBuilder
    //   1047: astore #4
    //   1049: aload #4
    //   1051: invokespecial <init> : ()V
    //   1054: aload #4
    //   1056: ldc_w 'USRCERT_'
    //   1059: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1062: pop
    //   1063: aload #4
    //   1065: aload_1
    //   1066: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1069: pop
    //   1070: aload #4
    //   1072: invokevirtual toString : ()Ljava/lang/String;
    //   1075: astore #4
    //   1077: aload_0
    //   1078: getfield mUid : I
    //   1081: istore #10
    //   1083: aload_2
    //   1084: aload #4
    //   1086: aload #8
    //   1088: iload #10
    //   1090: iload #6
    //   1092: invokevirtual insert : (Ljava/lang/String;[BII)I
    //   1095: istore #10
    //   1097: iload #10
    //   1099: iconst_1
    //   1100: if_icmpne -> 1227
    //   1103: aload_0
    //   1104: getfield mKeyStore : Landroid/security/KeyStore;
    //   1107: astore_2
    //   1108: new java/lang/StringBuilder
    //   1111: astore #4
    //   1113: aload #4
    //   1115: invokespecial <init> : ()V
    //   1118: aload #4
    //   1120: ldc 'CACERT_'
    //   1122: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1125: pop
    //   1126: aload #4
    //   1128: aload_1
    //   1129: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1132: pop
    //   1133: aload_2
    //   1134: aload #4
    //   1136: invokevirtual toString : ()Ljava/lang/String;
    //   1139: aload_3
    //   1140: aload_0
    //   1141: getfield mUid : I
    //   1144: iload #6
    //   1146: invokevirtual insert : (Ljava/lang/String;[BII)I
    //   1149: istore #6
    //   1151: iload #6
    //   1153: iconst_1
    //   1154: if_icmpne -> 1209
    //   1157: iconst_1
    //   1158: ifne -> 1208
    //   1161: iload #5
    //   1163: ifeq -> 1182
    //   1166: aload_0
    //   1167: getfield mKeyStore : Landroid/security/KeyStore;
    //   1170: aload_1
    //   1171: aload_0
    //   1172: getfield mUid : I
    //   1175: invokestatic deleteAllTypesForAlias : (Landroid/security/KeyStore;Ljava/lang/String;I)Z
    //   1178: pop
    //   1179: goto -> 1208
    //   1182: aload_0
    //   1183: getfield mKeyStore : Landroid/security/KeyStore;
    //   1186: aload_1
    //   1187: aload_0
    //   1188: getfield mUid : I
    //   1191: invokestatic deleteCertificateTypesForAlias : (Landroid/security/KeyStore;Ljava/lang/String;I)Z
    //   1194: pop
    //   1195: aload_0
    //   1196: getfield mKeyStore : Landroid/security/KeyStore;
    //   1199: aload_1
    //   1200: aload_0
    //   1201: getfield mUid : I
    //   1204: invokestatic deleteLegacyKeyForAlias : (Landroid/security/KeyStore;Ljava/lang/String;I)Z
    //   1207: pop
    //   1208: return
    //   1209: new java/security/KeyStoreException
    //   1212: astore_2
    //   1213: aload_2
    //   1214: ldc_w 'Failed to store certificate chain'
    //   1217: iload #6
    //   1219: invokestatic getKeyStoreException : (I)Landroid/security/KeyStoreException;
    //   1222: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   1225: aload_2
    //   1226: athrow
    //   1227: new java/security/KeyStoreException
    //   1230: astore_2
    //   1231: aload_2
    //   1232: ldc_w 'Failed to store certificate #0'
    //   1235: iload #10
    //   1237: invokestatic getKeyStoreException : (I)Landroid/security/KeyStoreException;
    //   1240: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   1243: aload_2
    //   1244: athrow
    //   1245: astore_2
    //   1246: goto -> 1250
    //   1249: astore_2
    //   1250: iconst_0
    //   1251: ifne -> 1301
    //   1254: iload #5
    //   1256: ifeq -> 1275
    //   1259: aload_0
    //   1260: getfield mKeyStore : Landroid/security/KeyStore;
    //   1263: aload_1
    //   1264: aload_0
    //   1265: getfield mUid : I
    //   1268: invokestatic deleteAllTypesForAlias : (Landroid/security/KeyStore;Ljava/lang/String;I)Z
    //   1271: pop
    //   1272: goto -> 1301
    //   1275: aload_0
    //   1276: getfield mKeyStore : Landroid/security/KeyStore;
    //   1279: aload_1
    //   1280: aload_0
    //   1281: getfield mUid : I
    //   1284: invokestatic deleteCertificateTypesForAlias : (Landroid/security/KeyStore;Ljava/lang/String;I)Z
    //   1287: pop
    //   1288: aload_0
    //   1289: getfield mKeyStore : Landroid/security/KeyStore;
    //   1292: aload_1
    //   1293: aload_0
    //   1294: getfield mUid : I
    //   1297: invokestatic deleteLegacyKeyForAlias : (Landroid/security/KeyStore;Ljava/lang/String;I)Z
    //   1300: pop
    //   1301: aload_2
    //   1302: athrow
    //   1303: astore_1
    //   1304: new java/security/KeyStoreException
    //   1307: dup
    //   1308: aload_1
    //   1309: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   1312: athrow
    //   1313: new java/security/KeyStoreException
    //   1316: dup
    //   1317: ldc_w 'Private key did not export any key material'
    //   1320: invokespecial <init> : (Ljava/lang/String;)V
    //   1323: athrow
    //   1324: new java/lang/StringBuilder
    //   1327: dup
    //   1328: invokespecial <init> : ()V
    //   1331: astore_1
    //   1332: aload_1
    //   1333: ldc_w 'Unsupported private key export format: '
    //   1336: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1339: pop
    //   1340: aload_1
    //   1341: aload #7
    //   1343: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1346: pop
    //   1347: aload_1
    //   1348: ldc_w '. Only private keys which export their key material in PKCS#8 format are supported.'
    //   1351: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1354: pop
    //   1355: new java/security/KeyStoreException
    //   1358: dup
    //   1359: aload_1
    //   1360: invokevirtual toString : ()Ljava/lang/String;
    //   1363: invokespecial <init> : (Ljava/lang/String;)V
    //   1366: athrow
    //   1367: astore_1
    //   1368: new java/security/KeyStoreException
    //   1371: dup
    //   1372: ldc_w 'Failed to encode certificate #0'
    //   1375: aload_1
    //   1376: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   1379: athrow
    //   1380: new java/security/KeyStoreException
    //   1383: dup
    //   1384: ldc_w 'Must supply at least one Certificate with PrivateKey'
    //   1387: invokespecial <init> : (Ljava/lang/String;)V
    //   1390: athrow
    //   1391: new java/lang/StringBuilder
    //   1394: dup
    //   1395: invokespecial <init> : ()V
    //   1398: astore_1
    //   1399: aload_1
    //   1400: ldc_w 'Unsupported protection parameter class:'
    //   1403: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1406: pop
    //   1407: aload_1
    //   1408: aload #4
    //   1410: invokevirtual getClass : ()Ljava/lang/Class;
    //   1413: invokevirtual getName : ()Ljava/lang/String;
    //   1416: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1419: pop
    //   1420: aload_1
    //   1421: ldc_w '. Supported: '
    //   1424: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1427: pop
    //   1428: aload_1
    //   1429: ldc android/security/keystore/KeyProtection
    //   1431: invokevirtual getName : ()Ljava/lang/String;
    //   1434: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1437: pop
    //   1438: aload_1
    //   1439: ldc_w ', '
    //   1442: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1445: pop
    //   1446: aload_1
    //   1447: ldc android/security/KeyStoreParameter
    //   1449: invokevirtual getName : ()Ljava/lang/String;
    //   1452: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1455: pop
    //   1456: new java/security/KeyStoreException
    //   1459: dup
    //   1460: aload_1
    //   1461: invokevirtual toString : ()Ljava/lang/String;
    //   1464: invokespecial <init> : (Ljava/lang/String;)V
    //   1467: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #357	-> 0
    //   #359	-> 6
    //   #360	-> 11
    //   #361	-> 23
    //   #362	-> 31
    //   #363	-> 37
    //   #364	-> 44
    //   #365	-> 52
    //   #367	-> 55
    //   #368	-> 70
    //   #369	-> 77
    //   #370	-> 89
    //   #373	-> 95
    //   #374	-> 103
    //   #373	-> 113
    //   #384	-> 113
    //   #389	-> 122
    //   #390	-> 129
    //   #391	-> 139
    //   #396	-> 154
    //   #401	-> 164
    //   #390	-> 176
    //   #397	-> 182
    //   #392	-> 216
    //   #406	-> 250
    //   #409	-> 259
    //   #417	-> 259
    //   #422	-> 265
    //   #423	-> 275
    //   #424	-> 278
    //   #426	-> 289
    //   #427	-> 304
    //   #430	-> 319
    //   #424	-> 319
    //   #428	-> 325
    //   #429	-> 326
    //   #437	-> 361
    //   #438	-> 366
    //   #439	-> 369
    //   #440	-> 380
    //   #441	-> 388
    //   #442	-> 402
    //   #443	-> 409
    //   #439	-> 415
    //   #445	-> 421
    //   #446	-> 424
    //   #450	-> 426
    //   #451	-> 433
    //   #453	-> 445
    //   #459	-> 448
    //   #460	-> 463
    //   #461	-> 474
    //   #465	-> 482
    //   #466	-> 482
    //   #467	-> 482
    //   #468	-> 482
    //   #462	-> 493
    //   #469	-> 541
    //   #471	-> 541
    //   #472	-> 549
    //   #480	-> 565
    //   #481	-> 573
    //   #485	-> 578
    //   #487	-> 587
    //   #489	-> 587
    //   #488	-> 594
    //   #487	-> 600
    //   #490	-> 610
    //   #491	-> 617
    //   #492	-> 617
    //   #491	-> 623
    //   #493	-> 632
    //   #494	-> 644
    //   #495	-> 644
    //   #494	-> 653
    //   #528	-> 665
    //   #498	-> 669
    //   #499	-> 669
    //   #498	-> 678
    //   #500	-> 687
    //   #502	-> 687
    //   #501	-> 693
    //   #503	-> 698
    //   #504	-> 705
    //   #505	-> 713
    //   #506	-> 733
    //   #507	-> 733
    //   #505	-> 741
    //   #509	-> 747
    //   #512	-> 767
    //   #504	-> 795
    //   #503	-> 798
    //   #518	-> 798
    //   #519	-> 807
    //   #520	-> 807
    //   #519	-> 816
    //   #521	-> 825
    //   #522	-> 832
    //   #523	-> 832
    //   #522	-> 838
    //   #524	-> 847
    //   #525	-> 847
    //   #524	-> 853
    //   #526	-> 862
    //   #527	-> 862
    //   #526	-> 868
    //   #530	-> 877
    //   #534	-> 887
    //   #537	-> 887
    //   #540	-> 892
    //   #541	-> 905
    //   #542	-> 915
    //   #550	-> 978
    //   #554	-> 984
    //   #551	-> 987
    //   #552	-> 991
    //   #577	-> 1005
    //   #556	-> 1013
    //   #557	-> 1026
    //   #561	-> 1039
    //   #563	-> 1097
    //   #569	-> 1103
    //   #571	-> 1151
    //   #575	-> 1157
    //   #577	-> 1157
    //   #578	-> 1161
    //   #579	-> 1166
    //   #581	-> 1182
    //   #582	-> 1195
    //   #586	-> 1208
    //   #572	-> 1209
    //   #573	-> 1213
    //   #564	-> 1227
    //   #565	-> 1231
    //   #577	-> 1245
    //   #578	-> 1254
    //   #579	-> 1259
    //   #581	-> 1275
    //   #582	-> 1288
    //   #585	-> 1301
    //   #528	-> 1303
    //   #529	-> 1304
    //   #482	-> 1313
    //   #472	-> 1324
    //   #473	-> 1324
    //   #407	-> 1367
    //   #408	-> 1368
    //   #385	-> 1380
    //   #377	-> 1391
    //   #378	-> 1407
    //   #379	-> 1428
    //   #380	-> 1446
    // Exception table:
    //   from	to	target	type
    //   250	259	1367	java/security/cert/CertificateEncodingException
    //   289	304	325	java/security/cert/CertificateEncodingException
    //   304	312	325	java/security/cert/CertificateEncodingException
    //   587	594	1303	java/lang/IllegalArgumentException
    //   587	594	1303	java/lang/IllegalStateException
    //   594	600	1303	java/lang/IllegalArgumentException
    //   594	600	1303	java/lang/IllegalStateException
    //   600	610	1303	java/lang/IllegalArgumentException
    //   600	610	1303	java/lang/IllegalStateException
    //   610	617	1303	java/lang/IllegalArgumentException
    //   610	617	1303	java/lang/IllegalStateException
    //   617	623	1303	java/lang/IllegalArgumentException
    //   617	623	1303	java/lang/IllegalStateException
    //   623	632	1303	java/lang/IllegalArgumentException
    //   623	632	1303	java/lang/IllegalStateException
    //   632	639	1303	java/lang/IllegalArgumentException
    //   632	639	1303	java/lang/IllegalStateException
    //   644	653	665	java/lang/IllegalArgumentException
    //   644	653	665	java/lang/IllegalStateException
    //   653	662	665	java/lang/IllegalArgumentException
    //   653	662	665	java/lang/IllegalStateException
    //   669	678	1303	java/lang/IllegalArgumentException
    //   669	678	1303	java/lang/IllegalStateException
    //   678	687	1303	java/lang/IllegalArgumentException
    //   678	687	1303	java/lang/IllegalStateException
    //   687	693	1303	java/lang/IllegalArgumentException
    //   687	693	1303	java/lang/IllegalStateException
    //   693	698	1303	java/lang/IllegalArgumentException
    //   693	698	1303	java/lang/IllegalStateException
    //   705	713	665	java/lang/IllegalArgumentException
    //   705	713	665	java/lang/IllegalStateException
    //   713	717	665	java/lang/IllegalArgumentException
    //   713	717	665	java/lang/IllegalStateException
    //   733	741	665	java/lang/IllegalArgumentException
    //   733	741	665	java/lang/IllegalStateException
    //   747	767	665	java/lang/IllegalArgumentException
    //   747	767	665	java/lang/IllegalStateException
    //   767	795	665	java/lang/IllegalArgumentException
    //   767	795	665	java/lang/IllegalStateException
    //   798	807	1303	java/lang/IllegalArgumentException
    //   798	807	1303	java/lang/IllegalStateException
    //   807	816	1303	java/lang/IllegalArgumentException
    //   807	816	1303	java/lang/IllegalStateException
    //   816	825	1303	java/lang/IllegalArgumentException
    //   816	825	1303	java/lang/IllegalStateException
    //   825	832	1303	java/lang/IllegalArgumentException
    //   825	832	1303	java/lang/IllegalStateException
    //   832	838	1303	java/lang/IllegalArgumentException
    //   832	838	1303	java/lang/IllegalStateException
    //   838	847	1303	java/lang/IllegalArgumentException
    //   838	847	1303	java/lang/IllegalStateException
    //   847	853	1303	java/lang/IllegalArgumentException
    //   847	853	1303	java/lang/IllegalStateException
    //   853	862	1303	java/lang/IllegalArgumentException
    //   853	862	1303	java/lang/IllegalStateException
    //   862	868	1303	java/lang/IllegalArgumentException
    //   862	868	1303	java/lang/IllegalStateException
    //   868	877	1303	java/lang/IllegalArgumentException
    //   868	877	1303	java/lang/IllegalStateException
    //   892	905	1009	finally
    //   905	915	1009	finally
    //   915	959	1009	finally
    //   959	978	1005	finally
    //   987	991	1005	finally
    //   991	1005	1005	finally
    //   1013	1026	1249	finally
    //   1026	1039	1249	finally
    //   1039	1083	1249	finally
    //   1083	1097	1245	finally
    //   1103	1151	1245	finally
    //   1209	1213	1245	finally
    //   1213	1227	1245	finally
    //   1227	1231	1245	finally
    //   1231	1245	1245	finally
  }
  
  private void setSecretKeyEntry(String paramString, SecretKey paramSecretKey, KeyStore.ProtectionParameter paramProtectionParameter) throws KeyStoreException {
    int[] arrayOfInt;
    if (paramProtectionParameter == null || paramProtectionParameter instanceof KeyProtection) {
      String str;
      KeyProtection keyProtection = (KeyProtection)paramProtectionParameter;
      if (paramSecretKey instanceof AndroidKeyStoreSecretKey) {
        String str1 = ((AndroidKeyStoreSecretKey)paramSecretKey).getAlias();
        if (str1 != null) {
          StringBuilder stringBuilder1;
          str = "USRPKEY_";
          if (!str1.startsWith("USRPKEY_")) {
            str = "USRSKEY_";
            if (!str1.startsWith("USRSKEY_")) {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("KeyStore-backed secret key has invalid alias: ");
              stringBuilder1.append(str1);
              throw new KeyStoreException(stringBuilder1.toString());
            } 
          } 
          str = str1.substring(str.length());
          if (stringBuilder1.equals(str)) {
            if (keyProtection == null)
              return; 
            throw new KeyStoreException("Modifying KeyStore-backed key using protection parameters not supported");
          } 
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Can only replace KeyStore-backed keys with same alias: ");
          stringBuilder2.append((String)stringBuilder1);
          stringBuilder2.append(" != ");
          stringBuilder2.append(str);
          throw new KeyStoreException(stringBuilder2.toString());
        } 
        throw new KeyStoreException("KeyStore-backed secret key does not have an alias");
      } 
      if (keyProtection != null) {
        String str1 = str.getFormat();
        if (str1 != null) {
          if ("RAW".equals(str1)) {
            byte[] arrayOfByte = str.getEncoded();
            if (arrayOfByte != null) {
              KeymasterArguments keymasterArguments = new KeymasterArguments();
              try {
                KeyStoreException keyStoreException;
                int[] arrayOfInt1;
                StringBuilder stringBuilder2;
                int i = KeyProperties.KeyAlgorithm.toKeymasterSecretKeyAlgorithm(str.getAlgorithm());
                keymasterArguments.addEnum(268435458, i);
                int j = 0;
                if (i == 128) {
                  try {
                    KeyStoreException keyStoreException1;
                    int m = KeyProperties.KeyAlgorithm.toKeymasterDigest(str.getAlgorithm());
                    if (m != -1) {
                      KeyStoreException keyStoreException2;
                      int[] arrayOfInt3 = new int[1];
                      arrayOfInt3[0] = m;
                      if (keyProtection.isDigestsSpecified()) {
                        int[] arrayOfInt4 = KeyProperties.Digest.allToKeymaster(keyProtection.getDigests());
                        if (arrayOfInt4.length != 1 || arrayOfInt4[0] != m) {
                          keyStoreException2 = new KeyStoreException();
                          StringBuilder stringBuilder3 = new StringBuilder();
                          this();
                          stringBuilder3.append("Unsupported digests specification: ");
                          stringBuilder3.append(Arrays.asList(keyProtection.getDigests()));
                          stringBuilder3.append(". Only ");
                          stringBuilder3.append(KeyProperties.Digest.fromKeymaster(m));
                          stringBuilder3.append(" supported for HMAC key algorithm ");
                          stringBuilder3.append(str.getAlgorithm());
                          this(stringBuilder3.toString());
                          throw keyStoreException2;
                        } 
                      } 
                      keyStoreException1 = keyStoreException2;
                    } else {
                      ProviderException providerException = new ProviderException();
                      StringBuilder stringBuilder3 = new StringBuilder();
                      this();
                      stringBuilder3.append("HMAC key algorithm digest unknown for key algorithm ");
                      stringBuilder3.append(keyStoreException1.getAlgorithm());
                      this(stringBuilder3.toString());
                      throw providerException;
                    } 
                  } catch (IllegalArgumentException|IllegalStateException null) {}
                } else {
                  boolean bool = keyProtection.isDigestsSpecified();
                  if (bool) {
                    arrayOfInt1 = KeyProperties.Digest.allToKeymaster(keyProtection.getDigests());
                  } else {
                    arrayOfInt1 = EmptyArray.INT;
                  } 
                } 
                keymasterArguments.addEnums(536870917, arrayOfInt1);
                int k = keyProtection.getPurposes();
                arrayOfInt = KeyProperties.BlockMode.allToKeymaster(keyProtection.getBlockModes());
                if ((k & 0x1) != 0 && keyProtection.isRandomizedEncryptionRequired())
                  for (int m = arrayOfInt.length; j < m; ) {
                    int n = arrayOfInt[j];
                    if (KeymasterUtils.isKeymasterBlockModeIndCpaCompatibleWithSymmetricCrypto(n)) {
                      j++;
                      continue;
                    } 
                    keyStoreException = new KeyStoreException();
                    stringBuilder2 = new StringBuilder();
                    this();
                    stringBuilder2.append("Randomized encryption (IND-CPA) required but may be violated by block mode: ");
                    stringBuilder2.append(KeyProperties.BlockMode.fromKeymaster(n));
                    stringBuilder2.append(". See KeyProtection documentation.");
                    this(stringBuilder2.toString());
                    throw keyStoreException;
                  }  
                int[] arrayOfInt2 = KeyProperties.Purpose.allToKeymaster(k);
                keymasterArguments.addEnums(536870913, arrayOfInt2);
                keymasterArguments.addEnums(536870916, arrayOfInt);
                if ((keyProtection.getSignaturePaddings()).length <= 0) {
                  String[] arrayOfString = keyProtection.getEncryptionPaddings();
                  int[] arrayOfInt3 = KeyProperties.EncryptionPadding.allToKeymaster(arrayOfString);
                  keymasterArguments.addEnums(536870918, arrayOfInt3);
                  KeymasterUtils.addUserAuthArgs(keymasterArguments, keyProtection);
                  KeymasterUtils.addMinMacLengthAuthorizationIfNecessary(keymasterArguments, i, arrayOfInt, (int[])stringBuilder2);
                  Date date = keyProtection.getKeyValidityStart();
                  keymasterArguments.addDateIfNotNull(1610613136, date);
                  date = keyProtection.getKeyValidityForOriginationEnd();
                  keymasterArguments.addDateIfNotNull(1610613137, date);
                  date = keyProtection.getKeyValidityForConsumptionEnd();
                  keymasterArguments.addDateIfNotNull(1610613138, date);
                  if ((k & 0x1) != 0 && !keyProtection.isRandomizedEncryptionRequired())
                    keymasterArguments.addBoolean(1879048199); 
                  j = 0;
                  if (keyProtection.isCriticalToDeviceEncryption())
                    j = 0x0 | 0x8; 
                  i = j;
                  if (keyProtection.isStrongBoxBacked())
                    i = j | 0x10; 
                  Credentials.deleteAllTypesForAlias(this.mKeyStore, (String)keyStoreException, this.mUid);
                  StringBuilder stringBuilder4 = new StringBuilder();
                  stringBuilder4.append("USRPKEY_");
                  stringBuilder4.append((String)keyStoreException);
                  String str2 = stringBuilder4.toString();
                  j = this.mKeyStore.importKey(str2, keymasterArguments, 3, arrayOfByte, this.mUid, i, new KeyCharacteristics());
                  if (j == 1)
                    return; 
                  StringBuilder stringBuilder3 = new StringBuilder();
                  stringBuilder3.append("Failed to import secret key. Keystore error code: ");
                  stringBuilder3.append(j);
                  throw new KeyStoreException(stringBuilder3.toString());
                } 
                try {
                  keyStoreException = new KeyStoreException();
                  this("Signature paddings not supported for symmetric keys");
                  throw keyStoreException;
                } catch (IllegalArgumentException|IllegalStateException null) {}
              } catch (IllegalArgumentException|IllegalStateException illegalArgumentException) {}
              throw new KeyStoreException(illegalArgumentException);
            } 
            throw new KeyStoreException("Key did not export its key material despite supporting RAW format export");
          } 
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Unsupported secret key material export format: ");
          stringBuilder1.append((String)arrayOfInt);
          throw new KeyStoreException(stringBuilder1.toString());
        } 
        throw new KeyStoreException("Only secret keys that export their key material are supported");
      } 
      throw new KeyStoreException("Protection parameters must be specified when importing a symmetric key");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported protection parameter class: ");
    stringBuilder.append(arrayOfInt.getClass().getName());
    stringBuilder.append(". Supported: ");
    stringBuilder.append(KeyProtection.class.getName());
    throw new KeyStoreException(stringBuilder.toString());
  }
  
  private void setWrappedKeyEntry(String paramString, WrappedKeyEntry paramWrappedKeyEntry, KeyStore.ProtectionParameter paramProtectionParameter) throws KeyStoreException {
    if (paramProtectionParameter == null) {
      byte[] arrayOfByte1 = new byte[32];
      KeymasterArguments keymasterArguments = new KeymasterArguments();
      String[] arrayOfString = paramWrappedKeyEntry.getTransformation().split("/");
      String str1 = arrayOfString[0];
      if ("RSA".equalsIgnoreCase(str1)) {
        keymasterArguments.addEnum(268435458, 1);
      } else if ("EC".equalsIgnoreCase(str1)) {
        keymasterArguments.addEnum(268435458, 1);
      } 
      if (arrayOfString.length > 1) {
        str1 = arrayOfString[1];
        if ("ECB".equalsIgnoreCase(str1)) {
          keymasterArguments.addEnums(536870916, new int[] { 1 });
        } else if ("CBC".equalsIgnoreCase(str1)) {
          keymasterArguments.addEnums(536870916, new int[] { 2 });
        } else if ("CTR".equalsIgnoreCase(str1)) {
          keymasterArguments.addEnums(536870916, new int[] { 3 });
        } else if ("GCM".equalsIgnoreCase(str1)) {
          keymasterArguments.addEnums(536870916, new int[] { 32 });
        } 
      } 
      if (arrayOfString.length > 2) {
        String str = arrayOfString[2];
        if (!"NoPadding".equalsIgnoreCase(str))
          if ("PKCS7Padding".equalsIgnoreCase(str)) {
            keymasterArguments.addEnums(536870918, new int[] { 64 });
          } else if ("PKCS1Padding".equalsIgnoreCase(str)) {
            keymasterArguments.addEnums(536870918, new int[] { 4 });
          } else if ("OAEPPadding".equalsIgnoreCase(str)) {
            keymasterArguments.addEnums(536870918, new int[] { 2 });
          }  
      } 
      KeyGenParameterSpec keyGenParameterSpec = (KeyGenParameterSpec)paramWrappedKeyEntry.getAlgorithmParameterSpec();
      if (keyGenParameterSpec.isDigestsSpecified()) {
        String str = keyGenParameterSpec.getDigests()[0];
        if (!"NONE".equalsIgnoreCase(str))
          if ("MD5".equalsIgnoreCase(str)) {
            keymasterArguments.addEnums(536870917, new int[] { 1 });
          } else if ("SHA-1".equalsIgnoreCase(str)) {
            keymasterArguments.addEnums(536870917, new int[] { 2 });
          } else if ("SHA-224".equalsIgnoreCase(str)) {
            keymasterArguments.addEnums(536870917, new int[] { 3 });
          } else if ("SHA-256".equalsIgnoreCase(str)) {
            keymasterArguments.addEnums(536870917, new int[] { 4 });
          } else if ("SHA-384".equalsIgnoreCase(str)) {
            keymasterArguments.addEnums(536870917, new int[] { 5 });
          } else if ("SHA-512".equalsIgnoreCase(str)) {
            keymasterArguments.addEnums(536870917, new int[] { 6 });
          }  
      } 
      KeyStore keyStore = this.mKeyStore;
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("USRPKEY_");
      stringBuilder1.append(paramString);
      paramString = stringBuilder1.toString();
      byte[] arrayOfByte2 = paramWrappedKeyEntry.getWrappedKeyBytes();
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("USRPKEY_");
      stringBuilder2.append(paramWrappedKeyEntry.getWrappingKeyAlias());
      String str2 = stringBuilder2.toString();
      long l = GateKeeper.getSecureUserId();
      int i = this.mUid;
      KeyCharacteristics keyCharacteristics = new KeyCharacteristics();
      i = keyStore.importWrappedKey(paramString, arrayOfByte2, str2, arrayOfByte1, keymasterArguments, l, 0L, i, keyCharacteristics);
      if (i != -100) {
        if (i == 1)
          return; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to import wrapped key. Keystore error code: ");
        stringBuilder.append(i);
        throw new KeyStoreException(stringBuilder.toString());
      } 
      throw new SecureKeyImportUnavailableException("Could not import wrapped key");
    } 
    throw new KeyStoreException("Protection parameters are specified inside wrapped keys");
  }
  
  public void engineSetKeyEntry(String paramString, byte[] paramArrayOfbyte, Certificate[] paramArrayOfCertificate) throws KeyStoreException {
    throw new KeyStoreException("Operation not supported because key encoding is unknown");
  }
  
  public void engineSetCertificateEntry(String paramString, Certificate paramCertificate) throws KeyStoreException {
    if (!isKeyEntry(paramString)) {
      if (paramCertificate != null)
        try {
          byte[] arrayOfByte = paramCertificate.getEncoded();
          KeyStore keyStore = this.mKeyStore;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("CACERT_");
          stringBuilder.append(paramString);
          if (keyStore.put(stringBuilder.toString(), arrayOfByte, this.mUid, 0))
            return; 
          throw new KeyStoreException("Couldn't insert certificate; is KeyStore initialized?");
        } catch (CertificateEncodingException certificateEncodingException) {
          throw new KeyStoreException(certificateEncodingException);
        }  
      throw new NullPointerException("cert == null");
    } 
    throw new KeyStoreException("Entry exists and is not a trusted certificate");
  }
  
  public void engineDeleteEntry(String paramString) throws KeyStoreException {
    if (Credentials.deleteAllTypesForAlias(this.mKeyStore, paramString, this.mUid))
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to delete entry: ");
    stringBuilder.append(paramString);
    throw new KeyStoreException(stringBuilder.toString());
  }
  
  private Set<String> getUniqueAliases() {
    String[] arrayOfString = this.mKeyStore.list("", this.mUid);
    if (arrayOfString == null)
      return new HashSet<>(); 
    HashSet<String> hashSet = new HashSet(arrayOfString.length);
    int i;
    byte b;
    for (i = arrayOfString.length, b = 0; b < i; ) {
      String str = arrayOfString[b];
      int j = str.indexOf('_');
      if (j == -1 || str.length() <= j) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("invalid alias: ");
        stringBuilder.append(str);
        Log.e("AndroidKeyStore", stringBuilder.toString());
      } else {
        hashSet.add(new String(str.substring(j + 1)));
      } 
      b++;
    } 
    return hashSet;
  }
  
  public Enumeration<String> engineAliases() {
    return Collections.enumeration(getUniqueAliases());
  }
  
  public boolean engineContainsAlias(String paramString) {
    if (paramString != null) {
      KeyStore keyStore = this.mKeyStore;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("USRPKEY_");
      stringBuilder.append(paramString);
      if (!keyStore.contains(stringBuilder.toString(), this.mUid)) {
        keyStore = this.mKeyStore;
        stringBuilder = new StringBuilder();
        stringBuilder.append("USRSKEY_");
        stringBuilder.append(paramString);
        String str = stringBuilder.toString();
        int i = this.mUid;
        if (!keyStore.contains(str, i)) {
          keyStore = this.mKeyStore;
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("USRCERT_");
          stringBuilder1.append(paramString);
          String str1 = stringBuilder1.toString();
          i = this.mUid;
          if (!keyStore.contains(str1, i)) {
            keyStore = this.mKeyStore;
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("CACERT_");
            stringBuilder2.append(paramString);
            paramString = stringBuilder2.toString();
            i = this.mUid;
            return keyStore.contains(paramString, i);
          } 
        } 
      } 
    } else {
      throw new NullPointerException("alias == null");
    } 
    return true;
  }
  
  public int engineSize() {
    return getUniqueAliases().size();
  }
  
  public boolean engineIsKeyEntry(String paramString) {
    return isKeyEntry(paramString);
  }
  
  private boolean isKeyEntry(String paramString) {
    KeyStore keyStore = this.mKeyStore;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("USRPKEY_");
    stringBuilder.append(paramString);
    if (!keyStore.contains(stringBuilder.toString(), this.mUid)) {
      keyStore = this.mKeyStore;
      stringBuilder = new StringBuilder();
      stringBuilder.append("USRSKEY_");
      stringBuilder.append(paramString);
      paramString = stringBuilder.toString();
      int i = this.mUid;
      return 
        keyStore.contains(paramString, i);
    } 
    return true;
  }
  
  private boolean isCertificateEntry(String paramString) {
    if (paramString != null) {
      KeyStore keyStore = this.mKeyStore;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("CACERT_");
      stringBuilder.append(paramString);
      return keyStore.contains(stringBuilder.toString(), this.mUid);
    } 
    throw new NullPointerException("alias == null");
  }
  
  public boolean engineIsCertificateEntry(String paramString) {
    boolean bool;
    if (!isKeyEntry(paramString) && isCertificateEntry(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String engineGetCertificateAlias(Certificate paramCertificate) {
    if (paramCertificate == null)
      return null; 
    if (!"X.509".equalsIgnoreCase(paramCertificate.getType()))
      return null; 
    try {
      byte[] arrayOfByte = paramCertificate.getEncoded();
      if (arrayOfByte == null)
        return null; 
      HashSet<String> hashSet = new HashSet();
      String[] arrayOfString1 = this.mKeyStore.list("USRCERT_", this.mUid);
      byte b = 0;
      if (arrayOfString1 != null) {
        int i;
        byte b1;
        for (i = arrayOfString1.length, b1 = 0; b1 < i; ) {
          String str = arrayOfString1[b1];
          KeyStore keyStore = this.mKeyStore;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("USRCERT_");
          stringBuilder.append(str);
          byte[] arrayOfByte1 = keyStore.get(stringBuilder.toString(), this.mUid);
          if (arrayOfByte1 != null) {
            hashSet.add(str);
            if (Arrays.equals(arrayOfByte1, arrayOfByte))
              return str; 
          } 
          b1++;
        } 
      } 
      String[] arrayOfString2 = this.mKeyStore.list("CACERT_", this.mUid);
      if (arrayOfString1 != null) {
        int i;
        byte b1;
        for (i = arrayOfString2.length, b1 = b; b1 < i; ) {
          String str = arrayOfString2[b1];
          if (!hashSet.contains(str)) {
            KeyStore keyStore = this.mKeyStore;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("CACERT_");
            stringBuilder.append(str);
            byte[] arrayOfByte1 = keyStore.get(stringBuilder.toString(), this.mUid);
            if (arrayOfByte1 != null)
              if (Arrays.equals(arrayOfByte1, arrayOfByte))
                return str;  
          } 
          b1++;
        } 
      } 
      return null;
    } catch (CertificateEncodingException certificateEncodingException) {
      return null;
    } 
  }
  
  public void engineStore(OutputStream paramOutputStream, char[] paramArrayOfchar) throws IOException, NoSuchAlgorithmException, CertificateException {
    throw new UnsupportedOperationException("Can not serialize AndroidKeyStore to OutputStream");
  }
  
  public void engineLoad(InputStream paramInputStream, char[] paramArrayOfchar) throws IOException, NoSuchAlgorithmException, CertificateException {
    if (paramInputStream == null) {
      if (paramArrayOfchar == null) {
        this.mKeyStore = KeyStore.getInstance();
        this.mUid = -1;
        return;
      } 
      throw new IllegalArgumentException("password not supported");
    } 
    throw new IllegalArgumentException("InputStream not supported");
  }
  
  public void engineLoad(KeyStore.LoadStoreParameter paramLoadStoreParameter) throws IOException, NoSuchAlgorithmException, CertificateException {
    int i = -1;
    if (paramLoadStoreParameter != null)
      if (paramLoadStoreParameter instanceof AndroidKeyStoreLoadStoreParameter) {
        i = ((AndroidKeyStoreLoadStoreParameter)paramLoadStoreParameter).getUid();
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported param type: ");
        stringBuilder.append(paramLoadStoreParameter.getClass());
        throw new IllegalArgumentException(stringBuilder.toString());
      }  
    this.mKeyStore = KeyStore.getInstance();
    this.mUid = i;
  }
  
  public void engineSetEntry(String paramString, KeyStore.Entry paramEntry, KeyStore.ProtectionParameter paramProtectionParameter) throws KeyStoreException {
    if (paramEntry != null) {
      Credentials.deleteAllTypesForAlias(this.mKeyStore, paramString, this.mUid);
      if (paramEntry instanceof KeyStore.TrustedCertificateEntry) {
        paramEntry = paramEntry;
        engineSetCertificateEntry(paramString, paramEntry.getTrustedCertificate());
        return;
      } 
      if (paramEntry instanceof KeyStore.PrivateKeyEntry) {
        paramEntry = paramEntry;
        setPrivateKeyEntry(paramString, paramEntry.getPrivateKey(), paramEntry.getCertificateChain(), paramProtectionParameter);
      } else if (paramEntry instanceof KeyStore.SecretKeyEntry) {
        paramEntry = paramEntry;
        setSecretKeyEntry(paramString, paramEntry.getSecretKey(), paramProtectionParameter);
      } else {
        if (paramEntry instanceof WrappedKeyEntry) {
          paramEntry = paramEntry;
          setWrappedKeyEntry(paramString, (WrappedKeyEntry)paramEntry, paramProtectionParameter);
          return;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Entry must be a PrivateKeyEntry, SecretKeyEntry or TrustedCertificateEntry; was ");
        stringBuilder.append(paramEntry);
        throw new KeyStoreException(stringBuilder.toString());
      } 
      return;
    } 
    throw new KeyStoreException("entry == null");
  }
  
  class KeyStoreX509Certificate extends DelegatingX509Certificate {
    private final String mPrivateKeyAlias;
    
    private final int mPrivateKeyUid;
    
    KeyStoreX509Certificate(AndroidKeyStoreSpi this$0, int param1Int, X509Certificate param1X509Certificate) {
      super(param1X509Certificate);
      this.mPrivateKeyAlias = (String)this$0;
      this.mPrivateKeyUid = param1Int;
    }
    
    public PublicKey getPublicKey() {
      PublicKey publicKey = super.getPublicKey();
      String str1 = this.mPrivateKeyAlias;
      int i = this.mPrivateKeyUid;
      String str2 = publicKey.getAlgorithm();
      byte[] arrayOfByte = publicKey.getEncoded();
      return AndroidKeyStoreProvider.getAndroidKeyStorePublicKey(str1, i, str2, arrayOfByte);
    }
  }
}
