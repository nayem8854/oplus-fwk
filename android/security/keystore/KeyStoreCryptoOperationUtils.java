package android.security.keystore;

import android.security.KeyStore;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.SecureRandom;
import libcore.util.EmptyArray;

abstract class KeyStoreCryptoOperationUtils {
  private static volatile SecureRandom sRng;
  
  static InvalidKeyException getInvalidKeyExceptionForInit(KeyStore paramKeyStore, AndroidKeyStoreKey paramAndroidKeyStoreKey, int paramInt) {
    if (paramInt == 1)
      return null; 
    InvalidKeyException invalidKeyException = paramKeyStore.getInvalidKeyException(paramAndroidKeyStoreKey.getAlias(), paramAndroidKeyStoreKey.getUid(), paramInt);
    if (paramInt == 15)
      if (invalidKeyException instanceof UserNotAuthenticatedException)
        return null;  
    return invalidKeyException;
  }
  
  public static GeneralSecurityException getExceptionForCipherInit(KeyStore paramKeyStore, AndroidKeyStoreKey paramAndroidKeyStoreKey, int paramInt) {
    if (paramInt == 1)
      return null; 
    if (paramInt != -55) {
      if (paramInt != -52)
        return getInvalidKeyExceptionForInit(paramKeyStore, paramAndroidKeyStoreKey, paramInt); 
      return new InvalidAlgorithmParameterException("Invalid IV");
    } 
    return new InvalidAlgorithmParameterException("Caller-provided IV not permitted");
  }
  
  static byte[] getRandomBytesToMixIntoKeystoreRng(SecureRandom paramSecureRandom, int paramInt) {
    if (paramInt <= 0)
      return EmptyArray.BYTE; 
    SecureRandom secureRandom = paramSecureRandom;
    if (paramSecureRandom == null)
      secureRandom = getRng(); 
    byte[] arrayOfByte = new byte[paramInt];
    secureRandom.nextBytes(arrayOfByte);
    return arrayOfByte;
  }
  
  private static SecureRandom getRng() {
    if (sRng == null)
      sRng = new SecureRandom(); 
    return sRng;
  }
}
