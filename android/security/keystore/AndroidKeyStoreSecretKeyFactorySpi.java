package android.security.keystore;

import android.security.GateKeeper;
import android.security.KeyStore;
import android.security.keymaster.KeyCharacteristics;
import android.security.keymaster.KeymasterArguments;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.ProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactorySpi;
import javax.crypto.spec.SecretKeySpec;

public class AndroidKeyStoreSecretKeyFactorySpi extends SecretKeyFactorySpi {
  private final KeyStore mKeyStore = KeyStore.getInstance();
  
  protected KeySpec engineGetKeySpec(SecretKey paramSecretKey, Class paramClass) throws InvalidKeySpecException {
    if (paramClass != null) {
      String str;
      StringBuilder stringBuilder;
      if (!(paramSecretKey instanceof AndroidKeyStoreSecretKey)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Only Android KeyStore secret keys supported: ");
        if (paramSecretKey != null) {
          str = paramSecretKey.getClass().getName();
        } else {
          str = "null";
        } 
        stringBuilder.append(str);
        throw new InvalidKeySpecException(stringBuilder.toString());
      } 
      if (!SecretKeySpec.class.isAssignableFrom((Class<?>)stringBuilder)) {
        String str1;
        if (KeyInfo.class.equals(stringBuilder)) {
          StringBuilder stringBuilder2;
          AndroidKeyStoreKey androidKeyStoreKey = (AndroidKeyStoreKey)str;
          str1 = androidKeyStoreKey.getAlias();
          if (str1.startsWith("USRPKEY_")) {
            str = str1.substring("USRPKEY_".length());
          } else {
            if (str1.startsWith("USRSKEY_")) {
              str = str1.substring("USRSKEY_".length());
              return getKeyInfo(this.mKeyStore, str, str1, androidKeyStoreKey.getUid());
            } 
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Invalid key alias: ");
            stringBuilder2.append(str1);
            throw new InvalidKeySpecException(stringBuilder2.toString());
          } 
          return getKeyInfo(this.mKeyStore, (String)stringBuilder2, str1, androidKeyStoreKey.getUid());
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Unsupported key spec: ");
        stringBuilder1.append(str1.getName());
        throw new InvalidKeySpecException(stringBuilder1.toString());
      } 
      throw new InvalidKeySpecException("Key material export of Android KeyStore keys is not supported");
    } 
    throw new InvalidKeySpecException("keySpecClass == null");
  }
  
  static KeyInfo getKeyInfo(KeyStore paramKeyStore, String paramString1, String paramString2, int paramInt) {
    KeyCharacteristics keyCharacteristics = new KeyCharacteristics();
    paramInt = paramKeyStore.getKeyCharacteristics(paramString2, null, null, paramInt, keyCharacteristics);
    boolean bool = true;
    if (paramInt == 1) {
      try {
        boolean bool1 = keyCharacteristics.hwEnforced.containsTag(268436158);
        if (bool1) {
          try {
            KeymasterArguments keymasterArguments = keyCharacteristics.hwEnforced;
            paramInt = keymasterArguments.getEnum(268436158, -1);
            paramInt = KeyProperties.Origin.fromKeymaster(paramInt);
            bool1 = true;
          } catch (IllegalArgumentException null) {}
        } else if (keyCharacteristics.swEnforced.containsTag(268436158)) {
          KeymasterArguments keymasterArguments = keyCharacteristics.swEnforced;
          paramInt = keymasterArguments.getEnum(268436158, -1);
          paramInt = KeyProperties.Origin.fromKeymaster(paramInt);
          bool1 = false;
        } else {
          ProviderException providerException = new ProviderException();
          this("Key origin not available");
          throw providerException;
        } 
        long l = keyCharacteristics.getUnsignedInt(805306371, -1L);
        if (l != -1L) {
          if (l <= 2147483647L) {
            ProviderException providerException;
            int i = (int)l;
            List<Integer> list1 = keyCharacteristics.getEnums(536870913);
            int j = KeyProperties.Purpose.allFromKeymaster(list1);
            ArrayList<String> arrayList2 = new ArrayList();
            this();
            ArrayList<String> arrayList1 = new ArrayList();
            this();
            Iterator<Integer> iterator = keyCharacteristics.getEnums(536870918).iterator();
            while (true) {
              boolean bool2 = iterator.hasNext();
              if (bool2) {
                int i1 = ((Integer)iterator.next()).intValue();
                try {
                  String str = KeyProperties.EncryptionPadding.fromKeymaster(i1);
                  arrayList2.add(str);
                } catch (IllegalArgumentException illegalArgumentException1) {
                  try {
                    String str = KeyProperties.SignaturePadding.fromKeymaster(i1);
                    arrayList1.add(str);
                  } catch (IllegalArgumentException illegalArgumentException2) {
                    providerException = new ProviderException();
                    StringBuilder stringBuilder2 = new StringBuilder();
                    this();
                    stringBuilder2.append("Unsupported encryption padding: ");
                    stringBuilder2.append(i1);
                    this(stringBuilder2.toString());
                    throw providerException;
                  } 
                } 
                continue;
              } 
              break;
            } 
            String[] arrayOfString1 = arrayList2.<String>toArray(new String[arrayList2.size()]);
            String[] arrayOfString2 = arrayList1.<String>toArray(new String[arrayList1.size()]);
            List<Integer> list2 = keyCharacteristics.getEnums(536870917);
            String[] arrayOfString3 = KeyProperties.Digest.allFromKeymaster(list2);
            List<Integer> list3 = keyCharacteristics.getEnums(536870916);
            String[] arrayOfString4 = KeyProperties.BlockMode.allFromKeymaster(list3);
            KeymasterArguments keymasterArguments = keyCharacteristics.swEnforced;
            int m = keymasterArguments.getEnum(268435960, 0);
            keymasterArguments = keyCharacteristics.hwEnforced;
            int k = keymasterArguments.getEnum(268435960, 0);
            List<BigInteger> list = keyCharacteristics.getUnsignedLongs(-1610612234);
            Date date1 = keyCharacteristics.getDate(1610613136);
            Date date2 = keyCharacteristics.getDate(1610613137);
            Date date3 = keyCharacteristics.getDate(1610613138);
            int n = keyCharacteristics.getBoolean(1879048695) ^ true;
            l = keyCharacteristics.getUnsignedInt(805306873, 0L);
            if (l <= 2147483647L) {
              boolean bool2;
              if (n != 0 && k != 0 && m == 0) {
                bool2 = true;
              } else {
                bool2 = false;
              } 
              KeymasterArguments keymasterArguments1 = keyCharacteristics.hwEnforced;
              boolean bool3 = keymasterArguments1.getBoolean(1879048698);
              keymasterArguments1 = keyCharacteristics.hwEnforced;
              boolean bool4 = keymasterArguments1.getBoolean(1879048699);
              if (m == 2 || k == 2) {
                if (list == null || 
                  list.isEmpty() || 
                  list.contains(getGateKeeperSecureUserId()))
                  bool = false; 
                boolean bool6 = keyCharacteristics.hwEnforced.getBoolean(1879048700);
                return new KeyInfo((String)providerException, bool1, paramInt, i, date1, date2, date3, j, arrayOfString1, arrayOfString2, arrayOfString3, arrayOfString4, n, (int)l, k, bool2, bool3, bool4, bool, bool6);
              } 
              bool = false;
              boolean bool5 = keyCharacteristics.hwEnforced.getBoolean(1879048700);
              return new KeyInfo((String)providerException, bool1, paramInt, i, date1, date2, date3, j, arrayOfString1, arrayOfString2, arrayOfString3, arrayOfString4, n, (int)l, k, bool2, bool3, bool4, bool, bool5);
            } 
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("User authentication timeout validity too long: ");
            stringBuilder1.append(l);
            stringBuilder1.append(" seconds");
            throw new ProviderException(stringBuilder1.toString());
          } 
          try {
            ProviderException providerException = new ProviderException();
            StringBuilder stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append("Key too large: ");
            stringBuilder1.append(l);
            stringBuilder1.append(" bits");
            this(stringBuilder1.toString());
            throw providerException;
          } catch (IllegalArgumentException null) {}
        } else {
          ProviderException providerException = new ProviderException();
          this("Key size not available");
          throw providerException;
        } 
      } catch (IllegalArgumentException illegalArgumentException) {}
      throw new ProviderException("Unsupported key characteristic", illegalArgumentException);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to obtain information about key. Keystore error: ");
    stringBuilder.append(paramInt);
    throw new ProviderException(stringBuilder.toString());
  }
  
  private static BigInteger getGateKeeperSecureUserId() throws ProviderException {
    try {
      return BigInteger.valueOf(GateKeeper.getSecureUserId());
    } catch (IllegalStateException illegalStateException) {
      throw new ProviderException("Failed to get GateKeeper secure user ID", illegalStateException);
    } 
  }
  
  protected SecretKey engineGenerateSecret(KeySpec paramKeySpec) throws InvalidKeySpecException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("To generate secret key in Android Keystore, use KeyGenerator initialized with ");
    stringBuilder.append(KeyGenParameterSpec.class.getName());
    throw new InvalidKeySpecException(stringBuilder.toString());
  }
  
  protected SecretKey engineTranslateKey(SecretKey paramSecretKey) throws InvalidKeyException {
    if (paramSecretKey != null) {
      if (paramSecretKey instanceof AndroidKeyStoreSecretKey)
        return paramSecretKey; 
      throw new InvalidKeyException("To import a secret key into Android Keystore, use KeyStore.setEntry");
    } 
    throw new InvalidKeyException("key == null");
  }
}
