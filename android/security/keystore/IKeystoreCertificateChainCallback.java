package android.security.keystore;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.security.keymaster.KeymasterCertificateChain;

public interface IKeystoreCertificateChainCallback extends IInterface {
  void onFinished(KeystoreResponse paramKeystoreResponse, KeymasterCertificateChain paramKeymasterCertificateChain) throws RemoteException;
  
  class Default implements IKeystoreCertificateChainCallback {
    public void onFinished(KeystoreResponse param1KeystoreResponse, KeymasterCertificateChain param1KeymasterCertificateChain) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IKeystoreCertificateChainCallback {
    private static final String DESCRIPTOR = "android.security.keystore.IKeystoreCertificateChainCallback";
    
    static final int TRANSACTION_onFinished = 1;
    
    public Stub() {
      attachInterface(this, "android.security.keystore.IKeystoreCertificateChainCallback");
    }
    
    public static IKeystoreCertificateChainCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.keystore.IKeystoreCertificateChainCallback");
      if (iInterface != null && iInterface instanceof IKeystoreCertificateChainCallback)
        return (IKeystoreCertificateChainCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onFinished";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.security.keystore.IKeystoreCertificateChainCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.security.keystore.IKeystoreCertificateChainCallback");
      if (param1Parcel1.readInt() != 0) {
        KeystoreResponse keystoreResponse = KeystoreResponse.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        KeymasterCertificateChain keymasterCertificateChain = KeymasterCertificateChain.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onFinished((KeystoreResponse)param1Parcel2, (KeymasterCertificateChain)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IKeystoreCertificateChainCallback {
      public static IKeystoreCertificateChainCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.keystore.IKeystoreCertificateChainCallback";
      }
      
      public void onFinished(KeystoreResponse param2KeystoreResponse, KeymasterCertificateChain param2KeymasterCertificateChain) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.security.keystore.IKeystoreCertificateChainCallback");
          if (param2KeystoreResponse != null) {
            parcel.writeInt(1);
            param2KeystoreResponse.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2KeymasterCertificateChain != null) {
            parcel.writeInt(1);
            param2KeymasterCertificateChain.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IKeystoreCertificateChainCallback.Stub.getDefaultImpl() != null) {
            IKeystoreCertificateChainCallback.Stub.getDefaultImpl().onFinished(param2KeystoreResponse, param2KeymasterCertificateChain);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IKeystoreCertificateChainCallback param1IKeystoreCertificateChainCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IKeystoreCertificateChainCallback != null) {
          Proxy.sDefaultImpl = param1IKeystoreCertificateChainCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IKeystoreCertificateChainCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
