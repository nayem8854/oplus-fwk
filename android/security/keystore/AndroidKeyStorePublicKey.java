package android.security.keystore;

import java.security.PublicKey;
import java.util.Arrays;

public class AndroidKeyStorePublicKey extends AndroidKeyStoreKey implements PublicKey {
  private final byte[] mEncoded;
  
  public AndroidKeyStorePublicKey(String paramString1, int paramInt, String paramString2, byte[] paramArrayOfbyte) {
    super(paramString1, paramInt, paramString2);
    this.mEncoded = ArrayUtils.cloneIfNotEmpty(paramArrayOfbyte);
  }
  
  public String getFormat() {
    return "X.509";
  }
  
  public byte[] getEncoded() {
    return ArrayUtils.cloneIfNotEmpty(this.mEncoded);
  }
  
  public int hashCode() {
    int i = super.hashCode();
    int j = Arrays.hashCode(this.mEncoded);
    return i * 31 + j;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (!super.equals(paramObject))
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (!Arrays.equals(this.mEncoded, ((AndroidKeyStorePublicKey)paramObject).mEncoded))
      return false; 
    return true;
  }
}
