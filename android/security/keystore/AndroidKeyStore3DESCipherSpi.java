package android.security.keystore;

import android.security.keymaster.KeymasterArguments;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.ProviderException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidParameterSpecException;
import java.util.Arrays;
import javax.crypto.spec.IvParameterSpec;

public class AndroidKeyStore3DESCipherSpi extends AndroidKeyStoreCipherSpiBase {
  private static final int BLOCK_SIZE_BYTES = 8;
  
  private byte[] mIv;
  
  private boolean mIvHasBeenUsed;
  
  private final boolean mIvRequired;
  
  private final int mKeymasterBlockMode;
  
  private final int mKeymasterPadding;
  
  AndroidKeyStore3DESCipherSpi(int paramInt1, int paramInt2, boolean paramBoolean) {
    this.mKeymasterBlockMode = paramInt1;
    this.mKeymasterPadding = paramInt2;
    this.mIvRequired = paramBoolean;
  }
  
  class ECB extends AndroidKeyStore3DESCipherSpi {
    protected ECB(AndroidKeyStore3DESCipherSpi this$0) {
      super(1, this$0, false);
    }
    
    class NoPadding extends ECB {
      public NoPadding() {
        super(1);
      }
    }
    
    class PKCS7Padding extends ECB {
      public PKCS7Padding() {
        super(64);
      }
    }
  }
  
  class CBC extends AndroidKeyStore3DESCipherSpi {
    protected CBC(AndroidKeyStore3DESCipherSpi this$0) {
      super(2, this$0, true);
    }
    
    class NoPadding extends CBC {
      public NoPadding() {
        super(1);
      }
    }
    
    class PKCS7Padding extends CBC {
      public PKCS7Padding() {
        super(64);
      }
    }
  }
  
  protected void initKey(int paramInt, Key paramKey) throws InvalidKeyException {
    String str;
    if (!(paramKey instanceof AndroidKeyStoreSecretKey)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Unsupported key: ");
      if (paramKey != null) {
        str = paramKey.getClass().getName();
      } else {
        str = "null";
      } 
      stringBuilder1.append(str);
      throw new InvalidKeyException(stringBuilder1.toString());
    } 
    if ("DESede".equalsIgnoreCase(str.getAlgorithm())) {
      setKey((AndroidKeyStoreSecretKey)str);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported key algorithm: ");
    stringBuilder.append(str.getAlgorithm());
    stringBuilder.append(". Only ");
    stringBuilder.append("DESede");
    stringBuilder.append(" supported");
    throw new InvalidKeyException(stringBuilder.toString());
  }
  
  protected int engineGetBlockSize() {
    return 8;
  }
  
  protected int engineGetOutputSize(int paramInt) {
    return paramInt + 24;
  }
  
  protected final byte[] engineGetIV() {
    return ArrayUtils.cloneIfNotEmpty(this.mIv);
  }
  
  protected AlgorithmParameters engineGetParameters() {
    if (!this.mIvRequired)
      return null; 
    byte[] arrayOfByte = this.mIv;
    if (arrayOfByte != null && arrayOfByte.length > 0)
      try {
        AlgorithmParameters algorithmParameters = AlgorithmParameters.getInstance("DESede");
        IvParameterSpec ivParameterSpec = new IvParameterSpec();
        this(this.mIv);
        algorithmParameters.init(ivParameterSpec);
        return algorithmParameters;
      } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
        throw new ProviderException("Failed to obtain 3DES AlgorithmParameters", noSuchAlgorithmException);
      } catch (InvalidParameterSpecException invalidParameterSpecException) {
        throw new ProviderException("Failed to initialize 3DES AlgorithmParameters with an IV", invalidParameterSpecException);
      }  
    return null;
  }
  
  protected void initAlgorithmSpecificParameters() throws InvalidKeyException {
    if (!this.mIvRequired)
      return; 
    if (isEncrypting())
      return; 
    throw new InvalidKeyException("IV required when decrypting. Use IvParameterSpec or AlgorithmParameters to provide it.");
  }
  
  protected void initAlgorithmSpecificParameters(AlgorithmParameterSpec paramAlgorithmParameterSpec) throws InvalidAlgorithmParameterException {
    if (!this.mIvRequired) {
      if (paramAlgorithmParameterSpec == null)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported parameters: ");
      stringBuilder.append(paramAlgorithmParameterSpec);
      throw new InvalidAlgorithmParameterException(stringBuilder.toString());
    } 
    if (paramAlgorithmParameterSpec == null) {
      if (isEncrypting())
        return; 
      throw new InvalidAlgorithmParameterException("IvParameterSpec must be provided when decrypting");
    } 
    if (paramAlgorithmParameterSpec instanceof IvParameterSpec) {
      byte[] arrayOfByte = ((IvParameterSpec)paramAlgorithmParameterSpec).getIV();
      if (arrayOfByte != null)
        return; 
      throw new InvalidAlgorithmParameterException("Null IV in IvParameterSpec");
    } 
    throw new InvalidAlgorithmParameterException("Only IvParameterSpec supported");
  }
  
  protected void initAlgorithmSpecificParameters(AlgorithmParameters paramAlgorithmParameters) throws InvalidAlgorithmParameterException {
    byte[] arrayOfByte;
    if (!this.mIvRequired) {
      if (paramAlgorithmParameters == null)
        return; 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Unsupported parameters: ");
      stringBuilder1.append(paramAlgorithmParameters);
      throw new InvalidAlgorithmParameterException(stringBuilder1.toString());
    } 
    if (paramAlgorithmParameters == null) {
      if (isEncrypting())
        return; 
      throw new InvalidAlgorithmParameterException("IV required when decrypting. Use IvParameterSpec or AlgorithmParameters to provide it.");
    } 
    if ("DESede".equalsIgnoreCase(paramAlgorithmParameters.getAlgorithm()))
      try {
        IvParameterSpec ivParameterSpec = paramAlgorithmParameters.<IvParameterSpec>getParameterSpec(IvParameterSpec.class);
        this.mIv = arrayOfByte = ivParameterSpec.getIV();
        if (arrayOfByte != null)
          return; 
        throw new InvalidAlgorithmParameterException("Null IV in AlgorithmParameters");
      } catch (InvalidParameterSpecException invalidParameterSpecException) {
        if (isEncrypting()) {
          this.mIv = null;
          return;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("IV required when decrypting, but not found in parameters: ");
        stringBuilder1.append(arrayOfByte);
        throw new InvalidAlgorithmParameterException(stringBuilder1.toString(), invalidParameterSpecException);
      }  
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported AlgorithmParameters algorithm: ");
    stringBuilder.append(arrayOfByte.getAlgorithm());
    stringBuilder.append(". Supported: DESede");
    throw new InvalidAlgorithmParameterException(stringBuilder.toString());
  }
  
  protected final int getAdditionalEntropyAmountForBegin() {
    if (this.mIvRequired && this.mIv == null && isEncrypting())
      return 8; 
    return 0;
  }
  
  protected int getAdditionalEntropyAmountForFinish() {
    return 0;
  }
  
  protected void addAlgorithmSpecificParametersToBegin(KeymasterArguments paramKeymasterArguments) {
    if (!isEncrypting() || !this.mIvRequired || !this.mIvHasBeenUsed) {
      paramKeymasterArguments.addEnum(268435458, 33);
      paramKeymasterArguments.addEnum(536870916, this.mKeymasterBlockMode);
      paramKeymasterArguments.addEnum(536870918, this.mKeymasterPadding);
      if (this.mIvRequired) {
        byte[] arrayOfByte = this.mIv;
        if (arrayOfByte != null)
          paramKeymasterArguments.addBytes(-1879047191, arrayOfByte); 
      } 
      return;
    } 
    throw new IllegalStateException("IV has already been used. Reusing IV in encryption mode violates security best practices.");
  }
  
  protected void loadAlgorithmSpecificParametersFromBeginResult(KeymasterArguments paramKeymasterArguments) {
    this.mIvHasBeenUsed = true;
    byte[] arrayOfByte2 = paramKeymasterArguments.getBytes(-1879047191, null);
    byte[] arrayOfByte1 = arrayOfByte2;
    if (arrayOfByte2 != null) {
      arrayOfByte1 = arrayOfByte2;
      if (arrayOfByte2.length == 0)
        arrayOfByte1 = null; 
    } 
    if (this.mIvRequired) {
      arrayOfByte2 = this.mIv;
      if (arrayOfByte2 == null) {
        this.mIv = arrayOfByte1;
      } else if (arrayOfByte1 != null && !Arrays.equals(arrayOfByte1, arrayOfByte2)) {
        throw new ProviderException("IV in use differs from provided IV");
      } 
    } else if (arrayOfByte1 != null) {
      throw new ProviderException("IV in use despite IV not being used by this transformation");
    } 
  }
  
  protected final void resetAll() {
    this.mIv = null;
    this.mIvHasBeenUsed = false;
    super.resetAll();
  }
}
