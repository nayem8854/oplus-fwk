package android.security.keystore;

import android.annotation.SystemApi;
import android.os.SystemProperties;
import android.security.KeyStore;
import android.security.keymaster.ExportResult;
import android.security.keymaster.KeyCharacteristics;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.ProviderException;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureSpi;
import java.security.UnrecoverableKeyException;
import java.security.interfaces.ECKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.CipherSpi;
import javax.crypto.Mac;
import javax.crypto.MacSpi;

@SystemApi
public class AndroidKeyStoreProvider extends Provider {
  private static final String DESEDE_SYSTEM_PROPERTY = "ro.hardware.keystore_desede";
  
  private static final String PACKAGE_NAME = "android.security.keystore";
  
  private static final String PROVIDER_NAME = "AndroidKeyStore";
  
  public AndroidKeyStoreProvider() {
    super("AndroidKeyStore", 1.0D, "Android KeyStore security provider");
    boolean bool = "true".equals(SystemProperties.get("ro.hardware.keystore_desede"));
    put("KeyStore.AndroidKeyStore", "android.security.keystore.AndroidKeyStoreSpi");
    put("KeyPairGenerator.EC", "android.security.keystore.AndroidKeyStoreKeyPairGeneratorSpi$EC");
    put("KeyPairGenerator.RSA", "android.security.keystore.AndroidKeyStoreKeyPairGeneratorSpi$RSA");
    putKeyFactoryImpl("EC");
    putKeyFactoryImpl("RSA");
    put("KeyGenerator.AES", "android.security.keystore.AndroidKeyStoreKeyGeneratorSpi$AES");
    put("KeyGenerator.HmacSHA1", "android.security.keystore.AndroidKeyStoreKeyGeneratorSpi$HmacSHA1");
    put("KeyGenerator.HmacSHA224", "android.security.keystore.AndroidKeyStoreKeyGeneratorSpi$HmacSHA224");
    put("KeyGenerator.HmacSHA256", "android.security.keystore.AndroidKeyStoreKeyGeneratorSpi$HmacSHA256");
    put("KeyGenerator.HmacSHA384", "android.security.keystore.AndroidKeyStoreKeyGeneratorSpi$HmacSHA384");
    put("KeyGenerator.HmacSHA512", "android.security.keystore.AndroidKeyStoreKeyGeneratorSpi$HmacSHA512");
    if (bool)
      put("KeyGenerator.DESede", "android.security.keystore.AndroidKeyStoreKeyGeneratorSpi$DESede"); 
    putSecretKeyFactoryImpl("AES");
    if (bool)
      putSecretKeyFactoryImpl("DESede"); 
    putSecretKeyFactoryImpl("HmacSHA1");
    putSecretKeyFactoryImpl("HmacSHA224");
    putSecretKeyFactoryImpl("HmacSHA256");
    putSecretKeyFactoryImpl("HmacSHA384");
    putSecretKeyFactoryImpl("HmacSHA512");
  }
  
  public static void install() {
    byte b2;
    Provider[] arrayOfProvider = Security.getProviders();
    byte b1 = -1;
    byte b = 0;
    while (true) {
      b2 = b1;
      if (b < arrayOfProvider.length) {
        Provider provider = arrayOfProvider[b];
        if ("BC".equals(provider.getName())) {
          b2 = b;
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    Security.addProvider(new AndroidKeyStoreProvider());
    AndroidKeyStoreBCWorkaroundProvider androidKeyStoreBCWorkaroundProvider = new AndroidKeyStoreBCWorkaroundProvider();
    if (b2 != -1) {
      Security.insertProviderAt(androidKeyStoreBCWorkaroundProvider, b2 + 1);
    } else {
      Security.addProvider(androidKeyStoreBCWorkaroundProvider);
    } 
  }
  
  private void putSecretKeyFactoryImpl(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SecretKeyFactory.");
    stringBuilder.append(paramString);
    put(stringBuilder.toString(), "android.security.keystore.AndroidKeyStoreSecretKeyFactorySpi");
  }
  
  private void putKeyFactoryImpl(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("KeyFactory.");
    stringBuilder.append(paramString);
    put(stringBuilder.toString(), "android.security.keystore.AndroidKeyStoreKeyFactorySpi");
  }
  
  public static long getKeyStoreOperationHandle(Object paramObject) {
    if (paramObject != null) {
      StringBuilder stringBuilder;
      if (paramObject instanceof Signature) {
        SignatureSpi signatureSpi = ((Signature)paramObject).getCurrentSpi();
      } else if (paramObject instanceof Mac) {
        MacSpi macSpi = ((Mac)paramObject).getCurrentSpi();
      } else if (paramObject instanceof Cipher) {
        CipherSpi cipherSpi = ((Cipher)paramObject).getCurrentSpi();
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported crypto primitive: ");
        stringBuilder.append(paramObject);
        stringBuilder.append(". Supported: Signature, Mac, Cipher");
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      if (stringBuilder != null) {
        if (stringBuilder instanceof KeyStoreCryptoOperation)
          return ((KeyStoreCryptoOperation)stringBuilder).getOperationHandle(); 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Crypto primitive not backed by AndroidKeyStore provider: ");
        stringBuilder1.append(paramObject);
        stringBuilder1.append(", spi: ");
        stringBuilder1.append(stringBuilder);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      throw new IllegalStateException("Crypto primitive not initialized");
    } 
    throw null;
  }
  
  public static AndroidKeyStorePublicKey getAndroidKeyStorePublicKey(String paramString1, int paramInt, String paramString2, byte[] paramArrayOfbyte) {
    try {
      KeyFactory keyFactory = KeyFactory.getInstance(paramString2);
      X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec();
      this(paramArrayOfbyte);
      PublicKey publicKey = keyFactory.generatePublic(x509EncodedKeySpec);
      if ("EC".equalsIgnoreCase(paramString2))
        return new AndroidKeyStoreECPublicKey(paramString1, paramInt, (ECPublicKey)publicKey); 
      if ("RSA".equalsIgnoreCase(paramString2))
        return new AndroidKeyStoreRSAPublicKey(paramString1, paramInt, (RSAPublicKey)publicKey); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported Android Keystore public key algorithm: ");
      stringBuilder.append(paramString2);
      throw new ProviderException(stringBuilder.toString());
    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to obtain ");
      stringBuilder.append(paramString2);
      stringBuilder.append(" KeyFactory");
      throw new ProviderException(stringBuilder.toString(), noSuchAlgorithmException);
    } catch (InvalidKeySpecException invalidKeySpecException) {
      throw new ProviderException("Invalid X.509 encoding of public key", invalidKeySpecException);
    } 
  }
  
  private static AndroidKeyStorePrivateKey getAndroidKeyStorePrivateKey(AndroidKeyStorePublicKey paramAndroidKeyStorePublicKey) {
    String str = paramAndroidKeyStorePublicKey.getAlgorithm();
    if ("EC".equalsIgnoreCase(str))
      return 
        new AndroidKeyStoreECPrivateKey(paramAndroidKeyStorePublicKey.getAlias(), paramAndroidKeyStorePublicKey.getUid(), ((ECKey)paramAndroidKeyStorePublicKey).getParams()); 
    if ("RSA".equalsIgnoreCase(str))
      return 
        new AndroidKeyStoreRSAPrivateKey(paramAndroidKeyStorePublicKey.getAlias(), paramAndroidKeyStorePublicKey.getUid(), ((RSAKey)paramAndroidKeyStorePublicKey).getModulus()); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported Android Keystore public key algorithm: ");
    stringBuilder.append(str);
    throw new ProviderException(stringBuilder.toString());
  }
  
  private static KeyCharacteristics getKeyCharacteristics(KeyStore paramKeyStore, String paramString, int paramInt) throws UnrecoverableKeyException, KeyPermanentlyInvalidatedException {
    KeyCharacteristics keyCharacteristics = new KeyCharacteristics();
    paramInt = paramKeyStore.getKeyCharacteristics(paramString, null, null, paramInt, keyCharacteristics);
    if (paramInt != 17) {
      if (paramInt == 1)
        return keyCharacteristics; 
      UnrecoverableKeyException unrecoverableKeyException = new UnrecoverableKeyException("Failed to obtain information about key");
      throw (UnrecoverableKeyException)unrecoverableKeyException.initCause(KeyStore.getKeyStoreException(paramInt));
    } 
    throw new KeyPermanentlyInvalidatedException("User changed or deleted their auth credentials", KeyStore.getKeyStoreException(paramInt));
  }
  
  private static AndroidKeyStorePublicKey loadAndroidKeyStorePublicKeyFromKeystore(KeyStore paramKeyStore, String paramString, int paramInt, KeyCharacteristics paramKeyCharacteristics) throws UnrecoverableKeyException {
    UnrecoverableKeyException unrecoverableKeyException1;
    ExportResult exportResult = paramKeyStore.exportKey(paramString, 0, null, null, paramInt);
    if (exportResult.resultCode == 1) {
      byte[] arrayOfByte = exportResult.exportData;
      Integer integer = paramKeyCharacteristics.getEnum(268435458);
      if (integer != null)
        try {
          int i = integer.intValue();
          String str = KeyProperties.KeyAlgorithm.fromKeymasterAsymmetricKeyAlgorithm(i);
          return getAndroidKeyStorePublicKey(paramString, paramInt, str, arrayOfByte);
        } catch (IllegalArgumentException illegalArgumentException) {
          unrecoverableKeyException1 = new UnrecoverableKeyException("Failed to load private key");
          throw (UnrecoverableKeyException)unrecoverableKeyException1.initCause(illegalArgumentException);
        }  
      throw new UnrecoverableKeyException("Key algorithm unknown");
    } 
    UnrecoverableKeyException unrecoverableKeyException2 = new UnrecoverableKeyException("Failed to obtain X.509 form of public key");
    paramInt = ((ExportResult)unrecoverableKeyException1).resultCode;
    throw (UnrecoverableKeyException)unrecoverableKeyException2.initCause(KeyStore.getKeyStoreException(paramInt));
  }
  
  public static AndroidKeyStorePublicKey loadAndroidKeyStorePublicKeyFromKeystore(KeyStore paramKeyStore, String paramString, int paramInt) throws UnrecoverableKeyException, KeyPermanentlyInvalidatedException {
    KeyCharacteristics keyCharacteristics = getKeyCharacteristics(paramKeyStore, paramString, paramInt);
    return loadAndroidKeyStorePublicKeyFromKeystore(paramKeyStore, paramString, paramInt, keyCharacteristics);
  }
  
  private static KeyPair loadAndroidKeyStoreKeyPairFromKeystore(KeyStore paramKeyStore, String paramString, int paramInt, KeyCharacteristics paramKeyCharacteristics) throws UnrecoverableKeyException {
    AndroidKeyStorePublicKey androidKeyStorePublicKey = loadAndroidKeyStorePublicKeyFromKeystore(paramKeyStore, paramString, paramInt, paramKeyCharacteristics);
    AndroidKeyStorePrivateKey androidKeyStorePrivateKey = getAndroidKeyStorePrivateKey(androidKeyStorePublicKey);
    return new KeyPair(androidKeyStorePublicKey, androidKeyStorePrivateKey);
  }
  
  public static KeyPair loadAndroidKeyStoreKeyPairFromKeystore(KeyStore paramKeyStore, String paramString, int paramInt) throws UnrecoverableKeyException, KeyPermanentlyInvalidatedException {
    KeyCharacteristics keyCharacteristics = getKeyCharacteristics(paramKeyStore, paramString, paramInt);
    return loadAndroidKeyStoreKeyPairFromKeystore(paramKeyStore, paramString, paramInt, keyCharacteristics);
  }
  
  private static AndroidKeyStorePrivateKey loadAndroidKeyStorePrivateKeyFromKeystore(KeyStore paramKeyStore, String paramString, int paramInt, KeyCharacteristics paramKeyCharacteristics) throws UnrecoverableKeyException {
    KeyPair keyPair = loadAndroidKeyStoreKeyPairFromKeystore(paramKeyStore, paramString, paramInt, paramKeyCharacteristics);
    return (AndroidKeyStorePrivateKey)keyPair.getPrivate();
  }
  
  public static AndroidKeyStorePrivateKey loadAndroidKeyStorePrivateKeyFromKeystore(KeyStore paramKeyStore, String paramString, int paramInt) throws UnrecoverableKeyException, KeyPermanentlyInvalidatedException {
    KeyCharacteristics keyCharacteristics = getKeyCharacteristics(paramKeyStore, paramString, paramInt);
    return loadAndroidKeyStorePrivateKeyFromKeystore(paramKeyStore, paramString, paramInt, keyCharacteristics);
  }
  
  private static AndroidKeyStoreSecretKey loadAndroidKeyStoreSecretKeyFromKeystore(String paramString, int paramInt, KeyCharacteristics paramKeyCharacteristics) throws UnrecoverableKeyException {
    Integer integer = paramKeyCharacteristics.getEnum(268435458);
    if (integer != null) {
      int i;
      List<Integer> list = paramKeyCharacteristics.getEnums(536870917);
      if (list.isEmpty()) {
        i = -1;
      } else {
        i = ((Integer)list.get(0)).intValue();
      } 
      try {
        int j = integer.intValue();
        String str = KeyProperties.KeyAlgorithm.fromKeymasterSecretKeyAlgorithm(j, i);
        return new AndroidKeyStoreSecretKey(paramString, paramInt, str);
      } catch (IllegalArgumentException illegalArgumentException) {
        UnrecoverableKeyException unrecoverableKeyException = new UnrecoverableKeyException("Unsupported secret key type");
        throw (UnrecoverableKeyException)unrecoverableKeyException.initCause(illegalArgumentException);
      } 
    } 
    throw new UnrecoverableKeyException("Key algorithm unknown");
  }
  
  public static AndroidKeyStoreKey loadAndroidKeyStoreKeyFromKeystore(KeyStore paramKeyStore, String paramString, int paramInt) throws UnrecoverableKeyException, KeyPermanentlyInvalidatedException {
    KeyCharacteristics keyCharacteristics = getKeyCharacteristics(paramKeyStore, paramString, paramInt);
    Integer integer = keyCharacteristics.getEnum(268435458);
    if (integer != null) {
      if (integer.intValue() == 128 || 
        integer.intValue() == 32 || 
        integer.intValue() == 33)
        return loadAndroidKeyStoreSecretKeyFromKeystore(paramString, paramInt, keyCharacteristics); 
      if (integer.intValue() == 1 || 
        integer.intValue() == 3)
        return loadAndroidKeyStorePrivateKeyFromKeystore(paramKeyStore, paramString, paramInt, keyCharacteristics); 
      throw new UnrecoverableKeyException("Key algorithm unknown");
    } 
    throw new UnrecoverableKeyException("Key algorithm unknown");
  }
  
  @SystemApi
  public static KeyStore getKeyStoreForUid(int paramInt) throws KeyStoreException, NoSuchProviderException {
    KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore", "AndroidKeyStore");
    try {
      AndroidKeyStoreLoadStoreParameter androidKeyStoreLoadStoreParameter = new AndroidKeyStoreLoadStoreParameter();
      this(paramInt);
      keyStore.load(androidKeyStoreLoadStoreParameter);
      return keyStore;
    } catch (NoSuchAlgorithmException|java.security.cert.CertificateException|java.io.IOException noSuchAlgorithmException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to load AndroidKeyStore KeyStore for UID ");
      stringBuilder.append(paramInt);
      throw new KeyStoreException(stringBuilder.toString(), noSuchAlgorithmException);
    } 
  }
}
