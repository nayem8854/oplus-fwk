package android.security.keystore;

import android.os.IBinder;
import android.security.KeyStore;
import android.security.KeyStoreException;
import android.security.keymaster.KeymasterArguments;
import android.security.keymaster.OperationResult;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.ProviderException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.AEADBadTagException;
import javax.crypto.BadPaddingException;
import javax.crypto.CipherSpi;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;
import libcore.util.EmptyArray;

abstract class AndroidKeyStoreCipherSpiBase extends CipherSpi implements KeyStoreCryptoOperation {
  private KeyStoreCryptoOperationStreamer mAdditionalAuthenticationDataStreamer;
  
  private boolean mAdditionalAuthenticationDataStreamerClosed;
  
  private Exception mCachedException;
  
  private boolean mEncrypting;
  
  private AndroidKeyStoreKey mKey;
  
  private final KeyStore mKeyStore;
  
  private int mKeymasterPurposeOverride = -1;
  
  private KeyStoreCryptoOperationStreamer mMainDataStreamer;
  
  private long mOperationHandle;
  
  private IBinder mOperationToken;
  
  private SecureRandom mRng;
  
  AndroidKeyStoreCipherSpiBase() {
    this.mKeyStore = KeyStore.getInstance();
  }
  
  protected final void engineInit(int paramInt, Key paramKey, SecureRandom paramSecureRandom) throws InvalidKeyException {
    resetAll();
    try {
      init(paramInt, paramKey, paramSecureRandom);
      initAlgorithmSpecificParameters();
    } finally {
      if (!false)
        resetAll(); 
    } 
  }
  
  protected final void engineInit(int paramInt, Key paramKey, AlgorithmParameters paramAlgorithmParameters, SecureRandom paramSecureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
    resetAll();
    try {
      init(paramInt, paramKey, paramSecureRandom);
      initAlgorithmSpecificParameters(paramAlgorithmParameters);
      ensureKeystoreOperationInitialized();
      return;
    } finally {
      if (!false)
        resetAll(); 
    } 
  }
  
  protected final void engineInit(int paramInt, Key paramKey, AlgorithmParameterSpec paramAlgorithmParameterSpec, SecureRandom paramSecureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
    resetAll();
    try {
      init(paramInt, paramKey, paramSecureRandom);
      initAlgorithmSpecificParameters(paramAlgorithmParameterSpec);
      ensureKeystoreOperationInitialized();
      return;
    } finally {
      if (!false)
        resetAll(); 
    } 
  }
  
  private void init(int paramInt, Key paramKey, SecureRandom paramSecureRandom) throws InvalidKeyException {
    // Byte code:
    //   0: iload_1
    //   1: iconst_1
    //   2: if_icmpeq -> 64
    //   5: iload_1
    //   6: iconst_2
    //   7: if_icmpeq -> 56
    //   10: iload_1
    //   11: iconst_3
    //   12: if_icmpeq -> 64
    //   15: iload_1
    //   16: iconst_4
    //   17: if_icmpne -> 23
    //   20: goto -> 56
    //   23: new java/lang/StringBuilder
    //   26: dup
    //   27: invokespecial <init> : ()V
    //   30: astore_2
    //   31: aload_2
    //   32: ldc 'Unsupported opmode: '
    //   34: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   37: pop
    //   38: aload_2
    //   39: iload_1
    //   40: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   43: pop
    //   44: new java/security/InvalidParameterException
    //   47: dup
    //   48: aload_2
    //   49: invokevirtual toString : ()Ljava/lang/String;
    //   52: invokespecial <init> : (Ljava/lang/String;)V
    //   55: athrow
    //   56: aload_0
    //   57: iconst_0
    //   58: putfield mEncrypting : Z
    //   61: goto -> 69
    //   64: aload_0
    //   65: iconst_1
    //   66: putfield mEncrypting : Z
    //   69: aload_0
    //   70: iload_1
    //   71: aload_2
    //   72: invokevirtual initKey : (ILjava/security/Key;)V
    //   75: aload_0
    //   76: getfield mKey : Landroid/security/keystore/AndroidKeyStoreKey;
    //   79: ifnull -> 88
    //   82: aload_0
    //   83: aload_3
    //   84: putfield mRng : Ljava/security/SecureRandom;
    //   87: return
    //   88: new java/security/ProviderException
    //   91: dup
    //   92: ldc 'initKey did not initialize the key'
    //   94: invokespecial <init> : (Ljava/lang/String;)V
    //   97: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #158	-> 0
    //   #168	-> 23
    //   #165	-> 56
    //   #166	-> 61
    //   #161	-> 64
    //   #162	-> 69
    //   #170	-> 69
    //   #171	-> 75
    //   #174	-> 82
    //   #175	-> 87
    //   #172	-> 88
  }
  
  protected void resetAll() {
    IBinder iBinder = this.mOperationToken;
    if (iBinder != null)
      this.mKeyStore.abort(iBinder); 
    this.mEncrypting = false;
    this.mKeymasterPurposeOverride = -1;
    this.mKey = null;
    this.mRng = null;
    this.mOperationToken = null;
    this.mOperationHandle = 0L;
    this.mMainDataStreamer = null;
    this.mAdditionalAuthenticationDataStreamer = null;
    this.mAdditionalAuthenticationDataStreamerClosed = false;
    this.mCachedException = null;
  }
  
  protected void resetWhilePreservingInitState() {
    IBinder iBinder = this.mOperationToken;
    if (iBinder != null)
      this.mKeyStore.abort(iBinder); 
    this.mOperationToken = null;
    this.mOperationHandle = 0L;
    this.mMainDataStreamer = null;
    this.mAdditionalAuthenticationDataStreamer = null;
    this.mAdditionalAuthenticationDataStreamerClosed = false;
    this.mCachedException = null;
  }
  
  private void ensureKeystoreOperationInitialized() throws InvalidKeyException, InvalidAlgorithmParameterException {
    if (this.mMainDataStreamer != null)
      return; 
    if (this.mCachedException != null)
      return; 
    if (this.mKey != null) {
      KeymasterArguments keymasterArguments = new KeymasterArguments();
      addAlgorithmSpecificParametersToBegin(keymasterArguments);
      SecureRandom secureRandom = this.mRng;
      int i = getAdditionalEntropyAmountForBegin();
      byte[] arrayOfByte = KeyStoreCryptoOperationUtils.getRandomBytesToMixIntoKeystoreRng(secureRandom, i);
      if (this.mKeymasterPurposeOverride != -1) {
        i = this.mKeymasterPurposeOverride;
      } else if (this.mEncrypting) {
        i = 0;
      } else {
        i = 1;
      } 
      KeyStore keyStore = this.mKeyStore;
      AndroidKeyStoreKey androidKeyStoreKey1 = this.mKey;
      String str = androidKeyStoreKey1.getAlias();
      AndroidKeyStoreKey androidKeyStoreKey2 = this.mKey;
      int j = androidKeyStoreKey2.getUid();
      OperationResult operationResult = keyStore.begin(str, i, true, keymasterArguments, arrayOfByte, j);
      if (operationResult != null) {
        this.mOperationToken = operationResult.token;
        this.mOperationHandle = operationResult.operationHandle;
        GeneralSecurityException generalSecurityException = KeyStoreCryptoOperationUtils.getExceptionForCipherInit(this.mKeyStore, this.mKey, operationResult.resultCode);
        if (generalSecurityException != null) {
          if (!(generalSecurityException instanceof InvalidKeyException)) {
            if (generalSecurityException instanceof InvalidAlgorithmParameterException)
              throw (InvalidAlgorithmParameterException)generalSecurityException; 
            throw new ProviderException("Unexpected exception type", generalSecurityException);
          } 
          throw (InvalidKeyException)generalSecurityException;
        } 
        if (this.mOperationToken != null) {
          if (this.mOperationHandle != 0L) {
            loadAlgorithmSpecificParametersFromBeginResult(operationResult.outParams);
            this.mMainDataStreamer = createMainDataStreamer(this.mKeyStore, operationResult.token);
            KeyStore keyStore1 = this.mKeyStore;
            IBinder iBinder = operationResult.token;
            this.mAdditionalAuthenticationDataStreamer = createAdditionalAuthenticationDataStreamer(keyStore1, iBinder);
            this.mAdditionalAuthenticationDataStreamerClosed = false;
            return;
          } 
          throw new ProviderException("Keystore returned invalid operation handle");
        } 
        throw new ProviderException("Keystore returned null operation token");
      } 
      throw new KeyStoreConnectException();
    } 
    throw new IllegalStateException("Not initialized");
  }
  
  protected KeyStoreCryptoOperationStreamer createMainDataStreamer(KeyStore paramKeyStore, IBinder paramIBinder) {
    return new KeyStoreCryptoOperationChunkedStreamer(new KeyStoreCryptoOperationChunkedStreamer.MainDataStream(paramKeyStore, paramIBinder), 0);
  }
  
  protected KeyStoreCryptoOperationStreamer createAdditionalAuthenticationDataStreamer(KeyStore paramKeyStore, IBinder paramIBinder) {
    return null;
  }
  
  protected final byte[] engineUpdate(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    if (this.mCachedException != null)
      return null; 
    try {
      ensureKeystoreOperationInitialized();
      if (paramInt2 == 0)
        return null; 
      try {
        flushAAD();
        paramArrayOfbyte = this.mMainDataStreamer.update(paramArrayOfbyte, paramInt1, paramInt2);
        if (paramArrayOfbyte.length == 0)
          return null; 
        return paramArrayOfbyte;
      } catch (KeyStoreException keyStoreException) {
        this.mCachedException = keyStoreException;
        return null;
      } 
    } catch (InvalidKeyException|InvalidAlgorithmParameterException invalidKeyException) {
      this.mCachedException = invalidKeyException;
      return null;
    } 
  }
  
  private void flushAAD() throws KeyStoreException {
    KeyStoreCryptoOperationStreamer keyStoreCryptoOperationStreamer = this.mAdditionalAuthenticationDataStreamer;
    if (keyStoreCryptoOperationStreamer != null && !this.mAdditionalAuthenticationDataStreamerClosed)
      try {
        byte[] arrayOfByte = keyStoreCryptoOperationStreamer.doFinal(EmptyArray.BYTE, 0, 0, null, null);
        this.mAdditionalAuthenticationDataStreamerClosed = true;
      } finally {
        this.mAdditionalAuthenticationDataStreamerClosed = true;
      }  
  }
  
  protected final int engineUpdate(byte[] paramArrayOfbyte1, int paramInt1, int paramInt2, byte[] paramArrayOfbyte2, int paramInt3) throws ShortBufferException {
    paramArrayOfbyte1 = engineUpdate(paramArrayOfbyte1, paramInt1, paramInt2);
    if (paramArrayOfbyte1 == null)
      return 0; 
    paramInt1 = paramArrayOfbyte2.length - paramInt3;
    if (paramArrayOfbyte1.length <= paramInt1) {
      System.arraycopy(paramArrayOfbyte1, 0, paramArrayOfbyte2, paramInt3, paramArrayOfbyte1.length);
      return paramArrayOfbyte1.length;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Output buffer too short. Produced: ");
    stringBuilder.append(paramArrayOfbyte1.length);
    stringBuilder.append(", available: ");
    stringBuilder.append(paramInt1);
    throw new ShortBufferException(stringBuilder.toString());
  }
  
  protected final int engineUpdate(ByteBuffer paramByteBuffer1, ByteBuffer paramByteBuffer2) throws ShortBufferException {
    if (paramByteBuffer1 != null) {
      if (paramByteBuffer2 != null) {
        byte[] arrayOfByte;
        int i = paramByteBuffer1.remaining();
        boolean bool = paramByteBuffer1.hasArray();
        int j = 0;
        if (bool) {
          byte[] arrayOfByte1 = paramByteBuffer1.array();
          int k = paramByteBuffer1.arrayOffset(), m = paramByteBuffer1.position();
          arrayOfByte1 = engineUpdate(arrayOfByte1, k + m, i);
          paramByteBuffer1.position(paramByteBuffer1.position() + i);
          arrayOfByte = arrayOfByte1;
        } else {
          byte[] arrayOfByte1 = new byte[i];
          arrayOfByte.get(arrayOfByte1);
          arrayOfByte = engineUpdate(arrayOfByte1, 0, i);
        } 
        if (arrayOfByte != null)
          j = arrayOfByte.length; 
        if (j > 0) {
          int k = paramByteBuffer2.remaining();
          try {
            paramByteBuffer2.put(arrayOfByte);
          } catch (BufferOverflowException bufferOverflowException) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Output buffer too small. Produced: ");
            stringBuilder.append(j);
            stringBuilder.append(", available: ");
            stringBuilder.append(k);
            throw new ShortBufferException(stringBuilder.toString());
          } 
        } 
        return j;
      } 
      throw new NullPointerException("output == null");
    } 
    throw new NullPointerException("input == null");
  }
  
  protected final void engineUpdateAAD(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    if (this.mCachedException != null)
      return; 
    try {
      ensureKeystoreOperationInitialized();
      if (!this.mAdditionalAuthenticationDataStreamerClosed) {
        KeyStoreCryptoOperationStreamer keyStoreCryptoOperationStreamer = this.mAdditionalAuthenticationDataStreamer;
        if (keyStoreCryptoOperationStreamer != null)
          try {
            paramArrayOfbyte = keyStoreCryptoOperationStreamer.update(paramArrayOfbyte, paramInt1, paramInt2);
            if (paramArrayOfbyte == null || paramArrayOfbyte.length <= 0)
              return; 
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("AAD update unexpectedly produced output: ");
            stringBuilder.append(paramArrayOfbyte.length);
            stringBuilder.append(" bytes");
            throw new ProviderException(stringBuilder.toString());
          } catch (KeyStoreException keyStoreException) {
            this.mCachedException = keyStoreException;
            return;
          }  
        throw new IllegalStateException("This cipher does not support AAD");
      } 
      throw new IllegalStateException("AAD can only be provided before Cipher.update is invoked");
    } catch (InvalidKeyException|InvalidAlgorithmParameterException invalidKeyException) {
      this.mCachedException = invalidKeyException;
      return;
    } 
  }
  
  protected final void engineUpdateAAD(ByteBuffer paramByteBuffer) {
    if (paramByteBuffer != null) {
      byte[] arrayOfByte;
      boolean bool;
      int i;
      if (!paramByteBuffer.hasRemaining())
        return; 
      if (paramByteBuffer.hasArray()) {
        byte[] arrayOfByte1 = paramByteBuffer.array();
        bool = paramByteBuffer.arrayOffset() + paramByteBuffer.position();
        i = paramByteBuffer.remaining();
        paramByteBuffer.position(paramByteBuffer.limit());
        arrayOfByte = arrayOfByte1;
      } else {
        byte[] arrayOfByte1 = new byte[arrayOfByte.remaining()];
        bool = false;
        i = arrayOfByte1.length;
        arrayOfByte.get(arrayOfByte1);
        arrayOfByte = arrayOfByte1;
      } 
      engineUpdateAAD(arrayOfByte, bool, i);
      return;
    } 
    throw new IllegalArgumentException("src == null");
  }
  
  protected final byte[] engineDoFinal(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IllegalBlockSizeException, BadPaddingException {
    if (this.mCachedException == null)
      try {
        ensureKeystoreOperationInitialized();
        try {
          flushAAD();
          SecureRandom secureRandom = this.mRng;
          int i = getAdditionalEntropyAmountForFinish();
          byte[] arrayOfByte = KeyStoreCryptoOperationUtils.getRandomBytesToMixIntoKeystoreRng(secureRandom, i);
          paramArrayOfbyte = this.mMainDataStreamer.doFinal(paramArrayOfbyte, paramInt1, paramInt2, null, arrayOfByte);
          resetWhilePreservingInitState();
          return paramArrayOfbyte;
        } catch (KeyStoreException keyStoreException) {
          paramInt1 = keyStoreException.getErrorCode();
          if (paramInt1 != -38) {
            if (paramInt1 != -30) {
              if (paramInt1 != -21)
                throw (IllegalBlockSizeException)(new IllegalBlockSizeException()).initCause(keyStoreException); 
              throw (IllegalBlockSizeException)(new IllegalBlockSizeException()).initCause(keyStoreException);
            } 
            throw (AEADBadTagException)(new AEADBadTagException()).initCause(keyStoreException);
          } 
          throw (BadPaddingException)(new BadPaddingException()).initCause(keyStoreException);
        } 
      } catch (InvalidKeyException|InvalidAlgorithmParameterException invalidKeyException) {
        throw (IllegalBlockSizeException)(new IllegalBlockSizeException()).initCause(invalidKeyException);
      }  
    IllegalBlockSizeException illegalBlockSizeException = new IllegalBlockSizeException();
    Exception exception = this.mCachedException;
    throw (IllegalBlockSizeException)illegalBlockSizeException.initCause(exception);
  }
  
  protected final int engineDoFinal(byte[] paramArrayOfbyte1, int paramInt1, int paramInt2, byte[] paramArrayOfbyte2, int paramInt3) throws ShortBufferException, IllegalBlockSizeException, BadPaddingException {
    paramArrayOfbyte1 = engineDoFinal(paramArrayOfbyte1, paramInt1, paramInt2);
    if (paramArrayOfbyte1 == null)
      return 0; 
    paramInt1 = paramArrayOfbyte2.length - paramInt3;
    if (paramArrayOfbyte1.length <= paramInt1) {
      System.arraycopy(paramArrayOfbyte1, 0, paramArrayOfbyte2, paramInt3, paramArrayOfbyte1.length);
      return paramArrayOfbyte1.length;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Output buffer too short. Produced: ");
    stringBuilder.append(paramArrayOfbyte1.length);
    stringBuilder.append(", available: ");
    stringBuilder.append(paramInt1);
    throw new ShortBufferException(stringBuilder.toString());
  }
  
  protected final int engineDoFinal(ByteBuffer paramByteBuffer1, ByteBuffer paramByteBuffer2) throws ShortBufferException, IllegalBlockSizeException, BadPaddingException {
    if (paramByteBuffer1 != null) {
      if (paramByteBuffer2 != null) {
        byte[] arrayOfByte;
        int i = paramByteBuffer1.remaining();
        boolean bool = paramByteBuffer1.hasArray();
        int j = 0;
        if (bool) {
          byte[] arrayOfByte1 = paramByteBuffer1.array();
          int k = paramByteBuffer1.arrayOffset(), m = paramByteBuffer1.position();
          arrayOfByte1 = engineDoFinal(arrayOfByte1, k + m, i);
          paramByteBuffer1.position(paramByteBuffer1.position() + i);
          arrayOfByte = arrayOfByte1;
        } else {
          byte[] arrayOfByte1 = new byte[i];
          arrayOfByte.get(arrayOfByte1);
          arrayOfByte = engineDoFinal(arrayOfByte1, 0, i);
        } 
        if (arrayOfByte != null)
          j = arrayOfByte.length; 
        if (j > 0) {
          int k = paramByteBuffer2.remaining();
          try {
            paramByteBuffer2.put(arrayOfByte);
          } catch (BufferOverflowException bufferOverflowException) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Output buffer too small. Produced: ");
            stringBuilder.append(j);
            stringBuilder.append(", available: ");
            stringBuilder.append(k);
            throw new ShortBufferException(stringBuilder.toString());
          } 
        } 
        return j;
      } 
      throw new NullPointerException("output == null");
    } 
    throw new NullPointerException("input == null");
  }
  
  protected final byte[] engineWrap(Key paramKey) throws IllegalBlockSizeException, InvalidKeyException {
    if (this.mKey != null) {
      if (isEncrypting()) {
        if (paramKey != null) {
          StringBuilder stringBuilder;
          byte[] arrayOfByte1 = null, arrayOfByte2 = null, arrayOfByte3 = null;
          if (paramKey instanceof SecretKey) {
            arrayOfByte1 = arrayOfByte3;
            if ("RAW".equalsIgnoreCase(paramKey.getFormat()))
              arrayOfByte1 = paramKey.getEncoded(); 
            arrayOfByte2 = arrayOfByte1;
            if (arrayOfByte1 == null)
              try {
                SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(paramKey.getAlgorithm());
                paramKey = paramKey;
                paramKey = (SecretKeySpec)secretKeyFactory.getKeySpec((SecretKey)paramKey, SecretKeySpec.class);
                byte[] arrayOfByte = paramKey.getEncoded();
              } catch (NoSuchAlgorithmException|InvalidKeySpecException noSuchAlgorithmException) {
                throw new InvalidKeyException("Failed to wrap key because it does not export its key material", noSuchAlgorithmException);
              }  
          } else {
            byte[] arrayOfByte;
            if (noSuchAlgorithmException instanceof java.security.PrivateKey) {
              if ("PKCS8".equalsIgnoreCase(noSuchAlgorithmException.getFormat()))
                arrayOfByte1 = noSuchAlgorithmException.getEncoded(); 
              arrayOfByte2 = arrayOfByte1;
              if (arrayOfByte1 == null)
                try {
                  KeyFactory keyFactory = KeyFactory.getInstance(noSuchAlgorithmException.getAlgorithm());
                  PKCS8EncodedKeySpec pKCS8EncodedKeySpec = keyFactory.<PKCS8EncodedKeySpec>getKeySpec((Key)noSuchAlgorithmException, PKCS8EncodedKeySpec.class);
                  arrayOfByte = pKCS8EncodedKeySpec.getEncoded();
                } catch (NoSuchAlgorithmException|InvalidKeySpecException noSuchAlgorithmException1) {
                  throw new InvalidKeyException("Failed to wrap key because it does not export its key material", noSuchAlgorithmException1);
                }  
            } else if (noSuchAlgorithmException1 instanceof java.security.PublicKey) {
              arrayOfByte1 = arrayOfByte;
              if ("X.509".equalsIgnoreCase(noSuchAlgorithmException1.getFormat()))
                arrayOfByte1 = noSuchAlgorithmException1.getEncoded(); 
              arrayOfByte = arrayOfByte1;
              if (arrayOfByte1 == null)
                try {
                  KeyFactory keyFactory = KeyFactory.getInstance(noSuchAlgorithmException1.getAlgorithm());
                  X509EncodedKeySpec x509EncodedKeySpec = keyFactory.<X509EncodedKeySpec>getKeySpec((Key)noSuchAlgorithmException1, X509EncodedKeySpec.class);
                  byte[] arrayOfByte4 = x509EncodedKeySpec.getEncoded();
                } catch (NoSuchAlgorithmException|InvalidKeySpecException noSuchAlgorithmException2) {
                  throw new InvalidKeyException("Failed to wrap key because it does not export its key material", noSuchAlgorithmException2);
                }  
            } else {
              stringBuilder = new StringBuilder();
              stringBuilder.append("Unsupported key type: ");
              stringBuilder.append(noSuchAlgorithmException2.getClass().getName());
              throw new InvalidKeyException(stringBuilder.toString());
            } 
          } 
          if (stringBuilder != null)
            try {
              return engineDoFinal((byte[])stringBuilder, 0, stringBuilder.length);
            } catch (BadPaddingException badPaddingException) {
              throw (IllegalBlockSizeException)(new IllegalBlockSizeException()).initCause(badPaddingException);
            }  
          throw new InvalidKeyException("Failed to wrap key because it does not export its key material");
        } 
        throw new NullPointerException("key == null");
      } 
      throw new IllegalStateException("Cipher must be initialized in Cipher.WRAP_MODE to wrap keys");
    } 
    throw new IllegalStateException("Not initilized");
  }
  
  protected final Key engineUnwrap(byte[] paramArrayOfbyte, String paramString, int paramInt) throws InvalidKeyException, NoSuchAlgorithmException {
    if (this.mKey != null) {
      if (!isEncrypting()) {
        if (paramArrayOfbyte != null)
          try {
            PKCS8EncodedKeySpec pKCS8EncodedKeySpec;
            paramArrayOfbyte = engineDoFinal(paramArrayOfbyte, 0, paramArrayOfbyte.length);
            if (paramInt != 1) {
              StringBuilder stringBuilder;
              if (paramInt != 2) {
                if (paramInt == 3)
                  return new SecretKeySpec(paramArrayOfbyte, paramString); 
                stringBuilder = new StringBuilder();
                stringBuilder.append("Unsupported wrappedKeyType: ");
                stringBuilder.append(paramInt);
                throw new InvalidParameterException(stringBuilder.toString());
              } 
              KeyFactory keyFactory1 = KeyFactory.getInstance(paramString);
              try {
                pKCS8EncodedKeySpec = new PKCS8EncodedKeySpec();
                this((byte[])stringBuilder);
                return keyFactory1.generatePrivate(pKCS8EncodedKeySpec);
              } catch (InvalidKeySpecException invalidKeySpecException) {
                throw new InvalidKeyException("Failed to create private key from its PKCS#8 encoded form", invalidKeySpecException);
              } 
            } 
            KeyFactory keyFactory = KeyFactory.getInstance((String)pKCS8EncodedKeySpec);
            try {
              X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec();
              this((byte[])invalidKeySpecException);
              return keyFactory.generatePublic(x509EncodedKeySpec);
            } catch (InvalidKeySpecException invalidKeySpecException1) {
              throw new InvalidKeyException("Failed to create public key from its X.509 encoded form", invalidKeySpecException1);
            } 
          } catch (IllegalBlockSizeException|BadPaddingException illegalBlockSizeException) {
            throw new InvalidKeyException("Failed to unwrap key", illegalBlockSizeException);
          }  
        throw new NullPointerException("wrappedKey == null");
      } 
      throw new IllegalStateException("Cipher must be initialized in Cipher.WRAP_MODE to wrap keys");
    } 
    throw new IllegalStateException("Not initilized");
  }
  
  protected final void engineSetMode(String paramString) throws NoSuchAlgorithmException {
    throw new UnsupportedOperationException();
  }
  
  protected final void engineSetPadding(String paramString) throws NoSuchPaddingException {
    throw new UnsupportedOperationException();
  }
  
  protected final int engineGetKeySize(Key paramKey) throws InvalidKeyException {
    throw new UnsupportedOperationException();
  }
  
  public void finalize() throws Throwable {
    try {
      IBinder iBinder = this.mOperationToken;
      if (iBinder != null)
        this.mKeyStore.abort(iBinder); 
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public final long getOperationHandle() {
    return this.mOperationHandle;
  }
  
  protected final void setKey(AndroidKeyStoreKey paramAndroidKeyStoreKey) {
    this.mKey = paramAndroidKeyStoreKey;
  }
  
  protected final void setKeymasterPurposeOverride(int paramInt) {
    this.mKeymasterPurposeOverride = paramInt;
  }
  
  protected final int getKeymasterPurposeOverride() {
    return this.mKeymasterPurposeOverride;
  }
  
  protected final boolean isEncrypting() {
    return this.mEncrypting;
  }
  
  protected final KeyStore getKeyStore() {
    return this.mKeyStore;
  }
  
  protected final long getConsumedInputSizeBytes() {
    KeyStoreCryptoOperationStreamer keyStoreCryptoOperationStreamer = this.mMainDataStreamer;
    if (keyStoreCryptoOperationStreamer != null)
      return keyStoreCryptoOperationStreamer.getConsumedInputSizeBytes(); 
    throw new IllegalStateException("Not initialized");
  }
  
  protected final long getProducedOutputSizeBytes() {
    KeyStoreCryptoOperationStreamer keyStoreCryptoOperationStreamer = this.mMainDataStreamer;
    if (keyStoreCryptoOperationStreamer != null)
      return keyStoreCryptoOperationStreamer.getProducedOutputSizeBytes(); 
    throw new IllegalStateException("Not initialized");
  }
  
  static String opmodeToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3) {
          if (paramInt != 4)
            return String.valueOf(paramInt); 
          return "UNWRAP_MODE";
        } 
        return "WRAP_MODE";
      } 
      return "DECRYPT_MODE";
    } 
    return "ENCRYPT_MODE";
  }
  
  protected abstract void addAlgorithmSpecificParametersToBegin(KeymasterArguments paramKeymasterArguments);
  
  protected abstract AlgorithmParameters engineGetParameters();
  
  protected abstract int getAdditionalEntropyAmountForBegin();
  
  protected abstract int getAdditionalEntropyAmountForFinish();
  
  protected abstract void initAlgorithmSpecificParameters() throws InvalidKeyException;
  
  protected abstract void initAlgorithmSpecificParameters(AlgorithmParameters paramAlgorithmParameters) throws InvalidAlgorithmParameterException;
  
  protected abstract void initAlgorithmSpecificParameters(AlgorithmParameterSpec paramAlgorithmParameterSpec) throws InvalidAlgorithmParameterException;
  
  protected abstract void initKey(int paramInt, Key paramKey) throws InvalidKeyException;
  
  protected abstract void loadAlgorithmSpecificParametersFromBeginResult(KeymasterArguments paramKeymasterArguments);
}
