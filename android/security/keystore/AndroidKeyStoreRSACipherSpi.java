package android.security.keystore;

import android.security.keymaster.KeymasterArguments;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.ProviderException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.MGF1ParameterSpec;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;

abstract class AndroidKeyStoreRSACipherSpi extends AndroidKeyStoreCipherSpiBase {
  private final int mKeymasterPadding;
  
  private int mKeymasterPaddingOverride;
  
  class NoPadding extends AndroidKeyStoreRSACipherSpi {
    public NoPadding() {
      super(1);
    }
    
    protected boolean adjustConfigForEncryptingWithPrivateKey() {
      setKeymasterPurposeOverride(2);
      return true;
    }
    
    protected void initAlgorithmSpecificParameters() throws InvalidKeyException {}
    
    protected void initAlgorithmSpecificParameters(AlgorithmParameterSpec param1AlgorithmParameterSpec) throws InvalidAlgorithmParameterException {
      if (param1AlgorithmParameterSpec == null)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected parameters: ");
      stringBuilder.append(param1AlgorithmParameterSpec);
      stringBuilder.append(". No parameters supported");
      throw new InvalidAlgorithmParameterException(stringBuilder.toString());
    }
    
    protected void initAlgorithmSpecificParameters(AlgorithmParameters param1AlgorithmParameters) throws InvalidAlgorithmParameterException {
      if (param1AlgorithmParameters == null)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected parameters: ");
      stringBuilder.append(param1AlgorithmParameters);
      stringBuilder.append(". No parameters supported");
      throw new InvalidAlgorithmParameterException(stringBuilder.toString());
    }
    
    protected AlgorithmParameters engineGetParameters() {
      return null;
    }
    
    protected final int getAdditionalEntropyAmountForBegin() {
      return 0;
    }
    
    protected final int getAdditionalEntropyAmountForFinish() {
      return 0;
    }
  }
  
  class PKCS1Padding extends AndroidKeyStoreRSACipherSpi {
    public PKCS1Padding() {
      super(4);
    }
    
    protected boolean adjustConfigForEncryptingWithPrivateKey() {
      setKeymasterPurposeOverride(2);
      setKeymasterPaddingOverride(5);
      return true;
    }
    
    protected void initAlgorithmSpecificParameters() throws InvalidKeyException {}
    
    protected void initAlgorithmSpecificParameters(AlgorithmParameterSpec param1AlgorithmParameterSpec) throws InvalidAlgorithmParameterException {
      if (param1AlgorithmParameterSpec == null)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected parameters: ");
      stringBuilder.append(param1AlgorithmParameterSpec);
      stringBuilder.append(". No parameters supported");
      throw new InvalidAlgorithmParameterException(stringBuilder.toString());
    }
    
    protected void initAlgorithmSpecificParameters(AlgorithmParameters param1AlgorithmParameters) throws InvalidAlgorithmParameterException {
      if (param1AlgorithmParameters == null)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected parameters: ");
      stringBuilder.append(param1AlgorithmParameters);
      stringBuilder.append(". No parameters supported");
      throw new InvalidAlgorithmParameterException(stringBuilder.toString());
    }
    
    protected AlgorithmParameters engineGetParameters() {
      return null;
    }
    
    protected final int getAdditionalEntropyAmountForBegin() {
      return 0;
    }
    
    protected final int getAdditionalEntropyAmountForFinish() {
      boolean bool;
      if (isEncrypting()) {
        bool = getModulusSizeBytes();
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  class OAEPWithMGF1Padding extends AndroidKeyStoreRSACipherSpi {
    private static final String MGF_ALGORITGM_MGF1 = "MGF1";
    
    private int mDigestOutputSizeBytes;
    
    private int mKeymasterDigest = -1;
    
    OAEPWithMGF1Padding(AndroidKeyStoreRSACipherSpi this$0) {
      super(2);
      this.mKeymasterDigest = this$0;
      this.mDigestOutputSizeBytes = (KeymasterUtils.getDigestOutputSizeBits(this$0) + 7) / 8;
    }
    
    protected final void initAlgorithmSpecificParameters() throws InvalidKeyException {}
    
    protected final void initAlgorithmSpecificParameters(AlgorithmParameterSpec param1AlgorithmParameterSpec) throws InvalidAlgorithmParameterException {
      StringBuilder stringBuilder1;
      if (param1AlgorithmParameterSpec == null)
        return; 
      if (param1AlgorithmParameterSpec instanceof OAEPParameterSpec) {
        param1AlgorithmParameterSpec = param1AlgorithmParameterSpec;
        if ("MGF1".equalsIgnoreCase(param1AlgorithmParameterSpec.getMGFAlgorithm())) {
          StringBuilder stringBuilder3;
          String str = param1AlgorithmParameterSpec.getDigestAlgorithm();
          try {
            int i = KeyProperties.Digest.toKeymaster(str);
            if (i == 2 || i == 3 || i == 4 || i == 5 || i == 6) {
              AlgorithmParameterSpec algorithmParameterSpec = param1AlgorithmParameterSpec.getMGFParameters();
              if (algorithmParameterSpec != null) {
                if (algorithmParameterSpec instanceof MGF1ParameterSpec) {
                  algorithmParameterSpec = algorithmParameterSpec;
                  String str1 = algorithmParameterSpec.getDigestAlgorithm();
                  if ("SHA-1".equalsIgnoreCase(str1)) {
                    PSource pSource = param1AlgorithmParameterSpec.getPSource();
                    if (pSource instanceof PSource.PSpecified) {
                      PSource.PSpecified pSpecified = (PSource.PSpecified)pSource;
                      byte[] arrayOfByte = pSpecified.getValue();
                      if (arrayOfByte == null || arrayOfByte.length <= 0) {
                        this.mKeymasterDigest = i;
                        this.mDigestOutputSizeBytes = (KeymasterUtils.getDigestOutputSizeBits(i) + 7) / 8;
                        return;
                      } 
                      StringBuilder stringBuilder6 = new StringBuilder();
                      stringBuilder6.append("Unsupported source of encoding input P: ");
                      stringBuilder6.append(pSource);
                      stringBuilder6.append(". Only pSpecifiedEmpty (PSource.PSpecified.DEFAULT) supported");
                      throw new InvalidAlgorithmParameterException(stringBuilder6.toString());
                    } 
                    stringBuilder3 = new StringBuilder();
                    stringBuilder3.append("Unsupported source of encoding input P: ");
                    stringBuilder3.append(pSource);
                    stringBuilder3.append(". Only pSpecifiedEmpty (PSource.PSpecified.DEFAULT) supported");
                    throw new InvalidAlgorithmParameterException(stringBuilder3.toString());
                  } 
                  StringBuilder stringBuilder5 = new StringBuilder();
                  stringBuilder5.append("Unsupported MGF1 digest: ");
                  stringBuilder5.append((String)stringBuilder3);
                  stringBuilder5.append(". Only ");
                  stringBuilder5.append("SHA-1");
                  stringBuilder5.append(" supported");
                  throw new InvalidAlgorithmParameterException(stringBuilder5.toString());
                } 
                StringBuilder stringBuilder4 = new StringBuilder();
                stringBuilder4.append("Unsupported MGF parameters: ");
                stringBuilder4.append(stringBuilder3);
                stringBuilder4.append(". Only MGF1ParameterSpec supported");
                throw new InvalidAlgorithmParameterException(stringBuilder4.toString());
              } 
              throw new InvalidAlgorithmParameterException("MGF parameters must be provided");
            } 
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Unsupported digest: ");
            stringBuilder1.append((String)stringBuilder3);
            throw new InvalidAlgorithmParameterException(stringBuilder1.toString());
          } catch (IllegalArgumentException illegalArgumentException) {
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Unsupported digest: ");
            stringBuilder1.append((String)stringBuilder3);
            throw new InvalidAlgorithmParameterException(stringBuilder1.toString(), illegalArgumentException);
          } 
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported MGF: ");
        stringBuilder.append(stringBuilder1.getMGFAlgorithm());
        stringBuilder.append(". Only ");
        stringBuilder.append("MGF1");
        stringBuilder.append(" supported");
        throw new InvalidAlgorithmParameterException(stringBuilder.toString());
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Unsupported parameter spec: ");
      stringBuilder2.append(stringBuilder1);
      stringBuilder2.append(". Only OAEPParameterSpec supported");
      throw new InvalidAlgorithmParameterException(stringBuilder2.toString());
    }
    
    protected final void initAlgorithmSpecificParameters(AlgorithmParameters param1AlgorithmParameters) throws InvalidAlgorithmParameterException {
      if (param1AlgorithmParameters == null)
        return; 
      try {
        OAEPParameterSpec oAEPParameterSpec = param1AlgorithmParameters.<OAEPParameterSpec>getParameterSpec(OAEPParameterSpec.class);
        if (oAEPParameterSpec != null) {
          initAlgorithmSpecificParameters(oAEPParameterSpec);
          return;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("OAEP parameters required, but not provided in parameters: ");
        stringBuilder.append(param1AlgorithmParameters);
        throw new InvalidAlgorithmParameterException(stringBuilder.toString());
      } catch (InvalidParameterSpecException invalidParameterSpecException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("OAEP parameters required, but not found in parameters: ");
        stringBuilder.append(param1AlgorithmParameters);
        throw new InvalidAlgorithmParameterException(stringBuilder.toString(), invalidParameterSpecException);
      } 
    }
    
    protected final AlgorithmParameters engineGetParameters() {
      int i = this.mKeymasterDigest;
      OAEPParameterSpec oAEPParameterSpec = new OAEPParameterSpec(KeyProperties.Digest.fromKeymaster(i), "MGF1", MGF1ParameterSpec.SHA1, PSource.PSpecified.DEFAULT);
      try {
        AlgorithmParameters algorithmParameters = AlgorithmParameters.getInstance("OAEP");
        algorithmParameters.init(oAEPParameterSpec);
        return algorithmParameters;
      } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
        throw new ProviderException("Failed to obtain OAEP AlgorithmParameters", noSuchAlgorithmException);
      } catch (InvalidParameterSpecException invalidParameterSpecException) {
        throw new ProviderException("Failed to initialize OAEP AlgorithmParameters with an IV", invalidParameterSpecException);
      } 
    }
    
    protected final void addAlgorithmSpecificParametersToBegin(KeymasterArguments param1KeymasterArguments) {
      super.addAlgorithmSpecificParametersToBegin(param1KeymasterArguments);
      param1KeymasterArguments.addEnum(536870917, this.mKeymasterDigest);
    }
    
    protected final void loadAlgorithmSpecificParametersFromBeginResult(KeymasterArguments param1KeymasterArguments) {
      super.loadAlgorithmSpecificParametersFromBeginResult(param1KeymasterArguments);
    }
    
    protected final int getAdditionalEntropyAmountForBegin() {
      return 0;
    }
    
    protected final int getAdditionalEntropyAmountForFinish() {
      boolean bool;
      if (isEncrypting()) {
        bool = this.mDigestOutputSizeBytes;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  class OAEPWithSHA1AndMGF1Padding extends OAEPWithMGF1Padding {
    public OAEPWithSHA1AndMGF1Padding() {
      super(2);
    }
  }
  
  class OAEPWithSHA224AndMGF1Padding extends OAEPWithMGF1Padding {
    public OAEPWithSHA224AndMGF1Padding() {
      super(3);
    }
  }
  
  class OAEPWithSHA256AndMGF1Padding extends OAEPWithMGF1Padding {
    public OAEPWithSHA256AndMGF1Padding() {
      super(4);
    }
  }
  
  class OAEPWithSHA384AndMGF1Padding extends OAEPWithMGF1Padding {
    public OAEPWithSHA384AndMGF1Padding() {
      super(5);
    }
  }
  
  class OAEPWithSHA512AndMGF1Padding extends OAEPWithMGF1Padding {
    public OAEPWithSHA512AndMGF1Padding() {
      super(6);
    }
  }
  
  private int mModulusSizeBytes = -1;
  
  AndroidKeyStoreRSACipherSpi(int paramInt) {
    this.mKeymasterPadding = paramInt;
  }
  
  protected final void initKey(int paramInt, Key paramKey) throws InvalidKeyException {
    // Byte code:
    //   0: aload_2
    //   1: ifnull -> 578
    //   4: ldc 'RSA'
    //   6: aload_2
    //   7: invokeinterface getAlgorithm : ()Ljava/lang/String;
    //   12: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   15: ifeq -> 512
    //   18: aload_2
    //   19: instanceof android/security/keystore/AndroidKeyStorePrivateKey
    //   22: ifeq -> 33
    //   25: aload_2
    //   26: checkcast android/security/keystore/AndroidKeyStoreKey
    //   29: astore_2
    //   30: goto -> 45
    //   33: aload_2
    //   34: instanceof android/security/keystore/AndroidKeyStorePublicKey
    //   37: ifeq -> 475
    //   40: aload_2
    //   41: checkcast android/security/keystore/AndroidKeyStoreKey
    //   44: astore_2
    //   45: aload_2
    //   46: instanceof java/security/PrivateKey
    //   49: ifeq -> 185
    //   52: iload_1
    //   53: iconst_1
    //   54: if_icmpeq -> 111
    //   57: iload_1
    //   58: iconst_2
    //   59: if_icmpeq -> 108
    //   62: iload_1
    //   63: iconst_3
    //   64: if_icmpeq -> 111
    //   67: iload_1
    //   68: iconst_4
    //   69: if_icmpne -> 75
    //   72: goto -> 108
    //   75: new java/lang/StringBuilder
    //   78: dup
    //   79: invokespecial <init> : ()V
    //   82: astore_2
    //   83: aload_2
    //   84: ldc 'RSA private keys cannot be used with opmode: '
    //   86: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   89: pop
    //   90: aload_2
    //   91: iload_1
    //   92: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   95: pop
    //   96: new java/security/InvalidKeyException
    //   99: dup
    //   100: aload_2
    //   101: invokevirtual toString : ()Ljava/lang/String;
    //   104: invokespecial <init> : (Ljava/lang/String;)V
    //   107: athrow
    //   108: goto -> 305
    //   111: aload_0
    //   112: invokevirtual adjustConfigForEncryptingWithPrivateKey : ()Z
    //   115: ifeq -> 121
    //   118: goto -> 305
    //   121: new java/lang/StringBuilder
    //   124: dup
    //   125: invokespecial <init> : ()V
    //   128: astore_2
    //   129: aload_2
    //   130: ldc 'RSA private keys cannot be used with '
    //   132: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   135: pop
    //   136: aload_2
    //   137: iload_1
    //   138: invokestatic opmodeToString : (I)Ljava/lang/String;
    //   141: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   144: pop
    //   145: aload_2
    //   146: ldc ' and padding '
    //   148: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   151: pop
    //   152: aload_0
    //   153: getfield mKeymasterPadding : I
    //   156: istore_1
    //   157: aload_2
    //   158: iload_1
    //   159: invokestatic fromKeymaster : (I)Ljava/lang/String;
    //   162: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   165: pop
    //   166: aload_2
    //   167: ldc '. Only RSA public keys supported for this mode'
    //   169: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: pop
    //   173: new java/security/InvalidKeyException
    //   176: dup
    //   177: aload_2
    //   178: invokevirtual toString : ()Ljava/lang/String;
    //   181: invokespecial <init> : (Ljava/lang/String;)V
    //   184: athrow
    //   185: iload_1
    //   186: iconst_1
    //   187: if_icmpeq -> 305
    //   190: iload_1
    //   191: iconst_2
    //   192: if_icmpeq -> 241
    //   195: iload_1
    //   196: iconst_3
    //   197: if_icmpeq -> 305
    //   200: iload_1
    //   201: iconst_4
    //   202: if_icmpeq -> 241
    //   205: new java/lang/StringBuilder
    //   208: dup
    //   209: invokespecial <init> : ()V
    //   212: astore_2
    //   213: aload_2
    //   214: ldc 'RSA public keys cannot be used with '
    //   216: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   219: pop
    //   220: aload_2
    //   221: iload_1
    //   222: invokestatic opmodeToString : (I)Ljava/lang/String;
    //   225: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   228: pop
    //   229: new java/security/InvalidKeyException
    //   232: dup
    //   233: aload_2
    //   234: invokevirtual toString : ()Ljava/lang/String;
    //   237: invokespecial <init> : (Ljava/lang/String;)V
    //   240: athrow
    //   241: new java/lang/StringBuilder
    //   244: dup
    //   245: invokespecial <init> : ()V
    //   248: astore_2
    //   249: aload_2
    //   250: ldc 'RSA public keys cannot be used with '
    //   252: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   255: pop
    //   256: aload_2
    //   257: iload_1
    //   258: invokestatic opmodeToString : (I)Ljava/lang/String;
    //   261: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   264: pop
    //   265: aload_2
    //   266: ldc ' and padding '
    //   268: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   271: pop
    //   272: aload_0
    //   273: getfield mKeymasterPadding : I
    //   276: istore_1
    //   277: aload_2
    //   278: iload_1
    //   279: invokestatic fromKeymaster : (I)Ljava/lang/String;
    //   282: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   285: pop
    //   286: aload_2
    //   287: ldc '. Only RSA private keys supported for this opmode.'
    //   289: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   292: pop
    //   293: new java/security/InvalidKeyException
    //   296: dup
    //   297: aload_2
    //   298: invokevirtual toString : ()Ljava/lang/String;
    //   301: invokespecial <init> : (Ljava/lang/String;)V
    //   304: athrow
    //   305: new android/security/keymaster/KeyCharacteristics
    //   308: dup
    //   309: invokespecial <init> : ()V
    //   312: astore_3
    //   313: aload_0
    //   314: invokevirtual getKeyStore : ()Landroid/security/KeyStore;
    //   317: astore #4
    //   319: aload_2
    //   320: invokevirtual getAlias : ()Ljava/lang/String;
    //   323: astore #5
    //   325: aload_2
    //   326: invokevirtual getUid : ()I
    //   329: istore_1
    //   330: aload #4
    //   332: aload #5
    //   334: aconst_null
    //   335: aconst_null
    //   336: iload_1
    //   337: aload_3
    //   338: invokevirtual getKeyCharacteristics : (Ljava/lang/String;Landroid/security/keymaster/KeymasterBlob;Landroid/security/keymaster/KeymasterBlob;ILandroid/security/keymaster/KeyCharacteristics;)I
    //   341: istore_1
    //   342: iload_1
    //   343: iconst_1
    //   344: if_icmpne -> 448
    //   347: aload_3
    //   348: ldc 805306371
    //   350: ldc2_w -1
    //   353: invokevirtual getUnsignedInt : (IJ)J
    //   356: lstore #6
    //   358: lload #6
    //   360: ldc2_w -1
    //   363: lcmp
    //   364: ifeq -> 438
    //   367: lload #6
    //   369: ldc2_w 2147483647
    //   372: lcmp
    //   373: ifgt -> 397
    //   376: aload_0
    //   377: ldc2_w 7
    //   380: lload #6
    //   382: ladd
    //   383: ldc2_w 8
    //   386: ldiv
    //   387: l2i
    //   388: putfield mModulusSizeBytes : I
    //   391: aload_0
    //   392: aload_2
    //   393: invokevirtual setKey : (Landroid/security/keystore/AndroidKeyStoreKey;)V
    //   396: return
    //   397: new java/lang/StringBuilder
    //   400: dup
    //   401: invokespecial <init> : ()V
    //   404: astore_2
    //   405: aload_2
    //   406: ldc 'Key too large: '
    //   408: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   411: pop
    //   412: aload_2
    //   413: lload #6
    //   415: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   418: pop
    //   419: aload_2
    //   420: ldc ' bits'
    //   422: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   425: pop
    //   426: new java/security/InvalidKeyException
    //   429: dup
    //   430: aload_2
    //   431: invokevirtual toString : ()Ljava/lang/String;
    //   434: invokespecial <init> : (Ljava/lang/String;)V
    //   437: athrow
    //   438: new java/security/InvalidKeyException
    //   441: dup
    //   442: ldc 'Size of key not known'
    //   444: invokespecial <init> : (Ljava/lang/String;)V
    //   447: athrow
    //   448: aload_0
    //   449: invokevirtual getKeyStore : ()Landroid/security/KeyStore;
    //   452: astore #5
    //   454: aload_2
    //   455: invokevirtual getAlias : ()Ljava/lang/String;
    //   458: astore_3
    //   459: aload_2
    //   460: invokevirtual getUid : ()I
    //   463: istore #8
    //   465: aload #5
    //   467: aload_3
    //   468: iload #8
    //   470: iload_1
    //   471: invokevirtual getInvalidKeyException : (Ljava/lang/String;II)Ljava/security/InvalidKeyException;
    //   474: athrow
    //   475: new java/lang/StringBuilder
    //   478: dup
    //   479: invokespecial <init> : ()V
    //   482: astore #5
    //   484: aload #5
    //   486: ldc 'Unsupported key type: '
    //   488: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   491: pop
    //   492: aload #5
    //   494: aload_2
    //   495: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   498: pop
    //   499: new java/security/InvalidKeyException
    //   502: dup
    //   503: aload #5
    //   505: invokevirtual toString : ()Ljava/lang/String;
    //   508: invokespecial <init> : (Ljava/lang/String;)V
    //   511: athrow
    //   512: new java/lang/StringBuilder
    //   515: dup
    //   516: invokespecial <init> : ()V
    //   519: astore #5
    //   521: aload #5
    //   523: ldc 'Unsupported key algorithm: '
    //   525: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   528: pop
    //   529: aload #5
    //   531: aload_2
    //   532: invokeinterface getAlgorithm : ()Ljava/lang/String;
    //   537: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   540: pop
    //   541: aload #5
    //   543: ldc '. Only '
    //   545: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   548: pop
    //   549: aload #5
    //   551: ldc 'RSA'
    //   553: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   556: pop
    //   557: aload #5
    //   559: ldc ' supported'
    //   561: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   564: pop
    //   565: new java/security/InvalidKeyException
    //   568: dup
    //   569: aload #5
    //   571: invokevirtual toString : ()Ljava/lang/String;
    //   574: invokespecial <init> : (Ljava/lang/String;)V
    //   577: athrow
    //   578: new java/security/InvalidKeyException
    //   581: dup
    //   582: ldc 'Unsupported key: null'
    //   584: invokespecial <init> : (Ljava/lang/String;)V
    //   587: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #358	-> 0
    //   #361	-> 4
    //   #366	-> 18
    //   #367	-> 25
    //   #368	-> 33
    //   #369	-> 40
    //   #374	-> 45
    //   #376	-> 52
    //   #392	-> 75
    //   #380	-> 108
    //   #383	-> 111
    //   #384	-> 121
    //   #385	-> 136
    //   #387	-> 157
    //   #397	-> 185
    //   #411	-> 205
    //   #412	-> 220
    //   #404	-> 241
    //   #405	-> 256
    //   #407	-> 277
    //   #401	-> 305
    //   #416	-> 305
    //   #417	-> 313
    //   #418	-> 319
    //   #417	-> 330
    //   #419	-> 342
    //   #423	-> 347
    //   #424	-> 358
    //   #426	-> 367
    //   #429	-> 376
    //   #431	-> 391
    //   #432	-> 396
    //   #427	-> 397
    //   #425	-> 438
    //   #420	-> 448
    //   #421	-> 454
    //   #420	-> 465
    //   #371	-> 475
    //   #362	-> 512
    //   #359	-> 578
  }
  
  protected boolean adjustConfigForEncryptingWithPrivateKey() {
    return false;
  }
  
  protected final void resetAll() {
    this.mModulusSizeBytes = -1;
    this.mKeymasterPaddingOverride = -1;
    super.resetAll();
  }
  
  protected final void resetWhilePreservingInitState() {
    super.resetWhilePreservingInitState();
  }
  
  protected void addAlgorithmSpecificParametersToBegin(KeymasterArguments paramKeymasterArguments) {
    paramKeymasterArguments.addEnum(268435458, 1);
    int i = getKeymasterPaddingOverride();
    int j = i;
    if (i == -1)
      j = this.mKeymasterPadding; 
    paramKeymasterArguments.addEnum(536870918, j);
    j = getKeymasterPurposeOverride();
    if (j != -1 && (j == 2 || j == 3))
      paramKeymasterArguments.addEnum(536870917, 0); 
  }
  
  protected void loadAlgorithmSpecificParametersFromBeginResult(KeymasterArguments paramKeymasterArguments) {}
  
  protected final int engineGetBlockSize() {
    return 0;
  }
  
  protected final byte[] engineGetIV() {
    return null;
  }
  
  protected final int engineGetOutputSize(int paramInt) {
    return getModulusSizeBytes();
  }
  
  protected final int getModulusSizeBytes() {
    int i = this.mModulusSizeBytes;
    if (i != -1)
      return i; 
    throw new IllegalStateException("Not initialized");
  }
  
  protected final void setKeymasterPaddingOverride(int paramInt) {
    this.mKeymasterPaddingOverride = paramInt;
  }
  
  protected final int getKeymasterPaddingOverride() {
    return this.mKeymasterPaddingOverride;
  }
}
