package android.security.keystore;

import android.security.keymaster.KeymasterArguments;
import java.security.InvalidKeyException;

abstract class AndroidKeyStoreRSASignatureSpi extends AndroidKeyStoreSignatureSpiBase {
  private final int mKeymasterDigest;
  
  private final int mKeymasterPadding;
  
  class PKCS1Padding extends AndroidKeyStoreRSASignatureSpi {
    PKCS1Padding(AndroidKeyStoreRSASignatureSpi this$0) {
      super(this$0, 5);
    }
    
    protected final int getAdditionalEntropyAmountForSign() {
      return 0;
    }
  }
  
  class NONEWithPKCS1Padding extends PKCS1Padding {
    public NONEWithPKCS1Padding() {
      super(0);
    }
  }
  
  class MD5WithPKCS1Padding extends PKCS1Padding {
    public MD5WithPKCS1Padding() {
      super(1);
    }
  }
  
  class SHA1WithPKCS1Padding extends PKCS1Padding {
    public SHA1WithPKCS1Padding() {
      super(2);
    }
  }
  
  class SHA224WithPKCS1Padding extends PKCS1Padding {
    public SHA224WithPKCS1Padding() {
      super(3);
    }
  }
  
  class SHA256WithPKCS1Padding extends PKCS1Padding {
    public SHA256WithPKCS1Padding() {
      super(4);
    }
  }
  
  class SHA384WithPKCS1Padding extends PKCS1Padding {
    public SHA384WithPKCS1Padding() {
      super(5);
    }
  }
  
  class SHA512WithPKCS1Padding extends PKCS1Padding {
    public SHA512WithPKCS1Padding() {
      super(6);
    }
  }
  
  class PSSPadding extends AndroidKeyStoreRSASignatureSpi {
    private static final int SALT_LENGTH_BYTES = 20;
    
    PSSPadding(AndroidKeyStoreRSASignatureSpi this$0) {
      super(this$0, 3);
    }
    
    protected final int getAdditionalEntropyAmountForSign() {
      return 20;
    }
  }
  
  class SHA1WithPSSPadding extends PSSPadding {
    public SHA1WithPSSPadding() {
      super(2);
    }
  }
  
  class SHA224WithPSSPadding extends PSSPadding {
    public SHA224WithPSSPadding() {
      super(3);
    }
  }
  
  class SHA256WithPSSPadding extends PSSPadding {
    public SHA256WithPSSPadding() {
      super(4);
    }
  }
  
  class SHA384WithPSSPadding extends PSSPadding {
    public SHA384WithPSSPadding() {
      super(5);
    }
  }
  
  class SHA512WithPSSPadding extends PSSPadding {
    public SHA512WithPSSPadding() {
      super(6);
    }
  }
  
  AndroidKeyStoreRSASignatureSpi(int paramInt1, int paramInt2) {
    this.mKeymasterDigest = paramInt1;
    this.mKeymasterPadding = paramInt2;
  }
  
  protected final void initKey(AndroidKeyStoreKey paramAndroidKeyStoreKey) throws InvalidKeyException {
    if ("RSA".equalsIgnoreCase(paramAndroidKeyStoreKey.getAlgorithm())) {
      super.initKey(paramAndroidKeyStoreKey);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported key algorithm: ");
    stringBuilder.append(paramAndroidKeyStoreKey.getAlgorithm());
    stringBuilder.append(". Only");
    stringBuilder.append("RSA");
    stringBuilder.append(" supported");
    throw new InvalidKeyException(stringBuilder.toString());
  }
  
  protected final void resetAll() {
    super.resetAll();
  }
  
  protected final void resetWhilePreservingInitState() {
    super.resetWhilePreservingInitState();
  }
  
  protected final void addAlgorithmSpecificParametersToBegin(KeymasterArguments paramKeymasterArguments) {
    paramKeymasterArguments.addEnum(268435458, 1);
    paramKeymasterArguments.addEnum(536870917, this.mKeymasterDigest);
    paramKeymasterArguments.addEnum(536870918, this.mKeymasterPadding);
  }
}
