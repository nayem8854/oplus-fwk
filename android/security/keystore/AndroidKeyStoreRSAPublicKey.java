package android.security.keystore;

import java.math.BigInteger;
import java.security.interfaces.RSAPublicKey;

public class AndroidKeyStoreRSAPublicKey extends AndroidKeyStorePublicKey implements RSAPublicKey {
  private final BigInteger mModulus;
  
  private final BigInteger mPublicExponent;
  
  public AndroidKeyStoreRSAPublicKey(String paramString, int paramInt, byte[] paramArrayOfbyte, BigInteger paramBigInteger1, BigInteger paramBigInteger2) {
    super(paramString, paramInt, "RSA", paramArrayOfbyte);
    this.mModulus = paramBigInteger1;
    this.mPublicExponent = paramBigInteger2;
  }
  
  public AndroidKeyStoreRSAPublicKey(String paramString, int paramInt, RSAPublicKey paramRSAPublicKey) {
    this(paramString, paramInt, paramRSAPublicKey.getEncoded(), paramRSAPublicKey.getModulus(), paramRSAPublicKey.getPublicExponent());
    if ("X.509".equalsIgnoreCase(paramRSAPublicKey.getFormat()))
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported key export format: ");
    stringBuilder.append(paramRSAPublicKey.getFormat());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public BigInteger getModulus() {
    return this.mModulus;
  }
  
  public BigInteger getPublicExponent() {
    return this.mPublicExponent;
  }
}
