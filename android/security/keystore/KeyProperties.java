package android.security.keystore;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import libcore.util.EmptyArray;

public abstract class KeyProperties {
  public static final int AUTH_BIOMETRIC_STRONG = 2;
  
  public static final int AUTH_DEVICE_CREDENTIAL = 1;
  
  public static final String BLOCK_MODE_CBC = "CBC";
  
  public static final String BLOCK_MODE_CTR = "CTR";
  
  public static final String BLOCK_MODE_ECB = "ECB";
  
  public static final String BLOCK_MODE_GCM = "GCM";
  
  public static final String DIGEST_MD5 = "MD5";
  
  public static final String DIGEST_NONE = "NONE";
  
  public static final String DIGEST_SHA1 = "SHA-1";
  
  public static final String DIGEST_SHA224 = "SHA-224";
  
  public static final String DIGEST_SHA256 = "SHA-256";
  
  public static final String DIGEST_SHA384 = "SHA-384";
  
  public static final String DIGEST_SHA512 = "SHA-512";
  
  public static final String ENCRYPTION_PADDING_NONE = "NoPadding";
  
  public static final String ENCRYPTION_PADDING_PKCS7 = "PKCS7Padding";
  
  public static final String ENCRYPTION_PADDING_RSA_OAEP = "OAEPPadding";
  
  public static final String ENCRYPTION_PADDING_RSA_PKCS1 = "PKCS1Padding";
  
  @Deprecated
  public static final String KEY_ALGORITHM_3DES = "DESede";
  
  public static final String KEY_ALGORITHM_AES = "AES";
  
  public static final String KEY_ALGORITHM_EC = "EC";
  
  public static final String KEY_ALGORITHM_HMAC_SHA1 = "HmacSHA1";
  
  public static final String KEY_ALGORITHM_HMAC_SHA224 = "HmacSHA224";
  
  public static final String KEY_ALGORITHM_HMAC_SHA256 = "HmacSHA256";
  
  public static final String KEY_ALGORITHM_HMAC_SHA384 = "HmacSHA384";
  
  public static final String KEY_ALGORITHM_HMAC_SHA512 = "HmacSHA512";
  
  public static final String KEY_ALGORITHM_RSA = "RSA";
  
  public static final int ORIGIN_GENERATED = 1;
  
  public static final int ORIGIN_IMPORTED = 2;
  
  public static final int ORIGIN_SECURELY_IMPORTED = 8;
  
  public static final int ORIGIN_UNKNOWN = 4;
  
  public static final int PURPOSE_DECRYPT = 2;
  
  public static final int PURPOSE_ENCRYPT = 1;
  
  public static final int PURPOSE_SIGN = 4;
  
  public static final int PURPOSE_VERIFY = 8;
  
  public static final int PURPOSE_WRAP_KEY = 32;
  
  public static final String SIGNATURE_PADDING_RSA_PKCS1 = "PKCS1";
  
  public static final String SIGNATURE_PADDING_RSA_PSS = "PSS";
  
  public static abstract class Purpose {
    public static int toKeymaster(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 4) {
            if (param1Int != 8) {
              if (param1Int == 32)
                return 5; 
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Unknown purpose: ");
              stringBuilder.append(param1Int);
              throw new IllegalArgumentException(stringBuilder.toString());
            } 
            return 3;
          } 
          return 2;
        } 
        return 1;
      } 
      return 0;
    }
    
    public static int fromKeymaster(int param1Int) {
      if (param1Int != 0) {
        if (param1Int != 1) {
          if (param1Int != 2) {
            if (param1Int != 3) {
              if (param1Int == 5)
                return 32; 
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Unknown purpose: ");
              stringBuilder.append(param1Int);
              throw new IllegalArgumentException(stringBuilder.toString());
            } 
            return 8;
          } 
          return 4;
        } 
        return 2;
      } 
      return 1;
    }
    
    public static int[] allToKeymaster(int param1Int) {
      int[] arrayOfInt = KeyProperties.getSetFlags(param1Int);
      for (param1Int = 0; param1Int < arrayOfInt.length; param1Int++)
        arrayOfInt[param1Int] = toKeymaster(arrayOfInt[param1Int]); 
      return arrayOfInt;
    }
    
    public static int allFromKeymaster(Collection<Integer> param1Collection) {
      int i = 0;
      for (Iterator<Integer> iterator = param1Collection.iterator(); iterator.hasNext(); ) {
        int j = ((Integer)iterator.next()).intValue();
        i |= fromKeymaster(j);
      } 
      return i;
    }
  }
  
  public static abstract class KeyAlgorithm {
    public static int toKeymasterAsymmetricKeyAlgorithm(String param1String) {
      if ("EC".equalsIgnoreCase(param1String))
        return 3; 
      if ("RSA".equalsIgnoreCase(param1String))
        return 1; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported key algorithm: ");
      stringBuilder.append(param1String);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public static String fromKeymasterAsymmetricKeyAlgorithm(int param1Int) {
      if (param1Int != 1) {
        if (param1Int == 3)
          return "EC"; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported key algorithm: ");
        stringBuilder.append(param1Int);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return "RSA";
    }
    
    public static int toKeymasterSecretKeyAlgorithm(String param1String) {
      if ("AES".equalsIgnoreCase(param1String))
        return 32; 
      if ("DESede".equalsIgnoreCase(param1String))
        return 33; 
      if (param1String.toUpperCase(Locale.US).startsWith("HMAC"))
        return 128; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported secret key algorithm: ");
      stringBuilder.append(param1String);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public static String fromKeymasterSecretKeyAlgorithm(int param1Int1, int param1Int2) {
      if (param1Int1 != 32) {
        if (param1Int1 != 33) {
          if (param1Int1 == 128) {
            if (param1Int2 != 2) {
              if (param1Int2 != 3) {
                if (param1Int2 != 4) {
                  if (param1Int2 != 5) {
                    if (param1Int2 == 6)
                      return "HmacSHA512"; 
                    StringBuilder stringBuilder1 = new StringBuilder();
                    stringBuilder1.append("Unsupported HMAC digest: ");
                    stringBuilder1.append(KeyProperties.Digest.fromKeymaster(param1Int2));
                    throw new IllegalArgumentException(stringBuilder1.toString());
                  } 
                  return "HmacSHA384";
                } 
                return "HmacSHA256";
              } 
              return "HmacSHA224";
            } 
            return "HmacSHA1";
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unsupported key algorithm: ");
          stringBuilder.append(param1Int1);
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
        return "DESede";
      } 
      return "AES";
    }
    
    public static int toKeymasterDigest(String param1String) {
      param1String = param1String.toUpperCase(Locale.US);
      boolean bool = param1String.startsWith("HMAC");
      byte b = -1;
      if (bool) {
        String str = param1String.substring("HMAC".length());
        switch (str.hashCode()) {
          case 2543909:
            if (str.equals("SHA1"))
              b = 0; 
            break;
          case -1850265334:
            if (str.equals("SHA512"))
              b = 4; 
            break;
          case -1850267037:
            if (str.equals("SHA384"))
              b = 3; 
            break;
          case -1850268089:
            if (str.equals("SHA256"))
              b = 2; 
            break;
          case -1850268184:
            if (str.equals("SHA224"))
              b = 1; 
            break;
        } 
        if (b != 0) {
          if (b != 1) {
            if (b != 2) {
              if (b != 3) {
                if (b == 4)
                  return 6; 
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Unsupported HMAC digest: ");
                stringBuilder.append(str);
                throw new IllegalArgumentException(stringBuilder.toString());
              } 
              return 5;
            } 
            return 4;
          } 
          return 3;
        } 
        return 2;
      } 
      return -1;
    }
  }
  
  public static abstract class BlockMode {
    public static int toKeymaster(String param1String) {
      if ("ECB".equalsIgnoreCase(param1String))
        return 1; 
      if ("CBC".equalsIgnoreCase(param1String))
        return 2; 
      if ("CTR".equalsIgnoreCase(param1String))
        return 3; 
      if ("GCM".equalsIgnoreCase(param1String))
        return 32; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported block mode: ");
      stringBuilder.append(param1String);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public static String fromKeymaster(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int == 32)
              return "GCM"; 
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unsupported block mode: ");
            stringBuilder.append(param1Int);
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
          return "CTR";
        } 
        return "CBC";
      } 
      return "ECB";
    }
    
    public static String[] allFromKeymaster(Collection<Integer> param1Collection) {
      if (param1Collection == null || param1Collection.isEmpty())
        return EmptyArray.STRING; 
      String[] arrayOfString = new String[param1Collection.size()];
      byte b = 0;
      for (Iterator<Integer> iterator = param1Collection.iterator(); iterator.hasNext(); ) {
        int i = ((Integer)iterator.next()).intValue();
        arrayOfString[b] = fromKeymaster(i);
        b++;
      } 
      return arrayOfString;
    }
    
    public static int[] allToKeymaster(String[] param1ArrayOfString) {
      if (param1ArrayOfString == null || param1ArrayOfString.length == 0)
        return EmptyArray.INT; 
      int[] arrayOfInt = new int[param1ArrayOfString.length];
      for (byte b = 0; b < param1ArrayOfString.length; b++)
        arrayOfInt[b] = toKeymaster(param1ArrayOfString[b]); 
      return arrayOfInt;
    }
  }
  
  public static abstract class EncryptionPadding {
    public static int toKeymaster(String param1String) {
      if ("NoPadding".equalsIgnoreCase(param1String))
        return 1; 
      if ("PKCS7Padding".equalsIgnoreCase(param1String))
        return 64; 
      if ("PKCS1Padding".equalsIgnoreCase(param1String))
        return 4; 
      if ("OAEPPadding".equalsIgnoreCase(param1String))
        return 2; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported encryption padding scheme: ");
      stringBuilder.append(param1String);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public static String fromKeymaster(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 4) {
            if (param1Int == 64)
              return "PKCS7Padding"; 
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unsupported encryption padding: ");
            stringBuilder.append(param1Int);
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
          return "PKCS1Padding";
        } 
        return "OAEPPadding";
      } 
      return "NoPadding";
    }
    
    public static int[] allToKeymaster(String[] param1ArrayOfString) {
      if (param1ArrayOfString == null || param1ArrayOfString.length == 0)
        return EmptyArray.INT; 
      int[] arrayOfInt = new int[param1ArrayOfString.length];
      for (byte b = 0; b < param1ArrayOfString.length; b++)
        arrayOfInt[b] = toKeymaster(param1ArrayOfString[b]); 
      return arrayOfInt;
    }
  }
  
  static abstract class SignaturePadding {
    static int toKeymaster(String param1String) {
      // Byte code:
      //   0: aload_0
      //   1: getstatic java/util/Locale.US : Ljava/util/Locale;
      //   4: invokevirtual toUpperCase : (Ljava/util/Locale;)Ljava/lang/String;
      //   7: astore_1
      //   8: aload_1
      //   9: invokevirtual hashCode : ()I
      //   12: istore_2
      //   13: iload_2
      //   14: ldc 79536
      //   16: if_icmpeq -> 42
      //   19: iload_2
      //   20: ldc 76183014
      //   22: if_icmpeq -> 28
      //   25: goto -> 56
      //   28: aload_1
      //   29: ldc 'PKCS1'
      //   31: invokevirtual equals : (Ljava/lang/Object;)Z
      //   34: ifeq -> 25
      //   37: iconst_0
      //   38: istore_2
      //   39: goto -> 58
      //   42: aload_1
      //   43: ldc 'PSS'
      //   45: invokevirtual equals : (Ljava/lang/Object;)Z
      //   48: ifeq -> 25
      //   51: iconst_1
      //   52: istore_2
      //   53: goto -> 58
      //   56: iconst_m1
      //   57: istore_2
      //   58: iload_2
      //   59: ifeq -> 102
      //   62: iload_2
      //   63: iconst_1
      //   64: if_icmpne -> 69
      //   67: iconst_3
      //   68: ireturn
      //   69: new java/lang/StringBuilder
      //   72: dup
      //   73: invokespecial <init> : ()V
      //   76: astore_1
      //   77: aload_1
      //   78: ldc 'Unsupported signature padding scheme: '
      //   80: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   83: pop
      //   84: aload_1
      //   85: aload_0
      //   86: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   89: pop
      //   90: new java/lang/IllegalArgumentException
      //   93: dup
      //   94: aload_1
      //   95: invokevirtual toString : ()Ljava/lang/String;
      //   98: invokespecial <init> : (Ljava/lang/String;)V
      //   101: athrow
      //   102: iconst_5
      //   103: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #503	-> 0
      //   #507	-> 67
      //   #509	-> 69
      //   #505	-> 102
    }
    
    static String fromKeymaster(int param1Int) {
      if (param1Int != 3) {
        if (param1Int == 5)
          return "PKCS1"; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported signature padding: ");
        stringBuilder.append(param1Int);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return "PSS";
    }
    
    static int[] allToKeymaster(String[] param1ArrayOfString) {
      if (param1ArrayOfString == null || param1ArrayOfString.length == 0)
        return EmptyArray.INT; 
      int[] arrayOfInt = new int[param1ArrayOfString.length];
      for (byte b = 0; b < param1ArrayOfString.length; b++)
        arrayOfInt[b] = toKeymaster(param1ArrayOfString[b]); 
      return arrayOfInt;
    }
  }
  
  public static abstract class Digest {
    public static int toKeymaster(String param1String) {
      StringBuilder stringBuilder;
      byte b;
      String str = param1String.toUpperCase(Locale.US);
      switch (str.hashCode()) {
        default:
          b = -1;
          break;
        case 78861104:
          if (str.equals("SHA-1")) {
            b = 0;
            break;
          } 
        case 2402104:
          if (str.equals("NONE")) {
            b = 5;
            break;
          } 
        case 76158:
          if (str.equals("MD5")) {
            b = 6;
            break;
          } 
        case -1523884971:
          if (str.equals("SHA-512")) {
            b = 4;
            break;
          } 
        case -1523886674:
          if (str.equals("SHA-384")) {
            b = 3;
            break;
          } 
        case -1523887726:
          if (str.equals("SHA-256")) {
            b = 2;
            break;
          } 
        case -1523887821:
          if (str.equals("SHA-224")) {
            b = 1;
            break;
          } 
      } 
      switch (b) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unsupported digest algorithm: ");
          stringBuilder.append(param1String);
          throw new IllegalArgumentException(stringBuilder.toString());
        case 6:
          return 1;
        case 5:
          return 0;
        case 4:
          return 6;
        case 3:
          return 5;
        case 2:
          return 4;
        case 1:
          return 3;
        case 0:
          break;
      } 
      return 2;
    }
    
    public static String fromKeymaster(int param1Int) {
      StringBuilder stringBuilder;
      switch (param1Int) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unsupported digest algorithm: ");
          stringBuilder.append(param1Int);
          throw new IllegalArgumentException(stringBuilder.toString());
        case 6:
          return "SHA-512";
        case 5:
          return "SHA-384";
        case 4:
          return "SHA-256";
        case 3:
          return "SHA-224";
        case 2:
          return "SHA-1";
        case 1:
          return "MD5";
        case 0:
          break;
      } 
      return "NONE";
    }
    
    public static String fromKeymasterToSignatureAlgorithmDigest(int param1Int) {
      StringBuilder stringBuilder;
      switch (param1Int) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unsupported digest algorithm: ");
          stringBuilder.append(param1Int);
          throw new IllegalArgumentException(stringBuilder.toString());
        case 6:
          return "SHA512";
        case 5:
          return "SHA384";
        case 4:
          return "SHA256";
        case 3:
          return "SHA224";
        case 2:
          return "SHA1";
        case 1:
          return "MD5";
        case 0:
          break;
      } 
      return "NONE";
    }
    
    public static String[] allFromKeymaster(Collection<Integer> param1Collection) {
      if (param1Collection.isEmpty())
        return EmptyArray.STRING; 
      String[] arrayOfString = new String[param1Collection.size()];
      byte b = 0;
      for (Iterator<Integer> iterator = param1Collection.iterator(); iterator.hasNext(); ) {
        int i = ((Integer)iterator.next()).intValue();
        arrayOfString[b] = fromKeymaster(i);
        b++;
      } 
      return arrayOfString;
    }
    
    public static int[] allToKeymaster(String[] param1ArrayOfString) {
      if (param1ArrayOfString == null || param1ArrayOfString.length == 0)
        return EmptyArray.INT; 
      int[] arrayOfInt = new int[param1ArrayOfString.length];
      byte b1 = 0;
      int i;
      byte b2;
      for (i = param1ArrayOfString.length, b2 = 0; b2 < i; ) {
        String str = param1ArrayOfString[b2];
        arrayOfInt[b1] = toKeymaster(str);
        b1++;
        b2++;
      } 
      return arrayOfInt;
    }
  }
  
  public static abstract class Origin {
    public static int fromKeymaster(int param1Int) {
      if (param1Int != 0) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int == 4)
              return 8; 
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown origin: ");
            stringBuilder.append(param1Int);
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
          return 4;
        } 
        return 2;
      } 
      return 1;
    }
  }
  
  private static int[] getSetFlags(int paramInt) {
    if (paramInt == 0)
      return EmptyArray.INT; 
    int[] arrayOfInt = new int[getSetBitCount(paramInt)];
    int i = 0;
    int j = 1, k = paramInt;
    paramInt = j;
    while (k != 0) {
      j = i;
      if ((k & 0x1) != 0) {
        arrayOfInt[i] = paramInt;
        j = i + 1;
      } 
      k >>>= 1;
      paramInt <<= 1;
      i = j;
    } 
    return arrayOfInt;
  }
  
  private static int getSetBitCount(int paramInt) {
    if (paramInt == 0)
      return 0; 
    int i = 0;
    while (paramInt != 0) {
      int j = i;
      if ((paramInt & 0x1) != 0)
        j = i + 1; 
      paramInt >>>= 1;
      i = j;
    } 
    return i;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface AuthEnum {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface BlockModeEnum {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DigestEnum {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface EncryptionPaddingEnum {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface KeyAlgorithmEnum {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface OriginEnum {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PurposeEnum {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SignaturePaddingEnum {}
}
