package android.security.keystore;

import android.annotation.SystemApi;

@SystemApi
public class DeviceIdAttestationException extends Exception {
  public DeviceIdAttestationException(String paramString) {
    super(paramString);
  }
  
  public DeviceIdAttestationException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
