package android.security.keystore;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.security.keymaster.KeyCharacteristics;

public interface IKeystoreKeyCharacteristicsCallback extends IInterface {
  void onFinished(KeystoreResponse paramKeystoreResponse, KeyCharacteristics paramKeyCharacteristics) throws RemoteException;
  
  class Default implements IKeystoreKeyCharacteristicsCallback {
    public void onFinished(KeystoreResponse param1KeystoreResponse, KeyCharacteristics param1KeyCharacteristics) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IKeystoreKeyCharacteristicsCallback {
    private static final String DESCRIPTOR = "android.security.keystore.IKeystoreKeyCharacteristicsCallback";
    
    static final int TRANSACTION_onFinished = 1;
    
    public Stub() {
      attachInterface(this, "android.security.keystore.IKeystoreKeyCharacteristicsCallback");
    }
    
    public static IKeystoreKeyCharacteristicsCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.keystore.IKeystoreKeyCharacteristicsCallback");
      if (iInterface != null && iInterface instanceof IKeystoreKeyCharacteristicsCallback)
        return (IKeystoreKeyCharacteristicsCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onFinished";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.security.keystore.IKeystoreKeyCharacteristicsCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.security.keystore.IKeystoreKeyCharacteristicsCallback");
      if (param1Parcel1.readInt() != 0) {
        KeystoreResponse keystoreResponse = KeystoreResponse.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        KeyCharacteristics keyCharacteristics = KeyCharacteristics.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onFinished((KeystoreResponse)param1Parcel2, (KeyCharacteristics)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IKeystoreKeyCharacteristicsCallback {
      public static IKeystoreKeyCharacteristicsCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.keystore.IKeystoreKeyCharacteristicsCallback";
      }
      
      public void onFinished(KeystoreResponse param2KeystoreResponse, KeyCharacteristics param2KeyCharacteristics) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.security.keystore.IKeystoreKeyCharacteristicsCallback");
          if (param2KeystoreResponse != null) {
            parcel.writeInt(1);
            param2KeystoreResponse.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2KeyCharacteristics != null) {
            parcel.writeInt(1);
            param2KeyCharacteristics.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IKeystoreKeyCharacteristicsCallback.Stub.getDefaultImpl() != null) {
            IKeystoreKeyCharacteristicsCallback.Stub.getDefaultImpl().onFinished(param2KeystoreResponse, param2KeyCharacteristics);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IKeystoreKeyCharacteristicsCallback param1IKeystoreKeyCharacteristicsCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IKeystoreKeyCharacteristicsCallback != null) {
          Proxy.sDefaultImpl = param1IKeystoreKeyCharacteristicsCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IKeystoreKeyCharacteristicsCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
