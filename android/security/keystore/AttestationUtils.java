package android.security.keystore;

import android.annotation.SystemApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.security.KeyStore;
import android.security.keymaster.KeymasterArguments;
import android.security.keymaster.KeymasterCertificateChain;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.ArraySet;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@SystemApi
public abstract class AttestationUtils {
  public static final int ID_TYPE_IMEI = 2;
  
  public static final int ID_TYPE_MEID = 3;
  
  public static final int ID_TYPE_SERIAL = 1;
  
  public static final int USE_INDIVIDUAL_ATTESTATION = 4;
  
  public static X509Certificate[] parseCertificateChain(KeymasterCertificateChain paramKeymasterCertificateChain) throws KeyAttestationException {
    ByteArrayInputStream byteArrayInputStream;
    List<byte[]> list = paramKeymasterCertificateChain.getCertificates();
    if (list.size() >= 2) {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      try {
        for (byte[] arrayOfByte : list)
          byteArrayOutputStream.write(arrayOfByte); 
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        byteArrayInputStream = new ByteArrayInputStream();
        this(byteArrayOutputStream.toByteArray());
        Collection<? extends Certificate> collection = certificateFactory.generateCertificates(byteArrayInputStream);
        return collection.<X509Certificate>toArray(new X509Certificate[0]);
      } catch (Exception exception) {
        throw new KeyAttestationException("Unable to construct certificate chain", exception);
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Attestation certificate chain contained ");
    stringBuilder.append(byteArrayInputStream.size());
    stringBuilder.append(" entries. At least two are required.");
    throw new KeyAttestationException(stringBuilder.toString());
  }
  
  private static KeymasterArguments prepareAttestationArgumentsForDeviceId(Context paramContext, int[] paramArrayOfint, byte[] paramArrayOfbyte) throws DeviceIdAttestationException {
    if (paramArrayOfint != null)
      return prepareAttestationArguments(paramContext, paramArrayOfint, paramArrayOfbyte); 
    throw new NullPointerException("Missing id types");
  }
  
  public static KeymasterArguments prepareAttestationArguments(Context paramContext, int[] paramArrayOfint, byte[] paramArrayOfbyte) throws DeviceIdAttestationException {
    return prepareAttestationArguments(paramContext, paramArrayOfint, paramArrayOfbyte, Build.BRAND);
  }
  
  public static KeymasterArguments prepareAttestationArgumentsIfMisprovisioned(Context paramContext, int[] paramArrayOfint, byte[] paramArrayOfbyte) throws DeviceIdAttestationException {
    Resources resources = paramContext.getResources();
    String str = resources.getString(17039929);
    if (!TextUtils.isEmpty(str) && !isPotentiallyMisprovisionedDevice(paramContext))
      return null; 
    return prepareAttestationArguments(paramContext, paramArrayOfint, paramArrayOfbyte, str);
  }
  
  private static boolean isPotentiallyMisprovisionedDevice(Context paramContext) {
    Resources resources = paramContext.getResources();
    String str = resources.getString(17039930);
    return Build.MODEL.equals(str);
  }
  
  private static KeymasterArguments prepareAttestationArguments(Context paramContext, int[] paramArrayOfint, byte[] paramArrayOfbyte, String paramString) throws DeviceIdAttestationException {
    if (paramArrayOfbyte != null) {
      TelephonyManager telephonyManager;
      KeymasterArguments keymasterArguments = new KeymasterArguments();
      keymasterArguments.addBytes(-1879047484, paramArrayOfbyte);
      if (paramArrayOfint == null)
        return keymasterArguments; 
      ArraySet<Integer> arraySet = new ArraySet(paramArrayOfint.length);
      int j;
      for (int i = paramArrayOfint.length; j < i; ) {
        int k = paramArrayOfint[j];
        arraySet.add(Integer.valueOf(k));
        j++;
      } 
      paramArrayOfint = null;
      if (arraySet.contains(Integer.valueOf(2)) || arraySet.contains(Integer.valueOf(3))) {
        telephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
        if (telephonyManager == null)
          throw new DeviceIdAttestationException("Unable to access telephony service"); 
      } 
      for (Iterator<Integer> iterator = arraySet.iterator(); iterator.hasNext(); ) {
        Integer integer = iterator.next();
        j = integer.intValue();
        if (j != 1) {
          if (j != 2) {
            if (j != 3) {
              if (j == 4) {
                keymasterArguments.addBoolean(1879048912);
                continue;
              } 
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Unknown device ID type ");
              stringBuilder.append(integer);
              throw new IllegalArgumentException(stringBuilder.toString());
            } 
            String str4 = telephonyManager.getMeid(0);
            if (str4 != null) {
              Charset charset = StandardCharsets.UTF_8;
              byte[] arrayOfByte6 = str4.getBytes(charset);
              keymasterArguments.addBytes(-1879047477, arrayOfByte6);
              continue;
            } 
            throw new DeviceIdAttestationException("Unable to retrieve MEID");
          } 
          String str = telephonyManager.getImei(0);
          if (str != null) {
            Charset charset = StandardCharsets.UTF_8;
            byte[] arrayOfByte6 = str.getBytes(charset);
            keymasterArguments.addBytes(-1879047478, arrayOfByte6);
            continue;
          } 
          throw new DeviceIdAttestationException("Unable to retrieve IMEI");
        } 
        byte[] arrayOfByte = Build.getSerial().getBytes(StandardCharsets.UTF_8);
        keymasterArguments.addBytes(-1879047479, arrayOfByte);
      } 
      Charset charset4 = StandardCharsets.UTF_8;
      byte[] arrayOfByte5 = paramString.getBytes(charset4);
      keymasterArguments.addBytes(-1879047482, arrayOfByte5);
      String str3 = Build.DEVICE;
      Charset charset3 = StandardCharsets.UTF_8;
      byte[] arrayOfByte4 = str3.getBytes(charset3);
      keymasterArguments.addBytes(-1879047481, arrayOfByte4);
      String str1 = Build.PRODUCT;
      Charset charset5 = StandardCharsets.UTF_8;
      byte[] arrayOfByte3 = str1.getBytes(charset5);
      keymasterArguments.addBytes(-1879047480, arrayOfByte3);
      String str2 = Build.MANUFACTURER;
      Charset charset2 = StandardCharsets.UTF_8;
      byte[] arrayOfByte2 = str2.getBytes(charset2);
      keymasterArguments.addBytes(-1879047476, arrayOfByte2);
      str2 = Build.MODEL;
      Charset charset1 = StandardCharsets.UTF_8;
      byte[] arrayOfByte1 = str2.getBytes(charset1);
      keymasterArguments.addBytes(-1879047475, arrayOfByte1);
      return keymasterArguments;
    } 
    throw new NullPointerException("Missing attestation challenge");
  }
  
  public static X509Certificate[] attestDeviceIds(Context paramContext, int[] paramArrayOfint, byte[] paramArrayOfbyte) throws DeviceIdAttestationException {
    KeymasterArguments keymasterArguments = prepareAttestationArgumentsForDeviceId(paramContext, paramArrayOfint, paramArrayOfbyte);
    KeymasterCertificateChain keymasterCertificateChain = new KeymasterCertificateChain();
    int i = KeyStore.getInstance().attestDeviceIds(keymasterArguments, keymasterCertificateChain);
    if (i == 1)
      try {
        return parseCertificateChain(keymasterCertificateChain);
      } catch (KeyAttestationException keyAttestationException) {
        throw new DeviceIdAttestationException(keyAttestationException.getMessage(), keyAttestationException);
      }  
    throw new DeviceIdAttestationException("Unable to perform attestation", KeyStore.getKeyStoreException(i));
  }
  
  public static boolean isChainValid(KeymasterCertificateChain paramKeymasterCertificateChain) {
    boolean bool;
    if (paramKeymasterCertificateChain != null && paramKeymasterCertificateChain.getCertificates().size() >= 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
