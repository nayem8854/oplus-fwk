package android.security.keystore;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.security.keymaster.KeymasterArguments;
import android.security.keymaster.KeymasterBlob;
import java.util.ArrayList;
import java.util.List;

public interface IKeystoreService extends IInterface {
  int abort(IKeystoreResponseCallback paramIKeystoreResponseCallback, IBinder paramIBinder) throws RemoteException;
  
  int addAuthToken(byte[] paramArrayOfbyte) throws RemoteException;
  
  int addRngEntropy(IKeystoreResponseCallback paramIKeystoreResponseCallback, byte[] paramArrayOfbyte, int paramInt) throws RemoteException;
  
  int attestDeviceIds(IKeystoreCertificateChainCallback paramIKeystoreCertificateChainCallback, KeymasterArguments paramKeymasterArguments) throws RemoteException;
  
  int attestKey(IKeystoreCertificateChainCallback paramIKeystoreCertificateChainCallback, String paramString, KeymasterArguments paramKeymasterArguments) throws RemoteException;
  
  int begin(IKeystoreOperationResultCallback paramIKeystoreOperationResultCallback, IBinder paramIBinder, String paramString, int paramInt1, boolean paramBoolean, KeymasterArguments paramKeymasterArguments, byte[] paramArrayOfbyte, int paramInt2) throws RemoteException;
  
  int cancelConfirmationPrompt(IBinder paramIBinder) throws RemoteException;
  
  int clear_uid(long paramLong) throws RemoteException;
  
  int del(String paramString, int paramInt) throws RemoteException;
  
  int exist(String paramString, int paramInt) throws RemoteException;
  
  int exportKey(IKeystoreExportKeyCallback paramIKeystoreExportKeyCallback, String paramString, int paramInt1, KeymasterBlob paramKeymasterBlob1, KeymasterBlob paramKeymasterBlob2, int paramInt2) throws RemoteException;
  
  int finish(IKeystoreOperationResultCallback paramIKeystoreOperationResultCallback, IBinder paramIBinder, KeymasterArguments paramKeymasterArguments, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3) throws RemoteException;
  
  int generateKey(IKeystoreKeyCharacteristicsCallback paramIKeystoreKeyCharacteristicsCallback, String paramString, KeymasterArguments paramKeymasterArguments, byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws RemoteException;
  
  byte[] get(String paramString, int paramInt) throws RemoteException;
  
  byte[] getGateKeeperAuthToken() throws RemoteException;
  
  int getKeyCharacteristics(IKeystoreKeyCharacteristicsCallback paramIKeystoreKeyCharacteristicsCallback, String paramString, KeymasterBlob paramKeymasterBlob1, KeymasterBlob paramKeymasterBlob2, int paramInt) throws RemoteException;
  
  int getState(int paramInt) throws RemoteException;
  
  void getTokensForCredstore(long paramLong1, long paramLong2, int paramInt, ICredstoreTokenCallback paramICredstoreTokenCallback) throws RemoteException;
  
  long getmtime(String paramString, int paramInt) throws RemoteException;
  
  String grant(String paramString, int paramInt) throws RemoteException;
  
  int importKey(IKeystoreKeyCharacteristicsCallback paramIKeystoreKeyCharacteristicsCallback, String paramString, KeymasterArguments paramKeymasterArguments, int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3) throws RemoteException;
  
  int importWrappedKey(IKeystoreKeyCharacteristicsCallback paramIKeystoreKeyCharacteristicsCallback, String paramString1, byte[] paramArrayOfbyte1, String paramString2, byte[] paramArrayOfbyte2, KeymasterArguments paramKeymasterArguments, long paramLong1, long paramLong2) throws RemoteException;
  
  int insert(String paramString, byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws RemoteException;
  
  boolean isConfirmationPromptSupported() throws RemoteException;
  
  int isEmpty(int paramInt) throws RemoteException;
  
  int is_hardware_backed(String paramString) throws RemoteException;
  
  String[] list(String paramString, int paramInt) throws RemoteException;
  
  int listUidsOfAuthBoundKeys(List<String> paramList) throws RemoteException;
  
  int lock(int paramInt) throws RemoteException;
  
  int onDeviceOffBody() throws RemoteException;
  
  int onKeyguardVisibilityChanged(boolean paramBoolean, int paramInt) throws RemoteException;
  
  int onUserAdded(int paramInt1, int paramInt2) throws RemoteException;
  
  int onUserPasswordChanged(int paramInt, String paramString) throws RemoteException;
  
  int onUserRemoved(int paramInt) throws RemoteException;
  
  int presentConfirmationPrompt(IBinder paramIBinder, String paramString1, byte[] paramArrayOfbyte, String paramString2, int paramInt) throws RemoteException;
  
  int ungrant(String paramString, int paramInt) throws RemoteException;
  
  int unlock(int paramInt, String paramString) throws RemoteException;
  
  int update(IKeystoreOperationResultCallback paramIKeystoreOperationResultCallback, IBinder paramIBinder, KeymasterArguments paramKeymasterArguments, byte[] paramArrayOfbyte) throws RemoteException;
  
  class Default implements IKeystoreService {
    public int getState(int param1Int) throws RemoteException {
      return 0;
    }
    
    public byte[] get(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public int insert(String param1String, byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int del(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int exist(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public String[] list(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public int onUserPasswordChanged(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public int lock(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int unlock(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public int isEmpty(int param1Int) throws RemoteException {
      return 0;
    }
    
    public String grant(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public int ungrant(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public long getmtime(String param1String, int param1Int) throws RemoteException {
      return 0L;
    }
    
    public int is_hardware_backed(String param1String) throws RemoteException {
      return 0;
    }
    
    public int clear_uid(long param1Long) throws RemoteException {
      return 0;
    }
    
    public int addRngEntropy(IKeystoreResponseCallback param1IKeystoreResponseCallback, byte[] param1ArrayOfbyte, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int generateKey(IKeystoreKeyCharacteristicsCallback param1IKeystoreKeyCharacteristicsCallback, String param1String, KeymasterArguments param1KeymasterArguments, byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int getKeyCharacteristics(IKeystoreKeyCharacteristicsCallback param1IKeystoreKeyCharacteristicsCallback, String param1String, KeymasterBlob param1KeymasterBlob1, KeymasterBlob param1KeymasterBlob2, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int importKey(IKeystoreKeyCharacteristicsCallback param1IKeystoreKeyCharacteristicsCallback, String param1String, KeymasterArguments param1KeymasterArguments, int param1Int1, byte[] param1ArrayOfbyte, int param1Int2, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public int exportKey(IKeystoreExportKeyCallback param1IKeystoreExportKeyCallback, String param1String, int param1Int1, KeymasterBlob param1KeymasterBlob1, KeymasterBlob param1KeymasterBlob2, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int begin(IKeystoreOperationResultCallback param1IKeystoreOperationResultCallback, IBinder param1IBinder, String param1String, int param1Int1, boolean param1Boolean, KeymasterArguments param1KeymasterArguments, byte[] param1ArrayOfbyte, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int update(IKeystoreOperationResultCallback param1IKeystoreOperationResultCallback, IBinder param1IBinder, KeymasterArguments param1KeymasterArguments, byte[] param1ArrayOfbyte) throws RemoteException {
      return 0;
    }
    
    public int finish(IKeystoreOperationResultCallback param1IKeystoreOperationResultCallback, IBinder param1IBinder, KeymasterArguments param1KeymasterArguments, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3) throws RemoteException {
      return 0;
    }
    
    public int abort(IKeystoreResponseCallback param1IKeystoreResponseCallback, IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public int addAuthToken(byte[] param1ArrayOfbyte) throws RemoteException {
      return 0;
    }
    
    public int onUserAdded(int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int onUserRemoved(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int attestKey(IKeystoreCertificateChainCallback param1IKeystoreCertificateChainCallback, String param1String, KeymasterArguments param1KeymasterArguments) throws RemoteException {
      return 0;
    }
    
    public int attestDeviceIds(IKeystoreCertificateChainCallback param1IKeystoreCertificateChainCallback, KeymasterArguments param1KeymasterArguments) throws RemoteException {
      return 0;
    }
    
    public int onDeviceOffBody() throws RemoteException {
      return 0;
    }
    
    public int importWrappedKey(IKeystoreKeyCharacteristicsCallback param1IKeystoreKeyCharacteristicsCallback, String param1String1, byte[] param1ArrayOfbyte1, String param1String2, byte[] param1ArrayOfbyte2, KeymasterArguments param1KeymasterArguments, long param1Long1, long param1Long2) throws RemoteException {
      return 0;
    }
    
    public int presentConfirmationPrompt(IBinder param1IBinder, String param1String1, byte[] param1ArrayOfbyte, String param1String2, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int cancelConfirmationPrompt(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public boolean isConfirmationPromptSupported() throws RemoteException {
      return false;
    }
    
    public int onKeyguardVisibilityChanged(boolean param1Boolean, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int listUidsOfAuthBoundKeys(List<String> param1List) throws RemoteException {
      return 0;
    }
    
    public void getTokensForCredstore(long param1Long1, long param1Long2, int param1Int, ICredstoreTokenCallback param1ICredstoreTokenCallback) throws RemoteException {}
    
    public byte[] getGateKeeperAuthToken() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IKeystoreService {
    private static final String DESCRIPTOR = "android.security.keystore.IKeystoreService";
    
    static final int TRANSACTION_abort = 24;
    
    static final int TRANSACTION_addAuthToken = 25;
    
    static final int TRANSACTION_addRngEntropy = 16;
    
    static final int TRANSACTION_attestDeviceIds = 29;
    
    static final int TRANSACTION_attestKey = 28;
    
    static final int TRANSACTION_begin = 21;
    
    static final int TRANSACTION_cancelConfirmationPrompt = 33;
    
    static final int TRANSACTION_clear_uid = 15;
    
    static final int TRANSACTION_del = 4;
    
    static final int TRANSACTION_exist = 5;
    
    static final int TRANSACTION_exportKey = 20;
    
    static final int TRANSACTION_finish = 23;
    
    static final int TRANSACTION_generateKey = 17;
    
    static final int TRANSACTION_get = 2;
    
    static final int TRANSACTION_getGateKeeperAuthToken = 38;
    
    static final int TRANSACTION_getKeyCharacteristics = 18;
    
    static final int TRANSACTION_getState = 1;
    
    static final int TRANSACTION_getTokensForCredstore = 37;
    
    static final int TRANSACTION_getmtime = 13;
    
    static final int TRANSACTION_grant = 11;
    
    static final int TRANSACTION_importKey = 19;
    
    static final int TRANSACTION_importWrappedKey = 31;
    
    static final int TRANSACTION_insert = 3;
    
    static final int TRANSACTION_isConfirmationPromptSupported = 34;
    
    static final int TRANSACTION_isEmpty = 10;
    
    static final int TRANSACTION_is_hardware_backed = 14;
    
    static final int TRANSACTION_list = 6;
    
    static final int TRANSACTION_listUidsOfAuthBoundKeys = 36;
    
    static final int TRANSACTION_lock = 8;
    
    static final int TRANSACTION_onDeviceOffBody = 30;
    
    static final int TRANSACTION_onKeyguardVisibilityChanged = 35;
    
    static final int TRANSACTION_onUserAdded = 26;
    
    static final int TRANSACTION_onUserPasswordChanged = 7;
    
    static final int TRANSACTION_onUserRemoved = 27;
    
    static final int TRANSACTION_presentConfirmationPrompt = 32;
    
    static final int TRANSACTION_ungrant = 12;
    
    static final int TRANSACTION_unlock = 9;
    
    static final int TRANSACTION_update = 22;
    
    public Stub() {
      attachInterface(this, "android.security.keystore.IKeystoreService");
    }
    
    public static IKeystoreService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.keystore.IKeystoreService");
      if (iInterface != null && iInterface instanceof IKeystoreService)
        return (IKeystoreService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 38:
          return "getGateKeeperAuthToken";
        case 37:
          return "getTokensForCredstore";
        case 36:
          return "listUidsOfAuthBoundKeys";
        case 35:
          return "onKeyguardVisibilityChanged";
        case 34:
          return "isConfirmationPromptSupported";
        case 33:
          return "cancelConfirmationPrompt";
        case 32:
          return "presentConfirmationPrompt";
        case 31:
          return "importWrappedKey";
        case 30:
          return "onDeviceOffBody";
        case 29:
          return "attestDeviceIds";
        case 28:
          return "attestKey";
        case 27:
          return "onUserRemoved";
        case 26:
          return "onUserAdded";
        case 25:
          return "addAuthToken";
        case 24:
          return "abort";
        case 23:
          return "finish";
        case 22:
          return "update";
        case 21:
          return "begin";
        case 20:
          return "exportKey";
        case 19:
          return "importKey";
        case 18:
          return "getKeyCharacteristics";
        case 17:
          return "generateKey";
        case 16:
          return "addRngEntropy";
        case 15:
          return "clear_uid";
        case 14:
          return "is_hardware_backed";
        case 13:
          return "getmtime";
        case 12:
          return "ungrant";
        case 11:
          return "grant";
        case 10:
          return "isEmpty";
        case 9:
          return "unlock";
        case 8:
          return "lock";
        case 7:
          return "onUserPasswordChanged";
        case 6:
          return "list";
        case 5:
          return "exist";
        case 4:
          return "del";
        case 3:
          return "insert";
        case 2:
          return "get";
        case 1:
          break;
      } 
      return "getState";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        byte[] arrayOfByte4;
        ICredstoreTokenCallback iCredstoreTokenCallback;
        ArrayList<String> arrayList;
        IBinder iBinder2;
        byte[] arrayOfByte3;
        IBinder iBinder1;
        byte[] arrayOfByte2;
        String str1, arrayOfString[];
        byte[] arrayOfByte1;
        long l1, l2;
        IBinder iBinder4;
        String str3;
        IKeystoreCertificateChainCallback iKeystoreCertificateChainCallback1;
        IBinder iBinder3;
        IKeystoreOperationResultCallback iKeystoreOperationResultCallback1;
        IKeystoreKeyCharacteristicsCallback iKeystoreKeyCharacteristicsCallback1;
        byte[] arrayOfByte5;
        String str2, str6;
        IKeystoreOperationResultCallback iKeystoreOperationResultCallback2;
        IBinder iBinder5;
        String str5;
        IKeystoreExportKeyCallback iKeystoreExportKeyCallback;
        String str4;
        byte[] arrayOfByte8;
        IBinder iBinder6;
        String str7;
        byte[] arrayOfByte7;
        IKeystoreKeyCharacteristicsCallback iKeystoreKeyCharacteristicsCallback2;
        byte[] arrayOfByte6;
        String str11;
        IKeystoreCertificateChainCallback iKeystoreCertificateChainCallback2;
        String str10;
        IKeystoreResponseCallback iKeystoreResponseCallback;
        String str9;
        byte[] arrayOfByte9;
        String str8;
        IKeystoreKeyCharacteristicsCallback iKeystoreKeyCharacteristicsCallback3;
        byte[] arrayOfByte10, arrayOfByte11;
        int j;
        boolean bool1 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 38:
            param1Parcel1.enforceInterface("android.security.keystore.IKeystoreService");
            arrayOfByte4 = getGateKeeperAuthToken();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte4);
            return true;
          case 37:
            arrayOfByte4.enforceInterface("android.security.keystore.IKeystoreService");
            l1 = arrayOfByte4.readLong();
            l2 = arrayOfByte4.readLong();
            param1Int1 = arrayOfByte4.readInt();
            iCredstoreTokenCallback = ICredstoreTokenCallback.Stub.asInterface(arrayOfByte4.readStrongBinder());
            getTokensForCredstore(l1, l2, param1Int1, iCredstoreTokenCallback);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            iCredstoreTokenCallback.enforceInterface("android.security.keystore.IKeystoreService");
            arrayList = new ArrayList();
            param1Int1 = listUidsOfAuthBoundKeys(arrayList);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            param1Parcel2.writeStringList(arrayList);
            return true;
          case 35:
            arrayList.enforceInterface("android.security.keystore.IKeystoreService");
            if (arrayList.readInt() != 0)
              bool1 = true; 
            param1Int1 = arrayList.readInt();
            param1Int1 = onKeyguardVisibilityChanged(bool1, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 34:
            arrayList.enforceInterface("android.security.keystore.IKeystoreService");
            bool = isConfirmationPromptSupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 33:
            arrayList.enforceInterface("android.security.keystore.IKeystoreService");
            iBinder2 = arrayList.readStrongBinder();
            i = cancelConfirmationPrompt(iBinder2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 32:
            iBinder2.enforceInterface("android.security.keystore.IKeystoreService");
            iBinder4 = iBinder2.readStrongBinder();
            str6 = iBinder2.readString();
            arrayOfByte8 = iBinder2.createByteArray();
            str11 = iBinder2.readString();
            i = iBinder2.readInt();
            i = presentConfirmationPrompt(iBinder4, str6, arrayOfByte8, str11, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 31:
            iBinder2.enforceInterface("android.security.keystore.IKeystoreService");
            iKeystoreKeyCharacteristicsCallback3 = IKeystoreKeyCharacteristicsCallback.Stub.asInterface(iBinder2.readStrongBinder());
            str6 = iBinder2.readString();
            arrayOfByte11 = iBinder2.createByteArray();
            str3 = iBinder2.readString();
            arrayOfByte8 = iBinder2.createByteArray();
            if (iBinder2.readInt() != 0) {
              KeymasterArguments keymasterArguments = KeymasterArguments.CREATOR.createFromParcel((Parcel)iBinder2);
            } else {
              str11 = null;
            } 
            l1 = iBinder2.readLong();
            l2 = iBinder2.readLong();
            i = importWrappedKey(iKeystoreKeyCharacteristicsCallback3, str6, arrayOfByte11, str3, arrayOfByte8, (KeymasterArguments)str11, l1, l2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 30:
            iBinder2.enforceInterface("android.security.keystore.IKeystoreService");
            i = onDeviceOffBody();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 29:
            iBinder2.enforceInterface("android.security.keystore.IKeystoreService");
            iKeystoreCertificateChainCallback2 = IKeystoreCertificateChainCallback.Stub.asInterface(iBinder2.readStrongBinder());
            if (iBinder2.readInt() != 0) {
              KeymasterArguments keymasterArguments = KeymasterArguments.CREATOR.createFromParcel((Parcel)iBinder2);
            } else {
              iBinder2 = null;
            } 
            i = attestDeviceIds(iKeystoreCertificateChainCallback2, (KeymasterArguments)iBinder2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 28:
            iBinder2.enforceInterface("android.security.keystore.IKeystoreService");
            iKeystoreCertificateChainCallback1 = IKeystoreCertificateChainCallback.Stub.asInterface(iBinder2.readStrongBinder());
            str10 = iBinder2.readString();
            if (iBinder2.readInt() != 0) {
              KeymasterArguments keymasterArguments = KeymasterArguments.CREATOR.createFromParcel((Parcel)iBinder2);
            } else {
              iBinder2 = null;
            } 
            i = attestKey(iKeystoreCertificateChainCallback1, str10, (KeymasterArguments)iBinder2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 27:
            iBinder2.enforceInterface("android.security.keystore.IKeystoreService");
            i = iBinder2.readInt();
            i = onUserRemoved(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 26:
            iBinder2.enforceInterface("android.security.keystore.IKeystoreService");
            param1Int2 = iBinder2.readInt();
            i = iBinder2.readInt();
            i = onUserAdded(param1Int2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 25:
            iBinder2.enforceInterface("android.security.keystore.IKeystoreService");
            arrayOfByte3 = iBinder2.createByteArray();
            i = addAuthToken(arrayOfByte3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 24:
            arrayOfByte3.enforceInterface("android.security.keystore.IKeystoreService");
            iKeystoreResponseCallback = IKeystoreResponseCallback.Stub.asInterface(arrayOfByte3.readStrongBinder());
            iBinder1 = arrayOfByte3.readStrongBinder();
            i = abort(iKeystoreResponseCallback, iBinder1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 23:
            iBinder1.enforceInterface("android.security.keystore.IKeystoreService");
            iKeystoreOperationResultCallback2 = IKeystoreOperationResultCallback.Stub.asInterface(iBinder1.readStrongBinder());
            iBinder3 = iBinder1.readStrongBinder();
            if (iBinder1.readInt() != 0) {
              KeymasterArguments keymasterArguments = KeymasterArguments.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iKeystoreResponseCallback = null;
            } 
            arrayOfByte10 = iBinder1.createByteArray();
            arrayOfByte8 = iBinder1.createByteArray();
            arrayOfByte2 = iBinder1.createByteArray();
            i = finish(iKeystoreOperationResultCallback2, iBinder3, (KeymasterArguments)iKeystoreResponseCallback, arrayOfByte10, arrayOfByte8, arrayOfByte2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 22:
            arrayOfByte2.enforceInterface("android.security.keystore.IKeystoreService");
            iKeystoreOperationResultCallback1 = IKeystoreOperationResultCallback.Stub.asInterface(arrayOfByte2.readStrongBinder());
            iBinder5 = arrayOfByte2.readStrongBinder();
            if (arrayOfByte2.readInt() != 0) {
              KeymasterArguments keymasterArguments = KeymasterArguments.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              iKeystoreResponseCallback = null;
            } 
            arrayOfByte2 = arrayOfByte2.createByteArray();
            i = update(iKeystoreOperationResultCallback1, iBinder5, (KeymasterArguments)iKeystoreResponseCallback, arrayOfByte2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 21:
            arrayOfByte2.enforceInterface("android.security.keystore.IKeystoreService");
            iKeystoreOperationResultCallback1 = IKeystoreOperationResultCallback.Stub.asInterface(arrayOfByte2.readStrongBinder());
            iBinder6 = arrayOfByte2.readStrongBinder();
            str5 = arrayOfByte2.readString();
            param1Int2 = arrayOfByte2.readInt();
            if (arrayOfByte2.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            if (arrayOfByte2.readInt() != 0) {
              KeymasterArguments keymasterArguments = KeymasterArguments.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              iKeystoreResponseCallback = null;
            } 
            arrayOfByte10 = arrayOfByte2.createByteArray();
            i = arrayOfByte2.readInt();
            i = begin(iKeystoreOperationResultCallback1, iBinder6, str5, param1Int2, bool1, (KeymasterArguments)iKeystoreResponseCallback, arrayOfByte10, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 20:
            arrayOfByte2.enforceInterface("android.security.keystore.IKeystoreService");
            iKeystoreExportKeyCallback = IKeystoreExportKeyCallback.Stub.asInterface(arrayOfByte2.readStrongBinder());
            str7 = arrayOfByte2.readString();
            i = arrayOfByte2.readInt();
            if (arrayOfByte2.readInt() != 0) {
              KeymasterBlob keymasterBlob = KeymasterBlob.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              iKeystoreResponseCallback = null;
            } 
            if (arrayOfByte2.readInt() != 0) {
              KeymasterBlob keymasterBlob = KeymasterBlob.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              iKeystoreOperationResultCallback1 = null;
            } 
            param1Int2 = arrayOfByte2.readInt();
            i = exportKey(iKeystoreExportKeyCallback, str7, i, (KeymasterBlob)iKeystoreResponseCallback, (KeymasterBlob)iKeystoreOperationResultCallback1, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 19:
            arrayOfByte2.enforceInterface("android.security.keystore.IKeystoreService");
            iKeystoreKeyCharacteristicsCallback1 = IKeystoreKeyCharacteristicsCallback.Stub.asInterface(arrayOfByte2.readStrongBinder());
            str4 = arrayOfByte2.readString();
            if (arrayOfByte2.readInt() != 0) {
              KeymasterArguments keymasterArguments = KeymasterArguments.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              iKeystoreResponseCallback = null;
            } 
            j = arrayOfByte2.readInt();
            arrayOfByte7 = arrayOfByte2.createByteArray();
            param1Int2 = arrayOfByte2.readInt();
            i = arrayOfByte2.readInt();
            i = importKey(iKeystoreKeyCharacteristicsCallback1, str4, (KeymasterArguments)iKeystoreResponseCallback, j, arrayOfByte7, param1Int2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 18:
            arrayOfByte2.enforceInterface("android.security.keystore.IKeystoreService");
            iKeystoreKeyCharacteristicsCallback2 = IKeystoreKeyCharacteristicsCallback.Stub.asInterface(arrayOfByte2.readStrongBinder());
            str4 = arrayOfByte2.readString();
            if (arrayOfByte2.readInt() != 0) {
              KeymasterBlob keymasterBlob = KeymasterBlob.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              iKeystoreResponseCallback = null;
            } 
            if (arrayOfByte2.readInt() != 0) {
              KeymasterBlob keymasterBlob = KeymasterBlob.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              iKeystoreKeyCharacteristicsCallback1 = null;
            } 
            i = arrayOfByte2.readInt();
            i = getKeyCharacteristics(iKeystoreKeyCharacteristicsCallback2, str4, (KeymasterBlob)iKeystoreResponseCallback, (KeymasterBlob)iKeystoreKeyCharacteristicsCallback1, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 17:
            arrayOfByte2.enforceInterface("android.security.keystore.IKeystoreService");
            iKeystoreKeyCharacteristicsCallback1 = IKeystoreKeyCharacteristicsCallback.Stub.asInterface(arrayOfByte2.readStrongBinder());
            str4 = arrayOfByte2.readString();
            if (arrayOfByte2.readInt() != 0) {
              KeymasterArguments keymasterArguments = KeymasterArguments.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              iKeystoreResponseCallback = null;
            } 
            arrayOfByte6 = arrayOfByte2.createByteArray();
            param1Int2 = arrayOfByte2.readInt();
            i = arrayOfByte2.readInt();
            i = generateKey(iKeystoreKeyCharacteristicsCallback1, str4, (KeymasterArguments)iKeystoreResponseCallback, arrayOfByte6, param1Int2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 16:
            arrayOfByte2.enforceInterface("android.security.keystore.IKeystoreService");
            iKeystoreResponseCallback = IKeystoreResponseCallback.Stub.asInterface(arrayOfByte2.readStrongBinder());
            arrayOfByte5 = arrayOfByte2.createByteArray();
            i = arrayOfByte2.readInt();
            i = addRngEntropy(iKeystoreResponseCallback, arrayOfByte5, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 15:
            arrayOfByte2.enforceInterface("android.security.keystore.IKeystoreService");
            l2 = arrayOfByte2.readLong();
            i = clear_uid(l2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 14:
            arrayOfByte2.enforceInterface("android.security.keystore.IKeystoreService");
            str1 = arrayOfByte2.readString();
            i = is_hardware_backed(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 13:
            str1.enforceInterface("android.security.keystore.IKeystoreService");
            str9 = str1.readString();
            i = str1.readInt();
            l2 = getmtime(str9, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l2);
            return true;
          case 12:
            str1.enforceInterface("android.security.keystore.IKeystoreService");
            str9 = str1.readString();
            i = str1.readInt();
            i = ungrant(str9, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 11:
            str1.enforceInterface("android.security.keystore.IKeystoreService");
            str9 = str1.readString();
            i = str1.readInt();
            str1 = grant(str9, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 10:
            str1.enforceInterface("android.security.keystore.IKeystoreService");
            i = str1.readInt();
            i = isEmpty(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 9:
            str1.enforceInterface("android.security.keystore.IKeystoreService");
            i = str1.readInt();
            str1 = str1.readString();
            i = unlock(i, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 8:
            str1.enforceInterface("android.security.keystore.IKeystoreService");
            i = str1.readInt();
            i = lock(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 7:
            str1.enforceInterface("android.security.keystore.IKeystoreService");
            i = str1.readInt();
            str1 = str1.readString();
            i = onUserPasswordChanged(i, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 6:
            str1.enforceInterface("android.security.keystore.IKeystoreService");
            str9 = str1.readString();
            i = str1.readInt();
            arrayOfString = list(str9, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 5:
            arrayOfString.enforceInterface("android.security.keystore.IKeystoreService");
            str9 = arrayOfString.readString();
            i = arrayOfString.readInt();
            i = exist(str9, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 4:
            arrayOfString.enforceInterface("android.security.keystore.IKeystoreService");
            str9 = arrayOfString.readString();
            i = arrayOfString.readInt();
            i = del(str9, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 3:
            arrayOfString.enforceInterface("android.security.keystore.IKeystoreService");
            str2 = arrayOfString.readString();
            arrayOfByte9 = arrayOfString.createByteArray();
            param1Int2 = arrayOfString.readInt();
            i = arrayOfString.readInt();
            i = insert(str2, arrayOfByte9, param1Int2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 2:
            arrayOfString.enforceInterface("android.security.keystore.IKeystoreService");
            str8 = arrayOfString.readString();
            i = arrayOfString.readInt();
            arrayOfByte1 = get(str8, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte1);
            return true;
          case 1:
            break;
        } 
        arrayOfByte1.enforceInterface("android.security.keystore.IKeystoreService");
        int i = arrayOfByte1.readInt();
        i = getState(i);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("android.security.keystore.IKeystoreService");
      return true;
    }
    
    private static class Proxy implements IKeystoreService {
      public static IKeystoreService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.keystore.IKeystoreService";
      }
      
      public int getState(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int = IKeystoreService.Stub.getDefaultImpl().getState(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] get(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().get(param2String, param2Int); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int insert(String param2String, byte[] param2ArrayOfbyte, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int1 = IKeystoreService.Stub.getDefaultImpl().insert(param2String, param2ArrayOfbyte, param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int del(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int = IKeystoreService.Stub.getDefaultImpl().del(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int exist(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int = IKeystoreService.Stub.getDefaultImpl().exist(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] list(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().list(param2String, param2Int); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int onUserPasswordChanged(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int = IKeystoreService.Stub.getDefaultImpl().onUserPasswordChanged(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int lock(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int = IKeystoreService.Stub.getDefaultImpl().lock(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int unlock(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int = IKeystoreService.Stub.getDefaultImpl().unlock(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int isEmpty(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int = IKeystoreService.Stub.getDefaultImpl().isEmpty(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String grant(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2String = IKeystoreService.Stub.getDefaultImpl().grant(param2String, param2Int);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int ungrant(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int = IKeystoreService.Stub.getDefaultImpl().ungrant(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getmtime(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().getmtime(param2String, param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int is_hardware_backed(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().is_hardware_backed(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int clear_uid(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().clear_uid(param2Long); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int addRngEntropy(IKeystoreResponseCallback param2IKeystoreResponseCallback, byte[] param2ArrayOfbyte, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          if (param2IKeystoreResponseCallback != null) {
            iBinder = param2IKeystoreResponseCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int = IKeystoreService.Stub.getDefaultImpl().addRngEntropy(param2IKeystoreResponseCallback, param2ArrayOfbyte, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int generateKey(IKeystoreKeyCharacteristicsCallback param2IKeystoreKeyCharacteristicsCallback, String param2String, KeymasterArguments param2KeymasterArguments, byte[] param2ArrayOfbyte, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          if (param2IKeystoreKeyCharacteristicsCallback != null) {
            iBinder = param2IKeystoreKeyCharacteristicsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          try {
            parcel1.writeString(param2String);
            if (param2KeymasterArguments != null) {
              parcel1.writeInt(1);
              param2KeymasterArguments.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeByteArray(param2ArrayOfbyte);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
                    if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
                      param2Int1 = IKeystoreService.Stub.getDefaultImpl().generateKey(param2IKeystoreKeyCharacteristicsCallback, param2String, param2KeymasterArguments, param2ArrayOfbyte, param2Int1, param2Int2);
                      parcel2.recycle();
                      parcel1.recycle();
                      return param2Int1;
                    } 
                    parcel2.readException();
                    param2Int1 = parcel2.readInt();
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Int1;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IKeystoreKeyCharacteristicsCallback;
      }
      
      public int getKeyCharacteristics(IKeystoreKeyCharacteristicsCallback param2IKeystoreKeyCharacteristicsCallback, String param2String, KeymasterBlob param2KeymasterBlob1, KeymasterBlob param2KeymasterBlob2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          if (param2IKeystoreKeyCharacteristicsCallback != null) {
            iBinder = param2IKeystoreKeyCharacteristicsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          if (param2KeymasterBlob1 != null) {
            parcel1.writeInt(1);
            param2KeymasterBlob1.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2KeymasterBlob2 != null) {
            parcel1.writeInt(1);
            param2KeymasterBlob2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int = IKeystoreService.Stub.getDefaultImpl().getKeyCharacteristics(param2IKeystoreKeyCharacteristicsCallback, param2String, param2KeymasterBlob1, param2KeymasterBlob2, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int importKey(IKeystoreKeyCharacteristicsCallback param2IKeystoreKeyCharacteristicsCallback, String param2String, KeymasterArguments param2KeymasterArguments, int param2Int1, byte[] param2ArrayOfbyte, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          if (param2IKeystoreKeyCharacteristicsCallback != null) {
            iBinder = param2IKeystoreKeyCharacteristicsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          try {
            parcel1.writeString(param2String);
            if (param2KeymasterArguments != null) {
              parcel1.writeInt(1);
              param2KeymasterArguments.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeInt(param2Int1);
              try {
                parcel1.writeByteArray(param2ArrayOfbyte);
                try {
                  parcel1.writeInt(param2Int2);
                  parcel1.writeInt(param2Int3);
                  boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
                  if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
                    param2Int1 = IKeystoreService.Stub.getDefaultImpl().importKey(param2IKeystoreKeyCharacteristicsCallback, param2String, param2KeymasterArguments, param2Int1, param2ArrayOfbyte, param2Int2, param2Int3);
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Int1;
                  } 
                  parcel2.readException();
                  param2Int1 = parcel2.readInt();
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Int1;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IKeystoreKeyCharacteristicsCallback;
      }
      
      public int exportKey(IKeystoreExportKeyCallback param2IKeystoreExportKeyCallback, String param2String, int param2Int1, KeymasterBlob param2KeymasterBlob1, KeymasterBlob param2KeymasterBlob2, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          if (param2IKeystoreExportKeyCallback != null) {
            iBinder = param2IKeystoreExportKeyCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          try {
            parcel1.writeString(param2String);
            try {
              parcel1.writeInt(param2Int1);
              if (param2KeymasterBlob1 != null) {
                parcel1.writeInt(1);
                param2KeymasterBlob1.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              if (param2KeymasterBlob2 != null) {
                parcel1.writeInt(1);
                param2KeymasterBlob2.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeInt(param2Int2);
                try {
                  boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
                  if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
                    param2Int1 = IKeystoreService.Stub.getDefaultImpl().exportKey(param2IKeystoreExportKeyCallback, param2String, param2Int1, param2KeymasterBlob1, param2KeymasterBlob2, param2Int2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Int1;
                  } 
                  parcel2.readException();
                  param2Int1 = parcel2.readInt();
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Int1;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IKeystoreExportKeyCallback;
      }
      
      public int begin(IKeystoreOperationResultCallback param2IKeystoreOperationResultCallback, IBinder param2IBinder, String param2String, int param2Int1, boolean param2Boolean, KeymasterArguments param2KeymasterArguments, byte[] param2ArrayOfbyte, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          if (param2IKeystoreOperationResultCallback != null) {
            iBinder = param2IKeystoreOperationResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeString(param2String);
              try {
                boolean bool;
                parcel1.writeInt(param2Int1);
                if (param2Boolean) {
                  bool = true;
                } else {
                  bool = false;
                } 
                parcel1.writeInt(bool);
                if (param2KeymasterArguments != null) {
                  parcel1.writeInt(1);
                  param2KeymasterArguments.writeToParcel(parcel1, 0);
                } else {
                  parcel1.writeInt(0);
                } 
                parcel1.writeByteArray(param2ArrayOfbyte);
                parcel1.writeInt(param2Int2);
                boolean bool1 = this.mRemote.transact(21, parcel1, parcel2, 0);
                if (!bool1 && IKeystoreService.Stub.getDefaultImpl() != null) {
                  param2Int1 = IKeystoreService.Stub.getDefaultImpl().begin(param2IKeystoreOperationResultCallback, param2IBinder, param2String, param2Int1, param2Boolean, param2KeymasterArguments, param2ArrayOfbyte, param2Int2);
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Int1;
                } 
                parcel2.readException();
                param2Int1 = parcel2.readInt();
                parcel2.recycle();
                parcel1.recycle();
                return param2Int1;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IKeystoreOperationResultCallback;
      }
      
      public int update(IKeystoreOperationResultCallback param2IKeystoreOperationResultCallback, IBinder param2IBinder, KeymasterArguments param2KeymasterArguments, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          if (param2IKeystoreOperationResultCallback != null) {
            iBinder = param2IKeystoreOperationResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeStrongBinder(param2IBinder);
          if (param2KeymasterArguments != null) {
            parcel1.writeInt(1);
            param2KeymasterArguments.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().update(param2IKeystoreOperationResultCallback, param2IBinder, param2KeymasterArguments, param2ArrayOfbyte); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int finish(IKeystoreOperationResultCallback param2IKeystoreOperationResultCallback, IBinder param2IBinder, KeymasterArguments param2KeymasterArguments, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2, byte[] param2ArrayOfbyte3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          if (param2IKeystoreOperationResultCallback != null) {
            iBinder = param2IKeystoreOperationResultCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          try {
            parcel1.writeStrongBinder(param2IBinder);
            if (param2KeymasterArguments != null) {
              parcel1.writeInt(1);
              param2KeymasterArguments.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeByteArray(param2ArrayOfbyte1);
              try {
                parcel1.writeByteArray(param2ArrayOfbyte2);
                try {
                  parcel1.writeByteArray(param2ArrayOfbyte3);
                  try {
                    boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
                    if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
                      int j = IKeystoreService.Stub.getDefaultImpl().finish(param2IKeystoreOperationResultCallback, param2IBinder, param2KeymasterArguments, param2ArrayOfbyte1, param2ArrayOfbyte2, param2ArrayOfbyte3);
                      parcel2.recycle();
                      parcel1.recycle();
                      return j;
                    } 
                    parcel2.readException();
                    int i = parcel2.readInt();
                    parcel2.recycle();
                    parcel1.recycle();
                    return i;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IKeystoreOperationResultCallback;
      }
      
      public int abort(IKeystoreResponseCallback param2IKeystoreResponseCallback, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          if (param2IKeystoreResponseCallback != null) {
            iBinder = param2IKeystoreResponseCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().abort(param2IKeystoreResponseCallback, param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int addAuthToken(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().addAuthToken(param2ArrayOfbyte); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int onUserAdded(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int1 = IKeystoreService.Stub.getDefaultImpl().onUserAdded(param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int onUserRemoved(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int = IKeystoreService.Stub.getDefaultImpl().onUserRemoved(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int attestKey(IKeystoreCertificateChainCallback param2IKeystoreCertificateChainCallback, String param2String, KeymasterArguments param2KeymasterArguments) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          if (param2IKeystoreCertificateChainCallback != null) {
            iBinder = param2IKeystoreCertificateChainCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          if (param2KeymasterArguments != null) {
            parcel1.writeInt(1);
            param2KeymasterArguments.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().attestKey(param2IKeystoreCertificateChainCallback, param2String, param2KeymasterArguments); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int attestDeviceIds(IKeystoreCertificateChainCallback param2IKeystoreCertificateChainCallback, KeymasterArguments param2KeymasterArguments) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          if (param2IKeystoreCertificateChainCallback != null) {
            iBinder = param2IKeystoreCertificateChainCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2KeymasterArguments != null) {
            parcel1.writeInt(1);
            param2KeymasterArguments.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().attestDeviceIds(param2IKeystoreCertificateChainCallback, param2KeymasterArguments); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int onDeviceOffBody() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().onDeviceOffBody(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int importWrappedKey(IKeystoreKeyCharacteristicsCallback param2IKeystoreKeyCharacteristicsCallback, String param2String1, byte[] param2ArrayOfbyte1, String param2String2, byte[] param2ArrayOfbyte2, KeymasterArguments param2KeymasterArguments, long param2Long1, long param2Long2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          if (param2IKeystoreKeyCharacteristicsCallback != null) {
            iBinder = param2IKeystoreKeyCharacteristicsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          try {
            parcel1.writeString(param2String1);
            parcel1.writeByteArray(param2ArrayOfbyte1);
            parcel1.writeString(param2String2);
            parcel1.writeByteArray(param2ArrayOfbyte2);
            if (param2KeymasterArguments != null) {
              parcel1.writeInt(1);
              param2KeymasterArguments.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            parcel1.writeLong(param2Long1);
            parcel1.writeLong(param2Long2);
            boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
            if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
              int j = IKeystoreService.Stub.getDefaultImpl().importWrappedKey(param2IKeystoreKeyCharacteristicsCallback, param2String1, param2ArrayOfbyte1, param2String2, param2ArrayOfbyte2, param2KeymasterArguments, param2Long1, param2Long2);
              parcel2.recycle();
              parcel1.recycle();
              return j;
            } 
            parcel2.readException();
            int i = parcel2.readInt();
            parcel2.recycle();
            parcel1.recycle();
            return i;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IKeystoreKeyCharacteristicsCallback;
      }
      
      public int presentConfirmationPrompt(IBinder param2IBinder, String param2String1, byte[] param2ArrayOfbyte, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String1);
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int = IKeystoreService.Stub.getDefaultImpl().presentConfirmationPrompt(param2IBinder, param2String1, param2ArrayOfbyte, param2String2, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int cancelConfirmationPrompt(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().cancelConfirmationPrompt(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isConfirmationPromptSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(34, parcel1, parcel2, 0);
          if (!bool2 && IKeystoreService.Stub.getDefaultImpl() != null) {
            bool1 = IKeystoreService.Stub.getDefaultImpl().isConfirmationPromptSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int onKeyguardVisibilityChanged(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool1 && IKeystoreService.Stub.getDefaultImpl() != null) {
            param2Int = IKeystoreService.Stub.getDefaultImpl().onKeyguardVisibilityChanged(param2Boolean, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int listUidsOfAuthBoundKeys(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().listUidsOfAuthBoundKeys(param2List); 
          parcel2.readException();
          int i = parcel2.readInt();
          parcel2.readStringList(param2List);
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getTokensForCredstore(long param2Long1, long param2Long2, int param2Int, ICredstoreTokenCallback param2ICredstoreTokenCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          try {
            parcel1.writeLong(param2Long1);
            try {
              parcel1.writeLong(param2Long2);
              try {
                IBinder iBinder;
                parcel1.writeInt(param2Int);
                if (param2ICredstoreTokenCallback != null) {
                  iBinder = param2ICredstoreTokenCallback.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel1.writeStrongBinder(iBinder);
                try {
                  boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
                  if (!bool && IKeystoreService.Stub.getDefaultImpl() != null) {
                    IKeystoreService.Stub.getDefaultImpl().getTokensForCredstore(param2Long1, param2Long2, param2Int, param2ICredstoreTokenCallback);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ICredstoreTokenCallback;
      }
      
      public byte[] getGateKeeperAuthToken() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.keystore.IKeystoreService");
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IKeystoreService.Stub.getDefaultImpl() != null)
            return IKeystoreService.Stub.getDefaultImpl().getGateKeeperAuthToken(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IKeystoreService param1IKeystoreService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IKeystoreService != null) {
          Proxy.sDefaultImpl = param1IKeystoreService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IKeystoreService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
