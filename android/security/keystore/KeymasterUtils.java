package android.security.keystore;

import android.content.Context;
import android.hardware.biometrics.BiometricManager;
import android.security.GateKeeper;
import android.security.KeyStore;
import android.security.keymaster.KeymasterArguments;
import com.android.internal.util.ArrayUtils;
import java.math.BigInteger;
import java.security.ProviderException;
import java.util.ArrayList;

public abstract class KeymasterUtils {
  public static int getDigestOutputSizeBits(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown digest: ");
        stringBuilder.append(paramInt);
        throw new IllegalArgumentException(stringBuilder.toString());
      case 6:
        return 512;
      case 5:
        return 384;
      case 4:
        return 256;
      case 3:
        return 224;
      case 2:
        return 160;
      case 1:
        return 128;
      case 0:
        break;
    } 
    return -1;
  }
  
  public static boolean isKeymasterBlockModeIndCpaCompatibleWithSymmetricCrypto(int paramInt) {
    if (paramInt != 1) {
      if (paramInt == 2 || paramInt == 3 || paramInt == 32)
        return true; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported block mode: ");
      stringBuilder.append(paramInt);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return false;
  }
  
  public static boolean isKeymasterPaddingSchemeIndCpaCompatibleWithAsymmetricCrypto(int paramInt) {
    if (paramInt != 1) {
      if (paramInt == 2 || paramInt == 4)
        return true; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported asymmetric encryption padding scheme: ");
      stringBuilder.append(paramInt);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return false;
  }
  
  private static void addSids(KeymasterArguments paramKeymasterArguments, UserAuthArgs paramUserAuthArgs) {
    BigInteger bigInteger;
    if (paramUserAuthArgs.getUserAuthenticationType() == 3) {
      if (paramUserAuthArgs.getBoundToSpecificSecureUserId() != 0L) {
        bigInteger = KeymasterArguments.toUint64(paramUserAuthArgs.getBoundToSpecificSecureUserId());
        paramKeymasterArguments.addUnsignedLong(-1610612234, bigInteger);
      } else {
        bigInteger = KeymasterArguments.toUint64(getRootSid());
        paramKeymasterArguments.addUnsignedLong(-1610612234, bigInteger);
      } 
    } else {
      ArrayList<Long> arrayList = new ArrayList();
      if ((bigInteger.getUserAuthenticationType() & 0x2) != 0) {
        Context context = KeyStore.getApplicationContext();
        BiometricManager biometricManager = (BiometricManager)context.getSystemService(BiometricManager.class);
        long[] arrayOfLong = biometricManager.getAuthenticatorIds();
        if (arrayOfLong.length != 0) {
          if (bigInteger.getBoundToSpecificSecureUserId() != 0L) {
            arrayList.add(Long.valueOf(bigInteger.getBoundToSpecificSecureUserId()));
          } else if (bigInteger.isInvalidatedByBiometricEnrollment()) {
            int i;
            byte b1;
            for (i = arrayOfLong.length, b1 = 0; b1 < i; ) {
              long l = arrayOfLong[b1];
              arrayList.add(Long.valueOf(l));
              b1++;
            } 
          } else {
            arrayList.add(Long.valueOf(getRootSid()));
          } 
        } else {
          throw new IllegalStateException("At least one biometric must be enrolled to create keys requiring user authentication for every use");
        } 
      } else if ((bigInteger.getUserAuthenticationType() & 0x1) != 0) {
        arrayList.add(Long.valueOf(getRootSid()));
      } else {
        throw new IllegalStateException("Invalid or no authentication type specified.");
      } 
      for (byte b = 0; b < arrayList.size(); 
        paramKeymasterArguments.addUnsignedLong(-1610612234, bigInteger), b++)
        bigInteger = KeymasterArguments.toUint64(((Long)arrayList.get(b)).longValue()); 
    } 
  }
  
  public static void addUserAuthArgs(KeymasterArguments paramKeymasterArguments, UserAuthArgs paramUserAuthArgs) {
    if (paramUserAuthArgs.isUserConfirmationRequired())
      paramKeymasterArguments.addBoolean(1879048700); 
    if (paramUserAuthArgs.isUserPresenceRequired())
      paramKeymasterArguments.addBoolean(1879048699); 
    if (paramUserAuthArgs.isUnlockedDeviceRequired())
      paramKeymasterArguments.addBoolean(1879048701); 
    if (!paramUserAuthArgs.isUserAuthenticationRequired()) {
      paramKeymasterArguments.addBoolean(1879048695);
      return;
    } 
    if (paramUserAuthArgs.getUserAuthenticationValidityDurationSeconds() == 0) {
      addSids(paramKeymasterArguments, paramUserAuthArgs);
      paramKeymasterArguments.addEnum(268435960, paramUserAuthArgs.getUserAuthenticationType());
      if (paramUserAuthArgs.isUserAuthenticationValidWhileOnBody())
        throw new ProviderException("Key validity extension while device is on-body is not supported for keys requiring fingerprint authentication"); 
    } else {
      addSids(paramKeymasterArguments, paramUserAuthArgs);
      paramKeymasterArguments.addEnum(268435960, paramUserAuthArgs.getUserAuthenticationType());
      long l = paramUserAuthArgs.getUserAuthenticationValidityDurationSeconds();
      paramKeymasterArguments.addUnsignedInt(805306873, l);
      if (paramUserAuthArgs.isUserAuthenticationValidWhileOnBody())
        paramKeymasterArguments.addBoolean(1879048698); 
    } 
  }
  
  public static void addMinMacLengthAuthorizationIfNecessary(KeymasterArguments paramKeymasterArguments, int paramInt, int[] paramArrayOfint1, int[] paramArrayOfint2) {
    StringBuilder stringBuilder;
    if (paramInt != 32) {
      if (paramInt == 128)
        if (paramArrayOfint2.length == 1) {
          paramInt = paramArrayOfint2[0];
          int i = getDigestOutputSizeBits(paramInt);
          if (i != -1) {
            paramKeymasterArguments.addUnsignedInt(805306376, i);
          } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append("HMAC key authorized for unsupported digest: ");
            stringBuilder.append(KeyProperties.Digest.fromKeymaster(paramInt));
            throw new ProviderException(stringBuilder.toString());
          } 
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unsupported number of authorized digests for HMAC key: ");
          stringBuilder.append(paramArrayOfint2.length);
          stringBuilder.append(". Exactly one digest must be authorized");
          throw new ProviderException(stringBuilder.toString());
        }  
    } else if (ArrayUtils.contains(paramArrayOfint1, 32)) {
      stringBuilder.addUnsignedInt(805306376, 96L);
    } 
  }
  
  private static long getRootSid() {
    long l = GateKeeper.getSecureUserId();
    if (l != 0L)
      return l; 
    throw new IllegalStateException("Secure lock screen must be enabled to create keys requiring user authentication");
  }
}
