package android.security.keystore;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.security.keymaster.ExportResult;

public interface IKeystoreExportKeyCallback extends IInterface {
  void onFinished(ExportResult paramExportResult) throws RemoteException;
  
  class Default implements IKeystoreExportKeyCallback {
    public void onFinished(ExportResult param1ExportResult) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IKeystoreExportKeyCallback {
    private static final String DESCRIPTOR = "android.security.keystore.IKeystoreExportKeyCallback";
    
    static final int TRANSACTION_onFinished = 1;
    
    public Stub() {
      attachInterface(this, "android.security.keystore.IKeystoreExportKeyCallback");
    }
    
    public static IKeystoreExportKeyCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.keystore.IKeystoreExportKeyCallback");
      if (iInterface != null && iInterface instanceof IKeystoreExportKeyCallback)
        return (IKeystoreExportKeyCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onFinished";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.security.keystore.IKeystoreExportKeyCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.security.keystore.IKeystoreExportKeyCallback");
      if (param1Parcel1.readInt() != 0) {
        ExportResult exportResult = ExportResult.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onFinished((ExportResult)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IKeystoreExportKeyCallback {
      public static IKeystoreExportKeyCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.keystore.IKeystoreExportKeyCallback";
      }
      
      public void onFinished(ExportResult param2ExportResult) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.security.keystore.IKeystoreExportKeyCallback");
          if (param2ExportResult != null) {
            parcel.writeInt(1);
            param2ExportResult.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IKeystoreExportKeyCallback.Stub.getDefaultImpl() != null) {
            IKeystoreExportKeyCallback.Stub.getDefaultImpl().onFinished(param2ExportResult);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IKeystoreExportKeyCallback param1IKeystoreExportKeyCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IKeystoreExportKeyCallback != null) {
          Proxy.sDefaultImpl = param1IKeystoreExportKeyCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IKeystoreExportKeyCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
