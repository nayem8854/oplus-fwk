package android.security;

import android.app.ActivityThread;
import android.app.Application;
import android.content.Context;
import android.hardware.biometrics.BiometricManager;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.ServiceSpecificException;
import android.os.UserHandle;
import android.security.keymaster.ExportResult;
import android.security.keymaster.KeyCharacteristics;
import android.security.keymaster.KeymasterArguments;
import android.security.keymaster.KeymasterBlob;
import android.security.keymaster.KeymasterCertificateChain;
import android.security.keymaster.KeymasterDefs;
import android.security.keymaster.OperationResult;
import android.security.keystore.IKeystoreCertificateChainCallback;
import android.security.keystore.IKeystoreExportKeyCallback;
import android.security.keystore.IKeystoreKeyCharacteristicsCallback;
import android.security.keystore.IKeystoreOperationResultCallback;
import android.security.keystore.IKeystoreResponseCallback;
import android.security.keystore.IKeystoreService;
import android.security.keystore.KeyExpiredException;
import android.security.keystore.KeyNotYetValidException;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.security.keystore.KeystoreResponse;
import android.security.keystore.UserNotAuthenticatedException;
import android.util.Log;
import com.android.org.bouncycastle.asn1.ASN1InputStream;
import com.android.org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.ToIntFunction;
import sun.security.util.ObjectIdentifier;
import sun.security.x509.AlgorithmId;

public class KeyStore implements IOplusKeyStoreEx {
  public static final int CANNOT_ATTEST_IDS = -66;
  
  public static final int CONFIRMATIONUI_ABORTED = 2;
  
  public static final int CONFIRMATIONUI_CANCELED = 1;
  
  public static final int CONFIRMATIONUI_IGNORED = 4;
  
  public static final int CONFIRMATIONUI_OK = 0;
  
  public static final int CONFIRMATIONUI_OPERATION_PENDING = 3;
  
  public static final int CONFIRMATIONUI_SYSTEM_ERROR = 5;
  
  public static final int CONFIRMATIONUI_UIERROR = 65536;
  
  public static final int CONFIRMATIONUI_UIERROR_MALFORMED_UTF8_ENCODING = 65539;
  
  public static final int CONFIRMATIONUI_UIERROR_MESSAGE_TOO_LONG = 65538;
  
  public static final int CONFIRMATIONUI_UIERROR_MISSING_GLYPH = 65537;
  
  public static final int CONFIRMATIONUI_UNEXPECTED = 7;
  
  public static final int CONFIRMATIONUI_UNIMPLEMENTED = 6;
  
  public static final int FLAG_CRITICAL_TO_DEVICE_ENCRYPTION = 8;
  
  public static final int FLAG_ENCRYPTED = 1;
  
  public static final int FLAG_NONE = 0;
  
  public static final int FLAG_SOFTWARE = 2;
  
  public static final int FLAG_STRONGBOX = 16;
  
  public static final int HARDWARE_TYPE_UNAVAILABLE = -68;
  
  public static final int KEY_ALREADY_EXISTS = 16;
  
  public static final int KEY_NOT_FOUND = 7;
  
  public static final int KEY_PERMANENTLY_INVALIDATED = 17;
  
  public static final int LOCKED = 2;
  
  public static final int NO_ERROR = 1;
  
  public static final int OP_AUTH_NEEDED = 15;
  
  public static final int PERMISSION_DENIED = 6;
  
  public static final int PROTOCOL_ERROR = 5;
  
  public static final int SYSTEM_ERROR = 4;
  
  private static final String TAG = "KeyStore";
  
  public static final int UID_SELF = -1;
  
  public static final int UNDEFINED_ACTION = 9;
  
  public static final int UNINITIALIZED = 3;
  
  public static final int VALUE_CORRUPTED = 8;
  
  public static final int WRONG_PASSWORD = 10;
  
  private final IKeystoreService mBinder;
  
  private final Context mContext;
  
  class State extends Enum<State> {
    private static final State[] $VALUES;
    
    public static final State LOCKED = new State("LOCKED", 1);
    
    public static final State UNINITIALIZED;
    
    public static State[] values() {
      return (State[])$VALUES.clone();
    }
    
    public static State valueOf(String param1String) {
      return Enum.<State>valueOf(State.class, param1String);
    }
    
    private State(KeyStore this$0, int param1Int) {
      super((String)this$0, param1Int);
    }
    
    public static final State UNLOCKED = new State("UNLOCKED", 0);
    
    static {
      State state = new State("UNINITIALIZED", 2);
      $VALUES = new State[] { UNLOCKED, LOCKED, state };
    }
  }
  
  private int mError = 1;
  
  private IBinder mToken;
  
  private KeyStore(IKeystoreService paramIKeystoreService) {
    this.mBinder = paramIKeystoreService;
    this.mContext = getApplicationContext();
  }
  
  public static Context getApplicationContext() {
    Application application = ActivityThread.currentApplication();
    if (application != null)
      return (Context)application; 
    throw new IllegalStateException("Failed to obtain application Context from ActivityThread");
  }
  
  public static KeyStore getInstance() {
    IBinder iBinder = ServiceManager.getService("android.security.keystore");
    IKeystoreService iKeystoreService = IKeystoreService.Stub.asInterface(iBinder);
    return new KeyStore(iKeystoreService);
  }
  
  private IBinder getToken() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mToken : Landroid/os/IBinder;
    //   6: ifnonnull -> 22
    //   9: new android/os/Binder
    //   12: astore_1
    //   13: aload_1
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: aload_1
    //   19: putfield mToken : Landroid/os/IBinder;
    //   22: aload_0
    //   23: getfield mToken : Landroid/os/IBinder;
    //   26: astore_1
    //   27: aload_0
    //   28: monitorexit
    //   29: aload_1
    //   30: areturn
    //   31: astore_1
    //   32: aload_0
    //   33: monitorexit
    //   34: aload_1
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #193	-> 2
    //   #194	-> 9
    //   #196	-> 22
    //   #192	-> 31
    // Exception table:
    //   from	to	target	type
    //   2	9	31	finally
    //   9	22	31	finally
    //   22	27	31	finally
  }
  
  public State state(int paramInt) {
    try {
      paramInt = this.mBinder.getState(paramInt);
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3)
            return State.UNINITIALIZED; 
          throw new AssertionError(this.mError);
        } 
        return State.LOCKED;
      } 
      return State.UNLOCKED;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      throw new AssertionError(remoteException);
    } 
  }
  
  public State state() {
    return state(UserHandle.myUserId());
  }
  
  public boolean isUnlocked() {
    boolean bool;
    if (state() == State.UNLOCKED) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public byte[] get(String paramString, int paramInt) {
    return get(paramString, paramInt, false);
  }
  
  public byte[] get(String paramString) {
    return get(paramString, -1);
  }
  
  public byte[] get(String paramString, int paramInt, boolean paramBoolean) {
    if (paramString == null)
      paramString = ""; 
    try {
      return this.mBinder.get(paramString, paramInt);
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return null;
    } catch (ServiceSpecificException serviceSpecificException) {
      if (!paramBoolean || serviceSpecificException.errorCode != 7)
        Log.w("KeyStore", "KeyStore exception", serviceSpecificException); 
      return null;
    } 
  }
  
  public byte[] get(String paramString, boolean paramBoolean) {
    return get(paramString, -1, paramBoolean);
  }
  
  public boolean put(String paramString, byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    paramInt1 = insert(paramString, paramArrayOfbyte, paramInt1, paramInt2);
    boolean bool = true;
    if (paramInt1 != 1)
      bool = false; 
    return bool;
  }
  
  public int insert(String paramString, byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_2
    //   1: astore #5
    //   3: aload_2
    //   4: ifnonnull -> 12
    //   7: iconst_0
    //   8: newarray byte
    //   10: astore #5
    //   12: aload_0
    //   13: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   16: aload_1
    //   17: aload #5
    //   19: iload_3
    //   20: iload #4
    //   22: invokeinterface insert : (Ljava/lang/String;[BII)I
    //   27: istore #6
    //   29: iload #6
    //   31: istore #7
    //   33: iload #6
    //   35: bipush #16
    //   37: if_icmpne -> 69
    //   40: aload_0
    //   41: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   44: aload_1
    //   45: iload_3
    //   46: invokeinterface del : (Ljava/lang/String;I)I
    //   51: pop
    //   52: aload_0
    //   53: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   56: aload_1
    //   57: aload #5
    //   59: iload_3
    //   60: iload #4
    //   62: invokeinterface insert : (Ljava/lang/String;[BII)I
    //   67: istore #7
    //   69: iload #7
    //   71: ireturn
    //   72: astore_1
    //   73: ldc 'KeyStore'
    //   75: ldc_w 'Cannot connect to keystore'
    //   78: aload_1
    //   79: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   82: pop
    //   83: iconst_4
    //   84: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #261	-> 0
    //   #262	-> 7
    //   #264	-> 12
    //   #265	-> 29
    //   #266	-> 40
    //   #267	-> 52
    //   #269	-> 69
    //   #270	-> 72
    //   #271	-> 73
    //   #272	-> 83
    // Exception table:
    //   from	to	target	type
    //   7	12	72	android/os/RemoteException
    //   12	29	72	android/os/RemoteException
    //   40	52	72	android/os/RemoteException
    //   52	69	72	android/os/RemoteException
  }
  
  int delete2(String paramString, int paramInt) {
    try {
      return this.mBinder.del(paramString, paramInt);
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return 4;
    } 
  }
  
  public boolean delete(String paramString, int paramInt) {
    paramInt = delete2(paramString, paramInt);
    boolean bool1 = true, bool2 = bool1;
    if (paramInt != 1)
      if (paramInt == 7) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    return bool2;
  }
  
  public boolean delete(String paramString) {
    return delete(paramString, -1);
  }
  
  public boolean contains(String paramString, int paramInt) {
    boolean bool = false;
    try {
      paramInt = this.mBinder.exist(paramString, paramInt);
      if (paramInt == 1)
        bool = true; 
      return bool;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean contains(String paramString) {
    return contains(paramString, -1);
  }
  
  public String[] list(String paramString, int paramInt) {
    try {
      return this.mBinder.list(paramString, paramInt);
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return null;
    } catch (ServiceSpecificException serviceSpecificException) {
      Log.w("KeyStore", "KeyStore exception", serviceSpecificException);
      return null;
    } 
  }
  
  public int[] listUidsOfAuthBoundKeys() {
    ArrayList<String> arrayList = new ArrayList();
    try {
      int i = this.mBinder.listUidsOfAuthBoundKeys(arrayList);
      if (i != 1) {
        Log.w("KeyStore", String.format("listUidsOfAuthBoundKeys failed with error code %d", new Object[] { Integer.valueOf(i) }));
        return null;
      } 
      return arrayList.stream().mapToInt((ToIntFunction)_$$Lambda$wddj3_hVVrg0MkscpMtYt3BzY8Y.INSTANCE).toArray();
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return null;
    } catch (ServiceSpecificException serviceSpecificException) {
      Log.w("KeyStore", "KeyStore exception", serviceSpecificException);
      return null;
    } 
  }
  
  public String[] list(String paramString) {
    return list(paramString, -1);
  }
  
  public boolean lock(int paramInt) {
    boolean bool = false;
    try {
      paramInt = this.mBinder.lock(paramInt);
      if (paramInt == 1)
        bool = true; 
      return bool;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean lock() {
    return lock(UserHandle.myUserId());
  }
  
  public boolean unlock(int paramInt, String paramString) {
    boolean bool = false;
    if (paramString == null)
      paramString = ""; 
    try {
      this.mError = paramInt = this.mBinder.unlock(paramInt, paramString);
      if (paramInt == 1)
        bool = true; 
      return bool;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean unlock(String paramString) {
    return unlock(UserHandle.getUserId(Process.myUid()), paramString);
  }
  
  public boolean isEmpty(int paramInt) {
    boolean bool = false;
    try {
      paramInt = this.mBinder.isEmpty(paramInt);
      if (paramInt != 0)
        bool = true; 
      return bool;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean isEmpty() {
    return isEmpty(UserHandle.myUserId());
  }
  
  public String grant(String paramString, int paramInt) {
    try {
      paramString = this.mBinder.grant(paramString, paramInt);
      if (paramString == "")
        return null; 
      return paramString;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return null;
    } 
  }
  
  public boolean ungrant(String paramString, int paramInt) {
    boolean bool = false;
    try {
      paramInt = this.mBinder.ungrant(paramString, paramInt);
      if (paramInt == 1)
        bool = true; 
      return bool;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return false;
    } 
  }
  
  public long getmtime(String paramString, int paramInt) {
    try {
      long l = this.mBinder.getmtime(paramString, paramInt);
      if (l == -1L)
        return -1L; 
      return 1000L * l;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return -1L;
    } 
  }
  
  public long getmtime(String paramString) {
    return getmtime(paramString, -1);
  }
  
  public boolean isHardwareBacked() {
    return isHardwareBacked("RSA");
  }
  
  public boolean isHardwareBacked(String paramString) {
    boolean bool = false;
    try {
      int i = this.mBinder.is_hardware_backed(paramString.toUpperCase(Locale.US));
      if (i == 1)
        bool = true; 
      return bool;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return false;
    } 
  }
  
  public boolean clearUid(int paramInt) {
    boolean bool = false;
    try {
      paramInt = this.mBinder.clear_uid(paramInt);
      if (paramInt == 1)
        bool = true; 
      return bool;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return false;
    } 
  }
  
  public int getLastError() {
    return this.mError;
  }
  
  public boolean addRngEntropy(byte[] paramArrayOfbyte, int paramInt) {
    KeystoreResultPromise keystoreResultPromise = new KeystoreResultPromise();
    try {
      this.mBinder.asBinder().linkToDeath(keystoreResultPromise, 0);
      paramInt = this.mBinder.addRngEntropy(keystoreResultPromise, paramArrayOfbyte, paramInt);
      boolean bool = true;
      if (paramInt == 1) {
        paramInt = ((KeystoreResponse)interruptedPreservingGet(keystoreResultPromise.getFuture())).getErrorCode();
        if (paramInt != 1)
          bool = false; 
        this.mBinder.asBinder().unlinkToDeath(keystoreResultPromise, 0);
        return bool;
      } 
      this.mBinder.asBinder().unlinkToDeath(keystoreResultPromise, 0);
      return false;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      this.mBinder.asBinder().unlinkToDeath(keystoreResultPromise, 0);
      return false;
    } catch (ExecutionException executionException) {
      Log.e("KeyStore", "AddRngEntropy completed with exception", executionException);
      this.mBinder.asBinder().unlinkToDeath(keystoreResultPromise, 0);
      return false;
    } finally {}
    this.mBinder.asBinder().unlinkToDeath(keystoreResultPromise, 0);
    throw paramArrayOfbyte;
  }
  
  class KeyCharacteristicsCallbackResult {
    private KeyCharacteristics keyCharacteristics;
    
    private KeystoreResponse keystoreResponse;
    
    final KeyStore this$0;
    
    public KeyCharacteristicsCallbackResult(KeystoreResponse param1KeystoreResponse, KeyCharacteristics param1KeyCharacteristics) {
      this.keystoreResponse = param1KeystoreResponse;
      this.keyCharacteristics = param1KeyCharacteristics;
    }
    
    public KeystoreResponse getKeystoreResponse() {
      return this.keystoreResponse;
    }
    
    public void setKeystoreResponse(KeystoreResponse param1KeystoreResponse) {
      this.keystoreResponse = param1KeystoreResponse;
    }
    
    public KeyCharacteristics getKeyCharacteristics() {
      return this.keyCharacteristics;
    }
    
    public void setKeyCharacteristics(KeyCharacteristics param1KeyCharacteristics) {
      this.keyCharacteristics = param1KeyCharacteristics;
    }
  }
  
  class KeyCharacteristicsPromise extends IKeystoreKeyCharacteristicsCallback.Stub implements IBinder.DeathRecipient {
    private final CompletableFuture<KeyStore.KeyCharacteristicsCallbackResult> future;
    
    final KeyStore this$0;
    
    private KeyCharacteristicsPromise() {
      this.future = new CompletableFuture<>();
    }
    
    public void onFinished(KeystoreResponse param1KeystoreResponse, KeyCharacteristics param1KeyCharacteristics) throws RemoteException {
      this.future.complete(new KeyStore.KeyCharacteristicsCallbackResult(param1KeystoreResponse, param1KeyCharacteristics));
    }
    
    public final CompletableFuture<KeyStore.KeyCharacteristicsCallbackResult> getFuture() {
      return this.future;
    }
    
    public void binderDied() {
      this.future.completeExceptionally((Throwable)new RemoteException("Keystore died"));
    }
  }
  
  private int generateKeyInternal(String paramString, KeymasterArguments paramKeymasterArguments, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, KeyCharacteristics paramKeyCharacteristics) throws RemoteException, ExecutionException {
    KeyCharacteristicsPromise keyCharacteristicsPromise = new KeyCharacteristicsPromise();
    try {
      StringBuilder stringBuilder2, stringBuilder1;
      this.mBinder.asBinder().linkToDeath(keyCharacteristicsPromise, 0);
      paramInt1 = this.mBinder.generateKey(keyCharacteristicsPromise, paramString, paramKeymasterArguments, paramArrayOfbyte, paramInt1, paramInt2);
      if (paramInt1 != 1) {
        stringBuilder2 = new StringBuilder();
        this();
        stringBuilder2.append("generateKeyInternal failed on request ");
        stringBuilder2.append(paramInt1);
        Log.e("KeyStore", stringBuilder2.toString());
        return paramInt1;
      } 
      KeyCharacteristicsCallbackResult keyCharacteristicsCallbackResult = interruptedPreservingGet(keyCharacteristicsPromise.getFuture());
      this.mBinder.asBinder().unlinkToDeath(keyCharacteristicsPromise, 0);
      paramInt1 = keyCharacteristicsCallbackResult.getKeystoreResponse().getErrorCode();
      if (paramInt1 != 1) {
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("generateKeyInternal failed on response ");
        stringBuilder2.append(paramInt1);
        return paramInt1;
      } 
      KeyCharacteristics keyCharacteristics = stringBuilder2.getKeyCharacteristics();
      if (keyCharacteristics == null) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("generateKeyInternal got empty key characteristics ");
        stringBuilder1.append(paramInt1);
        return 4;
      } 
      return 1;
    } finally {
      this.mBinder.asBinder().unlinkToDeath(keyCharacteristicsPromise, 0);
    } 
  }
  
  public int generateKey(String paramString, KeymasterArguments paramKeymasterArguments, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, KeyCharacteristics paramKeyCharacteristics) {
    // Byte code:
    //   0: aload_3
    //   1: ifnull -> 7
    //   4: goto -> 11
    //   7: iconst_0
    //   8: newarray byte
    //   10: astore_3
    //   11: aload_2
    //   12: ifnull -> 18
    //   15: goto -> 26
    //   18: new android/security/keymaster/KeymasterArguments
    //   21: dup
    //   22: invokespecial <init> : ()V
    //   25: astore_2
    //   26: aload_0
    //   27: aload_1
    //   28: aload_2
    //   29: aload_3
    //   30: iload #4
    //   32: iload #5
    //   34: aload #6
    //   36: invokespecial generateKeyInternal : (Ljava/lang/String;Landroid/security/keymaster/KeymasterArguments;[BIILandroid/security/keymaster/KeyCharacteristics;)I
    //   39: istore #7
    //   41: iload #7
    //   43: bipush #16
    //   45: if_icmpne -> 99
    //   48: aload_0
    //   49: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   52: astore #8
    //   54: aload #8
    //   56: aload_1
    //   57: iload #4
    //   59: invokeinterface del : (Ljava/lang/String;I)I
    //   64: pop
    //   65: aload_0
    //   66: aload_1
    //   67: aload_2
    //   68: aload_3
    //   69: iload #4
    //   71: iload #5
    //   73: aload #6
    //   75: invokespecial generateKeyInternal : (Ljava/lang/String;Landroid/security/keymaster/KeymasterArguments;[BIILandroid/security/keymaster/KeyCharacteristics;)I
    //   78: istore #4
    //   80: goto -> 103
    //   83: astore_1
    //   84: goto -> 122
    //   87: astore_1
    //   88: goto -> 134
    //   91: astore_1
    //   92: goto -> 107
    //   95: astore_1
    //   96: goto -> 111
    //   99: iload #7
    //   101: istore #4
    //   103: iload #4
    //   105: ireturn
    //   106: astore_1
    //   107: goto -> 122
    //   110: astore_1
    //   111: goto -> 134
    //   114: astore_1
    //   115: goto -> 122
    //   118: astore_1
    //   119: goto -> 134
    //   122: ldc 'KeyStore'
    //   124: ldc_w 'generateKey completed with exception'
    //   127: aload_1
    //   128: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   131: pop
    //   132: iconst_4
    //   133: ireturn
    //   134: ldc 'KeyStore'
    //   136: ldc_w 'Cannot connect to keystore'
    //   139: aload_1
    //   140: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   143: pop
    //   144: iconst_4
    //   145: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #589	-> 0
    //   #590	-> 11
    //   #591	-> 26
    //   #592	-> 41
    //   #593	-> 48
    //   #594	-> 65
    //   #600	-> 83
    //   #597	-> 87
    //   #600	-> 91
    //   #597	-> 95
    //   #592	-> 99
    //   #596	-> 103
    //   #600	-> 106
    //   #597	-> 110
    //   #600	-> 114
    //   #597	-> 118
    //   #601	-> 122
    //   #602	-> 132
    //   #598	-> 134
    //   #599	-> 144
    // Exception table:
    //   from	to	target	type
    //   18	26	118	android/os/RemoteException
    //   18	26	114	java/util/concurrent/ExecutionException
    //   26	41	110	android/os/RemoteException
    //   26	41	106	java/util/concurrent/ExecutionException
    //   48	54	95	android/os/RemoteException
    //   48	54	91	java/util/concurrent/ExecutionException
    //   54	65	87	android/os/RemoteException
    //   54	65	83	java/util/concurrent/ExecutionException
    //   65	80	87	android/os/RemoteException
    //   65	80	83	java/util/concurrent/ExecutionException
  }
  
  public int generateKey(String paramString, KeymasterArguments paramKeymasterArguments, byte[] paramArrayOfbyte, int paramInt, KeyCharacteristics paramKeyCharacteristics) {
    return generateKey(paramString, paramKeymasterArguments, paramArrayOfbyte, -1, paramInt, paramKeyCharacteristics);
  }
  
  public int getKeyCharacteristics(String paramString, KeymasterBlob paramKeymasterBlob1, KeymasterBlob paramKeymasterBlob2, int paramInt, KeyCharacteristics paramKeyCharacteristics) {
    // Byte code:
    //   0: new android/security/KeyStore$KeyCharacteristicsPromise
    //   3: dup
    //   4: aload_0
    //   5: aconst_null
    //   6: invokespecial <init> : (Landroid/security/KeyStore;Landroid/security/KeyStore$1;)V
    //   9: astore #6
    //   11: aload_0
    //   12: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   15: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   20: aload #6
    //   22: iconst_0
    //   23: invokeinterface linkToDeath : (Landroid/os/IBinder$DeathRecipient;I)V
    //   28: aload_2
    //   29: ifnull -> 35
    //   32: goto -> 46
    //   35: new android/security/keymaster/KeymasterBlob
    //   38: dup
    //   39: iconst_0
    //   40: newarray byte
    //   42: invokespecial <init> : ([B)V
    //   45: astore_2
    //   46: aload_3
    //   47: ifnull -> 53
    //   50: goto -> 64
    //   53: new android/security/keymaster/KeymasterBlob
    //   56: dup
    //   57: iconst_0
    //   58: newarray byte
    //   60: invokespecial <init> : ([B)V
    //   63: astore_3
    //   64: aload_0
    //   65: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   68: aload #6
    //   70: aload_1
    //   71: aload_2
    //   72: aload_3
    //   73: iload #4
    //   75: invokeinterface getKeyCharacteristics : (Landroid/security/keystore/IKeystoreKeyCharacteristicsCallback;Ljava/lang/String;Landroid/security/keymaster/KeymasterBlob;Landroid/security/keymaster/KeymasterBlob;I)I
    //   80: istore #4
    //   82: iload #4
    //   84: iconst_1
    //   85: if_icmpeq -> 109
    //   88: aload_0
    //   89: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   92: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   97: aload #6
    //   99: iconst_0
    //   100: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
    //   105: pop
    //   106: iload #4
    //   108: ireturn
    //   109: aload #6
    //   111: invokevirtual getFuture : ()Ljava/util/concurrent/CompletableFuture;
    //   114: invokestatic interruptedPreservingGet : (Ljava/util/concurrent/CompletableFuture;)Ljava/lang/Object;
    //   117: checkcast android/security/KeyStore$KeyCharacteristicsCallbackResult
    //   120: astore_1
    //   121: aload_1
    //   122: invokevirtual getKeystoreResponse : ()Landroid/security/keystore/KeystoreResponse;
    //   125: invokevirtual getErrorCode : ()I
    //   128: istore #4
    //   130: iload #4
    //   132: iconst_1
    //   133: if_icmpeq -> 157
    //   136: aload_0
    //   137: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   140: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   145: aload #6
    //   147: iconst_0
    //   148: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
    //   153: pop
    //   154: iload #4
    //   156: ireturn
    //   157: aload_1
    //   158: invokevirtual getKeyCharacteristics : ()Landroid/security/keymaster/KeyCharacteristics;
    //   161: astore_1
    //   162: aload_1
    //   163: ifnonnull -> 186
    //   166: aload_0
    //   167: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   170: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   175: aload #6
    //   177: iconst_0
    //   178: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
    //   183: pop
    //   184: iconst_4
    //   185: ireturn
    //   186: aload #5
    //   188: aload_1
    //   189: invokevirtual shallowCopyFrom : (Landroid/security/keymaster/KeyCharacteristics;)V
    //   192: aload_0
    //   193: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   196: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   201: aload #6
    //   203: iconst_0
    //   204: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
    //   209: pop
    //   210: iconst_1
    //   211: ireturn
    //   212: astore_1
    //   213: goto -> 299
    //   216: astore_1
    //   217: goto -> 237
    //   220: astore_1
    //   221: goto -> 268
    //   224: astore_1
    //   225: goto -> 237
    //   228: astore_1
    //   229: goto -> 268
    //   232: astore_1
    //   233: goto -> 299
    //   236: astore_1
    //   237: ldc 'KeyStore'
    //   239: ldc_w 'GetKeyCharacteristics completed with exception'
    //   242: aload_1
    //   243: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   246: pop
    //   247: aload_0
    //   248: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   251: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   256: aload #6
    //   258: iconst_0
    //   259: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
    //   264: pop
    //   265: iconst_4
    //   266: ireturn
    //   267: astore_1
    //   268: ldc 'KeyStore'
    //   270: ldc_w 'Cannot connect to keystore'
    //   273: aload_1
    //   274: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   277: pop
    //   278: aload_0
    //   279: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   282: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   287: aload #6
    //   289: iconst_0
    //   290: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
    //   295: pop
    //   296: iconst_4
    //   297: ireturn
    //   298: astore_1
    //   299: aload_0
    //   300: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   303: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   308: aload #6
    //   310: iconst_0
    //   311: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
    //   316: pop
    //   317: aload_1
    //   318: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #613	-> 0
    //   #615	-> 11
    //   #616	-> 28
    //   #617	-> 46
    //   #619	-> 64
    //   #620	-> 82
    //   #637	-> 88
    //   #620	-> 106
    //   #622	-> 109
    //   #623	-> 121
    //   #624	-> 130
    //   #637	-> 136
    //   #624	-> 154
    //   #626	-> 157
    //   #627	-> 162
    //   #637	-> 166
    //   #627	-> 184
    //   #628	-> 186
    //   #629	-> 192
    //   #637	-> 192
    //   #629	-> 210
    //   #637	-> 212
    //   #633	-> 216
    //   #630	-> 220
    //   #633	-> 224
    //   #630	-> 228
    //   #637	-> 232
    //   #633	-> 236
    //   #634	-> 237
    //   #635	-> 247
    //   #637	-> 247
    //   #635	-> 265
    //   #630	-> 267
    //   #631	-> 268
    //   #632	-> 278
    //   #637	-> 278
    //   #632	-> 296
    //   #637	-> 298
    //   #638	-> 317
    // Exception table:
    //   from	to	target	type
    //   11	28	267	android/os/RemoteException
    //   11	28	236	java/util/concurrent/ExecutionException
    //   11	28	232	finally
    //   35	46	267	android/os/RemoteException
    //   35	46	236	java/util/concurrent/ExecutionException
    //   35	46	232	finally
    //   53	64	228	android/os/RemoteException
    //   53	64	224	java/util/concurrent/ExecutionException
    //   53	64	298	finally
    //   64	82	220	android/os/RemoteException
    //   64	82	216	java/util/concurrent/ExecutionException
    //   64	82	212	finally
    //   109	121	220	android/os/RemoteException
    //   109	121	216	java/util/concurrent/ExecutionException
    //   109	121	212	finally
    //   121	130	220	android/os/RemoteException
    //   121	130	216	java/util/concurrent/ExecutionException
    //   121	130	212	finally
    //   157	162	220	android/os/RemoteException
    //   157	162	216	java/util/concurrent/ExecutionException
    //   157	162	212	finally
    //   186	192	220	android/os/RemoteException
    //   186	192	216	java/util/concurrent/ExecutionException
    //   186	192	212	finally
    //   237	247	298	finally
    //   268	278	298	finally
  }
  
  public int getKeyCharacteristics(String paramString, KeymasterBlob paramKeymasterBlob1, KeymasterBlob paramKeymasterBlob2, KeyCharacteristics paramKeyCharacteristics) {
    return getKeyCharacteristics(paramString, paramKeymasterBlob1, paramKeymasterBlob2, -1, paramKeyCharacteristics);
  }
  
  private int importKeyInternal(String paramString, KeymasterArguments paramKeymasterArguments, int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, KeyCharacteristics paramKeyCharacteristics) throws RemoteException, ExecutionException {
    KeyCharacteristicsPromise keyCharacteristicsPromise = new KeyCharacteristicsPromise();
    this.mBinder.asBinder().linkToDeath(keyCharacteristicsPromise, 0);
    try {
      paramInt1 = this.mBinder.importKey(keyCharacteristicsPromise, paramString, paramKeymasterArguments, paramInt1, paramArrayOfbyte, paramInt2, paramInt3);
      if (paramInt1 != 1) {
        this.mBinder.asBinder().unlinkToDeath(keyCharacteristicsPromise, 0);
        return paramInt1;
      } 
      KeyCharacteristicsCallbackResult keyCharacteristicsCallbackResult = interruptedPreservingGet(keyCharacteristicsPromise.getFuture());
      paramInt1 = keyCharacteristicsCallbackResult.getKeystoreResponse().getErrorCode();
      if (paramInt1 != 1) {
        this.mBinder.asBinder().unlinkToDeath(keyCharacteristicsPromise, 0);
        return paramInt1;
      } 
      KeyCharacteristics keyCharacteristics = keyCharacteristicsCallbackResult.getKeyCharacteristics();
      if (keyCharacteristics == null) {
        this.mBinder.asBinder().unlinkToDeath(keyCharacteristicsPromise, 0);
        return 4;
      } 
      try {
        paramKeyCharacteristics.shallowCopyFrom(keyCharacteristics);
        this.mBinder.asBinder().unlinkToDeath(keyCharacteristicsPromise, 0);
        return 1;
      } finally {}
    } finally {}
    this.mBinder.asBinder().unlinkToDeath(keyCharacteristicsPromise, 0);
    throw paramString;
  }
  
  public int importKey(String paramString, KeymasterArguments paramKeymasterArguments, int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, KeyCharacteristics paramKeyCharacteristics) {
    try {
      int i = importKeyInternal(paramString, paramKeymasterArguments, paramInt1, paramArrayOfbyte, paramInt2, paramInt3, paramKeyCharacteristics);
      int j = i;
      if (i == 16) {
        this.mBinder.del(paramString, paramInt2);
        j = importKeyInternal(paramString, paramKeymasterArguments, paramInt1, paramArrayOfbyte, paramInt2, paramInt3, paramKeyCharacteristics);
      } 
      return j;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return 4;
    } catch (ExecutionException executionException) {
      Log.e("KeyStore", "ImportKey completed with exception", executionException);
      return 4;
    } 
  }
  
  public int importKey(String paramString, KeymasterArguments paramKeymasterArguments, int paramInt1, byte[] paramArrayOfbyte, int paramInt2, KeyCharacteristics paramKeyCharacteristics) {
    return importKey(paramString, paramKeymasterArguments, paramInt1, paramArrayOfbyte, -1, paramInt2, paramKeyCharacteristics);
  }
  
  private String getAlgorithmFromPKCS8(byte[] paramArrayOfbyte) {
    try {
      ASN1InputStream aSN1InputStream = new ASN1InputStream();
      ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
      this(paramArrayOfbyte);
      this(byteArrayInputStream);
      PrivateKeyInfo privateKeyInfo = PrivateKeyInfo.getInstance(aSN1InputStream.readObject());
      String str = privateKeyInfo.getPrivateKeyAlgorithm().getAlgorithm().getId();
      AlgorithmId algorithmId = new AlgorithmId();
      ObjectIdentifier objectIdentifier = new ObjectIdentifier();
      this(str);
      this(objectIdentifier);
      return algorithmId.getName();
    } catch (IOException iOException) {
      Log.e("KeyStore", "getAlgorithmFromPKCS8 Failed to parse key data");
      Log.e("KeyStore", Log.getStackTraceString(iOException));
      return null;
    } 
  }
  
  private KeymasterArguments makeLegacyArguments(String paramString) {
    KeymasterArguments keymasterArguments = new KeymasterArguments();
    int i = KeyProperties.KeyAlgorithm.toKeymasterAsymmetricKeyAlgorithm(paramString);
    keymasterArguments.addEnum(268435458, i);
    keymasterArguments.addEnum(536870913, 2);
    keymasterArguments.addEnum(536870913, 3);
    keymasterArguments.addEnum(536870913, 0);
    keymasterArguments.addEnum(536870913, 1);
    keymasterArguments.addEnum(536870918, 1);
    if (paramString.equalsIgnoreCase("RSA")) {
      keymasterArguments.addEnum(536870918, 2);
      keymasterArguments.addEnum(536870918, 4);
      keymasterArguments.addEnum(536870918, 5);
      keymasterArguments.addEnum(536870918, 3);
    } 
    keymasterArguments.addEnum(536870917, 0);
    keymasterArguments.addEnum(536870917, 1);
    keymasterArguments.addEnum(536870917, 2);
    keymasterArguments.addEnum(536870917, 3);
    keymasterArguments.addEnum(536870917, 4);
    keymasterArguments.addEnum(536870917, 5);
    keymasterArguments.addEnum(536870917, 6);
    keymasterArguments.addBoolean(1879048695);
    keymasterArguments.addDate(1610613137, new Date(Long.MAX_VALUE));
    keymasterArguments.addDate(1610613138, new Date(Long.MAX_VALUE));
    keymasterArguments.addDate(1610613136, new Date(0L));
    return keymasterArguments;
  }
  
  public boolean importKey(String paramString, byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    String str = getAlgorithmFromPKCS8(paramArrayOfbyte);
    if (str == null)
      return false; 
    KeymasterArguments keymasterArguments = makeLegacyArguments(str);
    KeyCharacteristics keyCharacteristics = new KeyCharacteristics();
    paramInt1 = importKey(paramString, keymasterArguments, 1, paramArrayOfbyte, paramInt1, paramInt2, keyCharacteristics);
    if (paramInt1 != 1) {
      Log.e("KeyStore", Log.getStackTraceString(new KeyStoreException(paramInt1, "legacy key import failed")));
      return false;
    } 
    return true;
  }
  
  private int importWrappedKeyInternal(String paramString1, byte[] paramArrayOfbyte1, String paramString2, byte[] paramArrayOfbyte2, KeymasterArguments paramKeymasterArguments, long paramLong1, long paramLong2, KeyCharacteristics paramKeyCharacteristics) throws RemoteException, ExecutionException {
    KeyCharacteristicsPromise keyCharacteristicsPromise = new KeyCharacteristicsPromise();
    this.mBinder.asBinder().linkToDeath(keyCharacteristicsPromise, 0);
    try {
      int i = this.mBinder.importWrappedKey(keyCharacteristicsPromise, paramString1, paramArrayOfbyte1, paramString2, paramArrayOfbyte2, paramKeymasterArguments, paramLong1, paramLong2);
      if (i != 1) {
        this.mBinder.asBinder().unlinkToDeath(keyCharacteristicsPromise, 0);
        return i;
      } 
      KeyCharacteristicsCallbackResult keyCharacteristicsCallbackResult = interruptedPreservingGet(keyCharacteristicsPromise.getFuture());
      i = keyCharacteristicsCallbackResult.getKeystoreResponse().getErrorCode();
      if (i != 1) {
        this.mBinder.asBinder().unlinkToDeath(keyCharacteristicsPromise, 0);
        return i;
      } 
      KeyCharacteristics keyCharacteristics = keyCharacteristicsCallbackResult.getKeyCharacteristics();
      if (keyCharacteristics == null) {
        this.mBinder.asBinder().unlinkToDeath(keyCharacteristicsPromise, 0);
        return 4;
      } 
      try {
        paramKeyCharacteristics.shallowCopyFrom(keyCharacteristics);
        this.mBinder.asBinder().unlinkToDeath(keyCharacteristicsPromise, 0);
        return 1;
      } finally {}
    } finally {}
    this.mBinder.asBinder().unlinkToDeath(keyCharacteristicsPromise, 0);
    throw paramString1;
  }
  
  public int importWrappedKey(String paramString1, byte[] paramArrayOfbyte1, String paramString2, byte[] paramArrayOfbyte2, KeymasterArguments paramKeymasterArguments, long paramLong1, long paramLong2, int paramInt, KeyCharacteristics paramKeyCharacteristics) {
    try {
      paramInt = importWrappedKeyInternal(paramString1, paramArrayOfbyte1, paramString2, paramArrayOfbyte2, paramKeymasterArguments, paramLong1, paramLong2, paramKeyCharacteristics);
      if (paramInt == 16)
        try {
          IKeystoreService iKeystoreService = this.mBinder;
          try {
            iKeystoreService.del(paramString1, -1);
            paramInt = importWrappedKeyInternal(paramString1, paramArrayOfbyte1, paramString2, paramArrayOfbyte2, paramKeymasterArguments, paramLong1, paramLong2, paramKeyCharacteristics);
          } catch (RemoteException remoteException) {
          
          } catch (ExecutionException executionException) {}
        } catch (RemoteException remoteException) {
        
        } catch (ExecutionException executionException) {} 
      return paramInt;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return 4;
    } catch (ExecutionException executionException) {
      Log.e("KeyStore", "ImportWrappedKey completed with exception", executionException);
      return 4;
    } 
  }
  
  class ExportKeyPromise extends IKeystoreExportKeyCallback.Stub implements IBinder.DeathRecipient {
    private final CompletableFuture<ExportResult> future = new CompletableFuture<>();
    
    final KeyStore this$0;
    
    public void onFinished(ExportResult param1ExportResult) throws RemoteException {
      this.future.complete(param1ExportResult);
    }
    
    public final CompletableFuture<ExportResult> getFuture() {
      return this.future;
    }
    
    public void binderDied() {
      this.future.completeExceptionally((Throwable)new RemoteException("Keystore died"));
    }
    
    private ExportKeyPromise() {}
  }
  
  public ExportResult exportKey(String paramString, int paramInt1, KeymasterBlob paramKeymasterBlob1, KeymasterBlob paramKeymasterBlob2, int paramInt2) {
    // Byte code:
    //   0: new android/security/KeyStore$ExportKeyPromise
    //   3: dup
    //   4: aload_0
    //   5: aconst_null
    //   6: invokespecial <init> : (Landroid/security/KeyStore;Landroid/security/KeyStore$1;)V
    //   9: astore #6
    //   11: aload_0
    //   12: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   15: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   20: aload #6
    //   22: iconst_0
    //   23: invokeinterface linkToDeath : (Landroid/os/IBinder$DeathRecipient;I)V
    //   28: aload_3
    //   29: ifnull -> 35
    //   32: goto -> 46
    //   35: new android/security/keymaster/KeymasterBlob
    //   38: dup
    //   39: iconst_0
    //   40: newarray byte
    //   42: invokespecial <init> : ([B)V
    //   45: astore_3
    //   46: aload #4
    //   48: ifnull -> 54
    //   51: goto -> 66
    //   54: new android/security/keymaster/KeymasterBlob
    //   57: dup
    //   58: iconst_0
    //   59: newarray byte
    //   61: invokespecial <init> : ([B)V
    //   64: astore #4
    //   66: aload_0
    //   67: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   70: aload #6
    //   72: aload_1
    //   73: iload_2
    //   74: aload_3
    //   75: aload #4
    //   77: iload #5
    //   79: invokeinterface exportKey : (Landroid/security/keystore/IKeystoreExportKeyCallback;Ljava/lang/String;ILandroid/security/keymaster/KeymasterBlob;Landroid/security/keymaster/KeymasterBlob;I)I
    //   84: istore_2
    //   85: iload_2
    //   86: iconst_1
    //   87: if_icmpne -> 122
    //   90: aload #6
    //   92: invokevirtual getFuture : ()Ljava/util/concurrent/CompletableFuture;
    //   95: invokestatic interruptedPreservingGet : (Ljava/util/concurrent/CompletableFuture;)Ljava/lang/Object;
    //   98: checkcast android/security/keymaster/ExportResult
    //   101: astore_1
    //   102: aload_0
    //   103: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   106: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   111: aload #6
    //   113: iconst_0
    //   114: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
    //   119: pop
    //   120: aload_1
    //   121: areturn
    //   122: new android/security/keymaster/ExportResult
    //   125: dup
    //   126: iload_2
    //   127: invokespecial <init> : (I)V
    //   130: astore_1
    //   131: aload_0
    //   132: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   135: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   140: aload #6
    //   142: iconst_0
    //   143: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
    //   148: pop
    //   149: aload_1
    //   150: areturn
    //   151: astore_1
    //   152: goto -> 176
    //   155: astore_1
    //   156: goto -> 207
    //   159: astore_1
    //   160: goto -> 238
    //   163: astore_1
    //   164: goto -> 176
    //   167: astore_1
    //   168: goto -> 207
    //   171: astore_1
    //   172: goto -> 238
    //   175: astore_1
    //   176: ldc 'KeyStore'
    //   178: ldc_w 'ExportKey completed with exception'
    //   181: aload_1
    //   182: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   185: pop
    //   186: aload_0
    //   187: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   190: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   195: aload #6
    //   197: iconst_0
    //   198: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
    //   203: pop
    //   204: aconst_null
    //   205: areturn
    //   206: astore_1
    //   207: ldc 'KeyStore'
    //   209: ldc_w 'Cannot connect to keystore'
    //   212: aload_1
    //   213: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   216: pop
    //   217: aload_0
    //   218: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   221: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   226: aload #6
    //   228: iconst_0
    //   229: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
    //   234: pop
    //   235: aconst_null
    //   236: areturn
    //   237: astore_1
    //   238: aload_0
    //   239: getfield mBinder : Landroid/security/keystore/IKeystoreService;
    //   242: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   247: aload #6
    //   249: iconst_0
    //   250: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
    //   255: pop
    //   256: aload_1
    //   257: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #819	-> 0
    //   #821	-> 11
    //   #822	-> 28
    //   #823	-> 46
    //   #824	-> 66
    //   #825	-> 85
    //   #826	-> 90
    //   #837	-> 102
    //   #826	-> 120
    //   #828	-> 122
    //   #837	-> 131
    //   #828	-> 149
    //   #833	-> 151
    //   #830	-> 155
    //   #837	-> 159
    //   #833	-> 163
    //   #830	-> 167
    //   #837	-> 171
    //   #833	-> 175
    //   #834	-> 176
    //   #835	-> 186
    //   #837	-> 186
    //   #835	-> 204
    //   #830	-> 206
    //   #831	-> 207
    //   #832	-> 217
    //   #837	-> 217
    //   #832	-> 235
    //   #837	-> 237
    //   #838	-> 256
    // Exception table:
    //   from	to	target	type
    //   11	28	206	android/os/RemoteException
    //   11	28	175	java/util/concurrent/ExecutionException
    //   11	28	171	finally
    //   35	46	206	android/os/RemoteException
    //   35	46	175	java/util/concurrent/ExecutionException
    //   35	46	171	finally
    //   54	66	167	android/os/RemoteException
    //   54	66	163	java/util/concurrent/ExecutionException
    //   54	66	159	finally
    //   66	85	155	android/os/RemoteException
    //   66	85	151	java/util/concurrent/ExecutionException
    //   66	85	237	finally
    //   90	102	155	android/os/RemoteException
    //   90	102	151	java/util/concurrent/ExecutionException
    //   90	102	237	finally
    //   122	131	155	android/os/RemoteException
    //   122	131	151	java/util/concurrent/ExecutionException
    //   122	131	237	finally
    //   176	186	237	finally
    //   207	217	237	finally
  }
  
  public ExportResult exportKey(String paramString, int paramInt, KeymasterBlob paramKeymasterBlob1, KeymasterBlob paramKeymasterBlob2) {
    return exportKey(paramString, paramInt, paramKeymasterBlob1, paramKeymasterBlob2, -1);
  }
  
  class OperationPromise extends IKeystoreOperationResultCallback.Stub implements IBinder.DeathRecipient {
    private final CompletableFuture<OperationResult> future = new CompletableFuture<>();
    
    final KeyStore this$0;
    
    public void onFinished(OperationResult param1OperationResult) throws RemoteException {
      this.future.complete(param1OperationResult);
    }
    
    public final CompletableFuture<OperationResult> getFuture() {
      return this.future;
    }
    
    public void binderDied() {
      this.future.completeExceptionally((Throwable)new RemoteException("Keystore died"));
    }
    
    private OperationPromise() {}
  }
  
  public OperationResult begin(String paramString, int paramInt1, boolean paramBoolean, KeymasterArguments paramKeymasterArguments, byte[] paramArrayOfbyte, int paramInt2) {
    OperationPromise operationPromise = new OperationPromise();
    try {
      this.mBinder.asBinder().linkToDeath(operationPromise, 0);
      if (paramKeymasterArguments == null)
        paramKeymasterArguments = new KeymasterArguments(); 
      if (paramArrayOfbyte == null)
        paramArrayOfbyte = new byte[0]; 
      try {
        paramInt1 = this.mBinder.begin(operationPromise, getToken(), paramString, paramInt1, paramBoolean, paramKeymasterArguments, paramArrayOfbyte, paramInt2);
        if (paramInt1 == 1) {
          OperationResult operationResult1 = interruptedPreservingGet(operationPromise.getFuture());
          this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
          return operationResult1;
        } 
        OperationResult operationResult = new OperationResult(paramInt1);
        this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
        return operationResult;
      } catch (RemoteException remoteException) {
        Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
        this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
        return null;
      } catch (ExecutionException executionException) {
        Log.e("KeyStore", "Begin completed with exception", executionException);
        this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
        return null;
      } finally {}
    } catch (RemoteException remoteException) {
    
    } catch (ExecutionException executionException) {
      Log.e("KeyStore", "Begin completed with exception", executionException);
      this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
      return null;
    } finally {}
    Log.w("KeyStore", "Cannot connect to keystore", (Throwable)paramString);
    this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
    return null;
  }
  
  public OperationResult begin(String paramString, int paramInt, boolean paramBoolean, KeymasterArguments paramKeymasterArguments, byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null)
      paramArrayOfbyte = new byte[0]; 
    if (paramKeymasterArguments == null)
      paramKeymasterArguments = new KeymasterArguments(); 
    return begin(paramString, paramInt, paramBoolean, paramKeymasterArguments, paramArrayOfbyte, -1);
  }
  
  public OperationResult update(IBinder paramIBinder, KeymasterArguments paramKeymasterArguments, byte[] paramArrayOfbyte) {
    OperationPromise operationPromise = new OperationPromise();
    try {
      this.mBinder.asBinder().linkToDeath(operationPromise, 0);
      if (paramKeymasterArguments == null)
        paramKeymasterArguments = new KeymasterArguments(); 
      if (paramArrayOfbyte == null)
        paramArrayOfbyte = new byte[0]; 
      int i = this.mBinder.update(operationPromise, paramIBinder, paramKeymasterArguments, paramArrayOfbyte);
      if (i == 1) {
        OperationResult operationResult1 = interruptedPreservingGet(operationPromise.getFuture());
        this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
        return operationResult1;
      } 
      OperationResult operationResult = new OperationResult(i);
      this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
      return operationResult;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
      return null;
    } catch (ExecutionException executionException) {
      Log.e("KeyStore", "Update completed with exception", executionException);
      this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
      return null;
    } finally {}
    this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
    throw paramIBinder;
  }
  
  public OperationResult finish(IBinder paramIBinder, KeymasterArguments paramKeymasterArguments, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3) {
    OperationPromise operationPromise = new OperationPromise();
    try {
      this.mBinder.asBinder().linkToDeath(operationPromise, 0);
      if (paramKeymasterArguments == null)
        paramKeymasterArguments = new KeymasterArguments(); 
      if (paramArrayOfbyte3 == null)
        paramArrayOfbyte3 = new byte[0]; 
      if (paramArrayOfbyte1 == null)
        paramArrayOfbyte1 = new byte[0]; 
      if (paramArrayOfbyte2 == null)
        paramArrayOfbyte2 = new byte[0]; 
      try {
        int i = this.mBinder.finish(operationPromise, paramIBinder, paramKeymasterArguments, paramArrayOfbyte1, paramArrayOfbyte2, paramArrayOfbyte3);
        if (i == 1) {
          OperationResult operationResult1 = interruptedPreservingGet(operationPromise.getFuture());
          this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
          return operationResult1;
        } 
        OperationResult operationResult = new OperationResult(i);
        this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
        return operationResult;
      } catch (RemoteException remoteException) {
        Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
        this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
        return null;
      } catch (ExecutionException executionException) {
        Log.e("KeyStore", "Finish completed with exception", executionException);
        this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
        return null;
      } finally {}
    } catch (RemoteException remoteException) {
    
    } catch (ExecutionException executionException) {
      Log.e("KeyStore", "Finish completed with exception", executionException);
      this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
      return null;
    } finally {}
    Log.w("KeyStore", "Cannot connect to keystore", (Throwable)paramIBinder);
    this.mBinder.asBinder().unlinkToDeath(operationPromise, 0);
    return null;
  }
  
  public OperationResult finish(IBinder paramIBinder, KeymasterArguments paramKeymasterArguments, byte[] paramArrayOfbyte) {
    return finish(paramIBinder, paramKeymasterArguments, null, paramArrayOfbyte, null);
  }
  
  class KeystoreResultPromise extends IKeystoreResponseCallback.Stub implements IBinder.DeathRecipient {
    private final CompletableFuture<KeystoreResponse> future;
    
    final KeyStore this$0;
    
    private KeystoreResultPromise() {
      this.future = new CompletableFuture<>();
    }
    
    public void onFinished(KeystoreResponse param1KeystoreResponse) throws RemoteException {
      this.future.complete(param1KeystoreResponse);
    }
    
    public final CompletableFuture<KeystoreResponse> getFuture() {
      return this.future;
    }
    
    public void binderDied() {
      this.future.completeExceptionally((Throwable)new RemoteException("Keystore died"));
    }
  }
  
  public int abort(IBinder paramIBinder) {
    KeystoreResultPromise keystoreResultPromise = new KeystoreResultPromise();
    try {
      this.mBinder.asBinder().linkToDeath(keystoreResultPromise, 0);
      int i = this.mBinder.abort(keystoreResultPromise, paramIBinder);
      if (i == 1) {
        i = ((KeystoreResponse)interruptedPreservingGet(keystoreResultPromise.getFuture())).getErrorCode();
        this.mBinder.asBinder().unlinkToDeath(keystoreResultPromise, 0);
        return i;
      } 
      this.mBinder.asBinder().unlinkToDeath(keystoreResultPromise, 0);
      return i;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      this.mBinder.asBinder().unlinkToDeath(keystoreResultPromise, 0);
      return 4;
    } catch (ExecutionException executionException) {
      Log.e("KeyStore", "Abort completed with exception", executionException);
      this.mBinder.asBinder().unlinkToDeath(keystoreResultPromise, 0);
      return 4;
    } finally {}
    this.mBinder.asBinder().unlinkToDeath(keystoreResultPromise, 0);
    throw paramIBinder;
  }
  
  public int addAuthToken(byte[] paramArrayOfbyte) {
    try {
      return this.mBinder.addAuthToken(paramArrayOfbyte);
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return 4;
    } 
  }
  
  public boolean onUserPasswordChanged(int paramInt, String paramString) {
    String str = paramString;
    if (paramString == null)
      str = ""; 
    boolean bool = false;
    try {
      paramInt = this.mBinder.onUserPasswordChanged(paramInt, str);
      if (paramInt == 1)
        bool = true; 
      return bool;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return false;
    } 
  }
  
  public void onUserAdded(int paramInt1, int paramInt2) {
    try {
      this.mBinder.onUserAdded(paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
    } 
  }
  
  public void onUserAdded(int paramInt) {
    onUserAdded(paramInt, -1);
  }
  
  public void onUserRemoved(int paramInt) {
    try {
      this.mBinder.onUserRemoved(paramInt);
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
    } 
  }
  
  public boolean onUserPasswordChanged(String paramString) {
    return onUserPasswordChanged(UserHandle.getUserId(Process.myUid()), paramString);
  }
  
  public void onUserLockedStateChanged(int paramInt, boolean paramBoolean) {
    try {
      this.mBinder.onKeyguardVisibilityChanged(paramBoolean, paramInt);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to update user locked state ");
      stringBuilder.append(paramInt);
      Log.w("KeyStore", stringBuilder.toString(), (Throwable)remoteException);
    } 
  }
  
  class KeyAttestationCallbackResult {
    private KeymasterCertificateChain certificateChain;
    
    private KeystoreResponse keystoreResponse;
    
    final KeyStore this$0;
    
    public KeyAttestationCallbackResult(KeystoreResponse param1KeystoreResponse, KeymasterCertificateChain param1KeymasterCertificateChain) {
      this.keystoreResponse = param1KeystoreResponse;
      this.certificateChain = param1KeymasterCertificateChain;
    }
    
    public KeystoreResponse getKeystoreResponse() {
      return this.keystoreResponse;
    }
    
    public void setKeystoreResponse(KeystoreResponse param1KeystoreResponse) {
      this.keystoreResponse = param1KeystoreResponse;
    }
    
    public KeymasterCertificateChain getCertificateChain() {
      return this.certificateChain;
    }
    
    public void setCertificateChain(KeymasterCertificateChain param1KeymasterCertificateChain) {
      this.certificateChain = param1KeymasterCertificateChain;
    }
  }
  
  class CertificateChainPromise extends IKeystoreCertificateChainCallback.Stub implements IBinder.DeathRecipient {
    private final CompletableFuture<KeyStore.KeyAttestationCallbackResult> future = new CompletableFuture<>();
    
    final KeyStore this$0;
    
    public void onFinished(KeystoreResponse param1KeystoreResponse, KeymasterCertificateChain param1KeymasterCertificateChain) throws RemoteException {
      this.future.complete(new KeyStore.KeyAttestationCallbackResult(param1KeystoreResponse, param1KeymasterCertificateChain));
    }
    
    public final CompletableFuture<KeyStore.KeyAttestationCallbackResult> getFuture() {
      return this.future;
    }
    
    public void binderDied() {
      this.future.completeExceptionally((Throwable)new RemoteException("Keystore died"));
    }
    
    private CertificateChainPromise() {}
  }
  
  public int attestKey(String paramString, KeymasterArguments paramKeymasterArguments, KeymasterCertificateChain paramKeymasterCertificateChain) {
    CertificateChainPromise certificateChainPromise = new CertificateChainPromise();
    try {
      this.mBinder.asBinder().linkToDeath(certificateChainPromise, 0);
      KeymasterArguments keymasterArguments = paramKeymasterArguments;
      if (paramKeymasterArguments == null) {
        keymasterArguments = new KeymasterArguments();
        this();
      } 
      KeymasterCertificateChain keymasterCertificateChain = paramKeymasterCertificateChain;
      if (paramKeymasterCertificateChain == null) {
        keymasterCertificateChain = new KeymasterCertificateChain();
        this();
      } 
      int i = this.mBinder.attestKey(certificateChainPromise, paramString, keymasterArguments);
      if (i != 1) {
        this.mBinder.asBinder().unlinkToDeath(certificateChainPromise, 0);
        return i;
      } 
      KeyAttestationCallbackResult keyAttestationCallbackResult = interruptedPreservingGet(certificateChainPromise.getFuture());
      i = keyAttestationCallbackResult.getKeystoreResponse().getErrorCode();
      if (i == 1)
        keymasterCertificateChain.shallowCopyFrom(keyAttestationCallbackResult.getCertificateChain()); 
      this.mBinder.asBinder().unlinkToDeath(certificateChainPromise, 0);
      return i;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      this.mBinder.asBinder().unlinkToDeath(certificateChainPromise, 0);
      return 4;
    } catch (ExecutionException executionException) {
      Log.e("KeyStore", "AttestKey completed with exception", executionException);
      this.mBinder.asBinder().unlinkToDeath(certificateChainPromise, 0);
      return 4;
    } finally {}
    this.mBinder.asBinder().unlinkToDeath(certificateChainPromise, 0);
    throw paramString;
  }
  
  public int attestDeviceIds(KeymasterArguments paramKeymasterArguments, KeymasterCertificateChain paramKeymasterCertificateChain) {
    CertificateChainPromise certificateChainPromise = new CertificateChainPromise();
    try {
      this.mBinder.asBinder().linkToDeath(certificateChainPromise, 0);
      KeymasterArguments keymasterArguments = paramKeymasterArguments;
      if (paramKeymasterArguments == null) {
        keymasterArguments = new KeymasterArguments();
        this();
      } 
      KeymasterCertificateChain keymasterCertificateChain = paramKeymasterCertificateChain;
      if (paramKeymasterCertificateChain == null) {
        keymasterCertificateChain = new KeymasterCertificateChain();
        this();
      } 
      int i = this.mBinder.attestDeviceIds(certificateChainPromise, keymasterArguments);
      if (i != 1) {
        this.mBinder.asBinder().unlinkToDeath(certificateChainPromise, 0);
        return i;
      } 
      KeyAttestationCallbackResult keyAttestationCallbackResult = interruptedPreservingGet(certificateChainPromise.getFuture());
      i = keyAttestationCallbackResult.getKeystoreResponse().getErrorCode();
      if (i == 1)
        keymasterCertificateChain.shallowCopyFrom(keyAttestationCallbackResult.getCertificateChain()); 
      this.mBinder.asBinder().unlinkToDeath(certificateChainPromise, 0);
      return i;
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      this.mBinder.asBinder().unlinkToDeath(certificateChainPromise, 0);
      return 4;
    } catch (ExecutionException executionException) {
      Log.e("KeyStore", "AttestDevicdeIds completed with exception", executionException);
      this.mBinder.asBinder().unlinkToDeath(certificateChainPromise, 0);
      return 4;
    } finally {}
    this.mBinder.asBinder().unlinkToDeath(certificateChainPromise, 0);
    throw paramKeymasterArguments;
  }
  
  public void onDeviceOffBody() {
    try {
      this.mBinder.onDeviceOffBody();
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
    } 
  }
  
  public int presentConfirmationPrompt(IBinder paramIBinder, String paramString1, byte[] paramArrayOfbyte, String paramString2, int paramInt) {
    try {
      return this.mBinder.presentConfirmationPrompt(paramIBinder, paramString1, paramArrayOfbyte, paramString2, paramInt);
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return 5;
    } 
  }
  
  public int cancelConfirmationPrompt(IBinder paramIBinder) {
    try {
      return this.mBinder.cancelConfirmationPrompt(paramIBinder);
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return 5;
    } 
  }
  
  public boolean isConfirmationPromptSupported() {
    try {
      return this.mBinder.isConfirmationPromptSupported();
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return false;
    } 
  }
  
  public static KeyStoreException getKeyStoreException(int paramInt) {
    if (paramInt > 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4) {
              if (paramInt != 6) {
                if (paramInt != 7) {
                  if (paramInt != 8) {
                    if (paramInt != 15) {
                      if (paramInt != 17)
                        return new KeyStoreException(paramInt, String.valueOf(paramInt)); 
                      return new KeyStoreException(paramInt, "Key permanently invalidated");
                    } 
                    return new KeyStoreException(paramInt, "Operation requires authorization");
                  } 
                  return new KeyStoreException(paramInt, "Key blob corrupted");
                } 
                return new KeyStoreException(paramInt, "Key not found");
              } 
              return new KeyStoreException(paramInt, "Permission denied");
            } 
            return new KeyStoreException(paramInt, "System error");
          } 
          return new KeyStoreException(paramInt, "Keystore not initialized");
        } 
        return new KeyStoreException(paramInt, "User authentication required");
      } 
      return new KeyStoreException(paramInt, "OK");
    } 
    if (paramInt != -16)
      return 
        new KeyStoreException(paramInt, KeymasterDefs.getErrorMessage(paramInt)); 
    return new KeyStoreException(paramInt, "Invalid user authentication validity duration");
  }
  
  public InvalidKeyException getInvalidKeyException(String paramString, int paramInt, KeyStoreException paramKeyStoreException) {
    int i = paramKeyStoreException.getErrorCode();
    if (i != 2) {
      if (i != 3) {
        if (i != 15)
          switch (i) {
            default:
              return new InvalidKeyException("Keystore operation failed", paramKeyStoreException);
            case -24:
              return new KeyNotYetValidException();
            case -25:
              return new KeyExpiredException();
            case -26:
              break;
          }  
        KeyCharacteristics keyCharacteristics = new KeyCharacteristics();
        paramInt = getKeyCharacteristics(paramString, null, null, paramInt, keyCharacteristics);
        if (paramInt != 1)
          return new InvalidKeyException("Failed to obtained key characteristics", getKeyStoreException(paramInt)); 
        List<BigInteger> list = keyCharacteristics.getUnsignedLongs(-1610612234);
        if (list.isEmpty())
          return new KeyPermanentlyInvalidatedException(); 
        long l = GateKeeper.getSecureUserId();
        if (l != 0L && list.contains(KeymasterArguments.toUint64(l)))
          return new UserNotAuthenticatedException(); 
        BiometricManager biometricManager = (BiometricManager)this.mContext.getSystemService(BiometricManager.class);
        long[] arrayOfLong = biometricManager.getAuthenticatorIds();
        boolean bool = true;
        int j = arrayOfLong.length;
        paramInt = 0;
        while (true) {
          i = bool;
          if (paramInt < j) {
            l = arrayOfLong[paramInt];
            if (!list.contains(KeymasterArguments.toUint64(l))) {
              i = 0;
              break;
            } 
            paramInt++;
            continue;
          } 
          break;
        } 
        if (i != 0)
          return new UserNotAuthenticatedException(); 
        return new KeyPermanentlyInvalidatedException();
      } 
      return new KeyPermanentlyInvalidatedException();
    } 
    return new UserNotAuthenticatedException();
  }
  
  public byte[] getGateKeeperAuthToken() {
    try {
      return this.mBinder.getGateKeeperAuthToken();
    } catch (RemoteException remoteException) {
      Log.w("KeyStore", "Cannot connect to keystore", (Throwable)remoteException);
      return null;
    } 
  }
  
  public InvalidKeyException getInvalidKeyException(String paramString, int paramInt1, int paramInt2) {
    return getInvalidKeyException(paramString, paramInt1, getKeyStoreException(paramInt2));
  }
  
  private static <R> R interruptedPreservingGet(CompletableFuture<R> paramCompletableFuture) throws ExecutionException {
    boolean bool = false;
    while (true) {
      try {
        R r = paramCompletableFuture.get();
        if (bool)
          Thread.currentThread().interrupt(); 
        return r;
      } catch (InterruptedException interruptedException) {
        bool = true;
      } 
    } 
  }
}
