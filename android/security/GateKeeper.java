package android.security;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.service.gatekeeper.IGateKeeperService;
import com.oplus.multiapp.OplusMultiAppManager;

public abstract class GateKeeper {
  public static final long INVALID_SECURE_USER_ID = 0L;
  
  public static IGateKeeperService getService() {
    IBinder iBinder = ServiceManager.getService("android.service.gatekeeper.IGateKeeperService");
    IGateKeeperService iGateKeeperService = IGateKeeperService.Stub.asInterface(iBinder);
    if (iGateKeeperService != null)
      return iGateKeeperService; 
    throw new IllegalStateException("Gatekeeper service not available");
  }
  
  public static long getSecureUserId() throws IllegalStateException {
    try {
      int i = UserHandle.myUserId();
      int j = i;
      if (OplusMultiAppManager.getInstance().isMultiAppUserId(i))
        j = 0; 
      return getService().getSecureUserId(j);
    } catch (RemoteException remoteException) {
      throw new IllegalStateException("Failed to obtain secure user ID from gatekeeper", remoteException);
    } 
  }
}
