package android.security;

public interface IOplusKeyStoreEx {
  byte[] getGateKeeperAuthToken();
}
