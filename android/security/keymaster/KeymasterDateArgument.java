package android.security.keymaster;

import android.os.Parcel;
import java.util.Date;

class KeymasterDateArgument extends KeymasterArgument {
  public final Date date;
  
  public KeymasterDateArgument(int paramInt, Date paramDate) {
    super(paramInt);
    if (KeymasterDefs.getTagType(paramInt) == 1610612736) {
      this.date = paramDate;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad date tag ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public KeymasterDateArgument(int paramInt, Parcel paramParcel) {
    super(paramInt);
    this.date = new Date(paramParcel.readLong());
  }
  
  public void writeValue(Parcel paramParcel) {
    paramParcel.writeLong(this.date.getTime());
  }
}
