package android.security.keymaster;

import android.os.Parcel;

class KeymasterBlobArgument extends KeymasterArgument {
  public final byte[] blob;
  
  public KeymasterBlobArgument(int paramInt, byte[] paramArrayOfbyte) {
    super(paramInt);
    int i = KeymasterDefs.getTagType(paramInt);
    if (i == Integer.MIN_VALUE || i == -1879048192) {
      this.blob = paramArrayOfbyte;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad blob tag ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public KeymasterBlobArgument(int paramInt, Parcel paramParcel) {
    super(paramInt);
    this.blob = paramParcel.createByteArray();
  }
  
  public void writeValue(Parcel paramParcel) {
    paramParcel.writeByteArray(this.blob);
  }
}
