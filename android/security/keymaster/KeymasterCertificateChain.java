package android.security.keymaster;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public class KeymasterCertificateChain implements Parcelable {
  public static final Parcelable.Creator<KeymasterCertificateChain> CREATOR = new Parcelable.Creator<KeymasterCertificateChain>() {
      public KeymasterCertificateChain createFromParcel(Parcel param1Parcel) {
        return new KeymasterCertificateChain(param1Parcel);
      }
      
      public KeymasterCertificateChain[] newArray(int param1Int) {
        return new KeymasterCertificateChain[param1Int];
      }
    };
  
  private List<byte[]> mCertificates;
  
  public KeymasterCertificateChain() {
    this.mCertificates = null;
  }
  
  public KeymasterCertificateChain(List<byte[]> paramList) {
    this.mCertificates = paramList;
  }
  
  private KeymasterCertificateChain(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void shallowCopyFrom(KeymasterCertificateChain paramKeymasterCertificateChain) {
    this.mCertificates = paramKeymasterCertificateChain.mCertificates;
  }
  
  public List<byte[]> getCertificates() {
    return this.mCertificates;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    List<byte[]> list = this.mCertificates;
    if (list == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(list.size());
      for (byte[] arrayOfByte : this.mCertificates)
        paramParcel.writeByteArray(arrayOfByte); 
    } 
  }
  
  public void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.readInt();
    this.mCertificates = (List)new ArrayList<>(i);
    for (byte b = 0; b < i; b++)
      this.mCertificates.add(paramParcel.createByteArray()); 
  }
  
  public int describeContents() {
    return 0;
  }
}
