package android.security.keymaster;

import android.content.pm.Signature;
import android.os.Parcel;
import android.os.Parcelable;

public class KeyAttestationPackageInfo implements Parcelable {
  public KeyAttestationPackageInfo(String paramString, long paramLong, Signature[] paramArrayOfSignature) {
    this.mPackageName = paramString;
    this.mPackageVersionCode = paramLong;
    this.mPackageSignatures = paramArrayOfSignature;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public long getPackageVersionCode() {
    return this.mPackageVersionCode;
  }
  
  public Signature[] getPackageSignatures() {
    return this.mPackageSignatures;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeLong(this.mPackageVersionCode);
    paramParcel.writeTypedArray(this.mPackageSignatures, paramInt);
  }
  
  public static final Parcelable.Creator<KeyAttestationPackageInfo> CREATOR = new Parcelable.Creator<KeyAttestationPackageInfo>() {
      public KeyAttestationPackageInfo createFromParcel(Parcel param1Parcel) {
        return new KeyAttestationPackageInfo(param1Parcel);
      }
      
      public KeyAttestationPackageInfo[] newArray(int param1Int) {
        return new KeyAttestationPackageInfo[param1Int];
      }
    };
  
  private final String mPackageName;
  
  private final Signature[] mPackageSignatures;
  
  private final long mPackageVersionCode;
  
  private KeyAttestationPackageInfo(Parcel paramParcel) {
    this.mPackageName = paramParcel.readString();
    this.mPackageVersionCode = paramParcel.readLong();
    this.mPackageSignatures = paramParcel.<Signature>createTypedArray(Signature.CREATOR);
  }
}
