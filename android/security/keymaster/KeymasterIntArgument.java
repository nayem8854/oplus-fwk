package android.security.keymaster;

import android.os.Parcel;

class KeymasterIntArgument extends KeymasterArgument {
  public final int value;
  
  public KeymasterIntArgument(int paramInt1, int paramInt2) {
    super(paramInt1);
    int i = KeymasterDefs.getTagType(paramInt1);
    if (i == 268435456 || i == 536870912 || i == 805306368 || i == 1073741824) {
      this.value = paramInt2;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad int tag ");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public KeymasterIntArgument(int paramInt, Parcel paramParcel) {
    super(paramInt);
    this.value = paramParcel.readInt();
  }
  
  public void writeValue(Parcel paramParcel) {
    paramParcel.writeInt(this.value);
  }
}
