package android.security.keymaster;

import android.os.Parcel;

class KeymasterLongArgument extends KeymasterArgument {
  public final long value;
  
  public KeymasterLongArgument(int paramInt, long paramLong) {
    super(paramInt);
    int i = KeymasterDefs.getTagType(paramInt);
    if (i == -1610612736 || i == 1342177280) {
      this.value = paramLong;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad long tag ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public KeymasterLongArgument(int paramInt, Parcel paramParcel) {
    super(paramInt);
    this.value = paramParcel.readLong();
  }
  
  public void writeValue(Parcel paramParcel) {
    paramParcel.writeLong(this.value);
  }
}
