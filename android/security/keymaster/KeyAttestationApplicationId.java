package android.security.keymaster;

import android.os.Parcel;
import android.os.Parcelable;

public class KeyAttestationApplicationId implements Parcelable {
  public KeyAttestationApplicationId(KeyAttestationPackageInfo[] paramArrayOfKeyAttestationPackageInfo) {
    this.mAttestationPackageInfos = paramArrayOfKeyAttestationPackageInfo;
  }
  
  public KeyAttestationPackageInfo[] getAttestationPackageInfos() {
    return this.mAttestationPackageInfos;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedArray(this.mAttestationPackageInfos, paramInt);
  }
  
  public static final Parcelable.Creator<KeyAttestationApplicationId> CREATOR = new Parcelable.Creator<KeyAttestationApplicationId>() {
      public KeyAttestationApplicationId createFromParcel(Parcel param1Parcel) {
        return new KeyAttestationApplicationId(param1Parcel);
      }
      
      public KeyAttestationApplicationId[] newArray(int param1Int) {
        return new KeyAttestationApplicationId[param1Int];
      }
    };
  
  private final KeyAttestationPackageInfo[] mAttestationPackageInfos;
  
  KeyAttestationApplicationId(Parcel paramParcel) {
    this.mAttestationPackageInfos = paramParcel.<KeyAttestationPackageInfo>createTypedArray(KeyAttestationPackageInfo.CREATOR);
  }
}
