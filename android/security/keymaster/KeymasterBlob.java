package android.security.keymaster;

import android.os.Parcel;
import android.os.Parcelable;

public class KeymasterBlob implements Parcelable {
  public KeymasterBlob(byte[] paramArrayOfbyte) {
    this.blob = paramArrayOfbyte;
  }
  
  public static final Parcelable.Creator<KeymasterBlob> CREATOR = new Parcelable.Creator<KeymasterBlob>() {
      public KeymasterBlob createFromParcel(Parcel param1Parcel) {
        return new KeymasterBlob(param1Parcel);
      }
      
      public KeymasterBlob[] newArray(int param1Int) {
        return new KeymasterBlob[param1Int];
      }
    };
  
  public byte[] blob;
  
  protected KeymasterBlob(Parcel paramParcel) {
    this.blob = paramParcel.createByteArray();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeByteArray(this.blob);
  }
}
