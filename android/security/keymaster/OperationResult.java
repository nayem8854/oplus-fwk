package android.security.keymaster;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public class OperationResult implements Parcelable {
  public static final Parcelable.Creator<OperationResult> CREATOR = new Parcelable.Creator<OperationResult>() {
      public OperationResult createFromParcel(Parcel param1Parcel) {
        return new OperationResult(param1Parcel);
      }
      
      public OperationResult[] newArray(int param1Int) {
        return new OperationResult[param1Int];
      }
    };
  
  public final int inputConsumed;
  
  public final long operationHandle;
  
  public final KeymasterArguments outParams;
  
  public final byte[] output;
  
  public final int resultCode;
  
  public final IBinder token;
  
  public OperationResult(int paramInt1, IBinder paramIBinder, long paramLong, int paramInt2, byte[] paramArrayOfbyte, KeymasterArguments paramKeymasterArguments) {
    this.resultCode = paramInt1;
    this.token = paramIBinder;
    this.operationHandle = paramLong;
    this.inputConsumed = paramInt2;
    this.output = paramArrayOfbyte;
    this.outParams = paramKeymasterArguments;
  }
  
  public OperationResult(int paramInt) {
    this(paramInt, null, 0L, 0, null, null);
  }
  
  protected OperationResult(Parcel paramParcel) {
    this.resultCode = paramParcel.readInt();
    this.token = paramParcel.readStrongBinder();
    this.operationHandle = paramParcel.readLong();
    this.inputConsumed = paramParcel.readInt();
    this.output = paramParcel.createByteArray();
    this.outParams = KeymasterArguments.CREATOR.createFromParcel(paramParcel);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.resultCode);
    paramParcel.writeStrongBinder(this.token);
    paramParcel.writeLong(this.operationHandle);
    paramParcel.writeInt(this.inputConsumed);
    paramParcel.writeByteArray(this.output);
    this.outParams.writeToParcel(paramParcel, paramInt);
  }
}
