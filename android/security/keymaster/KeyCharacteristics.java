package android.security.keymaster;

import android.os.Parcel;
import android.os.Parcelable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class KeyCharacteristics implements Parcelable {
  public static final Parcelable.Creator<KeyCharacteristics> CREATOR = new Parcelable.Creator<KeyCharacteristics>() {
      public KeyCharacteristics createFromParcel(Parcel param1Parcel) {
        return new KeyCharacteristics(param1Parcel);
      }
      
      public KeyCharacteristics[] newArray(int param1Int) {
        return new KeyCharacteristics[param1Int];
      }
    };
  
  public KeymasterArguments hwEnforced;
  
  public KeymasterArguments swEnforced;
  
  public KeyCharacteristics() {}
  
  protected KeyCharacteristics(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void shallowCopyFrom(KeyCharacteristics paramKeyCharacteristics) {
    this.swEnforced = paramKeyCharacteristics.swEnforced;
    this.hwEnforced = paramKeyCharacteristics.hwEnforced;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.swEnforced.writeToParcel(paramParcel, paramInt);
    this.hwEnforced.writeToParcel(paramParcel, paramInt);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.swEnforced = KeymasterArguments.CREATOR.createFromParcel(paramParcel);
    this.hwEnforced = KeymasterArguments.CREATOR.createFromParcel(paramParcel);
  }
  
  public Integer getEnum(int paramInt) {
    if (this.hwEnforced.containsTag(paramInt))
      return Integer.valueOf(this.hwEnforced.getEnum(paramInt, -1)); 
    if (this.swEnforced.containsTag(paramInt))
      return Integer.valueOf(this.swEnforced.getEnum(paramInt, -1)); 
    return null;
  }
  
  public List<Integer> getEnums(int paramInt) {
    ArrayList<Integer> arrayList = new ArrayList();
    arrayList.addAll(this.hwEnforced.getEnums(paramInt));
    arrayList.addAll(this.swEnforced.getEnums(paramInt));
    return arrayList;
  }
  
  public long getUnsignedInt(int paramInt, long paramLong) {
    if (this.hwEnforced.containsTag(paramInt))
      return this.hwEnforced.getUnsignedInt(paramInt, paramLong); 
    return this.swEnforced.getUnsignedInt(paramInt, paramLong);
  }
  
  public List<BigInteger> getUnsignedLongs(int paramInt) {
    ArrayList<BigInteger> arrayList = new ArrayList();
    arrayList.addAll(this.hwEnforced.getUnsignedLongs(paramInt));
    arrayList.addAll(this.swEnforced.getUnsignedLongs(paramInt));
    return arrayList;
  }
  
  public Date getDate(int paramInt) {
    Date date = this.swEnforced.getDate(paramInt, null);
    if (date != null)
      return date; 
    return this.hwEnforced.getDate(paramInt, null);
  }
  
  public boolean getBoolean(int paramInt) {
    if (this.hwEnforced.containsTag(paramInt))
      return this.hwEnforced.getBoolean(paramInt); 
    return this.swEnforced.getBoolean(paramInt);
  }
}
