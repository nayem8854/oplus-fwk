package android.security.keymaster;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IKeyAttestationApplicationIdProvider extends IInterface {
  KeyAttestationApplicationId getKeyAttestationApplicationId(int paramInt) throws RemoteException;
  
  class Default implements IKeyAttestationApplicationIdProvider {
    public KeyAttestationApplicationId getKeyAttestationApplicationId(int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IKeyAttestationApplicationIdProvider {
    private static final String DESCRIPTOR = "android.security.keymaster.IKeyAttestationApplicationIdProvider";
    
    static final int TRANSACTION_getKeyAttestationApplicationId = 1;
    
    public Stub() {
      attachInterface(this, "android.security.keymaster.IKeyAttestationApplicationIdProvider");
    }
    
    public static IKeyAttestationApplicationIdProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.keymaster.IKeyAttestationApplicationIdProvider");
      if (iInterface != null && iInterface instanceof IKeyAttestationApplicationIdProvider)
        return (IKeyAttestationApplicationIdProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getKeyAttestationApplicationId";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.security.keymaster.IKeyAttestationApplicationIdProvider");
        return true;
      } 
      param1Parcel1.enforceInterface("android.security.keymaster.IKeyAttestationApplicationIdProvider");
      param1Int1 = param1Parcel1.readInt();
      KeyAttestationApplicationId keyAttestationApplicationId = getKeyAttestationApplicationId(param1Int1);
      param1Parcel2.writeNoException();
      if (keyAttestationApplicationId != null) {
        param1Parcel2.writeInt(1);
        keyAttestationApplicationId.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements IKeyAttestationApplicationIdProvider {
      public static IKeyAttestationApplicationIdProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.keymaster.IKeyAttestationApplicationIdProvider";
      }
      
      public KeyAttestationApplicationId getKeyAttestationApplicationId(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          KeyAttestationApplicationId keyAttestationApplicationId;
          parcel1.writeInterfaceToken("android.security.keymaster.IKeyAttestationApplicationIdProvider");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IKeyAttestationApplicationIdProvider.Stub.getDefaultImpl() != null) {
            keyAttestationApplicationId = IKeyAttestationApplicationIdProvider.Stub.getDefaultImpl().getKeyAttestationApplicationId(param2Int);
            return keyAttestationApplicationId;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            keyAttestationApplicationId = KeyAttestationApplicationId.CREATOR.createFromParcel(parcel2);
          } else {
            keyAttestationApplicationId = null;
          } 
          return keyAttestationApplicationId;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IKeyAttestationApplicationIdProvider param1IKeyAttestationApplicationIdProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IKeyAttestationApplicationIdProvider != null) {
          Proxy.sDefaultImpl = param1IKeyAttestationApplicationIdProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IKeyAttestationApplicationIdProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
