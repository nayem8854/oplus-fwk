package android.security.keymaster;

import android.os.Parcel;

class KeymasterBooleanArgument extends KeymasterArgument {
  public final boolean value = true;
  
  public KeymasterBooleanArgument(int paramInt) {
    super(paramInt);
    if (KeymasterDefs.getTagType(paramInt) == 1879048192)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad bool tag ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public KeymasterBooleanArgument(int paramInt, Parcel paramParcel) {
    super(paramInt);
  }
  
  public void writeValue(Parcel paramParcel) {}
}
