package android.security.keymaster;

import android.os.Parcel;
import android.os.Parcelable;

public class ExportResult implements Parcelable {
  public ExportResult(int paramInt) {
    this.resultCode = paramInt;
    this.exportData = new byte[0];
  }
  
  public static final Parcelable.Creator<ExportResult> CREATOR = new Parcelable.Creator<ExportResult>() {
      public ExportResult createFromParcel(Parcel param1Parcel) {
        return new ExportResult(param1Parcel);
      }
      
      public ExportResult[] newArray(int param1Int) {
        return new ExportResult[param1Int];
      }
    };
  
  public final byte[] exportData;
  
  public final int resultCode;
  
  protected ExportResult(Parcel paramParcel) {
    this.resultCode = paramParcel.readInt();
    this.exportData = paramParcel.createByteArray();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.resultCode);
    paramParcel.writeByteArray(this.exportData);
  }
}
