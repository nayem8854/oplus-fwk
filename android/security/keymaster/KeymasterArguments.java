package android.security.keymaster;

import android.os.Parcel;
import android.os.Parcelable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class KeymasterArguments implements Parcelable {
  public static final Parcelable.Creator<KeymasterArguments> CREATOR;
  
  public static final long UINT32_MAX_VALUE = 4294967295L;
  
  private static final long UINT32_RANGE = 4294967296L;
  
  public static final BigInteger UINT64_MAX_VALUE;
  
  private static final BigInteger UINT64_RANGE;
  
  private List<KeymasterArgument> mArguments;
  
  static {
    BigInteger bigInteger = BigInteger.ONE.shiftLeft(64);
    UINT64_MAX_VALUE = bigInteger.subtract(BigInteger.ONE);
    CREATOR = new Parcelable.Creator<KeymasterArguments>() {
        public KeymasterArguments createFromParcel(Parcel param1Parcel) {
          return new KeymasterArguments(param1Parcel);
        }
        
        public KeymasterArguments[] newArray(int param1Int) {
          return new KeymasterArguments[param1Int];
        }
      };
  }
  
  public KeymasterArguments() {
    this.mArguments = new ArrayList<>();
  }
  
  private KeymasterArguments(Parcel paramParcel) {
    this.mArguments = paramParcel.createTypedArrayList(KeymasterArgument.CREATOR);
  }
  
  public void addEnum(int paramInt1, int paramInt2) {
    int i = KeymasterDefs.getTagType(paramInt1);
    if (i == 268435456 || i == 536870912) {
      addEnumTag(paramInt1, paramInt2);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not an enum or repeating enum tag: ");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void addEnums(int paramInt, int... paramVarArgs) {
    if (KeymasterDefs.getTagType(paramInt) == 536870912) {
      int i;
      byte b;
      for (i = paramVarArgs.length, b = 0; b < i; ) {
        int j = paramVarArgs[b];
        addEnumTag(paramInt, j);
        b++;
      } 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not a repeating enum tag: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int getEnum(int paramInt1, int paramInt2) {
    if (KeymasterDefs.getTagType(paramInt1) == 268435456) {
      KeymasterArgument keymasterArgument = getArgumentByTag(paramInt1);
      if (keymasterArgument == null)
        return paramInt2; 
      return getEnumTagValue(keymasterArgument);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not an enum tag: ");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public List<Integer> getEnums(int paramInt) {
    if (KeymasterDefs.getTagType(paramInt) == 536870912) {
      ArrayList<Integer> arrayList = new ArrayList();
      for (KeymasterArgument keymasterArgument : this.mArguments) {
        if (keymasterArgument.tag == paramInt)
          arrayList.add(Integer.valueOf(getEnumTagValue(keymasterArgument))); 
      } 
      return arrayList;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not a repeating enum tag: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private void addEnumTag(int paramInt1, int paramInt2) {
    this.mArguments.add(new KeymasterIntArgument(paramInt1, paramInt2));
  }
  
  private int getEnumTagValue(KeymasterArgument paramKeymasterArgument) {
    return ((KeymasterIntArgument)paramKeymasterArgument).value;
  }
  
  public void addUnsignedInt(int paramInt, long paramLong) {
    int i = KeymasterDefs.getTagType(paramInt);
    if (i == 805306368 || i == 1073741824) {
      if (paramLong >= 0L && paramLong <= 4294967295L) {
        this.mArguments.add(new KeymasterIntArgument(paramInt, (int)paramLong));
        return;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Int tag value out of range: ");
      stringBuilder1.append(paramLong);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not an int or repeating int tag: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public long getUnsignedInt(int paramInt, long paramLong) {
    if (KeymasterDefs.getTagType(paramInt) == 805306368) {
      KeymasterArgument keymasterArgument = getArgumentByTag(paramInt);
      if (keymasterArgument == null)
        return paramLong; 
      return ((KeymasterIntArgument)keymasterArgument).value & 0xFFFFFFFFL;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not an int tag: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void addUnsignedLong(int paramInt, BigInteger paramBigInteger) {
    int i = KeymasterDefs.getTagType(paramInt);
    if (i == 1342177280 || i == -1610612736) {
      addLongTag(paramInt, paramBigInteger);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not a long or repeating long tag: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public List<BigInteger> getUnsignedLongs(int paramInt) {
    if (KeymasterDefs.getTagType(paramInt) == -1610612736) {
      ArrayList<BigInteger> arrayList = new ArrayList();
      for (KeymasterArgument keymasterArgument : this.mArguments) {
        if (keymasterArgument.tag == paramInt)
          arrayList.add(getLongTagValue(keymasterArgument)); 
      } 
      return arrayList;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Tag is not a repeating long: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private void addLongTag(int paramInt, BigInteger paramBigInteger) {
    if (paramBigInteger.signum() != -1 && paramBigInteger.compareTo(UINT64_MAX_VALUE) <= 0) {
      this.mArguments.add(new KeymasterLongArgument(paramInt, paramBigInteger.longValue()));
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Long tag value out of range: ");
    stringBuilder.append(paramBigInteger);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private BigInteger getLongTagValue(KeymasterArgument paramKeymasterArgument) {
    return toUint64(((KeymasterLongArgument)paramKeymasterArgument).value);
  }
  
  public void addBoolean(int paramInt) {
    if (KeymasterDefs.getTagType(paramInt) == 1879048192) {
      this.mArguments.add(new KeymasterBooleanArgument(paramInt));
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not a boolean tag: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public boolean getBoolean(int paramInt) {
    if (KeymasterDefs.getTagType(paramInt) == 1879048192) {
      KeymasterArgument keymasterArgument = getArgumentByTag(paramInt);
      if (keymasterArgument == null)
        return false; 
      return true;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not a boolean tag: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void addBytes(int paramInt, byte[] paramArrayOfbyte) {
    if (KeymasterDefs.getTagType(paramInt) == -1879048192) {
      if (paramArrayOfbyte != null) {
        this.mArguments.add(new KeymasterBlobArgument(paramInt, paramArrayOfbyte));
        return;
      } 
      throw new NullPointerException("value == nulll");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not a bytes tag: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public byte[] getBytes(int paramInt, byte[] paramArrayOfbyte) {
    if (KeymasterDefs.getTagType(paramInt) == -1879048192) {
      KeymasterArgument keymasterArgument = getArgumentByTag(paramInt);
      if (keymasterArgument == null)
        return paramArrayOfbyte; 
      return ((KeymasterBlobArgument)keymasterArgument).blob;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not a bytes tag: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void addDate(int paramInt, Date paramDate) {
    if (KeymasterDefs.getTagType(paramInt) == 1610612736) {
      if (paramDate != null) {
        if (paramDate.getTime() >= 0L) {
          this.mArguments.add(new KeymasterDateArgument(paramInt, paramDate));
          return;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Date tag value out of range: ");
        stringBuilder1.append(paramDate);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      throw new NullPointerException("value == nulll");
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not a date tag: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void addDateIfNotNull(int paramInt, Date paramDate) {
    if (KeymasterDefs.getTagType(paramInt) == 1610612736) {
      if (paramDate != null)
        addDate(paramInt, paramDate); 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not a date tag: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public Date getDate(int paramInt, Date paramDate) {
    if (KeymasterDefs.getTagType(paramInt) == 1610612736) {
      KeymasterArgument keymasterArgument = getArgumentByTag(paramInt);
      if (keymasterArgument == null)
        return paramDate; 
      paramDate = ((KeymasterDateArgument)keymasterArgument).date;
      if (paramDate.getTime() >= 0L)
        return paramDate; 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Tag value too large. Tag: ");
      stringBuilder1.append(paramInt);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Tag is not a date type: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private KeymasterArgument getArgumentByTag(int paramInt) {
    for (KeymasterArgument keymasterArgument : this.mArguments) {
      if (keymasterArgument.tag == paramInt)
        return keymasterArgument; 
    } 
    return null;
  }
  
  public boolean containsTag(int paramInt) {
    boolean bool;
    if (getArgumentByTag(paramInt) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int size() {
    return this.mArguments.size();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedList(this.mArguments);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    paramParcel.readTypedList(this.mArguments, KeymasterArgument.CREATOR);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static BigInteger toUint64(long paramLong) {
    if (paramLong >= 0L)
      return BigInteger.valueOf(paramLong); 
    return BigInteger.valueOf(paramLong).add(UINT64_RANGE);
  }
}
