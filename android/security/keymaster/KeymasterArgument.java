package android.security.keymaster;

import android.os.Parcel;
import android.os.ParcelFormatException;
import android.os.Parcelable;

abstract class KeymasterArgument implements Parcelable {
  public static final Parcelable.Creator<KeymasterArgument> CREATOR = new Parcelable.Creator<KeymasterArgument>() {
      public KeymasterArgument createFromParcel(Parcel param1Parcel) {
        StringBuilder stringBuilder;
        int i = param1Parcel.dataPosition();
        int j = param1Parcel.readInt();
        int k = KeymasterDefs.getTagType(j);
        if (k != Integer.MIN_VALUE && k != -1879048192) {
          if (k != -1610612736)
            if (k != 268435456 && k != 536870912 && k != 805306368 && k != 1073741824) {
              if (k != 1342177280) {
                if (k != 1610612736) {
                  if (k == 1879048192)
                    return new KeymasterBooleanArgument(j, param1Parcel); 
                  stringBuilder = new StringBuilder();
                  stringBuilder.append("Bad tag: ");
                  stringBuilder.append(j);
                  stringBuilder.append(" at ");
                  stringBuilder.append(i);
                  throw new ParcelFormatException(stringBuilder.toString());
                } 
                return new KeymasterDateArgument(j, (Parcel)stringBuilder);
              } 
            } else {
              return new KeymasterIntArgument(j, (Parcel)stringBuilder);
            }  
          return new KeymasterLongArgument(j, (Parcel)stringBuilder);
        } 
        return new KeymasterBlobArgument(j, (Parcel)stringBuilder);
      }
      
      public KeymasterArgument[] newArray(int param1Int) {
        return new KeymasterArgument[param1Int];
      }
    };
  
  public final int tag;
  
  protected KeymasterArgument(int paramInt) {
    this.tag = paramInt;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.tag);
    writeValue(paramParcel);
  }
  
  public abstract void writeValue(Parcel paramParcel);
}
