package android.security;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IKeyChainAliasCallback extends IInterface {
  void alias(String paramString) throws RemoteException;
  
  class Default implements IKeyChainAliasCallback {
    public void alias(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IKeyChainAliasCallback {
    private static final String DESCRIPTOR = "android.security.IKeyChainAliasCallback";
    
    static final int TRANSACTION_alias = 1;
    
    public Stub() {
      attachInterface(this, "android.security.IKeyChainAliasCallback");
    }
    
    public static IKeyChainAliasCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.IKeyChainAliasCallback");
      if (iInterface != null && iInterface instanceof IKeyChainAliasCallback)
        return (IKeyChainAliasCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "alias";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.security.IKeyChainAliasCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.security.IKeyChainAliasCallback");
      String str = param1Parcel1.readString();
      alias(str);
      return true;
    }
    
    private static class Proxy implements IKeyChainAliasCallback {
      public static IKeyChainAliasCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.IKeyChainAliasCallback";
      }
      
      public void alias(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.security.IKeyChainAliasCallback");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IKeyChainAliasCallback.Stub.getDefaultImpl() != null) {
            IKeyChainAliasCallback.Stub.getDefaultImpl().alias(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IKeyChainAliasCallback param1IKeyChainAliasCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IKeyChainAliasCallback != null) {
          Proxy.sDefaultImpl = param1IKeyChainAliasCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IKeyChainAliasCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
