package android.security.identity;

public class AccessControlProfileId {
  private int mId = 0;
  
  public AccessControlProfileId(int paramInt) {
    this.mId = paramInt;
  }
  
  public int getId() {
    return this.mId;
  }
}
