package android.security.identity;

import android.os.Parcel;
import android.os.Parcelable;

public class AuthKeyParcel implements Parcelable {
  public static final Parcelable.Creator<AuthKeyParcel> CREATOR = new Parcelable.Creator<AuthKeyParcel>() {
      public AuthKeyParcel createFromParcel(Parcel param1Parcel) {
        AuthKeyParcel authKeyParcel = new AuthKeyParcel();
        authKeyParcel.readFromParcel(param1Parcel);
        return authKeyParcel;
      }
      
      public AuthKeyParcel[] newArray(int param1Int) {
        return new AuthKeyParcel[param1Int];
      }
    };
  
  public byte[] x509cert;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeByteArray(this.x509cert);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.x509cert = paramParcel.createByteArray();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
