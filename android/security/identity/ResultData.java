package android.security.identity;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collection;

public abstract class ResultData {
  public static final int STATUS_NOT_IN_REQUEST_MESSAGE = 3;
  
  public static final int STATUS_NOT_REQUESTED = 2;
  
  public static final int STATUS_NO_ACCESS_CONTROL_PROFILES = 6;
  
  public static final int STATUS_NO_SUCH_ENTRY = 1;
  
  public static final int STATUS_OK = 0;
  
  public static final int STATUS_READER_AUTHENTICATION_FAILED = 5;
  
  public static final int STATUS_USER_AUTHENTICATION_FAILED = 4;
  
  public abstract byte[] getAuthenticatedData();
  
  public abstract byte[] getEntry(String paramString1, String paramString2);
  
  public abstract Collection<String> getEntryNames(String paramString);
  
  public abstract byte[] getMessageAuthenticationCode();
  
  public abstract Collection<String> getNamespaces();
  
  public abstract Collection<String> getRetrievedEntryNames(String paramString);
  
  public abstract byte[] getStaticAuthenticationData();
  
  public abstract int getStatus(String paramString1, String paramString2);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Status {}
}
