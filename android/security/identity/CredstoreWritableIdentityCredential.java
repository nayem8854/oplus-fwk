package android.security.identity;

import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceSpecificException;
import android.security.GateKeeper;
import java.io.ByteArrayInputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

class CredstoreWritableIdentityCredential extends WritableIdentityCredential {
  private static final String TAG = "CredstoreWritableIdentityCredential";
  
  private IWritableCredential mBinder;
  
  private Context mContext;
  
  private String mCredentialName;
  
  private String mDocType;
  
  CredstoreWritableIdentityCredential(Context paramContext, String paramString1, String paramString2, IWritableCredential paramIWritableCredential) {
    this.mContext = paramContext;
    this.mDocType = paramString2;
    this.mCredentialName = paramString1;
    this.mBinder = paramIWritableCredential;
  }
  
  public Collection<X509Certificate> getCredentialKeyCertificateChain(byte[] paramArrayOfbyte) {
    try {
      byte[] arrayOfByte = this.mBinder.getCredentialKeyCertificateChain(paramArrayOfbyte);
      ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
      this(arrayOfByte);
      try {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        Collection<? extends Certificate> collection = certificateFactory.generateCertificates(byteArrayInputStream);
        LinkedList<X509Certificate> linkedList = new LinkedList();
        this();
        for (Certificate certificate : collection)
          linkedList.add((X509Certificate)certificate); 
        return linkedList;
      } catch (CertificateException certificateException) {
        RuntimeException runtimeException = new RuntimeException();
        this("Error decoding certificates", certificateException);
        throw runtimeException;
      } 
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected ServiceSpecificException with code ");
      stringBuilder.append(serviceSpecificException.errorCode);
      throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
    } 
  }
  
  public byte[] personalize(PersonalizationData paramPersonalizationData) {
    long l;
    Collection<AccessControlProfile> collection = paramPersonalizationData.getAccessControlProfiles();
    AccessControlProfileParcel[] arrayOfAccessControlProfileParcel = new AccessControlProfileParcel[collection.size()];
    byte b;
    boolean bool;
    for (Iterator<AccessControlProfile> iterator = collection.iterator(); iterator.hasNext(); ) {
      AccessControlProfile accessControlProfile = iterator.next();
      arrayOfAccessControlProfileParcel[b] = new AccessControlProfileParcel();
      (arrayOfAccessControlProfileParcel[b]).id = accessControlProfile.getAccessControlProfileId().getId();
      X509Certificate x509Certificate = accessControlProfile.getReaderCertificate();
      if (x509Certificate != null) {
        try {
          (arrayOfAccessControlProfileParcel[b]).readerCertificate = x509Certificate.getEncoded();
        } catch (CertificateException certificateException) {
          throw new RuntimeException("Error encoding reader certificate", certificateException);
        } 
      } else {
        (arrayOfAccessControlProfileParcel[b]).readerCertificate = new byte[0];
      } 
      (arrayOfAccessControlProfileParcel[b]).userAuthenticationRequired = accessControlProfile.isUserAuthenticationRequired();
      (arrayOfAccessControlProfileParcel[b]).userAuthenticationTimeoutMillis = accessControlProfile.getUserAuthenticationTimeout();
      if (accessControlProfile.isUserAuthenticationRequired())
        bool = true; 
      b++;
    } 
    Collection<String> collection1 = certificateException.getNamespaces();
    EntryNamespaceParcel[] arrayOfEntryNamespaceParcel = new EntryNamespaceParcel[collection1.size()];
    for (Iterator<String> iterator1 = collection1.iterator(); iterator1.hasNext(); ) {
      String str = iterator1.next();
      PersonalizationData.NamespaceData namespaceData = certificateException.getNamespaceData(str);
      arrayOfEntryNamespaceParcel[b] = new EntryNamespaceParcel();
      (arrayOfEntryNamespaceParcel[b]).namespaceName = str;
      Collection<String> collection2 = namespaceData.getEntryNames();
      EntryParcel[] arrayOfEntryParcel = new EntryParcel[collection2.size()];
      byte b1 = 0;
      for (String str1 : collection2) {
        arrayOfEntryParcel[b1] = new EntryParcel();
        (arrayOfEntryParcel[b1]).name = str1;
        (arrayOfEntryParcel[b1]).value = namespaceData.getEntryValue(str1);
        Collection<AccessControlProfileId> collection3 = namespaceData.getAccessControlProfileIds(str1);
        (arrayOfEntryParcel[b1]).accessControlProfileIds = new int[collection3.size()];
        byte b2 = 0;
        for (AccessControlProfileId accessControlProfileId : collection3) {
          (arrayOfEntryParcel[b1]).accessControlProfileIds[b2] = accessControlProfileId.getId();
          b2++;
        } 
        b1++;
      } 
      (arrayOfEntryNamespaceParcel[b]).entries = arrayOfEntryParcel;
      b++;
    } 
    if (bool) {
      l = getRootSid();
    } else {
      l = 0L;
    } 
    try {
      return this.mBinder.personalize(arrayOfAccessControlProfileParcel, arrayOfEntryNamespaceParcel, l);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected ServiceSpecificException with code ");
      stringBuilder.append(serviceSpecificException.errorCode);
      throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
    } 
  }
  
  private static long getRootSid() {
    long l = GateKeeper.getSecureUserId();
    if (l != 0L)
      return l; 
    throw new IllegalStateException("Secure lock screen must be enabled to create credentials requiring user authentication");
  }
}
