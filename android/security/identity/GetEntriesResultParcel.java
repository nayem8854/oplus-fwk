package android.security.identity;

import android.os.Parcel;
import android.os.Parcelable;

public class GetEntriesResultParcel implements Parcelable {
  public static final Parcelable.Creator<GetEntriesResultParcel> CREATOR = new Parcelable.Creator<GetEntriesResultParcel>() {
      public GetEntriesResultParcel createFromParcel(Parcel param1Parcel) {
        GetEntriesResultParcel getEntriesResultParcel = new GetEntriesResultParcel();
        getEntriesResultParcel.readFromParcel(param1Parcel);
        return getEntriesResultParcel;
      }
      
      public GetEntriesResultParcel[] newArray(int param1Int) {
        return new GetEntriesResultParcel[param1Int];
      }
    };
  
  public byte[] deviceNameSpaces;
  
  public byte[] mac;
  
  public ResultNamespaceParcel[] resultNamespaces;
  
  public byte[] staticAuthenticationData;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeTypedArray(this.resultNamespaces, 0);
    paramParcel.writeByteArray(this.deviceNameSpaces);
    paramParcel.writeByteArray(this.mac);
    paramParcel.writeByteArray(this.staticAuthenticationData);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.resultNamespaces = paramParcel.<ResultNamespaceParcel>createTypedArray(ResultNamespaceParcel.CREATOR);
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.deviceNameSpaces = paramParcel.createByteArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.mac = paramParcel.createByteArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.staticAuthenticationData = paramParcel.createByteArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
