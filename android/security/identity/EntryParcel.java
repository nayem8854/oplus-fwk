package android.security.identity;

import android.os.Parcel;
import android.os.Parcelable;

public class EntryParcel implements Parcelable {
  public static final Parcelable.Creator<EntryParcel> CREATOR = new Parcelable.Creator<EntryParcel>() {
      public EntryParcel createFromParcel(Parcel param1Parcel) {
        EntryParcel entryParcel = new EntryParcel();
        entryParcel.readFromParcel(param1Parcel);
        return entryParcel;
      }
      
      public EntryParcel[] newArray(int param1Int) {
        return new EntryParcel[param1Int];
      }
    };
  
  public int[] accessControlProfileIds;
  
  public String name;
  
  public byte[] value;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeString(this.name);
    paramParcel.writeByteArray(this.value);
    paramParcel.writeIntArray(this.accessControlProfileIds);
    paramInt = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    paramParcel.writeInt(paramInt - i);
    paramParcel.setDataPosition(paramInt);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.name = paramParcel.readString();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.value = paramParcel.createByteArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.accessControlProfileIds = paramParcel.createIntArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
