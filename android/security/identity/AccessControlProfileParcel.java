package android.security.identity;

import android.os.Parcel;
import android.os.Parcelable;

public class AccessControlProfileParcel implements Parcelable {
  public static final Parcelable.Creator<AccessControlProfileParcel> CREATOR = new Parcelable.Creator<AccessControlProfileParcel>() {
      public AccessControlProfileParcel createFromParcel(Parcel param1Parcel) {
        AccessControlProfileParcel accessControlProfileParcel = new AccessControlProfileParcel();
        accessControlProfileParcel.readFromParcel(param1Parcel);
        return accessControlProfileParcel;
      }
      
      public AccessControlProfileParcel[] newArray(int param1Int) {
        return new AccessControlProfileParcel[param1Int];
      }
    };
  
  public int id;
  
  public byte[] readerCertificate;
  
  public boolean userAuthenticationRequired;
  
  public long userAuthenticationTimeoutMillis;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.id);
    paramParcel.writeByteArray(this.readerCertificate);
    paramParcel.writeInt(this.userAuthenticationRequired);
    paramParcel.writeLong(this.userAuthenticationTimeoutMillis);
    paramInt = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    paramParcel.writeInt(paramInt - i);
    paramParcel.setDataPosition(paramInt);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      boolean bool;
      this.id = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.readerCertificate = paramParcel.createByteArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      if (paramParcel.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.userAuthenticationRequired = bool;
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.userAuthenticationTimeoutMillis = paramParcel.readLong();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
