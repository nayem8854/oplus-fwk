package android.security.identity;

import android.os.Parcel;
import android.os.Parcelable;

public class ResultNamespaceParcel implements Parcelable {
  public static final Parcelable.Creator<ResultNamespaceParcel> CREATOR = new Parcelable.Creator<ResultNamespaceParcel>() {
      public ResultNamespaceParcel createFromParcel(Parcel param1Parcel) {
        ResultNamespaceParcel resultNamespaceParcel = new ResultNamespaceParcel();
        resultNamespaceParcel.readFromParcel(param1Parcel);
        return resultNamespaceParcel;
      }
      
      public ResultNamespaceParcel[] newArray(int param1Int) {
        return new ResultNamespaceParcel[param1Int];
      }
    };
  
  public ResultEntryParcel[] entries;
  
  public String namespaceName;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeString(this.namespaceName);
    paramParcel.writeTypedArray(this.entries, 0);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.namespaceName = paramParcel.readString();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.entries = paramParcel.<ResultEntryParcel>createTypedArray(ResultEntryParcel.CREATOR);
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
