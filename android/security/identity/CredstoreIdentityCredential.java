package android.security.identity;

import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceSpecificException;
import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

class CredstoreIdentityCredential extends IdentityCredential {
  private static final String TAG = "CredstoreIdentityCredential";
  
  private boolean mAllowUsingExhaustedKeys;
  
  private ICredential mBinder;
  
  private int mCipherSuite;
  
  private Context mContext;
  
  private String mCredentialName;
  
  private int mEphemeralCounter;
  
  private KeyPair mEphemeralKeyPair;
  
  private long mOperationHandle;
  
  private boolean mOperationHandleSet;
  
  private SecretKey mReaderSecretKey;
  
  private int mReadersExpectedEphemeralCounter;
  
  private SecretKey mSecretKey;
  
  CredstoreIdentityCredential(Context paramContext, String paramString, int paramInt, ICredential paramICredential) {
    this.mEphemeralKeyPair = null;
    this.mSecretKey = null;
    this.mReaderSecretKey = null;
    this.mAllowUsingExhaustedKeys = true;
    this.mOperationHandleSet = false;
    this.mOperationHandle = 0L;
    this.mContext = paramContext;
    this.mCredentialName = paramString;
    this.mCipherSuite = paramInt;
    this.mBinder = paramICredential;
  }
  
  private void ensureEphemeralKeyPair() {
    if (this.mEphemeralKeyPair != null)
      return; 
    try {
      byte[] arrayOfByte = this.mBinder.createEphemeralKeyPair();
      char[] arrayOfChar = new char[0];
      KeyStore keyStore = KeyStore.getInstance("PKCS12");
      ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
      this(arrayOfByte);
      keyStore.load(byteArrayInputStream, arrayOfChar);
      PrivateKey privateKey = (PrivateKey)keyStore.getKey("ephemeralKey", arrayOfChar);
      Certificate certificate = keyStore.getCertificate("ephemeralKey");
      PublicKey publicKey = certificate.getPublicKey();
      KeyPair keyPair = new KeyPair();
      this(publicKey, privateKey);
      this.mEphemeralKeyPair = keyPair;
      return;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected ServiceSpecificException with code ");
      stringBuilder.append(serviceSpecificException.errorCode);
      throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
    } catch (KeyStoreException|CertificateException|java.security.UnrecoverableKeyException|NoSuchAlgorithmException|java.io.IOException keyStoreException) {
      throw new RuntimeException("Unexpected exception ", keyStoreException);
    } 
  }
  
  public KeyPair createEphemeralKeyPair() {
    ensureEphemeralKeyPair();
    return this.mEphemeralKeyPair;
  }
  
  public void setReaderEphemeralPublicKey(PublicKey paramPublicKey) throws InvalidKeyException {
    try {
      byte[] arrayOfByte = Util.publicKeyEncodeUncompressedForm(paramPublicKey);
      this.mBinder.setReaderEphemeralPublicKey(arrayOfByte);
      ensureEphemeralKeyPair();
      try {
        KeyAgreement keyAgreement = KeyAgreement.getInstance("ECDH");
        keyAgreement.init(this.mEphemeralKeyPair.getPrivate());
        keyAgreement.doPhase(paramPublicKey, true);
        byte[] arrayOfByte3 = keyAgreement.generateSecret();
        byte[] arrayOfByte4 = new byte[1];
        byte[] arrayOfByte1 = new byte[0];
        arrayOfByte4[0] = 1;
        byte[] arrayOfByte2 = Util.computeHkdf("HmacSha256", arrayOfByte3, arrayOfByte4, arrayOfByte1, 32);
        SecretKeySpec secretKeySpec2 = new SecretKeySpec();
        this(arrayOfByte2, "AES");
        this.mSecretKey = secretKeySpec2;
        arrayOfByte4[0] = 0;
        arrayOfByte1 = Util.computeHkdf("HmacSha256", arrayOfByte3, arrayOfByte4, arrayOfByte1, 32);
        SecretKeySpec secretKeySpec1 = new SecretKeySpec();
        this(arrayOfByte1, "AES");
        this.mReaderSecretKey = secretKeySpec1;
        this.mEphemeralCounter = 1;
        this.mReadersExpectedEphemeralCounter = 1;
        return;
      } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
        throw new RuntimeException("Error performing key agreement", noSuchAlgorithmException);
      } 
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected ServiceSpecificException with code ");
      stringBuilder.append(serviceSpecificException.errorCode);
      throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
    } 
  }
  
  public byte[] encryptMessageToReader(byte[] paramArrayOfbyte) {
    try {
      ByteBuffer byteBuffer = ByteBuffer.allocate(12);
      byteBuffer.putInt(0, 0);
      byteBuffer.putInt(4, 1);
      byteBuffer.putInt(8, this.mEphemeralCounter);
      Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
      GCMParameterSpec gCMParameterSpec = new GCMParameterSpec();
      this(128, byteBuffer.array());
      cipher.init(1, this.mSecretKey, gCMParameterSpec);
      paramArrayOfbyte = cipher.doFinal(paramArrayOfbyte);
      this.mEphemeralCounter++;
      return paramArrayOfbyte;
    } catch (BadPaddingException|javax.crypto.IllegalBlockSizeException|javax.crypto.NoSuchPaddingException|InvalidKeyException|NoSuchAlgorithmException|java.security.InvalidAlgorithmParameterException badPaddingException) {
      throw new RuntimeException("Error encrypting message", badPaddingException);
    } 
  }
  
  public byte[] decryptMessageFromReader(byte[] paramArrayOfbyte) throws MessageDecryptionException {
    ByteBuffer byteBuffer = ByteBuffer.allocate(12);
    byteBuffer.putInt(0, 0);
    byteBuffer.putInt(4, 0);
    byteBuffer.putInt(8, this.mReadersExpectedEphemeralCounter);
    try {
      Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
      SecretKey secretKey = this.mReaderSecretKey;
      GCMParameterSpec gCMParameterSpec = new GCMParameterSpec();
      this(128, byteBuffer.array());
      cipher.init(2, secretKey, gCMParameterSpec);
      paramArrayOfbyte = cipher.doFinal(paramArrayOfbyte);
      this.mReadersExpectedEphemeralCounter++;
      return paramArrayOfbyte;
    } catch (BadPaddingException|javax.crypto.IllegalBlockSizeException|java.security.InvalidAlgorithmParameterException|InvalidKeyException|NoSuchAlgorithmException|javax.crypto.NoSuchPaddingException badPaddingException) {
      throw new MessageDecryptionException("Error decrypting message", badPaddingException);
    } 
  }
  
  public Collection<X509Certificate> getCredentialKeyCertificateChain() {
    try {
      byte[] arrayOfByte = this.mBinder.getCredentialKeyCertificateChain();
      ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
      this(arrayOfByte);
      try {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        Collection<? extends Certificate> collection = certificateFactory.generateCertificates(byteArrayInputStream);
        LinkedList<X509Certificate> linkedList = new LinkedList();
        this();
        for (Certificate certificate : collection)
          linkedList.add((X509Certificate)certificate); 
        return linkedList;
      } catch (CertificateException certificateException) {
        RuntimeException runtimeException = new RuntimeException();
        this("Error decoding certificates", certificateException);
        throw runtimeException;
      } 
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected ServiceSpecificException with code ");
      stringBuilder.append(serviceSpecificException.errorCode);
      throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
    } 
  }
  
  public void setAllowUsingExhaustedKeys(boolean paramBoolean) {
    this.mAllowUsingExhaustedKeys = paramBoolean;
  }
  
  public long getCredstoreOperationHandle() {
    if (!this.mOperationHandleSet)
      try {
        this.mOperationHandle = this.mBinder.selectAuthKey(this.mAllowUsingExhaustedKeys);
        this.mOperationHandleSet = true;
      } catch (RemoteException remoteException) {
        throw new RuntimeException("Unexpected RemoteException ", remoteException);
      } catch (ServiceSpecificException serviceSpecificException) {
        int i = serviceSpecificException.errorCode;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected ServiceSpecificException with code ");
        stringBuilder.append(serviceSpecificException.errorCode);
        throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
      }  
    return this.mOperationHandle;
  }
  
  public ResultData getEntries(byte[] paramArrayOfbyte1, Map<String, Collection<String>> paramMap, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3) throws SessionTranscriptMismatchException, NoAuthenticationKeyAvailableException, InvalidReaderSignatureException, EphemeralPublicKeyNotFoundException, InvalidRequestMessageException {
    RequestNamespaceParcel[] arrayOfRequestNamespaceParcel = new RequestNamespaceParcel[paramMap.size()];
    byte b;
    for (Iterator<String> iterator = paramMap.keySet().iterator(); iterator.hasNext(); ) {
      String str = iterator.next();
      Collection collection = paramMap.get(str);
      arrayOfRequestNamespaceParcel[b] = new RequestNamespaceParcel();
      (arrayOfRequestNamespaceParcel[b]).namespaceName = str;
      (arrayOfRequestNamespaceParcel[b]).entries = new RequestEntryParcel[collection.size()];
      byte b1 = 0;
      for (String str1 : collection) {
        (arrayOfRequestNamespaceParcel[b]).entries[b1] = new RequestEntryParcel();
        ((arrayOfRequestNamespaceParcel[b]).entries[b1]).name = str1;
        b1++;
      } 
      b++;
    } 
    try {
      byte[] arrayOfByte;
      ICredential iCredential = this.mBinder;
      if (paramArrayOfbyte1 == null)
        paramArrayOfbyte1 = new byte[0]; 
      if (paramArrayOfbyte2 != null) {
        arrayOfByte = paramArrayOfbyte2;
      } else {
        arrayOfByte = new byte[0];
      } 
      if (paramArrayOfbyte3 == null)
        paramArrayOfbyte3 = new byte[0]; 
      boolean bool = this.mAllowUsingExhaustedKeys;
      GetEntriesResultParcel getEntriesResultParcel2 = iCredential.getEntries(paramArrayOfbyte1, arrayOfRequestNamespaceParcel, arrayOfByte, paramArrayOfbyte3, bool);
      paramArrayOfbyte2 = getEntriesResultParcel2.mac;
      paramArrayOfbyte1 = paramArrayOfbyte2;
      if (paramArrayOfbyte2 != null) {
        paramArrayOfbyte1 = paramArrayOfbyte2;
        if (paramArrayOfbyte2.length == 0)
          paramArrayOfbyte1 = null; 
      } 
      CredstoreResultData.Builder builder = new CredstoreResultData.Builder(getEntriesResultParcel2.staticAuthenticationData, getEntriesResultParcel2.deviceNameSpaces, paramArrayOfbyte1);
      GetEntriesResultParcel getEntriesResultParcel1;
      ResultNamespaceParcel[] arrayOfResultNamespaceParcel;
      int i;
      for (arrayOfResultNamespaceParcel = getEntriesResultParcel2.resultNamespaces, i = arrayOfResultNamespaceParcel.length, b = 0, getEntriesResultParcel1 = getEntriesResultParcel2; b < i; ) {
        ResultNamespaceParcel resultNamespaceParcel = arrayOfResultNamespaceParcel[b];
        for (ResultEntryParcel resultEntryParcel : resultNamespaceParcel.entries) {
          if (resultEntryParcel.status == 0) {
            builder.addEntry(resultNamespaceParcel.namespaceName, resultEntryParcel.name, resultEntryParcel.value);
          } else {
            builder.addErrorStatus(resultNamespaceParcel.namespaceName, resultEntryParcel.name, resultEntryParcel.status);
          } 
        } 
        b++;
      } 
      return builder.build();
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      if (serviceSpecificException.errorCode != 5) {
        if (serviceSpecificException.errorCode != 7) {
          if (serviceSpecificException.errorCode != 6) {
            if (serviceSpecificException.errorCode != 10) {
              if (serviceSpecificException.errorCode == 11)
                throw new SessionTranscriptMismatchException(serviceSpecificException.getMessage(), serviceSpecificException); 
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Unexpected ServiceSpecificException with code ");
              stringBuilder.append(serviceSpecificException.errorCode);
              throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
            } 
            throw new InvalidRequestMessageException(serviceSpecificException.getMessage(), serviceSpecificException);
          } 
          throw new NoAuthenticationKeyAvailableException(serviceSpecificException.getMessage(), serviceSpecificException);
        } 
        throw new InvalidReaderSignatureException(serviceSpecificException.getMessage(), serviceSpecificException);
      } 
      throw new EphemeralPublicKeyNotFoundException(serviceSpecificException.getMessage(), serviceSpecificException);
    } 
  }
  
  public void setAvailableAuthenticationKeys(int paramInt1, int paramInt2) {
    try {
      this.mBinder.setAvailableAuthenticationKeys(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected ServiceSpecificException with code ");
      stringBuilder.append(serviceSpecificException.errorCode);
      throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
    } 
  }
  
  public Collection<X509Certificate> getAuthKeysNeedingCertification() {
    try {
      AuthKeyParcel[] arrayOfAuthKeyParcel = this.mBinder.getAuthKeysNeedingCertification();
      LinkedList<X509Certificate> linkedList = new LinkedList();
      this();
      CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
      int i;
      byte b;
      for (i = arrayOfAuthKeyParcel.length, b = 0; b < i; ) {
        AuthKeyParcel authKeyParcel = arrayOfAuthKeyParcel[b];
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
        this(authKeyParcel.x509cert);
        Collection<? extends Certificate> collection = certificateFactory.generateCertificates(byteArrayInputStream);
        if (collection.size() == 1) {
          X509Certificate x509Certificate = collection.iterator().next();
          linkedList.add(x509Certificate);
          b++;
        } 
        RuntimeException runtimeException = new RuntimeException();
        this("Returned blob yields more than one X509 cert");
        throw runtimeException;
      } 
      return linkedList;
    } catch (CertificateException certificateException) {
      throw new RuntimeException("Error decoding authenticationKey", certificateException);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected ServiceSpecificException with code ");
      stringBuilder.append(serviceSpecificException.errorCode);
      throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
    } 
  }
  
  public void storeStaticAuthenticationData(X509Certificate paramX509Certificate, byte[] paramArrayOfbyte) throws UnknownAuthenticationKeyException {
    try {
      AuthKeyParcel authKeyParcel = new AuthKeyParcel();
      this();
      authKeyParcel.x509cert = paramX509Certificate.getEncoded();
      this.mBinder.storeStaticAuthenticationData(authKeyParcel, paramArrayOfbyte);
      return;
    } catch (CertificateEncodingException certificateEncodingException) {
      throw new RuntimeException("Error encoding authenticationKey", certificateEncodingException);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      if (serviceSpecificException.errorCode == 9)
        throw new UnknownAuthenticationKeyException(serviceSpecificException.getMessage(), serviceSpecificException); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected ServiceSpecificException with code ");
      stringBuilder.append(serviceSpecificException.errorCode);
      throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
    } 
  }
  
  public int[] getAuthenticationDataUsageCount() {
    try {
      return this.mBinder.getAuthenticationDataUsageCount();
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected ServiceSpecificException with code ");
      stringBuilder.append(serviceSpecificException.errorCode);
      throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
    } 
  }
}
