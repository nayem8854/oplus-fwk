package android.security.identity;

import android.os.Parcel;
import android.os.Parcelable;

public class ResultEntryParcel implements Parcelable {
  public static final Parcelable.Creator<ResultEntryParcel> CREATOR = new Parcelable.Creator<ResultEntryParcel>() {
      public ResultEntryParcel createFromParcel(Parcel param1Parcel) {
        ResultEntryParcel resultEntryParcel = new ResultEntryParcel();
        resultEntryParcel.readFromParcel(param1Parcel);
        return resultEntryParcel;
      }
      
      public ResultEntryParcel[] newArray(int param1Int) {
        return new ResultEntryParcel[param1Int];
      }
    };
  
  public String name;
  
  public int status;
  
  public byte[] value;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.status);
    paramParcel.writeString(this.name);
    paramParcel.writeByteArray(this.value);
    paramInt = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    paramParcel.writeInt(paramInt - i);
    paramParcel.setDataPosition(paramInt);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.status = paramParcel.readInt();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.name = paramParcel.readString();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.value = paramParcel.createByteArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
