package android.security.identity;

import android.os.Parcel;
import android.os.Parcelable;

public class EntryNamespaceParcel implements Parcelable {
  public static final Parcelable.Creator<EntryNamespaceParcel> CREATOR = new Parcelable.Creator<EntryNamespaceParcel>() {
      public EntryNamespaceParcel createFromParcel(Parcel param1Parcel) {
        EntryNamespaceParcel entryNamespaceParcel = new EntryNamespaceParcel();
        entryNamespaceParcel.readFromParcel(param1Parcel);
        return entryNamespaceParcel;
      }
      
      public EntryNamespaceParcel[] newArray(int param1Int) {
        return new EntryNamespaceParcel[param1Int];
      }
    };
  
  public EntryParcel[] entries;
  
  public String namespaceName;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeString(this.namespaceName);
    paramParcel.writeTypedArray(this.entries, 0);
    paramInt = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    paramParcel.writeInt(paramInt - i);
    paramParcel.setDataPosition(paramInt);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.namespaceName = paramParcel.readString();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.entries = paramParcel.<EntryParcel>createTypedArray(EntryParcel.CREATOR);
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
