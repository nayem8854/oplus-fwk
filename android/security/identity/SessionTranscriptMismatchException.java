package android.security.identity;

public class SessionTranscriptMismatchException extends IdentityCredentialException {
  public SessionTranscriptMismatchException(String paramString) {
    super(paramString);
  }
  
  public SessionTranscriptMismatchException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
