package android.security.identity;

import android.content.Context;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class IdentityCredentialStore {
  public static final int CIPHERSUITE_ECDHE_HKDF_ECDSA_WITH_AES_256_GCM_SHA256 = 1;
  
  public abstract String[] getSupportedDocTypes();
  
  public abstract IdentityCredential getCredentialByName(String paramString, int paramInt) throws CipherSuiteNotSupportedException;
  
  public abstract byte[] deleteCredentialByName(String paramString);
  
  public abstract WritableIdentityCredential createCredential(String paramString1, String paramString2) throws AlreadyPersonalizedException, DocTypeNotSupportedException;
  
  public static IdentityCredentialStore getInstance(Context paramContext) {
    return CredstoreIdentityCredentialStore.getInstance(paramContext);
  }
  
  public static IdentityCredentialStore getDirectAccessInstance(Context paramContext) {
    return CredstoreIdentityCredentialStore.getDirectAccessInstance(paramContext);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Ciphersuite {}
}
