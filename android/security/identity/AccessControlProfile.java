package android.security.identity;

import java.security.cert.X509Certificate;

public class AccessControlProfile {
  private AccessControlProfileId mAccessControlProfileId = new AccessControlProfileId(0);
  
  private X509Certificate mReaderCertificate = null;
  
  private boolean mUserAuthenticationRequired = true;
  
  private long mUserAuthenticationTimeout = 0L;
  
  AccessControlProfileId getAccessControlProfileId() {
    return this.mAccessControlProfileId;
  }
  
  long getUserAuthenticationTimeout() {
    return this.mUserAuthenticationTimeout;
  }
  
  boolean isUserAuthenticationRequired() {
    return this.mUserAuthenticationRequired;
  }
  
  X509Certificate getReaderCertificate() {
    return this.mReaderCertificate;
  }
  
  private AccessControlProfile() {}
  
  public static final class Builder {
    private AccessControlProfile mProfile;
    
    public Builder(AccessControlProfileId param1AccessControlProfileId) {
      AccessControlProfile accessControlProfile = new AccessControlProfile();
      AccessControlProfile.access$102(accessControlProfile, param1AccessControlProfileId);
    }
    
    public Builder setUserAuthenticationRequired(boolean param1Boolean) {
      AccessControlProfile.access$202(this.mProfile, param1Boolean);
      return this;
    }
    
    public Builder setUserAuthenticationTimeout(long param1Long) {
      AccessControlProfile.access$302(this.mProfile, param1Long);
      return this;
    }
    
    public Builder setReaderCertificate(X509Certificate param1X509Certificate) {
      AccessControlProfile.access$402(this.mProfile, param1X509Certificate);
      return this;
    }
    
    public AccessControlProfile build() {
      return this.mProfile;
    }
  }
}
