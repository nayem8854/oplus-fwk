package android.security.identity;

public class NoAuthenticationKeyAvailableException extends IdentityCredentialException {
  public NoAuthenticationKeyAvailableException(String paramString) {
    super(paramString);
  }
  
  public NoAuthenticationKeyAvailableException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
