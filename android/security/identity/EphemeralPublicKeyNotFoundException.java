package android.security.identity;

public class EphemeralPublicKeyNotFoundException extends IdentityCredentialException {
  public EphemeralPublicKeyNotFoundException(String paramString) {
    super(paramString);
  }
  
  public EphemeralPublicKeyNotFoundException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
