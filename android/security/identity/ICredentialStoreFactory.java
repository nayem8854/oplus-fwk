package android.security.identity;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICredentialStoreFactory extends IInterface {
  public static final int CREDENTIAL_STORE_TYPE_DEFAULT = 0;
  
  public static final int CREDENTIAL_STORE_TYPE_DIRECT_ACCESS = 1;
  
  ICredentialStore getCredentialStore(int paramInt) throws RemoteException;
  
  class Default implements ICredentialStoreFactory {
    public ICredentialStore getCredentialStore(int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICredentialStoreFactory {
    private static final String DESCRIPTOR = "android.security.identity.ICredentialStoreFactory";
    
    static final int TRANSACTION_getCredentialStore = 1;
    
    public Stub() {
      attachInterface(this, "android.security.identity.ICredentialStoreFactory");
    }
    
    public static ICredentialStoreFactory asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.identity.ICredentialStoreFactory");
      if (iInterface != null && iInterface instanceof ICredentialStoreFactory)
        return (ICredentialStoreFactory)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getCredentialStore";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.security.identity.ICredentialStoreFactory");
        return true;
      } 
      param1Parcel1.enforceInterface("android.security.identity.ICredentialStoreFactory");
      param1Int1 = param1Parcel1.readInt();
      ICredentialStore iCredentialStore = getCredentialStore(param1Int1);
      param1Parcel2.writeNoException();
      if (iCredentialStore != null) {
        IBinder iBinder = iCredentialStore.asBinder();
      } else {
        iCredentialStore = null;
      } 
      param1Parcel2.writeStrongBinder((IBinder)iCredentialStore);
      return true;
    }
    
    private static class Proxy implements ICredentialStoreFactory {
      public static ICredentialStoreFactory sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.identity.ICredentialStoreFactory";
      }
      
      public ICredentialStore getCredentialStore(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.identity.ICredentialStoreFactory");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ICredentialStoreFactory.Stub.getDefaultImpl() != null)
            return ICredentialStoreFactory.Stub.getDefaultImpl().getCredentialStore(param2Int); 
          parcel2.readException();
          return ICredentialStore.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICredentialStoreFactory param1ICredentialStoreFactory) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICredentialStoreFactory != null) {
          Proxy.sDefaultImpl = param1ICredentialStoreFactory;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICredentialStoreFactory getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
