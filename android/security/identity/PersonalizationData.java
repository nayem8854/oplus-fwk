package android.security.identity;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class PersonalizationData {
  private LinkedList<AccessControlProfile> mProfiles = new LinkedList<>();
  
  private LinkedHashMap<String, NamespaceData> mNamespaces = new LinkedHashMap<>();
  
  Collection<AccessControlProfile> getAccessControlProfiles() {
    return Collections.unmodifiableCollection(this.mProfiles);
  }
  
  Collection<String> getNamespaces() {
    return Collections.unmodifiableCollection(this.mNamespaces.keySet());
  }
  
  NamespaceData getNamespaceData(String paramString) {
    return this.mNamespaces.get(paramString);
  }
  
  private PersonalizationData() {}
  
  static class NamespaceData {
    private LinkedHashMap<String, PersonalizationData.EntryData> mEntries = new LinkedHashMap<>();
    
    private String mNamespace;
    
    private NamespaceData(String param1String) {
      this.mNamespace = param1String;
    }
    
    String getNamespaceName() {
      return this.mNamespace;
    }
    
    Collection<String> getEntryNames() {
      return Collections.unmodifiableCollection(this.mEntries.keySet());
    }
    
    Collection<AccessControlProfileId> getAccessControlProfileIds(String param1String) {
      PersonalizationData.EntryData entryData = this.mEntries.get(param1String);
      if (entryData != null)
        return entryData.mAccessControlProfileIds; 
      return null;
    }
    
    byte[] getEntryValue(String param1String) {
      PersonalizationData.EntryData entryData = this.mEntries.get(param1String);
      if (entryData != null)
        return entryData.mValue; 
      return null;
    }
  }
  
  private static class EntryData {
    Collection<AccessControlProfileId> mAccessControlProfileIds;
    
    byte[] mValue;
    
    EntryData(byte[] param1ArrayOfbyte, Collection<AccessControlProfileId> param1Collection) {
      this.mValue = param1ArrayOfbyte;
      this.mAccessControlProfileIds = param1Collection;
    }
  }
  
  public static final class Builder {
    private PersonalizationData mData = new PersonalizationData();
    
    public Builder putEntry(String param1String1, String param1String2, Collection<AccessControlProfileId> param1Collection, byte[] param1ArrayOfbyte) {
      PersonalizationData.NamespaceData namespaceData1 = (PersonalizationData.NamespaceData)this.mData.mNamespaces.get(param1String1);
      PersonalizationData.NamespaceData namespaceData2 = namespaceData1;
      if (namespaceData1 == null) {
        namespaceData2 = new PersonalizationData.NamespaceData(param1String1);
        this.mData.mNamespaces.put(param1String1, namespaceData2);
      } 
      namespaceData2.mEntries.put(param1String2, new PersonalizationData.EntryData(param1ArrayOfbyte, param1Collection));
      return this;
    }
    
    public Builder addAccessControlProfile(AccessControlProfile param1AccessControlProfile) {
      this.mData.mProfiles.add(param1AccessControlProfile);
      return this;
    }
    
    public PersonalizationData build() {
      return this.mData;
    }
  }
}
