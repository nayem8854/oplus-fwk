package android.security.identity;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

class CredstoreResultData extends ResultData {
  byte[] mStaticAuthenticationData = null;
  
  byte[] mAuthenticatedData = null;
  
  byte[] mMessageAuthenticationCode = null;
  
  private Map<String, Map<String, EntryData>> mData = new LinkedHashMap<>();
  
  class EntryData {
    int mStatus;
    
    byte[] mValue;
    
    EntryData(CredstoreResultData this$0, int param1Int) {
      this.mValue = (byte[])this$0;
      this.mStatus = param1Int;
    }
  }
  
  public byte[] getAuthenticatedData() {
    return this.mAuthenticatedData;
  }
  
  public byte[] getMessageAuthenticationCode() {
    return this.mMessageAuthenticationCode;
  }
  
  public byte[] getStaticAuthenticationData() {
    return this.mStaticAuthenticationData;
  }
  
  public Collection<String> getNamespaces() {
    return Collections.unmodifiableCollection(this.mData.keySet());
  }
  
  public Collection<String> getEntryNames(String paramString) {
    Map map = this.mData.get(paramString);
    if (map == null)
      return null; 
    return Collections.unmodifiableCollection(map.keySet());
  }
  
  public Collection<String> getRetrievedEntryNames(String paramString) {
    Map map = this.mData.get(paramString);
    if (map == null)
      return null; 
    LinkedList<String> linkedList = new LinkedList();
    for (Map.Entry entry : map.entrySet()) {
      if (((EntryData)entry.getValue()).mStatus == 0)
        linkedList.add((String)entry.getKey()); 
    } 
    return linkedList;
  }
  
  private EntryData getEntryData(String paramString1, String paramString2) {
    Map map = this.mData.get(paramString1);
    if (map == null)
      return null; 
    return (EntryData)map.get(paramString2);
  }
  
  public int getStatus(String paramString1, String paramString2) {
    EntryData entryData = getEntryData(paramString1, paramString2);
    if (entryData == null)
      return 2; 
    return entryData.mStatus;
  }
  
  public byte[] getEntry(String paramString1, String paramString2) {
    EntryData entryData = getEntryData(paramString1, paramString2);
    if (entryData == null)
      return null; 
    return entryData.mValue;
  }
  
  class Builder {
    private CredstoreResultData mResultData;
    
    Builder(CredstoreResultData this$0, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      CredstoreResultData credstoreResultData = new CredstoreResultData();
      credstoreResultData.mStaticAuthenticationData = (byte[])this$0;
      this.mResultData.mAuthenticatedData = param1ArrayOfbyte1;
      this.mResultData.mMessageAuthenticationCode = param1ArrayOfbyte2;
    }
    
    private Map<String, CredstoreResultData.EntryData> getOrCreateInnerMap(String param1String) {
      Map<Object, Object> map1 = (Map)this.mResultData.mData.get(param1String);
      Map<Object, Object> map2 = map1;
      if (map1 == null) {
        map2 = new LinkedHashMap<>();
        this.mResultData.mData.put(param1String, map2);
      } 
      return (Map)map2;
    }
    
    Builder addEntry(String param1String1, String param1String2, byte[] param1ArrayOfbyte) {
      Map<String, CredstoreResultData.EntryData> map = getOrCreateInnerMap(param1String1);
      map.put(param1String2, new CredstoreResultData.EntryData(param1ArrayOfbyte, 0));
      return this;
    }
    
    Builder addErrorStatus(String param1String1, String param1String2, int param1Int) {
      Map<String, CredstoreResultData.EntryData> map = getOrCreateInnerMap(param1String1);
      map.put(param1String2, new CredstoreResultData.EntryData(null, param1Int));
      return this;
    }
    
    CredstoreResultData build() {
      return this.mResultData;
    }
  }
}
