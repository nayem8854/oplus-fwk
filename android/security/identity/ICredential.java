package android.security.identity;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICredential extends IInterface {
  public static final int STATUS_NOT_IN_REQUEST_MESSAGE = 3;
  
  public static final int STATUS_NOT_REQUESTED = 2;
  
  public static final int STATUS_NO_ACCESS_CONTROL_PROFILES = 6;
  
  public static final int STATUS_NO_SUCH_ENTRY = 1;
  
  public static final int STATUS_OK = 0;
  
  public static final int STATUS_READER_AUTHENTICATION_FAILED = 5;
  
  public static final int STATUS_USER_AUTHENTICATION_FAILED = 4;
  
  byte[] createEphemeralKeyPair() throws RemoteException;
  
  byte[] deleteCredential() throws RemoteException;
  
  AuthKeyParcel[] getAuthKeysNeedingCertification() throws RemoteException;
  
  int[] getAuthenticationDataUsageCount() throws RemoteException;
  
  byte[] getCredentialKeyCertificateChain() throws RemoteException;
  
  GetEntriesResultParcel getEntries(byte[] paramArrayOfbyte1, RequestNamespaceParcel[] paramArrayOfRequestNamespaceParcel, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3, boolean paramBoolean) throws RemoteException;
  
  long selectAuthKey(boolean paramBoolean) throws RemoteException;
  
  void setAvailableAuthenticationKeys(int paramInt1, int paramInt2) throws RemoteException;
  
  void setReaderEphemeralPublicKey(byte[] paramArrayOfbyte) throws RemoteException;
  
  void storeStaticAuthenticationData(AuthKeyParcel paramAuthKeyParcel, byte[] paramArrayOfbyte) throws RemoteException;
  
  class Default implements ICredential {
    public byte[] createEphemeralKeyPair() throws RemoteException {
      return null;
    }
    
    public void setReaderEphemeralPublicKey(byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public byte[] deleteCredential() throws RemoteException {
      return null;
    }
    
    public byte[] getCredentialKeyCertificateChain() throws RemoteException {
      return null;
    }
    
    public long selectAuthKey(boolean param1Boolean) throws RemoteException {
      return 0L;
    }
    
    public GetEntriesResultParcel getEntries(byte[] param1ArrayOfbyte1, RequestNamespaceParcel[] param1ArrayOfRequestNamespaceParcel, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public void setAvailableAuthenticationKeys(int param1Int1, int param1Int2) throws RemoteException {}
    
    public AuthKeyParcel[] getAuthKeysNeedingCertification() throws RemoteException {
      return null;
    }
    
    public void storeStaticAuthenticationData(AuthKeyParcel param1AuthKeyParcel, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public int[] getAuthenticationDataUsageCount() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICredential {
    private static final String DESCRIPTOR = "android.security.identity.ICredential";
    
    static final int TRANSACTION_createEphemeralKeyPair = 1;
    
    static final int TRANSACTION_deleteCredential = 3;
    
    static final int TRANSACTION_getAuthKeysNeedingCertification = 8;
    
    static final int TRANSACTION_getAuthenticationDataUsageCount = 10;
    
    static final int TRANSACTION_getCredentialKeyCertificateChain = 4;
    
    static final int TRANSACTION_getEntries = 6;
    
    static final int TRANSACTION_selectAuthKey = 5;
    
    static final int TRANSACTION_setAvailableAuthenticationKeys = 7;
    
    static final int TRANSACTION_setReaderEphemeralPublicKey = 2;
    
    static final int TRANSACTION_storeStaticAuthenticationData = 9;
    
    public Stub() {
      attachInterface(this, "android.security.identity.ICredential");
    }
    
    public static ICredential asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.identity.ICredential");
      if (iInterface != null && iInterface instanceof ICredential)
        return (ICredential)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 10:
          return "getAuthenticationDataUsageCount";
        case 9:
          return "storeStaticAuthenticationData";
        case 8:
          return "getAuthKeysNeedingCertification";
        case 7:
          return "setAvailableAuthenticationKeys";
        case 6:
          return "getEntries";
        case 5:
          return "selectAuthKey";
        case 4:
          return "getCredentialKeyCertificateChain";
        case 3:
          return "deleteCredential";
        case 2:
          return "setReaderEphemeralPublicKey";
        case 1:
          break;
      } 
      return "createEphemeralKeyPair";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        int[] arrayOfInt;
        byte[] arrayOfByte2;
        AuthKeyParcel[] arrayOfAuthKeyParcel;
        GetEntriesResultParcel getEntriesResultParcel;
        AuthKeyParcel authKeyParcel;
        byte[] arrayOfByte3;
        RequestNamespaceParcel[] arrayOfRequestNamespaceParcel;
        byte[] arrayOfByte4, arrayOfByte5;
        long l;
        boolean bool = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 10:
            param1Parcel1.enforceInterface("android.security.identity.ICredential");
            arrayOfInt = getAuthenticationDataUsageCount();
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt);
            return true;
          case 9:
            arrayOfInt.enforceInterface("android.security.identity.ICredential");
            if (arrayOfInt.readInt() != 0) {
              authKeyParcel = AuthKeyParcel.CREATOR.createFromParcel((Parcel)arrayOfInt);
            } else {
              authKeyParcel = null;
            } 
            arrayOfByte2 = arrayOfInt.createByteArray();
            storeStaticAuthenticationData(authKeyParcel, arrayOfByte2);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            arrayOfByte2.enforceInterface("android.security.identity.ICredential");
            arrayOfAuthKeyParcel = getAuthKeysNeedingCertification();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray(arrayOfAuthKeyParcel, 1);
            return true;
          case 7:
            arrayOfAuthKeyParcel.enforceInterface("android.security.identity.ICredential");
            param1Int2 = arrayOfAuthKeyParcel.readInt();
            param1Int1 = arrayOfAuthKeyParcel.readInt();
            setAvailableAuthenticationKeys(param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            arrayOfAuthKeyParcel.enforceInterface("android.security.identity.ICredential");
            arrayOfByte3 = arrayOfAuthKeyParcel.createByteArray();
            arrayOfRequestNamespaceParcel = arrayOfAuthKeyParcel.<RequestNamespaceParcel>createTypedArray(RequestNamespaceParcel.CREATOR);
            arrayOfByte4 = arrayOfAuthKeyParcel.createByteArray();
            arrayOfByte5 = arrayOfAuthKeyParcel.createByteArray();
            if (arrayOfAuthKeyParcel.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            getEntriesResultParcel = getEntries(arrayOfByte3, arrayOfRequestNamespaceParcel, arrayOfByte4, arrayOfByte5, bool);
            param1Parcel2.writeNoException();
            if (getEntriesResultParcel != null) {
              param1Parcel2.writeInt(1);
              getEntriesResultParcel.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 5:
            getEntriesResultParcel.enforceInterface("android.security.identity.ICredential");
            if (getEntriesResultParcel.readInt() != 0)
              bool = true; 
            l = selectAuthKey(bool);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 4:
            getEntriesResultParcel.enforceInterface("android.security.identity.ICredential");
            arrayOfByte1 = getCredentialKeyCertificateChain();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte1);
            return true;
          case 3:
            arrayOfByte1.enforceInterface("android.security.identity.ICredential");
            arrayOfByte1 = deleteCredential();
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte1);
            return true;
          case 2:
            arrayOfByte1.enforceInterface("android.security.identity.ICredential");
            arrayOfByte1 = arrayOfByte1.createByteArray();
            setReaderEphemeralPublicKey(arrayOfByte1);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfByte1.enforceInterface("android.security.identity.ICredential");
        byte[] arrayOfByte1 = createEphemeralKeyPair();
        param1Parcel2.writeNoException();
        param1Parcel2.writeByteArray(arrayOfByte1);
        return true;
      } 
      param1Parcel2.writeString("android.security.identity.ICredential");
      return true;
    }
    
    private static class Proxy implements ICredential {
      public static ICredential sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.identity.ICredential";
      }
      
      public byte[] createEphemeralKeyPair() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.identity.ICredential");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ICredential.Stub.getDefaultImpl() != null)
            return ICredential.Stub.getDefaultImpl().createEphemeralKeyPair(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setReaderEphemeralPublicKey(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.identity.ICredential");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ICredential.Stub.getDefaultImpl() != null) {
            ICredential.Stub.getDefaultImpl().setReaderEphemeralPublicKey(param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] deleteCredential() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.identity.ICredential");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ICredential.Stub.getDefaultImpl() != null)
            return ICredential.Stub.getDefaultImpl().deleteCredential(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getCredentialKeyCertificateChain() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.identity.ICredential");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ICredential.Stub.getDefaultImpl() != null)
            return ICredential.Stub.getDefaultImpl().getCredentialKeyCertificateChain(); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long selectAuthKey(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.security.identity.ICredential");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool1 && ICredential.Stub.getDefaultImpl() != null)
            return ICredential.Stub.getDefaultImpl().selectAuthKey(param2Boolean); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public GetEntriesResultParcel getEntries(byte[] param2ArrayOfbyte1, RequestNamespaceParcel[] param2ArrayOfRequestNamespaceParcel, byte[] param2ArrayOfbyte2, byte[] param2ArrayOfbyte3, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.security.identity.ICredential");
          parcel1.writeByteArray(param2ArrayOfbyte1);
          parcel1.writeTypedArray(param2ArrayOfRequestNamespaceParcel, 0);
          parcel1.writeByteArray(param2ArrayOfbyte2);
          parcel1.writeByteArray(param2ArrayOfbyte3);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool1 && ICredential.Stub.getDefaultImpl() != null)
            return ICredential.Stub.getDefaultImpl().getEntries(param2ArrayOfbyte1, param2ArrayOfRequestNamespaceParcel, param2ArrayOfbyte2, param2ArrayOfbyte3, param2Boolean); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            GetEntriesResultParcel getEntriesResultParcel = GetEntriesResultParcel.CREATOR.createFromParcel(parcel2);
          } else {
            param2ArrayOfbyte1 = null;
          } 
          return (GetEntriesResultParcel)param2ArrayOfbyte1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAvailableAuthenticationKeys(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.identity.ICredential");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ICredential.Stub.getDefaultImpl() != null) {
            ICredential.Stub.getDefaultImpl().setAvailableAuthenticationKeys(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public AuthKeyParcel[] getAuthKeysNeedingCertification() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.identity.ICredential");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ICredential.Stub.getDefaultImpl() != null)
            return ICredential.Stub.getDefaultImpl().getAuthKeysNeedingCertification(); 
          parcel2.readException();
          return parcel2.<AuthKeyParcel>createTypedArray(AuthKeyParcel.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void storeStaticAuthenticationData(AuthKeyParcel param2AuthKeyParcel, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.identity.ICredential");
          if (param2AuthKeyParcel != null) {
            parcel1.writeInt(1);
            param2AuthKeyParcel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ICredential.Stub.getDefaultImpl() != null) {
            ICredential.Stub.getDefaultImpl().storeStaticAuthenticationData(param2AuthKeyParcel, param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getAuthenticationDataUsageCount() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.identity.ICredential");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && ICredential.Stub.getDefaultImpl() != null)
            return ICredential.Stub.getDefaultImpl().getAuthenticationDataUsageCount(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICredential param1ICredential) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICredential != null) {
          Proxy.sDefaultImpl = param1ICredential;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICredential getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
