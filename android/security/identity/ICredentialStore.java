package android.security.identity;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICredentialStore extends IInterface {
  public static final int ERROR_ALREADY_PERSONALIZED = 2;
  
  public static final int ERROR_AUTHENTICATION_KEY_NOT_FOUND = 9;
  
  public static final int ERROR_CIPHER_SUITE_NOT_SUPPORTED = 4;
  
  public static final int ERROR_DOCUMENT_TYPE_NOT_SUPPORTED = 8;
  
  public static final int ERROR_EPHEMERAL_PUBLIC_KEY_NOT_FOUND = 5;
  
  public static final int ERROR_GENERIC = 1;
  
  public static final int ERROR_INVALID_ITEMS_REQUEST_MESSAGE = 10;
  
  public static final int ERROR_INVALID_READER_SIGNATURE = 7;
  
  public static final int ERROR_NONE = 0;
  
  public static final int ERROR_NO_AUTHENTICATION_KEY_AVAILABLE = 6;
  
  public static final int ERROR_NO_SUCH_CREDENTIAL = 3;
  
  public static final int ERROR_SESSION_TRANSCRIPT_MISMATCH = 11;
  
  IWritableCredential createCredential(String paramString1, String paramString2) throws RemoteException;
  
  ICredential getCredentialByName(String paramString, int paramInt) throws RemoteException;
  
  SecurityHardwareInfoParcel getSecurityHardwareInfo() throws RemoteException;
  
  class Default implements ICredentialStore {
    public SecurityHardwareInfoParcel getSecurityHardwareInfo() throws RemoteException {
      return null;
    }
    
    public IWritableCredential createCredential(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public ICredential getCredentialByName(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICredentialStore {
    private static final String DESCRIPTOR = "android.security.identity.ICredentialStore";
    
    static final int TRANSACTION_createCredential = 2;
    
    static final int TRANSACTION_getCredentialByName = 3;
    
    static final int TRANSACTION_getSecurityHardwareInfo = 1;
    
    public Stub() {
      attachInterface(this, "android.security.identity.ICredentialStore");
    }
    
    public static ICredentialStore asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.identity.ICredentialStore");
      if (iInterface != null && iInterface instanceof ICredentialStore)
        return (ICredentialStore)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "getCredentialByName";
        } 
        return "createCredential";
      } 
      return "getSecurityHardwareInfo";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder;
      if (param1Int1 != 1) {
        IBinder iBinder1;
        ICredential iCredential2;
        String str2 = null;
        Parcel parcel = null;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.security.identity.ICredentialStore");
            return true;
          } 
          param1Parcel1.enforceInterface("android.security.identity.ICredentialStore");
          str2 = param1Parcel1.readString();
          param1Int1 = param1Parcel1.readInt();
          iCredential2 = getCredentialByName(str2, param1Int1);
          param1Parcel2.writeNoException();
          param1Parcel1 = parcel;
          if (iCredential2 != null)
            iBinder1 = iCredential2.asBinder(); 
          param1Parcel2.writeStrongBinder(iBinder1);
          return true;
        } 
        iBinder1.enforceInterface("android.security.identity.ICredentialStore");
        String str3 = iBinder1.readString();
        String str1 = iBinder1.readString();
        IWritableCredential iWritableCredential = createCredential(str3, str1);
        param1Parcel2.writeNoException();
        ICredential iCredential1 = iCredential2;
        if (iWritableCredential != null)
          iBinder = iWritableCredential.asBinder(); 
        param1Parcel2.writeStrongBinder(iBinder);
        return true;
      } 
      iBinder.enforceInterface("android.security.identity.ICredentialStore");
      SecurityHardwareInfoParcel securityHardwareInfoParcel = getSecurityHardwareInfo();
      param1Parcel2.writeNoException();
      if (securityHardwareInfoParcel != null) {
        param1Parcel2.writeInt(1);
        securityHardwareInfoParcel.writeToParcel(param1Parcel2, 1);
      } else {
        param1Parcel2.writeInt(0);
      } 
      return true;
    }
    
    private static class Proxy implements ICredentialStore {
      public static ICredentialStore sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.identity.ICredentialStore";
      }
      
      public SecurityHardwareInfoParcel getSecurityHardwareInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          SecurityHardwareInfoParcel securityHardwareInfoParcel;
          parcel1.writeInterfaceToken("android.security.identity.ICredentialStore");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ICredentialStore.Stub.getDefaultImpl() != null) {
            securityHardwareInfoParcel = ICredentialStore.Stub.getDefaultImpl().getSecurityHardwareInfo();
            return securityHardwareInfoParcel;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            securityHardwareInfoParcel = SecurityHardwareInfoParcel.CREATOR.createFromParcel(parcel2);
          } else {
            securityHardwareInfoParcel = null;
          } 
          return securityHardwareInfoParcel;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IWritableCredential createCredential(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.identity.ICredentialStore");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ICredentialStore.Stub.getDefaultImpl() != null)
            return ICredentialStore.Stub.getDefaultImpl().createCredential(param2String1, param2String2); 
          parcel2.readException();
          return IWritableCredential.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ICredential getCredentialByName(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.identity.ICredentialStore");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ICredentialStore.Stub.getDefaultImpl() != null)
            return ICredentialStore.Stub.getDefaultImpl().getCredentialByName(param2String, param2Int); 
          parcel2.readException();
          return ICredential.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICredentialStore param1ICredentialStore) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICredentialStore != null) {
          Proxy.sDefaultImpl = param1ICredentialStore;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICredentialStore getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
