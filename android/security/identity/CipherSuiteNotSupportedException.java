package android.security.identity;

public class CipherSuiteNotSupportedException extends IdentityCredentialException {
  public CipherSuiteNotSupportedException(String paramString) {
    super(paramString);
  }
  
  public CipherSuiteNotSupportedException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
