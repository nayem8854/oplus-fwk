package android.security.identity;

import android.os.Parcel;
import android.os.Parcelable;

public class SecurityHardwareInfoParcel implements Parcelable {
  public static final Parcelable.Creator<SecurityHardwareInfoParcel> CREATOR = new Parcelable.Creator<SecurityHardwareInfoParcel>() {
      public SecurityHardwareInfoParcel createFromParcel(Parcel param1Parcel) {
        SecurityHardwareInfoParcel securityHardwareInfoParcel = new SecurityHardwareInfoParcel();
        securityHardwareInfoParcel.readFromParcel(param1Parcel);
        return securityHardwareInfoParcel;
      }
      
      public SecurityHardwareInfoParcel[] newArray(int param1Int) {
        return new SecurityHardwareInfoParcel[param1Int];
      }
    };
  
  public boolean directAccess;
  
  public String[] supportedDocTypes;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeInt(this.directAccess);
    paramParcel.writeStringArray(this.supportedDocTypes);
    paramInt = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    paramParcel.writeInt(paramInt - i);
    paramParcel.setDataPosition(paramInt);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      boolean bool;
      if (paramParcel.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.directAccess = bool;
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      this.supportedDocTypes = paramParcel.createStringArray();
      k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
