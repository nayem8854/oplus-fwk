package android.security.identity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECPoint;
import java.util.Collection;
import java.util.Iterator;

class Util {
  private static final String TAG = "Util";
  
  static int[] integerCollectionToArray(Collection<Integer> paramCollection) {
    int[] arrayOfInt = new int[paramCollection.size()];
    byte b = 0;
    for (Iterator<Integer> iterator = paramCollection.iterator(); iterator.hasNext(); ) {
      int i = ((Integer)iterator.next()).intValue();
      arrayOfInt[b] = i;
      b++;
    } 
    return arrayOfInt;
  }
  
  static byte[] stripLeadingZeroes(byte[] paramArrayOfbyte) {
    byte b = 0;
    while (b < paramArrayOfbyte.length && paramArrayOfbyte[b] == 0)
      b++; 
    int i = paramArrayOfbyte.length;
    byte[] arrayOfByte = new byte[i - b];
    i = 0;
    while (b < paramArrayOfbyte.length) {
      arrayOfByte[i] = paramArrayOfbyte[b];
      i++;
      b++;
    } 
    return arrayOfByte;
  }
  
  static byte[] publicKeyEncodeUncompressedForm(PublicKey paramPublicKey) {
    ECPoint eCPoint = ((ECPublicKey)paramPublicKey).getW();
    byte[] arrayOfByte1 = stripLeadingZeroes(eCPoint.getAffineX().toByteArray());
    byte[] arrayOfByte2 = stripLeadingZeroes(eCPoint.getAffineY().toByteArray());
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      this();
      byteArrayOutputStream.write(4);
      byteArrayOutputStream.write(arrayOfByte1);
      byteArrayOutputStream.write(arrayOfByte2);
      return byteArrayOutputStream.toByteArray();
    } catch (IOException iOException) {
      throw new RuntimeException("Unexpected IOException", iOException);
    } 
  }
  
  static byte[] computeHkdf(String paramString, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic getInstance : (Ljava/lang/String;)Ljavax/crypto/Mac;
    //   4: astore #5
    //   6: iload #4
    //   8: aload #5
    //   10: invokevirtual getMacLength : ()I
    //   13: sipush #255
    //   16: imul
    //   17: if_icmpgt -> 203
    //   20: aload_2
    //   21: ifnull -> 54
    //   24: aload_2
    //   25: arraylength
    //   26: ifne -> 32
    //   29: goto -> 54
    //   32: new javax/crypto/spec/SecretKeySpec
    //   35: astore #6
    //   37: aload #6
    //   39: aload_2
    //   40: aload_0
    //   41: invokespecial <init> : ([BLjava/lang/String;)V
    //   44: aload #5
    //   46: aload #6
    //   48: invokevirtual init : (Ljava/security/Key;)V
    //   51: goto -> 76
    //   54: new javax/crypto/spec/SecretKeySpec
    //   57: astore_2
    //   58: aload_2
    //   59: aload #5
    //   61: invokevirtual getMacLength : ()I
    //   64: newarray byte
    //   66: aload_0
    //   67: invokespecial <init> : ([BLjava/lang/String;)V
    //   70: aload #5
    //   72: aload_2
    //   73: invokevirtual init : (Ljava/security/Key;)V
    //   76: aload #5
    //   78: aload_1
    //   79: invokevirtual doFinal : ([B)[B
    //   82: astore_2
    //   83: iload #4
    //   85: newarray byte
    //   87: astore_1
    //   88: iconst_1
    //   89: istore #7
    //   91: iconst_0
    //   92: istore #8
    //   94: new javax/crypto/spec/SecretKeySpec
    //   97: astore #6
    //   99: aload #6
    //   101: aload_2
    //   102: aload_0
    //   103: invokespecial <init> : ([BLjava/lang/String;)V
    //   106: aload #5
    //   108: aload #6
    //   110: invokevirtual init : (Ljava/security/Key;)V
    //   113: iconst_0
    //   114: newarray byte
    //   116: astore_0
    //   117: aload #5
    //   119: aload_0
    //   120: invokevirtual update : ([B)V
    //   123: aload #5
    //   125: aload_3
    //   126: invokevirtual update : ([B)V
    //   129: aload #5
    //   131: iload #7
    //   133: i2b
    //   134: invokevirtual update : (B)V
    //   137: aload #5
    //   139: invokevirtual doFinal : ()[B
    //   142: astore_0
    //   143: aload_0
    //   144: arraylength
    //   145: iload #8
    //   147: iadd
    //   148: iload #4
    //   150: if_icmpge -> 176
    //   153: aload_0
    //   154: iconst_0
    //   155: aload_1
    //   156: iload #8
    //   158: aload_0
    //   159: arraylength
    //   160: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   163: iload #8
    //   165: aload_0
    //   166: arraylength
    //   167: iadd
    //   168: istore #8
    //   170: iinc #7, 1
    //   173: goto -> 117
    //   176: aload_0
    //   177: iconst_0
    //   178: aload_1
    //   179: iload #8
    //   181: iload #4
    //   183: iload #8
    //   185: isub
    //   186: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   189: aload_1
    //   190: areturn
    //   191: astore_0
    //   192: new java/lang/RuntimeException
    //   195: dup
    //   196: ldc 'Error MACing'
    //   198: aload_0
    //   199: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   202: athrow
    //   203: new java/lang/RuntimeException
    //   206: dup
    //   207: ldc 'size too large'
    //   209: invokespecial <init> : (Ljava/lang/String;)V
    //   212: athrow
    //   213: astore_1
    //   214: new java/lang/StringBuilder
    //   217: dup
    //   218: invokespecial <init> : ()V
    //   221: astore_2
    //   222: aload_2
    //   223: ldc 'No such algorithm: '
    //   225: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   228: pop
    //   229: aload_2
    //   230: aload_0
    //   231: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   234: pop
    //   235: new java/lang/RuntimeException
    //   238: dup
    //   239: aload_2
    //   240: invokevirtual toString : ()Ljava/lang/String;
    //   243: aload_1
    //   244: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   247: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #96	-> 0
    //   #98	-> 0
    //   #101	-> 6
    //   #102	-> 6
    //   #106	-> 20
    //   #112	-> 32
    //   #110	-> 54
    //   #114	-> 76
    //   #115	-> 83
    //   #116	-> 88
    //   #117	-> 91
    //   #118	-> 94
    //   #119	-> 113
    //   #121	-> 117
    //   #122	-> 123
    //   #123	-> 129
    //   #124	-> 137
    //   #125	-> 143
    //   #126	-> 153
    //   #127	-> 163
    //   #128	-> 170
    //   #130	-> 176
    //   #131	-> 189
    //   #134	-> 189
    //   #135	-> 191
    //   #136	-> 192
    //   #103	-> 203
    //   #99	-> 213
    //   #100	-> 214
    // Exception table:
    //   from	to	target	type
    //   0	6	213	java/security/NoSuchAlgorithmException
    //   24	29	191	java/security/InvalidKeyException
    //   32	51	191	java/security/InvalidKeyException
    //   54	76	191	java/security/InvalidKeyException
    //   76	83	191	java/security/InvalidKeyException
    //   83	88	191	java/security/InvalidKeyException
    //   94	113	191	java/security/InvalidKeyException
    //   113	117	191	java/security/InvalidKeyException
    //   117	123	191	java/security/InvalidKeyException
    //   123	129	191	java/security/InvalidKeyException
    //   129	137	191	java/security/InvalidKeyException
    //   137	143	191	java/security/InvalidKeyException
    //   143	153	191	java/security/InvalidKeyException
    //   153	163	191	java/security/InvalidKeyException
    //   163	170	191	java/security/InvalidKeyException
    //   176	189	191	java/security/InvalidKeyException
  }
}
