package android.security.identity;

public class IdentityCredentialException extends Exception {
  public IdentityCredentialException(String paramString) {
    super(paramString);
  }
  
  public IdentityCredentialException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
