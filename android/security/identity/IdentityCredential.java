package android.security.identity;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Map;

public abstract class IdentityCredential {
  public abstract KeyPair createEphemeralKeyPair();
  
  public abstract byte[] decryptMessageFromReader(byte[] paramArrayOfbyte) throws MessageDecryptionException;
  
  public abstract byte[] encryptMessageToReader(byte[] paramArrayOfbyte);
  
  public abstract Collection<X509Certificate> getAuthKeysNeedingCertification();
  
  public abstract int[] getAuthenticationDataUsageCount();
  
  public abstract Collection<X509Certificate> getCredentialKeyCertificateChain();
  
  public abstract long getCredstoreOperationHandle();
  
  public abstract ResultData getEntries(byte[] paramArrayOfbyte1, Map<String, Collection<String>> paramMap, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3) throws SessionTranscriptMismatchException, NoAuthenticationKeyAvailableException, InvalidReaderSignatureException, EphemeralPublicKeyNotFoundException, InvalidRequestMessageException;
  
  public abstract void setAllowUsingExhaustedKeys(boolean paramBoolean);
  
  public abstract void setAvailableAuthenticationKeys(int paramInt1, int paramInt2);
  
  public abstract void setReaderEphemeralPublicKey(PublicKey paramPublicKey) throws InvalidKeyException;
  
  public abstract void storeStaticAuthenticationData(X509Certificate paramX509Certificate, byte[] paramArrayOfbyte) throws UnknownAuthenticationKeyException;
}
