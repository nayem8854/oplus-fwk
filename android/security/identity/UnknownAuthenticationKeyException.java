package android.security.identity;

public class UnknownAuthenticationKeyException extends IdentityCredentialException {
  public UnknownAuthenticationKeyException(String paramString) {
    super(paramString);
  }
  
  public UnknownAuthenticationKeyException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
