package android.security.identity;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IWritableCredential extends IInterface {
  byte[] getCredentialKeyCertificateChain(byte[] paramArrayOfbyte) throws RemoteException;
  
  byte[] personalize(AccessControlProfileParcel[] paramArrayOfAccessControlProfileParcel, EntryNamespaceParcel[] paramArrayOfEntryNamespaceParcel, long paramLong) throws RemoteException;
  
  class Default implements IWritableCredential {
    public byte[] getCredentialKeyCertificateChain(byte[] param1ArrayOfbyte) throws RemoteException {
      return null;
    }
    
    public byte[] personalize(AccessControlProfileParcel[] param1ArrayOfAccessControlProfileParcel, EntryNamespaceParcel[] param1ArrayOfEntryNamespaceParcel, long param1Long) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IWritableCredential {
    private static final String DESCRIPTOR = "android.security.identity.IWritableCredential";
    
    static final int TRANSACTION_getCredentialKeyCertificateChain = 1;
    
    static final int TRANSACTION_personalize = 2;
    
    public Stub() {
      attachInterface(this, "android.security.identity.IWritableCredential");
    }
    
    public static IWritableCredential asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.identity.IWritableCredential");
      if (iInterface != null && iInterface instanceof IWritableCredential)
        return (IWritableCredential)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "personalize";
      } 
      return "getCredentialKeyCertificateChain";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.security.identity.IWritableCredential");
          return true;
        } 
        param1Parcel1.enforceInterface("android.security.identity.IWritableCredential");
        AccessControlProfileParcel[] arrayOfAccessControlProfileParcel = param1Parcel1.<AccessControlProfileParcel>createTypedArray(AccessControlProfileParcel.CREATOR);
        EntryNamespaceParcel[] arrayOfEntryNamespaceParcel = param1Parcel1.<EntryNamespaceParcel>createTypedArray(EntryNamespaceParcel.CREATOR);
        long l = param1Parcel1.readLong();
        arrayOfByte = personalize(arrayOfAccessControlProfileParcel, arrayOfEntryNamespaceParcel, l);
        param1Parcel2.writeNoException();
        param1Parcel2.writeByteArray(arrayOfByte);
        return true;
      } 
      arrayOfByte.enforceInterface("android.security.identity.IWritableCredential");
      byte[] arrayOfByte = arrayOfByte.createByteArray();
      arrayOfByte = getCredentialKeyCertificateChain(arrayOfByte);
      param1Parcel2.writeNoException();
      param1Parcel2.writeByteArray(arrayOfByte);
      return true;
    }
    
    private static class Proxy implements IWritableCredential {
      public static IWritableCredential sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.identity.IWritableCredential";
      }
      
      public byte[] getCredentialKeyCertificateChain(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.identity.IWritableCredential");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IWritableCredential.Stub.getDefaultImpl() != null) {
            param2ArrayOfbyte = IWritableCredential.Stub.getDefaultImpl().getCredentialKeyCertificateChain(param2ArrayOfbyte);
            return param2ArrayOfbyte;
          } 
          parcel2.readException();
          param2ArrayOfbyte = parcel2.createByteArray();
          return param2ArrayOfbyte;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] personalize(AccessControlProfileParcel[] param2ArrayOfAccessControlProfileParcel, EntryNamespaceParcel[] param2ArrayOfEntryNamespaceParcel, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.identity.IWritableCredential");
          parcel1.writeTypedArray(param2ArrayOfAccessControlProfileParcel, 0);
          parcel1.writeTypedArray(param2ArrayOfEntryNamespaceParcel, 0);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IWritableCredential.Stub.getDefaultImpl() != null)
            return IWritableCredential.Stub.getDefaultImpl().personalize(param2ArrayOfAccessControlProfileParcel, param2ArrayOfEntryNamespaceParcel, param2Long); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IWritableCredential param1IWritableCredential) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IWritableCredential != null) {
          Proxy.sDefaultImpl = param1IWritableCredential;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IWritableCredential getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
