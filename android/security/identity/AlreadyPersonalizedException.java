package android.security.identity;

public class AlreadyPersonalizedException extends IdentityCredentialException {
  public AlreadyPersonalizedException(String paramString) {
    super(paramString);
  }
  
  public AlreadyPersonalizedException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
