package android.security.identity;

public class MessageDecryptionException extends IdentityCredentialException {
  public MessageDecryptionException(String paramString) {
    super(paramString);
  }
  
  public MessageDecryptionException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
