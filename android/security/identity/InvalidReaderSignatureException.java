package android.security.identity;

public class InvalidReaderSignatureException extends IdentityCredentialException {
  public InvalidReaderSignatureException(String paramString) {
    super(paramString);
  }
  
  public InvalidReaderSignatureException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
