package android.security.identity;

public class InvalidRequestMessageException extends IdentityCredentialException {
  public InvalidRequestMessageException(String paramString) {
    super(paramString);
  }
  
  public InvalidRequestMessageException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
