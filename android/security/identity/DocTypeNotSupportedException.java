package android.security.identity;

public class DocTypeNotSupportedException extends IdentityCredentialException {
  public DocTypeNotSupportedException(String paramString) {
    super(paramString);
  }
  
  public DocTypeNotSupportedException(String paramString, Throwable paramThrowable) {
    super(paramString, paramThrowable);
  }
}
