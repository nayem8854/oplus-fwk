package android.security.identity;

import android.os.Parcel;
import android.os.Parcelable;

public class RequestEntryParcel implements Parcelable {
  public static final Parcelable.Creator<RequestEntryParcel> CREATOR = new Parcelable.Creator<RequestEntryParcel>() {
      public RequestEntryParcel createFromParcel(Parcel param1Parcel) {
        RequestEntryParcel requestEntryParcel = new RequestEntryParcel();
        requestEntryParcel.readFromParcel(param1Parcel);
        return requestEntryParcel;
      }
      
      public RequestEntryParcel[] newArray(int param1Int) {
        return new RequestEntryParcel[param1Int];
      }
    };
  
  public String name;
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramInt = paramParcel.dataPosition();
    paramParcel.writeInt(0);
    paramParcel.writeString(this.name);
    int i = paramParcel.dataPosition();
    paramParcel.setDataPosition(paramInt);
    paramParcel.writeInt(i - paramInt);
    paramParcel.setDataPosition(i);
  }
  
  public final void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (j < 0)
      return; 
    try {
      this.name = paramParcel.readString();
      int k = paramParcel.dataPosition();
      if (k - i >= j)
        return; 
      return;
    } finally {
      paramParcel.setDataPosition(i + j);
    } 
  }
  
  public int describeContents() {
    return 0;
  }
}
