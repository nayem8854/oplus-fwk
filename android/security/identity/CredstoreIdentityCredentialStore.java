package android.security.identity;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.ServiceSpecificException;

class CredstoreIdentityCredentialStore extends IdentityCredentialStore {
  private Context mContext = null;
  
  private ICredentialStore mStore = null;
  
  private CredstoreIdentityCredentialStore(Context paramContext, ICredentialStore paramICredentialStore) {
    this.mContext = paramContext;
    this.mStore = paramICredentialStore;
  }
  
  static CredstoreIdentityCredentialStore getInstanceForType(Context paramContext, int paramInt) {
    IBinder iBinder = ServiceManager.getService("android.security.identity");
    ICredentialStoreFactory iCredentialStoreFactory = ICredentialStoreFactory.Stub.asInterface(iBinder);
    if (iCredentialStoreFactory == null)
      return null; 
    try {
      ICredentialStore iCredentialStore = iCredentialStoreFactory.getCredentialStore(paramInt);
      if (iCredentialStore == null)
        return null; 
      return new CredstoreIdentityCredentialStore(paramContext, iCredentialStore);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      if (serviceSpecificException.errorCode == 1)
        return null; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected ServiceSpecificException with code ");
      stringBuilder.append(serviceSpecificException.errorCode);
      throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
    } 
  }
  
  private static CredstoreIdentityCredentialStore sInstanceDefault = null;
  
  private static CredstoreIdentityCredentialStore sInstanceDirectAccess = null;
  
  private static final String TAG = "CredstoreIdentityCredentialStore";
  
  public static IdentityCredentialStore getInstance(Context paramContext) {
    if (sInstanceDefault == null)
      sInstanceDefault = getInstanceForType(paramContext, 0); 
    return sInstanceDefault;
  }
  
  public static IdentityCredentialStore getDirectAccessInstance(Context paramContext) {
    if (sInstanceDirectAccess == null)
      sInstanceDirectAccess = getInstanceForType(paramContext, 1); 
    return sInstanceDirectAccess;
  }
  
  public String[] getSupportedDocTypes() {
    try {
      SecurityHardwareInfoParcel securityHardwareInfoParcel = this.mStore.getSecurityHardwareInfo();
      return securityHardwareInfoParcel.supportedDocTypes;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected ServiceSpecificException with code ");
      stringBuilder.append(serviceSpecificException.errorCode);
      throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
    } 
  }
  
  public WritableIdentityCredential createCredential(String paramString1, String paramString2) throws AlreadyPersonalizedException, DocTypeNotSupportedException {
    try {
      IWritableCredential iWritableCredential = this.mStore.createCredential(paramString1, paramString2);
      return new CredstoreWritableIdentityCredential(this.mContext, paramString1, paramString2, iWritableCredential);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      if (serviceSpecificException.errorCode != 2) {
        if (serviceSpecificException.errorCode == 8)
          throw new DocTypeNotSupportedException(serviceSpecificException.getMessage(), serviceSpecificException); 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected ServiceSpecificException with code ");
        stringBuilder.append(serviceSpecificException.errorCode);
        throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
      } 
      throw new AlreadyPersonalizedException(serviceSpecificException.getMessage(), serviceSpecificException);
    } 
  }
  
  public IdentityCredential getCredentialByName(String paramString, int paramInt) throws CipherSuiteNotSupportedException {
    try {
      ICredential iCredential = this.mStore.getCredentialByName(paramString, paramInt);
      return new CredstoreIdentityCredential(this.mContext, paramString, paramInt, iCredential);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Unexpected RemoteException ", remoteException);
    } catch (ServiceSpecificException serviceSpecificException) {
      if (serviceSpecificException.errorCode == 3)
        return null; 
      if (serviceSpecificException.errorCode == 4)
        throw new CipherSuiteNotSupportedException(serviceSpecificException.getMessage(), serviceSpecificException); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected ServiceSpecificException with code ");
      stringBuilder.append(serviceSpecificException.errorCode);
      throw new RuntimeException(stringBuilder.toString(), serviceSpecificException);
    } 
  }
  
  public byte[] deleteCredentialByName(String paramString) {
    String str = null;
    try {
      ICredential iCredential = this.mStore.getCredentialByName(paramString, 1);
    } catch (ServiceSpecificException serviceSpecificException) {
      paramString = str;
      try {
        if (serviceSpecificException.errorCode == 3)
          return null; 
        return paramString.deleteCredential();
      } catch (ServiceSpecificException serviceSpecificException1) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected ServiceSpecificException with code ");
        stringBuilder.append(serviceSpecificException1.errorCode);
        throw new RuntimeException(stringBuilder.toString(), serviceSpecificException1);
      } 
    } catch (RemoteException remoteException) {}
    return remoteException.deleteCredential();
  }
}
