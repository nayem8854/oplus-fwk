package android.security;

import android.content.Context;
import android.content.pm.PackageManager;
import android.security.net.config.ApplicationConfig;
import android.security.net.config.ManifestConfigSource;

public class NetworkSecurityPolicy {
  private static final NetworkSecurityPolicy INSTANCE = new NetworkSecurityPolicy();
  
  public static NetworkSecurityPolicy getInstance() {
    return INSTANCE;
  }
  
  public boolean isCleartextTrafficPermitted() {
    return libcore.net.NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted();
  }
  
  public boolean isCleartextTrafficPermitted(String paramString) {
    libcore.net.NetworkSecurityPolicy networkSecurityPolicy = libcore.net.NetworkSecurityPolicy.getInstance();
    return 
      networkSecurityPolicy.isCleartextTrafficPermitted(paramString);
  }
  
  public void setCleartextTrafficPermitted(boolean paramBoolean) {
    FrameworkNetworkSecurityPolicy frameworkNetworkSecurityPolicy = new FrameworkNetworkSecurityPolicy(paramBoolean);
    libcore.net.NetworkSecurityPolicy.setInstance(frameworkNetworkSecurityPolicy);
  }
  
  public void handleTrustStorageUpdate() {
    ApplicationConfig applicationConfig = ApplicationConfig.getDefaultInstance();
    if (applicationConfig != null)
      applicationConfig.handleTrustStorageUpdate(); 
  }
  
  public static ApplicationConfig getApplicationConfigForPackage(Context paramContext, String paramString) throws PackageManager.NameNotFoundException {
    paramContext = paramContext.createPackageContext(paramString, 0);
    ManifestConfigSource manifestConfigSource = new ManifestConfigSource(paramContext);
    return new ApplicationConfig(manifestConfigSource);
  }
}
