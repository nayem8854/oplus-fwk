package android.security;

import java.security.KeyPair;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class AttestedKeyPair {
  private final List<Certificate> mAttestationRecord;
  
  private final KeyPair mKeyPair;
  
  public AttestedKeyPair(KeyPair paramKeyPair, List<Certificate> paramList) {
    this.mKeyPair = paramKeyPair;
    this.mAttestationRecord = paramList;
  }
  
  public AttestedKeyPair(KeyPair paramKeyPair, Certificate[] paramArrayOfCertificate) {
    this.mKeyPair = paramKeyPair;
    if (paramArrayOfCertificate == null) {
      this.mAttestationRecord = new ArrayList<>();
    } else {
      this.mAttestationRecord = Arrays.asList(paramArrayOfCertificate);
    } 
  }
  
  public KeyPair getKeyPair() {
    return this.mKeyPair;
  }
  
  public List<Certificate> getAttestationRecord() {
    return Collections.unmodifiableList(this.mAttestationRecord);
  }
}
