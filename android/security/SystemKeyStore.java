package android.security;

import android.os.Environment;
import android.os.FileUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import libcore.io.IoUtils;

public class SystemKeyStore {
  private static final String KEY_FILE_EXTENSION = ".sks";
  
  private static final String SYSTEM_KEYSTORE_DIRECTORY = "misc/systemkeys";
  
  private static SystemKeyStore mInstance = new SystemKeyStore();
  
  public static SystemKeyStore getInstance() {
    return mInstance;
  }
  
  public static String toHexString(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null)
      return null; 
    int i = paramArrayOfbyte.length;
    i = paramArrayOfbyte.length;
    StringBuilder stringBuilder = new StringBuilder(i * 2);
    for (i = 0; i < paramArrayOfbyte.length; i++) {
      String str1 = Integer.toString(paramArrayOfbyte[i] & 0xFF, 16);
      String str2 = str1;
      if (str1.length() == 1) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("0");
        stringBuilder1.append(str1);
        str2 = stringBuilder1.toString();
      } 
      stringBuilder.append(str2);
    } 
    return stringBuilder.toString();
  }
  
  public String generateNewKeyHexString(int paramInt, String paramString1, String paramString2) throws NoSuchAlgorithmException {
    return toHexString(generateNewKey(paramInt, paramString1, paramString2));
  }
  
  public byte[] generateNewKey(int paramInt, String paramString1, String paramString2) throws NoSuchAlgorithmException {
    File file = getKeyFile(paramString2);
    if (!file.exists()) {
      KeyGenerator keyGenerator = KeyGenerator.getInstance(paramString1);
      SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
      keyGenerator.init(paramInt, secureRandom);
      SecretKey secretKey = keyGenerator.generateKey();
      byte[] arrayOfByte = secretKey.getEncoded();
      try {
        if (file.createNewFile()) {
          FileOutputStream fileOutputStream = new FileOutputStream();
          this(file);
          fileOutputStream.write(arrayOfByte);
          fileOutputStream.flush();
          FileUtils.sync(fileOutputStream);
          fileOutputStream.close();
          FileUtils.setPermissions(file.getName(), 384, -1, -1);
          return arrayOfByte;
        } 
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this();
        throw illegalArgumentException;
      } catch (IOException iOException) {
        return null;
      } 
    } 
    throw new IllegalArgumentException();
  }
  
  private File getKeyFile(String paramString) {
    File file = new File(Environment.getDataDirectory(), "misc/systemkeys");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(".sks");
    return new File(file, stringBuilder.toString());
  }
  
  public String retrieveKeyHexString(String paramString) throws IOException {
    return toHexString(retrieveKey(paramString));
  }
  
  public byte[] retrieveKey(String paramString) throws IOException {
    File file = getKeyFile(paramString);
    if (!file.exists())
      return null; 
    return IoUtils.readFileAsByteArray(file.toString());
  }
  
  public void deleteKey(String paramString) {
    File file = getKeyFile(paramString);
    if (file.exists()) {
      file.delete();
      return;
    } 
    throw new IllegalArgumentException();
  }
}
