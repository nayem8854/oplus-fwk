package android.security;

import android.os.Parcel;
import android.os.Parcelable;

public class KeystoreArguments implements Parcelable {
  public static final Parcelable.Creator<KeystoreArguments> CREATOR = new Parcelable.Creator<KeystoreArguments>() {
      public KeystoreArguments createFromParcel(Parcel param1Parcel) {
        return new KeystoreArguments(param1Parcel);
      }
      
      public KeystoreArguments[] newArray(int param1Int) {
        return new KeystoreArguments[param1Int];
      }
    };
  
  public byte[][] args;
  
  public KeystoreArguments() {
    this.args = null;
  }
  
  public KeystoreArguments(byte[][] paramArrayOfbyte) {
    this.args = paramArrayOfbyte;
  }
  
  private KeystoreArguments(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    byte[][] arrayOfByte = this.args;
    paramInt = 0;
    if (arrayOfByte == null) {
      paramParcel.writeInt(0);
    } else {
      paramParcel.writeInt(arrayOfByte.length);
      int i;
      for (arrayOfByte = this.args, i = arrayOfByte.length; paramInt < i; ) {
        byte[] arrayOfByte1 = arrayOfByte[paramInt];
        paramParcel.writeByteArray(arrayOfByte1);
        paramInt++;
      } 
    } 
  }
  
  private void readFromParcel(Parcel paramParcel) {
    int i = paramParcel.readInt();
    this.args = new byte[i][];
    for (byte b = 0; b < i; b++)
      this.args[b] = paramParcel.createByteArray(); 
  }
  
  public int describeContents() {
    return 0;
  }
}
