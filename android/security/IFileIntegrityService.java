package android.security;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IFileIntegrityService extends IInterface {
  boolean isApkVeritySupported() throws RemoteException;
  
  boolean isAppSourceCertificateTrusted(byte[] paramArrayOfbyte, String paramString) throws RemoteException;
  
  class Default implements IFileIntegrityService {
    public boolean isApkVeritySupported() throws RemoteException {
      return false;
    }
    
    public boolean isAppSourceCertificateTrusted(byte[] param1ArrayOfbyte, String param1String) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IFileIntegrityService {
    private static final String DESCRIPTOR = "android.security.IFileIntegrityService";
    
    static final int TRANSACTION_isApkVeritySupported = 1;
    
    static final int TRANSACTION_isAppSourceCertificateTrusted = 2;
    
    public Stub() {
      attachInterface(this, "android.security.IFileIntegrityService");
    }
    
    public static IFileIntegrityService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.security.IFileIntegrityService");
      if (iInterface != null && iInterface instanceof IFileIntegrityService)
        return (IFileIntegrityService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "isAppSourceCertificateTrusted";
      } 
      return "isApkVeritySupported";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.security.IFileIntegrityService");
          return true;
        } 
        param1Parcel1.enforceInterface("android.security.IFileIntegrityService");
        byte[] arrayOfByte = param1Parcel1.createByteArray();
        str = param1Parcel1.readString();
        boolean bool1 = isAppSourceCertificateTrusted(arrayOfByte, str);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      str.enforceInterface("android.security.IFileIntegrityService");
      boolean bool = isApkVeritySupported();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements IFileIntegrityService {
      public static IFileIntegrityService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.security.IFileIntegrityService";
      }
      
      public boolean isApkVeritySupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IFileIntegrityService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IFileIntegrityService.Stub.getDefaultImpl() != null) {
            bool1 = IFileIntegrityService.Stub.getDefaultImpl().isApkVeritySupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAppSourceCertificateTrusted(byte[] param2ArrayOfbyte, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.security.IFileIntegrityService");
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IFileIntegrityService.Stub.getDefaultImpl() != null) {
            bool1 = IFileIntegrityService.Stub.getDefaultImpl().isAppSourceCertificateTrusted(param2ArrayOfbyte, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IFileIntegrityService param1IFileIntegrityService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IFileIntegrityService != null) {
          Proxy.sDefaultImpl = param1IFileIntegrityService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IFileIntegrityService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
