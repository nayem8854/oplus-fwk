package android.security;

import android.content.Context;
import java.security.KeyStore;

@Deprecated
public final class KeyStoreParameter implements KeyStore.ProtectionParameter {
  private final int mFlags;
  
  private KeyStoreParameter(int paramInt) {
    this.mFlags = paramInt;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public boolean isEncryptionRequired() {
    int i = this.mFlags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  @Deprecated
  public static final class Builder {
    private int mFlags;
    
    public Builder(Context param1Context) {
      if (param1Context != null)
        return; 
      throw new NullPointerException("context == null");
    }
    
    public Builder setEncryptionRequired(boolean param1Boolean) {
      if (param1Boolean) {
        this.mFlags |= 0x1;
      } else {
        this.mFlags &= 0xFFFFFFFE;
      } 
      return this;
    }
    
    public KeyStoreParameter build() {
      return new KeyStoreParameter(this.mFlags);
    }
  }
}
