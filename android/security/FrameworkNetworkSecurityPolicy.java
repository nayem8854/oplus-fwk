package android.security;

import libcore.net.NetworkSecurityPolicy;

public class FrameworkNetworkSecurityPolicy extends NetworkSecurityPolicy {
  private final boolean mCleartextTrafficPermitted;
  
  public FrameworkNetworkSecurityPolicy(boolean paramBoolean) {
    this.mCleartextTrafficPermitted = paramBoolean;
  }
  
  public boolean isCleartextTrafficPermitted() {
    return this.mCleartextTrafficPermitted;
  }
  
  public boolean isCleartextTrafficPermitted(String paramString) {
    return isCleartextTrafficPermitted();
  }
  
  public boolean isCertificateTransparencyVerificationRequired(String paramString) {
    return false;
  }
}
