package android.security;

public class Scrypt {
  native byte[] nativeScrypt(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  public byte[] scrypt(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    return nativeScrypt(paramArrayOfbyte1, paramArrayOfbyte2, paramInt1, paramInt2, paramInt3, paramInt4);
  }
}
