package android.debug;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.os.UserHandle;
import com.android.internal.notification.SystemNotificationChannels;

public final class AdbNotifications {
  private static final String ADB_NOTIFICATION_CHANNEL_ID_TV = "usbdevicemanager.adb.tv";
  
  public static Notification createNotification(Context paramContext, byte paramByte) {
    StringBuilder stringBuilder;
    int i, j;
    Resources resources = paramContext.getResources();
    if (paramByte == 0) {
      i = 17039605;
      j = 17039604;
    } else if (i == 1) {
      i = 17039608;
      j = 17039607;
    } else {
      stringBuilder = new StringBuilder();
      stringBuilder.append("createNotification called with unknown transport type=");
      stringBuilder.append(i);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    CharSequence charSequence1 = resources.getText(i);
    CharSequence charSequence2 = resources.getText(j);
    Intent intent = new Intent("android.settings.APPLICATION_DEVELOPMENT_SETTINGS");
    intent.addFlags(268468224);
    ResolveInfo resolveInfo = stringBuilder.getPackageManager().resolveActivity(intent, 1048576);
    if (resolveInfo != null) {
      intent.setPackage(resolveInfo.activityInfo.packageName);
      PendingIntent pendingIntent = PendingIntent.getActivityAsUser((Context)stringBuilder, 0, intent, 67108864, null, UserHandle.CURRENT);
    } else {
      intent = null;
    } 
    Notification.Builder builder2 = new Notification.Builder((Context)stringBuilder, SystemNotificationChannels.DEVELOPER_IMPORTANT);
    builder2 = builder2.setSmallIcon(17303564);
    builder2 = builder2.setWhen(0L);
    builder2 = builder2.setOngoing(true);
    builder2 = builder2.setTicker(charSequence1);
    builder2 = builder2.setDefaults(0);
    Notification.Builder builder1 = builder2.setColor(stringBuilder.getColor(17170460));
    builder1 = builder1.setContentTitle(charSequence1);
    builder1 = builder1.setContentText(charSequence2);
    builder1 = builder1.setContentIntent((PendingIntent)intent);
    builder1 = builder1.setVisibility(1);
    Notification.TvExtender tvExtender = new Notification.TvExtender();
    tvExtender = tvExtender.setChannelId("usbdevicemanager.adb.tv");
    builder1 = builder1.extend(tvExtender);
    return builder1.build();
  }
}
