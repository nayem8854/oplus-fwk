package android.debug;

import java.io.File;

public abstract class AdbManagerInternal {
  public abstract File getAdbKeysFile();
  
  public abstract File getAdbTempKeysFile();
  
  public abstract boolean isAdbEnabled(byte paramByte);
  
  public abstract void registerTransport(IAdbTransport paramIAdbTransport);
  
  public abstract void startAdbdForTransport(byte paramByte);
  
  public abstract void stopAdbdForTransport(byte paramByte);
  
  public abstract void unregisterTransport(IAdbTransport paramIAdbTransport);
}
