package android.debug;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.Map;

public interface IAdbManager extends IInterface {
  void allowDebugging(boolean paramBoolean, String paramString) throws RemoteException;
  
  void allowWirelessDebugging(boolean paramBoolean, String paramString) throws RemoteException;
  
  void clearDebuggingKeys() throws RemoteException;
  
  void denyDebugging() throws RemoteException;
  
  void denyWirelessDebugging() throws RemoteException;
  
  void disablePairing() throws RemoteException;
  
  void enablePairingByPairingCode() throws RemoteException;
  
  void enablePairingByQrCode(String paramString1, String paramString2) throws RemoteException;
  
  int getAdbWirelessPort() throws RemoteException;
  
  Map getPairedDevices() throws RemoteException;
  
  boolean isAdbWifiQrSupported() throws RemoteException;
  
  boolean isAdbWifiSupported() throws RemoteException;
  
  void unpairDevice(String paramString) throws RemoteException;
  
  class Default implements IAdbManager {
    public void allowDebugging(boolean param1Boolean, String param1String) throws RemoteException {}
    
    public void denyDebugging() throws RemoteException {}
    
    public void clearDebuggingKeys() throws RemoteException {}
    
    public void allowWirelessDebugging(boolean param1Boolean, String param1String) throws RemoteException {}
    
    public void denyWirelessDebugging() throws RemoteException {}
    
    public Map getPairedDevices() throws RemoteException {
      return null;
    }
    
    public void unpairDevice(String param1String) throws RemoteException {}
    
    public void enablePairingByPairingCode() throws RemoteException {}
    
    public void enablePairingByQrCode(String param1String1, String param1String2) throws RemoteException {}
    
    public int getAdbWirelessPort() throws RemoteException {
      return 0;
    }
    
    public void disablePairing() throws RemoteException {}
    
    public boolean isAdbWifiSupported() throws RemoteException {
      return false;
    }
    
    public boolean isAdbWifiQrSupported() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAdbManager {
    private static final String DESCRIPTOR = "android.debug.IAdbManager";
    
    static final int TRANSACTION_allowDebugging = 1;
    
    static final int TRANSACTION_allowWirelessDebugging = 4;
    
    static final int TRANSACTION_clearDebuggingKeys = 3;
    
    static final int TRANSACTION_denyDebugging = 2;
    
    static final int TRANSACTION_denyWirelessDebugging = 5;
    
    static final int TRANSACTION_disablePairing = 11;
    
    static final int TRANSACTION_enablePairingByPairingCode = 8;
    
    static final int TRANSACTION_enablePairingByQrCode = 9;
    
    static final int TRANSACTION_getAdbWirelessPort = 10;
    
    static final int TRANSACTION_getPairedDevices = 6;
    
    static final int TRANSACTION_isAdbWifiQrSupported = 13;
    
    static final int TRANSACTION_isAdbWifiSupported = 12;
    
    static final int TRANSACTION_unpairDevice = 7;
    
    public Stub() {
      attachInterface(this, "android.debug.IAdbManager");
    }
    
    public static IAdbManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.debug.IAdbManager");
      if (iInterface != null && iInterface instanceof IAdbManager)
        return (IAdbManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 13:
          return "isAdbWifiQrSupported";
        case 12:
          return "isAdbWifiSupported";
        case 11:
          return "disablePairing";
        case 10:
          return "getAdbWirelessPort";
        case 9:
          return "enablePairingByQrCode";
        case 8:
          return "enablePairingByPairingCode";
        case 7:
          return "unpairDevice";
        case 6:
          return "getPairedDevices";
        case 5:
          return "denyWirelessDebugging";
        case 4:
          return "allowWirelessDebugging";
        case 3:
          return "clearDebuggingKeys";
        case 2:
          return "denyDebugging";
        case 1:
          break;
      } 
      return "allowDebugging";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        int i;
        String str2;
        Map map;
        String str3;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 13:
            param1Parcel1.enforceInterface("android.debug.IAdbManager");
            bool = isAdbWifiQrSupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 12:
            param1Parcel1.enforceInterface("android.debug.IAdbManager");
            bool = isAdbWifiSupported();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 11:
            param1Parcel1.enforceInterface("android.debug.IAdbManager");
            disablePairing();
            param1Parcel2.writeNoException();
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.debug.IAdbManager");
            i = getAdbWirelessPort();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.debug.IAdbManager");
            str3 = param1Parcel1.readString();
            str2 = param1Parcel1.readString();
            enablePairingByQrCode(str3, str2);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str2.enforceInterface("android.debug.IAdbManager");
            enablePairingByPairingCode();
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str2.enforceInterface("android.debug.IAdbManager");
            str2 = str2.readString();
            unpairDevice(str2);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str2.enforceInterface("android.debug.IAdbManager");
            map = getPairedDevices();
            param1Parcel2.writeNoException();
            param1Parcel2.writeMap(map);
            return true;
          case 5:
            map.enforceInterface("android.debug.IAdbManager");
            denyWirelessDebugging();
            param1Parcel2.writeNoException();
            return true;
          case 4:
            map.enforceInterface("android.debug.IAdbManager");
            bool1 = bool2;
            if (map.readInt() != 0)
              bool1 = true; 
            str1 = map.readString();
            allowWirelessDebugging(bool1, str1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("android.debug.IAdbManager");
            clearDebuggingKeys();
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("android.debug.IAdbManager");
            denyDebugging();
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.debug.IAdbManager");
        if (str1.readInt() != 0)
          bool1 = true; 
        String str1 = str1.readString();
        allowDebugging(bool1, str1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.debug.IAdbManager");
      return true;
    }
    
    private static class Proxy implements IAdbManager {
      public static IAdbManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.debug.IAdbManager";
      }
      
      public void allowDebugging(boolean param2Boolean, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.debug.IAdbManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeString(param2String);
          boolean bool1 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool1 && IAdbManager.Stub.getDefaultImpl() != null) {
            IAdbManager.Stub.getDefaultImpl().allowDebugging(param2Boolean, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void denyDebugging() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.debug.IAdbManager");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IAdbManager.Stub.getDefaultImpl() != null) {
            IAdbManager.Stub.getDefaultImpl().denyDebugging();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearDebuggingKeys() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.debug.IAdbManager");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IAdbManager.Stub.getDefaultImpl() != null) {
            IAdbManager.Stub.getDefaultImpl().clearDebuggingKeys();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void allowWirelessDebugging(boolean param2Boolean, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.debug.IAdbManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeString(param2String);
          boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool1 && IAdbManager.Stub.getDefaultImpl() != null) {
            IAdbManager.Stub.getDefaultImpl().allowWirelessDebugging(param2Boolean, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void denyWirelessDebugging() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.debug.IAdbManager");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IAdbManager.Stub.getDefaultImpl() != null) {
            IAdbManager.Stub.getDefaultImpl().denyWirelessDebugging();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Map getPairedDevices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.debug.IAdbManager");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IAdbManager.Stub.getDefaultImpl() != null)
            return IAdbManager.Stub.getDefaultImpl().getPairedDevices(); 
          parcel2.readException();
          ClassLoader classLoader = getClass().getClassLoader();
          return parcel2.readHashMap(classLoader);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unpairDevice(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.debug.IAdbManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IAdbManager.Stub.getDefaultImpl() != null) {
            IAdbManager.Stub.getDefaultImpl().unpairDevice(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enablePairingByPairingCode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.debug.IAdbManager");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IAdbManager.Stub.getDefaultImpl() != null) {
            IAdbManager.Stub.getDefaultImpl().enablePairingByPairingCode();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enablePairingByQrCode(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.debug.IAdbManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IAdbManager.Stub.getDefaultImpl() != null) {
            IAdbManager.Stub.getDefaultImpl().enablePairingByQrCode(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getAdbWirelessPort() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.debug.IAdbManager");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IAdbManager.Stub.getDefaultImpl() != null)
            return IAdbManager.Stub.getDefaultImpl().getAdbWirelessPort(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disablePairing() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.debug.IAdbManager");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IAdbManager.Stub.getDefaultImpl() != null) {
            IAdbManager.Stub.getDefaultImpl().disablePairing();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAdbWifiSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.debug.IAdbManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && IAdbManager.Stub.getDefaultImpl() != null) {
            bool1 = IAdbManager.Stub.getDefaultImpl().isAdbWifiSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAdbWifiQrSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.debug.IAdbManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IAdbManager.Stub.getDefaultImpl() != null) {
            bool1 = IAdbManager.Stub.getDefaultImpl().isAdbWifiQrSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAdbManager param1IAdbManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAdbManager != null) {
          Proxy.sDefaultImpl = param1IAdbManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAdbManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
