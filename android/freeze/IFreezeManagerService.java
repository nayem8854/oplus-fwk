package android.freeze;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.os.Parcel;

public interface IFreezeManagerService extends IOplusCommonFeature {
  public static final IFreezeManagerService DEFAULT = (IFreezeManagerService)new Object();
  
  default IFreezeManagerService getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IFreezeManagerService;
  }
  
  default boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2) {
    return false;
  }
}
