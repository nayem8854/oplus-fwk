package android.freeze;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.Context;
import android.os.UserHandle;

public interface IFreezeManagerHelp extends IOplusCommonFeature {
  public static final IFreezeManagerHelp DEFAULT = (IFreezeManagerHelp)new Object();
  
  default IFreezeManagerHelp getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IFreezeManagerHelp;
  }
  
  default boolean isFreezeSupport(Context paramContext) {
    return false;
  }
  
  default boolean handleStartForUid(Context paramContext, String paramString, int paramInt1, int paramInt2) {
    return handleStartForUserId(paramContext, paramString, paramInt1, UserHandle.getUserId(paramInt2));
  }
  
  default void handleRemoveTask(String paramString, int paramInt) {}
  
  default boolean handleStartForUserId(Context paramContext, String paramString, int paramInt1, int paramInt2) {
    return false;
  }
  
  default boolean handleStartProcForUserId(Context paramContext, String paramString, int paramInt) {
    return false;
  }
  
  default boolean handleActivityStart(Context paramContext, String paramString1, String paramString2, int paramInt) {
    return false;
  }
}
