package android.freeze;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.UserHandle;
import android.util.Log;
import java.util.List;

public class FreezeManagerHelp implements IFreezeManagerHelp {
  private static final String TAG = "FreezeManagerHelp";
  
  private static volatile Boolean sFreezeSupport = null;
  
  private static volatile FreezeManagerHelp sInstance;
  
  public static FreezeManagerHelp getInstance() {
    // Byte code:
    //   0: getstatic android/freeze/FreezeManagerHelp.sInstance : Landroid/freeze/FreezeManagerHelp;
    //   3: ifnonnull -> 39
    //   6: ldc android/freeze/FreezeManagerHelp
    //   8: monitorenter
    //   9: getstatic android/freeze/FreezeManagerHelp.sInstance : Landroid/freeze/FreezeManagerHelp;
    //   12: ifnonnull -> 27
    //   15: new android/freeze/FreezeManagerHelp
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic android/freeze/FreezeManagerHelp.sInstance : Landroid/freeze/FreezeManagerHelp;
    //   27: ldc android/freeze/FreezeManagerHelp
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc android/freeze/FreezeManagerHelp
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic android/freeze/FreezeManagerHelp.sInstance : Landroid/freeze/FreezeManagerHelp;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #42	-> 0
    //   #43	-> 6
    //   #44	-> 9
    //   #45	-> 15
    //   #47	-> 27
    //   #49	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public boolean isFreezeSupport(Context paramContext) {
    // Byte code:
    //   0: getstatic android/freeze/FreezeManagerHelp.sFreezeSupport : Ljava/lang/Boolean;
    //   3: ifnonnull -> 43
    //   6: ldc android/freeze/FreezeManager
    //   8: monitorenter
    //   9: getstatic android/freeze/FreezeManagerHelp.sFreezeSupport : Ljava/lang/Boolean;
    //   12: ifnonnull -> 31
    //   15: invokestatic getInstance : ()Lcom/oplus/content/OplusFeatureConfigManager;
    //   18: ldc 'oplus.software.forwardly_freeze'
    //   20: invokevirtual hasFeature : (Ljava/lang/String;)Z
    //   23: istore_2
    //   24: iload_2
    //   25: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   28: putstatic android/freeze/FreezeManagerHelp.sFreezeSupport : Ljava/lang/Boolean;
    //   31: ldc android/freeze/FreezeManager
    //   33: monitorexit
    //   34: goto -> 43
    //   37: astore_1
    //   38: ldc android/freeze/FreezeManager
    //   40: monitorexit
    //   41: aload_1
    //   42: athrow
    //   43: getstatic android/freeze/FreezeManagerHelp.sFreezeSupport : Ljava/lang/Boolean;
    //   46: invokevirtual booleanValue : ()Z
    //   49: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #54	-> 0
    //   #55	-> 6
    //   #56	-> 9
    //   #57	-> 15
    //   #58	-> 24
    //   #60	-> 31
    //   #62	-> 43
    // Exception table:
    //   from	to	target	type
    //   9	15	37	finally
    //   15	24	37	finally
    //   24	31	37	finally
    //   31	34	37	finally
    //   38	41	37	finally
  }
  
  public boolean handleStartForUid(Context paramContext, String paramString, int paramInt1, int paramInt2) {
    return handleStartForUserId(paramContext, paramString, paramInt1, UserHandle.getUserId(paramInt2));
  }
  
  public void handleRemoveTask(String paramString, int paramInt) {
    boolean bool;
    if (FreezeManager.getInstance().getPackageFreezeUserSetting(paramString, UserHandle.of(paramInt)) == 1) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleRemoveTask : pkg");
      stringBuilder.append(paramString);
      stringBuilder.append(paramInt);
      Log.d("FreezeManagerHelp", stringBuilder.toString());
      FreezeManager.getInstance().setPackageFreezeState(paramString, 1, UserHandle.of(paramInt));
    } 
  }
  
  public boolean handleStartForUserId(Context paramContext, String paramString, int paramInt1, int paramInt2) {
    int i = FreezeManager.getInstance().getPackageFreezeState(paramString, UserHandle.of(paramInt2));
    boolean bool1 = true;
    if (i != 1)
      bool1 = false; 
    boolean bool2 = bool1;
    bool1 = bool2;
    if (bool2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleStartForUserId : pkg");
      stringBuilder.append(paramString);
      stringBuilder.append(paramInt2);
      Log.d("FreezeManagerHelp", stringBuilder.toString());
      bool1 = bool2;
      if (isFromPendingIntent(paramContext, paramInt1, paramString)) {
        bool1 = false;
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("handleStartForUserId : pkg");
        stringBuilder1.append(paramString);
        stringBuilder1.append(paramInt2);
        stringBuilder1.append("isFromPendingIntent");
        Log.d("FreezeManagerHelp", stringBuilder1.toString());
        FreezeManager.getInstance().setPackageFreezeState(paramString, 0, UserHandle.of(paramInt2));
      } 
    } 
    return bool1;
  }
  
  public boolean handleStartProcForUserId(Context paramContext, String paramString, int paramInt) {
    int i = FreezeManager.getInstance().getPackageFreezeState(paramString, UserHandle.of(paramInt));
    boolean bool = true;
    if (i != 1)
      bool = false; 
    if (bool) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleStartProcForUserId : pkg");
      stringBuilder.append(paramString);
      stringBuilder.append(paramInt);
      Log.d("FreezeManagerHelp", stringBuilder.toString());
    } 
    return bool;
  }
  
  private boolean isCallerFromLauncher(Context paramContext, int paramInt) {
    String[] arrayOfString = paramContext.getPackageManager().getPackagesForUid(paramInt);
    if (arrayOfString != null && arrayOfString.length > 0) {
      Intent intent = new Intent("android.intent.action.MAIN");
      intent.addCategory("android.intent.category.HOME");
      List<ResolveInfo> list = paramContext.getPackageManager().queryIntentActivities(intent, 65536);
      if (list != null)
        for (ResolveInfo resolveInfo : list) {
          for (int i = arrayOfString.length; paramInt < i; ) {
            String str = arrayOfString[paramInt];
            if (resolveInfo.activityInfo.packageName.equals(str))
              return true; 
            paramInt++;
          } 
        }  
    } 
    return false;
  }
  
  private boolean isFromPendingIntent(Context paramContext, int paramInt, String paramString) {
    String[] arrayOfString = paramContext.getPackageManager().getPackagesForUid(paramInt);
    if (arrayOfString != null && arrayOfString.length > 0)
      for (int i = arrayOfString.length; paramInt < i; ) {
        String str = arrayOfString[paramInt];
        if (str.equals(paramString))
          return true; 
        paramInt++;
      }  
    return false;
  }
  
  public boolean handleActivityStart(Context paramContext, String paramString1, String paramString2, int paramInt) {
    int i = FreezeManager.getInstance().getPackageFreezeState(paramString1, UserHandle.getUserHandleForUid(paramInt));
    if (i == 1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleActivityStart : pkg");
      stringBuilder.append(paramString1);
      Log.d("FreezeManagerHelp", stringBuilder.toString());
      FreezeManager.getInstance().setPackageFreezeState(paramString1, 0, UserHandle.getUserHandleForUid(paramInt));
    } 
    return false;
  }
}
