package android.freeze;

import android.content.Context;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class FreezeManager {
  private IBinder mRemote;
  
  String mDESCRIPTOR = "android.app.IPackageManager";
  
  private static volatile Boolean sFreezeSupport = null;
  
  private static volatile FreezeManager sFreezeManager;
  
  public static final int TRANSACTION_setPackageFreezeUserSetting = 90005;
  
  public static final int TRANSACTION_setPackageFreezeState = 90004;
  
  public static final int TRANSACTION_setFreezeEnable = 90001;
  
  public static final int TRANSACTION_isFreezeEnabled = 90000;
  
  public static final int TRANSACTION_getUserSettingFreezeableApplicationList = 90007;
  
  public static final int TRANSACTION_getPackageFreezeUserSetting = 90003;
  
  public static final int TRANSACTION_getPackageFreezeState = 90002;
  
  public static final int TRANSACTION_getFreezedApplicationList = 90006;
  
  public static final int TRANSACTION_first = 90000;
  
  private static final String TAG = "FreezeManager";
  
  public static final int FREEZE_STATE_UNKNOW = -1;
  
  public static final int FREEZE_STATE_NOMAL = 0;
  
  public static final int FREEZE_STATE_FREEZED = 1;
  
  public static final int FREEZE_SETTING_UNKNOW = -1;
  
  public static final int FREEZE_SETTING_NOMAL = 0;
  
  public static final int FREEZE_SETTING_CAN_REEZE = 1;
  
  private FreezeManager() {
    this.mRemote = ServiceManager.getService("package");
  }
  
  public static FreezeManager getInstance() {
    // Byte code:
    //   0: getstatic android/freeze/FreezeManager.sFreezeManager : Landroid/freeze/FreezeManager;
    //   3: ifnonnull -> 39
    //   6: ldc android/freeze/FreezeManager
    //   8: monitorenter
    //   9: getstatic android/freeze/FreezeManager.sFreezeManager : Landroid/freeze/FreezeManager;
    //   12: ifnonnull -> 27
    //   15: new android/freeze/FreezeManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic android/freeze/FreezeManager.sFreezeManager : Landroid/freeze/FreezeManager;
    //   27: ldc android/freeze/FreezeManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc android/freeze/FreezeManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic android/freeze/FreezeManager.sFreezeManager : Landroid/freeze/FreezeManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #63	-> 0
    //   #64	-> 6
    //   #65	-> 9
    //   #66	-> 15
    //   #68	-> 27
    //   #70	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public static boolean isFreezeSupport(Context paramContext) {
    // Byte code:
    //   0: getstatic android/freeze/FreezeManager.sFreezeSupport : Ljava/lang/Boolean;
    //   3: ifnonnull -> 43
    //   6: ldc android/freeze/FreezeManager
    //   8: monitorenter
    //   9: getstatic android/freeze/FreezeManager.sFreezeSupport : Ljava/lang/Boolean;
    //   12: ifnonnull -> 31
    //   15: invokestatic getInstance : ()Lcom/oplus/content/OplusFeatureConfigManager;
    //   18: ldc 'oplus.software.forwardly_freeze'
    //   20: invokevirtual hasFeature : (Ljava/lang/String;)Z
    //   23: istore_1
    //   24: iload_1
    //   25: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   28: putstatic android/freeze/FreezeManager.sFreezeSupport : Ljava/lang/Boolean;
    //   31: ldc android/freeze/FreezeManager
    //   33: monitorexit
    //   34: goto -> 43
    //   37: astore_0
    //   38: ldc android/freeze/FreezeManager
    //   40: monitorexit
    //   41: aload_0
    //   42: athrow
    //   43: getstatic android/freeze/FreezeManager.sFreezeSupport : Ljava/lang/Boolean;
    //   46: invokevirtual booleanValue : ()Z
    //   49: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #74	-> 0
    //   #75	-> 6
    //   #76	-> 9
    //   #77	-> 15
    //   #78	-> 24
    //   #80	-> 31
    //   #82	-> 43
    // Exception table:
    //   from	to	target	type
    //   9	15	37	finally
    //   15	24	37	finally
    //   24	31	37	finally
    //   31	34	37	finally
    //   38	41	37	finally
  }
  
  public boolean isFreezeEnabled() {
    boolean bool2;
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    boolean bool1 = false;
    try {
      parcel1.writeInterfaceToken(this.mDESCRIPTOR);
      this.mRemote.transact(90000, parcel1, parcel2, 0);
      parcel2.readException();
      bool2 = parcel2.readBoolean();
    } catch (RemoteException remoteException) {
      Log.d("FreezeManager", remoteException.getMessage());
      bool2 = bool1;
    } finally {
      Exception exception;
    } 
    parcel1.recycle();
    parcel2.recycle();
    return bool2;
  }
  
  public void setFreezeEnable(boolean paramBoolean) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken(this.mDESCRIPTOR);
      parcel1.writeBoolean(paramBoolean);
      this.mRemote.transact(90001, parcel1, parcel2, 0);
      parcel2.readException();
    } catch (RemoteException remoteException) {
      Log.d("FreezeManager", remoteException.getMessage());
    } finally {
      Exception exception;
    } 
    parcel1.recycle();
    parcel2.recycle();
  }
  
  public int getPackageFreezeState(String paramString) {
    UserHandle userHandle = Process.myUserHandle();
    return getPackageFreezeState(paramString, userHandle);
  }
  
  public int getPackageFreezeState(String paramString, UserHandle paramUserHandle) {
    byte b2;
    UserHandle userHandle = paramUserHandle;
    if (paramUserHandle == null)
      userHandle = Process.myUserHandle(); 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    byte b1 = -1;
    try {
      parcel1.writeInterfaceToken(this.mDESCRIPTOR);
      parcel1.writeString(paramString);
      if (userHandle != null) {
        parcel1.writeInt(1);
        userHandle.writeToParcel(parcel1, 0);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(90002, parcel1, parcel2, 0);
      parcel2.readException();
      b2 = parcel2.readInt();
    } catch (RemoteException remoteException) {
      Log.d("FreezeManager", remoteException.getMessage());
      b2 = b1;
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
    return b2;
  }
  
  public int getPackageFreezeUserSetting(String paramString) {
    UserHandle userHandle = Process.myUserHandle();
    return getPackageFreezeUserSetting(paramString, userHandle);
  }
  
  public int getPackageFreezeUserSetting(String paramString, UserHandle paramUserHandle) {
    UserHandle userHandle = paramUserHandle;
    if (paramUserHandle == null)
      userHandle = Process.myUserHandle(); 
    Parcel parcel2 = Parcel.obtain();
    Parcel parcel1 = Parcel.obtain();
    int i = -1;
    try {
      parcel2.writeInterfaceToken(this.mDESCRIPTOR);
      parcel2.writeString(paramString);
      if (userHandle != null) {
        parcel2.writeInt(1);
        userHandle.writeToParcel(parcel2, 0);
      } else {
        parcel2.writeInt(0);
      } 
      this.mRemote.transact(90003, parcel2, parcel1, 0);
      parcel1.readException();
      int j = parcel1.readInt();
    } catch (RemoteException remoteException) {
      Log.d("FreezeManager", remoteException.getMessage());
    } finally {}
    parcel2.recycle();
    parcel1.recycle();
    return i;
  }
  
  public void setPackageFreezeState(String paramString, int paramInt) {
    UserHandle userHandle = Process.myUserHandle();
    setPackageFreezeState(paramString, paramInt, userHandle);
  }
  
  public void setPackageFreezeState(String paramString, int paramInt, UserHandle paramUserHandle) {
    UserHandle userHandle = paramUserHandle;
    if (paramUserHandle == null)
      userHandle = Process.myUserHandle(); 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken(this.mDESCRIPTOR);
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      if (userHandle != null) {
        parcel1.writeInt(1);
        userHandle.writeToParcel(parcel1, 0);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(90004, parcel1, parcel2, 0);
      parcel2.readException();
    } catch (RemoteException remoteException) {
      Log.d("FreezeManager", remoteException.getMessage());
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
  }
  
  public void setPackageFreezeUserSetting(String paramString, int paramInt) {
    UserHandle userHandle = Process.myUserHandle();
    setPackageFreezeUserSetting(paramString, paramInt, userHandle);
  }
  
  public void setPackageFreezeUserSetting(String paramString, int paramInt, UserHandle paramUserHandle) {
    UserHandle userHandle = paramUserHandle;
    if (paramUserHandle == null)
      userHandle = Process.myUserHandle(); 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken(this.mDESCRIPTOR);
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      if (userHandle != null) {
        parcel1.writeInt(1);
        userHandle.writeToParcel(parcel1, 0);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(90005, parcel1, parcel2, 0);
      parcel2.readException();
    } catch (RemoteException remoteException) {
      Log.d("FreezeManager", remoteException.getMessage());
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
  }
  
  public List<String> getFreezedApplicationList(UserHandle paramUserHandle) {
    UserHandle userHandle = paramUserHandle;
    if (paramUserHandle == null)
      userHandle = Process.myUserHandle(); 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    ArrayList<String> arrayList = new ArrayList();
    try {
      parcel1.writeInterfaceToken(this.mDESCRIPTOR);
      if (userHandle != null) {
        parcel1.writeInt(1);
        userHandle.writeToParcel(parcel1, 0);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(90006, parcel1, parcel2, 0);
      parcel2.readException();
      parcel2.readStringList(arrayList);
    } catch (RemoteException remoteException) {
      Log.d("FreezeManager", remoteException.getMessage());
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
    return arrayList;
  }
  
  public List<String> getUserSettingFreezeableApplicationList(UserHandle paramUserHandle) {
    UserHandle userHandle = paramUserHandle;
    if (paramUserHandle == null)
      userHandle = Process.myUserHandle(); 
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    ArrayList<String> arrayList = new ArrayList();
    try {
      parcel1.writeInterfaceToken(this.mDESCRIPTOR);
      if (userHandle != null) {
        parcel1.writeInt(1);
        userHandle.writeToParcel(parcel1, 0);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(90007, parcel1, parcel2, 0);
      parcel2.readException();
      parcel2.readStringList(arrayList);
    } catch (RemoteException remoteException) {
      Log.d("FreezeManager", remoteException.getMessage());
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
    return arrayList;
  }
}
