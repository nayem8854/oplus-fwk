package android.drm;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

@Deprecated
public class DrmRights {
  private String mAccountId;
  
  private byte[] mData;
  
  private String mMimeType;
  
  private String mSubscriptionId;
  
  public DrmRights(String paramString1, String paramString2) {
    File file = new File(paramString1);
    instantiate(file, paramString2);
  }
  
  public DrmRights(String paramString1, String paramString2, String paramString3) {
    this(paramString1, paramString2);
    this.mAccountId = paramString3;
  }
  
  public DrmRights(String paramString1, String paramString2, String paramString3, String paramString4) {
    this(paramString1, paramString2);
    this.mAccountId = paramString3;
    this.mSubscriptionId = paramString4;
  }
  
  public DrmRights(File paramFile, String paramString) {
    instantiate(paramFile, paramString);
  }
  
  private void instantiate(File paramFile, String paramString) {
    try {
      this.mData = DrmUtils.readBytes(paramFile);
    } catch (IOException iOException) {
      iOException.printStackTrace();
    } 
    this.mMimeType = paramString;
    if (isValid())
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("mimeType: ");
    stringBuilder.append(this.mMimeType);
    stringBuilder.append(",data: ");
    byte[] arrayOfByte = this.mData;
    stringBuilder.append(Arrays.toString(arrayOfByte));
    String str = stringBuilder.toString();
    throw new IllegalArgumentException(str);
  }
  
  public DrmRights(ProcessedData paramProcessedData, String paramString) {
    if (paramProcessedData != null) {
      this.mData = paramProcessedData.getData();
      this.mAccountId = paramProcessedData.getAccountId();
      this.mSubscriptionId = paramProcessedData.getSubscriptionId();
      this.mMimeType = paramString;
      if (isValid())
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("mimeType: ");
      stringBuilder.append(this.mMimeType);
      stringBuilder.append(",data: ");
      byte[] arrayOfByte = this.mData;
      stringBuilder.append(Arrays.toString(arrayOfByte));
      String str = stringBuilder.toString();
      throw new IllegalArgumentException(str);
    } 
    throw new IllegalArgumentException("data is null");
  }
  
  public byte[] getData() {
    return this.mData;
  }
  
  public String getMimeType() {
    return this.mMimeType;
  }
  
  public String getAccountId() {
    return this.mAccountId;
  }
  
  public String getSubscriptionId() {
    return this.mSubscriptionId;
  }
  
  boolean isValid() {
    String str = this.mMimeType;
    if (str != null && !str.equals("")) {
      byte[] arrayOfByte = this.mData;
      if (arrayOfByte != null && arrayOfByte.length > 0)
        return true; 
    } 
    return false;
  }
}
