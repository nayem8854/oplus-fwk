package android.drm;

import java.util.ArrayList;
import java.util.Iterator;

@Deprecated
public class DrmSupportInfo {
  private final ArrayList<String> mFileSuffixList = new ArrayList<>();
  
  private final ArrayList<String> mMimeTypeList = new ArrayList<>();
  
  private String mDescription = "";
  
  public void addMimeType(String paramString) {
    if (paramString != null) {
      if (paramString != "") {
        this.mMimeTypeList.add(paramString);
        return;
      } 
      throw new IllegalArgumentException("mimeType is an empty string");
    } 
    throw new IllegalArgumentException("mimeType is null");
  }
  
  public void addFileSuffix(String paramString) {
    if (paramString != "") {
      this.mFileSuffixList.add(paramString);
      return;
    } 
    throw new IllegalArgumentException("fileSuffix is an empty string");
  }
  
  public Iterator<String> getMimeTypeIterator() {
    return this.mMimeTypeList.iterator();
  }
  
  public Iterator<String> getFileSuffixIterator() {
    return this.mFileSuffixList.iterator();
  }
  
  public void setDescription(String paramString) {
    if (paramString != null) {
      if (paramString != "") {
        this.mDescription = paramString;
        return;
      } 
      throw new IllegalArgumentException("description is an empty string");
    } 
    throw new IllegalArgumentException("description is null");
  }
  
  public String getDescriprition() {
    return this.mDescription;
  }
  
  public String getDescription() {
    return this.mDescription;
  }
  
  public int hashCode() {
    return this.mFileSuffixList.hashCode() + this.mMimeTypeList.hashCode() + this.mDescription.hashCode();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof DrmSupportInfo;
    boolean bool1 = false;
    if (bool) {
      paramObject = paramObject;
      if (this.mFileSuffixList.equals(((DrmSupportInfo)paramObject).mFileSuffixList)) {
        ArrayList<String> arrayList1 = this.mMimeTypeList, arrayList2 = ((DrmSupportInfo)paramObject).mMimeTypeList;
        if (arrayList1.equals(arrayList2)) {
          String str = this.mDescription;
          paramObject = ((DrmSupportInfo)paramObject).mDescription;
          if (str.equals(paramObject))
            bool1 = true; 
        } 
      } 
      return bool1;
    } 
    return false;
  }
  
  boolean isSupportedMimeType(String paramString) {
    if (paramString != null && !paramString.equals(""))
      for (byte b = 0; b < this.mMimeTypeList.size(); b++) {
        String str = this.mMimeTypeList.get(b);
        if (str.startsWith(paramString))
          return true; 
      }  
    return false;
  }
  
  boolean isSupportedFileSuffix(String paramString) {
    return this.mFileSuffixList.contains(paramString);
  }
}
