package android.drm;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

@Deprecated
public class DrmUtils {
  static byte[] readBytes(String paramString) throws IOException {
    File file = new File(paramString);
    return readBytes(file);
  }
  
  static byte[] readBytes(File paramFile) throws IOException {
    FileInputStream fileInputStream = new FileInputStream(paramFile);
    BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
    paramFile = null;
    try {
      byte[] arrayOfByte;
      int i = bufferedInputStream.available();
      if (i > 0) {
        arrayOfByte = new byte[i];
        bufferedInputStream.read(arrayOfByte);
      } 
      return arrayOfByte;
    } finally {
      quietlyDispose(bufferedInputStream);
      quietlyDispose(fileInputStream);
    } 
  }
  
  static void writeToFile(String paramString, byte[] paramArrayOfbyte) throws IOException {
    FileOutputStream fileOutputStream = null;
    if (paramString != null && paramArrayOfbyte != null) {
      FileOutputStream fileOutputStream1 = fileOutputStream;
      try {
        FileOutputStream fileOutputStream3 = new FileOutputStream();
        fileOutputStream1 = fileOutputStream;
        this(paramString);
        FileOutputStream fileOutputStream2 = fileOutputStream3;
        fileOutputStream1 = fileOutputStream2;
        fileOutputStream2.write(paramArrayOfbyte);
      } finally {
        quietlyDispose(fileOutputStream1);
      } 
    } 
  }
  
  static void removeFile(String paramString) throws IOException {
    File file = new File(paramString);
    file.delete();
  }
  
  private static void quietlyDispose(Closeable paramCloseable) {
    if (paramCloseable != null)
      try {
        paramCloseable.close();
      } catch (IOException iOException) {} 
  }
  
  public static ExtendedMetadataParser getExtendedMetadataParser(byte[] paramArrayOfbyte) {
    return new ExtendedMetadataParser(paramArrayOfbyte);
  }
  
  public static class ExtendedMetadataParser {
    HashMap<String, String> mMap = new HashMap<>();
    
    private int readByte(byte[] param1ArrayOfbyte, int param1Int) {
      return param1ArrayOfbyte[param1Int];
    }
    
    private String readMultipleBytes(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) {
      byte[] arrayOfByte = new byte[param1Int1];
      int i;
      byte b;
      for (i = param1Int2, b = 0; i < param1Int2 + param1Int1; i++, b++)
        arrayOfByte[b] = param1ArrayOfbyte[i]; 
      return new String(arrayOfByte);
    }
    
    private ExtendedMetadataParser(byte[] param1ArrayOfbyte) {
      int i = 0;
      while (i < param1ArrayOfbyte.length) {
        int j = readByte(param1ArrayOfbyte, i);
        int k = i + 1;
        i = readByte(param1ArrayOfbyte, k);
        k++;
        String str1 = readMultipleBytes(param1ArrayOfbyte, j, k);
        j = k + j;
        String str2 = readMultipleBytes(param1ArrayOfbyte, i, j);
        String str3 = str2;
        if (str2.equals(" "))
          str3 = ""; 
        i = j + i;
        this.mMap.put(str1, str3);
      } 
    }
    
    public Iterator<String> iterator() {
      return this.mMap.values().iterator();
    }
    
    public Iterator<String> keyIterator() {
      return this.mMap.keySet().iterator();
    }
    
    public String get(String param1String) {
      return this.mMap.get(param1String);
    }
  }
}
