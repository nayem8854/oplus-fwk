package android.sax;

import java.util.ArrayList;
import org.xml.sax.Locator;
import org.xml.sax.SAXParseException;

public class Element {
  Children children;
  
  final int depth;
  
  EndElementListener endElementListener;
  
  EndTextElementListener endTextElementListener;
  
  final String localName;
  
  final Element parent;
  
  ArrayList<Element> requiredChilden;
  
  StartElementListener startElementListener;
  
  final String uri;
  
  boolean visited;
  
  Element(Element paramElement, String paramString1, String paramString2, int paramInt) {
    this.parent = paramElement;
    this.uri = paramString1;
    this.localName = paramString2;
    this.depth = paramInt;
  }
  
  public Element getChild(String paramString) {
    return getChild("", paramString);
  }
  
  public Element getChild(String paramString1, String paramString2) {
    if (this.endTextElementListener == null) {
      if (this.children == null)
        this.children = new Children(); 
      return this.children.getOrCreate(this, paramString1, paramString2);
    } 
    throw new IllegalStateException("This element already has an end text element listener. It cannot have children.");
  }
  
  public Element requireChild(String paramString) {
    return requireChild("", paramString);
  }
  
  public Element requireChild(String paramString1, String paramString2) {
    Element element = getChild(paramString1, paramString2);
    ArrayList<Element> arrayList = this.requiredChilden;
    if (arrayList == null) {
      this.requiredChilden = arrayList = new ArrayList<>();
      arrayList.add(element);
    } else if (!arrayList.contains(element)) {
      this.requiredChilden.add(element);
    } 
    return element;
  }
  
  public void setElementListener(ElementListener paramElementListener) {
    setStartElementListener(paramElementListener);
    setEndElementListener(paramElementListener);
  }
  
  public void setTextElementListener(TextElementListener paramTextElementListener) {
    setStartElementListener(paramTextElementListener);
    setEndTextElementListener(paramTextElementListener);
  }
  
  public void setStartElementListener(StartElementListener paramStartElementListener) {
    if (this.startElementListener == null) {
      this.startElementListener = paramStartElementListener;
      return;
    } 
    throw new IllegalStateException("Start element listener has already been set.");
  }
  
  public void setEndElementListener(EndElementListener paramEndElementListener) {
    if (this.endElementListener == null) {
      this.endElementListener = paramEndElementListener;
      return;
    } 
    throw new IllegalStateException("End element listener has already been set.");
  }
  
  public void setEndTextElementListener(EndTextElementListener paramEndTextElementListener) {
    if (this.endTextElementListener == null) {
      if (this.children == null) {
        this.endTextElementListener = paramEndTextElementListener;
        return;
      } 
      throw new IllegalStateException("This element already has children. It cannot have an end text element listener.");
    } 
    throw new IllegalStateException("End text element listener has already been set.");
  }
  
  public String toString() {
    return toString(this.uri, this.localName);
  }
  
  static String toString(String paramString1, String paramString2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("'");
    if (paramString1.equals("")) {
      paramString1 = paramString2;
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString1);
      stringBuilder1.append(":");
      stringBuilder1.append(paramString2);
      paramString1 = stringBuilder1.toString();
    } 
    stringBuilder.append(paramString1);
    stringBuilder.append("'");
    return stringBuilder.toString();
  }
  
  void resetRequiredChildren() {
    ArrayList<Element> arrayList = this.requiredChilden;
    if (arrayList != null)
      for (int i = arrayList.size() - 1; i >= 0; i--)
        ((Element)arrayList.get(i)).visited = false;  
  }
  
  void checkRequiredChildren(Locator paramLocator) throws SAXParseException {
    ArrayList<Element> arrayList = this.requiredChilden;
    if (arrayList != null)
      for (int i = arrayList.size() - 1; i >= 0; ) {
        Element element = arrayList.get(i);
        if (element.visited) {
          i--;
          continue;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Element named ");
        stringBuilder.append(this);
        stringBuilder.append(" is missing required child element named ");
        stringBuilder.append(element);
        stringBuilder.append(".");
        throw new BadXmlException(stringBuilder.toString(), paramLocator);
      }  
  }
}
