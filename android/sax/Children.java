package android.sax;

class Children {
  Child[] children = new Child[16];
  
  Element getOrCreate(Element paramElement, String paramString1, String paramString2) {
    int i = paramString1.hashCode() * 31 + paramString2.hashCode();
    int j = i & 0xF;
    Child child1 = this.children[j];
    Child child2 = child1;
    if (child1 == null) {
      paramElement = new Child(paramElement, paramString1, paramString2, paramElement.depth + 1, i);
      this.children[j] = (Child)paramElement;
      return paramElement;
    } 
    while (true) {
      if (child2.hash == i) {
        String str = child2.uri;
        if (str.compareTo(paramString1) == 0) {
          str = child2.localName;
          if (str.compareTo(paramString2) == 0)
            return child2; 
        } 
      } 
      child1 = child2.next;
      if (child1 == null) {
        paramElement = new Child(paramElement, paramString1, paramString2, paramElement.depth + 1, i);
        child2.next = (Child)paramElement;
        return paramElement;
      } 
      child2 = child1;
    } 
  }
  
  Element get(String paramString1, String paramString2) {
    int i = paramString1.hashCode() * 31 + paramString2.hashCode();
    Child child1 = this.children[i & 0xF];
    Child child2 = child1;
    if (child1 == null)
      return null; 
    do {
      if (child2.hash == i) {
        String str = child2.uri;
        if (str.compareTo(paramString1) == 0) {
          str = child2.localName;
          if (str.compareTo(paramString2) == 0)
            return child2; 
        } 
      } 
      child2 = child2.next;
    } while (child2 != null);
    return null;
  }
  
  class Child extends Element {
    final int hash;
    
    Child next;
    
    Child(Children this$0, String param1String1, String param1String2, int param1Int1, int param1Int2) {
      super((Element)this$0, param1String1, param1String2, param1Int1);
      this.hash = param1Int2;
    }
  }
}
