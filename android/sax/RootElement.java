package android.sax;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class RootElement extends Element {
  final Handler handler = new Handler();
  
  public RootElement(String paramString1, String paramString2) {
    super(null, paramString1, paramString2, 0);
  }
  
  public RootElement(String paramString) {
    this("", paramString);
  }
  
  public ContentHandler getContentHandler() {
    return this.handler;
  }
  
  class Handler extends DefaultHandler {
    final RootElement this$0;
    
    Locator locator;
    
    int depth = -1;
    
    Element current = null;
    
    StringBuilder bodyBuilder = null;
    
    public void setDocumentLocator(Locator param1Locator) {
      this.locator = param1Locator;
    }
    
    public void startElement(String param1String1, String param1String2, String param1String3, Attributes param1Attributes) throws SAXException {
      int i = this.depth + 1;
      if (i == 0) {
        startRoot(param1String1, param1String2, param1Attributes);
        return;
      } 
      if (this.bodyBuilder == null) {
        if (i == this.current.depth + 1) {
          Children children = this.current.children;
          if (children != null) {
            Element element = children.get(param1String1, param1String2);
            if (element != null)
              start(element, param1Attributes); 
          } 
        } 
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Encountered mixed content within text element named ");
      stringBuilder.append(this.current);
      stringBuilder.append(".");
      throw new BadXmlException(stringBuilder.toString(), this.locator);
    }
    
    void startRoot(String param1String1, String param1String2, Attributes param1Attributes) throws SAXException {
      RootElement rootElement = RootElement.this;
      if (rootElement.uri.compareTo(param1String1) == 0) {
        String str = rootElement.localName;
        if (str.compareTo(param1String2) == 0) {
          start(rootElement, param1Attributes);
          return;
        } 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Root element name does not match. Expected: ");
      stringBuilder.append(rootElement);
      stringBuilder.append(", Got: ");
      stringBuilder.append(Element.toString(param1String1, param1String2));
      throw new BadXmlException(stringBuilder.toString(), this.locator);
    }
    
    void start(Element param1Element, Attributes param1Attributes) {
      this.current = param1Element;
      if (param1Element.startElementListener != null)
        param1Element.startElementListener.start(param1Attributes); 
      if (param1Element.endTextElementListener != null)
        this.bodyBuilder = new StringBuilder(); 
      param1Element.resetRequiredChildren();
      param1Element.visited = true;
    }
    
    public void characters(char[] param1ArrayOfchar, int param1Int1, int param1Int2) throws SAXException {
      StringBuilder stringBuilder = this.bodyBuilder;
      if (stringBuilder != null)
        stringBuilder.append(param1ArrayOfchar, param1Int1, param1Int2); 
    }
    
    public void endElement(String param1String1, String param1String2, String param1String3) throws SAXException {
      Element element = this.current;
      if (this.depth == element.depth) {
        element.checkRequiredChildren(this.locator);
        if (element.endElementListener != null)
          element.endElementListener.end(); 
        StringBuilder stringBuilder = this.bodyBuilder;
        if (stringBuilder != null) {
          String str = stringBuilder.toString();
          this.bodyBuilder = null;
          element.endTextElementListener.end(str);
        } 
        this.current = element.parent;
      } 
      this.depth--;
    }
  }
}
