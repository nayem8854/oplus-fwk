package android.onetrace.proto.nano;

public final class Trace {
  public static final long CLIENT_ID = 1138166333455L;
  
  public static final long CUSTOM_METRICS_TYPE = 2237677961234L;
  
  public static final long DEVICE_BOOT_TIMESTAMP = 1116691496966L;
  
  public static final long FINISH_TIMESTAMP = 1116691496963L;
  
  public static final long METRICS_GROUP_TYPE = 1138166333457L;
  
  public static final long OTRACE_VERSION = 1138166333449L;
  
  public static final long PACKET = 2246267895809L;
  
  public static final long RECORD_TIMESTAMP = 1116691496962L;
  
  public static final long REQUEST_ID = 1138166333456L;
  
  public static final long RO_BUILD_DATE_UTC = 1116691496970L;
  
  public static final long RO_BUILD_VERSION_OTA = 1138166333453L;
  
  public static final long RO_MARKET_NAME = 1138166333451L;
  
  public static final long RO_PRODUCT_MODEL = 1138166333452L;
  
  public static final long SYSTEM_INIT_TIMESTAMP = 1116691496967L;
  
  public static final long SYSTEM_SERVER_START_COUNT = 1155346202638L;
  
  public static final long TIME_ZONE = 1138166333444L;
  
  public static final long TIME_ZONE_GMT = 1138166333459L;
}
