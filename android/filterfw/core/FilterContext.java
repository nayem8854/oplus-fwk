package android.filterfw.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class FilterContext {
  private HashMap<String, Frame> mStoredFrames = new HashMap<>();
  
  private Set<FilterGraph> mGraphs = new HashSet<>();
  
  private GLEnvironment mGLEnvironment;
  
  private FrameManager mFrameManager;
  
  public FrameManager getFrameManager() {
    return this.mFrameManager;
  }
  
  public void setFrameManager(FrameManager paramFrameManager) {
    if (paramFrameManager != null) {
      if (paramFrameManager.getContext() == null) {
        this.mFrameManager = paramFrameManager;
        paramFrameManager.setContext(this);
        return;
      } 
      throw new IllegalArgumentException("Attempting to set FrameManager which is already bound to another FilterContext!");
    } 
    throw new NullPointerException("Attempting to set null FrameManager!");
  }
  
  public GLEnvironment getGLEnvironment() {
    return this.mGLEnvironment;
  }
  
  public void initGLEnvironment(GLEnvironment paramGLEnvironment) {
    if (this.mGLEnvironment == null) {
      this.mGLEnvironment = paramGLEnvironment;
      return;
    } 
    throw new RuntimeException("Attempting to re-initialize GL Environment for FilterContext!");
  }
  
  public void storeFrame(String paramString, Frame paramFrame) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual fetchFrame : (Ljava/lang/String;)Landroid/filterfw/core/Frame;
    //   7: astore_3
    //   8: aload_3
    //   9: ifnull -> 17
    //   12: aload_3
    //   13: invokevirtual release : ()Landroid/filterfw/core/Frame;
    //   16: pop
    //   17: aload_2
    //   18: invokevirtual onFrameStore : ()V
    //   21: aload_0
    //   22: getfield mStoredFrames : Ljava/util/HashMap;
    //   25: aload_1
    //   26: aload_2
    //   27: invokevirtual retain : ()Landroid/filterfw/core/Frame;
    //   30: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   33: pop
    //   34: aload_0
    //   35: monitorexit
    //   36: return
    //   37: astore_1
    //   38: aload_0
    //   39: monitorexit
    //   40: aload_1
    //   41: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #72	-> 2
    //   #73	-> 8
    //   #74	-> 12
    //   #76	-> 17
    //   #77	-> 21
    //   #78	-> 34
    //   #71	-> 37
    // Exception table:
    //   from	to	target	type
    //   2	8	37	finally
    //   12	17	37	finally
    //   17	21	37	finally
    //   21	34	37	finally
  }
  
  public Frame fetchFrame(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mStoredFrames : Ljava/util/HashMap;
    //   6: aload_1
    //   7: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   10: checkcast android/filterfw/core/Frame
    //   13: astore_1
    //   14: aload_1
    //   15: ifnull -> 22
    //   18: aload_1
    //   19: invokevirtual onFrameFetch : ()V
    //   22: aload_0
    //   23: monitorexit
    //   24: aload_1
    //   25: areturn
    //   26: astore_1
    //   27: aload_0
    //   28: monitorexit
    //   29: aload_1
    //   30: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #81	-> 2
    //   #82	-> 14
    //   #83	-> 18
    //   #85	-> 22
    //   #80	-> 26
    // Exception table:
    //   from	to	target	type
    //   2	14	26	finally
    //   18	22	26	finally
  }
  
  public void removeFrame(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mStoredFrames : Ljava/util/HashMap;
    //   6: aload_1
    //   7: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   10: checkcast android/filterfw/core/Frame
    //   13: astore_2
    //   14: aload_2
    //   15: ifnull -> 32
    //   18: aload_0
    //   19: getfield mStoredFrames : Ljava/util/HashMap;
    //   22: aload_1
    //   23: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   26: pop
    //   27: aload_2
    //   28: invokevirtual release : ()Landroid/filterfw/core/Frame;
    //   31: pop
    //   32: aload_0
    //   33: monitorexit
    //   34: return
    //   35: astore_1
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_1
    //   39: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #89	-> 2
    //   #90	-> 14
    //   #91	-> 18
    //   #92	-> 27
    //   #94	-> 32
    //   #88	-> 35
    // Exception table:
    //   from	to	target	type
    //   2	14	35	finally
    //   18	27	35	finally
    //   27	32	35	finally
  }
  
  public void tearDown() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mStoredFrames : Ljava/util/HashMap;
    //   6: invokevirtual values : ()Ljava/util/Collection;
    //   9: invokeinterface iterator : ()Ljava/util/Iterator;
    //   14: astore_1
    //   15: aload_1
    //   16: invokeinterface hasNext : ()Z
    //   21: ifeq -> 42
    //   24: aload_1
    //   25: invokeinterface next : ()Ljava/lang/Object;
    //   30: checkcast android/filterfw/core/Frame
    //   33: astore_2
    //   34: aload_2
    //   35: invokevirtual release : ()Landroid/filterfw/core/Frame;
    //   38: pop
    //   39: goto -> 15
    //   42: aload_0
    //   43: getfield mStoredFrames : Ljava/util/HashMap;
    //   46: invokevirtual clear : ()V
    //   49: aload_0
    //   50: getfield mGraphs : Ljava/util/Set;
    //   53: invokeinterface iterator : ()Ljava/util/Iterator;
    //   58: astore_1
    //   59: aload_1
    //   60: invokeinterface hasNext : ()Z
    //   65: ifeq -> 86
    //   68: aload_1
    //   69: invokeinterface next : ()Ljava/lang/Object;
    //   74: checkcast android/filterfw/core/FilterGraph
    //   77: astore_2
    //   78: aload_2
    //   79: aload_0
    //   80: invokevirtual tearDown : (Landroid/filterfw/core/FilterContext;)V
    //   83: goto -> 59
    //   86: aload_0
    //   87: getfield mGraphs : Ljava/util/Set;
    //   90: invokeinterface clear : ()V
    //   95: aload_0
    //   96: getfield mFrameManager : Landroid/filterfw/core/FrameManager;
    //   99: ifnull -> 114
    //   102: aload_0
    //   103: getfield mFrameManager : Landroid/filterfw/core/FrameManager;
    //   106: invokevirtual tearDown : ()V
    //   109: aload_0
    //   110: aconst_null
    //   111: putfield mFrameManager : Landroid/filterfw/core/FrameManager;
    //   114: aload_0
    //   115: getfield mGLEnvironment : Landroid/filterfw/core/GLEnvironment;
    //   118: ifnull -> 133
    //   121: aload_0
    //   122: getfield mGLEnvironment : Landroid/filterfw/core/GLEnvironment;
    //   125: invokevirtual tearDown : ()V
    //   128: aload_0
    //   129: aconst_null
    //   130: putfield mGLEnvironment : Landroid/filterfw/core/GLEnvironment;
    //   133: aload_0
    //   134: monitorexit
    //   135: return
    //   136: astore_2
    //   137: aload_0
    //   138: monitorexit
    //   139: aload_2
    //   140: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #98	-> 2
    //   #99	-> 34
    //   #100	-> 39
    //   #101	-> 42
    //   #104	-> 49
    //   #105	-> 78
    //   #106	-> 83
    //   #107	-> 86
    //   #110	-> 95
    //   #111	-> 102
    //   #112	-> 109
    //   #116	-> 114
    //   #117	-> 121
    //   #118	-> 128
    //   #120	-> 133
    //   #97	-> 136
    // Exception table:
    //   from	to	target	type
    //   2	15	136	finally
    //   15	34	136	finally
    //   34	39	136	finally
    //   42	49	136	finally
    //   49	59	136	finally
    //   59	78	136	finally
    //   78	83	136	finally
    //   86	95	136	finally
    //   95	102	136	finally
    //   102	109	136	finally
    //   109	114	136	finally
    //   114	121	136	finally
    //   121	128	136	finally
    //   128	133	136	finally
  }
  
  final void addGraph(FilterGraph paramFilterGraph) {
    this.mGraphs.add(paramFilterGraph);
  }
  
  public static interface OnFrameReceivedListener {
    void onFrameReceived(Filter param1Filter, Frame param1Frame, Object param1Object);
  }
}
