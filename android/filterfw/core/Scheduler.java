package android.filterfw.core;

public abstract class Scheduler {
  private FilterGraph mGraph;
  
  Scheduler(FilterGraph paramFilterGraph) {
    this.mGraph = paramFilterGraph;
  }
  
  FilterGraph getGraph() {
    return this.mGraph;
  }
  
  boolean finished() {
    return true;
  }
  
  abstract void reset();
  
  abstract Filter scheduleNextNode();
}
