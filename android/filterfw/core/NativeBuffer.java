package android.filterfw.core;

public class NativeBuffer {
  private long mDataPointer = 0L;
  
  private int mSize = 0;
  
  private boolean mOwnsData = false;
  
  private int mRefCount = 1;
  
  private Frame mAttachedFrame;
  
  public NativeBuffer(int paramInt) {
    allocate(getElementSize() * paramInt);
    this.mOwnsData = true;
  }
  
  public NativeBuffer mutableCopy() {
    try {
      Class<?> clazz = getClass();
      NativeBuffer nativeBuffer = (NativeBuffer)clazz.newInstance();
      if (this.mSize <= 0 || nativeCopyTo(nativeBuffer))
        return nativeBuffer; 
      throw new RuntimeException("Failed to copy NativeBuffer to mutable instance!");
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to allocate a copy of ");
      stringBuilder.append(getClass());
      stringBuilder.append("! Make sure the class has a default constructor!");
      throw new RuntimeException(stringBuilder.toString());
    } 
  }
  
  public int size() {
    return this.mSize;
  }
  
  public int count() {
    boolean bool;
    if (this.mDataPointer != 0L) {
      bool = this.mSize / getElementSize();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getElementSize() {
    return 1;
  }
  
  public NativeBuffer retain() {
    Frame frame = this.mAttachedFrame;
    if (frame != null) {
      frame.retain();
    } else if (this.mOwnsData) {
      this.mRefCount++;
    } 
    return this;
  }
  
  public NativeBuffer release() {
    int i = 0;
    Frame frame = this.mAttachedFrame;
    boolean bool = false;
    int j = 0;
    if (frame != null) {
      if (frame.release() == null)
        j = 1; 
    } else {
      j = i;
      if (this.mOwnsData) {
        this.mRefCount = i = this.mRefCount - 1;
        j = bool;
        if (i == 0)
          j = 1; 
      } 
    } 
    if (j != 0) {
      deallocate(this.mOwnsData);
      return null;
    } 
    return this;
  }
  
  public boolean isReadOnly() {
    boolean bool;
    Frame frame = this.mAttachedFrame;
    if (frame != null) {
      bool = frame.isReadOnly();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  static {
    System.loadLibrary("filterfw");
  }
  
  void attachToFrame(Frame paramFrame) {
    this.mAttachedFrame = paramFrame;
  }
  
  protected void assertReadable() {
    if (this.mDataPointer != 0L && this.mSize != 0) {
      Frame frame = this.mAttachedFrame;
      if (frame == null || 
        frame.hasNativeAllocation())
        return; 
    } 
    throw new NullPointerException("Attempting to read from null data frame!");
  }
  
  protected void assertWritable() {
    if (!isReadOnly())
      return; 
    throw new RuntimeException("Attempting to modify read-only native (structured) data!");
  }
  
  public NativeBuffer() {}
  
  private native boolean allocate(int paramInt);
  
  private native boolean deallocate(boolean paramBoolean);
  
  private native boolean nativeCopyTo(NativeBuffer paramNativeBuffer);
}
