package android.filterfw.core;

import android.os.SystemClock;
import android.util.Log;

class StopWatch {
  private int STOP_WATCH_LOGGING_PERIOD = 200;
  
  private String TAG = "MFF";
  
  private String mName;
  
  private int mNumCalls;
  
  private long mStartTime;
  
  private long mTotalTime;
  
  public StopWatch(String paramString) {
    this.mName = paramString;
    this.mStartTime = -1L;
    this.mTotalTime = 0L;
    this.mNumCalls = 0;
  }
  
  public void start() {
    if (this.mStartTime == -1L) {
      this.mStartTime = SystemClock.elapsedRealtime();
      return;
    } 
    throw new RuntimeException("Calling start with StopWatch already running");
  }
  
  public void stop() {
    if (this.mStartTime != -1L) {
      long l = SystemClock.elapsedRealtime();
      this.mTotalTime += l - this.mStartTime;
      int i = this.mNumCalls + 1;
      this.mStartTime = -1L;
      if (i % this.STOP_WATCH_LOGGING_PERIOD == 0) {
        String str1 = this.TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("AVG ms/call ");
        stringBuilder.append(this.mName);
        stringBuilder.append(": ");
        float f = (float)this.mTotalTime * 1.0F / this.mNumCalls;
        stringBuilder.append(String.format("%.1f", new Object[] { Float.valueOf(f) }));
        String str2 = stringBuilder.toString();
        Log.i(str1, str2);
        this.mTotalTime = 0L;
        this.mNumCalls = 0;
      } 
      return;
    } 
    throw new RuntimeException("Calling stop with StopWatch already stopped");
  }
}
