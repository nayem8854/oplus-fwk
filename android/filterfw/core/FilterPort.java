package android.filterfw.core;

import android.util.Log;

public abstract class FilterPort {
  protected boolean mIsBlocking = true;
  
  protected boolean mIsOpen = false;
  
  protected boolean mChecksType = false;
  
  private static final String TAG = "FilterPort";
  
  protected Filter mFilter;
  
  private boolean mLogVerbose;
  
  protected String mName;
  
  protected FrameFormat mPortFormat;
  
  public FilterPort(Filter paramFilter, String paramString) {
    this.mName = paramString;
    this.mFilter = paramFilter;
    this.mLogVerbose = Log.isLoggable("FilterPort", 2);
  }
  
  public boolean isAttached() {
    boolean bool;
    if (this.mFilter != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public FrameFormat getPortFormat() {
    return this.mPortFormat;
  }
  
  public void setPortFormat(FrameFormat paramFrameFormat) {
    this.mPortFormat = paramFrameFormat;
  }
  
  public Filter getFilter() {
    return this.mFilter;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public void setBlocking(boolean paramBoolean) {
    this.mIsBlocking = paramBoolean;
  }
  
  public void setChecksType(boolean paramBoolean) {
    this.mChecksType = paramBoolean;
  }
  
  public void open() {
    if (!this.mIsOpen && 
      this.mLogVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Opening ");
      stringBuilder.append(this);
      Log.v("FilterPort", stringBuilder.toString());
    } 
    this.mIsOpen = true;
  }
  
  public void close() {
    if (this.mIsOpen && 
      this.mLogVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Closing ");
      stringBuilder.append(this);
      Log.v("FilterPort", stringBuilder.toString());
    } 
    this.mIsOpen = false;
  }
  
  public boolean isOpen() {
    return this.mIsOpen;
  }
  
  public boolean isBlocking() {
    return this.mIsBlocking;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("port '");
    stringBuilder.append(this.mName);
    stringBuilder.append("' of ");
    stringBuilder.append(this.mFilter);
    return stringBuilder.toString();
  }
  
  protected void assertPortIsOpen() {
    if (isOpen())
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Illegal operation on closed ");
    stringBuilder.append(this);
    stringBuilder.append("!");
    throw new RuntimeException(stringBuilder.toString());
  }
  
  protected void checkFrameType(Frame paramFrame, boolean paramBoolean) {
    if ((this.mChecksType || paramBoolean) && this.mPortFormat != null)
      if (!paramFrame.getFormat().isCompatibleWith(this.mPortFormat)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Frame passed to ");
        stringBuilder.append(this);
        stringBuilder.append(" is of incorrect type! Expected ");
        stringBuilder.append(this.mPortFormat);
        stringBuilder.append(" but got ");
        stringBuilder.append(paramFrame.getFormat());
        throw new RuntimeException(stringBuilder.toString());
      }  
  }
  
  protected void checkFrameManager(Frame paramFrame, FilterContext paramFilterContext) {
    if (paramFrame.getFrameManager() == null || 
      paramFrame.getFrameManager() == paramFilterContext.getFrameManager())
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Frame ");
    stringBuilder.append(paramFrame);
    stringBuilder.append(" is managed by foreign FrameManager! ");
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public abstract void clear();
  
  public abstract boolean filterMustClose();
  
  public abstract boolean hasFrame();
  
  public abstract boolean isReady();
  
  public abstract Frame pullFrame();
  
  public abstract void pushFrame(Frame paramFrame);
  
  public abstract void setFrame(Frame paramFrame);
}
