package android.filterfw.core;

import java.lang.reflect.Field;

public class ProgramPort extends FieldPort {
  protected String mVarName;
  
  public ProgramPort(Filter paramFilter, String paramString1, String paramString2, Field paramField, boolean paramBoolean) {
    super(paramFilter, paramString1, paramField, paramBoolean);
    this.mVarName = paramString2;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Program ");
    stringBuilder.append(super.toString());
    return stringBuilder.toString();
  }
  
  public void transfer(FilterContext paramFilterContext) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mValueWaiting : Z
    //   6: istore_2
    //   7: iload_2
    //   8: ifeq -> 158
    //   11: aload_0
    //   12: getfield mField : Ljava/lang/reflect/Field;
    //   15: aload_0
    //   16: getfield mFilter : Landroid/filterfw/core/Filter;
    //   19: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   22: astore_1
    //   23: aload_1
    //   24: ifnull -> 49
    //   27: aload_1
    //   28: checkcast android/filterfw/core/Program
    //   31: astore_1
    //   32: aload_1
    //   33: aload_0
    //   34: getfield mVarName : Ljava/lang/String;
    //   37: aload_0
    //   38: getfield mValue : Ljava/lang/Object;
    //   41: invokevirtual setHostValue : (Ljava/lang/String;Ljava/lang/Object;)V
    //   44: aload_0
    //   45: iconst_0
    //   46: putfield mValueWaiting : Z
    //   49: goto -> 158
    //   52: astore_1
    //   53: new java/lang/RuntimeException
    //   56: astore_3
    //   57: new java/lang/StringBuilder
    //   60: astore_1
    //   61: aload_1
    //   62: invokespecial <init> : ()V
    //   65: aload_1
    //   66: ldc 'Non Program field ''
    //   68: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   71: pop
    //   72: aload_1
    //   73: aload_0
    //   74: getfield mField : Ljava/lang/reflect/Field;
    //   77: invokevirtual getName : ()Ljava/lang/String;
    //   80: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   83: pop
    //   84: aload_1
    //   85: ldc '' annotated with ProgramParameter!'
    //   87: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   90: pop
    //   91: aload_3
    //   92: aload_1
    //   93: invokevirtual toString : ()Ljava/lang/String;
    //   96: invokespecial <init> : (Ljava/lang/String;)V
    //   99: aload_3
    //   100: athrow
    //   101: astore_1
    //   102: new java/lang/RuntimeException
    //   105: astore_1
    //   106: new java/lang/StringBuilder
    //   109: astore #4
    //   111: aload #4
    //   113: invokespecial <init> : ()V
    //   116: aload #4
    //   118: ldc 'Access to program field ''
    //   120: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   123: pop
    //   124: aload_0
    //   125: getfield mField : Ljava/lang/reflect/Field;
    //   128: astore_3
    //   129: aload #4
    //   131: aload_3
    //   132: invokevirtual getName : ()Ljava/lang/String;
    //   135: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: aload #4
    //   141: ldc '' was denied!'
    //   143: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   146: pop
    //   147: aload_1
    //   148: aload #4
    //   150: invokevirtual toString : ()Ljava/lang/String;
    //   153: invokespecial <init> : (Ljava/lang/String;)V
    //   156: aload_1
    //   157: athrow
    //   158: aload_0
    //   159: monitorexit
    //   160: return
    //   161: astore_1
    //   162: aload_0
    //   163: monitorexit
    //   164: aload_1
    //   165: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #45	-> 2
    //   #47	-> 11
    //   #48	-> 23
    //   #49	-> 27
    //   #50	-> 32
    //   #51	-> 44
    //   #59	-> 49
    //   #56	-> 52
    //   #57	-> 53
    //   #53	-> 101
    //   #54	-> 102
    //   #55	-> 129
    //   #61	-> 158
    //   #44	-> 161
    // Exception table:
    //   from	to	target	type
    //   2	7	161	finally
    //   11	23	101	java/lang/IllegalAccessException
    //   11	23	52	java/lang/ClassCastException
    //   11	23	161	finally
    //   27	32	101	java/lang/IllegalAccessException
    //   27	32	52	java/lang/ClassCastException
    //   27	32	161	finally
    //   32	44	101	java/lang/IllegalAccessException
    //   32	44	52	java/lang/ClassCastException
    //   32	44	161	finally
    //   44	49	101	java/lang/IllegalAccessException
    //   44	49	52	java/lang/ClassCastException
    //   44	49	161	finally
    //   53	101	161	finally
    //   102	129	161	finally
    //   129	158	161	finally
  }
}
