package android.filterfw.core;

import java.util.Random;
import java.util.Vector;

public class RandomScheduler extends Scheduler {
  private Random mRand = new Random();
  
  public RandomScheduler(FilterGraph paramFilterGraph) {
    super(paramFilterGraph);
  }
  
  public void reset() {}
  
  public Filter scheduleNextNode() {
    Vector<Filter> vector = new Vector();
    for (Filter filter : getGraph().getFilters()) {
      if (filter.canProcess())
        vector.add(filter); 
    } 
    if (vector.size() > 0) {
      int i = this.mRand.nextInt(vector.size());
      return vector.elementAt(i);
    } 
    return null;
  }
}
