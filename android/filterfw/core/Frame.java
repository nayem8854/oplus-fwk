package android.filterfw.core;

import android.graphics.Bitmap;
import java.nio.ByteBuffer;

public abstract class Frame {
  private boolean mReadOnly = false;
  
  private boolean mReusable = false;
  
  private int mRefCount = 1;
  
  private int mBindingType = 0;
  
  private long mBindingId = 0L;
  
  private long mTimestamp = -2L;
  
  public static final int NO_BINDING = 0;
  
  public static final long TIMESTAMP_NOT_SET = -2L;
  
  public static final long TIMESTAMP_UNKNOWN = -1L;
  
  private FrameFormat mFormat;
  
  private FrameManager mFrameManager;
  
  Frame(FrameFormat paramFrameFormat, FrameManager paramFrameManager) {
    this.mFormat = paramFrameFormat.mutableCopy();
    this.mFrameManager = paramFrameManager;
  }
  
  Frame(FrameFormat paramFrameFormat, FrameManager paramFrameManager, int paramInt, long paramLong) {
    this.mFormat = paramFrameFormat.mutableCopy();
    this.mFrameManager = paramFrameManager;
    this.mBindingType = paramInt;
    this.mBindingId = paramLong;
  }
  
  public FrameFormat getFormat() {
    return this.mFormat;
  }
  
  public int getCapacity() {
    return getFormat().getSize();
  }
  
  public boolean isReadOnly() {
    return this.mReadOnly;
  }
  
  public int getBindingType() {
    return this.mBindingType;
  }
  
  public long getBindingId() {
    return this.mBindingId;
  }
  
  public void setObjectValue(Object paramObject) {
    assertFrameMutable();
    if (paramObject instanceof int[]) {
      setInts((int[])paramObject);
    } else if (paramObject instanceof float[]) {
      setFloats((float[])paramObject);
    } else if (paramObject instanceof ByteBuffer) {
      setData((ByteBuffer)paramObject);
    } else if (paramObject instanceof Bitmap) {
      setBitmap((Bitmap)paramObject);
    } else {
      setGenericObjectValue(paramObject);
    } 
  }
  
  public void setData(ByteBuffer paramByteBuffer) {
    setData(paramByteBuffer, 0, paramByteBuffer.limit());
  }
  
  public void setData(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    setData(ByteBuffer.wrap(paramArrayOfbyte, paramInt1, paramInt2));
  }
  
  public void setTimestamp(long paramLong) {
    this.mTimestamp = paramLong;
  }
  
  public long getTimestamp() {
    return this.mTimestamp;
  }
  
  public void setDataFromFrame(Frame paramFrame) {
    setData(paramFrame.getData());
  }
  
  protected boolean requestResize(int[] paramArrayOfint) {
    return false;
  }
  
  public int getRefCount() {
    return this.mRefCount;
  }
  
  public Frame release() {
    FrameManager frameManager = this.mFrameManager;
    if (frameManager != null)
      return frameManager.releaseFrame(this); 
    return this;
  }
  
  public Frame retain() {
    FrameManager frameManager = this.mFrameManager;
    if (frameManager != null)
      return frameManager.retainFrame(this); 
    return this;
  }
  
  public FrameManager getFrameManager() {
    return this.mFrameManager;
  }
  
  protected void assertFrameMutable() {
    if (!isReadOnly())
      return; 
    throw new RuntimeException("Attempting to modify read-only frame!");
  }
  
  protected void setReusable(boolean paramBoolean) {
    this.mReusable = paramBoolean;
  }
  
  protected void setFormat(FrameFormat paramFrameFormat) {
    this.mFormat = paramFrameFormat.mutableCopy();
  }
  
  protected void setGenericObjectValue(Object paramObject) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Cannot set object value of unsupported type: ");
    stringBuilder.append(paramObject.getClass());
    throw new RuntimeException(stringBuilder.toString());
  }
  
  protected static Bitmap convertBitmapToRGBA(Bitmap paramBitmap) {
    if (paramBitmap.getConfig() == Bitmap.Config.ARGB_8888)
      return paramBitmap; 
    paramBitmap = paramBitmap.copy(Bitmap.Config.ARGB_8888, false);
    if (paramBitmap != null) {
      if (paramBitmap.getRowBytes() == paramBitmap.getWidth() * 4)
        return paramBitmap; 
      throw new RuntimeException("Unsupported row byte count in bitmap!");
    } 
    throw new RuntimeException("Error converting bitmap to RGBA!");
  }
  
  protected void reset(FrameFormat paramFrameFormat) {
    this.mFormat = paramFrameFormat.mutableCopy();
    this.mReadOnly = false;
    this.mRefCount = 1;
  }
  
  protected void onFrameStore() {}
  
  protected void onFrameFetch() {}
  
  final int incRefCount() {
    int i = this.mRefCount + 1;
    return i;
  }
  
  final int decRefCount() {
    int i = this.mRefCount - 1;
    return i;
  }
  
  final boolean isReusable() {
    return this.mReusable;
  }
  
  final void markReadOnly() {
    this.mReadOnly = true;
  }
  
  public abstract Bitmap getBitmap();
  
  public abstract ByteBuffer getData();
  
  public abstract float[] getFloats();
  
  public abstract int[] getInts();
  
  public abstract Object getObjectValue();
  
  protected abstract boolean hasNativeAllocation();
  
  protected abstract void releaseNativeAllocation();
  
  public abstract void setBitmap(Bitmap paramBitmap);
  
  public abstract void setData(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2);
  
  public abstract void setFloats(float[] paramArrayOffloat);
  
  public abstract void setInts(int[] paramArrayOfint);
}
