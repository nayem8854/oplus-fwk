package android.filterfw.core;

public class NativeProgram extends Program {
  private boolean mHasGetValueFunction;
  
  private boolean mHasInitFunction;
  
  private boolean mHasResetFunction;
  
  private boolean mHasSetValueFunction;
  
  private boolean mHasTeardownFunction;
  
  private boolean mTornDown;
  
  private int nativeProgramId;
  
  public NativeProgram(String paramString1, String paramString2) {
    String str;
    this.mHasInitFunction = false;
    this.mHasTeardownFunction = false;
    this.mHasSetValueFunction = false;
    this.mHasGetValueFunction = false;
    this.mHasResetFunction = false;
    this.mTornDown = false;
    allocate();
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("lib");
    stringBuilder2.append(paramString1);
    stringBuilder2.append(".so");
    paramString1 = stringBuilder2.toString();
    if (openNativeLibrary(paramString1)) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(paramString2);
      stringBuilder2.append("_process");
      String str1 = stringBuilder2.toString();
      if (bindProcessFunction(str1)) {
        StringBuilder stringBuilder7 = new StringBuilder();
        stringBuilder7.append(paramString2);
        stringBuilder7.append("_init");
        String str5 = stringBuilder7.toString();
        this.mHasInitFunction = bindInitFunction(str5);
        StringBuilder stringBuilder6 = new StringBuilder();
        stringBuilder6.append(paramString2);
        stringBuilder6.append("_teardown");
        String str4 = stringBuilder6.toString();
        this.mHasTeardownFunction = bindTeardownFunction(str4);
        StringBuilder stringBuilder5 = new StringBuilder();
        stringBuilder5.append(paramString2);
        stringBuilder5.append("_setvalue");
        String str3 = stringBuilder5.toString();
        this.mHasSetValueFunction = bindSetValueFunction(str3);
        StringBuilder stringBuilder4 = new StringBuilder();
        stringBuilder4.append(paramString2);
        stringBuilder4.append("_getvalue");
        String str2 = stringBuilder4.toString();
        this.mHasGetValueFunction = bindGetValueFunction(str2);
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append(paramString2);
        stringBuilder3.append("_reset");
        str = stringBuilder3.toString();
        this.mHasResetFunction = bindResetFunction(str);
        if (!this.mHasInitFunction || callNativeInit())
          return; 
        throw new RuntimeException("Could not initialize NativeProgram!");
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Could not find native program function name ");
      stringBuilder.append(str1);
      stringBuilder.append(" in library ");
      stringBuilder.append(str);
      stringBuilder.append("! This function is required!");
      throw new RuntimeException(stringBuilder.toString());
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Could not find native library named '");
    stringBuilder1.append(str);
    stringBuilder1.append("' required for native program!");
    throw new RuntimeException(stringBuilder1.toString());
  }
  
  public void tearDown() {
    if (this.mTornDown)
      return; 
    if (!this.mHasTeardownFunction || callNativeTeardown()) {
      deallocate();
      this.mTornDown = true;
      return;
    } 
    throw new RuntimeException("Could not tear down NativeProgram!");
  }
  
  public void reset() {
    if (!this.mHasResetFunction || callNativeReset())
      return; 
    throw new RuntimeException("Could not reset NativeProgram!");
  }
  
  protected void finalize() throws Throwable {
    tearDown();
  }
  
  public void process(Frame[] paramArrayOfFrame, Frame paramFrame) {
    if (!this.mTornDown) {
      NativeFrame[] arrayOfNativeFrame = new NativeFrame[paramArrayOfFrame.length];
      for (byte b = 0; b < paramArrayOfFrame.length; ) {
        if (paramArrayOfFrame[b] == null || paramArrayOfFrame[b] instanceof NativeFrame) {
          arrayOfNativeFrame[b] = (NativeFrame)paramArrayOfFrame[b];
          b++;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("NativeProgram got non-native frame as input ");
        stringBuilder.append(b);
        stringBuilder.append("!");
        throw new RuntimeException(stringBuilder.toString());
      } 
      if (paramFrame == null || paramFrame instanceof NativeFrame) {
        NativeFrame nativeFrame = (NativeFrame)paramFrame;
        if (callNativeProcess(arrayOfNativeFrame, nativeFrame))
          return; 
        throw new RuntimeException("Calling native process() caused error!");
      } 
      throw new RuntimeException("NativeProgram got non-native output frame!");
    } 
    throw new RuntimeException("NativeProgram already torn down!");
  }
  
  public void setHostValue(String paramString, Object paramObject) {
    if (!this.mTornDown) {
      if (this.mHasSetValueFunction) {
        if (callNativeSetValue(paramString, paramObject.toString()))
          return; 
        paramObject = new StringBuilder();
        paramObject.append("Error setting native value for variable '");
        paramObject.append(paramString);
        paramObject.append("'!");
        throw new RuntimeException(paramObject.toString());
      } 
      throw new RuntimeException("Attempting to set native variable, but native code does not define native setvalue function!");
    } 
    throw new RuntimeException("NativeProgram already torn down!");
  }
  
  public Object getHostValue(String paramString) {
    if (!this.mTornDown) {
      if (this.mHasGetValueFunction)
        return callNativeGetValue(paramString); 
      throw new RuntimeException("Attempting to get native variable, but native code does not define native getvalue function!");
    } 
    throw new RuntimeException("NativeProgram already torn down!");
  }
  
  static {
    System.loadLibrary("filterfw");
  }
  
  private native boolean allocate();
  
  private native boolean bindGetValueFunction(String paramString);
  
  private native boolean bindInitFunction(String paramString);
  
  private native boolean bindProcessFunction(String paramString);
  
  private native boolean bindResetFunction(String paramString);
  
  private native boolean bindSetValueFunction(String paramString);
  
  private native boolean bindTeardownFunction(String paramString);
  
  private native String callNativeGetValue(String paramString);
  
  private native boolean callNativeInit();
  
  private native boolean callNativeProcess(NativeFrame[] paramArrayOfNativeFrame, NativeFrame paramNativeFrame);
  
  private native boolean callNativeReset();
  
  private native boolean callNativeSetValue(String paramString1, String paramString2);
  
  private native boolean callNativeTeardown();
  
  private native boolean deallocate();
  
  private native boolean nativeInit();
  
  private native boolean openNativeLibrary(String paramString);
}
