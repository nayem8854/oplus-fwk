package android.filterfw.io;

import android.content.Context;
import android.filterfw.core.FilterGraph;
import android.filterfw.core.KeyValueMap;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

public abstract class GraphReader {
  protected KeyValueMap mReferences = new KeyValueMap();
  
  public FilterGraph readGraphResource(Context paramContext, int paramInt) throws GraphIOException {
    InputStream inputStream = paramContext.getResources().openRawResource(paramInt);
    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
    StringWriter stringWriter = new StringWriter();
    char[] arrayOfChar = new char[1024];
    try {
      while (true) {
        paramInt = inputStreamReader.read(arrayOfChar, 0, 1024);
        if (paramInt > 0) {
          stringWriter.write(arrayOfChar, 0, paramInt);
          continue;
        } 
        break;
      } 
      return readGraphString(stringWriter.toString());
    } catch (IOException iOException) {
      throw new RuntimeException("Could not read specified resource file!");
    } 
  }
  
  public void addReference(String paramString, Object paramObject) {
    this.mReferences.put(paramString, paramObject);
  }
  
  public void addReferencesByMap(KeyValueMap paramKeyValueMap) {
    this.mReferences.putAll(paramKeyValueMap);
  }
  
  public void addReferencesByKeysAndValues(Object... paramVarArgs) {
    this.mReferences.setKeyValues(paramVarArgs);
  }
  
  public abstract FilterGraph readGraphString(String paramString) throws GraphIOException;
  
  public abstract KeyValueMap readKeyValueAssignments(String paramString) throws GraphIOException;
}
