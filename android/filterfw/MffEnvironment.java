package android.filterfw;

import android.filterfw.core.CachedFrameManager;
import android.filterfw.core.FilterContext;
import android.filterfw.core.FrameManager;
import android.filterfw.core.GLEnvironment;

public class MffEnvironment {
  private FilterContext mContext;
  
  protected MffEnvironment(FrameManager paramFrameManager) {
    FrameManager frameManager = paramFrameManager;
    if (paramFrameManager == null)
      frameManager = new CachedFrameManager(); 
    FilterContext filterContext = new FilterContext();
    filterContext.setFrameManager(frameManager);
  }
  
  public FilterContext getContext() {
    return this.mContext;
  }
  
  public void setGLEnvironment(GLEnvironment paramGLEnvironment) {
    this.mContext.initGLEnvironment(paramGLEnvironment);
  }
  
  public void createGLEnvironment() {
    GLEnvironment gLEnvironment = new GLEnvironment();
    gLEnvironment.initWithNewContext();
    setGLEnvironment(gLEnvironment);
  }
  
  public void activateGLEnvironment() {
    GLEnvironment gLEnvironment = this.mContext.getGLEnvironment();
    if (gLEnvironment != null) {
      this.mContext.getGLEnvironment().activate();
      return;
    } 
    throw new NullPointerException("No GLEnvironment in place to activate!");
  }
  
  public void deactivateGLEnvironment() {
    GLEnvironment gLEnvironment = this.mContext.getGLEnvironment();
    if (gLEnvironment != null) {
      this.mContext.getGLEnvironment().deactivate();
      return;
    } 
    throw new NullPointerException("No GLEnvironment in place to deactivate!");
  }
}
