package android.filterfw;

import android.filterfw.core.Filter;
import android.filterfw.core.FilterFactory;
import android.filterfw.core.FilterFunction;
import android.filterfw.core.FrameManager;

public class FilterFunctionEnvironment extends MffEnvironment {
  public FilterFunctionEnvironment() {
    super(null);
  }
  
  public FilterFunctionEnvironment(FrameManager paramFrameManager) {
    super(paramFrameManager);
  }
  
  public FilterFunction createFunction(Class paramClass, Object... paramVarArgs) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("FilterFunction(");
    stringBuilder.append(paramClass.getSimpleName());
    stringBuilder.append(")");
    String str = stringBuilder.toString();
    Filter filter = FilterFactory.sharedFactory().createFilterByClass(paramClass, str);
    filter.initWithAssignmentList(paramVarArgs);
    return new FilterFunction(getContext(), filter);
  }
}
