package android.printservice.recommendation;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import java.util.List;

@SystemApi
public abstract class RecommendationService extends Service {
  private static final String LOG_TAG = "PrintServiceRecS";
  
  public static final String SERVICE_INTERFACE = "android.printservice.recommendation.RecommendationService";
  
  private IRecommendationServiceCallbacks mCallbacks;
  
  private Handler mHandler;
  
  protected void attachBaseContext(Context paramContext) {
    super.attachBaseContext(paramContext);
    this.mHandler = new MyHandler();
  }
  
  public final void updateRecommendations(List<RecommendationInfo> paramList) {
    this.mHandler.obtainMessage(3, paramList).sendToTarget();
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return new IRecommendationService.Stub() {
        final RecommendationService this$0;
        
        public void registerCallbacks(IRecommendationServiceCallbacks param1IRecommendationServiceCallbacks) {
          if (param1IRecommendationServiceCallbacks != null) {
            RecommendationService.this.mHandler.obtainMessage(1, param1IRecommendationServiceCallbacks).sendToTarget();
          } else {
            RecommendationService.this.mHandler.obtainMessage(2).sendToTarget();
          } 
        }
      };
  }
  
  public abstract void onConnected();
  
  public abstract void onDisconnected();
  
  class MyHandler extends Handler {
    static final int MSG_CONNECT = 1;
    
    static final int MSG_DISCONNECT = 2;
    
    static final int MSG_UPDATE = 3;
    
    final RecommendationService this$0;
    
    MyHandler() {
      super(Looper.getMainLooper());
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 1) {
        if (i != 2) {
          if (i == 3)
            if (RecommendationService.this.mCallbacks != null)
              try {
                RecommendationService.this.mCallbacks.onRecommendationsUpdated((List<RecommendationInfo>)param1Message.obj);
              } catch (RemoteException|NullPointerException remoteException) {
                Log.e("PrintServiceRecS", "Could not update recommended services", (Throwable)remoteException);
              }   
        } else {
          RecommendationService.this.onDisconnected();
          RecommendationService.access$102(RecommendationService.this, null);
        } 
      } else {
        RecommendationService.access$102(RecommendationService.this, (IRecommendationServiceCallbacks)((Message)remoteException).obj);
        RecommendationService.this.onConnected();
      } 
    }
  }
}
