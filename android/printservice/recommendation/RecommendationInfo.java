package android.printservice.recommendation;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

@SystemApi
public final class RecommendationInfo implements Parcelable {
  public RecommendationInfo(CharSequence paramCharSequence1, CharSequence paramCharSequence2, List<InetAddress> paramList, boolean paramBoolean) {
    this.mPackageName = Preconditions.checkStringNotEmpty(paramCharSequence1);
    this.mName = Preconditions.checkStringNotEmpty(paramCharSequence2);
    this.mDiscoveredPrinters = (List<InetAddress>)Preconditions.checkCollectionElementsNotNull(paramList, "discoveredPrinters");
    this.mRecommendsMultiVendorService = paramBoolean;
  }
  
  @Deprecated
  public RecommendationInfo(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt, boolean paramBoolean) {
    throw new IllegalArgumentException("This constructor has been deprecated");
  }
  
  private static ArrayList<InetAddress> readDiscoveredPrinters(Parcel paramParcel) {
    int i = paramParcel.readInt();
    ArrayList<InetAddress> arrayList = new ArrayList(i);
    for (byte b = 0; b < i;) {
      try {
        arrayList.add(InetAddress.getByAddress(paramParcel.readBlob()));
        b++;
      } catch (UnknownHostException unknownHostException) {
        throw new IllegalArgumentException(unknownHostException);
      } 
    } 
    return arrayList;
  }
  
  private RecommendationInfo(Parcel paramParcel) {
    this(charSequence1, charSequence2, arrayList, bool);
  }
  
  public CharSequence getPackageName() {
    return this.mPackageName;
  }
  
  public boolean recommendsMultiVendorService() {
    return this.mRecommendsMultiVendorService;
  }
  
  public List<InetAddress> getDiscoveredPrinters() {
    return this.mDiscoveredPrinters;
  }
  
  public int getNumDiscoveredPrinters() {
    return this.mDiscoveredPrinters.size();
  }
  
  public CharSequence getName() {
    return this.mName;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeCharSequence(this.mPackageName);
    paramParcel.writeCharSequence(this.mName);
    paramInt = this.mDiscoveredPrinters.size();
    paramParcel.writeInt(paramInt);
    for (InetAddress inetAddress : this.mDiscoveredPrinters)
      paramParcel.writeBlob(inetAddress.getAddress()); 
    paramParcel.writeByte((byte)this.mRecommendsMultiVendorService);
  }
  
  public static final Parcelable.Creator<RecommendationInfo> CREATOR = new Parcelable.Creator<RecommendationInfo>() {
      public RecommendationInfo createFromParcel(Parcel param1Parcel) {
        return new RecommendationInfo(param1Parcel);
      }
      
      public RecommendationInfo[] newArray(int param1Int) {
        return new RecommendationInfo[param1Int];
      }
    };
  
  private final List<InetAddress> mDiscoveredPrinters;
  
  private final CharSequence mName;
  
  private final CharSequence mPackageName;
  
  private final boolean mRecommendsMultiVendorService;
}
