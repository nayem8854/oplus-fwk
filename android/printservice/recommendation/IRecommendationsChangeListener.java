package android.printservice.recommendation;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRecommendationsChangeListener extends IInterface {
  void onRecommendationsChanged() throws RemoteException;
  
  class Default implements IRecommendationsChangeListener {
    public void onRecommendationsChanged() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRecommendationsChangeListener {
    private static final String DESCRIPTOR = "android.printservice.recommendation.IRecommendationsChangeListener";
    
    static final int TRANSACTION_onRecommendationsChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.printservice.recommendation.IRecommendationsChangeListener");
    }
    
    public static IRecommendationsChangeListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.printservice.recommendation.IRecommendationsChangeListener");
      if (iInterface != null && iInterface instanceof IRecommendationsChangeListener)
        return (IRecommendationsChangeListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onRecommendationsChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.printservice.recommendation.IRecommendationsChangeListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.printservice.recommendation.IRecommendationsChangeListener");
      onRecommendationsChanged();
      return true;
    }
    
    private static class Proxy implements IRecommendationsChangeListener {
      public static IRecommendationsChangeListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.printservice.recommendation.IRecommendationsChangeListener";
      }
      
      public void onRecommendationsChanged() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.printservice.recommendation.IRecommendationsChangeListener");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IRecommendationsChangeListener.Stub.getDefaultImpl() != null) {
            IRecommendationsChangeListener.Stub.getDefaultImpl().onRecommendationsChanged();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRecommendationsChangeListener param1IRecommendationsChangeListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRecommendationsChangeListener != null) {
          Proxy.sDefaultImpl = param1IRecommendationsChangeListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRecommendationsChangeListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
