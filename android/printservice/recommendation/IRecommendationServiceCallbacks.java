package android.printservice.recommendation;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IRecommendationServiceCallbacks extends IInterface {
  void onRecommendationsUpdated(List<RecommendationInfo> paramList) throws RemoteException;
  
  class Default implements IRecommendationServiceCallbacks {
    public void onRecommendationsUpdated(List<RecommendationInfo> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRecommendationServiceCallbacks {
    private static final String DESCRIPTOR = "android.printservice.recommendation.IRecommendationServiceCallbacks";
    
    static final int TRANSACTION_onRecommendationsUpdated = 1;
    
    public Stub() {
      attachInterface(this, "android.printservice.recommendation.IRecommendationServiceCallbacks");
    }
    
    public static IRecommendationServiceCallbacks asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.printservice.recommendation.IRecommendationServiceCallbacks");
      if (iInterface != null && iInterface instanceof IRecommendationServiceCallbacks)
        return (IRecommendationServiceCallbacks)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onRecommendationsUpdated";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.printservice.recommendation.IRecommendationServiceCallbacks");
        return true;
      } 
      param1Parcel1.enforceInterface("android.printservice.recommendation.IRecommendationServiceCallbacks");
      ArrayList<RecommendationInfo> arrayList = param1Parcel1.createTypedArrayList(RecommendationInfo.CREATOR);
      onRecommendationsUpdated(arrayList);
      return true;
    }
    
    private static class Proxy implements IRecommendationServiceCallbacks {
      public static IRecommendationServiceCallbacks sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.printservice.recommendation.IRecommendationServiceCallbacks";
      }
      
      public void onRecommendationsUpdated(List<RecommendationInfo> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.printservice.recommendation.IRecommendationServiceCallbacks");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IRecommendationServiceCallbacks.Stub.getDefaultImpl() != null) {
            IRecommendationServiceCallbacks.Stub.getDefaultImpl().onRecommendationsUpdated(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRecommendationServiceCallbacks param1IRecommendationServiceCallbacks) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRecommendationServiceCallbacks != null) {
          Proxy.sDefaultImpl = param1IRecommendationServiceCallbacks;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRecommendationServiceCallbacks getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
