package android.printservice;

import android.content.Context;
import android.os.RemoteException;
import android.print.PrintJobId;
import android.print.PrintJobInfo;
import android.util.Log;

public final class PrintJob {
  private static final String LOG_TAG = "PrintJob";
  
  private PrintJobInfo mCachedInfo;
  
  private final Context mContext;
  
  private final PrintDocument mDocument;
  
  private final IPrintServiceClient mPrintServiceClient;
  
  PrintJob(Context paramContext, PrintJobInfo paramPrintJobInfo, IPrintServiceClient paramIPrintServiceClient) {
    this.mContext = paramContext;
    this.mCachedInfo = paramPrintJobInfo;
    this.mPrintServiceClient = paramIPrintServiceClient;
    PrintJobId printJobId = paramPrintJobInfo.getId();
    this.mDocument = new PrintDocument(printJobId, paramIPrintServiceClient, paramPrintJobInfo.getDocumentInfo());
  }
  
  public PrintJobId getId() {
    PrintService.throwIfNotCalledOnMainThread();
    return this.mCachedInfo.getId();
  }
  
  public PrintJobInfo getInfo() {
    PrintService.throwIfNotCalledOnMainThread();
    if (isInImmutableState())
      return this.mCachedInfo; 
    PrintJobInfo printJobInfo = null;
    try {
      PrintJobInfo printJobInfo1 = this.mPrintServiceClient.getPrintJobInfo(this.mCachedInfo.getId());
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Couldn't get info for job: ");
      stringBuilder.append(this.mCachedInfo.getId());
      Log.e("PrintJob", stringBuilder.toString(), (Throwable)remoteException);
    } 
    if (printJobInfo != null)
      this.mCachedInfo = printJobInfo; 
    return this.mCachedInfo;
  }
  
  public PrintDocument getDocument() {
    PrintService.throwIfNotCalledOnMainThread();
    return this.mDocument;
  }
  
  public boolean isQueued() {
    boolean bool;
    PrintService.throwIfNotCalledOnMainThread();
    if (getInfo().getState() == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isStarted() {
    boolean bool;
    PrintService.throwIfNotCalledOnMainThread();
    if (getInfo().getState() == 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isBlocked() {
    boolean bool;
    PrintService.throwIfNotCalledOnMainThread();
    if (getInfo().getState() == 4) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isCompleted() {
    boolean bool;
    PrintService.throwIfNotCalledOnMainThread();
    if (getInfo().getState() == 5) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isFailed() {
    boolean bool;
    PrintService.throwIfNotCalledOnMainThread();
    if (getInfo().getState() == 6) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isCancelled() {
    boolean bool;
    PrintService.throwIfNotCalledOnMainThread();
    if (getInfo().getState() == 7) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean start() {
    PrintService.throwIfNotCalledOnMainThread();
    int i = getInfo().getState();
    if (i == 2 || i == 4)
      return setState(3, null); 
    return false;
  }
  
  public boolean block(String paramString) {
    PrintService.throwIfNotCalledOnMainThread();
    PrintJobInfo printJobInfo = getInfo();
    int i = printJobInfo.getState();
    if (i == 3 || i == 4)
      return setState(4, paramString); 
    return false;
  }
  
  public boolean complete() {
    PrintService.throwIfNotCalledOnMainThread();
    if (isStarted())
      return setState(5, null); 
    return false;
  }
  
  public boolean fail(String paramString) {
    PrintService.throwIfNotCalledOnMainThread();
    if (!isInImmutableState())
      return setState(6, paramString); 
    return false;
  }
  
  public boolean cancel() {
    PrintService.throwIfNotCalledOnMainThread();
    if (!isInImmutableState())
      return setState(7, null); 
    return false;
  }
  
  public void setProgress(float paramFloat) {
    PrintService.throwIfNotCalledOnMainThread();
    try {
      this.mPrintServiceClient.setProgress(this.mCachedInfo.getId(), paramFloat);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error setting progress for job: ");
      stringBuilder.append(this.mCachedInfo.getId());
      Log.e("PrintJob", stringBuilder.toString(), (Throwable)remoteException);
    } 
  }
  
  public void setStatus(CharSequence paramCharSequence) {
    PrintService.throwIfNotCalledOnMainThread();
    try {
      this.mPrintServiceClient.setStatus(this.mCachedInfo.getId(), paramCharSequence);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error setting status for job: ");
      stringBuilder.append(this.mCachedInfo.getId());
      Log.e("PrintJob", stringBuilder.toString(), (Throwable)remoteException);
    } 
  }
  
  public void setStatus(int paramInt) {
    PrintService.throwIfNotCalledOnMainThread();
    try {
      IPrintServiceClient iPrintServiceClient = this.mPrintServiceClient;
      PrintJobId printJobId = this.mCachedInfo.getId();
      Context context = this.mContext;
      String str = context.getPackageName();
      iPrintServiceClient.setStatusRes(printJobId, paramInt, str);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error setting status for job: ");
      stringBuilder.append(this.mCachedInfo.getId());
      Log.e("PrintJob", stringBuilder.toString(), (Throwable)remoteException);
    } 
  }
  
  public boolean setTag(String paramString) {
    PrintService.throwIfNotCalledOnMainThread();
    if (isInImmutableState())
      return false; 
    try {
      return this.mPrintServiceClient.setPrintJobTag(this.mCachedInfo.getId(), paramString);
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error setting tag for job: ");
      stringBuilder.append(this.mCachedInfo.getId());
      Log.e("PrintJob", stringBuilder.toString(), (Throwable)remoteException);
      return false;
    } 
  }
  
  public String getTag() {
    PrintService.throwIfNotCalledOnMainThread();
    return getInfo().getTag();
  }
  
  public String getAdvancedStringOption(String paramString) {
    PrintService.throwIfNotCalledOnMainThread();
    return getInfo().getAdvancedStringOption(paramString);
  }
  
  public boolean hasAdvancedOption(String paramString) {
    PrintService.throwIfNotCalledOnMainThread();
    return getInfo().hasAdvancedOption(paramString);
  }
  
  public int getAdvancedIntOption(String paramString) {
    PrintService.throwIfNotCalledOnMainThread();
    return getInfo().getAdvancedIntOption(paramString);
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    return this.mCachedInfo.getId().equals(((PrintJob)paramObject).mCachedInfo.getId());
  }
  
  public int hashCode() {
    return this.mCachedInfo.getId().hashCode();
  }
  
  private boolean isInImmutableState() {
    int i = this.mCachedInfo.getState();
    return (i == 5 || i == 7 || i == 6);
  }
  
  private boolean setState(int paramInt, String paramString) {
    try {
      if (this.mPrintServiceClient.setPrintJobState(this.mCachedInfo.getId(), paramInt, paramString)) {
        this.mCachedInfo.setState(paramInt);
        this.mCachedInfo.setStatus(paramString);
        return true;
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error setting the state of job: ");
      stringBuilder.append(this.mCachedInfo.getId());
      Log.e("PrintJob", stringBuilder.toString(), (Throwable)remoteException);
    } 
    return false;
  }
}
