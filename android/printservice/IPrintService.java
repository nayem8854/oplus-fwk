package android.printservice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.print.PrintJobInfo;
import android.print.PrinterId;
import java.util.ArrayList;
import java.util.List;

public interface IPrintService extends IInterface {
  void createPrinterDiscoverySession() throws RemoteException;
  
  void destroyPrinterDiscoverySession() throws RemoteException;
  
  void onPrintJobQueued(PrintJobInfo paramPrintJobInfo) throws RemoteException;
  
  void requestCancelPrintJob(PrintJobInfo paramPrintJobInfo) throws RemoteException;
  
  void requestCustomPrinterIcon(PrinterId paramPrinterId) throws RemoteException;
  
  void setClient(IPrintServiceClient paramIPrintServiceClient) throws RemoteException;
  
  void startPrinterDiscovery(List<PrinterId> paramList) throws RemoteException;
  
  void startPrinterStateTracking(PrinterId paramPrinterId) throws RemoteException;
  
  void stopPrinterDiscovery() throws RemoteException;
  
  void stopPrinterStateTracking(PrinterId paramPrinterId) throws RemoteException;
  
  void validatePrinters(List<PrinterId> paramList) throws RemoteException;
  
  class Default implements IPrintService {
    public void setClient(IPrintServiceClient param1IPrintServiceClient) throws RemoteException {}
    
    public void requestCancelPrintJob(PrintJobInfo param1PrintJobInfo) throws RemoteException {}
    
    public void onPrintJobQueued(PrintJobInfo param1PrintJobInfo) throws RemoteException {}
    
    public void createPrinterDiscoverySession() throws RemoteException {}
    
    public void startPrinterDiscovery(List<PrinterId> param1List) throws RemoteException {}
    
    public void stopPrinterDiscovery() throws RemoteException {}
    
    public void validatePrinters(List<PrinterId> param1List) throws RemoteException {}
    
    public void startPrinterStateTracking(PrinterId param1PrinterId) throws RemoteException {}
    
    public void requestCustomPrinterIcon(PrinterId param1PrinterId) throws RemoteException {}
    
    public void stopPrinterStateTracking(PrinterId param1PrinterId) throws RemoteException {}
    
    public void destroyPrinterDiscoverySession() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPrintService {
    private static final String DESCRIPTOR = "android.printservice.IPrintService";
    
    static final int TRANSACTION_createPrinterDiscoverySession = 4;
    
    static final int TRANSACTION_destroyPrinterDiscoverySession = 11;
    
    static final int TRANSACTION_onPrintJobQueued = 3;
    
    static final int TRANSACTION_requestCancelPrintJob = 2;
    
    static final int TRANSACTION_requestCustomPrinterIcon = 9;
    
    static final int TRANSACTION_setClient = 1;
    
    static final int TRANSACTION_startPrinterDiscovery = 5;
    
    static final int TRANSACTION_startPrinterStateTracking = 8;
    
    static final int TRANSACTION_stopPrinterDiscovery = 6;
    
    static final int TRANSACTION_stopPrinterStateTracking = 10;
    
    static final int TRANSACTION_validatePrinters = 7;
    
    public Stub() {
      attachInterface(this, "android.printservice.IPrintService");
    }
    
    public static IPrintService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.printservice.IPrintService");
      if (iInterface != null && iInterface instanceof IPrintService)
        return (IPrintService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "destroyPrinterDiscoverySession";
        case 10:
          return "stopPrinterStateTracking";
        case 9:
          return "requestCustomPrinterIcon";
        case 8:
          return "startPrinterStateTracking";
        case 7:
          return "validatePrinters";
        case 6:
          return "stopPrinterDiscovery";
        case 5:
          return "startPrinterDiscovery";
        case 4:
          return "createPrinterDiscoverySession";
        case 3:
          return "onPrintJobQueued";
        case 2:
          return "requestCancelPrintJob";
        case 1:
          break;
      } 
      return "setClient";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        ArrayList<PrinterId> arrayList;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.printservice.IPrintService");
            destroyPrinterDiscoverySession();
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.printservice.IPrintService");
            if (param1Parcel1.readInt() != 0) {
              PrinterId printerId = PrinterId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            stopPrinterStateTracking((PrinterId)param1Parcel1);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.printservice.IPrintService");
            if (param1Parcel1.readInt() != 0) {
              PrinterId printerId = PrinterId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            requestCustomPrinterIcon((PrinterId)param1Parcel1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.printservice.IPrintService");
            if (param1Parcel1.readInt() != 0) {
              PrinterId printerId = PrinterId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            startPrinterStateTracking((PrinterId)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.printservice.IPrintService");
            arrayList = param1Parcel1.createTypedArrayList(PrinterId.CREATOR);
            validatePrinters(arrayList);
            return true;
          case 6:
            arrayList.enforceInterface("android.printservice.IPrintService");
            stopPrinterDiscovery();
            return true;
          case 5:
            arrayList.enforceInterface("android.printservice.IPrintService");
            arrayList = arrayList.createTypedArrayList(PrinterId.CREATOR);
            startPrinterDiscovery(arrayList);
            return true;
          case 4:
            arrayList.enforceInterface("android.printservice.IPrintService");
            createPrinterDiscoverySession();
            return true;
          case 3:
            arrayList.enforceInterface("android.printservice.IPrintService");
            if (arrayList.readInt() != 0) {
              PrintJobInfo printJobInfo = PrintJobInfo.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            onPrintJobQueued((PrintJobInfo)arrayList);
            return true;
          case 2:
            arrayList.enforceInterface("android.printservice.IPrintService");
            if (arrayList.readInt() != 0) {
              PrintJobInfo printJobInfo = PrintJobInfo.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            requestCancelPrintJob((PrintJobInfo)arrayList);
            return true;
          case 1:
            break;
        } 
        arrayList.enforceInterface("android.printservice.IPrintService");
        IPrintServiceClient iPrintServiceClient = IPrintServiceClient.Stub.asInterface(arrayList.readStrongBinder());
        setClient(iPrintServiceClient);
        return true;
      } 
      param1Parcel2.writeString("android.printservice.IPrintService");
      return true;
    }
    
    private static class Proxy implements IPrintService {
      public static IPrintService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.printservice.IPrintService";
      }
      
      public void setClient(IPrintServiceClient param2IPrintServiceClient) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.printservice.IPrintService");
          if (param2IPrintServiceClient != null) {
            iBinder = param2IPrintServiceClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IPrintService.Stub.getDefaultImpl() != null) {
            IPrintService.Stub.getDefaultImpl().setClient(param2IPrintServiceClient);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestCancelPrintJob(PrintJobInfo param2PrintJobInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.printservice.IPrintService");
          if (param2PrintJobInfo != null) {
            parcel.writeInt(1);
            param2PrintJobInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IPrintService.Stub.getDefaultImpl() != null) {
            IPrintService.Stub.getDefaultImpl().requestCancelPrintJob(param2PrintJobInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPrintJobQueued(PrintJobInfo param2PrintJobInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.printservice.IPrintService");
          if (param2PrintJobInfo != null) {
            parcel.writeInt(1);
            param2PrintJobInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IPrintService.Stub.getDefaultImpl() != null) {
            IPrintService.Stub.getDefaultImpl().onPrintJobQueued(param2PrintJobInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void createPrinterDiscoverySession() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.printservice.IPrintService");
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IPrintService.Stub.getDefaultImpl() != null) {
            IPrintService.Stub.getDefaultImpl().createPrinterDiscoverySession();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startPrinterDiscovery(List<PrinterId> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.printservice.IPrintService");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IPrintService.Stub.getDefaultImpl() != null) {
            IPrintService.Stub.getDefaultImpl().startPrinterDiscovery(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopPrinterDiscovery() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.printservice.IPrintService");
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IPrintService.Stub.getDefaultImpl() != null) {
            IPrintService.Stub.getDefaultImpl().stopPrinterDiscovery();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void validatePrinters(List<PrinterId> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.printservice.IPrintService");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IPrintService.Stub.getDefaultImpl() != null) {
            IPrintService.Stub.getDefaultImpl().validatePrinters(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startPrinterStateTracking(PrinterId param2PrinterId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.printservice.IPrintService");
          if (param2PrinterId != null) {
            parcel.writeInt(1);
            param2PrinterId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && IPrintService.Stub.getDefaultImpl() != null) {
            IPrintService.Stub.getDefaultImpl().startPrinterStateTracking(param2PrinterId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestCustomPrinterIcon(PrinterId param2PrinterId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.printservice.IPrintService");
          if (param2PrinterId != null) {
            parcel.writeInt(1);
            param2PrinterId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && IPrintService.Stub.getDefaultImpl() != null) {
            IPrintService.Stub.getDefaultImpl().requestCustomPrinterIcon(param2PrinterId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopPrinterStateTracking(PrinterId param2PrinterId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.printservice.IPrintService");
          if (param2PrinterId != null) {
            parcel.writeInt(1);
            param2PrinterId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool && IPrintService.Stub.getDefaultImpl() != null) {
            IPrintService.Stub.getDefaultImpl().stopPrinterStateTracking(param2PrinterId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void destroyPrinterDiscoverySession() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.printservice.IPrintService");
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && IPrintService.Stub.getDefaultImpl() != null) {
            IPrintService.Stub.getDefaultImpl().destroyPrinterDiscoverySession();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPrintService param1IPrintService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPrintService != null) {
          Proxy.sDefaultImpl = param1IPrintService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPrintService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
