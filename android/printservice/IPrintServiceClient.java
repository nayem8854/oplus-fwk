package android.printservice;

import android.content.pm.ParceledListSlice;
import android.graphics.drawable.Icon;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.print.PrintJobId;
import android.print.PrintJobInfo;
import android.print.PrinterId;
import android.text.TextUtils;
import java.util.List;

public interface IPrintServiceClient extends IInterface {
  PrintJobInfo getPrintJobInfo(PrintJobId paramPrintJobId) throws RemoteException;
  
  List<PrintJobInfo> getPrintJobInfos() throws RemoteException;
  
  void onCustomPrinterIconLoaded(PrinterId paramPrinterId, Icon paramIcon) throws RemoteException;
  
  void onPrintersAdded(ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  void onPrintersRemoved(ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  boolean setPrintJobState(PrintJobId paramPrintJobId, int paramInt, String paramString) throws RemoteException;
  
  boolean setPrintJobTag(PrintJobId paramPrintJobId, String paramString) throws RemoteException;
  
  void setProgress(PrintJobId paramPrintJobId, float paramFloat) throws RemoteException;
  
  void setStatus(PrintJobId paramPrintJobId, CharSequence paramCharSequence) throws RemoteException;
  
  void setStatusRes(PrintJobId paramPrintJobId, int paramInt, CharSequence paramCharSequence) throws RemoteException;
  
  void writePrintJobData(ParcelFileDescriptor paramParcelFileDescriptor, PrintJobId paramPrintJobId) throws RemoteException;
  
  class Default implements IPrintServiceClient {
    public List<PrintJobInfo> getPrintJobInfos() throws RemoteException {
      return null;
    }
    
    public PrintJobInfo getPrintJobInfo(PrintJobId param1PrintJobId) throws RemoteException {
      return null;
    }
    
    public boolean setPrintJobState(PrintJobId param1PrintJobId, int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean setPrintJobTag(PrintJobId param1PrintJobId, String param1String) throws RemoteException {
      return false;
    }
    
    public void writePrintJobData(ParcelFileDescriptor param1ParcelFileDescriptor, PrintJobId param1PrintJobId) throws RemoteException {}
    
    public void setProgress(PrintJobId param1PrintJobId, float param1Float) throws RemoteException {}
    
    public void setStatus(PrintJobId param1PrintJobId, CharSequence param1CharSequence) throws RemoteException {}
    
    public void setStatusRes(PrintJobId param1PrintJobId, int param1Int, CharSequence param1CharSequence) throws RemoteException {}
    
    public void onPrintersAdded(ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public void onPrintersRemoved(ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public void onCustomPrinterIconLoaded(PrinterId param1PrinterId, Icon param1Icon) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPrintServiceClient {
    private static final String DESCRIPTOR = "android.printservice.IPrintServiceClient";
    
    static final int TRANSACTION_getPrintJobInfo = 2;
    
    static final int TRANSACTION_getPrintJobInfos = 1;
    
    static final int TRANSACTION_onCustomPrinterIconLoaded = 11;
    
    static final int TRANSACTION_onPrintersAdded = 9;
    
    static final int TRANSACTION_onPrintersRemoved = 10;
    
    static final int TRANSACTION_setPrintJobState = 3;
    
    static final int TRANSACTION_setPrintJobTag = 4;
    
    static final int TRANSACTION_setProgress = 6;
    
    static final int TRANSACTION_setStatus = 7;
    
    static final int TRANSACTION_setStatusRes = 8;
    
    static final int TRANSACTION_writePrintJobData = 5;
    
    public Stub() {
      attachInterface(this, "android.printservice.IPrintServiceClient");
    }
    
    public static IPrintServiceClient asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.printservice.IPrintServiceClient");
      if (iInterface != null && iInterface instanceof IPrintServiceClient)
        return (IPrintServiceClient)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "onCustomPrinterIconLoaded";
        case 10:
          return "onPrintersRemoved";
        case 9:
          return "onPrintersAdded";
        case 8:
          return "setStatusRes";
        case 7:
          return "setStatus";
        case 6:
          return "setProgress";
        case 5:
          return "writePrintJobData";
        case 4:
          return "setPrintJobTag";
        case 3:
          return "setPrintJobState";
        case 2:
          return "getPrintJobInfo";
        case 1:
          break;
      } 
      return "getPrintJobInfos";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int i;
        boolean bool1;
        String str;
        PrintJobInfo printJobInfo;
        PrinterId printerId;
        float f;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.printservice.IPrintServiceClient");
            if (param1Parcel1.readInt() != 0) {
              printerId = PrinterId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              printerId = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Icon icon = Icon.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onCustomPrinterIconLoaded(printerId, (Icon)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.printservice.IPrintServiceClient");
            if (param1Parcel1.readInt() != 0) {
              ParceledListSlice parceledListSlice = ParceledListSlice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPrintersRemoved((ParceledListSlice)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.printservice.IPrintServiceClient");
            if (param1Parcel1.readInt() != 0) {
              ParceledListSlice parceledListSlice = ParceledListSlice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPrintersAdded((ParceledListSlice)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.printservice.IPrintServiceClient");
            if (param1Parcel1.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              printerId = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              CharSequence charSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setStatusRes((PrintJobId)printerId, param1Int1, (CharSequence)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.printservice.IPrintServiceClient");
            if (param1Parcel1.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              printerId = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              CharSequence charSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setStatus((PrintJobId)printerId, (CharSequence)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.printservice.IPrintServiceClient");
            if (param1Parcel1.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              printerId = null;
            } 
            f = param1Parcel1.readFloat();
            setProgress((PrintJobId)printerId, f);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.printservice.IPrintServiceClient");
            if (param1Parcel1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            writePrintJobData((ParcelFileDescriptor)param1Parcel2, (PrintJobId)param1Parcel1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.printservice.IPrintServiceClient");
            if (param1Parcel1.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel(param1Parcel1);
            } else {
              printerId = null;
            } 
            str = param1Parcel1.readString();
            bool2 = setPrintJobTag((PrintJobId)printerId, str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 3:
            str.enforceInterface("android.printservice.IPrintServiceClient");
            if (str.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel((Parcel)str);
            } else {
              printerId = null;
            } 
            i = str.readInt();
            str = str.readString();
            bool1 = setPrintJobState((PrintJobId)printerId, i, str);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            str.enforceInterface("android.printservice.IPrintServiceClient");
            if (str.readInt() != 0) {
              PrintJobId printJobId = PrintJobId.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            printJobInfo = getPrintJobInfo((PrintJobId)str);
            param1Parcel2.writeNoException();
            if (printJobInfo != null) {
              param1Parcel2.writeInt(1);
              printJobInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        printJobInfo.enforceInterface("android.printservice.IPrintServiceClient");
        List<PrintJobInfo> list = getPrintJobInfos();
        param1Parcel2.writeNoException();
        param1Parcel2.writeTypedList(list);
        return true;
      } 
      param1Parcel2.writeString("android.printservice.IPrintServiceClient");
      return true;
    }
    
    private static class Proxy implements IPrintServiceClient {
      public static IPrintServiceClient sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.printservice.IPrintServiceClient";
      }
      
      public List<PrintJobInfo> getPrintJobInfos() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.printservice.IPrintServiceClient");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IPrintServiceClient.Stub.getDefaultImpl() != null)
            return IPrintServiceClient.Stub.getDefaultImpl().getPrintJobInfos(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(PrintJobInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PrintJobInfo getPrintJobInfo(PrintJobId param2PrintJobId) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.printservice.IPrintServiceClient");
          if (param2PrintJobId != null) {
            parcel1.writeInt(1);
            param2PrintJobId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IPrintServiceClient.Stub.getDefaultImpl() != null)
            return IPrintServiceClient.Stub.getDefaultImpl().getPrintJobInfo(param2PrintJobId); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PrintJobInfo printJobInfo = PrintJobInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2PrintJobId = null;
          } 
          return (PrintJobInfo)param2PrintJobId;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setPrintJobState(PrintJobId param2PrintJobId, int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.printservice.IPrintServiceClient");
          boolean bool1 = true;
          if (param2PrintJobId != null) {
            parcel1.writeInt(1);
            param2PrintJobId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IPrintServiceClient.Stub.getDefaultImpl() != null) {
            bool1 = IPrintServiceClient.Stub.getDefaultImpl().setPrintJobState(param2PrintJobId, param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setPrintJobTag(PrintJobId param2PrintJobId, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.printservice.IPrintServiceClient");
          boolean bool1 = true;
          if (param2PrintJobId != null) {
            parcel1.writeInt(1);
            param2PrintJobId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IPrintServiceClient.Stub.getDefaultImpl() != null) {
            bool1 = IPrintServiceClient.Stub.getDefaultImpl().setPrintJobTag(param2PrintJobId, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void writePrintJobData(ParcelFileDescriptor param2ParcelFileDescriptor, PrintJobId param2PrintJobId) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.printservice.IPrintServiceClient");
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2PrintJobId != null) {
            parcel.writeInt(1);
            param2PrintJobId.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IPrintServiceClient.Stub.getDefaultImpl() != null) {
            IPrintServiceClient.Stub.getDefaultImpl().writePrintJobData(param2ParcelFileDescriptor, param2PrintJobId);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setProgress(PrintJobId param2PrintJobId, float param2Float) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.printservice.IPrintServiceClient");
          if (param2PrintJobId != null) {
            parcel1.writeInt(1);
            param2PrintJobId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IPrintServiceClient.Stub.getDefaultImpl() != null) {
            IPrintServiceClient.Stub.getDefaultImpl().setProgress(param2PrintJobId, param2Float);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setStatus(PrintJobId param2PrintJobId, CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.printservice.IPrintServiceClient");
          if (param2PrintJobId != null) {
            parcel1.writeInt(1);
            param2PrintJobId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2CharSequence != null) {
            parcel1.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IPrintServiceClient.Stub.getDefaultImpl() != null) {
            IPrintServiceClient.Stub.getDefaultImpl().setStatus(param2PrintJobId, param2CharSequence);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setStatusRes(PrintJobId param2PrintJobId, int param2Int, CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.printservice.IPrintServiceClient");
          if (param2PrintJobId != null) {
            parcel1.writeInt(1);
            param2PrintJobId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (param2CharSequence != null) {
            parcel1.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IPrintServiceClient.Stub.getDefaultImpl() != null) {
            IPrintServiceClient.Stub.getDefaultImpl().setStatusRes(param2PrintJobId, param2Int, param2CharSequence);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onPrintersAdded(ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.printservice.IPrintServiceClient");
          if (param2ParceledListSlice != null) {
            parcel1.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IPrintServiceClient.Stub.getDefaultImpl() != null) {
            IPrintServiceClient.Stub.getDefaultImpl().onPrintersAdded(param2ParceledListSlice);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onPrintersRemoved(ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.printservice.IPrintServiceClient");
          if (param2ParceledListSlice != null) {
            parcel1.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IPrintServiceClient.Stub.getDefaultImpl() != null) {
            IPrintServiceClient.Stub.getDefaultImpl().onPrintersRemoved(param2ParceledListSlice);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onCustomPrinterIconLoaded(PrinterId param2PrinterId, Icon param2Icon) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.printservice.IPrintServiceClient");
          if (param2PrinterId != null) {
            parcel1.writeInt(1);
            param2PrinterId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Icon != null) {
            parcel1.writeInt(1);
            param2Icon.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IPrintServiceClient.Stub.getDefaultImpl() != null) {
            IPrintServiceClient.Stub.getDefaultImpl().onCustomPrinterIconLoaded(param2PrinterId, param2Icon);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPrintServiceClient param1IPrintServiceClient) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPrintServiceClient != null) {
          Proxy.sDefaultImpl = param1IPrintServiceClient;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPrintServiceClient getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
