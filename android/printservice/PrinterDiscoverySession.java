package android.printservice;

import android.content.pm.ParceledListSlice;
import android.os.CancellationSignal;
import android.os.RemoteException;
import android.print.PrinterId;
import android.print.PrinterInfo;
import android.util.ArrayMap;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class PrinterDiscoverySession {
  private static final String LOG_TAG = "PrinterDiscoverySession";
  
  private static int sIdCounter = 0;
  
  private final int mId;
  
  private boolean mIsDestroyed;
  
  private boolean mIsDiscoveryStarted;
  
  private ArrayMap<PrinterId, PrinterInfo> mLastSentPrinters;
  
  private IPrintServiceClient mObserver;
  
  private final ArrayMap<PrinterId, PrinterInfo> mPrinters = new ArrayMap();
  
  private final List<PrinterId> mTrackedPrinters = new ArrayList<>();
  
  public PrinterDiscoverySession() {
    int i = sIdCounter;
    sIdCounter = i + 1;
    this.mId = i;
  }
  
  void setObserver(IPrintServiceClient paramIPrintServiceClient) {
    this.mObserver = paramIPrintServiceClient;
    if (!this.mPrinters.isEmpty())
      try {
        IPrintServiceClient iPrintServiceClient = this.mObserver;
        ParceledListSlice parceledListSlice = new ParceledListSlice();
        this(getPrinters());
        iPrintServiceClient.onPrintersAdded(parceledListSlice);
      } catch (RemoteException remoteException) {
        Log.e("PrinterDiscoverySession", "Error sending added printers", (Throwable)remoteException);
      }  
  }
  
  int getId() {
    return this.mId;
  }
  
  public final List<PrinterInfo> getPrinters() {
    PrintService.throwIfNotCalledOnMainThread();
    if (this.mIsDestroyed)
      return Collections.emptyList(); 
    return new ArrayList<>(this.mPrinters.values());
  }
  
  public final void addPrinters(List<PrinterInfo> paramList) {
    // Byte code:
    //   0: invokestatic throwIfNotCalledOnMainThread : ()V
    //   3: aload_0
    //   4: getfield mIsDestroyed : Z
    //   7: ifeq -> 19
    //   10: ldc 'PrinterDiscoverySession'
    //   12: ldc 'Not adding printers - session destroyed.'
    //   14: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   17: pop
    //   18: return
    //   19: aload_0
    //   20: getfield mIsDiscoveryStarted : Z
    //   23: ifeq -> 173
    //   26: aconst_null
    //   27: astore_2
    //   28: aload_1
    //   29: invokeinterface size : ()I
    //   34: istore_3
    //   35: iconst_0
    //   36: istore #4
    //   38: iload #4
    //   40: iload_3
    //   41: if_icmpge -> 129
    //   44: aload_1
    //   45: iload #4
    //   47: invokeinterface get : (I)Ljava/lang/Object;
    //   52: checkcast android/print/PrinterInfo
    //   55: astore #5
    //   57: aload_0
    //   58: getfield mPrinters : Landroid/util/ArrayMap;
    //   61: aload #5
    //   63: invokevirtual getId : ()Landroid/print/PrinterId;
    //   66: aload #5
    //   68: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   71: checkcast android/print/PrinterInfo
    //   74: astore #6
    //   76: aload #6
    //   78: ifnull -> 94
    //   81: aload_2
    //   82: astore #7
    //   84: aload #6
    //   86: aload #5
    //   88: invokevirtual equals : (Ljava/lang/Object;)Z
    //   91: ifne -> 120
    //   94: aload_2
    //   95: astore #7
    //   97: aload_2
    //   98: ifnonnull -> 110
    //   101: new java/util/ArrayList
    //   104: dup
    //   105: invokespecial <init> : ()V
    //   108: astore #7
    //   110: aload #7
    //   112: aload #5
    //   114: invokeinterface add : (Ljava/lang/Object;)Z
    //   119: pop
    //   120: iinc #4, 1
    //   123: aload #7
    //   125: astore_2
    //   126: goto -> 38
    //   129: aload_2
    //   130: ifnull -> 170
    //   133: aload_0
    //   134: getfield mObserver : Landroid/printservice/IPrintServiceClient;
    //   137: astore_1
    //   138: new android/content/pm/ParceledListSlice
    //   141: astore #7
    //   143: aload #7
    //   145: aload_2
    //   146: invokespecial <init> : (Ljava/util/List;)V
    //   149: aload_1
    //   150: aload #7
    //   152: invokeinterface onPrintersAdded : (Landroid/content/pm/ParceledListSlice;)V
    //   157: goto -> 170
    //   160: astore_1
    //   161: ldc 'PrinterDiscoverySession'
    //   163: ldc 'Error sending added printers'
    //   165: aload_1
    //   166: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   169: pop
    //   170: goto -> 256
    //   173: aload_0
    //   174: getfield mLastSentPrinters : Landroid/util/ArrayMap;
    //   177: ifnonnull -> 195
    //   180: aload_0
    //   181: new android/util/ArrayMap
    //   184: dup
    //   185: aload_0
    //   186: getfield mPrinters : Landroid/util/ArrayMap;
    //   189: invokespecial <init> : (Landroid/util/ArrayMap;)V
    //   192: putfield mLastSentPrinters : Landroid/util/ArrayMap;
    //   195: aload_1
    //   196: invokeinterface size : ()I
    //   201: istore_3
    //   202: iconst_0
    //   203: istore #4
    //   205: iload #4
    //   207: iload_3
    //   208: if_icmpge -> 256
    //   211: aload_1
    //   212: iload #4
    //   214: invokeinterface get : (I)Ljava/lang/Object;
    //   219: checkcast android/print/PrinterInfo
    //   222: astore_2
    //   223: aload_0
    //   224: getfield mPrinters : Landroid/util/ArrayMap;
    //   227: aload_2
    //   228: invokevirtual getId : ()Landroid/print/PrinterId;
    //   231: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   234: ifnonnull -> 250
    //   237: aload_0
    //   238: getfield mPrinters : Landroid/util/ArrayMap;
    //   241: aload_2
    //   242: invokevirtual getId : ()Landroid/print/PrinterId;
    //   245: aload_2
    //   246: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   249: pop
    //   250: iinc #4, 1
    //   253: goto -> 205
    //   256: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #167	-> 0
    //   #170	-> 3
    //   #171	-> 10
    //   #172	-> 18
    //   #175	-> 19
    //   #177	-> 26
    //   #178	-> 28
    //   #179	-> 35
    //   #180	-> 44
    //   #181	-> 57
    //   #182	-> 76
    //   #183	-> 94
    //   #184	-> 101
    //   #186	-> 110
    //   #179	-> 120
    //   #191	-> 129
    //   #193	-> 133
    //   #196	-> 157
    //   #194	-> 160
    //   #195	-> 161
    //   #198	-> 170
    //   #200	-> 173
    //   #201	-> 180
    //   #205	-> 195
    //   #206	-> 202
    //   #207	-> 211
    //   #208	-> 223
    //   #209	-> 237
    //   #206	-> 250
    //   #213	-> 256
    // Exception table:
    //   from	to	target	type
    //   133	157	160	android/os/RemoteException
  }
  
  public final void removePrinters(List<PrinterId> paramList) {
    PrintService.throwIfNotCalledOnMainThread();
    if (this.mIsDestroyed) {
      Log.w("PrinterDiscoverySession", "Not removing printers - session destroyed.");
      return;
    } 
    if (this.mIsDiscoveryStarted) {
      ArrayList<PrinterId> arrayList = new ArrayList();
      int i = paramList.size();
      for (byte b = 0; b < i; b++) {
        PrinterId printerId = paramList.get(b);
        if (this.mPrinters.remove(printerId) != null)
          arrayList.add(printerId); 
      } 
      if (!arrayList.isEmpty())
        try {
          IPrintServiceClient iPrintServiceClient = this.mObserver;
          ParceledListSlice parceledListSlice = new ParceledListSlice();
          this(arrayList);
          iPrintServiceClient.onPrintersRemoved(parceledListSlice);
        } catch (RemoteException remoteException) {
          Log.e("PrinterDiscoverySession", "Error sending removed printers", (Throwable)remoteException);
        }  
    } else {
      if (this.mLastSentPrinters == null)
        this.mLastSentPrinters = new ArrayMap(this.mPrinters); 
      int i = remoteException.size();
      for (byte b = 0; b < i; b++) {
        PrinterId printerId = remoteException.get(b);
        this.mPrinters.remove(printerId);
      } 
    } 
  }
  
  private void sendOutOfDiscoveryPeriodPrinterChanges() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLastSentPrinters : Landroid/util/ArrayMap;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnull -> 289
    //   9: aload_1
    //   10: invokevirtual isEmpty : ()Z
    //   13: ifeq -> 19
    //   16: goto -> 289
    //   19: aconst_null
    //   20: astore_1
    //   21: aload_0
    //   22: getfield mPrinters : Landroid/util/ArrayMap;
    //   25: invokevirtual values : ()Ljava/util/Collection;
    //   28: invokeinterface iterator : ()Ljava/util/Iterator;
    //   33: astore_2
    //   34: aload_2
    //   35: invokeinterface hasNext : ()Z
    //   40: ifeq -> 117
    //   43: aload_2
    //   44: invokeinterface next : ()Ljava/lang/Object;
    //   49: checkcast android/print/PrinterInfo
    //   52: astore_3
    //   53: aload_0
    //   54: getfield mLastSentPrinters : Landroid/util/ArrayMap;
    //   57: aload_3
    //   58: invokevirtual getId : ()Landroid/print/PrinterId;
    //   61: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   64: checkcast android/print/PrinterInfo
    //   67: astore #4
    //   69: aload #4
    //   71: ifnull -> 86
    //   74: aload_1
    //   75: astore #5
    //   77: aload #4
    //   79: aload_3
    //   80: invokevirtual equals : (Ljava/lang/Object;)Z
    //   83: ifne -> 111
    //   86: aload_1
    //   87: astore #5
    //   89: aload_1
    //   90: ifnonnull -> 102
    //   93: new java/util/ArrayList
    //   96: dup
    //   97: invokespecial <init> : ()V
    //   100: astore #5
    //   102: aload #5
    //   104: aload_3
    //   105: invokeinterface add : (Ljava/lang/Object;)Z
    //   110: pop
    //   111: aload #5
    //   113: astore_1
    //   114: goto -> 34
    //   117: aload_1
    //   118: ifnull -> 157
    //   121: aload_0
    //   122: getfield mObserver : Landroid/printservice/IPrintServiceClient;
    //   125: astore #5
    //   127: new android/content/pm/ParceledListSlice
    //   130: astore_2
    //   131: aload_2
    //   132: aload_1
    //   133: invokespecial <init> : (Ljava/util/List;)V
    //   136: aload #5
    //   138: aload_2
    //   139: invokeinterface onPrintersAdded : (Landroid/content/pm/ParceledListSlice;)V
    //   144: goto -> 157
    //   147: astore_1
    //   148: ldc 'PrinterDiscoverySession'
    //   150: ldc 'Error sending added printers'
    //   152: aload_1
    //   153: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   156: pop
    //   157: aconst_null
    //   158: astore_1
    //   159: aload_0
    //   160: getfield mLastSentPrinters : Landroid/util/ArrayMap;
    //   163: invokevirtual values : ()Ljava/util/Collection;
    //   166: invokeinterface iterator : ()Ljava/util/Iterator;
    //   171: astore_2
    //   172: aload_2
    //   173: invokeinterface hasNext : ()Z
    //   178: ifeq -> 242
    //   181: aload_2
    //   182: invokeinterface next : ()Ljava/lang/Object;
    //   187: checkcast android/print/PrinterInfo
    //   190: astore_3
    //   191: aload_1
    //   192: astore #5
    //   194: aload_0
    //   195: getfield mPrinters : Landroid/util/ArrayMap;
    //   198: aload_3
    //   199: invokevirtual getId : ()Landroid/print/PrinterId;
    //   202: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   205: ifne -> 236
    //   208: aload_1
    //   209: astore #5
    //   211: aload_1
    //   212: ifnonnull -> 224
    //   215: new java/util/ArrayList
    //   218: dup
    //   219: invokespecial <init> : ()V
    //   222: astore #5
    //   224: aload #5
    //   226: aload_3
    //   227: invokevirtual getId : ()Landroid/print/PrinterId;
    //   230: invokeinterface add : (Ljava/lang/Object;)Z
    //   235: pop
    //   236: aload #5
    //   238: astore_1
    //   239: goto -> 172
    //   242: aload_1
    //   243: ifnull -> 283
    //   246: aload_0
    //   247: getfield mObserver : Landroid/printservice/IPrintServiceClient;
    //   250: astore_2
    //   251: new android/content/pm/ParceledListSlice
    //   254: astore #5
    //   256: aload #5
    //   258: aload_1
    //   259: invokespecial <init> : (Ljava/util/List;)V
    //   262: aload_2
    //   263: aload #5
    //   265: invokeinterface onPrintersRemoved : (Landroid/content/pm/ParceledListSlice;)V
    //   270: goto -> 283
    //   273: astore_1
    //   274: ldc 'PrinterDiscoverySession'
    //   276: ldc 'Error sending removed printers'
    //   278: aload_1
    //   279: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   282: pop
    //   283: aload_0
    //   284: aconst_null
    //   285: putfield mLastSentPrinters : Landroid/util/ArrayMap;
    //   288: return
    //   289: aload_0
    //   290: aconst_null
    //   291: putfield mLastSentPrinters : Landroid/util/ArrayMap;
    //   294: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #276	-> 0
    //   #282	-> 19
    //   #283	-> 21
    //   #284	-> 53
    //   #285	-> 69
    //   #286	-> 86
    //   #287	-> 93
    //   #289	-> 102
    //   #291	-> 111
    //   #294	-> 117
    //   #296	-> 121
    //   #299	-> 144
    //   #297	-> 147
    //   #298	-> 148
    //   #303	-> 157
    //   #304	-> 159
    //   #305	-> 191
    //   #306	-> 208
    //   #307	-> 215
    //   #309	-> 224
    //   #311	-> 236
    //   #314	-> 242
    //   #316	-> 246
    //   #319	-> 270
    //   #317	-> 273
    //   #318	-> 274
    //   #322	-> 283
    //   #323	-> 288
    //   #277	-> 289
    //   #278	-> 294
    // Exception table:
    //   from	to	target	type
    //   121	144	147	android/os/RemoteException
    //   246	270	273	android/os/RemoteException
  }
  
  public void onRequestCustomPrinterIcon(PrinterId paramPrinterId, CancellationSignal paramCancellationSignal, CustomPrinterIconCallback paramCustomPrinterIconCallback) {}
  
  public final List<PrinterId> getTrackedPrinters() {
    PrintService.throwIfNotCalledOnMainThread();
    if (this.mIsDestroyed)
      return Collections.emptyList(); 
    return new ArrayList<>(this.mTrackedPrinters);
  }
  
  public final boolean isDestroyed() {
    PrintService.throwIfNotCalledOnMainThread();
    return this.mIsDestroyed;
  }
  
  public final boolean isPrinterDiscoveryStarted() {
    PrintService.throwIfNotCalledOnMainThread();
    return this.mIsDiscoveryStarted;
  }
  
  void startPrinterDiscovery(List<PrinterId> paramList) {
    if (!this.mIsDestroyed) {
      this.mIsDiscoveryStarted = true;
      sendOutOfDiscoveryPeriodPrinterChanges();
      List<PrinterId> list = paramList;
      if (paramList == null)
        list = Collections.emptyList(); 
      onStartPrinterDiscovery(list);
    } 
  }
  
  void stopPrinterDiscovery() {
    if (!this.mIsDestroyed) {
      this.mIsDiscoveryStarted = false;
      onStopPrinterDiscovery();
    } 
  }
  
  void validatePrinters(List<PrinterId> paramList) {
    if (!this.mIsDestroyed && this.mObserver != null)
      onValidatePrinters(paramList); 
  }
  
  void startPrinterStateTracking(PrinterId paramPrinterId) {
    if (!this.mIsDestroyed && this.mObserver != null) {
      List<PrinterId> list = this.mTrackedPrinters;
      if (!list.contains(paramPrinterId)) {
        this.mTrackedPrinters.add(paramPrinterId);
        onStartPrinterStateTracking(paramPrinterId);
      } 
    } 
  }
  
  void requestCustomPrinterIcon(PrinterId paramPrinterId) {
    if (!this.mIsDestroyed) {
      IPrintServiceClient iPrintServiceClient = this.mObserver;
      if (iPrintServiceClient != null) {
        CustomPrinterIconCallback customPrinterIconCallback = new CustomPrinterIconCallback(paramPrinterId, iPrintServiceClient);
        onRequestCustomPrinterIcon(paramPrinterId, new CancellationSignal(), customPrinterIconCallback);
      } 
    } 
  }
  
  void stopPrinterStateTracking(PrinterId paramPrinterId) {
    if (!this.mIsDestroyed && this.mObserver != null) {
      List<PrinterId> list = this.mTrackedPrinters;
      if (list.remove(paramPrinterId))
        onStopPrinterStateTracking(paramPrinterId); 
    } 
  }
  
  void destroy() {
    if (!this.mIsDestroyed) {
      this.mIsDestroyed = true;
      this.mIsDiscoveryStarted = false;
      this.mPrinters.clear();
      this.mLastSentPrinters = null;
      this.mObserver = null;
      onDestroy();
    } 
  }
  
  public abstract void onDestroy();
  
  public abstract void onStartPrinterDiscovery(List<PrinterId> paramList);
  
  public abstract void onStartPrinterStateTracking(PrinterId paramPrinterId);
  
  public abstract void onStopPrinterDiscovery();
  
  public abstract void onStopPrinterStateTracking(PrinterId paramPrinterId);
  
  public abstract void onValidatePrinters(List<PrinterId> paramList);
}
