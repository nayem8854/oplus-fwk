package android.printservice;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.print.PrintJobInfo;
import android.print.PrinterId;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class PrintService extends Service {
  private static final boolean DEBUG = false;
  
  public static final String EXTRA_CAN_SELECT_PRINTER = "android.printservice.extra.CAN_SELECT_PRINTER";
  
  public static final String EXTRA_PRINTER_INFO = "android.intent.extra.print.EXTRA_PRINTER_INFO";
  
  public static final String EXTRA_PRINT_DOCUMENT_INFO = "android.printservice.extra.PRINT_DOCUMENT_INFO";
  
  public static final String EXTRA_PRINT_JOB_INFO = "android.intent.extra.print.PRINT_JOB_INFO";
  
  public static final String EXTRA_SELECT_PRINTER = "android.printservice.extra.SELECT_PRINTER";
  
  private static final String LOG_TAG = "PrintService";
  
  public static final String SERVICE_INTERFACE = "android.printservice.PrintService";
  
  public static final String SERVICE_META_DATA = "android.printservice";
  
  private IPrintServiceClient mClient;
  
  private PrinterDiscoverySession mDiscoverySession;
  
  private Handler mHandler;
  
  private int mLastSessionId = -1;
  
  protected final void attachBaseContext(Context paramContext) {
    super.attachBaseContext(paramContext);
    this.mHandler = new ServiceHandler(paramContext.getMainLooper());
  }
  
  protected void onConnected() {}
  
  protected void onDisconnected() {}
  
  public final List<PrintJob> getActivePrintJobs() {
    throwIfNotCalledOnMainThread();
    IPrintServiceClient iPrintServiceClient = this.mClient;
    if (iPrintServiceClient == null)
      return Collections.emptyList(); 
    ArrayList<PrintJob> arrayList = null;
    try {
      PrintJob printJob;
      List<PrintJobInfo> list = iPrintServiceClient.getPrintJobInfos();
      if (list != null) {
        int i = list.size();
        ArrayList<PrintJob> arrayList1 = new ArrayList();
        this(i);
        byte b = 0;
        while (true) {
          arrayList = arrayList1;
          if (b < i) {
            printJob = new PrintJob();
            this((Context)this, list.get(b), this.mClient);
            arrayList1.add(printJob);
            b++;
            continue;
          } 
          break;
        } 
      } 
      if (printJob != null)
        return (List<PrintJob>)printJob; 
    } catch (RemoteException remoteException) {
      Log.e("PrintService", "Error calling getPrintJobs()", (Throwable)remoteException);
    } 
    return Collections.emptyList();
  }
  
  public final PrinterId generatePrinterId(String paramString) {
    throwIfNotCalledOnMainThread();
    paramString = (String)Preconditions.checkNotNull(paramString, "localId cannot be null");
    String str = getPackageName();
    return 
      new PrinterId(new ComponentName(str, getClass().getName()), paramString);
  }
  
  static void throwIfNotCalledOnMainThread() {
    if (Looper.getMainLooper().isCurrentThread())
      return; 
    throw new IllegalAccessError("must be called from the main thread");
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return new IPrintService.Stub() {
        final PrintService this$0;
        
        public void createPrinterDiscoverySession() {
          PrintService.this.mHandler.sendEmptyMessage(1);
        }
        
        public void destroyPrinterDiscoverySession() {
          PrintService.this.mHandler.sendEmptyMessage(2);
        }
        
        public void startPrinterDiscovery(List<PrinterId> param1List) {
          Message message = PrintService.this.mHandler.obtainMessage(3, param1List);
          message.sendToTarget();
        }
        
        public void stopPrinterDiscovery() {
          PrintService.this.mHandler.sendEmptyMessage(4);
        }
        
        public void validatePrinters(List<PrinterId> param1List) {
          Message message = PrintService.this.mHandler.obtainMessage(5, param1List);
          message.sendToTarget();
        }
        
        public void startPrinterStateTracking(PrinterId param1PrinterId) {
          Message message = PrintService.this.mHandler.obtainMessage(6, param1PrinterId);
          message.sendToTarget();
        }
        
        public void requestCustomPrinterIcon(PrinterId param1PrinterId) {
          Message message = PrintService.this.mHandler.obtainMessage(7, param1PrinterId);
          message.sendToTarget();
        }
        
        public void stopPrinterStateTracking(PrinterId param1PrinterId) {
          Message message = PrintService.this.mHandler.obtainMessage(8, param1PrinterId);
          message.sendToTarget();
        }
        
        public void setClient(IPrintServiceClient param1IPrintServiceClient) {
          Message message = PrintService.this.mHandler.obtainMessage(11, param1IPrintServiceClient);
          message.sendToTarget();
        }
        
        public void requestCancelPrintJob(PrintJobInfo param1PrintJobInfo) {
          Message message = PrintService.this.mHandler.obtainMessage(10, param1PrintJobInfo);
          message.sendToTarget();
        }
        
        public void onPrintJobQueued(PrintJobInfo param1PrintJobInfo) {
          Message message = PrintService.this.mHandler.obtainMessage(9, param1PrintJobInfo);
          message.sendToTarget();
        }
      };
  }
  
  protected abstract PrinterDiscoverySession onCreatePrinterDiscoverySession();
  
  protected abstract void onPrintJobQueued(PrintJob paramPrintJob);
  
  protected abstract void onRequestCancelPrintJob(PrintJob paramPrintJob);
  
  class ServiceHandler extends Handler {
    public static final int MSG_CREATE_PRINTER_DISCOVERY_SESSION = 1;
    
    public static final int MSG_DESTROY_PRINTER_DISCOVERY_SESSION = 2;
    
    public static final int MSG_ON_PRINTJOB_QUEUED = 9;
    
    public static final int MSG_ON_REQUEST_CANCEL_PRINTJOB = 10;
    
    public static final int MSG_REQUEST_CUSTOM_PRINTER_ICON = 7;
    
    public static final int MSG_SET_CLIENT = 11;
    
    public static final int MSG_START_PRINTER_DISCOVERY = 3;
    
    public static final int MSG_START_PRINTER_STATE_TRACKING = 6;
    
    public static final int MSG_STOP_PRINTER_DISCOVERY = 4;
    
    public static final int MSG_STOP_PRINTER_STATE_TRACKING = 8;
    
    public static final int MSG_VALIDATE_PRINTERS = 5;
    
    final PrintService this$0;
    
    public ServiceHandler(Looper param1Looper) {
      super(param1Looper, null, true);
    }
    
    public void handleMessage(Message param1Message) {
      StringBuilder stringBuilder;
      PrintJobInfo printJobInfo;
      PrinterId printerId;
      List<PrinterId> list;
      PrintService printService;
      int i = param1Message.what;
      switch (i) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown message: ");
          stringBuilder.append(i);
          throw new IllegalArgumentException(stringBuilder.toString());
        case 11:
          PrintService.access$302(PrintService.this, (IPrintServiceClient)((Message)stringBuilder).obj);
          if (PrintService.this.mClient != null) {
            PrintService.this.onConnected();
          } else {
            PrintService.this.onDisconnected();
          } 
          return;
        case 10:
          printJobInfo = (PrintJobInfo)((Message)stringBuilder).obj;
          printService = PrintService.this;
          printService.onRequestCancelPrintJob(new PrintJob((Context)printService, printJobInfo, printService.mClient));
          return;
        case 9:
          printJobInfo = (PrintJobInfo)((Message)printJobInfo).obj;
          printService = PrintService.this;
          printService.onPrintJobQueued(new PrintJob((Context)printService, printJobInfo, printService.mClient));
          return;
        case 8:
          if (PrintService.this.mDiscoverySession != null) {
            printerId = (PrinterId)((Message)printJobInfo).obj;
            PrintService.this.mDiscoverySession.stopPrinterStateTracking(printerId);
          } 
          return;
        case 7:
          if (PrintService.this.mDiscoverySession != null) {
            printerId = (PrinterId)((Message)printerId).obj;
            PrintService.this.mDiscoverySession.requestCustomPrinterIcon(printerId);
          } 
          return;
        case 6:
          if (PrintService.this.mDiscoverySession != null) {
            printerId = (PrinterId)((Message)printerId).obj;
            PrintService.this.mDiscoverySession.startPrinterStateTracking(printerId);
          } 
          return;
        case 5:
          if (PrintService.this.mDiscoverySession != null) {
            list = (List)((Message)printerId).obj;
            PrintService.this.mDiscoverySession.validatePrinters(list);
          } 
          return;
        case 4:
          if (PrintService.this.mDiscoverySession != null)
            PrintService.this.mDiscoverySession.stopPrinterDiscovery(); 
          return;
        case 3:
          if (PrintService.this.mDiscoverySession != null) {
            list = (ArrayList)((Message)list).obj;
            PrintService.this.mDiscoverySession.startPrinterDiscovery(list);
          } 
          return;
        case 2:
          if (PrintService.this.mDiscoverySession != null) {
            PrintService.this.mDiscoverySession.destroy();
            PrintService.access$202(PrintService.this, null);
          } 
          return;
        case 1:
          break;
      } 
      PrinterDiscoverySession printerDiscoverySession = PrintService.this.onCreatePrinterDiscoverySession();
      if (printerDiscoverySession != null) {
        if (printerDiscoverySession.getId() != PrintService.this.mLastSessionId) {
          PrintService.access$202(PrintService.this, printerDiscoverySession);
          PrintService.access$102(PrintService.this, printerDiscoverySession.getId());
          printerDiscoverySession.setObserver(PrintService.this.mClient);
          return;
        } 
        throw new IllegalStateException("cannot reuse session instances");
      } 
      throw new NullPointerException("session cannot be null");
    }
  }
}
