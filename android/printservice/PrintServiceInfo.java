package android.printservice;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import com.android.internal.R;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@SystemApi
public final class PrintServiceInfo implements Parcelable {
  public static final Parcelable.Creator<PrintServiceInfo> CREATOR;
  
  private static final String LOG_TAG = PrintServiceInfo.class.getSimpleName();
  
  private static final String TAG_PRINT_SERVICE = "print-service";
  
  private final String mAddPrintersActivityName;
  
  private final String mAdvancedPrintOptionsActivityName;
  
  private final String mId;
  
  private boolean mIsEnabled;
  
  private final ResolveInfo mResolveInfo;
  
  private final String mSettingsActivityName;
  
  public PrintServiceInfo(Parcel paramParcel) {
    boolean bool;
    this.mId = paramParcel.readString();
    if (paramParcel.readByte() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mIsEnabled = bool;
    this.mResolveInfo = paramParcel.<ResolveInfo>readParcelable(null);
    this.mSettingsActivityName = paramParcel.readString();
    this.mAddPrintersActivityName = paramParcel.readString();
    this.mAdvancedPrintOptionsActivityName = paramParcel.readString();
  }
  
  public PrintServiceInfo(ResolveInfo paramResolveInfo, String paramString1, String paramString2, String paramString3) {
    ComponentName componentName = new ComponentName(paramResolveInfo.serviceInfo.packageName, paramResolveInfo.serviceInfo.name);
    this.mId = componentName.flattenToString();
    this.mResolveInfo = paramResolveInfo;
    this.mSettingsActivityName = paramString1;
    this.mAddPrintersActivityName = paramString2;
    this.mAdvancedPrintOptionsActivityName = paramString3;
  }
  
  public ComponentName getComponentName() {
    return new ComponentName(this.mResolveInfo.serviceInfo.packageName, this.mResolveInfo.serviceInfo.name);
  }
  
  public static PrintServiceInfo create(Context paramContext, ResolveInfo paramResolveInfo) {
    String str1, str6, str9;
    StringBuilder stringBuilder1 = null;
    String str2 = null, str3 = null;
    Context context1 = null, context2 = null;
    String str4 = null;
    StringBuilder stringBuilder2 = null;
    String str5 = null;
    StringBuilder stringBuilder3 = null;
    String str7 = null;
    Context context3 = null;
    XmlPullParserException xmlPullParserException1 = null;
    String str8 = null;
    Context context4 = null;
    XmlPullParserException xmlPullParserException2 = null;
    PackageManager packageManager = paramContext.getPackageManager();
    XmlResourceParser xmlResourceParser = paramResolveInfo.serviceInfo.loadXmlMetaData(packageManager, "android.printservice");
    paramContext = context1;
    if (xmlResourceParser != null) {
      int i = 0;
      while (true) {
        if (i != 1 && i != 2) {
          StringBuilder stringBuilder4 = stringBuilder1;
          String str10 = str4;
          context1 = context3;
          String str11 = str2;
          StringBuilder stringBuilder5 = stringBuilder2;
          XmlPullParserException xmlPullParserException = xmlPullParserException1;
          String str12 = str3, str13 = str5, str14 = str8;
          try {
            i = xmlResourceParser.next();
            continue;
          } catch (IOException iOException) {
          
          } catch (XmlPullParserException xmlPullParserException3) {
            str1 = LOG_TAG;
            stringBuilder3 = new StringBuilder();
            this();
            stringBuilder3.append("Error reading meta-data:");
            stringBuilder3.append(xmlPullParserException3);
            Log.w(str1, stringBuilder3.toString());
            str1 = str11;
            stringBuilder3 = stringBuilder5;
            xmlPullParserException3 = xmlPullParserException;
            if (xmlResourceParser != null) {
              str1 = str11;
              stringBuilder3 = stringBuilder5;
              xmlPullParserException3 = xmlPullParserException;
            } else {
              break;
            } 
          } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
            str6 = LOG_TAG;
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Unable to load resources for: ");
            stringBuilder.append(paramResolveInfo.serviceInfo.packageName);
            Log.e(str6, stringBuilder.toString());
            stringBuilder = stringBuilder4;
            str6 = str10;
            context4 = context1;
            if (xmlResourceParser != null) {
              stringBuilder = stringBuilder4;
              str6 = str10;
              context4 = context1;
            } else {
              break;
            } 
          } finally {}
        } else {
          String str10, str13;
          StringBuilder stringBuilder4 = stringBuilder1;
          String str11 = str4;
          context1 = context3;
          String str12 = str2;
          StringBuilder stringBuilder5 = stringBuilder2;
          XmlPullParserException xmlPullParserException = xmlPullParserException1;
          String str14 = str3, str15 = str5, str16 = str8;
          str1 = xmlResourceParser.getName();
          stringBuilder4 = stringBuilder1;
          str11 = str4;
          context1 = context3;
          str12 = str2;
          stringBuilder5 = stringBuilder2;
          xmlPullParserException = xmlPullParserException1;
          str14 = str3;
          str15 = str5;
          str16 = str8;
          if (!"print-service".equals(str1)) {
            stringBuilder4 = stringBuilder1;
            str11 = str4;
            context1 = context3;
            str12 = str2;
            stringBuilder5 = stringBuilder2;
            xmlPullParserException = xmlPullParserException1;
            str14 = str3;
            str15 = str5;
            str16 = str8;
            Log.e(LOG_TAG, "Ignoring meta-data that does not start with print-service tag");
            context1 = context2;
            str16 = str7;
            xmlPullParserException = xmlPullParserException2;
          } else {
            stringBuilder4 = stringBuilder1;
            str11 = str4;
            context1 = context3;
            str12 = str2;
            stringBuilder5 = stringBuilder2;
            xmlPullParserException = xmlPullParserException1;
            str14 = str3;
            str15 = str5;
            str16 = str8;
            Resources resources = packageManager.getResourcesForApplication(paramResolveInfo.serviceInfo.applicationInfo);
            stringBuilder4 = stringBuilder1;
            str11 = str4;
            context1 = context3;
            str12 = str2;
            stringBuilder5 = stringBuilder2;
            xmlPullParserException = xmlPullParserException1;
            str14 = str3;
            str15 = str5;
            str16 = str8;
            AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)xmlResourceParser);
            stringBuilder4 = stringBuilder1;
            str11 = str4;
            context1 = context3;
            str12 = str2;
            stringBuilder5 = stringBuilder2;
            xmlPullParserException = xmlPullParserException1;
            str14 = str3;
            str15 = str5;
            str16 = str8;
            TypedArray typedArray = resources.obtainAttributes(attributeSet, R.styleable.PrintService);
            stringBuilder4 = stringBuilder1;
            str11 = str4;
            context1 = context3;
            str12 = str2;
            stringBuilder5 = stringBuilder2;
            xmlPullParserException = xmlPullParserException1;
            str14 = str3;
            str15 = str5;
            str16 = str8;
            String str17 = typedArray.getString(0);
            String str20 = str17;
            str11 = str4;
            context1 = context3;
            str12 = str17;
            stringBuilder5 = stringBuilder2;
            xmlPullParserException = xmlPullParserException1;
            str14 = str17;
            str15 = str5;
            str16 = str8;
            String str18 = typedArray.getString(1);
            str20 = str17;
            str11 = str18;
            context1 = context3;
            str12 = str17;
            String str21 = str18;
            xmlPullParserException = xmlPullParserException1;
            str14 = str17;
            str15 = str18;
            str16 = str8;
            String str19 = typedArray.getString(3);
            str20 = str17;
            str11 = str18;
            str10 = str19;
            str12 = str17;
            str21 = str18;
            str13 = str19;
            str14 = str17;
            str15 = str18;
            str16 = str19;
            typedArray.recycle();
            str13 = str19;
            str16 = str18;
            str10 = str17;
          } 
          str1 = str10;
          str6 = str16;
          str9 = str13;
          if (xmlResourceParser != null) {
            str9 = str13;
            str6 = str16;
            str1 = str10;
          } else {
            break;
          } 
        } 
        xmlResourceParser.close();
        break;
      } 
    } 
    return new PrintServiceInfo(paramResolveInfo, str1, str6, str9);
  }
  
  public String getId() {
    return this.mId;
  }
  
  public boolean isEnabled() {
    return this.mIsEnabled;
  }
  
  public void setIsEnabled(boolean paramBoolean) {
    this.mIsEnabled = paramBoolean;
  }
  
  public ResolveInfo getResolveInfo() {
    return this.mResolveInfo;
  }
  
  public String getSettingsActivityName() {
    return this.mSettingsActivityName;
  }
  
  public String getAddPrintersActivityName() {
    return this.mAddPrintersActivityName;
  }
  
  public String getAdvancedOptionsActivityName() {
    return this.mAdvancedPrintOptionsActivityName;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mId);
    paramParcel.writeByte((byte)this.mIsEnabled);
    paramParcel.writeParcelable((Parcelable)this.mResolveInfo, 0);
    paramParcel.writeString(this.mSettingsActivityName);
    paramParcel.writeString(this.mAddPrintersActivityName);
    paramParcel.writeString(this.mAdvancedPrintOptionsActivityName);
  }
  
  public int hashCode() {
    int i;
    String str = this.mId;
    if (str == null) {
      i = 0;
    } else {
      i = str.hashCode();
    } 
    return i + 31;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    String str = this.mId;
    if (str == null) {
      if (((PrintServiceInfo)paramObject).mId != null)
        return false; 
    } else if (!str.equals(((PrintServiceInfo)paramObject).mId)) {
      return false;
    } 
    return true;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PrintServiceInfo{");
    stringBuilder.append("id=");
    stringBuilder.append(this.mId);
    stringBuilder.append("isEnabled=");
    stringBuilder.append(this.mIsEnabled);
    stringBuilder.append(", resolveInfo=");
    stringBuilder.append(this.mResolveInfo);
    stringBuilder.append(", settingsActivityName=");
    stringBuilder.append(this.mSettingsActivityName);
    stringBuilder.append(", addPrintersActivityName=");
    stringBuilder.append(this.mAddPrintersActivityName);
    stringBuilder.append(", advancedPrintOptionsActivityName=");
    String str = this.mAdvancedPrintOptionsActivityName;
    stringBuilder.append(str);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  static {
    CREATOR = new Parcelable.Creator<PrintServiceInfo>() {
        public PrintServiceInfo createFromParcel(Parcel param1Parcel) {
          return new PrintServiceInfo(param1Parcel);
        }
        
        public PrintServiceInfo[] newArray(int param1Int) {
          return new PrintServiceInfo[param1Int];
        }
      };
  }
}
