package android.printservice;

import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.print.PrintDocumentInfo;
import android.print.PrintJobId;
import android.util.Log;
import java.io.IOException;

public final class PrintDocument {
  private static final String LOG_TAG = "PrintDocument";
  
  private final PrintDocumentInfo mInfo;
  
  private final PrintJobId mPrintJobId;
  
  private final IPrintServiceClient mPrintServiceClient;
  
  PrintDocument(PrintJobId paramPrintJobId, IPrintServiceClient paramIPrintServiceClient, PrintDocumentInfo paramPrintDocumentInfo) {
    this.mPrintJobId = paramPrintJobId;
    this.mPrintServiceClient = paramIPrintServiceClient;
    this.mInfo = paramPrintDocumentInfo;
  }
  
  public PrintDocumentInfo getInfo() {
    PrintService.throwIfNotCalledOnMainThread();
    return this.mInfo;
  }
  
  public ParcelFileDescriptor getData() {
    PrintService.throwIfNotCalledOnMainThread();
    ParcelFileDescriptor parcelFileDescriptor1 = null, parcelFileDescriptor2 = null, parcelFileDescriptor3 = null;
    try {
      ParcelFileDescriptor[] arrayOfParcelFileDescriptor = ParcelFileDescriptor.createPipe();
      ParcelFileDescriptor parcelFileDescriptor5 = arrayOfParcelFileDescriptor[0];
      ParcelFileDescriptor parcelFileDescriptor4 = arrayOfParcelFileDescriptor[1];
      parcelFileDescriptor3 = parcelFileDescriptor4;
      parcelFileDescriptor1 = parcelFileDescriptor4;
      parcelFileDescriptor2 = parcelFileDescriptor4;
      this.mPrintServiceClient.writePrintJobData(parcelFileDescriptor4, this.mPrintJobId);
      if (parcelFileDescriptor4 != null)
        try {
          parcelFileDescriptor4.close();
        } catch (IOException iOException1) {} 
      return parcelFileDescriptor5;
    } catch (IOException iOException) {
      parcelFileDescriptor3 = parcelFileDescriptor2;
      Log.e("PrintDocument", "Error calling getting print job data!", iOException);
      if (parcelFileDescriptor2 != null)
        parcelFileDescriptor2.close(); 
    } catch (RemoteException remoteException) {
      IOException iOException1 = iOException;
      Log.e("PrintDocument", "Error calling getting print job data!", (Throwable)remoteException);
      if (iOException != null)
        try {
          iOException.close();
        } catch (IOException iOException2) {} 
    } finally {}
    return null;
  }
}
