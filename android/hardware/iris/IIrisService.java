package android.hardware.iris;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IIrisService extends IInterface {
  void initConfiguredStrength(int paramInt) throws RemoteException;
  
  class Default implements IIrisService {
    public void initConfiguredStrength(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IIrisService {
    private static final String DESCRIPTOR = "android.hardware.iris.IIrisService";
    
    static final int TRANSACTION_initConfiguredStrength = 1;
    
    public Stub() {
      attachInterface(this, "android.hardware.iris.IIrisService");
    }
    
    public static IIrisService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.iris.IIrisService");
      if (iInterface != null && iInterface instanceof IIrisService)
        return (IIrisService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "initConfiguredStrength";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.hardware.iris.IIrisService");
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.iris.IIrisService");
      param1Int1 = param1Parcel1.readInt();
      initConfiguredStrength(param1Int1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IIrisService {
      public static IIrisService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.iris.IIrisService";
      }
      
      public void initConfiguredStrength(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.iris.IIrisService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IIrisService.Stub.getDefaultImpl() != null) {
            IIrisService.Stub.getDefaultImpl().initConfiguredStrength(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IIrisService param1IIrisService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IIrisService != null) {
          Proxy.sDefaultImpl = param1IIrisService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IIrisService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
