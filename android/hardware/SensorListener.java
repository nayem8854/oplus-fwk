package android.hardware;

@Deprecated
public interface SensorListener {
  void onAccuracyChanged(int paramInt1, int paramInt2);
  
  void onSensorChanged(int paramInt, float[] paramArrayOffloat);
}
