package android.hardware.display;

import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import java.time.LocalTime;

public class NightDisplayListener {
  private Callback mCallback;
  
  private final ContentObserver mContentObserver;
  
  private final Context mContext;
  
  private final Handler mHandler;
  
  private final ColorDisplayManager mManager;
  
  private final int mUserId;
  
  public NightDisplayListener(Context paramContext) {
    this(paramContext, ActivityManager.getCurrentUser(), new Handler(Looper.getMainLooper()));
  }
  
  public NightDisplayListener(Context paramContext, Handler paramHandler) {
    this(paramContext, ActivityManager.getCurrentUser(), paramHandler);
  }
  
  public NightDisplayListener(Context paramContext, int paramInt, Handler paramHandler) {
    this.mContext = paramContext = paramContext.getApplicationContext();
    this.mManager = paramContext.<ColorDisplayManager>getSystemService(ColorDisplayManager.class);
    this.mUserId = paramInt;
    this.mHandler = paramHandler;
    this.mContentObserver = (ContentObserver)new Object(this, paramHandler);
  }
  
  public void setCallback(Callback paramCallback) {
    if (Looper.myLooper() != this.mHandler.getLooper())
      this.mHandler.post(new _$$Lambda$NightDisplayListener$sOK1HmSbMnFLzc4SdDD1WpVWJiI(this, paramCallback)); 
    setCallbackInternal(paramCallback);
  }
  
  private void setCallbackInternal(Callback paramCallback) {
    Callback callback = this.mCallback;
    if (callback != paramCallback) {
      this.mCallback = paramCallback;
      if (paramCallback == null) {
        this.mContext.getContentResolver().unregisterContentObserver(this.mContentObserver);
      } else if (callback == null) {
        ContentResolver contentResolver = this.mContext.getContentResolver();
        contentResolver.registerContentObserver(Settings.Secure.getUriFor("night_display_activated"), false, this.mContentObserver, this.mUserId);
        contentResolver.registerContentObserver(Settings.Secure.getUriFor("night_display_auto_mode"), false, this.mContentObserver, this.mUserId);
        Uri uri2 = Settings.Secure.getUriFor("night_display_custom_start_time");
        ContentObserver contentObserver3 = this.mContentObserver;
        int i = this.mUserId;
        contentResolver.registerContentObserver(uri2, false, contentObserver3, i);
        Uri uri3 = Settings.Secure.getUriFor("night_display_custom_end_time");
        ContentObserver contentObserver1 = this.mContentObserver;
        i = this.mUserId;
        contentResolver.registerContentObserver(uri3, false, contentObserver1, i);
        Uri uri1 = Settings.Secure.getUriFor("night_display_color_temperature");
        ContentObserver contentObserver2 = this.mContentObserver;
        i = this.mUserId;
        contentResolver.registerContentObserver(uri1, false, contentObserver2, i);
      } 
    } 
  }
  
  public static interface Callback {
    default void onActivated(boolean param1Boolean) {}
    
    default void onAutoModeChanged(int param1Int) {}
    
    default void onCustomStartTimeChanged(LocalTime param1LocalTime) {}
    
    default void onCustomEndTimeChanged(LocalTime param1LocalTime) {}
    
    default void onColorTemperatureChanged(int param1Int) {}
  }
}
