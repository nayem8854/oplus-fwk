package android.hardware.display;

import oplus.app.OplusCommonManager;

public abstract class OplusBaseDisplayManager extends OplusCommonManager {
  public OplusBaseDisplayManager() {
    super("display");
  }
}
