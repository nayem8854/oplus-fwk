package android.hardware.display;

import android.graphics.Point;
import android.hardware.SensorManager;
import android.os.Handler;
import android.util.IntArray;
import android.util.SparseArray;
import android.view.Display;
import android.view.DisplayInfo;
import android.view.SurfaceControl;

public abstract class DisplayManagerInternal implements IOplusDisplayManagerInternalEx {
  public abstract void blockScreenOnByBiometrics(String paramString);
  
  public abstract DisplayInfo getDisplayInfo(int paramInt);
  
  public abstract Point getDisplayPosition(int paramInt);
  
  public abstract DisplayedContentSample getDisplayedContentSample(int paramInt, long paramLong1, long paramLong2);
  
  public abstract DisplayedContentSamplingAttributes getDisplayedContentSamplingAttributes(int paramInt);
  
  public abstract void getNonOverrideDisplayInfo(int paramInt, DisplayInfo paramDisplayInfo);
  
  public abstract int getScreenState();
  
  public abstract boolean hasBiometricsBlockedReason(String paramString);
  
  public abstract void initPowerManagement(DisplayPowerCallbacks paramDisplayPowerCallbacks, Handler paramHandler, SensorManager paramSensorManager);
  
  public abstract boolean isBlockDisplayByBiometrics();
  
  public abstract boolean isBlockScreenOnByBiometrics();
  
  public abstract boolean isProximitySensorAvailable();
  
  public abstract void onOverlayChanged();
  
  public abstract void performTraversal(SurfaceControl.Transaction paramTransaction);
  
  public abstract void persistBrightnessTrackerState();
  
  public abstract void registerDisplayTransactionListener(DisplayTransactionListener paramDisplayTransactionListener);
  
  public abstract void removeFaceBlockReasonFromBlockReasonList();
  
  public abstract boolean requestPowerState(DisplayPowerRequest paramDisplayPowerRequest, boolean paramBoolean);
  
  public abstract void setDisplayAccessUIDs(SparseArray<IntArray> paramSparseArray);
  
  public abstract void setDisplayInfoOverrideFromWindowManager(int paramInt, DisplayInfo paramDisplayInfo);
  
  public abstract void setDisplayOffsets(int paramInt1, int paramInt2, int paramInt3);
  
  public abstract void setDisplayProperties(int paramInt1, boolean paramBoolean1, float paramFloat, int paramInt2, boolean paramBoolean2, boolean paramBoolean3);
  
  public abstract void setDisplayScalingDisabled(int paramInt, boolean paramBoolean);
  
  public abstract boolean setDisplayedContentSamplingEnabled(int paramInt1, boolean paramBoolean, int paramInt2, int paramInt3);
  
  public abstract void setUseProximityForceSuspend(boolean paramBoolean);
  
  public abstract SurfaceControl.ScreenshotGraphicBuffer systemScreenshot(int paramInt);
  
  public abstract void unblockScreenOnByBiometrics(String paramString);
  
  public abstract void unregisterDisplayTransactionListener(DisplayTransactionListener paramDisplayTransactionListener);
  
  public abstract void updateScreenOnBlockedState(boolean paramBoolean);
  
  public abstract SurfaceControl.ScreenshotGraphicBuffer userScreenshot(int paramInt);
  
  public static interface DisplayPowerCallbacks extends IOplusBaseDisplayPowerCallbacks {
    void acquireSuspendBlocker();
    
    void onDisplayStateChange(int param1Int);
    
    void onProximityNegative();
    
    void onProximityPositive();
    
    void onStateChanged();
    
    void releaseSuspendBlocker();
  }
  
  class DisplayPowerRequest {
    public static final int POLICY_BRIGHT = 3;
    
    public static final int POLICY_DIM = 2;
    
    public static final int POLICY_DOZE = 1;
    
    public static final int POLICY_OFF = 0;
    
    public static final int POLICY_VR = 4;
    
    public boolean blockScreenOn;
    
    public boolean boostScreenBrightness;
    
    public float dozeScreenBrightness;
    
    public int dozeScreenState;
    
    public boolean lowPowerMode;
    
    public int policy;
    
    public float screenAutoBrightnessAdjustmentOverride;
    
    public float screenBrightnessOverride;
    
    public float screenLowPowerBrightnessFactor;
    
    public boolean useAutoBrightness;
    
    public boolean useProximitySensor;
    
    public DisplayPowerRequest() {
      this.policy = 3;
      this.useProximitySensor = false;
      this.screenBrightnessOverride = Float.NaN;
      this.useAutoBrightness = false;
      this.screenAutoBrightnessAdjustmentOverride = Float.NaN;
      this.screenLowPowerBrightnessFactor = 0.5F;
      this.blockScreenOn = false;
      this.dozeScreenBrightness = Float.NaN;
      this.dozeScreenState = 0;
    }
    
    public DisplayPowerRequest(DisplayManagerInternal this$0) {
      copyFrom((DisplayPowerRequest)this$0);
    }
    
    public boolean isBrightOrDim() {
      int i = this.policy;
      return (i == 3 || i == 2);
    }
    
    public boolean isVr() {
      boolean bool;
      if (this.policy == 4) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void copyFrom(DisplayPowerRequest param1DisplayPowerRequest) {
      this.policy = param1DisplayPowerRequest.policy;
      this.useProximitySensor = param1DisplayPowerRequest.useProximitySensor;
      this.screenBrightnessOverride = param1DisplayPowerRequest.screenBrightnessOverride;
      this.useAutoBrightness = param1DisplayPowerRequest.useAutoBrightness;
      this.screenAutoBrightnessAdjustmentOverride = param1DisplayPowerRequest.screenAutoBrightnessAdjustmentOverride;
      this.screenLowPowerBrightnessFactor = param1DisplayPowerRequest.screenLowPowerBrightnessFactor;
      this.blockScreenOn = param1DisplayPowerRequest.blockScreenOn;
      this.lowPowerMode = param1DisplayPowerRequest.lowPowerMode;
      this.boostScreenBrightness = param1DisplayPowerRequest.boostScreenBrightness;
      this.dozeScreenBrightness = param1DisplayPowerRequest.dozeScreenBrightness;
      this.dozeScreenState = param1DisplayPowerRequest.dozeScreenState;
    }
    
    public boolean equals(Object param1Object) {
      if (param1Object instanceof DisplayPowerRequest) {
        param1Object = param1Object;
        if (equals((DisplayPowerRequest)param1Object))
          return true; 
      } 
      return false;
    }
    
    public boolean equals(DisplayPowerRequest param1DisplayPowerRequest) {
      if (param1DisplayPowerRequest != null && this.policy == param1DisplayPowerRequest.policy && this.useProximitySensor == param1DisplayPowerRequest.useProximitySensor) {
        float f1 = this.screenBrightnessOverride, f2 = param1DisplayPowerRequest.screenBrightnessOverride;
        if (floatEquals(f1, f2) && this.useAutoBrightness == param1DisplayPowerRequest.useAutoBrightness) {
          f2 = this.screenAutoBrightnessAdjustmentOverride;
          f1 = param1DisplayPowerRequest.screenAutoBrightnessAdjustmentOverride;
          if (floatEquals(f2, f1) && this.screenLowPowerBrightnessFactor == param1DisplayPowerRequest.screenLowPowerBrightnessFactor && this.blockScreenOn == param1DisplayPowerRequest.blockScreenOn && this.lowPowerMode == param1DisplayPowerRequest.lowPowerMode && this.boostScreenBrightness == param1DisplayPowerRequest.boostScreenBrightness) {
            f2 = this.dozeScreenBrightness;
            f1 = param1DisplayPowerRequest.dozeScreenBrightness;
            if (floatEquals(f2, f1) && this.dozeScreenState == param1DisplayPowerRequest.dozeScreenState)
              return true; 
          } 
        } 
      } 
      return false;
    }
    
    private boolean floatEquals(float param1Float1, float param1Float2) {
      return (param1Float1 == param1Float2 || (Float.isNaN(param1Float1) && Float.isNaN(param1Float2)));
    }
    
    public int hashCode() {
      return 0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("policy=");
      stringBuilder.append(policyToString(this.policy));
      stringBuilder.append(", useProximitySensor=");
      stringBuilder.append(this.useProximitySensor);
      stringBuilder.append(", screenBrightnessOverride=");
      stringBuilder.append(this.screenBrightnessOverride);
      stringBuilder.append(", useAutoBrightness=");
      stringBuilder.append(this.useAutoBrightness);
      stringBuilder.append(", screenAutoBrightnessAdjustmentOverride=");
      stringBuilder.append(this.screenAutoBrightnessAdjustmentOverride);
      stringBuilder.append(", screenLowPowerBrightnessFactor=");
      stringBuilder.append(this.screenLowPowerBrightnessFactor);
      stringBuilder.append(", blockScreenOn=");
      stringBuilder.append(this.blockScreenOn);
      stringBuilder.append(", lowPowerMode=");
      stringBuilder.append(this.lowPowerMode);
      stringBuilder.append(", boostScreenBrightness=");
      stringBuilder.append(this.boostScreenBrightness);
      stringBuilder.append(", dozeScreenBrightness=");
      stringBuilder.append(this.dozeScreenBrightness);
      stringBuilder.append(", dozeScreenState=");
      int i = this.dozeScreenState;
      stringBuilder.append(Display.stateToString(i));
      return stringBuilder.toString();
    }
    
    public static String policyToString(int param1Int) {
      if (param1Int != 0) {
        if (param1Int != 1) {
          if (param1Int != 2) {
            if (param1Int != 3) {
              if (param1Int != 4)
                return Integer.toString(param1Int); 
              return "VR";
            } 
            return "BRIGHT";
          } 
          return "DIM";
        } 
        return "DOZE";
      } 
      return "OFF";
    }
  }
  
  class DisplayTransactionListener {
    public abstract void onDisplayTransaction(SurfaceControl.Transaction param1Transaction);
  }
}
