package android.hardware.display;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IColorDisplayManager extends IInterface {
  int getColorMode() throws RemoteException;
  
  int getNightDisplayAutoMode() throws RemoteException;
  
  int getNightDisplayAutoModeRaw() throws RemoteException;
  
  int getNightDisplayColorTemperature() throws RemoteException;
  
  Time getNightDisplayCustomEndTime() throws RemoteException;
  
  Time getNightDisplayCustomStartTime() throws RemoteException;
  
  int getTransformCapabilities() throws RemoteException;
  
  boolean isDeviceColorManaged() throws RemoteException;
  
  boolean isDisplayWhiteBalanceEnabled() throws RemoteException;
  
  boolean isNightDisplayActivated() throws RemoteException;
  
  boolean isSaturationActivated() throws RemoteException;
  
  boolean setAppSaturationLevel(String paramString, int paramInt) throws RemoteException;
  
  void setColorMode(int paramInt) throws RemoteException;
  
  boolean setDisplayWhiteBalanceEnabled(boolean paramBoolean) throws RemoteException;
  
  boolean setNightDisplayActivated(boolean paramBoolean) throws RemoteException;
  
  boolean setNightDisplayAutoMode(int paramInt) throws RemoteException;
  
  boolean setNightDisplayColorTemperature(int paramInt) throws RemoteException;
  
  boolean setNightDisplayCustomEndTime(Time paramTime) throws RemoteException;
  
  boolean setNightDisplayCustomStartTime(Time paramTime) throws RemoteException;
  
  boolean setSaturationLevel(int paramInt) throws RemoteException;
  
  class Default implements IColorDisplayManager {
    public boolean isDeviceColorManaged() throws RemoteException {
      return false;
    }
    
    public boolean setSaturationLevel(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean setAppSaturationLevel(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isSaturationActivated() throws RemoteException {
      return false;
    }
    
    public int getTransformCapabilities() throws RemoteException {
      return 0;
    }
    
    public boolean isNightDisplayActivated() throws RemoteException {
      return false;
    }
    
    public boolean setNightDisplayActivated(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public int getNightDisplayColorTemperature() throws RemoteException {
      return 0;
    }
    
    public boolean setNightDisplayColorTemperature(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getNightDisplayAutoMode() throws RemoteException {
      return 0;
    }
    
    public int getNightDisplayAutoModeRaw() throws RemoteException {
      return 0;
    }
    
    public boolean setNightDisplayAutoMode(int param1Int) throws RemoteException {
      return false;
    }
    
    public Time getNightDisplayCustomStartTime() throws RemoteException {
      return null;
    }
    
    public boolean setNightDisplayCustomStartTime(Time param1Time) throws RemoteException {
      return false;
    }
    
    public Time getNightDisplayCustomEndTime() throws RemoteException {
      return null;
    }
    
    public boolean setNightDisplayCustomEndTime(Time param1Time) throws RemoteException {
      return false;
    }
    
    public int getColorMode() throws RemoteException {
      return 0;
    }
    
    public void setColorMode(int param1Int) throws RemoteException {}
    
    public boolean isDisplayWhiteBalanceEnabled() throws RemoteException {
      return false;
    }
    
    public boolean setDisplayWhiteBalanceEnabled(boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IColorDisplayManager {
    private static final String DESCRIPTOR = "android.hardware.display.IColorDisplayManager";
    
    static final int TRANSACTION_getColorMode = 17;
    
    static final int TRANSACTION_getNightDisplayAutoMode = 10;
    
    static final int TRANSACTION_getNightDisplayAutoModeRaw = 11;
    
    static final int TRANSACTION_getNightDisplayColorTemperature = 8;
    
    static final int TRANSACTION_getNightDisplayCustomEndTime = 15;
    
    static final int TRANSACTION_getNightDisplayCustomStartTime = 13;
    
    static final int TRANSACTION_getTransformCapabilities = 5;
    
    static final int TRANSACTION_isDeviceColorManaged = 1;
    
    static final int TRANSACTION_isDisplayWhiteBalanceEnabled = 19;
    
    static final int TRANSACTION_isNightDisplayActivated = 6;
    
    static final int TRANSACTION_isSaturationActivated = 4;
    
    static final int TRANSACTION_setAppSaturationLevel = 3;
    
    static final int TRANSACTION_setColorMode = 18;
    
    static final int TRANSACTION_setDisplayWhiteBalanceEnabled = 20;
    
    static final int TRANSACTION_setNightDisplayActivated = 7;
    
    static final int TRANSACTION_setNightDisplayAutoMode = 12;
    
    static final int TRANSACTION_setNightDisplayColorTemperature = 9;
    
    static final int TRANSACTION_setNightDisplayCustomEndTime = 16;
    
    static final int TRANSACTION_setNightDisplayCustomStartTime = 14;
    
    static final int TRANSACTION_setSaturationLevel = 2;
    
    public Stub() {
      attachInterface(this, "android.hardware.display.IColorDisplayManager");
    }
    
    public static IColorDisplayManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.display.IColorDisplayManager");
      if (iInterface != null && iInterface instanceof IColorDisplayManager)
        return (IColorDisplayManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 20:
          return "setDisplayWhiteBalanceEnabled";
        case 19:
          return "isDisplayWhiteBalanceEnabled";
        case 18:
          return "setColorMode";
        case 17:
          return "getColorMode";
        case 16:
          return "setNightDisplayCustomEndTime";
        case 15:
          return "getNightDisplayCustomEndTime";
        case 14:
          return "setNightDisplayCustomStartTime";
        case 13:
          return "getNightDisplayCustomStartTime";
        case 12:
          return "setNightDisplayAutoMode";
        case 11:
          return "getNightDisplayAutoModeRaw";
        case 10:
          return "getNightDisplayAutoMode";
        case 9:
          return "setNightDisplayColorTemperature";
        case 8:
          return "getNightDisplayColorTemperature";
        case 7:
          return "setNightDisplayActivated";
        case 6:
          return "isNightDisplayActivated";
        case 5:
          return "getTransformCapabilities";
        case 4:
          return "isSaturationActivated";
        case 3:
          return "setAppSaturationLevel";
        case 2:
          return "setSaturationLevel";
        case 1:
          break;
      } 
      return "isDeviceColorManaged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool8;
        int i2;
        boolean bool7;
        int i1;
        boolean bool6;
        int n;
        boolean bool5;
        int m;
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        int i;
        Time time;
        String str;
        boolean bool9 = false, bool10 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 20:
            param1Parcel1.enforceInterface("android.hardware.display.IColorDisplayManager");
            bool9 = bool10;
            if (param1Parcel1.readInt() != 0)
              bool9 = true; 
            bool8 = setDisplayWhiteBalanceEnabled(bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 19:
            param1Parcel1.enforceInterface("android.hardware.display.IColorDisplayManager");
            bool8 = isDisplayWhiteBalanceEnabled();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 18:
            param1Parcel1.enforceInterface("android.hardware.display.IColorDisplayManager");
            i2 = param1Parcel1.readInt();
            setColorMode(i2);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            param1Parcel1.enforceInterface("android.hardware.display.IColorDisplayManager");
            i2 = getColorMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 16:
            param1Parcel1.enforceInterface("android.hardware.display.IColorDisplayManager");
            if (param1Parcel1.readInt() != 0) {
              Time time1 = (Time)Time.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool7 = setNightDisplayCustomEndTime((Time)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 15:
            param1Parcel1.enforceInterface("android.hardware.display.IColorDisplayManager");
            time = getNightDisplayCustomEndTime();
            param1Parcel2.writeNoException();
            if (time != null) {
              param1Parcel2.writeInt(1);
              time.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 14:
            time.enforceInterface("android.hardware.display.IColorDisplayManager");
            if (time.readInt() != 0) {
              time = (Time)Time.CREATOR.createFromParcel((Parcel)time);
            } else {
              time = null;
            } 
            bool7 = setNightDisplayCustomStartTime(time);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 13:
            time.enforceInterface("android.hardware.display.IColorDisplayManager");
            time = getNightDisplayCustomStartTime();
            param1Parcel2.writeNoException();
            if (time != null) {
              param1Parcel2.writeInt(1);
              time.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 12:
            time.enforceInterface("android.hardware.display.IColorDisplayManager");
            i1 = time.readInt();
            bool6 = setNightDisplayAutoMode(i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 11:
            time.enforceInterface("android.hardware.display.IColorDisplayManager");
            n = getNightDisplayAutoModeRaw();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(n);
            return true;
          case 10:
            time.enforceInterface("android.hardware.display.IColorDisplayManager");
            n = getNightDisplayAutoMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(n);
            return true;
          case 9:
            time.enforceInterface("android.hardware.display.IColorDisplayManager");
            n = time.readInt();
            bool5 = setNightDisplayColorTemperature(n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 8:
            time.enforceInterface("android.hardware.display.IColorDisplayManager");
            m = getNightDisplayColorTemperature();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 7:
            time.enforceInterface("android.hardware.display.IColorDisplayManager");
            if (time.readInt() != 0)
              bool9 = true; 
            bool4 = setNightDisplayActivated(bool9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 6:
            time.enforceInterface("android.hardware.display.IColorDisplayManager");
            bool4 = isNightDisplayActivated();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 5:
            time.enforceInterface("android.hardware.display.IColorDisplayManager");
            k = getTransformCapabilities();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 4:
            time.enforceInterface("android.hardware.display.IColorDisplayManager");
            bool3 = isSaturationActivated();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 3:
            time.enforceInterface("android.hardware.display.IColorDisplayManager");
            str = time.readString();
            j = time.readInt();
            bool2 = setAppSaturationLevel(str, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 2:
            time.enforceInterface("android.hardware.display.IColorDisplayManager");
            i = time.readInt();
            bool1 = setSaturationLevel(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        time.enforceInterface("android.hardware.display.IColorDisplayManager");
        boolean bool1 = isDeviceColorManaged();
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.hardware.display.IColorDisplayManager");
      return true;
    }
    
    private static class Proxy implements IColorDisplayManager {
      public static IColorDisplayManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.display.IColorDisplayManager";
      }
      
      public boolean isDeviceColorManaged() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            bool1 = IColorDisplayManager.Stub.getDefaultImpl().isDeviceColorManaged();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSaturationLevel(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            bool1 = IColorDisplayManager.Stub.getDefaultImpl().setSaturationLevel(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setAppSaturationLevel(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            bool1 = IColorDisplayManager.Stub.getDefaultImpl().setAppSaturationLevel(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSaturationActivated() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            bool1 = IColorDisplayManager.Stub.getDefaultImpl().isSaturationActivated();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getTransformCapabilities() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IColorDisplayManager.Stub.getDefaultImpl() != null)
            return IColorDisplayManager.Stub.getDefaultImpl().getTransformCapabilities(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNightDisplayActivated() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            bool1 = IColorDisplayManager.Stub.getDefaultImpl().isNightDisplayActivated();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setNightDisplayActivated(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool1 && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IColorDisplayManager.Stub.getDefaultImpl().setNightDisplayActivated(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getNightDisplayColorTemperature() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IColorDisplayManager.Stub.getDefaultImpl() != null)
            return IColorDisplayManager.Stub.getDefaultImpl().getNightDisplayColorTemperature(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setNightDisplayColorTemperature(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            bool1 = IColorDisplayManager.Stub.getDefaultImpl().setNightDisplayColorTemperature(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getNightDisplayAutoMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IColorDisplayManager.Stub.getDefaultImpl() != null)
            return IColorDisplayManager.Stub.getDefaultImpl().getNightDisplayAutoMode(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getNightDisplayAutoModeRaw() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IColorDisplayManager.Stub.getDefaultImpl() != null)
            return IColorDisplayManager.Stub.getDefaultImpl().getNightDisplayAutoModeRaw(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setNightDisplayAutoMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            bool1 = IColorDisplayManager.Stub.getDefaultImpl().setNightDisplayAutoMode(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Time getNightDisplayCustomStartTime() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Time time;
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            time = IColorDisplayManager.Stub.getDefaultImpl().getNightDisplayCustomStartTime();
            return time;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            time = (Time)Time.CREATOR.createFromParcel(parcel2);
          } else {
            time = null;
          } 
          return time;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setNightDisplayCustomStartTime(Time param2Time) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          boolean bool1 = true;
          if (param2Time != null) {
            parcel1.writeInt(1);
            param2Time.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool2 && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            bool1 = IColorDisplayManager.Stub.getDefaultImpl().setNightDisplayCustomStartTime(param2Time);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Time getNightDisplayCustomEndTime() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Time time;
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            time = IColorDisplayManager.Stub.getDefaultImpl().getNightDisplayCustomEndTime();
            return time;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            time = (Time)Time.CREATOR.createFromParcel(parcel2);
          } else {
            time = null;
          } 
          return time;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setNightDisplayCustomEndTime(Time param2Time) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          boolean bool1 = true;
          if (param2Time != null) {
            parcel1.writeInt(1);
            param2Time.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool2 && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            bool1 = IColorDisplayManager.Stub.getDefaultImpl().setNightDisplayCustomEndTime(param2Time);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getColorMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IColorDisplayManager.Stub.getDefaultImpl() != null)
            return IColorDisplayManager.Stub.getDefaultImpl().getColorMode(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setColorMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            IColorDisplayManager.Stub.getDefaultImpl().setColorMode(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDisplayWhiteBalanceEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(19, parcel1, parcel2, 0);
          if (!bool2 && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            bool1 = IColorDisplayManager.Stub.getDefaultImpl().isDisplayWhiteBalanceEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setDisplayWhiteBalanceEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.display.IColorDisplayManager");
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool1 && IColorDisplayManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IColorDisplayManager.Stub.getDefaultImpl().setDisplayWhiteBalanceEnabled(param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IColorDisplayManager param1IColorDisplayManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IColorDisplayManager != null) {
          Proxy.sDefaultImpl = param1IColorDisplayManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IColorDisplayManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
