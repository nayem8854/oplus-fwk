package android.hardware.display;

import android.os.Parcel;
import android.os.Parcelable;

public final class Curve implements Parcelable {
  public Curve(float[] paramArrayOffloat1, float[] paramArrayOffloat2) {
    this.mX = paramArrayOffloat1;
    this.mY = paramArrayOffloat2;
  }
  
  public float[] getX() {
    return this.mX;
  }
  
  public float[] getY() {
    return this.mY;
  }
  
  public static final Parcelable.Creator<Curve> CREATOR = new Parcelable.Creator<Curve>() {
      public Curve createFromParcel(Parcel param1Parcel) {
        float[] arrayOfFloat2 = param1Parcel.createFloatArray();
        float[] arrayOfFloat1 = param1Parcel.createFloatArray();
        return new Curve(arrayOfFloat2, arrayOfFloat1);
      }
      
      public Curve[] newArray(int param1Int) {
        return new Curve[param1Int];
      }
    };
  
  private final float[] mX;
  
  private final float[] mY;
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeFloatArray(this.mX);
    paramParcel.writeFloatArray(this.mY);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("[");
    int i = this.mX.length;
    for (byte b = 0; b < i; b++) {
      if (b != 0)
        stringBuilder.append(", "); 
      stringBuilder.append("(");
      stringBuilder.append(this.mX[b]);
      stringBuilder.append(", ");
      stringBuilder.append(this.mY[b]);
      stringBuilder.append(")");
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
}
