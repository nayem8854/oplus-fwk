package android.hardware.display;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDisplayManagerCallback extends IInterface {
  void onDisplayEvent(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IDisplayManagerCallback {
    public void onDisplayEvent(int param1Int1, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDisplayManagerCallback {
    private static final String DESCRIPTOR = "android.hardware.display.IDisplayManagerCallback";
    
    static final int TRANSACTION_onDisplayEvent = 1;
    
    public Stub() {
      attachInterface(this, "android.hardware.display.IDisplayManagerCallback");
    }
    
    public static IDisplayManagerCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.display.IDisplayManagerCallback");
      if (iInterface != null && iInterface instanceof IDisplayManagerCallback)
        return (IDisplayManagerCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onDisplayEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.hardware.display.IDisplayManagerCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.display.IDisplayManagerCallback");
      param1Int1 = param1Parcel1.readInt();
      param1Int2 = param1Parcel1.readInt();
      onDisplayEvent(param1Int1, param1Int2);
      return true;
    }
    
    private static class Proxy implements IDisplayManagerCallback {
      public static IDisplayManagerCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.display.IDisplayManagerCallback";
      }
      
      public void onDisplayEvent(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.display.IDisplayManagerCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IDisplayManagerCallback.Stub.getDefaultImpl() != null) {
            IDisplayManagerCallback.Stub.getDefaultImpl().onDisplayEvent(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDisplayManagerCallback param1IDisplayManagerCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDisplayManagerCallback != null) {
          Proxy.sDefaultImpl = param1IDisplayManagerCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDisplayManagerCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
