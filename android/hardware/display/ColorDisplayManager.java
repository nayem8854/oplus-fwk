package android.hardware.display;

import android.annotation.SystemApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.metrics.LogMaker;
import android.os.RemoteException;
import android.provider.Settings;
import com.android.internal.logging.MetricsLogger;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.time.LocalTime;

@SystemApi
public final class ColorDisplayManager {
  @SystemApi
  public static final int AUTO_MODE_CUSTOM_TIME = 1;
  
  @SystemApi
  public static final int AUTO_MODE_DISABLED = 0;
  
  @SystemApi
  public static final int AUTO_MODE_TWILIGHT = 2;
  
  @SystemApi
  public static final int CAPABILITY_HARDWARE_ACCELERATION_GLOBAL = 2;
  
  @SystemApi
  public static final int CAPABILITY_HARDWARE_ACCELERATION_PER_APP = 4;
  
  @SystemApi
  public static final int CAPABILITY_NONE = 0;
  
  @SystemApi
  public static final int CAPABILITY_PROTECTED_CONTENT = 1;
  
  public static final int COLOR_MODE_AUTOMATIC = 3;
  
  public static final int COLOR_MODE_BOOSTED = 1;
  
  public static final int COLOR_MODE_NATURAL = 0;
  
  public static final int COLOR_MODE_SATURATED = 2;
  
  public static final int VENDOR_COLOR_MODE_RANGE_MAX = 511;
  
  public static final int VENDOR_COLOR_MODE_RANGE_MIN = 256;
  
  private final ColorDisplayManagerInternal mManager = ColorDisplayManagerInternal.getInstance();
  
  private MetricsLogger mMetricsLogger;
  
  public boolean setNightDisplayActivated(boolean paramBoolean) {
    return this.mManager.setNightDisplayActivated(paramBoolean);
  }
  
  public boolean isNightDisplayActivated() {
    return this.mManager.isNightDisplayActivated();
  }
  
  public boolean setNightDisplayColorTemperature(int paramInt) {
    return this.mManager.setNightDisplayColorTemperature(paramInt);
  }
  
  public int getNightDisplayColorTemperature() {
    return this.mManager.getNightDisplayColorTemperature();
  }
  
  @SystemApi
  public int getNightDisplayAutoMode() {
    return this.mManager.getNightDisplayAutoMode();
  }
  
  public int getNightDisplayAutoModeRaw() {
    return this.mManager.getNightDisplayAutoModeRaw();
  }
  
  @SystemApi
  public boolean setNightDisplayAutoMode(int paramInt) {
    if (paramInt == 0 || paramInt == 1 || paramInt == 2) {
      if (this.mManager.getNightDisplayAutoMode() != paramInt) {
        MetricsLogger metricsLogger = getMetricsLogger();
        LogMaker logMaker = new LogMaker(1309);
        logMaker = logMaker.setType(4);
        logMaker = logMaker.setSubtype(paramInt);
        metricsLogger.write(logMaker);
      } 
      return this.mManager.setNightDisplayAutoMode(paramInt);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid autoMode: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public LocalTime getNightDisplayCustomStartTime() {
    return this.mManager.getNightDisplayCustomStartTime().getLocalTime();
  }
  
  @SystemApi
  public boolean setNightDisplayCustomStartTime(LocalTime paramLocalTime) {
    if (paramLocalTime != null) {
      MetricsLogger metricsLogger = getMetricsLogger();
      LogMaker logMaker = new LogMaker(1310);
      logMaker = logMaker.setType(4);
      logMaker = logMaker.setSubtype(0);
      metricsLogger.write(logMaker);
      return this.mManager.setNightDisplayCustomStartTime(new Time(paramLocalTime));
    } 
    throw new IllegalArgumentException("startTime cannot be null");
  }
  
  public LocalTime getNightDisplayCustomEndTime() {
    return this.mManager.getNightDisplayCustomEndTime().getLocalTime();
  }
  
  @SystemApi
  public boolean setNightDisplayCustomEndTime(LocalTime paramLocalTime) {
    if (paramLocalTime != null) {
      MetricsLogger metricsLogger = getMetricsLogger();
      LogMaker logMaker = new LogMaker(1310);
      logMaker = logMaker.setType(4);
      logMaker = logMaker.setSubtype(1);
      metricsLogger.write(logMaker);
      return this.mManager.setNightDisplayCustomEndTime(new Time(paramLocalTime));
    } 
    throw new IllegalArgumentException("endTime cannot be null");
  }
  
  public void setColorMode(int paramInt) {
    this.mManager.setColorMode(paramInt);
  }
  
  public int getColorMode() {
    return this.mManager.getColorMode();
  }
  
  public boolean isDeviceColorManaged() {
    return this.mManager.isDeviceColorManaged();
  }
  
  @SystemApi
  public boolean setSaturationLevel(int paramInt) {
    return this.mManager.setSaturationLevel(paramInt);
  }
  
  public boolean isSaturationActivated() {
    return this.mManager.isSaturationActivated();
  }
  
  @SystemApi
  public boolean setAppSaturationLevel(String paramString, int paramInt) {
    return this.mManager.setAppSaturationLevel(paramString, paramInt);
  }
  
  public boolean setDisplayWhiteBalanceEnabled(boolean paramBoolean) {
    return this.mManager.setDisplayWhiteBalanceEnabled(paramBoolean);
  }
  
  public boolean isDisplayWhiteBalanceEnabled() {
    return this.mManager.isDisplayWhiteBalanceEnabled();
  }
  
  public static boolean isNightDisplayAvailable(Context paramContext) {
    return paramContext.getResources().getBoolean(17891494);
  }
  
  public static int getMinimumColorTemperature(Context paramContext) {
    Resources resources = paramContext.getResources();
    return 
      resources.getInteger(17694862);
  }
  
  public static int getMaximumColorTemperature(Context paramContext) {
    Resources resources = paramContext.getResources();
    return 
      resources.getInteger(17694861);
  }
  
  public static boolean isDisplayWhiteBalanceAvailable(Context paramContext) {
    return paramContext.getResources().getBoolean(17891417);
  }
  
  public static boolean isColorTransformAccelerated(Context paramContext) {
    return paramContext.getResources().getBoolean(17891517);
  }
  
  @SystemApi
  public int getTransformCapabilities() {
    return this.mManager.getTransformCapabilities();
  }
  
  public static boolean areAccessibilityTransformsEnabled(Context paramContext) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    boolean bool = false;
    if (Settings.Secure.getInt(contentResolver, "accessibility_display_inversion_enabled", 0) == 1 || 
      Settings.Secure.getInt(contentResolver, "accessibility_display_daltonizer_enabled", 0) == 1)
      bool = true; 
    return bool;
  }
  
  private MetricsLogger getMetricsLogger() {
    if (this.mMetricsLogger == null)
      this.mMetricsLogger = new MetricsLogger(); 
    return this.mMetricsLogger;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface AutoMode {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CapabilityType {}
  
  private static class ColorDisplayManagerInternal {
    private static ColorDisplayManagerInternal sInstance;
    
    private final IColorDisplayManager mCdm;
    
    private ColorDisplayManagerInternal(IColorDisplayManager param1IColorDisplayManager) {
      this.mCdm = param1IColorDisplayManager;
    }
    
    public static ColorDisplayManagerInternal getInstance() {
      // Byte code:
      //   0: ldc android/hardware/display/ColorDisplayManager$ColorDisplayManagerInternal
      //   2: monitorenter
      //   3: getstatic android/hardware/display/ColorDisplayManager$ColorDisplayManagerInternal.sInstance : Landroid/hardware/display/ColorDisplayManager$ColorDisplayManagerInternal;
      //   6: astore_0
      //   7: aload_0
      //   8: ifnonnull -> 48
      //   11: ldc 'color_display'
      //   13: invokestatic getServiceOrThrow : (Ljava/lang/String;)Landroid/os/IBinder;
      //   16: astore_1
      //   17: new android/hardware/display/ColorDisplayManager$ColorDisplayManagerInternal
      //   20: astore_0
      //   21: aload_0
      //   22: aload_1
      //   23: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/hardware/display/IColorDisplayManager;
      //   26: invokespecial <init> : (Landroid/hardware/display/IColorDisplayManager;)V
      //   29: aload_0
      //   30: putstatic android/hardware/display/ColorDisplayManager$ColorDisplayManagerInternal.sInstance : Landroid/hardware/display/ColorDisplayManager$ColorDisplayManagerInternal;
      //   33: goto -> 48
      //   36: astore_1
      //   37: new java/lang/IllegalStateException
      //   40: astore_0
      //   41: aload_0
      //   42: aload_1
      //   43: invokespecial <init> : (Ljava/lang/Throwable;)V
      //   46: aload_0
      //   47: athrow
      //   48: getstatic android/hardware/display/ColorDisplayManager$ColorDisplayManagerInternal.sInstance : Landroid/hardware/display/ColorDisplayManager$ColorDisplayManagerInternal;
      //   51: astore_0
      //   52: ldc android/hardware/display/ColorDisplayManager$ColorDisplayManagerInternal
      //   54: monitorexit
      //   55: aload_0
      //   56: areturn
      //   57: astore_0
      //   58: ldc android/hardware/display/ColorDisplayManager$ColorDisplayManagerInternal
      //   60: monitorexit
      //   61: aload_0
      //   62: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #519	-> 0
      //   #520	-> 3
      //   #522	-> 11
      //   #523	-> 17
      //   #524	-> 21
      //   #527	-> 33
      //   #525	-> 36
      //   #526	-> 37
      //   #529	-> 48
      //   #530	-> 57
      // Exception table:
      //   from	to	target	type
      //   3	7	57	finally
      //   11	17	36	android/os/ServiceManager$ServiceNotFoundException
      //   11	17	57	finally
      //   17	21	36	android/os/ServiceManager$ServiceNotFoundException
      //   17	21	57	finally
      //   21	33	36	android/os/ServiceManager$ServiceNotFoundException
      //   21	33	57	finally
      //   37	48	57	finally
      //   48	55	57	finally
      //   58	61	57	finally
    }
    
    boolean isNightDisplayActivated() {
      try {
        return this.mCdm.isNightDisplayActivated();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    boolean setNightDisplayActivated(boolean param1Boolean) {
      try {
        return this.mCdm.setNightDisplayActivated(param1Boolean);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    int getNightDisplayColorTemperature() {
      try {
        return this.mCdm.getNightDisplayColorTemperature();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    boolean setNightDisplayColorTemperature(int param1Int) {
      try {
        return this.mCdm.setNightDisplayColorTemperature(param1Int);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    int getNightDisplayAutoMode() {
      try {
        return this.mCdm.getNightDisplayAutoMode();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    int getNightDisplayAutoModeRaw() {
      try {
        return this.mCdm.getNightDisplayAutoModeRaw();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    boolean setNightDisplayAutoMode(int param1Int) {
      try {
        return this.mCdm.setNightDisplayAutoMode(param1Int);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    Time getNightDisplayCustomStartTime() {
      try {
        return this.mCdm.getNightDisplayCustomStartTime();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    boolean setNightDisplayCustomStartTime(Time param1Time) {
      try {
        return this.mCdm.setNightDisplayCustomStartTime(param1Time);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    Time getNightDisplayCustomEndTime() {
      try {
        return this.mCdm.getNightDisplayCustomEndTime();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    boolean setNightDisplayCustomEndTime(Time param1Time) {
      try {
        return this.mCdm.setNightDisplayCustomEndTime(param1Time);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    boolean isDeviceColorManaged() {
      try {
        return this.mCdm.isDeviceColorManaged();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    boolean setSaturationLevel(int param1Int) {
      try {
        return this.mCdm.setSaturationLevel(param1Int);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    boolean isSaturationActivated() {
      try {
        return this.mCdm.isSaturationActivated();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    boolean setAppSaturationLevel(String param1String, int param1Int) {
      try {
        return this.mCdm.setAppSaturationLevel(param1String, param1Int);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    boolean isDisplayWhiteBalanceEnabled() {
      try {
        return this.mCdm.isDisplayWhiteBalanceEnabled();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    boolean setDisplayWhiteBalanceEnabled(boolean param1Boolean) {
      try {
        return this.mCdm.setDisplayWhiteBalanceEnabled(param1Boolean);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    int getColorMode() {
      try {
        return this.mCdm.getColorMode();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    void setColorMode(int param1Int) {
      try {
        this.mCdm.setColorMode(param1Int);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    int getTransformCapabilities() {
      try {
        return this.mCdm.getTransformCapabilities();
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ColorMode {}
}
