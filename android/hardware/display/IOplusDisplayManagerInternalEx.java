package android.hardware.display;

public interface IOplusDisplayManagerInternalEx {
  void blockScreenOnByBiometrics(String paramString);
  
  int getScreenState();
  
  boolean hasBiometricsBlockedReason(String paramString);
  
  boolean isBlockDisplayByBiometrics();
  
  boolean isBlockScreenOnByBiometrics();
  
  void removeFaceBlockReasonFromBlockReasonList();
  
  void setUseProximityForceSuspend(boolean paramBoolean);
  
  void unblockScreenOnByBiometrics(String paramString);
}
