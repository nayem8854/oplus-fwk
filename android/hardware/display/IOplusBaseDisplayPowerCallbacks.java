package android.hardware.display;

public interface IOplusBaseDisplayPowerCallbacks {
  void onProximityNegativeForceSuspend();
  
  void onProximityPositiveForceSuspend();
}
