package android.hardware.display;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;

public class OplusosDisplayManager extends OplusBaseDisplayManager implements IOplusosDisplayManager {
  public static final String KEY_GAME_LOCK_SWITCH = "game_lock_switch";
  
  public static final int MSG_GAME_SPACE = 0;
  
  public boolean setStateChanged(int paramInt, Bundle paramBundle) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.hardware.display.IDisplayManager");
      parcel1.writeInt(paramInt);
      parcel1.writeBundle(paramBundle);
      this.mRemote.transact(10111, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
}
