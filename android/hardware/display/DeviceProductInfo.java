package android.hardware.display;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Objects;

public final class DeviceProductInfo implements Parcelable {
  public DeviceProductInfo(String paramString1, String paramString2, String paramString3, Integer paramInteger, ManufactureDate paramManufactureDate, int[] paramArrayOfint) {
    this.mName = paramString1;
    this.mManufacturerPnpId = paramString2;
    this.mProductId = paramString3;
    this.mModelYear = paramInteger;
    this.mManufactureDate = paramManufactureDate;
    this.mRelativeAddress = paramArrayOfint;
  }
  
  private DeviceProductInfo(Parcel paramParcel) {
    this.mName = paramParcel.readString();
    this.mManufacturerPnpId = paramParcel.readString();
    this.mProductId = (String)paramParcel.readValue(null);
    this.mModelYear = (Integer)paramParcel.readValue(null);
    this.mManufactureDate = (ManufactureDate)paramParcel.readValue(null);
    this.mRelativeAddress = paramParcel.createIntArray();
  }
  
  public String getName() {
    return this.mName;
  }
  
  public String getManufacturerPnpId() {
    return this.mManufacturerPnpId;
  }
  
  public String getProductId() {
    return this.mProductId;
  }
  
  public Integer getModelYear() {
    return this.mModelYear;
  }
  
  public ManufactureDate getManufactureDate() {
    return this.mManufactureDate;
  }
  
  public int[] getRelativeAddress() {
    return this.mRelativeAddress;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DeviceProductInfo{name=");
    stringBuilder.append(this.mName);
    stringBuilder.append(", manufacturerPnpId=");
    stringBuilder.append(this.mManufacturerPnpId);
    stringBuilder.append(", productId=");
    stringBuilder.append(this.mProductId);
    stringBuilder.append(", modelYear=");
    stringBuilder.append(this.mModelYear);
    stringBuilder.append(", manufactureDate=");
    stringBuilder.append(this.mManufactureDate);
    stringBuilder.append(", relativeAddress=");
    int[] arrayOfInt = this.mRelativeAddress;
    stringBuilder.append(Arrays.toString(arrayOfInt));
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mName, ((DeviceProductInfo)paramObject).mName)) {
      String str1 = this.mManufacturerPnpId, str2 = ((DeviceProductInfo)paramObject).mManufacturerPnpId;
      if (Objects.equals(str1, str2)) {
        str2 = this.mProductId;
        str1 = ((DeviceProductInfo)paramObject).mProductId;
        if (Objects.equals(str2, str1)) {
          Integer integer1 = this.mModelYear, integer2 = ((DeviceProductInfo)paramObject).mModelYear;
          if (Objects.equals(integer1, integer2)) {
            ManufactureDate manufactureDate2 = this.mManufactureDate, manufactureDate1 = ((DeviceProductInfo)paramObject).mManufactureDate;
            if (Objects.equals(manufactureDate2, manufactureDate1)) {
              int[] arrayOfInt = this.mRelativeAddress;
              paramObject = ((DeviceProductInfo)paramObject).mRelativeAddress;
              if (Arrays.equals(arrayOfInt, (int[])paramObject))
                return null; 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    String str1 = this.mName, str2 = this.mManufacturerPnpId, str3 = this.mProductId;
    Integer integer = this.mModelYear;
    ManufactureDate manufactureDate = this.mManufactureDate;
    int[] arrayOfInt = this.mRelativeAddress;
    int i = Arrays.hashCode(arrayOfInt);
    return Objects.hash(new Object[] { str1, str2, str3, integer, manufactureDate, Integer.valueOf(i) });
  }
  
  public static final Parcelable.Creator<DeviceProductInfo> CREATOR = new Parcelable.Creator<DeviceProductInfo>() {
      public DeviceProductInfo createFromParcel(Parcel param1Parcel) {
        return new DeviceProductInfo(param1Parcel);
      }
      
      public DeviceProductInfo[] newArray(int param1Int) {
        return new DeviceProductInfo[param1Int];
      }
    };
  
  private final ManufactureDate mManufactureDate;
  
  private final String mManufacturerPnpId;
  
  private final Integer mModelYear;
  
  private final String mName;
  
  private final String mProductId;
  
  private final int[] mRelativeAddress;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mName);
    paramParcel.writeString(this.mManufacturerPnpId);
    paramParcel.writeValue(this.mProductId);
    paramParcel.writeValue(this.mModelYear);
    paramParcel.writeValue(this.mManufactureDate);
    paramParcel.writeIntArray(this.mRelativeAddress);
  }
  
  public static class ManufactureDate implements Parcelable {
    public ManufactureDate(Integer param1Integer1, Integer param1Integer2) {
      this.mWeek = param1Integer1;
      this.mYear = param1Integer2;
    }
    
    protected ManufactureDate(Parcel param1Parcel) {
      this.mWeek = (Integer)param1Parcel.readValue(null);
      this.mYear = (Integer)param1Parcel.readValue(null);
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeValue(this.mWeek);
      param1Parcel.writeValue(this.mYear);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public static final Parcelable.Creator<ManufactureDate> CREATOR = new Parcelable.Creator<ManufactureDate>() {
        public DeviceProductInfo.ManufactureDate createFromParcel(Parcel param2Parcel) {
          return new DeviceProductInfo.ManufactureDate(param2Parcel);
        }
        
        public DeviceProductInfo.ManufactureDate[] newArray(int param2Int) {
          return new DeviceProductInfo.ManufactureDate[param2Int];
        }
      };
    
    private final Integer mWeek;
    
    private final Integer mYear;
    
    public Integer getYear() {
      return this.mYear;
    }
    
    public Integer getWeek() {
      return this.mWeek;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ManufactureDate{week=");
      stringBuilder.append(this.mWeek);
      stringBuilder.append(", year=");
      stringBuilder.append(this.mYear);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (!Objects.equals(this.mWeek, ((ManufactureDate)param1Object).mWeek) || !Objects.equals(this.mYear, ((ManufactureDate)param1Object).mYear))
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { this.mWeek, this.mYear });
    }
  }
  
  class null implements Parcelable.Creator<ManufactureDate> {
    public DeviceProductInfo.ManufactureDate createFromParcel(Parcel param1Parcel) {
      return new DeviceProductInfo.ManufactureDate(param1Parcel);
    }
    
    public DeviceProductInfo.ManufactureDate[] newArray(int param1Int) {
      return new DeviceProductInfo.ManufactureDate[param1Int];
    }
  }
}
