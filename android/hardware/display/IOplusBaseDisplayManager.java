package android.hardware.display;

import oplus.app.IOplusCommonManager;

public interface IOplusBaseDisplayManager extends IOplusCommonManager {
  public static final String DESCRIPTOR = "android.hardware.display.IDisplayManager";
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
}
