package android.hardware.input;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.view.InputDevice;
import android.view.InputEvent;
import android.view.InputMonitor;
import android.view.PointerIcon;
import android.view.VerifiedInputEvent;

public interface IInputManager extends IInterface {
  void addKeyboardLayoutForInputDevice(InputDeviceIdentifier paramInputDeviceIdentifier, String paramString) throws RemoteException;
  
  void addPortAssociation(String paramString, int paramInt) throws RemoteException;
  
  void cancelVibrate(int paramInt, IBinder paramIBinder) throws RemoteException;
  
  void disableInputDevice(int paramInt) throws RemoteException;
  
  void enableInputDevice(int paramInt) throws RemoteException;
  
  String getCurrentKeyboardLayoutForInputDevice(InputDeviceIdentifier paramInputDeviceIdentifier) throws RemoteException;
  
  String[] getEnabledKeyboardLayoutsForInputDevice(InputDeviceIdentifier paramInputDeviceIdentifier) throws RemoteException;
  
  InputDevice getInputDevice(int paramInt) throws RemoteException;
  
  int[] getInputDeviceIds() throws RemoteException;
  
  KeyboardLayout getKeyboardLayout(String paramString) throws RemoteException;
  
  KeyboardLayout[] getKeyboardLayouts() throws RemoteException;
  
  KeyboardLayout[] getKeyboardLayoutsForInputDevice(InputDeviceIdentifier paramInputDeviceIdentifier) throws RemoteException;
  
  TouchCalibration getTouchCalibrationForInputDevice(String paramString, int paramInt) throws RemoteException;
  
  boolean hasKeys(int paramInt1, int paramInt2, int[] paramArrayOfint, boolean[] paramArrayOfboolean) throws RemoteException;
  
  boolean injectInputEvent(InputEvent paramInputEvent, int paramInt) throws RemoteException;
  
  int isInTabletMode() throws RemoteException;
  
  boolean isInputDeviceEnabled(int paramInt) throws RemoteException;
  
  int isMicMuted() throws RemoteException;
  
  InputMonitor monitorGestureInput(String paramString, int paramInt) throws RemoteException;
  
  void registerInputDevicesChangedListener(IInputDevicesChangedListener paramIInputDevicesChangedListener) throws RemoteException;
  
  void registerTabletModeChangedListener(ITabletModeChangedListener paramITabletModeChangedListener) throws RemoteException;
  
  void removeKeyboardLayoutForInputDevice(InputDeviceIdentifier paramInputDeviceIdentifier, String paramString) throws RemoteException;
  
  void removePortAssociation(String paramString) throws RemoteException;
  
  void requestPointerCapture(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void setCurrentKeyboardLayoutForInputDevice(InputDeviceIdentifier paramInputDeviceIdentifier, String paramString) throws RemoteException;
  
  void setCustomPointerIcon(PointerIcon paramPointerIcon) throws RemoteException;
  
  void setPointerIconType(int paramInt) throws RemoteException;
  
  void setTouchCalibrationForInputDevice(String paramString, int paramInt, TouchCalibration paramTouchCalibration) throws RemoteException;
  
  void tryPointerSpeed(int paramInt) throws RemoteException;
  
  VerifiedInputEvent verifyInputEvent(InputEvent paramInputEvent) throws RemoteException;
  
  void vibrate(int paramInt1, long[] paramArrayOflong, int paramInt2, IBinder paramIBinder) throws RemoteException;
  
  class Default implements IInputManager {
    public InputDevice getInputDevice(int param1Int) throws RemoteException {
      return null;
    }
    
    public int[] getInputDeviceIds() throws RemoteException {
      return null;
    }
    
    public boolean isInputDeviceEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public void enableInputDevice(int param1Int) throws RemoteException {}
    
    public void disableInputDevice(int param1Int) throws RemoteException {}
    
    public boolean hasKeys(int param1Int1, int param1Int2, int[] param1ArrayOfint, boolean[] param1ArrayOfboolean) throws RemoteException {
      return false;
    }
    
    public void tryPointerSpeed(int param1Int) throws RemoteException {}
    
    public boolean injectInputEvent(InputEvent param1InputEvent, int param1Int) throws RemoteException {
      return false;
    }
    
    public VerifiedInputEvent verifyInputEvent(InputEvent param1InputEvent) throws RemoteException {
      return null;
    }
    
    public TouchCalibration getTouchCalibrationForInputDevice(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void setTouchCalibrationForInputDevice(String param1String, int param1Int, TouchCalibration param1TouchCalibration) throws RemoteException {}
    
    public KeyboardLayout[] getKeyboardLayouts() throws RemoteException {
      return null;
    }
    
    public KeyboardLayout[] getKeyboardLayoutsForInputDevice(InputDeviceIdentifier param1InputDeviceIdentifier) throws RemoteException {
      return null;
    }
    
    public KeyboardLayout getKeyboardLayout(String param1String) throws RemoteException {
      return null;
    }
    
    public String getCurrentKeyboardLayoutForInputDevice(InputDeviceIdentifier param1InputDeviceIdentifier) throws RemoteException {
      return null;
    }
    
    public void setCurrentKeyboardLayoutForInputDevice(InputDeviceIdentifier param1InputDeviceIdentifier, String param1String) throws RemoteException {}
    
    public String[] getEnabledKeyboardLayoutsForInputDevice(InputDeviceIdentifier param1InputDeviceIdentifier) throws RemoteException {
      return null;
    }
    
    public void addKeyboardLayoutForInputDevice(InputDeviceIdentifier param1InputDeviceIdentifier, String param1String) throws RemoteException {}
    
    public void removeKeyboardLayoutForInputDevice(InputDeviceIdentifier param1InputDeviceIdentifier, String param1String) throws RemoteException {}
    
    public void registerInputDevicesChangedListener(IInputDevicesChangedListener param1IInputDevicesChangedListener) throws RemoteException {}
    
    public int isInTabletMode() throws RemoteException {
      return 0;
    }
    
    public void registerTabletModeChangedListener(ITabletModeChangedListener param1ITabletModeChangedListener) throws RemoteException {}
    
    public int isMicMuted() throws RemoteException {
      return 0;
    }
    
    public void vibrate(int param1Int1, long[] param1ArrayOflong, int param1Int2, IBinder param1IBinder) throws RemoteException {}
    
    public void cancelVibrate(int param1Int, IBinder param1IBinder) throws RemoteException {}
    
    public void setPointerIconType(int param1Int) throws RemoteException {}
    
    public void setCustomPointerIcon(PointerIcon param1PointerIcon) throws RemoteException {}
    
    public void requestPointerCapture(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public InputMonitor monitorGestureInput(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void addPortAssociation(String param1String, int param1Int) throws RemoteException {}
    
    public void removePortAssociation(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInputManager {
    private static final String DESCRIPTOR = "android.hardware.input.IInputManager";
    
    static final int TRANSACTION_addKeyboardLayoutForInputDevice = 18;
    
    static final int TRANSACTION_addPortAssociation = 30;
    
    static final int TRANSACTION_cancelVibrate = 25;
    
    static final int TRANSACTION_disableInputDevice = 5;
    
    static final int TRANSACTION_enableInputDevice = 4;
    
    static final int TRANSACTION_getCurrentKeyboardLayoutForInputDevice = 15;
    
    static final int TRANSACTION_getEnabledKeyboardLayoutsForInputDevice = 17;
    
    static final int TRANSACTION_getInputDevice = 1;
    
    static final int TRANSACTION_getInputDeviceIds = 2;
    
    static final int TRANSACTION_getKeyboardLayout = 14;
    
    static final int TRANSACTION_getKeyboardLayouts = 12;
    
    static final int TRANSACTION_getKeyboardLayoutsForInputDevice = 13;
    
    static final int TRANSACTION_getTouchCalibrationForInputDevice = 10;
    
    static final int TRANSACTION_hasKeys = 6;
    
    static final int TRANSACTION_injectInputEvent = 8;
    
    static final int TRANSACTION_isInTabletMode = 21;
    
    static final int TRANSACTION_isInputDeviceEnabled = 3;
    
    static final int TRANSACTION_isMicMuted = 23;
    
    static final int TRANSACTION_monitorGestureInput = 29;
    
    static final int TRANSACTION_registerInputDevicesChangedListener = 20;
    
    static final int TRANSACTION_registerTabletModeChangedListener = 22;
    
    static final int TRANSACTION_removeKeyboardLayoutForInputDevice = 19;
    
    static final int TRANSACTION_removePortAssociation = 31;
    
    static final int TRANSACTION_requestPointerCapture = 28;
    
    static final int TRANSACTION_setCurrentKeyboardLayoutForInputDevice = 16;
    
    static final int TRANSACTION_setCustomPointerIcon = 27;
    
    static final int TRANSACTION_setPointerIconType = 26;
    
    static final int TRANSACTION_setTouchCalibrationForInputDevice = 11;
    
    static final int TRANSACTION_tryPointerSpeed = 7;
    
    static final int TRANSACTION_verifyInputEvent = 9;
    
    static final int TRANSACTION_vibrate = 24;
    
    public Stub() {
      attachInterface(this, "android.hardware.input.IInputManager");
    }
    
    public static IInputManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.input.IInputManager");
      if (iInterface != null && iInterface instanceof IInputManager)
        return (IInputManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 31:
          return "removePortAssociation";
        case 30:
          return "addPortAssociation";
        case 29:
          return "monitorGestureInput";
        case 28:
          return "requestPointerCapture";
        case 27:
          return "setCustomPointerIcon";
        case 26:
          return "setPointerIconType";
        case 25:
          return "cancelVibrate";
        case 24:
          return "vibrate";
        case 23:
          return "isMicMuted";
        case 22:
          return "registerTabletModeChangedListener";
        case 21:
          return "isInTabletMode";
        case 20:
          return "registerInputDevicesChangedListener";
        case 19:
          return "removeKeyboardLayoutForInputDevice";
        case 18:
          return "addKeyboardLayoutForInputDevice";
        case 17:
          return "getEnabledKeyboardLayoutsForInputDevice";
        case 16:
          return "setCurrentKeyboardLayoutForInputDevice";
        case 15:
          return "getCurrentKeyboardLayoutForInputDevice";
        case 14:
          return "getKeyboardLayout";
        case 13:
          return "getKeyboardLayoutsForInputDevice";
        case 12:
          return "getKeyboardLayouts";
        case 11:
          return "setTouchCalibrationForInputDevice";
        case 10:
          return "getTouchCalibrationForInputDevice";
        case 9:
          return "verifyInputEvent";
        case 8:
          return "injectInputEvent";
        case 7:
          return "tryPointerSpeed";
        case 6:
          return "hasKeys";
        case 5:
          return "disableInputDevice";
        case 4:
          return "enableInputDevice";
        case 3:
          return "isInputDeviceEnabled";
        case 2:
          return "getInputDeviceIds";
        case 1:
          break;
      } 
      return "getInputDevice";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        String str3;
        InputMonitor inputMonitor;
        IBinder iBinder1;
        ITabletModeChangedListener iTabletModeChangedListener;
        IInputDevicesChangedListener iInputDevicesChangedListener;
        String str2, arrayOfString[], str1;
        KeyboardLayout keyboardLayout, arrayOfKeyboardLayout[];
        TouchCalibration touchCalibration;
        VerifiedInputEvent verifiedInputEvent;
        boolean[] arrayOfBoolean;
        int[] arrayOfInt1;
        String str5;
        IBinder iBinder2;
        long[] arrayOfLong;
        String str4;
        int arrayOfInt2[], m;
        boolean bool = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 31:
            param1Parcel1.enforceInterface("android.hardware.input.IInputManager");
            str3 = param1Parcel1.readString();
            removePortAssociation(str3);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            str3.enforceInterface("android.hardware.input.IInputManager");
            str5 = str3.readString();
            param1Int1 = str3.readInt();
            addPortAssociation(str5, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            str3.enforceInterface("android.hardware.input.IInputManager");
            str5 = str3.readString();
            param1Int1 = str3.readInt();
            inputMonitor = monitorGestureInput(str5, param1Int1);
            param1Parcel2.writeNoException();
            if (inputMonitor != null) {
              param1Parcel2.writeInt(1);
              inputMonitor.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 28:
            inputMonitor.enforceInterface("android.hardware.input.IInputManager");
            iBinder2 = inputMonitor.readStrongBinder();
            if (inputMonitor.readInt() != 0)
              bool = true; 
            requestPointerCapture(iBinder2, bool);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            inputMonitor.enforceInterface("android.hardware.input.IInputManager");
            if (inputMonitor.readInt() != 0) {
              PointerIcon pointerIcon = (PointerIcon)PointerIcon.CREATOR.createFromParcel((Parcel)inputMonitor);
            } else {
              inputMonitor = null;
            } 
            setCustomPointerIcon((PointerIcon)inputMonitor);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            inputMonitor.enforceInterface("android.hardware.input.IInputManager");
            param1Int1 = inputMonitor.readInt();
            setPointerIconType(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            inputMonitor.enforceInterface("android.hardware.input.IInputManager");
            param1Int1 = inputMonitor.readInt();
            iBinder1 = inputMonitor.readStrongBinder();
            cancelVibrate(param1Int1, iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            iBinder1.enforceInterface("android.hardware.input.IInputManager");
            param1Int2 = iBinder1.readInt();
            arrayOfLong = iBinder1.createLongArray();
            param1Int1 = iBinder1.readInt();
            iBinder1 = iBinder1.readStrongBinder();
            vibrate(param1Int2, arrayOfLong, param1Int1, iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            iBinder1.enforceInterface("android.hardware.input.IInputManager");
            param1Int1 = isMicMuted();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 22:
            iBinder1.enforceInterface("android.hardware.input.IInputManager");
            iTabletModeChangedListener = ITabletModeChangedListener.Stub.asInterface(iBinder1.readStrongBinder());
            registerTabletModeChangedListener(iTabletModeChangedListener);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            iTabletModeChangedListener.enforceInterface("android.hardware.input.IInputManager");
            param1Int1 = isInTabletMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 20:
            iTabletModeChangedListener.enforceInterface("android.hardware.input.IInputManager");
            iInputDevicesChangedListener = IInputDevicesChangedListener.Stub.asInterface(iTabletModeChangedListener.readStrongBinder());
            registerInputDevicesChangedListener(iInputDevicesChangedListener);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            iInputDevicesChangedListener.enforceInterface("android.hardware.input.IInputManager");
            if (iInputDevicesChangedListener.readInt() != 0) {
              InputDeviceIdentifier inputDeviceIdentifier = (InputDeviceIdentifier)InputDeviceIdentifier.CREATOR.createFromParcel((Parcel)iInputDevicesChangedListener);
            } else {
              arrayOfLong = null;
            } 
            str2 = iInputDevicesChangedListener.readString();
            removeKeyboardLayoutForInputDevice((InputDeviceIdentifier)arrayOfLong, str2);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            str2.enforceInterface("android.hardware.input.IInputManager");
            if (str2.readInt() != 0) {
              InputDeviceIdentifier inputDeviceIdentifier = (InputDeviceIdentifier)InputDeviceIdentifier.CREATOR.createFromParcel((Parcel)str2);
            } else {
              arrayOfLong = null;
            } 
            str2 = str2.readString();
            addKeyboardLayoutForInputDevice((InputDeviceIdentifier)arrayOfLong, str2);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            str2.enforceInterface("android.hardware.input.IInputManager");
            if (str2.readInt() != 0) {
              InputDeviceIdentifier inputDeviceIdentifier = (InputDeviceIdentifier)InputDeviceIdentifier.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            arrayOfString = getEnabledKeyboardLayoutsForInputDevice((InputDeviceIdentifier)str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 16:
            arrayOfString.enforceInterface("android.hardware.input.IInputManager");
            if (arrayOfString.readInt() != 0) {
              InputDeviceIdentifier inputDeviceIdentifier = (InputDeviceIdentifier)InputDeviceIdentifier.CREATOR.createFromParcel((Parcel)arrayOfString);
            } else {
              arrayOfLong = null;
            } 
            str1 = arrayOfString.readString();
            setCurrentKeyboardLayoutForInputDevice((InputDeviceIdentifier)arrayOfLong, str1);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str1.enforceInterface("android.hardware.input.IInputManager");
            if (str1.readInt() != 0) {
              InputDeviceIdentifier inputDeviceIdentifier = (InputDeviceIdentifier)InputDeviceIdentifier.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            str1 = getCurrentKeyboardLayoutForInputDevice((InputDeviceIdentifier)str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 14:
            str1.enforceInterface("android.hardware.input.IInputManager");
            str1 = str1.readString();
            keyboardLayout = getKeyboardLayout(str1);
            param1Parcel2.writeNoException();
            if (keyboardLayout != null) {
              param1Parcel2.writeInt(1);
              keyboardLayout.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 13:
            keyboardLayout.enforceInterface("android.hardware.input.IInputManager");
            if (keyboardLayout.readInt() != 0) {
              InputDeviceIdentifier inputDeviceIdentifier = (InputDeviceIdentifier)InputDeviceIdentifier.CREATOR.createFromParcel((Parcel)keyboardLayout);
            } else {
              keyboardLayout = null;
            } 
            arrayOfKeyboardLayout = getKeyboardLayoutsForInputDevice((InputDeviceIdentifier)keyboardLayout);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfKeyboardLayout, 1);
            return true;
          case 12:
            arrayOfKeyboardLayout.enforceInterface("android.hardware.input.IInputManager");
            arrayOfKeyboardLayout = getKeyboardLayouts();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfKeyboardLayout, 1);
            return true;
          case 11:
            arrayOfKeyboardLayout.enforceInterface("android.hardware.input.IInputManager");
            str4 = arrayOfKeyboardLayout.readString();
            param1Int1 = arrayOfKeyboardLayout.readInt();
            if (arrayOfKeyboardLayout.readInt() != 0) {
              TouchCalibration touchCalibration1 = (TouchCalibration)TouchCalibration.CREATOR.createFromParcel((Parcel)arrayOfKeyboardLayout);
            } else {
              arrayOfKeyboardLayout = null;
            } 
            setTouchCalibrationForInputDevice(str4, param1Int1, (TouchCalibration)arrayOfKeyboardLayout);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            arrayOfKeyboardLayout.enforceInterface("android.hardware.input.IInputManager");
            str4 = arrayOfKeyboardLayout.readString();
            param1Int1 = arrayOfKeyboardLayout.readInt();
            touchCalibration = getTouchCalibrationForInputDevice(str4, param1Int1);
            param1Parcel2.writeNoException();
            if (touchCalibration != null) {
              param1Parcel2.writeInt(1);
              touchCalibration.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 9:
            touchCalibration.enforceInterface("android.hardware.input.IInputManager");
            if (touchCalibration.readInt() != 0) {
              InputEvent inputEvent = (InputEvent)InputEvent.CREATOR.createFromParcel((Parcel)touchCalibration);
            } else {
              touchCalibration = null;
            } 
            verifiedInputEvent = verifyInputEvent((InputEvent)touchCalibration);
            param1Parcel2.writeNoException();
            if (verifiedInputEvent != null) {
              param1Parcel2.writeInt(1);
              verifiedInputEvent.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 8:
            verifiedInputEvent.enforceInterface("android.hardware.input.IInputManager");
            if (verifiedInputEvent.readInt() != 0) {
              InputEvent inputEvent = (InputEvent)InputEvent.CREATOR.createFromParcel((Parcel)verifiedInputEvent);
            } else {
              str4 = null;
            } 
            param1Int1 = verifiedInputEvent.readInt();
            bool3 = injectInputEvent((InputEvent)str4, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 7:
            verifiedInputEvent.enforceInterface("android.hardware.input.IInputManager");
            k = verifiedInputEvent.readInt();
            tryPointerSpeed(k);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            verifiedInputEvent.enforceInterface("android.hardware.input.IInputManager");
            param1Int2 = verifiedInputEvent.readInt();
            k = verifiedInputEvent.readInt();
            arrayOfInt2 = verifiedInputEvent.createIntArray();
            m = verifiedInputEvent.readInt();
            if (m < 0) {
              verifiedInputEvent = null;
            } else {
              arrayOfBoolean = new boolean[m];
            } 
            bool2 = hasKeys(param1Int2, k, arrayOfInt2, arrayOfBoolean);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            param1Parcel2.writeBooleanArray(arrayOfBoolean);
            return true;
          case 5:
            arrayOfBoolean.enforceInterface("android.hardware.input.IInputManager");
            j = arrayOfBoolean.readInt();
            disableInputDevice(j);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            arrayOfBoolean.enforceInterface("android.hardware.input.IInputManager");
            j = arrayOfBoolean.readInt();
            enableInputDevice(j);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            arrayOfBoolean.enforceInterface("android.hardware.input.IInputManager");
            j = arrayOfBoolean.readInt();
            bool1 = isInputDeviceEnabled(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            arrayOfBoolean.enforceInterface("android.hardware.input.IInputManager");
            arrayOfInt1 = getInputDeviceIds();
            param1Parcel2.writeNoException();
            param1Parcel2.writeIntArray(arrayOfInt1);
            return true;
          case 1:
            break;
        } 
        arrayOfInt1.enforceInterface("android.hardware.input.IInputManager");
        int i = arrayOfInt1.readInt();
        InputDevice inputDevice = getInputDevice(i);
        param1Parcel2.writeNoException();
        if (inputDevice != null) {
          param1Parcel2.writeInt(1);
          inputDevice.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("android.hardware.input.IInputManager");
      return true;
    }
    
    private static class Proxy implements IInputManager {
      public static IInputManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.input.IInputManager";
      }
      
      public InputDevice getInputDevice(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          InputDevice inputDevice;
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            inputDevice = IInputManager.Stub.getDefaultImpl().getInputDevice(param2Int);
            return inputDevice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            inputDevice = (InputDevice)InputDevice.CREATOR.createFromParcel(parcel2);
          } else {
            inputDevice = null;
          } 
          return inputDevice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getInputDeviceIds() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null)
            return IInputManager.Stub.getDefaultImpl().getInputDeviceIds(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInputDeviceEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && IInputManager.Stub.getDefaultImpl() != null) {
            bool1 = IInputManager.Stub.getDefaultImpl().isInputDeviceEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableInputDevice(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().enableInputDevice(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableInputDevice(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().disableInputDevice(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasKeys(int param2Int1, int param2Int2, int[] param2ArrayOfint, boolean[] param2ArrayOfboolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeIntArray(param2ArrayOfint);
          if (param2ArrayOfboolean == null) {
            parcel1.writeInt(-1);
          } else {
            parcel1.writeInt(param2ArrayOfboolean.length);
          } 
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IInputManager.Stub.getDefaultImpl() != null) {
            bool1 = IInputManager.Stub.getDefaultImpl().hasKeys(param2Int1, param2Int2, param2ArrayOfint, param2ArrayOfboolean);
            return bool1;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            bool1 = true; 
          parcel2.readBooleanArray(param2ArrayOfboolean);
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void tryPointerSpeed(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().tryPointerSpeed(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean injectInputEvent(InputEvent param2InputEvent, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          boolean bool1 = true;
          if (param2InputEvent != null) {
            parcel1.writeInt(1);
            param2InputEvent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool2 && IInputManager.Stub.getDefaultImpl() != null) {
            bool1 = IInputManager.Stub.getDefaultImpl().injectInputEvent(param2InputEvent, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public VerifiedInputEvent verifyInputEvent(InputEvent param2InputEvent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          if (param2InputEvent != null) {
            parcel1.writeInt(1);
            param2InputEvent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null)
            return IInputManager.Stub.getDefaultImpl().verifyInputEvent(param2InputEvent); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            VerifiedInputEvent verifiedInputEvent = (VerifiedInputEvent)VerifiedInputEvent.CREATOR.createFromParcel(parcel2);
          } else {
            param2InputEvent = null;
          } 
          return (VerifiedInputEvent)param2InputEvent;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public TouchCalibration getTouchCalibrationForInputDevice(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null)
            return IInputManager.Stub.getDefaultImpl().getTouchCalibrationForInputDevice(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            TouchCalibration touchCalibration = (TouchCalibration)TouchCalibration.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (TouchCalibration)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTouchCalibrationForInputDevice(String param2String, int param2Int, TouchCalibration param2TouchCalibration) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2TouchCalibration != null) {
            parcel1.writeInt(1);
            param2TouchCalibration.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().setTouchCalibrationForInputDevice(param2String, param2Int, param2TouchCalibration);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public KeyboardLayout[] getKeyboardLayouts() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null)
            return IInputManager.Stub.getDefaultImpl().getKeyboardLayouts(); 
          parcel2.readException();
          return (KeyboardLayout[])parcel2.createTypedArray(KeyboardLayout.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public KeyboardLayout[] getKeyboardLayoutsForInputDevice(InputDeviceIdentifier param2InputDeviceIdentifier) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          if (param2InputDeviceIdentifier != null) {
            parcel1.writeInt(1);
            param2InputDeviceIdentifier.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null)
            return IInputManager.Stub.getDefaultImpl().getKeyboardLayoutsForInputDevice(param2InputDeviceIdentifier); 
          parcel2.readException();
          return (KeyboardLayout[])parcel2.createTypedArray(KeyboardLayout.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public KeyboardLayout getKeyboardLayout(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null)
            return IInputManager.Stub.getDefaultImpl().getKeyboardLayout(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            KeyboardLayout keyboardLayout = (KeyboardLayout)KeyboardLayout.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (KeyboardLayout)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCurrentKeyboardLayoutForInputDevice(InputDeviceIdentifier param2InputDeviceIdentifier) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          if (param2InputDeviceIdentifier != null) {
            parcel1.writeInt(1);
            param2InputDeviceIdentifier.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null)
            return IInputManager.Stub.getDefaultImpl().getCurrentKeyboardLayoutForInputDevice(param2InputDeviceIdentifier); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCurrentKeyboardLayoutForInputDevice(InputDeviceIdentifier param2InputDeviceIdentifier, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          if (param2InputDeviceIdentifier != null) {
            parcel1.writeInt(1);
            param2InputDeviceIdentifier.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().setCurrentKeyboardLayoutForInputDevice(param2InputDeviceIdentifier, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getEnabledKeyboardLayoutsForInputDevice(InputDeviceIdentifier param2InputDeviceIdentifier) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          if (param2InputDeviceIdentifier != null) {
            parcel1.writeInt(1);
            param2InputDeviceIdentifier.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null)
            return IInputManager.Stub.getDefaultImpl().getEnabledKeyboardLayoutsForInputDevice(param2InputDeviceIdentifier); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addKeyboardLayoutForInputDevice(InputDeviceIdentifier param2InputDeviceIdentifier, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          if (param2InputDeviceIdentifier != null) {
            parcel1.writeInt(1);
            param2InputDeviceIdentifier.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().addKeyboardLayoutForInputDevice(param2InputDeviceIdentifier, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeKeyboardLayoutForInputDevice(InputDeviceIdentifier param2InputDeviceIdentifier, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          if (param2InputDeviceIdentifier != null) {
            parcel1.writeInt(1);
            param2InputDeviceIdentifier.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().removeKeyboardLayoutForInputDevice(param2InputDeviceIdentifier, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerInputDevicesChangedListener(IInputDevicesChangedListener param2IInputDevicesChangedListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          if (param2IInputDevicesChangedListener != null) {
            iBinder = param2IInputDevicesChangedListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().registerInputDevicesChangedListener(param2IInputDevicesChangedListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int isInTabletMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null)
            return IInputManager.Stub.getDefaultImpl().isInTabletMode(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerTabletModeChangedListener(ITabletModeChangedListener param2ITabletModeChangedListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          if (param2ITabletModeChangedListener != null) {
            iBinder = param2ITabletModeChangedListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().registerTabletModeChangedListener(param2ITabletModeChangedListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int isMicMuted() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null)
            return IInputManager.Stub.getDefaultImpl().isMicMuted(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void vibrate(int param2Int1, long[] param2ArrayOflong, int param2Int2, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeLongArray(param2ArrayOflong);
          parcel1.writeInt(param2Int2);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().vibrate(param2Int1, param2ArrayOflong, param2Int2, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelVibrate(int param2Int, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeInt(param2Int);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().cancelVibrate(param2Int, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPointerIconType(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().setPointerIconType(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCustomPointerIcon(PointerIcon param2PointerIcon) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          if (param2PointerIcon != null) {
            parcel1.writeInt(1);
            param2PointerIcon.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().setCustomPointerIcon(param2PointerIcon);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestPointerCapture(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool1 && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().requestPointerCapture(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public InputMonitor monitorGestureInput(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null)
            return IInputManager.Stub.getDefaultImpl().monitorGestureInput(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            InputMonitor inputMonitor = (InputMonitor)InputMonitor.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (InputMonitor)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addPortAssociation(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().addPortAssociation(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removePortAssociation(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.input.IInputManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IInputManager.Stub.getDefaultImpl() != null) {
            IInputManager.Stub.getDefaultImpl().removePortAssociation(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInputManager param1IInputManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInputManager != null) {
          Proxy.sDefaultImpl = param1IInputManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInputManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
