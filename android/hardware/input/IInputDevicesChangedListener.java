package android.hardware.input;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IInputDevicesChangedListener extends IInterface {
  void onInputDevicesChanged(int[] paramArrayOfint) throws RemoteException;
  
  class Default implements IInputDevicesChangedListener {
    public void onInputDevicesChanged(int[] param1ArrayOfint) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IInputDevicesChangedListener {
    private static final String DESCRIPTOR = "android.hardware.input.IInputDevicesChangedListener";
    
    static final int TRANSACTION_onInputDevicesChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.hardware.input.IInputDevicesChangedListener");
    }
    
    public static IInputDevicesChangedListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.input.IInputDevicesChangedListener");
      if (iInterface != null && iInterface instanceof IInputDevicesChangedListener)
        return (IInputDevicesChangedListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onInputDevicesChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.hardware.input.IInputDevicesChangedListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.input.IInputDevicesChangedListener");
      int[] arrayOfInt = param1Parcel1.createIntArray();
      onInputDevicesChanged(arrayOfInt);
      return true;
    }
    
    private static class Proxy implements IInputDevicesChangedListener {
      public static IInputDevicesChangedListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.input.IInputDevicesChangedListener";
      }
      
      public void onInputDevicesChanged(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.input.IInputDevicesChangedListener");
          parcel.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IInputDevicesChangedListener.Stub.getDefaultImpl() != null) {
            IInputDevicesChangedListener.Stub.getDefaultImpl().onInputDevicesChanged(param2ArrayOfint);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IInputDevicesChangedListener param1IInputDevicesChangedListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IInputDevicesChangedListener != null) {
          Proxy.sDefaultImpl = param1IInputDevicesChangedListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IInputDevicesChangedListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
