package android.hardware.input;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITabletModeChangedListener extends IInterface {
  void onTabletModeChanged(long paramLong, boolean paramBoolean) throws RemoteException;
  
  class Default implements ITabletModeChangedListener {
    public void onTabletModeChanged(long param1Long, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITabletModeChangedListener {
    private static final String DESCRIPTOR = "android.hardware.input.ITabletModeChangedListener";
    
    static final int TRANSACTION_onTabletModeChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.hardware.input.ITabletModeChangedListener");
    }
    
    public static ITabletModeChangedListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.input.ITabletModeChangedListener");
      if (iInterface != null && iInterface instanceof ITabletModeChangedListener)
        return (ITabletModeChangedListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onTabletModeChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.hardware.input.ITabletModeChangedListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.input.ITabletModeChangedListener");
      long l = param1Parcel1.readLong();
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      onTabletModeChanged(l, bool);
      return true;
    }
    
    private static class Proxy implements ITabletModeChangedListener {
      public static ITabletModeChangedListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.input.ITabletModeChangedListener";
      }
      
      public void onTabletModeChanged(long param2Long, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.hardware.input.ITabletModeChangedListener");
          parcel.writeLong(param2Long);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && ITabletModeChangedListener.Stub.getDefaultImpl() != null) {
            ITabletModeChangedListener.Stub.getDefaultImpl().onTabletModeChanged(param2Long, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITabletModeChangedListener param1ITabletModeChangedListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITabletModeChangedListener != null) {
          Proxy.sDefaultImpl = param1ITabletModeChangedListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITabletModeChangedListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
