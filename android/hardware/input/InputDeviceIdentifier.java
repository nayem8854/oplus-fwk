package android.hardware.input;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.Objects;

public final class InputDeviceIdentifier implements Parcelable {
  public InputDeviceIdentifier(String paramString, int paramInt1, int paramInt2) {
    this.mDescriptor = paramString;
    this.mVendorId = paramInt1;
    this.mProductId = paramInt2;
  }
  
  private InputDeviceIdentifier(Parcel paramParcel) {
    this.mDescriptor = paramParcel.readString();
    this.mVendorId = paramParcel.readInt();
    this.mProductId = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mDescriptor);
    paramParcel.writeInt(this.mVendorId);
    paramParcel.writeInt(this.mProductId);
  }
  
  public String getDescriptor() {
    return this.mDescriptor;
  }
  
  public int getVendorId() {
    return this.mVendorId;
  }
  
  public int getProductId() {
    return this.mProductId;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || !(paramObject instanceof InputDeviceIdentifier))
      return false; 
    InputDeviceIdentifier inputDeviceIdentifier = (InputDeviceIdentifier)paramObject;
    if (this.mVendorId == inputDeviceIdentifier.mVendorId && this.mProductId == inputDeviceIdentifier.mProductId) {
      paramObject = this.mDescriptor;
      String str = inputDeviceIdentifier.mDescriptor;
      if (TextUtils.equals((CharSequence)paramObject, str))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mDescriptor, Integer.valueOf(this.mVendorId), Integer.valueOf(this.mProductId) });
  }
  
  public static final Parcelable.Creator<InputDeviceIdentifier> CREATOR = new Parcelable.Creator<InputDeviceIdentifier>() {
      public InputDeviceIdentifier createFromParcel(Parcel param1Parcel) {
        return new InputDeviceIdentifier(param1Parcel);
      }
      
      public InputDeviceIdentifier[] newArray(int param1Int) {
        return new InputDeviceIdentifier[param1Int];
      }
    };
  
  private final String mDescriptor;
  
  private final int mProductId;
  
  private final int mVendorId;
}
