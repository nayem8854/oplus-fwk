package android.hardware.vibrator.V1_0;

import java.util.ArrayList;

public final class EffectStrength {
  public static final byte LIGHT = 0;
  
  public static final byte MEDIUM = 1;
  
  public static final byte STRONG = 2;
  
  public static final String toString(byte paramByte) {
    if (paramByte == 0)
      return "LIGHT"; 
    if (paramByte == 1)
      return "MEDIUM"; 
    if (paramByte == 2)
      return "STRONG"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(Byte.toUnsignedInt(paramByte)));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(byte paramByte) {
    ArrayList<String> arrayList = new ArrayList();
    byte b1 = 0;
    arrayList.add("LIGHT");
    if ((paramByte & 0x1) == 1) {
      arrayList.add("MEDIUM");
      b1 = (byte)(false | true);
    } 
    byte b2 = b1;
    if ((paramByte & 0x2) == 2) {
      arrayList.add("STRONG");
      b2 = (byte)(b1 | 0x2);
    } 
    if (paramByte != b2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString(Byte.toUnsignedInt((byte)((b2 ^ 0xFFFFFFFF) & paramByte))));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
