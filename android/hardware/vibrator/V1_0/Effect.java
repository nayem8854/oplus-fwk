package android.hardware.vibrator.V1_0;

import java.util.ArrayList;

public final class Effect {
  public static final int CLICK = 0;
  
  public static final int DOUBLE_CLICK = 1;
  
  public static final String toString(int paramInt) {
    if (paramInt == 0)
      return "CLICK"; 
    if (paramInt == 1)
      return "DOUBLE_CLICK"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    arrayList.add("CLICK");
    if ((paramInt & 0x1) == 1) {
      arrayList.add("DOUBLE_CLICK");
      i = false | true;
    } 
    if (paramInt != i) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((i ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
