package android.hardware.vibrator.V1_0;

import java.util.ArrayList;

public final class Status {
  public static final int BAD_VALUE = 2;
  
  public static final int OK = 0;
  
  public static final int UNKNOWN_ERROR = 1;
  
  public static final int UNSUPPORTED_OPERATION = 3;
  
  public static final String toString(int paramInt) {
    if (paramInt == 0)
      return "OK"; 
    if (paramInt == 1)
      return "UNKNOWN_ERROR"; 
    if (paramInt == 2)
      return "BAD_VALUE"; 
    if (paramInt == 3)
      return "UNSUPPORTED_OPERATION"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    arrayList.add("OK");
    if ((paramInt & 0x1) == 1) {
      arrayList.add("UNKNOWN_ERROR");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("BAD_VALUE");
      j = i | 0x2;
    } 
    i = j;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("UNSUPPORTED_OPERATION");
      i = j | 0x3;
    } 
    if (paramInt != i) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((i ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
