package android.hardware.vibrator.V1_3;

import java.util.ArrayList;

public final class Effect {
  public static final int CLICK = 0;
  
  public static final int DOUBLE_CLICK = 1;
  
  public static final int HEAVY_CLICK = 5;
  
  public static final int POP = 4;
  
  public static final int RINGTONE_1 = 6;
  
  public static final int RINGTONE_10 = 15;
  
  public static final int RINGTONE_11 = 16;
  
  public static final int RINGTONE_12 = 17;
  
  public static final int RINGTONE_13 = 18;
  
  public static final int RINGTONE_14 = 19;
  
  public static final int RINGTONE_15 = 20;
  
  public static final int RINGTONE_2 = 7;
  
  public static final int RINGTONE_3 = 8;
  
  public static final int RINGTONE_4 = 9;
  
  public static final int RINGTONE_5 = 10;
  
  public static final int RINGTONE_6 = 11;
  
  public static final int RINGTONE_7 = 12;
  
  public static final int RINGTONE_8 = 13;
  
  public static final int RINGTONE_9 = 14;
  
  public static final int TEXTURE_TICK = 21;
  
  public static final int THUD = 3;
  
  public static final int TICK = 2;
  
  public static final String toString(int paramInt) {
    if (paramInt == 0)
      return "CLICK"; 
    if (paramInt == 1)
      return "DOUBLE_CLICK"; 
    if (paramInt == 2)
      return "TICK"; 
    if (paramInt == 3)
      return "THUD"; 
    if (paramInt == 4)
      return "POP"; 
    if (paramInt == 5)
      return "HEAVY_CLICK"; 
    if (paramInt == 6)
      return "RINGTONE_1"; 
    if (paramInt == 7)
      return "RINGTONE_2"; 
    if (paramInt == 8)
      return "RINGTONE_3"; 
    if (paramInt == 9)
      return "RINGTONE_4"; 
    if (paramInt == 10)
      return "RINGTONE_5"; 
    if (paramInt == 11)
      return "RINGTONE_6"; 
    if (paramInt == 12)
      return "RINGTONE_7"; 
    if (paramInt == 13)
      return "RINGTONE_8"; 
    if (paramInt == 14)
      return "RINGTONE_9"; 
    if (paramInt == 15)
      return "RINGTONE_10"; 
    if (paramInt == 16)
      return "RINGTONE_11"; 
    if (paramInt == 17)
      return "RINGTONE_12"; 
    if (paramInt == 18)
      return "RINGTONE_13"; 
    if (paramInt == 19)
      return "RINGTONE_14"; 
    if (paramInt == 20)
      return "RINGTONE_15"; 
    if (paramInt == 21)
      return "TEXTURE_TICK"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    arrayList.add("CLICK");
    if ((paramInt & 0x1) == 1) {
      arrayList.add("DOUBLE_CLICK");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("TICK");
      j = i | 0x2;
    } 
    i = j;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("THUD");
      i = j | 0x3;
    } 
    j = i;
    if ((paramInt & 0x4) == 4) {
      arrayList.add("POP");
      j = i | 0x4;
    } 
    i = j;
    if ((paramInt & 0x5) == 5) {
      arrayList.add("HEAVY_CLICK");
      i = j | 0x5;
    } 
    j = i;
    if ((paramInt & 0x6) == 6) {
      arrayList.add("RINGTONE_1");
      j = i | 0x6;
    } 
    i = j;
    if ((paramInt & 0x7) == 7) {
      arrayList.add("RINGTONE_2");
      i = j | 0x7;
    } 
    j = i;
    if ((paramInt & 0x8) == 8) {
      arrayList.add("RINGTONE_3");
      j = i | 0x8;
    } 
    i = j;
    if ((paramInt & 0x9) == 9) {
      arrayList.add("RINGTONE_4");
      i = j | 0x9;
    } 
    j = i;
    if ((paramInt & 0xA) == 10) {
      arrayList.add("RINGTONE_5");
      j = i | 0xA;
    } 
    i = j;
    if ((paramInt & 0xB) == 11) {
      arrayList.add("RINGTONE_6");
      i = j | 0xB;
    } 
    j = i;
    if ((paramInt & 0xC) == 12) {
      arrayList.add("RINGTONE_7");
      j = i | 0xC;
    } 
    i = j;
    if ((paramInt & 0xD) == 13) {
      arrayList.add("RINGTONE_8");
      i = j | 0xD;
    } 
    j = i;
    if ((paramInt & 0xE) == 14) {
      arrayList.add("RINGTONE_9");
      j = i | 0xE;
    } 
    i = j;
    if ((paramInt & 0xF) == 15) {
      arrayList.add("RINGTONE_10");
      i = j | 0xF;
    } 
    j = i;
    if ((paramInt & 0x10) == 16) {
      arrayList.add("RINGTONE_11");
      j = i | 0x10;
    } 
    int k = j;
    if ((paramInt & 0x11) == 17) {
      arrayList.add("RINGTONE_12");
      k = j | 0x11;
    } 
    i = k;
    if ((paramInt & 0x12) == 18) {
      arrayList.add("RINGTONE_13");
      i = k | 0x12;
    } 
    j = i;
    if ((paramInt & 0x13) == 19) {
      arrayList.add("RINGTONE_14");
      j = i | 0x13;
    } 
    i = j;
    if ((paramInt & 0x14) == 20) {
      arrayList.add("RINGTONE_15");
      i = j | 0x14;
    } 
    j = i;
    if ((paramInt & 0x15) == 21) {
      arrayList.add("TEXTURE_TICK");
      j = i | 0x15;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
