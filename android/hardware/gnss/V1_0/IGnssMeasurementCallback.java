package android.hardware.gnss.V1_0;

import android.internal.hidl.base.V1_0.DebugInfo;
import android.internal.hidl.base.V1_0.IBase;
import android.os.HidlSupport;
import android.os.HwBinder;
import android.os.HwBlob;
import android.os.HwParcel;
import android.os.IHwBinder;
import android.os.IHwInterface;
import android.os.NativeHandle;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public interface IGnssMeasurementCallback extends IBase {
  public static final String kInterfaceName = "android.hardware.gnss@1.0::IGnssMeasurementCallback";
  
  static IGnssMeasurementCallback asInterface(IHwBinder paramIHwBinder) {
    if (paramIHwBinder == null)
      return null; 
    IHwInterface iHwInterface = paramIHwBinder.queryLocalInterface("android.hardware.gnss@1.0::IGnssMeasurementCallback");
    if (iHwInterface != null && iHwInterface instanceof IGnssMeasurementCallback)
      return (IGnssMeasurementCallback)iHwInterface; 
    Proxy proxy = new Proxy(paramIHwBinder);
    try {
      for (String str : proxy.interfaceChain()) {
        boolean bool = str.equals("android.hardware.gnss@1.0::IGnssMeasurementCallback");
        if (bool)
          return proxy; 
      } 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  static IGnssMeasurementCallback castFrom(IHwInterface paramIHwInterface) {
    IGnssMeasurementCallback iGnssMeasurementCallback;
    if (paramIHwInterface == null) {
      paramIHwInterface = null;
    } else {
      iGnssMeasurementCallback = asInterface(paramIHwInterface.asBinder());
    } 
    return iGnssMeasurementCallback;
  }
  
  static IGnssMeasurementCallback getService(String paramString, boolean paramBoolean) throws RemoteException {
    return asInterface(HwBinder.getService("android.hardware.gnss@1.0::IGnssMeasurementCallback", paramString, paramBoolean));
  }
  
  static IGnssMeasurementCallback getService(boolean paramBoolean) throws RemoteException {
    return getService("default", paramBoolean);
  }
  
  static IGnssMeasurementCallback getService(String paramString) throws RemoteException {
    return asInterface(HwBinder.getService("android.hardware.gnss@1.0::IGnssMeasurementCallback", paramString));
  }
  
  static IGnssMeasurementCallback getService() throws RemoteException {
    return getService("default");
  }
  
  void GnssMeasurementCb(GnssData paramGnssData) throws RemoteException;
  
  IHwBinder asBinder();
  
  void debug(NativeHandle paramNativeHandle, ArrayList<String> paramArrayList) throws RemoteException;
  
  DebugInfo getDebugInfo() throws RemoteException;
  
  ArrayList<byte[]> getHashChain() throws RemoteException;
  
  ArrayList<String> interfaceChain() throws RemoteException;
  
  String interfaceDescriptor() throws RemoteException;
  
  boolean linkToDeath(IHwBinder.DeathRecipient paramDeathRecipient, long paramLong) throws RemoteException;
  
  void notifySyspropsChanged() throws RemoteException;
  
  void ping() throws RemoteException;
  
  void setHALInstrumentation() throws RemoteException;
  
  boolean unlinkToDeath(IHwBinder.DeathRecipient paramDeathRecipient) throws RemoteException;
  
  class GnssClockFlags {
    public static final short HAS_BIAS = 8;
    
    public static final short HAS_BIAS_UNCERTAINTY = 16;
    
    public static final short HAS_DRIFT = 32;
    
    public static final short HAS_DRIFT_UNCERTAINTY = 64;
    
    public static final short HAS_FULL_BIAS = 4;
    
    public static final short HAS_LEAP_SECOND = 1;
    
    public static final short HAS_TIME_UNCERTAINTY = 2;
    
    public static final String toString(short param1Short) {
      if (param1Short == 1)
        return "HAS_LEAP_SECOND"; 
      if (param1Short == 2)
        return "HAS_TIME_UNCERTAINTY"; 
      if (param1Short == 4)
        return "HAS_FULL_BIAS"; 
      if (param1Short == 8)
        return "HAS_BIAS"; 
      if (param1Short == 16)
        return "HAS_BIAS_UNCERTAINTY"; 
      if (param1Short == 32)
        return "HAS_DRIFT"; 
      if (param1Short == 64)
        return "HAS_DRIFT_UNCERTAINTY"; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString(Short.toUnsignedInt(param1Short)));
      return stringBuilder.toString();
    }
    
    public static final String dumpBitfield(short param1Short) {
      ArrayList<String> arrayList = new ArrayList();
      short s1 = 0;
      if ((param1Short & 0x1) == 1) {
        arrayList.add("HAS_LEAP_SECOND");
        s1 = (short)(false | true);
      } 
      short s2 = s1;
      if ((param1Short & 0x2) == 2) {
        arrayList.add("HAS_TIME_UNCERTAINTY");
        s2 = (short)(s1 | 0x2);
      } 
      s1 = s2;
      if ((param1Short & 0x4) == 4) {
        arrayList.add("HAS_FULL_BIAS");
        s1 = (short)(s2 | 0x4);
      } 
      short s3 = s1;
      if ((param1Short & 0x8) == 8) {
        arrayList.add("HAS_BIAS");
        s3 = (short)(s1 | 0x8);
      } 
      s2 = s3;
      if ((param1Short & 0x10) == 16) {
        arrayList.add("HAS_BIAS_UNCERTAINTY");
        s2 = (short)(s3 | 0x10);
      } 
      s1 = s2;
      if ((param1Short & 0x20) == 32) {
        arrayList.add("HAS_DRIFT");
        s1 = (short)(s2 | 0x20);
      } 
      s2 = s1;
      if ((param1Short & 0x40) == 64) {
        arrayList.add("HAS_DRIFT_UNCERTAINTY");
        s2 = (short)(s1 | 0x40);
      } 
      if (param1Short != s2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("0x");
        stringBuilder.append(Integer.toHexString(Short.toUnsignedInt((short)((s2 ^ 0xFFFFFFFF) & param1Short))));
        arrayList.add(stringBuilder.toString());
      } 
      return String.join(" | ", (Iterable)arrayList);
    }
  }
  
  class GnssMeasurementFlags {
    public static final int HAS_AUTOMATIC_GAIN_CONTROL = 8192;
    
    public static final int HAS_CARRIER_CYCLES = 1024;
    
    public static final int HAS_CARRIER_FREQUENCY = 512;
    
    public static final int HAS_CARRIER_PHASE = 2048;
    
    public static final int HAS_CARRIER_PHASE_UNCERTAINTY = 4096;
    
    public static final int HAS_SNR = 1;
    
    public static final String toString(int param1Int) {
      if (param1Int == 1)
        return "HAS_SNR"; 
      if (param1Int == 512)
        return "HAS_CARRIER_FREQUENCY"; 
      if (param1Int == 1024)
        return "HAS_CARRIER_CYCLES"; 
      if (param1Int == 2048)
        return "HAS_CARRIER_PHASE"; 
      if (param1Int == 4096)
        return "HAS_CARRIER_PHASE_UNCERTAINTY"; 
      if (param1Int == 8192)
        return "HAS_AUTOMATIC_GAIN_CONTROL"; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString(param1Int));
      return stringBuilder.toString();
    }
    
    public static final String dumpBitfield(int param1Int) {
      ArrayList<String> arrayList = new ArrayList();
      int i = 0;
      if ((param1Int & 0x1) == 1) {
        arrayList.add("HAS_SNR");
        i = false | true;
      } 
      int j = i;
      if ((param1Int & 0x200) == 512) {
        arrayList.add("HAS_CARRIER_FREQUENCY");
        j = i | 0x200;
      } 
      i = j;
      if ((param1Int & 0x400) == 1024) {
        arrayList.add("HAS_CARRIER_CYCLES");
        i = j | 0x400;
      } 
      j = i;
      if ((param1Int & 0x800) == 2048) {
        arrayList.add("HAS_CARRIER_PHASE");
        j = i | 0x800;
      } 
      i = j;
      if ((param1Int & 0x1000) == 4096) {
        arrayList.add("HAS_CARRIER_PHASE_UNCERTAINTY");
        i = j | 0x1000;
      } 
      j = i;
      if ((param1Int & 0x2000) == 8192) {
        arrayList.add("HAS_AUTOMATIC_GAIN_CONTROL");
        j = i | 0x2000;
      } 
      if (param1Int != j) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("0x");
        stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & param1Int));
        arrayList.add(stringBuilder.toString());
      } 
      return String.join(" | ", (Iterable)arrayList);
    }
  }
  
  class GnssMultipathIndicator {
    public static final byte INDICATIOR_NOT_PRESENT = 2;
    
    public static final byte INDICATOR_PRESENT = 1;
    
    public static final byte INDICATOR_UNKNOWN = 0;
    
    public static final String toString(byte param1Byte) {
      if (param1Byte == 0)
        return "INDICATOR_UNKNOWN"; 
      if (param1Byte == 1)
        return "INDICATOR_PRESENT"; 
      if (param1Byte == 2)
        return "INDICATIOR_NOT_PRESENT"; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString(Byte.toUnsignedInt(param1Byte)));
      return stringBuilder.toString();
    }
    
    public static final String dumpBitfield(byte param1Byte) {
      ArrayList<String> arrayList = new ArrayList();
      byte b1 = 0;
      arrayList.add("INDICATOR_UNKNOWN");
      if ((param1Byte & 0x1) == 1) {
        arrayList.add("INDICATOR_PRESENT");
        b1 = (byte)(false | true);
      } 
      byte b2 = b1;
      if ((param1Byte & 0x2) == 2) {
        arrayList.add("INDICATIOR_NOT_PRESENT");
        b2 = (byte)(b1 | 0x2);
      } 
      if (param1Byte != b2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("0x");
        stringBuilder.append(Integer.toHexString(Byte.toUnsignedInt((byte)((b2 ^ 0xFFFFFFFF) & param1Byte))));
        arrayList.add(stringBuilder.toString());
      } 
      return String.join(" | ", (Iterable)arrayList);
    }
  }
  
  class GnssMeasurementState {
    public static final int STATE_BDS_D2_BIT_SYNC = 256;
    
    public static final int STATE_BDS_D2_SUBFRAME_SYNC = 512;
    
    public static final int STATE_BIT_SYNC = 2;
    
    public static final int STATE_CODE_LOCK = 1;
    
    public static final int STATE_GAL_E1BC_CODE_LOCK = 1024;
    
    public static final int STATE_GAL_E1B_PAGE_SYNC = 4096;
    
    public static final int STATE_GAL_E1C_2ND_CODE_LOCK = 2048;
    
    public static final int STATE_GLO_STRING_SYNC = 64;
    
    public static final int STATE_GLO_TOD_DECODED = 128;
    
    public static final int STATE_GLO_TOD_KNOWN = 32768;
    
    public static final int STATE_MSEC_AMBIGUOUS = 16;
    
    public static final int STATE_SBAS_SYNC = 8192;
    
    public static final int STATE_SUBFRAME_SYNC = 4;
    
    public static final int STATE_SYMBOL_SYNC = 32;
    
    public static final int STATE_TOW_DECODED = 8;
    
    public static final int STATE_TOW_KNOWN = 16384;
    
    public static final int STATE_UNKNOWN = 0;
    
    public static final String toString(int param1Int) {
      if (param1Int == 0)
        return "STATE_UNKNOWN"; 
      if (param1Int == 1)
        return "STATE_CODE_LOCK"; 
      if (param1Int == 2)
        return "STATE_BIT_SYNC"; 
      if (param1Int == 4)
        return "STATE_SUBFRAME_SYNC"; 
      if (param1Int == 8)
        return "STATE_TOW_DECODED"; 
      if (param1Int == 16)
        return "STATE_MSEC_AMBIGUOUS"; 
      if (param1Int == 32)
        return "STATE_SYMBOL_SYNC"; 
      if (param1Int == 64)
        return "STATE_GLO_STRING_SYNC"; 
      if (param1Int == 128)
        return "STATE_GLO_TOD_DECODED"; 
      if (param1Int == 256)
        return "STATE_BDS_D2_BIT_SYNC"; 
      if (param1Int == 512)
        return "STATE_BDS_D2_SUBFRAME_SYNC"; 
      if (param1Int == 1024)
        return "STATE_GAL_E1BC_CODE_LOCK"; 
      if (param1Int == 2048)
        return "STATE_GAL_E1C_2ND_CODE_LOCK"; 
      if (param1Int == 4096)
        return "STATE_GAL_E1B_PAGE_SYNC"; 
      if (param1Int == 8192)
        return "STATE_SBAS_SYNC"; 
      if (param1Int == 16384)
        return "STATE_TOW_KNOWN"; 
      if (param1Int == 32768)
        return "STATE_GLO_TOD_KNOWN"; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString(param1Int));
      return stringBuilder.toString();
    }
    
    public static final String dumpBitfield(int param1Int) {
      ArrayList<String> arrayList = new ArrayList();
      int i = 0;
      arrayList.add("STATE_UNKNOWN");
      if ((param1Int & 0x1) == 1) {
        arrayList.add("STATE_CODE_LOCK");
        i = false | true;
      } 
      int j = i;
      if ((param1Int & 0x2) == 2) {
        arrayList.add("STATE_BIT_SYNC");
        j = i | 0x2;
      } 
      i = j;
      if ((param1Int & 0x4) == 4) {
        arrayList.add("STATE_SUBFRAME_SYNC");
        i = j | 0x4;
      } 
      j = i;
      if ((param1Int & 0x8) == 8) {
        arrayList.add("STATE_TOW_DECODED");
        j = i | 0x8;
      } 
      i = j;
      if ((param1Int & 0x10) == 16) {
        arrayList.add("STATE_MSEC_AMBIGUOUS");
        i = j | 0x10;
      } 
      j = i;
      if ((param1Int & 0x20) == 32) {
        arrayList.add("STATE_SYMBOL_SYNC");
        j = i | 0x20;
      } 
      i = j;
      if ((param1Int & 0x40) == 64) {
        arrayList.add("STATE_GLO_STRING_SYNC");
        i = j | 0x40;
      } 
      j = i;
      if ((param1Int & 0x80) == 128) {
        arrayList.add("STATE_GLO_TOD_DECODED");
        j = i | 0x80;
      } 
      i = j;
      if ((param1Int & 0x100) == 256) {
        arrayList.add("STATE_BDS_D2_BIT_SYNC");
        i = j | 0x100;
      } 
      int k = i;
      if ((param1Int & 0x200) == 512) {
        arrayList.add("STATE_BDS_D2_SUBFRAME_SYNC");
        k = i | 0x200;
      } 
      j = k;
      if ((param1Int & 0x400) == 1024) {
        arrayList.add("STATE_GAL_E1BC_CODE_LOCK");
        j = k | 0x400;
      } 
      i = j;
      if ((param1Int & 0x800) == 2048) {
        arrayList.add("STATE_GAL_E1C_2ND_CODE_LOCK");
        i = j | 0x800;
      } 
      j = i;
      if ((param1Int & 0x1000) == 4096) {
        arrayList.add("STATE_GAL_E1B_PAGE_SYNC");
        j = i | 0x1000;
      } 
      k = j;
      if ((param1Int & 0x2000) == 8192) {
        arrayList.add("STATE_SBAS_SYNC");
        k = j | 0x2000;
      } 
      i = k;
      if ((param1Int & 0x4000) == 16384) {
        arrayList.add("STATE_TOW_KNOWN");
        i = k | 0x4000;
      } 
      j = i;
      if ((param1Int & 0x8000) == 32768) {
        arrayList.add("STATE_GLO_TOD_KNOWN");
        j = i | 0x8000;
      } 
      if (param1Int != j) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("0x");
        stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & param1Int));
        arrayList.add(stringBuilder.toString());
      } 
      return String.join(" | ", (Iterable)arrayList);
    }
  }
  
  class GnssAccumulatedDeltaRangeState {
    public static final short ADR_STATE_CYCLE_SLIP = 4;
    
    public static final short ADR_STATE_RESET = 2;
    
    public static final short ADR_STATE_UNKNOWN = 0;
    
    public static final short ADR_STATE_VALID = 1;
    
    public static final String toString(short param1Short) {
      if (param1Short == 0)
        return "ADR_STATE_UNKNOWN"; 
      if (param1Short == 1)
        return "ADR_STATE_VALID"; 
      if (param1Short == 2)
        return "ADR_STATE_RESET"; 
      if (param1Short == 4)
        return "ADR_STATE_CYCLE_SLIP"; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString(Short.toUnsignedInt(param1Short)));
      return stringBuilder.toString();
    }
    
    public static final String dumpBitfield(short param1Short) {
      ArrayList<String> arrayList = new ArrayList();
      short s1 = 0;
      arrayList.add("ADR_STATE_UNKNOWN");
      if ((param1Short & 0x1) == 1) {
        arrayList.add("ADR_STATE_VALID");
        s1 = (short)(false | true);
      } 
      short s2 = s1;
      if ((param1Short & 0x2) == 2) {
        arrayList.add("ADR_STATE_RESET");
        s2 = (short)(s1 | 0x2);
      } 
      s1 = s2;
      if ((param1Short & 0x4) == 4) {
        arrayList.add("ADR_STATE_CYCLE_SLIP");
        s1 = (short)(s2 | 0x4);
      } 
      if (param1Short != s1) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("0x");
        stringBuilder.append(Integer.toHexString(Short.toUnsignedInt((short)((s1 ^ 0xFFFFFFFF) & param1Short))));
        arrayList.add(stringBuilder.toString());
      } 
      return String.join(" | ", (Iterable)arrayList);
    }
  }
  
  class GnssClock {
    public double biasNs;
    
    public double biasUncertaintyNs;
    
    public double driftNsps;
    
    public double driftUncertaintyNsps;
    
    public long fullBiasNs;
    
    public short gnssClockFlags;
    
    public int hwClockDiscontinuityCount;
    
    public short leapSecond;
    
    public long timeNs;
    
    public double timeUncertaintyNs;
    
    public GnssClock() {
      this.leapSecond = 0;
      this.timeNs = 0L;
      this.timeUncertaintyNs = 0.0D;
      this.fullBiasNs = 0L;
      this.biasNs = 0.0D;
      this.biasUncertaintyNs = 0.0D;
      this.driftNsps = 0.0D;
      this.driftUncertaintyNsps = 0.0D;
      this.hwClockDiscontinuityCount = 0;
    }
    
    public final boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (param1Object.getClass() != GnssClock.class)
        return false; 
      param1Object = param1Object;
      if (!HidlSupport.deepEquals(Short.valueOf(this.gnssClockFlags), Short.valueOf(((GnssClock)param1Object).gnssClockFlags)))
        return false; 
      if (this.leapSecond != ((GnssClock)param1Object).leapSecond)
        return false; 
      if (this.timeNs != ((GnssClock)param1Object).timeNs)
        return false; 
      if (this.timeUncertaintyNs != ((GnssClock)param1Object).timeUncertaintyNs)
        return false; 
      if (this.fullBiasNs != ((GnssClock)param1Object).fullBiasNs)
        return false; 
      if (this.biasNs != ((GnssClock)param1Object).biasNs)
        return false; 
      if (this.biasUncertaintyNs != ((GnssClock)param1Object).biasUncertaintyNs)
        return false; 
      if (this.driftNsps != ((GnssClock)param1Object).driftNsps)
        return false; 
      if (this.driftUncertaintyNsps != ((GnssClock)param1Object).driftUncertaintyNsps)
        return false; 
      if (this.hwClockDiscontinuityCount != ((GnssClock)param1Object).hwClockDiscontinuityCount)
        return false; 
      return true;
    }
    
    public final int hashCode() {
      short s = this.gnssClockFlags;
      int i = HidlSupport.deepHashCode(Short.valueOf(s));
      s = this.leapSecond;
      int j = HidlSupport.deepHashCode(Short.valueOf(s));
      long l = this.timeNs;
      int k = HidlSupport.deepHashCode(Long.valueOf(l));
      double d = this.timeUncertaintyNs;
      int m = HidlSupport.deepHashCode(Double.valueOf(d));
      l = this.fullBiasNs;
      int n = HidlSupport.deepHashCode(Long.valueOf(l));
      d = this.biasNs;
      int i1 = HidlSupport.deepHashCode(Double.valueOf(d));
      d = this.biasUncertaintyNs;
      int i2 = HidlSupport.deepHashCode(Double.valueOf(d));
      d = this.driftNsps;
      int i3 = HidlSupport.deepHashCode(Double.valueOf(d));
      d = this.driftUncertaintyNsps;
      int i4 = HidlSupport.deepHashCode(Double.valueOf(d)), i5 = this.hwClockDiscontinuityCount;
      i5 = HidlSupport.deepHashCode(Integer.valueOf(i5));
      return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5) });
    }
    
    public final String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      stringBuilder.append(".gnssClockFlags = ");
      stringBuilder.append(IGnssMeasurementCallback.GnssClockFlags.dumpBitfield(this.gnssClockFlags));
      stringBuilder.append(", .leapSecond = ");
      stringBuilder.append(this.leapSecond);
      stringBuilder.append(", .timeNs = ");
      stringBuilder.append(this.timeNs);
      stringBuilder.append(", .timeUncertaintyNs = ");
      stringBuilder.append(this.timeUncertaintyNs);
      stringBuilder.append(", .fullBiasNs = ");
      stringBuilder.append(this.fullBiasNs);
      stringBuilder.append(", .biasNs = ");
      stringBuilder.append(this.biasNs);
      stringBuilder.append(", .biasUncertaintyNs = ");
      stringBuilder.append(this.biasUncertaintyNs);
      stringBuilder.append(", .driftNsps = ");
      stringBuilder.append(this.driftNsps);
      stringBuilder.append(", .driftUncertaintyNsps = ");
      stringBuilder.append(this.driftUncertaintyNsps);
      stringBuilder.append(", .hwClockDiscontinuityCount = ");
      stringBuilder.append(this.hwClockDiscontinuityCount);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public final void readFromParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = param1HwParcel.readBuffer(72L);
      readEmbeddedFromParcel(param1HwParcel, hwBlob, 0L);
    }
    
    public static final ArrayList<GnssClock> readVectorFromParcel(HwParcel param1HwParcel) {
      ArrayList<GnssClock> arrayList = new ArrayList();
      HwBlob hwBlob = param1HwParcel.readBuffer(16L);
      int i = hwBlob.getInt32(8L);
      long l1 = (i * 72);
      long l2 = hwBlob.handle();
      hwBlob = param1HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
      arrayList.clear();
      for (byte b = 0; b < i; b++) {
        GnssClock gnssClock = new GnssClock();
        gnssClock.readEmbeddedFromParcel(param1HwParcel, hwBlob, (b * 72));
        arrayList.add(gnssClock);
      } 
      return arrayList;
    }
    
    public final void readEmbeddedFromParcel(HwParcel param1HwParcel, HwBlob param1HwBlob, long param1Long) {
      this.gnssClockFlags = param1HwBlob.getInt16(0L + param1Long);
      this.leapSecond = param1HwBlob.getInt16(2L + param1Long);
      this.timeNs = param1HwBlob.getInt64(8L + param1Long);
      this.timeUncertaintyNs = param1HwBlob.getDouble(16L + param1Long);
      this.fullBiasNs = param1HwBlob.getInt64(24L + param1Long);
      this.biasNs = param1HwBlob.getDouble(32L + param1Long);
      this.biasUncertaintyNs = param1HwBlob.getDouble(40L + param1Long);
      this.driftNsps = param1HwBlob.getDouble(48L + param1Long);
      this.driftUncertaintyNsps = param1HwBlob.getDouble(56L + param1Long);
      this.hwClockDiscontinuityCount = param1HwBlob.getInt32(64L + param1Long);
    }
    
    public final void writeToParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = new HwBlob(72);
      writeEmbeddedToBlob(hwBlob, 0L);
      param1HwParcel.writeBuffer(hwBlob);
    }
    
    public static final void writeVectorToParcel(HwParcel param1HwParcel, ArrayList<GnssClock> param1ArrayList) {
      HwBlob hwBlob1 = new HwBlob(16);
      int i = param1ArrayList.size();
      hwBlob1.putInt32(8L, i);
      hwBlob1.putBool(12L, false);
      HwBlob hwBlob2 = new HwBlob(i * 72);
      for (byte b = 0; b < i; b++)
        ((GnssClock)param1ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 72)); 
      hwBlob1.putBlob(0L, hwBlob2);
      param1HwParcel.writeBuffer(hwBlob1);
    }
    
    public final void writeEmbeddedToBlob(HwBlob param1HwBlob, long param1Long) {
      param1HwBlob.putInt16(0L + param1Long, this.gnssClockFlags);
      param1HwBlob.putInt16(2L + param1Long, this.leapSecond);
      param1HwBlob.putInt64(8L + param1Long, this.timeNs);
      param1HwBlob.putDouble(16L + param1Long, this.timeUncertaintyNs);
      param1HwBlob.putInt64(24L + param1Long, this.fullBiasNs);
      param1HwBlob.putDouble(32L + param1Long, this.biasNs);
      param1HwBlob.putDouble(40L + param1Long, this.biasUncertaintyNs);
      param1HwBlob.putDouble(48L + param1Long, this.driftNsps);
      param1HwBlob.putDouble(56L + param1Long, this.driftUncertaintyNsps);
      param1HwBlob.putInt32(64L + param1Long, this.hwClockDiscontinuityCount);
    }
  }
  
  class GnssMeasurement {
    public short svid = 0;
    
    public byte constellation = 0;
    
    public double timeOffsetNs = 0.0D;
    
    public long receivedSvTimeInNs = 0L;
    
    public long receivedSvTimeUncertaintyInNs = 0L;
    
    public double cN0DbHz = 0.0D;
    
    public double pseudorangeRateMps = 0.0D;
    
    public double pseudorangeRateUncertaintyMps = 0.0D;
    
    public double accumulatedDeltaRangeM = 0.0D;
    
    public double accumulatedDeltaRangeUncertaintyM = 0.0D;
    
    public float carrierFrequencyHz = 0.0F;
    
    public long carrierCycles = 0L;
    
    public double carrierPhase = 0.0D;
    
    public double carrierPhaseUncertainty = 0.0D;
    
    public byte multipathIndicator = 0;
    
    public double snrDb = 0.0D;
    
    public double agcLevelDb = 0.0D;
    
    public short accumulatedDeltaRangeState;
    
    public int flags;
    
    public int state;
    
    public final boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (param1Object.getClass() != GnssMeasurement.class)
        return false; 
      param1Object = param1Object;
      if (!HidlSupport.deepEquals(Integer.valueOf(this.flags), Integer.valueOf(((GnssMeasurement)param1Object).flags)))
        return false; 
      if (this.svid != ((GnssMeasurement)param1Object).svid)
        return false; 
      if (this.constellation != ((GnssMeasurement)param1Object).constellation)
        return false; 
      if (this.timeOffsetNs != ((GnssMeasurement)param1Object).timeOffsetNs)
        return false; 
      if (!HidlSupport.deepEquals(Integer.valueOf(this.state), Integer.valueOf(((GnssMeasurement)param1Object).state)))
        return false; 
      if (this.receivedSvTimeInNs != ((GnssMeasurement)param1Object).receivedSvTimeInNs)
        return false; 
      if (this.receivedSvTimeUncertaintyInNs != ((GnssMeasurement)param1Object).receivedSvTimeUncertaintyInNs)
        return false; 
      if (this.cN0DbHz != ((GnssMeasurement)param1Object).cN0DbHz)
        return false; 
      if (this.pseudorangeRateMps != ((GnssMeasurement)param1Object).pseudorangeRateMps)
        return false; 
      if (this.pseudorangeRateUncertaintyMps != ((GnssMeasurement)param1Object).pseudorangeRateUncertaintyMps)
        return false; 
      if (!HidlSupport.deepEquals(Short.valueOf(this.accumulatedDeltaRangeState), Short.valueOf(((GnssMeasurement)param1Object).accumulatedDeltaRangeState)))
        return false; 
      if (this.accumulatedDeltaRangeM != ((GnssMeasurement)param1Object).accumulatedDeltaRangeM)
        return false; 
      if (this.accumulatedDeltaRangeUncertaintyM != ((GnssMeasurement)param1Object).accumulatedDeltaRangeUncertaintyM)
        return false; 
      if (this.carrierFrequencyHz != ((GnssMeasurement)param1Object).carrierFrequencyHz)
        return false; 
      if (this.carrierCycles != ((GnssMeasurement)param1Object).carrierCycles)
        return false; 
      if (this.carrierPhase != ((GnssMeasurement)param1Object).carrierPhase)
        return false; 
      if (this.carrierPhaseUncertainty != ((GnssMeasurement)param1Object).carrierPhaseUncertainty)
        return false; 
      if (this.multipathIndicator != ((GnssMeasurement)param1Object).multipathIndicator)
        return false; 
      if (this.snrDb != ((GnssMeasurement)param1Object).snrDb)
        return false; 
      if (this.agcLevelDb != ((GnssMeasurement)param1Object).agcLevelDb)
        return false; 
      return true;
    }
    
    public final int hashCode() {
      int i = this.flags;
      int j = HidlSupport.deepHashCode(Integer.valueOf(i));
      short s = this.svid;
      int k = HidlSupport.deepHashCode(Short.valueOf(s));
      byte b = this.constellation;
      i = HidlSupport.deepHashCode(Byte.valueOf(b));
      double d = this.timeOffsetNs;
      int m = HidlSupport.deepHashCode(Double.valueOf(d)), n = this.state;
      n = HidlSupport.deepHashCode(Integer.valueOf(n));
      long l = this.receivedSvTimeInNs;
      int i1 = HidlSupport.deepHashCode(Long.valueOf(l));
      l = this.receivedSvTimeUncertaintyInNs;
      int i2 = HidlSupport.deepHashCode(Long.valueOf(l));
      d = this.cN0DbHz;
      int i3 = HidlSupport.deepHashCode(Double.valueOf(d));
      d = this.pseudorangeRateMps;
      int i4 = HidlSupport.deepHashCode(Double.valueOf(d));
      d = this.pseudorangeRateUncertaintyMps;
      int i5 = HidlSupport.deepHashCode(Double.valueOf(d));
      s = this.accumulatedDeltaRangeState;
      int i6 = HidlSupport.deepHashCode(Short.valueOf(s));
      d = this.accumulatedDeltaRangeM;
      int i7 = HidlSupport.deepHashCode(Double.valueOf(d));
      d = this.accumulatedDeltaRangeUncertaintyM;
      int i8 = HidlSupport.deepHashCode(Double.valueOf(d));
      float f = this.carrierFrequencyHz;
      int i9 = HidlSupport.deepHashCode(Float.valueOf(f));
      l = this.carrierCycles;
      int i10 = HidlSupport.deepHashCode(Long.valueOf(l));
      d = this.carrierPhase;
      int i11 = HidlSupport.deepHashCode(Double.valueOf(d));
      d = this.carrierPhaseUncertainty;
      int i12 = HidlSupport.deepHashCode(Double.valueOf(d));
      b = this.multipathIndicator;
      int i13 = HidlSupport.deepHashCode(Byte.valueOf(b));
      d = this.snrDb;
      int i14 = HidlSupport.deepHashCode(Double.valueOf(d));
      d = this.agcLevelDb;
      int i15 = HidlSupport.deepHashCode(Double.valueOf(d));
      return Objects.hash(new Object[] { 
            Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(i), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), 
            Integer.valueOf(i6), Integer.valueOf(i7), Integer.valueOf(i8), Integer.valueOf(i9), Integer.valueOf(i10), Integer.valueOf(i11), Integer.valueOf(i12), Integer.valueOf(i13), Integer.valueOf(i14), Integer.valueOf(i15) });
    }
    
    public final String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      stringBuilder.append(".flags = ");
      stringBuilder.append(IGnssMeasurementCallback.GnssMeasurementFlags.dumpBitfield(this.flags));
      stringBuilder.append(", .svid = ");
      stringBuilder.append(this.svid);
      stringBuilder.append(", .constellation = ");
      stringBuilder.append(GnssConstellationType.toString(this.constellation));
      stringBuilder.append(", .timeOffsetNs = ");
      stringBuilder.append(this.timeOffsetNs);
      stringBuilder.append(", .state = ");
      stringBuilder.append(IGnssMeasurementCallback.GnssMeasurementState.dumpBitfield(this.state));
      stringBuilder.append(", .receivedSvTimeInNs = ");
      stringBuilder.append(this.receivedSvTimeInNs);
      stringBuilder.append(", .receivedSvTimeUncertaintyInNs = ");
      stringBuilder.append(this.receivedSvTimeUncertaintyInNs);
      stringBuilder.append(", .cN0DbHz = ");
      stringBuilder.append(this.cN0DbHz);
      stringBuilder.append(", .pseudorangeRateMps = ");
      stringBuilder.append(this.pseudorangeRateMps);
      stringBuilder.append(", .pseudorangeRateUncertaintyMps = ");
      stringBuilder.append(this.pseudorangeRateUncertaintyMps);
      stringBuilder.append(", .accumulatedDeltaRangeState = ");
      stringBuilder.append(IGnssMeasurementCallback.GnssAccumulatedDeltaRangeState.dumpBitfield(this.accumulatedDeltaRangeState));
      stringBuilder.append(", .accumulatedDeltaRangeM = ");
      stringBuilder.append(this.accumulatedDeltaRangeM);
      stringBuilder.append(", .accumulatedDeltaRangeUncertaintyM = ");
      stringBuilder.append(this.accumulatedDeltaRangeUncertaintyM);
      stringBuilder.append(", .carrierFrequencyHz = ");
      stringBuilder.append(this.carrierFrequencyHz);
      stringBuilder.append(", .carrierCycles = ");
      stringBuilder.append(this.carrierCycles);
      stringBuilder.append(", .carrierPhase = ");
      stringBuilder.append(this.carrierPhase);
      stringBuilder.append(", .carrierPhaseUncertainty = ");
      stringBuilder.append(this.carrierPhaseUncertainty);
      stringBuilder.append(", .multipathIndicator = ");
      stringBuilder.append(IGnssMeasurementCallback.GnssMultipathIndicator.toString(this.multipathIndicator));
      stringBuilder.append(", .snrDb = ");
      stringBuilder.append(this.snrDb);
      stringBuilder.append(", .agcLevelDb = ");
      stringBuilder.append(this.agcLevelDb);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public final void readFromParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = param1HwParcel.readBuffer(144L);
      readEmbeddedFromParcel(param1HwParcel, hwBlob, 0L);
    }
    
    public static final ArrayList<GnssMeasurement> readVectorFromParcel(HwParcel param1HwParcel) {
      ArrayList<GnssMeasurement> arrayList = new ArrayList();
      HwBlob hwBlob = param1HwParcel.readBuffer(16L);
      int i = hwBlob.getInt32(8L);
      long l1 = (i * 144);
      long l2 = hwBlob.handle();
      hwBlob = param1HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
      arrayList.clear();
      for (byte b = 0; b < i; b++) {
        GnssMeasurement gnssMeasurement = new GnssMeasurement();
        gnssMeasurement.readEmbeddedFromParcel(param1HwParcel, hwBlob, (b * 144));
        arrayList.add(gnssMeasurement);
      } 
      return arrayList;
    }
    
    public final void readEmbeddedFromParcel(HwParcel param1HwParcel, HwBlob param1HwBlob, long param1Long) {
      this.flags = param1HwBlob.getInt32(0L + param1Long);
      this.svid = param1HwBlob.getInt16(4L + param1Long);
      this.constellation = param1HwBlob.getInt8(6L + param1Long);
      this.timeOffsetNs = param1HwBlob.getDouble(8L + param1Long);
      this.state = param1HwBlob.getInt32(16L + param1Long);
      this.receivedSvTimeInNs = param1HwBlob.getInt64(24L + param1Long);
      this.receivedSvTimeUncertaintyInNs = param1HwBlob.getInt64(32L + param1Long);
      this.cN0DbHz = param1HwBlob.getDouble(40L + param1Long);
      this.pseudorangeRateMps = param1HwBlob.getDouble(48L + param1Long);
      this.pseudorangeRateUncertaintyMps = param1HwBlob.getDouble(56L + param1Long);
      this.accumulatedDeltaRangeState = param1HwBlob.getInt16(64L + param1Long);
      this.accumulatedDeltaRangeM = param1HwBlob.getDouble(72L + param1Long);
      this.accumulatedDeltaRangeUncertaintyM = param1HwBlob.getDouble(80L + param1Long);
      this.carrierFrequencyHz = param1HwBlob.getFloat(88L + param1Long);
      this.carrierCycles = param1HwBlob.getInt64(96L + param1Long);
      this.carrierPhase = param1HwBlob.getDouble(104L + param1Long);
      this.carrierPhaseUncertainty = param1HwBlob.getDouble(112L + param1Long);
      this.multipathIndicator = param1HwBlob.getInt8(120L + param1Long);
      this.snrDb = param1HwBlob.getDouble(128L + param1Long);
      this.agcLevelDb = param1HwBlob.getDouble(136L + param1Long);
    }
    
    public final void writeToParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = new HwBlob(144);
      writeEmbeddedToBlob(hwBlob, 0L);
      param1HwParcel.writeBuffer(hwBlob);
    }
    
    public static final void writeVectorToParcel(HwParcel param1HwParcel, ArrayList<GnssMeasurement> param1ArrayList) {
      HwBlob hwBlob1 = new HwBlob(16);
      int i = param1ArrayList.size();
      hwBlob1.putInt32(8L, i);
      hwBlob1.putBool(12L, false);
      HwBlob hwBlob2 = new HwBlob(i * 144);
      for (byte b = 0; b < i; b++)
        ((GnssMeasurement)param1ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 144)); 
      hwBlob1.putBlob(0L, hwBlob2);
      param1HwParcel.writeBuffer(hwBlob1);
    }
    
    public final void writeEmbeddedToBlob(HwBlob param1HwBlob, long param1Long) {
      param1HwBlob.putInt32(0L + param1Long, this.flags);
      param1HwBlob.putInt16(4L + param1Long, this.svid);
      param1HwBlob.putInt8(6L + param1Long, this.constellation);
      param1HwBlob.putDouble(8L + param1Long, this.timeOffsetNs);
      param1HwBlob.putInt32(16L + param1Long, this.state);
      param1HwBlob.putInt64(24L + param1Long, this.receivedSvTimeInNs);
      param1HwBlob.putInt64(32L + param1Long, this.receivedSvTimeUncertaintyInNs);
      param1HwBlob.putDouble(40L + param1Long, this.cN0DbHz);
      param1HwBlob.putDouble(48L + param1Long, this.pseudorangeRateMps);
      param1HwBlob.putDouble(56L + param1Long, this.pseudorangeRateUncertaintyMps);
      param1HwBlob.putInt16(64L + param1Long, this.accumulatedDeltaRangeState);
      param1HwBlob.putDouble(72L + param1Long, this.accumulatedDeltaRangeM);
      param1HwBlob.putDouble(80L + param1Long, this.accumulatedDeltaRangeUncertaintyM);
      param1HwBlob.putFloat(88L + param1Long, this.carrierFrequencyHz);
      param1HwBlob.putInt64(96L + param1Long, this.carrierCycles);
      param1HwBlob.putDouble(104L + param1Long, this.carrierPhase);
      param1HwBlob.putDouble(112L + param1Long, this.carrierPhaseUncertainty);
      param1HwBlob.putInt8(120L + param1Long, this.multipathIndicator);
      param1HwBlob.putDouble(128L + param1Long, this.snrDb);
      param1HwBlob.putDouble(136L + param1Long, this.agcLevelDb);
    }
  }
  
  class GnssData {
    public IGnssMeasurementCallback.GnssClock clock;
    
    public int measurementCount;
    
    public IGnssMeasurementCallback.GnssMeasurement[] measurements;
    
    public GnssData() {
      this.measurementCount = 0;
      this.measurements = new IGnssMeasurementCallback.GnssMeasurement[64];
      this.clock = new IGnssMeasurementCallback.GnssClock();
    }
    
    public final boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (param1Object.getClass() != GnssData.class)
        return false; 
      param1Object = param1Object;
      if (this.measurementCount != ((GnssData)param1Object).measurementCount)
        return false; 
      if (!HidlSupport.deepEquals(this.measurements, ((GnssData)param1Object).measurements))
        return false; 
      if (!HidlSupport.deepEquals(this.clock, ((GnssData)param1Object).clock))
        return false; 
      return true;
    }
    
    public final int hashCode() {
      int i = this.measurementCount;
      int j = HidlSupport.deepHashCode(Integer.valueOf(i));
      IGnssMeasurementCallback.GnssMeasurement[] arrayOfGnssMeasurement = this.measurements;
      int k = HidlSupport.deepHashCode(arrayOfGnssMeasurement);
      IGnssMeasurementCallback.GnssClock gnssClock = this.clock;
      i = HidlSupport.deepHashCode(gnssClock);
      return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(i) });
    }
    
    public final String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      stringBuilder.append(".measurementCount = ");
      stringBuilder.append(this.measurementCount);
      stringBuilder.append(", .measurements = ");
      stringBuilder.append(Arrays.toString((Object[])this.measurements));
      stringBuilder.append(", .clock = ");
      stringBuilder.append(this.clock);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public final void readFromParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = param1HwParcel.readBuffer(9296L);
      readEmbeddedFromParcel(param1HwParcel, hwBlob, 0L);
    }
    
    public static final ArrayList<GnssData> readVectorFromParcel(HwParcel param1HwParcel) {
      ArrayList<GnssData> arrayList = new ArrayList();
      HwBlob hwBlob1 = param1HwParcel.readBuffer(16L);
      int i = hwBlob1.getInt32(8L);
      long l1 = (i * 9296);
      long l2 = hwBlob1.handle();
      HwBlob hwBlob2 = param1HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
      arrayList.clear();
      for (byte b = 0; b < i; b++) {
        GnssData gnssData = new GnssData();
        gnssData.readEmbeddedFromParcel(param1HwParcel, hwBlob2, (b * 9296));
        arrayList.add(gnssData);
      } 
      return arrayList;
    }
    
    public final void readEmbeddedFromParcel(HwParcel param1HwParcel, HwBlob param1HwBlob, long param1Long) {
      this.measurementCount = param1HwBlob.getInt32(0L + param1Long);
      long l = 8L + param1Long;
      for (byte b = 0; b < 64; b++) {
        this.measurements[b] = new IGnssMeasurementCallback.GnssMeasurement();
        this.measurements[b].readEmbeddedFromParcel(param1HwParcel, param1HwBlob, l);
        l += 144L;
      } 
      this.clock.readEmbeddedFromParcel(param1HwParcel, param1HwBlob, 9224L + param1Long);
    }
    
    public final void writeToParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = new HwBlob(9296);
      writeEmbeddedToBlob(hwBlob, 0L);
      param1HwParcel.writeBuffer(hwBlob);
    }
    
    public static final void writeVectorToParcel(HwParcel param1HwParcel, ArrayList<GnssData> param1ArrayList) {
      HwBlob hwBlob1 = new HwBlob(16);
      int i = param1ArrayList.size();
      hwBlob1.putInt32(8L, i);
      hwBlob1.putBool(12L, false);
      HwBlob hwBlob2 = new HwBlob(i * 9296);
      for (byte b = 0; b < i; b++)
        ((GnssData)param1ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 9296)); 
      hwBlob1.putBlob(0L, hwBlob2);
      param1HwParcel.writeBuffer(hwBlob1);
    }
    
    public final void writeEmbeddedToBlob(HwBlob param1HwBlob, long param1Long) {
      param1HwBlob.putInt32(0L + param1Long, this.measurementCount);
      long l = 8L + param1Long;
      for (byte b = 0; b < 64; b++) {
        this.measurements[b].writeEmbeddedToBlob(param1HwBlob, l);
        l += 144L;
      } 
      this.clock.writeEmbeddedToBlob(param1HwBlob, 9224L + param1Long);
    }
  }
  
  class Proxy implements IGnssMeasurementCallback {
    private IHwBinder mRemote;
    
    public Proxy(IGnssMeasurementCallback this$0) {
      Objects.requireNonNull(this$0);
      this.mRemote = (IHwBinder)this$0;
    }
    
    public IHwBinder asBinder() {
      return this.mRemote;
    }
    
    public String toString() {
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(interfaceDescriptor());
        stringBuilder.append("@Proxy");
        return stringBuilder.toString();
      } catch (RemoteException remoteException) {
        return "[class or subclass of android.hardware.gnss@1.0::IGnssMeasurementCallback]@Proxy";
      } 
    }
    
    public final boolean equals(Object param1Object) {
      return HidlSupport.interfacesEqual((IHwInterface)this, param1Object);
    }
    
    public final int hashCode() {
      return asBinder().hashCode();
    }
    
    public void GnssMeasurementCb(IGnssMeasurementCallback.GnssData param1GnssData) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.gnss@1.0::IGnssMeasurementCallback");
      param1GnssData.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(1, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<String> interfaceChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256067662, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readStringVector();
      } finally {
        hwParcel.release();
      } 
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hidl.base@1.0::IBase");
      hwParcel2.writeNativeHandle(param1NativeHandle);
      hwParcel2.writeStringVector(param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(256131655, hwParcel2, hwParcel1, 0);
        hwParcel1.verifySuccess();
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public String interfaceDescriptor() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256136003, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<byte[]> getHashChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256398152, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ArrayList<byte[]> arrayList = new ArrayList();
        this();
        HwBlob hwBlob = hwParcel.readBuffer(16L);
        int i = hwBlob.getInt32(8L);
        long l1 = (i * 32);
        long l2 = hwBlob.handle();
        hwBlob = hwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
        arrayList.clear();
        for (byte b = 0; b < i; b++) {
          byte[] arrayOfByte = new byte[32];
          l1 = (b * 32);
          hwBlob.copyToInt8Array(l1, arrayOfByte, 32);
          arrayList.add(arrayOfByte);
        } 
        return arrayList;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setHALInstrumentation() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256462420, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) throws RemoteException {
      return this.mRemote.linkToDeath(param1DeathRecipient, param1Long);
    }
    
    public void ping() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256921159, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public DebugInfo getDebugInfo() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257049926, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        DebugInfo debugInfo = new DebugInfo();
        this();
        debugInfo.readFromParcel(hwParcel);
        return debugInfo;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void notifySyspropsChanged() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257120595, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) throws RemoteException {
      return this.mRemote.unlinkToDeath(param1DeathRecipient);
    }
  }
  
  class Stub extends HwBinder implements IGnssMeasurementCallback {
    public IHwBinder asBinder() {
      return (IHwBinder)this;
    }
    
    public final ArrayList<String> interfaceChain() {
      return new ArrayList<>(Arrays.asList(new String[] { "android.hardware.gnss@1.0::IGnssMeasurementCallback", "android.hidl.base@1.0::IBase" }));
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) {}
    
    public final String interfaceDescriptor() {
      return "android.hardware.gnss@1.0::IGnssMeasurementCallback";
    }
    
    public final ArrayList<byte[]> getHashChain() {
      return (ArrayList)new ArrayList<>(Arrays.asList((byte[])new byte[][] { { 
                -41, 2, -5, 1, -36, 42, 7, 51, -86, -126, 
                11, 126, -74, 84, 53, -18, 51, 52, -9, 86, 
                50, -17, -120, 11, -81, -46, -5, -120, 3, -94, 
                10, 88 }, { 
                -20, Byte.MAX_VALUE, -41, -98, -48, 45, -6, -123, -68, 73, 
                -108, 38, -83, -82, 62, -66, 35, -17, 5, 36, 
                -13, -51, 105, 87, 19, -109, 36, -72, 59, 24, 
                -54, 76 } }));
    }
    
    public final void setHALInstrumentation() {}
    
    public final boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) {
      return true;
    }
    
    public final void ping() {}
    
    public final DebugInfo getDebugInfo() {
      DebugInfo debugInfo = new DebugInfo();
      debugInfo.pid = HidlSupport.getPidIfSharable();
      debugInfo.ptr = 0L;
      debugInfo.arch = 0;
      return debugInfo;
    }
    
    public final void notifySyspropsChanged() {
      HwBinder.enableInstrumentation();
    }
    
    public final boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) {
      return true;
    }
    
    public IHwInterface queryLocalInterface(String param1String) {
      if ("android.hardware.gnss@1.0::IGnssMeasurementCallback".equals(param1String))
        return (IHwInterface)this; 
      return null;
    }
    
    public void registerAsService(String param1String) throws RemoteException {
      registerService(param1String);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(interfaceDescriptor());
      stringBuilder.append("@Stub");
      return stringBuilder.toString();
    }
    
    public void onTransact(int param1Int1, HwParcel param1HwParcel1, HwParcel param1HwParcel2, int param1Int2) throws RemoteException {
      DebugInfo debugInfo;
      ArrayList<byte[]> arrayList1;
      String str;
      ArrayList<String> arrayList;
      HwBlob hwBlob1, hwBlob2;
      NativeHandle nativeHandle;
      switch (param1Int1) {
        default:
          return;
        case 257120595:
          param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
          notifySyspropsChanged();
        case 257049926:
          param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
          debugInfo = getDebugInfo();
          param1HwParcel2.writeStatus(0);
          debugInfo.writeToParcel(param1HwParcel2);
          param1HwParcel2.send();
        case 256921159:
          debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
          ping();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.send();
        case 256462420:
          debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
          setHALInstrumentation();
        case 256398152:
          debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
          arrayList1 = getHashChain();
          param1HwParcel2.writeStatus(0);
          hwBlob1 = new HwBlob(16);
          param1Int2 = arrayList1.size();
          hwBlob1.putInt32(8L, param1Int2);
          hwBlob1.putBool(12L, false);
          hwBlob2 = new HwBlob(param1Int2 * 32);
          for (param1Int1 = 0; param1Int1 < param1Int2; ) {
            long l = (param1Int1 * 32);
            byte[] arrayOfByte = arrayList1.get(param1Int1);
            if (arrayOfByte != null && arrayOfByte.length == 32) {
              hwBlob2.putInt8Array(l, arrayOfByte);
              param1Int1++;
            } 
            throw new IllegalArgumentException("Array element is not of the expected length");
          } 
          hwBlob1.putBlob(0L, hwBlob2);
          param1HwParcel2.writeBuffer(hwBlob1);
          param1HwParcel2.send();
        case 256136003:
          arrayList1.enforceInterface("android.hidl.base@1.0::IBase");
          str = interfaceDescriptor();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeString(str);
          param1HwParcel2.send();
        case 256131655:
          str.enforceInterface("android.hidl.base@1.0::IBase");
          nativeHandle = str.readNativeHandle();
          arrayList = str.readStringVector();
          debug(nativeHandle, arrayList);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.send();
        case 256067662:
          arrayList.enforceInterface("android.hidl.base@1.0::IBase");
          arrayList = interfaceChain();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeStringVector(arrayList);
          param1HwParcel2.send();
        case 1:
          break;
      } 
      arrayList.enforceInterface("android.hardware.gnss@1.0::IGnssMeasurementCallback");
      IGnssMeasurementCallback.GnssData gnssData = new IGnssMeasurementCallback.GnssData();
      gnssData.readFromParcel((HwParcel)arrayList);
      GnssMeasurementCb(gnssData);
      param1HwParcel2.writeStatus(0);
      param1HwParcel2.send();
    }
  }
}
