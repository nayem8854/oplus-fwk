package android.hardware.gnss.measurement_corrections.V1_1;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class MeasurementCorrections {
  public android.hardware.gnss.measurement_corrections.V1_0.MeasurementCorrections v1_0 = new android.hardware.gnss.measurement_corrections.V1_0.MeasurementCorrections();
  
  public boolean hasEnvironmentBearing = false;
  
  public float environmentBearingDegrees = 0.0F;
  
  public float environmentBearingUncertaintyDegrees = 0.0F;
  
  public ArrayList<SingleSatCorrection> satCorrections = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != MeasurementCorrections.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.v1_0, ((MeasurementCorrections)paramObject).v1_0))
      return false; 
    if (this.hasEnvironmentBearing != ((MeasurementCorrections)paramObject).hasEnvironmentBearing)
      return false; 
    if (this.environmentBearingDegrees != ((MeasurementCorrections)paramObject).environmentBearingDegrees)
      return false; 
    if (this.environmentBearingUncertaintyDegrees != ((MeasurementCorrections)paramObject).environmentBearingUncertaintyDegrees)
      return false; 
    if (!HidlSupport.deepEquals(this.satCorrections, ((MeasurementCorrections)paramObject).satCorrections))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    android.hardware.gnss.measurement_corrections.V1_0.MeasurementCorrections measurementCorrections = this.v1_0;
    int i = HidlSupport.deepHashCode(measurementCorrections);
    boolean bool = this.hasEnvironmentBearing;
    int j = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    float f = this.environmentBearingDegrees;
    int k = HidlSupport.deepHashCode(Float.valueOf(f));
    f = this.environmentBearingUncertaintyDegrees;
    int m = HidlSupport.deepHashCode(Float.valueOf(f));
    ArrayList<SingleSatCorrection> arrayList = this.satCorrections;
    int n = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".v1_0 = ");
    stringBuilder.append(this.v1_0);
    stringBuilder.append(", .hasEnvironmentBearing = ");
    stringBuilder.append(this.hasEnvironmentBearing);
    stringBuilder.append(", .environmentBearingDegrees = ");
    stringBuilder.append(this.environmentBearingDegrees);
    stringBuilder.append(", .environmentBearingUncertaintyDegrees = ");
    stringBuilder.append(this.environmentBearingUncertaintyDegrees);
    stringBuilder.append(", .satCorrections = ");
    stringBuilder.append(this.satCorrections);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(96L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<MeasurementCorrections> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<MeasurementCorrections> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 96);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      MeasurementCorrections measurementCorrections = new MeasurementCorrections();
      measurementCorrections.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 96));
      arrayList.add(measurementCorrections);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.v1_0.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 0L);
    this.hasEnvironmentBearing = paramHwBlob.getBool(paramLong + 64L);
    this.environmentBearingDegrees = paramHwBlob.getFloat(paramLong + 68L);
    this.environmentBearingUncertaintyDegrees = paramHwBlob.getFloat(paramLong + 72L);
    int i = paramHwBlob.getInt32(paramLong + 80L + 8L);
    long l1 = (i * 64);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 80L + 0L, true);
    this.satCorrections.clear();
    for (byte b = 0; b < i; b++) {
      SingleSatCorrection singleSatCorrection = new SingleSatCorrection();
      singleSatCorrection.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 64));
      this.satCorrections.add(singleSatCorrection);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(96);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<MeasurementCorrections> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 96);
    for (byte b = 0; b < i; b++)
      ((MeasurementCorrections)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 96)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.v1_0.writeEmbeddedToBlob(paramHwBlob, paramLong + 0L);
    paramHwBlob.putBool(64L + paramLong, this.hasEnvironmentBearing);
    paramHwBlob.putFloat(68L + paramLong, this.environmentBearingDegrees);
    paramHwBlob.putFloat(72L + paramLong, this.environmentBearingUncertaintyDegrees);
    int i = this.satCorrections.size();
    paramHwBlob.putInt32(paramLong + 80L + 8L, i);
    paramHwBlob.putBool(paramLong + 80L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 64);
    for (byte b = 0; b < i; b++)
      ((SingleSatCorrection)this.satCorrections.get(b)).writeEmbeddedToBlob(hwBlob, (b * 64)); 
    paramHwBlob.putBlob(80L + paramLong + 0L, hwBlob);
  }
}
