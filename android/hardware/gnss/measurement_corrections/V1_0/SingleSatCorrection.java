package android.hardware.gnss.measurement_corrections.V1_0;

import android.hardware.gnss.V1_0.GnssConstellationType;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class SingleSatCorrection {
  public byte constellation = 0;
  
  public short svid = 0;
  
  public float carrierFrequencyHz = 0.0F;
  
  public float probSatIsLos = 0.0F;
  
  public float excessPathLengthMeters = 0.0F;
  
  public float excessPathLengthUncertaintyMeters = 0.0F;
  
  public ReflectingPlane reflectingPlane = new ReflectingPlane();
  
  public short singleSatCorrectionFlags;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != SingleSatCorrection.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(Short.valueOf(this.singleSatCorrectionFlags), Short.valueOf(((SingleSatCorrection)paramObject).singleSatCorrectionFlags)))
      return false; 
    if (this.constellation != ((SingleSatCorrection)paramObject).constellation)
      return false; 
    if (this.svid != ((SingleSatCorrection)paramObject).svid)
      return false; 
    if (this.carrierFrequencyHz != ((SingleSatCorrection)paramObject).carrierFrequencyHz)
      return false; 
    if (this.probSatIsLos != ((SingleSatCorrection)paramObject).probSatIsLos)
      return false; 
    if (this.excessPathLengthMeters != ((SingleSatCorrection)paramObject).excessPathLengthMeters)
      return false; 
    if (this.excessPathLengthUncertaintyMeters != ((SingleSatCorrection)paramObject).excessPathLengthUncertaintyMeters)
      return false; 
    if (!HidlSupport.deepEquals(this.reflectingPlane, ((SingleSatCorrection)paramObject).reflectingPlane))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    short s = this.singleSatCorrectionFlags;
    int i = HidlSupport.deepHashCode(Short.valueOf(s));
    byte b = this.constellation;
    int j = HidlSupport.deepHashCode(Byte.valueOf(b));
    s = this.svid;
    int k = HidlSupport.deepHashCode(Short.valueOf(s));
    float f = this.carrierFrequencyHz;
    int m = HidlSupport.deepHashCode(Float.valueOf(f));
    f = this.probSatIsLos;
    int n = HidlSupport.deepHashCode(Float.valueOf(f));
    f = this.excessPathLengthMeters;
    int i1 = HidlSupport.deepHashCode(Float.valueOf(f));
    f = this.excessPathLengthUncertaintyMeters;
    int i2 = HidlSupport.deepHashCode(Float.valueOf(f));
    ReflectingPlane reflectingPlane = this.reflectingPlane;
    int i3 = HidlSupport.deepHashCode(reflectingPlane);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".singleSatCorrectionFlags = ");
    stringBuilder.append(GnssSingleSatCorrectionFlags.dumpBitfield(this.singleSatCorrectionFlags));
    stringBuilder.append(", .constellation = ");
    stringBuilder.append(GnssConstellationType.toString(this.constellation));
    stringBuilder.append(", .svid = ");
    stringBuilder.append(this.svid);
    stringBuilder.append(", .carrierFrequencyHz = ");
    stringBuilder.append(this.carrierFrequencyHz);
    stringBuilder.append(", .probSatIsLos = ");
    stringBuilder.append(this.probSatIsLos);
    stringBuilder.append(", .excessPathLengthMeters = ");
    stringBuilder.append(this.excessPathLengthMeters);
    stringBuilder.append(", .excessPathLengthUncertaintyMeters = ");
    stringBuilder.append(this.excessPathLengthUncertaintyMeters);
    stringBuilder.append(", .reflectingPlane = ");
    stringBuilder.append(this.reflectingPlane);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(56L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<SingleSatCorrection> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<SingleSatCorrection> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 56);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      SingleSatCorrection singleSatCorrection = new SingleSatCorrection();
      singleSatCorrection.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 56));
      arrayList.add(singleSatCorrection);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.singleSatCorrectionFlags = paramHwBlob.getInt16(0L + paramLong);
    this.constellation = paramHwBlob.getInt8(2L + paramLong);
    this.svid = paramHwBlob.getInt16(4L + paramLong);
    this.carrierFrequencyHz = paramHwBlob.getFloat(8L + paramLong);
    this.probSatIsLos = paramHwBlob.getFloat(12L + paramLong);
    this.excessPathLengthMeters = paramHwBlob.getFloat(16L + paramLong);
    this.excessPathLengthUncertaintyMeters = paramHwBlob.getFloat(20L + paramLong);
    this.reflectingPlane.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 24L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(56);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<SingleSatCorrection> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 56);
    for (byte b = 0; b < i; b++)
      ((SingleSatCorrection)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 56)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt16(0L + paramLong, this.singleSatCorrectionFlags);
    paramHwBlob.putInt8(2L + paramLong, this.constellation);
    paramHwBlob.putInt16(4L + paramLong, this.svid);
    paramHwBlob.putFloat(8L + paramLong, this.carrierFrequencyHz);
    paramHwBlob.putFloat(12L + paramLong, this.probSatIsLos);
    paramHwBlob.putFloat(16L + paramLong, this.excessPathLengthMeters);
    paramHwBlob.putFloat(20L + paramLong, this.excessPathLengthUncertaintyMeters);
    this.reflectingPlane.writeEmbeddedToBlob(paramHwBlob, 24L + paramLong);
  }
}
