package android.hardware.gnss.V2_1;

import android.hardware.gnss.V2_0.GnssConstellationType;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class GnssSignalType {
  public byte constellation = 0;
  
  public double carrierFrequencyHz = 0.0D;
  
  public String codeType = new String();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != GnssSignalType.class)
      return false; 
    paramObject = paramObject;
    if (this.constellation != ((GnssSignalType)paramObject).constellation)
      return false; 
    if (this.carrierFrequencyHz != ((GnssSignalType)paramObject).carrierFrequencyHz)
      return false; 
    if (!HidlSupport.deepEquals(this.codeType, ((GnssSignalType)paramObject).codeType))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    byte b = this.constellation;
    int i = HidlSupport.deepHashCode(Byte.valueOf(b));
    double d = this.carrierFrequencyHz;
    int j = HidlSupport.deepHashCode(Double.valueOf(d));
    String str = this.codeType;
    int k = HidlSupport.deepHashCode(str);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".constellation = ");
    stringBuilder.append(GnssConstellationType.toString(this.constellation));
    stringBuilder.append(", .carrierFrequencyHz = ");
    stringBuilder.append(this.carrierFrequencyHz);
    stringBuilder.append(", .codeType = ");
    stringBuilder.append(this.codeType);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(32L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<GnssSignalType> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<GnssSignalType> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 32);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      GnssSignalType gnssSignalType = new GnssSignalType();
      gnssSignalType.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 32));
      arrayList.add(gnssSignalType);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.constellation = paramHwBlob.getInt8(paramLong + 0L);
    this.carrierFrequencyHz = paramHwBlob.getDouble(paramLong + 8L);
    String str = paramHwBlob.getString(paramLong + 16L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 16L + 0L, false);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(32);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<GnssSignalType> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 32);
    for (byte b = 0; b < i; b++)
      ((GnssSignalType)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 32)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt8(0L + paramLong, this.constellation);
    paramHwBlob.putDouble(8L + paramLong, this.carrierFrequencyHz);
    paramHwBlob.putString(16L + paramLong, this.codeType);
  }
}
