package android.hardware.gnss.V2_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class ElapsedRealtime {
  public long timestampNs = 0L;
  
  public long timeUncertaintyNs = 0L;
  
  public short flags;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != ElapsedRealtime.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(Short.valueOf(this.flags), Short.valueOf(((ElapsedRealtime)paramObject).flags)))
      return false; 
    if (this.timestampNs != ((ElapsedRealtime)paramObject).timestampNs)
      return false; 
    if (this.timeUncertaintyNs != ((ElapsedRealtime)paramObject).timeUncertaintyNs)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    short s = this.flags;
    int i = HidlSupport.deepHashCode(Short.valueOf(s));
    long l = this.timestampNs;
    int j = HidlSupport.deepHashCode(Long.valueOf(l));
    l = this.timeUncertaintyNs;
    int k = HidlSupport.deepHashCode(Long.valueOf(l));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".flags = ");
    stringBuilder.append(ElapsedRealtimeFlags.dumpBitfield(this.flags));
    stringBuilder.append(", .timestampNs = ");
    stringBuilder.append(this.timestampNs);
    stringBuilder.append(", .timeUncertaintyNs = ");
    stringBuilder.append(this.timeUncertaintyNs);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(24L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<ElapsedRealtime> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<ElapsedRealtime> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 24);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      ElapsedRealtime elapsedRealtime = new ElapsedRealtime();
      elapsedRealtime.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 24));
      arrayList.add(elapsedRealtime);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.flags = paramHwBlob.getInt16(0L + paramLong);
    this.timestampNs = paramHwBlob.getInt64(8L + paramLong);
    this.timeUncertaintyNs = paramHwBlob.getInt64(16L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(24);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<ElapsedRealtime> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 24);
    for (byte b = 0; b < i; b++)
      ((ElapsedRealtime)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 24)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt16(0L + paramLong, this.flags);
    paramHwBlob.putInt64(8L + paramLong, this.timestampNs);
    paramHwBlob.putInt64(16L + paramLong, this.timeUncertaintyNs);
  }
}
