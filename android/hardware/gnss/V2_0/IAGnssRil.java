package android.hardware.gnss.V2_0;

import android.hardware.gnss.V1_0.IAGnssRil;
import android.hardware.gnss.V1_0.IAGnssRilCallback;
import android.internal.hidl.base.V1_0.DebugInfo;
import android.os.HidlSupport;
import android.os.HwBinder;
import android.os.HwBlob;
import android.os.HwParcel;
import android.os.IHwBinder;
import android.os.IHwInterface;
import android.os.NativeHandle;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public interface IAGnssRil extends IAGnssRil {
  public static final String kInterfaceName = "android.hardware.gnss@2.0::IAGnssRil";
  
  static IAGnssRil asInterface(IHwBinder paramIHwBinder) {
    if (paramIHwBinder == null)
      return null; 
    IHwInterface iHwInterface = paramIHwBinder.queryLocalInterface("android.hardware.gnss@2.0::IAGnssRil");
    if (iHwInterface != null && iHwInterface instanceof IAGnssRil)
      return (IAGnssRil)iHwInterface; 
    Proxy proxy = new Proxy(paramIHwBinder);
    try {
      for (String str : proxy.interfaceChain()) {
        boolean bool = str.equals("android.hardware.gnss@2.0::IAGnssRil");
        if (bool)
          return proxy; 
      } 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  static IAGnssRil castFrom(IHwInterface paramIHwInterface) {
    IAGnssRil iAGnssRil;
    if (paramIHwInterface == null) {
      paramIHwInterface = null;
    } else {
      iAGnssRil = asInterface(paramIHwInterface.asBinder());
    } 
    return iAGnssRil;
  }
  
  static IAGnssRil getService(String paramString, boolean paramBoolean) throws RemoteException {
    return asInterface(HwBinder.getService("android.hardware.gnss@2.0::IAGnssRil", paramString, paramBoolean));
  }
  
  static IAGnssRil getService(boolean paramBoolean) throws RemoteException {
    return getService("default", paramBoolean);
  }
  
  static IAGnssRil getService(String paramString) throws RemoteException {
    return asInterface(HwBinder.getService("android.hardware.gnss@2.0::IAGnssRil", paramString));
  }
  
  static IAGnssRil getService() throws RemoteException {
    return getService("default");
  }
  
  IHwBinder asBinder();
  
  void debug(NativeHandle paramNativeHandle, ArrayList<String> paramArrayList) throws RemoteException;
  
  DebugInfo getDebugInfo() throws RemoteException;
  
  ArrayList<byte[]> getHashChain() throws RemoteException;
  
  ArrayList<String> interfaceChain() throws RemoteException;
  
  String interfaceDescriptor() throws RemoteException;
  
  boolean linkToDeath(IHwBinder.DeathRecipient paramDeathRecipient, long paramLong) throws RemoteException;
  
  void notifySyspropsChanged() throws RemoteException;
  
  void ping() throws RemoteException;
  
  void setHALInstrumentation() throws RemoteException;
  
  boolean unlinkToDeath(IHwBinder.DeathRecipient paramDeathRecipient) throws RemoteException;
  
  boolean updateNetworkState_2_0(NetworkAttributes paramNetworkAttributes) throws RemoteException;
  
  class NetworkCapability {
    public static final short NOT_METERED = 1;
    
    public static final short NOT_ROAMING = 2;
    
    public static final String toString(short param1Short) {
      if (param1Short == 1)
        return "NOT_METERED"; 
      if (param1Short == 2)
        return "NOT_ROAMING"; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString(Short.toUnsignedInt(param1Short)));
      return stringBuilder.toString();
    }
    
    public static final String dumpBitfield(short param1Short) {
      ArrayList<String> arrayList = new ArrayList();
      short s1 = 0;
      if ((param1Short & 0x1) == 1) {
        arrayList.add("NOT_METERED");
        s1 = (short)(false | true);
      } 
      short s2 = s1;
      if ((param1Short & 0x2) == 2) {
        arrayList.add("NOT_ROAMING");
        s2 = (short)(s1 | 0x2);
      } 
      if (param1Short != s2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("0x");
        stringBuilder.append(Integer.toHexString(Short.toUnsignedInt((short)((s2 ^ 0xFFFFFFFF) & param1Short))));
        arrayList.add(stringBuilder.toString());
      } 
      return String.join(" | ", (Iterable)arrayList);
    }
  }
  
  class NetworkAttributes {
    public long networkHandle = 0L;
    
    public boolean isConnected = false;
    
    public short capabilities;
    
    public String apn = new String();
    
    public final boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (param1Object.getClass() != NetworkAttributes.class)
        return false; 
      param1Object = param1Object;
      if (this.networkHandle != ((NetworkAttributes)param1Object).networkHandle)
        return false; 
      if (this.isConnected != ((NetworkAttributes)param1Object).isConnected)
        return false; 
      if (!HidlSupport.deepEquals(Short.valueOf(this.capabilities), Short.valueOf(((NetworkAttributes)param1Object).capabilities)))
        return false; 
      if (!HidlSupport.deepEquals(this.apn, ((NetworkAttributes)param1Object).apn))
        return false; 
      return true;
    }
    
    public final int hashCode() {
      long l = this.networkHandle;
      int i = HidlSupport.deepHashCode(Long.valueOf(l));
      boolean bool = this.isConnected;
      int j = HidlSupport.deepHashCode(Boolean.valueOf(bool));
      short s = this.capabilities;
      int k = HidlSupport.deepHashCode(Short.valueOf(s));
      String str = this.apn;
      int m = HidlSupport.deepHashCode(str);
      return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
    }
    
    public final String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      stringBuilder.append(".networkHandle = ");
      stringBuilder.append(this.networkHandle);
      stringBuilder.append(", .isConnected = ");
      stringBuilder.append(this.isConnected);
      stringBuilder.append(", .capabilities = ");
      stringBuilder.append(IAGnssRil.NetworkCapability.dumpBitfield(this.capabilities));
      stringBuilder.append(", .apn = ");
      stringBuilder.append(this.apn);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public final void readFromParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = param1HwParcel.readBuffer(32L);
      readEmbeddedFromParcel(param1HwParcel, hwBlob, 0L);
    }
    
    public static final ArrayList<NetworkAttributes> readVectorFromParcel(HwParcel param1HwParcel) {
      ArrayList<NetworkAttributes> arrayList = new ArrayList();
      HwBlob hwBlob = param1HwParcel.readBuffer(16L);
      int i = hwBlob.getInt32(8L);
      long l1 = (i * 32);
      long l2 = hwBlob.handle();
      hwBlob = param1HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
      arrayList.clear();
      for (byte b = 0; b < i; b++) {
        NetworkAttributes networkAttributes = new NetworkAttributes();
        networkAttributes.readEmbeddedFromParcel(param1HwParcel, hwBlob, (b * 32));
        arrayList.add(networkAttributes);
      } 
      return arrayList;
    }
    
    public final void readEmbeddedFromParcel(HwParcel param1HwParcel, HwBlob param1HwBlob, long param1Long) {
      this.networkHandle = param1HwBlob.getInt64(param1Long + 0L);
      this.isConnected = param1HwBlob.getBool(param1Long + 8L);
      this.capabilities = param1HwBlob.getInt16(param1Long + 10L);
      String str = param1HwBlob.getString(param1Long + 16L);
      long l1 = ((str.getBytes()).length + 1);
      long l2 = param1HwBlob.handle();
      param1HwParcel.readEmbeddedBuffer(l1, l2, param1Long + 16L + 0L, false);
    }
    
    public final void writeToParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = new HwBlob(32);
      writeEmbeddedToBlob(hwBlob, 0L);
      param1HwParcel.writeBuffer(hwBlob);
    }
    
    public static final void writeVectorToParcel(HwParcel param1HwParcel, ArrayList<NetworkAttributes> param1ArrayList) {
      HwBlob hwBlob1 = new HwBlob(16);
      int i = param1ArrayList.size();
      hwBlob1.putInt32(8L, i);
      hwBlob1.putBool(12L, false);
      HwBlob hwBlob2 = new HwBlob(i * 32);
      for (byte b = 0; b < i; b++)
        ((NetworkAttributes)param1ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 32)); 
      hwBlob1.putBlob(0L, hwBlob2);
      param1HwParcel.writeBuffer(hwBlob1);
    }
    
    public final void writeEmbeddedToBlob(HwBlob param1HwBlob, long param1Long) {
      param1HwBlob.putInt64(0L + param1Long, this.networkHandle);
      param1HwBlob.putBool(8L + param1Long, this.isConnected);
      param1HwBlob.putInt16(10L + param1Long, this.capabilities);
      param1HwBlob.putString(16L + param1Long, this.apn);
    }
  }
  
  class Proxy implements IAGnssRil {
    private IHwBinder mRemote;
    
    public Proxy(IAGnssRil this$0) {
      Objects.requireNonNull(this$0);
      this.mRemote = (IHwBinder)this$0;
    }
    
    public IHwBinder asBinder() {
      return this.mRemote;
    }
    
    public String toString() {
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(interfaceDescriptor());
        stringBuilder.append("@Proxy");
        return stringBuilder.toString();
      } catch (RemoteException remoteException) {
        return "[class or subclass of android.hardware.gnss@2.0::IAGnssRil]@Proxy";
      } 
    }
    
    public final boolean equals(Object param1Object) {
      return HidlSupport.interfacesEqual((IHwInterface)this, param1Object);
    }
    
    public final int hashCode() {
      return asBinder().hashCode();
    }
    
    public void setCallback(IAGnssRilCallback param1IAGnssRilCallback) throws RemoteException {
      IHwBinder iHwBinder;
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.gnss@1.0::IAGnssRil");
      if (param1IAGnssRilCallback == null) {
        param1IAGnssRilCallback = null;
      } else {
        iHwBinder = param1IAGnssRilCallback.asBinder();
      } 
      null.writeStrongBinder(iHwBinder);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(1, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setRefLocation(IAGnssRil.AGnssRefLocation param1AGnssRefLocation) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.gnss@1.0::IAGnssRil");
      param1AGnssRefLocation.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(2, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean setSetId(byte param1Byte, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.gnss@1.0::IAGnssRil");
      null.writeInt8(param1Byte);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(3, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readBool();
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean updateNetworkState(boolean param1Boolean1, byte param1Byte, boolean param1Boolean2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.gnss@1.0::IAGnssRil");
      null.writeBool(param1Boolean1);
      null.writeInt8(param1Byte);
      null.writeBool(param1Boolean2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(4, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        param1Boolean1 = hwParcel.readBool();
        return param1Boolean1;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean updateNetworkAvailability(boolean param1Boolean, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.gnss@1.0::IAGnssRil");
      null.writeBool(param1Boolean);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(5, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        param1Boolean = hwParcel.readBool();
        return param1Boolean;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean updateNetworkState_2_0(IAGnssRil.NetworkAttributes param1NetworkAttributes) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.gnss@2.0::IAGnssRil");
      param1NetworkAttributes.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(6, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readBool();
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<String> interfaceChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256067662, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readStringVector();
      } finally {
        hwParcel.release();
      } 
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hidl.base@1.0::IBase");
      hwParcel2.writeNativeHandle(param1NativeHandle);
      hwParcel2.writeStringVector(param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(256131655, hwParcel2, hwParcel1, 0);
        hwParcel1.verifySuccess();
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public String interfaceDescriptor() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256136003, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<byte[]> getHashChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256398152, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ArrayList<byte[]> arrayList = new ArrayList();
        this();
        HwBlob hwBlob1 = hwParcel.readBuffer(16L);
        int i = hwBlob1.getInt32(8L);
        long l1 = (i * 32);
        long l2 = hwBlob1.handle();
        HwBlob hwBlob2 = hwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
        arrayList.clear();
        for (byte b = 0; b < i; b++) {
          byte[] arrayOfByte = new byte[32];
          l1 = (b * 32);
          hwBlob2.copyToInt8Array(l1, arrayOfByte, 32);
          arrayList.add(arrayOfByte);
        } 
        return arrayList;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setHALInstrumentation() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256462420, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) throws RemoteException {
      return this.mRemote.linkToDeath(param1DeathRecipient, param1Long);
    }
    
    public void ping() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256921159, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public DebugInfo getDebugInfo() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257049926, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        DebugInfo debugInfo = new DebugInfo();
        this();
        debugInfo.readFromParcel(hwParcel);
        return debugInfo;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void notifySyspropsChanged() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257120595, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) throws RemoteException {
      return this.mRemote.unlinkToDeath(param1DeathRecipient);
    }
  }
  
  class Stub extends HwBinder implements IAGnssRil {
    public IHwBinder asBinder() {
      return (IHwBinder)this;
    }
    
    public final ArrayList<String> interfaceChain() {
      return new ArrayList<>(Arrays.asList(new String[] { "android.hardware.gnss@2.0::IAGnssRil", "android.hardware.gnss@1.0::IAGnssRil", "android.hidl.base@1.0::IBase" }));
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) {}
    
    public final String interfaceDescriptor() {
      return "android.hardware.gnss@2.0::IAGnssRil";
    }
    
    public final ArrayList<byte[]> getHashChain() {
      return (ArrayList)new ArrayList<>(Arrays.asList((byte[])new byte[][] { { 
                31, 74, -64, 104, -88, -118, 114, 54, 2, Byte.MIN_VALUE, 
                -39, 74, Byte.MAX_VALUE, 111, -41, -58, 56, 19, -63, -18, 
                -92, -119, 26, 14, -80, 19, -108, -45, -25, -25, 
                117, -14 }, { 
                -47, 110, 106, 53, -101, -26, -106, 62, -89, 83, 
                -41, 19, -114, -124, -20, -14, -71, 48, 82, 9, 
                121, 56, -109, -116, 77, 54, -41, -92, 126, -94, 
                -30, -82 }, { 
                -20, Byte.MAX_VALUE, -41, -98, -48, 45, -6, -123, -68, 73, 
                -108, 38, -83, -82, 62, -66, 35, -17, 5, 36, 
                -13, -51, 105, 87, 19, -109, 36, -72, 59, 24, 
                -54, 76 } }));
    }
    
    public final void setHALInstrumentation() {}
    
    public final boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) {
      return true;
    }
    
    public final void ping() {}
    
    public final DebugInfo getDebugInfo() {
      DebugInfo debugInfo = new DebugInfo();
      debugInfo.pid = HidlSupport.getPidIfSharable();
      debugInfo.ptr = 0L;
      debugInfo.arch = 0;
      return debugInfo;
    }
    
    public final void notifySyspropsChanged() {
      HwBinder.enableInstrumentation();
    }
    
    public final boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) {
      return true;
    }
    
    public IHwInterface queryLocalInterface(String param1String) {
      if ("android.hardware.gnss@2.0::IAGnssRil".equals(param1String))
        return (IHwInterface)this; 
      return null;
    }
    
    public void registerAsService(String param1String) throws RemoteException {
      registerService(param1String);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(interfaceDescriptor());
      stringBuilder.append("@Stub");
      return stringBuilder.toString();
    }
    
    public void onTransact(int param1Int1, HwParcel param1HwParcel1, HwParcel param1HwParcel2, int param1Int2) throws RemoteException {
      DebugInfo debugInfo;
      ArrayList<byte[]> arrayList1;
      String str2;
      ArrayList<String> arrayList;
      String str1;
      HwBlob hwBlob1, hwBlob2;
      NativeHandle nativeHandle;
      IAGnssRil.NetworkAttributes networkAttributes;
      IAGnssRil.AGnssRefLocation aGnssRefLocation;
      boolean bool1, bool2;
      byte b;
      switch (param1Int1) {
        default:
          switch (param1Int1) {
            default:
              return;
            case 257120595:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              notifySyspropsChanged();
            case 257049926:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              debugInfo = getDebugInfo();
              param1HwParcel2.writeStatus(0);
              debugInfo.writeToParcel(param1HwParcel2);
              param1HwParcel2.send();
            case 256921159:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              ping();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256462420:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              setHALInstrumentation();
            case 256398152:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              arrayList1 = getHashChain();
              param1HwParcel2.writeStatus(0);
              hwBlob1 = new HwBlob(16);
              param1Int2 = arrayList1.size();
              hwBlob1.putInt32(8L, param1Int2);
              hwBlob1.putBool(12L, false);
              hwBlob2 = new HwBlob(param1Int2 * 32);
              for (param1Int1 = 0; param1Int1 < param1Int2; ) {
                long l = (param1Int1 * 32);
                byte[] arrayOfByte = arrayList1.get(param1Int1);
                if (arrayOfByte != null && arrayOfByte.length == 32) {
                  hwBlob2.putInt8Array(l, arrayOfByte);
                  param1Int1++;
                } 
                throw new IllegalArgumentException("Array element is not of the expected length");
              } 
              hwBlob1.putBlob(0L, hwBlob2);
              param1HwParcel2.writeBuffer(hwBlob1);
              param1HwParcel2.send();
            case 256136003:
              arrayList1.enforceInterface("android.hidl.base@1.0::IBase");
              str2 = interfaceDescriptor();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.writeString(str2);
              param1HwParcel2.send();
            case 256131655:
              str2.enforceInterface("android.hidl.base@1.0::IBase");
              nativeHandle = str2.readNativeHandle();
              arrayList = str2.readStringVector();
              debug(nativeHandle, arrayList);
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256067662:
              break;
          } 
          arrayList.enforceInterface("android.hidl.base@1.0::IBase");
          arrayList = interfaceChain();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeStringVector(arrayList);
          param1HwParcel2.send();
        case 6:
          arrayList.enforceInterface("android.hardware.gnss@2.0::IAGnssRil");
          networkAttributes = new IAGnssRil.NetworkAttributes();
          networkAttributes.readFromParcel((HwParcel)arrayList);
          bool1 = updateNetworkState_2_0(networkAttributes);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeBool(bool1);
          param1HwParcel2.send();
        case 5:
          arrayList.enforceInterface("android.hardware.gnss@1.0::IAGnssRil");
          bool1 = arrayList.readBool();
          str1 = arrayList.readString();
          bool1 = updateNetworkAvailability(bool1, str1);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeBool(bool1);
          param1HwParcel2.send();
        case 4:
          str1.enforceInterface("android.hardware.gnss@1.0::IAGnssRil");
          bool2 = str1.readBool();
          b = str1.readInt8();
          bool1 = str1.readBool();
          bool1 = updateNetworkState(bool2, b, bool1);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeBool(bool1);
          param1HwParcel2.send();
        case 3:
          str1.enforceInterface("android.hardware.gnss@1.0::IAGnssRil");
          b = str1.readInt8();
          str1 = str1.readString();
          bool1 = setSetId(b, str1);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeBool(bool1);
          param1HwParcel2.send();
        case 2:
          str1.enforceInterface("android.hardware.gnss@1.0::IAGnssRil");
          aGnssRefLocation = new IAGnssRil.AGnssRefLocation();
          aGnssRefLocation.readFromParcel((HwParcel)str1);
          setRefLocation(aGnssRefLocation);
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.send();
        case 1:
          break;
      } 
      str1.enforceInterface("android.hardware.gnss@1.0::IAGnssRil");
      IAGnssRilCallback iAGnssRilCallback = IAGnssRilCallback.asInterface(str1.readStrongBinder());
      setCallback(iAGnssRilCallback);
      param1HwParcel2.writeStatus(0);
      param1HwParcel2.send();
    }
  }
}
