package android.hardware;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICamera extends IInterface {
  void disconnect() throws RemoteException;
  
  class Default implements ICamera {
    public void disconnect() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICamera {
    private static final String DESCRIPTOR = "android.hardware.ICamera";
    
    static final int TRANSACTION_disconnect = 1;
    
    public Stub() {
      attachInterface(this, "android.hardware.ICamera");
    }
    
    public static ICamera asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.ICamera");
      if (iInterface != null && iInterface instanceof ICamera)
        return (ICamera)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "disconnect";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.hardware.ICamera");
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.ICamera");
      disconnect();
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements ICamera {
      public static ICamera sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.ICamera";
      }
      
      public void disconnect() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.ICamera");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ICamera.Stub.getDefaultImpl() != null) {
            ICamera.Stub.getDefaultImpl().disconnect();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICamera param1ICamera) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICamera != null) {
          Proxy.sDefaultImpl = param1ICamera;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICamera getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
