package android.hardware.cas.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class HidlCasPluginDescriptor {
  public int caSystemId = 0;
  
  public String name = new String();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != HidlCasPluginDescriptor.class)
      return false; 
    paramObject = paramObject;
    if (this.caSystemId != ((HidlCasPluginDescriptor)paramObject).caSystemId)
      return false; 
    if (!HidlSupport.deepEquals(this.name, ((HidlCasPluginDescriptor)paramObject).name))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.caSystemId;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    String str = this.name;
    i = HidlSupport.deepHashCode(str);
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(i) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".caSystemId = ");
    stringBuilder.append(this.caSystemId);
    stringBuilder.append(", .name = ");
    stringBuilder.append(this.name);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(24L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<HidlCasPluginDescriptor> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<HidlCasPluginDescriptor> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 24);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      HidlCasPluginDescriptor hidlCasPluginDescriptor = new HidlCasPluginDescriptor();
      hidlCasPluginDescriptor.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 24));
      arrayList.add(hidlCasPluginDescriptor);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.caSystemId = paramHwBlob.getInt32(paramLong + 0L);
    String str = paramHwBlob.getString(paramLong + 8L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, false);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(24);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<HidlCasPluginDescriptor> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 24);
    for (byte b = 0; b < i; b++)
      ((HidlCasPluginDescriptor)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 24)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.caSystemId);
    paramHwBlob.putString(8L + paramLong, this.name);
  }
}
