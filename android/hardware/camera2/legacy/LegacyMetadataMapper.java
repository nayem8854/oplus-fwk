package android.hardware.camera2.legacy;

import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.CameraInfo;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.impl.CameraMetadataNative;
import android.hardware.camera2.params.MeteringRectangle;
import android.hardware.camera2.params.StreamConfiguration;
import android.hardware.camera2.params.StreamConfigurationDuration;
import android.hardware.camera2.utils.ArrayUtils;
import android.hardware.camera2.utils.ListUtils;
import android.hardware.camera2.utils.ParamsUtils;
import android.hardware.camera2.utils.SizeAreaComparator;
import android.util.Log;
import android.util.Range;
import android.util.Rational;
import android.util.Size;
import android.util.SizeF;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class LegacyMetadataMapper {
  private static final long APPROXIMATE_CAPTURE_DELAY_MS = 200L;
  
  private static final long APPROXIMATE_JPEG_ENCODE_TIME_MS = 600L;
  
  private static final long APPROXIMATE_SENSOR_AREA_PX = 8388608L;
  
  private static final boolean DEBUG = false;
  
  public static final int HAL_PIXEL_FORMAT_BGRA_8888 = 5;
  
  public static final int HAL_PIXEL_FORMAT_BLOB = 33;
  
  public static final int HAL_PIXEL_FORMAT_IMPLEMENTATION_DEFINED = 34;
  
  public static final int HAL_PIXEL_FORMAT_RGBA_8888 = 1;
  
  private static final float LENS_INFO_MINIMUM_FOCUS_DISTANCE_FIXED_FOCUS = 0.0F;
  
  static final boolean LIE_ABOUT_AE_MAX_REGIONS = false;
  
  static final boolean LIE_ABOUT_AE_STATE = false;
  
  static final boolean LIE_ABOUT_AF = false;
  
  static final boolean LIE_ABOUT_AF_MAX_REGIONS = false;
  
  static final boolean LIE_ABOUT_AWB = false;
  
  static final boolean LIE_ABOUT_AWB_STATE = false;
  
  private static final long NS_PER_MS = 1000000L;
  
  private static final float PREVIEW_ASPECT_RATIO_TOLERANCE = 0.01F;
  
  private static final int REQUEST_MAX_NUM_INPUT_STREAMS_COUNT = 0;
  
  private static final int REQUEST_MAX_NUM_OUTPUT_STREAMS_COUNT_PROC = 3;
  
  private static final int REQUEST_MAX_NUM_OUTPUT_STREAMS_COUNT_PROC_STALL = 1;
  
  private static final int REQUEST_MAX_NUM_OUTPUT_STREAMS_COUNT_RAW = 0;
  
  private static final int REQUEST_PIPELINE_MAX_DEPTH_HAL1 = 3;
  
  private static final int REQUEST_PIPELINE_MAX_DEPTH_OURS = 3;
  
  private static final String TAG = "LegacyMetadataMapper";
  
  static final int UNKNOWN_MODE = -1;
  
  private static final int[] sAllowedTemplates;
  
  private static final int[] sEffectModes;
  
  private static final String[] sLegacyEffectMode;
  
  public static CameraCharacteristics createCharacteristics(Camera.Parameters paramParameters, Camera.CameraInfo paramCameraInfo, int paramInt, Size paramSize) {
    Preconditions.checkNotNull(paramParameters, "parameters must not be null");
    Preconditions.checkNotNull(paramCameraInfo, "info must not be null");
    String str = paramParameters.flatten();
    CameraInfo cameraInfo = new CameraInfo();
    cameraInfo.info = paramCameraInfo;
    return createCharacteristics(str, cameraInfo, paramInt, paramSize);
  }
  
  public static CameraCharacteristics createCharacteristics(String paramString, CameraInfo paramCameraInfo, int paramInt, Size paramSize) {
    Preconditions.checkNotNull(paramString, "parameters must not be null");
    Preconditions.checkNotNull(paramCameraInfo, "info must not be null");
    Preconditions.checkNotNull(paramCameraInfo.info, "info.info must not be null");
    CameraMetadataNative cameraMetadataNative = new CameraMetadataNative();
    mapCharacteristicsFromInfo(cameraMetadataNative, paramCameraInfo.info);
    Camera.Parameters parameters = Camera.getEmptyParameters();
    parameters.unflatten(paramString);
    mapCharacteristicsFromParameters(cameraMetadataNative, parameters);
    cameraMetadataNative.setCameraId(paramInt);
    cameraMetadataNative.setDisplaySize(paramSize);
    return new CameraCharacteristics(cameraMetadataNative);
  }
  
  private static void mapCharacteristicsFromInfo(CameraMetadataNative paramCameraMetadataNative, Camera.CameraInfo paramCameraInfo) {
    boolean bool;
    CameraCharacteristics.Key<Integer> key = CameraCharacteristics.LENS_FACING;
    if (paramCameraInfo.facing == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    paramCameraMetadataNative.set(key, Integer.valueOf(bool));
    paramCameraMetadataNative.set(CameraCharacteristics.SENSOR_ORIENTATION, Integer.valueOf(paramCameraInfo.orientation));
  }
  
  private static void mapCharacteristicsFromParameters(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.COLOR_CORRECTION_AVAILABLE_ABERRATION_MODES, new int[] { 1, 2 });
    mapControlAe(paramCameraMetadataNative, paramParameters);
    mapControlAf(paramCameraMetadataNative, paramParameters);
    mapControlAwb(paramCameraMetadataNative, paramParameters);
    mapControlOther(paramCameraMetadataNative, paramParameters);
    mapLens(paramCameraMetadataNative, paramParameters);
    mapFlash(paramCameraMetadataNative, paramParameters);
    mapJpeg(paramCameraMetadataNative, paramParameters);
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.NOISE_REDUCTION_AVAILABLE_NOISE_REDUCTION_MODES, new int[] { 1, 2 });
    mapScaler(paramCameraMetadataNative, paramParameters);
    mapSensor(paramCameraMetadataNative, paramParameters);
    mapStatistics(paramCameraMetadataNative, paramParameters);
    mapSync(paramCameraMetadataNative, paramParameters);
    paramCameraMetadataNative.set(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL, Integer.valueOf(2));
    mapScalerStreamConfigs(paramCameraMetadataNative, paramParameters);
    mapRequest(paramCameraMetadataNative, paramParameters);
  }
  
  private static void mapScalerStreamConfigs(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    ArrayList<StreamConfiguration> arrayList = new ArrayList();
    List<Camera.Size> list2 = paramParameters.getSupportedPreviewSizes();
    List<Camera.Size> list3 = paramParameters.getSupportedPictureSizes();
    SizeAreaComparator sizeAreaComparator = new SizeAreaComparator();
    Collections.sort(list2, sizeAreaComparator);
    Camera.Size size = SizeAreaComparator.findLargestByArea(list3);
    float f = size.width * 1.0F / size.height;
    while (!list2.isEmpty()) {
      int i = list2.size() - 1;
      size = list2.get(i);
      float f1 = size.width * 1.0F / size.height;
      if (Math.abs(f - f1) >= 0.01F)
        list2.remove(i); 
    } 
    List<Camera.Size> list4 = list2;
    if (list2.isEmpty()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("mapScalerStreamConfigs - failed to find any preview size matching JPEG aspect ratio ");
      stringBuilder.append(f);
      Log.w("LegacyMetadataMapper", stringBuilder.toString());
      list4 = paramParameters.getSupportedPreviewSizes();
    } 
    Collections.sort(list4, Collections.reverseOrder(sizeAreaComparator));
    appendStreamConfig(arrayList, 34, list4);
    appendStreamConfig(arrayList, 35, list4);
    for (Iterator<Integer> iterator = paramParameters.getSupportedPreviewFormats().iterator(); iterator.hasNext(); ) {
      int i = ((Integer)iterator.next()).intValue();
      if (ImageFormat.isPublicFormat(i) && i != 17)
        appendStreamConfig(arrayList, i, list4); 
    } 
    List<Camera.Size> list1 = paramParameters.getSupportedPictureSizes();
    appendStreamConfig(arrayList, 33, list1);
    CameraCharacteristics.Key<StreamConfiguration[]> key = CameraCharacteristics.SCALER_AVAILABLE_STREAM_CONFIGURATIONS;
    StreamConfiguration[] arrayOfStreamConfiguration = arrayList.<StreamConfiguration>toArray(new StreamConfiguration[0]);
    paramCameraMetadataNative.set((CameraCharacteristics.Key)key, arrayOfStreamConfiguration);
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.SCALER_AVAILABLE_MIN_FRAME_DURATIONS, new StreamConfigurationDuration[0]);
    StreamConfigurationDuration[] arrayOfStreamConfigurationDuration = new StreamConfigurationDuration[list3.size()];
    byte b = 0;
    long l = -1L;
    for (Camera.Size size1 : list3) {
      long l1 = calculateJpegStallDuration(size1);
      arrayOfStreamConfigurationDuration[b] = new StreamConfigurationDuration(33, size1.width, size1.height, l1);
      long l2 = l;
      if (l < l1)
        l2 = l1; 
      b++;
      l = l2;
    } 
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.SCALER_AVAILABLE_STALL_DURATIONS, arrayOfStreamConfigurationDuration);
    paramCameraMetadataNative.set(CameraCharacteristics.SENSOR_INFO_MAX_FRAME_DURATION, Long.valueOf(l));
  }
  
  private static void mapControlAe(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getSupportedAntibanding : ()Ljava/util/List;
    //   4: astore_2
    //   5: aload_2
    //   6: ifnull -> 92
    //   9: aload_2
    //   10: invokeinterface size : ()I
    //   15: ifle -> 92
    //   18: aload_2
    //   19: invokeinterface size : ()I
    //   24: newarray int
    //   26: astore_3
    //   27: iconst_0
    //   28: istore #4
    //   30: aload_2
    //   31: invokeinterface iterator : ()Ljava/util/Iterator;
    //   36: astore_2
    //   37: aload_2
    //   38: invokeinterface hasNext : ()Z
    //   43: ifeq -> 76
    //   46: aload_2
    //   47: invokeinterface next : ()Ljava/lang/Object;
    //   52: checkcast java/lang/String
    //   55: astore #5
    //   57: aload #5
    //   59: invokestatic convertAntiBandingMode : (Ljava/lang/String;)I
    //   62: istore #6
    //   64: aload_3
    //   65: iload #4
    //   67: iload #6
    //   69: iastore
    //   70: iinc #4, 1
    //   73: goto -> 37
    //   76: aload_0
    //   77: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AE_AVAILABLE_ANTIBANDING_MODES : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   80: aload_3
    //   81: iload #4
    //   83: invokestatic copyOf : ([II)[I
    //   86: invokevirtual set : (Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)V
    //   89: goto -> 102
    //   92: aload_0
    //   93: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AE_AVAILABLE_ANTIBANDING_MODES : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   96: iconst_0
    //   97: newarray int
    //   99: invokevirtual set : (Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)V
    //   102: aload_1
    //   103: invokevirtual getSupportedPreviewFpsRange : ()Ljava/util/List;
    //   106: astore_3
    //   107: aload_3
    //   108: ifnull -> 400
    //   111: aload_3
    //   112: invokeinterface size : ()I
    //   117: istore #4
    //   119: iload #4
    //   121: ifle -> 389
    //   124: iload #4
    //   126: anewarray android/util/Range
    //   129: astore #5
    //   131: iconst_0
    //   132: istore #4
    //   134: aload_3
    //   135: invokeinterface iterator : ()Ljava/util/Iterator;
    //   140: astore_2
    //   141: aload_2
    //   142: invokeinterface hasNext : ()Z
    //   147: ifeq -> 223
    //   150: aload_2
    //   151: invokeinterface next : ()Ljava/lang/Object;
    //   156: checkcast [I
    //   159: astore #7
    //   161: aload #7
    //   163: iconst_0
    //   164: iaload
    //   165: i2d
    //   166: ldc2_w 1000.0
    //   169: ddiv
    //   170: dstore #8
    //   172: dload #8
    //   174: invokestatic floor : (D)D
    //   177: d2i
    //   178: istore #10
    //   180: aload #7
    //   182: iconst_1
    //   183: iaload
    //   184: i2d
    //   185: ldc2_w 1000.0
    //   188: ddiv
    //   189: dstore #8
    //   191: dload #8
    //   193: invokestatic ceil : (D)D
    //   196: d2i
    //   197: istore #6
    //   199: aload #5
    //   201: iload #4
    //   203: iload #10
    //   205: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   208: iload #6
    //   210: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   213: invokestatic create : (Ljava/lang/Comparable;Ljava/lang/Comparable;)Landroid/util/Range;
    //   216: aastore
    //   217: iinc #4, 1
    //   220: goto -> 141
    //   223: aload_0
    //   224: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   227: aload #5
    //   229: invokevirtual set : (Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)V
    //   232: aload_1
    //   233: invokevirtual getSupportedFlashModes : ()Ljava/util/List;
    //   236: astore_3
    //   237: aload_3
    //   238: iconst_5
    //   239: anewarray java/lang/String
    //   242: dup
    //   243: iconst_0
    //   244: ldc 'off'
    //   246: aastore
    //   247: dup
    //   248: iconst_1
    //   249: ldc 'auto'
    //   251: aastore
    //   252: dup
    //   253: iconst_2
    //   254: ldc_w 'on'
    //   257: aastore
    //   258: dup
    //   259: iconst_3
    //   260: ldc_w 'red-eye'
    //   263: aastore
    //   264: dup
    //   265: iconst_4
    //   266: ldc_w 'torch'
    //   269: aastore
    //   270: iconst_4
    //   271: newarray int
    //   273: dup
    //   274: iconst_0
    //   275: iconst_1
    //   276: iastore
    //   277: dup
    //   278: iconst_1
    //   279: iconst_2
    //   280: iastore
    //   281: dup
    //   282: iconst_2
    //   283: iconst_3
    //   284: iastore
    //   285: dup
    //   286: iconst_3
    //   287: iconst_4
    //   288: iastore
    //   289: invokestatic convertStringListToIntArray : (Ljava/util/List;[Ljava/lang/String;[I)[I
    //   292: astore_2
    //   293: aload_2
    //   294: ifnull -> 304
    //   297: aload_2
    //   298: astore_3
    //   299: aload_2
    //   300: arraylength
    //   301: ifne -> 312
    //   304: iconst_1
    //   305: newarray int
    //   307: dup
    //   308: iconst_0
    //   309: iconst_1
    //   310: iastore
    //   311: astore_3
    //   312: aload_0
    //   313: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AE_AVAILABLE_MODES : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   316: aload_3
    //   317: invokevirtual set : (Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)V
    //   320: aload_1
    //   321: invokevirtual getMinExposureCompensation : ()I
    //   324: istore #6
    //   326: aload_1
    //   327: invokevirtual getMaxExposureCompensation : ()I
    //   330: istore #4
    //   332: aload_0
    //   333: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AE_COMPENSATION_RANGE : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   336: iload #6
    //   338: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   341: iload #4
    //   343: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   346: invokestatic create : (Ljava/lang/Comparable;Ljava/lang/Comparable;)Landroid/util/Range;
    //   349: invokevirtual set : (Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)V
    //   352: aload_1
    //   353: invokevirtual getExposureCompensationStep : ()F
    //   356: fstore #11
    //   358: aload_0
    //   359: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AE_COMPENSATION_STEP : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   362: fload #11
    //   364: invokestatic createRational : (F)Landroid/util/Rational;
    //   367: invokevirtual set : (Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)V
    //   370: aload_1
    //   371: invokevirtual isAutoExposureLockSupported : ()Z
    //   374: istore #12
    //   376: aload_0
    //   377: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AE_LOCK_AVAILABLE : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   380: iload #12
    //   382: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   385: invokevirtual set : (Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)V
    //   388: return
    //   389: new java/lang/AssertionError
    //   392: dup
    //   393: ldc_w 'At least one FPS range must be supported.'
    //   396: invokespecial <init> : (Ljava/lang/Object;)V
    //   399: athrow
    //   400: new java/lang/AssertionError
    //   403: dup
    //   404: ldc_w 'Supported FPS ranges cannot be null.'
    //   407: invokespecial <init> : (Ljava/lang/Object;)V
    //   410: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #394	-> 0
    //   #395	-> 5
    //   #396	-> 18
    //   #397	-> 27
    //   #398	-> 30
    //   #399	-> 57
    //   #404	-> 64
    //   #406	-> 70
    //   #407	-> 76
    //   #408	-> 89
    //   #409	-> 92
    //   #416	-> 102
    //   #417	-> 107
    //   #420	-> 111
    //   #421	-> 119
    //   #424	-> 124
    //   #425	-> 131
    //   #426	-> 134
    //   #427	-> 161
    //   #428	-> 172
    //   #429	-> 191
    //   #427	-> 199
    //   #430	-> 217
    //   #431	-> 223
    //   #438	-> 232
    //   #440	-> 237
    //   #448	-> 237
    //   #454	-> 237
    //   #458	-> 293
    //   #459	-> 304
    //   #465	-> 312
    //   #472	-> 320
    //   #473	-> 326
    //   #475	-> 332
    //   #482	-> 352
    //   #484	-> 358
    //   #491	-> 370
    //   #493	-> 376
    //   #495	-> 388
    //   #422	-> 389
    //   #418	-> 400
  }
  
  private static void mapControlAf(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getSupportedFocusModes : ()Ljava/util/List;
    //   4: astore_1
    //   5: aload_1
    //   6: bipush #7
    //   8: anewarray java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc 'auto'
    //   15: aastore
    //   16: dup
    //   17: iconst_1
    //   18: ldc 'continuous-picture'
    //   20: aastore
    //   21: dup
    //   22: iconst_2
    //   23: ldc 'continuous-video'
    //   25: aastore
    //   26: dup
    //   27: iconst_3
    //   28: ldc 'edof'
    //   30: aastore
    //   31: dup
    //   32: iconst_4
    //   33: ldc 'infinity'
    //   35: aastore
    //   36: dup
    //   37: iconst_5
    //   38: ldc 'macro'
    //   40: aastore
    //   41: dup
    //   42: bipush #6
    //   44: ldc 'fixed'
    //   46: aastore
    //   47: bipush #7
    //   49: newarray int
    //   51: dup
    //   52: iconst_0
    //   53: iconst_1
    //   54: iastore
    //   55: dup
    //   56: iconst_1
    //   57: iconst_4
    //   58: iastore
    //   59: dup
    //   60: iconst_2
    //   61: iconst_3
    //   62: iastore
    //   63: dup
    //   64: iconst_3
    //   65: iconst_5
    //   66: iastore
    //   67: dup
    //   68: iconst_4
    //   69: iconst_0
    //   70: iastore
    //   71: dup
    //   72: iconst_5
    //   73: iconst_2
    //   74: iastore
    //   75: dup
    //   76: bipush #6
    //   78: iconst_0
    //   79: iastore
    //   80: invokestatic convertStringListToIntList : (Ljava/util/List;[Ljava/lang/String;[I)Ljava/util/List;
    //   83: astore_2
    //   84: aload_2
    //   85: ifnull -> 99
    //   88: aload_2
    //   89: astore_1
    //   90: aload_2
    //   91: invokeinterface size : ()I
    //   96: ifne -> 128
    //   99: ldc 'LegacyMetadataMapper'
    //   101: ldc_w 'No AF modes supported (HAL bug); defaulting to AF_MODE_OFF only'
    //   104: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   107: pop
    //   108: new java/util/ArrayList
    //   111: dup
    //   112: iconst_1
    //   113: invokespecial <init> : (I)V
    //   116: astore_1
    //   117: aload_1
    //   118: iconst_0
    //   119: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   122: invokeinterface add : (Ljava/lang/Object;)Z
    //   127: pop
    //   128: aload_0
    //   129: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   132: aload_1
    //   133: invokestatic toIntArray : (Ljava/util/List;)[I
    //   136: invokevirtual set : (Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)V
    //   139: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #504	-> 0
    //   #506	-> 5
    //   #516	-> 5
    //   #526	-> 5
    //   #530	-> 84
    //   #531	-> 99
    //   #532	-> 108
    //   #533	-> 117
    //   #536	-> 128
    //   #543	-> 139
  }
  
  private static void mapControlAwb(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getSupportedWhiteBalance : ()Ljava/util/List;
    //   4: astore_2
    //   5: aload_2
    //   6: bipush #8
    //   8: anewarray java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc 'auto'
    //   15: aastore
    //   16: dup
    //   17: iconst_1
    //   18: ldc_w 'incandescent'
    //   21: aastore
    //   22: dup
    //   23: iconst_2
    //   24: ldc_w 'fluorescent'
    //   27: aastore
    //   28: dup
    //   29: iconst_3
    //   30: ldc_w 'warm-fluorescent'
    //   33: aastore
    //   34: dup
    //   35: iconst_4
    //   36: ldc_w 'daylight'
    //   39: aastore
    //   40: dup
    //   41: iconst_5
    //   42: ldc_w 'cloudy-daylight'
    //   45: aastore
    //   46: dup
    //   47: bipush #6
    //   49: ldc_w 'twilight'
    //   52: aastore
    //   53: dup
    //   54: bipush #7
    //   56: ldc_w 'shade'
    //   59: aastore
    //   60: bipush #8
    //   62: newarray int
    //   64: dup
    //   65: iconst_0
    //   66: iconst_1
    //   67: iastore
    //   68: dup
    //   69: iconst_1
    //   70: iconst_2
    //   71: iastore
    //   72: dup
    //   73: iconst_2
    //   74: iconst_3
    //   75: iastore
    //   76: dup
    //   77: iconst_3
    //   78: iconst_4
    //   79: iastore
    //   80: dup
    //   81: iconst_4
    //   82: iconst_5
    //   83: iastore
    //   84: dup
    //   85: iconst_5
    //   86: bipush #6
    //   88: iastore
    //   89: dup
    //   90: bipush #6
    //   92: bipush #7
    //   94: iastore
    //   95: dup
    //   96: bipush #7
    //   98: bipush #8
    //   100: iastore
    //   101: invokestatic convertStringListToIntList : (Ljava/util/List;[Ljava/lang/String;[I)Ljava/util/List;
    //   104: astore_3
    //   105: aload_3
    //   106: ifnull -> 120
    //   109: aload_3
    //   110: astore_2
    //   111: aload_3
    //   112: invokeinterface size : ()I
    //   117: ifne -> 149
    //   120: ldc 'LegacyMetadataMapper'
    //   122: ldc_w 'No AWB modes supported (HAL bug); defaulting to AWB_MODE_AUTO only'
    //   125: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   128: pop
    //   129: new java/util/ArrayList
    //   132: dup
    //   133: iconst_1
    //   134: invokespecial <init> : (I)V
    //   137: astore_2
    //   138: aload_2
    //   139: iconst_1
    //   140: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   143: invokeinterface add : (Ljava/lang/Object;)Z
    //   148: pop
    //   149: aload_0
    //   150: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AWB_AVAILABLE_MODES : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   153: aload_2
    //   154: invokestatic toIntArray : (Ljava/util/List;)[I
    //   157: invokevirtual set : (Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)V
    //   160: aload_1
    //   161: invokevirtual isAutoWhiteBalanceLockSupported : ()Z
    //   164: istore #4
    //   166: aload_0
    //   167: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AWB_LOCK_AVAILABLE : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   170: iload #4
    //   172: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   175: invokevirtual set : (Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)V
    //   178: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #551	-> 0
    //   #553	-> 5
    //   #564	-> 5
    //   #576	-> 5
    //   #580	-> 105
    //   #581	-> 120
    //   #582	-> 129
    //   #583	-> 138
    //   #586	-> 149
    //   #598	-> 160
    //   #600	-> 166
    //   #603	-> 178
  }
  
  private static void mapControlOther(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    int[] arrayOfInt1, arrayOfInt4, arrayOfInt3;
    if (paramParameters.isVideoStabilizationSupported()) {
      arrayOfInt4 = new int[2];
      arrayOfInt4[0] = 0;
      arrayOfInt4[1] = 1;
    } else {
      arrayOfInt4 = new int[1];
      arrayOfInt4[0] = 0;
    } 
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES, arrayOfInt4);
    int i = paramParameters.getMaxNumMeteringAreas();
    int j = paramParameters.getMaxNumFocusAreas();
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.CONTROL_MAX_REGIONS, new int[] { i, 0, j });
    List<String> list3 = paramParameters.getSupportedColorEffects();
    if (list3 == null) {
      arrayOfInt3 = new int[0];
    } else {
      arrayOfInt3 = ArrayUtils.convertStringListToIntArray((List<String>)arrayOfInt3, sLegacyEffectMode, sEffectModes);
    } 
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.CONTROL_AVAILABLE_EFFECTS, arrayOfInt3);
    int k = paramParameters.getMaxNumDetectedFaces();
    List<String> list4 = paramParameters.getSupportedSceneModes();
    String[] arrayOfString = sLegacySceneModes;
    int[] arrayOfInt2 = sSceneModes;
    List<Integer> list2 = ArrayUtils.convertStringListToIntList(list4, arrayOfString, arrayOfInt2);
    List<Integer> list1 = list2;
    if (list4 != null) {
      list1 = list2;
      if (list4.size() == 1) {
        list1 = list2;
        if (((String)list4.get(0)).equals("auto"))
          list1 = null; 
      } 
    } 
    j = 1;
    i = j;
    if (list1 == null) {
      i = j;
      if (k == 0)
        i = 0; 
    } 
    if (i != 0) {
      list2 = list1;
      if (list1 == null)
        list2 = new ArrayList<>(); 
      if (k > 0)
        list2.add(Integer.valueOf(1)); 
      if (list2.contains(Integer.valueOf(0)))
        while (list2.remove(new Integer(0))); 
      paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.CONTROL_AVAILABLE_SCENE_MODES, ArrayUtils.toIntArray(list2));
    } else {
      paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.CONTROL_AVAILABLE_SCENE_MODES, new int[] { 0 });
    } 
    CameraCharacteristics.Key<int[]> key = CameraCharacteristics.CONTROL_AVAILABLE_MODES;
    if (i != 0) {
      arrayOfInt1 = new int[2];
      arrayOfInt1[0] = 1;
      arrayOfInt1[1] = 2;
    } else {
      arrayOfInt1 = new int[1];
      arrayOfInt1[0] = 1;
    } 
    paramCameraMetadataNative.set((CameraCharacteristics.Key)key, arrayOfInt1);
  }
  
  private static void mapLens(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    if ("fixed".equals(paramParameters.getFocusMode()))
      paramCameraMetadataNative.set(CameraCharacteristics.LENS_INFO_MINIMUM_FOCUS_DISTANCE, Float.valueOf(0.0F)); 
    float f = paramParameters.getFocalLength();
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS, new float[] { f });
  }
  
  private static void mapFlash(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    int i = 0;
    List<String> list = paramParameters.getSupportedFlashModes();
    if (list != null)
      i = ListUtils.<String>listElementsEqualTo(list, "off") ^ true; 
    paramCameraMetadataNative.set(CameraCharacteristics.FLASH_INFO_AVAILABLE, Boolean.valueOf(i));
  }
  
  private static void mapJpeg(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    List<Camera.Size> list = paramParameters.getSupportedJpegThumbnailSizes();
    if (list != null) {
      Size[] arrayOfSize = ParameterUtils.convertSizeListToArray(list);
      Arrays.sort(arrayOfSize, new SizeAreaComparator());
      paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.JPEG_AVAILABLE_THUMBNAIL_SIZES, arrayOfSize);
    } 
  }
  
  private static void mapRequest(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES, new int[] { 0 });
    CameraCharacteristics.Key<int[]> key2 = CameraCharacteristics.COLOR_CORRECTION_AVAILABLE_ABERRATION_MODES, key5 = CameraCharacteristics.CONTROL_AE_AVAILABLE_ANTIBANDING_MODES, key8 = CameraCharacteristics.CONTROL_AE_AVAILABLE_MODES;
    CameraCharacteristics.Key<Range<Integer>[]> key = CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES;
    CameraCharacteristics.Key<Range<Integer>> key12 = CameraCharacteristics.CONTROL_AE_COMPENSATION_RANGE;
    CameraCharacteristics.Key<Rational> key13 = CameraCharacteristics.CONTROL_AE_COMPENSATION_STEP;
    CameraCharacteristics.Key<Boolean> key14 = CameraCharacteristics.CONTROL_AE_LOCK_AVAILABLE;
    CameraCharacteristics.Key<int[]> key17 = CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES, key20 = CameraCharacteristics.CONTROL_AVAILABLE_EFFECTS, key23 = CameraCharacteristics.CONTROL_AVAILABLE_MODES, key26 = CameraCharacteristics.CONTROL_AVAILABLE_SCENE_MODES, key29 = CameraCharacteristics.CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES, key32 = CameraCharacteristics.CONTROL_AWB_AVAILABLE_MODES;
    CameraCharacteristics.Key<Boolean> key35 = CameraCharacteristics.CONTROL_AWB_LOCK_AVAILABLE;
    CameraCharacteristics.Key<int[]> key36 = CameraCharacteristics.CONTROL_MAX_REGIONS;
    CameraCharacteristics.Key<Range<Float>> key39 = CameraCharacteristics.CONTROL_ZOOM_RATIO_RANGE;
    CameraCharacteristics.Key<Boolean> key40 = CameraCharacteristics.FLASH_INFO_AVAILABLE;
    CameraCharacteristics.Key<Integer> key41 = CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL;
    CameraCharacteristics.Key<Size[]> key44 = CameraCharacteristics.JPEG_AVAILABLE_THUMBNAIL_SIZES;
    CameraCharacteristics.Key<Integer> key47 = CameraCharacteristics.LENS_FACING;
    CameraCharacteristics.Key<float[]> key51 = CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS;
    CameraCharacteristics.Key<int[]> key54 = CameraCharacteristics.NOISE_REDUCTION_AVAILABLE_NOISE_REDUCTION_MODES, key57 = CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES, key60 = CameraCharacteristics.REQUEST_MAX_NUM_OUTPUT_STREAMS;
    CameraCharacteristics.Key<Integer> key63 = CameraCharacteristics.REQUEST_PARTIAL_RESULT_COUNT;
    CameraCharacteristics.Key<Byte> key66 = CameraCharacteristics.REQUEST_PIPELINE_MAX_DEPTH;
    CameraCharacteristics.Key<Float> key67 = CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM;
    CameraCharacteristics.Key<Integer> key70 = CameraCharacteristics.SCALER_CROPPING_TYPE;
    CameraCharacteristics.Key<int[]> key73 = CameraCharacteristics.SENSOR_AVAILABLE_TEST_PATTERN_MODES;
    CameraCharacteristics.Key<Rect> key75 = CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE;
    CameraCharacteristics.Key<SizeF> key78 = CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE;
    CameraCharacteristics.Key<Size> key79 = CameraCharacteristics.SENSOR_INFO_PIXEL_ARRAY_SIZE;
    CameraCharacteristics.Key<Rect> key80 = CameraCharacteristics.SENSOR_INFO_PRE_CORRECTION_ACTIVE_ARRAY_SIZE;
    CameraCharacteristics.Key<Integer> key83 = CameraCharacteristics.SENSOR_INFO_TIMESTAMP_SOURCE, key85 = CameraCharacteristics.SENSOR_ORIENTATION;
    CameraCharacteristics.Key<int[]> key88 = CameraCharacteristics.STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES;
    CameraCharacteristics.Key<Integer> key89 = CameraCharacteristics.STATISTICS_INFO_MAX_FACE_COUNT, key91 = CameraCharacteristics.SYNC_MAX_LATENCY;
    ArrayList<CameraCharacteristics.Key<Float>> arrayList1 = new ArrayList(Arrays.asList((Object[])new CameraCharacteristics.Key[] { 
            key2, key5, key8, key, key12, key13, key14, key17, key20, key23, 
            key26, key29, key32, key35, key36, key39, key40, key41, key44, key47, 
            key51, key54, key57, key60, key63, key66, key67, key70, key73, key75, 
            key78, key79, key80, key83, key85, key88, key89, key91 }));
    if (paramCameraMetadataNative.get(CameraCharacteristics.LENS_INFO_MINIMUM_FOCUS_DISTANCE) != null)
      arrayList1.add(CameraCharacteristics.LENS_INFO_MINIMUM_FOCUS_DISTANCE); 
    CameraCharacteristics.Key<int[]> key50 = CameraCharacteristics.REQUEST_AVAILABLE_CHARACTERISTICS_KEYS;
    int[] arrayOfInt = getTagsForKeys((CameraCharacteristics.Key<?>[])arrayList1.<CameraCharacteristics.Key>toArray(new CameraCharacteristics.Key[0]));
    paramCameraMetadataNative.set((CameraCharacteristics.Key)key50, arrayOfInt);
    CaptureRequest.Key<Integer> key38 = CaptureRequest.COLOR_CORRECTION_ABERRATION_MODE, key59 = CaptureRequest.CONTROL_AE_ANTIBANDING_MODE, key7 = CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION;
    CaptureRequest.Key<Boolean> key10 = CaptureRequest.CONTROL_AE_LOCK;
    CaptureRequest.Key<Integer> key56 = CaptureRequest.CONTROL_AE_MODE;
    CaptureRequest.Key<Range<Integer>> key82 = CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE;
    CaptureRequest.Key<Integer> key74 = CaptureRequest.CONTROL_AF_MODE, key53 = CaptureRequest.CONTROL_AF_TRIGGER;
    CaptureRequest.Key<Boolean> key69 = CaptureRequest.CONTROL_AWB_LOCK;
    CaptureRequest.Key<Integer> key31 = CaptureRequest.CONTROL_AWB_MODE, key28 = CaptureRequest.CONTROL_CAPTURE_INTENT, key22 = CaptureRequest.CONTROL_EFFECT_MODE, key62 = CaptureRequest.CONTROL_MODE, key11 = CaptureRequest.CONTROL_SCENE_MODE, key46 = CaptureRequest.CONTROL_VIDEO_STABILIZATION_MODE;
    CaptureRequest.Key<Float> key43 = CaptureRequest.CONTROL_ZOOM_RATIO;
    CaptureRequest.Key<Integer> key84 = CaptureRequest.FLASH_MODE;
    CaptureRequest.Key<double[]> key16 = CaptureRequest.JPEG_GPS_COORDINATES;
    CaptureRequest.Key<String> key65 = CaptureRequest.JPEG_GPS_PROCESSING_METHOD;
    CaptureRequest.Key<Long> key77 = CaptureRequest.JPEG_GPS_TIMESTAMP;
    CaptureRequest.Key<Integer> key25 = CaptureRequest.JPEG_ORIENTATION;
    CaptureRequest.Key<Byte> key34 = CaptureRequest.JPEG_QUALITY, key4 = CaptureRequest.JPEG_THUMBNAIL_QUALITY;
    CaptureRequest.Key<Size> key49 = CaptureRequest.JPEG_THUMBNAIL_SIZE;
    CaptureRequest.Key<Float> key19 = CaptureRequest.LENS_FOCAL_LENGTH;
    CaptureRequest.Key<Integer> key72 = CaptureRequest.NOISE_REDUCTION_MODE;
    CaptureRequest.Key<Rect> key90 = CaptureRequest.SCALER_CROP_REGION;
    CaptureRequest.Key<Integer> key87 = CaptureRequest.STATISTICS_FACE_DETECT_MODE;
    ArrayList<CaptureRequest.Key<MeteringRectangle[]>> arrayList = new ArrayList(Arrays.asList((Object[])new CaptureRequest.Key[] { 
            key38, key59, key7, key10, key56, key82, key74, key53, key69, key31, 
            key28, key22, key62, key11, key46, key43, key84, key16, key65, key77, 
            key25, key34, key4, key49, key19, key72, key90, key87 }));
    if (paramParameters.getMaxNumMeteringAreas() > 0)
      arrayList.add(CaptureRequest.CONTROL_AE_REGIONS); 
    if (paramParameters.getMaxNumFocusAreas() > 0)
      arrayList.add(CaptureRequest.CONTROL_AF_REGIONS); 
    CaptureRequest.Key[] arrayOfKey1 = new CaptureRequest.Key[arrayList.size()];
    arrayList.toArray(arrayOfKey1);
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.REQUEST_AVAILABLE_REQUEST_KEYS, getTagsForKeys((CaptureRequest.Key<?>[])arrayOfKey1));
    CaptureResult.Key<Integer> key3 = CaptureResult.COLOR_CORRECTION_ABERRATION_MODE, key45 = CaptureResult.CONTROL_AE_ANTIBANDING_MODE, key58 = CaptureResult.CONTROL_AE_EXPOSURE_COMPENSATION;
    CaptureResult.Key<Boolean> key48 = CaptureResult.CONTROL_AE_LOCK;
    CaptureResult.Key<Integer> key76 = CaptureResult.CONTROL_AE_MODE, key30 = CaptureResult.CONTROL_AF_MODE, key81 = CaptureResult.CONTROL_AF_STATE, key86 = CaptureResult.CONTROL_AWB_MODE;
    CaptureResult.Key<Boolean> key71 = CaptureResult.CONTROL_AWB_LOCK;
    CaptureResult.Key<Integer> key6 = CaptureResult.CONTROL_MODE;
    CaptureResult.Key<Float> key33 = CaptureResult.CONTROL_ZOOM_RATIO;
    CaptureResult.Key<Integer> key9 = CaptureResult.FLASH_MODE;
    CaptureResult.Key<double[]> key37 = CaptureResult.JPEG_GPS_COORDINATES;
    CaptureResult.Key<String> key21 = CaptureResult.JPEG_GPS_PROCESSING_METHOD;
    CaptureResult.Key<Long> key64 = CaptureResult.JPEG_GPS_TIMESTAMP;
    CaptureResult.Key<Integer> key52 = CaptureResult.JPEG_ORIENTATION;
    CaptureResult.Key<Byte> key42 = CaptureResult.JPEG_QUALITY, key18 = CaptureResult.JPEG_THUMBNAIL_QUALITY;
    CaptureResult.Key<Float> key15 = CaptureResult.LENS_FOCAL_LENGTH;
    CaptureResult.Key<Integer> key55 = CaptureResult.NOISE_REDUCTION_MODE;
    CaptureResult.Key<Byte> key27 = CaptureResult.REQUEST_PIPELINE_DEPTH;
    CaptureResult.Key<Rect> key24 = CaptureResult.SCALER_CROP_REGION;
    CaptureResult.Key<Long> key68 = CaptureResult.SENSOR_TIMESTAMP;
    CaptureResult.Key<Integer> key61 = CaptureResult.STATISTICS_FACE_DETECT_MODE;
    ArrayList<CaptureResult.Key<MeteringRectangle[]>> arrayList2 = new ArrayList(Arrays.asList((Object[])new CaptureResult.Key[] { 
            key3, key45, key58, key48, key76, key30, key81, key86, key71, key6, 
            key33, key9, key37, key21, key64, key52, key42, key18, key15, key55, 
            key27, key24, key68, key61 }));
    if (paramParameters.getMaxNumMeteringAreas() > 0)
      arrayList2.add(CaptureResult.CONTROL_AE_REGIONS); 
    if (paramParameters.getMaxNumFocusAreas() > 0)
      arrayList2.add(CaptureResult.CONTROL_AF_REGIONS); 
    CaptureResult.Key[] arrayOfKey = new CaptureResult.Key[arrayList2.size()];
    arrayList2.toArray(arrayOfKey);
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.REQUEST_AVAILABLE_RESULT_KEYS, getTagsForKeys((CaptureResult.Key<?>[])arrayOfKey));
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.REQUEST_MAX_NUM_OUTPUT_STREAMS, new int[] { 0, 3, 1 });
    paramCameraMetadataNative.set(CameraCharacteristics.REQUEST_MAX_NUM_INPUT_STREAMS, Integer.valueOf(0));
    paramCameraMetadataNative.set(CameraCharacteristics.REQUEST_PARTIAL_RESULT_COUNT, Integer.valueOf(1));
    CameraCharacteristics.Key<Byte> key1 = CameraCharacteristics.REQUEST_PIPELINE_MAX_DEPTH;
    paramCameraMetadataNative.set(key1, Byte.valueOf((byte)6));
  }
  
  private static void mapScaler(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    Range<Float> range = new Range(Float.valueOf(1.0F), Float.valueOf(ParameterUtils.getMaxZoomRatio(paramParameters)));
    paramCameraMetadataNative.set(CameraCharacteristics.CONTROL_ZOOM_RATIO_RANGE, range);
    paramCameraMetadataNative.set(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM, Float.valueOf(ParameterUtils.getMaxZoomRatio(paramParameters)));
    paramCameraMetadataNative.set(CameraCharacteristics.SCALER_CROPPING_TYPE, Integer.valueOf(0));
  }
  
  private static void mapSensor(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    Size size = ParameterUtils.getLargestSupportedJpegSizeByArea(paramParameters);
    Rect rect = ParamsUtils.createRect(size);
    paramCameraMetadataNative.set(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE, rect);
    paramCameraMetadataNative.set(CameraCharacteristics.SENSOR_INFO_PRE_CORRECTION_ACTIVE_ARRAY_SIZE, rect);
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.SENSOR_AVAILABLE_TEST_PATTERN_MODES, new int[] { 0 });
    paramCameraMetadataNative.set(CameraCharacteristics.SENSOR_INFO_PIXEL_ARRAY_SIZE, size);
    float f1 = paramParameters.getFocalLength();
    double d1 = paramParameters.getHorizontalViewAngle() * Math.PI / 180.0D;
    double d2 = paramParameters.getVerticalViewAngle() * Math.PI / 180.0D;
    float f2 = (float)Math.abs((f1 * 2.0F) * Math.tan(d2 / 2.0D));
    f1 = (float)Math.abs((2.0F * f1) * Math.tan(d1 / 2.0D));
    paramCameraMetadataNative.set(CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE, new SizeF(f1, f2));
    paramCameraMetadataNative.set(CameraCharacteristics.SENSOR_INFO_TIMESTAMP_SOURCE, Integer.valueOf(0));
  }
  
  private static void mapStatistics(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    int[] arrayOfInt;
    if (paramParameters.getMaxNumDetectedFaces() > 0) {
      arrayOfInt = new int[2];
      arrayOfInt[0] = 0;
      arrayOfInt[1] = 1;
    } else {
      arrayOfInt = new int[1];
      arrayOfInt[0] = 0;
    } 
    paramCameraMetadataNative.set((CameraCharacteristics.Key)CameraCharacteristics.STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES, arrayOfInt);
    paramCameraMetadataNative.set(CameraCharacteristics.STATISTICS_INFO_MAX_FACE_COUNT, Integer.valueOf(paramParameters.getMaxNumDetectedFaces()));
  }
  
  private static void mapSync(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    paramCameraMetadataNative.set(CameraCharacteristics.SYNC_MAX_LATENCY, Integer.valueOf(-1));
  }
  
  private static void appendStreamConfig(ArrayList<StreamConfiguration> paramArrayList, int paramInt, List<Camera.Size> paramList) {
    for (Camera.Size size : paramList) {
      StreamConfiguration streamConfiguration = new StreamConfiguration(paramInt, size.width, size.height, false);
      paramArrayList.add(streamConfiguration);
    } 
  }
  
  private static final String[] sLegacySceneModes = new String[] { 
      "auto", "action", "portrait", "landscape", "night", "night-portrait", "theatre", "beach", "snow", "sunset", 
      "steadyphoto", "fireworks", "sports", "party", "candlelight", "barcode", "hdr" };
  
  private static final int[] sSceneModes = new int[] { 
      0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
      11, 12, 13, 14, 15, 16, 18 };
  
  static int convertSceneModeFromLegacy(String paramString) {
    if (paramString == null)
      return 0; 
    int i = ArrayUtils.getArrayIndex(sLegacySceneModes, paramString);
    if (i < 0)
      return -1; 
    return sSceneModes[i];
  }
  
  static String convertSceneModeToLegacy(int paramInt) {
    if (paramInt == 1)
      return "auto"; 
    paramInt = ArrayUtils.getArrayIndex(sSceneModes, paramInt);
    if (paramInt < 0)
      return null; 
    return sLegacySceneModes[paramInt];
  }
  
  static {
    sLegacyEffectMode = new String[] { "none", "mono", "negative", "solarize", "sepia", "posterize", "whiteboard", "blackboard", "aqua" };
    sEffectModes = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
    sAllowedTemplates = new int[] { 1, 2, 3 };
  }
  
  static int convertEffectModeFromLegacy(String paramString) {
    if (paramString == null)
      return 0; 
    int i = ArrayUtils.getArrayIndex(sLegacyEffectMode, paramString);
    if (i < 0)
      return -1; 
    return sEffectModes[i];
  }
  
  static String convertEffectModeToLegacy(int paramInt) {
    paramInt = ArrayUtils.getArrayIndex(sEffectModes, paramInt);
    if (paramInt < 0)
      return null; 
    return sLegacyEffectMode[paramInt];
  }
  
  private static int convertAntiBandingMode(String paramString) {
    byte b;
    if (paramString == null)
      return -1; 
    switch (paramString.hashCode()) {
      default:
        b = -1;
        break;
      case 3005871:
        if (paramString.equals("auto")) {
          b = 3;
          break;
        } 
      case 1658188:
        if (paramString.equals("60hz")) {
          b = 2;
          break;
        } 
      case 1628397:
        if (paramString.equals("50hz")) {
          b = 1;
          break;
        } 
      case 109935:
        if (paramString.equals("off")) {
          b = 0;
          break;
        } 
    } 
    if (b != 0) {
      if (b != 1) {
        if (b != 2) {
          if (b != 3) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("convertAntiBandingMode - Unknown antibanding mode ");
            stringBuilder.append(paramString);
            Log.w("LegacyMetadataMapper", stringBuilder.toString());
            return -1;
          } 
          return 3;
        } 
        return 2;
      } 
      return 1;
    } 
    return 0;
  }
  
  static int convertAntiBandingModeOrDefault(String paramString) {
    int i = convertAntiBandingMode(paramString);
    if (i == -1)
      return 0; 
    return i;
  }
  
  private static int[] convertAeFpsRangeToLegacy(Range<Integer> paramRange) {
    int i = ((Integer)paramRange.getLower()).intValue();
    int j = ((Integer)paramRange.getUpper()).intValue();
    return new int[] { i, j };
  }
  
  private static long calculateJpegStallDuration(Camera.Size paramSize) {
    long l1 = paramSize.width, l2 = paramSize.height;
    return l1 * l2 * 71L + 200000000L;
  }
  
  public static void convertRequestMetadata(LegacyRequest paramLegacyRequest) {
    LegacyRequestMapper.convertRequestMetadata(paramLegacyRequest);
  }
  
  public static CameraMetadataNative createRequestTemplate(CameraCharacteristics paramCameraCharacteristics, int paramInt) {
    // Byte code:
    //   0: getstatic android/hardware/camera2/legacy/LegacyMetadataMapper.sAllowedTemplates : [I
    //   3: iload_1
    //   4: invokestatic contains : ([II)Z
    //   7: ifeq -> 687
    //   10: new android/hardware/camera2/impl/CameraMetadataNative
    //   13: dup
    //   14: invokespecial <init> : ()V
    //   17: astore_2
    //   18: aload_2
    //   19: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AWB_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   22: iconst_1
    //   23: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   26: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   29: aload_2
    //   30: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_ANTIBANDING_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   33: iconst_3
    //   34: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   37: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   40: aload_2
    //   41: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION : Landroid/hardware/camera2/CaptureRequest$Key;
    //   44: iconst_0
    //   45: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   48: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   51: aload_2
    //   52: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_LOCK : Landroid/hardware/camera2/CaptureRequest$Key;
    //   55: iconst_0
    //   56: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   59: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   62: aload_2
    //   63: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER : Landroid/hardware/camera2/CaptureRequest$Key;
    //   66: iconst_0
    //   67: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   70: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   73: aload_2
    //   74: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AF_TRIGGER : Landroid/hardware/camera2/CaptureRequest$Key;
    //   77: iconst_0
    //   78: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   81: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   84: aload_2
    //   85: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AWB_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   88: iconst_1
    //   89: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   92: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   95: aload_2
    //   96: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AWB_LOCK : Landroid/hardware/camera2/CaptureRequest$Key;
    //   99: iconst_0
    //   100: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   103: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   106: aload_0
    //   107: getstatic android/hardware/camera2/CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   110: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   113: checkcast android/graphics/Rect
    //   116: astore_3
    //   117: iconst_1
    //   118: anewarray android/hardware/camera2/params/MeteringRectangle
    //   121: astore #4
    //   123: aload_3
    //   124: invokevirtual width : ()I
    //   127: istore #5
    //   129: aload #4
    //   131: iconst_0
    //   132: new android/hardware/camera2/params/MeteringRectangle
    //   135: dup
    //   136: iconst_0
    //   137: iconst_0
    //   138: iload #5
    //   140: iconst_1
    //   141: isub
    //   142: aload_3
    //   143: invokevirtual height : ()I
    //   146: iconst_1
    //   147: isub
    //   148: iconst_0
    //   149: invokespecial <init> : (IIIII)V
    //   152: aastore
    //   153: aload_2
    //   154: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_REGIONS : Landroid/hardware/camera2/CaptureRequest$Key;
    //   157: aload #4
    //   159: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   162: aload_2
    //   163: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AWB_REGIONS : Landroid/hardware/camera2/CaptureRequest$Key;
    //   166: aload #4
    //   168: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   171: aload_2
    //   172: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AF_REGIONS : Landroid/hardware/camera2/CaptureRequest$Key;
    //   175: aload #4
    //   177: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   180: iload_1
    //   181: iconst_1
    //   182: if_icmpeq -> 218
    //   185: iload_1
    //   186: iconst_2
    //   187: if_icmpeq -> 212
    //   190: iload_1
    //   191: iconst_3
    //   192: if_icmpne -> 201
    //   195: iconst_3
    //   196: istore #5
    //   198: goto -> 221
    //   201: new java/lang/AssertionError
    //   204: dup
    //   205: ldc_w 'Impossible; keep in sync with sAllowedTemplates'
    //   208: invokespecial <init> : (Ljava/lang/Object;)V
    //   211: athrow
    //   212: iconst_2
    //   213: istore #5
    //   215: goto -> 221
    //   218: iconst_1
    //   219: istore #5
    //   221: aload_2
    //   222: getstatic android/hardware/camera2/CaptureRequest.CONTROL_CAPTURE_INTENT : Landroid/hardware/camera2/CaptureRequest$Key;
    //   225: iload #5
    //   227: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   230: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   233: aload_2
    //   234: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   237: iconst_1
    //   238: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   241: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   244: aload_2
    //   245: getstatic android/hardware/camera2/CaptureRequest.CONTROL_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   248: iconst_1
    //   249: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   252: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   255: aload_0
    //   256: getstatic android/hardware/camera2/CameraCharacteristics.LENS_INFO_MINIMUM_FOCUS_DISTANCE : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   259: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   262: checkcast java/lang/Float
    //   265: astore_3
    //   266: aload_3
    //   267: ifnull -> 285
    //   270: aload_3
    //   271: invokevirtual floatValue : ()F
    //   274: fconst_0
    //   275: fcmpl
    //   276: ifne -> 285
    //   279: iconst_0
    //   280: istore #5
    //   282: goto -> 357
    //   285: iload_1
    //   286: iconst_3
    //   287: if_icmpeq -> 331
    //   290: iload_1
    //   291: iconst_4
    //   292: if_icmpne -> 298
    //   295: goto -> 331
    //   298: iload_1
    //   299: iconst_1
    //   300: if_icmpeq -> 308
    //   303: iload_1
    //   304: iconst_2
    //   305: if_icmpne -> 354
    //   308: aload_0
    //   309: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   312: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   315: checkcast [I
    //   318: iconst_4
    //   319: invokestatic contains : ([II)Z
    //   322: ifeq -> 354
    //   325: iconst_4
    //   326: istore #5
    //   328: goto -> 357
    //   331: aload_0
    //   332: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   335: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   338: checkcast [I
    //   341: iconst_3
    //   342: invokestatic contains : ([II)Z
    //   345: ifeq -> 354
    //   348: iconst_3
    //   349: istore #5
    //   351: goto -> 357
    //   354: iconst_1
    //   355: istore #5
    //   357: aload_2
    //   358: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AF_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   361: iload #5
    //   363: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   366: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   369: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   372: astore_3
    //   373: aload_0
    //   374: aload_3
    //   375: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   378: checkcast [Landroid/util/Range;
    //   381: astore #6
    //   383: aload #6
    //   385: iconst_0
    //   386: aaload
    //   387: astore #4
    //   389: aload #6
    //   391: arraylength
    //   392: istore #7
    //   394: iconst_0
    //   395: istore #5
    //   397: iload #5
    //   399: iload #7
    //   401: if_icmpge -> 498
    //   404: aload #6
    //   406: iload #5
    //   408: aaload
    //   409: astore #8
    //   411: aload #4
    //   413: invokevirtual getUpper : ()Ljava/lang/Comparable;
    //   416: checkcast java/lang/Integer
    //   419: invokevirtual intValue : ()I
    //   422: aload #8
    //   424: invokevirtual getUpper : ()Ljava/lang/Comparable;
    //   427: checkcast java/lang/Integer
    //   430: invokevirtual intValue : ()I
    //   433: if_icmpge -> 442
    //   436: aload #8
    //   438: astore_3
    //   439: goto -> 489
    //   442: aload #4
    //   444: astore_3
    //   445: aload #4
    //   447: invokevirtual getUpper : ()Ljava/lang/Comparable;
    //   450: aload #8
    //   452: invokevirtual getUpper : ()Ljava/lang/Comparable;
    //   455: if_acmpne -> 489
    //   458: aload #4
    //   460: astore_3
    //   461: aload #4
    //   463: invokevirtual getLower : ()Ljava/lang/Comparable;
    //   466: checkcast java/lang/Integer
    //   469: invokevirtual intValue : ()I
    //   472: aload #8
    //   474: invokevirtual getLower : ()Ljava/lang/Comparable;
    //   477: checkcast java/lang/Integer
    //   480: invokevirtual intValue : ()I
    //   483: if_icmpge -> 489
    //   486: aload #8
    //   488: astore_3
    //   489: iinc #5, 1
    //   492: aload_3
    //   493: astore #4
    //   495: goto -> 397
    //   498: aload_2
    //   499: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   502: aload #4
    //   504: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   507: aload_2
    //   508: getstatic android/hardware/camera2/CaptureRequest.CONTROL_SCENE_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   511: iconst_0
    //   512: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   515: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   518: aload_2
    //   519: getstatic android/hardware/camera2/CaptureRequest.CONTROL_ZOOM_RATIO : Landroid/hardware/camera2/CaptureRequest$Key;
    //   522: fconst_1
    //   523: invokestatic valueOf : (F)Ljava/lang/Float;
    //   526: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   529: aload_2
    //   530: getstatic android/hardware/camera2/CaptureRequest.STATISTICS_FACE_DETECT_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   533: iconst_0
    //   534: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   537: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   540: aload_2
    //   541: getstatic android/hardware/camera2/CaptureRequest.FLASH_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   544: iconst_0
    //   545: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   548: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   551: iload_1
    //   552: iconst_2
    //   553: if_icmpne -> 570
    //   556: aload_2
    //   557: getstatic android/hardware/camera2/CaptureRequest.NOISE_REDUCTION_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   560: iconst_2
    //   561: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   564: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   567: goto -> 581
    //   570: aload_2
    //   571: getstatic android/hardware/camera2/CaptureRequest.NOISE_REDUCTION_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   574: iconst_1
    //   575: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   578: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   581: iload_1
    //   582: iconst_2
    //   583: if_icmpne -> 602
    //   586: getstatic android/hardware/camera2/CaptureRequest.COLOR_CORRECTION_ABERRATION_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   589: astore_3
    //   590: aload_2
    //   591: aload_3
    //   592: iconst_2
    //   593: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   596: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   599: goto -> 615
    //   602: getstatic android/hardware/camera2/CaptureRequest.COLOR_CORRECTION_ABERRATION_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   605: astore_3
    //   606: aload_2
    //   607: aload_3
    //   608: iconst_1
    //   609: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   612: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   615: getstatic android/hardware/camera2/CaptureRequest.LENS_FOCAL_LENGTH : Landroid/hardware/camera2/CaptureRequest$Key;
    //   618: astore_3
    //   619: getstatic android/hardware/camera2/CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   622: astore #4
    //   624: aload_0
    //   625: aload #4
    //   627: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   630: checkcast [F
    //   633: iconst_0
    //   634: faload
    //   635: fstore #9
    //   637: aload_2
    //   638: aload_3
    //   639: fload #9
    //   641: invokestatic valueOf : (F)Ljava/lang/Float;
    //   644: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   647: aload_0
    //   648: getstatic android/hardware/camera2/CameraCharacteristics.JPEG_AVAILABLE_THUMBNAIL_SIZES : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   651: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   654: checkcast [Landroid/util/Size;
    //   657: astore_0
    //   658: getstatic android/hardware/camera2/CaptureRequest.JPEG_THUMBNAIL_SIZE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   661: astore_3
    //   662: aload_0
    //   663: arraylength
    //   664: iconst_1
    //   665: if_icmple -> 675
    //   668: aload_0
    //   669: iconst_1
    //   670: aaload
    //   671: astore_0
    //   672: goto -> 679
    //   675: aload_0
    //   676: iconst_0
    //   677: aaload
    //   678: astore_0
    //   679: aload_2
    //   680: aload_3
    //   681: aload_0
    //   682: invokevirtual set : (Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V
    //   685: aload_2
    //   686: areturn
    //   687: new java/lang/IllegalArgumentException
    //   690: dup
    //   691: ldc_w 'templateId out of range'
    //   694: invokespecial <init> : (Ljava/lang/String;)V
    //   697: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1259	-> 0
    //   #1263	-> 10
    //   #1276	-> 18
    //   #1280	-> 29
    //   #1283	-> 40
    //   #1286	-> 51
    //   #1289	-> 62
    //   #1292	-> 73
    //   #1295	-> 84
    //   #1298	-> 95
    //   #1302	-> 106
    //   #1303	-> 117
    //   #1304	-> 123
    //   #1305	-> 129
    //   #1306	-> 153
    //   #1307	-> 162
    //   #1308	-> 171
    //   #1314	-> 180
    //   #1322	-> 195
    //   #1323	-> 198
    //   #1326	-> 201
    //   #1319	-> 212
    //   #1320	-> 215
    //   #1316	-> 218
    //   #1317	-> 221
    //   #1328	-> 221
    //   #1332	-> 233
    //   #1336	-> 244
    //   #1340	-> 255
    //   #1343	-> 266
    //   #1344	-> 270
    //   #1346	-> 279
    //   #1349	-> 285
    //   #1351	-> 285
    //   #1357	-> 298
    //   #1359	-> 308
    //   #1361	-> 325
    //   #1353	-> 331
    //   #1355	-> 348
    //   #1371	-> 354
    //   #1376	-> 369
    //   #1377	-> 373
    //   #1380	-> 383
    //   #1381	-> 389
    //   #1382	-> 411
    //   #1383	-> 436
    //   #1384	-> 442
    //   #1385	-> 458
    //   #1386	-> 486
    //   #1381	-> 489
    //   #1389	-> 498
    //   #1393	-> 507
    //   #1396	-> 518
    //   #1403	-> 529
    //   #1410	-> 540
    //   #1415	-> 551
    //   #1416	-> 556
    //   #1418	-> 570
    //   #1424	-> 581
    //   #1425	-> 586
    //   #1426	-> 590
    //   #1425	-> 590
    //   #1428	-> 602
    //   #1429	-> 606
    //   #1428	-> 606
    //   #1437	-> 615
    //   #1438	-> 624
    //   #1437	-> 637
    //   #1445	-> 647
    //   #1446	-> 658
    //   #1449	-> 685
    //   #1260	-> 687
  }
  
  private static int[] getTagsForKeys(CameraCharacteristics.Key<?>[] paramArrayOfKey) {
    int[] arrayOfInt = new int[paramArrayOfKey.length];
    for (byte b = 0; b < paramArrayOfKey.length; b++)
      arrayOfInt[b] = paramArrayOfKey[b].getNativeKey().getTag(); 
    return arrayOfInt;
  }
  
  private static int[] getTagsForKeys(CaptureRequest.Key<?>[] paramArrayOfKey) {
    int[] arrayOfInt = new int[paramArrayOfKey.length];
    for (byte b = 0; b < paramArrayOfKey.length; b++)
      arrayOfInt[b] = paramArrayOfKey[b].getNativeKey().getTag(); 
    return arrayOfInt;
  }
  
  private static int[] getTagsForKeys(CaptureResult.Key<?>[] paramArrayOfKey) {
    int[] arrayOfInt = new int[paramArrayOfKey.length];
    for (byte b = 0; b < paramArrayOfKey.length; b++)
      arrayOfInt[b] = paramArrayOfKey[b].getNativeKey().getTag(); 
    return arrayOfInt;
  }
  
  static String convertAfModeToLegacy(int paramInt, List<String> paramList) {
    if (paramList == null || paramList.isEmpty()) {
      Log.w("LegacyMetadataMapper", "No focus modes supported; API1 bug");
      return null;
    } 
    String str1 = null;
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4) {
              if (paramInt == 5)
                str1 = "edof"; 
            } else {
              str1 = "continuous-picture";
            } 
          } else {
            str1 = "continuous-video";
          } 
        } else {
          str1 = "macro";
        } 
      } else {
        str1 = "auto";
      } 
    } else if (paramList.contains("fixed")) {
      str1 = "fixed";
    } else {
      str1 = "infinity";
    } 
    String str2 = str1;
    if (!paramList.contains(str1)) {
      str2 = paramList.get(0);
      String str = String.format("convertAfModeToLegacy - ignoring unsupported mode %d, defaulting to %s", new Object[] { Integer.valueOf(paramInt), str2 });
      Log.w("LegacyMetadataMapper", str);
    } 
    return str2;
  }
}
