package android.hardware.camera2.legacy;

import android.hardware.camera2.impl.CameraMetadataNative;
import android.os.Handler;
import android.util.Log;

public class CameraDeviceState {
  private static final String[] sStateNames = new String[] { "ERROR", "UNCONFIGURED", "CONFIGURING", "IDLE", "CAPTURING" };
  
  private int mCurrentState = 1;
  
  private int mCurrentError = -1;
  
  private RequestHolder mCurrentRequest = null;
  
  private Handler mCurrentHandler = null;
  
  private CameraDeviceStateListener mCurrentListener = null;
  
  private static final boolean DEBUG = false;
  
  public static final int NO_CAPTURE_ERROR = -1;
  
  private static final int STATE_CAPTURING = 4;
  
  private static final int STATE_CONFIGURING = 2;
  
  private static final int STATE_ERROR = 0;
  
  private static final int STATE_IDLE = 3;
  
  private static final int STATE_UNCONFIGURED = 1;
  
  private static final String TAG = "CameraDeviceState";
  
  public void setError(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iload_1
    //   4: putfield mCurrentError : I
    //   7: aload_0
    //   8: iconst_0
    //   9: invokespecial doStateTransition : (I)V
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_2
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_2
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #96	-> 2
    //   #97	-> 7
    //   #98	-> 12
    //   #95	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	7	15	finally
    //   7	12	15	finally
  }
  
  public boolean setConfiguring() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_2
    //   4: invokespecial doStateTransition : (I)V
    //   7: aload_0
    //   8: getfield mCurrentError : I
    //   11: istore_1
    //   12: iload_1
    //   13: iconst_m1
    //   14: if_icmpne -> 22
    //   17: iconst_1
    //   18: istore_2
    //   19: goto -> 24
    //   22: iconst_0
    //   23: istore_2
    //   24: aload_0
    //   25: monitorexit
    //   26: iload_2
    //   27: ireturn
    //   28: astore_3
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_3
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #111	-> 2
    //   #112	-> 7
    //   #110	-> 28
    // Exception table:
    //   from	to	target	type
    //   2	7	28	finally
    //   7	12	28	finally
  }
  
  public boolean setIdle() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_3
    //   4: invokespecial doStateTransition : (I)V
    //   7: aload_0
    //   8: getfield mCurrentError : I
    //   11: istore_1
    //   12: iload_1
    //   13: iconst_m1
    //   14: if_icmpne -> 22
    //   17: iconst_1
    //   18: istore_2
    //   19: goto -> 24
    //   22: iconst_0
    //   23: istore_2
    //   24: aload_0
    //   25: monitorexit
    //   26: iload_2
    //   27: ireturn
    //   28: astore_3
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_3
    //   32: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #126	-> 2
    //   #127	-> 7
    //   #125	-> 28
    // Exception table:
    //   from	to	target	type
    //   2	7	28	finally
    //   7	12	28	finally
  }
  
  public boolean setCaptureStart(RequestHolder paramRequestHolder, long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: putfield mCurrentRequest : Landroid/hardware/camera2/legacy/RequestHolder;
    //   7: aload_0
    //   8: iconst_4
    //   9: lload_2
    //   10: iload #4
    //   12: invokespecial doStateTransition : (IJI)V
    //   15: aload_0
    //   16: getfield mCurrentError : I
    //   19: istore #4
    //   21: iload #4
    //   23: iconst_m1
    //   24: if_icmpne -> 33
    //   27: iconst_1
    //   28: istore #5
    //   30: goto -> 36
    //   33: iconst_0
    //   34: istore #5
    //   36: aload_0
    //   37: monitorexit
    //   38: iload #5
    //   40: ireturn
    //   41: astore_1
    //   42: aload_0
    //   43: monitorexit
    //   44: aload_1
    //   45: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #147	-> 2
    //   #148	-> 7
    //   #149	-> 15
    //   #146	-> 41
    // Exception table:
    //   from	to	target	type
    //   2	7	41	finally
    //   7	15	41	finally
    //   15	21	41	finally
  }
  
  public boolean setCaptureResult(RequestHolder paramRequestHolder, CameraMetadataNative paramCameraMetadataNative, int paramInt, Object paramObject) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mCurrentState : I
    //   6: istore #5
    //   8: iconst_1
    //   9: istore #6
    //   11: iconst_1
    //   12: istore #7
    //   14: iload #5
    //   16: iconst_4
    //   17: if_icmpeq -> 89
    //   20: new java/lang/StringBuilder
    //   23: astore_1
    //   24: aload_1
    //   25: invokespecial <init> : ()V
    //   28: aload_1
    //   29: ldc 'Cannot receive result while in state: '
    //   31: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   34: pop
    //   35: aload_1
    //   36: aload_0
    //   37: getfield mCurrentState : I
    //   40: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   43: pop
    //   44: ldc 'CameraDeviceState'
    //   46: aload_1
    //   47: invokevirtual toString : ()Ljava/lang/String;
    //   50: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   53: pop
    //   54: aload_0
    //   55: iconst_1
    //   56: putfield mCurrentError : I
    //   59: aload_0
    //   60: iconst_0
    //   61: invokespecial doStateTransition : (I)V
    //   64: aload_0
    //   65: getfield mCurrentError : I
    //   68: istore_3
    //   69: iload_3
    //   70: iconst_m1
    //   71: if_icmpne -> 81
    //   74: iload #7
    //   76: istore #6
    //   78: goto -> 84
    //   81: iconst_0
    //   82: istore #6
    //   84: aload_0
    //   85: monitorexit
    //   86: iload #6
    //   88: ireturn
    //   89: aload_0
    //   90: getfield mCurrentHandler : Landroid/os/Handler;
    //   93: ifnull -> 165
    //   96: aload_0
    //   97: getfield mCurrentListener : Landroid/hardware/camera2/legacy/CameraDeviceState$CameraDeviceStateListener;
    //   100: ifnull -> 165
    //   103: iload_3
    //   104: iconst_m1
    //   105: if_icmpeq -> 138
    //   108: aload_0
    //   109: getfield mCurrentHandler : Landroid/os/Handler;
    //   112: astore_2
    //   113: new android/hardware/camera2/legacy/CameraDeviceState$1
    //   116: astore #8
    //   118: aload #8
    //   120: aload_0
    //   121: iload_3
    //   122: aload #4
    //   124: aload_1
    //   125: invokespecial <init> : (Landroid/hardware/camera2/legacy/CameraDeviceState;ILjava/lang/Object;Landroid/hardware/camera2/legacy/RequestHolder;)V
    //   128: aload_2
    //   129: aload #8
    //   131: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   134: pop
    //   135: goto -> 165
    //   138: aload_0
    //   139: getfield mCurrentHandler : Landroid/os/Handler;
    //   142: astore #4
    //   144: new android/hardware/camera2/legacy/CameraDeviceState$2
    //   147: astore #8
    //   149: aload #8
    //   151: aload_0
    //   152: aload_2
    //   153: aload_1
    //   154: invokespecial <init> : (Landroid/hardware/camera2/legacy/CameraDeviceState;Landroid/hardware/camera2/impl/CameraMetadataNative;Landroid/hardware/camera2/legacy/RequestHolder;)V
    //   157: aload #4
    //   159: aload #8
    //   161: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   164: pop
    //   165: aload_0
    //   166: getfield mCurrentError : I
    //   169: istore_3
    //   170: iload_3
    //   171: iconst_m1
    //   172: if_icmpne -> 178
    //   175: goto -> 181
    //   178: iconst_0
    //   179: istore #6
    //   181: aload_0
    //   182: monitorexit
    //   183: iload #6
    //   185: ireturn
    //   186: astore_1
    //   187: aload_0
    //   188: monitorexit
    //   189: aload_1
    //   190: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #173	-> 2
    //   #174	-> 20
    //   #175	-> 54
    //   #176	-> 59
    //   #177	-> 64
    //   #180	-> 89
    //   #181	-> 103
    //   #182	-> 108
    //   #189	-> 138
    //   #197	-> 165
    //   #172	-> 186
    // Exception table:
    //   from	to	target	type
    //   2	8	186	finally
    //   20	54	186	finally
    //   54	59	186	finally
    //   59	64	186	finally
    //   64	69	186	finally
    //   89	103	186	finally
    //   108	135	186	finally
    //   138	165	186	finally
    //   165	170	186	finally
  }
  
  public boolean setCaptureResult(RequestHolder paramRequestHolder, CameraMetadataNative paramCameraMetadataNative) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: aload_2
    //   5: iconst_m1
    //   6: aconst_null
    //   7: invokevirtual setCaptureResult : (Landroid/hardware/camera2/legacy/RequestHolder;Landroid/hardware/camera2/impl/CameraMetadataNative;ILjava/lang/Object;)Z
    //   10: istore_3
    //   11: aload_0
    //   12: monitorexit
    //   13: iload_3
    //   14: ireturn
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #202	-> 2
    //   #202	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	11	15	finally
  }
  
  public void setRepeatingRequestError(long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mCurrentHandler : Landroid/os/Handler;
    //   6: astore #4
    //   8: new android/hardware/camera2/legacy/CameraDeviceState$3
    //   11: astore #5
    //   13: aload #5
    //   15: aload_0
    //   16: lload_1
    //   17: iload_3
    //   18: invokespecial <init> : (Landroid/hardware/camera2/legacy/CameraDeviceState;JI)V
    //   21: aload #4
    //   23: aload #5
    //   25: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   28: pop
    //   29: aload_0
    //   30: monitorexit
    //   31: return
    //   32: astore #5
    //   34: aload_0
    //   35: monitorexit
    //   36: aload #5
    //   38: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #215	-> 2
    //   #221	-> 29
    //   #214	-> 32
    // Exception table:
    //   from	to	target	type
    //   2	29	32	finally
  }
  
  public void setRequestQueueEmpty() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mCurrentHandler : Landroid/os/Handler;
    //   6: astore_1
    //   7: new android/hardware/camera2/legacy/CameraDeviceState$4
    //   10: astore_2
    //   11: aload_2
    //   12: aload_0
    //   13: invokespecial <init> : (Landroid/hardware/camera2/legacy/CameraDeviceState;)V
    //   16: aload_1
    //   17: aload_2
    //   18: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   21: pop
    //   22: aload_0
    //   23: monitorexit
    //   24: return
    //   25: astore_1
    //   26: aload_0
    //   27: monitorexit
    //   28: aload_1
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #229	-> 2
    //   #235	-> 22
    //   #228	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	22	25	finally
  }
  
  public void setCameraDeviceCallbacks(Handler paramHandler, CameraDeviceStateListener paramCameraDeviceStateListener) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: putfield mCurrentHandler : Landroid/os/Handler;
    //   7: aload_0
    //   8: aload_2
    //   9: putfield mCurrentListener : Landroid/hardware/camera2/legacy/CameraDeviceState$CameraDeviceStateListener;
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: astore_1
    //   16: aload_0
    //   17: monitorexit
    //   18: aload_1
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #245	-> 2
    //   #246	-> 7
    //   #247	-> 12
    //   #244	-> 15
    // Exception table:
    //   from	to	target	type
    //   2	7	15	finally
    //   7	12	15	finally
  }
  
  private void doStateTransition(int paramInt) {
    doStateTransition(paramInt, 0L, -1);
  }
  
  private void doStateTransition(int paramInt1, final long timestamp, final int error) {
    if (paramInt1 != this.mCurrentState) {
      String str1 = "UNKNOWN";
      String str2 = str1;
      if (paramInt1 >= 0) {
        String[] arrayOfString = sStateNames;
        str2 = str1;
        if (paramInt1 < arrayOfString.length)
          str2 = arrayOfString[paramInt1]; 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Legacy camera service transitioning to state ");
      stringBuilder.append(str2);
      Log.i("CameraDeviceState", stringBuilder.toString());
    } 
    if (paramInt1 != 0 && paramInt1 != 3 && 
      this.mCurrentState != paramInt1) {
      Handler handler = this.mCurrentHandler;
      if (handler != null && this.mCurrentListener != null)
        handler.post(new Runnable() {
              final CameraDeviceState this$0;
              
              public void run() {
                CameraDeviceState.this.mCurrentListener.onBusy();
              }
            }); 
    } 
    if (paramInt1 != 0) {
      if (paramInt1 != 2) {
        if (paramInt1 != 3) {
          if (paramInt1 == 4) {
            paramInt1 = this.mCurrentState;
            if (paramInt1 != 3 && paramInt1 != 4) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Cannot call capture while in state: ");
              stringBuilder.append(this.mCurrentState);
              Log.e("CameraDeviceState", stringBuilder.toString());
              this.mCurrentError = 1;
              doStateTransition(0);
            } else {
              Handler handler = this.mCurrentHandler;
              if (handler != null && this.mCurrentListener != null)
                if (error != -1) {
                  handler.post(new Runnable() {
                        final CameraDeviceState this$0;
                        
                        final int val$error;
                        
                        public void run() {
                          CameraDeviceState.this.mCurrentListener.onError(error, null, CameraDeviceState.this.mCurrentRequest);
                        }
                      });
                } else {
                  handler.post(new Runnable() {
                        final CameraDeviceState this$0;
                        
                        final long val$timestamp;
                        
                        public void run() {
                          CameraDeviceState.this.mCurrentListener.onCaptureStarted(CameraDeviceState.this.mCurrentRequest, timestamp);
                        }
                      });
                }  
              this.mCurrentState = 4;
            } 
          } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Transition to unknown state: ");
            stringBuilder.append(paramInt1);
            throw new IllegalStateException(stringBuilder.toString());
          } 
        } else {
          paramInt1 = this.mCurrentState;
          if (paramInt1 != 3)
            if (paramInt1 != 2 && paramInt1 != 4) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Cannot call idle while in state: ");
              stringBuilder.append(this.mCurrentState);
              Log.e("CameraDeviceState", stringBuilder.toString());
              this.mCurrentError = 1;
              doStateTransition(0);
            } else {
              if (this.mCurrentState != 3) {
                Handler handler = this.mCurrentHandler;
                if (handler != null && this.mCurrentListener != null)
                  handler.post(new Runnable() {
                        final CameraDeviceState this$0;
                        
                        public void run() {
                          CameraDeviceState.this.mCurrentListener.onIdle();
                        }
                      }); 
              } 
              this.mCurrentState = 3;
            }  
        } 
      } else {
        paramInt1 = this.mCurrentState;
        if (paramInt1 != 1 && paramInt1 != 3) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Cannot call configure while in state: ");
          stringBuilder.append(this.mCurrentState);
          Log.e("CameraDeviceState", stringBuilder.toString());
          this.mCurrentError = 1;
          doStateTransition(0);
        } else {
          if (this.mCurrentState != 2) {
            Handler handler = this.mCurrentHandler;
            if (handler != null && this.mCurrentListener != null)
              handler.post(new Runnable() {
                    final CameraDeviceState this$0;
                    
                    public void run() {
                      CameraDeviceState.this.mCurrentListener.onConfiguring();
                    }
                  }); 
          } 
          this.mCurrentState = 2;
        } 
      } 
    } else {
      if (this.mCurrentState != 0) {
        Handler handler = this.mCurrentHandler;
        if (handler != null && this.mCurrentListener != null)
          handler.post(new Runnable() {
                final CameraDeviceState this$0;
                
                public void run() {
                  CameraDeviceState.this.mCurrentListener.onError(CameraDeviceState.this.mCurrentError, null, CameraDeviceState.this.mCurrentRequest);
                }
              }); 
      } 
      this.mCurrentState = 0;
    } 
  }
  
  public static interface CameraDeviceStateListener {
    void onBusy();
    
    void onCaptureResult(CameraMetadataNative param1CameraMetadataNative, RequestHolder param1RequestHolder);
    
    void onCaptureStarted(RequestHolder param1RequestHolder, long param1Long);
    
    void onConfiguring();
    
    void onError(int param1Int, Object param1Object, RequestHolder param1RequestHolder);
    
    void onIdle();
    
    void onRepeatingRequestError(long param1Long, int param1Int);
    
    void onRequestQueueEmpty();
  }
}
