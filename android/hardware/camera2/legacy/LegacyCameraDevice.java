package android.hardware.camera2.legacy;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.ICameraDeviceCallbacks;
import android.hardware.camera2.impl.CaptureResultExtras;
import android.hardware.camera2.utils.SubmitInfo;
import android.os.ConditionVariable;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.ServiceSpecificException;
import android.util.Log;
import android.util.Size;
import android.util.SparseArray;
import android.view.Surface;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LegacyCameraDevice implements AutoCloseable {
  private final CameraDeviceState mDeviceState = new CameraDeviceState();
  
  private boolean mClosed = false;
  
  private final ConditionVariable mIdle = new ConditionVariable(true);
  
  private final HandlerThread mResultThread = new HandlerThread("ResultThread");
  
  private final HandlerThread mCallbackHandlerThread = new HandlerThread("CallbackThread");
  
  private CaptureResultExtras getExtrasFromRequest(RequestHolder paramRequestHolder) {
    return getExtrasFromRequest(paramRequestHolder, -1, null);
  }
  
  private CaptureResultExtras getExtrasFromRequest(RequestHolder paramRequestHolder, int paramInt, Object paramObject) {
    int i = -1;
    int j = i;
    if (paramInt == 5) {
      paramObject = paramObject;
      paramInt = this.mConfiguredSurfaces.indexOfValue(paramObject);
      if (paramInt < 0) {
        Log.e(this.TAG, "Buffer drop error reported for unknown Surface");
        j = i;
      } else {
        j = this.mConfiguredSurfaces.keyAt(paramInt);
      } 
    } 
    if (paramRequestHolder == null)
      return new CaptureResultExtras(-1, -1, -1, -1, -1L, -1, -1, null, -1L, -1L, -1L); 
    i = paramRequestHolder.getRequestId();
    paramInt = paramRequestHolder.getSubsequeceId();
    long l = paramRequestHolder.getFrameNumber();
    return new CaptureResultExtras(i, paramInt, 0, 0, l, 1, j, null, paramRequestHolder.getFrameNumber(), -1L, -1L);
  }
  
  private final CameraDeviceState.CameraDeviceStateListener mStateListener = (CameraDeviceState.CameraDeviceStateListener)new Object(this);
  
  private static final boolean DEBUG = false;
  
  private static final int GRALLOC_USAGE_HW_COMPOSER = 2048;
  
  private static final int GRALLOC_USAGE_HW_RENDER = 512;
  
  private static final int GRALLOC_USAGE_HW_TEXTURE = 256;
  
  private static final int GRALLOC_USAGE_HW_VIDEO_ENCODER = 65536;
  
  private static final int GRALLOC_USAGE_RENDERSCRIPT = 1048576;
  
  private static final int GRALLOC_USAGE_SW_READ_OFTEN = 3;
  
  private static final int ILLEGAL_VALUE = -1;
  
  public static final int MAX_DIMEN_FOR_ROUNDING = 1920;
  
  public static final int NATIVE_WINDOW_SCALING_MODE_SCALE_TO_WINDOW = 1;
  
  private final String TAG;
  
  private final Handler mCallbackHandler;
  
  private final int mCameraId;
  
  private SparseArray<Surface> mConfiguredSurfaces;
  
  private final ICameraDeviceCallbacks mDeviceCallbacks;
  
  private final RequestThreadManager mRequestThreadManager;
  
  private final Handler mResultHandler;
  
  private final CameraCharacteristics mStaticCharacteristics;
  
  static boolean needsConversion(Surface paramSurface) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    int i = detectSurfaceType(paramSurface);
    return (i == 35 || i == 842094169 || i == 17);
  }
  
  public LegacyCameraDevice(int paramInt, Camera paramCamera, CameraCharacteristics paramCameraCharacteristics, ICameraDeviceCallbacks paramICameraDeviceCallbacks) {
    this.mCameraId = paramInt;
    this.mDeviceCallbacks = paramICameraDeviceCallbacks;
    this.TAG = String.format("CameraDevice-%d-LE", new Object[] { Integer.valueOf(paramInt) });
    this.mResultThread.start();
    this.mResultHandler = new Handler(this.mResultThread.getLooper());
    this.mCallbackHandlerThread.start();
    Handler handler = new Handler(this.mCallbackHandlerThread.getLooper());
    this.mDeviceState.setCameraDeviceCallbacks(handler, this.mStateListener);
    this.mStaticCharacteristics = paramCameraCharacteristics;
    RequestThreadManager requestThreadManager = new RequestThreadManager(paramInt, paramCamera, paramCameraCharacteristics, this.mDeviceState);
    requestThreadManager.start();
  }
  
  public int configureOutputs(SparseArray<Surface> paramSparseArray) {
    return configureOutputs(paramSparseArray, false);
  }
  
  public int configureOutputs(SparseArray<Surface> paramSparseArray, boolean paramBoolean) {
    // Byte code:
    //   0: new java/util/ArrayList
    //   3: dup
    //   4: invokespecial <init> : ()V
    //   7: astore_3
    //   8: aload_1
    //   9: ifnull -> 427
    //   12: aload_1
    //   13: invokevirtual size : ()I
    //   16: istore #4
    //   18: iconst_0
    //   19: istore #5
    //   21: iload #5
    //   23: iload #4
    //   25: if_icmpge -> 427
    //   28: aload_1
    //   29: iload #5
    //   31: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   34: checkcast android/view/Surface
    //   37: astore #6
    //   39: aload #6
    //   41: ifnonnull -> 59
    //   44: aload_0
    //   45: getfield TAG : Ljava/lang/String;
    //   48: ldc_w 'configureOutputs - null outputs are not allowed'
    //   51: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   54: pop
    //   55: getstatic android/hardware/camera2/legacy/LegacyExceptionUtils.BAD_VALUE : I
    //   58: ireturn
    //   59: aload #6
    //   61: invokevirtual isValid : ()Z
    //   64: ifne -> 82
    //   67: aload_0
    //   68: getfield TAG : Ljava/lang/String;
    //   71: ldc_w 'configureOutputs - invalid output surfaces are not allowed'
    //   74: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   77: pop
    //   78: getstatic android/hardware/camera2/legacy/LegacyExceptionUtils.BAD_VALUE : I
    //   81: ireturn
    //   82: aload_0
    //   83: getfield mStaticCharacteristics : Landroid/hardware/camera2/CameraCharacteristics;
    //   86: astore #7
    //   88: getstatic android/hardware/camera2/CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   91: astore #8
    //   93: aload #7
    //   95: aload #8
    //   97: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   100: checkcast android/hardware/camera2/params/StreamConfigurationMap
    //   103: astore #9
    //   105: aload #6
    //   107: invokestatic getSurfaceSize : (Landroid/view/Surface;)Landroid/util/Size;
    //   110: astore #8
    //   112: aload #6
    //   114: invokestatic detectSurfaceType : (Landroid/view/Surface;)I
    //   117: istore #10
    //   119: aload #6
    //   121: invokestatic isFlexibleConsumer : (Landroid/view/Surface;)Z
    //   124: istore #11
    //   126: aload #9
    //   128: iload #10
    //   130: invokevirtual getOutputSizes : (I)[Landroid/util/Size;
    //   133: astore #12
    //   135: aload #12
    //   137: astore #7
    //   139: aload #12
    //   141: ifnonnull -> 184
    //   144: iload #10
    //   146: bipush #34
    //   148: if_icmpne -> 163
    //   151: aload #9
    //   153: bipush #35
    //   155: invokevirtual getOutputSizes : (I)[Landroid/util/Size;
    //   158: astore #7
    //   160: goto -> 184
    //   163: aload #12
    //   165: astore #7
    //   167: iload #10
    //   169: bipush #33
    //   171: if_icmpne -> 184
    //   174: aload #9
    //   176: sipush #256
    //   179: invokevirtual getOutputSizes : (I)[Landroid/util/Size;
    //   182: astore #7
    //   184: aload #7
    //   186: aload #8
    //   188: invokestatic contains : ([Ljava/lang/Object;Ljava/lang/Object;)Z
    //   191: ifne -> 362
    //   194: aload #8
    //   196: astore #12
    //   198: iload #11
    //   200: ifeq -> 251
    //   203: aload #8
    //   205: aload #7
    //   207: invokestatic findClosestSize : (Landroid/util/Size;[Landroid/util/Size;)Landroid/util/Size;
    //   210: astore #9
    //   212: aload #9
    //   214: astore #8
    //   216: aload #8
    //   218: astore #12
    //   220: aload #9
    //   222: ifnull -> 251
    //   225: new android/util/Pair
    //   228: astore #7
    //   230: aload #7
    //   232: aload #6
    //   234: aload #8
    //   236: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   239: aload_3
    //   240: aload #7
    //   242: invokeinterface add : (Ljava/lang/Object;)Z
    //   247: pop
    //   248: goto -> 385
    //   251: aload #7
    //   253: ifnonnull -> 263
    //   256: ldc_w 'format is invalid.'
    //   259: astore_1
    //   260: goto -> 294
    //   263: new java/lang/StringBuilder
    //   266: astore_1
    //   267: aload_1
    //   268: invokespecial <init> : ()V
    //   271: aload_1
    //   272: ldc_w 'size not in valid set: '
    //   275: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   278: pop
    //   279: aload_1
    //   280: aload #7
    //   282: invokestatic toString : ([Ljava/lang/Object;)Ljava/lang/String;
    //   285: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   288: pop
    //   289: aload_1
    //   290: invokevirtual toString : ()Ljava/lang/String;
    //   293: astore_1
    //   294: aload_0
    //   295: getfield TAG : Ljava/lang/String;
    //   298: astore #7
    //   300: aload #12
    //   302: invokevirtual getWidth : ()I
    //   305: istore #4
    //   307: aload #12
    //   309: invokevirtual getHeight : ()I
    //   312: istore #5
    //   314: aload #7
    //   316: ldc_w 'Surface with size (w=%d, h=%d) and format 0x%x is not valid, %s'
    //   319: iconst_4
    //   320: anewarray java/lang/Object
    //   323: dup
    //   324: iconst_0
    //   325: iload #4
    //   327: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   330: aastore
    //   331: dup
    //   332: iconst_1
    //   333: iload #5
    //   335: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   338: aastore
    //   339: dup
    //   340: iconst_2
    //   341: iload #10
    //   343: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   346: aastore
    //   347: dup
    //   348: iconst_3
    //   349: aload_1
    //   350: aastore
    //   351: invokestatic format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   354: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   357: pop
    //   358: getstatic android/hardware/camera2/legacy/LegacyExceptionUtils.BAD_VALUE : I
    //   361: ireturn
    //   362: new android/util/Pair
    //   365: astore #7
    //   367: aload #7
    //   369: aload #6
    //   371: aload #8
    //   373: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   376: aload_3
    //   377: aload #7
    //   379: invokeinterface add : (Ljava/lang/Object;)Z
    //   384: pop
    //   385: iload_2
    //   386: ifne -> 404
    //   389: aload #6
    //   391: aload #8
    //   393: invokevirtual getWidth : ()I
    //   396: aload #8
    //   398: invokevirtual getHeight : ()I
    //   401: invokestatic setSurfaceDimens : (Landroid/view/Surface;II)V
    //   404: iinc #5, 1
    //   407: goto -> 21
    //   410: astore_1
    //   411: aload_0
    //   412: getfield TAG : Ljava/lang/String;
    //   415: ldc_w 'Surface bufferqueue is abandoned, cannot configure as output: '
    //   418: aload_1
    //   419: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   422: pop
    //   423: getstatic android/hardware/camera2/legacy/LegacyExceptionUtils.BAD_VALUE : I
    //   426: ireturn
    //   427: iload_2
    //   428: ifeq -> 433
    //   431: iconst_0
    //   432: ireturn
    //   433: iconst_0
    //   434: istore_2
    //   435: aload_0
    //   436: getfield mDeviceState : Landroid/hardware/camera2/legacy/CameraDeviceState;
    //   439: invokevirtual setConfiguring : ()Z
    //   442: ifeq -> 461
    //   445: aload_0
    //   446: getfield mRequestThreadManager : Landroid/hardware/camera2/legacy/RequestThreadManager;
    //   449: aload_3
    //   450: invokevirtual configure : (Ljava/util/Collection;)V
    //   453: aload_0
    //   454: getfield mDeviceState : Landroid/hardware/camera2/legacy/CameraDeviceState;
    //   457: invokevirtual setIdle : ()Z
    //   460: istore_2
    //   461: iload_2
    //   462: ifeq -> 472
    //   465: aload_0
    //   466: aload_1
    //   467: putfield mConfiguredSurfaces : Landroid/util/SparseArray;
    //   470: iconst_0
    //   471: ireturn
    //   472: getstatic android/hardware/camera2/legacy/LegacyExceptionUtils.INVALID_OPERATION : I
    //   475: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #369	-> 0
    //   #370	-> 8
    //   #371	-> 12
    //   #372	-> 18
    //   #373	-> 28
    //   #374	-> 39
    //   #375	-> 44
    //   #376	-> 55
    //   #378	-> 59
    //   #379	-> 67
    //   #380	-> 78
    //   #382	-> 82
    //   #383	-> 93
    //   #387	-> 105
    //   #388	-> 112
    //   #390	-> 119
    //   #392	-> 126
    //   #393	-> 135
    //   #394	-> 144
    //   #399	-> 151
    //   #400	-> 163
    //   #401	-> 174
    //   #405	-> 184
    //   #406	-> 194
    //   #407	-> 225
    //   #409	-> 251
    //   #410	-> 263
    //   #411	-> 294
    //   #412	-> 300
    //   #413	-> 314
    //   #411	-> 314
    //   #414	-> 358
    //   #417	-> 362
    //   #420	-> 385
    //   #421	-> 389
    //   #426	-> 404
    //   #372	-> 404
    //   #423	-> 410
    //   #424	-> 411
    //   #425	-> 423
    //   #431	-> 427
    //   #432	-> 431
    //   #435	-> 433
    //   #436	-> 435
    //   #437	-> 445
    //   #438	-> 453
    //   #441	-> 461
    //   #442	-> 465
    //   #446	-> 470
    //   #444	-> 472
    // Exception table:
    //   from	to	target	type
    //   105	112	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   112	119	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   119	126	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   126	135	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   151	160	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   174	184	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   184	194	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   203	212	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   225	248	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   263	294	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   294	300	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   300	314	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   314	358	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   358	362	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   362	385	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
    //   389	404	410	android/hardware/camera2/legacy/LegacyExceptionUtils$BufferQueueAbandonedException
  }
  
  public SubmitInfo submitRequestList(CaptureRequest[] paramArrayOfCaptureRequest, boolean paramBoolean) {
    if (paramArrayOfCaptureRequest != null && paramArrayOfCaptureRequest.length != 0)
      try {
        List<Long> list;
        if (this.mConfiguredSurfaces == null) {
          list = new ArrayList();
        } else {
          list = getSurfaceIds(this.mConfiguredSurfaces);
        } 
        int i;
        byte b;
        for (i = paramArrayOfCaptureRequest.length, b = 0; b < i; ) {
          CaptureRequest captureRequest = paramArrayOfCaptureRequest[b];
          if (!captureRequest.getTargets().isEmpty()) {
            for (Surface surface : captureRequest.getTargets()) {
              if (surface != null) {
                if (this.mConfiguredSurfaces != null) {
                  if (containsSurfaceId(surface, list))
                    continue; 
                  Log.e(this.TAG, "submitRequestList - cannot use a surface that wasn't configured");
                  throw new ServiceSpecificException(LegacyExceptionUtils.BAD_VALUE, "submitRequestList - cannot use a surface that wasn't configured");
                } 
                Log.e(this.TAG, "submitRequestList - must configure  device with valid surfaces before submitting requests");
                throw new ServiceSpecificException(LegacyExceptionUtils.INVALID_OPERATION, "submitRequestList - must configure  device with valid surfaces before submitting requests");
              } 
              Log.e(this.TAG, "submitRequestList - Null Surface targets are not allowed");
              throw new ServiceSpecificException(LegacyExceptionUtils.BAD_VALUE, "submitRequestList - Null Surface targets are not allowed");
            } 
            b++;
          } 
          Log.e(this.TAG, "submitRequestList - Each request must have at least one Surface target");
          throw new ServiceSpecificException(LegacyExceptionUtils.BAD_VALUE, "submitRequestList - Each request must have at least one Surface target");
        } 
        this.mIdle.close();
        return this.mRequestThreadManager.submitCaptureRequests(paramArrayOfCaptureRequest, paramBoolean);
      } catch (BufferQueueAbandonedException bufferQueueAbandonedException) {
        throw new ServiceSpecificException(LegacyExceptionUtils.BAD_VALUE, "submitRequestList - configured surface is abandoned.");
      }  
    Log.e(this.TAG, "submitRequestList - Empty/null requests are not allowed");
    throw new ServiceSpecificException(LegacyExceptionUtils.BAD_VALUE, "submitRequestList - Empty/null requests are not allowed");
  }
  
  public SubmitInfo submitRequest(CaptureRequest paramCaptureRequest, boolean paramBoolean) {
    return submitRequestList(new CaptureRequest[] { paramCaptureRequest }, paramBoolean);
  }
  
  public long cancelRequest(int paramInt) {
    return this.mRequestThreadManager.cancelRepeating(paramInt);
  }
  
  public void waitUntilIdle() {
    this.mIdle.block();
  }
  
  public long flush() {
    long l = this.mRequestThreadManager.flush();
    waitUntilIdle();
    return l;
  }
  
  public void setAudioRestriction(int paramInt) {
    this.mRequestThreadManager.setAudioRestriction(paramInt);
  }
  
  public int getAudioRestriction() {
    return this.mRequestThreadManager.getAudioRestriction();
  }
  
  public boolean isClosed() {
    return this.mClosed;
  }
  
  public void close() {
    this.mRequestThreadManager.quit();
    this.mCallbackHandlerThread.quitSafely();
    this.mResultThread.quitSafely();
    try {
      this.mCallbackHandlerThread.join();
    } catch (InterruptedException interruptedException) {
      String str1 = this.TAG;
      HandlerThread handlerThread = this.mCallbackHandlerThread;
      String str2 = handlerThread.getName();
      long l = this.mCallbackHandlerThread.getId();
      Log.e(str1, String.format("Thread %s (%d) interrupted while quitting.", new Object[] { str2, Long.valueOf(l) }));
    } 
    try {
      this.mResultThread.join();
    } catch (InterruptedException interruptedException) {
      String str1 = this.TAG;
      HandlerThread handlerThread = this.mResultThread;
      String str2 = handlerThread.getName();
      long l = this.mResultThread.getId();
      Log.e(str1, String.format("Thread %s (%d) interrupted while quitting.", new Object[] { str2, Long.valueOf(l) }));
    } 
    this.mClosed = true;
  }
  
  protected void finalize() throws Throwable {
    try {
      close();
      super.finalize();
    } catch (ServiceSpecificException serviceSpecificException) {
      String str = this.TAG;
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Got error while trying to finalize, ignoring: ");
      stringBuilder.append(serviceSpecificException.getMessage());
      Log.e(str, stringBuilder.toString());
      super.finalize();
    } finally {
      Exception exception;
    } 
  }
  
  static long findEuclidDistSquare(Size paramSize1, Size paramSize2) {
    long l1 = (paramSize1.getWidth() - paramSize2.getWidth());
    long l2 = (paramSize1.getHeight() - paramSize2.getHeight());
    return l1 * l1 + l2 * l2;
  }
  
  static Size findClosestSize(Size paramSize, Size[] paramArrayOfSize) {
    // Byte code:
    //   0: aload_0
    //   1: ifnull -> 98
    //   4: aload_1
    //   5: ifnonnull -> 11
    //   8: goto -> 98
    //   11: aconst_null
    //   12: astore_2
    //   13: aload_1
    //   14: arraylength
    //   15: istore_3
    //   16: iconst_0
    //   17: istore #4
    //   19: iload #4
    //   21: iload_3
    //   22: if_icmpge -> 96
    //   25: aload_1
    //   26: iload #4
    //   28: aaload
    //   29: astore #5
    //   31: aload #5
    //   33: aload_0
    //   34: invokevirtual equals : (Ljava/lang/Object;)Z
    //   37: ifeq -> 42
    //   40: aload_0
    //   41: areturn
    //   42: aload_2
    //   43: astore #6
    //   45: aload #5
    //   47: invokevirtual getWidth : ()I
    //   50: sipush #1920
    //   53: if_icmpgt -> 87
    //   56: aload_2
    //   57: ifnull -> 83
    //   60: aload_0
    //   61: aload #5
    //   63: invokestatic findEuclidDistSquare : (Landroid/util/Size;Landroid/util/Size;)J
    //   66: lstore #7
    //   68: aload_2
    //   69: astore #6
    //   71: lload #7
    //   73: aload_2
    //   74: aload #5
    //   76: invokestatic findEuclidDistSquare : (Landroid/util/Size;Landroid/util/Size;)J
    //   79: lcmp
    //   80: ifge -> 87
    //   83: aload #5
    //   85: astore #6
    //   87: iinc #4, 1
    //   90: aload #6
    //   92: astore_2
    //   93: goto -> 19
    //   96: aload_2
    //   97: areturn
    //   98: aconst_null
    //   99: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #612	-> 0
    //   #615	-> 11
    //   #616	-> 13
    //   #617	-> 31
    //   #618	-> 40
    //   #619	-> 42
    //   #620	-> 60
    //   #621	-> 68
    //   #622	-> 83
    //   #616	-> 87
    //   #625	-> 96
    //   #613	-> 98
  }
  
  public static Size getSurfaceSize(Surface paramSurface) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    Preconditions.checkNotNull(paramSurface);
    int[] arrayOfInt = new int[2];
    LegacyExceptionUtils.throwOnError(nativeDetectSurfaceDimens(paramSurface, arrayOfInt));
    return new Size(arrayOfInt[0], arrayOfInt[1]);
  }
  
  public static boolean isFlexibleConsumer(Surface paramSurface) {
    boolean bool;
    int i = detectSurfaceUsageFlags(paramSurface);
    if ((i & 0x110000) == 0 && (i & 0x903) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isPreviewConsumer(Surface paramSurface) {
    boolean bool;
    int i = detectSurfaceUsageFlags(paramSurface);
    if ((i & 0x110003) == 0 && (i & 0xB00) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    try {
      detectSurfaceType(paramSurface);
      return bool;
    } catch (BufferQueueAbandonedException bufferQueueAbandonedException) {
      throw new IllegalArgumentException("Surface was abandoned", bufferQueueAbandonedException);
    } 
  }
  
  public static boolean isVideoEncoderConsumer(Surface paramSurface) {
    boolean bool;
    int i = detectSurfaceUsageFlags(paramSurface);
    if ((i & 0x100903) == 0 && (i & 0x10000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    try {
      detectSurfaceType(paramSurface);
      return bool;
    } catch (BufferQueueAbandonedException bufferQueueAbandonedException) {
      throw new IllegalArgumentException("Surface was abandoned", bufferQueueAbandonedException);
    } 
  }
  
  static int detectSurfaceUsageFlags(Surface paramSurface) {
    Preconditions.checkNotNull(paramSurface);
    return nativeDetectSurfaceUsageFlags(paramSurface);
  }
  
  public static int detectSurfaceType(Surface paramSurface) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    Preconditions.checkNotNull(paramSurface);
    int i = nativeDetectSurfaceType(paramSurface);
    int j = i;
    if (i >= 1) {
      j = i;
      if (i <= 5)
        j = 34; 
    } 
    return LegacyExceptionUtils.throwOnError(j);
  }
  
  public static int detectSurfaceDataspace(Surface paramSurface) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    Preconditions.checkNotNull(paramSurface);
    return LegacyExceptionUtils.throwOnError(nativeDetectSurfaceDataspace(paramSurface));
  }
  
  static void connectSurface(Surface paramSurface) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    Preconditions.checkNotNull(paramSurface);
    LegacyExceptionUtils.throwOnError(nativeConnectSurface(paramSurface));
  }
  
  static void disconnectSurface(Surface paramSurface) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    if (paramSurface == null)
      return; 
    LegacyExceptionUtils.throwOnError(nativeDisconnectSurface(paramSurface));
  }
  
  static void produceFrame(Surface paramSurface, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    Preconditions.checkNotNull(paramSurface);
    Preconditions.checkNotNull(paramArrayOfbyte);
    Preconditions.checkArgumentPositive(paramInt1, "width must be positive.");
    Preconditions.checkArgumentPositive(paramInt2, "height must be positive.");
    LegacyExceptionUtils.throwOnError(nativeProduceFrame(paramSurface, paramArrayOfbyte, paramInt1, paramInt2, paramInt3));
  }
  
  static void setSurfaceFormat(Surface paramSurface, int paramInt) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    Preconditions.checkNotNull(paramSurface);
    LegacyExceptionUtils.throwOnError(nativeSetSurfaceFormat(paramSurface, paramInt));
  }
  
  static void setSurfaceDimens(Surface paramSurface, int paramInt1, int paramInt2) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    Preconditions.checkNotNull(paramSurface);
    Preconditions.checkArgumentPositive(paramInt1, "width must be positive.");
    Preconditions.checkArgumentPositive(paramInt2, "height must be positive.");
    LegacyExceptionUtils.throwOnError(nativeSetSurfaceDimens(paramSurface, paramInt1, paramInt2));
  }
  
  public static long getSurfaceId(Surface paramSurface) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    Preconditions.checkNotNull(paramSurface);
    try {
      return nativeGetSurfaceId(paramSurface);
    } catch (IllegalArgumentException illegalArgumentException) {
      throw new LegacyExceptionUtils.BufferQueueAbandonedException();
    } 
  }
  
  static List<Long> getSurfaceIds(SparseArray<Surface> paramSparseArray) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    if (paramSparseArray != null) {
      ArrayList<Long> arrayList = new ArrayList();
      int i = paramSparseArray.size();
      for (byte b = 0; b < i; ) {
        long l = getSurfaceId((Surface)paramSparseArray.valueAt(b));
        if (l != 0L) {
          arrayList.add(Long.valueOf(l));
          b++;
        } 
        throw new IllegalStateException("Configured surface had null native GraphicBufferProducer pointer!");
      } 
      return arrayList;
    } 
    throw new NullPointerException("Null argument surfaces");
  }
  
  static List<Long> getSurfaceIds(Collection<Surface> paramCollection) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    if (paramCollection != null) {
      ArrayList<Long> arrayList = new ArrayList();
      for (Surface surface : paramCollection) {
        long l = getSurfaceId(surface);
        if (l != 0L) {
          arrayList.add(Long.valueOf(l));
          continue;
        } 
        throw new IllegalStateException("Configured surface had null native GraphicBufferProducer pointer!");
      } 
      return arrayList;
    } 
    throw new NullPointerException("Null argument surfaces");
  }
  
  static boolean containsSurfaceId(Surface paramSurface, Collection<Long> paramCollection) {
    try {
      long l = getSurfaceId(paramSurface);
      return paramCollection.contains(Long.valueOf(l));
    } catch (BufferQueueAbandonedException bufferQueueAbandonedException) {
      return false;
    } 
  }
  
  static void setSurfaceOrientation(Surface paramSurface, int paramInt1, int paramInt2) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    Preconditions.checkNotNull(paramSurface);
    LegacyExceptionUtils.throwOnError(nativeSetSurfaceOrientation(paramSurface, paramInt1, paramInt2));
  }
  
  static Size getTextureSize(SurfaceTexture paramSurfaceTexture) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    Preconditions.checkNotNull(paramSurfaceTexture);
    int[] arrayOfInt = new int[2];
    LegacyExceptionUtils.throwOnError(nativeDetectTextureDimens(paramSurfaceTexture, arrayOfInt));
    return new Size(arrayOfInt[0], arrayOfInt[1]);
  }
  
  static void setNextTimestamp(Surface paramSurface, long paramLong) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    Preconditions.checkNotNull(paramSurface);
    LegacyExceptionUtils.throwOnError(nativeSetNextTimestamp(paramSurface, paramLong));
  }
  
  static void setScalingMode(Surface paramSurface, int paramInt) throws LegacyExceptionUtils.BufferQueueAbandonedException {
    Preconditions.checkNotNull(paramSurface);
    LegacyExceptionUtils.throwOnError(nativeSetScalingMode(paramSurface, paramInt));
  }
  
  private static native int nativeConnectSurface(Surface paramSurface);
  
  private static native int nativeDetectSurfaceDataspace(Surface paramSurface);
  
  private static native int nativeDetectSurfaceDimens(Surface paramSurface, int[] paramArrayOfint);
  
  private static native int nativeDetectSurfaceType(Surface paramSurface);
  
  private static native int nativeDetectSurfaceUsageFlags(Surface paramSurface);
  
  private static native int nativeDetectTextureDimens(SurfaceTexture paramSurfaceTexture, int[] paramArrayOfint);
  
  private static native int nativeDisconnectSurface(Surface paramSurface);
  
  static native int nativeGetJpegFooterSize();
  
  private static native long nativeGetSurfaceId(Surface paramSurface);
  
  private static native int nativeProduceFrame(Surface paramSurface, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3);
  
  private static native int nativeSetNextTimestamp(Surface paramSurface, long paramLong);
  
  private static native int nativeSetScalingMode(Surface paramSurface, int paramInt);
  
  private static native int nativeSetSurfaceDimens(Surface paramSurface, int paramInt1, int paramInt2);
  
  private static native int nativeSetSurfaceFormat(Surface paramSurface, int paramInt);
  
  private static native int nativeSetSurfaceOrientation(Surface paramSurface, int paramInt1, int paramInt2);
}
