package android.hardware.camera2.legacy;

import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.MeteringRectangle;
import android.hardware.camera2.utils.ListUtils;
import android.hardware.camera2.utils.ParamsUtils;
import android.location.Location;
import android.util.Log;
import android.util.Range;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class LegacyRequestMapper {
  private static final boolean DEBUG = false;
  
  private static final byte DEFAULT_JPEG_QUALITY = 85;
  
  private static final String TAG = "LegacyRequestMapper";
  
  public static void convertRequestMetadata(LegacyRequest paramLegacyRequest) {
    // Byte code:
    //   0: aload_0
    //   1: getfield characteristics : Landroid/hardware/camera2/CameraCharacteristics;
    //   4: astore_1
    //   5: aload_0
    //   6: getfield captureRequest : Landroid/hardware/camera2/CaptureRequest;
    //   9: astore_2
    //   10: aload_0
    //   11: getfield previewSize : Landroid/util/Size;
    //   14: astore_3
    //   15: aload_0
    //   16: getfield parameters : Landroid/hardware/Camera$Parameters;
    //   19: astore #4
    //   21: aload_1
    //   22: getstatic android/hardware/camera2/CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   25: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   28: checkcast android/graphics/Rect
    //   31: astore #5
    //   33: getstatic android/hardware/camera2/CaptureRequest.SCALER_CROP_REGION : Landroid/hardware/camera2/CaptureRequest$Key;
    //   36: astore_0
    //   37: aload_2
    //   38: aload_0
    //   39: invokevirtual get : (Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;
    //   42: checkcast android/graphics/Rect
    //   45: astore_0
    //   46: getstatic android/hardware/camera2/CaptureRequest.CONTROL_ZOOM_RATIO : Landroid/hardware/camera2/CaptureRequest$Key;
    //   49: astore #6
    //   51: aload_2
    //   52: aload #6
    //   54: invokevirtual get : (Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;
    //   57: checkcast java/lang/Float
    //   60: astore #6
    //   62: aload #5
    //   64: aload_0
    //   65: aload #6
    //   67: aload_3
    //   68: aload #4
    //   70: invokestatic convertToLegacyZoom : (Landroid/graphics/Rect;Landroid/graphics/Rect;Ljava/lang/Float;Landroid/util/Size;Landroid/hardware/Camera$Parameters;)Landroid/hardware/camera2/legacy/ParameterUtils$ZoomData;
    //   73: astore_3
    //   74: aload #4
    //   76: invokevirtual isZoomSupported : ()Z
    //   79: ifeq -> 91
    //   82: aload #4
    //   84: aload_3
    //   85: getfield zoomIndex : I
    //   88: invokevirtual setZoom : (I)V
    //   91: getstatic android/hardware/camera2/CaptureRequest.COLOR_CORRECTION_ABERRATION_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   94: astore_0
    //   95: aload_2
    //   96: aload_0
    //   97: iconst_1
    //   98: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   101: invokestatic getOrDefault : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)Ljava/lang/Object;
    //   104: checkcast java/lang/Integer
    //   107: invokevirtual intValue : ()I
    //   110: istore #7
    //   112: iload #7
    //   114: iconst_1
    //   115: if_icmpeq -> 156
    //   118: iload #7
    //   120: iconst_2
    //   121: if_icmpeq -> 156
    //   124: new java/lang/StringBuilder
    //   127: dup
    //   128: invokespecial <init> : ()V
    //   131: astore_0
    //   132: aload_0
    //   133: ldc 'convertRequestToMetadata - Ignoring unsupported colorCorrection.aberrationMode = '
    //   135: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: aload_0
    //   140: iload #7
    //   142: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   145: pop
    //   146: ldc 'LegacyRequestMapper'
    //   148: aload_0
    //   149: invokevirtual toString : ()Ljava/lang/String;
    //   152: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   155: pop
    //   156: aload_2
    //   157: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_ANTIBANDING_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   160: invokevirtual get : (Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;
    //   163: checkcast java/lang/Integer
    //   166: astore_0
    //   167: aload_0
    //   168: ifnull -> 182
    //   171: aload_0
    //   172: invokevirtual intValue : ()I
    //   175: invokestatic convertAeAntiBandingModeToLegacy : (I)Ljava/lang/String;
    //   178: astore_0
    //   179: goto -> 218
    //   182: aload #4
    //   184: invokevirtual getSupportedAntibanding : ()Ljava/util/List;
    //   187: iconst_4
    //   188: anewarray java/lang/String
    //   191: dup
    //   192: iconst_0
    //   193: ldc 'auto'
    //   195: aastore
    //   196: dup
    //   197: iconst_1
    //   198: ldc 'off'
    //   200: aastore
    //   201: dup
    //   202: iconst_2
    //   203: ldc '50hz'
    //   205: aastore
    //   206: dup
    //   207: iconst_3
    //   208: ldc '60hz'
    //   210: aastore
    //   211: invokestatic listSelectFirstFrom : (Ljava/util/List;[Ljava/lang/Object;)Ljava/lang/Object;
    //   214: checkcast java/lang/String
    //   217: astore_0
    //   218: aload_0
    //   219: ifnull -> 228
    //   222: aload #4
    //   224: aload_0
    //   225: invokevirtual setAntibanding : (Ljava/lang/String;)V
    //   228: aload_2
    //   229: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_REGIONS : Landroid/hardware/camera2/CaptureRequest$Key;
    //   232: invokevirtual get : (Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;
    //   235: checkcast [Landroid/hardware/camera2/params/MeteringRectangle;
    //   238: astore_0
    //   239: aload_2
    //   240: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AWB_REGIONS : Landroid/hardware/camera2/CaptureRequest$Key;
    //   243: invokevirtual get : (Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;
    //   246: ifnull -> 258
    //   249: ldc 'LegacyRequestMapper'
    //   251: ldc_w 'convertRequestMetadata - control.awbRegions setting is not supported, ignoring value'
    //   254: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   257: pop
    //   258: aload #4
    //   260: invokevirtual getMaxNumMeteringAreas : ()I
    //   263: istore #7
    //   265: aload #5
    //   267: aload_3
    //   268: aload_0
    //   269: iload #7
    //   271: ldc_w 'AE'
    //   274: invokestatic convertMeteringRegionsToLegacy : (Landroid/graphics/Rect;Landroid/hardware/camera2/legacy/ParameterUtils$ZoomData;[Landroid/hardware/camera2/params/MeteringRectangle;ILjava/lang/String;)Ljava/util/List;
    //   277: astore_0
    //   278: iload #7
    //   280: ifle -> 289
    //   283: aload #4
    //   285: aload_0
    //   286: invokevirtual setMeteringAreas : (Ljava/util/List;)V
    //   289: aload_2
    //   290: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AF_REGIONS : Landroid/hardware/camera2/CaptureRequest$Key;
    //   293: invokevirtual get : (Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;
    //   296: checkcast [Landroid/hardware/camera2/params/MeteringRectangle;
    //   299: astore_0
    //   300: aload #4
    //   302: invokevirtual getMaxNumFocusAreas : ()I
    //   305: istore #7
    //   307: aload #5
    //   309: aload_3
    //   310: aload_0
    //   311: iload #7
    //   313: ldc_w 'AF'
    //   316: invokestatic convertMeteringRegionsToLegacy : (Landroid/graphics/Rect;Landroid/hardware/camera2/legacy/ParameterUtils$ZoomData;[Landroid/hardware/camera2/params/MeteringRectangle;ILjava/lang/String;)Ljava/util/List;
    //   319: astore_0
    //   320: iload #7
    //   322: ifle -> 331
    //   325: aload #4
    //   327: aload_0
    //   328: invokevirtual setFocusAreas : (Ljava/util/List;)V
    //   331: aload_2
    //   332: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   335: invokevirtual get : (Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;
    //   338: checkcast android/util/Range
    //   341: astore_0
    //   342: aload_0
    //   343: ifnull -> 538
    //   346: aload_0
    //   347: invokestatic convertAeFpsRangeToLegacy : (Landroid/util/Range;)[I
    //   350: astore_0
    //   351: aconst_null
    //   352: astore #5
    //   354: aload #4
    //   356: invokevirtual getSupportedPreviewFpsRange : ()Ljava/util/List;
    //   359: invokeinterface iterator : ()Ljava/util/Iterator;
    //   364: astore #6
    //   366: aload #6
    //   368: invokeinterface hasNext : ()Z
    //   373: ifeq -> 452
    //   376: aload #6
    //   378: invokeinterface next : ()Ljava/lang/Object;
    //   383: checkcast [I
    //   386: astore_3
    //   387: aload_3
    //   388: iconst_0
    //   389: iaload
    //   390: istore #7
    //   392: iload #7
    //   394: i2d
    //   395: ldc2_w 1000.0
    //   398: ddiv
    //   399: invokestatic floor : (D)D
    //   402: d2i
    //   403: istore #7
    //   405: aload_3
    //   406: iconst_1
    //   407: iaload
    //   408: i2d
    //   409: ldc2_w 1000.0
    //   412: ddiv
    //   413: invokestatic ceil : (D)D
    //   416: d2i
    //   417: istore #8
    //   419: aload_0
    //   420: iconst_0
    //   421: iaload
    //   422: iload #7
    //   424: sipush #1000
    //   427: imul
    //   428: if_icmpne -> 449
    //   431: aload_0
    //   432: iconst_1
    //   433: iaload
    //   434: iload #8
    //   436: sipush #1000
    //   439: imul
    //   440: if_icmpne -> 449
    //   443: aload_3
    //   444: astore #5
    //   446: goto -> 452
    //   449: goto -> 366
    //   452: aload #5
    //   454: ifnull -> 473
    //   457: aload #4
    //   459: aload #5
    //   461: iconst_0
    //   462: iaload
    //   463: aload #5
    //   465: iconst_1
    //   466: iaload
    //   467: invokevirtual setPreviewFpsRange : (II)V
    //   470: goto -> 538
    //   473: new java/lang/StringBuilder
    //   476: dup
    //   477: invokespecial <init> : ()V
    //   480: astore #5
    //   482: aload #5
    //   484: ldc_w 'Unsupported FPS range set ['
    //   487: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   490: pop
    //   491: aload #5
    //   493: aload_0
    //   494: iconst_0
    //   495: iaload
    //   496: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   499: pop
    //   500: aload #5
    //   502: ldc_w ','
    //   505: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   508: pop
    //   509: aload #5
    //   511: aload_0
    //   512: iconst_1
    //   513: iaload
    //   514: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   517: pop
    //   518: aload #5
    //   520: ldc_w ']'
    //   523: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   526: pop
    //   527: ldc 'LegacyRequestMapper'
    //   529: aload #5
    //   531: invokevirtual toString : ()Ljava/lang/String;
    //   534: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   537: pop
    //   538: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AE_COMPENSATION_RANGE : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   541: astore_0
    //   542: aload_1
    //   543: aload_0
    //   544: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   547: checkcast android/util/Range
    //   550: astore_0
    //   551: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION : Landroid/hardware/camera2/CaptureRequest$Key;
    //   554: astore #5
    //   556: aload_2
    //   557: aload #5
    //   559: iconst_0
    //   560: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   563: invokestatic getOrDefault : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)Ljava/lang/Object;
    //   566: checkcast java/lang/Integer
    //   569: invokevirtual intValue : ()I
    //   572: istore #8
    //   574: iload #8
    //   576: istore #7
    //   578: aload_0
    //   579: iload #8
    //   581: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   584: invokevirtual contains : (Ljava/lang/Comparable;)Z
    //   587: ifne -> 602
    //   590: ldc 'LegacyRequestMapper'
    //   592: ldc_w 'convertRequestMetadata - control.aeExposureCompensation is out of range, ignoring value'
    //   595: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   598: pop
    //   599: iconst_0
    //   600: istore #7
    //   602: aload #4
    //   604: iload #7
    //   606: invokevirtual setExposureCompensation : (I)V
    //   609: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_LOCK : Landroid/hardware/camera2/CaptureRequest$Key;
    //   612: astore_0
    //   613: aload #4
    //   615: invokevirtual isAutoExposureLockSupported : ()Z
    //   618: istore #9
    //   620: aload_2
    //   621: aload_0
    //   622: iconst_0
    //   623: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   626: iload #9
    //   628: iconst_0
    //   629: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   632: invokestatic getIfSupported : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;ZLjava/lang/Object;)Ljava/lang/Object;
    //   635: checkcast java/lang/Boolean
    //   638: astore_0
    //   639: aload_0
    //   640: ifnull -> 652
    //   643: aload #4
    //   645: aload_0
    //   646: invokevirtual booleanValue : ()Z
    //   649: invokevirtual setAutoExposureLock : (Z)V
    //   652: aload_2
    //   653: aload #4
    //   655: invokestatic mapAeAndFlashMode : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/Camera$Parameters;)V
    //   658: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AF_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   661: astore_0
    //   662: aload_2
    //   663: aload_0
    //   664: iconst_0
    //   665: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   668: invokestatic getOrDefault : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)Ljava/lang/Object;
    //   671: checkcast java/lang/Integer
    //   674: invokevirtual intValue : ()I
    //   677: istore #7
    //   679: aload #4
    //   681: invokevirtual getSupportedFocusModes : ()Ljava/util/List;
    //   684: astore_0
    //   685: iload #7
    //   687: aload_0
    //   688: invokestatic convertAfModeToLegacy : (ILjava/util/List;)Ljava/lang/String;
    //   691: astore_0
    //   692: aload_0
    //   693: ifnull -> 702
    //   696: aload #4
    //   698: aload_0
    //   699: invokevirtual setFocusMode : (Ljava/lang/String;)V
    //   702: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AWB_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   705: astore_0
    //   706: aload #4
    //   708: invokevirtual getSupportedWhiteBalance : ()Ljava/util/List;
    //   711: ifnull -> 720
    //   714: iconst_1
    //   715: istore #9
    //   717: goto -> 723
    //   720: iconst_0
    //   721: istore #9
    //   723: aload_2
    //   724: aload_0
    //   725: iconst_1
    //   726: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   729: iload #9
    //   731: iconst_1
    //   732: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   735: invokestatic getIfSupported : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;ZLjava/lang/Object;)Ljava/lang/Object;
    //   738: checkcast java/lang/Integer
    //   741: astore_0
    //   742: aload_0
    //   743: ifnull -> 760
    //   746: aload_0
    //   747: invokevirtual intValue : ()I
    //   750: invokestatic convertAwbModeToLegacy : (I)Ljava/lang/String;
    //   753: astore_0
    //   754: aload #4
    //   756: aload_0
    //   757: invokevirtual setWhiteBalance : (Ljava/lang/String;)V
    //   760: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AWB_LOCK : Landroid/hardware/camera2/CaptureRequest$Key;
    //   763: astore_0
    //   764: aload #4
    //   766: invokevirtual isAutoWhiteBalanceLockSupported : ()Z
    //   769: istore #9
    //   771: aload_2
    //   772: aload_0
    //   773: iconst_0
    //   774: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   777: iload #9
    //   779: iconst_0
    //   780: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   783: invokestatic getIfSupported : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;ZLjava/lang/Object;)Ljava/lang/Object;
    //   786: checkcast java/lang/Boolean
    //   789: astore_0
    //   790: aload_0
    //   791: ifnull -> 803
    //   794: aload #4
    //   796: aload_0
    //   797: invokevirtual booleanValue : ()Z
    //   800: invokevirtual setAutoWhiteBalanceLock : (Z)V
    //   803: getstatic android/hardware/camera2/CaptureRequest.CONTROL_CAPTURE_INTENT : Landroid/hardware/camera2/CaptureRequest$Key;
    //   806: astore_0
    //   807: aload_2
    //   808: aload_0
    //   809: iconst_1
    //   810: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   813: invokestatic getOrDefault : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)Ljava/lang/Object;
    //   816: checkcast java/lang/Integer
    //   819: invokevirtual intValue : ()I
    //   822: istore #7
    //   824: iload #7
    //   826: invokestatic filterSupportedCaptureIntent : (I)I
    //   829: istore #7
    //   831: iload #7
    //   833: iconst_3
    //   834: if_icmpeq -> 852
    //   837: iload #7
    //   839: iconst_4
    //   840: if_icmpne -> 846
    //   843: goto -> 852
    //   846: iconst_0
    //   847: istore #9
    //   849: goto -> 855
    //   852: iconst_1
    //   853: istore #9
    //   855: aload #4
    //   857: iload #9
    //   859: invokevirtual setRecordingHint : (Z)V
    //   862: getstatic android/hardware/camera2/CaptureRequest.CONTROL_VIDEO_STABILIZATION_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   865: astore_0
    //   866: aload #4
    //   868: invokevirtual isVideoStabilizationSupported : ()Z
    //   871: istore #9
    //   873: aload_2
    //   874: aload_0
    //   875: iconst_0
    //   876: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   879: iload #9
    //   881: iconst_0
    //   882: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   885: invokestatic getIfSupported : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;ZLjava/lang/Object;)Ljava/lang/Object;
    //   888: checkcast java/lang/Integer
    //   891: astore_0
    //   892: aload_0
    //   893: ifnull -> 920
    //   896: aload_0
    //   897: invokevirtual intValue : ()I
    //   900: iconst_1
    //   901: if_icmpne -> 910
    //   904: iconst_1
    //   905: istore #9
    //   907: goto -> 913
    //   910: iconst_0
    //   911: istore #9
    //   913: aload #4
    //   915: iload #9
    //   917: invokevirtual setVideoStabilization : (Z)V
    //   920: aload #4
    //   922: invokevirtual getSupportedFocusModes : ()Ljava/util/List;
    //   925: ldc_w 'infinity'
    //   928: invokestatic listContains : (Ljava/util/List;Ljava/lang/Object;)Z
    //   931: istore #9
    //   933: getstatic android/hardware/camera2/CaptureRequest.LENS_FOCUS_DISTANCE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   936: astore_0
    //   937: aload_2
    //   938: aload_0
    //   939: fconst_0
    //   940: invokestatic valueOf : (F)Ljava/lang/Float;
    //   943: iload #9
    //   945: fconst_0
    //   946: invokestatic valueOf : (F)Ljava/lang/Float;
    //   949: invokestatic getIfSupported : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;ZLjava/lang/Object;)Ljava/lang/Object;
    //   952: checkcast java/lang/Float
    //   955: astore_0
    //   956: aload_0
    //   957: ifnull -> 969
    //   960: aload_0
    //   961: invokevirtual floatValue : ()F
    //   964: fconst_0
    //   965: fcmpl
    //   966: ifeq -> 1010
    //   969: new java/lang/StringBuilder
    //   972: dup
    //   973: invokespecial <init> : ()V
    //   976: astore_0
    //   977: aload_0
    //   978: ldc_w 'convertRequestToMetadata - Ignoring android.lens.focusDistance '
    //   981: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   984: pop
    //   985: aload_0
    //   986: iload #9
    //   988: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   991: pop
    //   992: aload_0
    //   993: ldc_w ', only 0.0f is supported'
    //   996: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   999: pop
    //   1000: ldc 'LegacyRequestMapper'
    //   1002: aload_0
    //   1003: invokevirtual toString : ()Ljava/lang/String;
    //   1006: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1009: pop
    //   1010: aload #4
    //   1012: invokevirtual getSupportedSceneModes : ()Ljava/util/List;
    //   1015: ifnull -> 1180
    //   1018: getstatic android/hardware/camera2/CaptureRequest.CONTROL_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   1021: astore_0
    //   1022: aload_2
    //   1023: aload_0
    //   1024: iconst_1
    //   1025: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1028: invokestatic getOrDefault : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)Ljava/lang/Object;
    //   1031: checkcast java/lang/Integer
    //   1034: invokevirtual intValue : ()I
    //   1037: istore #7
    //   1039: iload #7
    //   1041: iconst_1
    //   1042: if_icmpeq -> 1171
    //   1045: iload #7
    //   1047: iconst_2
    //   1048: if_icmpeq -> 1098
    //   1051: new java/lang/StringBuilder
    //   1054: dup
    //   1055: invokespecial <init> : ()V
    //   1058: astore_0
    //   1059: aload_0
    //   1060: ldc_w 'Control mode '
    //   1063: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1066: pop
    //   1067: aload_0
    //   1068: iload #7
    //   1070: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1073: pop
    //   1074: aload_0
    //   1075: ldc_w ' is unsupported, defaulting to AUTO'
    //   1078: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1081: pop
    //   1082: ldc 'LegacyRequestMapper'
    //   1084: aload_0
    //   1085: invokevirtual toString : ()Ljava/lang/String;
    //   1088: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1091: pop
    //   1092: ldc 'auto'
    //   1094: astore_0
    //   1095: goto -> 1174
    //   1098: getstatic android/hardware/camera2/CaptureRequest.CONTROL_SCENE_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   1101: astore_0
    //   1102: aload_2
    //   1103: aload_0
    //   1104: iconst_0
    //   1105: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1108: invokestatic getOrDefault : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)Ljava/lang/Object;
    //   1111: checkcast java/lang/Integer
    //   1114: invokevirtual intValue : ()I
    //   1117: istore #7
    //   1119: iload #7
    //   1121: invokestatic convertSceneModeToLegacy : (I)Ljava/lang/String;
    //   1124: astore_0
    //   1125: aload_0
    //   1126: ifnull -> 1132
    //   1129: goto -> 1174
    //   1132: new java/lang/StringBuilder
    //   1135: dup
    //   1136: invokespecial <init> : ()V
    //   1139: astore_0
    //   1140: aload_0
    //   1141: ldc_w 'Skipping unknown requested scene mode: '
    //   1144: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1147: pop
    //   1148: aload_0
    //   1149: iload #7
    //   1151: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1154: pop
    //   1155: ldc 'LegacyRequestMapper'
    //   1157: aload_0
    //   1158: invokevirtual toString : ()Ljava/lang/String;
    //   1161: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1164: pop
    //   1165: ldc 'auto'
    //   1167: astore_0
    //   1168: goto -> 1174
    //   1171: ldc 'auto'
    //   1173: astore_0
    //   1174: aload #4
    //   1176: aload_0
    //   1177: invokevirtual setSceneMode : (Ljava/lang/String;)V
    //   1180: aload #4
    //   1182: invokevirtual getSupportedColorEffects : ()Ljava/util/List;
    //   1185: ifnull -> 1269
    //   1188: getstatic android/hardware/camera2/CaptureRequest.CONTROL_EFFECT_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   1191: astore_0
    //   1192: aload_2
    //   1193: aload_0
    //   1194: iconst_0
    //   1195: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1198: invokestatic getOrDefault : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)Ljava/lang/Object;
    //   1201: checkcast java/lang/Integer
    //   1204: invokevirtual intValue : ()I
    //   1207: istore #7
    //   1209: iload #7
    //   1211: invokestatic convertEffectModeToLegacy : (I)Ljava/lang/String;
    //   1214: astore_0
    //   1215: aload_0
    //   1216: ifnull -> 1228
    //   1219: aload #4
    //   1221: aload_0
    //   1222: invokevirtual setColorEffect : (Ljava/lang/String;)V
    //   1225: goto -> 1269
    //   1228: aload #4
    //   1230: ldc_w 'none'
    //   1233: invokevirtual setColorEffect : (Ljava/lang/String;)V
    //   1236: new java/lang/StringBuilder
    //   1239: dup
    //   1240: invokespecial <init> : ()V
    //   1243: astore_0
    //   1244: aload_0
    //   1245: ldc_w 'Skipping unknown requested effect mode: '
    //   1248: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1251: pop
    //   1252: aload_0
    //   1253: iload #7
    //   1255: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1258: pop
    //   1259: ldc 'LegacyRequestMapper'
    //   1261: aload_0
    //   1262: invokevirtual toString : ()Ljava/lang/String;
    //   1265: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1268: pop
    //   1269: getstatic android/hardware/camera2/CaptureRequest.SENSOR_TEST_PATTERN_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   1272: astore_0
    //   1273: aload_2
    //   1274: aload_0
    //   1275: iconst_0
    //   1276: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1279: invokestatic getOrDefault : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)Ljava/lang/Object;
    //   1282: checkcast java/lang/Integer
    //   1285: invokevirtual intValue : ()I
    //   1288: istore #7
    //   1290: iload #7
    //   1292: ifeq -> 1336
    //   1295: new java/lang/StringBuilder
    //   1298: dup
    //   1299: invokespecial <init> : ()V
    //   1302: astore_0
    //   1303: aload_0
    //   1304: ldc_w 'convertRequestToMetadata - ignoring sensor.testPatternMode '
    //   1307: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1310: pop
    //   1311: aload_0
    //   1312: iload #7
    //   1314: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1317: pop
    //   1318: aload_0
    //   1319: ldc_w '; only OFF is supported'
    //   1322: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1325: pop
    //   1326: ldc 'LegacyRequestMapper'
    //   1328: aload_0
    //   1329: invokevirtual toString : ()Ljava/lang/String;
    //   1332: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1335: pop
    //   1336: aload_2
    //   1337: getstatic android/hardware/camera2/CaptureRequest.JPEG_GPS_LOCATION : Landroid/hardware/camera2/CaptureRequest$Key;
    //   1340: invokevirtual get : (Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;
    //   1343: checkcast android/location/Location
    //   1346: astore #5
    //   1348: aload #5
    //   1350: ifnull -> 1453
    //   1353: aload #5
    //   1355: invokestatic checkForCompleteGpsData : (Landroid/location/Location;)Z
    //   1358: ifeq -> 1417
    //   1361: aload #4
    //   1363: aload #5
    //   1365: invokevirtual getAltitude : ()D
    //   1368: invokevirtual setGpsAltitude : (D)V
    //   1371: aload #4
    //   1373: aload #5
    //   1375: invokevirtual getLatitude : ()D
    //   1378: invokevirtual setGpsLatitude : (D)V
    //   1381: aload #4
    //   1383: aload #5
    //   1385: invokevirtual getLongitude : ()D
    //   1388: invokevirtual setGpsLongitude : (D)V
    //   1391: aload #4
    //   1393: aload #5
    //   1395: invokevirtual getProvider : ()Ljava/lang/String;
    //   1398: invokevirtual toUpperCase : ()Ljava/lang/String;
    //   1401: invokevirtual setGpsProcessingMethod : (Ljava/lang/String;)V
    //   1404: aload #4
    //   1406: aload #5
    //   1408: invokevirtual getTime : ()J
    //   1411: invokevirtual setGpsTimestamp : (J)V
    //   1414: goto -> 1458
    //   1417: new java/lang/StringBuilder
    //   1420: dup
    //   1421: invokespecial <init> : ()V
    //   1424: astore_0
    //   1425: aload_0
    //   1426: ldc_w 'Incomplete GPS parameters provided in location '
    //   1429: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1432: pop
    //   1433: aload_0
    //   1434: aload #5
    //   1436: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1439: pop
    //   1440: ldc 'LegacyRequestMapper'
    //   1442: aload_0
    //   1443: invokevirtual toString : ()Ljava/lang/String;
    //   1446: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1449: pop
    //   1450: goto -> 1458
    //   1453: aload #4
    //   1455: invokevirtual removeGpsData : ()V
    //   1458: aload_2
    //   1459: getstatic android/hardware/camera2/CaptureRequest.JPEG_ORIENTATION : Landroid/hardware/camera2/CaptureRequest$Key;
    //   1462: invokevirtual get : (Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;
    //   1465: checkcast java/lang/Integer
    //   1468: astore_0
    //   1469: getstatic android/hardware/camera2/CaptureRequest.JPEG_ORIENTATION : Landroid/hardware/camera2/CaptureRequest$Key;
    //   1472: astore #5
    //   1474: aload_0
    //   1475: ifnonnull -> 1484
    //   1478: iconst_0
    //   1479: istore #7
    //   1481: goto -> 1490
    //   1484: aload_0
    //   1485: invokevirtual intValue : ()I
    //   1488: istore #7
    //   1490: aload #4
    //   1492: aload_2
    //   1493: aload #5
    //   1495: iload #7
    //   1497: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1500: invokestatic getOrDefault : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)Ljava/lang/Object;
    //   1503: checkcast java/lang/Integer
    //   1506: invokevirtual intValue : ()I
    //   1509: invokevirtual setRotation : (I)V
    //   1512: getstatic android/hardware/camera2/CaptureRequest.JPEG_QUALITY : Landroid/hardware/camera2/CaptureRequest$Key;
    //   1515: astore_0
    //   1516: aload #4
    //   1518: aload_2
    //   1519: aload_0
    //   1520: bipush #85
    //   1522: invokestatic valueOf : (B)Ljava/lang/Byte;
    //   1525: invokestatic getOrDefault : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)Ljava/lang/Object;
    //   1528: checkcast java/lang/Byte
    //   1531: invokevirtual byteValue : ()B
    //   1534: sipush #255
    //   1537: iand
    //   1538: invokevirtual setJpegQuality : (I)V
    //   1541: getstatic android/hardware/camera2/CaptureRequest.JPEG_THUMBNAIL_QUALITY : Landroid/hardware/camera2/CaptureRequest$Key;
    //   1544: astore_0
    //   1545: aload #4
    //   1547: aload_2
    //   1548: aload_0
    //   1549: bipush #85
    //   1551: invokestatic valueOf : (B)Ljava/lang/Byte;
    //   1554: invokestatic getOrDefault : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)Ljava/lang/Object;
    //   1557: checkcast java/lang/Byte
    //   1560: invokevirtual byteValue : ()B
    //   1563: sipush #255
    //   1566: iand
    //   1567: invokevirtual setJpegThumbnailQuality : (I)V
    //   1570: aload #4
    //   1572: invokevirtual getSupportedJpegThumbnailSizes : ()Ljava/util/List;
    //   1575: astore #5
    //   1577: aload #5
    //   1579: ifnull -> 1725
    //   1582: aload #5
    //   1584: invokeinterface size : ()I
    //   1589: ifle -> 1725
    //   1592: aload_2
    //   1593: getstatic android/hardware/camera2/CaptureRequest.JPEG_THUMBNAIL_SIZE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   1596: invokevirtual get : (Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;
    //   1599: checkcast android/util/Size
    //   1602: astore_0
    //   1603: aload_0
    //   1604: ifnonnull -> 1613
    //   1607: iconst_0
    //   1608: istore #7
    //   1610: goto -> 1640
    //   1613: aload_0
    //   1614: invokevirtual getWidth : ()I
    //   1617: istore #8
    //   1619: aload_0
    //   1620: invokevirtual getHeight : ()I
    //   1623: istore #7
    //   1625: aload #5
    //   1627: iload #8
    //   1629: iload #7
    //   1631: invokestatic containsSize : (Ljava/util/List;II)Z
    //   1634: ifne -> 1607
    //   1637: iconst_1
    //   1638: istore #7
    //   1640: iload #7
    //   1642: ifeq -> 1690
    //   1645: new java/lang/StringBuilder
    //   1648: dup
    //   1649: invokespecial <init> : ()V
    //   1652: astore #5
    //   1654: aload #5
    //   1656: ldc_w 'Invalid JPEG thumbnail size set '
    //   1659: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1662: pop
    //   1663: aload #5
    //   1665: aload_0
    //   1666: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1669: pop
    //   1670: aload #5
    //   1672: ldc_w ', skipping thumbnail...'
    //   1675: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1678: pop
    //   1679: ldc 'LegacyRequestMapper'
    //   1681: aload #5
    //   1683: invokevirtual toString : ()Ljava/lang/String;
    //   1686: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1689: pop
    //   1690: aload_0
    //   1691: ifnull -> 1718
    //   1694: iload #7
    //   1696: ifeq -> 1702
    //   1699: goto -> 1718
    //   1702: aload #4
    //   1704: aload_0
    //   1705: invokevirtual getWidth : ()I
    //   1708: aload_0
    //   1709: invokevirtual getHeight : ()I
    //   1712: invokevirtual setJpegThumbnailSize : (II)V
    //   1715: goto -> 1725
    //   1718: aload #4
    //   1720: iconst_0
    //   1721: iconst_0
    //   1722: invokevirtual setJpegThumbnailSize : (II)V
    //   1725: getstatic android/hardware/camera2/CaptureRequest.NOISE_REDUCTION_MODE : Landroid/hardware/camera2/CaptureRequest$Key;
    //   1728: astore_0
    //   1729: aload_2
    //   1730: aload_0
    //   1731: iconst_1
    //   1732: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1735: invokestatic getOrDefault : (Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)Ljava/lang/Object;
    //   1738: checkcast java/lang/Integer
    //   1741: invokevirtual intValue : ()I
    //   1744: istore #7
    //   1746: iload #7
    //   1748: iconst_1
    //   1749: if_icmpeq -> 1791
    //   1752: iload #7
    //   1754: iconst_2
    //   1755: if_icmpeq -> 1791
    //   1758: new java/lang/StringBuilder
    //   1761: dup
    //   1762: invokespecial <init> : ()V
    //   1765: astore_0
    //   1766: aload_0
    //   1767: ldc_w 'convertRequestToMetadata - Ignoring unsupported noiseReduction.mode = '
    //   1770: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1773: pop
    //   1774: aload_0
    //   1775: iload #7
    //   1777: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   1780: pop
    //   1781: ldc 'LegacyRequestMapper'
    //   1783: aload_0
    //   1784: invokevirtual toString : ()Ljava/lang/String;
    //   1787: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1790: pop
    //   1791: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #59	-> 0
    //   #60	-> 5
    //   #61	-> 10
    //   #62	-> 15
    //   #64	-> 21
    //   #71	-> 33
    //   #72	-> 37
    //   #73	-> 51
    //   #71	-> 62
    //   #77	-> 74
    //   #78	-> 82
    //   #89	-> 91
    //   #91	-> 95
    //   #89	-> 95
    //   #93	-> 112
    //   #95	-> 124
    //   #106	-> 156
    //   #107	-> 167
    //   #108	-> 171
    //   #110	-> 182
    //   #119	-> 218
    //   #120	-> 222
    //   #131	-> 228
    //   #132	-> 239
    //   #133	-> 249
    //   #136	-> 258
    //   #137	-> 265
    //   #142	-> 278
    //   #143	-> 283
    //   #149	-> 289
    //   #150	-> 300
    //   #151	-> 307
    //   #156	-> 320
    //   #157	-> 325
    //   #163	-> 331
    //   #164	-> 342
    //   #165	-> 346
    //   #167	-> 351
    //   #168	-> 354
    //   #170	-> 387
    //   #171	-> 405
    //   #172	-> 419
    //   #173	-> 443
    //   #174	-> 446
    //   #176	-> 449
    //   #168	-> 452
    //   #177	-> 452
    //   #178	-> 457
    //   #181	-> 473
    //   #191	-> 538
    //   #192	-> 542
    //   #193	-> 551
    //   #195	-> 556
    //   #193	-> 556
    //   #197	-> 574
    //   #198	-> 590
    //   #201	-> 599
    //   #204	-> 602
    //   #209	-> 609
    //   #210	-> 613
    //   #211	-> 620
    //   #209	-> 620
    //   #213	-> 639
    //   #214	-> 643
    //   #225	-> 652
    //   #229	-> 658
    //   #230	-> 662
    //   #229	-> 662
    //   #231	-> 679
    //   #232	-> 679
    //   #231	-> 685
    //   #234	-> 692
    //   #235	-> 696
    //   #246	-> 702
    //   #247	-> 706
    //   #248	-> 706
    //   #249	-> 723
    //   #246	-> 723
    //   #251	-> 742
    //   #252	-> 742
    //   #253	-> 746
    //   #254	-> 754
    //   #265	-> 760
    //   #266	-> 764
    //   #267	-> 771
    //   #265	-> 771
    //   #269	-> 790
    //   #270	-> 794
    //   #278	-> 803
    //   #280	-> 807
    //   #278	-> 807
    //   #282	-> 824
    //   #284	-> 831
    //   #291	-> 862
    //   #292	-> 866
    //   #293	-> 866
    //   #294	-> 873
    //   #291	-> 873
    //   #296	-> 892
    //   #297	-> 896
    //   #303	-> 920
    //   #304	-> 920
    //   #306	-> 933
    //   #307	-> 937
    //   #306	-> 937
    //   #309	-> 956
    //   #310	-> 969
    //   #320	-> 1010
    //   #321	-> 1018
    //   #322	-> 1022
    //   #321	-> 1022
    //   #324	-> 1039
    //   #343	-> 1051
    //   #345	-> 1092
    //   #326	-> 1098
    //   #327	-> 1102
    //   #326	-> 1102
    //   #328	-> 1119
    //   #329	-> 1119
    //   #330	-> 1125
    //   #331	-> 1129
    //   #333	-> 1132
    //   #334	-> 1132
    //   #336	-> 1165
    //   #339	-> 1171
    //   #340	-> 1174
    //   #348	-> 1174
    //   #354	-> 1180
    //   #355	-> 1188
    //   #356	-> 1192
    //   #355	-> 1192
    //   #357	-> 1209
    //   #358	-> 1215
    //   #359	-> 1219
    //   #361	-> 1228
    //   #362	-> 1236
    //   #373	-> 1269
    //   #374	-> 1273
    //   #373	-> 1273
    //   #375	-> 1290
    //   #376	-> 1295
    //   #387	-> 1336
    //   #388	-> 1348
    //   #389	-> 1353
    //   #390	-> 1361
    //   #391	-> 1371
    //   #392	-> 1381
    //   #393	-> 1391
    //   #394	-> 1404
    //   #396	-> 1417
    //   #399	-> 1453
    //   #405	-> 1458
    //   #406	-> 1469
    //   #407	-> 1474
    //   #406	-> 1490
    //   #412	-> 1512
    //   #413	-> 1516
    //   #412	-> 1516
    //   #418	-> 1541
    //   #419	-> 1545
    //   #418	-> 1545
    //   #424	-> 1570
    //   #426	-> 1577
    //   #427	-> 1592
    //   #428	-> 1603
    //   #429	-> 1613
    //   #428	-> 1625
    //   #430	-> 1640
    //   #431	-> 1645
    //   #433	-> 1690
    //   #437	-> 1702
    //   #435	-> 1718
    //   #447	-> 1725
    //   #449	-> 1729
    //   #447	-> 1729
    //   #451	-> 1746
    //   #453	-> 1758
    //   #457	-> 1791
  }
  
  private static boolean checkForCompleteGpsData(Location paramLocation) {
    boolean bool;
    if (paramLocation != null && paramLocation.getProvider() != null && paramLocation.getTime() != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  static int filterSupportedCaptureIntent(int paramInt) {
    switch (paramInt) {
      case 5:
      case 6:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported control.captureIntent value ");
        stringBuilder.append(1);
        stringBuilder.append("; default to PREVIEW");
        Log.w("LegacyRequestMapper", stringBuilder.toString());
        break;
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
        return paramInt;
    } 
    paramInt = 1;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown control.captureIntent value ");
    stringBuilder.append(1);
    stringBuilder.append("; default to PREVIEW");
    Log.w("LegacyRequestMapper", stringBuilder.toString());
  }
  
  private static List<Camera.Area> convertMeteringRegionsToLegacy(Rect paramRect, ParameterUtils.ZoomData paramZoomData, MeteringRectangle[] paramArrayOfMeteringRectangle, int paramInt, String paramString) {
    if (paramArrayOfMeteringRectangle == null || paramInt <= 0) {
      if (paramInt > 0)
        return Arrays.asList(new Camera.Area[] { ParameterUtils.CAMERA_AREA_DEFAULT }); 
      return null;
    } 
    ArrayList<MeteringRectangle> arrayList1 = new ArrayList();
    int i;
    byte b;
    for (i = paramArrayOfMeteringRectangle.length, b = 0; b < i; ) {
      MeteringRectangle meteringRectangle = paramArrayOfMeteringRectangle[b];
      if (meteringRectangle.getMeteringWeight() != 0)
        arrayList1.add(meteringRectangle); 
      b++;
    } 
    if (arrayList1.size() == 0) {
      Log.w("LegacyRequestMapper", "Only received metering rectangles with weight 0.");
      return Arrays.asList(new Camera.Area[] { ParameterUtils.CAMERA_AREA_DEFAULT });
    } 
    i = Math.min(paramInt, arrayList1.size());
    ArrayList<Camera.Area> arrayList = new ArrayList(i);
    for (b = 0; b < i; b++) {
      MeteringRectangle meteringRectangle = arrayList1.get(b);
      ParameterUtils.MeteringData meteringData = ParameterUtils.convertMeteringRectangleToLegacy(paramRect, meteringRectangle, paramZoomData);
      arrayList.add(meteringData.meteringArea);
    } 
    if (paramInt < arrayList1.size()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("convertMeteringRegionsToLegacy - Too many requested ");
      stringBuilder.append(paramString);
      stringBuilder.append(" regions, ignoring all beyond the first ");
      stringBuilder.append(paramInt);
      Log.w("LegacyRequestMapper", stringBuilder.toString());
    } 
    return arrayList;
  }
  
  private static void mapAeAndFlashMode(CaptureRequest paramCaptureRequest, Camera.Parameters paramParameters) {
    String str1;
    int i = ((Integer)ParamsUtils.<Integer>getOrDefault(paramCaptureRequest, CaptureRequest.FLASH_MODE, Integer.valueOf(0))).intValue();
    int j = ((Integer)ParamsUtils.<Integer>getOrDefault(paramCaptureRequest, CaptureRequest.CONTROL_AE_MODE, Integer.valueOf(1))).intValue();
    List<String> list = paramParameters.getSupportedFlashModes();
    String str2 = null;
    if (ListUtils.listContains(list, "off"))
      str2 = "off"; 
    if (j == 1) {
      if (i == 2) {
        if (ListUtils.listContains(list, "torch")) {
          str1 = "torch";
        } else {
          Log.w("LegacyRequestMapper", "mapAeAndFlashMode - Ignore flash.mode == TORCH;camera does not support it");
          str1 = str2;
        } 
      } else {
        str1 = str2;
        if (i == 1)
          if (ListUtils.listContains(list, "on")) {
            str1 = "on";
          } else {
            Log.w("LegacyRequestMapper", "mapAeAndFlashMode - Ignore flash.mode == SINGLE;camera does not support it");
            str1 = str2;
          }  
      } 
    } else if (j == 3) {
      if (ListUtils.listContains(list, "on")) {
        str1 = "on";
      } else {
        Log.w("LegacyRequestMapper", "mapAeAndFlashMode - Ignore control.aeMode == ON_ALWAYS_FLASH;camera does not support it");
        str1 = str2;
      } 
    } else if (j == 2) {
      if (ListUtils.listContains(list, "auto")) {
        str1 = "auto";
      } else {
        Log.w("LegacyRequestMapper", "mapAeAndFlashMode - Ignore control.aeMode == ON_AUTO_FLASH;camera does not support it");
        str1 = str2;
      } 
    } else {
      str1 = str2;
      if (j == 4)
        if (ListUtils.listContains(list, "red-eye")) {
          str1 = "red-eye";
        } else {
          Log.w("LegacyRequestMapper", "mapAeAndFlashMode - Ignore control.aeMode == ON_AUTO_FLASH_REDEYE;camera does not support it");
          str1 = str2;
        }  
    } 
    if (str1 != null)
      paramParameters.setFlashMode(str1); 
  }
  
  private static String convertAeAntiBandingModeToLegacy(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3)
            return null; 
          return "auto";
        } 
        return "60hz";
      } 
      return "50hz";
    } 
    return "off";
  }
  
  private static int[] convertAeFpsRangeToLegacy(Range<Integer> paramRange) {
    int i = ((Integer)paramRange.getLower()).intValue();
    int j = ((Integer)paramRange.getUpper()).intValue();
    return new int[] { i * 1000, j * 1000 };
  }
  
  private static String convertAwbModeToLegacy(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("convertAwbModeToLegacy - unrecognized control.awbMode");
        stringBuilder.append(paramInt);
        Log.w("LegacyRequestMapper", stringBuilder.toString());
        return "auto";
      case 8:
        return "shade";
      case 7:
        return "twilight";
      case 6:
        return "cloudy-daylight";
      case 5:
        return "daylight";
      case 4:
        return "warm-fluorescent";
      case 3:
        return "fluorescent";
      case 2:
        return "incandescent";
      case 1:
        break;
    } 
    return "auto";
  }
  
  private static <T> T getIfSupported(CaptureRequest paramCaptureRequest, CaptureRequest.Key<T> paramKey, T paramT1, boolean paramBoolean, T paramT2) {
    paramCaptureRequest = ParamsUtils.getOrDefault(paramCaptureRequest, (CaptureRequest.Key)paramKey, (CaptureRequest)paramT1);
    if (!paramBoolean) {
      if (!Objects.equals(paramCaptureRequest, paramT2)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramKey.getName());
        stringBuilder.append(" is not supported; ignoring requested value ");
        stringBuilder.append(paramCaptureRequest);
        Log.w("LegacyRequestMapper", stringBuilder.toString());
      } 
      return null;
    } 
    return (T)paramCaptureRequest;
  }
}
