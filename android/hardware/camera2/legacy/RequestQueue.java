package android.hardware.camera2.legacy;

import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.utils.SubmitInfo;
import java.util.ArrayDeque;
import java.util.List;

public class RequestQueue {
  private BurstHolder mRepeatingRequest = null;
  
  private final ArrayDeque<BurstHolder> mRequestQueue = new ArrayDeque<>();
  
  private long mCurrentFrameNumber = 0L;
  
  private long mCurrentRepeatingFrameNumber = -1L;
  
  private int mCurrentRequestId = 0;
  
  public static final long INVALID_FRAME = -1L;
  
  private static final String TAG = "RequestQueue";
  
  private final List<Long> mJpegSurfaceIds;
  
  public final class RequestQueueEntry {
    private final BurstHolder mBurstHolder;
    
    private final Long mFrameNumber;
    
    private final boolean mQueueEmpty;
    
    final RequestQueue this$0;
    
    public BurstHolder getBurstHolder() {
      return this.mBurstHolder;
    }
    
    public Long getFrameNumber() {
      return this.mFrameNumber;
    }
    
    public boolean isQueueEmpty() {
      return this.mQueueEmpty;
    }
    
    public RequestQueueEntry(BurstHolder param1BurstHolder, Long param1Long, boolean param1Boolean) {
      this.mBurstHolder = param1BurstHolder;
      this.mFrameNumber = param1Long;
      this.mQueueEmpty = param1Boolean;
    }
  }
  
  public RequestQueue(List<Long> paramList) {
    this.mJpegSurfaceIds = paramList;
  }
  
  public RequestQueueEntry getNext() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRequestQueue : Ljava/util/ArrayDeque;
    //   6: invokevirtual poll : ()Ljava/lang/Object;
    //   9: checkcast android/hardware/camera2/legacy/BurstHolder
    //   12: astore_1
    //   13: aload_1
    //   14: ifnull -> 32
    //   17: aload_0
    //   18: getfield mRequestQueue : Ljava/util/ArrayDeque;
    //   21: invokevirtual size : ()I
    //   24: ifne -> 32
    //   27: iconst_1
    //   28: istore_2
    //   29: goto -> 34
    //   32: iconst_0
    //   33: istore_2
    //   34: aload_1
    //   35: astore_3
    //   36: aload_1
    //   37: ifnonnull -> 72
    //   40: aload_1
    //   41: astore_3
    //   42: aload_0
    //   43: getfield mRepeatingRequest : Landroid/hardware/camera2/legacy/BurstHolder;
    //   46: ifnull -> 72
    //   49: aload_0
    //   50: getfield mRepeatingRequest : Landroid/hardware/camera2/legacy/BurstHolder;
    //   53: astore_3
    //   54: aload_0
    //   55: getfield mCurrentFrameNumber : J
    //   58: lstore #4
    //   60: aload_0
    //   61: lload #4
    //   63: aload_3
    //   64: invokevirtual getNumberOfRequests : ()I
    //   67: i2l
    //   68: ladd
    //   69: putfield mCurrentRepeatingFrameNumber : J
    //   72: aload_3
    //   73: ifnonnull -> 80
    //   76: aload_0
    //   77: monitorexit
    //   78: aconst_null
    //   79: areturn
    //   80: new android/hardware/camera2/legacy/RequestQueue$RequestQueueEntry
    //   83: astore_1
    //   84: aload_1
    //   85: aload_0
    //   86: aload_3
    //   87: aload_0
    //   88: getfield mCurrentFrameNumber : J
    //   91: invokestatic valueOf : (J)Ljava/lang/Long;
    //   94: iload_2
    //   95: invokespecial <init> : (Landroid/hardware/camera2/legacy/RequestQueue;Landroid/hardware/camera2/legacy/BurstHolder;Ljava/lang/Long;Z)V
    //   98: aload_0
    //   99: aload_0
    //   100: getfield mCurrentFrameNumber : J
    //   103: aload_3
    //   104: invokevirtual getNumberOfRequests : ()I
    //   107: i2l
    //   108: ladd
    //   109: putfield mCurrentFrameNumber : J
    //   112: aload_0
    //   113: monitorexit
    //   114: aload_1
    //   115: areturn
    //   116: astore_3
    //   117: aload_0
    //   118: monitorexit
    //   119: aload_3
    //   120: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #78	-> 2
    //   #79	-> 13
    //   #80	-> 34
    //   #81	-> 49
    //   #82	-> 54
    //   #83	-> 60
    //   #86	-> 72
    //   #87	-> 76
    //   #90	-> 80
    //   #91	-> 98
    //   #92	-> 112
    //   #77	-> 116
    // Exception table:
    //   from	to	target	type
    //   2	13	116	finally
    //   17	27	116	finally
    //   42	49	116	finally
    //   49	54	116	finally
    //   54	60	116	finally
    //   60	72	116	finally
    //   80	98	116	finally
    //   98	112	116	finally
  }
  
  public long stopRepeating(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: ldc2_w -1
    //   5: lstore_2
    //   6: aload_0
    //   7: getfield mRepeatingRequest : Landroid/hardware/camera2/legacy/BurstHolder;
    //   10: ifnull -> 72
    //   13: aload_0
    //   14: getfield mRepeatingRequest : Landroid/hardware/camera2/legacy/BurstHolder;
    //   17: invokevirtual getRequestId : ()I
    //   20: iload_1
    //   21: if_icmpne -> 72
    //   24: aload_0
    //   25: aconst_null
    //   26: putfield mRepeatingRequest : Landroid/hardware/camera2/legacy/BurstHolder;
    //   29: aload_0
    //   30: getfield mCurrentRepeatingFrameNumber : J
    //   33: ldc2_w -1
    //   36: lcmp
    //   37: ifne -> 47
    //   40: ldc2_w -1
    //   43: lstore_2
    //   44: goto -> 54
    //   47: aload_0
    //   48: getfield mCurrentRepeatingFrameNumber : J
    //   51: lconst_1
    //   52: lsub
    //   53: lstore_2
    //   54: aload_0
    //   55: ldc2_w -1
    //   58: putfield mCurrentRepeatingFrameNumber : J
    //   61: ldc 'RequestQueue'
    //   63: ldc 'Repeating capture request cancelled.'
    //   65: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   68: pop
    //   69: goto -> 108
    //   72: new java/lang/StringBuilder
    //   75: astore #4
    //   77: aload #4
    //   79: invokespecial <init> : ()V
    //   82: aload #4
    //   84: ldc 'cancel failed: no repeating request exists for request id: '
    //   86: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   89: pop
    //   90: aload #4
    //   92: iload_1
    //   93: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   96: pop
    //   97: ldc 'RequestQueue'
    //   99: aload #4
    //   101: invokevirtual toString : ()Ljava/lang/String;
    //   104: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   107: pop
    //   108: aload_0
    //   109: monitorexit
    //   110: lload_2
    //   111: lreturn
    //   112: astore #4
    //   114: aload_0
    //   115: monitorexit
    //   116: aload #4
    //   118: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #103	-> 2
    //   #104	-> 6
    //   #105	-> 24
    //   #106	-> 29
    //   #107	-> 47
    //   #108	-> 54
    //   #109	-> 61
    //   #111	-> 72
    //   #113	-> 108
    //   #102	-> 112
    // Exception table:
    //   from	to	target	type
    //   6	24	112	finally
    //   24	29	112	finally
    //   29	40	112	finally
    //   47	54	112	finally
    //   54	61	112	finally
    //   61	69	112	finally
    //   72	108	112	finally
  }
  
  public long stopRepeating() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRepeatingRequest : Landroid/hardware/camera2/legacy/BurstHolder;
    //   6: ifnonnull -> 23
    //   9: ldc 'RequestQueue'
    //   11: ldc 'cancel failed: no repeating request exists.'
    //   13: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   16: pop
    //   17: aload_0
    //   18: monitorexit
    //   19: ldc2_w -1
    //   22: lreturn
    //   23: aload_0
    //   24: aload_0
    //   25: getfield mRepeatingRequest : Landroid/hardware/camera2/legacy/BurstHolder;
    //   28: invokevirtual getRequestId : ()I
    //   31: invokevirtual stopRepeating : (I)J
    //   34: lstore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: lload_1
    //   38: lreturn
    //   39: astore_3
    //   40: aload_0
    //   41: monitorexit
    //   42: aload_3
    //   43: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #123	-> 2
    //   #124	-> 9
    //   #125	-> 17
    //   #127	-> 23
    //   #122	-> 39
    // Exception table:
    //   from	to	target	type
    //   2	9	39	finally
    //   9	17	39	finally
    //   23	35	39	finally
  }
  
  public SubmitInfo submit(CaptureRequest[] paramArrayOfCaptureRequest, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mCurrentRequestId : I
    //   6: istore_3
    //   7: aload_0
    //   8: iload_3
    //   9: iconst_1
    //   10: iadd
    //   11: putfield mCurrentRequestId : I
    //   14: new android/hardware/camera2/legacy/BurstHolder
    //   17: astore #4
    //   19: aload #4
    //   21: iload_3
    //   22: iload_2
    //   23: aload_1
    //   24: aload_0
    //   25: getfield mJpegSurfaceIds : Ljava/util/List;
    //   28: invokespecial <init> : (IZ[Landroid/hardware/camera2/CaptureRequest;Ljava/util/Collection;)V
    //   31: ldc2_w -1
    //   34: lstore #5
    //   36: aload #4
    //   38: invokevirtual isRepeating : ()Z
    //   41: ifeq -> 102
    //   44: ldc 'RequestQueue'
    //   46: ldc 'Repeating capture request set.'
    //   48: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   51: pop
    //   52: aload_0
    //   53: getfield mRepeatingRequest : Landroid/hardware/camera2/legacy/BurstHolder;
    //   56: ifnull -> 86
    //   59: aload_0
    //   60: getfield mCurrentRepeatingFrameNumber : J
    //   63: ldc2_w -1
    //   66: lcmp
    //   67: ifne -> 78
    //   70: ldc2_w -1
    //   73: lstore #5
    //   75: goto -> 86
    //   78: aload_0
    //   79: getfield mCurrentRepeatingFrameNumber : J
    //   82: lconst_1
    //   83: lsub
    //   84: lstore #5
    //   86: aload_0
    //   87: ldc2_w -1
    //   90: putfield mCurrentRepeatingFrameNumber : J
    //   93: aload_0
    //   94: aload #4
    //   96: putfield mRepeatingRequest : Landroid/hardware/camera2/legacy/BurstHolder;
    //   99: goto -> 123
    //   102: aload_0
    //   103: getfield mRequestQueue : Ljava/util/ArrayDeque;
    //   106: aload #4
    //   108: invokevirtual offer : (Ljava/lang/Object;)Z
    //   111: pop
    //   112: aload_0
    //   113: aload #4
    //   115: invokevirtual getRequestId : ()I
    //   118: invokespecial calculateLastFrame : (I)J
    //   121: lstore #5
    //   123: new android/hardware/camera2/utils/SubmitInfo
    //   126: dup
    //   127: iload_3
    //   128: lload #5
    //   130: invokespecial <init> : (IJ)V
    //   133: astore_1
    //   134: aload_0
    //   135: monitorexit
    //   136: aload_1
    //   137: areturn
    //   138: astore_1
    //   139: aload_0
    //   140: monitorexit
    //   141: aload_1
    //   142: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #143	-> 2
    //   #144	-> 14
    //   #145	-> 31
    //   #146	-> 36
    //   #147	-> 44
    //   #148	-> 52
    //   #149	-> 59
    //   #150	-> 78
    //   #152	-> 86
    //   #153	-> 93
    //   #155	-> 102
    //   #156	-> 112
    //   #158	-> 123
    //   #159	-> 134
    //   #142	-> 138
    // Exception table:
    //   from	to	target	type
    //   2	14	138	finally
    //   14	31	138	finally
    //   36	44	138	finally
    //   44	52	138	finally
    //   52	59	138	finally
    //   59	70	138	finally
    //   78	86	138	finally
    //   86	93	138	finally
    //   93	99	138	finally
    //   102	112	138	finally
    //   112	123	138	finally
    //   123	134	138	finally
  }
  
  private long calculateLastFrame(int paramInt) {
    long l = this.mCurrentFrameNumber;
    for (BurstHolder burstHolder : this.mRequestQueue) {
      l += burstHolder.getNumberOfRequests();
      if (burstHolder.getRequestId() == paramInt)
        return l - 1L; 
    } 
    throw new IllegalStateException("At least one request must be in the queue to calculate frame number");
  }
}
