package android.hardware.camera2.legacy;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.utils.SizeAreaComparator;
import android.hardware.camera2.utils.SubmitInfo;
import android.os.ConditionVariable;
import android.os.Handler;
import android.util.Log;
import android.util.Pair;
import android.util.Size;
import android.view.Surface;
import com.android.internal.util.Preconditions;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class RequestThreadManager {
  private final RequestHandlerThread mRequestThread;
  
  private final RequestQueue mRequestQueue;
  
  private final Handler.Callback mRequestHandlerCb;
  
  private final FpsCounter mRequestCounter;
  
  private final ConditionVariable mReceivedJpeg;
  
  private final AtomicBoolean mQuit;
  
  private SurfaceTexture mPreviewTexture;
  
  private boolean mPreviewRunning = false;
  
  private final List<Surface> mPreviewOutputs = new ArrayList<>();
  
  private final SurfaceTexture.OnFrameAvailableListener mPreviewCallback;
  
  private final FpsCounter mPrevCounter;
  
  private Camera.Parameters mParams;
  
  private LegacyRequest mLastRequest;
  
  private final List<Long> mJpegSurfaceIds;
  
  private final Camera.ShutterCallback mJpegShutterCallback;
  
  private final Camera.PictureCallback mJpegCallback;
  
  private Size mIntermediateBufferSize;
  
  private final Object mIdleLock;
  
  private GLThreadManager mGLThreadManager;
  
  private final LegacyFocusStateMapper mFocusStateMapper;
  
  private final LegacyFaceDetectMapper mFaceDetectMapper;
  
  private final Camera.ErrorCallback mErrorCallback;
  
  private SurfaceTexture mDummyTexture;
  
  private Surface mDummySurface;
  
  private final CameraDeviceState mDeviceState;
  
  private final CameraCharacteristics mCharacteristics;
  
  private final CaptureCollector mCaptureCollector;
  
  private final int mCameraId;
  
  private Camera mCamera;
  
  private final List<Surface> mCallbackOutputs = new ArrayList<>();
  
  private final String TAG;
  
  private static final boolean VERBOSE = false;
  
  private static final boolean USE_BLOB_FORMAT_OVERRIDE = true;
  
  private static final int REQUEST_COMPLETE_TIMEOUT = 4000;
  
  private static final int PREVIEW_FRAME_TIMEOUT = 1000;
  
  private static final int MSG_SUBMIT_CAPTURE_REQUEST = 2;
  
  private static final int MSG_CONFIGURE_OUTPUTS = 1;
  
  private static final int MSG_CLEANUP = 3;
  
  private static final int MAX_IN_FLIGHT_REQUESTS = 2;
  
  private static final int JPEG_FRAME_TIMEOUT = 4000;
  
  private static final boolean DEBUG = false;
  
  private static final float ASPECT_RATIO_TOLERANCE = 0.01F;
  
  public RequestThreadManager(int paramInt, Camera paramCamera, CameraCharacteristics paramCameraCharacteristics, CameraDeviceState paramCameraDeviceState) {
    ArrayList<Long> arrayList = new ArrayList();
    this.mRequestQueue = new RequestQueue(arrayList);
    this.mLastRequest = null;
    this.mIdleLock = new Object();
    this.mPrevCounter = new FpsCounter("Incoming Preview");
    this.mRequestCounter = new FpsCounter("Incoming Requests");
    this.mQuit = new AtomicBoolean(false);
    this.mErrorCallback = (Camera.ErrorCallback)new Object(this);
    this.mReceivedJpeg = new ConditionVariable(false);
    this.mJpegCallback = (Camera.PictureCallback)new Object(this);
    this.mJpegShutterCallback = (Camera.ShutterCallback)new Object(this);
    this.mPreviewCallback = (SurfaceTexture.OnFrameAvailableListener)new Object(this);
    this.mRequestHandlerCb = (Handler.Callback)new Object(this);
    this.mCamera = (Camera)Preconditions.checkNotNull(paramCamera, "camera must not be null");
    this.mCameraId = paramInt;
    this.mCharacteristics = (CameraCharacteristics)Preconditions.checkNotNull(paramCameraCharacteristics, "characteristics must not be null");
    String str = String.format("RequestThread-%d", new Object[] { Integer.valueOf(paramInt) });
    this.TAG = str;
    this.mDeviceState = (CameraDeviceState)Preconditions.checkNotNull(paramCameraDeviceState, "deviceState must not be null");
    this.mFocusStateMapper = new LegacyFocusStateMapper(this.mCamera);
    this.mFaceDetectMapper = new LegacyFaceDetectMapper(this.mCamera, this.mCharacteristics);
    this.mCaptureCollector = new CaptureCollector(2, this.mDeviceState);
    this.mRequestThread = new RequestHandlerThread(str, this.mRequestHandlerCb);
    this.mCamera.setDetailedErrorCallback(this.mErrorCallback);
  }
  
  private static class ConfigureHolder {
    public final ConditionVariable condition;
    
    public final Collection<Pair<Surface, Size>> surfaces;
    
    public ConfigureHolder(ConditionVariable param1ConditionVariable, Collection<Pair<Surface, Size>> param1Collection) {
      this.condition = param1ConditionVariable;
      this.surfaces = param1Collection;
    }
  }
  
  public static class FpsCounter {
    private int mFrameCount = 0;
    
    private long mLastTime = 0L;
    
    private long mLastPrintTime = 0L;
    
    private double mLastFps = 0.0D;
    
    private static final long NANO_PER_SECOND = 1000000000L;
    
    private static final String TAG = "FpsCounter";
    
    private final String mStreamType;
    
    public FpsCounter(String param1String) {
      this.mStreamType = param1String;
    }
    
    public void countFrame() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: aload_0
      //   4: getfield mFrameCount : I
      //   7: iconst_1
      //   8: iadd
      //   9: putfield mFrameCount : I
      //   12: invokestatic elapsedRealtimeNanos : ()J
      //   15: lstore_1
      //   16: aload_0
      //   17: getfield mLastTime : J
      //   20: lconst_0
      //   21: lcmp
      //   22: ifne -> 30
      //   25: aload_0
      //   26: lload_1
      //   27: putfield mLastTime : J
      //   30: lload_1
      //   31: aload_0
      //   32: getfield mLastTime : J
      //   35: ldc2_w 1000000000
      //   38: ladd
      //   39: lcmp
      //   40: ifle -> 76
      //   43: aload_0
      //   44: getfield mLastTime : J
      //   47: lstore_3
      //   48: aload_0
      //   49: aload_0
      //   50: getfield mFrameCount : I
      //   53: i2d
      //   54: ldc2_w 1.0E9
      //   57: lload_1
      //   58: lload_3
      //   59: lsub
      //   60: l2d
      //   61: ddiv
      //   62: dmul
      //   63: putfield mLastFps : D
      //   66: aload_0
      //   67: iconst_0
      //   68: putfield mFrameCount : I
      //   71: aload_0
      //   72: lload_1
      //   73: putfield mLastTime : J
      //   76: aload_0
      //   77: monitorexit
      //   78: return
      //   79: astore #5
      //   81: aload_0
      //   82: monitorexit
      //   83: aload #5
      //   85: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #146	-> 2
      //   #147	-> 12
      //   #148	-> 16
      //   #149	-> 25
      //   #151	-> 30
      //   #152	-> 43
      //   #153	-> 48
      //   #154	-> 66
      //   #155	-> 71
      //   #157	-> 76
      //   #145	-> 79
      // Exception table:
      //   from	to	target	type
      //   2	12	79	finally
      //   12	16	79	finally
      //   16	25	79	finally
      //   25	30	79	finally
      //   30	43	79	finally
      //   43	48	79	finally
      //   48	66	79	finally
      //   66	71	79	finally
      //   71	76	79	finally
    }
    
    public double checkFps() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mLastFps : D
      //   6: dstore_1
      //   7: aload_0
      //   8: monitorexit
      //   9: dload_1
      //   10: dreturn
      //   11: astore_3
      //   12: aload_0
      //   13: monitorexit
      //   14: aload_3
      //   15: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #160	-> 2
      //   #160	-> 11
      // Exception table:
      //   from	to	target	type
      //   2	7	11	finally
    }
    
    public void staggeredLog() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mLastTime : J
      //   6: aload_0
      //   7: getfield mLastPrintTime : J
      //   10: ldc2_w 5000000000
      //   13: ladd
      //   14: lcmp
      //   15: ifle -> 76
      //   18: aload_0
      //   19: aload_0
      //   20: getfield mLastTime : J
      //   23: putfield mLastPrintTime : J
      //   26: new java/lang/StringBuilder
      //   29: astore_1
      //   30: aload_1
      //   31: invokespecial <init> : ()V
      //   34: aload_1
      //   35: ldc 'FPS for '
      //   37: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   40: pop
      //   41: aload_1
      //   42: aload_0
      //   43: getfield mStreamType : Ljava/lang/String;
      //   46: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   49: pop
      //   50: aload_1
      //   51: ldc ' stream: '
      //   53: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   56: pop
      //   57: aload_1
      //   58: aload_0
      //   59: getfield mLastFps : D
      //   62: invokevirtual append : (D)Ljava/lang/StringBuilder;
      //   65: pop
      //   66: ldc 'FpsCounter'
      //   68: aload_1
      //   69: invokevirtual toString : ()Ljava/lang/String;
      //   72: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
      //   75: pop
      //   76: aload_0
      //   77: monitorexit
      //   78: return
      //   79: astore_1
      //   80: aload_0
      //   81: monitorexit
      //   82: aload_1
      //   83: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #164	-> 2
      //   #165	-> 18
      //   #166	-> 26
      //   #168	-> 76
      //   #163	-> 79
      // Exception table:
      //   from	to	target	type
      //   2	18	79	finally
      //   18	26	79	finally
      //   26	76	79	finally
    }
    
    public void countAndLog() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: invokevirtual countFrame : ()V
      //   6: aload_0
      //   7: invokevirtual staggeredLog : ()V
      //   10: aload_0
      //   11: monitorexit
      //   12: return
      //   13: astore_1
      //   14: aload_0
      //   15: monitorexit
      //   16: aload_1
      //   17: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #171	-> 2
      //   #172	-> 6
      //   #173	-> 10
      //   #170	-> 13
      // Exception table:
      //   from	to	target	type
      //   2	6	13	finally
      //   6	10	13	finally
    }
  }
  
  private void createDummySurface() {
    if (this.mDummyTexture == null || this.mDummySurface == null) {
      SurfaceTexture surfaceTexture = new SurfaceTexture(0);
      surfaceTexture.setDefaultBufferSize(640, 480);
      this.mDummySurface = new Surface(this.mDummyTexture);
    } 
  }
  
  private void stopPreview() {
    if (this.mPreviewRunning) {
      this.mCamera.stopPreview();
      this.mPreviewRunning = false;
    } 
  }
  
  private void startPreview() {
    if (!this.mPreviewRunning) {
      this.mCamera.startPreview();
      this.mPreviewRunning = true;
    } 
  }
  
  private void doJpegCapturePrepare(RequestHolder paramRequestHolder) throws IOException {
    if (!this.mPreviewRunning) {
      createDummySurface();
      this.mCamera.setPreviewTexture(this.mDummyTexture);
      startPreview();
    } 
  }
  
  private void doJpegCapture(RequestHolder paramRequestHolder) {
    this.mCamera.takePicture(this.mJpegShutterCallback, null, this.mJpegCallback);
    this.mPreviewRunning = false;
  }
  
  private void doPreviewCapture(RequestHolder paramRequestHolder) throws IOException {
    if (this.mPreviewRunning)
      return; 
    SurfaceTexture surfaceTexture = this.mPreviewTexture;
    if (surfaceTexture != null) {
      int i = this.mIntermediateBufferSize.getWidth();
      Size size = this.mIntermediateBufferSize;
      int j = size.getHeight();
      surfaceTexture.setDefaultBufferSize(i, j);
      this.mCamera.setPreviewTexture(this.mPreviewTexture);
      startPreview();
      return;
    } 
    throw new IllegalStateException("Preview capture called with no preview surfaces configured.");
  }
  
  private void disconnectCallbackSurfaces() {
    for (Surface surface : this.mCallbackOutputs) {
      try {
        LegacyCameraDevice.disconnectSurface(surface);
      } catch (BufferQueueAbandonedException bufferQueueAbandonedException) {
        Log.d(this.TAG, "Surface abandoned, skipping...", (Throwable)bufferQueueAbandonedException);
      } 
    } 
  }
  
  private void configureOutputs(Collection<Pair<Surface, Size>> paramCollection) {
    try {
      stopPreview();
      try {
        this.mCamera.setPreviewTexture(null);
      } catch (IOException iOException) {
        Log.w(this.TAG, "Failed to clear prior SurfaceTexture, may cause GL deadlock: ", iOException);
      } catch (RuntimeException runtimeException) {
        Log.e(this.TAG, "Received device exception in configure call: ", runtimeException);
        this.mDeviceState.setError(1);
        return;
      } 
      GLThreadManager gLThreadManager = this.mGLThreadManager;
      if (gLThreadManager != null) {
        gLThreadManager.waitUntilStarted();
        this.mGLThreadManager.ignoreNewFrames();
        this.mGLThreadManager.waitUntilIdle();
      } 
      resetJpegSurfaceFormats(this.mCallbackOutputs);
      disconnectCallbackSurfaces();
      this.mPreviewOutputs.clear();
      this.mCallbackOutputs.clear();
      this.mJpegSurfaceIds.clear();
      this.mPreviewTexture = null;
      ArrayList<Size> arrayList1 = new ArrayList();
      ArrayList<Size> arrayList2 = new ArrayList();
      int i = ((Integer)this.mCharacteristics.<Integer>get(CameraCharacteristics.LENS_FACING)).intValue();
      int j = ((Integer)this.mCharacteristics.<Integer>get(CameraCharacteristics.SENSOR_ORIENTATION)).intValue();
      if (runtimeException != null)
        for (Pair pair : runtimeException) {
          Surface surface = (Surface)pair.first;
          Size size = (Size)pair.second;
          try {
            int k = LegacyCameraDevice.detectSurfaceType(surface);
            LegacyCameraDevice.setSurfaceOrientation(surface, i, j);
            if (k != 33) {
              LegacyCameraDevice.setScalingMode(surface, 1);
              this.mPreviewOutputs.add(surface);
              arrayList1.add(size);
              continue;
            } 
            LegacyCameraDevice.setSurfaceFormat(surface, 1);
            this.mJpegSurfaceIds.add(Long.valueOf(LegacyCameraDevice.getSurfaceId(surface)));
            this.mCallbackOutputs.add(surface);
            arrayList2.add(size);
            LegacyCameraDevice.connectSurface(surface);
          } catch (BufferQueueAbandonedException bufferQueueAbandonedException) {
            Log.w(this.TAG, "Surface abandoned, skipping...", (Throwable)bufferQueueAbandonedException);
          } 
        }  
      try {
        Camera.Parameters parameters = this.mCamera.getParameters();
        List<int[]> list = parameters.getSupportedPreviewFpsRange();
        int[] arrayOfInt = getPhotoPreviewFpsRange(list);
        this.mParams.setPreviewFpsRange(arrayOfInt[0], arrayOfInt[1]);
        Size size = calculatePictureSize(this.mCallbackOutputs, arrayList2, this.mParams);
        if (arrayList1.size() > 0) {
          Size size3 = SizeAreaComparator.findLargestByArea(arrayList1);
          Size size1 = ParameterUtils.getLargestSupportedJpegSizeByArea(this.mParams);
          if (size != null)
            size1 = size; 
          Camera.Parameters parameters2 = this.mParams;
          List<Camera.Size> list1 = parameters2.getSupportedPreviewSizes();
          list1 = (List)ParameterUtils.convertSizeList(list1);
          long l1 = size3.getHeight(), l2 = size3.getWidth();
          Size size4 = SizeAreaComparator.findLargestByArea((List)list1);
          for (Size size5 : list1) {
            long l3 = (size5.getWidth() * size5.getHeight());
            long l4 = (size4.getWidth() * size4.getHeight());
            Size size6 = size4;
            if (checkAspectRatiosMatch(size1, size5)) {
              size6 = size4;
              if (l3 < l4) {
                size6 = size4;
                if (l3 >= l1 * l2)
                  size6 = size5; 
              } 
            } 
            size4 = size6;
          } 
          this.mIntermediateBufferSize = size4;
          Camera.Parameters parameters1 = this.mParams;
          int m = size4.getWidth();
          Size size2 = this.mIntermediateBufferSize;
          int k = size2.getHeight();
          parameters1.setPreviewSize(m, k);
        } else {
          this.mIntermediateBufferSize = null;
        } 
        if (size != null) {
          String str = this.TAG;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("configureOutputs - set take picture size to ");
          stringBuilder.append(size);
          Log.i(str, stringBuilder.toString());
          Camera.Parameters parameters1 = this.mParams;
          int k = size.getWidth(), m = size.getHeight();
          parameters1.setPictureSize(k, m);
        } 
        if (this.mGLThreadManager == null) {
          GLThreadManager gLThreadManager1 = new GLThreadManager(this.mCameraId, i, this.mDeviceState);
          gLThreadManager1.start();
        } 
        this.mGLThreadManager.waitUntilStarted();
        ArrayList<Pair> arrayList = new ArrayList();
        Iterator<Size> iterator = arrayList1.iterator();
        for (Surface surface : this.mPreviewOutputs)
          arrayList.add(new Pair(surface, iterator.next())); 
        this.mGLThreadManager.setConfigurationAndWait((Collection)arrayList, this.mCaptureCollector);
        for (Surface surface : this.mPreviewOutputs) {
          try {
            LegacyCameraDevice.setSurfaceOrientation(surface, i, j);
          } catch (BufferQueueAbandonedException bufferQueueAbandonedException) {
            Log.e(this.TAG, "Surface abandoned, skipping setSurfaceOrientation()", (Throwable)bufferQueueAbandonedException);
          } 
        } 
        this.mGLThreadManager.allowNewFrames();
        SurfaceTexture surfaceTexture = this.mGLThreadManager.getCurrentSurfaceTexture();
        if (surfaceTexture != null)
          surfaceTexture.setOnFrameAvailableListener(this.mPreviewCallback); 
        try {
          this.mCamera.setParameters(this.mParams);
        } catch (RuntimeException runtimeException1) {
          Log.e(this.TAG, "Received device exception while configuring: ", runtimeException1);
          this.mDeviceState.setError(1);
        } 
        return;
      } catch (RuntimeException runtimeException1) {
        Log.e(this.TAG, "Received device exception: ", runtimeException1);
        this.mDeviceState.setError(1);
        return;
      } 
    } catch (RuntimeException runtimeException) {
      Log.e(this.TAG, "Received device exception in configure call: ", runtimeException);
      this.mDeviceState.setError(1);
      return;
    } 
  }
  
  private void resetJpegSurfaceFormats(Collection<Surface> paramCollection) {
    if (paramCollection == null)
      return; 
    for (Surface surface : paramCollection) {
      if (surface == null || !surface.isValid()) {
        Log.w(this.TAG, "Jpeg surface is invalid, skipping...");
        continue;
      } 
      try {
        LegacyCameraDevice.setSurfaceFormat(surface, 33);
      } catch (BufferQueueAbandonedException bufferQueueAbandonedException) {
        Log.w(this.TAG, "Surface abandoned, skipping...", (Throwable)bufferQueueAbandonedException);
      } 
    } 
  }
  
  private Size calculatePictureSize(List<Surface> paramList, List<Size> paramList1, Camera.Parameters paramParameters) {
    if (paramList.size() == paramList1.size()) {
      ArrayList<Size> arrayList = new ArrayList();
      Iterator<Size> iterator = paramList1.iterator();
      for (Surface surface : paramList) {
        Size size = iterator.next();
        if (!LegacyCameraDevice.containsSurfaceId(surface, this.mJpegSurfaceIds))
          continue; 
        arrayList.add(size);
      } 
      if (!arrayList.isEmpty()) {
        String str;
        int i = -1;
        int j = -1;
        for (Size size1 : arrayList) {
          if (size1.getWidth() > i)
            i = size1.getWidth(); 
          if (size1.getHeight() > j)
            j = size1.getHeight(); 
        } 
        Size size = new Size(i, j);
        List<Camera.Size> list = paramParameters.getSupportedPictureSizes();
        List<Size> list1 = ParameterUtils.convertSizeList(list);
        list = new ArrayList<>();
        for (Size size1 : list1) {
          if (size1.getWidth() >= i && size1.getHeight() >= j)
            list.add(size1); 
        } 
        if (!list.isEmpty()) {
          Size size1 = (Size)Collections.min(list, new SizeAreaComparator());
          if (!size1.equals(size)) {
            String str1 = this.TAG;
            str = String.format("configureOutputs - Will need to crop picture %s into smallest bound size %s", new Object[] { size1, size });
            Log.w(str1, str);
          } 
          return size1;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Could not find any supported JPEG sizes large enough to fit ");
        stringBuilder.append(str);
        throw new AssertionError(stringBuilder.toString());
      } 
      return null;
    } 
    throw new IllegalStateException("Input collections must be same length");
  }
  
  private static boolean checkAspectRatiosMatch(Size paramSize1, Size paramSize2) {
    boolean bool;
    float f1 = paramSize1.getWidth() / paramSize1.getHeight();
    float f2 = paramSize2.getWidth() / paramSize2.getHeight();
    if (Math.abs(f1 - f2) < 0.01F) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private int[] getPhotoPreviewFpsRange(List<int[]> paramList) {
    // Byte code:
    //   0: aload_1
    //   1: invokeinterface size : ()I
    //   6: ifne -> 22
    //   9: aload_0
    //   10: getfield TAG : Ljava/lang/String;
    //   13: ldc_w 'No supported frame rates returned!'
    //   16: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   19: pop
    //   20: aconst_null
    //   21: areturn
    //   22: iconst_0
    //   23: istore_2
    //   24: iconst_0
    //   25: istore_3
    //   26: iconst_0
    //   27: istore #4
    //   29: iconst_0
    //   30: istore #5
    //   32: aload_1
    //   33: invokeinterface iterator : ()Ljava/util/Iterator;
    //   38: astore #6
    //   40: aload #6
    //   42: invokeinterface hasNext : ()Z
    //   47: ifeq -> 140
    //   50: aload #6
    //   52: invokeinterface next : ()Ljava/lang/Object;
    //   57: checkcast [I
    //   60: astore #7
    //   62: aload #7
    //   64: iconst_0
    //   65: iaload
    //   66: istore #8
    //   68: aload #7
    //   70: iconst_1
    //   71: iaload
    //   72: istore #9
    //   74: iload #9
    //   76: iload_3
    //   77: if_icmpgt -> 112
    //   80: iload_2
    //   81: istore #10
    //   83: iload_3
    //   84: istore #11
    //   86: iload #4
    //   88: istore #12
    //   90: iload #9
    //   92: iload_3
    //   93: if_icmpne -> 124
    //   96: iload_2
    //   97: istore #10
    //   99: iload_3
    //   100: istore #11
    //   102: iload #4
    //   104: istore #12
    //   106: iload #8
    //   108: iload_2
    //   109: if_icmple -> 124
    //   112: iload #8
    //   114: istore #10
    //   116: iload #9
    //   118: istore #11
    //   120: iload #5
    //   122: istore #12
    //   124: iinc #5, 1
    //   127: iload #10
    //   129: istore_2
    //   130: iload #11
    //   132: istore_3
    //   133: iload #12
    //   135: istore #4
    //   137: goto -> 40
    //   140: aload_1
    //   141: iload #4
    //   143: invokeinterface get : (I)Ljava/lang/Object;
    //   148: checkcast [I
    //   151: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #662	-> 0
    //   #663	-> 9
    //   #664	-> 20
    //   #667	-> 22
    //   #668	-> 24
    //   #669	-> 26
    //   #670	-> 29
    //   #671	-> 32
    //   #672	-> 62
    //   #673	-> 68
    //   #674	-> 74
    //   #675	-> 112
    //   #676	-> 116
    //   #677	-> 120
    //   #679	-> 124
    //   #680	-> 127
    //   #682	-> 140
  }
  
  public void start() {
    this.mRequestThread.start();
  }
  
  public long flush() {
    Log.i(this.TAG, "Flushing all pending requests.");
    long l = this.mRequestQueue.stopRepeating();
    this.mCaptureCollector.failAll();
    return l;
  }
  
  public void quit() {
    if (!this.mQuit.getAndSet(true)) {
      Handler handler = this.mRequestThread.waitAndGetHandler();
      handler.sendMessageAtFrontOfQueue(handler.obtainMessage(3));
      this.mRequestThread.quitSafely();
      try {
        this.mRequestThread.join();
      } catch (InterruptedException interruptedException) {
        String str1 = this.TAG;
        RequestHandlerThread requestHandlerThread = this.mRequestThread;
        String str2 = requestHandlerThread.getName();
        long l = this.mRequestThread.getId();
        Log.e(str1, String.format("Thread %s (%d) interrupted while quitting.", new Object[] { str2, Long.valueOf(l) }));
      } 
    } 
  }
  
  public SubmitInfo submitCaptureRequests(CaptureRequest[] paramArrayOfCaptureRequest, boolean paramBoolean) {
    Handler handler = this.mRequestThread.waitAndGetHandler();
    synchronized (this.mIdleLock) {
      SubmitInfo submitInfo = this.mRequestQueue.submit(paramArrayOfCaptureRequest, paramBoolean);
      handler.sendEmptyMessage(2);
      return submitInfo;
    } 
  }
  
  public long cancelRepeating(int paramInt) {
    return this.mRequestQueue.stopRepeating(paramInt);
  }
  
  public void configure(Collection<Pair<Surface, Size>> paramCollection) {
    Handler handler = this.mRequestThread.waitAndGetHandler();
    ConditionVariable conditionVariable = new ConditionVariable(false);
    ConfigureHolder configureHolder = new ConfigureHolder(conditionVariable, paramCollection);
    handler.sendMessage(handler.obtainMessage(1, 0, 0, configureHolder));
    conditionVariable.block();
  }
  
  public void setAudioRestriction(int paramInt) {
    Camera camera = this.mCamera;
    if (camera != null)
      camera.setAudioRestriction(paramInt); 
    throw new IllegalStateException("Camera has been released!");
  }
  
  public int getAudioRestriction() {
    Camera camera = this.mCamera;
    if (camera != null)
      return camera.getAudioRestriction(); 
    throw new IllegalStateException("Camera has been released!");
  }
}
