package android.hardware.camera2.legacy;

import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.impl.CameraMetadataNative;
import android.hardware.camera2.params.Face;
import android.hardware.camera2.utils.ParamsUtils;
import android.util.Log;
import android.util.Size;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;

public class LegacyFaceDetectMapper {
  private static String TAG = "LegacyFaceDetectMapper";
  
  private boolean mFaceDetectEnabled = false;
  
  private boolean mFaceDetectScenePriority = false;
  
  private boolean mFaceDetectReporting = false;
  
  private final Object mLock = new Object();
  
  private static final boolean DEBUG = false;
  
  private final Camera mCamera;
  
  private final boolean mFaceDetectSupported;
  
  private Camera.Face[] mFaces;
  
  private Camera.Face[] mFacesPrev;
  
  public LegacyFaceDetectMapper(Camera paramCamera, CameraCharacteristics paramCameraCharacteristics) {
    this.mCamera = (Camera)Preconditions.checkNotNull(paramCamera, "camera must not be null");
    Preconditions.checkNotNull(paramCameraCharacteristics, "characteristics must not be null");
    CameraCharacteristics.Key<int[]> key = CameraCharacteristics.STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES;
    int[] arrayOfInt = paramCameraCharacteristics.<int[]>get((CameraCharacteristics.Key)key);
    boolean bool = ArrayUtils.contains(arrayOfInt, 1);
    if (!bool)
      return; 
    this.mCamera.setFaceDetectionListener((Camera.FaceDetectionListener)new Object(this));
  }
  
  public void processFaceDetectMode(CaptureRequest paramCaptureRequest, Camera.Parameters paramParameters) {
    boolean bool1;
    Preconditions.checkNotNull(paramCaptureRequest, "captureRequest must not be null");
    CaptureRequest.Key<Integer> key = CaptureRequest.STATISTICS_FACE_DETECT_MODE;
    boolean bool = false;
    null = Integer.valueOf(0);
    int i = ((Integer)ParamsUtils.<Integer>getOrDefault(paramCaptureRequest, key, null)).intValue();
    if (i != 0 && !this.mFaceDetectSupported) {
      Log.w(TAG, "processFaceDetectMode - Ignoring statistics.faceDetectMode; face detection is not available");
      return;
    } 
    key = CaptureRequest.CONTROL_SCENE_MODE;
    int j = ((Integer)ParamsUtils.<Integer>getOrDefault(paramCaptureRequest, key, null)).intValue();
    if (j == 1 && !this.mFaceDetectSupported) {
      Log.w(TAG, "processFaceDetectMode - ignoring control.sceneMode == FACE_PRIORITY; face detection is not available");
      return;
    } 
    if (i != 0 && i != 1) {
      if (i != 2) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("processFaceDetectMode - ignoring unknown statistics.faceDetectMode = ");
        stringBuilder.append(i);
        Log.w(str, stringBuilder.toString());
        return;
      } 
      Log.w(TAG, "processFaceDetectMode - statistics.faceDetectMode == FULL unsupported, downgrading to SIMPLE");
    } 
    if (i != 0 || j == 1) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    synchronized (this.mLock) {
      if (bool1 != this.mFaceDetectEnabled) {
        if (bool1) {
          this.mCamera.startFaceDetection();
        } else {
          this.mCamera.stopFaceDetection();
          this.mFaces = null;
        } 
        this.mFaceDetectEnabled = bool1;
        if (j == 1) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        this.mFaceDetectScenePriority = bool1;
        bool1 = bool;
        if (i != 0)
          bool1 = true; 
        this.mFaceDetectReporting = bool1;
      } 
      return;
    } 
  }
  
  public void mapResultFaces(CameraMetadataNative paramCameraMetadataNative, LegacyRequest paramLegacyRequest) {
    Preconditions.checkNotNull(paramCameraMetadataNative, "result must not be null");
    Preconditions.checkNotNull(paramLegacyRequest, "legacyRequest must not be null");
    synchronized (this.mLock) {
      boolean bool;
      Camera.Face[] arrayOfFace1;
      if (this.mFaceDetectReporting) {
        bool = true;
      } else {
        bool = false;
      } 
      if (this.mFaceDetectReporting) {
        arrayOfFace1 = this.mFaces;
      } else {
        arrayOfFace1 = null;
      } 
      boolean bool1 = this.mFaceDetectScenePriority;
      Camera.Face[] arrayOfFace2 = this.mFacesPrev;
      this.mFacesPrev = arrayOfFace1;
      CameraCharacteristics cameraCharacteristics = paramLegacyRequest.characteristics;
      CaptureRequest captureRequest = paramLegacyRequest.captureRequest;
      null = paramLegacyRequest.previewSize;
      Camera.Parameters parameters = paramLegacyRequest.parameters;
      Rect rect1 = cameraCharacteristics.<Rect>get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
      CaptureRequest.Key<Rect> key = CaptureRequest.SCALER_CROP_REGION;
      Rect rect2 = captureRequest.<Rect>get(key);
      CaptureRequest.Key<Float> key1 = CaptureRequest.CONTROL_ZOOM_RATIO;
      Float float_ = captureRequest.<Float>get(key1);
      ParameterUtils.ZoomData zoomData = ParameterUtils.convertToLegacyZoom(rect1, rect2, float_, (Size)null, parameters);
      null = new ArrayList();
      if (arrayOfFace1 != null) {
        int i;
        byte b;
        for (i = arrayOfFace1.length, b = 0; b < i; ) {
          Camera.Face face = arrayOfFace1[b];
          if (face != null) {
            Face face1 = ParameterUtils.convertFaceFromLegacy(face, rect1, zoomData);
            null.add(face1);
          } else {
            Log.w(TAG, "mapResultFaces - read NULL face from camera1 device");
          } 
          b++;
        } 
      } 
      paramCameraMetadataNative.set((CaptureResult.Key)CaptureResult.STATISTICS_FACES, (Face[])null.toArray((Object[])new Face[0]));
      paramCameraMetadataNative.set(CaptureResult.STATISTICS_FACE_DETECT_MODE, Integer.valueOf(bool));
      if (bool1)
        paramCameraMetadataNative.set(CaptureResult.CONTROL_SCENE_MODE, Integer.valueOf(1)); 
      return;
    } 
  }
}
