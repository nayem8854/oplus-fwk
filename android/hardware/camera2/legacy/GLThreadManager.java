package android.hardware.camera2.legacy;

import android.graphics.SurfaceTexture;
import android.os.ConditionVariable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.Pair;
import android.util.Size;
import android.view.Surface;
import com.android.internal.util.Preconditions;
import java.util.Collection;

public class GLThreadManager {
  private final SurfaceTextureRenderer mTextureRenderer;
  
  private final RequestThreadManager.FpsCounter mPrevCounter = new RequestThreadManager.FpsCounter("GL Preview Producer");
  
  private final RequestHandlerThread mGLHandlerThread;
  
  private static class ConfigureHolder {
    public final CaptureCollector collector;
    
    public final ConditionVariable condition;
    
    public final Collection<Pair<Surface, Size>> surfaces;
    
    public ConfigureHolder(ConditionVariable param1ConditionVariable, Collection<Pair<Surface, Size>> param1Collection, CaptureCollector param1CaptureCollector) {
      this.condition = param1ConditionVariable;
      this.surfaces = param1Collection;
      this.collector = param1CaptureCollector;
    }
  }
  
  private final Handler.Callback mGLHandlerCb = (Handler.Callback)new Object(this);
  
  private final CameraDeviceState mDeviceState;
  
  private CaptureCollector mCaptureCollector;
  
  private final String TAG;
  
  private static final int MSG_NEW_FRAME = 2;
  
  private static final int MSG_NEW_CONFIGURATION = 1;
  
  private static final int MSG_DROP_FRAMES = 4;
  
  private static final int MSG_CLEANUP = 3;
  
  private static final int MSG_ALLOW_FRAMES = 5;
  
  private static final boolean DEBUG = false;
  
  public GLThreadManager(int paramInt1, int paramInt2, CameraDeviceState paramCameraDeviceState) {
    this.mTextureRenderer = new SurfaceTextureRenderer(paramInt2);
    this.TAG = String.format("CameraDeviceGLThread-%d", new Object[] { Integer.valueOf(paramInt1) });
    this.mGLHandlerThread = new RequestHandlerThread(this.TAG, this.mGLHandlerCb);
    this.mDeviceState = paramCameraDeviceState;
  }
  
  public void start() {
    this.mGLHandlerThread.start();
  }
  
  public void waitUntilStarted() {
    this.mGLHandlerThread.waitUntilStarted();
  }
  
  public void quit() {
    Handler handler = this.mGLHandlerThread.getHandler();
    handler.sendMessageAtFrontOfQueue(handler.obtainMessage(3));
    this.mGLHandlerThread.quitSafely();
    try {
      this.mGLHandlerThread.join();
    } catch (InterruptedException interruptedException) {
      String str1 = this.TAG;
      RequestHandlerThread requestHandlerThread = this.mGLHandlerThread;
      String str2 = requestHandlerThread.getName();
      long l = this.mGLHandlerThread.getId();
      Log.e(str1, String.format("Thread %s (%d) interrupted while quitting.", new Object[] { str2, Long.valueOf(l) }));
    } 
  }
  
  public void queueNewFrame() {
    Handler handler = this.mGLHandlerThread.getHandler();
    if (!handler.hasMessages(2)) {
      handler.sendMessage(handler.obtainMessage(2));
    } else {
      Log.e(this.TAG, "GLThread dropping frame.  Not consuming frames quickly enough!");
    } 
  }
  
  public void setConfigurationAndWait(Collection<Pair<Surface, Size>> paramCollection, CaptureCollector paramCaptureCollector) {
    Preconditions.checkNotNull(paramCaptureCollector, "collector must not be null");
    Handler handler = this.mGLHandlerThread.getHandler();
    ConditionVariable conditionVariable = new ConditionVariable(false);
    ConfigureHolder configureHolder = new ConfigureHolder(conditionVariable, paramCollection, paramCaptureCollector);
    Message message = handler.obtainMessage(1, 0, 0, configureHolder);
    handler.sendMessage(message);
    conditionVariable.block();
  }
  
  public SurfaceTexture getCurrentSurfaceTexture() {
    return this.mTextureRenderer.getSurfaceTexture();
  }
  
  public void ignoreNewFrames() {
    this.mGLHandlerThread.getHandler().sendEmptyMessage(4);
  }
  
  public void waitUntilIdle() {
    this.mGLHandlerThread.waitUntilIdle();
  }
  
  public void allowNewFrames() {
    this.mGLHandlerThread.getHandler().sendEmptyMessage(5);
  }
}
