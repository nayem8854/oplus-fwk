package android.hardware.camera2.legacy;

import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.impl.CameraMetadataNative;
import android.hardware.camera2.params.MeteringRectangle;
import android.hardware.camera2.utils.ParamsUtils;
import android.location.Location;
import android.util.Log;
import android.util.Size;
import java.util.ArrayList;
import java.util.List;

public class LegacyResultMapper {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "LegacyResultMapper";
  
  private LegacyRequest mCachedRequest = null;
  
  private CameraMetadataNative mCachedResult = null;
  
  public CameraMetadataNative cachedConvertResultMetadata(LegacyRequest paramLegacyRequest, long paramLong) {
    if (this.mCachedRequest != null) {
      Camera.Parameters parameters1 = paramLegacyRequest.parameters, parameters2 = this.mCachedRequest.parameters;
      if (parameters1.same(parameters2)) {
        CaptureRequest captureRequest2 = paramLegacyRequest.captureRequest, captureRequest1 = this.mCachedRequest.captureRequest;
        if (captureRequest2.equals(captureRequest1)) {
          cameraMetadataNative1 = new CameraMetadataNative(this.mCachedResult);
          cameraMetadataNative1.set(CaptureResult.SENSOR_TIMESTAMP, Long.valueOf(paramLong));
          return cameraMetadataNative1;
        } 
      } 
    } 
    CameraMetadataNative cameraMetadataNative2 = convertResultMetadata((LegacyRequest)cameraMetadataNative1);
    this.mCachedRequest = (LegacyRequest)cameraMetadataNative1;
    this.mCachedResult = new CameraMetadataNative(cameraMetadataNative2);
    CameraMetadataNative cameraMetadataNative1 = cameraMetadataNative2;
    cameraMetadataNative1.set(CaptureResult.SENSOR_TIMESTAMP, Long.valueOf(paramLong));
    return cameraMetadataNative1;
  }
  
  private static CameraMetadataNative convertResultMetadata(LegacyRequest paramLegacyRequest) {
    CameraCharacteristics cameraCharacteristics = paramLegacyRequest.characteristics;
    CaptureRequest captureRequest = paramLegacyRequest.captureRequest;
    Size size = paramLegacyRequest.previewSize;
    Camera.Parameters parameters = paramLegacyRequest.parameters;
    CameraMetadataNative cameraMetadataNative = new CameraMetadataNative();
    Rect rect1 = cameraCharacteristics.<Rect>get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
    CaptureRequest.Key<Rect> key4 = CaptureRequest.SCALER_CROP_REGION;
    Rect rect2 = captureRequest.<Rect>get(key4);
    CaptureRequest.Key<Float> key6 = CaptureRequest.CONTROL_ZOOM_RATIO;
    Float float_ = captureRequest.<Float>get(key6);
    ParameterUtils.ZoomData zoomData = ParameterUtils.convertToLegacyZoom(rect1, rect2, float_, size, parameters);
    CaptureResult.Key<Integer> key3 = CaptureResult.COLOR_CORRECTION_ABERRATION_MODE;
    CaptureRequest.Key<Integer> key5 = CaptureRequest.COLOR_CORRECTION_ABERRATION_MODE;
    Integer integer2 = captureRequest.<Integer>get(key5);
    cameraMetadataNative.set(key3, integer2);
    mapAe(cameraMetadataNative, cameraCharacteristics, captureRequest, rect1, zoomData, parameters);
    mapAf(cameraMetadataNative, rect1, zoomData, parameters);
    mapAwb(cameraMetadataNative, parameters);
    CaptureRequest.Key<Integer> key2 = CaptureRequest.CONTROL_CAPTURE_INTENT;
    boolean bool = true;
    Integer integer1 = Integer.valueOf(1);
    int i = ((Integer)ParamsUtils.<Integer>getOrDefault(captureRequest, key2, integer1)).intValue();
    i = LegacyRequestMapper.filterSupportedCaptureIntent(i);
    cameraMetadataNative.set(CaptureResult.CONTROL_CAPTURE_INTENT, Integer.valueOf(i));
    key2 = CaptureRequest.CONTROL_MODE;
    i = ((Integer)ParamsUtils.<Integer>getOrDefault(captureRequest, key2, integer1)).intValue();
    if (i == 2) {
      cameraMetadataNative.set(CaptureResult.CONTROL_MODE, Integer.valueOf(2));
    } else {
      cameraMetadataNative.set(CaptureResult.CONTROL_MODE, integer1);
    } 
    String str1 = parameters.getSceneMode();
    i = LegacyMetadataMapper.convertSceneModeFromLegacy(str1);
    if (i != -1) {
      cameraMetadataNative.set(CaptureResult.CONTROL_SCENE_MODE, Integer.valueOf(i));
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown scene mode ");
      stringBuilder.append(str1);
      stringBuilder.append(" returned by camera HAL, setting to disabled.");
      Log.w("LegacyResultMapper", stringBuilder.toString());
      cameraMetadataNative.set(CaptureResult.CONTROL_SCENE_MODE, Integer.valueOf(0));
    } 
    String str2 = parameters.getColorEffect();
    i = LegacyMetadataMapper.convertEffectModeFromLegacy(str2);
    if (i != -1) {
      cameraMetadataNative.set(CaptureResult.CONTROL_EFFECT_MODE, Integer.valueOf(i));
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown effect mode ");
      stringBuilder.append(str2);
      stringBuilder.append(" returned by camera HAL, setting to off.");
      Log.w("LegacyResultMapper", stringBuilder.toString());
      cameraMetadataNative.set(CaptureResult.CONTROL_EFFECT_MODE, Integer.valueOf(0));
    } 
    if (!parameters.isVideoStabilizationSupported() || !parameters.getVideoStabilization())
      bool = false; 
    cameraMetadataNative.set(CaptureResult.CONTROL_VIDEO_STABILIZATION_MODE, Integer.valueOf(bool));
    if ("infinity".equals(parameters.getFocusMode()))
      cameraMetadataNative.set(CaptureResult.LENS_FOCUS_DISTANCE, Float.valueOf(0.0F)); 
    cameraMetadataNative.set(CaptureResult.LENS_FOCAL_LENGTH, Float.valueOf(parameters.getFocalLength()));
    CaptureResult.Key<Byte> key = CaptureResult.REQUEST_PIPELINE_DEPTH;
    CameraCharacteristics.Key<Byte> key1 = CameraCharacteristics.REQUEST_PIPELINE_MAX_DEPTH;
    Byte byte_ = cameraCharacteristics.<Byte>get(key1);
    cameraMetadataNative.set(key, byte_);
    mapScaler(cameraMetadataNative, zoomData, parameters);
    cameraMetadataNative.set(CaptureResult.SENSOR_TEST_PATTERN_MODE, Integer.valueOf(0));
    cameraMetadataNative.set(CaptureResult.JPEG_GPS_LOCATION, captureRequest.<Location>get(CaptureRequest.JPEG_GPS_LOCATION));
    cameraMetadataNative.set(CaptureResult.JPEG_ORIENTATION, captureRequest.<Integer>get(CaptureRequest.JPEG_ORIENTATION));
    cameraMetadataNative.set(CaptureResult.JPEG_QUALITY, Byte.valueOf((byte)parameters.getJpegQuality()));
    cameraMetadataNative.set(CaptureResult.JPEG_THUMBNAIL_QUALITY, Byte.valueOf((byte)parameters.getJpegThumbnailQuality()));
    Camera.Size size1 = parameters.getJpegThumbnailSize();
    if (size1 != null) {
      cameraMetadataNative.set(CaptureResult.JPEG_THUMBNAIL_SIZE, ParameterUtils.convertSize(size1));
    } else {
      Log.w("LegacyResultMapper", "Null thumbnail size received from parameters.");
    } 
    cameraMetadataNative.set(CaptureResult.NOISE_REDUCTION_MODE, captureRequest.<Integer>get(CaptureRequest.NOISE_REDUCTION_MODE));
    return cameraMetadataNative;
  }
  
  private static void mapAe(CameraMetadataNative paramCameraMetadataNative, CameraCharacteristics paramCameraCharacteristics, CaptureRequest paramCaptureRequest, Rect paramRect, ParameterUtils.ZoomData paramZoomData, Camera.Parameters paramParameters) {
    boolean bool1;
    String str = paramParameters.getAntibanding();
    int i = LegacyMetadataMapper.convertAntiBandingModeOrDefault(str);
    paramCameraMetadataNative.set(CaptureResult.CONTROL_AE_ANTIBANDING_MODE, Integer.valueOf(i));
    paramCameraMetadataNative.set(CaptureResult.CONTROL_AE_EXPOSURE_COMPENSATION, Integer.valueOf(paramParameters.getExposureCompensation()));
    if (paramParameters.isAutoExposureLockSupported()) {
      bool1 = paramParameters.getAutoExposureLock();
    } else {
      bool1 = false;
    } 
    paramCameraMetadataNative.set(CaptureResult.CONTROL_AE_LOCK, Boolean.valueOf(bool1));
    Boolean bool = paramCaptureRequest.<Boolean>get(CaptureRequest.CONTROL_AE_LOCK);
    if (bool != null && bool.booleanValue() != bool1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("mapAe - android.control.aeLock was requested to ");
      stringBuilder.append(bool);
      stringBuilder.append(" but resulted in ");
      stringBuilder.append(bool1);
      Log.w("LegacyResultMapper", stringBuilder.toString());
    } 
    mapAeAndFlashMode(paramCameraMetadataNative, paramCameraCharacteristics, paramParameters);
    if (paramParameters.getMaxNumMeteringAreas() > 0) {
      List<Camera.Area> list = paramParameters.getMeteringAreas();
      MeteringRectangle[] arrayOfMeteringRectangle = getMeteringRectangles(paramRect, paramZoomData, list, "AE");
      paramCameraMetadataNative.set((CaptureResult.Key)CaptureResult.CONTROL_AE_REGIONS, arrayOfMeteringRectangle);
    } 
  }
  
  private static void mapAf(CameraMetadataNative paramCameraMetadataNative, Rect paramRect, ParameterUtils.ZoomData paramZoomData, Camera.Parameters paramParameters) {
    paramCameraMetadataNative.set(CaptureResult.CONTROL_AF_MODE, Integer.valueOf(convertLegacyAfMode(paramParameters.getFocusMode())));
    if (paramParameters.getMaxNumFocusAreas() > 0) {
      List<Camera.Area> list = paramParameters.getFocusAreas();
      MeteringRectangle[] arrayOfMeteringRectangle = getMeteringRectangles(paramRect, paramZoomData, list, "AF");
      paramCameraMetadataNative.set((CaptureResult.Key)CaptureResult.CONTROL_AF_REGIONS, arrayOfMeteringRectangle);
    } 
  }
  
  private static void mapAwb(CameraMetadataNative paramCameraMetadataNative, Camera.Parameters paramParameters) {
    boolean bool;
    if (paramParameters.isAutoWhiteBalanceLockSupported()) {
      bool = paramParameters.getAutoWhiteBalanceLock();
    } else {
      bool = false;
    } 
    paramCameraMetadataNative.set(CaptureResult.CONTROL_AWB_LOCK, Boolean.valueOf(bool));
    int i = convertLegacyAwbMode(paramParameters.getWhiteBalance());
    paramCameraMetadataNative.set(CaptureResult.CONTROL_AWB_MODE, Integer.valueOf(i));
  }
  
  private static MeteringRectangle[] getMeteringRectangles(Rect paramRect, ParameterUtils.ZoomData paramZoomData, List<Camera.Area> paramList, String paramString) {
    ArrayList<MeteringRectangle> arrayList = new ArrayList();
    if (paramList != null)
      for (Camera.Area area : paramList) {
        ParameterUtils.WeightedRectangle weightedRectangle = ParameterUtils.convertCameraAreaToActiveArrayRectangle(paramRect, paramZoomData, area);
        arrayList.add(weightedRectangle.toMetering());
      }  
    return arrayList.<MeteringRectangle>toArray(new MeteringRectangle[0]);
  }
  
  private static void mapAeAndFlashMode(CameraMetadataNative paramCameraMetadataNative, CameraCharacteristics paramCameraCharacteristics, Camera.Parameters paramParameters) {
    Integer integer1;
    byte b1 = 0;
    boolean bool = ((Boolean)paramCameraCharacteristics.<Boolean>get(CameraCharacteristics.FLASH_INFO_AVAILABLE)).booleanValue();
    byte b = 0;
    if (bool) {
      paramCameraCharacteristics = null;
    } else {
      integer1 = Integer.valueOf(0);
    } 
    byte b2 = 1;
    String str = paramParameters.getFlashMode();
    byte b3 = b1;
    Integer integer2 = integer1;
    byte b4 = b2;
    if (str != null) {
      switch (str.hashCode()) {
        default:
          b = -1;
          break;
        case 1081542389:
          if (str.equals("red-eye")) {
            b = 3;
            break;
          } 
        case 110547964:
          if (str.equals("torch")) {
            b = 4;
            break;
          } 
        case 3005871:
          if (str.equals("auto")) {
            b = 1;
            break;
          } 
        case 109935:
          if (str.equals("off"))
            break; 
        case 3551:
          if (str.equals("on")) {
            b = 2;
            break;
          } 
      } 
      b3 = b1;
      integer2 = integer1;
      b4 = b2;
      if (b != 0)
        if (b != 1) {
          if (b != 2) {
            if (b != 3) {
              if (b != 4) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("mapAeAndFlashMode - Ignoring unknown flash mode ");
                stringBuilder.append(paramParameters.getFlashMode());
                String str1 = stringBuilder.toString();
                Log.w("LegacyResultMapper", str1);
                b3 = b1;
                Integer integer = integer1;
                b4 = b2;
              } else {
                b3 = 2;
                integer2 = Integer.valueOf(3);
                b4 = b2;
              } 
            } else {
              b4 = 4;
              b3 = b1;
              integer2 = integer1;
            } 
          } else {
            b3 = 1;
            b4 = 3;
            integer2 = Integer.valueOf(3);
          } 
        } else {
          b4 = 2;
          integer2 = integer1;
          b3 = b1;
        }  
    } 
    paramCameraMetadataNative.set(CaptureResult.FLASH_STATE, integer2);
    paramCameraMetadataNative.set(CaptureResult.FLASH_MODE, Integer.valueOf(b3));
    paramCameraMetadataNative.set(CaptureResult.CONTROL_AE_MODE, Integer.valueOf(b4));
  }
  
  private static int convertLegacyAfMode(String paramString) {
    StringBuilder stringBuilder;
    if (paramString == null) {
      Log.w("LegacyResultMapper", "convertLegacyAfMode - no AF mode, default to OFF");
      return 0;
    } 
    byte b = -1;
    switch (paramString.hashCode()) {
      case 910005312:
        if (paramString.equals("continuous-picture"))
          b = 1; 
        break;
      case 173173288:
        if (paramString.equals("infinity"))
          b = 6; 
        break;
      case 103652300:
        if (paramString.equals("macro"))
          b = 4; 
        break;
      case 97445748:
        if (paramString.equals("fixed"))
          b = 5; 
        break;
      case 3108534:
        if (paramString.equals("edof"))
          b = 3; 
        break;
      case 3005871:
        if (paramString.equals("auto"))
          b = 0; 
        break;
      case -194628547:
        if (paramString.equals("continuous-video"))
          b = 2; 
        break;
    } 
    switch (b) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("convertLegacyAfMode - unknown mode ");
        stringBuilder.append(paramString);
        stringBuilder.append(" , ignoring");
        Log.w("LegacyResultMapper", stringBuilder.toString());
        return 0;
      case 6:
        return 0;
      case 5:
        return 0;
      case 4:
        return 2;
      case 3:
        return 5;
      case 2:
        return 3;
      case 1:
        return 4;
      case 0:
        break;
    } 
    return 1;
  }
  
  private static int convertLegacyAwbMode(String paramString) {
    StringBuilder stringBuilder;
    if (paramString == null)
      return 1; 
    byte b = -1;
    switch (paramString.hashCode()) {
      case 1942983418:
        if (paramString.equals("daylight"))
          b = 4; 
        break;
      case 1902580840:
        if (paramString.equals("fluorescent"))
          b = 2; 
        break;
      case 1650323088:
        if (paramString.equals("twilight"))
          b = 6; 
        break;
      case 474934723:
        if (paramString.equals("cloudy-daylight"))
          b = 5; 
        break;
      case 109399597:
        if (paramString.equals("shade"))
          b = 7; 
        break;
      case 3005871:
        if (paramString.equals("auto"))
          b = 0; 
        break;
      case -719316704:
        if (paramString.equals("warm-fluorescent"))
          b = 3; 
        break;
      case -939299377:
        if (paramString.equals("incandescent"))
          b = 1; 
        break;
    } 
    switch (b) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("convertAwbMode - unrecognized WB mode ");
        stringBuilder.append(paramString);
        Log.w("LegacyResultMapper", stringBuilder.toString());
        return 1;
      case 7:
        return 8;
      case 6:
        return 7;
      case 5:
        return 6;
      case 4:
        return 5;
      case 3:
        return 4;
      case 2:
        return 3;
      case 1:
        return 2;
      case 0:
        break;
    } 
    return 1;
  }
  
  private static void mapScaler(CameraMetadataNative paramCameraMetadataNative, ParameterUtils.ZoomData paramZoomData, Camera.Parameters paramParameters) {
    paramCameraMetadataNative.set(CaptureResult.SCALER_CROP_REGION, paramZoomData.reportedCrop);
    paramCameraMetadataNative.set(CaptureResult.CONTROL_ZOOM_RATIO, Float.valueOf(paramZoomData.reportedZoomRatio));
  }
}
