package android.hardware.camera2.legacy;

import android.hardware.Camera;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.impl.CameraMetadataNative;
import android.hardware.camera2.utils.ParamsUtils;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.util.Objects;

public class LegacyFocusStateMapper {
  private static String TAG = "LegacyFocusStateMapper";
  
  private int mAfStatePrevious = 0;
  
  private String mAfModePrevious = null;
  
  private final Object mLock = new Object();
  
  private int mAfRun = 0;
  
  private int mAfState = 0;
  
  private static final boolean DEBUG = false;
  
  private final Camera mCamera;
  
  public LegacyFocusStateMapper(Camera paramCamera) {
    this.mCamera = (Camera)Preconditions.checkNotNull(paramCamera, "camera must not be null");
  }
  
  public void processRequestTriggers(CaptureRequest paramCaptureRequest, Camera.Parameters paramParameters) {
    Preconditions.checkNotNull(paramCaptureRequest, "captureRequest must not be null");
    CaptureRequest.Key<Integer> key = CaptureRequest.CONTROL_AF_TRIGGER;
    int i = 0;
    int j = ((Integer)ParamsUtils.<Integer>getOrDefault(paramCaptureRequest, key, Integer.valueOf(0))).intValue();
    null = paramParameters.getFocusMode();
    if (!Objects.equals(this.mAfModePrevious, null))
      synchronized (this.mLock) {
        this.mAfRun++;
        this.mAfState = 0;
        this.mCamera.cancelAutoFocus();
      }  
    this.mAfModePrevious = null;
    synchronized (this.mLock) {
      int k = this.mAfRun;
      null = new Object(this, k, null);
      switch (null.hashCode()) {
        default:
          k = -1;
          break;
        case 910005312:
          if (null.equals("continuous-picture")) {
            k = 2;
            break;
          } 
        case 103652300:
          if (null.equals("macro")) {
            k = 1;
            break;
          } 
        case 3005871:
          if (null.equals("auto")) {
            k = 0;
            break;
          } 
        case -194628547:
          if (null.equals("continuous-video")) {
            k = 3;
            break;
          } 
      } 
      if (k == 0 || k == 1 || k == 2 || k == 3)
        this.mCamera.setAutoFocusMoveCallback((Camera.AutoFocusMoveCallback)null); 
      if (j != 0)
        if (j != 1) {
          if (j != 2) {
            null = TAG;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("processRequestTriggers - ignoring unknown control.afTrigger = ");
            stringBuilder.append(j);
            Log.w((String)null, stringBuilder.toString());
          } else {
            synchronized (this.mLock) {
              synchronized (this.mLock) {
                this.mAfRun++;
                this.mAfState = 0;
                this.mCamera.cancelAutoFocus();
              } 
            } 
          } 
        } else {
          switch (null.hashCode()) {
            default:
              k = -1;
              break;
            case 910005312:
              if (null.equals("continuous-picture")) {
                k = 2;
                break;
              } 
            case 103652300:
              if (null.equals("macro")) {
                k = 1;
                break;
              } 
            case 3005871:
              if (null.equals("auto")) {
                k = i;
                break;
              } 
            case -194628547:
              if (null.equals("continuous-video")) {
                k = 3;
                break;
              } 
          } 
          if (k != 0 && k != 1) {
            if (k != 2 && k != 3) {
              k = 0;
            } else {
              k = 1;
            } 
          } else {
            k = 3;
          } 
          synchronized (this.mLock) {
            this.mAfRun = i = this.mAfRun + 1;
            this.mAfState = k;
            if (k != 0)
              this.mCamera.autoFocus((Camera.AutoFocusCallback)new Object(this, i, null)); 
          } 
        }  
      return;
    } 
  }
  
  public void mapResultTriggers(CameraMetadataNative paramCameraMetadataNative) {
    Preconditions.checkNotNull(paramCameraMetadataNative, "result must not be null");
    synchronized (this.mLock) {
      int i = this.mAfState;
      paramCameraMetadataNative.set(CaptureResult.CONTROL_AF_STATE, Integer.valueOf(i));
      this.mAfStatePrevious = i;
      return;
    } 
  }
  
  private static String afStateToString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("UNKNOWN(");
        stringBuilder.append(paramInt);
        stringBuilder.append(")");
        return stringBuilder.toString();
      case 6:
        return "PASSIVE_UNFOCUSED";
      case 5:
        return "NOT_FOCUSED_LOCKED";
      case 4:
        return "FOCUSED_LOCKED";
      case 3:
        return "ACTIVE_SCAN";
      case 2:
        return "PASSIVE_FOCUSED";
      case 1:
        return "PASSIVE_SCAN";
      case 0:
        break;
    } 
    return "INACTIVE";
  }
}
