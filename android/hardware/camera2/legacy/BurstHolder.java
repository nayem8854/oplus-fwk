package android.hardware.camera2.legacy;

import android.hardware.camera2.CaptureRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BurstHolder {
  private static final String TAG = "BurstHolder";
  
  private final boolean mRepeating;
  
  private final ArrayList<RequestHolder.Builder> mRequestBuilders = new ArrayList<>();
  
  private final int mRequestId;
  
  public BurstHolder(int paramInt, boolean paramBoolean, CaptureRequest[] paramArrayOfCaptureRequest, Collection<Long> paramCollection) {
    byte b1 = 0;
    int i;
    byte b2;
    for (i = paramArrayOfCaptureRequest.length, b2 = 0; b2 < i; ) {
      CaptureRequest captureRequest = paramArrayOfCaptureRequest[b2];
      this.mRequestBuilders.add(new RequestHolder.Builder(paramInt, b1, captureRequest, paramBoolean, paramCollection));
      b1++;
      b2++;
    } 
    this.mRepeating = paramBoolean;
    this.mRequestId = paramInt;
  }
  
  public int getRequestId() {
    return this.mRequestId;
  }
  
  public boolean isRepeating() {
    return this.mRepeating;
  }
  
  public int getNumberOfRequests() {
    return this.mRequestBuilders.size();
  }
  
  public List<RequestHolder> produceRequestHolders(long paramLong) {
    ArrayList<RequestHolder> arrayList = new ArrayList();
    byte b = 0;
    for (RequestHolder.Builder builder : this.mRequestBuilders) {
      arrayList.add(builder.build(b + paramLong));
      b++;
    } 
    return arrayList;
  }
}
