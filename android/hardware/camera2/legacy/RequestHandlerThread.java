package android.hardware.camera2.legacy;

import android.os.ConditionVariable;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.MessageQueue;

public class RequestHandlerThread extends HandlerThread {
  private final ConditionVariable mStarted = new ConditionVariable(false);
  
  private final MessageQueue.IdleHandler mIdleHandler;
  
  private final ConditionVariable mIdle = new ConditionVariable(true);
  
  private volatile Handler mHandler;
  
  private Handler.Callback mCallback;
  
  public static final int MSG_POKE_IDLE_HANDLER = -1;
  
  public RequestHandlerThread(String paramString, Handler.Callback paramCallback) {
    super(paramString, 10);
    this.mIdleHandler = new MessageQueue.IdleHandler() {
        final RequestHandlerThread this$0;
        
        public boolean queueIdle() {
          RequestHandlerThread.this.mIdle.open();
          return false;
        }
      };
    this.mCallback = paramCallback;
  }
  
  protected void onLooperPrepared() {
    this.mHandler = new Handler(getLooper(), this.mCallback);
    this.mStarted.open();
  }
  
  public void waitUntilStarted() {
    this.mStarted.block();
  }
  
  public Handler getHandler() {
    return this.mHandler;
  }
  
  public Handler waitAndGetHandler() {
    waitUntilStarted();
    return getHandler();
  }
  
  public boolean hasAnyMessages(int[] paramArrayOfint) {
    synchronized (this.mHandler.getLooper().getQueue()) {
      int i;
      byte b;
      for (i = paramArrayOfint.length, b = 0; b < i; ) {
        int j = paramArrayOfint[b];
        if (this.mHandler.hasMessages(j))
          return true; 
        b++;
      } 
      return false;
    } 
  }
  
  public void removeMessages(int[] paramArrayOfint) {
    synchronized (this.mHandler.getLooper().getQueue()) {
      int i;
      byte b;
      for (i = paramArrayOfint.length, b = 0; b < i; ) {
        int j = paramArrayOfint[b];
        this.mHandler.removeMessages(j);
        b++;
      } 
      return;
    } 
  }
  
  public void waitUntilIdle() {
    Handler handler = waitAndGetHandler();
    MessageQueue messageQueue = handler.getLooper().getQueue();
    if (messageQueue.isIdle())
      return; 
    this.mIdle.close();
    messageQueue.addIdleHandler(this.mIdleHandler);
    handler.sendEmptyMessage(-1);
    if (messageQueue.isIdle())
      return; 
    this.mIdle.block();
  }
}
