package android.hardware.camera2;

import android.app.ActivityThread;
import android.hardware.camera2.params.Face;
import android.hardware.camera2.params.MeteringRectangle;
import android.util.Log;
import java.util.HashMap;
import oplus.util.OplusStatistics;

public final class OplusCamera2StatisticsManager {
  private static OplusCamera2StatisticsManager sInstance = new OplusCamera2StatisticsManager();
  
  private long mConnectTime = 0L;
  
  private long mDisconnectTime = 0L;
  
  private TotalCaptureResult mCurrentResult = null;
  
  private static final String TAG = "OplusCamera2StatisticsManager";
  
  public static OplusCamera2StatisticsManager getInstance() {
    // Byte code:
    //   0: ldc android/hardware/camera2/OplusCamera2StatisticsManager
    //   2: monitorenter
    //   3: getstatic android/hardware/camera2/OplusCamera2StatisticsManager.sInstance : Landroid/hardware/camera2/OplusCamera2StatisticsManager;
    //   6: astore_0
    //   7: ldc android/hardware/camera2/OplusCamera2StatisticsManager
    //   9: monitorexit
    //   10: aload_0
    //   11: areturn
    //   12: astore_0
    //   13: ldc android/hardware/camera2/OplusCamera2StatisticsManager
    //   15: monitorexit
    //   16: aload_0
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #40	-> 3
    //   #40	-> 12
    // Exception table:
    //   from	to	target	type
    //   3	7	12	finally
  }
  
  public void setCurrentResult(TotalCaptureResult paramTotalCaptureResult) {
    this.mCurrentResult = paramTotalCaptureResult;
  }
  
  public void setConnectTime(long paramLong) {
    this.mConnectTime = paramLong;
  }
  
  public void setDisconnectTime(long paramLong) {
    this.mDisconnectTime = paramLong;
  }
  
  public void addInfo(String paramString, long paramLong) {
    HashMap<Object, Object> hashMap = new HashMap<>();
    hashMap.put("pkgName", ActivityThread.currentOpPackageName());
    hashMap.put("cameraId", String.valueOf(paramString));
    hashMap.put("apLevel", "2");
    hashMap.put("halLevel", "3");
    hashMap.put("connentTime", String.valueOf(this.mConnectTime));
    if (paramLong > 1L) {
      hashMap.put("disconnectTime", String.valueOf(paramLong));
      hashMap.put("timeCost", String.valueOf(paramLong - this.mConnectTime));
      this.mConnectTime = 0L;
    } 
    OplusStatistics.onCommon(ActivityThread.currentApplication().getApplicationContext(), "2012002", "openCamera", hashMap, false);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("addInfo, eventMap: ");
    stringBuilder.append(hashMap.toString());
    Log.d("OplusCamera2StatisticsManager", stringBuilder.toString());
  }
  
  public void addPreviewInfo(String paramString, long paramLong) {
    try {
      HashMap<Object, Object> hashMap = new HashMap<>();
      this();
      hashMap.put("pkgName", ActivityThread.currentOpPackageName());
      hashMap.put("cameraId", String.valueOf(paramString));
      hashMap.put("apLevel", "2");
      hashMap.put("halLevel", "3");
      hashMap.put("preview_time", String.valueOf(paramLong - this.mConnectTime));
      if (this.mCurrentResult != null) {
        Face[] arrayOfFace = this.mCurrentResult.<Face[]>get((CaptureResult.Key)CaptureResult.STATISTICS_FACES);
        MeteringRectangle[] arrayOfMeteringRectangle = this.mCurrentResult.<MeteringRectangle[]>get((CaptureResult.Key)CaptureResult.CONTROL_AF_REGIONS);
        if (arrayOfFace != null)
          hashMap.put("face_count", String.valueOf(arrayOfFace.length)); 
        if (arrayOfMeteringRectangle != null && arrayOfMeteringRectangle.length > 0) {
          MeteringRectangle meteringRectangle = arrayOfMeteringRectangle[0];
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append(String.valueOf(meteringRectangle.getX()));
          stringBuilder1.append(",");
          stringBuilder1.append(String.valueOf(meteringRectangle.getY()));
          hashMap.put("touchxy_value", stringBuilder1.toString());
        } 
        this.mCurrentResult = null;
      } 
      OplusStatistics.onCommon(ActivityThread.currentApplication().getApplicationContext(), "2012002", "preview", hashMap, false);
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("addPreviewInfo, eventMap: ");
      stringBuilder.append(hashMap.toString());
      Log.d("OplusCamera2StatisticsManager", stringBuilder.toString());
    } catch (Exception exception) {
      Log.e("OplusCamera2StatisticsManager", "failure in addPreviewInfo");
    } 
  }
  
  public void addCaptureInfo(String paramString, CameraCharacteristics paramCameraCharacteristics, TotalCaptureResult paramTotalCaptureResult) {
    // Byte code:
    //   0: aload_3
    //   1: getstatic android/hardware/camera2/CaptureResult.CONTROL_CAPTURE_INTENT : Landroid/hardware/camera2/CaptureResult$Key;
    //   4: invokevirtual get : (Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;
    //   7: checkcast java/lang/Integer
    //   10: astore #4
    //   12: aload #4
    //   14: ifnull -> 522
    //   17: aload #4
    //   19: invokevirtual intValue : ()I
    //   22: istore #5
    //   24: iconst_1
    //   25: iload #5
    //   27: if_icmpne -> 38
    //   30: aload_0
    //   31: aload_3
    //   32: invokevirtual setCurrentResult : (Landroid/hardware/camera2/TotalCaptureResult;)V
    //   35: goto -> 38
    //   38: iconst_2
    //   39: aload #4
    //   41: invokevirtual intValue : ()I
    //   44: if_icmpne -> 522
    //   47: new java/util/HashMap
    //   50: astore #4
    //   52: aload #4
    //   54: invokespecial <init> : ()V
    //   57: aload #4
    //   59: ldc 'cameraId'
    //   61: aload_1
    //   62: invokestatic valueOf : (Ljava/lang/Object;)Ljava/lang/String;
    //   65: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   68: pop
    //   69: aload_2
    //   70: getstatic android/hardware/camera2/CameraCharacteristics.LENS_FACING : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   73: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   76: checkcast java/lang/Integer
    //   79: invokevirtual intValue : ()I
    //   82: istore #5
    //   84: iload #5
    //   86: ifne -> 102
    //   89: aload #4
    //   91: ldc 'rear_front'
    //   93: ldc 'front'
    //   95: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   98: pop
    //   99: goto -> 112
    //   102: aload #4
    //   104: ldc 'rear_front'
    //   106: ldc 'rear'
    //   108: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   111: pop
    //   112: aload #4
    //   114: ldc 'pkgName'
    //   116: invokestatic currentOpPackageName : ()Ljava/lang/String;
    //   119: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   122: pop
    //   123: aload #4
    //   125: ldc 'halLevel'
    //   127: ldc '3'
    //   129: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   132: pop
    //   133: aload #4
    //   135: ldc 'apLevel'
    //   137: ldc '2'
    //   139: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   142: pop
    //   143: aload_3
    //   144: ifnull -> 464
    //   147: aload_3
    //   148: getstatic android/hardware/camera2/CaptureResult.STATISTICS_FACES : Landroid/hardware/camera2/CaptureResult$Key;
    //   151: invokevirtual get : (Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;
    //   154: checkcast [Landroid/hardware/camera2/params/Face;
    //   157: astore #6
    //   159: aload_3
    //   160: getstatic android/hardware/camera2/CaptureResult.SENSOR_SENSITIVITY : Landroid/hardware/camera2/CaptureResult$Key;
    //   163: invokevirtual get : (Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;
    //   166: checkcast java/lang/Integer
    //   169: astore #7
    //   171: aload_3
    //   172: getstatic android/hardware/camera2/CaptureResult.SENSOR_EXPOSURE_TIME : Landroid/hardware/camera2/CaptureResult$Key;
    //   175: invokevirtual get : (Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;
    //   178: checkcast java/lang/Long
    //   181: astore #8
    //   183: aload_3
    //   184: getstatic android/hardware/camera2/CaptureResult.FLASH_MODE : Landroid/hardware/camera2/CaptureResult$Key;
    //   187: invokevirtual get : (Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;
    //   190: checkcast java/lang/Integer
    //   193: astore_1
    //   194: aload_3
    //   195: getstatic android/hardware/camera2/CaptureResult.SCALER_CROP_REGION : Landroid/hardware/camera2/CaptureResult$Key;
    //   198: invokevirtual get : (Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;
    //   201: checkcast android/graphics/Rect
    //   204: astore #9
    //   206: aload_2
    //   207: getstatic android/hardware/camera2/CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   210: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   213: checkcast android/graphics/Rect
    //   216: astore_2
    //   217: aload_3
    //   218: getstatic android/hardware/camera2/CaptureResult.CONTROL_AF_REGIONS : Landroid/hardware/camera2/CaptureResult$Key;
    //   221: invokevirtual get : (Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;
    //   224: checkcast [Landroid/hardware/camera2/params/MeteringRectangle;
    //   227: astore_3
    //   228: aload #6
    //   230: ifnull -> 247
    //   233: aload #4
    //   235: ldc 'face_count'
    //   237: aload #6
    //   239: arraylength
    //   240: invokestatic valueOf : (I)Ljava/lang/String;
    //   243: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   246: pop
    //   247: aload #7
    //   249: ifnull -> 265
    //   252: aload #4
    //   254: ldc 'iso_value'
    //   256: aload #7
    //   258: invokestatic valueOf : (Ljava/lang/Object;)Ljava/lang/String;
    //   261: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   264: pop
    //   265: aload #8
    //   267: ifnull -> 283
    //   270: aload #4
    //   272: ldc 'exp_value'
    //   274: aload #8
    //   276: invokestatic valueOf : (Ljava/lang/Object;)Ljava/lang/String;
    //   279: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   282: pop
    //   283: aload_1
    //   284: ifnull -> 350
    //   287: aload_1
    //   288: invokevirtual intValue : ()I
    //   291: istore #5
    //   293: iload #5
    //   295: ifne -> 311
    //   298: aload #4
    //   300: ldc 'flash_trigger'
    //   302: ldc '0'
    //   304: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   307: pop
    //   308: goto -> 350
    //   311: iconst_1
    //   312: aload_1
    //   313: invokevirtual intValue : ()I
    //   316: if_icmpne -> 332
    //   319: aload #4
    //   321: ldc 'flash_trigger'
    //   323: ldc '1'
    //   325: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   328: pop
    //   329: goto -> 350
    //   332: iconst_2
    //   333: aload_1
    //   334: invokevirtual intValue : ()I
    //   337: if_icmpne -> 350
    //   340: aload #4
    //   342: ldc 'flash_trigger'
    //   344: ldc '2'
    //   346: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   349: pop
    //   350: aload #9
    //   352: ifnull -> 400
    //   355: aload_2
    //   356: ifnull -> 400
    //   359: aload #9
    //   361: invokevirtual width : ()I
    //   364: ifle -> 400
    //   367: aload_2
    //   368: invokevirtual width : ()I
    //   371: ifle -> 400
    //   374: aload_2
    //   375: invokevirtual width : ()I
    //   378: aload #9
    //   380: invokevirtual width : ()I
    //   383: idiv
    //   384: i2f
    //   385: fstore #10
    //   387: aload #4
    //   389: ldc 'zoom_value'
    //   391: fload #10
    //   393: invokestatic valueOf : (F)Ljava/lang/String;
    //   396: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   399: pop
    //   400: aload_3
    //   401: ifnull -> 464
    //   404: aload_3
    //   405: arraylength
    //   406: ifle -> 464
    //   409: aload_3
    //   410: iconst_0
    //   411: aaload
    //   412: astore_1
    //   413: new java/lang/StringBuilder
    //   416: astore_2
    //   417: aload_2
    //   418: invokespecial <init> : ()V
    //   421: aload_2
    //   422: aload_1
    //   423: invokevirtual getX : ()I
    //   426: invokestatic valueOf : (I)Ljava/lang/String;
    //   429: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   432: pop
    //   433: aload_2
    //   434: ldc ','
    //   436: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   439: pop
    //   440: aload_2
    //   441: aload_1
    //   442: invokevirtual getY : ()I
    //   445: invokestatic valueOf : (I)Ljava/lang/String;
    //   448: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   451: pop
    //   452: aload #4
    //   454: ldc 'touchxy_value'
    //   456: aload_2
    //   457: invokevirtual toString : ()Ljava/lang/String;
    //   460: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   463: pop
    //   464: invokestatic currentApplication : ()Landroid/app/Application;
    //   467: invokevirtual getApplicationContext : ()Landroid/content/Context;
    //   470: ldc '2012002'
    //   472: ldc 'photograph'
    //   474: aload #4
    //   476: iconst_0
    //   477: invokestatic onCommon : (Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)V
    //   480: new java/lang/StringBuilder
    //   483: astore_1
    //   484: aload_1
    //   485: invokespecial <init> : ()V
    //   488: aload_1
    //   489: ldc 'addCaptureInfo, eventMap: '
    //   491: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   494: pop
    //   495: aload_1
    //   496: aload #4
    //   498: invokevirtual toString : ()Ljava/lang/String;
    //   501: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   504: pop
    //   505: ldc 'OplusCamera2StatisticsManager'
    //   507: aload_1
    //   508: invokevirtual toString : ()Ljava/lang/String;
    //   511: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   514: pop
    //   515: goto -> 522
    //   518: astore_1
    //   519: goto -> 526
    //   522: goto -> 534
    //   525: astore_1
    //   526: ldc 'OplusCamera2StatisticsManager'
    //   528: ldc 'failure in addCaptureInfo'
    //   530: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   533: pop
    //   534: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #111	-> 0
    //   #113	-> 12
    //   #114	-> 17
    //   #115	-> 30
    //   #114	-> 38
    //   #118	-> 38
    //   #119	-> 47
    //   #120	-> 57
    //   #122	-> 69
    //   #123	-> 89
    //   #125	-> 102
    //   #128	-> 112
    //   #129	-> 123
    //   #130	-> 133
    //   #132	-> 143
    //   #133	-> 147
    //   #134	-> 159
    //   #135	-> 171
    //   #136	-> 183
    //   #137	-> 194
    //   #138	-> 206
    //   #139	-> 217
    //   #141	-> 228
    //   #142	-> 233
    //   #145	-> 247
    //   #146	-> 252
    //   #149	-> 265
    //   #150	-> 270
    //   #153	-> 283
    //   #154	-> 287
    //   #155	-> 298
    //   #156	-> 311
    //   #157	-> 319
    //   #158	-> 332
    //   #159	-> 340
    //   #163	-> 350
    //   #165	-> 359
    //   #166	-> 367
    //   #167	-> 374
    //   #168	-> 387
    //   #171	-> 400
    //   #172	-> 409
    //   #173	-> 413
    //   #177	-> 464
    //   #180	-> 480
    //   #183	-> 518
    //   #113	-> 522
    //   #185	-> 522
    //   #183	-> 525
    //   #184	-> 526
    //   #186	-> 534
    // Exception table:
    //   from	to	target	type
    //   0	12	525	java/lang/Exception
    //   17	24	525	java/lang/Exception
    //   30	35	518	java/lang/Exception
    //   38	47	518	java/lang/Exception
    //   47	57	518	java/lang/Exception
    //   57	69	518	java/lang/Exception
    //   69	84	518	java/lang/Exception
    //   89	99	518	java/lang/Exception
    //   102	112	518	java/lang/Exception
    //   112	123	518	java/lang/Exception
    //   123	133	518	java/lang/Exception
    //   133	143	518	java/lang/Exception
    //   147	159	518	java/lang/Exception
    //   159	171	518	java/lang/Exception
    //   171	183	518	java/lang/Exception
    //   183	194	518	java/lang/Exception
    //   194	206	518	java/lang/Exception
    //   206	217	518	java/lang/Exception
    //   217	228	518	java/lang/Exception
    //   233	247	518	java/lang/Exception
    //   252	265	518	java/lang/Exception
    //   270	283	518	java/lang/Exception
    //   287	293	518	java/lang/Exception
    //   298	308	518	java/lang/Exception
    //   311	319	518	java/lang/Exception
    //   319	329	518	java/lang/Exception
    //   332	340	518	java/lang/Exception
    //   340	350	518	java/lang/Exception
    //   359	367	518	java/lang/Exception
    //   367	374	518	java/lang/Exception
    //   374	387	518	java/lang/Exception
    //   387	400	518	java/lang/Exception
    //   404	409	518	java/lang/Exception
    //   413	464	518	java/lang/Exception
    //   464	480	518	java/lang/Exception
    //   480	515	518	java/lang/Exception
  }
}
