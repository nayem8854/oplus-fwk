package android.hardware.camera2;

import android.content.Context;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;

public final class OplusCameraManager {
  private static final CaptureRequest.Key<byte[]> KEY_OPPO_PACKAGE = (CaptureRequest.Key)new CaptureRequest.Key<>("com.oplus.is.sdk.camera.package", (Class)byte[].class);
  
  private static OplusCameraManager mInstance = new OplusCameraManager();
  
  static {
    SET_PACKAGE_BLACK_LIST = new String[] { "com.coloros.oppoguardelf", "com.oplus.onetrace" };
    mSystemCameraPackageName = null;
  }
  
  private String mOpPackageName = "";
  
  private boolean mIsCameraUnitSession = false;
  
  private boolean mbHasSetClintInfo = false;
  
  private static final String PERMISSION_SAFE_CAMERA = "com.oppo.permission.safe.CAMERA";
  
  private static final String PROP_KEY_SYSTEM_CAMERA_PACKAGE = "ro.oplus.system.camera.name";
  
  private static String[] SET_PACKAGE_BLACK_LIST;
  
  private static final String TAG = "OplusCameraManager";
  
  private static String mSystemCameraPackageName;
  
  public static OplusCameraManager getInstance() {
    // Byte code:
    //   0: ldc android/hardware/camera2/OplusCameraManager
    //   2: monitorenter
    //   3: getstatic android/hardware/camera2/OplusCameraManager.mInstance : Landroid/hardware/camera2/OplusCameraManager;
    //   6: astore_0
    //   7: ldc android/hardware/camera2/OplusCameraManager
    //   9: monitorexit
    //   10: aload_0
    //   11: areturn
    //   12: astore_0
    //   13: ldc android/hardware/camera2/OplusCameraManager
    //   15: monitorexit
    //   16: aload_0
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #56	-> 3
    //   #56	-> 12
    // Exception table:
    //   from	to	target	type
    //   3	7	12	finally
  }
  
  public void addAuthResultInfo(Context paramContext, int paramInt1, int paramInt2, int paramInt3, String paramString) {
    paramContext.enforceCallingOrSelfPermission("com.oppo.permission.safe.CAMERA", "OplusCameraManager");
    if (paramInt1 != 0) {
      if (paramInt2 != 0) {
        if (paramString != null) {
          try {
            OplusCameraManagerGlobal.get().addAuthResultInfo(paramInt1, paramInt2, paramInt3, paramString);
          } catch (CameraAccessException cameraAccessException) {
            cameraAccessException.printStackTrace();
          } catch (RemoteException remoteException) {
            remoteException.printStackTrace();
          } 
          return;
        } 
        throw new IllegalArgumentException("packageName was null, which is illegal.");
      } 
      throw new IllegalArgumentException("pid was 0, which is illegal.");
    } 
    throw new IllegalArgumentException("uid was 0, which is illegal.");
  }
  
  public void setDeathRecipient(IBinder paramIBinder) {
    if (paramIBinder != null) {
      try {
        OplusCameraManagerGlobal.get().setDeathRecipient(paramIBinder);
      } catch (CameraAccessException cameraAccessException) {
        cameraAccessException.printStackTrace();
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      } 
      return;
    } 
    throw new IllegalArgumentException("client was null");
  }
  
  public boolean isAuthedClient(Context paramContext) {
    if (paramContext != null) {
      try {
        return OplusCameraManagerGlobal.get().isAuthedClient(paramContext.getOpPackageName());
      } catch (CameraAccessException cameraAccessException) {
        cameraAccessException.printStackTrace();
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      } 
      return false;
    } 
    throw new IllegalArgumentException("context was null");
  }
  
  public void setCallInfo() {
    try {
      OplusCameraManagerGlobal.get().setCallInfo();
    } catch (CameraAccessException cameraAccessException) {
      cameraAccessException.printStackTrace();
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
    } 
  }
  
  public void saveOpPackageName(String paramString) {
    this.mOpPackageName = paramString;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("saveOpPackageName, mOpPackageName: ");
    stringBuilder.append(this.mOpPackageName);
    Log.i("OplusCameraManager", stringBuilder.toString());
  }
  
  public void setPackageName() {
    for (String str : SET_PACKAGE_BLACK_LIST) {
      if (str.equals(this.mOpPackageName))
        return; 
    } 
    if (isSystemCameraPackage(this.mOpPackageName) && this.mbHasSetClintInfo)
      return; 
    try {
      OplusCameraManagerGlobal oplusCameraManagerGlobal = OplusCameraManagerGlobal.get();
      String str = this.mOpPackageName;
      int j = Binder.getCallingUid();
      int i = Binder.getCallingPid();
      oplusCameraManagerGlobal.setClientInfo(str, j, i);
      if (isSystemCameraPackage(this.mOpPackageName))
        this.mbHasSetClintInfo = true; 
    } catch (CameraAccessException cameraAccessException) {
      cameraAccessException.printStackTrace();
    } catch (RemoteException remoteException) {
      remoteException.printStackTrace();
    } 
  }
  
  public static boolean isPrivilegedApp(String paramString) {
    if (paramString == null || "".equals(paramString))
      return false; 
    if (isSystemCameraPackage(paramString))
      return true; 
    return false;
  }
  
  public static boolean isOplusPrivilegedApp(String paramString) {
    if (paramString == null)
      return false; 
    String str = SystemProperties.get("ro.board.platform");
    if ((str.equals("mt6771") || str.equals("mt6785")) && isSystemCameraPackage(paramString))
      return true; 
    return false;
  }
  
  public boolean isCameraUnitSession() {
    return this.mIsCameraUnitSession;
  }
  
  public void parseSessionParameters(CaptureRequest paramCaptureRequest) {
    if (paramCaptureRequest == null) {
      this.mIsCameraUnitSession = false;
      return;
    } 
    byte[] arrayOfByte = paramCaptureRequest.<byte[]>get((CaptureRequest.Key)KEY_OPPO_PACKAGE);
    if (arrayOfByte == null || arrayOfByte.length == 0) {
      this.mIsCameraUnitSession = false;
      return;
    } 
    if (1 == arrayOfByte[0])
      this.mIsCameraUnitSession = true; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("parseSessionParameters mIsCameraUnitSession: ");
    stringBuilder.append(this.mIsCameraUnitSession);
    Log.i("OplusCameraManager", stringBuilder.toString());
  }
  
  private static String getSystemCameraPackageName() {
    if (mSystemCameraPackageName == null) {
      String str = SystemProperties.get("ro.oplus.system.camera.name");
      if (str == null || "".equals(str)) {
        Log.e("OplusCameraManager", "Get system package name fail.");
        return "";
      } 
    } 
    return mSystemCameraPackageName;
  }
  
  private static boolean isSystemCameraPackage(String paramString) {
    if (paramString == null || "".equals(paramString))
      return false; 
    return paramString.equals(getSystemCameraPackageName());
  }
  
  class OplusCameraManagerGlobal implements IBinder.DeathRecipient {
    private final boolean DEBUG = false;
    
    private static final OplusCameraManagerGlobal gCameraManager = new OplusCameraManagerGlobal();
    
    private final Object mLock = new Object();
    
    private IBinder mRemote = null;
    
    public static final boolean sCameraServiceDisabled = SystemProperties.getBoolean("config.disable_cameraservice", false);
    
    private static final int ADD_AUTH_RESULT = 10001;
    
    private static final String CAMERA_SERVICE_BINDER_NAME = "media.camera";
    
    private static final int CLIENT_IS_AUTHED = 10004;
    
    private static final String DESCRIPTOR = "android.hardware.camera";
    
    private static final int OPLUS_CAMERA_FIRST_CALL_TRANSACTION = 10000;
    
    private static final int SET_CALL_INFO = 10006;
    
    private static final int SET_CLIENT_INFO = 10005;
    
    private static final int SET_DEATH_RECIPIENT = 10002;
    
    private static final int SET_PACKAGE_NAME = 10003;
    
    private static final String TAG = "OplusCameraManagerGlobal";
    
    public static OplusCameraManagerGlobal get() {
      return gCameraManager;
    }
    
    private void connectCameraServiceLocked() {
      if (this.mRemote != null || sCameraServiceDisabled)
        return; 
      Log.i("OplusCameraManagerGlobal", "Connecting to camera service");
      IBinder iBinder = ServiceManager.getService("media.camera");
      if (iBinder == null)
        return; 
      try {
        iBinder.linkToDeath(this, 0);
        return;
      } catch (RemoteException remoteException) {
        return;
      } 
    }
    
    public IBinder getCameraServiceRemote() {
      synchronized (this.mLock) {
        connectCameraServiceLocked();
        if (this.mRemote == null && !sCameraServiceDisabled)
          Log.e("OplusCameraManagerGlobal", "Camera service is unavailable"); 
        return this.mRemote;
      } 
    }
    
    public void addAuthResultInfo(int param1Int1, int param1Int2, int param1Int3, String param1String) throws CameraAccessException, RemoteException {
      Log.e("OplusCameraManagerGlobal", "addAuthResultInfo");
      IBinder iBinder = getCameraServiceRemote();
      if (iBinder != null) {
        Parcel parcel2 = Parcel.obtain();
        Parcel parcel1 = Parcel.obtain();
        try {
          parcel2.writeInterfaceToken("android.hardware.camera");
          parcel2.writeInt(param1Int1);
          parcel2.writeInt(param1Int2);
          parcel2.writeInt(param1Int3);
          parcel2.writeString(param1String);
          this.mRemote.transact(10001, parcel2, parcel1, 0);
          parcel1.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      } 
      throw new CameraAccessException(2, "Camera service is currently unavailable");
    }
    
    public void setDeathRecipient(IBinder param1IBinder) throws CameraAccessException, RemoteException {
      Log.e("OplusCameraManagerGlobal", "setDeathRecipient");
      IBinder iBinder = getCameraServiceRemote();
      if (iBinder != null) {
        Parcel parcel2 = Parcel.obtain();
        Parcel parcel1 = Parcel.obtain();
        try {
          parcel2.writeInterfaceToken("android.hardware.camera");
          parcel2.writeStrongBinder(param1IBinder);
          this.mRemote.transact(10002, parcel2, parcel1, 0);
          parcel1.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      } 
      throw new CameraAccessException(2, "Camera service is currently unavailable");
    }
    
    public boolean isAuthedClient(String param1String) throws CameraAccessException, RemoteException {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isAuthedClient, need check packageName: ");
      stringBuilder.append(param1String);
      Log.e("OplusCameraManagerGlobal", stringBuilder.toString());
      IBinder iBinder = getCameraServiceRemote();
      if (iBinder != null) {
        StringBuilder stringBuilder1;
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.camera");
          parcel1.writeString(param1String);
          this.mRemote.transact(10004, parcel1, parcel2, 0);
          parcel2.readException();
          boolean bool = parcel2.readBoolean();
          parcel1.recycle();
          parcel2.recycle();
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("isAuthedClient, the ");
          stringBuilder1.append(param1String);
          stringBuilder1.append(" is Authed ");
          return bool;
        } finally {
          stringBuilder1.recycle();
          parcel2.recycle();
        } 
      } 
      throw new CameraAccessException(2, "Camera service is currently unavailable");
    }
    
    public void setCallInfo() throws CameraAccessException, RemoteException {
      Log.e("OplusCameraManagerGlobal", "setCallInfo");
      IBinder iBinder = getCameraServiceRemote();
      if (iBinder != null) {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.camera");
          this.mRemote.transact(10006, parcel1, parcel2, 0);
          parcel2.readException();
          return;
        } finally {
          parcel1.recycle();
          parcel2.recycle();
        } 
      } 
      throw new CameraAccessException(2, "Camera service is currently unavailable");
    }
    
    public void setPackageName(String param1String) throws CameraAccessException, RemoteException {
      Log.i("OplusCameraManagerGlobal", "setPackageName");
      IBinder iBinder = getCameraServiceRemote();
      if (iBinder != null) {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.camera");
          parcel1.writeString(param1String);
          iBinder.transact(10003, parcel1, parcel2, 0);
          parcel2.readException();
          return;
        } finally {
          parcel1.recycle();
          parcel2.recycle();
        } 
      } 
      throw new CameraAccessException(2, "Camera service is currently unavailable");
    }
    
    public void setClientInfo(String param1String, int param1Int1, int param1Int2) throws CameraAccessException, RemoteException {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setClientInfo, packageName: ");
      stringBuilder.append(param1String);
      stringBuilder.append(", uid: ");
      stringBuilder.append(param1Int1);
      stringBuilder.append(", pid: ");
      stringBuilder.append(param1Int2);
      Log.i("OplusCameraManagerGlobal", stringBuilder.toString());
      IBinder iBinder = getCameraServiceRemote();
      if (iBinder != null) {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.camera");
          parcel1.writeString(param1String);
          parcel1.writeInt(param1Int1);
          parcel1.writeInt(param1Int2);
          iBinder.transact(10005, parcel1, parcel2, 0);
          parcel2.readException();
          return;
        } finally {
          parcel1.recycle();
          parcel2.recycle();
        } 
      } 
      throw new CameraAccessException(2, "Camera service is currently unavailable");
    }
    
    public void binderDied() {
      this.mRemote = null;
    }
  }
}
