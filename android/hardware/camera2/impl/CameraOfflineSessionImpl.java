package android.hardware.camera2.impl;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraOfflineSession;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.ICameraDeviceCallbacks;
import android.hardware.camera2.ICameraOfflineSession;
import android.hardware.camera2.params.InputConfiguration;
import android.hardware.camera2.params.OutputConfiguration;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import android.view.Surface;
import com.android.internal.util.Preconditions;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

public class CameraOfflineSessionImpl extends CameraOfflineSession implements IBinder.DeathRecipient {
  private final boolean DEBUG = false;
  
  private final AtomicBoolean mClosing = new AtomicBoolean();
  
  private AbstractMap.SimpleEntry<Integer, InputConfiguration> mOfflineInput = new AbstractMap.SimpleEntry<>(Integer.valueOf(-1), null);
  
  private SparseArray<OutputConfiguration> mOfflineOutputs = new SparseArray();
  
  private SparseArray<OutputConfiguration> mConfiguredOutputs = new SparseArray();
  
  final Object mInterfaceLock = new Object();
  
  private final CameraDeviceCallbacks mCallbacks = new CameraDeviceCallbacks();
  
  private List<RequestLastFrameNumbersHolder> mOfflineRequestLastFrameNumbersList = new ArrayList<>();
  
  private FrameNumberTracker mFrameNumberTracker = new FrameNumberTracker();
  
  private SparseArray<CaptureCallbackHolder> mCaptureCallbackMap = new SparseArray();
  
  private static final long NANO_PER_SECOND = 1000000000L;
  
  private static final int REQUEST_ID_NONE = -1;
  
  private static final String TAG = "CameraOfflineSessionImpl";
  
  private final String mCameraId;
  
  private final CameraCharacteristics mCharacteristics;
  
  private final CameraOfflineSession.CameraOfflineSessionCallback mOfflineCallback;
  
  private final Executor mOfflineExecutor;
  
  private ICameraOfflineSession mRemoteSession;
  
  private final int mTotalPartialCount;
  
  public CameraOfflineSessionImpl(String paramString, CameraCharacteristics paramCameraCharacteristics, Executor paramExecutor, CameraOfflineSession.CameraOfflineSessionCallback paramCameraOfflineSessionCallback, SparseArray<OutputConfiguration> paramSparseArray1, AbstractMap.SimpleEntry<Integer, InputConfiguration> paramSimpleEntry, SparseArray<OutputConfiguration> paramSparseArray2, FrameNumberTracker paramFrameNumberTracker, SparseArray<CaptureCallbackHolder> paramSparseArray, List<RequestLastFrameNumbersHolder> paramList) {
    if (paramString != null && paramCameraCharacteristics != null) {
      this.mCameraId = paramString;
      this.mCharacteristics = paramCameraCharacteristics;
      CameraCharacteristics.Key<Integer> key = CameraCharacteristics.REQUEST_PARTIAL_RESULT_COUNT;
      Integer integer = paramCameraCharacteristics.<Integer>get(key);
      if (integer == null) {
        this.mTotalPartialCount = 1;
      } else {
        this.mTotalPartialCount = integer.intValue();
      } 
      this.mOfflineRequestLastFrameNumbersList.addAll(paramList);
      this.mFrameNumberTracker = paramFrameNumberTracker;
      this.mCaptureCallbackMap = paramSparseArray;
      this.mConfiguredOutputs = paramSparseArray2;
      this.mOfflineOutputs = paramSparseArray1;
      this.mOfflineInput = paramSimpleEntry;
      this.mOfflineExecutor = (Executor)Preconditions.checkNotNull(paramExecutor, "offline executor must not be null");
      this.mOfflineCallback = (CameraOfflineSession.CameraOfflineSessionCallback)Preconditions.checkNotNull(paramCameraOfflineSessionCallback, "offline callback must not be null");
      return;
    } 
    throw new IllegalArgumentException("Null argument given");
  }
  
  public CameraDeviceCallbacks getCallbacks() {
    return this.mCallbacks;
  }
  
  class CameraDeviceCallbacks extends ICameraDeviceCallbacks.Stub {
    final CameraOfflineSessionImpl this$0;
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public void onDeviceError(int param1Int, CaptureResultExtras param1CaptureResultExtras) {
      // Byte code:
      //   0: aload_0
      //   1: getfield this$0 : Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   4: getfield mInterfaceLock : Ljava/lang/Object;
      //   7: astore_3
      //   8: aload_3
      //   9: monitorenter
      //   10: iload_1
      //   11: iconst_3
      //   12: if_icmpeq -> 68
      //   15: iload_1
      //   16: iconst_4
      //   17: if_icmpeq -> 68
      //   20: iload_1
      //   21: iconst_5
      //   22: if_icmpeq -> 68
      //   25: new android/hardware/camera2/impl/CameraOfflineSessionImpl$CameraDeviceCallbacks$1
      //   28: astore_2
      //   29: aload_2
      //   30: aload_0
      //   31: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl$CameraDeviceCallbacks;)V
      //   34: invokestatic clearCallingIdentity : ()J
      //   37: lstore #4
      //   39: aload_0
      //   40: getfield this$0 : Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   43: invokestatic access$200 : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;)Ljava/util/concurrent/Executor;
      //   46: aload_2
      //   47: invokeinterface execute : (Ljava/lang/Runnable;)V
      //   52: lload #4
      //   54: invokestatic restoreCallingIdentity : (J)V
      //   57: goto -> 74
      //   60: astore_2
      //   61: lload #4
      //   63: invokestatic restoreCallingIdentity : (J)V
      //   66: aload_2
      //   67: athrow
      //   68: aload_0
      //   69: iload_1
      //   70: aload_2
      //   71: invokespecial onCaptureErrorLocked : (ILandroid/hardware/camera2/impl/CaptureResultExtras;)V
      //   74: aload_3
      //   75: monitorexit
      //   76: return
      //   77: astore_2
      //   78: aload_3
      //   79: monitorexit
      //   80: aload_2
      //   81: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #142	-> 0
      //   #144	-> 10
      //   #151	-> 25
      //   #161	-> 34
      //   #163	-> 39
      //   #165	-> 52
      //   #166	-> 57
      //   #165	-> 60
      //   #166	-> 66
      //   #148	-> 68
      //   #149	-> 74
      //   #168	-> 74
      //   #169	-> 76
      //   #168	-> 77
      // Exception table:
      //   from	to	target	type
      //   25	34	77	finally
      //   34	39	77	finally
      //   39	52	60	finally
      //   52	57	77	finally
      //   61	66	77	finally
      //   66	68	77	finally
      //   68	74	77	finally
      //   74	76	77	finally
      //   78	80	77	finally
    }
    
    public void onRepeatingRequestError(long param1Long, int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected repeating request error received. Last frame number is ");
      stringBuilder.append(param1Long);
      Log.e("CameraOfflineSessionImpl", stringBuilder.toString());
    }
    
    public void onDeviceIdle() {
      synchronized (CameraOfflineSessionImpl.this.mInterfaceLock) {
        if (CameraOfflineSessionImpl.this.mRemoteSession == null) {
          Log.v("CameraOfflineSessionImpl", "Ignoring idle state notifications during offline switches");
          return;
        } 
        CameraOfflineSessionImpl.this.removeCompletedCallbackHolderLocked(Long.MAX_VALUE, Long.MAX_VALUE, Long.MAX_VALUE);
        null = new Object();
        super();
        long l = Binder.clearCallingIdentity();
        try {
          CameraOfflineSessionImpl.this.mOfflineExecutor.execute((Runnable)null);
          return;
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } 
    }
    
    public void onCaptureStarted(CaptureResultExtras param1CaptureResultExtras, long param1Long) {
      Object object3;
      int i = param1CaptureResultExtras.getRequestId();
      long l1 = param1CaptureResultExtras.getFrameNumber();
      long l2 = param1CaptureResultExtras.getLastCompletedRegularFrameNumber();
      long l3 = param1CaptureResultExtras.getLastCompletedReprocessFrameNumber();
      long l4 = param1CaptureResultExtras.getLastCompletedZslFrameNumber();
      Object object2 = CameraOfflineSessionImpl.this.mInterfaceLock;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      try {
        CameraOfflineSessionImpl.this.removeCompletedCallbackHolderLocked(l2, l3, l4);
        CaptureCallbackHolder captureCallbackHolder = (CaptureCallbackHolder)CameraOfflineSessionImpl.this.mCaptureCallbackMap.get(i);
        if (captureCallbackHolder == null) {
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return;
        } 
        Executor executor = captureCallbackHolder.getCallback().getExecutor();
        if (CameraOfflineSessionImpl.this.isClosed() || executor == null) {
          object = object2;
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return;
        } 
        l2 = Binder.clearCallingIdentity();
        try {
          Object object4 = new Object();
          object3 = object2;
          try {
            super(captureCallbackHolder, (CaptureResultExtras)object, param1Long, l1);
            executor.execute((Runnable)object4);
            object = object3;
            try {
              Binder.restoreCallingIdentity(l2);
              object = object3;
              return;
            } finally {
              object3 = null;
            } 
          } finally {}
        } finally {}
        Object object = object2;
        Binder.restoreCallingIdentity(l2);
        object = object2;
        throw object3;
      } finally {}
      Object object1 = object2;
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      throw object3;
    }
    
    public void onResultReceived(CameraMetadataNative param1CameraMetadataNative, CaptureResultExtras param1CaptureResultExtras, PhysicalCaptureResultInfo[] param1ArrayOfPhysicalCaptureResultInfo) throws RemoteException {
      // Byte code:
      //   0: aload_2
      //   1: invokevirtual getRequestId : ()I
      //   4: istore #4
      //   6: aload_2
      //   7: invokevirtual getFrameNumber : ()J
      //   10: lstore #5
      //   12: aload_0
      //   13: getfield this$0 : Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   16: getfield mInterfaceLock : Ljava/lang/Object;
      //   19: astore #7
      //   21: aload #7
      //   23: monitorenter
      //   24: getstatic android/hardware/camera2/CameraCharacteristics.LENS_INFO_SHADING_MAP_SIZE : Landroid/hardware/camera2/CameraCharacteristics$Key;
      //   27: astore #8
      //   29: aload_0
      //   30: getfield this$0 : Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   33: astore #9
      //   35: aload #9
      //   37: invokestatic access$600 : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;)Landroid/hardware/camera2/CameraCharacteristics;
      //   40: getstatic android/hardware/camera2/CameraCharacteristics.LENS_INFO_SHADING_MAP_SIZE : Landroid/hardware/camera2/CameraCharacteristics$Key;
      //   43: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
      //   46: checkcast android/util/Size
      //   49: astore #9
      //   51: aload_1
      //   52: aload #8
      //   54: aload #9
      //   56: invokevirtual set : (Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)V
      //   59: aload_0
      //   60: getfield this$0 : Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   63: astore #8
      //   65: aload #8
      //   67: invokestatic access$500 : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;)Landroid/util/SparseArray;
      //   70: iload #4
      //   72: invokevirtual get : (I)Ljava/lang/Object;
      //   75: checkcast android/hardware/camera2/impl/CaptureCallbackHolder
      //   78: astore #10
      //   80: aload #10
      //   82: aload_2
      //   83: invokevirtual getSubsequenceId : ()I
      //   86: invokevirtual getRequest : (I)Landroid/hardware/camera2/CaptureRequest;
      //   89: astore #11
      //   91: aload_2
      //   92: invokevirtual getPartialResultCount : ()I
      //   95: aload_0
      //   96: getfield this$0 : Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   99: invokestatic access$700 : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;)I
      //   102: if_icmpge -> 111
      //   105: iconst_1
      //   106: istore #12
      //   108: goto -> 114
      //   111: iconst_0
      //   112: istore #12
      //   114: aload #11
      //   116: invokevirtual getRequestType : ()I
      //   119: istore #13
      //   121: aload #10
      //   123: ifnonnull -> 154
      //   126: aload_0
      //   127: getfield this$0 : Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   130: invokestatic access$800 : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;)Landroid/hardware/camera2/impl/FrameNumberTracker;
      //   133: lload #5
      //   135: aconst_null
      //   136: iload #12
      //   138: iload #13
      //   140: invokevirtual updateTracker : (JLandroid/hardware/camera2/CaptureResult;ZI)V
      //   143: aload #7
      //   145: monitorexit
      //   146: return
      //   147: astore_1
      //   148: aload #7
      //   150: astore_2
      //   151: goto -> 484
      //   154: aload_0
      //   155: getfield this$0 : Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   158: invokestatic access$000 : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;)Z
      //   161: istore #14
      //   163: iload #14
      //   165: ifeq -> 189
      //   168: aload_0
      //   169: getfield this$0 : Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   172: invokestatic access$800 : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;)Landroid/hardware/camera2/impl/FrameNumberTracker;
      //   175: lload #5
      //   177: aconst_null
      //   178: iload #12
      //   180: iload #13
      //   182: invokevirtual updateTracker : (JLandroid/hardware/camera2/CaptureResult;ZI)V
      //   185: aload #7
      //   187: monitorexit
      //   188: return
      //   189: aload #10
      //   191: invokevirtual hasBatchedOutputs : ()Z
      //   194: istore #14
      //   196: iload #14
      //   198: ifeq -> 215
      //   201: new android/hardware/camera2/impl/CameraMetadataNative
      //   204: astore #8
      //   206: aload #8
      //   208: aload_1
      //   209: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraMetadataNative;)V
      //   212: goto -> 218
      //   215: aconst_null
      //   216: astore #8
      //   218: aload #10
      //   220: invokevirtual getCallback : ()Landroid/hardware/camera2/impl/CaptureCallback;
      //   223: invokevirtual getExecutor : ()Ljava/util/concurrent/Executor;
      //   226: astore #15
      //   228: iload #12
      //   230: ifeq -> 267
      //   233: new android/hardware/camera2/CaptureResult
      //   236: astore_3
      //   237: aload_3
      //   238: aload_1
      //   239: aload #11
      //   241: aload_2
      //   242: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraMetadataNative;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/impl/CaptureResultExtras;)V
      //   245: new android/hardware/camera2/impl/CameraOfflineSessionImpl$CameraDeviceCallbacks$4
      //   248: dup
      //   249: aload_0
      //   250: aload #10
      //   252: aload #8
      //   254: aload_2
      //   255: aload #11
      //   257: aload_3
      //   258: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl$CameraDeviceCallbacks;Landroid/hardware/camera2/impl/CaptureCallbackHolder;Landroid/hardware/camera2/impl/CameraMetadataNative;Landroid/hardware/camera2/impl/CaptureResultExtras;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureResult;)V
      //   261: astore_1
      //   262: aload_3
      //   263: astore_2
      //   264: goto -> 385
      //   267: aload_0
      //   268: getfield this$0 : Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   271: astore #9
      //   273: aload #9
      //   275: invokestatic access$800 : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;)Landroid/hardware/camera2/impl/FrameNumberTracker;
      //   278: lload #5
      //   280: invokevirtual popPartialResults : (J)Ljava/util/List;
      //   283: astore #16
      //   285: getstatic android/hardware/camera2/CaptureResult.SENSOR_TIMESTAMP : Landroid/hardware/camera2/CaptureResult$Key;
      //   288: astore #9
      //   290: aload_1
      //   291: aload #9
      //   293: invokevirtual get : (Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;
      //   296: checkcast java/lang/Long
      //   299: invokevirtual longValue : ()J
      //   302: lstore #17
      //   304: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE : Landroid/hardware/camera2/CaptureRequest$Key;
      //   307: astore #9
      //   309: aload #11
      //   311: aload #9
      //   313: invokevirtual get : (Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;
      //   316: checkcast android/util/Range
      //   319: astore #19
      //   321: aload_2
      //   322: invokevirtual getSubsequenceId : ()I
      //   325: istore #20
      //   327: new android/hardware/camera2/TotalCaptureResult
      //   330: astore #9
      //   332: aload #10
      //   334: invokevirtual getSessionId : ()I
      //   337: istore #4
      //   339: aload #9
      //   341: aload_1
      //   342: aload #11
      //   344: aload_2
      //   345: aload #16
      //   347: iload #4
      //   349: aload_3
      //   350: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraMetadataNative;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/impl/CaptureResultExtras;Ljava/util/List;I[Landroid/hardware/camera2/impl/PhysicalCaptureResultInfo;)V
      //   353: aload #7
      //   355: astore_3
      //   356: new android/hardware/camera2/impl/CameraOfflineSessionImpl$CameraDeviceCallbacks$5
      //   359: dup
      //   360: aload_0
      //   361: aload #10
      //   363: aload #8
      //   365: lload #17
      //   367: iload #20
      //   369: aload #19
      //   371: aload_2
      //   372: aload #16
      //   374: aload #11
      //   376: aload #9
      //   378: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl$CameraDeviceCallbacks;Landroid/hardware/camera2/impl/CaptureCallbackHolder;Landroid/hardware/camera2/impl/CameraMetadataNative;JILandroid/util/Range;Landroid/hardware/camera2/impl/CaptureResultExtras;Ljava/util/List;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V
      //   381: astore_1
      //   382: aload #9
      //   384: astore_2
      //   385: aload #15
      //   387: ifnull -> 431
      //   390: aload #7
      //   392: astore_3
      //   393: invokestatic clearCallingIdentity : ()J
      //   396: lstore #17
      //   398: aload #15
      //   400: aload_1
      //   401: invokeinterface execute : (Ljava/lang/Runnable;)V
      //   406: aload #7
      //   408: astore_3
      //   409: lload #17
      //   411: invokestatic restoreCallingIdentity : (J)V
      //   414: goto -> 431
      //   417: astore_1
      //   418: aload #7
      //   420: astore_3
      //   421: lload #17
      //   423: invokestatic restoreCallingIdentity : (J)V
      //   426: aload #7
      //   428: astore_3
      //   429: aload_1
      //   430: athrow
      //   431: aload #7
      //   433: astore_3
      //   434: aload_0
      //   435: getfield this$0 : Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   438: invokestatic access$800 : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;)Landroid/hardware/camera2/impl/FrameNumberTracker;
      //   441: lload #5
      //   443: aload_2
      //   444: iload #12
      //   446: iload #13
      //   448: invokevirtual updateTracker : (JLandroid/hardware/camera2/CaptureResult;ZI)V
      //   451: iload #12
      //   453: ifne -> 466
      //   456: aload #7
      //   458: astore_3
      //   459: aload_0
      //   460: getfield this$0 : Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   463: invokestatic access$900 : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;)V
      //   466: aload #7
      //   468: astore_3
      //   469: aload #7
      //   471: monitorexit
      //   472: return
      //   473: astore_1
      //   474: aload #7
      //   476: astore_2
      //   477: goto -> 484
      //   480: astore_1
      //   481: aload #7
      //   483: astore_2
      //   484: aload_2
      //   485: astore_3
      //   486: aload_2
      //   487: monitorexit
      //   488: aload_1
      //   489: athrow
      //   490: astore_1
      //   491: aload_3
      //   492: astore_2
      //   493: goto -> 484
      // Line number table:
      //   Java source line number -> byte code offset
      //   #289	-> 0
      //   #290	-> 6
      //   #292	-> 12
      //   #294	-> 24
      //   #295	-> 35
      //   #294	-> 51
      //   #297	-> 59
      //   #298	-> 65
      //   #299	-> 80
      //   #301	-> 91
      //   #302	-> 91
      //   #303	-> 114
      //   #306	-> 121
      //   #307	-> 126
      //   #310	-> 143
      //   #430	-> 147
      //   #313	-> 154
      //   #314	-> 168
      //   #316	-> 185
      //   #320	-> 189
      //   #326	-> 189
      //   #327	-> 201
      //   #329	-> 215
      //   #332	-> 218
      //   #334	-> 228
      //   #335	-> 233
      //   #338	-> 245
      //   #364	-> 262
      //   #365	-> 262
      //   #366	-> 267
      //   #367	-> 273
      //   #369	-> 285
      //   #370	-> 290
      //   #371	-> 304
      //   #372	-> 309
      //   #373	-> 321
      //   #374	-> 327
      //   #375	-> 332
      //   #378	-> 353
      //   #410	-> 382
      //   #413	-> 385
      //   #414	-> 390
      //   #416	-> 398
      //   #418	-> 406
      //   #419	-> 414
      //   #418	-> 417
      //   #419	-> 426
      //   #423	-> 431
      //   #427	-> 451
      //   #428	-> 456
      //   #430	-> 466
      //   #431	-> 472
      //   #430	-> 473
      // Exception table:
      //   from	to	target	type
      //   24	35	480	finally
      //   35	51	480	finally
      //   51	59	480	finally
      //   59	65	480	finally
      //   65	80	480	finally
      //   80	91	480	finally
      //   91	105	480	finally
      //   114	121	480	finally
      //   126	143	147	finally
      //   143	146	147	finally
      //   154	163	480	finally
      //   168	185	147	finally
      //   185	188	147	finally
      //   189	196	480	finally
      //   201	212	147	finally
      //   218	228	480	finally
      //   233	245	147	finally
      //   245	262	147	finally
      //   267	273	480	finally
      //   273	285	480	finally
      //   285	290	480	finally
      //   290	304	480	finally
      //   304	309	480	finally
      //   309	321	480	finally
      //   321	327	480	finally
      //   327	332	480	finally
      //   332	339	480	finally
      //   339	353	473	finally
      //   356	382	490	finally
      //   393	398	490	finally
      //   398	406	417	finally
      //   409	414	490	finally
      //   421	426	490	finally
      //   429	431	490	finally
      //   434	451	490	finally
      //   459	466	490	finally
      //   469	472	490	finally
      //   486	488	490	finally
    }
    
    public void onPrepared(int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected stream ");
      stringBuilder.append(param1Int);
      stringBuilder.append(" is prepared");
      Log.e("CameraOfflineSessionImpl", stringBuilder.toString());
    }
    
    public void onRequestQueueEmpty() {
      Log.v("CameraOfflineSessionImpl", "onRequestQueueEmpty");
    }
    
    private void onCaptureErrorLocked(int param1Int, CaptureResultExtras param1CaptureResultExtras) {
      Iterator<Surface> iterator;
      Executor executor;
      int i = param1CaptureResultExtras.getRequestId();
      int j = param1CaptureResultExtras.getSubsequenceId();
      long l = param1CaptureResultExtras.getFrameNumber();
      String str = param1CaptureResultExtras.getErrorPhysicalCameraId();
      CameraOfflineSessionImpl cameraOfflineSessionImpl = CameraOfflineSessionImpl.this;
      CaptureCallbackHolder captureCallbackHolder = (CaptureCallbackHolder)cameraOfflineSessionImpl.mCaptureCallbackMap.get(i);
      boolean bool = false;
      if (captureCallbackHolder == null) {
        Log.e("CameraOfflineSessionImpl", String.format("Receive capture error on unknown request ID %d", new Object[] { Integer.valueOf(i) }));
        return;
      } 
      CaptureRequest captureRequest = captureCallbackHolder.getRequest(j);
      if (param1Int == 5) {
        OutputConfiguration outputConfiguration;
        if (CameraOfflineSessionImpl.this.mRemoteSession == null && !CameraOfflineSessionImpl.this.isClosed()) {
          outputConfiguration = (OutputConfiguration)CameraOfflineSessionImpl.this.mConfiguredOutputs.get(param1CaptureResultExtras.getErrorStreamId());
        } else {
          outputConfiguration = (OutputConfiguration)CameraOfflineSessionImpl.this.mOfflineOutputs.get(param1CaptureResultExtras.getErrorStreamId());
        } 
        if (outputConfiguration == null) {
          param1Int = param1CaptureResultExtras.getErrorStreamId();
          Log.v("CameraOfflineSessionImpl", String.format("Stream %d has been removed. Skipping buffer lost callback", new Object[] { Integer.valueOf(param1Int) }));
          return;
        } 
        CaptureCallbackHolder captureCallbackHolder1;
        for (iterator = outputConfiguration.getSurfaces().iterator(), captureCallbackHolder1 = captureCallbackHolder; iterator.hasNext(); ) {
          Surface surface = iterator.next();
          if (!captureRequest.containsTarget(surface))
            continue; 
          executor = captureCallbackHolder1.getCallback().getExecutor();
          Object object = new Object(this, captureCallbackHolder1, captureRequest, surface, l);
          if (executor != null) {
            long l1 = Binder.clearCallingIdentity();
            try {
              executor.execute((Runnable)object);
            } finally {
              Binder.restoreCallingIdentity(l1);
            } 
          } 
        } 
      } else {
        if (param1Int == 4)
          bool = true; 
        CaptureFailure captureFailure = new CaptureFailure(captureRequest, 0, bool, i, l, (String)iterator);
        Executor executor1 = executor.getCallback().getExecutor();
        Object object = new Object(this, (CaptureCallbackHolder)executor, captureRequest, captureFailure);
        FrameNumberTracker frameNumberTracker = CameraOfflineSessionImpl.this.mFrameNumberTracker;
        param1Int = captureRequest.getRequestType();
        frameNumberTracker.updateTracker(l, true, param1Int);
        CameraOfflineSessionImpl.this.checkAndFireSequenceComplete();
        if (executor1 != null) {
          l = Binder.clearCallingIdentity();
          try {
            executor1.execute((Runnable)object);
          } finally {
            Binder.restoreCallingIdentity(l);
          } 
        } 
      } 
    }
  }
  
  private void checkAndFireSequenceComplete() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mFrameNumberTracker : Landroid/hardware/camera2/impl/FrameNumberTracker;
    //   4: invokevirtual getCompletedFrameNumber : ()J
    //   7: lstore_1
    //   8: aload_0
    //   9: getfield mFrameNumberTracker : Landroid/hardware/camera2/impl/FrameNumberTracker;
    //   12: invokevirtual getCompletedReprocessFrameNumber : ()J
    //   15: lstore_3
    //   16: aload_0
    //   17: getfield mFrameNumberTracker : Landroid/hardware/camera2/impl/FrameNumberTracker;
    //   20: invokevirtual getCompletedZslStillFrameNumber : ()J
    //   23: lstore #5
    //   25: aload_0
    //   26: getfield mOfflineRequestLastFrameNumbersList : Ljava/util/List;
    //   29: astore #7
    //   31: aload #7
    //   33: invokeinterface iterator : ()Ljava/util/Iterator;
    //   38: astore #8
    //   40: aload #8
    //   42: invokeinterface hasNext : ()Z
    //   47: ifeq -> 337
    //   50: aload #8
    //   52: invokeinterface next : ()Ljava/lang/Object;
    //   57: checkcast android/hardware/camera2/impl/RequestLastFrameNumbersHolder
    //   60: astore #9
    //   62: iconst_0
    //   63: istore #10
    //   65: iconst_0
    //   66: istore #11
    //   68: aload #9
    //   70: invokevirtual getRequestId : ()I
    //   73: istore #12
    //   75: aload_0
    //   76: getfield mInterfaceLock : Ljava/lang/Object;
    //   79: astore #13
    //   81: aload #13
    //   83: monitorenter
    //   84: aload_0
    //   85: getfield mCaptureCallbackMap : Landroid/util/SparseArray;
    //   88: iload #12
    //   90: invokevirtual indexOfKey : (I)I
    //   93: istore #14
    //   95: iload #14
    //   97: iflt -> 122
    //   100: aload_0
    //   101: getfield mCaptureCallbackMap : Landroid/util/SparseArray;
    //   104: iload #14
    //   106: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   109: checkcast android/hardware/camera2/impl/CaptureCallbackHolder
    //   112: astore #7
    //   114: goto -> 125
    //   117: astore #7
    //   119: goto -> 326
    //   122: aconst_null
    //   123: astore #7
    //   125: aload #7
    //   127: ifnull -> 211
    //   130: aload #9
    //   132: invokevirtual getLastRegularFrameNumber : ()J
    //   135: lstore #15
    //   137: aload #9
    //   139: invokevirtual getLastReprocessFrameNumber : ()J
    //   142: lstore #17
    //   144: aload #9
    //   146: invokevirtual getLastZslStillFrameNumber : ()J
    //   149: lstore #19
    //   151: aload #7
    //   153: invokevirtual getCallback : ()Landroid/hardware/camera2/impl/CaptureCallback;
    //   156: invokevirtual getExecutor : ()Ljava/util/concurrent/Executor;
    //   159: astore #21
    //   161: aload #7
    //   163: invokevirtual getCallback : ()Landroid/hardware/camera2/impl/CaptureCallback;
    //   166: invokevirtual getSessionCallback : ()Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;
    //   169: astore #22
    //   171: lload #15
    //   173: lload_1
    //   174: lcmp
    //   175: ifgt -> 208
    //   178: lload #17
    //   180: lload_3
    //   181: lcmp
    //   182: ifgt -> 208
    //   185: lload #19
    //   187: lload #5
    //   189: lcmp
    //   190: ifgt -> 208
    //   193: iconst_1
    //   194: istore #11
    //   196: aload_0
    //   197: getfield mCaptureCallbackMap : Landroid/util/SparseArray;
    //   200: iload #14
    //   202: invokevirtual removeAt : (I)V
    //   205: goto -> 208
    //   208: goto -> 221
    //   211: aconst_null
    //   212: astore #21
    //   214: aconst_null
    //   215: astore #22
    //   217: iload #10
    //   219: istore #11
    //   221: aload #13
    //   223: monitorexit
    //   224: aload #7
    //   226: ifnull -> 234
    //   229: iload #11
    //   231: ifeq -> 241
    //   234: aload #8
    //   236: invokeinterface remove : ()V
    //   241: iload #11
    //   243: ifeq -> 321
    //   246: aload #22
    //   248: ifnull -> 321
    //   251: aload #21
    //   253: ifnull -> 321
    //   256: new android/hardware/camera2/impl/CameraOfflineSessionImpl$1
    //   259: dup
    //   260: aload_0
    //   261: aload #22
    //   263: iload #12
    //   265: aload #9
    //   267: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;ILandroid/hardware/camera2/impl/RequestLastFrameNumbersHolder;)V
    //   270: astore #7
    //   272: invokestatic clearCallingIdentity : ()J
    //   275: lstore #19
    //   277: aload #21
    //   279: aload #7
    //   281: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   286: lload #19
    //   288: invokestatic restoreCallingIdentity : (J)V
    //   291: aload_0
    //   292: getfield mCaptureCallbackMap : Landroid/util/SparseArray;
    //   295: invokevirtual size : ()I
    //   298: ifne -> 321
    //   301: aload_0
    //   302: invokevirtual getCallbacks : ()Landroid/hardware/camera2/impl/CameraOfflineSessionImpl$CameraDeviceCallbacks;
    //   305: invokevirtual onDeviceIdle : ()V
    //   308: goto -> 321
    //   311: astore #7
    //   313: lload #19
    //   315: invokestatic restoreCallingIdentity : (J)V
    //   318: aload #7
    //   320: athrow
    //   321: goto -> 40
    //   324: astore #7
    //   326: aload #13
    //   328: monitorexit
    //   329: aload #7
    //   331: athrow
    //   332: astore #7
    //   334: goto -> 326
    //   337: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #551	-> 0
    //   #552	-> 8
    //   #553	-> 16
    //   #554	-> 25
    //   #555	-> 31
    //   #556	-> 40
    //   #557	-> 50
    //   #558	-> 62
    //   #559	-> 68
    //   #563	-> 75
    //   #564	-> 84
    //   #565	-> 95
    //   #566	-> 100
    //   #587	-> 117
    //   #566	-> 122
    //   #567	-> 125
    //   #568	-> 130
    //   #569	-> 130
    //   #570	-> 137
    //   #571	-> 137
    //   #572	-> 144
    //   #573	-> 144
    //   #574	-> 151
    //   #575	-> 161
    //   #577	-> 171
    //   #580	-> 193
    //   #581	-> 196
    //   #577	-> 208
    //   #583	-> 208
    //   #584	-> 211
    //   #585	-> 211
    //   #587	-> 221
    //   #591	-> 224
    //   #592	-> 234
    //   #596	-> 241
    //   #597	-> 256
    //   #607	-> 272
    //   #609	-> 277
    //   #611	-> 286
    //   #612	-> 291
    //   #614	-> 291
    //   #615	-> 301
    //   #611	-> 311
    //   #612	-> 318
    //   #619	-> 321
    //   #587	-> 324
    //   #620	-> 337
    // Exception table:
    //   from	to	target	type
    //   84	95	324	finally
    //   100	114	117	finally
    //   130	137	324	finally
    //   137	144	324	finally
    //   144	151	324	finally
    //   151	161	324	finally
    //   161	171	324	finally
    //   196	205	332	finally
    //   221	224	332	finally
    //   277	286	311	finally
    //   326	329	332	finally
  }
  
  private void removeCompletedCallbackHolderLocked(long paramLong1, long paramLong2, long paramLong3) {
    List<RequestLastFrameNumbersHolder> list = this.mOfflineRequestLastFrameNumbersList;
    Iterator<RequestLastFrameNumbersHolder> iterator = list.iterator();
    while (iterator.hasNext()) {
      RequestLastFrameNumbersHolder requestLastFrameNumbersHolder = iterator.next();
      int i = requestLastFrameNumbersHolder.getRequestId();
      int j = this.mCaptureCallbackMap.indexOfKey(i);
      if (j >= 0) {
        CaptureCallbackHolder captureCallbackHolder = (CaptureCallbackHolder)this.mCaptureCallbackMap.valueAt(j);
      } else {
        list = null;
      } 
      if (list != null) {
        long l1 = requestLastFrameNumbersHolder.getLastRegularFrameNumber();
        long l2 = requestLastFrameNumbersHolder.getLastReprocessFrameNumber();
        long l3 = requestLastFrameNumbersHolder.getLastZslStillFrameNumber();
        if (l1 <= paramLong1 && l2 <= paramLong2 && l3 <= paramLong3) {
          if (requestLastFrameNumbersHolder.isSequenceCompleted()) {
            this.mCaptureCallbackMap.removeAt(j);
            iterator.remove();
            continue;
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Sequence not yet completed for request id ");
          stringBuilder.append(i);
          Log.e("CameraOfflineSessionImpl", stringBuilder.toString());
        } 
      } 
    } 
  }
  
  public void notifyFailedSwitch() {
    synchronized (this.mInterfaceLock) {
      null = new Object();
      super(this);
      long l = Binder.clearCallingIdentity();
      try {
        this.mOfflineExecutor.execute((Runnable)null);
        return;
      } finally {
        Binder.restoreCallingIdentity(l);
      } 
    } 
  }
  
  public void setRemoteSession(ICameraOfflineSession paramICameraOfflineSession) throws CameraAccessException {
    // Byte code:
    //   0: aload_0
    //   1: getfield mInterfaceLock : Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnonnull -> 18
    //   11: aload_0
    //   12: invokevirtual notifyFailedSwitch : ()V
    //   15: aload_2
    //   16: monitorexit
    //   17: return
    //   18: aload_0
    //   19: aload_1
    //   20: putfield mRemoteSession : Landroid/hardware/camera2/ICameraOfflineSession;
    //   23: aload_1
    //   24: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   29: astore_1
    //   30: aload_1
    //   31: ifnull -> 94
    //   34: aload_1
    //   35: aload_0
    //   36: iconst_0
    //   37: invokeinterface linkToDeath : (Landroid/os/IBinder$DeathRecipient;I)V
    //   42: new android/hardware/camera2/impl/CameraOfflineSessionImpl$3
    //   45: astore_1
    //   46: aload_1
    //   47: aload_0
    //   48: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;)V
    //   51: invokestatic clearCallingIdentity : ()J
    //   54: lstore_3
    //   55: aload_0
    //   56: getfield mOfflineExecutor : Ljava/util/concurrent/Executor;
    //   59: aload_1
    //   60: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   65: lload_3
    //   66: invokestatic restoreCallingIdentity : (J)V
    //   69: aload_2
    //   70: monitorexit
    //   71: return
    //   72: astore_1
    //   73: lload_3
    //   74: invokestatic restoreCallingIdentity : (J)V
    //   77: aload_1
    //   78: athrow
    //   79: astore_1
    //   80: new android/hardware/camera2/CameraAccessException
    //   83: astore_1
    //   84: aload_1
    //   85: iconst_2
    //   86: ldc_w 'The camera offline session has encountered a serious error'
    //   89: invokespecial <init> : (ILjava/lang/String;)V
    //   92: aload_1
    //   93: athrow
    //   94: new android/hardware/camera2/CameraAccessException
    //   97: astore_1
    //   98: aload_1
    //   99: iconst_2
    //   100: ldc_w 'The camera offline session has encountered a serious error'
    //   103: invokespecial <init> : (ILjava/lang/String;)V
    //   106: aload_1
    //   107: athrow
    //   108: astore_1
    //   109: aload_2
    //   110: monitorexit
    //   111: aload_1
    //   112: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #700	-> 0
    //   #701	-> 7
    //   #702	-> 11
    //   #703	-> 15
    //   #706	-> 18
    //   #708	-> 23
    //   #709	-> 30
    //   #715	-> 34
    //   #719	-> 42
    //   #721	-> 42
    //   #730	-> 51
    //   #732	-> 55
    //   #734	-> 65
    //   #735	-> 69
    //   #736	-> 69
    //   #737	-> 71
    //   #734	-> 72
    //   #735	-> 77
    //   #716	-> 79
    //   #717	-> 80
    //   #710	-> 94
    //   #736	-> 108
    // Exception table:
    //   from	to	target	type
    //   11	15	108	finally
    //   15	17	108	finally
    //   18	23	108	finally
    //   23	30	108	finally
    //   34	42	79	android/os/RemoteException
    //   34	42	108	finally
    //   42	51	108	finally
    //   51	55	108	finally
    //   55	65	72	finally
    //   65	69	108	finally
    //   69	71	108	finally
    //   73	77	108	finally
    //   77	79	108	finally
    //   80	94	108	finally
    //   94	108	108	finally
    //   109	111	108	finally
  }
  
  private boolean isClosed() {
    return this.mClosing.get();
  }
  
  private void disconnect() {
    synchronized (this.mInterfaceLock) {
      if (this.mClosing.getAndSet(true))
        return; 
      if (this.mRemoteSession != null) {
        this.mRemoteSession.asBinder().unlinkToDeath(this, 0);
        try {
          this.mRemoteSession.disconnect();
        } catch (RemoteException remoteException) {
          Log.e("CameraOfflineSessionImpl", "Exception while disconnecting from offline session: ", (Throwable)remoteException);
        } 
        this.mRemoteSession = null;
        null = new Object();
        super(this);
        long l = Binder.clearCallingIdentity();
        try {
          this.mOfflineExecutor.execute((Runnable)null);
          return;
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Offline session is not yet ready");
      throw illegalStateException;
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      disconnect();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public void binderDied() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CameraOfflineSession on device ");
    stringBuilder.append(this.mCameraId);
    stringBuilder.append(" died unexpectedly");
    Log.w("CameraOfflineSessionImpl", stringBuilder.toString());
    disconnect();
  }
  
  public CameraDevice getDevice() {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public void prepare(Surface paramSurface) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public void prepare(int paramInt, Surface paramSurface) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public void tearDown(Surface paramSurface) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public void finalizeOutputConfigurations(List<OutputConfiguration> paramList) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public int capture(CaptureRequest paramCaptureRequest, CameraCaptureSession.CaptureCallback paramCaptureCallback, Handler paramHandler) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public int captureSingleRequest(CaptureRequest paramCaptureRequest, Executor paramExecutor, CameraCaptureSession.CaptureCallback paramCaptureCallback) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public int captureBurst(List<CaptureRequest> paramList, CameraCaptureSession.CaptureCallback paramCaptureCallback, Handler paramHandler) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public int captureBurstRequests(List<CaptureRequest> paramList, Executor paramExecutor, CameraCaptureSession.CaptureCallback paramCaptureCallback) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public int setRepeatingRequest(CaptureRequest paramCaptureRequest, CameraCaptureSession.CaptureCallback paramCaptureCallback, Handler paramHandler) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public int setSingleRepeatingRequest(CaptureRequest paramCaptureRequest, Executor paramExecutor, CameraCaptureSession.CaptureCallback paramCaptureCallback) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public int setRepeatingBurst(List<CaptureRequest> paramList, CameraCaptureSession.CaptureCallback paramCaptureCallback, Handler paramHandler) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public int setRepeatingBurstRequests(List<CaptureRequest> paramList, Executor paramExecutor, CameraCaptureSession.CaptureCallback paramCaptureCallback) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public void stopRepeating() throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public void abortCaptures() throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public void updateOutputConfiguration(OutputConfiguration paramOutputConfiguration) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public boolean isReprocessable() {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public Surface getInputSurface() {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public CameraOfflineSession switchToOffline(Collection<Surface> paramCollection, Executor paramExecutor, CameraOfflineSession.CameraOfflineSessionCallback paramCameraOfflineSessionCallback) throws CameraAccessException {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public boolean supportsOfflineProcessing(Surface paramSurface) {
    throw new UnsupportedOperationException("Operation not supported in offline mode");
  }
  
  public void close() {
    disconnect();
  }
}
