package android.hardware.camera2.impl;

public interface GetCommand {
  <T> T getValue(CameraMetadataNative paramCameraMetadataNative, CameraMetadataNative.Key<T> paramKey);
}
