package android.hardware.camera2.impl;

import android.app.ActivityThread;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraOfflineSession;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.ICameraDeviceCallbacks;
import android.hardware.camera2.ICameraDeviceUser;
import android.hardware.camera2.OplusCamera2StatisticsManager;
import android.hardware.camera2.OplusCameraManager;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.InputConfiguration;
import android.hardware.camera2.params.OutputConfiguration;
import android.hardware.camera2.params.SessionConfiguration;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.hardware.camera2.utils.SubmitInfo;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.os.ServiceSpecificException;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;
import android.util.Size;
import android.util.SparseArray;
import android.view.Surface;
import com.android.internal.util.function.pooled.PooledLambda;
import com.android.internal.util.function.pooled.PooledRunnable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;

public class CameraDeviceImpl extends CameraDevice implements IBinder.DeathRecipient {
  private final boolean DEBUG = false;
  
  private int customOpMode = 0;
  
  final Object mInterfaceLock = new Object();
  
  private final CameraDeviceCallbacks mCallbacks = new CameraDeviceCallbacks();
  
  private final AtomicBoolean mClosing = new AtomicBoolean();
  
  private boolean mInError = false;
  
  private boolean mIdle = true;
  
  private SparseArray<CaptureCallbackHolder> mCaptureCallbackMap = new SparseArray();
  
  private int mRepeatingRequestId = -1;
  
  private AbstractMap.SimpleEntry<Integer, InputConfiguration> mConfiguredInput = new AbstractMap.SimpleEntry<>(Integer.valueOf(-1), null);
  
  private final SparseArray<OutputConfiguration> mConfiguredOutputs = new SparseArray();
  
  private final HashSet<Integer> mOfflineSupport = new HashSet<>();
  
  private final List<RequestLastFrameNumbersHolder> mRequestLastFrameNumbersList = new ArrayList<>();
  
  private FrameNumberTracker mFrameNumberTracker = new FrameNumberTracker();
  
  private int mNextSessionId = 0;
  
  private boolean mIsPrivilegedApp = false;
  
  private OplusCamera2StatisticsManager mCamera2StatisticsManager = OplusCamera2StatisticsManager.getInstance();
  
  private final Runnable mCallOnOpened = (Runnable)new Object(this);
  
  private final Runnable mCallOnUnconfigured = (Runnable)new Object(this);
  
  private final Runnable mCallOnActive = (Runnable)new Object(this);
  
  private final Runnable mCallOnBusy = (Runnable)new Object(this);
  
  private final Runnable mCallOnClosed = (Runnable)new Object(this);
  
  private final Runnable mCallOnIdle = (Runnable)new Object(this);
  
  private final Runnable mCallOnDisconnected = (Runnable)new Object(this);
  
  private static final long NANO_PER_SECOND = 1000000000L;
  
  private static final int REQUEST_ID_NONE = -1;
  
  private final String TAG;
  
  private final int mAppTargetSdkVersion;
  
  private final String mCameraId;
  
  private final CameraCharacteristics mCharacteristics;
  
  private CameraCaptureSessionCore mCurrentSession;
  
  private final CameraDevice.StateCallback mDeviceCallback;
  
  private final Executor mDeviceExecutor;
  
  private CameraOfflineSessionImpl mOfflineSessionImpl;
  
  private ExecutorService mOfflineSwitchService;
  
  private ICameraDeviceUserWrapper mRemoteDevice;
  
  private int[] mRepeatingRequestTypes;
  
  private volatile StateCallbackKK mSessionStateCallback;
  
  private final int mTotalPartialCount;
  
  public CameraDeviceImpl(String paramString, CameraDevice.StateCallback paramStateCallback, Executor paramExecutor, CameraCharacteristics paramCameraCharacteristics, int paramInt) {
    if (paramString != null && paramStateCallback != null && paramExecutor != null && paramCameraCharacteristics != null) {
      this.mCameraId = paramString;
      this.mDeviceCallback = paramStateCallback;
      this.mDeviceExecutor = paramExecutor;
      this.mCharacteristics = paramCameraCharacteristics;
      this.mAppTargetSdkVersion = paramInt;
      String str = String.format("CameraDevice-JV-%s", new Object[] { paramString });
      paramString = str;
      if (str.length() > 23)
        paramString = str.substring(0, 23); 
      this.TAG = paramString;
      CameraCharacteristics cameraCharacteristics = this.mCharacteristics;
      CameraCharacteristics.Key<Integer> key = CameraCharacteristics.REQUEST_PARTIAL_RESULT_COUNT;
      Integer integer = cameraCharacteristics.<Integer>get(key);
      if (integer == null) {
        this.mTotalPartialCount = 1;
      } else {
        this.mTotalPartialCount = integer.intValue();
      } 
      this.mIsPrivilegedApp = checkPrivilegedAppList();
      return;
    } 
    throw new IllegalArgumentException("Null argument given");
  }
  
  public CameraDeviceCallbacks getCallbacks() {
    return this.mCallbacks;
  }
  
  public void setRemoteDevice(ICameraDeviceUser paramICameraDeviceUser) throws CameraAccessException {
    synchronized (this.mInterfaceLock) {
      if (this.mInError)
        return; 
      ICameraDeviceUserWrapper iCameraDeviceUserWrapper = new ICameraDeviceUserWrapper();
      this(paramICameraDeviceUser);
      this.mRemoteDevice = iCameraDeviceUserWrapper;
      IBinder iBinder = paramICameraDeviceUser.asBinder();
      if (iBinder != null)
        try {
          iBinder.linkToDeath(this, 0);
        } catch (RemoteException remoteException) {
          this.mDeviceExecutor.execute(this.mCallOnDisconnected);
          CameraAccessException cameraAccessException = new CameraAccessException();
          this(2, "The camera device has encountered a serious error");
          throw cameraAccessException;
        }  
      this.mDeviceExecutor.execute(this.mCallOnOpened);
      this.mDeviceExecutor.execute(this.mCallOnUnconfigured);
      return;
    } 
  }
  
  public void setRemoteFailure(ServiceSpecificException paramServiceSpecificException) {
    byte b = 4;
    boolean bool = true;
    int i = paramServiceSpecificException.errorCode;
    if (i != 4) {
      if (i != 10) {
        if (i != 6) {
          if (i != 7) {
            if (i != 8) {
              String str2 = this.TAG;
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Unexpected failure in opening camera device: ");
              stringBuilder.append(paramServiceSpecificException.errorCode);
              stringBuilder.append(paramServiceSpecificException.getMessage());
              String str1 = stringBuilder.toString();
              Log.e(str2, str1);
            } else {
              b = 2;
            } 
          } else {
            b = 1;
          } 
        } else {
          b = 3;
        } 
      } else {
        b = 4;
      } 
    } else {
      bool = false;
    } 
    synchronized (this.mInterfaceLock) {
      this.mInError = true;
      Executor executor = this.mDeviceExecutor;
      Object object = new Object();
      super(this, bool, b);
      executor.execute((Runnable)object);
      return;
    } 
  }
  
  public void setVendorStreamConfigMode(int paramInt) {
    this.customOpMode = paramInt;
  }
  
  public String getId() {
    return this.mCameraId;
  }
  
  public void configureOutputs(List<Surface> paramList) throws CameraAccessException {
    ArrayList<OutputConfiguration> arrayList = new ArrayList(paramList.size());
    for (Surface surface : paramList)
      arrayList.add(new OutputConfiguration(surface)); 
    configureStreamsChecked(null, arrayList, 0, null);
  }
  
  public boolean configureStreamsChecked(InputConfiguration paramInputConfiguration, List<OutputConfiguration> paramList, int paramInt, CaptureRequest paramCaptureRequest) throws CameraAccessException {
    // Byte code:
    //   0: aload_2
    //   1: ifnonnull -> 15
    //   4: new java/util/ArrayList
    //   7: dup
    //   8: invokespecial <init> : ()V
    //   11: astore_2
    //   12: goto -> 15
    //   15: aload_2
    //   16: invokeinterface size : ()I
    //   21: ifne -> 42
    //   24: aload_1
    //   25: ifnonnull -> 31
    //   28: goto -> 42
    //   31: new java/lang/IllegalArgumentException
    //   34: dup
    //   35: ldc_w 'cannot configure an input stream without any output streams'
    //   38: invokespecial <init> : (Ljava/lang/String;)V
    //   41: athrow
    //   42: aload_0
    //   43: aload_1
    //   44: invokespecial checkInputConfiguration : (Landroid/hardware/camera2/params/InputConfiguration;)V
    //   47: aload_0
    //   48: getfield mInterfaceLock : Ljava/lang/Object;
    //   51: astore #5
    //   53: aload #5
    //   55: monitorenter
    //   56: aload_0
    //   57: invokespecial checkIfCameraClosedOrInError : ()V
    //   60: new java/util/HashSet
    //   63: astore #6
    //   65: aload #6
    //   67: aload_2
    //   68: invokespecial <init> : (Ljava/util/Collection;)V
    //   71: new java/util/ArrayList
    //   74: astore #7
    //   76: aload #7
    //   78: invokespecial <init> : ()V
    //   81: iconst_0
    //   82: istore #8
    //   84: iload #8
    //   86: aload_0
    //   87: getfield mConfiguredOutputs : Landroid/util/SparseArray;
    //   90: invokevirtual size : ()I
    //   93: if_icmpge -> 173
    //   96: aload_0
    //   97: getfield mConfiguredOutputs : Landroid/util/SparseArray;
    //   100: iload #8
    //   102: invokevirtual keyAt : (I)I
    //   105: istore #9
    //   107: aload_0
    //   108: getfield mConfiguredOutputs : Landroid/util/SparseArray;
    //   111: iload #8
    //   113: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   116: checkcast android/hardware/camera2/params/OutputConfiguration
    //   119: astore #10
    //   121: aload_2
    //   122: aload #10
    //   124: invokeinterface contains : (Ljava/lang/Object;)Z
    //   129: ifeq -> 154
    //   132: aload #10
    //   134: invokevirtual isDeferredConfiguration : ()Z
    //   137: ifeq -> 143
    //   140: goto -> 154
    //   143: aload #6
    //   145: aload #10
    //   147: invokevirtual remove : (Ljava/lang/Object;)Z
    //   150: pop
    //   151: goto -> 167
    //   154: aload #7
    //   156: iload #9
    //   158: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   161: invokeinterface add : (Ljava/lang/Object;)Z
    //   166: pop
    //   167: iinc #8, 1
    //   170: goto -> 84
    //   173: aload_0
    //   174: getfield mDeviceExecutor : Ljava/util/concurrent/Executor;
    //   177: aload_0
    //   178: getfield mCallOnBusy : Ljava/lang/Runnable;
    //   181: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   186: aload_0
    //   187: invokevirtual stopRepeating : ()V
    //   190: aload_0
    //   191: invokespecial waitUntilIdle : ()V
    //   194: aload_0
    //   195: getfield mRemoteDevice : Landroid/hardware/camera2/impl/ICameraDeviceUserWrapper;
    //   198: invokevirtual beginConfigure : ()V
    //   201: aload_0
    //   202: getfield mConfiguredInput : Ljava/util/AbstractMap$SimpleEntry;
    //   205: invokevirtual getValue : ()Ljava/lang/Object;
    //   208: checkcast android/hardware/camera2/params/InputConfiguration
    //   211: astore #10
    //   213: aload_1
    //   214: aload #10
    //   216: if_acmpeq -> 341
    //   219: aload_1
    //   220: ifnull -> 232
    //   223: aload_1
    //   224: aload #10
    //   226: invokevirtual equals : (Ljava/lang/Object;)Z
    //   229: ifne -> 341
    //   232: aload #10
    //   234: ifnull -> 278
    //   237: aload_0
    //   238: getfield mRemoteDevice : Landroid/hardware/camera2/impl/ICameraDeviceUserWrapper;
    //   241: aload_0
    //   242: getfield mConfiguredInput : Ljava/util/AbstractMap$SimpleEntry;
    //   245: invokevirtual getKey : ()Ljava/lang/Object;
    //   248: checkcast java/lang/Integer
    //   251: invokevirtual intValue : ()I
    //   254: invokevirtual deleteStream : (I)V
    //   257: new java/util/AbstractMap$SimpleEntry
    //   260: astore #10
    //   262: aload #10
    //   264: iconst_m1
    //   265: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   268: aconst_null
    //   269: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   272: aload_0
    //   273: aload #10
    //   275: putfield mConfiguredInput : Ljava/util/AbstractMap$SimpleEntry;
    //   278: aload_1
    //   279: ifnull -> 341
    //   282: aload_0
    //   283: getfield mRemoteDevice : Landroid/hardware/camera2/impl/ICameraDeviceUserWrapper;
    //   286: astore #10
    //   288: aload_1
    //   289: invokevirtual getWidth : ()I
    //   292: istore #8
    //   294: aload_1
    //   295: invokevirtual getHeight : ()I
    //   298: istore #11
    //   300: aload_1
    //   301: invokevirtual getFormat : ()I
    //   304: istore #9
    //   306: aload #10
    //   308: iload #8
    //   310: iload #11
    //   312: iload #9
    //   314: invokevirtual createInputStream : (III)I
    //   317: istore #8
    //   319: new java/util/AbstractMap$SimpleEntry
    //   322: astore #10
    //   324: aload #10
    //   326: iload #8
    //   328: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   331: aload_1
    //   332: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
    //   335: aload_0
    //   336: aload #10
    //   338: putfield mConfiguredInput : Ljava/util/AbstractMap$SimpleEntry;
    //   341: aload #7
    //   343: invokeinterface iterator : ()Ljava/util/Iterator;
    //   348: astore_1
    //   349: aload_1
    //   350: invokeinterface hasNext : ()Z
    //   355: ifeq -> 396
    //   358: aload_1
    //   359: invokeinterface next : ()Ljava/lang/Object;
    //   364: checkcast java/lang/Integer
    //   367: astore #7
    //   369: aload_0
    //   370: getfield mRemoteDevice : Landroid/hardware/camera2/impl/ICameraDeviceUserWrapper;
    //   373: aload #7
    //   375: invokevirtual intValue : ()I
    //   378: invokevirtual deleteStream : (I)V
    //   381: aload_0
    //   382: getfield mConfiguredOutputs : Landroid/util/SparseArray;
    //   385: aload #7
    //   387: invokevirtual intValue : ()I
    //   390: invokevirtual delete : (I)V
    //   393: goto -> 349
    //   396: aload_2
    //   397: invokeinterface iterator : ()Ljava/util/Iterator;
    //   402: astore_1
    //   403: aload_1
    //   404: invokeinterface hasNext : ()Z
    //   409: ifeq -> 458
    //   412: aload_1
    //   413: invokeinterface next : ()Ljava/lang/Object;
    //   418: checkcast android/hardware/camera2/params/OutputConfiguration
    //   421: astore #7
    //   423: aload #6
    //   425: aload #7
    //   427: invokevirtual contains : (Ljava/lang/Object;)Z
    //   430: ifeq -> 455
    //   433: aload_0
    //   434: getfield mRemoteDevice : Landroid/hardware/camera2/impl/ICameraDeviceUserWrapper;
    //   437: aload #7
    //   439: invokevirtual createStream : (Landroid/hardware/camera2/params/OutputConfiguration;)I
    //   442: istore #8
    //   444: aload_0
    //   445: getfield mConfiguredOutputs : Landroid/util/SparseArray;
    //   448: iload #8
    //   450: aload #7
    //   452: invokevirtual put : (ILjava/lang/Object;)V
    //   455: goto -> 403
    //   458: aload_0
    //   459: getfield customOpMode : I
    //   462: istore #8
    //   464: iload_3
    //   465: iload #8
    //   467: bipush #16
    //   469: ishl
    //   470: ior
    //   471: istore_3
    //   472: aload #4
    //   474: ifnull -> 500
    //   477: aload_0
    //   478: getfield mRemoteDevice : Landroid/hardware/camera2/impl/ICameraDeviceUserWrapper;
    //   481: astore_1
    //   482: aload #4
    //   484: invokevirtual getNativeCopy : ()Landroid/hardware/camera2/impl/CameraMetadataNative;
    //   487: astore #4
    //   489: aload_1
    //   490: iload_3
    //   491: aload #4
    //   493: invokevirtual endConfigure : (ILandroid/hardware/camera2/impl/CameraMetadataNative;)[I
    //   496: astore_1
    //   497: goto -> 510
    //   500: aload_0
    //   501: getfield mRemoteDevice : Landroid/hardware/camera2/impl/ICameraDeviceUserWrapper;
    //   504: iload_3
    //   505: aconst_null
    //   506: invokevirtual endConfigure : (ILandroid/hardware/camera2/impl/CameraMetadataNative;)[I
    //   509: astore_1
    //   510: aload_0
    //   511: getfield mOfflineSupport : Ljava/util/HashSet;
    //   514: invokevirtual clear : ()V
    //   517: aload_1
    //   518: ifnull -> 562
    //   521: aload_1
    //   522: arraylength
    //   523: ifle -> 562
    //   526: aload_1
    //   527: arraylength
    //   528: istore #8
    //   530: iconst_0
    //   531: istore_3
    //   532: iload_3
    //   533: iload #8
    //   535: if_icmpge -> 562
    //   538: aload_1
    //   539: iload_3
    //   540: iaload
    //   541: istore #9
    //   543: aload_0
    //   544: getfield mOfflineSupport : Ljava/util/HashSet;
    //   547: iload #9
    //   549: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   552: invokevirtual add : (Ljava/lang/Object;)Z
    //   555: pop
    //   556: iinc #3, 1
    //   559: goto -> 532
    //   562: iconst_1
    //   563: ifeq -> 591
    //   566: aload_2
    //   567: invokeinterface size : ()I
    //   572: ifle -> 591
    //   575: aload_0
    //   576: getfield mDeviceExecutor : Ljava/util/concurrent/Executor;
    //   579: aload_0
    //   580: getfield mCallOnIdle : Ljava/lang/Runnable;
    //   583: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   588: goto -> 604
    //   591: aload_0
    //   592: getfield mDeviceExecutor : Ljava/util/concurrent/Executor;
    //   595: aload_0
    //   596: getfield mCallOnUnconfigured : Ljava/lang/Runnable;
    //   599: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   604: aload #5
    //   606: monitorexit
    //   607: iconst_1
    //   608: ireturn
    //   609: astore_1
    //   610: goto -> 622
    //   613: astore_1
    //   614: goto -> 650
    //   617: astore_1
    //   618: goto -> 744
    //   621: astore_1
    //   622: aload_1
    //   623: invokevirtual getReason : ()I
    //   626: iconst_4
    //   627: if_icmpne -> 647
    //   630: new java/lang/IllegalStateException
    //   633: astore #4
    //   635: aload #4
    //   637: ldc_w 'The camera is currently busy. You must wait until the previous operation completes.'
    //   640: aload_1
    //   641: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   644: aload #4
    //   646: athrow
    //   647: aload_1
    //   648: athrow
    //   649: astore_1
    //   650: aload_0
    //   651: getfield TAG : Ljava/lang/String;
    //   654: astore #6
    //   656: new java/lang/StringBuilder
    //   659: astore #4
    //   661: aload #4
    //   663: invokespecial <init> : ()V
    //   666: aload #4
    //   668: ldc_w 'Stream configuration failed due to: '
    //   671: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   674: pop
    //   675: aload #4
    //   677: aload_1
    //   678: invokevirtual getMessage : ()Ljava/lang/String;
    //   681: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   684: pop
    //   685: aload #6
    //   687: aload #4
    //   689: invokevirtual toString : ()Ljava/lang/String;
    //   692: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   695: pop
    //   696: iconst_0
    //   697: ifeq -> 725
    //   700: aload_2
    //   701: invokeinterface size : ()I
    //   706: ifle -> 725
    //   709: aload_0
    //   710: getfield mDeviceExecutor : Ljava/util/concurrent/Executor;
    //   713: aload_0
    //   714: getfield mCallOnIdle : Ljava/lang/Runnable;
    //   717: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   722: goto -> 738
    //   725: aload_0
    //   726: getfield mDeviceExecutor : Ljava/util/concurrent/Executor;
    //   729: aload_0
    //   730: getfield mCallOnUnconfigured : Ljava/lang/Runnable;
    //   733: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   738: aload #5
    //   740: monitorexit
    //   741: iconst_0
    //   742: ireturn
    //   743: astore_1
    //   744: iconst_0
    //   745: ifeq -> 773
    //   748: aload_2
    //   749: invokeinterface size : ()I
    //   754: ifle -> 773
    //   757: aload_0
    //   758: getfield mDeviceExecutor : Ljava/util/concurrent/Executor;
    //   761: aload_0
    //   762: getfield mCallOnIdle : Ljava/lang/Runnable;
    //   765: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   770: goto -> 786
    //   773: aload_0
    //   774: getfield mDeviceExecutor : Ljava/util/concurrent/Executor;
    //   777: aload_0
    //   778: getfield mCallOnUnconfigured : Ljava/lang/Runnable;
    //   781: invokeinterface execute : (Ljava/lang/Runnable;)V
    //   786: aload_1
    //   787: athrow
    //   788: astore_1
    //   789: aload #5
    //   791: monitorexit
    //   792: aload_1
    //   793: athrow
    //   794: astore_1
    //   795: goto -> 789
    // Line number table:
    //   Java source line number -> byte code offset
    //   #431	-> 0
    //   #432	-> 4
    //   #431	-> 15
    //   #434	-> 15
    //   #435	-> 31
    //   #439	-> 42
    //   #441	-> 47
    //   #443	-> 47
    //   #444	-> 56
    //   #446	-> 60
    //   #448	-> 71
    //   #451	-> 81
    //   #452	-> 96
    //   #453	-> 107
    //   #455	-> 121
    //   #461	-> 143
    //   #459	-> 154
    //   #451	-> 167
    //   #465	-> 173
    //   #466	-> 186
    //   #469	-> 190
    //   #471	-> 194
    //   #474	-> 201
    //   #475	-> 213
    //   #476	-> 223
    //   #477	-> 232
    //   #478	-> 237
    //   #479	-> 257
    //   #480	-> 262
    //   #482	-> 278
    //   #483	-> 282
    //   #484	-> 294
    //   #483	-> 306
    //   #485	-> 319
    //   #486	-> 324
    //   #491	-> 341
    //   #492	-> 369
    //   #493	-> 381
    //   #494	-> 393
    //   #497	-> 396
    //   #498	-> 423
    //   #499	-> 433
    //   #500	-> 444
    //   #502	-> 455
    //   #503	-> 458
    //   #506	-> 472
    //   #507	-> 477
    //   #508	-> 482
    //   #507	-> 489
    //   #510	-> 500
    //   #513	-> 510
    //   #514	-> 517
    //   #515	-> 526
    //   #516	-> 543
    //   #515	-> 556
    //   #520	-> 562
    //   #533	-> 562
    //   #534	-> 575
    //   #537	-> 591
    //   #539	-> 604
    //   #540	-> 604
    //   #542	-> 607
    //   #526	-> 609
    //   #521	-> 613
    //   #533	-> 617
    //   #526	-> 621
    //   #527	-> 622
    //   #528	-> 630
    //   #531	-> 647
    //   #521	-> 649
    //   #524	-> 650
    //   #525	-> 696
    //   #533	-> 696
    //   #534	-> 709
    //   #537	-> 725
    //   #525	-> 741
    //   #533	-> 743
    //   #534	-> 757
    //   #537	-> 773
    //   #539	-> 786
    //   #540	-> 788
    // Exception table:
    //   from	to	target	type
    //   56	60	788	finally
    //   60	71	788	finally
    //   71	81	788	finally
    //   84	96	788	finally
    //   96	107	788	finally
    //   107	121	788	finally
    //   121	140	788	finally
    //   143	151	788	finally
    //   154	167	788	finally
    //   173	186	788	finally
    //   186	190	788	finally
    //   190	194	649	java/lang/IllegalArgumentException
    //   190	194	621	android/hardware/camera2/CameraAccessException
    //   190	194	617	finally
    //   194	201	649	java/lang/IllegalArgumentException
    //   194	201	621	android/hardware/camera2/CameraAccessException
    //   194	201	617	finally
    //   201	213	649	java/lang/IllegalArgumentException
    //   201	213	621	android/hardware/camera2/CameraAccessException
    //   201	213	617	finally
    //   223	232	649	java/lang/IllegalArgumentException
    //   223	232	621	android/hardware/camera2/CameraAccessException
    //   223	232	617	finally
    //   237	257	649	java/lang/IllegalArgumentException
    //   237	257	621	android/hardware/camera2/CameraAccessException
    //   237	257	617	finally
    //   257	262	649	java/lang/IllegalArgumentException
    //   257	262	621	android/hardware/camera2/CameraAccessException
    //   257	262	617	finally
    //   262	278	649	java/lang/IllegalArgumentException
    //   262	278	621	android/hardware/camera2/CameraAccessException
    //   262	278	617	finally
    //   282	294	649	java/lang/IllegalArgumentException
    //   282	294	621	android/hardware/camera2/CameraAccessException
    //   282	294	617	finally
    //   294	306	649	java/lang/IllegalArgumentException
    //   294	306	621	android/hardware/camera2/CameraAccessException
    //   294	306	617	finally
    //   306	319	649	java/lang/IllegalArgumentException
    //   306	319	621	android/hardware/camera2/CameraAccessException
    //   306	319	617	finally
    //   319	324	649	java/lang/IllegalArgumentException
    //   319	324	621	android/hardware/camera2/CameraAccessException
    //   319	324	617	finally
    //   324	341	649	java/lang/IllegalArgumentException
    //   324	341	621	android/hardware/camera2/CameraAccessException
    //   324	341	617	finally
    //   341	349	649	java/lang/IllegalArgumentException
    //   341	349	621	android/hardware/camera2/CameraAccessException
    //   341	349	617	finally
    //   349	369	649	java/lang/IllegalArgumentException
    //   349	369	621	android/hardware/camera2/CameraAccessException
    //   349	369	617	finally
    //   369	381	649	java/lang/IllegalArgumentException
    //   369	381	621	android/hardware/camera2/CameraAccessException
    //   369	381	617	finally
    //   381	393	649	java/lang/IllegalArgumentException
    //   381	393	621	android/hardware/camera2/CameraAccessException
    //   381	393	617	finally
    //   396	403	649	java/lang/IllegalArgumentException
    //   396	403	621	android/hardware/camera2/CameraAccessException
    //   396	403	617	finally
    //   403	423	649	java/lang/IllegalArgumentException
    //   403	423	621	android/hardware/camera2/CameraAccessException
    //   403	423	617	finally
    //   423	433	649	java/lang/IllegalArgumentException
    //   423	433	621	android/hardware/camera2/CameraAccessException
    //   423	433	617	finally
    //   433	444	649	java/lang/IllegalArgumentException
    //   433	444	621	android/hardware/camera2/CameraAccessException
    //   433	444	617	finally
    //   444	455	649	java/lang/IllegalArgumentException
    //   444	455	621	android/hardware/camera2/CameraAccessException
    //   444	455	617	finally
    //   458	464	649	java/lang/IllegalArgumentException
    //   458	464	621	android/hardware/camera2/CameraAccessException
    //   458	464	617	finally
    //   477	482	613	java/lang/IllegalArgumentException
    //   477	482	609	android/hardware/camera2/CameraAccessException
    //   477	482	743	finally
    //   482	489	613	java/lang/IllegalArgumentException
    //   482	489	609	android/hardware/camera2/CameraAccessException
    //   482	489	743	finally
    //   489	497	613	java/lang/IllegalArgumentException
    //   489	497	609	android/hardware/camera2/CameraAccessException
    //   489	497	743	finally
    //   500	510	613	java/lang/IllegalArgumentException
    //   500	510	609	android/hardware/camera2/CameraAccessException
    //   500	510	743	finally
    //   510	517	613	java/lang/IllegalArgumentException
    //   510	517	609	android/hardware/camera2/CameraAccessException
    //   510	517	743	finally
    //   521	526	613	java/lang/IllegalArgumentException
    //   521	526	609	android/hardware/camera2/CameraAccessException
    //   521	526	743	finally
    //   526	530	613	java/lang/IllegalArgumentException
    //   526	530	609	android/hardware/camera2/CameraAccessException
    //   526	530	743	finally
    //   543	556	613	java/lang/IllegalArgumentException
    //   543	556	609	android/hardware/camera2/CameraAccessException
    //   543	556	743	finally
    //   566	575	794	finally
    //   575	588	794	finally
    //   591	604	794	finally
    //   604	607	794	finally
    //   622	630	743	finally
    //   630	647	743	finally
    //   647	649	743	finally
    //   650	696	743	finally
    //   700	709	794	finally
    //   709	722	794	finally
    //   725	738	794	finally
    //   738	741	794	finally
    //   748	757	794	finally
    //   757	770	794	finally
    //   773	786	794	finally
    //   786	788	794	finally
    //   789	792	794	finally
  }
  
  public void createCaptureSession(List<Surface> paramList, CameraCaptureSession.StateCallback paramStateCallback, Handler paramHandler) throws CameraAccessException {
    ArrayList<OutputConfiguration> arrayList = new ArrayList(paramList.size());
    for (Surface surface : paramList)
      arrayList.add(new OutputConfiguration(surface)); 
    Executor executor = checkAndWrapHandler(paramHandler);
    createCaptureSessionInternal(null, arrayList, paramStateCallback, executor, 0, null);
  }
  
  public void createCaptureSessionByOutputConfigurations(List<OutputConfiguration> paramList, CameraCaptureSession.StateCallback paramStateCallback, Handler paramHandler) throws CameraAccessException {
    paramList = new ArrayList<>(paramList);
    createCaptureSessionInternal(null, paramList, paramStateCallback, checkAndWrapHandler(paramHandler), 0, null);
  }
  
  public void createReprocessableCaptureSession(InputConfiguration paramInputConfiguration, List<Surface> paramList, CameraCaptureSession.StateCallback paramStateCallback, Handler paramHandler) throws CameraAccessException {
    if (paramInputConfiguration != null) {
      ArrayList<OutputConfiguration> arrayList = new ArrayList(paramList.size());
      for (Surface surface : paramList)
        arrayList.add(new OutputConfiguration(surface)); 
      Executor executor = checkAndWrapHandler(paramHandler);
      createCaptureSessionInternal(paramInputConfiguration, arrayList, paramStateCallback, executor, 0, null);
      return;
    } 
    throw new IllegalArgumentException("inputConfig cannot be null when creating a reprocessable capture session");
  }
  
  public void createReprocessableCaptureSessionByConfigurations(InputConfiguration paramInputConfiguration, List<OutputConfiguration> paramList, CameraCaptureSession.StateCallback paramStateCallback, Handler paramHandler) throws CameraAccessException {
    if (paramInputConfiguration != null) {
      if (paramList != null) {
        ArrayList<OutputConfiguration> arrayList = new ArrayList();
        for (OutputConfiguration outputConfiguration : paramList)
          arrayList.add(new OutputConfiguration(outputConfiguration)); 
        Executor executor = checkAndWrapHandler(paramHandler);
        createCaptureSessionInternal(paramInputConfiguration, arrayList, paramStateCallback, executor, 0, null);
        return;
      } 
      throw new IllegalArgumentException("Output configurations cannot be null when creating a reprocessable capture session");
    } 
    throw new IllegalArgumentException("inputConfig cannot be null when creating a reprocessable capture session");
  }
  
  public void createConstrainedHighSpeedCaptureSession(List<Surface> paramList, CameraCaptureSession.StateCallback paramStateCallback, Handler paramHandler) throws CameraAccessException {
    if (paramList != null && paramList.size() != 0 && paramList.size() <= 2) {
      ArrayList<OutputConfiguration> arrayList = new ArrayList(paramList.size());
      for (Surface surface : paramList)
        arrayList.add(new OutputConfiguration(surface)); 
      Executor executor = checkAndWrapHandler(paramHandler);
      createCaptureSessionInternal(null, arrayList, paramStateCallback, executor, 1, null);
      return;
    } 
    throw new IllegalArgumentException("Output surface list must not be null and the size must be no more than 2");
  }
  
  public void createCustomCaptureSession(InputConfiguration paramInputConfiguration, List<OutputConfiguration> paramList, int paramInt, CameraCaptureSession.StateCallback paramStateCallback, Handler paramHandler) throws CameraAccessException {
    ArrayList<OutputConfiguration> arrayList = new ArrayList();
    for (OutputConfiguration outputConfiguration : paramList)
      arrayList.add(new OutputConfiguration(outputConfiguration)); 
    Executor executor = checkAndWrapHandler(paramHandler);
    createCaptureSessionInternal(paramInputConfiguration, arrayList, paramStateCallback, executor, paramInt, null);
  }
  
  public void createCaptureSession(SessionConfiguration paramSessionConfiguration) throws CameraAccessException {
    if (paramSessionConfiguration != null) {
      List<OutputConfiguration> list = paramSessionConfiguration.getOutputConfigurations();
      if (list != null) {
        if (paramSessionConfiguration.getExecutor() != null) {
          InputConfiguration inputConfiguration = paramSessionConfiguration.getInputConfiguration();
          CameraCaptureSession.StateCallback stateCallback = paramSessionConfiguration.getStateCallback();
          Executor executor = paramSessionConfiguration.getExecutor();
          int i = paramSessionConfiguration.getSessionType();
          CaptureRequest captureRequest = paramSessionConfiguration.getSessionParameters();
          createCaptureSessionInternal(inputConfiguration, list, stateCallback, executor, i, captureRequest);
          return;
        } 
        throw new IllegalArgumentException("Invalid executor");
      } 
      throw new IllegalArgumentException("Invalid output configurations");
    } 
    throw new IllegalArgumentException("Invalid session configuration");
  }
  
  private void createCaptureSessionInternal(InputConfiguration paramInputConfiguration, List<OutputConfiguration> paramList, CameraCaptureSession.StateCallback paramStateCallback, Executor paramExecutor, int paramInt, CaptureRequest paramCaptureRequest) throws CameraAccessException {
    // Byte code:
    //   0: aload_0
    //   1: getfield mInterfaceLock : Ljava/lang/Object;
    //   4: astore #7
    //   6: aload #7
    //   8: monitorenter
    //   9: aload_0
    //   10: invokespecial checkIfCameraClosedOrInError : ()V
    //   13: iload #5
    //   15: iconst_1
    //   16: if_icmpne -> 25
    //   19: iconst_1
    //   20: istore #8
    //   22: goto -> 28
    //   25: iconst_0
    //   26: istore #8
    //   28: iload #8
    //   30: ifeq -> 53
    //   33: aload_1
    //   34: ifnonnull -> 40
    //   37: goto -> 53
    //   40: new java/lang/IllegalArgumentException
    //   43: astore_1
    //   44: aload_1
    //   45: ldc_w 'Constrained high speed session doesn't support input configuration yet.'
    //   48: invokespecial <init> : (Ljava/lang/String;)V
    //   51: aload_1
    //   52: athrow
    //   53: aload_0
    //   54: getfield mCurrentSession : Landroid/hardware/camera2/impl/CameraCaptureSessionCore;
    //   57: ifnull -> 69
    //   60: aload_0
    //   61: getfield mCurrentSession : Landroid/hardware/camera2/impl/CameraCaptureSessionCore;
    //   64: invokeinterface replaceSessionClose : ()V
    //   69: aconst_null
    //   70: astore #9
    //   72: invokestatic getInstance : ()Landroid/hardware/camera2/OplusCameraManager;
    //   75: aload #6
    //   77: invokevirtual parseSessionParameters : (Landroid/hardware/camera2/CaptureRequest;)V
    //   80: aload_0
    //   81: aload_1
    //   82: aload_2
    //   83: iload #5
    //   85: aload #6
    //   87: invokevirtual configureStreamsChecked : (Landroid/hardware/camera2/params/InputConfiguration;Ljava/util/List;ILandroid/hardware/camera2/CaptureRequest;)Z
    //   90: istore #10
    //   92: aload #9
    //   94: astore #6
    //   96: iload #10
    //   98: iconst_1
    //   99: if_icmpne -> 119
    //   102: aload #9
    //   104: astore #6
    //   106: aload_1
    //   107: ifnull -> 119
    //   110: aload_0
    //   111: getfield mRemoteDevice : Landroid/hardware/camera2/impl/ICameraDeviceUserWrapper;
    //   114: invokevirtual getInputSurface : ()Landroid/view/Surface;
    //   117: astore #6
    //   119: aconst_null
    //   120: astore_1
    //   121: goto -> 131
    //   124: astore_1
    //   125: iconst_0
    //   126: istore #10
    //   128: aconst_null
    //   129: astore #6
    //   131: iload #8
    //   133: ifeq -> 256
    //   136: new java/util/ArrayList
    //   139: astore #6
    //   141: aload #6
    //   143: aload_2
    //   144: invokeinterface size : ()I
    //   149: invokespecial <init> : (I)V
    //   152: aload_2
    //   153: invokeinterface iterator : ()Ljava/util/Iterator;
    //   158: astore #9
    //   160: aload #9
    //   162: invokeinterface hasNext : ()Z
    //   167: ifeq -> 194
    //   170: aload #9
    //   172: invokeinterface next : ()Ljava/lang/Object;
    //   177: checkcast android/hardware/camera2/params/OutputConfiguration
    //   180: astore_2
    //   181: aload #6
    //   183: aload_2
    //   184: invokevirtual getSurface : ()Landroid/view/Surface;
    //   187: invokevirtual add : (Ljava/lang/Object;)Z
    //   190: pop
    //   191: goto -> 160
    //   194: aload_0
    //   195: invokespecial getCharacteristics : ()Landroid/hardware/camera2/CameraCharacteristics;
    //   198: getstatic android/hardware/camera2/CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   201: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   204: checkcast android/hardware/camera2/params/StreamConfigurationMap
    //   207: astore_2
    //   208: aload #6
    //   210: aconst_null
    //   211: aload_2
    //   212: invokestatic checkConstrainedHighSpeedSurfaces : (Ljava/util/Collection;Landroid/util/Range;Landroid/hardware/camera2/params/StreamConfigurationMap;)V
    //   215: new android/hardware/camera2/impl/CameraConstrainedHighSpeedCaptureSessionImpl
    //   218: astore_2
    //   219: aload_0
    //   220: getfield mNextSessionId : I
    //   223: istore #5
    //   225: aload_0
    //   226: iload #5
    //   228: iconst_1
    //   229: iadd
    //   230: putfield mNextSessionId : I
    //   233: aload_2
    //   234: iload #5
    //   236: aload_3
    //   237: aload #4
    //   239: aload_0
    //   240: aload_0
    //   241: getfield mDeviceExecutor : Ljava/util/concurrent/Executor;
    //   244: iload #10
    //   246: aload_0
    //   247: getfield mCharacteristics : Landroid/hardware/camera2/CameraCharacteristics;
    //   250: invokespecial <init> : (ILandroid/hardware/camera2/CameraCaptureSession$StateCallback;Ljava/util/concurrent/Executor;Landroid/hardware/camera2/impl/CameraDeviceImpl;Ljava/util/concurrent/Executor;ZLandroid/hardware/camera2/CameraCharacteristics;)V
    //   253: goto -> 292
    //   256: aload_0
    //   257: getfield mNextSessionId : I
    //   260: istore #5
    //   262: aload_0
    //   263: iload #5
    //   265: iconst_1
    //   266: iadd
    //   267: putfield mNextSessionId : I
    //   270: new android/hardware/camera2/impl/CameraCaptureSessionImpl
    //   273: dup
    //   274: iload #5
    //   276: aload #6
    //   278: aload_3
    //   279: aload #4
    //   281: aload_0
    //   282: aload_0
    //   283: getfield mDeviceExecutor : Ljava/util/concurrent/Executor;
    //   286: iload #10
    //   288: invokespecial <init> : (ILandroid/view/Surface;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;Ljava/util/concurrent/Executor;Landroid/hardware/camera2/impl/CameraDeviceImpl;Ljava/util/concurrent/Executor;Z)V
    //   291: astore_2
    //   292: aload_0
    //   293: aload_2
    //   294: putfield mCurrentSession : Landroid/hardware/camera2/impl/CameraCaptureSessionCore;
    //   297: aload_1
    //   298: ifnonnull -> 315
    //   301: aload_0
    //   302: aload_2
    //   303: invokeinterface getDeviceStateCallback : ()Landroid/hardware/camera2/impl/CameraDeviceImpl$StateCallbackKK;
    //   308: putfield mSessionStateCallback : Landroid/hardware/camera2/impl/CameraDeviceImpl$StateCallbackKK;
    //   311: aload #7
    //   313: monitorexit
    //   314: return
    //   315: aload_1
    //   316: athrow
    //   317: astore_1
    //   318: aload #7
    //   320: monitorexit
    //   321: aload_1
    //   322: athrow
    //   323: astore_1
    //   324: goto -> 318
    // Line number table:
    //   Java source line number -> byte code offset
    //   #679	-> 0
    //   #684	-> 9
    //   #686	-> 13
    //   #688	-> 28
    //   #689	-> 40
    //   #695	-> 53
    //   #696	-> 60
    //   #700	-> 69
    //   #701	-> 69
    //   #702	-> 69
    //   #705	-> 72
    //   #709	-> 80
    //   #711	-> 92
    //   #712	-> 110
    //   #721	-> 119
    //   #714	-> 124
    //   #715	-> 125
    //   #716	-> 125
    //   #717	-> 125
    //   #724	-> 131
    //   #725	-> 131
    //   #726	-> 136
    //   #727	-> 152
    //   #728	-> 181
    //   #729	-> 191
    //   #730	-> 194
    //   #731	-> 194
    //   #732	-> 208
    //   #734	-> 215
    //   #737	-> 253
    //   #738	-> 256
    //   #743	-> 292
    //   #745	-> 297
    //   #749	-> 301
    //   #750	-> 311
    //   #751	-> 314
    //   #746	-> 315
    //   #750	-> 317
    // Exception table:
    //   from	to	target	type
    //   9	13	317	finally
    //   40	53	317	finally
    //   53	60	317	finally
    //   60	69	317	finally
    //   72	80	317	finally
    //   80	92	124	android/hardware/camera2/CameraAccessException
    //   80	92	323	finally
    //   110	119	124	android/hardware/camera2/CameraAccessException
    //   110	119	323	finally
    //   136	152	323	finally
    //   152	160	323	finally
    //   160	181	323	finally
    //   181	191	323	finally
    //   194	208	323	finally
    //   208	215	323	finally
    //   215	253	323	finally
    //   256	292	323	finally
    //   292	297	323	finally
    //   301	311	323	finally
    //   311	314	323	finally
    //   315	317	323	finally
    //   318	321	323	finally
  }
  
  public boolean isSessionConfigurationSupported(SessionConfiguration paramSessionConfiguration) throws CameraAccessException, UnsupportedOperationException, IllegalArgumentException {
    synchronized (this.mInterfaceLock) {
      checkIfCameraClosedOrInError();
      return this.mRemoteDevice.isSessionConfigurationSupported(paramSessionConfiguration);
    } 
  }
  
  public void setSessionListener(StateCallbackKK paramStateCallbackKK) {
    synchronized (this.mInterfaceLock) {
      this.mSessionStateCallback = paramStateCallbackKK;
      return;
    } 
  }
  
  private void overrideEnableZsl(CameraMetadataNative paramCameraMetadataNative, boolean paramBoolean) {
    Boolean bool = paramCameraMetadataNative.<Boolean>get(CaptureRequest.CONTROL_ENABLE_ZSL);
    if (bool == null)
      return; 
    paramCameraMetadataNative.set(CaptureRequest.CONTROL_ENABLE_ZSL, Boolean.valueOf(paramBoolean));
  }
  
  public CaptureRequest.Builder createCaptureRequest(int paramInt, Set<String> paramSet) throws CameraAccessException {
    synchronized (this.mInterfaceLock) {
      IllegalStateException illegalStateException;
      checkIfCameraClosedOrInError();
      for (String str : paramSet) {
        if (str != getId())
          continue; 
        illegalStateException = new IllegalStateException();
        this("Physical id matches the logical id!");
        throw illegalStateException;
      } 
      CameraMetadataNative cameraMetadataNative = this.mRemoteDevice.createDefaultRequest(paramInt);
      if (this.mAppTargetSdkVersion < 26 || paramInt != 2)
        overrideEnableZsl(cameraMetadataNative, false); 
      CaptureRequest.Builder builder = new CaptureRequest.Builder();
      this(cameraMetadataNative, false, -1, getId(), (Set<String>)illegalStateException);
      return builder;
    } 
  }
  
  public CaptureRequest.Builder createCaptureRequest(int paramInt) throws CameraAccessException {
    synchronized (this.mInterfaceLock) {
      checkIfCameraClosedOrInError();
      CameraMetadataNative cameraMetadataNative = this.mRemoteDevice.createDefaultRequest(paramInt);
      if (this.mAppTargetSdkVersion < 26 || paramInt != 2)
        overrideEnableZsl(cameraMetadataNative, false); 
      CaptureRequest.Builder builder = new CaptureRequest.Builder();
      this(cameraMetadataNative, false, -1, getId(), null);
      return builder;
    } 
  }
  
  public CaptureRequest.Builder createReprocessCaptureRequest(TotalCaptureResult paramTotalCaptureResult) throws CameraAccessException {
    synchronized (this.mInterfaceLock) {
      checkIfCameraClosedOrInError();
      CameraMetadataNative cameraMetadataNative = new CameraMetadataNative();
      this(paramTotalCaptureResult.getNativeCopy());
      CaptureRequest.Builder builder = new CaptureRequest.Builder();
      this(cameraMetadataNative, true, paramTotalCaptureResult.getSessionId(), getId(), null);
      return builder;
    } 
  }
  
  public void prepare(Surface paramSurface) throws CameraAccessException {
    if (paramSurface != null)
      synchronized (this.mInterfaceLock) {
        int i;
        checkIfCameraClosedOrInError();
        byte b = -1;
        byte b1 = 0;
        while (true) {
          i = b;
          if (b1 < this.mConfiguredOutputs.size()) {
            List<Surface> list = ((OutputConfiguration)this.mConfiguredOutputs.valueAt(b1)).getSurfaces();
            if (list.contains(paramSurface)) {
              i = this.mConfiguredOutputs.keyAt(b1);
              break;
            } 
            b1++;
            continue;
          } 
          break;
        } 
        if (i != -1) {
          this.mRemoteDevice.prepare(i);
          return;
        } 
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("Surface is not part of this session");
        throw illegalArgumentException;
      }  
    throw new IllegalArgumentException("Surface is null");
  }
  
  public void prepare(int paramInt, Surface paramSurface) throws CameraAccessException {
    if (paramSurface != null) {
      if (paramInt > 0)
        synchronized (this.mInterfaceLock) {
          int i;
          checkIfCameraClosedOrInError();
          byte b = -1;
          byte b1 = 0;
          while (true) {
            i = b;
            if (b1 < this.mConfiguredOutputs.size()) {
              if (paramSurface == ((OutputConfiguration)this.mConfiguredOutputs.valueAt(b1)).getSurface()) {
                i = this.mConfiguredOutputs.keyAt(b1);
                break;
              } 
              b1++;
              continue;
            } 
            break;
          } 
          if (i != -1) {
            this.mRemoteDevice.prepare2(paramInt, i);
            return;
          } 
          IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
          this("Surface is not part of this session");
          throw illegalArgumentException;
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid maxCount given: ");
      stringBuilder.append(paramInt);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    throw new IllegalArgumentException("Surface is null");
  }
  
  public void updateOutputConfiguration(OutputConfiguration paramOutputConfiguration) throws CameraAccessException {
    synchronized (this.mInterfaceLock) {
      int i;
      checkIfCameraClosedOrInError();
      byte b = -1;
      byte b1 = 0;
      while (true) {
        i = b;
        if (b1 < this.mConfiguredOutputs.size()) {
          if (paramOutputConfiguration.getSurface() == ((OutputConfiguration)this.mConfiguredOutputs.valueAt(b1)).getSurface()) {
            i = this.mConfiguredOutputs.keyAt(b1);
            break;
          } 
          b1++;
          continue;
        } 
        break;
      } 
      if (i != -1) {
        this.mRemoteDevice.updateOutputConfiguration(i, paramOutputConfiguration);
        this.mConfiguredOutputs.put(i, paramOutputConfiguration);
        return;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      this("Invalid output configuration");
      throw illegalArgumentException;
    } 
  }
  
  public CameraOfflineSession switchToOffline(Collection<Surface> paramCollection, Executor paramExecutor, CameraOfflineSession.CameraOfflineSessionCallback paramCameraOfflineSessionCallback) throws CameraAccessException {
    if (!paramCollection.isEmpty()) {
      HashSet<Integer> hashSet = new HashSet();
      SparseArray<OutputConfiguration> sparseArray = new SparseArray();
      synchronized (this.mInterfaceLock) {
        checkIfCameraClosedOrInError();
        if (this.mOfflineSessionImpl == null) {
          IllegalArgumentException illegalArgumentException;
          StringBuilder stringBuilder;
          for (Surface surface : paramCollection) {
            int i;
            byte b = -1;
            byte b1 = 0;
            while (true) {
              i = b;
              if (b1 < this.mConfiguredOutputs.size()) {
                if (surface == ((OutputConfiguration)this.mConfiguredOutputs.valueAt(b1)).getSurface()) {
                  i = this.mConfiguredOutputs.keyAt(b1);
                  sparseArray.append(i, this.mConfiguredOutputs.valueAt(b1));
                  break;
                } 
                b1++;
                continue;
              } 
              break;
            } 
            if (i != -1) {
              if (this.mOfflineSupport.contains(Integer.valueOf(i))) {
                hashSet.add(Integer.valueOf(i));
                continue;
              } 
              illegalArgumentException = new IllegalArgumentException();
              stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Surface: ");
              stringBuilder.append(surface);
              stringBuilder.append(" does not  support offline mode");
              this(stringBuilder.toString());
              throw illegalArgumentException;
            } 
            IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
            this("Offline surface is not part of this session");
            throw illegalArgumentException1;
          } 
          stopRepeating();
          CameraOfflineSessionImpl cameraOfflineSessionImpl = new CameraOfflineSessionImpl();
          this(this.mCameraId, this.mCharacteristics, (Executor)illegalArgumentException, (CameraOfflineSession.CameraOfflineSessionCallback)stringBuilder, sparseArray, this.mConfiguredInput, this.mConfiguredOutputs, this.mFrameNumberTracker, this.mCaptureCallbackMap, this.mRequestLastFrameNumbersList);
          this.mOfflineSessionImpl = cameraOfflineSessionImpl;
          this.mOfflineSwitchService = Executors.newSingleThreadExecutor();
          this.mConfiguredOutputs.clear();
          AbstractMap.SimpleEntry<Object, Object> simpleEntry = new AbstractMap.SimpleEntry<>();
          this((K)Integer.valueOf(-1), null);
          this.mConfiguredInput = (AbstractMap.SimpleEntry)simpleEntry;
          this.mIdle = true;
          SparseArray<CaptureCallbackHolder> sparseArray1 = new SparseArray();
          this();
          this.mCaptureCallbackMap = sparseArray1;
          FrameNumberTracker frameNumberTracker = new FrameNumberTracker();
          this();
          this.mFrameNumberTracker = frameNumberTracker;
          this.mCurrentSession.closeWithoutDraining();
          this.mCurrentSession = null;
          this.mOfflineSwitchService.execute((Runnable)new Object(this, hashSet));
          return cameraOfflineSessionImpl;
        } 
        IllegalStateException illegalStateException = new IllegalStateException();
        this("Switch to offline mode already in progress");
        throw illegalStateException;
      } 
    } 
    throw new IllegalArgumentException("Invalid offline surfaces!");
  }
  
  public boolean supportsOfflineProcessing(Surface paramSurface) {
    // Byte code:
    //   0: aload_1
    //   1: ifnull -> 113
    //   4: aload_0
    //   5: getfield mInterfaceLock : Ljava/lang/Object;
    //   8: astore_2
    //   9: aload_2
    //   10: monitorenter
    //   11: iconst_m1
    //   12: istore_3
    //   13: iconst_0
    //   14: istore #4
    //   16: iload_3
    //   17: istore #5
    //   19: iload #4
    //   21: aload_0
    //   22: getfield mConfiguredOutputs : Landroid/util/SparseArray;
    //   25: invokevirtual size : ()I
    //   28: if_icmpge -> 70
    //   31: aload_1
    //   32: aload_0
    //   33: getfield mConfiguredOutputs : Landroid/util/SparseArray;
    //   36: iload #4
    //   38: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   41: checkcast android/hardware/camera2/params/OutputConfiguration
    //   44: invokevirtual getSurface : ()Landroid/view/Surface;
    //   47: if_acmpne -> 64
    //   50: aload_0
    //   51: getfield mConfiguredOutputs : Landroid/util/SparseArray;
    //   54: iload #4
    //   56: invokevirtual keyAt : (I)I
    //   59: istore #5
    //   61: goto -> 70
    //   64: iinc #4, 1
    //   67: goto -> 16
    //   70: iload #5
    //   72: iconst_m1
    //   73: if_icmpeq -> 95
    //   76: aload_0
    //   77: getfield mOfflineSupport : Ljava/util/HashSet;
    //   80: iload #5
    //   82: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   85: invokevirtual contains : (Ljava/lang/Object;)Z
    //   88: istore #6
    //   90: aload_2
    //   91: monitorexit
    //   92: iload #6
    //   94: ireturn
    //   95: new java/lang/IllegalArgumentException
    //   98: astore_1
    //   99: aload_1
    //   100: ldc_w 'Surface is not part of this session'
    //   103: invokespecial <init> : (Ljava/lang/String;)V
    //   106: aload_1
    //   107: athrow
    //   108: astore_1
    //   109: aload_2
    //   110: monitorexit
    //   111: aload_1
    //   112: athrow
    //   113: new java/lang/IllegalArgumentException
    //   116: dup
    //   117: ldc_w 'Surface is null'
    //   120: invokespecial <init> : (Ljava/lang/String;)V
    //   123: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1002	-> 0
    //   #1004	-> 4
    //   #1005	-> 11
    //   #1006	-> 13
    //   #1007	-> 31
    //   #1008	-> 50
    //   #1009	-> 61
    //   #1006	-> 64
    //   #1012	-> 70
    //   #1016	-> 76
    //   #1013	-> 95
    //   #1017	-> 108
    //   #1002	-> 113
    // Exception table:
    //   from	to	target	type
    //   19	31	108	finally
    //   31	50	108	finally
    //   50	61	108	finally
    //   76	92	108	finally
    //   95	108	108	finally
    //   109	111	108	finally
  }
  
  public void tearDown(Surface paramSurface) throws CameraAccessException {
    if (paramSurface != null)
      synchronized (this.mInterfaceLock) {
        int i;
        checkIfCameraClosedOrInError();
        byte b = -1;
        byte b1 = 0;
        while (true) {
          i = b;
          if (b1 < this.mConfiguredOutputs.size()) {
            if (paramSurface == ((OutputConfiguration)this.mConfiguredOutputs.valueAt(b1)).getSurface()) {
              i = this.mConfiguredOutputs.keyAt(b1);
              break;
            } 
            b1++;
            continue;
          } 
          break;
        } 
        if (i != -1) {
          this.mRemoteDevice.tearDown(i);
          return;
        } 
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("Surface is not part of this session");
        throw illegalArgumentException;
      }  
    throw new IllegalArgumentException("Surface is null");
  }
  
  public void finalizeOutputConfigs(List<OutputConfiguration> paramList) throws CameraAccessException {
    if (paramList != null && paramList.size() != 0)
      synchronized (this.mInterfaceLock) {
        checkIfCameraClosedOrInError();
        for (Iterator<OutputConfiguration> iterator = paramList.iterator(); iterator.hasNext(); ) {
          int i;
          OutputConfiguration outputConfiguration = iterator.next();
          byte b = -1;
          byte b1 = 0;
          while (true) {
            i = b;
            if (b1 < this.mConfiguredOutputs.size()) {
              if (outputConfiguration.equals(this.mConfiguredOutputs.valueAt(b1))) {
                i = this.mConfiguredOutputs.keyAt(b1);
                break;
              } 
              b1++;
              continue;
            } 
            break;
          } 
          if (i != -1) {
            if (outputConfiguration.getSurfaces().size() != 0) {
              this.mRemoteDevice.finalizeOutputConfigurations(i, outputConfiguration);
              this.mConfiguredOutputs.put(i, outputConfiguration);
              continue;
            } 
            IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("The final config for stream ");
            stringBuilder.append(i);
            stringBuilder.append(" must have at least 1 surface");
            this(stringBuilder.toString());
            throw illegalArgumentException1;
          } 
          IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
          this("Deferred config is not part of this session");
          throw illegalArgumentException;
        } 
        return;
      }  
    throw new IllegalArgumentException("deferred config is null or empty");
  }
  
  public int capture(CaptureRequest paramCaptureRequest, CaptureCallback paramCaptureCallback, Executor paramExecutor) throws CameraAccessException {
    ArrayList<CaptureRequest> arrayList = new ArrayList();
    arrayList.add(paramCaptureRequest);
    return submitCaptureRequest(arrayList, paramCaptureCallback, paramExecutor, false);
  }
  
  public int captureBurst(List<CaptureRequest> paramList, CaptureCallback paramCaptureCallback, Executor paramExecutor) throws CameraAccessException {
    if (paramList != null && !paramList.isEmpty())
      return submitCaptureRequest(paramList, paramCaptureCallback, paramExecutor, false); 
    throw new IllegalArgumentException("At least one request must be given");
  }
  
  private void checkEarlyTriggerSequenceCompleteLocked(int paramInt, long paramLong, int[] paramArrayOfint) {
    String str;
    if (paramLong == -1L) {
      int i = this.mCaptureCallbackMap.indexOfKey(paramInt);
      if (i >= 0) {
        CaptureCallbackHolder captureCallbackHolder = (CaptureCallbackHolder)this.mCaptureCallbackMap.valueAt(i);
      } else {
        paramArrayOfint = null;
      } 
      if (paramArrayOfint != null)
        this.mCaptureCallbackMap.removeAt(i); 
      if (paramArrayOfint != null) {
        Object object = new Object(this, paramInt, (CaptureCallbackHolder)paramArrayOfint);
        paramLong = Binder.clearCallingIdentity();
        try {
          paramArrayOfint.getExecutor().execute((Runnable)object);
        } finally {
          Binder.restoreCallingIdentity(paramLong);
        } 
      } else {
        str = this.TAG;
        Log.w(str, String.format("did not register callback to request %d", new Object[] { Integer.valueOf(paramInt) }));
      } 
    } else {
      this.mRequestLastFrameNumbersList.add(new RequestLastFrameNumbersHolder(paramInt, paramLong, (int[])str));
      checkAndFireSequenceComplete();
    } 
  }
  
  private int[] getRequestTypes(CaptureRequest[] paramArrayOfCaptureRequest) {
    int[] arrayOfInt = new int[paramArrayOfCaptureRequest.length];
    for (byte b = 0; b < paramArrayOfCaptureRequest.length; b++)
      arrayOfInt[b] = paramArrayOfCaptureRequest[b].getRequestType(); 
    return arrayOfInt;
  }
  
  private int submitCaptureRequest(List<CaptureRequest> paramList, CaptureCallback paramCaptureCallback, Executor paramExecutor, boolean paramBoolean) throws CameraAccessException {
    Executor executor = checkExecutor(paramExecutor, paramCaptureCallback);
    synchronized (this.mInterfaceLock) {
      IllegalArgumentException illegalArgumentException;
      int[] arrayOfInt;
      checkIfCameraClosedOrInError();
      for (CaptureRequest captureRequest : paramList) {
        if (!captureRequest.getTargets().isEmpty()) {
          for (Surface surface : captureRequest.getTargets()) {
            if (surface != null)
              continue; 
            IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
            this("Null Surface targets are not allowed");
            throw illegalArgumentException1;
          } 
          continue;
        } 
        illegalArgumentException = new IllegalArgumentException();
        this("Each request must have at least one Surface target");
        throw illegalArgumentException;
      } 
      if (paramBoolean)
        stopRepeating(); 
      for (CaptureRequest captureRequest : (CaptureRequest[])illegalArgumentException.toArray((Object[])new CaptureRequest[illegalArgumentException.size()]))
        captureRequest.convertSurfaceToStreamId(this.mConfiguredOutputs); 
      SubmitInfo submitInfo = this.mRemoteDevice.submitRequestList((CaptureRequest[])SYNTHETIC_LOCAL_VARIABLE_7, paramBoolean);
      int j;
      for (int i = SYNTHETIC_LOCAL_VARIABLE_7.length; j < i; ) {
        Object object = SYNTHETIC_LOCAL_VARIABLE_7[j];
        object.recoverStreamIdToSurface();
        j++;
      } 
      if (paramCaptureCallback != null) {
        SparseArray<CaptureCallbackHolder> sparseArray = this.mCaptureCallbackMap;
        j = submitInfo.getRequestId();
        CaptureCallbackHolder captureCallbackHolder = new CaptureCallbackHolder();
        this(paramCaptureCallback, (List<CaptureRequest>)illegalArgumentException, executor, paramBoolean, this.mNextSessionId - 1);
        sparseArray.put(j, captureCallbackHolder);
      } 
      if (paramBoolean) {
        if (this.mRepeatingRequestId != -1) {
          j = this.mRepeatingRequestId;
          long l = submitInfo.getLastFrameNumber();
          arrayOfInt = this.mRepeatingRequestTypes;
          checkEarlyTriggerSequenceCompleteLocked(j, l, arrayOfInt);
        } 
        this.mRepeatingRequestId = submitInfo.getRequestId();
        this.mRepeatingRequestTypes = getRequestTypes((CaptureRequest[])SYNTHETIC_LOCAL_VARIABLE_7);
      } else {
        List<RequestLastFrameNumbersHolder> list = this.mRequestLastFrameNumbersList;
        RequestLastFrameNumbersHolder requestLastFrameNumbersHolder = new RequestLastFrameNumbersHolder();
        this((List<CaptureRequest>)arrayOfInt, submitInfo);
        list.add(requestLastFrameNumbersHolder);
      } 
      if (this.mIdle)
        this.mDeviceExecutor.execute(this.mCallOnActive); 
      this.mIdle = false;
      j = submitInfo.getRequestId();
      return j;
    } 
  }
  
  public int setRepeatingRequest(CaptureRequest paramCaptureRequest, CaptureCallback paramCaptureCallback, Executor paramExecutor) throws CameraAccessException {
    ArrayList<CaptureRequest> arrayList = new ArrayList();
    arrayList.add(paramCaptureRequest);
    return submitCaptureRequest(arrayList, paramCaptureCallback, paramExecutor, true);
  }
  
  public int setRepeatingBurst(List<CaptureRequest> paramList, CaptureCallback paramCaptureCallback, Executor paramExecutor) throws CameraAccessException {
    if (paramList != null && !paramList.isEmpty())
      return submitCaptureRequest(paramList, paramCaptureCallback, paramExecutor, true); 
    throw new IllegalArgumentException("At least one request must be given");
  }
  
  public void stopRepeating() throws CameraAccessException {
    synchronized (this.mInterfaceLock) {
      checkIfCameraClosedOrInError();
      if (this.mRepeatingRequestId != -1) {
        int i = this.mRepeatingRequestId;
        this.mRepeatingRequestId = -1;
        int[] arrayOfInt = this.mRepeatingRequestTypes;
        this.mRepeatingRequestTypes = null;
        try {
          long l = this.mRemoteDevice.cancelRequest(i);
          checkEarlyTriggerSequenceCompleteLocked(i, l, arrayOfInt);
        } catch (IllegalArgumentException illegalArgumentException) {
          return;
        } 
      } 
      return;
    } 
  }
  
  private void waitUntilIdle() throws CameraAccessException {
    synchronized (this.mInterfaceLock) {
      checkIfCameraClosedOrInError();
      if (this.mRepeatingRequestId == -1) {
        this.mRemoteDevice.waitUntilIdle();
        return;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Active repeating request ongoing");
      throw illegalStateException;
    } 
  }
  
  public void flush() throws CameraAccessException {
    synchronized (this.mInterfaceLock) {
      checkIfCameraClosedOrInError();
      this.mDeviceExecutor.execute(this.mCallOnBusy);
      if (this.mIdle) {
        this.mDeviceExecutor.execute(this.mCallOnIdle);
        return;
      } 
      long l = this.mRemoteDevice.flush();
      if (this.mRepeatingRequestId != -1) {
        checkEarlyTriggerSequenceCompleteLocked(this.mRepeatingRequestId, l, this.mRepeatingRequestTypes);
        this.mRepeatingRequestId = -1;
        this.mRepeatingRequestTypes = null;
      } 
      return;
    } 
  }
  
  public void close() {
    synchronized (this.mInterfaceLock) {
      if (this.mClosing.getAndSet(true))
        return; 
      if (this.mOfflineSwitchService != null) {
        this.mOfflineSwitchService.shutdownNow();
        this.mOfflineSwitchService = null;
      } 
      if (this.mRemoteDevice != null) {
        this.mRemoteDevice.disconnect();
        this.mRemoteDevice.unlinkToDeath(this, 0);
      } 
      if (this.mRemoteDevice != null || this.mInError)
        this.mDeviceExecutor.execute(this.mCallOnClosed); 
      this.mRemoteDevice = null;
      return;
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      close();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private boolean checkPrivilegedAppList() {
    String str1 = ActivityThread.currentOpPackageName();
    String str2 = SystemProperties.get("persist.vendor.camera.privapp.list");
    if (str2.length() > 0) {
      TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
      simpleStringSplitter.setString(str2);
      for (String str : simpleStringSplitter) {
        if (str1.equals(str))
          return true; 
      } 
    } 
    return OplusCameraManager.isPrivilegedApp(str1);
  }
  
  public boolean isPrivilegedApp() {
    return this.mIsPrivilegedApp;
  }
  
  private void checkInputConfiguration(InputConfiguration paramInputConfiguration) {
    if (paramInputConfiguration != null) {
      StreamConfigurationMap streamConfigurationMap = this.mCharacteristics.<StreamConfigurationMap>get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
      if (isPrivilegedApp() || OplusCameraManager.getInstance().isCameraUnitSession()) {
        Log.w(this.TAG, "ignore input format/size check for white listed app");
        return;
      } 
      int[] arrayOfInt = streamConfigurationMap.getInputFormats();
      boolean bool1 = false;
      int i;
      boolean bool2;
      byte b;
      for (i = arrayOfInt.length, bool2 = false, b = 0; b < i; ) {
        int j = arrayOfInt[b];
        if (j == paramInputConfiguration.getFormat())
          bool1 = true; 
        b++;
      } 
      if (bool1) {
        bool1 = false;
        Size[] arrayOfSize = streamConfigurationMap.getInputSizes(paramInputConfiguration.getFormat());
        for (i = arrayOfSize.length, b = bool2; b < i; ) {
          Size size = arrayOfSize[b];
          bool2 = bool1;
          if (paramInputConfiguration.getWidth() == size.getWidth()) {
            bool2 = bool1;
            if (paramInputConfiguration.getHeight() == size.getHeight())
              bool2 = true; 
          } 
          b++;
          bool1 = bool2;
        } 
        if (!bool1) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("input size ");
          stringBuilder.append(paramInputConfiguration.getWidth());
          stringBuilder.append("x");
          stringBuilder.append(paramInputConfiguration.getHeight());
          stringBuilder.append(" is not valid");
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("input format ");
        stringBuilder.append(paramInputConfiguration.getFormat());
        stringBuilder.append(" is not valid");
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    } 
  }
  
  public static abstract class StateCallbackKK extends CameraDevice.StateCallback {
    public void onUnconfigured(CameraDevice param1CameraDevice) {}
    
    public void onActive(CameraDevice param1CameraDevice) {}
    
    public void onBusy(CameraDevice param1CameraDevice) {}
    
    public void onIdle(CameraDevice param1CameraDevice) {}
    
    public void onRequestQueueEmpty() {}
    
    public void onSurfacePrepared(Surface param1Surface) {}
  }
  
  private void checkAndFireSequenceComplete() {
    long l1 = this.mFrameNumberTracker.getCompletedFrameNumber();
    long l2 = this.mFrameNumberTracker.getCompletedReprocessFrameNumber();
    long l3 = this.mFrameNumberTracker.getCompletedZslStillFrameNumber();
    Iterator<RequestLastFrameNumbersHolder> iterator = this.mRequestLastFrameNumbersList.iterator();
    while (true) {
      RequestLastFrameNumbersHolder requestLastFrameNumbersHolder;
      int i;
      if (iterator.hasNext()) {
        requestLastFrameNumbersHolder = iterator.next();
        i = requestLastFrameNumbersHolder.getRequestId();
        if (this.mRemoteDevice == null) {
          Log.w(this.TAG, "Camera closed while checking sequences");
          return;
        } 
        if (!requestLastFrameNumbersHolder.isSequenceCompleted()) {
          CaptureCallbackHolder captureCallbackHolder;
          long l4 = requestLastFrameNumbersHolder.getLastRegularFrameNumber();
          long l5 = requestLastFrameNumbersHolder.getLastReprocessFrameNumber();
          long l6 = requestLastFrameNumbersHolder.getLastZslStillFrameNumber();
          if (l4 <= l1 && l5 <= l2 && l6 <= l3)
            requestLastFrameNumbersHolder.markSequenceCompleted(); 
          int j = this.mCaptureCallbackMap.indexOfKey(i);
          if (j >= 0) {
            captureCallbackHolder = (CaptureCallbackHolder)this.mCaptureCallbackMap.valueAt(j);
          } else {
            captureCallbackHolder = null;
          } 
          if (captureCallbackHolder != null && requestLastFrameNumbersHolder.isSequenceCompleted()) {
            Object object = new Object(this, i, captureCallbackHolder, requestLastFrameNumbersHolder);
            l6 = Binder.clearCallingIdentity();
            try {
              Executor executor = captureCallbackHolder.getExecutor();
              try {
                executor.execute((Runnable)object);
                Binder.restoreCallingIdentity(l6);
              } finally {}
            } finally {}
            Binder.restoreCallingIdentity(l6);
            throw captureCallbackHolder;
          } 
        } 
      } else {
        break;
      } 
      if (requestLastFrameNumbersHolder.isSequenceCompleted() && 
        requestLastFrameNumbersHolder.isInflightCompleted()) {
        i = this.mCaptureCallbackMap.indexOfKey(i);
        if (i >= 0)
          this.mCaptureCallbackMap.removeAt(i); 
        iterator.remove();
      } 
    } 
  }
  
  private void removeCompletedCallbackHolderLocked(long paramLong1, long paramLong2, long paramLong3) {
    Iterator<RequestLastFrameNumbersHolder> iterator = this.mRequestLastFrameNumbersList.iterator();
    while (iterator.hasNext()) {
      RequestLastFrameNumbersHolder requestLastFrameNumbersHolder = iterator.next();
      int i = requestLastFrameNumbersHolder.getRequestId();
      if (this.mRemoteDevice == null) {
        Log.w(this.TAG, "Camera closed while removing completed callback holders");
        return;
      } 
      long l1 = requestLastFrameNumbersHolder.getLastRegularFrameNumber();
      long l2 = requestLastFrameNumbersHolder.getLastReprocessFrameNumber();
      long l3 = requestLastFrameNumbersHolder.getLastZslStillFrameNumber();
      if (l1 <= paramLong1 && l2 <= paramLong2 && l3 <= paramLong3) {
        if (requestLastFrameNumbersHolder.isSequenceCompleted()) {
          i = this.mCaptureCallbackMap.indexOfKey(i);
          if (i >= 0)
            this.mCaptureCallbackMap.removeAt(i); 
          iterator.remove();
          continue;
        } 
        requestLastFrameNumbersHolder.markInflightCompleted();
      } 
    } 
  }
  
  public void onDeviceError(int paramInt, CaptureResultExtras paramCaptureResultExtras) {
    synchronized (this.mInterfaceLock) {
      if (this.mRemoteDevice == null)
        return; 
      if (this.mOfflineSessionImpl != null) {
        this.mOfflineSessionImpl.getCallbacks().onDeviceError(paramInt, paramCaptureResultExtras);
        return;
      } 
      if (paramInt != 0) {
        if (paramInt != 1) {
          String str;
          if (paramInt != 3 && paramInt != 4 && paramInt != 5) {
            if (paramInt != 6) {
              str = this.TAG;
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Unknown error from camera device: ");
              stringBuilder.append(paramInt);
              Log.e(str, stringBuilder.toString());
              scheduleNotifyError(5);
            } else {
              scheduleNotifyError(3);
            } 
          } else {
            onCaptureErrorLocked(paramInt, (CaptureResultExtras)str);
          } 
        } else {
          scheduleNotifyError(4);
        } 
      } else {
        long l = Binder.clearCallingIdentity();
        try {
          this.mDeviceExecutor.execute(this.mCallOnDisconnected);
          return;
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } 
      return;
    } 
  }
  
  private void scheduleNotifyError(int paramInt) {
    this.mInError = true;
    long l = Binder.clearCallingIdentity();
    try {
      Executor executor = this.mDeviceExecutor;
      -$.Lambda.CameraDeviceImpl.oDs27OTfKFfK18rUW2nQxxkPdV0 oDs27OTfKFfK18rUW2nQxxkPdV0 = _$$Lambda$CameraDeviceImpl$oDs27OTfKFfK18rUW2nQxxkPdV0.INSTANCE;
      PooledRunnable pooledRunnable = PooledLambda.obtainRunnable((BiConsumer)oDs27OTfKFfK18rUW2nQxxkPdV0, this, Integer.valueOf(paramInt));
      pooledRunnable = pooledRunnable.recycleOnUse();
      executor.execute((Runnable)pooledRunnable);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  private void notifyError(int paramInt) {
    if (!isClosed())
      this.mDeviceCallback.onError(this, paramInt); 
  }
  
  private void onCaptureErrorLocked(int paramInt, CaptureResultExtras paramCaptureResultExtras) {
    String str1, str2;
    int i = paramCaptureResultExtras.getRequestId();
    int j = paramCaptureResultExtras.getSubsequenceId();
    long l = paramCaptureResultExtras.getFrameNumber();
    Object object = paramCaptureResultExtras.getErrorPhysicalCameraId();
    CaptureCallbackHolder captureCallbackHolder = (CaptureCallbackHolder)this.mCaptureCallbackMap.get(i);
    boolean bool = false;
    if (captureCallbackHolder == null) {
      str1 = this.TAG;
      Log.e(str1, String.format("Receive capture error on unknown request ID %d", new Object[] { Integer.valueOf(i) }));
      return;
    } 
    CaptureRequest captureRequest = captureCallbackHolder.getRequest(j);
    if (paramInt == 5) {
      SparseArray<OutputConfiguration> sparseArray = this.mConfiguredOutputs;
      paramInt = str1.getErrorStreamId();
      OutputConfiguration outputConfiguration = (OutputConfiguration)sparseArray.get(paramInt);
      if (outputConfiguration == null) {
        str2 = this.TAG;
        paramInt = str1.getErrorStreamId();
        Log.v(str2, String.format("Stream %d has been removed. Skipping buffer lost callback", new Object[] { Integer.valueOf(paramInt) }));
        return;
      } 
      for (Surface surface : outputConfiguration.getSurfaces()) {
        if (!captureRequest.containsTarget(surface))
          continue; 
        object = new Object(this, (CaptureCallbackHolder)str2, captureRequest, surface, l);
        long l1 = Binder.clearCallingIdentity();
        try {
          str2.getExecutor().execute((Runnable)object);
        } finally {
          Binder.restoreCallingIdentity(l1);
        } 
      } 
    } else {
      boolean bool1;
      if (paramInt == 4) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      CameraCaptureSessionCore cameraCaptureSessionCore = this.mCurrentSession;
      if (cameraCaptureSessionCore != null && cameraCaptureSessionCore.isAborting()) {
        paramInt = 1;
      } else {
        paramInt = bool;
      } 
      CaptureFailure captureFailure = new CaptureFailure(captureRequest, paramInt, bool1, i, l, (String)object);
      object = new Object(this, (CaptureCallbackHolder)str2, captureRequest, captureFailure);
      null = this.mFrameNumberTracker;
      paramInt = captureRequest.getRequestType();
      null.updateTracker(l, true, paramInt);
      checkAndFireSequenceComplete();
      long l1 = Binder.clearCallingIdentity();
      try {
        str2.getExecutor().execute((Runnable)object);
        return;
      } finally {
        Binder.restoreCallingIdentity(l1);
      } 
    } 
  }
  
  public void onDeviceIdle() {
    synchronized (this.mInterfaceLock) {
      if (this.mRemoteDevice == null)
        return; 
      if (this.mOfflineSessionImpl != null) {
        this.mOfflineSessionImpl.getCallbacks().onDeviceIdle();
        return;
      } 
      removeCompletedCallbackHolderLocked(Long.MAX_VALUE, Long.MAX_VALUE, Long.MAX_VALUE);
      if (!this.mIdle) {
        long l = Binder.clearCallingIdentity();
        try {
          this.mDeviceExecutor.execute(this.mCallOnIdle);
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } 
      this.mIdle = true;
      return;
    } 
  }
  
  class CameraDeviceCallbacks extends ICameraDeviceCallbacks.Stub {
    final CameraDeviceImpl this$0;
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public void onDeviceError(int param1Int, CaptureResultExtras param1CaptureResultExtras) {
      CameraDeviceImpl.this.onDeviceError(param1Int, param1CaptureResultExtras);
    }
    
    public void onRepeatingRequestError(long param1Long, int param1Int) {
      synchronized (CameraDeviceImpl.this.mInterfaceLock) {
        if (CameraDeviceImpl.this.mRemoteDevice == null || CameraDeviceImpl.this.mRepeatingRequestId == -1)
          return; 
        if (CameraDeviceImpl.this.mOfflineSessionImpl != null) {
          CameraDeviceImpl.this.mOfflineSessionImpl.getCallbacks().onRepeatingRequestError(param1Long, param1Int);
          return;
        } 
        CameraDeviceImpl cameraDeviceImpl1 = CameraDeviceImpl.this;
        int i = CameraDeviceImpl.this.mRepeatingRequestId;
        CameraDeviceImpl cameraDeviceImpl2 = CameraDeviceImpl.this;
        int[] arrayOfInt = cameraDeviceImpl2.mRepeatingRequestTypes;
        cameraDeviceImpl1.checkEarlyTriggerSequenceCompleteLocked(i, param1Long, arrayOfInt);
        if (CameraDeviceImpl.this.mRepeatingRequestId == param1Int) {
          CameraDeviceImpl.access$702(CameraDeviceImpl.this, -1);
          CameraDeviceImpl.access$802(CameraDeviceImpl.this, null);
        } 
        return;
      } 
    }
    
    public void onDeviceIdle() {
      CameraDeviceImpl.this.onDeviceIdle();
    }
    
    public void onCaptureStarted(CaptureResultExtras param1CaptureResultExtras, long param1Long) {
      int i = param1CaptureResultExtras.getRequestId();
      long l1 = param1CaptureResultExtras.getFrameNumber();
      long l2 = param1CaptureResultExtras.getLastCompletedRegularFrameNumber();
      long l3 = param1CaptureResultExtras.getLastCompletedReprocessFrameNumber();
      long l4 = param1CaptureResultExtras.getLastCompletedZslFrameNumber();
      Object object2 = CameraDeviceImpl.this.mInterfaceLock;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      try {
        if (CameraDeviceImpl.this.mRemoteDevice == null) {
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return;
        } 
        CameraOfflineSessionImpl cameraOfflineSessionImpl = CameraDeviceImpl.this.mOfflineSessionImpl;
        if (cameraOfflineSessionImpl != null) {
          try {
            CameraOfflineSessionImpl.CameraDeviceCallbacks cameraDeviceCallbacks = CameraDeviceImpl.this.mOfflineSessionImpl.getCallbacks();
            cameraDeviceCallbacks.onCaptureStarted(param1CaptureResultExtras, param1Long);
            /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
            return;
          } finally {}
        } else {
          CameraDeviceImpl.this.removeCompletedCallbackHolderLocked(l2, l3, l4);
          CaptureCallbackHolder captureCallbackHolder = (CaptureCallbackHolder)CameraDeviceImpl.this.mCaptureCallbackMap.get(i);
          if (captureCallbackHolder == null) {
            /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
            return;
          } 
          if (CameraDeviceImpl.this.isClosed()) {
            /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
            return;
          } 
          l4 = Binder.clearCallingIdentity();
          try {
            Executor executor = captureCallbackHolder.getExecutor();
            Object object4 = new Object(), object3 = object2;
            try {
              super(param1CaptureResultExtras, captureCallbackHolder, param1Long, l1);
              executor.execute((Runnable)object4);
              Object object5 = object3;
              try {
                Binder.restoreCallingIdentity(l4);
                object5 = object3;
                return;
              } finally {
                object3 = null;
              } 
            } finally {}
          } finally {}
          Object object = object2;
          Binder.restoreCallingIdentity(l4);
          object = object2;
          throw cameraOfflineSessionImpl;
        } 
      } finally {}
      CaptureResultExtras captureResultExtras = param1CaptureResultExtras;
      Object object1 = object2;
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      throw captureResultExtras;
    }
    
    public void onResultReceived(CameraMetadataNative param1CameraMetadataNative, CaptureResultExtras param1CaptureResultExtras, PhysicalCaptureResultInfo[] param1ArrayOfPhysicalCaptureResultInfo) throws RemoteException {
      // Byte code:
      //   0: aload_2
      //   1: invokevirtual getRequestId : ()I
      //   4: istore #4
      //   6: aload_2
      //   7: invokevirtual getFrameNumber : ()J
      //   10: lstore #5
      //   12: aload_0
      //   13: getfield this$0 : Landroid/hardware/camera2/impl/CameraDeviceImpl;
      //   16: getfield mInterfaceLock : Ljava/lang/Object;
      //   19: astore #7
      //   21: aload #7
      //   23: monitorenter
      //   24: aload_0
      //   25: getfield this$0 : Landroid/hardware/camera2/impl/CameraDeviceImpl;
      //   28: invokestatic access$000 : (Landroid/hardware/camera2/impl/CameraDeviceImpl;)Landroid/hardware/camera2/impl/ICameraDeviceUserWrapper;
      //   31: astore #8
      //   33: aload #8
      //   35: ifnonnull -> 49
      //   38: aload #7
      //   40: monitorexit
      //   41: return
      //   42: astore_1
      //   43: aload #7
      //   45: astore_2
      //   46: goto -> 555
      //   49: aload_0
      //   50: getfield this$0 : Landroid/hardware/camera2/impl/CameraDeviceImpl;
      //   53: invokestatic access$500 : (Landroid/hardware/camera2/impl/CameraDeviceImpl;)Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   56: astore #8
      //   58: aload #8
      //   60: ifnull -> 94
      //   63: aload_0
      //   64: getfield this$0 : Landroid/hardware/camera2/impl/CameraDeviceImpl;
      //   67: invokestatic access$500 : (Landroid/hardware/camera2/impl/CameraDeviceImpl;)Landroid/hardware/camera2/impl/CameraOfflineSessionImpl;
      //   70: invokevirtual getCallbacks : ()Landroid/hardware/camera2/impl/CameraOfflineSessionImpl$CameraDeviceCallbacks;
      //   73: astore #8
      //   75: aload #8
      //   77: aload_1
      //   78: aload_2
      //   79: aload_3
      //   80: invokevirtual onResultReceived : (Landroid/hardware/camera2/impl/CameraMetadataNative;Landroid/hardware/camera2/impl/CaptureResultExtras;[Landroid/hardware/camera2/impl/PhysicalCaptureResultInfo;)V
      //   83: aload #7
      //   85: monitorexit
      //   86: return
      //   87: astore_1
      //   88: aload #7
      //   90: astore_2
      //   91: goto -> 555
      //   94: getstatic android/hardware/camera2/CameraCharacteristics.LENS_INFO_SHADING_MAP_SIZE : Landroid/hardware/camera2/CameraCharacteristics$Key;
      //   97: astore #8
      //   99: aload_0
      //   100: getfield this$0 : Landroid/hardware/camera2/impl/CameraDeviceImpl;
      //   103: astore #9
      //   105: aload #9
      //   107: invokestatic access$1200 : (Landroid/hardware/camera2/impl/CameraDeviceImpl;)Landroid/hardware/camera2/CameraCharacteristics;
      //   110: getstatic android/hardware/camera2/CameraCharacteristics.LENS_INFO_SHADING_MAP_SIZE : Landroid/hardware/camera2/CameraCharacteristics$Key;
      //   113: invokevirtual get : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
      //   116: checkcast android/util/Size
      //   119: astore #9
      //   121: aload_1
      //   122: aload #8
      //   124: aload #9
      //   126: invokevirtual set : (Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)V
      //   129: aload_0
      //   130: getfield this$0 : Landroid/hardware/camera2/impl/CameraDeviceImpl;
      //   133: astore #8
      //   135: aload #8
      //   137: invokestatic access$1100 : (Landroid/hardware/camera2/impl/CameraDeviceImpl;)Landroid/util/SparseArray;
      //   140: iload #4
      //   142: invokevirtual get : (I)Ljava/lang/Object;
      //   145: checkcast android/hardware/camera2/impl/CaptureCallbackHolder
      //   148: astore #10
      //   150: aload #10
      //   152: aload_2
      //   153: invokevirtual getSubsequenceId : ()I
      //   156: invokevirtual getRequest : (I)Landroid/hardware/camera2/CaptureRequest;
      //   159: astore #11
      //   161: aload_2
      //   162: invokevirtual getPartialResultCount : ()I
      //   165: aload_0
      //   166: getfield this$0 : Landroid/hardware/camera2/impl/CameraDeviceImpl;
      //   169: invokestatic access$1300 : (Landroid/hardware/camera2/impl/CameraDeviceImpl;)I
      //   172: if_icmpge -> 181
      //   175: iconst_1
      //   176: istore #12
      //   178: goto -> 184
      //   181: iconst_0
      //   182: istore #12
      //   184: aload #11
      //   186: invokevirtual getRequestType : ()I
      //   189: istore #13
      //   191: aload #10
      //   193: ifnonnull -> 217
      //   196: aload_0
      //   197: getfield this$0 : Landroid/hardware/camera2/impl/CameraDeviceImpl;
      //   200: invokestatic access$1400 : (Landroid/hardware/camera2/impl/CameraDeviceImpl;)Landroid/hardware/camera2/impl/FrameNumberTracker;
      //   203: lload #5
      //   205: aconst_null
      //   206: iload #12
      //   208: iload #13
      //   210: invokevirtual updateTracker : (JLandroid/hardware/camera2/CaptureResult;ZI)V
      //   213: aload #7
      //   215: monitorexit
      //   216: return
      //   217: aload_0
      //   218: getfield this$0 : Landroid/hardware/camera2/impl/CameraDeviceImpl;
      //   221: invokestatic access$600 : (Landroid/hardware/camera2/impl/CameraDeviceImpl;)Z
      //   224: istore #14
      //   226: iload #14
      //   228: ifeq -> 261
      //   231: aload_0
      //   232: getfield this$0 : Landroid/hardware/camera2/impl/CameraDeviceImpl;
      //   235: invokestatic access$1400 : (Landroid/hardware/camera2/impl/CameraDeviceImpl;)Landroid/hardware/camera2/impl/FrameNumberTracker;
      //   238: astore_1
      //   239: aload_1
      //   240: lload #5
      //   242: aconst_null
      //   243: iload #12
      //   245: iload #13
      //   247: invokevirtual updateTracker : (JLandroid/hardware/camera2/CaptureResult;ZI)V
      //   250: aload #7
      //   252: monitorexit
      //   253: return
      //   254: astore_1
      //   255: aload #7
      //   257: astore_2
      //   258: goto -> 555
      //   261: aload #10
      //   263: invokevirtual hasBatchedOutputs : ()Z
      //   266: istore #14
      //   268: iload #14
      //   270: ifeq -> 294
      //   273: new android/hardware/camera2/impl/CameraMetadataNative
      //   276: astore #8
      //   278: aload #8
      //   280: aload_1
      //   281: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraMetadataNative;)V
      //   284: goto -> 297
      //   287: astore_1
      //   288: aload #7
      //   290: astore_2
      //   291: goto -> 555
      //   294: aconst_null
      //   295: astore #8
      //   297: iload #12
      //   299: ifeq -> 336
      //   302: new android/hardware/camera2/CaptureResult
      //   305: astore_3
      //   306: aload_3
      //   307: aload_1
      //   308: aload #11
      //   310: aload_2
      //   311: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraMetadataNative;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/impl/CaptureResultExtras;)V
      //   314: new android/hardware/camera2/impl/CameraDeviceImpl$CameraDeviceCallbacks$2
      //   317: dup
      //   318: aload_0
      //   319: aload #10
      //   321: aload #8
      //   323: aload_2
      //   324: aload #11
      //   326: aload_3
      //   327: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraDeviceImpl$CameraDeviceCallbacks;Landroid/hardware/camera2/impl/CaptureCallbackHolder;Landroid/hardware/camera2/impl/CameraMetadataNative;Landroid/hardware/camera2/impl/CaptureResultExtras;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureResult;)V
      //   330: astore_2
      //   331: aload_3
      //   332: astore_1
      //   333: goto -> 454
      //   336: aload_0
      //   337: getfield this$0 : Landroid/hardware/camera2/impl/CameraDeviceImpl;
      //   340: astore #9
      //   342: aload #9
      //   344: invokestatic access$1400 : (Landroid/hardware/camera2/impl/CameraDeviceImpl;)Landroid/hardware/camera2/impl/FrameNumberTracker;
      //   347: lload #5
      //   349: invokevirtual popPartialResults : (J)Ljava/util/List;
      //   352: astore #15
      //   354: getstatic android/hardware/camera2/CaptureResult.SENSOR_TIMESTAMP : Landroid/hardware/camera2/CaptureResult$Key;
      //   357: astore #9
      //   359: aload_1
      //   360: aload #9
      //   362: invokevirtual get : (Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;
      //   365: checkcast java/lang/Long
      //   368: invokevirtual longValue : ()J
      //   371: lstore #16
      //   373: getstatic android/hardware/camera2/CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE : Landroid/hardware/camera2/CaptureRequest$Key;
      //   376: astore #9
      //   378: aload #11
      //   380: aload #9
      //   382: invokevirtual get : (Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;
      //   385: checkcast android/util/Range
      //   388: astore #18
      //   390: aload_2
      //   391: invokevirtual getSubsequenceId : ()I
      //   394: istore #4
      //   396: new android/hardware/camera2/TotalCaptureResult
      //   399: astore #9
      //   401: aload #10
      //   403: invokevirtual getSessionId : ()I
      //   406: istore #19
      //   408: aload #9
      //   410: aload_1
      //   411: aload #11
      //   413: aload_2
      //   414: aload #15
      //   416: iload #19
      //   418: aload_3
      //   419: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraMetadataNative;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/impl/CaptureResultExtras;Ljava/util/List;I[Landroid/hardware/camera2/impl/PhysicalCaptureResultInfo;)V
      //   422: aload #7
      //   424: astore_3
      //   425: new android/hardware/camera2/impl/CameraDeviceImpl$CameraDeviceCallbacks$3
      //   428: dup
      //   429: aload_0
      //   430: aload #10
      //   432: aload #8
      //   434: lload #16
      //   436: iload #4
      //   438: aload #18
      //   440: aload_2
      //   441: aload #15
      //   443: aload #11
      //   445: aload #9
      //   447: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraDeviceImpl$CameraDeviceCallbacks;Landroid/hardware/camera2/impl/CaptureCallbackHolder;Landroid/hardware/camera2/impl/CameraMetadataNative;JILandroid/util/Range;Landroid/hardware/camera2/impl/CaptureResultExtras;Ljava/util/List;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V
      //   450: astore_2
      //   451: aload #9
      //   453: astore_1
      //   454: aload #7
      //   456: astore_3
      //   457: invokestatic clearCallingIdentity : ()J
      //   460: lstore #16
      //   462: aload #10
      //   464: invokevirtual getExecutor : ()Ljava/util/concurrent/Executor;
      //   467: aload_2
      //   468: invokeinterface execute : (Ljava/lang/Runnable;)V
      //   473: aload #7
      //   475: astore_3
      //   476: lload #16
      //   478: invokestatic restoreCallingIdentity : (J)V
      //   481: aload #7
      //   483: astore_3
      //   484: aload_0
      //   485: getfield this$0 : Landroid/hardware/camera2/impl/CameraDeviceImpl;
      //   488: invokestatic access$1400 : (Landroid/hardware/camera2/impl/CameraDeviceImpl;)Landroid/hardware/camera2/impl/FrameNumberTracker;
      //   491: lload #5
      //   493: aload_1
      //   494: iload #12
      //   496: iload #13
      //   498: invokevirtual updateTracker : (JLandroid/hardware/camera2/CaptureResult;ZI)V
      //   501: iload #12
      //   503: ifne -> 516
      //   506: aload #7
      //   508: astore_3
      //   509: aload_0
      //   510: getfield this$0 : Landroid/hardware/camera2/impl/CameraDeviceImpl;
      //   513: invokestatic access$1600 : (Landroid/hardware/camera2/impl/CameraDeviceImpl;)V
      //   516: aload #7
      //   518: astore_3
      //   519: aload #7
      //   521: monitorexit
      //   522: return
      //   523: astore_1
      //   524: aload #7
      //   526: astore_3
      //   527: lload #16
      //   529: invokestatic restoreCallingIdentity : (J)V
      //   532: aload #7
      //   534: astore_3
      //   535: aload_1
      //   536: athrow
      //   537: astore_1
      //   538: aload #7
      //   540: astore_2
      //   541: goto -> 555
      //   544: astore_1
      //   545: aload #7
      //   547: astore_2
      //   548: goto -> 555
      //   551: astore_1
      //   552: aload #7
      //   554: astore_2
      //   555: aload_2
      //   556: astore_3
      //   557: aload_2
      //   558: monitorexit
      //   559: aload_1
      //   560: athrow
      //   561: astore_1
      //   562: aload_3
      //   563: astore_2
      //   564: goto -> 555
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1971	-> 0
      //   #1972	-> 6
      //   #1979	-> 12
      //   #1980	-> 24
      //   #2142	-> 42
      //   #1985	-> 49
      //   #1986	-> 63
      //   #1988	-> 83
      //   #2142	-> 87
      //   #1992	-> 94
      //   #1993	-> 105
      //   #1992	-> 121
      //   #1995	-> 129
      //   #1996	-> 135
      //   #1997	-> 150
      //   #1999	-> 161
      //   #2000	-> 161
      //   #2001	-> 184
      //   #2004	-> 191
      //   #2011	-> 196
      //   #2014	-> 213
      //   #2017	-> 217
      //   #2024	-> 231
      //   #2026	-> 250
      //   #2142	-> 254
      //   #2030	-> 261
      //   #2036	-> 261
      //   #2037	-> 273
      //   #2142	-> 287
      //   #2039	-> 294
      //   #2043	-> 297
      //   #2044	-> 302
      //   #2047	-> 314
      //   #2074	-> 331
      //   #2075	-> 331
      //   #2076	-> 336
      //   #2077	-> 342
      //   #2079	-> 354
      //   #2080	-> 359
      //   #2081	-> 373
      //   #2082	-> 378
      //   #2083	-> 390
      //   #2084	-> 396
      //   #2085	-> 401
      //   #2088	-> 425
      //   #2124	-> 451
      //   #2127	-> 454
      //   #2129	-> 462
      //   #2131	-> 473
      //   #2132	-> 481
      //   #2135	-> 481
      //   #2139	-> 501
      //   #2140	-> 506
      //   #2142	-> 516
      //   #2143	-> 522
      //   #2131	-> 523
      //   #2132	-> 532
      // Exception table:
      //   from	to	target	type
      //   24	33	551	finally
      //   38	41	42	finally
      //   49	58	551	finally
      //   63	75	87	finally
      //   75	83	42	finally
      //   83	86	42	finally
      //   94	105	551	finally
      //   105	121	551	finally
      //   121	129	551	finally
      //   129	135	551	finally
      //   135	150	551	finally
      //   150	161	551	finally
      //   161	175	551	finally
      //   184	191	551	finally
      //   196	213	42	finally
      //   213	216	42	finally
      //   217	226	551	finally
      //   231	239	254	finally
      //   239	250	287	finally
      //   250	253	287	finally
      //   261	268	544	finally
      //   273	284	287	finally
      //   302	314	287	finally
      //   314	331	287	finally
      //   336	342	544	finally
      //   342	354	544	finally
      //   354	359	544	finally
      //   359	373	544	finally
      //   373	378	544	finally
      //   378	390	544	finally
      //   390	396	544	finally
      //   396	401	544	finally
      //   401	408	544	finally
      //   408	422	537	finally
      //   425	451	561	finally
      //   457	462	561	finally
      //   462	473	523	finally
      //   476	481	561	finally
      //   484	501	561	finally
      //   509	516	561	finally
      //   519	522	561	finally
      //   527	532	561	finally
      //   535	537	561	finally
      //   557	559	561	finally
    }
    
    public void onPrepared(int param1Int) {
      synchronized (CameraDeviceImpl.this.mInterfaceLock) {
        if (CameraDeviceImpl.this.mOfflineSessionImpl != null) {
          CameraDeviceImpl.this.mOfflineSessionImpl.getCallbacks().onPrepared(param1Int);
          return;
        } 
        OutputConfiguration outputConfiguration = (OutputConfiguration)CameraDeviceImpl.this.mConfiguredOutputs.get(param1Int);
        CameraDeviceImpl.StateCallbackKK stateCallbackKK = CameraDeviceImpl.this.mSessionStateCallback;
        if (stateCallbackKK == null)
          return; 
        if (outputConfiguration == null) {
          Log.w(CameraDeviceImpl.this.TAG, "onPrepared invoked for unknown output Surface");
          return;
        } 
        null = (Object<Surface>)outputConfiguration.getSurfaces();
        for (Surface surface : null)
          stateCallbackKK.onSurfacePrepared(surface); 
        return;
      } 
    }
    
    public void onRequestQueueEmpty() {
      synchronized (CameraDeviceImpl.this.mInterfaceLock) {
        if (CameraDeviceImpl.this.mOfflineSessionImpl != null) {
          CameraDeviceImpl.this.mOfflineSessionImpl.getCallbacks().onRequestQueueEmpty();
          return;
        } 
        CameraDeviceImpl.StateCallbackKK stateCallbackKK = CameraDeviceImpl.this.mSessionStateCallback;
        if (stateCallbackKK == null)
          return; 
        stateCallbackKK.onRequestQueueEmpty();
        return;
      } 
    }
  }
  
  class CameraHandlerExecutor implements Executor {
    private final Handler mHandler;
    
    public CameraHandlerExecutor(CameraDeviceImpl this$0) {
      Objects.requireNonNull(this$0);
      this.mHandler = (Handler)this$0;
    }
    
    public void execute(Runnable param1Runnable) {
      this.mHandler.post(param1Runnable);
    }
  }
  
  static Executor checkExecutor(Executor paramExecutor) {
    if (paramExecutor == null)
      paramExecutor = checkAndWrapHandler(null); 
    return paramExecutor;
  }
  
  public static <T> Executor checkExecutor(Executor paramExecutor, T paramT) {
    if (paramT != null)
      paramExecutor = checkExecutor(paramExecutor); 
    return paramExecutor;
  }
  
  public static Executor checkAndWrapHandler(Handler paramHandler) {
    return new CameraHandlerExecutor(checkHandler(paramHandler));
  }
  
  static Handler checkHandler(Handler paramHandler) {
    Handler handler = paramHandler;
    if (paramHandler == null) {
      Looper looper = Looper.myLooper();
      if (looper != null) {
        handler = new Handler(looper);
      } else {
        throw new IllegalArgumentException("No handler given, and current thread has no looper!");
      } 
    } 
    return handler;
  }
  
  static <T> Handler checkHandler(Handler paramHandler, T paramT) {
    if (paramT != null)
      return checkHandler(paramHandler); 
    return paramHandler;
  }
  
  private void checkIfCameraClosedOrInError() throws CameraAccessException {
    if (this.mRemoteDevice != null) {
      if (!this.mInError)
        return; 
      throw new CameraAccessException(3, "The camera device has encountered a serious error");
    } 
    throw new IllegalStateException("CameraDevice was already closed");
  }
  
  private boolean isClosed() {
    return this.mClosing.get();
  }
  
  private CameraCharacteristics getCharacteristics() {
    return this.mCharacteristics;
  }
  
  public void binderDied() {
    String str = this.TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CameraDevice ");
    stringBuilder.append(this.mCameraId);
    stringBuilder.append(" died unexpectedly");
    Log.w(str, stringBuilder.toString());
    if (this.mRemoteDevice == null)
      return; 
    this.mInError = true;
    null = new Object(this);
    long l = Binder.clearCallingIdentity();
    try {
      this.mDeviceExecutor.execute((Runnable)null);
      return;
    } finally {
      Binder.restoreCallingIdentity(l);
    } 
  }
  
  public void setCameraAudioRestriction(int paramInt) throws CameraAccessException {
    synchronized (this.mInterfaceLock) {
      checkIfCameraClosedOrInError();
      this.mRemoteDevice.setCameraAudioRestriction(paramInt);
      return;
    } 
  }
  
  public int getCameraAudioRestriction() throws CameraAccessException {
    synchronized (this.mInterfaceLock) {
      checkIfCameraClosedOrInError();
      return this.mRemoteDevice.getGlobalAudioRestriction();
    } 
  }
}
