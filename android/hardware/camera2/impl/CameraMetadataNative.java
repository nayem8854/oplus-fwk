package android.hardware.camera2.impl;

import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.marshal.MarshalQueryable;
import android.hardware.camera2.marshal.MarshalRegistry;
import android.hardware.camera2.marshal.Marshaler;
import android.hardware.camera2.marshal.impl.MarshalQueryableArray;
import android.hardware.camera2.marshal.impl.MarshalQueryableBlackLevelPattern;
import android.hardware.camera2.marshal.impl.MarshalQueryableBoolean;
import android.hardware.camera2.marshal.impl.MarshalQueryableColorSpaceTransform;
import android.hardware.camera2.marshal.impl.MarshalQueryableEnum;
import android.hardware.camera2.marshal.impl.MarshalQueryableHighSpeedVideoConfiguration;
import android.hardware.camera2.marshal.impl.MarshalQueryableMeteringRectangle;
import android.hardware.camera2.marshal.impl.MarshalQueryableNativeByteToInteger;
import android.hardware.camera2.marshal.impl.MarshalQueryablePair;
import android.hardware.camera2.marshal.impl.MarshalQueryableParcelable;
import android.hardware.camera2.marshal.impl.MarshalQueryablePrimitive;
import android.hardware.camera2.marshal.impl.MarshalQueryableRange;
import android.hardware.camera2.marshal.impl.MarshalQueryableRecommendedStreamConfiguration;
import android.hardware.camera2.marshal.impl.MarshalQueryableRect;
import android.hardware.camera2.marshal.impl.MarshalQueryableReprocessFormatsMap;
import android.hardware.camera2.marshal.impl.MarshalQueryableRggbChannelVector;
import android.hardware.camera2.marshal.impl.MarshalQueryableSize;
import android.hardware.camera2.marshal.impl.MarshalQueryableSizeF;
import android.hardware.camera2.marshal.impl.MarshalQueryableStreamConfiguration;
import android.hardware.camera2.marshal.impl.MarshalQueryableStreamConfigurationDuration;
import android.hardware.camera2.marshal.impl.MarshalQueryableString;
import android.hardware.camera2.params.Capability;
import android.hardware.camera2.params.Face;
import android.hardware.camera2.params.HighSpeedVideoConfiguration;
import android.hardware.camera2.params.LensShadingMap;
import android.hardware.camera2.params.MandatoryStreamCombination;
import android.hardware.camera2.params.OisSample;
import android.hardware.camera2.params.RecommendedStreamConfiguration;
import android.hardware.camera2.params.RecommendedStreamConfigurationMap;
import android.hardware.camera2.params.ReprocessFormatsMap;
import android.hardware.camera2.params.StreamConfiguration;
import android.hardware.camera2.params.StreamConfigurationDuration;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.hardware.camera2.params.TonemapCurve;
import android.hardware.camera2.utils.TypeReference;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ServiceSpecificException;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import dalvik.annotation.optimization.FastNative;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class CameraMetadataNative implements Parcelable {
  private static final String CELLID_PROCESS = "CELLID";
  
  class Key<T> {
    private final String mFallbackName;
    
    private boolean mHasTag;
    
    private final int mHash;
    
    private final String mName;
    
    private int mTag;
    
    private final Class<T> mType;
    
    private final TypeReference<T> mTypeReference;
    
    private long mVendorId = Long.MAX_VALUE;
    
    public Key(CameraMetadataNative this$0, Class<T> param1Class, long param1Long) {
      if (this$0 != null) {
        if (param1Class != null) {
          this.mName = (String)this$0;
          this.mFallbackName = null;
          this.mType = param1Class;
          this.mVendorId = param1Long;
          this.mTypeReference = TypeReference.createSpecializedTypeReference(param1Class);
          this.mHash = this.mName.hashCode() ^ this.mTypeReference.hashCode();
          return;
        } 
        throw new NullPointerException("Type needs to be non-null");
      } 
      throw new NullPointerException("Key needs a valid name");
    }
    
    public Key(CameraMetadataNative this$0, String param1String1, Class<T> param1Class) {
      if (this$0 != null) {
        if (param1Class != null) {
          this.mName = (String)this$0;
          this.mFallbackName = param1String1;
          this.mType = param1Class;
          this.mTypeReference = TypeReference.createSpecializedTypeReference(param1Class);
          this.mHash = this.mName.hashCode() ^ this.mTypeReference.hashCode();
          return;
        } 
        throw new NullPointerException("Type needs to be non-null");
      } 
      throw new NullPointerException("Key needs a valid name");
    }
    
    public Key(CameraMetadataNative this$0, Class<T> param1Class) {
      if (this$0 != null) {
        if (param1Class != null) {
          this.mName = (String)this$0;
          this.mFallbackName = null;
          this.mType = param1Class;
          this.mTypeReference = TypeReference.createSpecializedTypeReference(param1Class);
          this.mHash = this.mName.hashCode() ^ this.mTypeReference.hashCode();
          return;
        } 
        throw new NullPointerException("Type needs to be non-null");
      } 
      throw new NullPointerException("Key needs a valid name");
    }
    
    public Key(CameraMetadataNative this$0, TypeReference<T> param1TypeReference) {
      if (this$0 != null) {
        if (param1TypeReference != null) {
          this.mName = (String)this$0;
          this.mFallbackName = null;
          this.mType = (Class)param1TypeReference.getRawType();
          this.mTypeReference = param1TypeReference;
          this.mHash = this.mName.hashCode() ^ this.mTypeReference.hashCode();
          return;
        } 
        throw new NullPointerException("TypeReference needs to be non-null");
      } 
      throw new NullPointerException("Key needs a valid name");
    }
    
    public final String getName() {
      return this.mName;
    }
    
    public final int hashCode() {
      return this.mHash;
    }
    
    public final boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || hashCode() != param1Object.hashCode())
        return false; 
      if (param1Object instanceof CaptureResult.Key) {
        param1Object = ((CaptureResult.Key)param1Object).getNativeKey();
      } else if (param1Object instanceof CaptureRequest.Key) {
        param1Object = ((CaptureRequest.Key)param1Object).getNativeKey();
      } else if (param1Object instanceof CameraCharacteristics.Key) {
        param1Object = ((CameraCharacteristics.Key)param1Object).getNativeKey();
      } else if (param1Object instanceof Key) {
        param1Object = param1Object;
      } else {
        return false;
      } 
      if (!this.mName.equals(((Key)param1Object).mName) || !this.mTypeReference.equals(((Key)param1Object).mTypeReference))
        bool = false; 
      return bool;
    }
    
    public final int getTag() {
      if (!this.mHasTag) {
        this.mTag = CameraMetadataNative.getTag(this.mName, this.mVendorId);
        this.mHasTag = true;
      } 
      return this.mTag;
    }
    
    public final boolean hasTag() {
      return this.mHasTag;
    }
    
    public final void cacheTag(int param1Int) {
      this.mHasTag = true;
      this.mTag = param1Int;
    }
    
    public final Class<T> getType() {
      return this.mType;
    }
    
    public final long getVendorId() {
      return this.mVendorId;
    }
    
    public final TypeReference<T> getTypeReference() {
      return this.mTypeReference;
    }
  }
  
  private static String translateLocationProviderToProcess(String paramString) {
    if (paramString == null)
      return null; 
    byte b = -1;
    int i = paramString.hashCode();
    if (i != 102570) {
      if (i == 1843485230 && paramString.equals("network"))
        b = 1; 
    } else if (paramString.equals("gps")) {
      b = 0;
    } 
    if (b != 0) {
      if (b != 1)
        return null; 
      return "CELLID";
    } 
    return "GPS";
  }
  
  private static String translateProcessToLocationProvider(String paramString) {
    if (paramString == null)
      return null; 
    byte b = -1;
    int i = paramString.hashCode();
    if (i != 70794) {
      if (i == 1984215549 && paramString.equals("CELLID"))
        b = 1; 
    } else if (paramString.equals("GPS")) {
      b = 0;
    } 
    if (b != 0) {
      if (b != 1)
        return null; 
      return "network";
    } 
    return "gps";
  }
  
  public CameraMetadataNative() {
    this.mCameraId = -1;
    this.mHasMandatoryConcurrentStreams = false;
    this.mDisplaySize = new Size(0, 0);
    long l = nativeAllocate();
    if (l != 0L)
      return; 
    throw new OutOfMemoryError("Failed to allocate native CameraMetadata");
  }
  
  public CameraMetadataNative(CameraMetadataNative paramCameraMetadataNative) {
    this.mCameraId = -1;
    this.mHasMandatoryConcurrentStreams = false;
    this.mDisplaySize = new Size(0, 0);
    long l = nativeAllocateCopy(paramCameraMetadataNative.mMetadataPtr);
    if (l != 0L)
      return; 
    throw new OutOfMemoryError("Failed to allocate native CameraMetadata");
  }
  
  public static CameraMetadataNative move(CameraMetadataNative paramCameraMetadataNative) {
    CameraMetadataNative cameraMetadataNative = new CameraMetadataNative();
    cameraMetadataNative.swap(paramCameraMetadataNative);
    return cameraMetadataNative;
  }
  
  public static final Parcelable.Creator<CameraMetadataNative> CREATOR = new Parcelable.Creator<CameraMetadataNative>() {
      public CameraMetadataNative createFromParcel(Parcel param1Parcel) {
        CameraMetadataNative cameraMetadataNative = new CameraMetadataNative();
        cameraMetadataNative.readFromParcel(param1Parcel);
        return cameraMetadataNative;
      }
      
      public CameraMetadataNative[] newArray(int param1Int) {
        return new CameraMetadataNative[param1Int];
      }
    };
  
  private static final boolean DEBUG = false;
  
  private static final int FACE_LANDMARK_SIZE = 6;
  
  private static final String GPS_PROCESS = "GPS";
  
  public static final int NATIVE_JPEG_FORMAT = 33;
  
  public static final int NUM_TYPES = 6;
  
  private static final String TAG = "CameraMetadataJV";
  
  public static final int TYPE_BYTE = 0;
  
  public static final int TYPE_DOUBLE = 4;
  
  public static final int TYPE_FLOAT = 2;
  
  public static final int TYPE_INT32 = 1;
  
  public static final int TYPE_INT64 = 3;
  
  public static final int TYPE_RATIONAL = 5;
  
  private static final HashMap<Key<?>, GetCommand> sGetCommandMap;
  
  private static final HashMap<Key<?>, SetCommand> sSetCommandMap;
  
  private int mCameraId;
  
  private Size mDisplaySize;
  
  private boolean mHasMandatoryConcurrentStreams;
  
  private long mMetadataPtr;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    nativeWriteToParcel(paramParcel, this.mMetadataPtr);
  }
  
  public <T> T get(CameraCharacteristics.Key<T> paramKey) {
    return get(paramKey.getNativeKey());
  }
  
  public <T> T get(CaptureResult.Key<T> paramKey) {
    return get(paramKey.getNativeKey());
  }
  
  public <T> T get(CaptureRequest.Key<T> paramKey) {
    return get(paramKey.getNativeKey());
  }
  
  public <T> T get(Key<T> paramKey) {
    Objects.requireNonNull(paramKey, "key must not be null");
    GetCommand getCommand = sGetCommandMap.get(paramKey);
    if (getCommand != null)
      return getCommand.getValue(this, paramKey); 
    return getBase(paramKey);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    nativeReadFromParcel(paramParcel, this.mMetadataPtr);
  }
  
  public static void setupGlobalVendorTagDescriptor() throws ServiceSpecificException {
    int i = nativeSetupGlobalVendorTagDescriptor();
    if (i == 0)
      return; 
    throw new ServiceSpecificException(i, "Failure to set up global vendor tags");
  }
  
  public <T> void set(Key<T> paramKey, T paramT) {
    SetCommand setCommand = sSetCommandMap.get(paramKey);
    if (setCommand != null) {
      setCommand.setValue(this, paramT);
      return;
    } 
    setBase(paramKey, paramT);
  }
  
  public <T> void set(CaptureRequest.Key<T> paramKey, T paramT) {
    set(paramKey.getNativeKey(), paramT);
  }
  
  public <T> void set(CaptureResult.Key<T> paramKey, T paramT) {
    set(paramKey.getNativeKey(), paramT);
  }
  
  public <T> void set(CameraCharacteristics.Key<T> paramKey, T paramT) {
    set(paramKey.getNativeKey(), paramT);
  }
  
  private void close() {
    nativeClose(this.mMetadataPtr);
    this.mMetadataPtr = 0L;
  }
  
  private <T> T getBase(CameraCharacteristics.Key<T> paramKey) {
    return getBase(paramKey.getNativeKey());
  }
  
  private <T> T getBase(CaptureResult.Key<T> paramKey) {
    return getBase(paramKey.getNativeKey());
  }
  
  private <T> T getBase(CaptureRequest.Key<T> paramKey) {
    return getBase(paramKey.getNativeKey());
  }
  
  private <T> T getBase(Key<T> paramKey) {
    if (paramKey.hasTag()) {
      i = paramKey.getTag();
    } else {
      i = nativeGetTagFromKeyLocal(this.mMetadataPtr, paramKey.getName());
      paramKey.cacheTag(i);
    } 
    byte[] arrayOfByte1 = readValues(i);
    byte[] arrayOfByte2 = arrayOfByte1;
    if (arrayOfByte1 == null) {
      if (paramKey.mFallbackName == null)
        return null; 
      i = nativeGetTagFromKeyLocal(this.mMetadataPtr, paramKey.mFallbackName);
      arrayOfByte1 = readValues(i);
      arrayOfByte2 = arrayOfByte1;
      if (arrayOfByte1 == null)
        return null; 
    } 
    int i = nativeGetTypeFromTagLocal(this.mMetadataPtr, i);
    Marshaler<T> marshaler = getMarshalerForKey(paramKey, i);
    ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte2).order(ByteOrder.nativeOrder());
    return marshaler.unmarshal(byteBuffer);
  }
  
  static {
    HashMap<Object, Object> hashMap1 = new HashMap<>();
    CameraCharacteristics.Key<int[]> key24 = CameraCharacteristics.SCALER_AVAILABLE_FORMATS;
    Key<int> key23 = key24.getNativeKey();
    GetCommand getCommand12 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getAvailableFormats();
        }
      };
    hashMap1.put(key23, getCommand12);
    HashMap<Key<?>, GetCommand> hashMap = sGetCommandMap;
    CaptureResult.Key<Face[]> key22 = CaptureResult.STATISTICS_FACES;
    Key<Face> key21 = key22.getNativeKey();
    getCommand12 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getFaces();
        }
      };
    hashMap.put(key21, getCommand12);
    hashMap = sGetCommandMap;
    CaptureResult.Key<Rect[]> key20 = CaptureResult.STATISTICS_FACE_RECTANGLES;
    Key<Rect> key19 = key20.getNativeKey();
    getCommand12 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getFaceRectangles();
        }
      };
    hashMap.put(key19, getCommand12);
    hashMap = sGetCommandMap;
    CameraCharacteristics.Key<StreamConfigurationMap> key18 = CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP;
    Key<StreamConfigurationMap> key17 = key18.getNativeKey();
    getCommand12 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getStreamConfigurationMap();
        }
      };
    hashMap.put(key17, getCommand12);
    hashMap = sGetCommandMap;
    CameraCharacteristics.Key<MandatoryStreamCombination[]> key16 = CameraCharacteristics.SCALER_MANDATORY_STREAM_COMBINATIONS;
    Key<MandatoryStreamCombination> key15 = key16.getNativeKey();
    getCommand12 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getMandatoryStreamCombinations();
        }
      };
    hashMap.put(key15, getCommand12);
    hashMap = sGetCommandMap;
    CameraCharacteristics.Key<MandatoryStreamCombination[]> key14 = CameraCharacteristics.SCALER_MANDATORY_CONCURRENT_STREAM_COMBINATIONS;
    Key<MandatoryStreamCombination> key27 = key14.getNativeKey();
    GetCommand getCommand9 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getMandatoryConcurrentStreamCombinations();
        }
      };
    hashMap.put(key27, getCommand9);
    hashMap = sGetCommandMap;
    CameraCharacteristics.Key<Integer> key13 = CameraCharacteristics.CONTROL_MAX_REGIONS_AE;
    Key<Integer> key12 = key13.getNativeKey();
    GetCommand getCommand11 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getMaxRegions(param1Key);
        }
      };
    hashMap.put(key12, getCommand11);
    hashMap = sGetCommandMap;
    CameraCharacteristics.Key<Integer> key11 = CameraCharacteristics.CONTROL_MAX_REGIONS_AWB;
    Key<Integer> key10 = key11.getNativeKey();
    getCommand11 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getMaxRegions(param1Key);
        }
      };
    hashMap.put(key10, getCommand11);
    hashMap = sGetCommandMap;
    CameraCharacteristics.Key<Integer> key9 = CameraCharacteristics.CONTROL_MAX_REGIONS_AF;
    Key<Integer> key26 = key9.getNativeKey();
    GetCommand getCommand8 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getMaxRegions(param1Key);
        }
      };
    hashMap.put(key26, getCommand8);
    hashMap = sGetCommandMap;
    CameraCharacteristics.Key<Integer> key8 = CameraCharacteristics.REQUEST_MAX_NUM_OUTPUT_RAW;
    key26 = key8.getNativeKey();
    GetCommand getCommand7 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getMaxNumOutputs(param1Key);
        }
      };
    hashMap.put(key26, getCommand7);
    hashMap = sGetCommandMap;
    CameraCharacteristics.Key<Integer> key7 = CameraCharacteristics.REQUEST_MAX_NUM_OUTPUT_PROC;
    key26 = key7.getNativeKey();
    GetCommand getCommand6 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getMaxNumOutputs(param1Key);
        }
      };
    hashMap.put(key26, getCommand6);
    hashMap = sGetCommandMap;
    CameraCharacteristics.Key<Integer> key6 = CameraCharacteristics.REQUEST_MAX_NUM_OUTPUT_PROC_STALLING;
    key26 = key6.getNativeKey();
    GetCommand getCommand5 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getMaxNumOutputs(param1Key);
        }
      };
    hashMap.put(key26, getCommand5);
    hashMap = sGetCommandMap;
    CaptureRequest.Key<TonemapCurve> key5 = CaptureRequest.TONEMAP_CURVE;
    Key<TonemapCurve> key4 = key5.getNativeKey();
    GetCommand getCommand10 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getTonemapCurve();
        }
      };
    hashMap.put(key4, getCommand10);
    hashMap = sGetCommandMap;
    CaptureResult.Key<Location> key3 = CaptureResult.JPEG_GPS_LOCATION;
    Key<Location> key25 = key3.getNativeKey();
    GetCommand getCommand4 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getGpsLocation();
        }
      };
    hashMap.put(key25, getCommand4);
    hashMap = sGetCommandMap;
    CaptureResult.Key<LensShadingMap> key2 = CaptureResult.STATISTICS_LENS_SHADING_CORRECTION_MAP;
    key25 = (Key)key2.getNativeKey();
    GetCommand getCommand3 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getLensShadingMap();
        }
      };
    hashMap.put(key25, getCommand3);
    hashMap = sGetCommandMap;
    CaptureResult.Key<OisSample[]> key1 = CaptureResult.STATISTICS_OIS_SAMPLES;
    key25 = (Key)key1.getNativeKey();
    GetCommand getCommand2 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getOisSamples();
        }
      };
    hashMap.put(key25, getCommand2);
    hashMap = sGetCommandMap;
    CameraCharacteristics.Key<Capability[]> key = CameraCharacteristics.CONTROL_AVAILABLE_EXTENDED_SCENE_MODE_CAPABILITIES;
    key25 = (Key)key.getNativeKey();
    GetCommand getCommand1 = new GetCommand() {
        public <T> T getValue(CameraMetadataNative param1CameraMetadataNative, CameraMetadataNative.Key<T> param1Key) {
          return (T)param1CameraMetadataNative.getExtendedSceneModeCapabilities();
        }
      };
    hashMap.put(key25, getCommand1);
    sSetCommandMap = (HashMap)(hashMap = new HashMap<>());
    hashMap.put(CameraCharacteristics.SCALER_AVAILABLE_FORMATS.getNativeKey(), new SetCommand() {
          public <T> void setValue(CameraMetadataNative param1CameraMetadataNative, T param1T) {
            param1CameraMetadataNative.setAvailableFormats((int[])param1T);
          }
        });
    sSetCommandMap.put(CaptureResult.STATISTICS_FACE_RECTANGLES.getNativeKey(), new SetCommand() {
          public <T> void setValue(CameraMetadataNative param1CameraMetadataNative, T param1T) {
            param1CameraMetadataNative.setFaceRectangles((Rect[])param1T);
          }
        });
    sSetCommandMap.put(CaptureResult.STATISTICS_FACES.getNativeKey(), new SetCommand() {
          public <T> void setValue(CameraMetadataNative param1CameraMetadataNative, T param1T) {
            param1CameraMetadataNative.setFaces((Face[])param1T);
          }
        });
    sSetCommandMap.put(CaptureRequest.TONEMAP_CURVE.getNativeKey(), new SetCommand() {
          public <T> void setValue(CameraMetadataNative param1CameraMetadataNative, T param1T) {
            param1CameraMetadataNative.setTonemapCurve((TonemapCurve)param1T);
          }
        });
    sSetCommandMap.put(CaptureResult.JPEG_GPS_LOCATION.getNativeKey(), new SetCommand() {
          public <T> void setValue(CameraMetadataNative param1CameraMetadataNative, T param1T) {
            param1CameraMetadataNative.setGpsLocation((Location)param1T);
          }
        });
    registerAllMarshalers();
  }
  
  private int[] getAvailableFormats() {
    int[] arrayOfInt = getBase((CameraCharacteristics.Key)CameraCharacteristics.SCALER_AVAILABLE_FORMATS);
    if (arrayOfInt != null)
      for (byte b = 0; b < arrayOfInt.length; b++) {
        if (arrayOfInt[b] == 33)
          arrayOfInt[b] = 256; 
      }  
    return arrayOfInt;
  }
  
  private boolean setFaces(Face[] paramArrayOfFace) {
    int i = 0;
    if (paramArrayOfFace == null)
      return false; 
    int j = paramArrayOfFace.length;
    boolean bool = true;
    int k, m;
    for (k = paramArrayOfFace.length, m = 0; m < k; ) {
      int i1;
      Face face = paramArrayOfFace[m];
      if (face == null) {
        i1 = j - 1;
        Log.w("CameraMetadataJV", "setFaces - null face detected, skipping");
      } else {
        i1 = j;
        if (face.getId() == -1) {
          bool = false;
          i1 = j;
        } 
      } 
      m++;
      j = i1;
    } 
    Rect[] arrayOfRect = new Rect[j];
    byte[] arrayOfByte = new byte[j];
    int[] arrayOfInt2 = null;
    int[] arrayOfInt1 = null;
    if (bool) {
      arrayOfInt2 = new int[j];
      arrayOfInt1 = new int[j * 6];
    } 
    j = 0;
    for (int n = paramArrayOfFace.length; m < n; ) {
      Face face = paramArrayOfFace[m];
      if (face != null) {
        arrayOfRect[j] = face.getBounds();
        arrayOfByte[j] = (byte)face.getScore();
        if (bool) {
          arrayOfInt2[j] = face.getId();
          k = 0 + 1;
          arrayOfInt1[j * 6 + 0] = (face.getLeftEyePosition()).x;
          i = k + 1;
          arrayOfInt1[j * 6 + k] = (face.getLeftEyePosition()).y;
          k = i + 1;
          arrayOfInt1[j * 6 + i] = (face.getRightEyePosition()).x;
          i = k + 1;
          arrayOfInt1[j * 6 + k] = (face.getRightEyePosition()).y;
          k = i + 1;
          arrayOfInt1[j * 6 + i] = (face.getMouthPosition()).x;
          arrayOfInt1[j * 6 + k] = (face.getMouthPosition()).y;
        } 
        j++;
      } 
      m++;
    } 
    set((CaptureResult.Key)CaptureResult.STATISTICS_FACE_RECTANGLES, arrayOfRect);
    set((CaptureResult.Key)CaptureResult.STATISTICS_FACE_IDS, arrayOfInt2);
    set((CaptureResult.Key)CaptureResult.STATISTICS_FACE_LANDMARKS, arrayOfInt1);
    set((CaptureResult.Key)CaptureResult.STATISTICS_FACE_SCORES, arrayOfByte);
    return true;
  }
  
  private Face[] getFaces() {
    StringBuilder stringBuilder2;
    Integer integer = get(CaptureResult.STATISTICS_FACE_DETECT_MODE);
    byte[] arrayOfByte = get((CaptureResult.Key)CaptureResult.STATISTICS_FACE_SCORES);
    Rect[] arrayOfRect = get((CaptureResult.Key)CaptureResult.STATISTICS_FACE_RECTANGLES);
    int[] arrayOfInt1 = get((CaptureResult.Key)CaptureResult.STATISTICS_FACE_IDS);
    int[] arrayOfInt2 = get((CaptureResult.Key)CaptureResult.STATISTICS_FACE_LANDMARKS);
    if (areValuesAllNull(new Object[] { integer, arrayOfByte, arrayOfRect, arrayOfInt1, arrayOfInt2 }))
      return null; 
    if (integer == null) {
      Log.w("CameraMetadataJV", "Face detect mode metadata is null, assuming the mode is SIMPLE");
      Integer integer1 = Integer.valueOf(1);
    } else if (integer.intValue() > 2) {
      Integer integer1 = Integer.valueOf(2);
    } else {
      if (integer.intValue() == 0)
        return new Face[0]; 
      Integer integer1 = integer;
      if (integer.intValue() != 1) {
        integer1 = integer;
        if (integer.intValue() != 2) {
          stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Unknown face detect mode: ");
          stringBuilder2.append(integer);
          Log.w("CameraMetadataJV", stringBuilder2.toString());
          return new Face[0];
        } 
      } 
    } 
    if (arrayOfByte == null || arrayOfRect == null) {
      Log.w("CameraMetadataJV", "Expect face scores and rectangles to be non-null");
      return new Face[0];
    } 
    if (arrayOfByte.length != arrayOfRect.length) {
      int k = arrayOfByte.length;
      int m = arrayOfRect.length;
      Log.w("CameraMetadataJV", String.format("Face score size(%d) doesn match face rectangle size(%d)!", new Object[] { Integer.valueOf(k), Integer.valueOf(m) }));
    } 
    int i = Math.min(arrayOfByte.length, arrayOfRect.length);
    StringBuilder stringBuilder1 = stringBuilder2;
    int j = i;
    if (stringBuilder2.intValue() == 2)
      if (arrayOfInt1 == null || arrayOfInt2 == null) {
        Log.w("CameraMetadataJV", "Expect face ids and landmarks to be non-null for FULL mode,fallback to SIMPLE mode");
        Integer integer1 = Integer.valueOf(1);
        j = i;
      } else {
        if (arrayOfInt1.length != i || arrayOfInt2.length != i * 6) {
          j = arrayOfInt1.length;
          int k = arrayOfInt2.length;
          Log.w("CameraMetadataJV", String.format("Face id size(%d), or face landmark size(%d) don'tmatch face number(%d)!", new Object[] { Integer.valueOf(j), Integer.valueOf(k * 6), Integer.valueOf(i) }));
        } 
        j = Math.min(i, arrayOfInt1.length);
        j = Math.min(j, arrayOfInt2.length / 6);
        stringBuilder1 = stringBuilder2;
      }  
    ArrayList<Face> arrayList = new ArrayList();
    if (stringBuilder1.intValue() == 1) {
      for (i = 0; i < j; i++) {
        if (arrayOfByte[i] <= 100 && arrayOfByte[i] >= 1)
          arrayList.add(new Face(arrayOfRect[i], arrayOfByte[i])); 
      } 
    } else {
      for (i = 0; i < j; i++) {
        if (arrayOfByte[i] <= 100 && arrayOfByte[i] >= 1 && arrayOfInt1[i] >= 0) {
          Point point1 = new Point(arrayOfInt2[i * 6], arrayOfInt2[i * 6 + 1]);
          Point point2 = new Point(arrayOfInt2[i * 6 + 2], arrayOfInt2[i * 6 + 3]);
          Point point3 = new Point(arrayOfInt2[i * 6 + 4], arrayOfInt2[i * 6 + 5]);
          Face face = new Face(arrayOfRect[i], arrayOfByte[i], arrayOfInt1[i], point1, point2, point3);
          arrayList.add(face);
        } 
      } 
    } 
    Face[] arrayOfFace = new Face[arrayList.size()];
    arrayList.toArray(arrayOfFace);
    return arrayOfFace;
  }
  
  private Rect[] getFaceRectangles() {
    Rect[] arrayOfRect1 = getBase((CaptureResult.Key)CaptureResult.STATISTICS_FACE_RECTANGLES);
    if (arrayOfRect1 == null)
      return null; 
    Rect[] arrayOfRect2 = new Rect[arrayOfRect1.length];
    for (byte b = 0; b < arrayOfRect1.length; b++)
      arrayOfRect2[b] = new Rect((arrayOfRect1[b]).left, (arrayOfRect1[b]).top, (arrayOfRect1[b]).right - (arrayOfRect1[b]).left, (arrayOfRect1[b]).bottom - (arrayOfRect1[b]).top); 
    return arrayOfRect2;
  }
  
  private LensShadingMap getLensShadingMap() {
    float[] arrayOfFloat = getBase((CaptureResult.Key)CaptureResult.STATISTICS_LENS_SHADING_MAP);
    Size size = get(CameraCharacteristics.LENS_INFO_SHADING_MAP_SIZE);
    if (arrayOfFloat == null)
      return null; 
    if (size == null) {
      Log.w("CameraMetadataJV", "getLensShadingMap - Lens shading map size was null.");
      return null;
    } 
    return new LensShadingMap(arrayOfFloat, size.getHeight(), size.getWidth());
  }
  
  private Location getGpsLocation() {
    String str = get(CaptureResult.JPEG_GPS_PROCESSING_METHOD);
    double[] arrayOfDouble = get((CaptureResult.Key)CaptureResult.JPEG_GPS_COORDINATES);
    Long long_ = get(CaptureResult.JPEG_GPS_TIMESTAMP);
    if (areValuesAllNull(new Object[] { str, arrayOfDouble, long_ }))
      return null; 
    Location location = new Location(translateProcessToLocationProvider(str));
    if (long_ != null) {
      location.setTime(long_.longValue() * 1000L);
    } else {
      Log.w("CameraMetadataJV", "getGpsLocation - No timestamp for GPS location.");
    } 
    if (arrayOfDouble != null) {
      location.setLatitude(arrayOfDouble[0]);
      location.setLongitude(arrayOfDouble[1]);
      location.setAltitude(arrayOfDouble[2]);
    } else {
      Log.w("CameraMetadataJV", "getGpsLocation - No coordinates for GPS location");
    } 
    return location;
  }
  
  private boolean setGpsLocation(Location paramLocation) {
    if (paramLocation == null)
      return false; 
    double d1 = paramLocation.getLatitude(), d2 = paramLocation.getLongitude(), d3 = paramLocation.getAltitude();
    String str = translateLocationProviderToProcess(paramLocation.getProvider());
    long l = paramLocation.getTime() / 1000L;
    set(CaptureRequest.JPEG_GPS_TIMESTAMP, Long.valueOf(l));
    set((CaptureRequest.Key)CaptureRequest.JPEG_GPS_COORDINATES, new double[] { d1, d2, d3 });
    if (str == null) {
      Log.w("CameraMetadataJV", "setGpsLocation - No process method, Location is not from a GPS or NETWORKprovider");
    } else {
      setBase(CaptureRequest.JPEG_GPS_PROCESSING_METHOD, str);
    } 
    return true;
  }
  
  private void parseRecommendedConfigurations(RecommendedStreamConfiguration[] paramArrayOfRecommendedStreamConfiguration, StreamConfigurationMap paramStreamConfigurationMap, boolean paramBoolean, ArrayList<ArrayList<StreamConfiguration>> paramArrayList, ArrayList<ArrayList<StreamConfigurationDuration>> paramArrayList1, ArrayList<ArrayList<StreamConfigurationDuration>> paramArrayList2, boolean[] paramArrayOfboolean) {
    paramArrayList.ensureCapacity(32);
    paramArrayList1.ensureCapacity(32);
    paramArrayList2.ensureCapacity(32);
    int i;
    for (i = 0; i < 32; i++) {
      paramArrayList.add(new ArrayList<>());
      paramArrayList1.add(new ArrayList<>());
      paramArrayList2.add(new ArrayList<>());
    } 
    byte b;
    for (i = paramArrayOfRecommendedStreamConfiguration.length, b = 0; b < i; ) {
      int n;
      RecommendedStreamConfiguration recommendedStreamConfiguration = paramArrayOfRecommendedStreamConfiguration[b];
      int j = recommendedStreamConfiguration.getWidth();
      int k = recommendedStreamConfiguration.getHeight();
      int m = recommendedStreamConfiguration.getFormat();
      if (paramBoolean) {
        n = StreamConfigurationMap.depthFormatToPublic(m);
      } else {
        n = StreamConfigurationMap.imageFormatToPublic(m);
      } 
      Size size = new Size(j, k);
      int i1 = recommendedStreamConfiguration.getUsecaseBitmap();
      if (!recommendedStreamConfiguration.isInput()) {
        StreamConfiguration streamConfiguration = new StreamConfiguration(m, j, k, false);
        long l1 = paramStreamConfigurationMap.getOutputMinFrameDuration(n, size);
        if (l1 > 0L) {
          StreamConfigurationDuration streamConfigurationDuration = new StreamConfigurationDuration(m, j, k, l1);
        } else {
          recommendedStreamConfiguration = null;
        } 
        long l2 = paramStreamConfigurationMap.getOutputStallDuration(n, size);
        if (l2 > 0L) {
          StreamConfigurationDuration streamConfigurationDuration = new StreamConfigurationDuration(m, j, k, l2);
        } else {
          size = null;
        } 
        for (k = 0; k < 32; k++) {
          if ((i1 & 1 << k) != 0) {
            ArrayList<StreamConfiguration> arrayList = paramArrayList.get(k);
            arrayList.add(streamConfiguration);
            if (l1 > 0L) {
              arrayList = (ArrayList<StreamConfiguration>)paramArrayList1.get(k);
              arrayList.add(recommendedStreamConfiguration);
            } 
            if (l2 > 0L) {
              arrayList = (ArrayList<StreamConfiguration>)paramArrayList2.get(k);
              arrayList.add(size);
            } 
            if (paramArrayOfboolean != null && !paramArrayOfboolean[k] && n == 34)
              paramArrayOfboolean[k] = true; 
          } 
        } 
      } else if (i1 == 16) {
        ArrayList<StreamConfiguration> arrayList = paramArrayList.get(4);
        arrayList.add(new StreamConfiguration(m, j, k, true));
      } else {
        throw new IllegalArgumentException("Recommended input stream configurations should only be advertised in the ZSL use case!");
      } 
      b++;
    } 
  }
  
  class StreamConfigurationData {
    StreamConfigurationDuration[] minDurationArray;
    
    StreamConfigurationDuration[] stallDurationArray;
    
    StreamConfiguration[] streamConfigurationArray;
    
    final CameraMetadataNative this$0;
    
    private StreamConfigurationData() {
      this.streamConfigurationArray = null;
      this.minDurationArray = null;
      this.stallDurationArray = null;
    }
  }
  
  public void initializeStreamConfigurationData(ArrayList<StreamConfiguration> paramArrayList, ArrayList<StreamConfigurationDuration> paramArrayList1, ArrayList<StreamConfigurationDuration> paramArrayList2, StreamConfigurationData paramStreamConfigurationData) {
    if (paramStreamConfigurationData == null || paramArrayList == null)
      return; 
    paramStreamConfigurationData.streamConfigurationArray = new StreamConfiguration[paramArrayList.size()];
    paramStreamConfigurationData.streamConfigurationArray = paramArrayList.<StreamConfiguration>toArray(paramStreamConfigurationData.streamConfigurationArray);
    if (paramArrayList1 != null && !paramArrayList1.isEmpty()) {
      paramStreamConfigurationData.minDurationArray = new StreamConfigurationDuration[paramArrayList1.size()];
      paramStreamConfigurationData.minDurationArray = paramArrayList1.<StreamConfigurationDuration>toArray(paramStreamConfigurationData.minDurationArray);
    } else {
      paramStreamConfigurationData.minDurationArray = new StreamConfigurationDuration[0];
    } 
    if (paramArrayList2 != null && !paramArrayList2.isEmpty()) {
      paramStreamConfigurationData.stallDurationArray = new StreamConfigurationDuration[paramArrayList2.size()];
      paramStreamConfigurationData.stallDurationArray = paramArrayList2.<StreamConfigurationDuration>toArray(paramStreamConfigurationData.stallDurationArray);
    } else {
      paramStreamConfigurationData.stallDurationArray = new StreamConfigurationDuration[0];
    } 
  }
  
  public ArrayList<RecommendedStreamConfigurationMap> getRecommendedStreamConfigurations() {
    // Byte code:
    //   0: aload_0
    //   1: getstatic android/hardware/camera2/CameraCharacteristics.SCALER_AVAILABLE_RECOMMENDED_STREAM_CONFIGURATIONS : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   4: invokespecial getBase : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   7: checkcast [Landroid/hardware/camera2/params/RecommendedStreamConfiguration;
    //   10: astore_1
    //   11: aload_0
    //   12: getstatic android/hardware/camera2/CameraCharacteristics.DEPTH_AVAILABLE_RECOMMENDED_DEPTH_STREAM_CONFIGURATIONS : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   15: invokespecial getBase : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   18: checkcast [Landroid/hardware/camera2/params/RecommendedStreamConfiguration;
    //   21: astore_2
    //   22: aload_1
    //   23: ifnonnull -> 32
    //   26: aload_2
    //   27: ifnonnull -> 32
    //   30: aconst_null
    //   31: areturn
    //   32: aload_0
    //   33: invokespecial getStreamConfigurationMap : ()Landroid/hardware/camera2/params/StreamConfigurationMap;
    //   36: astore_3
    //   37: new java/util/ArrayList
    //   40: dup
    //   41: invokespecial <init> : ()V
    //   44: astore #4
    //   46: new java/util/ArrayList
    //   49: dup
    //   50: invokespecial <init> : ()V
    //   53: astore #5
    //   55: new java/util/ArrayList
    //   58: dup
    //   59: invokespecial <init> : ()V
    //   62: astore #6
    //   64: new java/util/ArrayList
    //   67: dup
    //   68: invokespecial <init> : ()V
    //   71: astore #7
    //   73: bipush #32
    //   75: newarray boolean
    //   77: astore #8
    //   79: aload_1
    //   80: ifnull -> 113
    //   83: aload_0
    //   84: aload_1
    //   85: aload_3
    //   86: iconst_0
    //   87: aload #5
    //   89: aload #6
    //   91: aload #7
    //   93: aload #8
    //   95: invokespecial parseRecommendedConfigurations : ([Landroid/hardware/camera2/params/RecommendedStreamConfiguration;Landroid/hardware/camera2/params/StreamConfigurationMap;ZLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;[Z)V
    //   98: goto -> 113
    //   101: astore_3
    //   102: ldc 'CameraMetadataJV'
    //   104: ldc_w 'Failed parsing the recommended stream configurations!'
    //   107: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   110: pop
    //   111: aconst_null
    //   112: areturn
    //   113: new java/util/ArrayList
    //   116: dup
    //   117: invokespecial <init> : ()V
    //   120: astore #9
    //   122: new java/util/ArrayList
    //   125: dup
    //   126: invokespecial <init> : ()V
    //   129: astore #10
    //   131: new java/util/ArrayList
    //   134: dup
    //   135: invokespecial <init> : ()V
    //   138: astore #11
    //   140: aload_2
    //   141: ifnull -> 173
    //   144: aload_0
    //   145: aload_2
    //   146: aload_3
    //   147: iconst_1
    //   148: aload #9
    //   150: aload #10
    //   152: aload #11
    //   154: aconst_null
    //   155: invokespecial parseRecommendedConfigurations : ([Landroid/hardware/camera2/params/RecommendedStreamConfiguration;Landroid/hardware/camera2/params/StreamConfigurationMap;ZLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;[Z)V
    //   158: goto -> 173
    //   161: astore_3
    //   162: ldc 'CameraMetadataJV'
    //   164: ldc_w 'Failed parsing the recommended depth stream configurations!'
    //   167: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   170: pop
    //   171: aconst_null
    //   172: areturn
    //   173: aload_0
    //   174: getstatic android/hardware/camera2/CameraCharacteristics.SCALER_AVAILABLE_RECOMMENDED_INPUT_OUTPUT_FORMATS_MAP : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   177: invokespecial getBase : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   180: checkcast android/hardware/camera2/params/ReprocessFormatsMap
    //   183: astore #12
    //   185: aload_0
    //   186: getstatic android/hardware/camera2/CameraCharacteristics.CONTROL_AVAILABLE_HIGH_SPEED_VIDEO_CONFIGURATIONS : Landroid/hardware/camera2/CameraCharacteristics$Key;
    //   189: invokespecial getBase : (Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
    //   192: checkcast [Landroid/hardware/camera2/params/HighSpeedVideoConfiguration;
    //   195: astore #13
    //   197: aload_0
    //   198: invokespecial isBurstSupported : ()Z
    //   201: istore #14
    //   203: aload #4
    //   205: bipush #32
    //   207: invokevirtual ensureCapacity : (I)V
    //   210: iconst_0
    //   211: istore #15
    //   213: iload #15
    //   215: bipush #32
    //   217: if_icmpge -> 645
    //   220: new android/hardware/camera2/impl/CameraMetadataNative$StreamConfigurationData
    //   223: dup
    //   224: aload_0
    //   225: aconst_null
    //   226: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraMetadataNative;Landroid/hardware/camera2/impl/CameraMetadataNative$1;)V
    //   229: astore_3
    //   230: aload_1
    //   231: ifnull -> 284
    //   234: aload #5
    //   236: iload #15
    //   238: invokevirtual get : (I)Ljava/lang/Object;
    //   241: checkcast java/util/ArrayList
    //   244: astore #16
    //   246: aload #6
    //   248: iload #15
    //   250: invokevirtual get : (I)Ljava/lang/Object;
    //   253: checkcast java/util/ArrayList
    //   256: astore #17
    //   258: aload #7
    //   260: iload #15
    //   262: invokevirtual get : (I)Ljava/lang/Object;
    //   265: checkcast java/util/ArrayList
    //   268: astore #18
    //   270: aload_0
    //   271: aload #16
    //   273: aload #17
    //   275: aload #18
    //   277: aload_3
    //   278: invokevirtual initializeStreamConfigurationData : (Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/hardware/camera2/impl/CameraMetadataNative$StreamConfigurationData;)V
    //   281: goto -> 284
    //   284: new android/hardware/camera2/impl/CameraMetadataNative$StreamConfigurationData
    //   287: dup
    //   288: aload_0
    //   289: aconst_null
    //   290: invokespecial <init> : (Landroid/hardware/camera2/impl/CameraMetadataNative;Landroid/hardware/camera2/impl/CameraMetadataNative$1;)V
    //   293: astore #19
    //   295: aload_2
    //   296: ifnull -> 350
    //   299: aload #9
    //   301: iload #15
    //   303: invokevirtual get : (I)Ljava/lang/Object;
    //   306: checkcast java/util/ArrayList
    //   309: astore #17
    //   311: aload #10
    //   313: iload #15
    //   315: invokevirtual get : (I)Ljava/lang/Object;
    //   318: checkcast java/util/ArrayList
    //   321: astore #18
    //   323: aload #11
    //   325: iload #15
    //   327: invokevirtual get : (I)Ljava/lang/Object;
    //   330: checkcast java/util/ArrayList
    //   333: astore #16
    //   335: aload_0
    //   336: aload #17
    //   338: aload #18
    //   340: aload #16
    //   342: aload #19
    //   344: invokevirtual initializeStreamConfigurationData : (Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/hardware/camera2/impl/CameraMetadataNative$StreamConfigurationData;)V
    //   347: goto -> 350
    //   350: aload_3
    //   351: getfield streamConfigurationArray : [Landroid/hardware/camera2/params/StreamConfiguration;
    //   354: ifnull -> 365
    //   357: aload_3
    //   358: getfield streamConfigurationArray : [Landroid/hardware/camera2/params/StreamConfiguration;
    //   361: arraylength
    //   362: ifne -> 385
    //   365: aload #19
    //   367: getfield streamConfigurationArray : [Landroid/hardware/camera2/params/StreamConfiguration;
    //   370: ifnull -> 632
    //   373: aload #19
    //   375: getfield streamConfigurationArray : [Landroid/hardware/camera2/params/StreamConfiguration;
    //   378: arraylength
    //   379: ifne -> 385
    //   382: goto -> 632
    //   385: iload #15
    //   387: ifeq -> 570
    //   390: iload #15
    //   392: iconst_1
    //   393: if_icmpeq -> 528
    //   396: iload #15
    //   398: iconst_2
    //   399: if_icmpeq -> 570
    //   402: iload #15
    //   404: iconst_4
    //   405: if_icmpeq -> 474
    //   408: iload #15
    //   410: iconst_5
    //   411: if_icmpeq -> 570
    //   414: iload #15
    //   416: bipush #6
    //   418: if_icmpeq -> 570
    //   421: new android/hardware/camera2/params/StreamConfigurationMap
    //   424: dup
    //   425: aload_3
    //   426: getfield streamConfigurationArray : [Landroid/hardware/camera2/params/StreamConfiguration;
    //   429: aload_3
    //   430: getfield minDurationArray : [Landroid/hardware/camera2/params/StreamConfigurationDuration;
    //   433: aload_3
    //   434: getfield stallDurationArray : [Landroid/hardware/camera2/params/StreamConfigurationDuration;
    //   437: aload #19
    //   439: getfield streamConfigurationArray : [Landroid/hardware/camera2/params/StreamConfiguration;
    //   442: aload #19
    //   444: getfield minDurationArray : [Landroid/hardware/camera2/params/StreamConfigurationDuration;
    //   447: aload #19
    //   449: getfield stallDurationArray : [Landroid/hardware/camera2/params/StreamConfigurationDuration;
    //   452: aconst_null
    //   453: aconst_null
    //   454: aconst_null
    //   455: aconst_null
    //   456: aconst_null
    //   457: aconst_null
    //   458: aconst_null
    //   459: aconst_null
    //   460: iload #14
    //   462: aload #8
    //   464: iload #15
    //   466: baload
    //   467: invokespecial <init> : ([Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/HighSpeedVideoConfiguration;Landroid/hardware/camera2/params/ReprocessFormatsMap;ZZ)V
    //   470: astore_3
    //   471: goto -> 608
    //   474: new android/hardware/camera2/params/StreamConfigurationMap
    //   477: dup
    //   478: aload_3
    //   479: getfield streamConfigurationArray : [Landroid/hardware/camera2/params/StreamConfiguration;
    //   482: aload_3
    //   483: getfield minDurationArray : [Landroid/hardware/camera2/params/StreamConfigurationDuration;
    //   486: aload_3
    //   487: getfield stallDurationArray : [Landroid/hardware/camera2/params/StreamConfigurationDuration;
    //   490: aload #19
    //   492: getfield streamConfigurationArray : [Landroid/hardware/camera2/params/StreamConfiguration;
    //   495: aload #19
    //   497: getfield minDurationArray : [Landroid/hardware/camera2/params/StreamConfigurationDuration;
    //   500: aload #19
    //   502: getfield stallDurationArray : [Landroid/hardware/camera2/params/StreamConfigurationDuration;
    //   505: aconst_null
    //   506: aconst_null
    //   507: aconst_null
    //   508: aconst_null
    //   509: aconst_null
    //   510: aconst_null
    //   511: aconst_null
    //   512: aload #12
    //   514: iload #14
    //   516: aload #8
    //   518: iload #15
    //   520: baload
    //   521: invokespecial <init> : ([Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/HighSpeedVideoConfiguration;Landroid/hardware/camera2/params/ReprocessFormatsMap;ZZ)V
    //   524: astore_3
    //   525: goto -> 608
    //   528: new android/hardware/camera2/params/StreamConfigurationMap
    //   531: dup
    //   532: aload_3
    //   533: getfield streamConfigurationArray : [Landroid/hardware/camera2/params/StreamConfiguration;
    //   536: aload_3
    //   537: getfield minDurationArray : [Landroid/hardware/camera2/params/StreamConfigurationDuration;
    //   540: aload_3
    //   541: getfield stallDurationArray : [Landroid/hardware/camera2/params/StreamConfigurationDuration;
    //   544: aconst_null
    //   545: aconst_null
    //   546: aconst_null
    //   547: aconst_null
    //   548: aconst_null
    //   549: aconst_null
    //   550: aconst_null
    //   551: aconst_null
    //   552: aconst_null
    //   553: aload #13
    //   555: aconst_null
    //   556: iload #14
    //   558: aload #8
    //   560: iload #15
    //   562: baload
    //   563: invokespecial <init> : ([Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/HighSpeedVideoConfiguration;Landroid/hardware/camera2/params/ReprocessFormatsMap;ZZ)V
    //   566: astore_3
    //   567: goto -> 608
    //   570: new android/hardware/camera2/params/StreamConfigurationMap
    //   573: dup
    //   574: aload_3
    //   575: getfield streamConfigurationArray : [Landroid/hardware/camera2/params/StreamConfiguration;
    //   578: aload_3
    //   579: getfield minDurationArray : [Landroid/hardware/camera2/params/StreamConfigurationDuration;
    //   582: aload_3
    //   583: getfield stallDurationArray : [Landroid/hardware/camera2/params/StreamConfigurationDuration;
    //   586: aconst_null
    //   587: aconst_null
    //   588: aconst_null
    //   589: aconst_null
    //   590: aconst_null
    //   591: aconst_null
    //   592: aconst_null
    //   593: aconst_null
    //   594: aconst_null
    //   595: aconst_null
    //   596: aconst_null
    //   597: iload #14
    //   599: aload #8
    //   601: iload #15
    //   603: baload
    //   604: invokespecial <init> : ([Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfiguration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/StreamConfigurationDuration;[Landroid/hardware/camera2/params/HighSpeedVideoConfiguration;Landroid/hardware/camera2/params/ReprocessFormatsMap;ZZ)V
    //   607: astore_3
    //   608: aload #4
    //   610: new android/hardware/camera2/params/RecommendedStreamConfigurationMap
    //   613: dup
    //   614: aload_3
    //   615: iload #15
    //   617: aload #8
    //   619: iload #15
    //   621: baload
    //   622: invokespecial <init> : (Landroid/hardware/camera2/params/StreamConfigurationMap;IZ)V
    //   625: invokevirtual add : (Ljava/lang/Object;)Z
    //   628: pop
    //   629: goto -> 639
    //   632: aload #4
    //   634: aconst_null
    //   635: invokevirtual add : (Ljava/lang/Object;)Z
    //   638: pop
    //   639: iinc #15, 1
    //   642: goto -> 213
    //   645: aload #4
    //   647: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1102	-> 0
    //   #1104	-> 11
    //   #1106	-> 22
    //   #1107	-> 30
    //   #1110	-> 32
    //   #1111	-> 37
    //   #1114	-> 46
    //   #1116	-> 55
    //   #1118	-> 64
    //   #1120	-> 73
    //   #1123	-> 79
    //   #1124	-> 83
    //   #1127	-> 101
    //   #1128	-> 102
    //   #1129	-> 111
    //   #1123	-> 113
    //   #1130	-> 113
    //   #1132	-> 113
    //   #1134	-> 122
    //   #1136	-> 131
    //   #1138	-> 140
    //   #1140	-> 144
    //   #1146	-> 158
    //   #1143	-> 161
    //   #1144	-> 162
    //   #1145	-> 171
    //   #1138	-> 173
    //   #1149	-> 173
    //   #1151	-> 185
    //   #1153	-> 197
    //   #1154	-> 203
    //   #1156	-> 210
    //   #1157	-> 220
    //   #1158	-> 230
    //   #1159	-> 234
    //   #1160	-> 246
    //   #1159	-> 270
    //   #1158	-> 284
    //   #1163	-> 284
    //   #1164	-> 295
    //   #1165	-> 299
    //   #1166	-> 311
    //   #1165	-> 335
    //   #1164	-> 350
    //   #1169	-> 350
    //   #1179	-> 385
    //   #1180	-> 385
    //   #1227	-> 421
    //   #1213	-> 474
    //   #1225	-> 525
    //   #1199	-> 528
    //   #1211	-> 567
    //   #1185	-> 570
    //   #1197	-> 608
    //   #1241	-> 608
    //   #1173	-> 632
    //   #1174	-> 639
    //   #1156	-> 639
    //   #1245	-> 645
    // Exception table:
    //   from	to	target	type
    //   83	98	101	java/lang/IllegalArgumentException
    //   144	158	161	java/lang/IllegalArgumentException
  }
  
  private boolean isBurstSupported() {
    boolean bool2, bool1 = false;
    int[] arrayOfInt = getBase((CameraCharacteristics.Key)CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES);
    int i = arrayOfInt.length;
    byte b = 0;
    while (true) {
      bool2 = bool1;
      if (b < i) {
        int j = arrayOfInt[b];
        if (j == 6) {
          bool2 = true;
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    return bool2;
  }
  
  private MandatoryStreamCombination[] getMandatoryStreamCombinationsHelper(boolean paramBoolean) {
    List<MandatoryStreamCombination> list;
    int[] arrayOfInt = getBase((CameraCharacteristics.Key)CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES);
    ArrayList<Integer> arrayList = new ArrayList();
    arrayList.ensureCapacity(arrayOfInt.length);
    int i, j;
    for (i = arrayOfInt.length, j = 0; j < i; ) {
      int k = arrayOfInt[j];
      arrayList.add(new Integer(k));
      j++;
    } 
    j = ((Integer)getBase(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL)).intValue();
    i = this.mCameraId;
    Size size = this.mDisplaySize;
    MandatoryStreamCombination.Builder builder = new MandatoryStreamCombination.Builder(i, j, size, arrayList, getStreamConfigurationMap());
    if (paramBoolean) {
      list = builder.getAvailableMandatoryConcurrentStreamCombinations();
    } else {
      list = list.getAvailableMandatoryStreamCombinations();
    } 
    if (list != null && !list.isEmpty()) {
      MandatoryStreamCombination[] arrayOfMandatoryStreamCombination = new MandatoryStreamCombination[list.size()];
      return list.<MandatoryStreamCombination>toArray(arrayOfMandatoryStreamCombination);
    } 
    return null;
  }
  
  private MandatoryStreamCombination[] getMandatoryConcurrentStreamCombinations() {
    if (!this.mHasMandatoryConcurrentStreams)
      return null; 
    return getMandatoryStreamCombinationsHelper(true);
  }
  
  private MandatoryStreamCombination[] getMandatoryStreamCombinations() {
    return getMandatoryStreamCombinationsHelper(false);
  }
  
  private StreamConfigurationMap getStreamConfigurationMap() {
    StreamConfiguration[] arrayOfStreamConfiguration1 = getBase((CameraCharacteristics.Key)CameraCharacteristics.SCALER_AVAILABLE_STREAM_CONFIGURATIONS);
    StreamConfigurationDuration[] arrayOfStreamConfigurationDuration1 = getBase((CameraCharacteristics.Key)CameraCharacteristics.SCALER_AVAILABLE_MIN_FRAME_DURATIONS);
    StreamConfigurationDuration[] arrayOfStreamConfigurationDuration2 = getBase((CameraCharacteristics.Key)CameraCharacteristics.SCALER_AVAILABLE_STALL_DURATIONS);
    StreamConfiguration[] arrayOfStreamConfiguration2 = getBase((CameraCharacteristics.Key)CameraCharacteristics.DEPTH_AVAILABLE_DEPTH_STREAM_CONFIGURATIONS);
    StreamConfigurationDuration[] arrayOfStreamConfigurationDuration3 = getBase((CameraCharacteristics.Key)CameraCharacteristics.DEPTH_AVAILABLE_DEPTH_MIN_FRAME_DURATIONS);
    StreamConfigurationDuration[] arrayOfStreamConfigurationDuration4 = getBase((CameraCharacteristics.Key)CameraCharacteristics.DEPTH_AVAILABLE_DEPTH_STALL_DURATIONS);
    StreamConfiguration[] arrayOfStreamConfiguration3 = getBase((CameraCharacteristics.Key)CameraCharacteristics.DEPTH_AVAILABLE_DYNAMIC_DEPTH_STREAM_CONFIGURATIONS);
    StreamConfigurationDuration[] arrayOfStreamConfigurationDuration5 = getBase((CameraCharacteristics.Key)CameraCharacteristics.DEPTH_AVAILABLE_DYNAMIC_DEPTH_MIN_FRAME_DURATIONS);
    StreamConfigurationDuration[] arrayOfStreamConfigurationDuration6 = getBase((CameraCharacteristics.Key)CameraCharacteristics.DEPTH_AVAILABLE_DYNAMIC_DEPTH_STALL_DURATIONS);
    StreamConfiguration[] arrayOfStreamConfiguration4 = getBase((CameraCharacteristics.Key)CameraCharacteristics.HEIC_AVAILABLE_HEIC_STREAM_CONFIGURATIONS);
    StreamConfigurationDuration[] arrayOfStreamConfigurationDuration7 = getBase((CameraCharacteristics.Key)CameraCharacteristics.HEIC_AVAILABLE_HEIC_MIN_FRAME_DURATIONS);
    StreamConfigurationDuration[] arrayOfStreamConfigurationDuration8 = getBase((CameraCharacteristics.Key)CameraCharacteristics.HEIC_AVAILABLE_HEIC_STALL_DURATIONS);
    HighSpeedVideoConfiguration[] arrayOfHighSpeedVideoConfiguration = getBase((CameraCharacteristics.Key)CameraCharacteristics.CONTROL_AVAILABLE_HIGH_SPEED_VIDEO_CONFIGURATIONS);
    ReprocessFormatsMap reprocessFormatsMap = getBase(CameraCharacteristics.SCALER_AVAILABLE_INPUT_OUTPUT_FORMATS_MAP);
    boolean bool = isBurstSupported();
    return new StreamConfigurationMap(arrayOfStreamConfiguration1, arrayOfStreamConfigurationDuration1, arrayOfStreamConfigurationDuration2, arrayOfStreamConfiguration2, arrayOfStreamConfigurationDuration3, arrayOfStreamConfigurationDuration4, arrayOfStreamConfiguration3, arrayOfStreamConfigurationDuration5, arrayOfStreamConfigurationDuration6, arrayOfStreamConfiguration4, arrayOfStreamConfigurationDuration7, arrayOfStreamConfigurationDuration8, arrayOfHighSpeedVideoConfiguration, reprocessFormatsMap, bool);
  }
  
  private <T> Integer getMaxRegions(Key<T> paramKey) {
    int[] arrayOfInt = getBase((CameraCharacteristics.Key)CameraCharacteristics.CONTROL_MAX_REGIONS);
    if (arrayOfInt == null)
      return null; 
    if (paramKey.equals(CameraCharacteristics.CONTROL_MAX_REGIONS_AE))
      return Integer.valueOf(arrayOfInt[0]); 
    if (paramKey.equals(CameraCharacteristics.CONTROL_MAX_REGIONS_AWB))
      return Integer.valueOf(arrayOfInt[1]); 
    if (paramKey.equals(CameraCharacteristics.CONTROL_MAX_REGIONS_AF))
      return Integer.valueOf(arrayOfInt[2]); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid key ");
    stringBuilder.append(paramKey);
    throw new AssertionError(stringBuilder.toString());
  }
  
  private <T> Integer getMaxNumOutputs(Key<T> paramKey) {
    int[] arrayOfInt = getBase((CameraCharacteristics.Key)CameraCharacteristics.REQUEST_MAX_NUM_OUTPUT_STREAMS);
    if (arrayOfInt == null)
      return null; 
    if (paramKey.equals(CameraCharacteristics.REQUEST_MAX_NUM_OUTPUT_RAW))
      return Integer.valueOf(arrayOfInt[0]); 
    if (paramKey.equals(CameraCharacteristics.REQUEST_MAX_NUM_OUTPUT_PROC))
      return Integer.valueOf(arrayOfInt[1]); 
    if (paramKey.equals(CameraCharacteristics.REQUEST_MAX_NUM_OUTPUT_PROC_STALLING))
      return Integer.valueOf(arrayOfInt[2]); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid key ");
    stringBuilder.append(paramKey);
    throw new AssertionError(stringBuilder.toString());
  }
  
  private <T> TonemapCurve getTonemapCurve() {
    float[] arrayOfFloat1 = getBase((CaptureRequest.Key)CaptureRequest.TONEMAP_CURVE_RED);
    float[] arrayOfFloat2 = getBase((CaptureRequest.Key)CaptureRequest.TONEMAP_CURVE_GREEN);
    float[] arrayOfFloat3 = getBase((CaptureRequest.Key)CaptureRequest.TONEMAP_CURVE_BLUE);
    if (areValuesAllNull(new Object[] { arrayOfFloat1, arrayOfFloat2, arrayOfFloat3 }))
      return null; 
    if (arrayOfFloat1 == null || arrayOfFloat2 == null || arrayOfFloat3 == null) {
      Log.w("CameraMetadataJV", "getTonemapCurve - missing tone curve components");
      return null;
    } 
    return new TonemapCurve(arrayOfFloat1, arrayOfFloat2, arrayOfFloat3);
  }
  
  private OisSample[] getOisSamples() {
    long[] arrayOfLong = getBase((CaptureResult.Key)CaptureResult.STATISTICS_OIS_TIMESTAMPS);
    float[] arrayOfFloat1 = getBase((CaptureResult.Key)CaptureResult.STATISTICS_OIS_X_SHIFTS);
    float[] arrayOfFloat2 = getBase((CaptureResult.Key)CaptureResult.STATISTICS_OIS_Y_SHIFTS);
    if (arrayOfLong == null) {
      if (arrayOfFloat1 == null) {
        if (arrayOfFloat2 == null)
          return null; 
        throw new AssertionError("timestamps is null but yShifts is not");
      } 
      throw new AssertionError("timestamps is null but xShifts is not");
    } 
    if (arrayOfFloat1 != null) {
      if (arrayOfFloat2 != null) {
        if (arrayOfFloat1.length == arrayOfLong.length) {
          if (arrayOfFloat2.length == arrayOfLong.length) {
            OisSample[] arrayOfOisSample = new OisSample[arrayOfLong.length];
            for (byte b = 0; b < arrayOfLong.length; b++)
              arrayOfOisSample[b] = new OisSample(arrayOfLong[b], arrayOfFloat1[b], arrayOfFloat2[b]); 
            return arrayOfOisSample;
          } 
          int k = arrayOfLong.length;
          int m = arrayOfFloat2.length;
          throw new AssertionError(String.format("timestamps has %d entries but yShifts has %d", new Object[] { Integer.valueOf(k), Integer.valueOf(m) }));
        } 
        int j = arrayOfLong.length;
        int i = arrayOfFloat1.length;
        throw new AssertionError(String.format("timestamps has %d entries but xShifts has %d", new Object[] { Integer.valueOf(j), Integer.valueOf(i) }));
      } 
      throw new AssertionError("timestamps is not null but yShifts is");
    } 
    throw new AssertionError("timestamps is not null but xShifts is");
  }
  
  private Capability[] getExtendedSceneModeCapabilities() {
    CameraCharacteristics.Key<int[]> key = CameraCharacteristics.CONTROL_AVAILABLE_EXTENDED_SCENE_MODE_MAX_SIZES;
    int[] arrayOfInt = getBase((CameraCharacteristics.Key)key);
    float[] arrayOfFloat = getBase((CameraCharacteristics.Key)CameraCharacteristics.CONTROL_AVAILABLE_EXTENDED_SCENE_MODE_ZOOM_RATIO_RANGES);
    Range range = getBase((CameraCharacteristics.Key)CameraCharacteristics.CONTROL_ZOOM_RATIO_RANGE);
    float f = ((Float)getBase(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM)).floatValue();
    if (arrayOfInt == null)
      return null; 
    if (arrayOfInt.length % 3 == 0) {
      int i = arrayOfInt.length / 3;
      int j = 0;
      if (arrayOfFloat != null)
        if (arrayOfFloat.length % 2 == 0) {
          j = arrayOfFloat.length / 2;
          if (i - j != 1)
            throw new AssertionError("Number of extended scene mode zoom ranges must be 1 less than number of supported modes"); 
        } else {
          throw new AssertionError("availableExtendedSceneModeZoomRanges must be tuples of [minZoom, maxZoom]");
        }  
      float f1 = 1.0F;
      if (range != null) {
        f1 = ((Float)range.getLower()).floatValue();
        f = ((Float)range.getUpper()).floatValue();
      } 
      Capability[] arrayOfCapability = new Capability[i];
      for (byte b1 = 0, b2 = 0; b1 < i; b1++) {
        int k = arrayOfInt[b1 * 3];
        int m = arrayOfInt[b1 * 3 + 1];
        int n = arrayOfInt[b1 * 3 + 2];
        if (k != 0 && b2 < j) {
          arrayOfCapability[b1] = new Capability(k, m, n, arrayOfFloat[b2 * 2], arrayOfFloat[b2 * 2 + 1]);
          b2++;
        } else {
          arrayOfCapability[b1] = new Capability(k, m, n, f1, f);
        } 
      } 
      return arrayOfCapability;
    } 
    throw new AssertionError("availableExtendedSceneModeMaxSizes must be tuples of [mode, width, height]");
  }
  
  private <T> void setBase(CameraCharacteristics.Key<T> paramKey, T paramT) {
    setBase(paramKey.getNativeKey(), paramT);
  }
  
  private <T> void setBase(CaptureResult.Key<T> paramKey, T paramT) {
    setBase(paramKey.getNativeKey(), paramT);
  }
  
  private <T> void setBase(CaptureRequest.Key<T> paramKey, T paramT) {
    setBase(paramKey.getNativeKey(), paramT);
  }
  
  private <T> void setBase(Key<T> paramKey, T paramT) {
    int i;
    if (paramKey.hasTag()) {
      i = paramKey.getTag();
    } else {
      i = nativeGetTagFromKeyLocal(this.mMetadataPtr, paramKey.getName());
      paramKey.cacheTag(i);
    } 
    if (paramT == null) {
      writeValues(i, null);
      return;
    } 
    int j = nativeGetTypeFromTagLocal(this.mMetadataPtr, i);
    Marshaler<T> marshaler = getMarshalerForKey(paramKey, j);
    j = marshaler.calculateMarshalSize(paramT);
    byte[] arrayOfByte = new byte[j];
    ByteBuffer byteBuffer = ByteBuffer.wrap(arrayOfByte).order(ByteOrder.nativeOrder());
    marshaler.marshal(paramT, byteBuffer);
    writeValues(i, arrayOfByte);
  }
  
  private boolean setAvailableFormats(int[] paramArrayOfint) {
    if (paramArrayOfint == null)
      return false; 
    int[] arrayOfInt = new int[paramArrayOfint.length];
    for (byte b = 0; b < paramArrayOfint.length; b++) {
      arrayOfInt[b] = paramArrayOfint[b];
      if (paramArrayOfint[b] == 256)
        arrayOfInt[b] = 33; 
    } 
    setBase((CameraCharacteristics.Key)CameraCharacteristics.SCALER_AVAILABLE_FORMATS, arrayOfInt);
    return true;
  }
  
  private boolean setFaceRectangles(Rect[] paramArrayOfRect) {
    if (paramArrayOfRect == null)
      return false; 
    Rect[] arrayOfRect = new Rect[paramArrayOfRect.length];
    for (byte b = 0; b < arrayOfRect.length; b++)
      arrayOfRect[b] = new Rect((paramArrayOfRect[b]).left, (paramArrayOfRect[b]).top, (paramArrayOfRect[b]).right + (paramArrayOfRect[b]).left, (paramArrayOfRect[b]).bottom + (paramArrayOfRect[b]).top); 
    setBase((CaptureResult.Key)CaptureResult.STATISTICS_FACE_RECTANGLES, arrayOfRect);
    return true;
  }
  
  private <T> boolean setTonemapCurve(TonemapCurve paramTonemapCurve) {
    if (paramTonemapCurve == null)
      return false; 
    float[][] arrayOfFloat = new float[3][];
    for (byte b = 0; b <= 2; b++) {
      int i = paramTonemapCurve.getPointCount(b);
      arrayOfFloat[b] = new float[i * 2];
      paramTonemapCurve.copyColorCurve(b, arrayOfFloat[b], 0);
    } 
    setBase((CaptureRequest.Key)CaptureRequest.TONEMAP_CURVE_RED, arrayOfFloat[0]);
    setBase((CaptureRequest.Key)CaptureRequest.TONEMAP_CURVE_GREEN, arrayOfFloat[1]);
    setBase((CaptureRequest.Key)CaptureRequest.TONEMAP_CURVE_BLUE, arrayOfFloat[2]);
    return true;
  }
  
  public void setCameraId(int paramInt) {
    this.mCameraId = paramInt;
  }
  
  public void setHasMandatoryConcurrentStreams(boolean paramBoolean) {
    this.mHasMandatoryConcurrentStreams = paramBoolean;
  }
  
  public void setDisplaySize(Size paramSize) {
    this.mDisplaySize = paramSize;
  }
  
  public void swap(CameraMetadataNative paramCameraMetadataNative) {
    nativeSwap(this.mMetadataPtr, paramCameraMetadataNative.mMetadataPtr);
    this.mCameraId = paramCameraMetadataNative.mCameraId;
    this.mHasMandatoryConcurrentStreams = paramCameraMetadataNative.mHasMandatoryConcurrentStreams;
    this.mDisplaySize = paramCameraMetadataNative.mDisplaySize;
  }
  
  public int getEntryCount() {
    return nativeGetEntryCount(this.mMetadataPtr);
  }
  
  public boolean isEmpty() {
    return nativeIsEmpty(this.mMetadataPtr);
  }
  
  public long getMetadataPtr() {
    return this.mMetadataPtr;
  }
  
  public <K> ArrayList<K> getAllVendorKeys(Class<K> paramClass) {
    if (paramClass != null)
      return nativeGetAllVendorKeys(this.mMetadataPtr, paramClass); 
    throw null;
  }
  
  public static int getTag(String paramString) {
    return nativeGetTagFromKey(paramString, Long.MAX_VALUE);
  }
  
  public static int getTag(String paramString, long paramLong) {
    return nativeGetTagFromKey(paramString, paramLong);
  }
  
  public static int getNativeType(int paramInt, long paramLong) {
    return nativeGetTypeFromTag(paramInt, paramLong);
  }
  
  public void writeValues(int paramInt, byte[] paramArrayOfbyte) {
    nativeWriteValues(paramInt, paramArrayOfbyte, this.mMetadataPtr);
  }
  
  public byte[] readValues(int paramInt) {
    return nativeReadValues(paramInt, this.mMetadataPtr);
  }
  
  public void dumpToLog() {
    try {
      nativeDump(this.mMetadataPtr);
    } catch (IOException iOException) {
      Log.wtf("CameraMetadataJV", "Dump logging failed", iOException);
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      close();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private static <T> Marshaler<T> getMarshalerForKey(Key<T> paramKey, int paramInt) {
    return MarshalRegistry.getMarshaler(paramKey.getTypeReference(), paramInt);
  }
  
  private static void registerAllMarshalers() {
    MarshalQueryable[] arrayOfMarshalQueryable = new MarshalQueryable[21];
    MarshalQueryablePrimitive marshalQueryablePrimitive = new MarshalQueryablePrimitive();
    byte b = 0;
    arrayOfMarshalQueryable[0] = marshalQueryablePrimitive;
    arrayOfMarshalQueryable[1] = new MarshalQueryableEnum<>();
    arrayOfMarshalQueryable[2] = new MarshalQueryableArray();
    arrayOfMarshalQueryable[3] = new MarshalQueryableBoolean();
    arrayOfMarshalQueryable[4] = new MarshalQueryableNativeByteToInteger();
    arrayOfMarshalQueryable[5] = new MarshalQueryableRect();
    arrayOfMarshalQueryable[6] = new MarshalQueryableSize();
    arrayOfMarshalQueryable[7] = new MarshalQueryableSizeF();
    arrayOfMarshalQueryable[8] = new MarshalQueryableString();
    arrayOfMarshalQueryable[9] = new MarshalQueryableReprocessFormatsMap();
    arrayOfMarshalQueryable[10] = new MarshalQueryableRange<>();
    arrayOfMarshalQueryable[11] = new MarshalQueryablePair<>();
    arrayOfMarshalQueryable[12] = new MarshalQueryableMeteringRectangle();
    arrayOfMarshalQueryable[13] = new MarshalQueryableColorSpaceTransform();
    arrayOfMarshalQueryable[14] = new MarshalQueryableStreamConfiguration();
    arrayOfMarshalQueryable[15] = new MarshalQueryableStreamConfigurationDuration();
    arrayOfMarshalQueryable[16] = new MarshalQueryableRggbChannelVector();
    arrayOfMarshalQueryable[17] = new MarshalQueryableBlackLevelPattern();
    arrayOfMarshalQueryable[18] = new MarshalQueryableHighSpeedVideoConfiguration();
    arrayOfMarshalQueryable[19] = new MarshalQueryableRecommendedStreamConfiguration();
    arrayOfMarshalQueryable[20] = new MarshalQueryableParcelable<>();
    for (int i = arrayOfMarshalQueryable.length; b < i; ) {
      MarshalQueryable<?> marshalQueryable = arrayOfMarshalQueryable[b];
      MarshalRegistry.registerMarshalQueryable(marshalQueryable);
      b++;
    } 
  }
  
  private static boolean areValuesAllNull(Object... paramVarArgs) {
    int i;
    byte b;
    for (i = paramVarArgs.length, b = 0; b < i; ) {
      Object object = paramVarArgs[b];
      if (object != null)
        return false; 
      b++;
    } 
    return true;
  }
  
  @FastNative
  private static native long nativeAllocate();
  
  @FastNative
  private static native long nativeAllocateCopy(long paramLong) throws NullPointerException;
  
  @FastNative
  private static synchronized native void nativeClose(long paramLong);
  
  private static synchronized native void nativeDump(long paramLong) throws IOException;
  
  @FastNative
  private static synchronized native ArrayList nativeGetAllVendorKeys(long paramLong, Class paramClass);
  
  @FastNative
  private static synchronized native int nativeGetEntryCount(long paramLong);
  
  @FastNative
  private static native int nativeGetTagFromKey(String paramString, long paramLong) throws IllegalArgumentException;
  
  @FastNative
  private static synchronized native int nativeGetTagFromKeyLocal(long paramLong, String paramString) throws IllegalArgumentException;
  
  @FastNative
  private static native int nativeGetTypeFromTag(int paramInt, long paramLong) throws IllegalArgumentException;
  
  @FastNative
  private static synchronized native int nativeGetTypeFromTagLocal(long paramLong, int paramInt) throws IllegalArgumentException;
  
  @FastNative
  private static synchronized native boolean nativeIsEmpty(long paramLong);
  
  @FastNative
  private static synchronized native void nativeReadFromParcel(Parcel paramParcel, long paramLong);
  
  @FastNative
  private static synchronized native byte[] nativeReadValues(int paramInt, long paramLong);
  
  private static native int nativeSetupGlobalVendorTagDescriptor();
  
  @FastNative
  private static synchronized native void nativeSwap(long paramLong1, long paramLong2) throws NullPointerException;
  
  @FastNative
  private static synchronized native void nativeWriteToParcel(Parcel paramParcel, long paramLong);
  
  @FastNative
  private static synchronized native void nativeWriteValues(int paramInt, byte[] paramArrayOfbyte, long paramLong);
}
