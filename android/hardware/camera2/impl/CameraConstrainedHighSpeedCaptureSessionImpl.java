package android.hardware.camera2.impl;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraConstrainedHighSpeedCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraOfflineSession;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.HighSpeedVideoConfiguration;
import android.hardware.camera2.params.OutputConfiguration;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.hardware.camera2.utils.SurfaceUtils;
import android.os.ConditionVariable;
import android.os.Handler;
import android.util.Range;
import android.util.Size;
import android.view.Surface;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;

public class CameraConstrainedHighSpeedCaptureSessionImpl extends CameraConstrainedHighSpeedCaptureSession implements CameraCaptureSessionCore {
  private final CameraCharacteristics mCharacteristics;
  
  private final ConditionVariable mInitialized = new ConditionVariable();
  
  private final CameraCaptureSessionImpl mSessionImpl;
  
  CameraConstrainedHighSpeedCaptureSessionImpl(int paramInt, CameraCaptureSession.StateCallback paramStateCallback, Executor paramExecutor1, CameraDeviceImpl paramCameraDeviceImpl, Executor paramExecutor2, boolean paramBoolean, CameraCharacteristics paramCameraCharacteristics) {
    this.mCharacteristics = paramCameraCharacteristics;
    paramStateCallback = new WrapperCallback(paramStateCallback);
    this.mSessionImpl = new CameraCaptureSessionImpl(paramInt, null, paramStateCallback, paramExecutor1, paramCameraDeviceImpl, paramExecutor2, paramBoolean);
    this.mInitialized.open();
  }
  
  public List<CaptureRequest> createHighSpeedRequestList(CaptureRequest paramCaptureRequest) throws CameraAccessException {
    if (paramCaptureRequest != null) {
      CaptureRequest.Builder builder1, builder2;
      Collection<Surface> collection = paramCaptureRequest.getTargets();
      Range<Integer> range = paramCaptureRequest.<Range>get((CaptureRequest.Key)CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE);
      CameraCharacteristics cameraCharacteristics = this.mCharacteristics;
      CameraCharacteristics.Key<StreamConfigurationMap> key = CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP;
      StreamConfigurationMap streamConfigurationMap = cameraCharacteristics.<StreamConfigurationMap>get(key);
      SurfaceUtils.checkConstrainedHighSpeedSurfaces(collection, range, streamConfigurationMap);
      int i = getHighSpeedRequestListSize(range, collection);
      ArrayList<CaptureRequest> arrayList = new ArrayList();
      CameraMetadataNative cameraMetadataNative = new CameraMetadataNative(paramCaptureRequest.getNativeCopy());
      CaptureRequest.Builder builder3 = new CaptureRequest.Builder(cameraMetadataNative, false, -1, paramCaptureRequest.getLogicalCameraId(), null);
      builder3.setTag(paramCaptureRequest.getTag());
      Iterator<Surface> iterator = collection.iterator();
      Surface surface = iterator.next();
      if (collection.size() == 1 && SurfaceUtils.isSurfaceForHwVideoEncoder(surface)) {
        CaptureRequest.Key<Integer> key1 = CaptureRequest.CONTROL_CAPTURE_INTENT;
        builder3.set(key1, Integer.valueOf(1));
      } else {
        CaptureRequest.Key<Integer> key1 = CaptureRequest.CONTROL_CAPTURE_INTENT;
        builder3.set(key1, Integer.valueOf(3));
      } 
      builder3.setPartOfCHSRequestList(true);
      streamConfigurationMap = null;
      if (collection.size() == 2) {
        CameraMetadataNative cameraMetadataNative1 = new CameraMetadataNative(paramCaptureRequest.getNativeCopy());
        builder2 = new CaptureRequest.Builder(cameraMetadataNative1, false, -1, paramCaptureRequest.getLogicalCameraId(), null);
        builder2.setTag(paramCaptureRequest.getTag());
        CaptureRequest.Key<Integer> key1 = CaptureRequest.CONTROL_CAPTURE_INTENT;
        builder2.set(key1, Integer.valueOf(3));
        builder2.addTarget(surface);
        Surface surface2 = iterator.next();
        builder2.addTarget(surface2);
        builder2.setPartOfCHSRequestList(true);
        Surface surface1 = surface;
        if (!SurfaceUtils.isSurfaceForHwVideoEncoder(surface))
          surface1 = surface2; 
        builder3.addTarget(surface1);
        builder1 = builder2;
      } else {
        builder3.addTarget(surface);
        builder1 = builder2;
      } 
      for (byte b = 0; b < i; b++) {
        if (b == 0 && builder1 != null) {
          arrayList.add(builder1.build());
        } else {
          arrayList.add(builder3.build());
        } 
      } 
      return Collections.unmodifiableList(arrayList);
    } 
    throw new IllegalArgumentException("Input capture request must not be null");
  }
  
  private boolean isConstrainedHighSpeedRequestList(List<CaptureRequest> paramList) {
    Preconditions.checkCollectionNotEmpty(paramList, "High speed request list");
    for (CaptureRequest captureRequest : paramList) {
      if (!captureRequest.isPartOfCRequestList())
        return false; 
    } 
    return true;
  }
  
  private int getHighSpeedRequestListSize(Range<Integer> paramRange, Collection<Surface> paramCollection) {
    int j, i = 0;
    byte b = 0;
    Iterator<Surface> iterator = paramCollection.iterator();
    while (true) {
      j = i;
      if (iterator.hasNext()) {
        Surface surface = iterator.next();
        if (SurfaceUtils.isSurfaceForHwVideoEncoder(surface)) {
          Size size = SurfaceUtils.getSurfaceSize(surface);
          CameraCharacteristics cameraCharacteristics = this.mCharacteristics;
          CameraCharacteristics.Key<HighSpeedVideoConfiguration[]> key = CameraCharacteristics.CONTROL_AVAILABLE_HIGH_SPEED_VIDEO_CONFIGURATIONS;
          HighSpeedVideoConfiguration[] arrayOfHighSpeedVideoConfiguration = cameraCharacteristics.<HighSpeedVideoConfiguration[]>get((CameraCharacteristics.Key)key);
          int k = arrayOfHighSpeedVideoConfiguration.length;
          i = 0;
          while (true) {
            j = b;
            if (i < k) {
              HighSpeedVideoConfiguration highSpeedVideoConfiguration = arrayOfHighSpeedVideoConfiguration[i];
              if (highSpeedVideoConfiguration.getSize().equals(size) && highSpeedVideoConfiguration.getFpsRange().equals(paramRange)) {
                j = highSpeedVideoConfiguration.getBatchSizeMax();
                break;
              } 
              i++;
              continue;
            } 
            break;
          } 
          break;
        } 
        continue;
      } 
      break;
    } 
    i = j;
    if (j == 0)
      i = ((Integer)paramRange.getUpper()).intValue() / 30; 
    return i;
  }
  
  public CameraDevice getDevice() {
    return this.mSessionImpl.getDevice();
  }
  
  public void prepare(Surface paramSurface) throws CameraAccessException {
    this.mSessionImpl.prepare(paramSurface);
  }
  
  public void prepare(int paramInt, Surface paramSurface) throws CameraAccessException {
    this.mSessionImpl.prepare(paramInt, paramSurface);
  }
  
  public void tearDown(Surface paramSurface) throws CameraAccessException {
    this.mSessionImpl.tearDown(paramSurface);
  }
  
  public int capture(CaptureRequest paramCaptureRequest, CameraCaptureSession.CaptureCallback paramCaptureCallback, Handler paramHandler) throws CameraAccessException {
    throw new UnsupportedOperationException("Constrained high speed session doesn't support this method");
  }
  
  public int captureSingleRequest(CaptureRequest paramCaptureRequest, Executor paramExecutor, CameraCaptureSession.CaptureCallback paramCaptureCallback) throws CameraAccessException {
    throw new UnsupportedOperationException("Constrained high speed session doesn't support this method");
  }
  
  public int captureBurst(List<CaptureRequest> paramList, CameraCaptureSession.CaptureCallback paramCaptureCallback, Handler paramHandler) throws CameraAccessException {
    if (isConstrainedHighSpeedRequestList(paramList))
      return this.mSessionImpl.captureBurst(paramList, paramCaptureCallback, paramHandler); 
    throw new IllegalArgumentException("Only request lists created by createHighSpeedRequestList() can be submitted to a constrained high speed capture session");
  }
  
  public int captureBurstRequests(List<CaptureRequest> paramList, Executor paramExecutor, CameraCaptureSession.CaptureCallback paramCaptureCallback) throws CameraAccessException {
    if (isConstrainedHighSpeedRequestList(paramList))
      return this.mSessionImpl.captureBurstRequests(paramList, paramExecutor, paramCaptureCallback); 
    throw new IllegalArgumentException("Only request lists created by createHighSpeedRequestList() can be submitted to a constrained high speed capture session");
  }
  
  public int setRepeatingRequest(CaptureRequest paramCaptureRequest, CameraCaptureSession.CaptureCallback paramCaptureCallback, Handler paramHandler) throws CameraAccessException {
    throw new UnsupportedOperationException("Constrained high speed session doesn't support this method");
  }
  
  public int setSingleRepeatingRequest(CaptureRequest paramCaptureRequest, Executor paramExecutor, CameraCaptureSession.CaptureCallback paramCaptureCallback) throws CameraAccessException {
    throw new UnsupportedOperationException("Constrained high speed session doesn't support this method");
  }
  
  public int setRepeatingBurst(List<CaptureRequest> paramList, CameraCaptureSession.CaptureCallback paramCaptureCallback, Handler paramHandler) throws CameraAccessException {
    if (isConstrainedHighSpeedRequestList(paramList))
      return this.mSessionImpl.setRepeatingBurst(paramList, paramCaptureCallback, paramHandler); 
    throw new IllegalArgumentException("Only request lists created by createHighSpeedRequestList() can be submitted to a constrained high speed capture session");
  }
  
  public int setRepeatingBurstRequests(List<CaptureRequest> paramList, Executor paramExecutor, CameraCaptureSession.CaptureCallback paramCaptureCallback) throws CameraAccessException {
    if (isConstrainedHighSpeedRequestList(paramList))
      return this.mSessionImpl.setRepeatingBurstRequests(paramList, paramExecutor, paramCaptureCallback); 
    throw new IllegalArgumentException("Only request lists created by createHighSpeedRequestList() can be submitted to a constrained high speed capture session");
  }
  
  public void stopRepeating() throws CameraAccessException {
    this.mSessionImpl.stopRepeating();
  }
  
  public void abortCaptures() throws CameraAccessException {
    this.mSessionImpl.abortCaptures();
  }
  
  public Surface getInputSurface() {
    return null;
  }
  
  public void updateOutputConfiguration(OutputConfiguration paramOutputConfiguration) throws CameraAccessException {
    throw new UnsupportedOperationException("Constrained high speed session doesn't support this method");
  }
  
  public CameraOfflineSession switchToOffline(Collection<Surface> paramCollection, Executor paramExecutor, CameraOfflineSession.CameraOfflineSessionCallback paramCameraOfflineSessionCallback) throws CameraAccessException {
    throw new UnsupportedOperationException("Constrained high speed session doesn't support this method");
  }
  
  public boolean supportsOfflineProcessing(Surface paramSurface) {
    throw new UnsupportedOperationException("Constrained high speed session doesn't support offline mode");
  }
  
  public void closeWithoutDraining() {
    throw new UnsupportedOperationException("Constrained high speed session doesn't support this method");
  }
  
  public void close() {
    this.mSessionImpl.close();
  }
  
  public boolean isReprocessable() {
    return false;
  }
  
  public void replaceSessionClose() {
    this.mSessionImpl.replaceSessionClose();
  }
  
  public CameraDeviceImpl.StateCallbackKK getDeviceStateCallback() {
    return this.mSessionImpl.getDeviceStateCallback();
  }
  
  public boolean isAborting() {
    return this.mSessionImpl.isAborting();
  }
  
  public void finalizeOutputConfigurations(List<OutputConfiguration> paramList) throws CameraAccessException {
    this.mSessionImpl.finalizeOutputConfigurations(paramList);
  }
  
  class WrapperCallback extends CameraCaptureSession.StateCallback {
    private final CameraCaptureSession.StateCallback mCallback;
    
    final CameraConstrainedHighSpeedCaptureSessionImpl this$0;
    
    public WrapperCallback(CameraCaptureSession.StateCallback param1StateCallback) {
      this.mCallback = param1StateCallback;
    }
    
    public void onConfigured(CameraCaptureSession param1CameraCaptureSession) {
      CameraConstrainedHighSpeedCaptureSessionImpl.this.mInitialized.block();
      this.mCallback.onConfigured(CameraConstrainedHighSpeedCaptureSessionImpl.this);
    }
    
    public void onConfigureFailed(CameraCaptureSession param1CameraCaptureSession) {
      CameraConstrainedHighSpeedCaptureSessionImpl.this.mInitialized.block();
      this.mCallback.onConfigureFailed(CameraConstrainedHighSpeedCaptureSessionImpl.this);
    }
    
    public void onReady(CameraCaptureSession param1CameraCaptureSession) {
      this.mCallback.onReady(CameraConstrainedHighSpeedCaptureSessionImpl.this);
    }
    
    public void onActive(CameraCaptureSession param1CameraCaptureSession) {
      this.mCallback.onActive(CameraConstrainedHighSpeedCaptureSessionImpl.this);
    }
    
    public void onCaptureQueueEmpty(CameraCaptureSession param1CameraCaptureSession) {
      this.mCallback.onCaptureQueueEmpty(CameraConstrainedHighSpeedCaptureSessionImpl.this);
    }
    
    public void onClosed(CameraCaptureSession param1CameraCaptureSession) {
      this.mCallback.onClosed(CameraConstrainedHighSpeedCaptureSessionImpl.this);
    }
    
    public void onSurfacePrepared(CameraCaptureSession param1CameraCaptureSession, Surface param1Surface) {
      this.mCallback.onSurfacePrepared(CameraConstrainedHighSpeedCaptureSessionImpl.this, param1Surface);
    }
  }
}
