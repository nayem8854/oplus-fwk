package android.hardware.camera2;

import android.app.ActivityThread;
import android.content.Context;
import android.hardware.CameraInfo;
import android.hardware.CameraStatus;
import android.hardware.ICameraService;
import android.hardware.ICameraServiceListener;
import android.hardware.camera2.impl.CameraDeviceImpl;
import android.hardware.camera2.impl.CameraMetadataNative;
import android.hardware.camera2.legacy.LegacyMetadataMapper;
import android.hardware.camera2.params.SessionConfiguration;
import android.hardware.display.DisplayManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceSpecificException;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.Log;
import android.util.Size;
import android.view.Display;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class CameraManager {
  private static final int API_VERSION_1 = 1;
  
  private static final int API_VERSION_2 = 2;
  
  private static final int CAMERA_TYPE_ALL = 1;
  
  private static final int CAMERA_TYPE_BACKWARD_COMPATIBLE = 0;
  
  private static final String TAG = "CameraManager";
  
  private static final int USE_CALLING_UID = -1;
  
  private final boolean DEBUG;
  
  private final Context mContext;
  
  private ArrayList<String> mDeviceIdList;
  
  private final Object mLock;
  
  public CameraManager(Context paramContext) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial <init> : ()V
    //   4: aload_0
    //   5: iconst_0
    //   6: putfield DEBUG : Z
    //   9: new java/lang/Object
    //   12: dup
    //   13: invokespecial <init> : ()V
    //   16: astore_2
    //   17: aload_0
    //   18: aload_2
    //   19: putfield mLock : Ljava/lang/Object;
    //   22: aload_2
    //   23: monitorenter
    //   24: aload_0
    //   25: aload_1
    //   26: putfield mContext : Landroid/content/Context;
    //   29: invokestatic getInstance : ()Landroid/hardware/camera2/OplusCameraManager;
    //   32: astore_1
    //   33: aload_0
    //   34: getfield mContext : Landroid/content/Context;
    //   37: astore_3
    //   38: aload_3
    //   39: invokevirtual getOpPackageName : ()Ljava/lang/String;
    //   42: astore_3
    //   43: aload_1
    //   44: aload_3
    //   45: invokevirtual saveOpPackageName : (Ljava/lang/String;)V
    //   48: aload_2
    //   49: monitorexit
    //   50: return
    //   51: astore_1
    //   52: aload_2
    //   53: monitorexit
    //   54: aload_1
    //   55: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #97	-> 0
    //   #78	-> 4
    //   #92	-> 9
    //   #98	-> 22
    //   #99	-> 24
    //   #103	-> 29
    //   #104	-> 38
    //   #103	-> 43
    //   #106	-> 48
    //   #107	-> 50
    //   #106	-> 51
    // Exception table:
    //   from	to	target	type
    //   24	29	51	finally
    //   29	38	51	finally
    //   38	43	51	finally
    //   43	48	51	finally
    //   48	50	51	finally
    //   52	54	51	finally
  }
  
  public String[] getCameraIdList() throws CameraAccessException {
    return CameraManagerGlobal.get().getCameraIdList();
  }
  
  public String[] getCameraIdListNoLazy() throws CameraAccessException {
    return CameraManagerGlobal.get().getCameraIdListNoLazy();
  }
  
  public Set<Set<String>> getConcurrentCameraIds() throws CameraAccessException {
    return CameraManagerGlobal.get().getConcurrentCameraIds();
  }
  
  public boolean isConcurrentSessionConfigurationSupported(Map<String, SessionConfiguration> paramMap) throws CameraAccessException {
    return CameraManagerGlobal.get().isConcurrentSessionConfigurationSupported(paramMap);
  }
  
  public void registerAvailabilityCallback(AvailabilityCallback paramAvailabilityCallback, Handler paramHandler) {
    CameraManagerGlobal cameraManagerGlobal = CameraManagerGlobal.get();
    Executor executor = CameraDeviceImpl.checkAndWrapHandler(paramHandler);
    cameraManagerGlobal.registerAvailabilityCallback(paramAvailabilityCallback, executor);
  }
  
  public void registerAvailabilityCallback(Executor paramExecutor, AvailabilityCallback paramAvailabilityCallback) {
    if (paramExecutor != null) {
      CameraManagerGlobal.get().registerAvailabilityCallback(paramAvailabilityCallback, paramExecutor);
      return;
    } 
    throw new IllegalArgumentException("executor was null");
  }
  
  public void unregisterAvailabilityCallback(AvailabilityCallback paramAvailabilityCallback) {
    CameraManagerGlobal.get().unregisterAvailabilityCallback(paramAvailabilityCallback);
  }
  
  public void registerTorchCallback(TorchCallback paramTorchCallback, Handler paramHandler) {
    CameraManagerGlobal cameraManagerGlobal = CameraManagerGlobal.get();
    Executor executor = CameraDeviceImpl.checkAndWrapHandler(paramHandler);
    cameraManagerGlobal.registerTorchCallback(paramTorchCallback, executor);
  }
  
  public void registerTorchCallback(Executor paramExecutor, TorchCallback paramTorchCallback) {
    if (paramExecutor != null) {
      CameraManagerGlobal.get().registerTorchCallback(paramTorchCallback, paramExecutor);
      return;
    } 
    throw new IllegalArgumentException("executor was null");
  }
  
  public void unregisterTorchCallback(TorchCallback paramTorchCallback) {
    CameraManagerGlobal.get().unregisterTorchCallback(paramTorchCallback);
  }
  
  private Size getDisplaySize() {
    Size size = new Size(0, 0);
    try {
      Context context = this.mContext;
      DisplayManager displayManager = (DisplayManager)context.getSystemService("display");
      Display display = displayManager.getDisplay(0);
      if (display != null) {
        int i = display.getWidth();
        int j = display.getHeight();
        int k = i, m = j;
        if (j > i) {
          m = i;
          k = display.getHeight();
        } 
        Size size1 = new Size();
        this(k, m);
        size = size1;
      } else {
        Log.e("CameraManager", "Invalid default display!");
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getDisplaySize Failed. ");
      stringBuilder.append(exception.toString());
      Log.e("CameraManager", stringBuilder.toString());
    } 
    return size;
  }
  
  public CameraCharacteristics getCameraCharacteristics(String paramString) throws CameraAccessException {
    ServiceSpecificException serviceSpecificException = null;
    if (!CameraManagerGlobal.sCameraServiceDisabled)
      synchronized (this.mLock) {
        ICameraService iCameraService = CameraManagerGlobal.get().getCameraService();
        if (iCameraService != null) {
          CameraAccessException cameraAccessException1;
          try {
            CameraCharacteristics cameraCharacteristics;
            CameraInfo cameraInfo;
            Size size = getDisplaySize();
            if (!isHiddenPhysicalCamera(paramString) && !supportsCamera2ApiLocked(paramString)) {
              int i = Integer.parseInt(paramString);
              paramString = iCameraService.getLegacyParameters(i);
              cameraInfo = iCameraService.getCameraInfo(i);
              cameraCharacteristics = LegacyMetadataMapper.createCharacteristics(paramString, cameraInfo, i, size);
            } else {
              CameraMetadataNative cameraMetadataNative = cameraInfo.getCameraCharacteristics((String)cameraCharacteristics);
              try {
                cameraMetadataNative.setCameraId(Integer.parseInt((String)cameraCharacteristics));
              } catch (NumberFormatException numberFormatException) {
                StringBuilder stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("Failed to parse camera Id ");
                stringBuilder.append((String)cameraCharacteristics);
                stringBuilder.append(" to integer");
                Log.v("CameraManager", stringBuilder.toString());
              } 
              boolean bool = CameraManagerGlobal.get().cameraIdHasConcurrentStreamsLocked((String)cameraCharacteristics);
              cameraMetadataNative.setHasMandatoryConcurrentStreams(bool);
              cameraMetadataNative.setDisplaySize(size);
              cameraCharacteristics = new CameraCharacteristics(cameraMetadataNative);
            } 
          } catch (ServiceSpecificException serviceSpecificException1) {
            throwAsPublicException((Throwable)serviceSpecificException1);
            serviceSpecificException1 = serviceSpecificException;
          } catch (RemoteException remoteException) {
            cameraAccessException1 = new CameraAccessException();
            this(2, "Camera service is currently unavailable", (Throwable)remoteException);
            throw cameraAccessException1;
          } 
          return (CameraCharacteristics)cameraAccessException1;
        } 
        CameraAccessException cameraAccessException = new CameraAccessException();
        this(2, "Camera service is currently unavailable");
        throw cameraAccessException;
      }  
    throw new IllegalArgumentException("No cameras available on device");
  }
  
  private CameraDevice openCameraDeviceUserAsync(String paramString, CameraDevice.StateCallback paramStateCallback, Executor paramExecutor, int paramInt) throws CameraAccessException {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokevirtual getCameraCharacteristics : (Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;
    //   5: astore #5
    //   7: aload_0
    //   8: getfield mLock : Ljava/lang/Object;
    //   11: astore #6
    //   13: aload #6
    //   15: monitorenter
    //   16: aconst_null
    //   17: astore #7
    //   19: new android/hardware/camera2/impl/CameraDeviceImpl
    //   22: astore #8
    //   24: aload_0
    //   25: getfield mContext : Landroid/content/Context;
    //   28: astore #9
    //   30: aload #8
    //   32: aload_1
    //   33: aload_2
    //   34: aload_3
    //   35: aload #5
    //   37: aload #9
    //   39: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   42: getfield targetSdkVersion : I
    //   45: invokespecial <init> : (Ljava/lang/String;Landroid/hardware/camera2/CameraDevice$StateCallback;Ljava/util/concurrent/Executor;Landroid/hardware/camera2/CameraCharacteristics;I)V
    //   48: aload #8
    //   50: invokevirtual getCallbacks : ()Landroid/hardware/camera2/impl/CameraDeviceImpl$CameraDeviceCallbacks;
    //   53: astore_2
    //   54: aload_0
    //   55: aload_1
    //   56: invokespecial supportsCamera2ApiLocked : (Ljava/lang/String;)Z
    //   59: ifeq -> 126
    //   62: invokestatic get : ()Landroid/hardware/camera2/CameraManager$CameraManagerGlobal;
    //   65: invokevirtual getCameraService : ()Landroid/hardware/ICameraService;
    //   68: astore_3
    //   69: aload_3
    //   70: ifnull -> 113
    //   73: aload_0
    //   74: getfield mContext : Landroid/content/Context;
    //   77: astore #5
    //   79: aload #5
    //   81: invokevirtual getOpPackageName : ()Ljava/lang/String;
    //   84: astore #9
    //   86: aload_0
    //   87: getfield mContext : Landroid/content/Context;
    //   90: invokevirtual getAttributionTag : ()Ljava/lang/String;
    //   93: astore #5
    //   95: aload_3
    //   96: aload_2
    //   97: aload_1
    //   98: aload #9
    //   100: aload #5
    //   102: iload #4
    //   104: invokeinterface connectDevice : (Landroid/hardware/camera2/ICameraDeviceCallbacks;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/hardware/camera2/ICameraDeviceUser;
    //   109: astore_1
    //   110: goto -> 153
    //   113: new android/os/ServiceSpecificException
    //   116: astore_1
    //   117: aload_1
    //   118: iconst_4
    //   119: ldc 'Camera service is currently unavailable'
    //   121: invokespecial <init> : (ILjava/lang/String;)V
    //   124: aload_1
    //   125: athrow
    //   126: aload_1
    //   127: invokestatic parseInt : (Ljava/lang/String;)I
    //   130: istore #4
    //   132: ldc 'CameraManager'
    //   134: ldc 'Using legacy camera HAL.'
    //   136: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   139: pop
    //   140: aload_0
    //   141: invokespecial getDisplaySize : ()Landroid/util/Size;
    //   144: astore_1
    //   145: aload_2
    //   146: iload #4
    //   148: aload_1
    //   149: invokestatic connectBinderShim : (Landroid/hardware/camera2/ICameraDeviceCallbacks;ILandroid/util/Size;)Landroid/hardware/camera2/legacy/CameraDeviceUserShim;
    //   152: astore_1
    //   153: goto -> 331
    //   156: astore_2
    //   157: new java/lang/IllegalArgumentException
    //   160: astore_2
    //   161: new java/lang/StringBuilder
    //   164: astore_3
    //   165: aload_3
    //   166: invokespecial <init> : ()V
    //   169: aload_3
    //   170: ldc 'Expected cameraId to be numeric, but it was: '
    //   172: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   175: pop
    //   176: aload_3
    //   177: aload_1
    //   178: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   181: pop
    //   182: aload_2
    //   183: aload_3
    //   184: invokevirtual toString : ()Ljava/lang/String;
    //   187: invokespecial <init> : (Ljava/lang/String;)V
    //   190: aload_2
    //   191: athrow
    //   192: astore_1
    //   193: goto -> 201
    //   196: astore_1
    //   197: goto -> 229
    //   200: astore_1
    //   201: new android/os/ServiceSpecificException
    //   204: astore_1
    //   205: aload_1
    //   206: iconst_4
    //   207: ldc 'Camera service is currently unavailable'
    //   209: invokespecial <init> : (ILjava/lang/String;)V
    //   212: aload #8
    //   214: aload_1
    //   215: invokevirtual setRemoteFailure : (Landroid/os/ServiceSpecificException;)V
    //   218: aload_1
    //   219: invokestatic throwAsPublicException : (Ljava/lang/Throwable;)V
    //   222: aload #7
    //   224: astore_1
    //   225: goto -> 331
    //   228: astore_1
    //   229: aload_1
    //   230: getfield errorCode : I
    //   233: bipush #9
    //   235: if_icmpeq -> 343
    //   238: aload_1
    //   239: getfield errorCode : I
    //   242: bipush #7
    //   244: if_icmpeq -> 292
    //   247: aload_1
    //   248: getfield errorCode : I
    //   251: bipush #8
    //   253: if_icmpeq -> 292
    //   256: aload_1
    //   257: getfield errorCode : I
    //   260: bipush #6
    //   262: if_icmpeq -> 292
    //   265: aload_1
    //   266: getfield errorCode : I
    //   269: iconst_4
    //   270: if_icmpeq -> 292
    //   273: aload_1
    //   274: getfield errorCode : I
    //   277: bipush #10
    //   279: if_icmpne -> 285
    //   282: goto -> 292
    //   285: aload_1
    //   286: invokestatic throwAsPublicException : (Ljava/lang/Throwable;)V
    //   289: goto -> 328
    //   292: aload #8
    //   294: aload_1
    //   295: invokevirtual setRemoteFailure : (Landroid/os/ServiceSpecificException;)V
    //   298: aload_1
    //   299: getfield errorCode : I
    //   302: bipush #6
    //   304: if_icmpeq -> 324
    //   307: aload_1
    //   308: getfield errorCode : I
    //   311: iconst_4
    //   312: if_icmpeq -> 324
    //   315: aload_1
    //   316: getfield errorCode : I
    //   319: bipush #7
    //   321: if_icmpne -> 328
    //   324: aload_1
    //   325: invokestatic throwAsPublicException : (Ljava/lang/Throwable;)V
    //   328: aload #7
    //   330: astore_1
    //   331: aload #8
    //   333: aload_1
    //   334: invokevirtual setRemoteDevice : (Landroid/hardware/camera2/ICameraDeviceUser;)V
    //   337: aload #6
    //   339: monitorexit
    //   340: aload #8
    //   342: areturn
    //   343: new java/lang/AssertionError
    //   346: astore_1
    //   347: aload_1
    //   348: ldc 'Should've gone down the shim path'
    //   350: invokespecial <init> : (Ljava/lang/Object;)V
    //   353: aload_1
    //   354: athrow
    //   355: astore_1
    //   356: aload #6
    //   358: monitorexit
    //   359: aload_1
    //   360: athrow
    //   361: astore_1
    //   362: goto -> 356
    // Line number table:
    //   Java source line number -> byte code offset
    //   #494	-> 0
    //   #495	-> 7
    //   #497	-> 7
    //   #499	-> 16
    //   #501	-> 19
    //   #507	-> 30
    //   #509	-> 48
    //   #512	-> 54
    //   #514	-> 62
    //   #515	-> 69
    //   #520	-> 73
    //   #521	-> 79
    //   #520	-> 95
    //   #522	-> 110
    //   #516	-> 113
    //   #526	-> 126
    //   #530	-> 132
    //   #532	-> 132
    //   #533	-> 140
    //   #534	-> 140
    //   #533	-> 145
    //   #566	-> 153
    //   #527	-> 156
    //   #528	-> 157
    //   #559	-> 192
    //   #536	-> 196
    //   #559	-> 200
    //   #561	-> 201
    //   #564	-> 212
    //   #565	-> 218
    //   #536	-> 228
    //   #537	-> 229
    //   #539	-> 238
    //   #557	-> 285
    //   #547	-> 292
    //   #549	-> 298
    //   #553	-> 324
    //   #566	-> 328
    //   #573	-> 331
    //   #574	-> 337
    //   #575	-> 337
    //   #577	-> 340
    //   #538	-> 343
    //   #575	-> 355
    // Exception table:
    //   from	to	target	type
    //   19	30	355	finally
    //   30	48	355	finally
    //   48	54	355	finally
    //   54	62	228	android/os/ServiceSpecificException
    //   54	62	200	android/os/RemoteException
    //   54	62	355	finally
    //   62	69	228	android/os/ServiceSpecificException
    //   62	69	200	android/os/RemoteException
    //   62	69	355	finally
    //   73	79	228	android/os/ServiceSpecificException
    //   73	79	200	android/os/RemoteException
    //   73	79	355	finally
    //   79	95	228	android/os/ServiceSpecificException
    //   79	95	200	android/os/RemoteException
    //   79	95	355	finally
    //   95	110	228	android/os/ServiceSpecificException
    //   95	110	200	android/os/RemoteException
    //   95	110	355	finally
    //   113	126	228	android/os/ServiceSpecificException
    //   113	126	200	android/os/RemoteException
    //   113	126	355	finally
    //   126	132	156	java/lang/NumberFormatException
    //   126	132	228	android/os/ServiceSpecificException
    //   126	132	200	android/os/RemoteException
    //   126	132	355	finally
    //   132	140	228	android/os/ServiceSpecificException
    //   132	140	200	android/os/RemoteException
    //   132	140	355	finally
    //   140	145	228	android/os/ServiceSpecificException
    //   140	145	200	android/os/RemoteException
    //   140	145	355	finally
    //   145	153	228	android/os/ServiceSpecificException
    //   145	153	200	android/os/RemoteException
    //   145	153	355	finally
    //   157	176	228	android/os/ServiceSpecificException
    //   157	176	200	android/os/RemoteException
    //   157	176	355	finally
    //   176	192	196	android/os/ServiceSpecificException
    //   176	192	192	android/os/RemoteException
    //   176	192	361	finally
    //   201	212	361	finally
    //   212	218	361	finally
    //   218	222	361	finally
    //   229	238	361	finally
    //   238	282	361	finally
    //   285	289	361	finally
    //   292	298	361	finally
    //   298	324	361	finally
    //   324	328	361	finally
    //   331	337	361	finally
    //   337	340	361	finally
    //   343	355	361	finally
    //   356	359	361	finally
  }
  
  public void openCamera(String paramString, CameraDevice.StateCallback paramStateCallback, Handler paramHandler) throws CameraAccessException {
    openCameraForUid(paramString, paramStateCallback, CameraDeviceImpl.checkAndWrapHandler(paramHandler), -1);
  }
  
  public void openCamera(String paramString, Executor paramExecutor, CameraDevice.StateCallback paramStateCallback) throws CameraAccessException {
    if (paramExecutor != null) {
      openCameraForUid(paramString, paramStateCallback, paramExecutor, -1);
      return;
    } 
    throw new IllegalArgumentException("executor was null");
  }
  
  public void openCameraForUid(String paramString, CameraDevice.StateCallback paramStateCallback, Executor paramExecutor, int paramInt) throws CameraAccessException {
    if (paramString != null) {
      if (paramStateCallback != null) {
        if (!CameraManagerGlobal.sCameraServiceDisabled) {
          openCameraDeviceUserAsync(paramString, paramStateCallback, paramExecutor, paramInt);
          return;
        } 
        throw new IllegalArgumentException("No cameras available on device");
      } 
      throw new IllegalArgumentException("callback was null");
    } 
    throw new IllegalArgumentException("cameraId was null");
  }
  
  public void setTorchMode(String paramString, boolean paramBoolean) throws CameraAccessException {
    if (!CameraManagerGlobal.sCameraServiceDisabled) {
      CameraManagerGlobal.get().setTorchMode(paramString, paramBoolean);
      return;
    } 
    throw new IllegalArgumentException("No cameras available on device");
  }
  
  public static abstract class AvailabilityCallback {
    public void onCameraAvailable(String param1String) {}
    
    public void onCameraUnavailable(String param1String) {}
    
    public void onCameraAccessPrioritiesChanged() {}
    
    public void onPhysicalCameraAvailable(String param1String1, String param1String2) {}
    
    public void onPhysicalCameraUnavailable(String param1String1, String param1String2) {}
    
    public void onCameraOpened(String param1String1, String param1String2) {}
    
    public void onCameraClosed(String param1String) {}
  }
  
  public static abstract class TorchCallback {
    public void onTorchModeUnavailable(String param1String) {}
    
    public void onTorchModeChanged(String param1String, boolean param1Boolean) {}
  }
  
  public static void throwAsPublicException(Throwable paramThrowable) throws CameraAccessException {
    ServiceSpecificException serviceSpecificException;
    RuntimeException runtimeException;
    if (paramThrowable instanceof ServiceSpecificException) {
      char c;
      serviceSpecificException = (ServiceSpecificException)paramThrowable;
      switch (serviceSpecificException.errorCode) {
        default:
          c = '\003';
          throw new CameraAccessException(c, serviceSpecificException.getMessage(), serviceSpecificException);
        case 9:
          c = 'Ϩ';
          throw new CameraAccessException(c, serviceSpecificException.getMessage(), serviceSpecificException);
        case 8:
          c = '\005';
          throw new CameraAccessException(c, serviceSpecificException.getMessage(), serviceSpecificException);
        case 7:
          c = '\004';
          throw new CameraAccessException(c, serviceSpecificException.getMessage(), serviceSpecificException);
        case 6:
          c = '\001';
          throw new CameraAccessException(c, serviceSpecificException.getMessage(), serviceSpecificException);
        case 4:
          c = '\002';
          throw new CameraAccessException(c, serviceSpecificException.getMessage(), serviceSpecificException);
        case 2:
        case 3:
          throw new IllegalArgumentException(serviceSpecificException.getMessage(), serviceSpecificException);
        case 1:
          break;
      } 
      throw new SecurityException(serviceSpecificException.getMessage(), serviceSpecificException);
    } 
    if (!(serviceSpecificException instanceof android.os.DeadObjectException)) {
      if (!(serviceSpecificException instanceof RemoteException)) {
        if (!(serviceSpecificException instanceof RuntimeException))
          return; 
        runtimeException = (RuntimeException)serviceSpecificException;
        throw runtimeException;
      } 
      throw new UnsupportedOperationException("An unknown RemoteException was thrown which should never happen.", runtimeException);
    } 
    throw new CameraAccessException(2, "Camera service has died unexpectedly", runtimeException);
  }
  
  private boolean supportsCamera2ApiLocked(String paramString) {
    return supportsCameraApiLocked(paramString, 2);
  }
  
  private boolean supportsCameraApiLocked(String paramString, int paramInt) {
    try {
      ICameraService iCameraService = CameraManagerGlobal.get().getCameraService();
      if (iCameraService == null)
        return false; 
      return iCameraService.supportsCameraApi(paramString, paramInt);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public static boolean isHiddenPhysicalCamera(String paramString) {
    try {
      ICameraService iCameraService = CameraManagerGlobal.get().getCameraService();
      if (iCameraService == null)
        return false; 
      return iCameraService.isHiddenPhysicalCamera(paramString);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  class CameraManagerGlobal extends ICameraServiceListener.Stub implements IBinder.DeathRecipient {
    private final boolean DEBUG = false;
    
    private final int CAMERA_SERVICE_RECONNECT_DELAY_MS = 1000;
    
    private static final CameraManagerGlobal gCameraManager = new CameraManagerGlobal();
    
    private final ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    
    private final ArrayMap<String, Integer> mDeviceStatus = new ArrayMap();
    
    private final ArrayMap<String, ArrayList<String>> mUnavailablePhysicalDevices = new ArrayMap();
    
    private final Set<Set<String>> mConcurrentCameraIdCombinations = (Set<Set<String>>)new ArraySet();
    
    private final ArrayMap<CameraManager.AvailabilityCallback, Executor> mCallbackMap = new ArrayMap();
    
    private Binder mTorchClientBinder = new Binder();
    
    private final ArrayMap<String, Integer> mTorchStatus = new ArrayMap();
    
    private final ArrayMap<CameraManager.TorchCallback, Executor> mTorchCallbackMap = new ArrayMap();
    
    private final Object mLock = new Object();
    
    public static final boolean sCameraServiceDisabled = SystemProperties.getBoolean("config.disable_cameraservice", false);
    
    private static final String CAMERA_SERVICE_BINDER_NAME = "media.camera";
    
    private static final String TAG = "CameraManagerGlobal";
    
    private ICameraService mCameraService;
    
    public static CameraManagerGlobal get() {
      return gCameraManager;
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public ICameraService getCameraService() {
      synchronized (this.mLock) {
        connectCameraServiceLocked();
        if (this.mCameraService == null && !sCameraServiceDisabled)
          Log.e("CameraManagerGlobal", "Camera service is unavailable"); 
        return this.mCameraService;
      } 
    }
    
    private void connectCameraServiceLocked() {
      // Byte code:
      //   0: invokestatic getInstance : ()Landroid/hardware/camera2/OplusCameraManager;
      //   3: invokevirtual setPackageName : ()V
      //   6: aload_0
      //   7: getfield mCameraService : Landroid/hardware/ICameraService;
      //   10: ifnonnull -> 254
      //   13: getstatic android/hardware/camera2/CameraManager$CameraManagerGlobal.sCameraServiceDisabled : Z
      //   16: ifeq -> 22
      //   19: goto -> 254
      //   22: ldc 'CameraManagerGlobal'
      //   24: ldc 'Connecting to camera service'
      //   26: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
      //   29: pop
      //   30: ldc 'media.camera'
      //   32: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
      //   35: astore_1
      //   36: aload_1
      //   37: ifnonnull -> 41
      //   40: return
      //   41: iconst_0
      //   42: istore_2
      //   43: aload_1
      //   44: aload_0
      //   45: iconst_0
      //   46: invokeinterface linkToDeath : (Landroid/os/IBinder$DeathRecipient;I)V
      //   51: aload_1
      //   52: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/hardware/ICameraService;
      //   55: astore_1
      //   56: invokestatic setupGlobalVendorTagDescriptor : ()V
      //   59: goto -> 68
      //   62: astore_3
      //   63: aload_0
      //   64: aload_3
      //   65: invokespecial handleRecoverableSetupErrors : (Landroid/os/ServiceSpecificException;)V
      //   68: aload_1
      //   69: aload_0
      //   70: invokeinterface addListener : (Landroid/hardware/ICameraServiceListener;)[Landroid/hardware/CameraStatus;
      //   75: astore #4
      //   77: aload #4
      //   79: arraylength
      //   80: istore #5
      //   82: iconst_0
      //   83: istore #6
      //   85: iload #6
      //   87: iload #5
      //   89: if_icmpge -> 168
      //   92: aload #4
      //   94: iload #6
      //   96: aaload
      //   97: astore_3
      //   98: aload_0
      //   99: aload_3
      //   100: getfield status : I
      //   103: aload_3
      //   104: getfield cameraId : Ljava/lang/String;
      //   107: invokespecial onStatusChangedLocked : (ILjava/lang/String;)V
      //   110: aload_3
      //   111: getfield unavailablePhysicalCameras : [Ljava/lang/String;
      //   114: ifnull -> 162
      //   117: aload_3
      //   118: getfield unavailablePhysicalCameras : [Ljava/lang/String;
      //   121: astore #7
      //   123: aload #7
      //   125: arraylength
      //   126: istore #8
      //   128: iconst_0
      //   129: istore #9
      //   131: iload #9
      //   133: iload #8
      //   135: if_icmpge -> 162
      //   138: aload #7
      //   140: iload #9
      //   142: aaload
      //   143: astore #10
      //   145: aload_0
      //   146: iconst_0
      //   147: aload_3
      //   148: getfield cameraId : Ljava/lang/String;
      //   151: aload #10
      //   153: invokespecial onPhysicalCameraStatusChangedLocked : (ILjava/lang/String;Ljava/lang/String;)V
      //   156: iinc #9, 1
      //   159: goto -> 131
      //   162: iinc #6, 1
      //   165: goto -> 85
      //   168: aload_0
      //   169: aload_1
      //   170: putfield mCameraService : Landroid/hardware/ICameraService;
      //   173: goto -> 177
      //   176: astore_3
      //   177: aload_1
      //   178: invokeinterface getConcurrentCameraIds : ()[Landroid/hardware/camera2/utils/ConcurrentCameraIdCombination;
      //   183: astore_1
      //   184: aload_1
      //   185: arraylength
      //   186: istore #9
      //   188: iload_2
      //   189: istore #6
      //   191: iload #6
      //   193: iload #9
      //   195: if_icmpge -> 223
      //   198: aload_1
      //   199: iload #6
      //   201: aaload
      //   202: astore_3
      //   203: aload_0
      //   204: getfield mConcurrentCameraIdCombinations : Ljava/util/Set;
      //   207: aload_3
      //   208: invokevirtual getConcurrentCameraIdCombination : ()Ljava/util/Set;
      //   211: invokeinterface add : (Ljava/lang/Object;)Z
      //   216: pop
      //   217: iinc #6, 1
      //   220: goto -> 191
      //   223: goto -> 227
      //   226: astore_1
      //   227: return
      //   228: astore_1
      //   229: new java/lang/IllegalStateException
      //   232: dup
      //   233: ldc 'Failed to get concurrent camera id combinations'
      //   235: aload_1
      //   236: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
      //   239: athrow
      //   240: astore_1
      //   241: new java/lang/IllegalStateException
      //   244: dup
      //   245: ldc 'Failed to register a camera service listener'
      //   247: aload_1
      //   248: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
      //   251: athrow
      //   252: astore_1
      //   253: return
      //   254: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1187	-> 0
      //   #1191	-> 6
      //   #1193	-> 22
      //   #1195	-> 30
      //   #1196	-> 36
      //   #1198	-> 40
      //   #1201	-> 41
      //   #1205	-> 51
      //   #1207	-> 51
      //   #1210	-> 56
      //   #1213	-> 59
      //   #1211	-> 62
      //   #1212	-> 63
      //   #1216	-> 68
      //   #1217	-> 77
      //   #1218	-> 98
      //   #1220	-> 110
      //   #1221	-> 117
      //   #1222	-> 145
      //   #1221	-> 156
      //   #1217	-> 162
      //   #1228	-> 168
      //   #1234	-> 173
      //   #1232	-> 176
      //   #1237	-> 177
      //   #1238	-> 177
      //   #1239	-> 184
      //   #1240	-> 203
      //   #1239	-> 217
      //   #1248	-> 223
      //   #1246	-> 226
      //   #1249	-> 227
      //   #1242	-> 228
      //   #1244	-> 229
      //   #1229	-> 240
      //   #1231	-> 241
      //   #1202	-> 252
      //   #1204	-> 253
      //   #1191	-> 254
      // Exception table:
      //   from	to	target	type
      //   43	51	252	android/os/RemoteException
      //   56	59	62	android/os/ServiceSpecificException
      //   68	77	240	android/os/ServiceSpecificException
      //   68	77	176	android/os/RemoteException
      //   77	82	240	android/os/ServiceSpecificException
      //   77	82	176	android/os/RemoteException
      //   98	110	240	android/os/ServiceSpecificException
      //   98	110	176	android/os/RemoteException
      //   110	117	240	android/os/ServiceSpecificException
      //   110	117	176	android/os/RemoteException
      //   117	128	240	android/os/ServiceSpecificException
      //   117	128	176	android/os/RemoteException
      //   145	156	240	android/os/ServiceSpecificException
      //   145	156	176	android/os/RemoteException
      //   168	173	240	android/os/ServiceSpecificException
      //   168	173	176	android/os/RemoteException
      //   177	184	228	android/os/ServiceSpecificException
      //   177	184	226	android/os/RemoteException
      //   184	188	228	android/os/ServiceSpecificException
      //   184	188	226	android/os/RemoteException
      //   203	217	228	android/os/ServiceSpecificException
      //   203	217	226	android/os/RemoteException
    }
    
    private String[] extractCameraIdListLocked() {
      String str1 = ActivityThread.currentOpPackageName();
      String str2 = SystemProperties.get("vendor.camera.aux.packagelist");
      if (str2.length() > 0) {
        TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
        simpleStringSplitter.setString(str2);
        for (String str : simpleStringSplitter) {
          if (str1.equals(str))
            break; 
        } 
      } 
      int i = 0;
      byte b;
      for (b = 0; b < this.mDeviceStatus.size() && (true || 
        b != 2); b++, i = m) {
        int k = ((Integer)this.mDeviceStatus.valueAt(b)).intValue();
        int m = i;
        if (k != 0)
          if (k == 2) {
            m = i;
          } else {
            m = i + 1;
          }  
      } 
      String[] arrayOfString = new String[i];
      int j = 0;
      for (b = 0; b < this.mDeviceStatus.size() && (true || 
        b != 2); b++, j = i) {
        int k = ((Integer)this.mDeviceStatus.valueAt(b)).intValue();
        i = j;
        if (k != 0)
          if (k == 2) {
            i = j;
          } else {
            arrayOfString[j] = (String)this.mDeviceStatus.keyAt(b);
            i = j + 1;
          }  
      } 
      return arrayOfString;
    }
    
    private Set<Set<String>> extractConcurrentCameraIdListLocked() {
      ArraySet<ArraySet<String>> arraySet = new ArraySet();
      for (Set<String> set : this.mConcurrentCameraIdCombinations) {
        ArraySet<String> arraySet1 = new ArraySet();
        for (String str : set) {
          Integer integer = (Integer)this.mDeviceStatus.get(str);
          if (integer == null)
            continue; 
          if (integer.intValue() == 2 || 
            integer.intValue() == 0)
            continue; 
          arraySet1.add(str);
        } 
        arraySet.add(arraySet1);
      } 
      return (Set)arraySet;
    }
    
    private static void sortCameraIds(String[] param1ArrayOfString) {
      Arrays.sort(param1ArrayOfString, new Comparator<String>() {
            public int compare(String param1String1, String param1String2) {
              byte b1;
              byte b2;
              try {
                b1 = Integer.parseInt(param1String1);
              } catch (NumberFormatException numberFormatException) {
                b1 = -1;
              } 
              try {
                b2 = Integer.parseInt(param1String2);
              } catch (NumberFormatException numberFormatException) {
                b2 = -1;
              } 
              if (b1 >= 0 && b2 >= 0)
                return b1 - b2; 
              if (b1 >= 0)
                return -1; 
              if (b2 >= 0)
                return 1; 
              return param1String1.compareTo(param1String2);
            }
          });
    }
    
    public static boolean cameraStatusesContains(CameraStatus[] param1ArrayOfCameraStatus, String param1String) {
      int i;
      byte b;
      for (i = param1ArrayOfCameraStatus.length, b = 0; b < i; ) {
        CameraStatus cameraStatus = param1ArrayOfCameraStatus[b];
        if (cameraStatus.cameraId.equals(param1String))
          return true; 
        b++;
      } 
      return false;
    }
    
    public String[] getCameraIdListNoLazy() {
      if (sCameraServiceDisabled)
        return new String[0]; 
      ICameraServiceListener.Stub stub = new ICameraServiceListener.Stub() {
          final CameraManager.CameraManagerGlobal this$0;
          
          public void onStatusChanged(int param2Int, String param2String) throws RemoteException {}
          
          public void onPhysicalCameraStatusChanged(int param2Int, String param2String1, String param2String2) throws RemoteException {}
          
          public void onTorchStatusChanged(int param2Int, String param2String) throws RemoteException {}
          
          public void onCameraAccessPrioritiesChanged() {}
          
          public void onCameraOpened(String param2String1, String param2String2) {}
          
          public void onCameraClosed(String param2String) {}
        };
      synchronized (this.mLock) {
        connectCameraServiceLocked();
        try {
          CameraStatus[] arrayOfCameraStatus = this.mCameraService.addListener(stub);
          this.mCameraService.removeListener(stub);
          int i;
          byte b;
          for (i = arrayOfCameraStatus.length, b = 0; b < i; ) {
            CameraStatus cameraStatus = arrayOfCameraStatus[b];
            onStatusChangedLocked(cameraStatus.status, cameraStatus.cameraId);
            b++;
          } 
          Set set = this.mDeviceStatus.keySet();
          ArrayList<String> arrayList = new ArrayList();
          this();
          for (String str : set) {
            if (!cameraStatusesContains(arrayOfCameraStatus, str))
              arrayList.add(str); 
          } 
          for (String str : arrayList)
            onStatusChangedLocked(0, str); 
          String[] arrayOfString = extractCameraIdListLocked();
          sortCameraIds(arrayOfString);
          return arrayOfString;
        } catch (ServiceSpecificException serviceSpecificException) {
          IllegalStateException illegalStateException = new IllegalStateException();
          this("Failed to register a camera service listener", (Throwable)serviceSpecificException);
          throw illegalStateException;
        } catch (RemoteException remoteException) {
          String[] arrayOfString = extractCameraIdListLocked();
          sortCameraIds(arrayOfString);
          return arrayOfString;
        } 
      } 
    }
    
    public String[] getCameraIdList() {
      synchronized (this.mLock) {
        connectCameraServiceLocked();
        String[] arrayOfString = extractCameraIdListLocked();
        sortCameraIds(arrayOfString);
        return arrayOfString;
      } 
    }
    
    public Set<Set<String>> getConcurrentCameraIds() {
      synchronized (this.mLock) {
        connectCameraServiceLocked();
        return extractConcurrentCameraIdListLocked();
      } 
    }
    
    public boolean isConcurrentSessionConfigurationSupported(Map<String, SessionConfiguration> param1Map) throws CameraAccessException {
      // Byte code:
      //   0: aload_1
      //   1: ifnull -> 239
      //   4: aload_1
      //   5: invokeinterface size : ()I
      //   10: istore_2
      //   11: iload_2
      //   12: ifeq -> 228
      //   15: aload_0
      //   16: getfield mLock : Ljava/lang/Object;
      //   19: astore_3
      //   20: aload_3
      //   21: monitorenter
      //   22: iconst_0
      //   23: istore #4
      //   25: aload_0
      //   26: getfield mConcurrentCameraIdCombinations : Ljava/util/Set;
      //   29: invokeinterface iterator : ()Ljava/util/Iterator;
      //   34: astore #5
      //   36: aload #5
      //   38: invokeinterface hasNext : ()Z
      //   43: ifeq -> 80
      //   46: aload #5
      //   48: invokeinterface next : ()Ljava/lang/Object;
      //   53: checkcast java/util/Set
      //   56: astore #6
      //   58: aload #6
      //   60: aload_1
      //   61: invokeinterface keySet : ()Ljava/util/Set;
      //   66: invokeinterface containsAll : (Ljava/util/Collection;)Z
      //   71: ifeq -> 77
      //   74: iconst_1
      //   75: istore #4
      //   77: goto -> 36
      //   80: iload #4
      //   82: ifne -> 98
      //   85: ldc 'CameraManagerGlobal'
      //   87: ldc_w 'isConcurrentSessionConfigurationSupported called with a subset ofcamera ids not returned by getConcurrentCameraIds'
      //   90: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
      //   93: pop
      //   94: aload_3
      //   95: monitorexit
      //   96: iconst_0
      //   97: ireturn
      //   98: iload_2
      //   99: anewarray android/hardware/camera2/utils/CameraIdAndSessionConfiguration
      //   102: astore #5
      //   104: iconst_0
      //   105: istore #4
      //   107: aload_1
      //   108: invokeinterface entrySet : ()Ljava/util/Set;
      //   113: invokeinterface iterator : ()Ljava/util/Iterator;
      //   118: astore_1
      //   119: aload_1
      //   120: invokeinterface hasNext : ()Z
      //   125: ifeq -> 177
      //   128: aload_1
      //   129: invokeinterface next : ()Ljava/lang/Object;
      //   134: checkcast java/util/Map$Entry
      //   137: astore #6
      //   139: aload #5
      //   141: iload #4
      //   143: new android/hardware/camera2/utils/CameraIdAndSessionConfiguration
      //   146: dup
      //   147: aload #6
      //   149: invokeinterface getKey : ()Ljava/lang/Object;
      //   154: checkcast java/lang/String
      //   157: aload #6
      //   159: invokeinterface getValue : ()Ljava/lang/Object;
      //   164: checkcast android/hardware/camera2/params/SessionConfiguration
      //   167: invokespecial <init> : (Ljava/lang/String;Landroid/hardware/camera2/params/SessionConfiguration;)V
      //   170: aastore
      //   171: iinc #4, 1
      //   174: goto -> 119
      //   177: aload_0
      //   178: getfield mCameraService : Landroid/hardware/ICameraService;
      //   181: aload #5
      //   183: invokeinterface isConcurrentSessionConfigurationSupported : ([Landroid/hardware/camera2/utils/CameraIdAndSessionConfiguration;)Z
      //   188: istore #7
      //   190: aload_3
      //   191: monitorexit
      //   192: iload #7
      //   194: ireturn
      //   195: astore_1
      //   196: new android/hardware/camera2/CameraAccessException
      //   199: astore #5
      //   201: aload #5
      //   203: iconst_2
      //   204: ldc_w 'Camera service is currently unavailable'
      //   207: aload_1
      //   208: invokespecial <init> : (ILjava/lang/String;Ljava/lang/Throwable;)V
      //   211: aload #5
      //   213: athrow
      //   214: astore_1
      //   215: aload_1
      //   216: invokestatic throwAsPublicException : (Ljava/lang/Throwable;)V
      //   219: aload_3
      //   220: monitorexit
      //   221: iconst_0
      //   222: ireturn
      //   223: astore_1
      //   224: aload_3
      //   225: monitorexit
      //   226: aload_1
      //   227: athrow
      //   228: new java/lang/IllegalArgumentException
      //   231: dup
      //   232: ldc_w 'camera id and session combination is empty'
      //   235: invokespecial <init> : (Ljava/lang/String;)V
      //   238: athrow
      //   239: new java/lang/IllegalArgumentException
      //   242: dup
      //   243: ldc_w 'cameraIdsAndSessionConfigurations was null'
      //   246: invokespecial <init> : (Ljava/lang/String;)V
      //   249: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1462	-> 0
      //   #1466	-> 4
      //   #1467	-> 11
      //   #1471	-> 15
      //   #1474	-> 22
      //   #1475	-> 25
      //   #1476	-> 58
      //   #1477	-> 74
      //   #1479	-> 77
      //   #1480	-> 80
      //   #1481	-> 85
      //   #1483	-> 94
      //   #1485	-> 98
      //   #1487	-> 104
      //   #1489	-> 107
      //   #1490	-> 139
      //   #1491	-> 139
      //   #1492	-> 171
      //   #1493	-> 174
      //   #1495	-> 177
      //   #1499	-> 195
      //   #1501	-> 196
      //   #1497	-> 214
      //   #1498	-> 215
      //   #1503	-> 219
      //   #1504	-> 219
      //   #1506	-> 221
      //   #1504	-> 223
      //   #1468	-> 228
      //   #1463	-> 239
      // Exception table:
      //   from	to	target	type
      //   25	36	223	finally
      //   36	58	223	finally
      //   58	74	223	finally
      //   85	94	223	finally
      //   94	96	223	finally
      //   98	104	223	finally
      //   107	119	223	finally
      //   119	139	223	finally
      //   139	171	223	finally
      //   177	190	214	android/os/ServiceSpecificException
      //   177	190	195	android/os/RemoteException
      //   177	190	223	finally
      //   190	192	223	finally
      //   196	214	223	finally
      //   215	219	223	finally
      //   219	221	223	finally
      //   224	226	223	finally
    }
    
    public boolean cameraIdHasConcurrentStreamsLocked(String param1String) {
      if (!this.mDeviceStatus.containsKey(param1String)) {
        Log.e("CameraManagerGlobal", "cameraIdHasConcurrentStreamsLocked called on non existing camera id");
        return false;
      } 
      for (Set<String> set : this.mConcurrentCameraIdCombinations) {
        if (set.contains(param1String))
          return true; 
      } 
      return false;
    }
    
    public void setTorchMode(String param1String, boolean param1Boolean) throws CameraAccessException {
      // Byte code:
      //   0: aload_0
      //   1: getfield mLock : Ljava/lang/Object;
      //   4: astore_3
      //   5: aload_3
      //   6: monitorenter
      //   7: aload_1
      //   8: ifnull -> 198
      //   11: invokestatic currentOpPackageName : ()Ljava/lang/String;
      //   14: astore #4
      //   16: ldc 'vendor.camera.aux.packagelist'
      //   18: invokestatic get : (Ljava/lang/String;)Ljava/lang/String;
      //   21: astore #5
      //   23: aload #5
      //   25: invokevirtual length : ()I
      //   28: ifle -> 99
      //   31: new android/text/TextUtils$SimpleStringSplitter
      //   34: astore #6
      //   36: aload #6
      //   38: bipush #44
      //   40: invokespecial <init> : (C)V
      //   43: aload #6
      //   45: aload #5
      //   47: invokeinterface setString : (Ljava/lang/String;)V
      //   52: aload #6
      //   54: invokeinterface iterator : ()Ljava/util/Iterator;
      //   59: astore #6
      //   61: aload #6
      //   63: invokeinterface hasNext : ()Z
      //   68: ifeq -> 99
      //   71: aload #6
      //   73: invokeinterface next : ()Ljava/lang/Object;
      //   78: checkcast java/lang/String
      //   81: astore #5
      //   83: aload #4
      //   85: aload #5
      //   87: invokevirtual equals : (Ljava/lang/Object;)Z
      //   90: ifeq -> 96
      //   93: goto -> 99
      //   96: goto -> 61
      //   99: iconst_1
      //   100: ifne -> 127
      //   103: aload_1
      //   104: invokestatic parseInt : (Ljava/lang/String;)I
      //   107: iconst_2
      //   108: if_icmpge -> 114
      //   111: goto -> 127
      //   114: new java/lang/IllegalArgumentException
      //   117: astore_1
      //   118: aload_1
      //   119: ldc_w 'invalid cameraId'
      //   122: invokespecial <init> : (Ljava/lang/String;)V
      //   125: aload_1
      //   126: athrow
      //   127: aload_0
      //   128: invokevirtual getCameraService : ()Landroid/hardware/ICameraService;
      //   131: astore #4
      //   133: aload #4
      //   135: ifnull -> 180
      //   138: aload #4
      //   140: aload_1
      //   141: iload_2
      //   142: aload_0
      //   143: getfield mTorchClientBinder : Landroid/os/Binder;
      //   146: invokeinterface setTorchMode : (Ljava/lang/String;ZLandroid/os/IBinder;)V
      //   151: goto -> 177
      //   154: astore_1
      //   155: new android/hardware/camera2/CameraAccessException
      //   158: astore_1
      //   159: aload_1
      //   160: iconst_2
      //   161: ldc_w 'Camera service is currently unavailable'
      //   164: invokespecial <init> : (ILjava/lang/String;)V
      //   167: aload_1
      //   168: athrow
      //   169: astore_1
      //   170: aload_1
      //   171: invokestatic throwAsPublicException : (Ljava/lang/Throwable;)V
      //   174: goto -> 151
      //   177: aload_3
      //   178: monitorexit
      //   179: return
      //   180: new android/hardware/camera2/CameraAccessException
      //   183: astore_1
      //   184: aload_1
      //   185: iconst_2
      //   186: ldc_w 'Camera service is currently unavailable'
      //   189: invokespecial <init> : (ILjava/lang/String;)V
      //   192: aload_1
      //   193: athrow
      //   194: astore_1
      //   195: goto -> 211
      //   198: new java/lang/IllegalArgumentException
      //   201: astore_1
      //   202: aload_1
      //   203: ldc_w 'cameraId was null'
      //   206: invokespecial <init> : (Ljava/lang/String;)V
      //   209: aload_1
      //   210: athrow
      //   211: aload_3
      //   212: monitorexit
      //   213: aload_1
      //   214: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1530	-> 0
      //   #1532	-> 7
      //   #1543	-> 11
      //   #1545	-> 11
      //   #1546	-> 16
      //   #1547	-> 23
      //   #1548	-> 31
      //   #1549	-> 43
      //   #1550	-> 52
      //   #1551	-> 83
      //   #1552	-> 93
      //   #1553	-> 93
      //   #1555	-> 96
      //   #1557	-> 99
      //   #1558	-> 114
      //   #1561	-> 127
      //   #1562	-> 133
      //   #1568	-> 138
      //   #1574	-> 151
      //   #1571	-> 154
      //   #1572	-> 155
      //   #1569	-> 169
      //   #1570	-> 170
      //   #1575	-> 177
      //   #1576	-> 179
      //   #1563	-> 180
      //   #1575	-> 194
      //   #1533	-> 198
      //   #1575	-> 211
      // Exception table:
      //   from	to	target	type
      //   11	16	194	finally
      //   16	23	194	finally
      //   23	31	194	finally
      //   31	43	194	finally
      //   43	52	194	finally
      //   52	61	194	finally
      //   61	83	194	finally
      //   83	93	194	finally
      //   103	111	194	finally
      //   114	127	194	finally
      //   127	133	194	finally
      //   138	151	169	android/os/ServiceSpecificException
      //   138	151	154	android/os/RemoteException
      //   138	151	194	finally
      //   155	169	194	finally
      //   170	174	194	finally
      //   177	179	194	finally
      //   180	194	194	finally
      //   198	211	194	finally
      //   211	213	194	finally
    }
    
    private void handleRecoverableSetupErrors(ServiceSpecificException param1ServiceSpecificException) {
      if (param1ServiceSpecificException.errorCode == 4) {
        Log.w("CameraManagerGlobal", param1ServiceSpecificException.getMessage());
        return;
      } 
      throw new IllegalStateException(param1ServiceSpecificException);
    }
    
    private boolean isAvailable(int param1Int) {
      if (param1Int != 1)
        return false; 
      return true;
    }
    
    private boolean validStatus(int param1Int) {
      if (param1Int != -2 && param1Int != 0 && param1Int != 1 && param1Int != 2)
        return false; 
      return true;
    }
    
    private boolean validTorchStatus(int param1Int) {
      if (param1Int != 0 && param1Int != 1 && param1Int != 2)
        return false; 
      return true;
    }
    
    private void postSingleAccessPriorityChangeUpdate(CameraManager.AvailabilityCallback param1AvailabilityCallback, Executor param1Executor) {
      long l = Binder.clearCallingIdentity();
      try {
        Runnable runnable = new Runnable() {
            final CameraManager.CameraManagerGlobal this$0;
            
            final CameraManager.AvailabilityCallback val$callback;
            
            public void run() {
              callback.onCameraAccessPrioritiesChanged();
            }
          };
        super(this, param1AvailabilityCallback);
        param1Executor.execute(runnable);
        return;
      } finally {
        Binder.restoreCallingIdentity(l);
      } 
    }
    
    private void postSingleCameraOpenedUpdate(CameraManager.AvailabilityCallback param1AvailabilityCallback, Executor param1Executor, String param1String1, String param1String2) {
      long l = Binder.clearCallingIdentity();
      try {
        Runnable runnable = new Runnable() {
            final CameraManager.CameraManagerGlobal this$0;
            
            final CameraManager.AvailabilityCallback val$callback;
            
            final String val$id;
            
            final String val$packageId;
            
            public void run() {
              callback.onCameraOpened(id, packageId);
            }
          };
        super(this, param1AvailabilityCallback, param1String1, param1String2);
        param1Executor.execute(runnable);
        return;
      } finally {
        Binder.restoreCallingIdentity(l);
      } 
    }
    
    private void postSingleCameraClosedUpdate(CameraManager.AvailabilityCallback param1AvailabilityCallback, Executor param1Executor, String param1String) {
      long l = Binder.clearCallingIdentity();
      try {
        Runnable runnable = new Runnable() {
            final CameraManager.CameraManagerGlobal this$0;
            
            final CameraManager.AvailabilityCallback val$callback;
            
            final String val$id;
            
            public void run() {
              callback.onCameraClosed(id);
            }
          };
        super(this, param1AvailabilityCallback, param1String);
        param1Executor.execute(runnable);
        return;
      } finally {
        Binder.restoreCallingIdentity(l);
      } 
    }
    
    private void postSingleUpdate(CameraManager.AvailabilityCallback param1AvailabilityCallback, Executor param1Executor, String param1String1, String param1String2, int param1Int) {
      if (isAvailable(param1Int)) {
        long l = Binder.clearCallingIdentity();
        try {
          Runnable runnable = new Runnable() {
              final CameraManager.CameraManagerGlobal this$0;
              
              final CameraManager.AvailabilityCallback val$callback;
              
              final String val$id;
              
              final String val$physicalId;
              
              public void run() {
                String str = physicalId;
                if (str == null) {
                  callback.onCameraAvailable(id);
                } else {
                  callback.onPhysicalCameraAvailable(id, str);
                } 
              }
            };
          super(this, param1String2, param1AvailabilityCallback, param1String1);
          param1Executor.execute(runnable);
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } else {
        long l = Binder.clearCallingIdentity();
        try {
          Runnable runnable = new Runnable() {
              final CameraManager.CameraManagerGlobal this$0;
              
              final CameraManager.AvailabilityCallback val$callback;
              
              final String val$id;
              
              final String val$physicalId;
              
              public void run() {
                String str = physicalId;
                if (str == null) {
                  callback.onCameraUnavailable(id);
                } else {
                  callback.onPhysicalCameraUnavailable(id, str);
                } 
              }
            };
          super(this, param1String2, param1AvailabilityCallback, param1String1);
          param1Executor.execute(runnable);
          return;
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } 
    }
    
    private void postSingleTorchUpdate(CameraManager.TorchCallback param1TorchCallback, Executor param1Executor, String param1String, int param1Int) {
      if (param1Int != 1 && param1Int != 2) {
        long l = Binder.clearCallingIdentity();
        try {
          _$$Lambda$CameraManager$CameraManagerGlobal$6Ptxoe4wF_VCkE_pml8t66mklao _$$Lambda$CameraManager$CameraManagerGlobal$6Ptxoe4wF_VCkE_pml8t66mklao = new _$$Lambda$CameraManager$CameraManagerGlobal$6Ptxoe4wF_VCkE_pml8t66mklao();
          this(param1TorchCallback, param1String);
          param1Executor.execute(_$$Lambda$CameraManager$CameraManagerGlobal$6Ptxoe4wF_VCkE_pml8t66mklao);
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } else {
        long l = Binder.clearCallingIdentity();
        try {
          _$$Lambda$CameraManager$CameraManagerGlobal$CONvadOBAEkcHSpx8j61v67qRGM _$$Lambda$CameraManager$CameraManagerGlobal$CONvadOBAEkcHSpx8j61v67qRGM = new _$$Lambda$CameraManager$CameraManagerGlobal$CONvadOBAEkcHSpx8j61v67qRGM();
          this(param1TorchCallback, param1String, param1Int);
          param1Executor.execute(_$$Lambda$CameraManager$CameraManagerGlobal$CONvadOBAEkcHSpx8j61v67qRGM);
          return;
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } 
    }
    
    private void updateCallbackLocked(CameraManager.AvailabilityCallback param1AvailabilityCallback, Executor param1Executor) {
      for (byte b = 0; b < this.mDeviceStatus.size(); b++) {
        String str = (String)this.mDeviceStatus.keyAt(b);
        Integer integer = (Integer)this.mDeviceStatus.valueAt(b);
        postSingleUpdate(param1AvailabilityCallback, param1Executor, str, null, integer.intValue());
        if (isAvailable(integer.intValue()) && this.mUnavailablePhysicalDevices.containsKey(str)) {
          ArrayList arrayList = (ArrayList)this.mUnavailablePhysicalDevices.get(str);
          for (String str1 : arrayList)
            postSingleUpdate(param1AvailabilityCallback, param1Executor, str, str1, 0); 
        } 
      } 
    }
    
    private void onStatusChangedLocked(int param1Int, String param1String) {
      Integer integer;
      String str1 = ActivityThread.currentOpPackageName();
      str2 = SystemProperties.get("vendor.camera.aux.packagelist");
      if (str2.length() > 0) {
        TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
        simpleStringSplitter.setString(str2);
        for (String str2 : simpleStringSplitter) {
          if (str1.equals(str2))
            break; 
        } 
      } 
      if (!true && 
        Integer.parseInt(param1String) >= 2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[soar.cts] ignore the status update of camera: ");
        stringBuilder.append(param1String);
        Log.w("CameraManagerGlobal", stringBuilder.toString());
        return;
      } 
      if (!validStatus(param1Int)) {
        Log.e("CameraManagerGlobal", String.format("Ignoring invalid device %s status 0x%x", new Object[] { param1String, Integer.valueOf(param1Int) }));
        return;
      } 
      if (param1Int == 0) {
        integer = (Integer)this.mDeviceStatus.remove(param1String);
        this.mUnavailablePhysicalDevices.remove(param1String);
      } else {
        integer = (Integer)this.mDeviceStatus.put(param1String, Integer.valueOf(param1Int));
        if (integer == null)
          this.mUnavailablePhysicalDevices.put(param1String, new ArrayList()); 
      } 
      if (integer != null && integer.intValue() == param1Int)
        return; 
      if (integer != null && isAvailable(param1Int) == isAvailable(integer.intValue()))
        return; 
      int i = this.mCallbackMap.size();
      for (byte b = 0; b < i; b++) {
        Executor executor = (Executor)this.mCallbackMap.valueAt(b);
        CameraManager.AvailabilityCallback availabilityCallback = (CameraManager.AvailabilityCallback)this.mCallbackMap.keyAt(b);
        postSingleUpdate(availabilityCallback, executor, param1String, null, param1Int);
      } 
    }
    
    private void onPhysicalCameraStatusChangedLocked(int param1Int, String param1String1, String param1String2) {
      if (!validStatus(param1Int)) {
        Log.e("CameraManagerGlobal", String.format("Ignoring invalid device %s physical device %s status 0x%x", new Object[] { param1String1, param1String2, Integer.valueOf(param1Int) }));
        return;
      } 
      if (this.mDeviceStatus.containsKey(param1String1) && isAvailable(((Integer)this.mDeviceStatus.get(param1String1)).intValue())) {
        ArrayMap<String, ArrayList<String>> arrayMap = this.mUnavailablePhysicalDevices;
        if (arrayMap.containsKey(param1String1)) {
          ArrayList<String> arrayList = (ArrayList)this.mUnavailablePhysicalDevices.get(param1String1);
          if (!isAvailable(param1Int) && 
            !arrayList.contains(param1String2)) {
            arrayList.add(param1String2);
          } else if (isAvailable(param1Int) && 
            arrayList.contains(param1String2)) {
            arrayList.remove(param1String2);
          } else {
            return;
          } 
          int i = this.mCallbackMap.size();
          for (byte b = 0; b < i; b++) {
            Executor executor = (Executor)this.mCallbackMap.valueAt(b);
            CameraManager.AvailabilityCallback availabilityCallback = (CameraManager.AvailabilityCallback)this.mCallbackMap.keyAt(b);
            postSingleUpdate(availabilityCallback, executor, param1String1, param1String2, param1Int);
          } 
          return;
        } 
      } 
      Log.e("CameraManagerGlobal", String.format("Camera %s is not available. Ignore physical camera status change", new Object[] { param1String1 }));
    }
    
    private void updateTorchCallbackLocked(CameraManager.TorchCallback param1TorchCallback, Executor param1Executor) {
      for (byte b = 0; b < this.mTorchStatus.size(); b++) {
        String str = (String)this.mTorchStatus.keyAt(b);
        Integer integer = (Integer)this.mTorchStatus.valueAt(b);
        postSingleTorchUpdate(param1TorchCallback, param1Executor, str, integer.intValue());
      } 
    }
    
    private void onTorchStatusChangedLocked(int param1Int, String param1String) {
      int i = 0;
      String str1 = ActivityThread.currentOpPackageName();
      String str2 = SystemProperties.get("vendor.camera.aux.packagelist");
      int j = i;
      if (str2.length() > 0) {
        TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
        simpleStringSplitter.setString(str2);
        Iterator<String> iterator = simpleStringSplitter.iterator();
        while (true) {
          j = i;
          if (iterator.hasNext()) {
            String str = iterator.next();
            if (str1.equals(str)) {
              j = 1;
              break;
            } 
            continue;
          } 
          break;
        } 
      } 
      if (!j && 
        Integer.parseInt(param1String) >= 2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ignore the torch status update of camera: ");
        stringBuilder.append(param1String);
        Log.w("CameraManagerGlobal", stringBuilder.toString());
        return;
      } 
      if (!validTorchStatus(param1Int)) {
        Log.e("CameraManagerGlobal", String.format("Ignoring invalid device %s torch status 0x%x", new Object[] { param1String, Integer.valueOf(param1Int) }));
        return;
      } 
      Integer integer = (Integer)this.mTorchStatus.put(param1String, Integer.valueOf(param1Int));
      if (integer != null && integer.intValue() == param1Int)
        return; 
      i = this.mTorchCallbackMap.size();
      for (j = 0; j < i; j++) {
        Executor executor = (Executor)this.mTorchCallbackMap.valueAt(j);
        CameraManager.TorchCallback torchCallback = (CameraManager.TorchCallback)this.mTorchCallbackMap.keyAt(j);
        postSingleTorchUpdate(torchCallback, executor, param1String, param1Int);
      } 
    }
    
    public void registerAvailabilityCallback(CameraManager.AvailabilityCallback param1AvailabilityCallback, Executor param1Executor) {
      synchronized (this.mLock) {
        connectCameraServiceLocked();
        Executor executor = (Executor)this.mCallbackMap.put(param1AvailabilityCallback, param1Executor);
        if (executor == null)
          updateCallbackLocked(param1AvailabilityCallback, param1Executor); 
        if (this.mCameraService == null)
          scheduleCameraServiceReconnectionLocked(); 
        return;
      } 
    }
    
    public void unregisterAvailabilityCallback(CameraManager.AvailabilityCallback param1AvailabilityCallback) {
      synchronized (this.mLock) {
        this.mCallbackMap.remove(param1AvailabilityCallback);
        return;
      } 
    }
    
    public void registerTorchCallback(CameraManager.TorchCallback param1TorchCallback, Executor param1Executor) {
      synchronized (this.mLock) {
        connectCameraServiceLocked();
        Executor executor = (Executor)this.mTorchCallbackMap.put(param1TorchCallback, param1Executor);
        if (executor == null)
          updateTorchCallbackLocked(param1TorchCallback, param1Executor); 
        if (this.mCameraService == null)
          scheduleCameraServiceReconnectionLocked(); 
        return;
      } 
    }
    
    public void unregisterTorchCallback(CameraManager.TorchCallback param1TorchCallback) {
      synchronized (this.mLock) {
        this.mTorchCallbackMap.remove(param1TorchCallback);
        return;
      } 
    }
    
    public void onStatusChanged(int param1Int, String param1String) throws RemoteException {
      synchronized (this.mLock) {
        onStatusChangedLocked(param1Int, param1String);
        return;
      } 
    }
    
    public void onPhysicalCameraStatusChanged(int param1Int, String param1String1, String param1String2) throws RemoteException {
      synchronized (this.mLock) {
        onPhysicalCameraStatusChangedLocked(param1Int, param1String1, param1String2);
        return;
      } 
    }
    
    public void onTorchStatusChanged(int param1Int, String param1String) throws RemoteException {
      synchronized (this.mLock) {
        onTorchStatusChangedLocked(param1Int, param1String);
        return;
      } 
    }
    
    public void onCameraAccessPrioritiesChanged() {
      synchronized (this.mLock) {
        int i = this.mCallbackMap.size();
        for (byte b = 0; b < i; b++) {
          Executor executor = (Executor)this.mCallbackMap.valueAt(b);
          CameraManager.AvailabilityCallback availabilityCallback = (CameraManager.AvailabilityCallback)this.mCallbackMap.keyAt(b);
          postSingleAccessPriorityChangeUpdate(availabilityCallback, executor);
        } 
        return;
      } 
    }
    
    public void onCameraOpened(String param1String1, String param1String2) {
      synchronized (this.mLock) {
        int i = this.mCallbackMap.size();
        for (byte b = 0; b < i; b++) {
          Executor executor = (Executor)this.mCallbackMap.valueAt(b);
          CameraManager.AvailabilityCallback availabilityCallback = (CameraManager.AvailabilityCallback)this.mCallbackMap.keyAt(b);
          postSingleCameraOpenedUpdate(availabilityCallback, executor, param1String1, param1String2);
        } 
        return;
      } 
    }
    
    public void onCameraClosed(String param1String) {
      synchronized (this.mLock) {
        int i = this.mCallbackMap.size();
        for (byte b = 0; b < i; b++) {
          Executor executor = (Executor)this.mCallbackMap.valueAt(b);
          CameraManager.AvailabilityCallback availabilityCallback = (CameraManager.AvailabilityCallback)this.mCallbackMap.keyAt(b);
          postSingleCameraClosedUpdate(availabilityCallback, executor, param1String);
        } 
        return;
      } 
    }
    
    private void scheduleCameraServiceReconnectionLocked() {
      if (this.mCallbackMap.isEmpty() && this.mTorchCallbackMap.isEmpty())
        return; 
      try {
        ScheduledExecutorService scheduledExecutorService = this.mScheduler;
        _$$Lambda$CameraManager$CameraManagerGlobal$w1y8myi6vgxAcTEs8WArI_NN3R0 _$$Lambda$CameraManager$CameraManagerGlobal$w1y8myi6vgxAcTEs8WArI_NN3R0 = new _$$Lambda$CameraManager$CameraManagerGlobal$w1y8myi6vgxAcTEs8WArI_NN3R0();
        this(this);
        scheduledExecutorService.schedule(_$$Lambda$CameraManager$CameraManagerGlobal$w1y8myi6vgxAcTEs8WArI_NN3R0, 1000L, TimeUnit.MILLISECONDS);
      } catch (RejectedExecutionException rejectedExecutionException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to schedule camera service re-connect: ");
        stringBuilder.append(rejectedExecutionException);
        Log.e("CameraManagerGlobal", stringBuilder.toString());
      } 
    }
    
    public void binderDied() {
      synchronized (this.mLock) {
        if (this.mCameraService == null)
          return; 
        this.mCameraService = null;
        byte b;
        for (b = 0; b < this.mDeviceStatus.size(); b++) {
          String str = (String)this.mDeviceStatus.keyAt(b);
          onStatusChangedLocked(0, str);
        } 
        for (b = 0; b < this.mTorchStatus.size(); b++) {
          String str = (String)this.mTorchStatus.keyAt(b);
          onTorchStatusChangedLocked(0, str);
        } 
        this.mConcurrentCameraIdCombinations.clear();
        scheduleCameraServiceReconnectionLocked();
        return;
      } 
    }
  }
  
  class null implements Comparator<String> {
    public int compare(String param1String1, String param1String2) {
      byte b1;
      byte b2;
      try {
        b1 = Integer.parseInt(param1String1);
      } catch (NumberFormatException numberFormatException) {
        b1 = -1;
      } 
      try {
        b2 = Integer.parseInt(param1String2);
      } catch (NumberFormatException numberFormatException) {
        b2 = -1;
      } 
      if (b1 >= 0 && b2 >= 0)
        return b1 - b2; 
      if (b1 >= 0)
        return -1; 
      if (b2 >= 0)
        return 1; 
      return param1String1.compareTo(param1String2);
    }
  }
  
  class null implements Runnable {
    final CameraManager.CameraManagerGlobal this$0;
    
    final CameraManager.AvailabilityCallback val$callback;
    
    public void run() {
      callback.onCameraAccessPrioritiesChanged();
    }
  }
  
  class null implements Runnable {
    final CameraManager.CameraManagerGlobal this$0;
    
    final CameraManager.AvailabilityCallback val$callback;
    
    final String val$id;
    
    final String val$packageId;
    
    public void run() {
      callback.onCameraOpened(id, packageId);
    }
  }
  
  class null implements Runnable {
    final CameraManager.CameraManagerGlobal this$0;
    
    final CameraManager.AvailabilityCallback val$callback;
    
    final String val$id;
    
    public void run() {
      callback.onCameraClosed(id);
    }
  }
  
  class null implements Runnable {
    final CameraManager.CameraManagerGlobal this$0;
    
    final CameraManager.AvailabilityCallback val$callback;
    
    final String val$id;
    
    final String val$physicalId;
    
    public void run() {
      String str = physicalId;
      if (str == null) {
        callback.onCameraAvailable(id);
      } else {
        callback.onPhysicalCameraAvailable(id, str);
      } 
    }
  }
  
  class null implements Runnable {
    final CameraManager.CameraManagerGlobal this$0;
    
    final CameraManager.AvailabilityCallback val$callback;
    
    final String val$id;
    
    final String val$physicalId;
    
    public void run() {
      String str = physicalId;
      if (str == null) {
        callback.onCameraUnavailable(id);
      } else {
        callback.onPhysicalCameraUnavailable(id, str);
      } 
    }
  }
}
