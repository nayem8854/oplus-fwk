package android.hardware.camera2;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public final class OplusCameraStateBroadcast {
  private static final String TAG = "OplusCameraStateBroadcast";
  
  private static OplusCameraStateBroadcast sInstance = new OplusCameraStateBroadcast();
  
  public static OplusCameraStateBroadcast getInstance() {
    // Byte code:
    //   0: ldc android/hardware/camera2/OplusCameraStateBroadcast
    //   2: monitorenter
    //   3: getstatic android/hardware/camera2/OplusCameraStateBroadcast.sInstance : Landroid/hardware/camera2/OplusCameraStateBroadcast;
    //   6: astore_0
    //   7: ldc android/hardware/camera2/OplusCameraStateBroadcast
    //   9: monitorexit
    //   10: aload_0
    //   11: areturn
    //   12: astore_0
    //   13: ldc android/hardware/camera2/OplusCameraStateBroadcast
    //   15: monitorexit
    //   16: aload_0
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #36	-> 3
    //   #36	-> 12
    // Exception table:
    //   from	to	target	type
    //   3	7	12	finally
  }
  
  public void notifyCameraState(Context paramContext, int paramInt1, String paramString, int paramInt2) {
    try {
      Intent intent = new Intent();
      this("oppo.intent.action.camerastate");
      intent.putExtra("packageName", paramString);
      intent.putExtra("cameraState", paramInt1);
      if (paramInt2 != 0) {
        intent.putExtra("frontCamera", true);
      } else {
        intent.putExtra("frontCamera", false);
      } 
      paramContext.sendBroadcast(intent);
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("notifyCameraState packageName:");
      stringBuilder.append(paramString);
      stringBuilder.append(" cameraState:");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" frontcamera:");
      stringBuilder.append(paramInt2);
      Log.i("OplusCameraStateBroadcast", stringBuilder.toString());
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
  }
}
