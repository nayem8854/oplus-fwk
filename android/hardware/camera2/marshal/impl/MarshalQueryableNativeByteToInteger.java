package android.hardware.camera2.marshal.impl;

import android.hardware.camera2.marshal.MarshalQueryable;
import android.hardware.camera2.marshal.Marshaler;
import android.hardware.camera2.utils.TypeReference;
import java.nio.ByteBuffer;

public class MarshalQueryableNativeByteToInteger implements MarshalQueryable<Integer> {
  private static final int UINT8_MASK = 255;
  
  private class MarshalerNativeByteToInteger extends Marshaler<Integer> {
    final MarshalQueryableNativeByteToInteger this$0;
    
    protected MarshalerNativeByteToInteger(TypeReference<Integer> param1TypeReference, int param1Int) {
      super(MarshalQueryableNativeByteToInteger.this, param1TypeReference, param1Int);
    }
    
    public void marshal(Integer param1Integer, ByteBuffer param1ByteBuffer) {
      param1ByteBuffer.put((byte)param1Integer.intValue());
    }
    
    public Integer unmarshal(ByteBuffer param1ByteBuffer) {
      return Integer.valueOf(param1ByteBuffer.get() & 0xFF);
    }
    
    public int getNativeSize() {
      return 1;
    }
  }
  
  public Marshaler<Integer> createMarshaler(TypeReference<Integer> paramTypeReference, int paramInt) {
    return new MarshalerNativeByteToInteger(paramTypeReference, paramInt);
  }
  
  public boolean isTypeMappingSupported(TypeReference<Integer> paramTypeReference, int paramInt) {
    // Byte code:
    //   0: ldc java/lang/Integer
    //   2: aload_1
    //   3: invokevirtual getType : ()Ljava/lang/reflect/Type;
    //   6: invokevirtual equals : (Ljava/lang/Object;)Z
    //   9: ifne -> 27
    //   12: getstatic java/lang/Integer.TYPE : Ljava/lang/Class;
    //   15: astore_3
    //   16: aload_3
    //   17: aload_1
    //   18: invokevirtual getType : ()Ljava/lang/reflect/Type;
    //   21: invokevirtual equals : (Ljava/lang/Object;)Z
    //   24: ifeq -> 37
    //   27: iload_2
    //   28: ifne -> 37
    //   31: iconst_1
    //   32: istore #4
    //   34: goto -> 40
    //   37: iconst_0
    //   38: istore #4
    //   40: iload #4
    //   42: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #65	-> 0
    //   #66	-> 16
    //   #65	-> 40
  }
}
