package android.hardware.camera2.marshal.impl;

import android.hardware.camera2.marshal.MarshalHelpers;
import android.hardware.camera2.marshal.MarshalQueryable;
import android.hardware.camera2.marshal.Marshaler;
import android.hardware.camera2.utils.TypeReference;
import android.util.Rational;
import java.nio.ByteBuffer;

public final class MarshalQueryablePrimitive<T> implements MarshalQueryable<T> {
  private class MarshalerPrimitive extends Marshaler<T> {
    private final Class<T> mClass;
    
    final MarshalQueryablePrimitive this$0;
    
    protected MarshalerPrimitive(TypeReference<T> param1TypeReference, int param1Int) {
      super(MarshalQueryablePrimitive.this, param1TypeReference, param1Int);
      this.mClass = MarshalHelpers.wrapClassIfPrimitive((Class)param1TypeReference.getRawType());
    }
    
    public T unmarshal(ByteBuffer param1ByteBuffer) {
      return this.mClass.cast(unmarshalObject(param1ByteBuffer));
    }
    
    public int calculateMarshalSize(T param1T) {
      return MarshalHelpers.getPrimitiveTypeSize(this.mNativeType);
    }
    
    public void marshal(T param1T, ByteBuffer param1ByteBuffer) {
      if (param1T instanceof Integer) {
        MarshalHelpers.checkNativeTypeEquals(1, this.mNativeType);
        int i = ((Integer)param1T).intValue();
        marshalPrimitive(i, param1ByteBuffer);
      } else if (param1T instanceof Float) {
        MarshalHelpers.checkNativeTypeEquals(2, this.mNativeType);
        float f = ((Float)param1T).floatValue();
        marshalPrimitive(f, param1ByteBuffer);
      } else if (param1T instanceof Long) {
        MarshalHelpers.checkNativeTypeEquals(3, this.mNativeType);
        long l = ((Long)param1T).longValue();
        marshalPrimitive(l, param1ByteBuffer);
      } else if (param1T instanceof Rational) {
        MarshalHelpers.checkNativeTypeEquals(5, this.mNativeType);
        marshalPrimitive((Rational)param1T, param1ByteBuffer);
      } else if (param1T instanceof Double) {
        MarshalHelpers.checkNativeTypeEquals(4, this.mNativeType);
        double d = ((Double)param1T).doubleValue();
        marshalPrimitive(d, param1ByteBuffer);
      } else {
        if (param1T instanceof Byte) {
          MarshalHelpers.checkNativeTypeEquals(0, this.mNativeType);
          byte b = ((Byte)param1T).byteValue();
          marshalPrimitive(b, param1ByteBuffer);
          return;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Can't marshal managed type ");
        stringBuilder.append(this.mTypeReference);
        throw new UnsupportedOperationException(stringBuilder.toString());
      } 
    }
    
    private void marshalPrimitive(int param1Int, ByteBuffer param1ByteBuffer) {
      param1ByteBuffer.putInt(param1Int);
    }
    
    private void marshalPrimitive(float param1Float, ByteBuffer param1ByteBuffer) {
      param1ByteBuffer.putFloat(param1Float);
    }
    
    private void marshalPrimitive(double param1Double, ByteBuffer param1ByteBuffer) {
      param1ByteBuffer.putDouble(param1Double);
    }
    
    private void marshalPrimitive(long param1Long, ByteBuffer param1ByteBuffer) {
      param1ByteBuffer.putLong(param1Long);
    }
    
    private void marshalPrimitive(Rational param1Rational, ByteBuffer param1ByteBuffer) {
      param1ByteBuffer.putInt(param1Rational.getNumerator());
      param1ByteBuffer.putInt(param1Rational.getDenominator());
    }
    
    private void marshalPrimitive(byte param1Byte, ByteBuffer param1ByteBuffer) {
      param1ByteBuffer.put(param1Byte);
    }
    
    private Object unmarshalObject(ByteBuffer param1ByteBuffer) {
      StringBuilder stringBuilder;
      int i = this.mNativeType;
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            if (i != 3) {
              if (i != 4) {
                if (i == 5) {
                  int j = param1ByteBuffer.getInt();
                  i = param1ByteBuffer.getInt();
                  return new Rational(j, i);
                } 
                stringBuilder = new StringBuilder();
                stringBuilder.append("Can't unmarshal native type ");
                stringBuilder.append(this.mNativeType);
                throw new UnsupportedOperationException(stringBuilder.toString());
              } 
              return Double.valueOf(stringBuilder.getDouble());
            } 
            return Long.valueOf(stringBuilder.getLong());
          } 
          return Float.valueOf(stringBuilder.getFloat());
        } 
        return Integer.valueOf(stringBuilder.getInt());
      } 
      return Byte.valueOf(stringBuilder.get());
    }
    
    public int getNativeSize() {
      return MarshalHelpers.getPrimitiveTypeSize(this.mNativeType);
    }
  }
  
  public Marshaler<T> createMarshaler(TypeReference<T> paramTypeReference, int paramInt) {
    return new MarshalerPrimitive(paramTypeReference, paramInt);
  }
  
  public boolean isTypeMappingSupported(TypeReference<T> paramTypeReference, int paramInt) {
    boolean bool = paramTypeReference.getType() instanceof Class;
    boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false, bool6 = false;
    if (bool) {
      Class<byte> clazz = (Class)paramTypeReference.getType();
      if (clazz == byte.class || clazz == Byte.class) {
        bool2 = bool5;
        if (paramInt == 0)
          bool2 = true; 
        return bool2;
      } 
      if (clazz == int.class || clazz == Integer.class) {
        bool2 = bool4;
        if (paramInt == 1)
          bool2 = true; 
        return bool2;
      } 
      if (clazz == float.class || clazz == Float.class) {
        bool2 = bool3;
        if (paramInt == 2)
          bool2 = true; 
        return bool2;
      } 
      if (clazz == long.class || clazz == Long.class) {
        if (paramInt == 3)
          bool2 = true; 
        return bool2;
      } 
      if (clazz == double.class || clazz == Double.class) {
        bool2 = bool1;
        if (paramInt == 4)
          bool2 = true; 
        return bool2;
      } 
      if (clazz == Rational.class) {
        bool2 = bool6;
        if (paramInt == 5)
          bool2 = true; 
        return bool2;
      } 
    } 
    return false;
  }
}
