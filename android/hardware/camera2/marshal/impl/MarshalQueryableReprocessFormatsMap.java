package android.hardware.camera2.marshal.impl;

import android.hardware.camera2.marshal.MarshalQueryable;
import android.hardware.camera2.marshal.Marshaler;
import android.hardware.camera2.params.ReprocessFormatsMap;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.hardware.camera2.utils.TypeReference;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

public class MarshalQueryableReprocessFormatsMap implements MarshalQueryable<ReprocessFormatsMap> {
  private class MarshalerReprocessFormatsMap extends Marshaler<ReprocessFormatsMap> {
    final MarshalQueryableReprocessFormatsMap this$0;
    
    protected MarshalerReprocessFormatsMap(TypeReference<ReprocessFormatsMap> param1TypeReference, int param1Int) {
      super(MarshalQueryableReprocessFormatsMap.this, param1TypeReference, param1Int);
    }
    
    public void marshal(ReprocessFormatsMap param1ReprocessFormatsMap, ByteBuffer param1ByteBuffer) {
      for (int i : StreamConfigurationMap.imageFormatToInternal(param1ReprocessFormatsMap.getInputs())) {
        param1ByteBuffer.putInt(i);
        int[] arrayOfInt = StreamConfigurationMap.imageFormatToInternal(param1ReprocessFormatsMap.getOutputs(i));
        param1ByteBuffer.putInt(arrayOfInt.length);
        for (int j = arrayOfInt.length; i < j; ) {
          int k = arrayOfInt[i];
          param1ByteBuffer.putInt(k);
          i++;
        } 
      } 
    }
    
    public ReprocessFormatsMap unmarshal(ByteBuffer param1ByteBuffer) {
      int i = param1ByteBuffer.remaining() / 4;
      if (param1ByteBuffer.remaining() % 4 == 0) {
        int[] arrayOfInt = new int[i];
        IntBuffer intBuffer = param1ByteBuffer.asIntBuffer();
        intBuffer.get(arrayOfInt);
        return new ReprocessFormatsMap(arrayOfInt);
      } 
      throw new AssertionError("ReprocessFormatsMap was not TYPE_INT32");
    }
    
    public int getNativeSize() {
      return NATIVE_SIZE_DYNAMIC;
    }
    
    public int calculateMarshalSize(ReprocessFormatsMap param1ReprocessFormatsMap) {
      int i = 0;
      for (int j : param1ReprocessFormatsMap.getInputs()) {
        int[] arrayOfInt = param1ReprocessFormatsMap.getOutputs(j);
        i = i + 1 + 1 + arrayOfInt.length;
      } 
      return i * 4;
    }
  }
  
  public Marshaler<ReprocessFormatsMap> createMarshaler(TypeReference<ReprocessFormatsMap> paramTypeReference, int paramInt) {
    return new MarshalerReprocessFormatsMap(paramTypeReference, paramInt);
  }
  
  public boolean isTypeMappingSupported(TypeReference<ReprocessFormatsMap> paramTypeReference, int paramInt) {
    boolean bool = true;
    if (paramInt != 1 || !paramTypeReference.getType().equals(ReprocessFormatsMap.class))
      bool = false; 
    return bool;
  }
}
