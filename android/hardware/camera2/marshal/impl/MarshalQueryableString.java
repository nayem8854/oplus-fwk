package android.hardware.camera2.marshal.impl;

import android.hardware.camera2.marshal.MarshalQueryable;
import android.hardware.camera2.marshal.Marshaler;
import android.hardware.camera2.utils.TypeReference;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class MarshalQueryableString implements MarshalQueryable<String> {
  private static final boolean DEBUG = false;
  
  private static final byte NUL = 0;
  
  private static final String TAG = MarshalQueryableString.class.getSimpleName();
  
  class PreloadHolder {
    public static final Charset UTF8_CHARSET = Charset.forName("UTF-8");
  }
  
  private class MarshalerString extends Marshaler<String> {
    final MarshalQueryableString this$0;
    
    protected MarshalerString(TypeReference<String> param1TypeReference, int param1Int) {
      super(MarshalQueryableString.this, param1TypeReference, param1Int);
    }
    
    public void marshal(String param1String, ByteBuffer param1ByteBuffer) {
      byte[] arrayOfByte = param1String.getBytes(MarshalQueryableString.PreloadHolder.UTF8_CHARSET);
      param1ByteBuffer.put(arrayOfByte);
      param1ByteBuffer.put((byte)0);
    }
    
    public int calculateMarshalSize(String param1String) {
      byte[] arrayOfByte = param1String.getBytes(MarshalQueryableString.PreloadHolder.UTF8_CHARSET);
      return arrayOfByte.length + 1;
    }
    
    public String unmarshal(ByteBuffer param1ByteBuffer) {
      boolean bool2;
      param1ByteBuffer.mark();
      boolean bool1 = false;
      byte b = 0;
      while (true) {
        bool2 = bool1;
        if (param1ByteBuffer.hasRemaining()) {
          if (param1ByteBuffer.get() == 0) {
            bool2 = true;
            break;
          } 
          b++;
          continue;
        } 
        break;
      } 
      if (bool2) {
        param1ByteBuffer.reset();
        byte[] arrayOfByte = new byte[b + 1];
        param1ByteBuffer.get(arrayOfByte, 0, b + 1);
        return new String(arrayOfByte, 0, b, MarshalQueryableString.PreloadHolder.UTF8_CHARSET);
      } 
      throw new UnsupportedOperationException("Strings must be null-terminated");
    }
    
    public int getNativeSize() {
      return NATIVE_SIZE_DYNAMIC;
    }
  }
  
  public Marshaler<String> createMarshaler(TypeReference<String> paramTypeReference, int paramInt) {
    return new MarshalerString(paramTypeReference, paramInt);
  }
  
  public boolean isTypeMappingSupported(TypeReference<String> paramTypeReference, int paramInt) {
    boolean bool;
    if (paramInt == 0 && String.class.equals(paramTypeReference.getType())) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
}
