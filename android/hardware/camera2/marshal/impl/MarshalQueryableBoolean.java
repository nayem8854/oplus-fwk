package android.hardware.camera2.marshal.impl;

import android.hardware.camera2.marshal.MarshalQueryable;
import android.hardware.camera2.marshal.Marshaler;
import android.hardware.camera2.utils.TypeReference;
import java.nio.ByteBuffer;

public class MarshalQueryableBoolean implements MarshalQueryable<Boolean> {
  private class MarshalerBoolean extends Marshaler<Boolean> {
    final MarshalQueryableBoolean this$0;
    
    protected MarshalerBoolean(TypeReference<Boolean> param1TypeReference, int param1Int) {
      super(MarshalQueryableBoolean.this, param1TypeReference, param1Int);
    }
    
    public void marshal(Boolean param1Boolean, ByteBuffer param1ByteBuffer) {
      boolean bool = param1Boolean.booleanValue();
      param1ByteBuffer.put((byte)bool);
    }
    
    public Boolean unmarshal(ByteBuffer param1ByteBuffer) {
      boolean bool;
      if (param1ByteBuffer.get() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return Boolean.valueOf(bool);
    }
    
    public int getNativeSize() {
      return 1;
    }
  }
  
  public Marshaler<Boolean> createMarshaler(TypeReference<Boolean> paramTypeReference, int paramInt) {
    return new MarshalerBoolean(paramTypeReference, paramInt);
  }
  
  public boolean isTypeMappingSupported(TypeReference<Boolean> paramTypeReference, int paramInt) {
    // Byte code:
    //   0: ldc java/lang/Boolean
    //   2: aload_1
    //   3: invokevirtual getType : ()Ljava/lang/reflect/Type;
    //   6: invokevirtual equals : (Ljava/lang/Object;)Z
    //   9: ifne -> 27
    //   12: getstatic java/lang/Boolean.TYPE : Ljava/lang/Class;
    //   15: astore_3
    //   16: aload_3
    //   17: aload_1
    //   18: invokevirtual getType : ()Ljava/lang/reflect/Type;
    //   21: invokevirtual equals : (Ljava/lang/Object;)Z
    //   24: ifeq -> 37
    //   27: iload_2
    //   28: ifne -> 37
    //   31: iconst_1
    //   32: istore #4
    //   34: goto -> 40
    //   37: iconst_0
    //   38: istore #4
    //   40: iload #4
    //   42: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #62	-> 0
    //   #63	-> 16
    //   #62	-> 40
  }
}
