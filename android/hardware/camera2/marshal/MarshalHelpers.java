package android.hardware.camera2.marshal;

import android.util.Rational;
import com.android.internal.util.Preconditions;

public final class MarshalHelpers {
  public static final int SIZEOF_BYTE = 1;
  
  public static final int SIZEOF_DOUBLE = 8;
  
  public static final int SIZEOF_FLOAT = 4;
  
  public static final int SIZEOF_INT32 = 4;
  
  public static final int SIZEOF_INT64 = 8;
  
  public static final int SIZEOF_RATIONAL = 8;
  
  public static int getPrimitiveTypeSize(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4) {
              if (paramInt == 5)
                return 8; 
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Unknown type, can't get size for ");
              stringBuilder.append(paramInt);
              throw new UnsupportedOperationException(stringBuilder.toString());
            } 
            return 8;
          } 
          return 8;
        } 
        return 4;
      } 
      return 4;
    } 
    return 1;
  }
  
  public static <T> Class<T> checkPrimitiveClass(Class<T> paramClass) {
    Preconditions.checkNotNull(paramClass, "klass must not be null");
    if (isPrimitiveClass(paramClass))
      return paramClass; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported class '");
    stringBuilder.append(paramClass);
    stringBuilder.append("'; expected a metadata primitive class");
    throw new UnsupportedOperationException(stringBuilder.toString());
  }
  
  public static <T> boolean isPrimitiveClass(Class<T> paramClass) {
    if (paramClass == null)
      return false; 
    if (paramClass == byte.class || paramClass == Byte.class)
      return true; 
    if (paramClass == int.class || paramClass == Integer.class)
      return true; 
    if (paramClass == float.class || paramClass == Float.class)
      return true; 
    if (paramClass == long.class || paramClass == Long.class)
      return true; 
    if (paramClass == double.class || paramClass == Double.class)
      return true; 
    if (paramClass == Rational.class)
      return true; 
    return false;
  }
  
  public static <T> Class<T> wrapClassIfPrimitive(Class<T> paramClass) {
    if (paramClass == byte.class)
      return (Class)Byte.class; 
    if (paramClass == int.class)
      return (Class)Integer.class; 
    if (paramClass == float.class)
      return (Class)Float.class; 
    if (paramClass == long.class)
      return (Class)Long.class; 
    if (paramClass == double.class)
      return (Class)Double.class; 
    return paramClass;
  }
  
  public static String toStringNativeType(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4) {
              if (paramInt != 5) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("UNKNOWN(");
                stringBuilder.append(paramInt);
                stringBuilder.append(")");
                return stringBuilder.toString();
              } 
              return "TYPE_RATIONAL";
            } 
            return "TYPE_DOUBLE";
          } 
          return "TYPE_INT64";
        } 
        return "TYPE_FLOAT";
      } 
      return "TYPE_INT32";
    } 
    return "TYPE_BYTE";
  }
  
  public static int checkNativeType(int paramInt) {
    if (paramInt == 0 || paramInt == 1 || paramInt == 2 || paramInt == 3 || paramInt == 4 || paramInt == 5)
      return paramInt; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown nativeType ");
    stringBuilder.append(paramInt);
    throw new UnsupportedOperationException(stringBuilder.toString());
  }
  
  public static int checkNativeTypeEquals(int paramInt1, int paramInt2) {
    if (paramInt1 == paramInt2)
      return paramInt2; 
    throw new UnsupportedOperationException(String.format("Expected native type %d, but got %d", new Object[] { Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) }));
  }
  
  private MarshalHelpers() {
    throw new AssertionError();
  }
}
