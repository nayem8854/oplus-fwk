package android.hardware.camera2.marshal;

import android.hardware.camera2.utils.TypeReference;

public interface MarshalQueryable<T> {
  Marshaler<T> createMarshaler(TypeReference<T> paramTypeReference, int paramInt);
  
  boolean isTypeMappingSupported(TypeReference<T> paramTypeReference, int paramInt);
}
