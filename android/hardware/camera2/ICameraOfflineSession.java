package android.hardware.camera2;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICameraOfflineSession extends IInterface {
  void disconnect() throws RemoteException;
  
  class Default implements ICameraOfflineSession {
    public void disconnect() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICameraOfflineSession {
    private static final String DESCRIPTOR = "android.hardware.camera2.ICameraOfflineSession";
    
    static final int TRANSACTION_disconnect = 1;
    
    public Stub() {
      attachInterface(this, "android.hardware.camera2.ICameraOfflineSession");
    }
    
    public static ICameraOfflineSession asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.camera2.ICameraOfflineSession");
      if (iInterface != null && iInterface instanceof ICameraOfflineSession)
        return (ICameraOfflineSession)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "disconnect";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.hardware.camera2.ICameraOfflineSession");
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.camera2.ICameraOfflineSession");
      disconnect();
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements ICameraOfflineSession {
      public static ICameraOfflineSession sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.camera2.ICameraOfflineSession";
      }
      
      public void disconnect() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.camera2.ICameraOfflineSession");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ICameraOfflineSession.Stub.getDefaultImpl() != null) {
            ICameraOfflineSession.Stub.getDefaultImpl().disconnect();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICameraOfflineSession param1ICameraOfflineSession) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICameraOfflineSession != null) {
          Proxy.sDefaultImpl = param1ICameraOfflineSession;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICameraOfflineSession getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
