package android.hardware.camera2;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class CaptureFailure {
  public static final int REASON_ERROR = 0;
  
  public static final int REASON_FLUSHED = 1;
  
  private final boolean mDropped;
  
  private final String mErrorPhysicalCameraId;
  
  private final long mFrameNumber;
  
  private final int mReason;
  
  private final CaptureRequest mRequest;
  
  private final int mSequenceId;
  
  public CaptureFailure(CaptureRequest paramCaptureRequest, int paramInt1, boolean paramBoolean, int paramInt2, long paramLong, String paramString) {
    this.mRequest = paramCaptureRequest;
    this.mReason = paramInt1;
    this.mDropped = paramBoolean;
    this.mSequenceId = paramInt2;
    this.mFrameNumber = paramLong;
    this.mErrorPhysicalCameraId = paramString;
  }
  
  public CaptureRequest getRequest() {
    return this.mRequest;
  }
  
  public long getFrameNumber() {
    return this.mFrameNumber;
  }
  
  public int getReason() {
    return this.mReason;
  }
  
  public boolean wasImageCaptured() {
    return this.mDropped ^ true;
  }
  
  public int getSequenceId() {
    return this.mSequenceId;
  }
  
  public String getPhysicalCameraId() {
    return this.mErrorPhysicalCameraId;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface FailureReason {}
}
