package android.hardware.camera2.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class ArrayUtils {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "ArrayUtils";
  
  public static <T> int getArrayIndex(T[] paramArrayOfT, T paramT) {
    if (paramArrayOfT == null)
      return -1; 
    byte b1 = 0;
    int i;
    byte b2;
    for (i = paramArrayOfT.length, b2 = 0; b2 < i; ) {
      T t = paramArrayOfT[b2];
      if (Objects.equals(t, paramT))
        return b1; 
      b1++;
      b2++;
    } 
    return -1;
  }
  
  public static int getArrayIndex(int[] paramArrayOfint, int paramInt) {
    if (paramArrayOfint == null)
      return -1; 
    for (byte b = 0; b < paramArrayOfint.length; b++) {
      if (paramArrayOfint[b] == paramInt)
        return b; 
    } 
    return -1;
  }
  
  public static int[] convertStringListToIntArray(List<String> paramList, String[] paramArrayOfString, int[] paramArrayOfint) {
    if (paramList == null)
      return null; 
    List<Integer> list = convertStringListToIntList(paramList, paramArrayOfString, paramArrayOfint);
    int[] arrayOfInt = new int[list.size()];
    for (byte b = 0; b < arrayOfInt.length; b++)
      arrayOfInt[b] = ((Integer)list.get(b)).intValue(); 
    return arrayOfInt;
  }
  
  public static List<Integer> convertStringListToIntList(List<String> paramList, String[] paramArrayOfString, int[] paramArrayOfint) {
    if (paramList == null)
      return null; 
    ArrayList<Integer> arrayList = new ArrayList(paramList.size());
    for (String str : paramList) {
      int i = getArrayIndex(paramArrayOfString, str);
      if (i < 0)
        continue; 
      if (i < paramArrayOfint.length)
        arrayList.add(Integer.valueOf(paramArrayOfint[i])); 
    } 
    return arrayList;
  }
  
  public static int[] toIntArray(List<Integer> paramList) {
    if (paramList == null)
      return null; 
    int[] arrayOfInt = new int[paramList.size()];
    byte b = 0;
    for (Iterator<Integer> iterator = paramList.iterator(); iterator.hasNext(); ) {
      int i = ((Integer)iterator.next()).intValue();
      arrayOfInt[b] = i;
      b++;
    } 
    return arrayOfInt;
  }
  
  public static boolean contains(int[] paramArrayOfint, int paramInt) {
    boolean bool;
    if (getArrayIndex(paramArrayOfint, paramInt) != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static <T> boolean contains(T[] paramArrayOfT, T paramT) {
    boolean bool;
    if (getArrayIndex(paramArrayOfT, paramT) != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private ArrayUtils() {
    throw new AssertionError();
  }
}
