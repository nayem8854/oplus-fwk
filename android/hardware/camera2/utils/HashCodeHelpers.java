package android.hardware.camera2.utils;

public final class HashCodeHelpers {
  public static int hashCode(int... paramVarArgs) {
    byte b = 0;
    if (paramVarArgs == null)
      return 0; 
    int i = 1;
    for (int j = paramVarArgs.length; b < j; ) {
      int k = paramVarArgs[b];
      i = (i << 5) - i ^ k;
      b++;
    } 
    return i;
  }
  
  public static int hashCode(float... paramVarArgs) {
    byte b = 0;
    if (paramVarArgs == null)
      return 0; 
    int i = 1;
    for (int j = paramVarArgs.length; b < j; ) {
      float f = paramVarArgs[b];
      int k = Float.floatToIntBits(f);
      i = (i << 5) - i ^ k;
      b++;
    } 
    return i;
  }
  
  public static <T> int hashCodeGeneric(T... paramVarArgs) {
    if (paramVarArgs == null)
      return 0; 
    int i = 1;
    int j;
    byte b;
    for (j = paramVarArgs.length, b = 0; b < j; ) {
      int k;
      T t = paramVarArgs[b];
      if (t == null) {
        k = 0;
      } else {
        k = t.hashCode();
      } 
      i = (i << 5) - i ^ k;
      b++;
    } 
    return i;
  }
}
