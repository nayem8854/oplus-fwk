package android.hardware.camera2.utils;

import java.util.List;

public class ListUtils {
  public static <T> boolean listContains(List<T> paramList, T paramT) {
    if (paramList == null)
      return false; 
    return paramList.contains(paramT);
  }
  
  public static <T> boolean listElementsEqualTo(List<T> paramList, T paramT) {
    boolean bool1 = false;
    if (paramList == null)
      return false; 
    boolean bool2 = bool1;
    if (paramList.size() == 1) {
      bool2 = bool1;
      if (paramList.contains(paramT))
        bool2 = true; 
    } 
    return bool2;
  }
  
  public static <T> String listToString(List<T> paramList) {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull -> 6
    //   4: aconst_null
    //   5: areturn
    //   6: new java/lang/StringBuilder
    //   9: dup
    //   10: invokespecial <init> : ()V
    //   13: astore_1
    //   14: aload_1
    //   15: bipush #91
    //   17: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   20: pop
    //   21: aload_0
    //   22: invokeinterface size : ()I
    //   27: istore_2
    //   28: iconst_0
    //   29: istore_3
    //   30: aload_0
    //   31: invokeinterface iterator : ()Ljava/util/Iterator;
    //   36: astore_0
    //   37: aload_0
    //   38: invokeinterface hasNext : ()Z
    //   43: ifeq -> 81
    //   46: aload_0
    //   47: invokeinterface next : ()Ljava/lang/Object;
    //   52: astore #4
    //   54: aload_1
    //   55: aload #4
    //   57: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   60: pop
    //   61: iload_3
    //   62: iload_2
    //   63: iconst_1
    //   64: isub
    //   65: if_icmpeq -> 75
    //   68: aload_1
    //   69: bipush #44
    //   71: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   74: pop
    //   75: iinc #3, 1
    //   78: goto -> 37
    //   81: aload_1
    //   82: bipush #93
    //   84: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   87: pop
    //   88: aload_1
    //   89: invokevirtual toString : ()Ljava/lang/String;
    //   92: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #54	-> 0
    //   #55	-> 4
    //   #58	-> 6
    //   #59	-> 14
    //   #61	-> 21
    //   #62	-> 28
    //   #63	-> 30
    //   #64	-> 54
    //   #66	-> 61
    //   #67	-> 68
    //   #69	-> 75
    //   #70	-> 78
    //   #71	-> 81
    //   #73	-> 88
  }
  
  public static <T> T listSelectFirstFrom(List<T> paramList, T[] paramArrayOfT) {
    if (paramList == null)
      return null; 
    int i;
    byte b;
    for (i = paramArrayOfT.length, b = 0; b < i; ) {
      T t = paramArrayOfT[b];
      if (paramList.contains(t))
        return t; 
      b++;
    } 
    return null;
  }
  
  private ListUtils() {
    throw new AssertionError();
  }
}
