package android.hardware.camera2.utils;

import com.android.internal.util.Preconditions;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executor;

public class TaskDrainer<T> {
  private final boolean DEBUG = false;
  
  private final Set<T> mTaskSet = new HashSet<>();
  
  private final Set<T> mEarlyFinishedTaskSet = new HashSet<>();
  
  private final Object mLock = new Object();
  
  private boolean mDraining = false;
  
  private boolean mDrainFinished = false;
  
  private static final String TAG = "TaskDrainer";
  
  private final Executor mExecutor;
  
  private final DrainListener mListener;
  
  private final String mName;
  
  public TaskDrainer(Executor paramExecutor, DrainListener paramDrainListener) {
    this.mExecutor = (Executor)Preconditions.checkNotNull(paramExecutor, "executor must not be null");
    this.mListener = (DrainListener)Preconditions.checkNotNull(paramDrainListener, "listener must not be null");
    this.mName = null;
  }
  
  public TaskDrainer(Executor paramExecutor, DrainListener paramDrainListener, String paramString) {
    this.mExecutor = (Executor)Preconditions.checkNotNull(paramExecutor, "executor must not be null");
    this.mListener = (DrainListener)Preconditions.checkNotNull(paramDrainListener, "listener must not be null");
    this.mName = paramString;
  }
  
  public void taskStarted(T paramT) {
    synchronized (this.mLock) {
      if (!this.mDraining) {
        if (!this.mEarlyFinishedTaskSet.remove(paramT))
          if (!this.mTaskSet.add(paramT)) {
            IllegalStateException illegalStateException1 = new IllegalStateException();
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Task ");
            stringBuilder.append(paramT);
            stringBuilder.append(" was already started");
            this(stringBuilder.toString());
            throw illegalStateException1;
          }  
        return;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Can't start more tasks after draining has begun");
      throw illegalStateException;
    } 
  }
  
  public void taskFinished(T paramT) {
    synchronized (this.mLock) {
      if (!this.mTaskSet.remove(paramT))
        if (!this.mEarlyFinishedTaskSet.add(paramT)) {
          IllegalStateException illegalStateException = new IllegalStateException();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Task ");
          stringBuilder.append(paramT);
          stringBuilder.append(" was already finished");
          this(stringBuilder.toString());
          throw illegalStateException;
        }  
      checkIfDrainFinished();
      return;
    } 
  }
  
  public void beginDrain() {
    synchronized (this.mLock) {
      if (!this.mDraining) {
        this.mDraining = true;
        checkIfDrainFinished();
      } 
      return;
    } 
  }
  
  private void checkIfDrainFinished() {
    if (this.mTaskSet.isEmpty() && this.mDraining && !this.mDrainFinished) {
      this.mDrainFinished = true;
      postDrained();
    } 
  }
  
  private void postDrained() {
    this.mExecutor.execute(new _$$Lambda$TaskDrainer$Jb53sDskEXp_qIjiikQeCRx0wJs(this));
  }
  
  public static interface DrainListener {
    void onDrained();
  }
}
