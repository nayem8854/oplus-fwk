package android.hardware.camera2.params;

import android.os.Parcel;
import android.os.Parcelable;

public final class VendorTagDescriptor implements Parcelable {
  private VendorTagDescriptor(Parcel paramParcel) {}
  
  public static final Parcelable.Creator<VendorTagDescriptor> CREATOR = new Parcelable.Creator<VendorTagDescriptor>() {
      public VendorTagDescriptor createFromParcel(Parcel param1Parcel) {
        return new VendorTagDescriptor(param1Parcel);
      }
      
      public VendorTagDescriptor[] newArray(int param1Int) {
        return new VendorTagDescriptor[param1Int];
      }
    };
  
  private static final String TAG = "VendorTagDescriptor";
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (paramParcel != null)
      return; 
    throw new IllegalArgumentException("dest must not be null");
  }
}
