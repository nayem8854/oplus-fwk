package android.hardware.camera2.params;

import android.os.Parcel;
import android.os.Parcelable;

public final class VendorTagDescriptorCache implements Parcelable {
  private VendorTagDescriptorCache(Parcel paramParcel) {}
  
  public static final Parcelable.Creator<VendorTagDescriptorCache> CREATOR = new Parcelable.Creator<VendorTagDescriptorCache>() {
      public VendorTagDescriptorCache createFromParcel(Parcel param1Parcel) {
        return new VendorTagDescriptorCache(param1Parcel);
      }
      
      public VendorTagDescriptorCache[] newArray(int param1Int) {
        return new VendorTagDescriptorCache[param1Int];
      }
    };
  
  private static final String TAG = "VendorTagDescriptorCache";
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (paramParcel != null)
      return; 
    throw new IllegalArgumentException("dest must not be null");
  }
}
