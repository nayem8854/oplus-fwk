package android.hardware.camera2.params;

import android.hardware.camera2.utils.HashCodeHelpers;

public final class InputConfiguration {
  private final int mFormat;
  
  private final int mHeight;
  
  private final int mWidth;
  
  public InputConfiguration(int paramInt1, int paramInt2, int paramInt3) {
    this.mWidth = paramInt1;
    this.mHeight = paramInt2;
    this.mFormat = paramInt3;
  }
  
  public int getWidth() {
    return this.mWidth;
  }
  
  public int getHeight() {
    return this.mHeight;
  }
  
  public int getFormat() {
    return this.mFormat;
  }
  
  public boolean equals(Object paramObject) {
    if (!(paramObject instanceof InputConfiguration))
      return false; 
    paramObject = paramObject;
    if (paramObject.getWidth() == this.mWidth && 
      paramObject.getHeight() == this.mHeight && 
      paramObject.getFormat() == this.mFormat)
      return true; 
    return false;
  }
  
  public int hashCode() {
    return HashCodeHelpers.hashCode(new int[] { this.mWidth, this.mHeight, this.mFormat });
  }
  
  public String toString() {
    return String.format("InputConfiguration(w:%d, h:%d, format:%d)", new Object[] { Integer.valueOf(this.mWidth), Integer.valueOf(this.mHeight), Integer.valueOf(this.mFormat) });
  }
}
