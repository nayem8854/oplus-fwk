package android.hardware.camera2.params;

import android.graphics.Point;
import android.graphics.Rect;

public final class Face {
  public static final int ID_UNSUPPORTED = -1;
  
  public static final int SCORE_MAX = 100;
  
  public static final int SCORE_MIN = 1;
  
  private final Rect mBounds;
  
  private final int mId;
  
  private final Point mLeftEye;
  
  private final Point mMouth;
  
  private final Point mRightEye;
  
  private final int mScore;
  
  public Face(Rect paramRect, int paramInt1, int paramInt2, Point paramPoint1, Point paramPoint2, Point paramPoint3) {
    checkNotNull("bounds", paramRect);
    if (paramInt1 >= 1 && paramInt1 <= 100) {
      if (paramInt2 >= 0 || paramInt2 == -1) {
        if (paramInt2 == -1) {
          checkNull("leftEyePosition", paramPoint1);
          checkNull("rightEyePosition", paramPoint2);
          checkNull("mouthPosition", paramPoint3);
        } 
        this.mBounds = paramRect;
        this.mScore = paramInt1;
        this.mId = paramInt2;
        this.mLeftEye = paramPoint1;
        this.mRightEye = paramPoint2;
        this.mMouth = paramPoint3;
        return;
      } 
      throw new IllegalArgumentException("Id out of range");
    } 
    throw new IllegalArgumentException("Confidence out of range");
  }
  
  public Face(Rect paramRect, int paramInt) {
    this(paramRect, paramInt, -1, null, null, null);
  }
  
  public Rect getBounds() {
    return this.mBounds;
  }
  
  public int getScore() {
    return this.mScore;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public Point getLeftEyePosition() {
    return this.mLeftEye;
  }
  
  public Point getRightEyePosition() {
    return this.mRightEye;
  }
  
  public Point getMouthPosition() {
    return this.mMouth;
  }
  
  public String toString() {
    Rect rect = this.mBounds;
    int i = this.mScore;
    int j = this.mId;
    Point point1 = this.mLeftEye, point2 = this.mRightEye, point3 = this.mMouth;
    return String.format("{ bounds: %s, score: %s, id: %d, leftEyePosition: %s, rightEyePosition: %s, mouthPosition: %s }", new Object[] { rect, Integer.valueOf(i), Integer.valueOf(j), point1, point2, point3 });
  }
  
  private static void checkNotNull(String paramString, Object paramObject) {
    if (paramObject != null)
      return; 
    paramObject = new StringBuilder();
    paramObject.append(paramString);
    paramObject.append(" was required, but it was null");
    throw new IllegalArgumentException(paramObject.toString());
  }
  
  private static void checkNull(String paramString, Object paramObject) {
    if (paramObject == null)
      return; 
    paramObject = new StringBuilder();
    paramObject.append(paramString);
    paramObject.append(" was required to be null, but it wasn't");
    throw new IllegalArgumentException(paramObject.toString());
  }
}
