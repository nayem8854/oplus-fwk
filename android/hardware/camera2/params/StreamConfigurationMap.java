package android.hardware.camera2.params;

import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.utils.HashCodeHelpers;
import android.hardware.camera2.utils.SurfaceUtils;
import android.media.ImageReader;
import android.media.MediaCodec;
import android.media.MediaRecorder;
import android.renderscript.Allocation;
import android.util.Range;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.SurfaceHolder;
import com.android.internal.util.Preconditions;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;
import java.util.Set;

public final class StreamConfigurationMap {
  public StreamConfigurationMap(StreamConfiguration[] paramArrayOfStreamConfiguration1, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration1, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration2, StreamConfiguration[] paramArrayOfStreamConfiguration2, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration3, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration4, StreamConfiguration[] paramArrayOfStreamConfiguration3, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration5, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration6, StreamConfiguration[] paramArrayOfStreamConfiguration4, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration7, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration8, HighSpeedVideoConfiguration[] paramArrayOfHighSpeedVideoConfiguration, ReprocessFormatsMap paramReprocessFormatsMap, boolean paramBoolean) {
    this(paramArrayOfStreamConfiguration1, paramArrayOfStreamConfigurationDuration1, paramArrayOfStreamConfigurationDuration2, paramArrayOfStreamConfiguration2, paramArrayOfStreamConfigurationDuration3, paramArrayOfStreamConfigurationDuration4, paramArrayOfStreamConfiguration3, paramArrayOfStreamConfigurationDuration5, paramArrayOfStreamConfigurationDuration6, paramArrayOfStreamConfiguration4, paramArrayOfStreamConfigurationDuration7, paramArrayOfStreamConfigurationDuration8, paramArrayOfHighSpeedVideoConfiguration, paramReprocessFormatsMap, paramBoolean, true);
  }
  
  public StreamConfigurationMap(StreamConfiguration[] paramArrayOfStreamConfiguration1, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration1, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration2, StreamConfiguration[] paramArrayOfStreamConfiguration2, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration3, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration4, StreamConfiguration[] paramArrayOfStreamConfiguration3, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration5, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration6, StreamConfiguration[] paramArrayOfStreamConfiguration4, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration7, StreamConfigurationDuration[] paramArrayOfStreamConfigurationDuration8, HighSpeedVideoConfiguration[] paramArrayOfHighSpeedVideoConfiguration, ReprocessFormatsMap paramReprocessFormatsMap, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramArrayOfStreamConfiguration1 != null || paramArrayOfStreamConfiguration2 != null || paramArrayOfStreamConfiguration4 != null) {
      if (paramArrayOfStreamConfiguration1 == null) {
        this.mConfigurations = new StreamConfiguration[0];
        this.mMinFrameDurations = new StreamConfigurationDuration[0];
        this.mStallDurations = new StreamConfigurationDuration[0];
      } else {
        this.mConfigurations = (StreamConfiguration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfStreamConfiguration1, "configurations");
        this.mMinFrameDurations = (StreamConfigurationDuration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfStreamConfigurationDuration1, "minFrameDurations");
        this.mStallDurations = (StreamConfigurationDuration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfStreamConfigurationDuration2, "stallDurations");
      } 
      this.mListHighResolution = paramBoolean1;
      if (paramArrayOfStreamConfiguration2 == null) {
        this.mDepthConfigurations = new StreamConfiguration[0];
        this.mDepthMinFrameDurations = new StreamConfigurationDuration[0];
        this.mDepthStallDurations = new StreamConfigurationDuration[0];
      } else {
        this.mDepthConfigurations = (StreamConfiguration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfStreamConfiguration2, "depthConfigurations");
        this.mDepthMinFrameDurations = (StreamConfigurationDuration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfStreamConfigurationDuration3, "depthMinFrameDurations");
        this.mDepthStallDurations = (StreamConfigurationDuration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfStreamConfigurationDuration4, "depthStallDurations");
      } 
      if (paramArrayOfStreamConfiguration3 == null) {
        this.mDynamicDepthConfigurations = new StreamConfiguration[0];
        this.mDynamicDepthMinFrameDurations = new StreamConfigurationDuration[0];
        this.mDynamicDepthStallDurations = new StreamConfigurationDuration[0];
      } else {
        this.mDynamicDepthConfigurations = (StreamConfiguration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfStreamConfiguration3, "dynamicDepthConfigurations");
        this.mDynamicDepthMinFrameDurations = (StreamConfigurationDuration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfStreamConfigurationDuration5, "dynamicDepthMinFrameDurations");
        this.mDynamicDepthStallDurations = (StreamConfigurationDuration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfStreamConfigurationDuration6, "dynamicDepthStallDurations");
      } 
      if (paramArrayOfStreamConfiguration4 == null) {
        this.mHeicConfigurations = new StreamConfiguration[0];
        this.mHeicMinFrameDurations = new StreamConfigurationDuration[0];
        this.mHeicStallDurations = new StreamConfigurationDuration[0];
      } else {
        this.mHeicConfigurations = (StreamConfiguration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfStreamConfiguration4, "heicConfigurations");
        this.mHeicMinFrameDurations = (StreamConfigurationDuration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfStreamConfigurationDuration7, "heicMinFrameDurations");
        this.mHeicStallDurations = (StreamConfigurationDuration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfStreamConfigurationDuration8, "heicStallDurations");
      } 
      if (paramArrayOfHighSpeedVideoConfiguration == null) {
        this.mHighSpeedVideoConfigurations = new HighSpeedVideoConfiguration[0];
      } else {
        this.mHighSpeedVideoConfigurations = (HighSpeedVideoConfiguration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfHighSpeedVideoConfiguration, "highSpeedVideoConfigurations");
      } 
      label107: for (StreamConfiguration streamConfiguration : this.mConfigurations) {
        SparseIntArray sparseIntArray;
        int i = streamConfiguration.getFormat();
        if (streamConfiguration.isOutput()) {
          sparseIntArray = this.mAllOutputFormats;
          sparseIntArray.put(i, sparseIntArray.get(i) + 1);
          if (this.mListHighResolution)
            for (StreamConfigurationDuration streamConfigurationDuration : this.mMinFrameDurations) {
              if (streamConfigurationDuration.getFormat() == i && 
                streamConfigurationDuration.getWidth() == streamConfiguration.getSize().getWidth() && 
                streamConfigurationDuration.getHeight() == streamConfiguration.getSize().getHeight()) {
                long l1 = streamConfigurationDuration.getDuration();
                continue label107;
              } 
            }  
          long l = 0L;
          if (l <= 50000000L) {
            sparseIntArray = this.mOutputFormats;
          } else {
            sparseIntArray = this.mHighResOutputFormats;
          } 
        } else {
          sparseIntArray = this.mInputFormats;
        } 
        sparseIntArray.put(i, sparseIntArray.get(i) + 1);
      } 
      for (StreamConfiguration streamConfiguration : this.mDepthConfigurations) {
        if (streamConfiguration.isOutput()) {
          SparseIntArray sparseIntArray1 = this.mDepthOutputFormats;
          int j = streamConfiguration.getFormat();
          SparseIntArray sparseIntArray2 = this.mDepthOutputFormats;
          int i = sparseIntArray2.get(streamConfiguration.getFormat());
          sparseIntArray1.put(j, i + 1);
        } 
      } 
      for (StreamConfiguration streamConfiguration : this.mDynamicDepthConfigurations) {
        if (streamConfiguration.isOutput()) {
          SparseIntArray sparseIntArray1 = this.mDynamicDepthOutputFormats;
          int i = streamConfiguration.getFormat();
          SparseIntArray sparseIntArray2 = this.mDynamicDepthOutputFormats;
          int j = sparseIntArray2.get(streamConfiguration.getFormat());
          sparseIntArray1.put(i, j + 1);
        } 
      } 
      for (StreamConfiguration streamConfiguration : this.mHeicConfigurations) {
        if (streamConfiguration.isOutput()) {
          SparseIntArray sparseIntArray2 = this.mHeicOutputFormats;
          int j = streamConfiguration.getFormat();
          SparseIntArray sparseIntArray1 = this.mHeicOutputFormats;
          int i = sparseIntArray1.get(streamConfiguration.getFormat());
          sparseIntArray2.put(j, i + 1);
        } 
      } 
      if (paramArrayOfStreamConfiguration1 != null && paramBoolean2) {
        SparseIntArray sparseIntArray = this.mOutputFormats;
        if (sparseIntArray.indexOfKey(34) < 0)
          throw new AssertionError("At least one stream configuration for IMPLEMENTATION_DEFINED must exist"); 
      } 
      for (HighSpeedVideoConfiguration highSpeedVideoConfiguration : this.mHighSpeedVideoConfigurations) {
        Size size = highSpeedVideoConfiguration.getSize();
        Range<Integer> range = highSpeedVideoConfiguration.getFpsRange();
        Integer integer2 = this.mHighSpeedVideoSizeMap.get(size);
        Integer integer1 = integer2;
        if (integer2 == null)
          integer1 = Integer.valueOf(0); 
        this.mHighSpeedVideoSizeMap.put(size, Integer.valueOf(integer1.intValue() + 1));
        integer1 = this.mHighSpeedVideoFpsRangeMap.get(range);
        if (integer1 == null)
          integer1 = Integer.valueOf(0); 
        this.mHighSpeedVideoFpsRangeMap.put(range, Integer.valueOf(integer1.intValue() + 1));
      } 
      this.mInputOutputFormatsMap = paramReprocessFormatsMap;
      return;
    } 
    throw new NullPointerException("At least one of color/depth/heic configurations must not be null");
  }
  
  public int[] getOutputFormats() {
    return getPublicFormats(true);
  }
  
  public int[] getValidOutputFormatsForInput(int paramInt) {
    ReprocessFormatsMap reprocessFormatsMap = this.mInputOutputFormatsMap;
    if (reprocessFormatsMap == null)
      return new int[0]; 
    int[] arrayOfInt = reprocessFormatsMap.getOutputs(paramInt);
    if (this.mHeicOutputFormats.size() > 0) {
      int[] arrayOfInt1 = Arrays.copyOf(arrayOfInt, arrayOfInt.length + 1);
      arrayOfInt1[arrayOfInt.length] = 1212500294;
      return arrayOfInt1;
    } 
    return arrayOfInt;
  }
  
  public int[] getInputFormats() {
    return getPublicFormats(false);
  }
  
  public Size[] getInputSizes(int paramInt) {
    return getPublicFormatSizes(paramInt, false, false);
  }
  
  public boolean isOutputSupportedFor(int paramInt) {
    checkArgumentFormat(paramInt);
    int i = imageFormatToInternal(paramInt);
    paramInt = imageFormatToDataspace(paramInt);
    boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false;
    if (paramInt == 4096) {
      bool3 = bool4;
      if (this.mDepthOutputFormats.indexOfKey(i) >= 0)
        bool3 = true; 
      return bool3;
    } 
    if (paramInt == 4098) {
      bool3 = bool1;
      if (this.mDynamicDepthOutputFormats.indexOfKey(i) >= 0)
        bool3 = true; 
      return bool3;
    } 
    if (paramInt == 4099) {
      bool3 = bool2;
      if (this.mHeicOutputFormats.indexOfKey(i) >= 0)
        bool3 = true; 
      return bool3;
    } 
    if (getFormatsMap(true).indexOfKey(i) >= 0)
      bool3 = true; 
    return bool3;
  }
  
  public static <T> boolean isOutputSupportedFor(Class<T> paramClass) {
    Objects.requireNonNull(paramClass, "klass must not be null");
    if (paramClass == ImageReader.class)
      return true; 
    if (paramClass == MediaRecorder.class)
      return true; 
    if (paramClass == MediaCodec.class)
      return true; 
    if (paramClass == Allocation.class)
      return true; 
    if (paramClass == SurfaceHolder.class)
      return true; 
    if (paramClass == SurfaceTexture.class)
      return true; 
    return false;
  }
  
  public boolean isOutputSupportedFor(Surface paramSurface) {
    StreamConfiguration[] arrayOfStreamConfiguration;
    Objects.requireNonNull(paramSurface, "surface must not be null");
    Size size = SurfaceUtils.getSurfaceSize(paramSurface);
    int i = SurfaceUtils.getSurfaceFormat(paramSurface);
    int j = SurfaceUtils.getSurfaceDataspace(paramSurface);
    boolean bool = SurfaceUtils.isFlexibleConsumer(paramSurface);
    if (j == 4096) {
      arrayOfStreamConfiguration = this.mDepthConfigurations;
    } else if (j == 4098) {
      arrayOfStreamConfiguration = this.mDynamicDepthConfigurations;
    } else if (j == 4099) {
      arrayOfStreamConfiguration = this.mHeicConfigurations;
    } else {
      arrayOfStreamConfiguration = this.mConfigurations;
    } 
    for (int k = arrayOfStreamConfiguration.length; j < k; ) {
      StreamConfiguration streamConfiguration = arrayOfStreamConfiguration[j];
      if (streamConfiguration.getFormat() == i && streamConfiguration.isOutput()) {
        if (streamConfiguration.getSize().equals(size))
          return true; 
        if (bool && 
          streamConfiguration.getSize().getWidth() <= 1920)
          return true; 
      } 
      j++;
    } 
    return false;
  }
  
  public boolean isOutputSupportedFor(Size paramSize, int paramInt) {
    StreamConfiguration[] arrayOfStreamConfiguration;
    int i = imageFormatToInternal(paramInt);
    paramInt = imageFormatToDataspace(paramInt);
    if (paramInt == 4096) {
      arrayOfStreamConfiguration = this.mDepthConfigurations;
    } else if (paramInt == 4098) {
      arrayOfStreamConfiguration = this.mDynamicDepthConfigurations;
    } else if (paramInt == 4099) {
      arrayOfStreamConfiguration = this.mHeicConfigurations;
    } else {
      arrayOfStreamConfiguration = this.mConfigurations;
    } 
    for (int j = arrayOfStreamConfiguration.length; paramInt < j; ) {
      StreamConfiguration streamConfiguration = arrayOfStreamConfiguration[paramInt];
      if (streamConfiguration.getFormat() == i && streamConfiguration.isOutput() && 
        streamConfiguration.getSize().equals(paramSize))
        return true; 
      paramInt++;
    } 
    return false;
  }
  
  public <T> Size[] getOutputSizes(Class<T> paramClass) {
    if (!isOutputSupportedFor(paramClass))
      return null; 
    return getInternalFormatSizes(34, 0, true, false);
  }
  
  public Size[] getOutputSizes(int paramInt) {
    return getPublicFormatSizes(paramInt, true, false);
  }
  
  public Size[] getHighSpeedVideoSizes() {
    Set<Size> set = this.mHighSpeedVideoSizeMap.keySet();
    return set.<Size>toArray(new Size[set.size()]);
  }
  
  public Range<Integer>[] getHighSpeedVideoFpsRangesFor(Size paramSize) {
    Integer integer = this.mHighSpeedVideoSizeMap.get(paramSize);
    byte b = 0;
    if (integer != null && integer.intValue() != 0) {
      Range[] arrayOfRange = new Range[integer.intValue()];
      int i = 0;
      HighSpeedVideoConfiguration[] arrayOfHighSpeedVideoConfiguration;
      int j;
      for (arrayOfHighSpeedVideoConfiguration = this.mHighSpeedVideoConfigurations, j = arrayOfHighSpeedVideoConfiguration.length; b < j; ) {
        HighSpeedVideoConfiguration highSpeedVideoConfiguration = arrayOfHighSpeedVideoConfiguration[b];
        int k = i;
        if (paramSize.equals(highSpeedVideoConfiguration.getSize())) {
          arrayOfRange[i] = highSpeedVideoConfiguration.getFpsRange();
          k = i + 1;
        } 
        b++;
        i = k;
      } 
      return (Range<Integer>[])arrayOfRange;
    } 
    throw new IllegalArgumentException(String.format("Size %s does not support high speed video recording", new Object[] { paramSize }));
  }
  
  public Range<Integer>[] getHighSpeedVideoFpsRanges() {
    Set<Range<Integer>> set = this.mHighSpeedVideoFpsRangeMap.keySet();
    return set.<Range<Integer>>toArray((Range<Integer>[])new Range[set.size()]);
  }
  
  public Size[] getHighSpeedVideoSizesFor(Range<Integer> paramRange) {
    Integer integer = this.mHighSpeedVideoFpsRangeMap.get(paramRange);
    byte b = 0;
    if (integer != null && integer.intValue() != 0) {
      Size[] arrayOfSize = new Size[integer.intValue()];
      int i = 0;
      HighSpeedVideoConfiguration[] arrayOfHighSpeedVideoConfiguration;
      int j;
      for (arrayOfHighSpeedVideoConfiguration = this.mHighSpeedVideoConfigurations, j = arrayOfHighSpeedVideoConfiguration.length; b < j; ) {
        HighSpeedVideoConfiguration highSpeedVideoConfiguration = arrayOfHighSpeedVideoConfiguration[b];
        int k = i;
        if (paramRange.equals(highSpeedVideoConfiguration.getFpsRange())) {
          arrayOfSize[i] = highSpeedVideoConfiguration.getSize();
          k = i + 1;
        } 
        b++;
        i = k;
      } 
      return arrayOfSize;
    } 
    throw new IllegalArgumentException(String.format("FpsRange %s does not support high speed video recording", new Object[] { paramRange }));
  }
  
  public Size[] getHighResolutionOutputSizes(int paramInt) {
    if (!this.mListHighResolution)
      return null; 
    return getPublicFormatSizes(paramInt, true, true);
  }
  
  public long getOutputMinFrameDuration(int paramInt, Size paramSize) {
    Objects.requireNonNull(paramSize, "size must not be null");
    checkArgumentFormatSupported(paramInt, true);
    int i = imageFormatToInternal(paramInt);
    paramInt = imageFormatToDataspace(paramInt);
    return getInternalFormatDuration(i, paramInt, paramSize, 0);
  }
  
  public <T> long getOutputMinFrameDuration(Class<T> paramClass, Size paramSize) {
    if (isOutputSupportedFor(paramClass))
      return getInternalFormatDuration(34, 0, paramSize, 0); 
    throw new IllegalArgumentException("klass was not supported");
  }
  
  public long getOutputStallDuration(int paramInt, Size paramSize) {
    checkArgumentFormatSupported(paramInt, true);
    int i = imageFormatToInternal(paramInt);
    paramInt = imageFormatToDataspace(paramInt);
    return getInternalFormatDuration(i, paramInt, paramSize, 1);
  }
  
  public <T> long getOutputStallDuration(Class<T> paramClass, Size paramSize) {
    if (isOutputSupportedFor(paramClass))
      return getInternalFormatDuration(34, 0, paramSize, 1); 
    throw new IllegalArgumentException("klass was not supported");
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = false;
    if (paramObject == null)
      return false; 
    if (this == paramObject)
      return true; 
    if (paramObject instanceof StreamConfigurationMap) {
      paramObject = paramObject;
      if (Arrays.equals((Object[])this.mConfigurations, (Object[])((StreamConfigurationMap)paramObject).mConfigurations)) {
        StreamConfigurationDuration[] arrayOfStreamConfigurationDuration1 = this.mMinFrameDurations, arrayOfStreamConfigurationDuration2 = ((StreamConfigurationMap)paramObject).mMinFrameDurations;
        if (Arrays.equals((Object[])arrayOfStreamConfigurationDuration1, (Object[])arrayOfStreamConfigurationDuration2)) {
          arrayOfStreamConfigurationDuration1 = this.mStallDurations;
          arrayOfStreamConfigurationDuration2 = ((StreamConfigurationMap)paramObject).mStallDurations;
          if (Arrays.equals((Object[])arrayOfStreamConfigurationDuration1, (Object[])arrayOfStreamConfigurationDuration2)) {
            StreamConfiguration[] arrayOfStreamConfiguration1 = this.mDepthConfigurations, arrayOfStreamConfiguration2 = ((StreamConfigurationMap)paramObject).mDepthConfigurations;
            if (Arrays.equals((Object[])arrayOfStreamConfiguration1, (Object[])arrayOfStreamConfiguration2)) {
              StreamConfigurationDuration[] arrayOfStreamConfigurationDuration3 = this.mDepthMinFrameDurations, arrayOfStreamConfigurationDuration4 = ((StreamConfigurationMap)paramObject).mDepthMinFrameDurations;
              if (Arrays.equals((Object[])arrayOfStreamConfigurationDuration3, (Object[])arrayOfStreamConfigurationDuration4)) {
                arrayOfStreamConfigurationDuration3 = this.mDepthStallDurations;
                arrayOfStreamConfigurationDuration4 = ((StreamConfigurationMap)paramObject).mDepthStallDurations;
                if (Arrays.equals((Object[])arrayOfStreamConfigurationDuration3, (Object[])arrayOfStreamConfigurationDuration4)) {
                  StreamConfiguration[] arrayOfStreamConfiguration3 = this.mDynamicDepthConfigurations, arrayOfStreamConfiguration4 = ((StreamConfigurationMap)paramObject).mDynamicDepthConfigurations;
                  if (Arrays.equals((Object[])arrayOfStreamConfiguration3, (Object[])arrayOfStreamConfiguration4)) {
                    StreamConfigurationDuration[] arrayOfStreamConfigurationDuration5 = this.mDynamicDepthMinFrameDurations, arrayOfStreamConfigurationDuration6 = ((StreamConfigurationMap)paramObject).mDynamicDepthMinFrameDurations;
                    if (Arrays.equals((Object[])arrayOfStreamConfigurationDuration5, (Object[])arrayOfStreamConfigurationDuration6)) {
                      arrayOfStreamConfigurationDuration5 = this.mDynamicDepthStallDurations;
                      arrayOfStreamConfigurationDuration6 = ((StreamConfigurationMap)paramObject).mDynamicDepthStallDurations;
                      if (Arrays.equals((Object[])arrayOfStreamConfigurationDuration5, (Object[])arrayOfStreamConfigurationDuration6)) {
                        StreamConfiguration[] arrayOfStreamConfiguration6 = this.mHeicConfigurations, arrayOfStreamConfiguration5 = ((StreamConfigurationMap)paramObject).mHeicConfigurations;
                        if (Arrays.equals((Object[])arrayOfStreamConfiguration6, (Object[])arrayOfStreamConfiguration5)) {
                          StreamConfigurationDuration[] arrayOfStreamConfigurationDuration7 = this.mHeicMinFrameDurations, arrayOfStreamConfigurationDuration8 = ((StreamConfigurationMap)paramObject).mHeicMinFrameDurations;
                          if (Arrays.equals((Object[])arrayOfStreamConfigurationDuration7, (Object[])arrayOfStreamConfigurationDuration8)) {
                            arrayOfStreamConfigurationDuration8 = this.mHeicStallDurations;
                            arrayOfStreamConfigurationDuration7 = ((StreamConfigurationMap)paramObject).mHeicStallDurations;
                            if (Arrays.equals((Object[])arrayOfStreamConfigurationDuration8, (Object[])arrayOfStreamConfigurationDuration7)) {
                              HighSpeedVideoConfiguration[] arrayOfHighSpeedVideoConfiguration = this.mHighSpeedVideoConfigurations;
                              paramObject = ((StreamConfigurationMap)paramObject).mHighSpeedVideoConfigurations;
                              if (Arrays.equals((Object[])arrayOfHighSpeedVideoConfiguration, (Object[])paramObject))
                                bool = true; 
                            } 
                          } 
                        } 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
      return bool;
    } 
    return false;
  }
  
  public int hashCode() {
    return HashCodeHelpers.hashCodeGeneric(new Object[][] { 
          (Object[])this.mConfigurations, (Object[])this.mMinFrameDurations, (Object[])this.mStallDurations, (Object[])this.mDepthConfigurations, (Object[])this.mDepthMinFrameDurations, (Object[])this.mDepthStallDurations, (Object[])this.mDynamicDepthConfigurations, (Object[])this.mDynamicDepthMinFrameDurations, (Object[])this.mDynamicDepthStallDurations, (Object[])this.mHeicConfigurations, 
          (Object[])this.mHeicMinFrameDurations, (Object[])this.mHeicStallDurations, (Object[])this.mHighSpeedVideoConfigurations });
  }
  
  private int checkArgumentFormatSupported(int paramInt, boolean paramBoolean) {
    checkArgumentFormat(paramInt);
    int i = imageFormatToInternal(paramInt);
    int j = imageFormatToDataspace(paramInt);
    if (paramBoolean) {
      if (j == 4096) {
        if (this.mDepthOutputFormats.indexOfKey(i) >= 0)
          return paramInt; 
      } else if (j == 4098) {
        if (this.mDynamicDepthOutputFormats.indexOfKey(i) >= 0)
          return paramInt; 
      } else if (j == 4099) {
        if (this.mHeicOutputFormats.indexOfKey(i) >= 0)
          return paramInt; 
      } else if (this.mAllOutputFormats.indexOfKey(i) >= 0) {
        return paramInt;
      } 
    } else if (this.mInputFormats.indexOfKey(i) >= 0) {
      return paramInt;
    } 
    throw new IllegalArgumentException(String.format("format %x is not supported by this stream configuration map", new Object[] { Integer.valueOf(paramInt) }));
  }
  
  static int checkArgumentFormatInternal(int paramInt) {
    if (paramInt != 33 && paramInt != 34 && paramInt != 36)
      if (paramInt != 256) {
        if (paramInt != 540422489) {
          if (paramInt != 1212500294)
            return checkArgumentFormat(paramInt); 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("An unknown internal format: ");
          stringBuilder.append(paramInt);
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("An unknown internal format: ");
        stringBuilder.append(paramInt);
        throw new IllegalArgumentException(stringBuilder.toString());
      }  
    return paramInt;
  }
  
  static int checkArgumentFormat(int paramInt) {
    if (ImageFormat.isPublicFormat(paramInt) || PixelFormat.isPublicFormat(paramInt))
      return paramInt; 
    throw new IllegalArgumentException(String.format("format 0x%x was not defined in either ImageFormat or PixelFormat", new Object[] { Integer.valueOf(paramInt) }));
  }
  
  public static int imageFormatToPublic(int paramInt) {
    if (paramInt != 33) {
      if (paramInt != 256)
        return paramInt; 
      throw new IllegalArgumentException("ImageFormat.JPEG is an unknown internal format");
    } 
    return 256;
  }
  
  public static int depthFormatToPublic(int paramInt) {
    if (paramInt != 256) {
      if (paramInt != 540422489) {
        StringBuilder stringBuilder;
        switch (paramInt) {
          default:
            stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown DATASPACE_DEPTH format ");
            stringBuilder.append(paramInt);
            throw new IllegalArgumentException(stringBuilder.toString());
          case 34:
            throw new IllegalArgumentException("IMPLEMENTATION_DEFINED must not leak to public API");
          case 33:
            return 257;
          case 32:
            break;
        } 
        return 4098;
      } 
      return 1144402265;
    } 
    throw new IllegalArgumentException("ImageFormat.JPEG is an unknown internal format");
  }
  
  static int[] imageFormatToPublic(int[] paramArrayOfint) {
    if (paramArrayOfint == null)
      return null; 
    for (byte b = 0; b < paramArrayOfint.length; b++)
      paramArrayOfint[b] = imageFormatToPublic(paramArrayOfint[b]); 
    return paramArrayOfint;
  }
  
  static int imageFormatToInternal(int paramInt) {
    if (paramInt != 256 && paramInt != 257)
      if (paramInt != 4098) {
        if (paramInt != 1144402265) {
          if (paramInt != 1212500294 && paramInt != 1768253795)
            return paramInt; 
        } else {
          return 540422489;
        } 
      } else {
        return 32;
      }  
    return 33;
  }
  
  static int imageFormatToDataspace(int paramInt) {
    if (paramInt != 256) {
      if (paramInt != 257 && paramInt != 4098 && paramInt != 1144402265) {
        if (paramInt != 1212500294) {
          if (paramInt != 1768253795)
            return 0; 
          return 4098;
        } 
        return 4099;
      } 
      return 4096;
    } 
    return 146931712;
  }
  
  public static int[] imageFormatToInternal(int[] paramArrayOfint) {
    if (paramArrayOfint == null)
      return null; 
    for (byte b = 0; b < paramArrayOfint.length; b++)
      paramArrayOfint[b] = imageFormatToInternal(paramArrayOfint[b]); 
    return paramArrayOfint;
  }
  
  private Size[] getPublicFormatSizes(int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    try {
      checkArgumentFormatSupported(paramInt, paramBoolean1);
      int i = imageFormatToInternal(paramInt);
      paramInt = imageFormatToDataspace(paramInt);
      return getInternalFormatSizes(i, paramInt, paramBoolean1, paramBoolean2);
    } catch (IllegalArgumentException illegalArgumentException) {
      return null;
    } 
  }
  
  private Size[] getInternalFormatSizes(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    SparseIntArray sparseIntArray;
    byte b = 0;
    int i = 4096;
    if (paramInt2 == 4096 && paramBoolean2)
      return new Size[0]; 
    if (!paramBoolean1) {
      sparseIntArray = this.mInputFormats;
    } else if (paramInt2 == 4096) {
      sparseIntArray = this.mDepthOutputFormats;
    } else if (paramInt2 == 4098) {
      sparseIntArray = this.mDynamicDepthOutputFormats;
    } else if (paramInt2 == 4099) {
      sparseIntArray = this.mHeicOutputFormats;
    } else if (paramBoolean2) {
      sparseIntArray = this.mHighResOutputFormats;
    } else {
      sparseIntArray = this.mOutputFormats;
    } 
    int j = sparseIntArray.get(paramInt1);
    if ((paramBoolean1 && paramInt2 != 4096 && paramInt2 != 4098 && paramInt2 != 4099) || j != 0) {
      StreamConfiguration[] arrayOfStreamConfiguration;
      StringBuilder stringBuilder;
      StreamConfigurationDuration[] arrayOfStreamConfigurationDuration;
      if (paramBoolean1 && paramInt2 != 4096 && paramInt2 != 4098 && paramInt2 != 4099) {
        sparseIntArray = this.mAllOutputFormats;
        if (sparseIntArray.get(paramInt1) == 0)
          return null; 
      } 
      Size[] arrayOfSize = new Size[j];
      byte b1 = 0;
      if (paramInt2 == 4096) {
        arrayOfStreamConfiguration = this.mDepthConfigurations;
      } else if (paramInt2 == 4098) {
        arrayOfStreamConfiguration = this.mDynamicDepthConfigurations;
      } else if (paramInt2 == 4099) {
        arrayOfStreamConfiguration = this.mHeicConfigurations;
      } else {
        arrayOfStreamConfiguration = this.mConfigurations;
      } 
      if (paramInt2 == 4096) {
        arrayOfStreamConfigurationDuration = this.mDepthMinFrameDurations;
      } else if (paramInt2 == 4098) {
        arrayOfStreamConfigurationDuration = this.mDynamicDepthMinFrameDurations;
      } else if (paramInt2 == 4099) {
        arrayOfStreamConfigurationDuration = this.mHeicMinFrameDurations;
      } else {
        arrayOfStreamConfigurationDuration = this.mMinFrameDurations;
      } 
      for (int k = arrayOfStreamConfiguration.length; b < k; ) {
        StreamConfiguration streamConfiguration = arrayOfStreamConfiguration[b];
        int m = streamConfiguration.getFormat();
        if (m == paramInt1 && streamConfiguration.isOutput() == paramBoolean1) {
          if (paramBoolean1 && this.mListHighResolution) {
            long l2, l1 = 0L;
            i = 0;
            while (true) {
              l2 = l1;
              if (i < arrayOfStreamConfigurationDuration.length) {
                StreamConfigurationDuration streamConfigurationDuration = arrayOfStreamConfigurationDuration[i];
                if (streamConfigurationDuration.getFormat() == m && 
                  streamConfigurationDuration.getWidth() == streamConfiguration.getSize().getWidth() && 
                  streamConfigurationDuration.getHeight() == streamConfiguration.getSize().getHeight()) {
                  l2 = streamConfigurationDuration.getDuration();
                  break;
                } 
                i++;
                continue;
              } 
              break;
            } 
            i = m = 4096;
            if (paramInt2 != 4096) {
              boolean bool;
              if (l2 > 50000000L) {
                bool = true;
              } else {
                bool = false;
              } 
              i = m;
              if (paramBoolean2 != bool) {
                i = m;
                continue;
              } 
            } 
          } 
          arrayOfSize[b1] = streamConfiguration.getSize();
          b1++;
        } 
        continue;
        b++;
      } 
      if (b1 != j && (paramInt2 == 4098 || paramInt2 == 4099)) {
        if (b1 <= j) {
          if (b1 <= 0) {
            Size[] arrayOfSize1 = new Size[0];
          } else {
            Size[] arrayOfSize1 = Arrays.<Size>copyOf(arrayOfSize, b1);
          } 
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Too many dynamic depth sizes (expected ");
          stringBuilder.append(j);
          stringBuilder.append(", actual ");
          stringBuilder.append(b1);
          stringBuilder.append(")");
          throw new AssertionError(stringBuilder.toString());
        } 
      } else {
        if (b1 == j)
          return arrayOfSize; 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Too few sizes (expected ");
        stringBuilder.append(j);
        stringBuilder.append(", actual ");
        stringBuilder.append(b1);
        stringBuilder.append(")");
        throw new AssertionError(stringBuilder.toString());
      } 
      return (Size[])stringBuilder;
    } 
    return null;
  }
  
  private int[] getPublicFormats(boolean paramBoolean) {
    int[] arrayOfInt = new int[getPublicFormatCount(paramBoolean)];
    byte b = 0;
    SparseIntArray sparseIntArray = getFormatsMap(paramBoolean);
    int i;
    for (i = 0; i < sparseIntArray.size(); i++, b++) {
      int j = sparseIntArray.keyAt(i);
      arrayOfInt[b] = imageFormatToPublic(j);
    } 
    i = b;
    if (paramBoolean) {
      for (i = 0; i < this.mDepthOutputFormats.size(); i++, b++)
        arrayOfInt[b] = depthFormatToPublic(this.mDepthOutputFormats.keyAt(i)); 
      int j = b;
      if (this.mDynamicDepthOutputFormats.size() > 0) {
        arrayOfInt[b] = 1768253795;
        j = b + 1;
      } 
      i = j;
      if (this.mHeicOutputFormats.size() > 0) {
        arrayOfInt[j] = 1212500294;
        i = j + 1;
      } 
    } 
    if (arrayOfInt.length == i)
      return arrayOfInt; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Too few formats ");
    stringBuilder.append(i);
    stringBuilder.append(", expected ");
    stringBuilder.append(arrayOfInt.length);
    throw new AssertionError(stringBuilder.toString());
  }
  
  private SparseIntArray getFormatsMap(boolean paramBoolean) {
    SparseIntArray sparseIntArray;
    if (paramBoolean) {
      sparseIntArray = this.mAllOutputFormats;
    } else {
      sparseIntArray = this.mInputFormats;
    } 
    return sparseIntArray;
  }
  
  private long getInternalFormatDuration(int paramInt1, int paramInt2, Size paramSize, int paramInt3) {
    if (isSupportedInternalConfiguration(paramInt1, paramInt2, paramSize)) {
      StreamConfigurationDuration[] arrayOfStreamConfigurationDuration = getDurations(paramInt3, paramInt2);
      for (paramInt3 = arrayOfStreamConfigurationDuration.length, paramInt2 = 0; paramInt2 < paramInt3; ) {
        StreamConfigurationDuration streamConfigurationDuration = arrayOfStreamConfigurationDuration[paramInt2];
        if (streamConfigurationDuration.getFormat() == paramInt1 && 
          streamConfigurationDuration.getWidth() == paramSize.getWidth() && 
          streamConfigurationDuration.getHeight() == paramSize.getHeight())
          return streamConfigurationDuration.getDuration(); 
        paramInt2++;
      } 
      return 0L;
    } 
    throw new IllegalArgumentException("size was not supported");
  }
  
  private StreamConfigurationDuration[] getDurations(int paramInt1, int paramInt2) {
    StreamConfigurationDuration[] arrayOfStreamConfigurationDuration;
    if (paramInt1 != 0) {
      if (paramInt1 == 1) {
        if (paramInt2 == 4096) {
          arrayOfStreamConfigurationDuration = this.mDepthStallDurations;
        } else if (paramInt2 == 4098) {
          arrayOfStreamConfigurationDuration = this.mDynamicDepthStallDurations;
        } else if (paramInt2 == 4099) {
          arrayOfStreamConfigurationDuration = this.mHeicStallDurations;
        } else {
          arrayOfStreamConfigurationDuration = this.mStallDurations;
        } 
        return arrayOfStreamConfigurationDuration;
      } 
      throw new IllegalArgumentException("duration was invalid");
    } 
    if (paramInt2 == 4096) {
      arrayOfStreamConfigurationDuration = this.mDepthMinFrameDurations;
    } else if (paramInt2 == 4098) {
      arrayOfStreamConfigurationDuration = this.mDynamicDepthMinFrameDurations;
    } else if (paramInt2 == 4099) {
      arrayOfStreamConfigurationDuration = this.mHeicMinFrameDurations;
    } else {
      arrayOfStreamConfigurationDuration = this.mMinFrameDurations;
    } 
    return arrayOfStreamConfigurationDuration;
  }
  
  private int getPublicFormatCount(boolean paramBoolean) {
    SparseIntArray sparseIntArray = getFormatsMap(paramBoolean);
    int i = sparseIntArray.size();
    int j = i;
    if (paramBoolean) {
      int k = this.mDepthOutputFormats.size();
      j = this.mDynamicDepthOutputFormats.size();
      j = i + k + j + this.mHeicOutputFormats.size();
    } 
    return j;
  }
  
  private static <T> boolean arrayContains(T[] paramArrayOfT, T paramT) {
    if (paramArrayOfT == null)
      return false; 
    int i;
    byte b;
    for (i = paramArrayOfT.length, b = 0; b < i; ) {
      T t = paramArrayOfT[b];
      if (Objects.equals(t, paramT))
        return true; 
      b++;
    } 
    return false;
  }
  
  private boolean isSupportedInternalConfiguration(int paramInt1, int paramInt2, Size paramSize) {
    StreamConfiguration[] arrayOfStreamConfiguration;
    if (paramInt2 == 4096) {
      arrayOfStreamConfiguration = this.mDepthConfigurations;
    } else if (paramInt2 == 4098) {
      arrayOfStreamConfiguration = this.mDynamicDepthConfigurations;
    } else if (paramInt2 == 4099) {
      arrayOfStreamConfiguration = this.mHeicConfigurations;
    } else {
      arrayOfStreamConfiguration = this.mConfigurations;
    } 
    for (paramInt2 = 0; paramInt2 < arrayOfStreamConfiguration.length; paramInt2++) {
      if (arrayOfStreamConfiguration[paramInt2].getFormat() == paramInt1) {
        StreamConfiguration streamConfiguration = arrayOfStreamConfiguration[paramInt2];
        if (streamConfiguration.getSize().equals(paramSize))
          return true; 
      } 
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("StreamConfiguration(");
    appendOutputsString(stringBuilder);
    stringBuilder.append(", ");
    appendHighResOutputsString(stringBuilder);
    stringBuilder.append(", ");
    appendInputsString(stringBuilder);
    stringBuilder.append(", ");
    appendValidOutputFormatsForInputString(stringBuilder);
    stringBuilder.append(", ");
    appendHighSpeedVideoConfigurationsString(stringBuilder);
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  private void appendOutputsString(StringBuilder paramStringBuilder) {
    paramStringBuilder.append("Outputs(");
    for (int i : getOutputFormats()) {
      for (Size size : getOutputSizes(i)) {
        long l1 = getOutputMinFrameDuration(i, size);
        long l2 = getOutputStallDuration(i, size);
        int j = size.getWidth(), k = size.getHeight();
        String str = formatToString(i);
        paramStringBuilder.append(String.format("[w:%d, h:%d, format:%s(%d), min_duration:%d, stall:%d], ", new Object[] { Integer.valueOf(j), Integer.valueOf(k), str, Integer.valueOf(i), Long.valueOf(l1), Long.valueOf(l2) }));
      } 
    } 
    if (paramStringBuilder.charAt(paramStringBuilder.length() - 1) == ' ')
      paramStringBuilder.delete(paramStringBuilder.length() - 2, paramStringBuilder.length()); 
    paramStringBuilder.append(")");
  }
  
  private void appendHighResOutputsString(StringBuilder paramStringBuilder) {
    paramStringBuilder.append("HighResolutionOutputs(");
    for (int i : getOutputFormats()) {
      Size[] arrayOfSize = getHighResolutionOutputSizes(i);
      if (arrayOfSize != null) {
        int j;
        byte b;
        for (j = arrayOfSize.length, b = 0; b < j; ) {
          Size size = arrayOfSize[b];
          long l1 = getOutputMinFrameDuration(i, size);
          long l2 = getOutputStallDuration(i, size);
          int k = size.getWidth(), m = size.getHeight();
          String str = formatToString(i);
          paramStringBuilder.append(String.format("[w:%d, h:%d, format:%s(%d), min_duration:%d, stall:%d], ", new Object[] { Integer.valueOf(k), Integer.valueOf(m), str, Integer.valueOf(i), Long.valueOf(l1), Long.valueOf(l2) }));
          b++;
        } 
      } 
    } 
    if (paramStringBuilder.charAt(paramStringBuilder.length() - 1) == ' ')
      paramStringBuilder.delete(paramStringBuilder.length() - 2, paramStringBuilder.length()); 
    paramStringBuilder.append(")");
  }
  
  private void appendInputsString(StringBuilder paramStringBuilder) {
    paramStringBuilder.append("Inputs(");
    for (int i : getInputFormats()) {
      for (Size size : getInputSizes(i)) {
        int j = size.getWidth();
        int k = size.getHeight();
        String str = formatToString(i);
        paramStringBuilder.append(String.format("[w:%d, h:%d, format:%s(%d)], ", new Object[] { Integer.valueOf(j), Integer.valueOf(k), str, Integer.valueOf(i) }));
      } 
    } 
    if (paramStringBuilder.charAt(paramStringBuilder.length() - 1) == ' ')
      paramStringBuilder.delete(paramStringBuilder.length() - 2, paramStringBuilder.length()); 
    paramStringBuilder.append(")");
  }
  
  private void appendValidOutputFormatsForInputString(StringBuilder paramStringBuilder) {
    paramStringBuilder.append("ValidOutputFormatsForInput(");
    for (int i : getInputFormats()) {
      paramStringBuilder.append(String.format("[in:%s(%d), out:", new Object[] { formatToString(i), Integer.valueOf(i) }));
      int[] arrayOfInt = getValidOutputFormatsForInput(i);
      for (i = 0; i < arrayOfInt.length; i++) {
        String str = formatToString(arrayOfInt[i]);
        int j = arrayOfInt[i];
        paramStringBuilder.append(String.format("%s(%d)", new Object[] { str, Integer.valueOf(j) }));
        if (i < arrayOfInt.length - 1)
          paramStringBuilder.append(", "); 
      } 
      paramStringBuilder.append("], ");
    } 
    if (paramStringBuilder.charAt(paramStringBuilder.length() - 1) == ' ')
      paramStringBuilder.delete(paramStringBuilder.length() - 2, paramStringBuilder.length()); 
    paramStringBuilder.append(")");
  }
  
  private void appendHighSpeedVideoConfigurationsString(StringBuilder paramStringBuilder) {
    paramStringBuilder.append("HighSpeedVideoConfigurations(");
    for (Size size : getHighSpeedVideoSizes()) {
      for (Range<Integer> range : getHighSpeedVideoFpsRangesFor(size)) {
        int i = size.getWidth();
        int j = size.getHeight();
        Comparable comparable2 = range.getLower(), comparable1 = range.getUpper();
        paramStringBuilder.append(String.format("[w:%d, h:%d, min_fps:%d, max_fps:%d], ", new Object[] { Integer.valueOf(i), Integer.valueOf(j), comparable2, comparable1 }));
      } 
    } 
    if (paramStringBuilder.charAt(paramStringBuilder.length() - 1) == ' ')
      paramStringBuilder.delete(paramStringBuilder.length() - 2, paramStringBuilder.length()); 
    paramStringBuilder.append(")");
  }
  
  private String formatToString(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 3) {
          if (paramInt != 4) {
            if (paramInt != 16) {
              if (paramInt != 17) {
                if (paramInt != 256) {
                  if (paramInt != 257) {
                    switch (paramInt) {
                      default:
                        switch (paramInt) {
                          default:
                            return "UNKNOWN";
                          case 37:
                            return "RAW10";
                          case 36:
                            return "RAW_PRIVATE";
                          case 35:
                            return "YUV_420_888";
                          case 34:
                            break;
                        } 
                        return "PRIVATE";
                      case 1768253795:
                        return "DEPTH_JPEG";
                      case 1212500294:
                        return "HEIC";
                      case 1144402265:
                        return "DEPTH16";
                      case 842094169:
                        return "YV12";
                      case 540422489:
                        return "Y16";
                      case 538982489:
                        return "Y8";
                      case 4098:
                        return "RAW_DEPTH";
                      case 32:
                        return "RAW_SENSOR";
                      case 20:
                        break;
                    } 
                    return "YUY2";
                  } 
                  return "DEPTH_POINT_CLOUD";
                } 
                return "JPEG";
              } 
              return "NV21";
            } 
            return "NV16";
          } 
          return "RGB_565";
        } 
        return "RGB_888";
      } 
      return "RGBX_8888";
    } 
    return "RGBA_8888";
  }
  
  private final SparseIntArray mOutputFormats = new SparseIntArray();
  
  private final SparseIntArray mHighResOutputFormats = new SparseIntArray();
  
  private final SparseIntArray mAllOutputFormats = new SparseIntArray();
  
  private final SparseIntArray mInputFormats = new SparseIntArray();
  
  private final SparseIntArray mDepthOutputFormats = new SparseIntArray();
  
  private final SparseIntArray mDynamicDepthOutputFormats = new SparseIntArray();
  
  private final SparseIntArray mHeicOutputFormats = new SparseIntArray();
  
  private final HashMap<Size, Integer> mHighSpeedVideoSizeMap = new HashMap<>();
  
  private final HashMap<Range<Integer>, Integer> mHighSpeedVideoFpsRangeMap = new HashMap<>();
  
  private static final long DURATION_20FPS_NS = 50000000L;
  
  private static final int DURATION_MIN_FRAME = 0;
  
  private static final int DURATION_STALL = 1;
  
  private static final int HAL_DATASPACE_DEPTH = 4096;
  
  private static final int HAL_DATASPACE_DYNAMIC_DEPTH = 4098;
  
  private static final int HAL_DATASPACE_HEIF = 4099;
  
  private static final int HAL_DATASPACE_RANGE_SHIFT = 27;
  
  private static final int HAL_DATASPACE_STANDARD_SHIFT = 16;
  
  private static final int HAL_DATASPACE_TRANSFER_SHIFT = 22;
  
  private static final int HAL_DATASPACE_UNKNOWN = 0;
  
  private static final int HAL_DATASPACE_V0_JFIF = 146931712;
  
  private static final int HAL_PIXEL_FORMAT_BLOB = 33;
  
  private static final int HAL_PIXEL_FORMAT_IMPLEMENTATION_DEFINED = 34;
  
  private static final int HAL_PIXEL_FORMAT_RAW10 = 37;
  
  private static final int HAL_PIXEL_FORMAT_RAW12 = 38;
  
  private static final int HAL_PIXEL_FORMAT_RAW16 = 32;
  
  private static final int HAL_PIXEL_FORMAT_RAW_OPAQUE = 36;
  
  private static final int HAL_PIXEL_FORMAT_Y16 = 540422489;
  
  private static final int HAL_PIXEL_FORMAT_YCbCr_420_888 = 35;
  
  private static final String TAG = "StreamConfigurationMap";
  
  private final StreamConfiguration[] mConfigurations;
  
  private final StreamConfiguration[] mDepthConfigurations;
  
  private final StreamConfigurationDuration[] mDepthMinFrameDurations;
  
  private final StreamConfigurationDuration[] mDepthStallDurations;
  
  private final StreamConfiguration[] mDynamicDepthConfigurations;
  
  private final StreamConfigurationDuration[] mDynamicDepthMinFrameDurations;
  
  private final StreamConfigurationDuration[] mDynamicDepthStallDurations;
  
  private final StreamConfiguration[] mHeicConfigurations;
  
  private final StreamConfigurationDuration[] mHeicMinFrameDurations;
  
  private final StreamConfigurationDuration[] mHeicStallDurations;
  
  private final HighSpeedVideoConfiguration[] mHighSpeedVideoConfigurations;
  
  private final ReprocessFormatsMap mInputOutputFormatsMap;
  
  private final boolean mListHighResolution;
  
  private final StreamConfigurationDuration[] mMinFrameDurations;
  
  private final StreamConfigurationDuration[] mStallDurations;
}
