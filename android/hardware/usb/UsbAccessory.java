package android.hardware.usb;

import android.app.ActivityThread;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.android.internal.util.Preconditions;
import java.util.Objects;

public class UsbAccessory implements Parcelable {
  public UsbAccessory(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, IUsbSerialReader paramIUsbSerialReader) {
    Objects.requireNonNull(paramString1);
    this.mManufacturer = paramString1;
    Objects.requireNonNull(paramString2);
    this.mModel = paramString2;
    this.mDescription = paramString3;
    this.mVersion = paramString4;
    this.mUri = paramString5;
    this.mSerialNumberReader = paramIUsbSerialReader;
    if (ActivityThread.isSystem())
      Preconditions.checkArgument(this.mSerialNumberReader instanceof IUsbSerialReader.Stub); 
  }
  
  @Deprecated
  public UsbAccessory(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6) {
    this(paramString1, paramString2, paramString3, paramString4, paramString5, (IUsbSerialReader)new Object(paramString6));
  }
  
  public String getManufacturer() {
    return this.mManufacturer;
  }
  
  public String getModel() {
    return this.mModel;
  }
  
  public String getDescription() {
    return this.mDescription;
  }
  
  public String getVersion() {
    return this.mVersion;
  }
  
  public String getUri() {
    return this.mUri;
  }
  
  public String getSerial() {
    try {
      return this.mSerialNumberReader.getSerial(ActivityThread.currentPackageName());
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return null;
    } 
  }
  
  private static boolean compare(String paramString1, String paramString2) {
    if (paramString1 == null) {
      boolean bool;
      if (paramString2 == null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
    return paramString1.equals(paramString2);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof UsbAccessory;
    boolean bool1 = false;
    if (bool) {
      paramObject = paramObject;
      if (compare(this.mManufacturer, paramObject.getManufacturer())) {
        String str = this.mModel;
        if (compare(str, paramObject.getModel())) {
          str = this.mDescription;
          if (compare(str, paramObject.getDescription())) {
            str = this.mVersion;
            if (compare(str, paramObject.getVersion())) {
              str = this.mUri;
              if (compare(str, paramObject.getUri()) && 
                compare(getSerial(), paramObject.getSerial()))
                bool1 = true; 
            } 
          } 
        } 
      } 
      return bool1;
    } 
    return false;
  }
  
  public int hashCode() {
    int m, n, i = this.mManufacturer.hashCode(), j = this.mModel.hashCode();
    String str = this.mDescription;
    int k = 0;
    if (str == null) {
      m = 0;
    } else {
      m = str.hashCode();
    } 
    str = this.mVersion;
    if (str == null) {
      n = 0;
    } else {
      n = str.hashCode();
    } 
    str = this.mUri;
    if (str != null)
      k = str.hashCode(); 
    return i ^ j ^ m ^ n ^ k;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UsbAccessory[mManufacturer=");
    stringBuilder.append(this.mManufacturer);
    stringBuilder.append(", mModel=");
    stringBuilder.append(this.mModel);
    stringBuilder.append(", mDescription=");
    stringBuilder.append(this.mDescription);
    stringBuilder.append(", mVersion=");
    stringBuilder.append(this.mVersion);
    stringBuilder.append(", mUri=");
    stringBuilder.append(this.mUri);
    stringBuilder.append(", mSerialNumberReader=");
    stringBuilder.append(this.mSerialNumberReader);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<UsbAccessory> CREATOR = new Parcelable.Creator<UsbAccessory>() {
      public UsbAccessory createFromParcel(Parcel param1Parcel) {
        String str1 = param1Parcel.readString();
        String str2 = param1Parcel.readString();
        String str3 = param1Parcel.readString();
        String str4 = param1Parcel.readString();
        String str5 = param1Parcel.readString();
        IBinder iBinder = param1Parcel.readStrongBinder();
        IUsbSerialReader iUsbSerialReader = IUsbSerialReader.Stub.asInterface(iBinder);
        return new UsbAccessory(str1, str2, str3, str4, str5, iUsbSerialReader);
      }
      
      public UsbAccessory[] newArray(int param1Int) {
        return new UsbAccessory[param1Int];
      }
    };
  
  public static final int DESCRIPTION_STRING = 2;
  
  public static final int MANUFACTURER_STRING = 0;
  
  public static final int MODEL_STRING = 1;
  
  public static final int SERIAL_STRING = 5;
  
  private static final String TAG = "UsbAccessory";
  
  public static final int URI_STRING = 4;
  
  public static final int VERSION_STRING = 3;
  
  private final String mDescription;
  
  private final String mManufacturer;
  
  private final String mModel;
  
  private final IUsbSerialReader mSerialNumberReader;
  
  private final String mUri;
  
  private final String mVersion;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mManufacturer);
    paramParcel.writeString(this.mModel);
    paramParcel.writeString(this.mDescription);
    paramParcel.writeString(this.mVersion);
    paramParcel.writeString(this.mUri);
    paramParcel.writeStrongBinder(this.mSerialNumberReader.asBinder());
  }
}
