package android.hardware.usb;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;

public class UsbConfiguration implements Parcelable {
  private static final int ATTR_REMOTE_WAKEUP = 32;
  
  private static final int ATTR_SELF_POWERED = 64;
  
  public UsbConfiguration(int paramInt1, String paramString, int paramInt2, int paramInt3) {
    this.mId = paramInt1;
    this.mName = paramString;
    this.mAttributes = paramInt2;
    this.mMaxPower = paramInt3;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public boolean isSelfPowered() {
    boolean bool;
    if ((this.mAttributes & 0x40) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isRemoteWakeup() {
    boolean bool;
    if ((this.mAttributes & 0x20) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getAttributes() {
    return this.mAttributes;
  }
  
  public int getMaxPower() {
    return this.mMaxPower * 2;
  }
  
  public int getInterfaceCount() {
    return this.mInterfaces.length;
  }
  
  public UsbInterface getInterface(int paramInt) {
    return (UsbInterface)this.mInterfaces[paramInt];
  }
  
  public void setInterfaces(Parcelable[] paramArrayOfParcelable) {
    this.mInterfaces = (Parcelable[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfParcelable, "interfaces");
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UsbConfiguration[mId=");
    stringBuilder.append(this.mId);
    stringBuilder.append(",mName=");
    stringBuilder.append(this.mName);
    stringBuilder.append(",mAttributes=");
    stringBuilder.append(this.mAttributes);
    stringBuilder.append(",mMaxPower=");
    stringBuilder.append(this.mMaxPower);
    stringBuilder.append(",mInterfaces=[");
    stringBuilder = new StringBuilder(stringBuilder.toString());
    for (byte b = 0; b < this.mInterfaces.length; b++) {
      stringBuilder.append("\n");
      stringBuilder.append(this.mInterfaces[b].toString());
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<UsbConfiguration> CREATOR = new Parcelable.Creator<UsbConfiguration>() {
      public UsbConfiguration createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        String str = param1Parcel.readString();
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        Parcelable[] arrayOfParcelable = param1Parcel.readParcelableArray(UsbInterface.class.getClassLoader());
        UsbConfiguration usbConfiguration = new UsbConfiguration(i, str, j, k);
        usbConfiguration.setInterfaces(arrayOfParcelable);
        return usbConfiguration;
      }
      
      public UsbConfiguration[] newArray(int param1Int) {
        return new UsbConfiguration[param1Int];
      }
    };
  
  private final int mAttributes;
  
  private final int mId;
  
  private Parcelable[] mInterfaces;
  
  private final int mMaxPower;
  
  private final String mName;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mId);
    paramParcel.writeString(this.mName);
    paramParcel.writeInt(this.mAttributes);
    paramParcel.writeInt(this.mMaxPower);
    paramParcel.writeParcelableArray(this.mInterfaces, 0);
  }
}
