package android.hardware.usb;

import android.util.Log;
import com.android.internal.util.Preconditions;
import dalvik.system.CloseGuard;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.util.Objects;

public class UsbRequest {
  static final int MAX_USBFS_BUFFER_SIZE = 16384;
  
  private static final String TAG = "UsbRequest";
  
  private ByteBuffer mBuffer;
  
  private Object mClientData;
  
  private final CloseGuard mCloseGuard = CloseGuard.get();
  
  private UsbDeviceConnection mConnection;
  
  private UsbEndpoint mEndpoint;
  
  private boolean mIsUsingNewQueue;
  
  private int mLength;
  
  private final Object mLock = new Object();
  
  private long mNativeContext;
  
  private ByteBuffer mTempBuffer;
  
  public boolean initialize(UsbDeviceConnection paramUsbDeviceConnection, UsbEndpoint paramUsbEndpoint) {
    this.mEndpoint = paramUsbEndpoint;
    Objects.requireNonNull(paramUsbDeviceConnection, "connection");
    this.mConnection = paramUsbDeviceConnection;
    int i = paramUsbEndpoint.getAddress();
    int j = paramUsbEndpoint.getAttributes(), k = paramUsbEndpoint.getMaxPacketSize(), m = paramUsbEndpoint.getInterval();
    boolean bool = native_init(paramUsbDeviceConnection, i, j, k, m);
    if (bool)
      this.mCloseGuard.open("close"); 
    return bool;
  }
  
  public void close() {
    if (this.mNativeContext != 0L) {
      this.mEndpoint = null;
      this.mConnection = null;
      native_close();
      this.mCloseGuard.close();
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mCloseGuard != null)
        this.mCloseGuard.warnIfOpen(); 
      close();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public UsbEndpoint getEndpoint() {
    return this.mEndpoint;
  }
  
  public Object getClientData() {
    return this.mClientData;
  }
  
  public void setClientData(Object paramObject) {
    this.mClientData = paramObject;
  }
  
  @Deprecated
  public boolean queue(ByteBuffer paramByteBuffer, int paramInt) {
    boolean bool;
    if (this.mEndpoint.getDirection() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    int i = paramInt;
    if ((this.mConnection.getContext().getApplicationInfo()).targetSdkVersion < 28) {
      i = paramInt;
      if (paramInt > 16384)
        i = 16384; 
    } 
    synchronized (this.mLock) {
      this.mBuffer = paramByteBuffer;
      this.mLength = i;
      if (paramByteBuffer.isDirect()) {
        bool = native_queue_direct(paramByteBuffer, i, bool);
      } else if (paramByteBuffer.hasArray()) {
        bool = native_queue_array(paramByteBuffer.array(), i, bool);
      } else {
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("buffer is not direct and has no array");
        throw illegalArgumentException;
      } 
      if (!bool) {
        this.mBuffer = null;
        this.mLength = 0;
      } 
      return bool;
    } 
  }
  
  public boolean queue(ByteBuffer paramByteBuffer) {
    boolean bool;
    boolean bool1;
    if (this.mNativeContext != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    Preconditions.checkState(bool, "request is not initialized");
    Preconditions.checkState(this.mIsUsingNewQueue ^ true, "this request is currently queued");
    if (this.mEndpoint.getDirection() == 0) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    synchronized (this.mLock) {
      this.mBuffer = paramByteBuffer;
      if (paramByteBuffer == null) {
        this.mIsUsingNewQueue = true;
        bool = native_queue(null, 0, 0);
      } else {
        if ((this.mConnection.getContext().getApplicationInfo()).targetSdkVersion < 28)
          Preconditions.checkArgumentInRange(paramByteBuffer.remaining(), 0, 16384, "number of remaining bytes"); 
        if (!paramByteBuffer.isReadOnly() || bool1) {
          bool = true;
        } else {
          bool = false;
        } 
        Preconditions.checkArgument(bool, "buffer can not be read-only when receiving data");
        ByteBuffer byteBuffer = paramByteBuffer;
        if (!paramByteBuffer.isDirect()) {
          this.mTempBuffer = ByteBuffer.allocateDirect(this.mBuffer.remaining());
          if (bool1) {
            this.mBuffer.mark();
            this.mTempBuffer.put(this.mBuffer);
            this.mTempBuffer.flip();
            this.mBuffer.reset();
          } 
          byteBuffer = this.mTempBuffer;
        } 
        this.mIsUsingNewQueue = true;
        bool = native_queue(byteBuffer, byteBuffer.position(), byteBuffer.remaining());
      } 
      if (!bool) {
        this.mIsUsingNewQueue = false;
        this.mTempBuffer = null;
        this.mBuffer = null;
      } 
      return bool;
    } 
  }
  
  void dequeue(boolean paramBoolean) {
    boolean bool;
    if (this.mEndpoint.getDirection() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    synchronized (this.mLock) {
      if (this.mIsUsingNewQueue) {
        int i = native_dequeue_direct();
        this.mIsUsingNewQueue = false;
        if (this.mBuffer != null)
          if (this.mTempBuffer == null) {
            this.mBuffer.position(this.mBuffer.position() + i);
          } else {
            this.mTempBuffer.limit(i);
            if (bool) {
              try {
                this.mBuffer.position(this.mBuffer.position() + i);
                this.mTempBuffer = null;
              } finally {
                this.mTempBuffer = null;
              } 
            } else {
              this.mBuffer.put(this.mTempBuffer);
              this.mTempBuffer = null;
            } 
          }  
      } else {
        int i;
        if (this.mBuffer.isDirect()) {
          i = native_dequeue_direct();
        } else {
          i = native_dequeue_array(this.mBuffer.array(), this.mLength, bool);
        } 
        if (i >= 0) {
          i = Math.min(i, this.mLength);
          try {
            this.mBuffer.position(i);
          } catch (IllegalArgumentException illegalArgumentException) {
            if (paramBoolean) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Buffer ");
              stringBuilder.append(this.mBuffer);
              stringBuilder.append(" does not have enough space to read ");
              stringBuilder.append(i);
              stringBuilder.append(" bytes");
              Log.e("UsbRequest", stringBuilder.toString(), illegalArgumentException);
              BufferOverflowException bufferOverflowException = new BufferOverflowException();
              this();
              throw bufferOverflowException;
            } 
            throw illegalArgumentException;
          } 
        } 
      } 
      this.mBuffer = null;
      this.mLength = 0;
      return;
    } 
  }
  
  public boolean cancel() {
    UsbDeviceConnection usbDeviceConnection = this.mConnection;
    if (usbDeviceConnection == null)
      return false; 
    return usbDeviceConnection.cancelRequest(this);
  }
  
  boolean cancelIfOpen() {
    if (this.mNativeContext != 0L) {
      UsbDeviceConnection usbDeviceConnection = this.mConnection;
      if (usbDeviceConnection == null || usbDeviceConnection.isOpen())
        return native_cancel(); 
    } 
    Log.w("UsbRequest", "Detected attempt to cancel a request on a connection which isn't open");
    return false;
  }
  
  private native boolean native_cancel();
  
  private native void native_close();
  
  private native int native_dequeue_array(byte[] paramArrayOfbyte, int paramInt, boolean paramBoolean);
  
  private native int native_dequeue_direct();
  
  private native boolean native_init(UsbDeviceConnection paramUsbDeviceConnection, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  private native boolean native_queue(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2);
  
  private native boolean native_queue_array(byte[] paramArrayOfbyte, int paramInt, boolean paramBoolean);
  
  private native boolean native_queue_direct(ByteBuffer paramByteBuffer, int paramInt, boolean paramBoolean);
}
