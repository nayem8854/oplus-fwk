package android.hardware.usb;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;

public class UsbInterface implements Parcelable {
  public UsbInterface(int paramInt1, int paramInt2, String paramString, int paramInt3, int paramInt4, int paramInt5) {
    this.mId = paramInt1;
    this.mAlternateSetting = paramInt2;
    this.mName = paramString;
    this.mClass = paramInt3;
    this.mSubclass = paramInt4;
    this.mProtocol = paramInt5;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public int getAlternateSetting() {
    return this.mAlternateSetting;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public int getInterfaceClass() {
    return this.mClass;
  }
  
  public int getInterfaceSubclass() {
    return this.mSubclass;
  }
  
  public int getInterfaceProtocol() {
    return this.mProtocol;
  }
  
  public int getEndpointCount() {
    return this.mEndpoints.length;
  }
  
  public UsbEndpoint getEndpoint(int paramInt) {
    return (UsbEndpoint)this.mEndpoints[paramInt];
  }
  
  public void setEndpoints(Parcelable[] paramArrayOfParcelable) {
    this.mEndpoints = (Parcelable[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfParcelable, "endpoints");
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UsbInterface[mId=");
    stringBuilder.append(this.mId);
    stringBuilder.append(",mAlternateSetting=");
    stringBuilder.append(this.mAlternateSetting);
    stringBuilder.append(",mName=");
    stringBuilder.append(this.mName);
    stringBuilder.append(",mClass=");
    stringBuilder.append(this.mClass);
    stringBuilder.append(",mSubclass=");
    stringBuilder.append(this.mSubclass);
    stringBuilder.append(",mProtocol=");
    stringBuilder.append(this.mProtocol);
    stringBuilder.append(",mEndpoints=[");
    stringBuilder = new StringBuilder(stringBuilder.toString());
    for (byte b = 0; b < this.mEndpoints.length; b++) {
      stringBuilder.append("\n");
      stringBuilder.append(this.mEndpoints[b].toString());
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<UsbInterface> CREATOR = new Parcelable.Creator<UsbInterface>() {
      public UsbInterface createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        String str = param1Parcel.readString();
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        int n = param1Parcel.readInt();
        Parcelable[] arrayOfParcelable = param1Parcel.readParcelableArray(UsbEndpoint.class.getClassLoader());
        UsbInterface usbInterface = new UsbInterface(i, j, str, k, m, n);
        usbInterface.setEndpoints(arrayOfParcelable);
        return usbInterface;
      }
      
      public UsbInterface[] newArray(int param1Int) {
        return new UsbInterface[param1Int];
      }
    };
  
  private final int mAlternateSetting;
  
  private final int mClass;
  
  private Parcelable[] mEndpoints;
  
  private final int mId;
  
  private final String mName;
  
  private final int mProtocol;
  
  private final int mSubclass;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mId);
    paramParcel.writeInt(this.mAlternateSetting);
    paramParcel.writeString(this.mName);
    paramParcel.writeInt(this.mClass);
    paramParcel.writeInt(this.mSubclass);
    paramParcel.writeInt(this.mProtocol);
    paramParcel.writeParcelableArray(this.mEndpoints, 0);
  }
}
