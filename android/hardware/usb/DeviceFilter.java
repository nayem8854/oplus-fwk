package android.hardware.usb;

import android.util.Slog;
import com.android.internal.util.dump.DualDumpOutputStream;
import java.io.IOException;
import java.util.Objects;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class DeviceFilter {
  private static final String TAG = DeviceFilter.class.getSimpleName();
  
  public final int mClass;
  
  public final String mManufacturerName;
  
  public final int mProductId;
  
  public final String mProductName;
  
  public final int mProtocol;
  
  public final String mSerialNumber;
  
  public final int mSubclass;
  
  public final int mVendorId;
  
  public DeviceFilter(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString1, String paramString2, String paramString3) {
    this.mVendorId = paramInt1;
    this.mProductId = paramInt2;
    this.mClass = paramInt3;
    this.mSubclass = paramInt4;
    this.mProtocol = paramInt5;
    this.mManufacturerName = paramString1;
    this.mProductName = paramString2;
    this.mSerialNumber = paramString3;
  }
  
  public DeviceFilter(UsbDevice paramUsbDevice) {
    this.mVendorId = paramUsbDevice.getVendorId();
    this.mProductId = paramUsbDevice.getProductId();
    this.mClass = paramUsbDevice.getDeviceClass();
    this.mSubclass = paramUsbDevice.getDeviceSubclass();
    this.mProtocol = paramUsbDevice.getDeviceProtocol();
    this.mManufacturerName = paramUsbDevice.getManufacturerName();
    this.mProductName = paramUsbDevice.getProductName();
    this.mSerialNumber = paramUsbDevice.getSerialNumber();
  }
  
  public DeviceFilter(DeviceFilter paramDeviceFilter) {
    this.mVendorId = paramDeviceFilter.mVendorId;
    this.mProductId = paramDeviceFilter.mProductId;
    this.mClass = paramDeviceFilter.mClass;
    this.mSubclass = paramDeviceFilter.mSubclass;
    this.mProtocol = paramDeviceFilter.mProtocol;
    this.mManufacturerName = paramDeviceFilter.mManufacturerName;
    this.mProductName = paramDeviceFilter.mProductName;
    this.mSerialNumber = paramDeviceFilter.mSerialNumber;
  }
  
  public static DeviceFilter read(XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    int i = paramXmlPullParser.getAttributeCount();
    String str1, str2, str3;
    int j, k, m, n, i1;
    for (byte b = 0; b < i; b++, str1 = str5) {
      String str4 = paramXmlPullParser.getAttributeName(b);
      String str5 = paramXmlPullParser.getAttributeValue(b);
      if ("manufacturer-name".equals(str4)) {
        str3 = str5;
        str5 = str1;
      } else if ("product-name".equals(str4)) {
        str2 = str5;
        str5 = str1;
      } else if (!"serial-number".equals(str4)) {
        int i2;
        if (str5 != null && str5.length() > 2 && str5.charAt(0) == '0' && (
          str5.charAt(1) == 'x' || str5.charAt(1) == 'X')) {
          str5 = str5.substring(2);
          i2 = 16;
        } else {
          i2 = 10;
        } 
        try {
          i2 = Integer.parseInt(str5, i2);
          if ("vendor-id".equals(str4)) {
            i1 = i2;
            str5 = str1;
          } else if ("product-id".equals(str4)) {
            n = i2;
            str5 = str1;
          } else if ("class".equals(str4)) {
            m = i2;
            str5 = str1;
          } else if ("subclass".equals(str4)) {
            k = i2;
            str5 = str1;
          } else if ("protocol".equals(str4)) {
            j = i2;
            str5 = str1;
          } else {
            str5 = str1;
          } 
        } catch (NumberFormatException numberFormatException) {
          String str = TAG;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("invalid number for field ");
          stringBuilder.append(str4);
          Slog.e(str, stringBuilder.toString(), numberFormatException);
          str5 = str1;
        } 
      } 
    } 
    return new DeviceFilter(i1, n, m, k, j, str3, str2, str1);
  }
  
  public void write(XmlSerializer paramXmlSerializer) throws IOException {
    paramXmlSerializer.startTag(null, "usb-device");
    int i = this.mVendorId;
    if (i != -1)
      paramXmlSerializer.attribute(null, "vendor-id", Integer.toString(i)); 
    i = this.mProductId;
    if (i != -1)
      paramXmlSerializer.attribute(null, "product-id", Integer.toString(i)); 
    i = this.mClass;
    if (i != -1)
      paramXmlSerializer.attribute(null, "class", Integer.toString(i)); 
    i = this.mSubclass;
    if (i != -1)
      paramXmlSerializer.attribute(null, "subclass", Integer.toString(i)); 
    i = this.mProtocol;
    if (i != -1)
      paramXmlSerializer.attribute(null, "protocol", Integer.toString(i)); 
    String str = this.mManufacturerName;
    if (str != null)
      paramXmlSerializer.attribute(null, "manufacturer-name", str); 
    str = this.mProductName;
    if (str != null)
      paramXmlSerializer.attribute(null, "product-name", str); 
    str = this.mSerialNumber;
    if (str != null)
      paramXmlSerializer.attribute(null, "serial-number", str); 
    paramXmlSerializer.endTag(null, "usb-device");
  }
  
  private boolean matches(int paramInt1, int paramInt2, int paramInt3) {
    int i = this.mClass;
    if (i == -1 || paramInt1 == i) {
      paramInt1 = this.mSubclass;
      if (paramInt1 == -1 || paramInt2 == paramInt1) {
        paramInt1 = this.mProtocol;
        if (paramInt1 == -1 || paramInt3 == paramInt1)
          return true; 
      } 
    } 
    return false;
  }
  
  public boolean matches(UsbDevice paramUsbDevice) {
    if (this.mVendorId != -1 && paramUsbDevice.getVendorId() != this.mVendorId)
      return false; 
    if (this.mProductId != -1 && paramUsbDevice.getProductId() != this.mProductId)
      return false; 
    if (this.mManufacturerName != null && paramUsbDevice.getManufacturerName() == null)
      return false; 
    if (this.mProductName != null && paramUsbDevice.getProductName() == null)
      return false; 
    if (this.mSerialNumber != null && paramUsbDevice.getSerialNumber() == null)
      return false; 
    if (this.mManufacturerName != null && paramUsbDevice.getManufacturerName() != null) {
      String str = this.mManufacturerName;
      if (!str.equals(paramUsbDevice.getManufacturerName()))
        return false; 
    } 
    if (this.mProductName != null && paramUsbDevice.getProductName() != null) {
      String str = this.mProductName;
      if (!str.equals(paramUsbDevice.getProductName()))
        return false; 
    } 
    if (this.mSerialNumber != null && paramUsbDevice.getSerialNumber() != null) {
      String str = this.mSerialNumber;
      if (!str.equals(paramUsbDevice.getSerialNumber()))
        return false; 
    } 
    int i = paramUsbDevice.getDeviceClass(), j = paramUsbDevice.getDeviceSubclass();
    int k = paramUsbDevice.getDeviceProtocol();
    if (matches(i, j, k))
      return true; 
    j = paramUsbDevice.getInterfaceCount();
    for (k = 0; k < j; k++) {
      UsbInterface usbInterface = paramUsbDevice.getInterface(k);
      int m = usbInterface.getInterfaceClass();
      i = usbInterface.getInterfaceSubclass();
      int n = usbInterface.getInterfaceProtocol();
      if (matches(m, i, n))
        return true; 
    } 
    return false;
  }
  
  public boolean contains(DeviceFilter paramDeviceFilter) {
    int i = this.mVendorId;
    if (i != -1 && paramDeviceFilter.mVendorId != i)
      return false; 
    i = this.mProductId;
    if (i != -1 && paramDeviceFilter.mProductId != i)
      return false; 
    String str = this.mManufacturerName;
    if (str != null && !Objects.equals(str, paramDeviceFilter.mManufacturerName))
      return false; 
    str = this.mProductName;
    if (str != null && !Objects.equals(str, paramDeviceFilter.mProductName))
      return false; 
    str = this.mSerialNumber;
    if (str != null) {
      String str1 = paramDeviceFilter.mSerialNumber;
      if (!Objects.equals(str, str1))
        return false; 
    } 
    return matches(paramDeviceFilter.mClass, paramDeviceFilter.mSubclass, paramDeviceFilter.mProtocol);
  }
  
  public boolean equals(Object paramObject) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mVendorId : I
    //   4: istore_2
    //   5: iload_2
    //   6: iconst_m1
    //   7: if_icmpeq -> 541
    //   10: aload_0
    //   11: getfield mProductId : I
    //   14: istore_3
    //   15: iload_3
    //   16: iconst_m1
    //   17: if_icmpeq -> 541
    //   20: aload_0
    //   21: getfield mClass : I
    //   24: istore #4
    //   26: iload #4
    //   28: iconst_m1
    //   29: if_icmpeq -> 541
    //   32: aload_0
    //   33: getfield mSubclass : I
    //   36: istore #5
    //   38: iload #5
    //   40: iconst_m1
    //   41: if_icmpeq -> 541
    //   44: aload_0
    //   45: getfield mProtocol : I
    //   48: istore #6
    //   50: iload #6
    //   52: iconst_m1
    //   53: if_icmpne -> 59
    //   56: goto -> 541
    //   59: aload_1
    //   60: instanceof android/hardware/usb/DeviceFilter
    //   63: ifeq -> 302
    //   66: aload_1
    //   67: checkcast android/hardware/usb/DeviceFilter
    //   70: astore_1
    //   71: aload_1
    //   72: getfield mVendorId : I
    //   75: iload_2
    //   76: if_icmpne -> 300
    //   79: aload_1
    //   80: getfield mProductId : I
    //   83: iload_3
    //   84: if_icmpne -> 300
    //   87: aload_1
    //   88: getfield mClass : I
    //   91: iload #4
    //   93: if_icmpne -> 300
    //   96: aload_1
    //   97: getfield mSubclass : I
    //   100: iload #5
    //   102: if_icmpne -> 300
    //   105: aload_1
    //   106: getfield mProtocol : I
    //   109: iload #6
    //   111: if_icmpeq -> 117
    //   114: goto -> 300
    //   117: aload_1
    //   118: getfield mManufacturerName : Ljava/lang/String;
    //   121: ifnull -> 131
    //   124: aload_0
    //   125: getfield mManufacturerName : Ljava/lang/String;
    //   128: ifnull -> 201
    //   131: aload_1
    //   132: getfield mManufacturerName : Ljava/lang/String;
    //   135: ifnonnull -> 145
    //   138: aload_0
    //   139: getfield mManufacturerName : Ljava/lang/String;
    //   142: ifnonnull -> 201
    //   145: aload_1
    //   146: getfield mProductName : Ljava/lang/String;
    //   149: ifnull -> 159
    //   152: aload_0
    //   153: getfield mProductName : Ljava/lang/String;
    //   156: ifnull -> 201
    //   159: aload_1
    //   160: getfield mProductName : Ljava/lang/String;
    //   163: ifnonnull -> 173
    //   166: aload_0
    //   167: getfield mProductName : Ljava/lang/String;
    //   170: ifnonnull -> 201
    //   173: aload_1
    //   174: getfield mSerialNumber : Ljava/lang/String;
    //   177: ifnull -> 187
    //   180: aload_0
    //   181: getfield mSerialNumber : Ljava/lang/String;
    //   184: ifnull -> 201
    //   187: aload_1
    //   188: getfield mSerialNumber : Ljava/lang/String;
    //   191: ifnonnull -> 203
    //   194: aload_0
    //   195: getfield mSerialNumber : Ljava/lang/String;
    //   198: ifnull -> 203
    //   201: iconst_0
    //   202: ireturn
    //   203: aload_1
    //   204: getfield mManufacturerName : Ljava/lang/String;
    //   207: astore #7
    //   209: aload #7
    //   211: ifnull -> 235
    //   214: aload_0
    //   215: getfield mManufacturerName : Ljava/lang/String;
    //   218: astore #8
    //   220: aload #8
    //   222: ifnull -> 235
    //   225: aload #8
    //   227: aload #7
    //   229: invokevirtual equals : (Ljava/lang/Object;)Z
    //   232: ifeq -> 296
    //   235: aload_1
    //   236: getfield mProductName : Ljava/lang/String;
    //   239: astore #7
    //   241: aload #7
    //   243: ifnull -> 267
    //   246: aload_0
    //   247: getfield mProductName : Ljava/lang/String;
    //   250: astore #8
    //   252: aload #8
    //   254: ifnull -> 267
    //   257: aload #8
    //   259: aload #7
    //   261: invokevirtual equals : (Ljava/lang/Object;)Z
    //   264: ifeq -> 296
    //   267: aload_1
    //   268: getfield mSerialNumber : Ljava/lang/String;
    //   271: astore #7
    //   273: aload #7
    //   275: ifnull -> 298
    //   278: aload_0
    //   279: getfield mSerialNumber : Ljava/lang/String;
    //   282: astore_1
    //   283: aload_1
    //   284: ifnull -> 298
    //   287: aload_1
    //   288: aload #7
    //   290: invokevirtual equals : (Ljava/lang/Object;)Z
    //   293: ifne -> 298
    //   296: iconst_0
    //   297: ireturn
    //   298: iconst_1
    //   299: ireturn
    //   300: iconst_0
    //   301: ireturn
    //   302: aload_1
    //   303: instanceof android/hardware/usb/UsbDevice
    //   306: ifeq -> 539
    //   309: aload_1
    //   310: checkcast android/hardware/usb/UsbDevice
    //   313: astore_1
    //   314: aload_1
    //   315: invokevirtual getVendorId : ()I
    //   318: aload_0
    //   319: getfield mVendorId : I
    //   322: if_icmpne -> 537
    //   325: aload_1
    //   326: invokevirtual getProductId : ()I
    //   329: aload_0
    //   330: getfield mProductId : I
    //   333: if_icmpne -> 537
    //   336: aload_1
    //   337: invokevirtual getDeviceClass : ()I
    //   340: aload_0
    //   341: getfield mClass : I
    //   344: if_icmpne -> 537
    //   347: aload_1
    //   348: invokevirtual getDeviceSubclass : ()I
    //   351: aload_0
    //   352: getfield mSubclass : I
    //   355: if_icmpne -> 537
    //   358: aload_1
    //   359: invokevirtual getDeviceProtocol : ()I
    //   362: aload_0
    //   363: getfield mProtocol : I
    //   366: if_icmpeq -> 372
    //   369: goto -> 537
    //   372: aload_0
    //   373: getfield mManufacturerName : Ljava/lang/String;
    //   376: ifnull -> 386
    //   379: aload_1
    //   380: invokevirtual getManufacturerName : ()Ljava/lang/String;
    //   383: ifnull -> 456
    //   386: aload_0
    //   387: getfield mManufacturerName : Ljava/lang/String;
    //   390: ifnonnull -> 400
    //   393: aload_1
    //   394: invokevirtual getManufacturerName : ()Ljava/lang/String;
    //   397: ifnonnull -> 456
    //   400: aload_0
    //   401: getfield mProductName : Ljava/lang/String;
    //   404: ifnull -> 414
    //   407: aload_1
    //   408: invokevirtual getProductName : ()Ljava/lang/String;
    //   411: ifnull -> 456
    //   414: aload_0
    //   415: getfield mProductName : Ljava/lang/String;
    //   418: ifnonnull -> 428
    //   421: aload_1
    //   422: invokevirtual getProductName : ()Ljava/lang/String;
    //   425: ifnonnull -> 456
    //   428: aload_0
    //   429: getfield mSerialNumber : Ljava/lang/String;
    //   432: ifnull -> 442
    //   435: aload_1
    //   436: invokevirtual getSerialNumber : ()Ljava/lang/String;
    //   439: ifnull -> 456
    //   442: aload_0
    //   443: getfield mSerialNumber : Ljava/lang/String;
    //   446: ifnonnull -> 458
    //   449: aload_1
    //   450: invokevirtual getSerialNumber : ()Ljava/lang/String;
    //   453: ifnull -> 458
    //   456: iconst_0
    //   457: ireturn
    //   458: aload_1
    //   459: invokevirtual getManufacturerName : ()Ljava/lang/String;
    //   462: ifnull -> 483
    //   465: aload_0
    //   466: getfield mManufacturerName : Ljava/lang/String;
    //   469: astore #7
    //   471: aload #7
    //   473: aload_1
    //   474: invokevirtual getManufacturerName : ()Ljava/lang/String;
    //   477: invokevirtual equals : (Ljava/lang/Object;)Z
    //   480: ifeq -> 533
    //   483: aload_1
    //   484: invokevirtual getProductName : ()Ljava/lang/String;
    //   487: ifnull -> 508
    //   490: aload_0
    //   491: getfield mProductName : Ljava/lang/String;
    //   494: astore #7
    //   496: aload #7
    //   498: aload_1
    //   499: invokevirtual getProductName : ()Ljava/lang/String;
    //   502: invokevirtual equals : (Ljava/lang/Object;)Z
    //   505: ifeq -> 533
    //   508: aload_1
    //   509: invokevirtual getSerialNumber : ()Ljava/lang/String;
    //   512: ifnull -> 535
    //   515: aload_0
    //   516: getfield mSerialNumber : Ljava/lang/String;
    //   519: astore #7
    //   521: aload #7
    //   523: aload_1
    //   524: invokevirtual getSerialNumber : ()Ljava/lang/String;
    //   527: invokevirtual equals : (Ljava/lang/Object;)Z
    //   530: ifne -> 535
    //   533: iconst_0
    //   534: ireturn
    //   535: iconst_1
    //   536: ireturn
    //   537: iconst_0
    //   538: ireturn
    //   539: iconst_0
    //   540: ireturn
    //   541: iconst_0
    //   542: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #243	-> 0
    //   #247	-> 59
    //   #248	-> 66
    //   #250	-> 71
    //   #257	-> 117
    //   #269	-> 201
    //   #271	-> 203
    //   #273	-> 225
    //   #276	-> 257
    //   #279	-> 287
    //   #280	-> 296
    //   #282	-> 298
    //   #255	-> 300
    //   #284	-> 302
    //   #285	-> 309
    //   #286	-> 314
    //   #287	-> 325
    //   #288	-> 336
    //   #289	-> 347
    //   #290	-> 358
    //   #293	-> 372
    //   #294	-> 393
    //   #295	-> 407
    //   #296	-> 421
    //   #297	-> 435
    //   #298	-> 449
    //   #299	-> 456
    //   #301	-> 458
    //   #302	-> 471
    //   #303	-> 483
    //   #304	-> 496
    //   #305	-> 508
    //   #306	-> 521
    //   #307	-> 533
    //   #309	-> 535
    //   #291	-> 537
    //   #311	-> 539
    //   #245	-> 541
  }
  
  public int hashCode() {
    return (this.mVendorId << 16 | this.mProductId) ^ (this.mClass << 16 | this.mSubclass << 8 | this.mProtocol);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DeviceFilter[mVendorId=");
    stringBuilder.append(this.mVendorId);
    stringBuilder.append(",mProductId=");
    stringBuilder.append(this.mProductId);
    stringBuilder.append(",mClass=");
    stringBuilder.append(this.mClass);
    stringBuilder.append(",mSubclass=");
    stringBuilder.append(this.mSubclass);
    stringBuilder.append(",mProtocol=");
    stringBuilder.append(this.mProtocol);
    stringBuilder.append(",mManufacturerName=");
    stringBuilder.append(this.mManufacturerName);
    stringBuilder.append(",mProductName=");
    stringBuilder.append(this.mProductName);
    stringBuilder.append(",mSerialNumber=");
    stringBuilder.append(this.mSerialNumber);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public void dump(DualDumpOutputStream paramDualDumpOutputStream, String paramString, long paramLong) {
    paramLong = paramDualDumpOutputStream.start(paramString, paramLong);
    paramDualDumpOutputStream.write("vendor_id", 1120986464257L, this.mVendorId);
    paramDualDumpOutputStream.write("product_id", 1120986464258L, this.mProductId);
    paramDualDumpOutputStream.write("class", 1120986464259L, this.mClass);
    paramDualDumpOutputStream.write("subclass", 1120986464260L, this.mSubclass);
    paramDualDumpOutputStream.write("protocol", 1120986464261L, this.mProtocol);
    paramDualDumpOutputStream.write("manufacturer_name", 1138166333446L, this.mManufacturerName);
    paramDualDumpOutputStream.write("product_name", 1138166333447L, this.mProductName);
    paramDualDumpOutputStream.write("serial_number", 1138166333448L, this.mSerialNumber);
    paramDualDumpOutputStream.end(paramLong);
  }
}
