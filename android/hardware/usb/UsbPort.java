package android.hardware.usb;

import android.annotation.SystemApi;
import com.android.internal.util.Preconditions;
import java.util.Objects;

@SystemApi
public final class UsbPort {
  private static final int NUM_DATA_ROLES = 3;
  
  private static final int POWER_ROLE_OFFSET = 0;
  
  private final String mId;
  
  private final int mSupportedContaminantProtectionModes;
  
  private final int mSupportedModes;
  
  private final boolean mSupportsEnableContaminantPresenceDetection;
  
  private final boolean mSupportsEnableContaminantPresenceProtection;
  
  private final UsbManager mUsbManager;
  
  public UsbPort(UsbManager paramUsbManager, String paramString, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    Objects.requireNonNull(paramString);
    Preconditions.checkFlagsArgument(paramInt1, 15);
    this.mUsbManager = paramUsbManager;
    this.mId = paramString;
    this.mSupportedModes = paramInt1;
    this.mSupportedContaminantProtectionModes = paramInt2;
    this.mSupportsEnableContaminantPresenceProtection = paramBoolean1;
    this.mSupportsEnableContaminantPresenceDetection = paramBoolean2;
  }
  
  public String getId() {
    return this.mId;
  }
  
  public int getSupportedModes() {
    return this.mSupportedModes;
  }
  
  public int getSupportedContaminantProtectionModes() {
    return this.mSupportedContaminantProtectionModes;
  }
  
  public boolean supportsEnableContaminantPresenceProtection() {
    return this.mSupportsEnableContaminantPresenceProtection;
  }
  
  public boolean supportsEnableContaminantPresenceDetection() {
    return this.mSupportsEnableContaminantPresenceDetection;
  }
  
  public UsbPortStatus getStatus() {
    return this.mUsbManager.getPortStatus(this);
  }
  
  public void setRoles(int paramInt1, int paramInt2) {
    checkRoles(paramInt1, paramInt2);
    this.mUsbManager.setPortRoles(this, paramInt1, paramInt2);
  }
  
  public void enableContaminantDetection(boolean paramBoolean) {
    this.mUsbManager.enableContaminantDetection(this, paramBoolean);
  }
  
  public static int combineRolesAsBit(int paramInt1, int paramInt2) {
    checkRoles(paramInt1, paramInt2);
    return 1 << (paramInt1 + 0) * 3 + paramInt2;
  }
  
  public static String modeToString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    if (paramInt == 0)
      return "none"; 
    if ((paramInt & 0x3) == 3) {
      stringBuilder.append("dual, ");
    } else if ((paramInt & 0x2) == 2) {
      stringBuilder.append("dfp, ");
    } else if ((paramInt & 0x1) == 1) {
      stringBuilder.append("ufp, ");
    } 
    if ((paramInt & 0x4) == 4)
      stringBuilder.append("audio_acc, "); 
    if ((paramInt & 0x8) == 8)
      stringBuilder.append("debug_acc, "); 
    if (stringBuilder.length() == 0)
      return Integer.toString(paramInt); 
    return stringBuilder.substring(0, stringBuilder.length() - 2);
  }
  
  public static String powerRoleToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2)
          return Integer.toString(paramInt); 
        return "sink";
      } 
      return "source";
    } 
    return "no-power";
  }
  
  public static String dataRoleToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2)
          return Integer.toString(paramInt); 
        return "device";
      } 
      return "host";
    } 
    return "no-data";
  }
  
  public static String contaminantPresenceStatusToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3)
            return Integer.toString(paramInt); 
          return "detected";
        } 
        return "not detected";
      } 
      return "disabled";
    } 
    return "not-supported";
  }
  
  public static String roleCombinationsToString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[");
    int i = 1, j = paramInt;
    paramInt = i;
    while (j != 0) {
      i = Integer.numberOfTrailingZeros(j);
      j &= 1 << i ^ 0xFFFFFFFF;
      int k = i / 3;
      if (paramInt != 0) {
        paramInt = 0;
      } else {
        stringBuilder.append(", ");
      } 
      stringBuilder.append(powerRoleToString(k + 0));
      stringBuilder.append(':');
      stringBuilder.append(dataRoleToString(i % 3));
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static void checkMode(int paramInt) {
    Preconditions.checkArgumentInRange(paramInt, 0, 3, "portMode");
  }
  
  public static void checkPowerRole(int paramInt) {
    Preconditions.checkArgumentInRange(paramInt, 0, 2, "powerRole");
  }
  
  public static void checkDataRole(int paramInt) {
    Preconditions.checkArgumentInRange(paramInt, 0, 2, "powerRole");
  }
  
  public static void checkRoles(int paramInt1, int paramInt2) {
    Preconditions.checkArgumentInRange(paramInt1, 0, 2, "powerRole");
    Preconditions.checkArgumentInRange(paramInt2, 0, 2, "dataRole");
  }
  
  public boolean isModeSupported(int paramInt) {
    if ((this.mSupportedModes & paramInt) == paramInt)
      return true; 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UsbPort{id=");
    stringBuilder.append(this.mId);
    stringBuilder.append(", supportedModes=");
    stringBuilder.append(modeToString(this.mSupportedModes));
    stringBuilder.append("supportedContaminantProtectionModes=");
    stringBuilder.append(this.mSupportedContaminantProtectionModes);
    stringBuilder.append("supportsEnableContaminantPresenceProtection=");
    stringBuilder.append(this.mSupportsEnableContaminantPresenceProtection);
    stringBuilder.append("supportsEnableContaminantPresenceDetection=");
    stringBuilder.append(this.mSupportsEnableContaminantPresenceDetection);
    return stringBuilder.toString();
  }
}
