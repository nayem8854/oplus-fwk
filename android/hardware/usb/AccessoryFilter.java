package android.hardware.usb;

import com.android.internal.util.dump.DualDumpOutputStream;
import java.io.IOException;
import java.util.Objects;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class AccessoryFilter {
  public final String mManufacturer;
  
  public final String mModel;
  
  public final String mVersion;
  
  public AccessoryFilter(String paramString1, String paramString2, String paramString3) {
    this.mManufacturer = paramString1;
    this.mModel = paramString2;
    this.mVersion = paramString3;
  }
  
  public AccessoryFilter(UsbAccessory paramUsbAccessory) {
    this.mManufacturer = paramUsbAccessory.getManufacturer();
    this.mModel = paramUsbAccessory.getModel();
    this.mVersion = paramUsbAccessory.getVersion();
  }
  
  public AccessoryFilter(AccessoryFilter paramAccessoryFilter) {
    this.mManufacturer = paramAccessoryFilter.mManufacturer;
    this.mModel = paramAccessoryFilter.mModel;
    this.mVersion = paramAccessoryFilter.mVersion;
  }
  
  public static AccessoryFilter read(XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    String str1 = null;
    String str2 = null;
    String str3 = null;
    int i = paramXmlPullParser.getAttributeCount();
    for (byte b = 0; b < i; b++, str1 = str6, str2 = str7) {
      String str6, str7, str4 = paramXmlPullParser.getAttributeName(b);
      String str5 = paramXmlPullParser.getAttributeValue(b);
      if ("manufacturer".equals(str4)) {
        str6 = str5;
        str7 = str2;
      } else if ("model".equals(str4)) {
        str6 = str1;
        str7 = str5;
      } else {
        str6 = str1;
        str7 = str2;
        if ("version".equals(str4)) {
          str3 = str5;
          str7 = str2;
          str6 = str1;
        } 
      } 
    } 
    return new AccessoryFilter(str1, str2, str3);
  }
  
  public void write(XmlSerializer paramXmlSerializer) throws IOException {
    paramXmlSerializer.startTag(null, "usb-accessory");
    String str = this.mManufacturer;
    if (str != null)
      paramXmlSerializer.attribute(null, "manufacturer", str); 
    str = this.mModel;
    if (str != null)
      paramXmlSerializer.attribute(null, "model", str); 
    str = this.mVersion;
    if (str != null)
      paramXmlSerializer.attribute(null, "version", str); 
    paramXmlSerializer.endTag(null, "usb-accessory");
  }
  
  public boolean matches(UsbAccessory paramUsbAccessory) {
    String str = this.mManufacturer;
    boolean bool = false;
    if (str != null && !paramUsbAccessory.getManufacturer().equals(this.mManufacturer))
      return false; 
    if (this.mModel != null && !paramUsbAccessory.getModel().equals(this.mModel))
      return false; 
    if (this.mVersion == null || paramUsbAccessory.getVersion().equals(this.mVersion))
      bool = true; 
    return bool;
  }
  
  public boolean contains(AccessoryFilter paramAccessoryFilter) {
    String str = this.mManufacturer;
    boolean bool = false;
    if (str != null && !Objects.equals(paramAccessoryFilter.mManufacturer, str))
      return false; 
    str = this.mModel;
    if (str != null && !Objects.equals(paramAccessoryFilter.mModel, str))
      return false; 
    str = this.mVersion;
    if (str == null || Objects.equals(paramAccessoryFilter.mVersion, str))
      bool = true; 
    return bool;
  }
  
  public boolean equals(Object paramObject) {
    String str = this.mManufacturer;
    boolean bool1 = false, bool2 = false;
    if (str == null || this.mModel == null || this.mVersion == null)
      return false; 
    if (paramObject instanceof AccessoryFilter) {
      paramObject = paramObject;
      if (str.equals(((AccessoryFilter)paramObject).mManufacturer)) {
        str = this.mModel;
        String str1 = ((AccessoryFilter)paramObject).mModel;
        if (str.equals(str1)) {
          str = this.mVersion;
          paramObject = ((AccessoryFilter)paramObject).mVersion;
          if (str.equals(paramObject))
            return true; 
        } 
      } 
      return bool2;
    } 
    if (paramObject instanceof UsbAccessory) {
      paramObject = paramObject;
      if (str.equals(paramObject.getManufacturer())) {
        str = this.mModel;
        if (str.equals(paramObject.getModel())) {
          str = this.mVersion;
          if (str.equals(paramObject.getVersion()))
            bool1 = true; 
        } 
      } 
      return bool1;
    } 
    return false;
  }
  
  public int hashCode() {
    int j, k;
    String str = this.mManufacturer;
    int i = 0;
    if (str == null) {
      j = 0;
    } else {
      j = str.hashCode();
    } 
    str = this.mModel;
    if (str == null) {
      k = 0;
    } else {
      k = str.hashCode();
    } 
    str = this.mVersion;
    if (str != null)
      i = str.hashCode(); 
    return j ^ k ^ i;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("AccessoryFilter[mManufacturer=\"");
    stringBuilder.append(this.mManufacturer);
    stringBuilder.append("\", mModel=\"");
    stringBuilder.append(this.mModel);
    stringBuilder.append("\", mVersion=\"");
    stringBuilder.append(this.mVersion);
    stringBuilder.append("\"]");
    return stringBuilder.toString();
  }
  
  public void dump(DualDumpOutputStream paramDualDumpOutputStream, String paramString, long paramLong) {
    paramLong = paramDualDumpOutputStream.start(paramString, paramLong);
    paramDualDumpOutputStream.write("manufacturer", 1138166333441L, this.mManufacturer);
    paramDualDumpOutputStream.write("model", 1138166333442L, this.mModel);
    paramDualDumpOutputStream.write("version", 1138166333443L, this.mVersion);
    paramDualDumpOutputStream.end(paramLong);
  }
}
