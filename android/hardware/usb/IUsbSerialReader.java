package android.hardware.usb;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IUsbSerialReader extends IInterface {
  String getSerial(String paramString) throws RemoteException;
  
  class Default implements IUsbSerialReader {
    public String getSerial(String param1String) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IUsbSerialReader {
    private static final String DESCRIPTOR = "android.hardware.usb.IUsbSerialReader";
    
    static final int TRANSACTION_getSerial = 1;
    
    public Stub() {
      attachInterface(this, "android.hardware.usb.IUsbSerialReader");
    }
    
    public static IUsbSerialReader asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.usb.IUsbSerialReader");
      if (iInterface != null && iInterface instanceof IUsbSerialReader)
        return (IUsbSerialReader)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "getSerial";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.hardware.usb.IUsbSerialReader");
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.usb.IUsbSerialReader");
      String str = param1Parcel1.readString();
      str = getSerial(str);
      param1Parcel2.writeNoException();
      param1Parcel2.writeString(str);
      return true;
    }
    
    private static class Proxy implements IUsbSerialReader {
      public static IUsbSerialReader sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.usb.IUsbSerialReader";
      }
      
      public String getSerial(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbSerialReader");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IUsbSerialReader.Stub.getDefaultImpl() != null) {
            param2String = IUsbSerialReader.Stub.getDefaultImpl().getSerial(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IUsbSerialReader param1IUsbSerialReader) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IUsbSerialReader != null) {
          Proxy.sDefaultImpl = param1IUsbSerialReader;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IUsbSerialReader getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
