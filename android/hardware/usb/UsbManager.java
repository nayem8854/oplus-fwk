package android.hardware.usb;

import android.annotation.SystemApi;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import android.os.RemoteException;
import android.util.Log;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

public class UsbManager {
  public static final String ACTION_USB_ACCESSORY_ATTACHED = "android.hardware.usb.action.USB_ACCESSORY_ATTACHED";
  
  public static final String ACTION_USB_ACCESSORY_DETACHED = "android.hardware.usb.action.USB_ACCESSORY_DETACHED";
  
  public static final String ACTION_USB_DEVICE_ATTACHED = "android.hardware.usb.action.USB_DEVICE_ATTACHED";
  
  public static final String ACTION_USB_DEVICE_DETACHED = "android.hardware.usb.action.USB_DEVICE_DETACHED";
  
  @SystemApi
  public static final String ACTION_USB_PORT_CHANGED = "android.hardware.usb.action.USB_PORT_CHANGED";
  
  @SystemApi
  public static final String ACTION_USB_STATE = "android.hardware.usb.action.USB_STATE";
  
  public static final String EXTRA_ACCESSORY = "accessory";
  
  public static final String EXTRA_CAN_BE_DEFAULT = "android.hardware.usb.extra.CAN_BE_DEFAULT";
  
  public static final String EXTRA_DEVICE = "device";
  
  public static final String EXTRA_PACKAGE = "android.hardware.usb.extra.PACKAGE";
  
  public static final String EXTRA_PERMISSION_GRANTED = "permission";
  
  public static final String EXTRA_PORT = "port";
  
  public static final String EXTRA_PORT_STATUS = "portStatus";
  
  @SystemApi
  public static final long FUNCTION_ACCESSORY = 2L;
  
  @SystemApi
  public static final long FUNCTION_ADB = 1L;
  
  @SystemApi
  public static final long FUNCTION_AUDIO_SOURCE = 64L;
  
  @SystemApi
  public static final long FUNCTION_MIDI = 8L;
  
  @SystemApi
  public static final long FUNCTION_MTP = 4L;
  
  private static final Map<String, Long> FUNCTION_NAME_TO_CODE;
  
  @SystemApi
  public static final long FUNCTION_NCM = 1024L;
  
  @SystemApi
  public static final long FUNCTION_NONE = 0L;
  
  @SystemApi
  public static final long FUNCTION_PTP = 16L;
  
  @SystemApi
  public static final long FUNCTION_RNDIS = 32L;
  
  private static final long SETTABLE_FUNCTIONS = 1084L;
  
  private static final String TAG = "UsbManager";
  
  @SystemApi
  public static final String USB_CONFIGURED = "configured";
  
  @SystemApi
  public static final String USB_CONNECTED = "connected";
  
  public static final String USB_DATA_UNLOCKED = "unlocked";
  
  public static final String USB_FUNCTION_ACCESSORY = "accessory";
  
  public static final String USB_FUNCTION_ADB = "adb";
  
  public static final String USB_FUNCTION_AUDIO_SOURCE = "audio_source";
  
  public static final String USB_FUNCTION_MIDI = "midi";
  
  public static final String USB_FUNCTION_MTP = "mtp";
  
  @SystemApi
  public static final String USB_FUNCTION_NCM = "ncm";
  
  public static final String USB_FUNCTION_NONE = "none";
  
  public static final String USB_FUNCTION_PTP = "ptp";
  
  @SystemApi
  public static final String USB_FUNCTION_RNDIS = "rndis";
  
  public static final String USB_HOST_CONNECTED = "host_connected";
  
  private final Context mContext;
  
  private final IUsbManager mService;
  
  static {
    HashMap<Object, Object> hashMap = new HashMap<>();
    hashMap.put("mtp", Long.valueOf(4L));
    FUNCTION_NAME_TO_CODE.put("ptp", Long.valueOf(16L));
    FUNCTION_NAME_TO_CODE.put("rndis", Long.valueOf(32L));
    FUNCTION_NAME_TO_CODE.put("midi", Long.valueOf(8L));
    FUNCTION_NAME_TO_CODE.put("accessory", Long.valueOf(2L));
    FUNCTION_NAME_TO_CODE.put("audio_source", Long.valueOf(64L));
    FUNCTION_NAME_TO_CODE.put("adb", Long.valueOf(1L));
    FUNCTION_NAME_TO_CODE.put("ncm", Long.valueOf(1024L));
  }
  
  public UsbManager(Context paramContext, IUsbManager paramIUsbManager) {
    this.mContext = paramContext;
    this.mService = paramIUsbManager;
  }
  
  public HashMap<String, UsbDevice> getDeviceList() {
    HashMap<Object, Object> hashMap = new HashMap<>();
    if (this.mService == null)
      return (HashMap)hashMap; 
    Bundle bundle = new Bundle();
    try {
      this.mService.getDeviceList(bundle);
      for (String str : bundle.keySet())
        hashMap.put(str, bundle.get(str)); 
      return (HashMap)hashMap;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public UsbDeviceConnection openDevice(UsbDevice paramUsbDevice) {
    try {
      String str = paramUsbDevice.getDeviceName();
      ParcelFileDescriptor parcelFileDescriptor = this.mService.openDevice(str, this.mContext.getPackageName());
      if (parcelFileDescriptor != null) {
        UsbDeviceConnection usbDeviceConnection = new UsbDeviceConnection();
        this(paramUsbDevice);
        boolean bool = usbDeviceConnection.open(str, parcelFileDescriptor, this.mContext);
        parcelFileDescriptor.close();
        if (bool)
          return usbDeviceConnection; 
      } 
    } catch (Exception exception) {
      Log.e("UsbManager", "exception in UsbManager.openDevice", exception);
    } 
    return null;
  }
  
  public UsbAccessory[] getAccessoryList() {
    IUsbManager iUsbManager = this.mService;
    if (iUsbManager == null)
      return null; 
    try {
      UsbAccessory usbAccessory = iUsbManager.getCurrentAccessory();
      if (usbAccessory == null)
        return null; 
      return new UsbAccessory[] { usbAccessory };
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ParcelFileDescriptor openAccessory(UsbAccessory paramUsbAccessory) {
    try {
      return this.mService.openAccessory(paramUsbAccessory);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ParcelFileDescriptor getControlFd(long paramLong) {
    try {
      return this.mService.getControlFd(paramLong);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean hasPermission(UsbDevice paramUsbDevice) {
    IUsbManager iUsbManager = this.mService;
    if (iUsbManager == null)
      return false; 
    try {
      return iUsbManager.hasDevicePermission(paramUsbDevice, this.mContext.getPackageName());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean hasPermission(UsbAccessory paramUsbAccessory) {
    IUsbManager iUsbManager = this.mService;
    if (iUsbManager == null)
      return false; 
    try {
      return iUsbManager.hasAccessoryPermission(paramUsbAccessory);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void requestPermission(UsbDevice paramUsbDevice, PendingIntent paramPendingIntent) {
    try {
      this.mService.requestDevicePermission(paramUsbDevice, this.mContext.getPackageName(), paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void requestPermission(UsbAccessory paramUsbAccessory, PendingIntent paramPendingIntent) {
    try {
      this.mService.requestAccessoryPermission(paramUsbAccessory, this.mContext.getPackageName(), paramPendingIntent);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void grantPermission(UsbDevice paramUsbDevice) {
    grantPermission(paramUsbDevice, Process.myUid());
  }
  
  public void grantPermission(UsbDevice paramUsbDevice, int paramInt) {
    try {
      this.mService.grantDevicePermission(paramUsbDevice, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void grantPermission(UsbDevice paramUsbDevice, String paramString) {
    try {
      PackageManager packageManager = this.mContext.getPackageManager();
      Context context = this.mContext;
      int i = packageManager.getPackageUidAsUser(paramString, context.getUserId());
      grantPermission(paramUsbDevice, i);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Package ");
      stringBuilder.append(paramString);
      stringBuilder.append(" not found.");
      Log.e("UsbManager", stringBuilder.toString(), (Throwable)nameNotFoundException);
    } 
  }
  
  @Deprecated
  public boolean isFunctionEnabled(String paramString) {
    try {
      return this.mService.isFunctionEnabled(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void setCurrentFunctions(long paramLong) {
    try {
      this.mService.setCurrentFunctions(paramLong);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void setCurrentFunction(String paramString, boolean paramBoolean) {
    try {
      this.mService.setCurrentFunction(paramString, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public long getCurrentFunctions() {
    try {
      return this.mService.getCurrentFunctions();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setScreenUnlockedFunctions(long paramLong) {
    try {
      this.mService.setScreenUnlockedFunctions(paramLong);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public long getScreenUnlockedFunctions() {
    try {
      return this.mService.getScreenUnlockedFunctions();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void resetUsbGadget() {
    try {
      this.mService.resetUsbGadget();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public List<UsbPort> getPorts() {
    IUsbManager iUsbManager = this.mService;
    if (iUsbManager == null)
      return Collections.emptyList(); 
    try {
      List<ParcelableUsbPort> list = iUsbManager.getPorts();
      if (list == null)
        return Collections.emptyList(); 
      int i = list.size();
      ArrayList<UsbPort> arrayList = new ArrayList(i);
      for (byte b = 0; b < i; b++)
        arrayList.add(((ParcelableUsbPort)list.get(b)).getUsbPort(this)); 
      return arrayList;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  UsbPortStatus getPortStatus(UsbPort paramUsbPort) {
    try {
      return this.mService.getPortStatus(paramUsbPort.getId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  void setPortRoles(UsbPort paramUsbPort, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("setPortRoles Package:");
    stringBuilder.append(this.mContext.getPackageName());
    Log.d("UsbManager", stringBuilder.toString());
    try {
      this.mService.setPortRoles(paramUsbPort.getId(), paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  void enableContaminantDetection(UsbPort paramUsbPort, boolean paramBoolean) {
    try {
      this.mService.enableContaminantDetection(paramUsbPort.getId(), paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setUsbDeviceConnectionHandler(ComponentName paramComponentName) {
    try {
      this.mService.setUsbDeviceConnectionHandler(paramComponentName);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean areSettableFunctions(long paramLong) {
    boolean bool = true;
    if (paramLong != 0L && ((0xFFFFFFFFFFFFFBC3L & paramLong) != 0L || 
      Long.bitCount(paramLong) != 1))
      bool = false; 
    return bool;
  }
  
  public static String usbFunctionsToString(long paramLong) {
    StringJoiner stringJoiner = new StringJoiner(",");
    if ((0x4L & paramLong) != 0L)
      stringJoiner.add("mtp"); 
    if ((0x10L & paramLong) != 0L)
      stringJoiner.add("ptp"); 
    if ((0x20L & paramLong) != 0L)
      stringJoiner.add("rndis"); 
    if ((0x8L & paramLong) != 0L)
      stringJoiner.add("midi"); 
    if ((0x2L & paramLong) != 0L)
      stringJoiner.add("accessory"); 
    if ((0x40L & paramLong) != 0L)
      stringJoiner.add("audio_source"); 
    if ((0x400L & paramLong) != 0L)
      stringJoiner.add("ncm"); 
    if ((0x1L & paramLong) != 0L)
      stringJoiner.add("adb"); 
    return stringJoiner.toString();
  }
  
  public static long usbFunctionsFromString(String paramString) {
    if (paramString == null || paramString.equals("none"))
      return 0L; 
    long l = 0L;
    for (String str : paramString.split(",")) {
      if (FUNCTION_NAME_TO_CODE.containsKey(str)) {
        l |= ((Long)FUNCTION_NAME_TO_CODE.get(str)).longValue();
      } else if (str.length() > 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid usb function ");
        stringBuilder.append(paramString);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    } 
    return l;
  }
  
  class UsbFunctionMode implements Annotation {}
}
