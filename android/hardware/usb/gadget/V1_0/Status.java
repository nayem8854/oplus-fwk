package android.hardware.usb.gadget.V1_0;

import java.util.ArrayList;

public final class Status {
  public static final int CONFIGURATION_NOT_SUPPORTED = 4;
  
  public static final int ERROR = 1;
  
  public static final int FUNCTIONS_APPLIED = 2;
  
  public static final int FUNCTIONS_NOT_APPLIED = 3;
  
  public static final int SUCCESS = 0;
  
  public static final String toString(int paramInt) {
    if (paramInt == 0)
      return "SUCCESS"; 
    if (paramInt == 1)
      return "ERROR"; 
    if (paramInt == 2)
      return "FUNCTIONS_APPLIED"; 
    if (paramInt == 3)
      return "FUNCTIONS_NOT_APPLIED"; 
    if (paramInt == 4)
      return "CONFIGURATION_NOT_SUPPORTED"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    arrayList.add("SUCCESS");
    if ((paramInt & 0x1) == 1) {
      arrayList.add("ERROR");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("FUNCTIONS_APPLIED");
      j = i | 0x2;
    } 
    i = j;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("FUNCTIONS_NOT_APPLIED");
      i = j | 0x3;
    } 
    j = i;
    if ((paramInt & 0x4) == 4) {
      arrayList.add("CONFIGURATION_NOT_SUPPORTED");
      j = i | 0x4;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
