package android.hardware.usb.gadget.V1_0;

import java.util.ArrayList;

public final class GadgetFunction {
  public static final long ACCESSORY = 2L;
  
  public static final long ADB = 1L;
  
  public static final long AUDIO_SOURCE = 64L;
  
  public static final long MIDI = 8L;
  
  public static final long MTP = 4L;
  
  public static final long NONE = 0L;
  
  public static final long PTP = 16L;
  
  public static final long RNDIS = 32L;
  
  public static final String toString(long paramLong) {
    if (paramLong == 0L)
      return "NONE"; 
    if (paramLong == 1L)
      return "ADB"; 
    if (paramLong == 2L)
      return "ACCESSORY"; 
    if (paramLong == 4L)
      return "MTP"; 
    if (paramLong == 8L)
      return "MIDI"; 
    if (paramLong == 16L)
      return "PTP"; 
    if (paramLong == 32L)
      return "RNDIS"; 
    if (paramLong == 64L)
      return "AUDIO_SOURCE"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Long.toHexString(paramLong));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(long paramLong) {
    ArrayList<String> arrayList = new ArrayList();
    long l1 = 0L;
    arrayList.add("NONE");
    if ((paramLong & 0x1L) == 1L) {
      arrayList.add("ADB");
      l1 = 0x0L | 0x1L;
    } 
    long l2 = l1;
    if ((paramLong & 0x2L) == 2L) {
      arrayList.add("ACCESSORY");
      l2 = l1 | 0x2L;
    } 
    l1 = l2;
    if ((paramLong & 0x4L) == 4L) {
      arrayList.add("MTP");
      l1 = l2 | 0x4L;
    } 
    l2 = l1;
    if ((paramLong & 0x8L) == 8L) {
      arrayList.add("MIDI");
      l2 = l1 | 0x8L;
    } 
    l1 = l2;
    if ((paramLong & 0x10L) == 16L) {
      arrayList.add("PTP");
      l1 = l2 | 0x10L;
    } 
    l2 = l1;
    if ((paramLong & 0x20L) == 32L) {
      arrayList.add("RNDIS");
      l2 = l1 | 0x20L;
    } 
    l1 = l2;
    if ((paramLong & 0x40L) == 64L) {
      arrayList.add("AUDIO_SOURCE");
      l1 = l2 | 0x40L;
    } 
    if (paramLong != l1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Long.toHexString((l1 ^ 0xFFFFFFFFFFFFFFFFL) & paramLong));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
