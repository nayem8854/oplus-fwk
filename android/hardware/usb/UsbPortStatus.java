package android.hardware.usb;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class UsbPortStatus implements Parcelable {
  public static final int CONTAMINANT_DETECTION_DETECTED = 3;
  
  public static final int CONTAMINANT_DETECTION_DISABLED = 1;
  
  public static final int CONTAMINANT_DETECTION_NOT_DETECTED = 2;
  
  public static final int CONTAMINANT_DETECTION_NOT_SUPPORTED = 0;
  
  public static final int CONTAMINANT_PROTECTION_DISABLED = 8;
  
  public static final int CONTAMINANT_PROTECTION_FORCE_DISABLE = 4;
  
  public static final int CONTAMINANT_PROTECTION_NONE = 0;
  
  public static final int CONTAMINANT_PROTECTION_SINK = 1;
  
  public static final int CONTAMINANT_PROTECTION_SOURCE = 2;
  
  public UsbPortStatus(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    this.mCurrentMode = paramInt1;
    this.mCurrentPowerRole = paramInt2;
    this.mCurrentDataRole = paramInt3;
    this.mSupportedRoleCombinations = paramInt4;
    this.mContaminantProtectionStatus = paramInt5;
    this.mContaminantDetectionStatus = paramInt6;
  }
  
  public boolean isConnected() {
    boolean bool;
    if (this.mCurrentMode != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getCurrentMode() {
    return this.mCurrentMode;
  }
  
  public int getCurrentPowerRole() {
    return this.mCurrentPowerRole;
  }
  
  public int getCurrentDataRole() {
    return this.mCurrentDataRole;
  }
  
  public boolean isRoleCombinationSupported(int paramInt1, int paramInt2) {
    boolean bool;
    int i = this.mSupportedRoleCombinations;
    if ((i & UsbPort.combineRolesAsBit(paramInt1, paramInt2)) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getSupportedRoleCombinations() {
    return this.mSupportedRoleCombinations;
  }
  
  public int getContaminantDetectionStatus() {
    return this.mContaminantDetectionStatus;
  }
  
  public int getContaminantProtectionStatus() {
    return this.mContaminantProtectionStatus;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UsbPortStatus{connected=");
    stringBuilder.append(isConnected());
    stringBuilder.append(", currentMode=");
    int i = this.mCurrentMode;
    stringBuilder.append(UsbPort.modeToString(i));
    stringBuilder.append(", currentPowerRole=");
    i = this.mCurrentPowerRole;
    stringBuilder.append(UsbPort.powerRoleToString(i));
    stringBuilder.append(", currentDataRole=");
    i = this.mCurrentDataRole;
    stringBuilder.append(UsbPort.dataRoleToString(i));
    stringBuilder.append(", supportedRoleCombinations=");
    i = this.mSupportedRoleCombinations;
    stringBuilder.append(UsbPort.roleCombinationsToString(i));
    stringBuilder.append(", contaminantDetectionStatus=");
    stringBuilder.append(getContaminantDetectionStatus());
    stringBuilder.append(", contaminantProtectionStatus=");
    stringBuilder.append(getContaminantProtectionStatus());
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mCurrentMode);
    paramParcel.writeInt(this.mCurrentPowerRole);
    paramParcel.writeInt(this.mCurrentDataRole);
    paramParcel.writeInt(this.mSupportedRoleCombinations);
    paramParcel.writeInt(this.mContaminantProtectionStatus);
    paramParcel.writeInt(this.mContaminantDetectionStatus);
  }
  
  public static final Parcelable.Creator<UsbPortStatus> CREATOR = new Parcelable.Creator<UsbPortStatus>() {
      public UsbPortStatus createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        int n = param1Parcel.readInt();
        int i1 = param1Parcel.readInt();
        return new UsbPortStatus(i, j, k, m, n, i1);
      }
      
      public UsbPortStatus[] newArray(int param1Int) {
        return new UsbPortStatus[param1Int];
      }
    };
  
  public static final int DATA_ROLE_DEVICE = 2;
  
  public static final int DATA_ROLE_HOST = 1;
  
  public static final int DATA_ROLE_NONE = 0;
  
  public static final int MODE_AUDIO_ACCESSORY = 4;
  
  public static final int MODE_DEBUG_ACCESSORY = 8;
  
  public static final int MODE_DFP = 2;
  
  public static final int MODE_DUAL = 3;
  
  public static final int MODE_NONE = 0;
  
  public static final int MODE_UFP = 1;
  
  public static final int POWER_ROLE_NONE = 0;
  
  public static final int POWER_ROLE_SINK = 2;
  
  public static final int POWER_ROLE_SOURCE = 1;
  
  private final int mContaminantDetectionStatus;
  
  private final int mContaminantProtectionStatus;
  
  private final int mCurrentDataRole;
  
  private final int mCurrentMode;
  
  private final int mCurrentPowerRole;
  
  private final int mSupportedRoleCombinations;
  
  @Retention(RetentionPolicy.SOURCE)
  class ContaminantDetectionStatus implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class ContaminantProtectionStatus implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class UsbDataRole implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class UsbPortMode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class UsbPowerRole implements Annotation {}
}
