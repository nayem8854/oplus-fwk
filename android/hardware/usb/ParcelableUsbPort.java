package android.hardware.usb;

import android.os.Parcel;
import android.os.Parcelable;

public final class ParcelableUsbPort implements Parcelable {
  private ParcelableUsbPort(String paramString, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2) {
    this.mId = paramString;
    this.mSupportedModes = paramInt1;
    this.mSupportedContaminantProtectionModes = paramInt2;
    this.mSupportsEnableContaminantPresenceProtection = paramBoolean1;
    this.mSupportsEnableContaminantPresenceDetection = paramBoolean2;
  }
  
  public static ParcelableUsbPort of(UsbPort paramUsbPort) {
    String str = paramUsbPort.getId();
    int i = paramUsbPort.getSupportedModes();
    int j = paramUsbPort.getSupportedContaminantProtectionModes();
    boolean bool = paramUsbPort.supportsEnableContaminantPresenceProtection();
    return new ParcelableUsbPort(str, i, j, bool, paramUsbPort.supportsEnableContaminantPresenceDetection());
  }
  
  public UsbPort getUsbPort(UsbManager paramUsbManager) {
    return new UsbPort(paramUsbManager, this.mId, this.mSupportedModes, this.mSupportedContaminantProtectionModes, this.mSupportsEnableContaminantPresenceProtection, this.mSupportsEnableContaminantPresenceDetection);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mId);
    paramParcel.writeInt(this.mSupportedModes);
    paramParcel.writeInt(this.mSupportedContaminantProtectionModes);
    paramParcel.writeBoolean(this.mSupportsEnableContaminantPresenceProtection);
    paramParcel.writeBoolean(this.mSupportsEnableContaminantPresenceDetection);
  }
  
  public static final Parcelable.Creator<ParcelableUsbPort> CREATOR = new Parcelable.Creator<ParcelableUsbPort>() {
      public ParcelableUsbPort createFromParcel(Parcel param1Parcel) {
        String str = param1Parcel.readString();
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        boolean bool1 = param1Parcel.readBoolean();
        boolean bool2 = param1Parcel.readBoolean();
        return new ParcelableUsbPort(str, i, j, bool1, bool2);
      }
      
      public ParcelableUsbPort[] newArray(int param1Int) {
        return new ParcelableUsbPort[param1Int];
      }
    };
  
  private final String mId;
  
  private final int mSupportedContaminantProtectionModes;
  
  private final int mSupportedModes;
  
  private final boolean mSupportsEnableContaminantPresenceDetection;
  
  private final boolean mSupportsEnableContaminantPresenceProtection;
}
