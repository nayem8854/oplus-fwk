package android.hardware.usb;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.ParcelFileDescriptor;
import com.android.internal.util.Preconditions;
import dalvik.system.CloseGuard;
import java.io.FileDescriptor;
import java.util.concurrent.TimeoutException;

public class UsbDeviceConnection {
  private static final String TAG = "UsbDeviceConnection";
  
  private final CloseGuard mCloseGuard = CloseGuard.get();
  
  private Context mContext;
  
  private final UsbDevice mDevice;
  
  private final Object mLock = new Object();
  
  private long mNativeContext;
  
  public UsbDeviceConnection(UsbDevice paramUsbDevice) {
    this.mDevice = paramUsbDevice;
  }
  
  boolean open(String paramString, ParcelFileDescriptor paramParcelFileDescriptor, Context paramContext) {
    this.mContext = paramContext.getApplicationContext();
    synchronized (this.mLock) {
      boolean bool = native_open(paramString, paramParcelFileDescriptor.getFileDescriptor());
      if (bool)
        this.mCloseGuard.open("close"); 
      return bool;
    } 
  }
  
  boolean isOpen() {
    boolean bool;
    if (this.mNativeContext != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  boolean cancelRequest(UsbRequest paramUsbRequest) {
    synchronized (this.mLock) {
      if (!isOpen())
        return false; 
      return paramUsbRequest.cancelIfOpen();
    } 
  }
  
  public void close() {
    synchronized (this.mLock) {
      if (isOpen()) {
        native_close();
        this.mCloseGuard.close();
      } 
      return;
    } 
  }
  
  public int getFileDescriptor() {
    return native_get_fd();
  }
  
  public byte[] getRawDescriptors() {
    return native_get_desc();
  }
  
  public boolean claimInterface(UsbInterface paramUsbInterface, boolean paramBoolean) {
    return native_claim_interface(paramUsbInterface.getId(), paramBoolean);
  }
  
  public boolean releaseInterface(UsbInterface paramUsbInterface) {
    return native_release_interface(paramUsbInterface.getId());
  }
  
  public boolean setInterface(UsbInterface paramUsbInterface) {
    return native_set_interface(paramUsbInterface.getId(), paramUsbInterface.getAlternateSetting());
  }
  
  public boolean setConfiguration(UsbConfiguration paramUsbConfiguration) {
    return native_set_configuration(paramUsbConfiguration.getId());
  }
  
  public int controlTransfer(int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfbyte, int paramInt5, int paramInt6) {
    return controlTransfer(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfbyte, 0, paramInt5, paramInt6);
  }
  
  public int controlTransfer(int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfbyte, int paramInt5, int paramInt6, int paramInt7) {
    checkBounds(paramArrayOfbyte, paramInt5, paramInt6);
    return native_control_request(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfbyte, paramInt5, paramInt6, paramInt7);
  }
  
  public int bulkTransfer(UsbEndpoint paramUsbEndpoint, byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return bulkTransfer(paramUsbEndpoint, paramArrayOfbyte, 0, paramInt1, paramInt2);
  }
  
  public int bulkTransfer(UsbEndpoint paramUsbEndpoint, byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) {
    checkBounds(paramArrayOfbyte, paramInt1, paramInt2);
    int i = paramInt2;
    if ((this.mContext.getApplicationInfo()).targetSdkVersion < 28) {
      i = paramInt2;
      if (paramInt2 > 16384)
        i = 16384; 
    } 
    return native_bulk_request(paramUsbEndpoint.getAddress(), paramArrayOfbyte, paramInt1, i, paramInt3);
  }
  
  @SystemApi
  public boolean resetDevice() {
    return native_reset_device();
  }
  
  public UsbRequest requestWait() {
    UsbRequest usbRequest = null;
    try {
      UsbRequest usbRequest1 = native_request_wait(-1L);
    } catch (TimeoutException timeoutException) {}
    if (usbRequest != null) {
      boolean bool;
      Context context = this.mContext;
      if ((context.getApplicationInfo()).targetSdkVersion >= 26) {
        bool = true;
      } else {
        bool = false;
      } 
      usbRequest.dequeue(bool);
    } 
    return usbRequest;
  }
  
  public UsbRequest requestWait(long paramLong) throws TimeoutException {
    paramLong = Preconditions.checkArgumentNonnegative(paramLong, "timeout");
    UsbRequest usbRequest = native_request_wait(paramLong);
    if (usbRequest != null)
      usbRequest.dequeue(true); 
    return usbRequest;
  }
  
  public String getSerial() {
    return native_get_serial();
  }
  
  private static void checkBounds(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    byte b;
    if (paramArrayOfbyte != null) {
      b = paramArrayOfbyte.length;
    } else {
      b = 0;
    } 
    if (paramInt2 >= 0 && paramInt1 >= 0 && paramInt1 + paramInt2 <= b)
      return; 
    throw new IllegalArgumentException("Buffer start or length out of bounds.");
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mCloseGuard != null)
        this.mCloseGuard.warnIfOpen(); 
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private native int native_bulk_request(int paramInt1, byte[] paramArrayOfbyte, int paramInt2, int paramInt3, int paramInt4);
  
  private native boolean native_claim_interface(int paramInt, boolean paramBoolean);
  
  private native void native_close();
  
  private native int native_control_request(int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte[] paramArrayOfbyte, int paramInt5, int paramInt6, int paramInt7);
  
  private native byte[] native_get_desc();
  
  private native int native_get_fd();
  
  private native String native_get_serial();
  
  private native boolean native_open(String paramString, FileDescriptor paramFileDescriptor);
  
  private native boolean native_release_interface(int paramInt);
  
  private native UsbRequest native_request_wait(long paramLong) throws TimeoutException;
  
  private native boolean native_reset_device();
  
  private native boolean native_set_configuration(int paramInt);
  
  private native boolean native_set_interface(int paramInt1, int paramInt2);
}
