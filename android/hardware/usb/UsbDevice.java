package android.hardware.usb;

import android.app.ActivityThread;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.android.internal.util.Preconditions;
import java.util.Objects;

public class UsbDevice implements Parcelable {
  private UsbDevice(String paramString1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString2, String paramString3, String paramString4, UsbConfiguration[] paramArrayOfUsbConfiguration, IUsbSerialReader paramIUsbSerialReader, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5) {
    Objects.requireNonNull(paramString1);
    this.mName = paramString1;
    this.mVendorId = paramInt1;
    this.mProductId = paramInt2;
    this.mClass = paramInt3;
    this.mSubclass = paramInt4;
    this.mProtocol = paramInt5;
    this.mManufacturerName = paramString2;
    this.mProductName = paramString3;
    this.mVersion = (String)Preconditions.checkStringNotEmpty(paramString4);
    this.mConfigurations = (UsbConfiguration[])Preconditions.checkArrayElementsNotNull((Object[])paramArrayOfUsbConfiguration, "configurations");
    Objects.requireNonNull(paramIUsbSerialReader);
    this.mSerialNumberReader = paramIUsbSerialReader;
    this.mHasAudioPlayback = paramBoolean1;
    this.mHasAudioCapture = paramBoolean2;
    this.mHasMidi = paramBoolean3;
    this.mHasVideoPlayback = paramBoolean4;
    this.mHasVideoCapture = paramBoolean5;
    if (ActivityThread.isSystem())
      Preconditions.checkArgument(this.mSerialNumberReader instanceof IUsbSerialReader.Stub); 
  }
  
  public String getDeviceName() {
    return this.mName;
  }
  
  public String getManufacturerName() {
    return this.mManufacturerName;
  }
  
  public String getProductName() {
    return this.mProductName;
  }
  
  public String getVersion() {
    return this.mVersion;
  }
  
  public String getSerialNumber() {
    try {
      return this.mSerialNumberReader.getSerial(ActivityThread.currentPackageName());
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return null;
    } 
  }
  
  public int getDeviceId() {
    return getDeviceId(this.mName);
  }
  
  public int getVendorId() {
    return this.mVendorId;
  }
  
  public int getProductId() {
    return this.mProductId;
  }
  
  public int getDeviceClass() {
    return this.mClass;
  }
  
  public int getDeviceSubclass() {
    return this.mSubclass;
  }
  
  public int getDeviceProtocol() {
    return this.mProtocol;
  }
  
  public int getConfigurationCount() {
    return this.mConfigurations.length;
  }
  
  public boolean getHasAudioPlayback() {
    return this.mHasAudioPlayback;
  }
  
  public boolean getHasAudioCapture() {
    return this.mHasAudioCapture;
  }
  
  public boolean getHasMidi() {
    return this.mHasMidi;
  }
  
  public boolean getHasVideoPlayback() {
    return this.mHasVideoPlayback;
  }
  
  public boolean getHasVideoCapture() {
    return this.mHasVideoCapture;
  }
  
  public UsbConfiguration getConfiguration(int paramInt) {
    return this.mConfigurations[paramInt];
  }
  
  private UsbInterface[] getInterfaceList() {
    if (this.mInterfaces == null) {
      int i = this.mConfigurations.length;
      int j = 0;
      byte b;
      for (b = 0; b < i; b++) {
        UsbConfiguration usbConfiguration = this.mConfigurations[b];
        j += usbConfiguration.getInterfaceCount();
      } 
      this.mInterfaces = new UsbInterface[j];
      j = 0;
      for (b = 0; b < i; b++) {
        UsbConfiguration usbConfiguration = this.mConfigurations[b];
        int k = usbConfiguration.getInterfaceCount();
        for (byte b1 = 0; b1 < k; b1++, j++)
          this.mInterfaces[j] = usbConfiguration.getInterface(b1); 
      } 
    } 
    return this.mInterfaces;
  }
  
  public int getInterfaceCount() {
    return (getInterfaceList()).length;
  }
  
  public UsbInterface getInterface(int paramInt) {
    return getInterfaceList()[paramInt];
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject instanceof UsbDevice)
      return ((UsbDevice)paramObject).mName.equals(this.mName); 
    if (paramObject instanceof String)
      return ((String)paramObject).equals(this.mName); 
    return false;
  }
  
  public int hashCode() {
    return this.mName.hashCode();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UsbDevice[mName=");
    stringBuilder.append(this.mName);
    stringBuilder.append(",mVendorId=");
    stringBuilder.append(this.mVendorId);
    stringBuilder.append(",mProductId=");
    stringBuilder.append(this.mProductId);
    stringBuilder.append(",mClass=");
    stringBuilder.append(this.mClass);
    stringBuilder.append(",mSubclass=");
    stringBuilder.append(this.mSubclass);
    stringBuilder.append(",mProtocol=");
    stringBuilder.append(this.mProtocol);
    stringBuilder.append(",mManufacturerName=");
    stringBuilder.append(this.mManufacturerName);
    stringBuilder.append(",mProductName=");
    stringBuilder.append(this.mProductName);
    stringBuilder.append(",mVersion=");
    stringBuilder.append(this.mVersion);
    stringBuilder.append(",mSerialNumberReader=");
    stringBuilder.append(this.mSerialNumberReader);
    stringBuilder.append(", mHasAudioPlayback=");
    stringBuilder.append(this.mHasAudioPlayback);
    stringBuilder.append(", mHasAudioCapture=");
    stringBuilder.append(this.mHasAudioCapture);
    stringBuilder.append(", mHasMidi=");
    stringBuilder.append(this.mHasMidi);
    stringBuilder.append(", mHasVideoCapture=");
    stringBuilder.append(this.mHasVideoCapture);
    stringBuilder.append(", mHasVideoPlayback=");
    stringBuilder.append(this.mHasVideoPlayback);
    stringBuilder.append(", mConfigurations=[");
    stringBuilder = new StringBuilder(stringBuilder.toString());
    for (byte b = 0; b < this.mConfigurations.length; b++) {
      stringBuilder.append("\n");
      stringBuilder.append(this.mConfigurations[b].toString());
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<UsbDevice> CREATOR = new Parcelable.Creator<UsbDevice>() {
      public UsbDevice createFromParcel(Parcel param1Parcel) {
        boolean bool1, bool2, bool3, bool4, bool5;
        String str1 = param1Parcel.readString();
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        int n = param1Parcel.readInt();
        String str2 = param1Parcel.readString();
        String str3 = param1Parcel.readString();
        String str4 = param1Parcel.readString();
        IUsbSerialReader iUsbSerialReader = IUsbSerialReader.Stub.asInterface(param1Parcel.readStrongBinder());
        ClassLoader classLoader = UsbConfiguration.class.getClassLoader();
        UsbConfiguration[] arrayOfUsbConfiguration = param1Parcel.<UsbConfiguration>readParcelableArray(classLoader, UsbConfiguration.class);
        if (param1Parcel.readInt() == 1) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        if (param1Parcel.readInt() == 1) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        if (param1Parcel.readInt() == 1) {
          bool3 = true;
        } else {
          bool3 = false;
        } 
        if (param1Parcel.readInt() == 1) {
          bool4 = true;
        } else {
          bool4 = false;
        } 
        if (param1Parcel.readInt() == 1) {
          bool5 = true;
        } else {
          bool5 = false;
        } 
        return new UsbDevice(str1, i, j, k, m, n, str2, str3, str4, arrayOfUsbConfiguration, iUsbSerialReader, bool1, bool2, bool3, bool4, bool5);
      }
      
      public UsbDevice[] newArray(int param1Int) {
        return new UsbDevice[param1Int];
      }
    };
  
  private static final boolean DEBUG = false;
  
  private static final String TAG = "UsbDevice";
  
  private final int mClass;
  
  private final UsbConfiguration[] mConfigurations;
  
  private final boolean mHasAudioCapture;
  
  private final boolean mHasAudioPlayback;
  
  private final boolean mHasMidi;
  
  private final boolean mHasVideoCapture;
  
  private final boolean mHasVideoPlayback;
  
  private UsbInterface[] mInterfaces;
  
  private final String mManufacturerName;
  
  private final String mName;
  
  private final int mProductId;
  
  private final String mProductName;
  
  private final int mProtocol;
  
  private final IUsbSerialReader mSerialNumberReader;
  
  private final int mSubclass;
  
  private final int mVendorId;
  
  private final String mVersion;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mName);
    paramParcel.writeInt(this.mVendorId);
    paramParcel.writeInt(this.mProductId);
    paramParcel.writeInt(this.mClass);
    paramParcel.writeInt(this.mSubclass);
    paramParcel.writeInt(this.mProtocol);
    paramParcel.writeString(this.mManufacturerName);
    paramParcel.writeString(this.mProductName);
    paramParcel.writeString(this.mVersion);
    paramParcel.writeStrongBinder(this.mSerialNumberReader.asBinder());
    paramParcel.writeParcelableArray(this.mConfigurations, 0);
    paramParcel.writeInt(this.mHasAudioPlayback);
    paramParcel.writeInt(this.mHasAudioCapture);
    paramParcel.writeInt(this.mHasMidi);
    paramParcel.writeInt(this.mHasVideoPlayback);
    paramParcel.writeInt(this.mHasVideoCapture);
  }
  
  public static int getDeviceId(String paramString) {
    return native_get_device_id(paramString);
  }
  
  public static String getDeviceName(int paramInt) {
    return native_get_device_name(paramInt);
  }
  
  private static native int native_get_device_id(String paramString);
  
  private static native String native_get_device_name(int paramInt);
  
  class Builder {
    private final int mClass;
    
    private final UsbConfiguration[] mConfigurations;
    
    private final boolean mHasAudioCapture;
    
    private final boolean mHasAudioPlayback;
    
    private final boolean mHasMidi;
    
    private final boolean mHasVideoCapture;
    
    private final boolean mHasVideoPlayback;
    
    private final String mManufacturerName;
    
    private final String mName;
    
    private final int mProductId;
    
    private final String mProductName;
    
    private final int mProtocol;
    
    private final int mSubclass;
    
    private final int mVendorId;
    
    private final String mVersion;
    
    public final String serialNumber;
    
    public Builder(UsbDevice this$0, int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, String param1String1, String param1String2, String param1String3, UsbConfiguration[] param1ArrayOfUsbConfiguration, String param1String4, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, boolean param1Boolean5) {
      Objects.requireNonNull(this$0);
      this.mName = (String)this$0;
      this.mVendorId = param1Int1;
      this.mProductId = param1Int2;
      this.mClass = param1Int3;
      this.mSubclass = param1Int4;
      this.mProtocol = param1Int5;
      this.mManufacturerName = param1String1;
      this.mProductName = param1String2;
      this.mVersion = (String)Preconditions.checkStringNotEmpty(param1String3);
      this.mConfigurations = param1ArrayOfUsbConfiguration;
      this.serialNumber = param1String4;
      this.mHasAudioPlayback = param1Boolean1;
      this.mHasAudioCapture = param1Boolean2;
      this.mHasMidi = param1Boolean3;
      this.mHasVideoPlayback = param1Boolean4;
      this.mHasVideoCapture = param1Boolean5;
    }
    
    public UsbDevice build(IUsbSerialReader param1IUsbSerialReader) {
      return new UsbDevice(this.mName, this.mVendorId, this.mProductId, this.mClass, this.mSubclass, this.mProtocol, this.mManufacturerName, this.mProductName, this.mVersion, this.mConfigurations, param1IUsbSerialReader, this.mHasAudioPlayback, this.mHasAudioCapture, this.mHasMidi, this.mHasVideoPlayback, this.mHasVideoCapture);
    }
  }
}
