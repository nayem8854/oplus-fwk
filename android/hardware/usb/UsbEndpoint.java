package android.hardware.usb;

import android.os.Parcel;
import android.os.Parcelable;

public class UsbEndpoint implements Parcelable {
  public UsbEndpoint(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mAddress = paramInt1;
    this.mAttributes = paramInt2;
    this.mMaxPacketSize = paramInt3;
    this.mInterval = paramInt4;
  }
  
  public int getAddress() {
    return this.mAddress;
  }
  
  public int getEndpointNumber() {
    return this.mAddress & 0xF;
  }
  
  public int getDirection() {
    return this.mAddress & 0x80;
  }
  
  public int getAttributes() {
    return this.mAttributes;
  }
  
  public int getType() {
    return this.mAttributes & 0x3;
  }
  
  public int getMaxPacketSize() {
    return this.mMaxPacketSize;
  }
  
  public int getInterval() {
    return this.mInterval;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UsbEndpoint[mAddress=");
    stringBuilder.append(this.mAddress);
    stringBuilder.append(",mAttributes=");
    stringBuilder.append(this.mAttributes);
    stringBuilder.append(",mMaxPacketSize=");
    stringBuilder.append(this.mMaxPacketSize);
    stringBuilder.append(",mInterval=");
    stringBuilder.append(this.mInterval);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<UsbEndpoint> CREATOR = new Parcelable.Creator<UsbEndpoint>() {
      public UsbEndpoint createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        return new UsbEndpoint(i, j, k, m);
      }
      
      public UsbEndpoint[] newArray(int param1Int) {
        return new UsbEndpoint[param1Int];
      }
    };
  
  private final int mAddress;
  
  private final int mAttributes;
  
  private final int mInterval;
  
  private final int mMaxPacketSize;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mAddress);
    paramParcel.writeInt(this.mAttributes);
    paramParcel.writeInt(this.mMaxPacketSize);
    paramParcel.writeInt(this.mInterval);
  }
}
