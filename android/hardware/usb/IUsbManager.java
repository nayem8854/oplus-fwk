package android.hardware.usb;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.UserHandle;
import java.util.List;

public interface IUsbManager extends IInterface {
  void addAccessoryPackagesToPreferenceDenied(UsbAccessory paramUsbAccessory, String[] paramArrayOfString, UserHandle paramUserHandle) throws RemoteException;
  
  void addDevicePackagesToPreferenceDenied(UsbDevice paramUsbDevice, String[] paramArrayOfString, UserHandle paramUserHandle) throws RemoteException;
  
  void clearDefaults(String paramString, int paramInt) throws RemoteException;
  
  void enableContaminantDetection(String paramString, boolean paramBoolean) throws RemoteException;
  
  ParcelFileDescriptor getControlFd(long paramLong) throws RemoteException;
  
  UsbAccessory getCurrentAccessory() throws RemoteException;
  
  long getCurrentFunctions() throws RemoteException;
  
  void getDeviceList(Bundle paramBundle) throws RemoteException;
  
  UsbPortStatus getPortStatus(String paramString) throws RemoteException;
  
  List<ParcelableUsbPort> getPorts() throws RemoteException;
  
  long getScreenUnlockedFunctions() throws RemoteException;
  
  void grantAccessoryPermission(UsbAccessory paramUsbAccessory, int paramInt) throws RemoteException;
  
  void grantDevicePermission(UsbDevice paramUsbDevice, int paramInt) throws RemoteException;
  
  boolean hasAccessoryPermission(UsbAccessory paramUsbAccessory) throws RemoteException;
  
  boolean hasDefaults(String paramString, int paramInt) throws RemoteException;
  
  boolean hasDevicePermission(UsbDevice paramUsbDevice, String paramString) throws RemoteException;
  
  boolean isFunctionEnabled(String paramString) throws RemoteException;
  
  ParcelFileDescriptor openAccessory(UsbAccessory paramUsbAccessory) throws RemoteException;
  
  ParcelFileDescriptor openDevice(String paramString1, String paramString2) throws RemoteException;
  
  void removeAccessoryPackagesFromPreferenceDenied(UsbAccessory paramUsbAccessory, String[] paramArrayOfString, UserHandle paramUserHandle) throws RemoteException;
  
  void removeDevicePackagesFromPreferenceDenied(UsbDevice paramUsbDevice, String[] paramArrayOfString, UserHandle paramUserHandle) throws RemoteException;
  
  void requestAccessoryPermission(UsbAccessory paramUsbAccessory, String paramString, PendingIntent paramPendingIntent) throws RemoteException;
  
  void requestDevicePermission(UsbDevice paramUsbDevice, String paramString, PendingIntent paramPendingIntent) throws RemoteException;
  
  void resetUsbGadget() throws RemoteException;
  
  void setAccessoryPackage(UsbAccessory paramUsbAccessory, String paramString, int paramInt) throws RemoteException;
  
  void setAccessoryPersistentPermission(UsbAccessory paramUsbAccessory, int paramInt, UserHandle paramUserHandle, boolean paramBoolean) throws RemoteException;
  
  void setCurrentFunction(String paramString, boolean paramBoolean) throws RemoteException;
  
  void setCurrentFunctions(long paramLong) throws RemoteException;
  
  void setDevicePackage(UsbDevice paramUsbDevice, String paramString, int paramInt) throws RemoteException;
  
  void setDevicePersistentPermission(UsbDevice paramUsbDevice, int paramInt, UserHandle paramUserHandle, boolean paramBoolean) throws RemoteException;
  
  void setPortRoles(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void setScreenUnlockedFunctions(long paramLong) throws RemoteException;
  
  void setUsbDeviceConnectionHandler(ComponentName paramComponentName) throws RemoteException;
  
  class Default implements IUsbManager {
    public void getDeviceList(Bundle param1Bundle) throws RemoteException {}
    
    public ParcelFileDescriptor openDevice(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public UsbAccessory getCurrentAccessory() throws RemoteException {
      return null;
    }
    
    public ParcelFileDescriptor openAccessory(UsbAccessory param1UsbAccessory) throws RemoteException {
      return null;
    }
    
    public void setDevicePackage(UsbDevice param1UsbDevice, String param1String, int param1Int) throws RemoteException {}
    
    public void setAccessoryPackage(UsbAccessory param1UsbAccessory, String param1String, int param1Int) throws RemoteException {}
    
    public void addDevicePackagesToPreferenceDenied(UsbDevice param1UsbDevice, String[] param1ArrayOfString, UserHandle param1UserHandle) throws RemoteException {}
    
    public void addAccessoryPackagesToPreferenceDenied(UsbAccessory param1UsbAccessory, String[] param1ArrayOfString, UserHandle param1UserHandle) throws RemoteException {}
    
    public void removeDevicePackagesFromPreferenceDenied(UsbDevice param1UsbDevice, String[] param1ArrayOfString, UserHandle param1UserHandle) throws RemoteException {}
    
    public void removeAccessoryPackagesFromPreferenceDenied(UsbAccessory param1UsbAccessory, String[] param1ArrayOfString, UserHandle param1UserHandle) throws RemoteException {}
    
    public void setDevicePersistentPermission(UsbDevice param1UsbDevice, int param1Int, UserHandle param1UserHandle, boolean param1Boolean) throws RemoteException {}
    
    public void setAccessoryPersistentPermission(UsbAccessory param1UsbAccessory, int param1Int, UserHandle param1UserHandle, boolean param1Boolean) throws RemoteException {}
    
    public boolean hasDevicePermission(UsbDevice param1UsbDevice, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean hasAccessoryPermission(UsbAccessory param1UsbAccessory) throws RemoteException {
      return false;
    }
    
    public void requestDevicePermission(UsbDevice param1UsbDevice, String param1String, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void requestAccessoryPermission(UsbAccessory param1UsbAccessory, String param1String, PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void grantDevicePermission(UsbDevice param1UsbDevice, int param1Int) throws RemoteException {}
    
    public void grantAccessoryPermission(UsbAccessory param1UsbAccessory, int param1Int) throws RemoteException {}
    
    public boolean hasDefaults(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public void clearDefaults(String param1String, int param1Int) throws RemoteException {}
    
    public boolean isFunctionEnabled(String param1String) throws RemoteException {
      return false;
    }
    
    public void setCurrentFunctions(long param1Long) throws RemoteException {}
    
    public void setCurrentFunction(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public long getCurrentFunctions() throws RemoteException {
      return 0L;
    }
    
    public void setScreenUnlockedFunctions(long param1Long) throws RemoteException {}
    
    public long getScreenUnlockedFunctions() throws RemoteException {
      return 0L;
    }
    
    public void resetUsbGadget() throws RemoteException {}
    
    public ParcelFileDescriptor getControlFd(long param1Long) throws RemoteException {
      return null;
    }
    
    public List<ParcelableUsbPort> getPorts() throws RemoteException {
      return null;
    }
    
    public UsbPortStatus getPortStatus(String param1String) throws RemoteException {
      return null;
    }
    
    public void setPortRoles(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void enableContaminantDetection(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void setUsbDeviceConnectionHandler(ComponentName param1ComponentName) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IUsbManager {
    private static final String DESCRIPTOR = "android.hardware.usb.IUsbManager";
    
    static final int TRANSACTION_addAccessoryPackagesToPreferenceDenied = 8;
    
    static final int TRANSACTION_addDevicePackagesToPreferenceDenied = 7;
    
    static final int TRANSACTION_clearDefaults = 20;
    
    static final int TRANSACTION_enableContaminantDetection = 32;
    
    static final int TRANSACTION_getControlFd = 28;
    
    static final int TRANSACTION_getCurrentAccessory = 3;
    
    static final int TRANSACTION_getCurrentFunctions = 24;
    
    static final int TRANSACTION_getDeviceList = 1;
    
    static final int TRANSACTION_getPortStatus = 30;
    
    static final int TRANSACTION_getPorts = 29;
    
    static final int TRANSACTION_getScreenUnlockedFunctions = 26;
    
    static final int TRANSACTION_grantAccessoryPermission = 18;
    
    static final int TRANSACTION_grantDevicePermission = 17;
    
    static final int TRANSACTION_hasAccessoryPermission = 14;
    
    static final int TRANSACTION_hasDefaults = 19;
    
    static final int TRANSACTION_hasDevicePermission = 13;
    
    static final int TRANSACTION_isFunctionEnabled = 21;
    
    static final int TRANSACTION_openAccessory = 4;
    
    static final int TRANSACTION_openDevice = 2;
    
    static final int TRANSACTION_removeAccessoryPackagesFromPreferenceDenied = 10;
    
    static final int TRANSACTION_removeDevicePackagesFromPreferenceDenied = 9;
    
    static final int TRANSACTION_requestAccessoryPermission = 16;
    
    static final int TRANSACTION_requestDevicePermission = 15;
    
    static final int TRANSACTION_resetUsbGadget = 27;
    
    static final int TRANSACTION_setAccessoryPackage = 6;
    
    static final int TRANSACTION_setAccessoryPersistentPermission = 12;
    
    static final int TRANSACTION_setCurrentFunction = 23;
    
    static final int TRANSACTION_setCurrentFunctions = 22;
    
    static final int TRANSACTION_setDevicePackage = 5;
    
    static final int TRANSACTION_setDevicePersistentPermission = 11;
    
    static final int TRANSACTION_setPortRoles = 31;
    
    static final int TRANSACTION_setScreenUnlockedFunctions = 25;
    
    static final int TRANSACTION_setUsbDeviceConnectionHandler = 33;
    
    public Stub() {
      attachInterface(this, "android.hardware.usb.IUsbManager");
    }
    
    public static IUsbManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.usb.IUsbManager");
      if (iInterface != null && iInterface instanceof IUsbManager)
        return (IUsbManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 33:
          return "setUsbDeviceConnectionHandler";
        case 32:
          return "enableContaminantDetection";
        case 31:
          return "setPortRoles";
        case 30:
          return "getPortStatus";
        case 29:
          return "getPorts";
        case 28:
          return "getControlFd";
        case 27:
          return "resetUsbGadget";
        case 26:
          return "getScreenUnlockedFunctions";
        case 25:
          return "setScreenUnlockedFunctions";
        case 24:
          return "getCurrentFunctions";
        case 23:
          return "setCurrentFunction";
        case 22:
          return "setCurrentFunctions";
        case 21:
          return "isFunctionEnabled";
        case 20:
          return "clearDefaults";
        case 19:
          return "hasDefaults";
        case 18:
          return "grantAccessoryPermission";
        case 17:
          return "grantDevicePermission";
        case 16:
          return "requestAccessoryPermission";
        case 15:
          return "requestDevicePermission";
        case 14:
          return "hasAccessoryPermission";
        case 13:
          return "hasDevicePermission";
        case 12:
          return "setAccessoryPersistentPermission";
        case 11:
          return "setDevicePersistentPermission";
        case 10:
          return "removeAccessoryPackagesFromPreferenceDenied";
        case 9:
          return "removeDevicePackagesFromPreferenceDenied";
        case 8:
          return "addAccessoryPackagesToPreferenceDenied";
        case 7:
          return "addDevicePackagesToPreferenceDenied";
        case 6:
          return "setAccessoryPackage";
        case 5:
          return "setDevicePackage";
        case 4:
          return "openAccessory";
        case 3:
          return "getCurrentAccessory";
        case 2:
          return "openDevice";
        case 1:
          break;
      } 
      return "getDeviceList";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        String str3;
        UsbPortStatus usbPortStatus;
        List<ParcelableUsbPort> list;
        ParcelFileDescriptor parcelFileDescriptor3;
        String str2;
        ParcelFileDescriptor parcelFileDescriptor2;
        UsbAccessory usbAccessory;
        String str1;
        ParcelFileDescriptor parcelFileDescriptor1;
        String str4;
        long l;
        String str6, arrayOfString[], str5;
        boolean bool4 = false, bool5 = false, bool6 = false, bool7 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 33:
            param1Parcel1.enforceInterface("android.hardware.usb.IUsbManager");
            if (param1Parcel1.readInt() != 0) {
              ComponentName componentName = ComponentName.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setUsbDeviceConnectionHandler((ComponentName)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            param1Parcel1.enforceInterface("android.hardware.usb.IUsbManager");
            str4 = param1Parcel1.readString();
            bool5 = bool7;
            if (param1Parcel1.readInt() != 0)
              bool5 = true; 
            enableContaminantDetection(str4, bool5);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            param1Parcel1.enforceInterface("android.hardware.usb.IUsbManager");
            str4 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            setPortRoles(str4, param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            param1Parcel1.enforceInterface("android.hardware.usb.IUsbManager");
            str3 = param1Parcel1.readString();
            usbPortStatus = getPortStatus(str3);
            param1Parcel2.writeNoException();
            if (usbPortStatus != null) {
              param1Parcel2.writeInt(1);
              usbPortStatus.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 29:
            usbPortStatus.enforceInterface("android.hardware.usb.IUsbManager");
            list = getPorts();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 28:
            list.enforceInterface("android.hardware.usb.IUsbManager");
            l = list.readLong();
            parcelFileDescriptor3 = getControlFd(l);
            param1Parcel2.writeNoException();
            if (parcelFileDescriptor3 != null) {
              param1Parcel2.writeInt(1);
              parcelFileDescriptor3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 27:
            parcelFileDescriptor3.enforceInterface("android.hardware.usb.IUsbManager");
            resetUsbGadget();
            param1Parcel2.writeNoException();
            return true;
          case 26:
            parcelFileDescriptor3.enforceInterface("android.hardware.usb.IUsbManager");
            l = getScreenUnlockedFunctions();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 25:
            parcelFileDescriptor3.enforceInterface("android.hardware.usb.IUsbManager");
            l = parcelFileDescriptor3.readLong();
            setScreenUnlockedFunctions(l);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            parcelFileDescriptor3.enforceInterface("android.hardware.usb.IUsbManager");
            l = getCurrentFunctions();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 23:
            parcelFileDescriptor3.enforceInterface("android.hardware.usb.IUsbManager");
            str4 = parcelFileDescriptor3.readString();
            bool5 = bool4;
            if (parcelFileDescriptor3.readInt() != 0)
              bool5 = true; 
            setCurrentFunction(str4, bool5);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            parcelFileDescriptor3.enforceInterface("android.hardware.usb.IUsbManager");
            l = parcelFileDescriptor3.readLong();
            setCurrentFunctions(l);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            parcelFileDescriptor3.enforceInterface("android.hardware.usb.IUsbManager");
            str2 = parcelFileDescriptor3.readString();
            bool3 = isFunctionEnabled(str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 20:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            str4 = str2.readString();
            k = str2.readInt();
            clearDefaults(str4, k);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            str4 = str2.readString();
            k = str2.readInt();
            bool2 = hasDefaults(str4, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 18:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbAccessory usbAccessory1 = UsbAccessory.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            j = str2.readInt();
            grantAccessoryPermission((UsbAccessory)str4, j);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbDevice usbDevice = UsbDevice.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            j = str2.readInt();
            grantDevicePermission((UsbDevice)str4, j);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbAccessory usbAccessory1 = UsbAccessory.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            str6 = str2.readString();
            if (str2.readInt() != 0) {
              PendingIntent pendingIntent = PendingIntent.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            requestAccessoryPermission((UsbAccessory)str4, str6, (PendingIntent)str2);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbDevice usbDevice = UsbDevice.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            str6 = str2.readString();
            if (str2.readInt() != 0) {
              PendingIntent pendingIntent = PendingIntent.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            requestDevicePermission((UsbDevice)str4, str6, (PendingIntent)str2);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbAccessory usbAccessory1 = UsbAccessory.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            bool1 = hasAccessoryPermission((UsbAccessory)str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 13:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbDevice usbDevice = UsbDevice.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            str2 = str2.readString();
            bool1 = hasDevicePermission((UsbDevice)str4, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 12:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbAccessory usbAccessory1 = UsbAccessory.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            i = str2.readInt();
            if (str2.readInt() != 0) {
              UserHandle userHandle = UserHandle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str6 = null;
            } 
            if (str2.readInt() != 0)
              bool5 = true; 
            setAccessoryPersistentPermission((UsbAccessory)str4, i, (UserHandle)str6, bool5);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbDevice usbDevice = UsbDevice.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            i = str2.readInt();
            if (str2.readInt() != 0) {
              UserHandle userHandle = UserHandle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str6 = null;
            } 
            bool5 = bool6;
            if (str2.readInt() != 0)
              bool5 = true; 
            setDevicePersistentPermission((UsbDevice)str4, i, (UserHandle)str6, bool5);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbAccessory usbAccessory1 = UsbAccessory.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            arrayOfString = str2.createStringArray();
            if (str2.readInt() != 0) {
              UserHandle userHandle = UserHandle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            removeAccessoryPackagesFromPreferenceDenied((UsbAccessory)str4, arrayOfString, (UserHandle)str2);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbDevice usbDevice = UsbDevice.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            arrayOfString = str2.createStringArray();
            if (str2.readInt() != 0) {
              UserHandle userHandle = UserHandle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            removeDevicePackagesFromPreferenceDenied((UsbDevice)str4, arrayOfString, (UserHandle)str2);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbAccessory usbAccessory1 = UsbAccessory.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            arrayOfString = str2.createStringArray();
            if (str2.readInt() != 0) {
              UserHandle userHandle = UserHandle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            addAccessoryPackagesToPreferenceDenied((UsbAccessory)str4, arrayOfString, (UserHandle)str2);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbDevice usbDevice = UsbDevice.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            arrayOfString = str2.createStringArray();
            if (str2.readInt() != 0) {
              UserHandle userHandle = UserHandle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            addDevicePackagesToPreferenceDenied((UsbDevice)str4, arrayOfString, (UserHandle)str2);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbAccessory usbAccessory1 = UsbAccessory.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            str5 = str2.readString();
            i = str2.readInt();
            setAccessoryPackage((UsbAccessory)str4, str5, i);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbDevice usbDevice = UsbDevice.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            str5 = str2.readString();
            i = str2.readInt();
            setDevicePackage((UsbDevice)str4, str5, i);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str2.enforceInterface("android.hardware.usb.IUsbManager");
            if (str2.readInt() != 0) {
              UsbAccessory usbAccessory1 = UsbAccessory.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            parcelFileDescriptor2 = openAccessory((UsbAccessory)str2);
            param1Parcel2.writeNoException();
            if (parcelFileDescriptor2 != null) {
              param1Parcel2.writeInt(1);
              parcelFileDescriptor2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            parcelFileDescriptor2.enforceInterface("android.hardware.usb.IUsbManager");
            usbAccessory = getCurrentAccessory();
            param1Parcel2.writeNoException();
            if (usbAccessory != null) {
              param1Parcel2.writeInt(1);
              usbAccessory.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            usbAccessory.enforceInterface("android.hardware.usb.IUsbManager");
            str4 = usbAccessory.readString();
            str1 = usbAccessory.readString();
            parcelFileDescriptor1 = openDevice(str4, str1);
            param1Parcel2.writeNoException();
            if (parcelFileDescriptor1 != null) {
              param1Parcel2.writeInt(1);
              parcelFileDescriptor1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        parcelFileDescriptor1.enforceInterface("android.hardware.usb.IUsbManager");
        Bundle bundle = new Bundle();
        getDeviceList(bundle);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(1);
        bundle.writeToParcel(param1Parcel2, 1);
        return true;
      } 
      param1Parcel2.writeString("android.hardware.usb.IUsbManager");
      return true;
    }
    
    private static class Proxy implements IUsbManager {
      public static IUsbManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.usb.IUsbManager";
      }
      
      public void getDeviceList(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().getDeviceList(param2Bundle);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2Bundle.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelFileDescriptor openDevice(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null)
            return IUsbManager.Stub.getDefaultImpl().openDevice(param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (ParcelFileDescriptor)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UsbAccessory getCurrentAccessory() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          UsbAccessory usbAccessory;
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            usbAccessory = IUsbManager.Stub.getDefaultImpl().getCurrentAccessory();
            return usbAccessory;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            usbAccessory = UsbAccessory.CREATOR.createFromParcel(parcel2);
          } else {
            usbAccessory = null;
          } 
          return usbAccessory;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelFileDescriptor openAccessory(UsbAccessory param2UsbAccessory) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          if (param2UsbAccessory != null) {
            parcel1.writeInt(1);
            param2UsbAccessory.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null)
            return IUsbManager.Stub.getDefaultImpl().openAccessory(param2UsbAccessory); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            param2UsbAccessory = null;
          } 
          return (ParcelFileDescriptor)param2UsbAccessory;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDevicePackage(UsbDevice param2UsbDevice, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          if (param2UsbDevice != null) {
            parcel1.writeInt(1);
            param2UsbDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().setDevicePackage(param2UsbDevice, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAccessoryPackage(UsbAccessory param2UsbAccessory, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          if (param2UsbAccessory != null) {
            parcel1.writeInt(1);
            param2UsbAccessory.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().setAccessoryPackage(param2UsbAccessory, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addDevicePackagesToPreferenceDenied(UsbDevice param2UsbDevice, String[] param2ArrayOfString, UserHandle param2UserHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          if (param2UsbDevice != null) {
            parcel1.writeInt(1);
            param2UsbDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStringArray(param2ArrayOfString);
          if (param2UserHandle != null) {
            parcel1.writeInt(1);
            param2UserHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().addDevicePackagesToPreferenceDenied(param2UsbDevice, param2ArrayOfString, param2UserHandle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addAccessoryPackagesToPreferenceDenied(UsbAccessory param2UsbAccessory, String[] param2ArrayOfString, UserHandle param2UserHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          if (param2UsbAccessory != null) {
            parcel1.writeInt(1);
            param2UsbAccessory.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStringArray(param2ArrayOfString);
          if (param2UserHandle != null) {
            parcel1.writeInt(1);
            param2UserHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().addAccessoryPackagesToPreferenceDenied(param2UsbAccessory, param2ArrayOfString, param2UserHandle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeDevicePackagesFromPreferenceDenied(UsbDevice param2UsbDevice, String[] param2ArrayOfString, UserHandle param2UserHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          if (param2UsbDevice != null) {
            parcel1.writeInt(1);
            param2UsbDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStringArray(param2ArrayOfString);
          if (param2UserHandle != null) {
            parcel1.writeInt(1);
            param2UserHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().removeDevicePackagesFromPreferenceDenied(param2UsbDevice, param2ArrayOfString, param2UserHandle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeAccessoryPackagesFromPreferenceDenied(UsbAccessory param2UsbAccessory, String[] param2ArrayOfString, UserHandle param2UserHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          if (param2UsbAccessory != null) {
            parcel1.writeInt(1);
            param2UsbAccessory.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStringArray(param2ArrayOfString);
          if (param2UserHandle != null) {
            parcel1.writeInt(1);
            param2UserHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().removeAccessoryPackagesFromPreferenceDenied(param2UsbAccessory, param2ArrayOfString, param2UserHandle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDevicePersistentPermission(UsbDevice param2UsbDevice, int param2Int, UserHandle param2UserHandle, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          boolean bool = true;
          if (param2UsbDevice != null) {
            parcel1.writeInt(1);
            param2UsbDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (param2UserHandle != null) {
            parcel1.writeInt(1);
            param2UserHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool1 && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().setDevicePersistentPermission(param2UsbDevice, param2Int, param2UserHandle, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAccessoryPersistentPermission(UsbAccessory param2UsbAccessory, int param2Int, UserHandle param2UserHandle, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          boolean bool = true;
          if (param2UsbAccessory != null) {
            parcel1.writeInt(1);
            param2UsbAccessory.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (param2UserHandle != null) {
            parcel1.writeInt(1);
            param2UserHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool1 && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().setAccessoryPersistentPermission(param2UsbAccessory, param2Int, param2UserHandle, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasDevicePermission(UsbDevice param2UsbDevice, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          boolean bool1 = true;
          if (param2UsbDevice != null) {
            parcel1.writeInt(1);
            param2UsbDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool2 && IUsbManager.Stub.getDefaultImpl() != null) {
            bool1 = IUsbManager.Stub.getDefaultImpl().hasDevicePermission(param2UsbDevice, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasAccessoryPermission(UsbAccessory param2UsbAccessory) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          boolean bool1 = true;
          if (param2UsbAccessory != null) {
            parcel1.writeInt(1);
            param2UsbAccessory.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool2 && IUsbManager.Stub.getDefaultImpl() != null) {
            bool1 = IUsbManager.Stub.getDefaultImpl().hasAccessoryPermission(param2UsbAccessory);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestDevicePermission(UsbDevice param2UsbDevice, String param2String, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          if (param2UsbDevice != null) {
            parcel1.writeInt(1);
            param2UsbDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().requestDevicePermission(param2UsbDevice, param2String, param2PendingIntent);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestAccessoryPermission(UsbAccessory param2UsbAccessory, String param2String, PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          if (param2UsbAccessory != null) {
            parcel1.writeInt(1);
            param2UsbAccessory.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().requestAccessoryPermission(param2UsbAccessory, param2String, param2PendingIntent);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void grantDevicePermission(UsbDevice param2UsbDevice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          if (param2UsbDevice != null) {
            parcel1.writeInt(1);
            param2UsbDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().grantDevicePermission(param2UsbDevice, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void grantAccessoryPermission(UsbAccessory param2UsbAccessory, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          if (param2UsbAccessory != null) {
            parcel1.writeInt(1);
            param2UsbAccessory.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().grantAccessoryPermission(param2UsbAccessory, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasDefaults(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(19, parcel1, parcel2, 0);
          if (!bool2 && IUsbManager.Stub.getDefaultImpl() != null) {
            bool1 = IUsbManager.Stub.getDefaultImpl().hasDefaults(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearDefaults(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().clearDefaults(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isFunctionEnabled(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(21, parcel1, parcel2, 0);
          if (!bool2 && IUsbManager.Stub.getDefaultImpl() != null) {
            bool1 = IUsbManager.Stub.getDefaultImpl().isFunctionEnabled(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCurrentFunctions(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().setCurrentFunctions(param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCurrentFunction(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool1 && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().setCurrentFunction(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getCurrentFunctions() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null)
            return IUsbManager.Stub.getDefaultImpl().getCurrentFunctions(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setScreenUnlockedFunctions(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().setScreenUnlockedFunctions(param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getScreenUnlockedFunctions() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null)
            return IUsbManager.Stub.getDefaultImpl().getScreenUnlockedFunctions(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetUsbGadget() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().resetUsbGadget();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelFileDescriptor getControlFd(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParcelFileDescriptor parcelFileDescriptor;
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            parcelFileDescriptor = IUsbManager.Stub.getDefaultImpl().getControlFd(param2Long);
            return parcelFileDescriptor;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            parcelFileDescriptor = null;
          } 
          return parcelFileDescriptor;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ParcelableUsbPort> getPorts() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null)
            return IUsbManager.Stub.getDefaultImpl().getPorts(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ParcelableUsbPort.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UsbPortStatus getPortStatus(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null)
            return IUsbManager.Stub.getDefaultImpl().getPortStatus(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            UsbPortStatus usbPortStatus = UsbPortStatus.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (UsbPortStatus)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPortRoles(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().setPortRoles(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableContaminantDetection(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool1 && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().enableContaminantDetection(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUsbDeviceConnectionHandler(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.usb.IUsbManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IUsbManager.Stub.getDefaultImpl() != null) {
            IUsbManager.Stub.getDefaultImpl().setUsbDeviceConnectionHandler(param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IUsbManager param1IUsbManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IUsbManager != null) {
          Proxy.sDefaultImpl = param1IUsbManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IUsbManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
