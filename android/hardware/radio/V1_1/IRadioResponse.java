package android.hardware.radio.V1_1;

import android.hardware.radio.V1_0.ActivityStatsInfo;
import android.hardware.radio.V1_0.Call;
import android.hardware.radio.V1_0.CallForwardInfo;
import android.hardware.radio.V1_0.CardStatus;
import android.hardware.radio.V1_0.CarrierRestrictions;
import android.hardware.radio.V1_0.CdmaBroadcastSmsConfigInfo;
import android.hardware.radio.V1_0.CellInfo;
import android.hardware.radio.V1_0.DataRegStateResult;
import android.hardware.radio.V1_0.GsmBroadcastSmsConfigInfo;
import android.hardware.radio.V1_0.HardwareConfig;
import android.hardware.radio.V1_0.IRadioResponse;
import android.hardware.radio.V1_0.IccIoResult;
import android.hardware.radio.V1_0.LastCallFailCauseInfo;
import android.hardware.radio.V1_0.LceDataInfo;
import android.hardware.radio.V1_0.LceStatusInfo;
import android.hardware.radio.V1_0.NeighboringCell;
import android.hardware.radio.V1_0.OperatorInfo;
import android.hardware.radio.V1_0.RadioCapability;
import android.hardware.radio.V1_0.RadioResponseInfo;
import android.hardware.radio.V1_0.SendSmsResult;
import android.hardware.radio.V1_0.SetupDataCallResult;
import android.hardware.radio.V1_0.SignalStrength;
import android.hardware.radio.V1_0.VoiceRegStateResult;
import android.internal.hidl.base.V1_0.DebugInfo;
import android.os.HidlSupport;
import android.os.HwBinder;
import android.os.HwBlob;
import android.os.HwParcel;
import android.os.IHwBinder;
import android.os.IHwInterface;
import android.os.NativeHandle;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public interface IRadioResponse extends IRadioResponse {
  public static final String kInterfaceName = "android.hardware.radio@1.1::IRadioResponse";
  
  static IRadioResponse asInterface(IHwBinder paramIHwBinder) {
    if (paramIHwBinder == null)
      return null; 
    IHwInterface iHwInterface = paramIHwBinder.queryLocalInterface("android.hardware.radio@1.1::IRadioResponse");
    if (iHwInterface != null && iHwInterface instanceof IRadioResponse)
      return (IRadioResponse)iHwInterface; 
    Proxy proxy = new Proxy(paramIHwBinder);
    try {
      for (String str : proxy.interfaceChain()) {
        boolean bool = str.equals("android.hardware.radio@1.1::IRadioResponse");
        if (bool)
          return proxy; 
      } 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  static IRadioResponse castFrom(IHwInterface paramIHwInterface) {
    if (paramIHwInterface == null) {
      paramIHwInterface = null;
    } else {
      paramIHwInterface = asInterface(paramIHwInterface.asBinder());
    } 
    return (IRadioResponse)paramIHwInterface;
  }
  
  static IRadioResponse getService(String paramString, boolean paramBoolean) throws RemoteException {
    return asInterface(HwBinder.getService("android.hardware.radio@1.1::IRadioResponse", paramString, paramBoolean));
  }
  
  static IRadioResponse getService(boolean paramBoolean) throws RemoteException {
    return getService("default", paramBoolean);
  }
  
  static IRadioResponse getService(String paramString) throws RemoteException {
    return asInterface(HwBinder.getService("android.hardware.radio@1.1::IRadioResponse", paramString));
  }
  
  static IRadioResponse getService() throws RemoteException {
    return getService("default");
  }
  
  IHwBinder asBinder();
  
  void debug(NativeHandle paramNativeHandle, ArrayList<String> paramArrayList) throws RemoteException;
  
  DebugInfo getDebugInfo() throws RemoteException;
  
  ArrayList<byte[]> getHashChain() throws RemoteException;
  
  ArrayList<String> interfaceChain() throws RemoteException;
  
  String interfaceDescriptor() throws RemoteException;
  
  boolean linkToDeath(IHwBinder.DeathRecipient paramDeathRecipient, long paramLong) throws RemoteException;
  
  void notifySyspropsChanged() throws RemoteException;
  
  void ping() throws RemoteException;
  
  void setCarrierInfoForImsiEncryptionResponse(RadioResponseInfo paramRadioResponseInfo) throws RemoteException;
  
  void setHALInstrumentation() throws RemoteException;
  
  void setSimCardPowerResponse_1_1(RadioResponseInfo paramRadioResponseInfo) throws RemoteException;
  
  void startKeepaliveResponse(RadioResponseInfo paramRadioResponseInfo, KeepaliveStatus paramKeepaliveStatus) throws RemoteException;
  
  void startNetworkScanResponse(RadioResponseInfo paramRadioResponseInfo) throws RemoteException;
  
  void stopKeepaliveResponse(RadioResponseInfo paramRadioResponseInfo) throws RemoteException;
  
  void stopNetworkScanResponse(RadioResponseInfo paramRadioResponseInfo) throws RemoteException;
  
  boolean unlinkToDeath(IHwBinder.DeathRecipient paramDeathRecipient) throws RemoteException;
  
  class Proxy implements IRadioResponse {
    private IHwBinder mRemote;
    
    public Proxy(IRadioResponse this$0) {
      Objects.requireNonNull(this$0);
      this.mRemote = (IHwBinder)this$0;
    }
    
    public IHwBinder asBinder() {
      return this.mRemote;
    }
    
    public String toString() {
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(interfaceDescriptor());
        stringBuilder.append("@Proxy");
        return stringBuilder.toString();
      } catch (RemoteException remoteException) {
        return "[class or subclass of android.hardware.radio@1.1::IRadioResponse]@Proxy";
      } 
    }
    
    public final boolean equals(Object param1Object) {
      return HidlSupport.interfacesEqual(this, param1Object);
    }
    
    public final int hashCode() {
      return asBinder().hashCode();
    }
    
    public void getIccCardStatusResponse(RadioResponseInfo param1RadioResponseInfo, CardStatus param1CardStatus) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1CardStatus.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(1, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void supplyIccPinForAppResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(2, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void supplyIccPukForAppResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(3, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void supplyIccPin2ForAppResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(4, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void supplyIccPuk2ForAppResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(5, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void changeIccPinForAppResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(6, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void changeIccPin2ForAppResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(7, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void supplyNetworkDepersonalizationResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(8, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCurrentCallsResponse(RadioResponseInfo param1RadioResponseInfo, ArrayList<Call> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      Call.writeVectorToParcel(hwParcel2, param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(9, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void dialResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(10, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getIMSIForAppResponse(RadioResponseInfo param1RadioResponseInfo, String param1String) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      hwParcel2.writeString(param1String);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(11, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void hangupConnectionResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(12, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void hangupWaitingOrBackgroundResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(13, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void hangupForegroundResumeBackgroundResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(14, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void switchWaitingOrHoldingAndActiveResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(15, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void conferenceResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(16, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void rejectCallResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(17, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getLastCallFailCauseResponse(RadioResponseInfo param1RadioResponseInfo, LastCallFailCauseInfo param1LastCallFailCauseInfo) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1LastCallFailCauseInfo.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(18, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void getSignalStrengthResponse(RadioResponseInfo param1RadioResponseInfo, SignalStrength param1SignalStrength) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1SignalStrength.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(19, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void getVoiceRegistrationStateResponse(RadioResponseInfo param1RadioResponseInfo, VoiceRegStateResult param1VoiceRegStateResult) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1VoiceRegStateResult.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(20, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void getDataRegistrationStateResponse(RadioResponseInfo param1RadioResponseInfo, DataRegStateResult param1DataRegStateResult) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1DataRegStateResult.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(21, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void getOperatorResponse(RadioResponseInfo param1RadioResponseInfo, String param1String1, String param1String2, String param1String3) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      hwParcel2.writeString(param1String1);
      hwParcel2.writeString(param1String2);
      hwParcel2.writeString(param1String3);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(22, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setRadioPowerResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(23, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendDtmfResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(24, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendSmsResponse(RadioResponseInfo param1RadioResponseInfo, SendSmsResult param1SendSmsResult) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1SendSmsResult.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(25, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void sendSMSExpectMoreResponse(RadioResponseInfo param1RadioResponseInfo, SendSmsResult param1SendSmsResult) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1SendSmsResult.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(26, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setupDataCallResponse(RadioResponseInfo param1RadioResponseInfo, SetupDataCallResult param1SetupDataCallResult) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1SetupDataCallResult.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(27, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void iccIOForAppResponse(RadioResponseInfo param1RadioResponseInfo, IccIoResult param1IccIoResult) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1IccIoResult.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(28, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void sendUssdResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(29, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void cancelPendingUssdResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(30, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getClirResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(31, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setClirResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(32, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCallForwardStatusResponse(RadioResponseInfo param1RadioResponseInfo, ArrayList<CallForwardInfo> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      CallForwardInfo.writeVectorToParcel(hwParcel2, param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(33, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setCallForwardResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(34, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCallWaitingResponse(RadioResponseInfo param1RadioResponseInfo, boolean param1Boolean, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeBool(param1Boolean);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(35, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setCallWaitingResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(36, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void acknowledgeLastIncomingGsmSmsResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(37, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void acceptCallResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(38, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void deactivateDataCallResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(39, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getFacilityLockForAppResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(40, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setFacilityLockForAppResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(41, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setBarringPasswordResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(42, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getNetworkSelectionModeResponse(RadioResponseInfo param1RadioResponseInfo, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(43, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setNetworkSelectionModeAutomaticResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(44, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setNetworkSelectionModeManualResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(45, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getAvailableNetworksResponse(RadioResponseInfo param1RadioResponseInfo, ArrayList<OperatorInfo> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      OperatorInfo.writeVectorToParcel(hwParcel2, param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(46, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void startDtmfResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(47, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void stopDtmfResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(48, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getBasebandVersionResponse(RadioResponseInfo param1RadioResponseInfo, String param1String) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      hwParcel2.writeString(param1String);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(49, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void separateConnectionResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(50, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setMuteResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(51, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getMuteResponse(RadioResponseInfo param1RadioResponseInfo, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(52, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getClipResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(53, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getDataCallListResponse(RadioResponseInfo param1RadioResponseInfo, ArrayList<SetupDataCallResult> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      SetupDataCallResult.writeVectorToParcel(hwParcel2, param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(54, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setSuppServiceNotificationsResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(55, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void writeSmsToSimResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(56, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void deleteSmsOnSimResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(57, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setBandModeResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(58, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getAvailableBandModesResponse(RadioResponseInfo param1RadioResponseInfo, ArrayList<Integer> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      hwParcel2.writeInt32Vector(param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(59, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void sendEnvelopeResponse(RadioResponseInfo param1RadioResponseInfo, String param1String) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      hwParcel2.writeString(param1String);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(60, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void sendTerminalResponseToSimResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(61, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void handleStkCallSetupRequestFromSimResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(62, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void explicitCallTransferResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(63, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setPreferredNetworkTypeResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(64, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getPreferredNetworkTypeResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(65, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getNeighboringCidsResponse(RadioResponseInfo param1RadioResponseInfo, ArrayList<NeighboringCell> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      NeighboringCell.writeVectorToParcel(hwParcel2, param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(66, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setLocationUpdatesResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(67, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setCdmaSubscriptionSourceResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(68, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setCdmaRoamingPreferenceResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(69, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCdmaRoamingPreferenceResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(70, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setTTYModeResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(71, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getTTYModeResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(72, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setPreferredVoicePrivacyResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(73, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getPreferredVoicePrivacyResponse(RadioResponseInfo param1RadioResponseInfo, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(74, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendCDMAFeatureCodeResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(75, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendBurstDtmfResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(76, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendCdmaSmsResponse(RadioResponseInfo param1RadioResponseInfo, SendSmsResult param1SendSmsResult) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1SendSmsResult.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(77, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void acknowledgeLastIncomingCdmaSmsResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(78, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getGsmBroadcastConfigResponse(RadioResponseInfo param1RadioResponseInfo, ArrayList<GsmBroadcastSmsConfigInfo> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      GsmBroadcastSmsConfigInfo.writeVectorToParcel(hwParcel2, param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(79, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setGsmBroadcastConfigResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(80, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setGsmBroadcastActivationResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(81, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCdmaBroadcastConfigResponse(RadioResponseInfo param1RadioResponseInfo, ArrayList<CdmaBroadcastSmsConfigInfo> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      CdmaBroadcastSmsConfigInfo.writeVectorToParcel(hwParcel2, param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(82, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setCdmaBroadcastConfigResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(83, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setCdmaBroadcastActivationResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(84, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCDMASubscriptionResponse(RadioResponseInfo param1RadioResponseInfo, String param1String1, String param1String2, String param1String3, String param1String4, String param1String5) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      hwParcel2.writeString(param1String1);
      hwParcel2.writeString(param1String2);
      hwParcel2.writeString(param1String3);
      hwParcel2.writeString(param1String4);
      hwParcel2.writeString(param1String5);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(85, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void writeSmsToRuimResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(86, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void deleteSmsOnRuimResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(87, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getDeviceIdentityResponse(RadioResponseInfo param1RadioResponseInfo, String param1String1, String param1String2, String param1String3, String param1String4) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      hwParcel2.writeString(param1String1);
      hwParcel2.writeString(param1String2);
      hwParcel2.writeString(param1String3);
      hwParcel2.writeString(param1String4);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(88, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void exitEmergencyCallbackModeResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(89, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getSmscAddressResponse(RadioResponseInfo param1RadioResponseInfo, String param1String) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      hwParcel2.writeString(param1String);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(90, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setSmscAddressResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(91, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void reportSmsMemoryStatusResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(92, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void reportStkServiceIsRunningResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(93, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCdmaSubscriptionSourceResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(94, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void requestIsimAuthenticationResponse(RadioResponseInfo param1RadioResponseInfo, String param1String) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      hwParcel2.writeString(param1String);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(95, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void acknowledgeIncomingGsmSmsWithPduResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(96, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendEnvelopeWithStatusResponse(RadioResponseInfo param1RadioResponseInfo, IccIoResult param1IccIoResult) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1IccIoResult.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(97, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void getVoiceRadioTechnologyResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(98, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCellInfoListResponse(RadioResponseInfo param1RadioResponseInfo, ArrayList<CellInfo> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      CellInfo.writeVectorToParcel(hwParcel2, param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(99, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setCellInfoListRateResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(100, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setInitialAttachApnResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(101, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getImsRegistrationStateResponse(RadioResponseInfo param1RadioResponseInfo, boolean param1Boolean, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeBool(param1Boolean);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(102, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendImsSmsResponse(RadioResponseInfo param1RadioResponseInfo, SendSmsResult param1SendSmsResult) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1SendSmsResult.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(103, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void iccTransmitApduBasicChannelResponse(RadioResponseInfo param1RadioResponseInfo, IccIoResult param1IccIoResult) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1IccIoResult.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(104, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void iccOpenLogicalChannelResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int, ArrayList<Byte> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      hwParcel2.writeInt32(param1Int);
      hwParcel2.writeInt8Vector(param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(105, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void iccCloseLogicalChannelResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(106, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void iccTransmitApduLogicalChannelResponse(RadioResponseInfo param1RadioResponseInfo, IccIoResult param1IccIoResult) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1IccIoResult.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(107, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void nvReadItemResponse(RadioResponseInfo param1RadioResponseInfo, String param1String) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      hwParcel2.writeString(param1String);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(108, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void nvWriteItemResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(109, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void nvWriteCdmaPrlResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(110, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void nvResetConfigResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(111, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setUiccSubscriptionResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(112, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setDataAllowedResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(113, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getHardwareConfigResponse(RadioResponseInfo param1RadioResponseInfo, ArrayList<HardwareConfig> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      HardwareConfig.writeVectorToParcel(hwParcel2, param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(114, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void requestIccSimAuthenticationResponse(RadioResponseInfo param1RadioResponseInfo, IccIoResult param1IccIoResult) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1IccIoResult.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(115, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setDataProfileResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(116, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void requestShutdownResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(117, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getRadioCapabilityResponse(RadioResponseInfo param1RadioResponseInfo, RadioCapability param1RadioCapability) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1RadioCapability.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(118, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setRadioCapabilityResponse(RadioResponseInfo param1RadioResponseInfo, RadioCapability param1RadioCapability) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1RadioCapability.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(119, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void startLceServiceResponse(RadioResponseInfo param1RadioResponseInfo, LceStatusInfo param1LceStatusInfo) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1LceStatusInfo.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(120, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void stopLceServiceResponse(RadioResponseInfo param1RadioResponseInfo, LceStatusInfo param1LceStatusInfo) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1LceStatusInfo.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(121, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void pullLceDataResponse(RadioResponseInfo param1RadioResponseInfo, LceDataInfo param1LceDataInfo) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1LceDataInfo.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(122, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void getModemActivityInfoResponse(RadioResponseInfo param1RadioResponseInfo, ActivityStatsInfo param1ActivityStatsInfo) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1ActivityStatsInfo.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(123, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setAllowedCarriersResponse(RadioResponseInfo param1RadioResponseInfo, int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(124, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getAllowedCarriersResponse(RadioResponseInfo param1RadioResponseInfo, boolean param1Boolean, CarrierRestrictions param1CarrierRestrictions) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      hwParcel2.writeBool(param1Boolean);
      param1CarrierRestrictions.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(125, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void sendDeviceStateResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(126, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setIndicationFilterResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(127, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setSimCardPowerResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(128, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void acknowledgeRequest(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadioResponse");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(129, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setCarrierInfoForImsiEncryptionResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.1::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(130, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setSimCardPowerResponse_1_1(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.1::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(131, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void startNetworkScanResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.1::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(132, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void stopNetworkScanResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.1::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(133, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void startKeepaliveResponse(RadioResponseInfo param1RadioResponseInfo, KeepaliveStatus param1KeepaliveStatus) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.1::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(hwParcel2);
      param1KeepaliveStatus.writeToParcel(hwParcel2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(134, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void stopKeepaliveResponse(RadioResponseInfo param1RadioResponseInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.1::IRadioResponse");
      param1RadioResponseInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(135, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<String> interfaceChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256067662, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readStringVector();
      } finally {
        hwParcel.release();
      } 
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hidl.base@1.0::IBase");
      hwParcel2.writeNativeHandle(param1NativeHandle);
      hwParcel2.writeStringVector(param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(256131655, hwParcel2, hwParcel1, 0);
        hwParcel1.verifySuccess();
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public String interfaceDescriptor() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256136003, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<byte[]> getHashChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256398152, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ArrayList<byte[]> arrayList = new ArrayList();
        this();
        HwBlob hwBlob = hwParcel.readBuffer(16L);
        int i = hwBlob.getInt32(8L);
        long l1 = (i * 32);
        long l2 = hwBlob.handle();
        hwBlob = hwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
        arrayList.clear();
        for (byte b = 0; b < i; b++) {
          byte[] arrayOfByte = new byte[32];
          l2 = (b * 32);
          hwBlob.copyToInt8Array(l2, arrayOfByte, 32);
          arrayList.add(arrayOfByte);
        } 
        return arrayList;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setHALInstrumentation() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256462420, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) throws RemoteException {
      return this.mRemote.linkToDeath(param1DeathRecipient, param1Long);
    }
    
    public void ping() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256921159, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public DebugInfo getDebugInfo() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257049926, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        DebugInfo debugInfo = new DebugInfo();
        this();
        debugInfo.readFromParcel(hwParcel);
        return debugInfo;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void notifySyspropsChanged() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257120595, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) throws RemoteException {
      return this.mRemote.unlinkToDeath(param1DeathRecipient);
    }
  }
  
  class Stub extends HwBinder implements IRadioResponse {
    public IHwBinder asBinder() {
      return this;
    }
    
    public final ArrayList<String> interfaceChain() {
      return new ArrayList<>(Arrays.asList(new String[] { "android.hardware.radio@1.1::IRadioResponse", "android.hardware.radio@1.0::IRadioResponse", "android.hidl.base@1.0::IBase" }));
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) {}
    
    public final String interfaceDescriptor() {
      return "android.hardware.radio@1.1::IRadioResponse";
    }
    
    public final ArrayList<byte[]> getHashChain() {
      return (ArrayList)new ArrayList<>(Arrays.asList((byte[])new byte[][] { { 
                5, -86, 61, -26, 19, 10, -105, -120, -3, -74, 
                -12, -45, -52, 87, -61, -22, -112, -16, 103, -25, 
                122, 94, 9, -42, -89, 114, -20, Byte.MAX_VALUE, 107, -54, 
                51, -46 }, { 
                29, 74, 87, 118, 97, 76, 8, -75, -41, -108, 
                -91, -20, 90, -80, 70, -105, 38, 12, -67, 75, 
                52, 65, -43, -109, 92, -43, 62, -25, 29, 25, 
                -38, 2 }, { 
                -20, Byte.MAX_VALUE, -41, -98, -48, 45, -6, -123, -68, 73, 
                -108, 38, -83, -82, 62, -66, 35, -17, 5, 36, 
                -13, -51, 105, 87, 19, -109, 36, -72, 59, 24, 
                -54, 76 } }));
    }
    
    public final void setHALInstrumentation() {}
    
    public final boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) {
      return true;
    }
    
    public final void ping() {}
    
    public final DebugInfo getDebugInfo() {
      DebugInfo debugInfo = new DebugInfo();
      debugInfo.pid = HidlSupport.getPidIfSharable();
      debugInfo.ptr = 0L;
      debugInfo.arch = 0;
      return debugInfo;
    }
    
    public final void notifySyspropsChanged() {
      HwBinder.enableInstrumentation();
    }
    
    public final boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) {
      return true;
    }
    
    public IHwInterface queryLocalInterface(String param1String) {
      if ("android.hardware.radio@1.1::IRadioResponse".equals(param1String))
        return this; 
      return null;
    }
    
    public void registerAsService(String param1String) throws RemoteException {
      registerService(param1String);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(interfaceDescriptor());
      stringBuilder.append("@Stub");
      return stringBuilder.toString();
    }
    
    public void onTransact(int param1Int1, HwParcel param1HwParcel1, HwParcel param1HwParcel2, int param1Int2) throws RemoteException {
      DebugInfo debugInfo;
      HwBlob hwBlob1;
      String str6;
      ArrayList<String> arrayList5;
      String str5;
      ArrayList<Byte> arrayList4;
      String str4;
      ArrayList<CdmaBroadcastSmsConfigInfo> arrayList3;
      String str3;
      ArrayList<Integer> arrayList2;
      String str2;
      ArrayList<OperatorInfo> arrayList1;
      String str1;
      ArrayList<Call> arrayList;
      RadioResponseInfo radioResponseInfo11;
      KeepaliveStatus keepaliveStatus;
      RadioResponseInfo radioResponseInfo10;
      CarrierRestrictions carrierRestrictions;
      RadioResponseInfo radioResponseInfo9;
      LceStatusInfo lceStatusInfo1;
      RadioCapability radioCapability;
      RadioResponseInfo radioResponseInfo8;
      IccIoResult iccIoResult3;
      RadioResponseInfo radioResponseInfo7;
      SendSmsResult sendSmsResult3;
      RadioResponseInfo radioResponseInfo6;
      IccIoResult iccIoResult2;
      RadioResponseInfo radioResponseInfo5;
      SendSmsResult sendSmsResult2;
      RadioResponseInfo radioResponseInfo4;
      IccIoResult iccIoResult1;
      SetupDataCallResult setupDataCallResult;
      RadioResponseInfo radioResponseInfo3;
      SendSmsResult sendSmsResult1;
      RadioResponseInfo radioResponseInfo2;
      String str7;
      DataRegStateResult dataRegStateResult;
      ArrayList<byte[]> arrayList6;
      String str8;
      HwBlob hwBlob2;
      NativeHandle nativeHandle;
      RadioResponseInfo radioResponseInfo16;
      ActivityStatsInfo activityStatsInfo;
      LceDataInfo lceDataInfo;
      LceStatusInfo lceStatusInfo2;
      RadioResponseInfo radioResponseInfo15;
      IccIoResult iccIoResult4;
      RadioResponseInfo radioResponseInfo14;
      String str9;
      RadioResponseInfo radioResponseInfo13;
      SendSmsResult sendSmsResult4;
      RadioResponseInfo radioResponseInfo12;
      VoiceRegStateResult voiceRegStateResult;
      SignalStrength signalStrength;
      LastCallFailCauseInfo lastCallFailCauseInfo;
      String str10;
      boolean bool;
      String str11;
      switch (param1Int1) {
        default:
          switch (param1Int1) {
            default:
              return;
            case 257120595:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              notifySyspropsChanged();
            case 257049926:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              debugInfo = getDebugInfo();
              param1HwParcel2.writeStatus(0);
              debugInfo.writeToParcel(param1HwParcel2);
              param1HwParcel2.send();
            case 256921159:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              ping();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256462420:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              setHALInstrumentation();
            case 256398152:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              arrayList6 = getHashChain();
              param1HwParcel2.writeStatus(0);
              hwBlob2 = new HwBlob(16);
              param1Int2 = arrayList6.size();
              hwBlob2.putInt32(8L, param1Int2);
              hwBlob2.putBool(12L, false);
              hwBlob1 = new HwBlob(param1Int2 * 32);
              for (param1Int1 = 0; param1Int1 < param1Int2; ) {
                long l = (param1Int1 * 32);
                byte[] arrayOfByte = arrayList6.get(param1Int1);
                if (arrayOfByte != null && arrayOfByte.length == 32) {
                  hwBlob1.putInt8Array(l, arrayOfByte);
                  param1Int1++;
                } 
                throw new IllegalArgumentException("Array element is not of the expected length");
              } 
              hwBlob2.putBlob(0L, hwBlob1);
              param1HwParcel2.writeBuffer(hwBlob2);
              param1HwParcel2.send();
            case 256136003:
              hwBlob1.enforceInterface("android.hidl.base@1.0::IBase");
              str6 = interfaceDescriptor();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.writeString(str6);
              param1HwParcel2.send();
            case 256131655:
              str6.enforceInterface("android.hidl.base@1.0::IBase");
              nativeHandle = str6.readNativeHandle();
              arrayList5 = str6.readStringVector();
              debug(nativeHandle, arrayList5);
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256067662:
              break;
          } 
          arrayList5.enforceInterface("android.hidl.base@1.0::IBase");
          arrayList5 = interfaceChain();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeStringVector(arrayList5);
          param1HwParcel2.send();
        case 135:
          arrayList5.enforceInterface("android.hardware.radio@1.1::IRadioResponse");
          radioResponseInfo11 = new RadioResponseInfo();
          radioResponseInfo11.readFromParcel((HwParcel)arrayList5);
          stopKeepaliveResponse(radioResponseInfo11);
        case 134:
          arrayList5.enforceInterface("android.hardware.radio@1.1::IRadioResponse");
          radioResponseInfo16 = new RadioResponseInfo();
          radioResponseInfo16.readFromParcel((HwParcel)arrayList5);
          keepaliveStatus = new KeepaliveStatus();
          keepaliveStatus.readFromParcel((HwParcel)arrayList5);
          startKeepaliveResponse(radioResponseInfo16, keepaliveStatus);
        case 133:
          arrayList5.enforceInterface("android.hardware.radio@1.1::IRadioResponse");
          radioResponseInfo10 = new RadioResponseInfo();
          radioResponseInfo10.readFromParcel((HwParcel)arrayList5);
          stopNetworkScanResponse(radioResponseInfo10);
        case 132:
          arrayList5.enforceInterface("android.hardware.radio@1.1::IRadioResponse");
          radioResponseInfo10 = new RadioResponseInfo();
          radioResponseInfo10.readFromParcel((HwParcel)arrayList5);
          startNetworkScanResponse(radioResponseInfo10);
        case 131:
          arrayList5.enforceInterface("android.hardware.radio@1.1::IRadioResponse");
          radioResponseInfo10 = new RadioResponseInfo();
          radioResponseInfo10.readFromParcel((HwParcel)arrayList5);
          setSimCardPowerResponse_1_1(radioResponseInfo10);
        case 130:
          arrayList5.enforceInterface("android.hardware.radio@1.1::IRadioResponse");
          radioResponseInfo10 = new RadioResponseInfo();
          radioResponseInfo10.readFromParcel((HwParcel)arrayList5);
          setCarrierInfoForImsiEncryptionResponse(radioResponseInfo10);
        case 129:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          param1Int1 = arrayList5.readInt32();
          acknowledgeRequest(param1Int1);
        case 128:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo10 = new RadioResponseInfo();
          radioResponseInfo10.readFromParcel((HwParcel)arrayList5);
          setSimCardPowerResponse(radioResponseInfo10);
        case 127:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo10 = new RadioResponseInfo();
          radioResponseInfo10.readFromParcel((HwParcel)arrayList5);
          setIndicationFilterResponse(radioResponseInfo10);
        case 126:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo10 = new RadioResponseInfo();
          radioResponseInfo10.readFromParcel((HwParcel)arrayList5);
          sendDeviceStateResponse(radioResponseInfo10);
        case 125:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo16 = new RadioResponseInfo();
          radioResponseInfo16.readFromParcel((HwParcel)arrayList5);
          bool = arrayList5.readBool();
          carrierRestrictions = new CarrierRestrictions();
          carrierRestrictions.readFromParcel((HwParcel)arrayList5);
          getAllowedCarriersResponse(radioResponseInfo16, bool, carrierRestrictions);
        case 124:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo9 = new RadioResponseInfo();
          radioResponseInfo9.readFromParcel((HwParcel)arrayList5);
          param1Int1 = arrayList5.readInt32();
          setAllowedCarriersResponse(radioResponseInfo9, param1Int1);
        case 123:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo9 = new RadioResponseInfo();
          radioResponseInfo9.readFromParcel((HwParcel)arrayList5);
          activityStatsInfo = new ActivityStatsInfo();
          activityStatsInfo.readFromParcel((HwParcel)arrayList5);
          getModemActivityInfoResponse(radioResponseInfo9, activityStatsInfo);
        case 122:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo9 = new RadioResponseInfo();
          radioResponseInfo9.readFromParcel((HwParcel)arrayList5);
          lceDataInfo = new LceDataInfo();
          lceDataInfo.readFromParcel((HwParcel)arrayList5);
          pullLceDataResponse(radioResponseInfo9, lceDataInfo);
        case 121:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo9 = new RadioResponseInfo();
          radioResponseInfo9.readFromParcel((HwParcel)arrayList5);
          lceStatusInfo2 = new LceStatusInfo();
          lceStatusInfo2.readFromParcel((HwParcel)arrayList5);
          stopLceServiceResponse(radioResponseInfo9, lceStatusInfo2);
        case 120:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo15 = new RadioResponseInfo();
          radioResponseInfo15.readFromParcel((HwParcel)arrayList5);
          lceStatusInfo1 = new LceStatusInfo();
          lceStatusInfo1.readFromParcel((HwParcel)arrayList5);
          startLceServiceResponse(radioResponseInfo15, lceStatusInfo1);
        case 119:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo15 = new RadioResponseInfo();
          radioResponseInfo15.readFromParcel((HwParcel)arrayList5);
          radioCapability = new RadioCapability();
          radioCapability.readFromParcel((HwParcel)arrayList5);
          setRadioCapabilityResponse(radioResponseInfo15, radioCapability);
        case 118:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo15 = new RadioResponseInfo();
          radioResponseInfo15.readFromParcel((HwParcel)arrayList5);
          radioCapability = new RadioCapability();
          radioCapability.readFromParcel((HwParcel)arrayList5);
          getRadioCapabilityResponse(radioResponseInfo15, radioCapability);
        case 117:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo8 = new RadioResponseInfo();
          radioResponseInfo8.readFromParcel((HwParcel)arrayList5);
          requestShutdownResponse(radioResponseInfo8);
        case 116:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo8 = new RadioResponseInfo();
          radioResponseInfo8.readFromParcel((HwParcel)arrayList5);
          setDataProfileResponse(radioResponseInfo8);
        case 115:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo15 = new RadioResponseInfo();
          radioResponseInfo15.readFromParcel((HwParcel)arrayList5);
          iccIoResult3 = new IccIoResult();
          iccIoResult3.readFromParcel((HwParcel)arrayList5);
          requestIccSimAuthenticationResponse(radioResponseInfo15, iccIoResult3);
        case 114:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo7 = new RadioResponseInfo();
          radioResponseInfo7.readFromParcel((HwParcel)arrayList5);
          arrayList5 = (ArrayList)HardwareConfig.readVectorFromParcel((HwParcel)arrayList5);
          getHardwareConfigResponse(radioResponseInfo7, (ArrayList)arrayList5);
        case 113:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo7 = new RadioResponseInfo();
          radioResponseInfo7.readFromParcel((HwParcel)arrayList5);
          setDataAllowedResponse(radioResponseInfo7);
        case 112:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo7 = new RadioResponseInfo();
          radioResponseInfo7.readFromParcel((HwParcel)arrayList5);
          setUiccSubscriptionResponse(radioResponseInfo7);
        case 111:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo7 = new RadioResponseInfo();
          radioResponseInfo7.readFromParcel((HwParcel)arrayList5);
          nvResetConfigResponse(radioResponseInfo7);
        case 110:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo7 = new RadioResponseInfo();
          radioResponseInfo7.readFromParcel((HwParcel)arrayList5);
          nvWriteCdmaPrlResponse(radioResponseInfo7);
        case 109:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo7 = new RadioResponseInfo();
          radioResponseInfo7.readFromParcel((HwParcel)arrayList5);
          nvWriteItemResponse(radioResponseInfo7);
        case 108:
          arrayList5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo7 = new RadioResponseInfo();
          radioResponseInfo7.readFromParcel((HwParcel)arrayList5);
          str5 = arrayList5.readString();
          nvReadItemResponse(radioResponseInfo7, str5);
        case 107:
          str5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo7 = new RadioResponseInfo();
          radioResponseInfo7.readFromParcel((HwParcel)str5);
          iccIoResult4 = new IccIoResult();
          iccIoResult4.readFromParcel((HwParcel)str5);
          iccTransmitApduLogicalChannelResponse(radioResponseInfo7, iccIoResult4);
        case 106:
          str5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo7 = new RadioResponseInfo();
          radioResponseInfo7.readFromParcel((HwParcel)str5);
          iccCloseLogicalChannelResponse(radioResponseInfo7);
        case 105:
          str5.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo7 = new RadioResponseInfo();
          radioResponseInfo7.readFromParcel((HwParcel)str5);
          param1Int1 = str5.readInt32();
          arrayList4 = str5.readInt8Vector();
          iccOpenLogicalChannelResponse(radioResponseInfo7, param1Int1, arrayList4);
        case 104:
          arrayList4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo7 = new RadioResponseInfo();
          radioResponseInfo7.readFromParcel((HwParcel)arrayList4);
          iccIoResult4 = new IccIoResult();
          iccIoResult4.readFromParcel((HwParcel)arrayList4);
          iccTransmitApduBasicChannelResponse(radioResponseInfo7, iccIoResult4);
        case 103:
          arrayList4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo14 = new RadioResponseInfo();
          radioResponseInfo14.readFromParcel((HwParcel)arrayList4);
          sendSmsResult3 = new SendSmsResult();
          sendSmsResult3.readFromParcel((HwParcel)arrayList4);
          sendImsSmsResponse(radioResponseInfo14, sendSmsResult3);
        case 102:
          arrayList4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo6 = new RadioResponseInfo();
          radioResponseInfo6.readFromParcel((HwParcel)arrayList4);
          bool = arrayList4.readBool();
          param1Int1 = arrayList4.readInt32();
          getImsRegistrationStateResponse(radioResponseInfo6, bool, param1Int1);
        case 101:
          arrayList4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo6 = new RadioResponseInfo();
          radioResponseInfo6.readFromParcel((HwParcel)arrayList4);
          setInitialAttachApnResponse(radioResponseInfo6);
        case 100:
          arrayList4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo6 = new RadioResponseInfo();
          radioResponseInfo6.readFromParcel((HwParcel)arrayList4);
          setCellInfoListRateResponse(radioResponseInfo6);
        case 99:
          arrayList4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo6 = new RadioResponseInfo();
          radioResponseInfo6.readFromParcel((HwParcel)arrayList4);
          arrayList4 = (ArrayList)CellInfo.readVectorFromParcel((HwParcel)arrayList4);
          getCellInfoListResponse(radioResponseInfo6, (ArrayList)arrayList4);
        case 98:
          arrayList4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo6 = new RadioResponseInfo();
          radioResponseInfo6.readFromParcel((HwParcel)arrayList4);
          param1Int1 = arrayList4.readInt32();
          getVoiceRadioTechnologyResponse(radioResponseInfo6, param1Int1);
        case 97:
          arrayList4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo14 = new RadioResponseInfo();
          radioResponseInfo14.readFromParcel((HwParcel)arrayList4);
          iccIoResult2 = new IccIoResult();
          iccIoResult2.readFromParcel((HwParcel)arrayList4);
          sendEnvelopeWithStatusResponse(radioResponseInfo14, iccIoResult2);
        case 96:
          arrayList4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)arrayList4);
          acknowledgeIncomingGsmSmsWithPduResponse(radioResponseInfo5);
        case 95:
          arrayList4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)arrayList4);
          str4 = arrayList4.readString();
          requestIsimAuthenticationResponse(radioResponseInfo5, str4);
        case 94:
          str4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)str4);
          param1Int1 = str4.readInt32();
          getCdmaSubscriptionSourceResponse(radioResponseInfo5, param1Int1);
        case 93:
          str4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)str4);
          reportStkServiceIsRunningResponse(radioResponseInfo5);
        case 92:
          str4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)str4);
          reportSmsMemoryStatusResponse(radioResponseInfo5);
        case 91:
          str4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)str4);
          setSmscAddressResponse(radioResponseInfo5);
        case 90:
          str4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)str4);
          str4 = str4.readString();
          getSmscAddressResponse(radioResponseInfo5, str4);
        case 89:
          str4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)str4);
          exitEmergencyCallbackModeResponse(radioResponseInfo5);
        case 88:
          str4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)str4);
          str10 = str4.readString();
          str8 = str4.readString();
          str9 = str4.readString();
          str4 = str4.readString();
          getDeviceIdentityResponse(radioResponseInfo5, str10, str8, str9, str4);
        case 87:
          str4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)str4);
          deleteSmsOnRuimResponse(radioResponseInfo5);
        case 86:
          str4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)str4);
          param1Int1 = str4.readInt32();
          writeSmsToRuimResponse(radioResponseInfo5, param1Int1);
        case 85:
          str4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)str4);
          str10 = str4.readString();
          str9 = str4.readString();
          str11 = str4.readString();
          str8 = str4.readString();
          str4 = str4.readString();
          getCDMASubscriptionResponse(radioResponseInfo5, str10, str9, str11, str8, str4);
        case 84:
          str4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)str4);
          setCdmaBroadcastActivationResponse(radioResponseInfo5);
        case 83:
          str4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)str4);
          setCdmaBroadcastConfigResponse(radioResponseInfo5);
        case 82:
          str4.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)str4);
          arrayList3 = CdmaBroadcastSmsConfigInfo.readVectorFromParcel((HwParcel)str4);
          getCdmaBroadcastConfigResponse(radioResponseInfo5, arrayList3);
        case 81:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)arrayList3);
          setGsmBroadcastActivationResponse(radioResponseInfo5);
        case 80:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)arrayList3);
          setGsmBroadcastConfigResponse(radioResponseInfo5);
        case 79:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)arrayList3);
          arrayList3 = (ArrayList)GsmBroadcastSmsConfigInfo.readVectorFromParcel((HwParcel)arrayList3);
          getGsmBroadcastConfigResponse(radioResponseInfo5, (ArrayList)arrayList3);
        case 78:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo5 = new RadioResponseInfo();
          radioResponseInfo5.readFromParcel((HwParcel)arrayList3);
          acknowledgeLastIncomingCdmaSmsResponse(radioResponseInfo5);
        case 77:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo13 = new RadioResponseInfo();
          radioResponseInfo13.readFromParcel((HwParcel)arrayList3);
          sendSmsResult2 = new SendSmsResult();
          sendSmsResult2.readFromParcel((HwParcel)arrayList3);
          sendCdmaSmsResponse(radioResponseInfo13, sendSmsResult2);
        case 76:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          sendBurstDtmfResponse(radioResponseInfo4);
        case 75:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          sendCDMAFeatureCodeResponse(radioResponseInfo4);
        case 74:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          bool = arrayList3.readBool();
          getPreferredVoicePrivacyResponse(radioResponseInfo4, bool);
        case 73:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          setPreferredVoicePrivacyResponse(radioResponseInfo4);
        case 72:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          param1Int1 = arrayList3.readInt32();
          getTTYModeResponse(radioResponseInfo4, param1Int1);
        case 71:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          setTTYModeResponse(radioResponseInfo4);
        case 70:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          param1Int1 = arrayList3.readInt32();
          getCdmaRoamingPreferenceResponse(radioResponseInfo4, param1Int1);
        case 69:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          setCdmaRoamingPreferenceResponse(radioResponseInfo4);
        case 68:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          setCdmaSubscriptionSourceResponse(radioResponseInfo4);
        case 67:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          setLocationUpdatesResponse(radioResponseInfo4);
        case 66:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          arrayList3 = (ArrayList)NeighboringCell.readVectorFromParcel((HwParcel)arrayList3);
          getNeighboringCidsResponse(radioResponseInfo4, (ArrayList)arrayList3);
        case 65:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          param1Int1 = arrayList3.readInt32();
          getPreferredNetworkTypeResponse(radioResponseInfo4, param1Int1);
        case 64:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          setPreferredNetworkTypeResponse(radioResponseInfo4);
        case 63:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          explicitCallTransferResponse(radioResponseInfo4);
        case 62:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          handleStkCallSetupRequestFromSimResponse(radioResponseInfo4);
        case 61:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          sendTerminalResponseToSimResponse(radioResponseInfo4);
        case 60:
          arrayList3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList3);
          str3 = arrayList3.readString();
          sendEnvelopeResponse(radioResponseInfo4, str3);
        case 59:
          str3.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)str3);
          arrayList2 = str3.readInt32Vector();
          getAvailableBandModesResponse(radioResponseInfo4, arrayList2);
        case 58:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList2);
          setBandModeResponse(radioResponseInfo4);
        case 57:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList2);
          deleteSmsOnSimResponse(radioResponseInfo4);
        case 56:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList2);
          param1Int1 = arrayList2.readInt32();
          writeSmsToSimResponse(radioResponseInfo4, param1Int1);
        case 55:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList2);
          setSuppServiceNotificationsResponse(radioResponseInfo4);
        case 54:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList2);
          arrayList2 = (ArrayList)SetupDataCallResult.readVectorFromParcel((HwParcel)arrayList2);
          getDataCallListResponse(radioResponseInfo4, (ArrayList)arrayList2);
        case 53:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList2);
          param1Int1 = arrayList2.readInt32();
          getClipResponse(radioResponseInfo4, param1Int1);
        case 52:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList2);
          bool = arrayList2.readBool();
          getMuteResponse(radioResponseInfo4, bool);
        case 51:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList2);
          setMuteResponse(radioResponseInfo4);
        case 50:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList2);
          separateConnectionResponse(radioResponseInfo4);
        case 49:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList2);
          str2 = arrayList2.readString();
          getBasebandVersionResponse(radioResponseInfo4, str2);
        case 48:
          str2.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)str2);
          stopDtmfResponse(radioResponseInfo4);
        case 47:
          str2.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)str2);
          startDtmfResponse(radioResponseInfo4);
        case 46:
          str2.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)str2);
          arrayList1 = OperatorInfo.readVectorFromParcel((HwParcel)str2);
          getAvailableNetworksResponse(radioResponseInfo4, arrayList1);
        case 45:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          setNetworkSelectionModeManualResponse(radioResponseInfo4);
        case 44:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          setNetworkSelectionModeAutomaticResponse(radioResponseInfo4);
        case 43:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          bool = arrayList1.readBool();
          getNetworkSelectionModeResponse(radioResponseInfo4, bool);
        case 42:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          setBarringPasswordResponse(radioResponseInfo4);
        case 41:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          param1Int1 = arrayList1.readInt32();
          setFacilityLockForAppResponse(radioResponseInfo4, param1Int1);
        case 40:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          param1Int1 = arrayList1.readInt32();
          getFacilityLockForAppResponse(radioResponseInfo4, param1Int1);
        case 39:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          deactivateDataCallResponse(radioResponseInfo4);
        case 38:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          acceptCallResponse(radioResponseInfo4);
        case 37:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          acknowledgeLastIncomingGsmSmsResponse(radioResponseInfo4);
        case 36:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          setCallWaitingResponse(radioResponseInfo4);
        case 35:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          bool = arrayList1.readBool();
          param1Int1 = arrayList1.readInt32();
          getCallWaitingResponse(radioResponseInfo4, bool, param1Int1);
        case 34:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          setCallForwardResponse(radioResponseInfo4);
        case 33:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          arrayList1 = (ArrayList)CallForwardInfo.readVectorFromParcel((HwParcel)arrayList1);
          getCallForwardStatusResponse(radioResponseInfo4, (ArrayList)arrayList1);
        case 32:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          setClirResponse(radioResponseInfo4);
        case 31:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          param1Int1 = arrayList1.readInt32();
          param1Int2 = arrayList1.readInt32();
          getClirResponse(radioResponseInfo4, param1Int1, param1Int2);
        case 30:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          cancelPendingUssdResponse(radioResponseInfo4);
        case 29:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo4 = new RadioResponseInfo();
          radioResponseInfo4.readFromParcel((HwParcel)arrayList1);
          sendUssdResponse(radioResponseInfo4);
        case 28:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo13 = new RadioResponseInfo();
          radioResponseInfo13.readFromParcel((HwParcel)arrayList1);
          iccIoResult1 = new IccIoResult();
          iccIoResult1.readFromParcel((HwParcel)arrayList1);
          iccIOForAppResponse(radioResponseInfo13, iccIoResult1);
        case 27:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo13 = new RadioResponseInfo();
          radioResponseInfo13.readFromParcel((HwParcel)arrayList1);
          setupDataCallResult = new SetupDataCallResult();
          setupDataCallResult.readFromParcel((HwParcel)arrayList1);
          setupDataCallResponse(radioResponseInfo13, setupDataCallResult);
        case 26:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo3 = new RadioResponseInfo();
          radioResponseInfo3.readFromParcel((HwParcel)arrayList1);
          sendSmsResult4 = new SendSmsResult();
          sendSmsResult4.readFromParcel((HwParcel)arrayList1);
          sendSMSExpectMoreResponse(radioResponseInfo3, sendSmsResult4);
        case 25:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo12 = new RadioResponseInfo();
          radioResponseInfo12.readFromParcel((HwParcel)arrayList1);
          sendSmsResult1 = new SendSmsResult();
          sendSmsResult1.readFromParcel((HwParcel)arrayList1);
          sendSmsResponse(radioResponseInfo12, sendSmsResult1);
        case 24:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo2 = new RadioResponseInfo();
          radioResponseInfo2.readFromParcel((HwParcel)arrayList1);
          sendDtmfResponse(radioResponseInfo2);
        case 23:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo2 = new RadioResponseInfo();
          radioResponseInfo2.readFromParcel((HwParcel)arrayList1);
          setRadioPowerResponse(radioResponseInfo2);
        case 22:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo12 = new RadioResponseInfo();
          radioResponseInfo12.readFromParcel((HwParcel)arrayList1);
          str8 = arrayList1.readString();
          str7 = arrayList1.readString();
          str1 = arrayList1.readString();
          getOperatorResponse(radioResponseInfo12, str8, str7, str1);
        case 21:
          str1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo12 = new RadioResponseInfo();
          radioResponseInfo12.readFromParcel((HwParcel)str1);
          dataRegStateResult = new DataRegStateResult();
          dataRegStateResult.readFromParcel((HwParcel)str1);
          getDataRegistrationStateResponse(radioResponseInfo12, dataRegStateResult);
        case 20:
          str1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)str1);
          voiceRegStateResult = new VoiceRegStateResult();
          voiceRegStateResult.readFromParcel((HwParcel)str1);
          getVoiceRegistrationStateResponse(radioResponseInfo1, voiceRegStateResult);
        case 19:
          str1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)str1);
          signalStrength = new SignalStrength();
          signalStrength.readFromParcel((HwParcel)str1);
          getSignalStrengthResponse(radioResponseInfo1, signalStrength);
        case 18:
          str1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)str1);
          lastCallFailCauseInfo = new LastCallFailCauseInfo();
          lastCallFailCauseInfo.readFromParcel((HwParcel)str1);
          getLastCallFailCauseResponse(radioResponseInfo1, lastCallFailCauseInfo);
        case 17:
          str1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)str1);
          rejectCallResponse(radioResponseInfo1);
        case 16:
          str1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)str1);
          conferenceResponse(radioResponseInfo1);
        case 15:
          str1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)str1);
          switchWaitingOrHoldingAndActiveResponse(radioResponseInfo1);
        case 14:
          str1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)str1);
          hangupForegroundResumeBackgroundResponse(radioResponseInfo1);
        case 13:
          str1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)str1);
          hangupWaitingOrBackgroundResponse(radioResponseInfo1);
        case 12:
          str1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)str1);
          hangupConnectionResponse(radioResponseInfo1);
        case 11:
          str1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)str1);
          str1 = str1.readString();
          getIMSIForAppResponse(radioResponseInfo1, str1);
        case 10:
          str1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)str1);
          dialResponse(radioResponseInfo1);
        case 9:
          str1.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)str1);
          arrayList = Call.readVectorFromParcel((HwParcel)str1);
          getCurrentCallsResponse(radioResponseInfo1, arrayList);
        case 8:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)arrayList);
          param1Int1 = arrayList.readInt32();
          supplyNetworkDepersonalizationResponse(radioResponseInfo1, param1Int1);
        case 7:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)arrayList);
          param1Int1 = arrayList.readInt32();
          changeIccPin2ForAppResponse(radioResponseInfo1, param1Int1);
        case 6:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)arrayList);
          param1Int1 = arrayList.readInt32();
          changeIccPinForAppResponse(radioResponseInfo1, param1Int1);
        case 5:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)arrayList);
          param1Int1 = arrayList.readInt32();
          supplyIccPuk2ForAppResponse(radioResponseInfo1, param1Int1);
        case 4:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)arrayList);
          param1Int1 = arrayList.readInt32();
          supplyIccPin2ForAppResponse(radioResponseInfo1, param1Int1);
        case 3:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)arrayList);
          param1Int1 = arrayList.readInt32();
          supplyIccPukForAppResponse(radioResponseInfo1, param1Int1);
        case 2:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
          radioResponseInfo1 = new RadioResponseInfo();
          radioResponseInfo1.readFromParcel((HwParcel)arrayList);
          param1Int1 = arrayList.readInt32();
          supplyIccPinForAppResponse(radioResponseInfo1, param1Int1);
        case 1:
          break;
      } 
      arrayList.enforceInterface("android.hardware.radio@1.0::IRadioResponse");
      RadioResponseInfo radioResponseInfo1 = new RadioResponseInfo();
      radioResponseInfo1.readFromParcel((HwParcel)arrayList);
      CardStatus cardStatus = new CardStatus();
      cardStatus.readFromParcel((HwParcel)arrayList);
      getIccCardStatusResponse(radioResponseInfo1, cardStatus);
    }
  }
}
