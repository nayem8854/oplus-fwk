package android.hardware.radio.V1_1;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class ImsiEncryptionInfo {
  public String mcc = new String();
  
  public String mnc = new String();
  
  public ArrayList<Byte> carrierKey = new ArrayList<>();
  
  public String keyIdentifier = new String();
  
  public long expirationTime = 0L;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != ImsiEncryptionInfo.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.mcc, ((ImsiEncryptionInfo)paramObject).mcc))
      return false; 
    if (!HidlSupport.deepEquals(this.mnc, ((ImsiEncryptionInfo)paramObject).mnc))
      return false; 
    if (!HidlSupport.deepEquals(this.carrierKey, ((ImsiEncryptionInfo)paramObject).carrierKey))
      return false; 
    if (!HidlSupport.deepEquals(this.keyIdentifier, ((ImsiEncryptionInfo)paramObject).keyIdentifier))
      return false; 
    if (this.expirationTime != ((ImsiEncryptionInfo)paramObject).expirationTime)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    String str2 = this.mcc;
    int i = HidlSupport.deepHashCode(str2);
    str2 = this.mnc;
    int j = HidlSupport.deepHashCode(str2);
    ArrayList<Byte> arrayList = this.carrierKey;
    int k = HidlSupport.deepHashCode(arrayList);
    String str1 = this.keyIdentifier;
    int m = HidlSupport.deepHashCode(str1);
    long l = this.expirationTime;
    int n = HidlSupport.deepHashCode(Long.valueOf(l));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".mcc = ");
    stringBuilder.append(this.mcc);
    stringBuilder.append(", .mnc = ");
    stringBuilder.append(this.mnc);
    stringBuilder.append(", .carrierKey = ");
    stringBuilder.append(this.carrierKey);
    stringBuilder.append(", .keyIdentifier = ");
    stringBuilder.append(this.keyIdentifier);
    stringBuilder.append(", .expirationTime = ");
    stringBuilder.append(this.expirationTime);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(72L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<ImsiEncryptionInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<ImsiEncryptionInfo> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 72);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      ImsiEncryptionInfo imsiEncryptionInfo = new ImsiEncryptionInfo();
      imsiEncryptionInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 72));
      arrayList.add(imsiEncryptionInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    String str2 = paramHwBlob.getString(paramLong + 0L);
    long l1 = ((str2.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, false);
    this.mnc = str2 = paramHwBlob.getString(paramLong + 16L);
    l1 = ((str2.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 16L + 0L, false);
    int i = paramHwBlob.getInt32(paramLong + 32L + 8L);
    l2 = (i * 1);
    l1 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 32L + 0L, true);
    this.carrierKey.clear();
    for (byte b = 0; b < i; b++) {
      byte b1 = hwBlob.getInt8((b * 1));
      this.carrierKey.add(Byte.valueOf(b1));
    } 
    String str1 = paramHwBlob.getString(paramLong + 48L);
    l1 = ((str1.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 48L + 0L, false);
    this.expirationTime = paramHwBlob.getInt64(paramLong + 64L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(72);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<ImsiEncryptionInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 72);
    for (byte b = 0; b < i; b++)
      ((ImsiEncryptionInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 72)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putString(paramLong + 0L, this.mcc);
    paramHwBlob.putString(16L + paramLong, this.mnc);
    int i = this.carrierKey.size();
    paramHwBlob.putInt32(paramLong + 32L + 8L, i);
    paramHwBlob.putBool(paramLong + 32L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 1);
    for (byte b = 0; b < i; b++)
      hwBlob.putInt8((b * 1), ((Byte)this.carrierKey.get(b)).byteValue()); 
    paramHwBlob.putBlob(32L + paramLong + 0L, hwBlob);
    paramHwBlob.putString(48L + paramLong, this.keyIdentifier);
    paramHwBlob.putInt64(64L + paramLong, this.expirationTime);
  }
}
