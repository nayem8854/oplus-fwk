package android.hardware.radio.V1_1;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class NetworkScanRequest {
  public int type = 0;
  
  public int interval = 0;
  
  public ArrayList<RadioAccessSpecifier> specifiers = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != NetworkScanRequest.class)
      return false; 
    paramObject = paramObject;
    if (this.type != ((NetworkScanRequest)paramObject).type)
      return false; 
    if (this.interval != ((NetworkScanRequest)paramObject).interval)
      return false; 
    if (!HidlSupport.deepEquals(this.specifiers, ((NetworkScanRequest)paramObject).specifiers))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.type;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.interval;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    ArrayList<RadioAccessSpecifier> arrayList = this.specifiers;
    int k = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".type = ");
    stringBuilder.append(ScanType.toString(this.type));
    stringBuilder.append(", .interval = ");
    stringBuilder.append(this.interval);
    stringBuilder.append(", .specifiers = ");
    stringBuilder.append(this.specifiers);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(24L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<NetworkScanRequest> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<NetworkScanRequest> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 24);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      NetworkScanRequest networkScanRequest = new NetworkScanRequest();
      networkScanRequest.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 24));
      arrayList.add(networkScanRequest);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.type = paramHwBlob.getInt32(paramLong + 0L);
    this.interval = paramHwBlob.getInt32(paramLong + 4L);
    int i = paramHwBlob.getInt32(paramLong + 8L + 8L);
    long l1 = (i * 72);
    long l2 = paramHwBlob.handle();
    paramHwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, true);
    this.specifiers.clear();
    for (byte b = 0; b < i; b++) {
      RadioAccessSpecifier radioAccessSpecifier = new RadioAccessSpecifier();
      radioAccessSpecifier.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, (b * 72));
      this.specifiers.add(radioAccessSpecifier);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(24);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<NetworkScanRequest> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 24);
    for (byte b = 0; b < i; b++)
      ((NetworkScanRequest)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 24)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.type);
    paramHwBlob.putInt32(4L + paramLong, this.interval);
    int i = this.specifiers.size();
    paramHwBlob.putInt32(paramLong + 8L + 8L, i);
    paramHwBlob.putBool(paramLong + 8L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 72);
    for (byte b = 0; b < i; b++)
      ((RadioAccessSpecifier)this.specifiers.get(b)).writeEmbeddedToBlob(hwBlob, (b * 72)); 
    paramHwBlob.putBlob(8L + paramLong + 0L, hwBlob);
  }
}
