package android.hardware.radio.V1_5;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellIdentityLte {
  public android.hardware.radio.V1_2.CellIdentityLte base = new android.hardware.radio.V1_2.CellIdentityLte();
  
  public ArrayList<String> additionalPlmns = new ArrayList<>();
  
  public OptionalCsgInfo optionalCsgInfo = new OptionalCsgInfo();
  
  public ArrayList<Integer> bands = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellIdentityLte.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.base, ((CellIdentityLte)paramObject).base))
      return false; 
    if (!HidlSupport.deepEquals(this.additionalPlmns, ((CellIdentityLte)paramObject).additionalPlmns))
      return false; 
    if (!HidlSupport.deepEquals(this.optionalCsgInfo, ((CellIdentityLte)paramObject).optionalCsgInfo))
      return false; 
    if (!HidlSupport.deepEquals(this.bands, ((CellIdentityLte)paramObject).bands))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    android.hardware.radio.V1_2.CellIdentityLte cellIdentityLte = this.base;
    int i = HidlSupport.deepHashCode(cellIdentityLte);
    ArrayList<String> arrayList1 = this.additionalPlmns;
    int j = HidlSupport.deepHashCode(arrayList1);
    OptionalCsgInfo optionalCsgInfo = this.optionalCsgInfo;
    int k = HidlSupport.deepHashCode(optionalCsgInfo);
    ArrayList<Integer> arrayList = this.bands;
    int m = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".base = ");
    stringBuilder.append(this.base);
    stringBuilder.append(", .additionalPlmns = ");
    stringBuilder.append(this.additionalPlmns);
    stringBuilder.append(", .optionalCsgInfo = ");
    stringBuilder.append(this.optionalCsgInfo);
    stringBuilder.append(", .bands = ");
    stringBuilder.append(this.bands);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(160L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellIdentityLte> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellIdentityLte> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 160);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellIdentityLte cellIdentityLte = new CellIdentityLte();
      cellIdentityLte.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 160));
      arrayList.add(cellIdentityLte);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.base.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 0L);
    int i = paramHwBlob.getInt32(paramLong + 88L + 8L);
    long l1 = (i * 16);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 88L + 0L, true);
    this.additionalPlmns.clear();
    byte b;
    for (b = 0; b < i; b++) {
      new String();
      String str = hwBlob2.getString((b * 16));
      l1 = ((str.getBytes()).length + 1);
      long l = hwBlob2.handle();
      l2 = (b * 16 + 0);
      paramHwParcel.readEmbeddedBuffer(l1, l, l2, false);
      this.additionalPlmns.add(str);
    } 
    this.optionalCsgInfo.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 104L);
    i = paramHwBlob.getInt32(paramLong + 144L + 8L);
    l2 = (i * 4);
    l1 = paramHwBlob.handle();
    HwBlob hwBlob1 = paramHwParcel.readEmbeddedBuffer(l2, l1, 0L + paramLong + 144L, true);
    this.bands.clear();
    for (b = 0; b < i; b++) {
      int j = hwBlob1.getInt32((b * 4));
      this.bands.add(Integer.valueOf(j));
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(160);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellIdentityLte> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 160);
    for (byte b = 0; b < i; b++)
      ((CellIdentityLte)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 160)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.base.writeEmbeddedToBlob(paramHwBlob, paramLong + 0L);
    int i = this.additionalPlmns.size();
    paramHwBlob.putInt32(paramLong + 88L + 8L, i);
    paramHwBlob.putBool(paramLong + 88L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 16);
    byte b;
    for (b = 0; b < i; b++)
      hwBlob.putString((b * 16), this.additionalPlmns.get(b)); 
    paramHwBlob.putBlob(paramLong + 88L + 0L, hwBlob);
    this.optionalCsgInfo.writeEmbeddedToBlob(paramHwBlob, paramLong + 104L);
    i = this.bands.size();
    paramHwBlob.putInt32(paramLong + 144L + 8L, i);
    paramHwBlob.putBool(paramLong + 144L + 12L, false);
    hwBlob = new HwBlob(i * 4);
    for (b = 0; b < i; b++)
      hwBlob.putInt32((b * 4), ((Integer)this.bands.get(b)).intValue()); 
    paramHwBlob.putBlob(paramLong + 144L + 0L, hwBlob);
  }
}
