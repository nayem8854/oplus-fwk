package android.hardware.radio.V1_5;

import android.internal.hidl.safe_union.V1_0.Monostate;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class OptionalCsgInfo {
  private byte hidl_d;
  
  private Object hidl_o;
  
  public OptionalCsgInfo() {
    this.hidl_d = 0;
    this.hidl_o = null;
    this.hidl_o = new Monostate();
  }
  
  public static final class hidl_discriminator {
    public static final byte csgInfo = 1;
    
    public static final byte noinit = 0;
    
    public static final String getName(byte param1Byte) {
      if (param1Byte != 0) {
        if (param1Byte != 1)
          return "Unknown"; 
        return "csgInfo";
      } 
      return "noinit";
    }
  }
  
  public void noinit(Monostate paramMonostate) {
    this.hidl_d = 0;
    this.hidl_o = paramMonostate;
  }
  
  public Monostate noinit() {
    if (this.hidl_d != 0) {
      Object object1 = this.hidl_o;
      if (object1 != null) {
        object1 = object1.getClass().getName();
      } else {
        object1 = "null";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
      stringBuilder.append(this.hidl_d);
      stringBuilder.append(" (corresponding to ");
      byte b = this.hidl_d;
      stringBuilder.append(hidl_discriminator.getName(b));
      stringBuilder.append("), and hidl_o is of type ");
      stringBuilder.append((String)object1);
      stringBuilder.append(".");
      throw new IllegalStateException(stringBuilder.toString());
    } 
    Object object = this.hidl_o;
    if (object == null || Monostate.class.isInstance(object))
      return (Monostate)this.hidl_o; 
    throw new Error("Union is in a corrupted state.");
  }
  
  public void csgInfo(ClosedSubscriberGroupInfo paramClosedSubscriberGroupInfo) {
    this.hidl_d = 1;
    this.hidl_o = paramClosedSubscriberGroupInfo;
  }
  
  public ClosedSubscriberGroupInfo csgInfo() {
    if (this.hidl_d != 1) {
      Object object1 = this.hidl_o;
      if (object1 != null) {
        object1 = object1.getClass().getName();
      } else {
        object1 = "null";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
      stringBuilder.append(this.hidl_d);
      stringBuilder.append(" (corresponding to ");
      byte b = this.hidl_d;
      stringBuilder.append(hidl_discriminator.getName(b));
      stringBuilder.append("), and hidl_o is of type ");
      stringBuilder.append((String)object1);
      stringBuilder.append(".");
      throw new IllegalStateException(stringBuilder.toString());
    } 
    Object object = this.hidl_o;
    if (object == null || ClosedSubscriberGroupInfo.class.isInstance(object))
      return (ClosedSubscriberGroupInfo)this.hidl_o; 
    throw new Error("Union is in a corrupted state.");
  }
  
  public byte getDiscriminator() {
    return this.hidl_d;
  }
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != OptionalCsgInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.hidl_d != ((OptionalCsgInfo)paramObject).hidl_d)
      return false; 
    if (!HidlSupport.deepEquals(this.hidl_o, ((OptionalCsgInfo)paramObject).hidl_o))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    Object object = this.hidl_o;
    int i = HidlSupport.deepHashCode(object);
    byte b = this.hidl_d;
    int j = Objects.hashCode(Byte.valueOf(b));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    byte b = this.hidl_d;
    if (b != 0) {
      if (b == 1) {
        stringBuilder.append(".csgInfo = ");
        stringBuilder.append(csgInfo());
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown union discriminator (value: ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(").");
        throw new Error(stringBuilder.toString());
      } 
    } else {
      stringBuilder.append(".noinit = ");
      stringBuilder.append(noinit());
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(40L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<OptionalCsgInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<OptionalCsgInfo> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 40);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      OptionalCsgInfo optionalCsgInfo = new OptionalCsgInfo();
      optionalCsgInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 40));
      arrayList.add(optionalCsgInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    StringBuilder stringBuilder;
    byte b = paramHwBlob.getInt8(0L + paramLong);
    if (b != 0) {
      if (b == 1) {
        ClosedSubscriberGroupInfo closedSubscriberGroupInfo = new ClosedSubscriberGroupInfo();
        closedSubscriberGroupInfo.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 8L + paramLong);
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown union discriminator (value: ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(").");
        throw new IllegalStateException(stringBuilder.toString());
      } 
    } else {
      Monostate monostate = new Monostate();
      monostate.readEmbeddedFromParcel((HwParcel)stringBuilder, paramHwBlob, 8L + paramLong);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(40);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<OptionalCsgInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 40);
    for (byte b = 0; b < i; b++)
      ((OptionalCsgInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 40)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    StringBuilder stringBuilder;
    paramHwBlob.putInt8(0L + paramLong, this.hidl_d);
    byte b = this.hidl_d;
    if (b != 0) {
      if (b == 1) {
        csgInfo().writeEmbeddedToBlob(paramHwBlob, 8L + paramLong);
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown union discriminator (value: ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(").");
        throw new Error(stringBuilder.toString());
      } 
    } else {
      noinit().writeEmbeddedToBlob((HwBlob)stringBuilder, 8L + paramLong);
    } 
  }
}
