package android.hardware.radio.V1_5;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellIdentityGsm {
  public android.hardware.radio.V1_2.CellIdentityGsm base = new android.hardware.radio.V1_2.CellIdentityGsm();
  
  public ArrayList<String> additionalPlmns = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellIdentityGsm.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.base, ((CellIdentityGsm)paramObject).base))
      return false; 
    if (!HidlSupport.deepEquals(this.additionalPlmns, ((CellIdentityGsm)paramObject).additionalPlmns))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    android.hardware.radio.V1_2.CellIdentityGsm cellIdentityGsm = this.base;
    int i = HidlSupport.deepHashCode(cellIdentityGsm);
    ArrayList<String> arrayList = this.additionalPlmns;
    int j = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".base = ");
    stringBuilder.append(this.base);
    stringBuilder.append(", .additionalPlmns = ");
    stringBuilder.append(this.additionalPlmns);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(96L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellIdentityGsm> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellIdentityGsm> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 96);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellIdentityGsm cellIdentityGsm = new CellIdentityGsm();
      cellIdentityGsm.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 96));
      arrayList.add(cellIdentityGsm);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.base.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 0L);
    int i = paramHwBlob.getInt32(paramLong + 80L + 8L);
    long l1 = (i * 16);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 80L + 0L, true);
    this.additionalPlmns.clear();
    for (byte b = 0; b < i; b++) {
      new String();
      String str = hwBlob.getString((b * 16));
      l2 = ((str.getBytes()).length + 1);
      l1 = hwBlob.handle();
      paramLong = (b * 16 + 0);
      paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong, false);
      this.additionalPlmns.add(str);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(96);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellIdentityGsm> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 96);
    for (byte b = 0; b < i; b++)
      ((CellIdentityGsm)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 96)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.base.writeEmbeddedToBlob(paramHwBlob, paramLong + 0L);
    int i = this.additionalPlmns.size();
    paramHwBlob.putInt32(paramLong + 80L + 8L, i);
    paramHwBlob.putBool(paramLong + 80L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 16);
    for (byte b = 0; b < i; b++)
      hwBlob.putString((b * 16), this.additionalPlmns.get(b)); 
    paramHwBlob.putBlob(80L + paramLong + 0L, hwBlob);
  }
}
