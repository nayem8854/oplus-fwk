package android.hardware.radio.V1_5;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellIdentityWcdma {
  public android.hardware.radio.V1_2.CellIdentityWcdma base = new android.hardware.radio.V1_2.CellIdentityWcdma();
  
  public ArrayList<String> additionalPlmns = new ArrayList<>();
  
  public OptionalCsgInfo optionalCsgInfo = new OptionalCsgInfo();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellIdentityWcdma.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.base, ((CellIdentityWcdma)paramObject).base))
      return false; 
    if (!HidlSupport.deepEquals(this.additionalPlmns, ((CellIdentityWcdma)paramObject).additionalPlmns))
      return false; 
    if (!HidlSupport.deepEquals(this.optionalCsgInfo, ((CellIdentityWcdma)paramObject).optionalCsgInfo))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    android.hardware.radio.V1_2.CellIdentityWcdma cellIdentityWcdma = this.base;
    int i = HidlSupport.deepHashCode(cellIdentityWcdma);
    ArrayList<String> arrayList = this.additionalPlmns;
    int j = HidlSupport.deepHashCode(arrayList);
    OptionalCsgInfo optionalCsgInfo = this.optionalCsgInfo;
    int k = HidlSupport.deepHashCode(optionalCsgInfo);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".base = ");
    stringBuilder.append(this.base);
    stringBuilder.append(", .additionalPlmns = ");
    stringBuilder.append(this.additionalPlmns);
    stringBuilder.append(", .optionalCsgInfo = ");
    stringBuilder.append(this.optionalCsgInfo);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(136L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellIdentityWcdma> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellIdentityWcdma> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 136);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellIdentityWcdma cellIdentityWcdma = new CellIdentityWcdma();
      cellIdentityWcdma.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 136));
      arrayList.add(cellIdentityWcdma);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.base.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 0L);
    int i = paramHwBlob.getInt32(paramLong + 80L + 8L);
    long l1 = (i * 16);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 80L + 0L, true);
    this.additionalPlmns.clear();
    for (byte b = 0; b < i; b++) {
      new String();
      String str = hwBlob.getString((b * 16));
      l2 = ((str.getBytes()).length + 1);
      l1 = hwBlob.handle();
      long l = (b * 16 + 0);
      paramHwParcel.readEmbeddedBuffer(l2, l1, l, false);
      this.additionalPlmns.add(str);
    } 
    this.optionalCsgInfo.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 96L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(136);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellIdentityWcdma> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 136);
    for (byte b = 0; b < i; b++)
      ((CellIdentityWcdma)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 136)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.base.writeEmbeddedToBlob(paramHwBlob, paramLong + 0L);
    int i = this.additionalPlmns.size();
    paramHwBlob.putInt32(paramLong + 80L + 8L, i);
    paramHwBlob.putBool(paramLong + 80L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 16);
    for (byte b = 0; b < i; b++)
      hwBlob.putString((b * 16), this.additionalPlmns.get(b)); 
    paramHwBlob.putBlob(80L + paramLong + 0L, hwBlob);
    this.optionalCsgInfo.writeEmbeddedToBlob(paramHwBlob, 96L + paramLong);
  }
}
