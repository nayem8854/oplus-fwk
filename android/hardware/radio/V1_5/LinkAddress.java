package android.hardware.radio.V1_5;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class LinkAddress {
  public String address = new String();
  
  public long deprecationTime = 0L;
  
  public long expirationTime = 0L;
  
  public int properties;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != LinkAddress.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.address, ((LinkAddress)paramObject).address))
      return false; 
    if (!HidlSupport.deepEquals(Integer.valueOf(this.properties), Integer.valueOf(((LinkAddress)paramObject).properties)))
      return false; 
    if (this.deprecationTime != ((LinkAddress)paramObject).deprecationTime)
      return false; 
    if (this.expirationTime != ((LinkAddress)paramObject).expirationTime)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    String str = this.address;
    int i = HidlSupport.deepHashCode(str), j = this.properties;
    int k = HidlSupport.deepHashCode(Integer.valueOf(j));
    long l = this.deprecationTime;
    j = HidlSupport.deepHashCode(Long.valueOf(l));
    l = this.expirationTime;
    int m = HidlSupport.deepHashCode(Long.valueOf(l));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(k), Integer.valueOf(j), Integer.valueOf(m) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".address = ");
    stringBuilder.append(this.address);
    stringBuilder.append(", .properties = ");
    stringBuilder.append(AddressProperty.dumpBitfield(this.properties));
    stringBuilder.append(", .deprecationTime = ");
    stringBuilder.append(this.deprecationTime);
    stringBuilder.append(", .expirationTime = ");
    stringBuilder.append(this.expirationTime);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(40L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<LinkAddress> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<LinkAddress> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 40);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      LinkAddress linkAddress = new LinkAddress();
      linkAddress.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 40));
      arrayList.add(linkAddress);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    String str = paramHwBlob.getString(paramLong + 0L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, false);
    this.properties = paramHwBlob.getInt32(16L + paramLong);
    this.deprecationTime = paramHwBlob.getInt64(24L + paramLong);
    this.expirationTime = paramHwBlob.getInt64(32L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(40);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<LinkAddress> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 40);
    for (byte b = 0; b < i; b++)
      ((LinkAddress)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 40)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putString(0L + paramLong, this.address);
    paramHwBlob.putInt32(16L + paramLong, this.properties);
    paramHwBlob.putInt64(24L + paramLong, this.deprecationTime);
    paramHwBlob.putInt64(32L + paramLong, this.expirationTime);
  }
}
