package android.hardware.radio.V1_5;

import android.hardware.radio.V1_0.RadioError;
import android.hardware.radio.V1_1.ScanStatus;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class NetworkScanResult {
  public int status = 0;
  
  public int error = 0;
  
  public ArrayList<CellInfo> networkInfos = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != NetworkScanResult.class)
      return false; 
    paramObject = paramObject;
    if (this.status != ((NetworkScanResult)paramObject).status)
      return false; 
    if (this.error != ((NetworkScanResult)paramObject).error)
      return false; 
    if (!HidlSupport.deepEquals(this.networkInfos, ((NetworkScanResult)paramObject).networkInfos))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.status;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.error;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    ArrayList<CellInfo> arrayList = this.networkInfos;
    int k = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".status = ");
    stringBuilder.append(ScanStatus.toString(this.status));
    stringBuilder.append(", .error = ");
    stringBuilder.append(RadioError.toString(this.error));
    stringBuilder.append(", .networkInfos = ");
    stringBuilder.append(this.networkInfos);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(24L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<NetworkScanResult> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<NetworkScanResult> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 24);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      NetworkScanResult networkScanResult = new NetworkScanResult();
      networkScanResult.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 24));
      arrayList.add(networkScanResult);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.status = paramHwBlob.getInt32(paramLong + 0L);
    this.error = paramHwBlob.getInt32(paramLong + 4L);
    int i = paramHwBlob.getInt32(paramLong + 8L + 8L);
    long l1 = (i * 216);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, true);
    this.networkInfos.clear();
    for (byte b = 0; b < i; b++) {
      CellInfo cellInfo = new CellInfo();
      cellInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 216));
      this.networkInfos.add(cellInfo);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(24);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<NetworkScanResult> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 24);
    for (byte b = 0; b < i; b++)
      ((NetworkScanResult)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 24)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.status);
    paramHwBlob.putInt32(4L + paramLong, this.error);
    int i = this.networkInfos.size();
    paramHwBlob.putInt32(paramLong + 8L + 8L, i);
    paramHwBlob.putBool(paramLong + 8L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 216);
    for (byte b = 0; b < i; b++)
      ((CellInfo)this.networkInfos.get(b)).writeEmbeddedToBlob(hwBlob, (b * 216)); 
    paramHwBlob.putBlob(8L + paramLong + 0L, hwBlob);
  }
}
