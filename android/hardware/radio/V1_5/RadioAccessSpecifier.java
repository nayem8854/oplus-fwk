package android.hardware.radio.V1_5;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class RadioAccessSpecifier {
  public static final class Bands {
    private byte hidl_d;
    
    private Object hidl_o;
    
    public Bands() {
      this.hidl_d = 0;
      this.hidl_o = null;
      this.hidl_o = new ArrayList();
    }
    
    public static final class hidl_discriminator {
      public static final byte eutranBands = 2;
      
      public static final byte geranBands = 0;
      
      public static final byte ngranBands = 3;
      
      public static final byte utranBands = 1;
      
      public static final String getName(byte param2Byte) {
        if (param2Byte != 0) {
          if (param2Byte != 1) {
            if (param2Byte != 2) {
              if (param2Byte != 3)
                return "Unknown"; 
              return "ngranBands";
            } 
            return "eutranBands";
          } 
          return "utranBands";
        } 
        return "geranBands";
      }
    }
    
    public void geranBands(ArrayList<Integer> param1ArrayList) {
      this.hidl_d = 0;
      this.hidl_o = param1ArrayList;
    }
    
    public ArrayList<Integer> geranBands() {
      if (this.hidl_d != 0) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || ArrayList.class.isInstance(object))
        return (ArrayList<Integer>)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public void utranBands(ArrayList<Integer> param1ArrayList) {
      this.hidl_d = 1;
      this.hidl_o = param1ArrayList;
    }
    
    public ArrayList<Integer> utranBands() {
      if (this.hidl_d != 1) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || ArrayList.class.isInstance(object))
        return (ArrayList<Integer>)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public void eutranBands(ArrayList<Integer> param1ArrayList) {
      this.hidl_d = 2;
      this.hidl_o = param1ArrayList;
    }
    
    public ArrayList<Integer> eutranBands() {
      if (this.hidl_d != 2) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || ArrayList.class.isInstance(object))
        return (ArrayList<Integer>)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public void ngranBands(ArrayList<Integer> param1ArrayList) {
      this.hidl_d = 3;
      this.hidl_o = param1ArrayList;
    }
    
    public ArrayList<Integer> ngranBands() {
      if (this.hidl_d != 3) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || ArrayList.class.isInstance(object))
        return (ArrayList<Integer>)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public byte getDiscriminator() {
      return this.hidl_d;
    }
    
    public final boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (param1Object.getClass() != Bands.class)
        return false; 
      param1Object = param1Object;
      if (this.hidl_d != ((Bands)param1Object).hidl_d)
        return false; 
      if (!HidlSupport.deepEquals(this.hidl_o, ((Bands)param1Object).hidl_o))
        return false; 
      return true;
    }
    
    public final int hashCode() {
      Object object = this.hidl_o;
      int i = HidlSupport.deepHashCode(object);
      byte b = this.hidl_d;
      int j = Objects.hashCode(Byte.valueOf(b));
      return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
    }
    
    public final String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      byte b = this.hidl_d;
      if (b != 0) {
        if (b != 1) {
          if (b != 2) {
            if (b == 3) {
              stringBuilder.append(".ngranBands = ");
              stringBuilder.append(ngranBands());
            } else {
              stringBuilder = new StringBuilder();
              stringBuilder.append("Unknown union discriminator (value: ");
              stringBuilder.append(this.hidl_d);
              stringBuilder.append(").");
              throw new Error(stringBuilder.toString());
            } 
          } else {
            stringBuilder.append(".eutranBands = ");
            stringBuilder.append(eutranBands());
          } 
        } else {
          stringBuilder.append(".utranBands = ");
          stringBuilder.append(utranBands());
        } 
      } else {
        stringBuilder.append(".geranBands = ");
        stringBuilder.append(geranBands());
      } 
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public final void readFromParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = param1HwParcel.readBuffer(24L);
      readEmbeddedFromParcel(param1HwParcel, hwBlob, 0L);
    }
    
    public static final ArrayList<Bands> readVectorFromParcel(HwParcel param1HwParcel) {
      ArrayList<Bands> arrayList = new ArrayList();
      HwBlob hwBlob = param1HwParcel.readBuffer(16L);
      int i = hwBlob.getInt32(8L);
      long l1 = (i * 24);
      long l2 = hwBlob.handle();
      hwBlob = param1HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
      arrayList.clear();
      for (byte b = 0; b < i; b++) {
        Bands bands = new Bands();
        bands.readEmbeddedFromParcel(param1HwParcel, hwBlob, (b * 24));
        arrayList.add(bands);
      } 
      return arrayList;
    }
    
    public final void readEmbeddedFromParcel(HwParcel param1HwParcel, HwBlob param1HwBlob, long param1Long) {
      HwBlob hwBlob;
      byte b = param1HwBlob.getInt8(param1Long + 0L);
      if (b != 0) {
        if (b != 1) {
          StringBuilder stringBuilder;
          if (b != 2) {
            if (b == 3) {
              this.hidl_o = new ArrayList();
              int i = param1HwBlob.getInt32(param1Long + 8L + 8L);
              long l1 = (i * 4);
              long l2 = param1HwBlob.handle();
              hwBlob = param1HwParcel.readEmbeddedBuffer(l1, l2, param1Long + 8L + 0L, true);
              ((ArrayList)this.hidl_o).clear();
              for (byte b1 = 0; b1 < i; b1++) {
                int j = hwBlob.getInt32((b1 * 4));
                ((ArrayList<Integer>)this.hidl_o).add(Integer.valueOf(j));
              } 
            } else {
              stringBuilder = new StringBuilder();
              stringBuilder.append("Unknown union discriminator (value: ");
              stringBuilder.append(this.hidl_d);
              stringBuilder.append(").");
              throw new IllegalStateException(stringBuilder.toString());
            } 
          } else {
            this.hidl_o = new ArrayList();
            int i = param1HwBlob.getInt32(param1Long + 8L + 8L);
            long l1 = (i * 4);
            long l2 = param1HwBlob.handle();
            hwBlob = stringBuilder.readEmbeddedBuffer(l1, l2, param1Long + 8L + 0L, true);
            ((ArrayList)this.hidl_o).clear();
            for (byte b1 = 0; b1 < i; b1++) {
              int j = hwBlob.getInt32((b1 * 4));
              ((ArrayList<Integer>)this.hidl_o).add(Integer.valueOf(j));
            } 
          } 
        } else {
          this.hidl_o = new ArrayList();
          int i = param1HwBlob.getInt32(param1Long + 8L + 8L);
          long l2 = (i * 4);
          long l1 = param1HwBlob.handle();
          hwBlob = hwBlob.readEmbeddedBuffer(l2, l1, param1Long + 8L + 0L, true);
          ((ArrayList)this.hidl_o).clear();
          for (byte b1 = 0; b1 < i; b1++) {
            int j = hwBlob.getInt32((b1 * 4));
            ((ArrayList<Integer>)this.hidl_o).add(Integer.valueOf(j));
          } 
        } 
      } else {
        this.hidl_o = new ArrayList();
        int i = param1HwBlob.getInt32(param1Long + 8L + 8L);
        long l1 = (i * 4);
        long l2 = param1HwBlob.handle();
        hwBlob = hwBlob.readEmbeddedBuffer(l1, l2, param1Long + 8L + 0L, true);
        ((ArrayList)this.hidl_o).clear();
        for (byte b1 = 0; b1 < i; b1++) {
          int j = hwBlob.getInt32((b1 * 4));
          ((ArrayList<Integer>)this.hidl_o).add(Integer.valueOf(j));
        } 
      } 
    }
    
    public final void writeToParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = new HwBlob(24);
      writeEmbeddedToBlob(hwBlob, 0L);
      param1HwParcel.writeBuffer(hwBlob);
    }
    
    public static final void writeVectorToParcel(HwParcel param1HwParcel, ArrayList<Bands> param1ArrayList) {
      HwBlob hwBlob1 = new HwBlob(16);
      int i = param1ArrayList.size();
      hwBlob1.putInt32(8L, i);
      hwBlob1.putBool(12L, false);
      HwBlob hwBlob2 = new HwBlob(i * 24);
      for (byte b = 0; b < i; b++)
        ((Bands)param1ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 24)); 
      hwBlob1.putBlob(0L, hwBlob2);
      param1HwParcel.writeBuffer(hwBlob1);
    }
    
    public final void writeEmbeddedToBlob(HwBlob param1HwBlob, long param1Long) {
      StringBuilder stringBuilder;
      param1HwBlob.putInt8(param1Long + 0L, this.hidl_d);
      byte b = this.hidl_d;
      if (b != 0) {
        if (b != 1) {
          if (b != 2) {
            if (b == 3) {
              int i = ngranBands().size();
              param1HwBlob.putInt32(param1Long + 8L + 8L, i);
              param1HwBlob.putBool(param1Long + 8L + 12L, false);
              HwBlob hwBlob = new HwBlob(i * 4);
              for (b = 0; b < i; b++)
                hwBlob.putInt32((b * 4), ((Integer)ngranBands().get(b)).intValue()); 
              param1HwBlob.putBlob(8L + param1Long + 0L, hwBlob);
            } else {
              stringBuilder = new StringBuilder();
              stringBuilder.append("Unknown union discriminator (value: ");
              stringBuilder.append(this.hidl_d);
              stringBuilder.append(").");
              throw new Error(stringBuilder.toString());
            } 
          } else {
            int i = eutranBands().size();
            stringBuilder.putInt32(param1Long + 8L + 8L, i);
            stringBuilder.putBool(param1Long + 8L + 12L, false);
            HwBlob hwBlob = new HwBlob(i * 4);
            for (b = 0; b < i; b++)
              hwBlob.putInt32((b * 4), ((Integer)eutranBands().get(b)).intValue()); 
            stringBuilder.putBlob(8L + param1Long + 0L, hwBlob);
          } 
        } else {
          int i = utranBands().size();
          stringBuilder.putInt32(param1Long + 8L + 8L, i);
          stringBuilder.putBool(param1Long + 8L + 12L, false);
          HwBlob hwBlob = new HwBlob(i * 4);
          for (b = 0; b < i; b++)
            hwBlob.putInt32((b * 4), ((Integer)utranBands().get(b)).intValue()); 
          stringBuilder.putBlob(8L + param1Long + 0L, hwBlob);
        } 
      } else {
        int i = geranBands().size();
        stringBuilder.putInt32(param1Long + 8L + 8L, i);
        stringBuilder.putBool(param1Long + 8L + 12L, false);
        HwBlob hwBlob = new HwBlob(i * 4);
        for (b = 0; b < i; b++)
          hwBlob.putInt32((b * 4), ((Integer)geranBands().get(b)).intValue()); 
        stringBuilder.putBlob(8L + param1Long + 0L, hwBlob);
      } 
    }
  }
  
  public static final class hidl_discriminator {
    public static final byte eutranBands = 2;
    
    public static final byte geranBands = 0;
    
    public static final byte ngranBands = 3;
    
    public static final byte utranBands = 1;
    
    public static final String getName(byte param1Byte) {
      if (param1Byte != 0) {
        if (param1Byte != 1) {
          if (param1Byte != 2) {
            if (param1Byte != 3)
              return "Unknown"; 
            return "ngranBands";
          } 
          return "eutranBands";
        } 
        return "utranBands";
      } 
      return "geranBands";
    }
  }
  
  public int radioAccessNetwork = 0;
  
  public Bands bands = new Bands();
  
  public ArrayList<Integer> channels = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != RadioAccessSpecifier.class)
      return false; 
    paramObject = paramObject;
    if (this.radioAccessNetwork != ((RadioAccessSpecifier)paramObject).radioAccessNetwork)
      return false; 
    if (!HidlSupport.deepEquals(this.bands, ((RadioAccessSpecifier)paramObject).bands))
      return false; 
    if (!HidlSupport.deepEquals(this.channels, ((RadioAccessSpecifier)paramObject).channels))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.radioAccessNetwork;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    Bands bands = this.bands;
    int k = HidlSupport.deepHashCode(bands);
    ArrayList<Integer> arrayList = this.channels;
    i = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(i) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".radioAccessNetwork = ");
    stringBuilder.append(RadioAccessNetworks.toString(this.radioAccessNetwork));
    stringBuilder.append(", .bands = ");
    stringBuilder.append(this.bands);
    stringBuilder.append(", .channels = ");
    stringBuilder.append(this.channels);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(48L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<RadioAccessSpecifier> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<RadioAccessSpecifier> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 48);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      RadioAccessSpecifier radioAccessSpecifier = new RadioAccessSpecifier();
      radioAccessSpecifier.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 48));
      arrayList.add(radioAccessSpecifier);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.radioAccessNetwork = paramHwBlob.getInt32(paramLong + 0L);
    this.bands.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 8L);
    int i = paramHwBlob.getInt32(paramLong + 32L + 8L);
    long l1 = (i * 4);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L + paramLong + 32L, true);
    this.channels.clear();
    for (byte b = 0; b < i; b++) {
      int j = hwBlob.getInt32((b * 4));
      this.channels.add(Integer.valueOf(j));
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(48);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<RadioAccessSpecifier> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 48);
    for (byte b = 0; b < i; b++)
      ((RadioAccessSpecifier)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 48)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.radioAccessNetwork);
    this.bands.writeEmbeddedToBlob(paramHwBlob, paramLong + 8L);
    int i = this.channels.size();
    paramHwBlob.putInt32(paramLong + 32L + 8L, i);
    paramHwBlob.putBool(paramLong + 32L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 4);
    for (byte b = 0; b < i; b++)
      hwBlob.putInt32((b * 4), ((Integer)this.channels.get(b)).intValue()); 
    paramHwBlob.putBlob(32L + paramLong + 0L, hwBlob);
  }
}
