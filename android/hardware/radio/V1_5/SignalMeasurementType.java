package android.hardware.radio.V1_5;

import java.util.ArrayList;

public final class SignalMeasurementType {
  public static final int RSCP = 2;
  
  public static final int RSRP = 3;
  
  public static final int RSRQ = 4;
  
  public static final int RSSI = 1;
  
  public static final int RSSNR = 5;
  
  public static final int SSRSRP = 6;
  
  public static final int SSRSRQ = 7;
  
  public static final int SSSINR = 8;
  
  public static final String toString(int paramInt) {
    if (paramInt == 1)
      return "RSSI"; 
    if (paramInt == 2)
      return "RSCP"; 
    if (paramInt == 3)
      return "RSRP"; 
    if (paramInt == 4)
      return "RSRQ"; 
    if (paramInt == 5)
      return "RSSNR"; 
    if (paramInt == 6)
      return "SSRSRP"; 
    if (paramInt == 7)
      return "SSRSRQ"; 
    if (paramInt == 8)
      return "SSSINR"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    if ((paramInt & 0x1) == 1) {
      arrayList.add("RSSI");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("RSCP");
      j = i | 0x2;
    } 
    i = j;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("RSRP");
      i = j | 0x3;
    } 
    j = i;
    if ((paramInt & 0x4) == 4) {
      arrayList.add("RSRQ");
      j = i | 0x4;
    } 
    i = j;
    if ((paramInt & 0x5) == 5) {
      arrayList.add("RSSNR");
      i = j | 0x5;
    } 
    j = i;
    if ((paramInt & 0x6) == 6) {
      arrayList.add("SSRSRP");
      j = i | 0x6;
    } 
    i = j;
    if ((paramInt & 0x7) == 7) {
      arrayList.add("SSRSRQ");
      i = j | 0x7;
    } 
    j = i;
    if ((paramInt & 0x8) == 8) {
      arrayList.add("SSSINR");
      j = i | 0x8;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
