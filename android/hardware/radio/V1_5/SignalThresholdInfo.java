package android.hardware.radio.V1_5;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class SignalThresholdInfo {
  public int signalMeasurement = 0;
  
  public int hysteresisMs = 0;
  
  public int hysteresisDb = 0;
  
  public ArrayList<Integer> thresholds = new ArrayList<>();
  
  public boolean isEnabled = false;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != SignalThresholdInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.signalMeasurement != ((SignalThresholdInfo)paramObject).signalMeasurement)
      return false; 
    if (this.hysteresisMs != ((SignalThresholdInfo)paramObject).hysteresisMs)
      return false; 
    if (this.hysteresisDb != ((SignalThresholdInfo)paramObject).hysteresisDb)
      return false; 
    if (!HidlSupport.deepEquals(this.thresholds, ((SignalThresholdInfo)paramObject).thresholds))
      return false; 
    if (this.isEnabled != ((SignalThresholdInfo)paramObject).isEnabled)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.signalMeasurement;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.hysteresisMs;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.hysteresisDb;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    ArrayList<Integer> arrayList = this.thresholds;
    int m = HidlSupport.deepHashCode(arrayList);
    boolean bool = this.isEnabled;
    int n = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".signalMeasurement = ");
    stringBuilder.append(SignalMeasurementType.toString(this.signalMeasurement));
    stringBuilder.append(", .hysteresisMs = ");
    stringBuilder.append(this.hysteresisMs);
    stringBuilder.append(", .hysteresisDb = ");
    stringBuilder.append(this.hysteresisDb);
    stringBuilder.append(", .thresholds = ");
    stringBuilder.append(this.thresholds);
    stringBuilder.append(", .isEnabled = ");
    stringBuilder.append(this.isEnabled);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(40L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<SignalThresholdInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<SignalThresholdInfo> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 40);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      SignalThresholdInfo signalThresholdInfo = new SignalThresholdInfo();
      signalThresholdInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 40));
      arrayList.add(signalThresholdInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.signalMeasurement = paramHwBlob.getInt32(paramLong + 0L);
    this.hysteresisMs = paramHwBlob.getInt32(paramLong + 4L);
    this.hysteresisDb = paramHwBlob.getInt32(paramLong + 8L);
    int i = paramHwBlob.getInt32(paramLong + 16L + 8L);
    long l1 = (i * 4);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 16L + 0L, true);
    this.thresholds.clear();
    for (byte b = 0; b < i; b++) {
      int j = hwBlob.getInt32((b * 4));
      this.thresholds.add(Integer.valueOf(j));
    } 
    this.isEnabled = paramHwBlob.getBool(paramLong + 32L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(40);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<SignalThresholdInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 40);
    for (byte b = 0; b < i; b++)
      ((SignalThresholdInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 40)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.signalMeasurement);
    paramHwBlob.putInt32(4L + paramLong, this.hysteresisMs);
    paramHwBlob.putInt32(paramLong + 8L, this.hysteresisDb);
    int i = this.thresholds.size();
    paramHwBlob.putInt32(paramLong + 16L + 8L, i);
    paramHwBlob.putBool(paramLong + 16L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 4);
    for (byte b = 0; b < i; b++)
      hwBlob.putInt32((b * 4), ((Integer)this.thresholds.get(b)).intValue()); 
    paramHwBlob.putBlob(16L + paramLong + 0L, hwBlob);
    paramHwBlob.putBool(32L + paramLong, this.isEnabled);
  }
}
