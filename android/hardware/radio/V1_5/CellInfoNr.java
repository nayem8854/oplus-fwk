package android.hardware.radio.V1_5;

import android.hardware.radio.V1_4.NrSignalStrength;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellInfoNr {
  public CellIdentityNr cellIdentityNr = new CellIdentityNr();
  
  public NrSignalStrength signalStrengthNr = new NrSignalStrength();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellInfoNr.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.cellIdentityNr, ((CellInfoNr)paramObject).cellIdentityNr))
      return false; 
    if (!HidlSupport.deepEquals(this.signalStrengthNr, ((CellInfoNr)paramObject).signalStrengthNr))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    CellIdentityNr cellIdentityNr = this.cellIdentityNr;
    int i = HidlSupport.deepHashCode(cellIdentityNr);
    NrSignalStrength nrSignalStrength = this.signalStrengthNr;
    int j = HidlSupport.deepHashCode(nrSignalStrength);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".cellIdentityNr = ");
    stringBuilder.append(this.cellIdentityNr);
    stringBuilder.append(", .signalStrengthNr = ");
    stringBuilder.append(this.signalStrengthNr);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(144L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellInfoNr> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellInfoNr> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 144);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellInfoNr cellInfoNr = new CellInfoNr();
      cellInfoNr.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 144));
      arrayList.add(cellInfoNr);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.cellIdentityNr.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 0L + paramLong);
    this.signalStrengthNr.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 120L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(144);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellInfoNr> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 144);
    for (byte b = 0; b < i; b++)
      ((CellInfoNr)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 144)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.cellIdentityNr.writeEmbeddedToBlob(paramHwBlob, 0L + paramLong);
    this.signalStrengthNr.writeEmbeddedToBlob(paramHwBlob, 120L + paramLong);
  }
}
