package android.hardware.radio.V1_5;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellIdentityTdscdma {
  public android.hardware.radio.V1_2.CellIdentityTdscdma base = new android.hardware.radio.V1_2.CellIdentityTdscdma();
  
  public ArrayList<String> additionalPlmns = new ArrayList<>();
  
  public OptionalCsgInfo optionalCsgInfo = new OptionalCsgInfo();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellIdentityTdscdma.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.base, ((CellIdentityTdscdma)paramObject).base))
      return false; 
    if (!HidlSupport.deepEquals(this.additionalPlmns, ((CellIdentityTdscdma)paramObject).additionalPlmns))
      return false; 
    if (!HidlSupport.deepEquals(this.optionalCsgInfo, ((CellIdentityTdscdma)paramObject).optionalCsgInfo))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    android.hardware.radio.V1_2.CellIdentityTdscdma cellIdentityTdscdma = this.base;
    int i = HidlSupport.deepHashCode(cellIdentityTdscdma);
    ArrayList<String> arrayList = this.additionalPlmns;
    int j = HidlSupport.deepHashCode(arrayList);
    OptionalCsgInfo optionalCsgInfo = this.optionalCsgInfo;
    int k = HidlSupport.deepHashCode(optionalCsgInfo);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".base = ");
    stringBuilder.append(this.base);
    stringBuilder.append(", .additionalPlmns = ");
    stringBuilder.append(this.additionalPlmns);
    stringBuilder.append(", .optionalCsgInfo = ");
    stringBuilder.append(this.optionalCsgInfo);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(144L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellIdentityTdscdma> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellIdentityTdscdma> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 144);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellIdentityTdscdma cellIdentityTdscdma = new CellIdentityTdscdma();
      cellIdentityTdscdma.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 144));
      arrayList.add(cellIdentityTdscdma);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.base.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 0L);
    int i = paramHwBlob.getInt32(paramLong + 88L + 8L);
    long l1 = (i * 16);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 88L + 0L, true);
    this.additionalPlmns.clear();
    for (byte b = 0; b < i; b++) {
      new String();
      String str = hwBlob.getString((b * 16));
      l1 = ((str.getBytes()).length + 1);
      long l = hwBlob.handle();
      l2 = (b * 16 + 0);
      paramHwParcel.readEmbeddedBuffer(l1, l, l2, false);
      this.additionalPlmns.add(str);
    } 
    this.optionalCsgInfo.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 104L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(144);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellIdentityTdscdma> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 144);
    for (byte b = 0; b < i; b++)
      ((CellIdentityTdscdma)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 144)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.base.writeEmbeddedToBlob(paramHwBlob, paramLong + 0L);
    int i = this.additionalPlmns.size();
    paramHwBlob.putInt32(paramLong + 88L + 8L, i);
    paramHwBlob.putBool(paramLong + 88L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 16);
    for (byte b = 0; b < i; b++)
      hwBlob.putString((b * 16), this.additionalPlmns.get(b)); 
    paramHwBlob.putBlob(88L + paramLong + 0L, hwBlob);
    this.optionalCsgInfo.writeEmbeddedToBlob(paramHwBlob, 104L + paramLong);
  }
}
