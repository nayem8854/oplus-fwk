package android.hardware.radio.V1_5;

import java.util.ArrayList;

public final class NgranBands {
  public static final int BAND_1 = 1;
  
  public static final int BAND_12 = 12;
  
  public static final int BAND_14 = 14;
  
  public static final int BAND_18 = 18;
  
  public static final int BAND_2 = 2;
  
  public static final int BAND_20 = 20;
  
  public static final int BAND_25 = 25;
  
  public static final int BAND_257 = 257;
  
  public static final int BAND_258 = 258;
  
  public static final int BAND_260 = 260;
  
  public static final int BAND_261 = 261;
  
  public static final int BAND_28 = 28;
  
  public static final int BAND_29 = 29;
  
  public static final int BAND_3 = 3;
  
  public static final int BAND_30 = 30;
  
  public static final int BAND_34 = 34;
  
  public static final int BAND_38 = 38;
  
  public static final int BAND_39 = 39;
  
  public static final int BAND_40 = 40;
  
  public static final int BAND_41 = 41;
  
  public static final int BAND_48 = 48;
  
  public static final int BAND_5 = 5;
  
  public static final int BAND_50 = 50;
  
  public static final int BAND_51 = 51;
  
  public static final int BAND_65 = 65;
  
  public static final int BAND_66 = 66;
  
  public static final int BAND_7 = 7;
  
  public static final int BAND_70 = 70;
  
  public static final int BAND_71 = 71;
  
  public static final int BAND_74 = 74;
  
  public static final int BAND_75 = 75;
  
  public static final int BAND_76 = 76;
  
  public static final int BAND_77 = 77;
  
  public static final int BAND_78 = 78;
  
  public static final int BAND_79 = 79;
  
  public static final int BAND_8 = 8;
  
  public static final int BAND_80 = 80;
  
  public static final int BAND_81 = 81;
  
  public static final int BAND_82 = 82;
  
  public static final int BAND_83 = 83;
  
  public static final int BAND_84 = 84;
  
  public static final int BAND_86 = 86;
  
  public static final int BAND_89 = 89;
  
  public static final int BAND_90 = 90;
  
  public static final int BAND_91 = 91;
  
  public static final int BAND_92 = 92;
  
  public static final int BAND_93 = 93;
  
  public static final int BAND_94 = 94;
  
  public static final int BAND_95 = 95;
  
  public static final String toString(int paramInt) {
    if (paramInt == 1)
      return "BAND_1"; 
    if (paramInt == 2)
      return "BAND_2"; 
    if (paramInt == 3)
      return "BAND_3"; 
    if (paramInt == 5)
      return "BAND_5"; 
    if (paramInt == 7)
      return "BAND_7"; 
    if (paramInt == 8)
      return "BAND_8"; 
    if (paramInt == 12)
      return "BAND_12"; 
    if (paramInt == 14)
      return "BAND_14"; 
    if (paramInt == 18)
      return "BAND_18"; 
    if (paramInt == 20)
      return "BAND_20"; 
    if (paramInt == 25)
      return "BAND_25"; 
    if (paramInt == 28)
      return "BAND_28"; 
    if (paramInt == 29)
      return "BAND_29"; 
    if (paramInt == 30)
      return "BAND_30"; 
    if (paramInt == 34)
      return "BAND_34"; 
    if (paramInt == 38)
      return "BAND_38"; 
    if (paramInt == 39)
      return "BAND_39"; 
    if (paramInt == 40)
      return "BAND_40"; 
    if (paramInt == 41)
      return "BAND_41"; 
    if (paramInt == 48)
      return "BAND_48"; 
    if (paramInt == 50)
      return "BAND_50"; 
    if (paramInt == 51)
      return "BAND_51"; 
    if (paramInt == 65)
      return "BAND_65"; 
    if (paramInt == 66)
      return "BAND_66"; 
    if (paramInt == 70)
      return "BAND_70"; 
    if (paramInt == 71)
      return "BAND_71"; 
    if (paramInt == 74)
      return "BAND_74"; 
    if (paramInt == 75)
      return "BAND_75"; 
    if (paramInt == 76)
      return "BAND_76"; 
    if (paramInt == 77)
      return "BAND_77"; 
    if (paramInt == 78)
      return "BAND_78"; 
    if (paramInt == 79)
      return "BAND_79"; 
    if (paramInt == 80)
      return "BAND_80"; 
    if (paramInt == 81)
      return "BAND_81"; 
    if (paramInt == 82)
      return "BAND_82"; 
    if (paramInt == 83)
      return "BAND_83"; 
    if (paramInt == 84)
      return "BAND_84"; 
    if (paramInt == 86)
      return "BAND_86"; 
    if (paramInt == 89)
      return "BAND_89"; 
    if (paramInt == 90)
      return "BAND_90"; 
    if (paramInt == 91)
      return "BAND_91"; 
    if (paramInt == 92)
      return "BAND_92"; 
    if (paramInt == 93)
      return "BAND_93"; 
    if (paramInt == 94)
      return "BAND_94"; 
    if (paramInt == 95)
      return "BAND_95"; 
    if (paramInt == 257)
      return "BAND_257"; 
    if (paramInt == 258)
      return "BAND_258"; 
    if (paramInt == 260)
      return "BAND_260"; 
    if (paramInt == 261)
      return "BAND_261"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    if ((paramInt & 0x1) == 1) {
      arrayList.add("BAND_1");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("BAND_2");
      j = i | 0x2;
    } 
    i = j;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("BAND_3");
      i = j | 0x3;
    } 
    j = i;
    if ((paramInt & 0x5) == 5) {
      arrayList.add("BAND_5");
      j = i | 0x5;
    } 
    i = j;
    if ((paramInt & 0x7) == 7) {
      arrayList.add("BAND_7");
      i = j | 0x7;
    } 
    j = i;
    if ((paramInt & 0x8) == 8) {
      arrayList.add("BAND_8");
      j = i | 0x8;
    } 
    i = j;
    if ((paramInt & 0xC) == 12) {
      arrayList.add("BAND_12");
      i = j | 0xC;
    } 
    int k = i;
    if ((paramInt & 0xE) == 14) {
      arrayList.add("BAND_14");
      k = i | 0xE;
    } 
    j = k;
    if ((paramInt & 0x12) == 18) {
      arrayList.add("BAND_18");
      j = k | 0x12;
    } 
    i = j;
    if ((paramInt & 0x14) == 20) {
      arrayList.add("BAND_20");
      i = j | 0x14;
    } 
    j = i;
    if ((paramInt & 0x19) == 25) {
      arrayList.add("BAND_25");
      j = i | 0x19;
    } 
    i = j;
    if ((paramInt & 0x1C) == 28) {
      arrayList.add("BAND_28");
      i = j | 0x1C;
    } 
    j = i;
    if ((paramInt & 0x1D) == 29) {
      arrayList.add("BAND_29");
      j = i | 0x1D;
    } 
    k = j;
    if ((paramInt & 0x1E) == 30) {
      arrayList.add("BAND_30");
      k = j | 0x1E;
    } 
    i = k;
    if ((paramInt & 0x22) == 34) {
      arrayList.add("BAND_34");
      i = k | 0x22;
    } 
    j = i;
    if ((paramInt & 0x26) == 38) {
      arrayList.add("BAND_38");
      j = i | 0x26;
    } 
    i = j;
    if ((paramInt & 0x27) == 39) {
      arrayList.add("BAND_39");
      i = j | 0x27;
    } 
    k = i;
    if ((paramInt & 0x28) == 40) {
      arrayList.add("BAND_40");
      k = i | 0x28;
    } 
    j = k;
    if ((paramInt & 0x29) == 41) {
      arrayList.add("BAND_41");
      j = k | 0x29;
    } 
    k = j;
    if ((paramInt & 0x30) == 48) {
      arrayList.add("BAND_48");
      k = j | 0x30;
    } 
    i = k;
    if ((paramInt & 0x32) == 50) {
      arrayList.add("BAND_50");
      i = k | 0x32;
    } 
    j = i;
    if ((paramInt & 0x33) == 51) {
      arrayList.add("BAND_51");
      j = i | 0x33;
    } 
    i = j;
    if ((paramInt & 0x41) == 65) {
      arrayList.add("BAND_65");
      i = j | 0x41;
    } 
    k = i;
    if ((paramInt & 0x42) == 66) {
      arrayList.add("BAND_66");
      k = i | 0x42;
    } 
    j = k;
    if ((paramInt & 0x46) == 70) {
      arrayList.add("BAND_70");
      j = k | 0x46;
    } 
    i = j;
    if ((paramInt & 0x47) == 71) {
      arrayList.add("BAND_71");
      i = j | 0x47;
    } 
    j = i;
    if ((paramInt & 0x4A) == 74) {
      arrayList.add("BAND_74");
      j = i | 0x4A;
    } 
    i = j;
    if ((paramInt & 0x4B) == 75) {
      arrayList.add("BAND_75");
      i = j | 0x4B;
    } 
    j = i;
    if ((paramInt & 0x4C) == 76) {
      arrayList.add("BAND_76");
      j = i | 0x4C;
    } 
    k = j;
    if ((paramInt & 0x4D) == 77) {
      arrayList.add("BAND_77");
      k = j | 0x4D;
    } 
    i = k;
    if ((paramInt & 0x4E) == 78) {
      arrayList.add("BAND_78");
      i = k | 0x4E;
    } 
    j = i;
    if ((paramInt & 0x4F) == 79) {
      arrayList.add("BAND_79");
      j = i | 0x4F;
    } 
    i = j;
    if ((paramInt & 0x50) == 80) {
      arrayList.add("BAND_80");
      i = j | 0x50;
    } 
    j = i;
    if ((paramInt & 0x51) == 81) {
      arrayList.add("BAND_81");
      j = i | 0x51;
    } 
    i = j;
    if ((paramInt & 0x52) == 82) {
      arrayList.add("BAND_82");
      i = j | 0x52;
    } 
    k = i;
    if ((paramInt & 0x53) == 83) {
      arrayList.add("BAND_83");
      k = i | 0x53;
    } 
    j = k;
    if ((paramInt & 0x54) == 84) {
      arrayList.add("BAND_84");
      j = k | 0x54;
    } 
    i = j;
    if ((paramInt & 0x56) == 86) {
      arrayList.add("BAND_86");
      i = j | 0x56;
    } 
    j = i;
    if ((paramInt & 0x59) == 89) {
      arrayList.add("BAND_89");
      j = i | 0x59;
    } 
    i = j;
    if ((paramInt & 0x5A) == 90) {
      arrayList.add("BAND_90");
      i = j | 0x5A;
    } 
    j = i;
    if ((paramInt & 0x5B) == 91) {
      arrayList.add("BAND_91");
      j = i | 0x5B;
    } 
    i = j;
    if ((paramInt & 0x5C) == 92) {
      arrayList.add("BAND_92");
      i = j | 0x5C;
    } 
    j = i;
    if ((paramInt & 0x5D) == 93) {
      arrayList.add("BAND_93");
      j = i | 0x5D;
    } 
    i = j;
    if ((paramInt & 0x5E) == 94) {
      arrayList.add("BAND_94");
      i = j | 0x5E;
    } 
    j = i;
    if ((paramInt & 0x5F) == 95) {
      arrayList.add("BAND_95");
      j = i | 0x5F;
    } 
    i = j;
    if ((paramInt & 0x101) == 257) {
      arrayList.add("BAND_257");
      i = j | 0x101;
    } 
    j = i;
    if ((paramInt & 0x102) == 258) {
      arrayList.add("BAND_258");
      j = i | 0x102;
    } 
    i = j;
    if ((paramInt & 0x104) == 260) {
      arrayList.add("BAND_260");
      i = j | 0x104;
    } 
    j = i;
    if ((paramInt & 0x105) == 261) {
      arrayList.add("BAND_261");
      j = i | 0x105;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
