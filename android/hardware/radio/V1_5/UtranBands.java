package android.hardware.radio.V1_5;

import java.util.ArrayList;

public final class UtranBands {
  public static final int BAND_1 = 1;
  
  public static final int BAND_10 = 10;
  
  public static final int BAND_11 = 11;
  
  public static final int BAND_12 = 12;
  
  public static final int BAND_13 = 13;
  
  public static final int BAND_14 = 14;
  
  public static final int BAND_19 = 19;
  
  public static final int BAND_2 = 2;
  
  public static final int BAND_20 = 20;
  
  public static final int BAND_21 = 21;
  
  public static final int BAND_22 = 22;
  
  public static final int BAND_25 = 25;
  
  public static final int BAND_26 = 26;
  
  public static final int BAND_3 = 3;
  
  public static final int BAND_4 = 4;
  
  public static final int BAND_5 = 5;
  
  public static final int BAND_6 = 6;
  
  public static final int BAND_7 = 7;
  
  public static final int BAND_8 = 8;
  
  public static final int BAND_9 = 9;
  
  public static final int BAND_A = 101;
  
  public static final int BAND_B = 102;
  
  public static final int BAND_C = 103;
  
  public static final int BAND_D = 104;
  
  public static final int BAND_E = 105;
  
  public static final int BAND_F = 106;
  
  public static final String toString(int paramInt) {
    if (paramInt == 1)
      return "BAND_1"; 
    if (paramInt == 2)
      return "BAND_2"; 
    if (paramInt == 3)
      return "BAND_3"; 
    if (paramInt == 4)
      return "BAND_4"; 
    if (paramInt == 5)
      return "BAND_5"; 
    if (paramInt == 6)
      return "BAND_6"; 
    if (paramInt == 7)
      return "BAND_7"; 
    if (paramInt == 8)
      return "BAND_8"; 
    if (paramInt == 9)
      return "BAND_9"; 
    if (paramInt == 10)
      return "BAND_10"; 
    if (paramInt == 11)
      return "BAND_11"; 
    if (paramInt == 12)
      return "BAND_12"; 
    if (paramInt == 13)
      return "BAND_13"; 
    if (paramInt == 14)
      return "BAND_14"; 
    if (paramInt == 19)
      return "BAND_19"; 
    if (paramInt == 20)
      return "BAND_20"; 
    if (paramInt == 21)
      return "BAND_21"; 
    if (paramInt == 22)
      return "BAND_22"; 
    if (paramInt == 25)
      return "BAND_25"; 
    if (paramInt == 26)
      return "BAND_26"; 
    if (paramInt == 101)
      return "BAND_A"; 
    if (paramInt == 102)
      return "BAND_B"; 
    if (paramInt == 103)
      return "BAND_C"; 
    if (paramInt == 104)
      return "BAND_D"; 
    if (paramInt == 105)
      return "BAND_E"; 
    if (paramInt == 106)
      return "BAND_F"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    if ((paramInt & 0x1) == 1) {
      arrayList.add("BAND_1");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("BAND_2");
      j = i | 0x2;
    } 
    i = j;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("BAND_3");
      i = j | 0x3;
    } 
    int k = i;
    if ((paramInt & 0x4) == 4) {
      arrayList.add("BAND_4");
      k = i | 0x4;
    } 
    j = k;
    if ((paramInt & 0x5) == 5) {
      arrayList.add("BAND_5");
      j = k | 0x5;
    } 
    i = j;
    if ((paramInt & 0x6) == 6) {
      arrayList.add("BAND_6");
      i = j | 0x6;
    } 
    j = i;
    if ((paramInt & 0x7) == 7) {
      arrayList.add("BAND_7");
      j = i | 0x7;
    } 
    k = j;
    if ((paramInt & 0x8) == 8) {
      arrayList.add("BAND_8");
      k = j | 0x8;
    } 
    i = k;
    if ((paramInt & 0x9) == 9) {
      arrayList.add("BAND_9");
      i = k | 0x9;
    } 
    j = i;
    if ((paramInt & 0xA) == 10) {
      arrayList.add("BAND_10");
      j = i | 0xA;
    } 
    i = j;
    if ((paramInt & 0xB) == 11) {
      arrayList.add("BAND_11");
      i = j | 0xB;
    } 
    k = i;
    if ((paramInt & 0xC) == 12) {
      arrayList.add("BAND_12");
      k = i | 0xC;
    } 
    j = k;
    if ((paramInt & 0xD) == 13) {
      arrayList.add("BAND_13");
      j = k | 0xD;
    } 
    i = j;
    if ((paramInt & 0xE) == 14) {
      arrayList.add("BAND_14");
      i = j | 0xE;
    } 
    j = i;
    if ((paramInt & 0x13) == 19) {
      arrayList.add("BAND_19");
      j = i | 0x13;
    } 
    i = j;
    if ((paramInt & 0x14) == 20) {
      arrayList.add("BAND_20");
      i = j | 0x14;
    } 
    j = i;
    if ((paramInt & 0x15) == 21) {
      arrayList.add("BAND_21");
      j = i | 0x15;
    } 
    k = j;
    if ((paramInt & 0x16) == 22) {
      arrayList.add("BAND_22");
      k = j | 0x16;
    } 
    i = k;
    if ((paramInt & 0x19) == 25) {
      arrayList.add("BAND_25");
      i = k | 0x19;
    } 
    j = i;
    if ((paramInt & 0x1A) == 26) {
      arrayList.add("BAND_26");
      j = i | 0x1A;
    } 
    i = j;
    if ((paramInt & 0x65) == 101) {
      arrayList.add("BAND_A");
      i = j | 0x65;
    } 
    j = i;
    if ((paramInt & 0x66) == 102) {
      arrayList.add("BAND_B");
      j = i | 0x66;
    } 
    i = j;
    if ((paramInt & 0x67) == 103) {
      arrayList.add("BAND_C");
      i = j | 0x67;
    } 
    j = i;
    if ((paramInt & 0x68) == 104) {
      arrayList.add("BAND_D");
      j = i | 0x68;
    } 
    i = j;
    if ((paramInt & 0x69) == 105) {
      arrayList.add("BAND_E");
      i = j | 0x69;
    } 
    j = i;
    if ((paramInt & 0x6A) == 106) {
      arrayList.add("BAND_F");
      j = i | 0x6A;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
