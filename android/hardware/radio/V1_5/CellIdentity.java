package android.hardware.radio.V1_5;

import android.hardware.radio.V1_2.CellIdentityCdma;
import android.internal.hidl.safe_union.V1_0.Monostate;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellIdentity {
  private byte hidl_d;
  
  private Object hidl_o;
  
  public CellIdentity() {
    this.hidl_d = 0;
    this.hidl_o = null;
    this.hidl_o = new Monostate();
  }
  
  public static final class hidl_discriminator {
    public static final byte cdma = 4;
    
    public static final byte gsm = 1;
    
    public static final byte lte = 5;
    
    public static final byte noinit = 0;
    
    public static final byte nr = 6;
    
    public static final byte tdscdma = 3;
    
    public static final byte wcdma = 2;
    
    public static final String getName(byte param1Byte) {
      switch (param1Byte) {
        default:
          return "Unknown";
        case 6:
          return "nr";
        case 5:
          return "lte";
        case 4:
          return "cdma";
        case 3:
          return "tdscdma";
        case 2:
          return "wcdma";
        case 1:
          return "gsm";
        case 0:
          break;
      } 
      return "noinit";
    }
  }
  
  public void noinit(Monostate paramMonostate) {
    this.hidl_d = 0;
    this.hidl_o = paramMonostate;
  }
  
  public Monostate noinit() {
    if (this.hidl_d != 0) {
      Object object1 = this.hidl_o;
      if (object1 != null) {
        object1 = object1.getClass().getName();
      } else {
        object1 = "null";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
      stringBuilder.append(this.hidl_d);
      stringBuilder.append(" (corresponding to ");
      byte b = this.hidl_d;
      stringBuilder.append(hidl_discriminator.getName(b));
      stringBuilder.append("), and hidl_o is of type ");
      stringBuilder.append((String)object1);
      stringBuilder.append(".");
      throw new IllegalStateException(stringBuilder.toString());
    } 
    Object object = this.hidl_o;
    if (object == null || Monostate.class.isInstance(object))
      return (Monostate)this.hidl_o; 
    throw new Error("Union is in a corrupted state.");
  }
  
  public void gsm(CellIdentityGsm paramCellIdentityGsm) {
    this.hidl_d = 1;
    this.hidl_o = paramCellIdentityGsm;
  }
  
  public CellIdentityGsm gsm() {
    if (this.hidl_d != 1) {
      Object object1 = this.hidl_o;
      if (object1 != null) {
        object1 = object1.getClass().getName();
      } else {
        object1 = "null";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
      stringBuilder.append(this.hidl_d);
      stringBuilder.append(" (corresponding to ");
      byte b = this.hidl_d;
      stringBuilder.append(hidl_discriminator.getName(b));
      stringBuilder.append("), and hidl_o is of type ");
      stringBuilder.append((String)object1);
      stringBuilder.append(".");
      throw new IllegalStateException(stringBuilder.toString());
    } 
    Object object = this.hidl_o;
    if (object == null || CellIdentityGsm.class.isInstance(object))
      return (CellIdentityGsm)this.hidl_o; 
    throw new Error("Union is in a corrupted state.");
  }
  
  public void wcdma(CellIdentityWcdma paramCellIdentityWcdma) {
    this.hidl_d = 2;
    this.hidl_o = paramCellIdentityWcdma;
  }
  
  public CellIdentityWcdma wcdma() {
    if (this.hidl_d != 2) {
      Object object1 = this.hidl_o;
      if (object1 != null) {
        object1 = object1.getClass().getName();
      } else {
        object1 = "null";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
      stringBuilder.append(this.hidl_d);
      stringBuilder.append(" (corresponding to ");
      byte b = this.hidl_d;
      stringBuilder.append(hidl_discriminator.getName(b));
      stringBuilder.append("), and hidl_o is of type ");
      stringBuilder.append((String)object1);
      stringBuilder.append(".");
      throw new IllegalStateException(stringBuilder.toString());
    } 
    Object object = this.hidl_o;
    if (object == null || CellIdentityWcdma.class.isInstance(object))
      return (CellIdentityWcdma)this.hidl_o; 
    throw new Error("Union is in a corrupted state.");
  }
  
  public void tdscdma(CellIdentityTdscdma paramCellIdentityTdscdma) {
    this.hidl_d = 3;
    this.hidl_o = paramCellIdentityTdscdma;
  }
  
  public CellIdentityTdscdma tdscdma() {
    if (this.hidl_d != 3) {
      Object object1 = this.hidl_o;
      if (object1 != null) {
        object1 = object1.getClass().getName();
      } else {
        object1 = "null";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
      stringBuilder.append(this.hidl_d);
      stringBuilder.append(" (corresponding to ");
      byte b = this.hidl_d;
      stringBuilder.append(hidl_discriminator.getName(b));
      stringBuilder.append("), and hidl_o is of type ");
      stringBuilder.append((String)object1);
      stringBuilder.append(".");
      throw new IllegalStateException(stringBuilder.toString());
    } 
    Object object = this.hidl_o;
    if (object == null || CellIdentityTdscdma.class.isInstance(object))
      return (CellIdentityTdscdma)this.hidl_o; 
    throw new Error("Union is in a corrupted state.");
  }
  
  public void cdma(CellIdentityCdma paramCellIdentityCdma) {
    this.hidl_d = 4;
    this.hidl_o = paramCellIdentityCdma;
  }
  
  public CellIdentityCdma cdma() {
    if (this.hidl_d != 4) {
      Object object1 = this.hidl_o;
      if (object1 != null) {
        object1 = object1.getClass().getName();
      } else {
        object1 = "null";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
      stringBuilder.append(this.hidl_d);
      stringBuilder.append(" (corresponding to ");
      byte b = this.hidl_d;
      stringBuilder.append(hidl_discriminator.getName(b));
      stringBuilder.append("), and hidl_o is of type ");
      stringBuilder.append((String)object1);
      stringBuilder.append(".");
      throw new IllegalStateException(stringBuilder.toString());
    } 
    Object object = this.hidl_o;
    if (object == null || CellIdentityCdma.class.isInstance(object))
      return (CellIdentityCdma)this.hidl_o; 
    throw new Error("Union is in a corrupted state.");
  }
  
  public void lte(CellIdentityLte paramCellIdentityLte) {
    this.hidl_d = 5;
    this.hidl_o = paramCellIdentityLte;
  }
  
  public CellIdentityLte lte() {
    if (this.hidl_d != 5) {
      Object object1 = this.hidl_o;
      if (object1 != null) {
        object1 = object1.getClass().getName();
      } else {
        object1 = "null";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
      stringBuilder.append(this.hidl_d);
      stringBuilder.append(" (corresponding to ");
      byte b = this.hidl_d;
      stringBuilder.append(hidl_discriminator.getName(b));
      stringBuilder.append("), and hidl_o is of type ");
      stringBuilder.append((String)object1);
      stringBuilder.append(".");
      throw new IllegalStateException(stringBuilder.toString());
    } 
    Object object = this.hidl_o;
    if (object == null || CellIdentityLte.class.isInstance(object))
      return (CellIdentityLte)this.hidl_o; 
    throw new Error("Union is in a corrupted state.");
  }
  
  public void nr(CellIdentityNr paramCellIdentityNr) {
    this.hidl_d = 6;
    this.hidl_o = paramCellIdentityNr;
  }
  
  public CellIdentityNr nr() {
    if (this.hidl_d != 6) {
      Object object1 = this.hidl_o;
      if (object1 != null) {
        object1 = object1.getClass().getName();
      } else {
        object1 = "null";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
      stringBuilder.append(this.hidl_d);
      stringBuilder.append(" (corresponding to ");
      byte b = this.hidl_d;
      stringBuilder.append(hidl_discriminator.getName(b));
      stringBuilder.append("), and hidl_o is of type ");
      stringBuilder.append((String)object1);
      stringBuilder.append(".");
      throw new IllegalStateException(stringBuilder.toString());
    } 
    Object object = this.hidl_o;
    if (object == null || CellIdentityNr.class.isInstance(object))
      return (CellIdentityNr)this.hidl_o; 
    throw new Error("Union is in a corrupted state.");
  }
  
  public byte getDiscriminator() {
    return this.hidl_d;
  }
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellIdentity.class)
      return false; 
    paramObject = paramObject;
    if (this.hidl_d != ((CellIdentity)paramObject).hidl_d)
      return false; 
    if (!HidlSupport.deepEquals(this.hidl_o, ((CellIdentity)paramObject).hidl_o))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    Object object = this.hidl_o;
    int i = HidlSupport.deepHashCode(object);
    byte b = this.hidl_d;
    int j = Objects.hashCode(Byte.valueOf(b));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    switch (this.hidl_d) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown union discriminator (value: ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(").");
        throw new Error(stringBuilder.toString());
      case 6:
        stringBuilder.append(".nr = ");
        stringBuilder.append(nr());
        stringBuilder.append("}");
        return stringBuilder.toString();
      case 5:
        stringBuilder.append(".lte = ");
        stringBuilder.append(lte());
        stringBuilder.append("}");
        return stringBuilder.toString();
      case 4:
        stringBuilder.append(".cdma = ");
        stringBuilder.append(cdma());
        stringBuilder.append("}");
        return stringBuilder.toString();
      case 3:
        stringBuilder.append(".tdscdma = ");
        stringBuilder.append(tdscdma());
        stringBuilder.append("}");
        return stringBuilder.toString();
      case 2:
        stringBuilder.append(".wcdma = ");
        stringBuilder.append(wcdma());
        stringBuilder.append("}");
        return stringBuilder.toString();
      case 1:
        stringBuilder.append(".gsm = ");
        stringBuilder.append(gsm());
        stringBuilder.append("}");
        return stringBuilder.toString();
      case 0:
        break;
    } 
    stringBuilder.append(".noinit = ");
    stringBuilder.append(noinit());
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(168L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellIdentity> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellIdentity> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 168);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellIdentity cellIdentity = new CellIdentity();
      cellIdentity.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 168));
      arrayList.add(cellIdentity);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    StringBuilder stringBuilder;
    CellIdentityNr cellIdentityNr;
    CellIdentityLte cellIdentityLte;
    CellIdentityCdma cellIdentityCdma;
    CellIdentityTdscdma cellIdentityTdscdma;
    CellIdentityWcdma cellIdentityWcdma;
    CellIdentityGsm cellIdentityGsm;
    byte b = paramHwBlob.getInt8(0L + paramLong);
    switch (b) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown union discriminator (value: ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(").");
        throw new IllegalStateException(stringBuilder.toString());
      case 6:
        this.hidl_o = cellIdentityNr = new CellIdentityNr();
        cellIdentityNr.readEmbeddedFromParcel((HwParcel)stringBuilder, paramHwBlob, 8L + paramLong);
        return;
      case 5:
        this.hidl_o = cellIdentityLte = new CellIdentityLte();
        cellIdentityLte.readEmbeddedFromParcel((HwParcel)stringBuilder, paramHwBlob, 8L + paramLong);
        return;
      case 4:
        this.hidl_o = cellIdentityCdma = new CellIdentityCdma();
        cellIdentityCdma.readEmbeddedFromParcel((HwParcel)stringBuilder, paramHwBlob, 8L + paramLong);
        return;
      case 3:
        this.hidl_o = cellIdentityTdscdma = new CellIdentityTdscdma();
        cellIdentityTdscdma.readEmbeddedFromParcel((HwParcel)stringBuilder, paramHwBlob, 8L + paramLong);
        return;
      case 2:
        this.hidl_o = cellIdentityWcdma = new CellIdentityWcdma();
        cellIdentityWcdma.readEmbeddedFromParcel((HwParcel)stringBuilder, paramHwBlob, 8L + paramLong);
        return;
      case 1:
        this.hidl_o = cellIdentityGsm = new CellIdentityGsm();
        cellIdentityGsm.readEmbeddedFromParcel((HwParcel)stringBuilder, paramHwBlob, 8L + paramLong);
        return;
      case 0:
        break;
    } 
    Monostate monostate = new Monostate();
    monostate.readEmbeddedFromParcel((HwParcel)stringBuilder, paramHwBlob, 8L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(168);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellIdentity> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 168);
    for (byte b = 0; b < i; b++)
      ((CellIdentity)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 168)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    StringBuilder stringBuilder;
    paramHwBlob.putInt8(0L + paramLong, this.hidl_d);
    switch (this.hidl_d) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown union discriminator (value: ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(").");
        throw new Error(stringBuilder.toString());
      case 6:
        nr().writeEmbeddedToBlob((HwBlob)stringBuilder, 8L + paramLong);
        return;
      case 5:
        lte().writeEmbeddedToBlob((HwBlob)stringBuilder, 8L + paramLong);
        return;
      case 4:
        cdma().writeEmbeddedToBlob((HwBlob)stringBuilder, 8L + paramLong);
        return;
      case 3:
        tdscdma().writeEmbeddedToBlob((HwBlob)stringBuilder, 8L + paramLong);
        return;
      case 2:
        wcdma().writeEmbeddedToBlob((HwBlob)stringBuilder, 8L + paramLong);
        return;
      case 1:
        gsm().writeEmbeddedToBlob((HwBlob)stringBuilder, 8L + paramLong);
        return;
      case 0:
        break;
    } 
    noinit().writeEmbeddedToBlob((HwBlob)stringBuilder, 8L + paramLong);
  }
}
