package android.hardware.radio.V1_5;

import java.util.ArrayList;

public final class PrlIndicator {
  public static final int IN_PRL = 1;
  
  public static final int NOT_IN_PRL = 0;
  
  public static final int NOT_REGISTERED = -1;
  
  public static final String toString(int paramInt) {
    if (paramInt == -1)
      return "NOT_REGISTERED"; 
    if (paramInt == 0)
      return "NOT_IN_PRL"; 
    if (paramInt == 1)
      return "IN_PRL"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    if ((paramInt & 0xFFFFFFFF) == -1) {
      arrayList.add("NOT_REGISTERED");
      i = 0x0 | 0xFFFFFFFF;
    } 
    arrayList.add("NOT_IN_PRL");
    int j = i;
    if ((paramInt & 0x1) == 1) {
      arrayList.add("IN_PRL");
      j = i | 0x1;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
