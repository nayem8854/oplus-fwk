package android.hardware.radio.V1_5;

import android.hardware.radio.V1_0.RegState;
import android.hardware.radio.V1_4.LteVopsInfo;
import android.hardware.radio.V1_4.NrIndicators;
import android.hardware.radio.V1_4.RadioTechnology;
import android.internal.hidl.safe_union.V1_0.Monostate;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class RegStateResult {
  public static final class AccessTechnologySpecificInfo {
    private byte hidl_d;
    
    private Object hidl_o;
    
    public static final class Cdma2000RegistrationInfo {
      public boolean cssSupported = false;
      
      public int roamingIndicator = 0;
      
      public int systemIsInPrl = 0;
      
      public int defaultRoamingIndicator = 0;
      
      public final boolean equals(Object param2Object) {
        if (this == param2Object)
          return true; 
        if (param2Object == null)
          return false; 
        if (param2Object.getClass() != Cdma2000RegistrationInfo.class)
          return false; 
        param2Object = param2Object;
        if (this.cssSupported != ((Cdma2000RegistrationInfo)param2Object).cssSupported)
          return false; 
        if (this.roamingIndicator != ((Cdma2000RegistrationInfo)param2Object).roamingIndicator)
          return false; 
        if (this.systemIsInPrl != ((Cdma2000RegistrationInfo)param2Object).systemIsInPrl)
          return false; 
        if (this.defaultRoamingIndicator != ((Cdma2000RegistrationInfo)param2Object).defaultRoamingIndicator)
          return false; 
        return true;
      }
      
      public final int hashCode() {
        boolean bool = this.cssSupported;
        int i = HidlSupport.deepHashCode(Boolean.valueOf(bool)), j = this.roamingIndicator;
        j = HidlSupport.deepHashCode(Integer.valueOf(j));
        int k = this.systemIsInPrl;
        k = HidlSupport.deepHashCode(Integer.valueOf(k));
        int m = this.defaultRoamingIndicator;
        m = HidlSupport.deepHashCode(Integer.valueOf(m));
        return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
      }
      
      public final String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        stringBuilder.append(".cssSupported = ");
        stringBuilder.append(this.cssSupported);
        stringBuilder.append(", .roamingIndicator = ");
        stringBuilder.append(this.roamingIndicator);
        stringBuilder.append(", .systemIsInPrl = ");
        stringBuilder.append(PrlIndicator.toString(this.systemIsInPrl));
        stringBuilder.append(", .defaultRoamingIndicator = ");
        stringBuilder.append(this.defaultRoamingIndicator);
        stringBuilder.append("}");
        return stringBuilder.toString();
      }
      
      public final void readFromParcel(HwParcel param2HwParcel) {
        HwBlob hwBlob = param2HwParcel.readBuffer(16L);
        readEmbeddedFromParcel(param2HwParcel, hwBlob, 0L);
      }
      
      public static final ArrayList<Cdma2000RegistrationInfo> readVectorFromParcel(HwParcel param2HwParcel) {
        ArrayList<Cdma2000RegistrationInfo> arrayList = new ArrayList();
        HwBlob hwBlob1 = param2HwParcel.readBuffer(16L);
        int i = hwBlob1.getInt32(8L);
        long l1 = (i * 16);
        long l2 = hwBlob1.handle();
        HwBlob hwBlob2 = param2HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
        arrayList.clear();
        for (byte b = 0; b < i; b++) {
          Cdma2000RegistrationInfo cdma2000RegistrationInfo = new Cdma2000RegistrationInfo();
          cdma2000RegistrationInfo.readEmbeddedFromParcel(param2HwParcel, hwBlob2, (b * 16));
          arrayList.add(cdma2000RegistrationInfo);
        } 
        return arrayList;
      }
      
      public final void readEmbeddedFromParcel(HwParcel param2HwParcel, HwBlob param2HwBlob, long param2Long) {
        this.cssSupported = param2HwBlob.getBool(0L + param2Long);
        this.roamingIndicator = param2HwBlob.getInt32(4L + param2Long);
        this.systemIsInPrl = param2HwBlob.getInt32(8L + param2Long);
        this.defaultRoamingIndicator = param2HwBlob.getInt32(12L + param2Long);
      }
      
      public final void writeToParcel(HwParcel param2HwParcel) {
        HwBlob hwBlob = new HwBlob(16);
        writeEmbeddedToBlob(hwBlob, 0L);
        param2HwParcel.writeBuffer(hwBlob);
      }
      
      public static final void writeVectorToParcel(HwParcel param2HwParcel, ArrayList<Cdma2000RegistrationInfo> param2ArrayList) {
        HwBlob hwBlob1 = new HwBlob(16);
        int i = param2ArrayList.size();
        hwBlob1.putInt32(8L, i);
        hwBlob1.putBool(12L, false);
        HwBlob hwBlob2 = new HwBlob(i * 16);
        for (byte b = 0; b < i; b++)
          ((Cdma2000RegistrationInfo)param2ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 16)); 
        hwBlob1.putBlob(0L, hwBlob2);
        param2HwParcel.writeBuffer(hwBlob1);
      }
      
      public final void writeEmbeddedToBlob(HwBlob param2HwBlob, long param2Long) {
        param2HwBlob.putBool(0L + param2Long, this.cssSupported);
        param2HwBlob.putInt32(4L + param2Long, this.roamingIndicator);
        param2HwBlob.putInt32(8L + param2Long, this.systemIsInPrl);
        param2HwBlob.putInt32(12L + param2Long, this.defaultRoamingIndicator);
      }
    }
    
    public static final class EutranRegistrationInfo {
      public LteVopsInfo lteVopsInfo;
      
      public NrIndicators nrIndicators;
      
      public EutranRegistrationInfo() {
        this.lteVopsInfo = new LteVopsInfo();
        this.nrIndicators = new NrIndicators();
      }
      
      public final boolean equals(Object param2Object) {
        if (this == param2Object)
          return true; 
        if (param2Object == null)
          return false; 
        if (param2Object.getClass() != EutranRegistrationInfo.class)
          return false; 
        param2Object = param2Object;
        if (!HidlSupport.deepEquals(this.lteVopsInfo, ((EutranRegistrationInfo)param2Object).lteVopsInfo))
          return false; 
        if (!HidlSupport.deepEquals(this.nrIndicators, ((EutranRegistrationInfo)param2Object).nrIndicators))
          return false; 
        return true;
      }
      
      public final int hashCode() {
        LteVopsInfo lteVopsInfo = this.lteVopsInfo;
        int i = HidlSupport.deepHashCode(lteVopsInfo);
        NrIndicators nrIndicators = this.nrIndicators;
        int j = HidlSupport.deepHashCode(nrIndicators);
        return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
      }
      
      public final String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        stringBuilder.append(".lteVopsInfo = ");
        stringBuilder.append(this.lteVopsInfo);
        stringBuilder.append(", .nrIndicators = ");
        stringBuilder.append(this.nrIndicators);
        stringBuilder.append("}");
        return stringBuilder.toString();
      }
      
      public final void readFromParcel(HwParcel param2HwParcel) {
        HwBlob hwBlob = param2HwParcel.readBuffer(5L);
        readEmbeddedFromParcel(param2HwParcel, hwBlob, 0L);
      }
      
      public static final ArrayList<EutranRegistrationInfo> readVectorFromParcel(HwParcel param2HwParcel) {
        ArrayList<EutranRegistrationInfo> arrayList = new ArrayList();
        HwBlob hwBlob1 = param2HwParcel.readBuffer(16L);
        int i = hwBlob1.getInt32(8L);
        long l1 = (i * 5);
        long l2 = hwBlob1.handle();
        HwBlob hwBlob2 = param2HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
        arrayList.clear();
        for (byte b = 0; b < i; b++) {
          EutranRegistrationInfo eutranRegistrationInfo = new EutranRegistrationInfo();
          eutranRegistrationInfo.readEmbeddedFromParcel(param2HwParcel, hwBlob2, (b * 5));
          arrayList.add(eutranRegistrationInfo);
        } 
        return arrayList;
      }
      
      public final void readEmbeddedFromParcel(HwParcel param2HwParcel, HwBlob param2HwBlob, long param2Long) {
        this.lteVopsInfo.readEmbeddedFromParcel(param2HwParcel, param2HwBlob, 0L + param2Long);
        this.nrIndicators.readEmbeddedFromParcel(param2HwParcel, param2HwBlob, 2L + param2Long);
      }
      
      public final void writeToParcel(HwParcel param2HwParcel) {
        HwBlob hwBlob = new HwBlob(5);
        writeEmbeddedToBlob(hwBlob, 0L);
        param2HwParcel.writeBuffer(hwBlob);
      }
      
      public static final void writeVectorToParcel(HwParcel param2HwParcel, ArrayList<EutranRegistrationInfo> param2ArrayList) {
        HwBlob hwBlob1 = new HwBlob(16);
        int i = param2ArrayList.size();
        hwBlob1.putInt32(8L, i);
        hwBlob1.putBool(12L, false);
        HwBlob hwBlob2 = new HwBlob(i * 5);
        for (byte b = 0; b < i; b++)
          ((EutranRegistrationInfo)param2ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 5)); 
        hwBlob1.putBlob(0L, hwBlob2);
        param2HwParcel.writeBuffer(hwBlob1);
      }
      
      public final void writeEmbeddedToBlob(HwBlob param2HwBlob, long param2Long) {
        this.lteVopsInfo.writeEmbeddedToBlob(param2HwBlob, 0L + param2Long);
        this.nrIndicators.writeEmbeddedToBlob(param2HwBlob, 2L + param2Long);
      }
    }
    
    public AccessTechnologySpecificInfo() {
      this.hidl_d = 0;
      this.hidl_o = null;
      this.hidl_o = new Monostate();
    }
    
    public static final class hidl_discriminator {
      public static final byte cdmaInfo = 1;
      
      public static final byte eutranInfo = 2;
      
      public static final byte noinit = 0;
      
      public static final String getName(byte param2Byte) {
        if (param2Byte != 0) {
          if (param2Byte != 1) {
            if (param2Byte != 2)
              return "Unknown"; 
            return "eutranInfo";
          } 
          return "cdmaInfo";
        } 
        return "noinit";
      }
    }
    
    public void noinit(Monostate param1Monostate) {
      this.hidl_d = 0;
      this.hidl_o = param1Monostate;
    }
    
    public Monostate noinit() {
      if (this.hidl_d != 0) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || Monostate.class.isInstance(object))
        return (Monostate)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public void cdmaInfo(Cdma2000RegistrationInfo param1Cdma2000RegistrationInfo) {
      this.hidl_d = 1;
      this.hidl_o = param1Cdma2000RegistrationInfo;
    }
    
    public Cdma2000RegistrationInfo cdmaInfo() {
      if (this.hidl_d != 1) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || Cdma2000RegistrationInfo.class.isInstance(object))
        return (Cdma2000RegistrationInfo)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public void eutranInfo(EutranRegistrationInfo param1EutranRegistrationInfo) {
      this.hidl_d = 2;
      this.hidl_o = param1EutranRegistrationInfo;
    }
    
    public EutranRegistrationInfo eutranInfo() {
      if (this.hidl_d != 2) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || EutranRegistrationInfo.class.isInstance(object))
        return (EutranRegistrationInfo)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public byte getDiscriminator() {
      return this.hidl_d;
    }
    
    public final boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (param1Object.getClass() != AccessTechnologySpecificInfo.class)
        return false; 
      param1Object = param1Object;
      if (this.hidl_d != ((AccessTechnologySpecificInfo)param1Object).hidl_d)
        return false; 
      if (!HidlSupport.deepEquals(this.hidl_o, ((AccessTechnologySpecificInfo)param1Object).hidl_o))
        return false; 
      return true;
    }
    
    public final int hashCode() {
      Object object = this.hidl_o;
      int i = HidlSupport.deepHashCode(object);
      byte b = this.hidl_d;
      int j = Objects.hashCode(Byte.valueOf(b));
      return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
    }
    
    public final String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      byte b = this.hidl_d;
      if (b != 0) {
        if (b != 1) {
          if (b == 2) {
            stringBuilder.append(".eutranInfo = ");
            stringBuilder.append(eutranInfo());
          } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown union discriminator (value: ");
            stringBuilder.append(this.hidl_d);
            stringBuilder.append(").");
            throw new Error(stringBuilder.toString());
          } 
        } else {
          stringBuilder.append(".cdmaInfo = ");
          stringBuilder.append(cdmaInfo());
        } 
      } else {
        stringBuilder.append(".noinit = ");
        stringBuilder.append(noinit());
      } 
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public final void readFromParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = param1HwParcel.readBuffer(20L);
      readEmbeddedFromParcel(param1HwParcel, hwBlob, 0L);
    }
    
    public static final ArrayList<AccessTechnologySpecificInfo> readVectorFromParcel(HwParcel param1HwParcel) {
      ArrayList<AccessTechnologySpecificInfo> arrayList = new ArrayList();
      HwBlob hwBlob1 = param1HwParcel.readBuffer(16L);
      int i = hwBlob1.getInt32(8L);
      long l1 = (i * 20);
      long l2 = hwBlob1.handle();
      HwBlob hwBlob2 = param1HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
      arrayList.clear();
      for (byte b = 0; b < i; b++) {
        AccessTechnologySpecificInfo accessTechnologySpecificInfo = new AccessTechnologySpecificInfo();
        accessTechnologySpecificInfo.readEmbeddedFromParcel(param1HwParcel, hwBlob2, (b * 20));
        arrayList.add(accessTechnologySpecificInfo);
      } 
      return arrayList;
    }
    
    public final void readEmbeddedFromParcel(HwParcel param1HwParcel, HwBlob param1HwBlob, long param1Long) {
      StringBuilder stringBuilder;
      byte b = param1HwBlob.getInt8(0L + param1Long);
      if (b != 0) {
        if (b != 1) {
          if (b == 2) {
            EutranRegistrationInfo eutranRegistrationInfo = new EutranRegistrationInfo();
            eutranRegistrationInfo.readEmbeddedFromParcel(param1HwParcel, param1HwBlob, 4L + param1Long);
          } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown union discriminator (value: ");
            stringBuilder.append(this.hidl_d);
            stringBuilder.append(").");
            throw new IllegalStateException(stringBuilder.toString());
          } 
        } else {
          Cdma2000RegistrationInfo cdma2000RegistrationInfo = new Cdma2000RegistrationInfo();
          cdma2000RegistrationInfo.readEmbeddedFromParcel((HwParcel)stringBuilder, param1HwBlob, 4L + param1Long);
        } 
      } else {
        Monostate monostate = new Monostate();
        monostate.readEmbeddedFromParcel((HwParcel)stringBuilder, param1HwBlob, 4L + param1Long);
      } 
    }
    
    public final void writeToParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = new HwBlob(20);
      writeEmbeddedToBlob(hwBlob, 0L);
      param1HwParcel.writeBuffer(hwBlob);
    }
    
    public static final void writeVectorToParcel(HwParcel param1HwParcel, ArrayList<AccessTechnologySpecificInfo> param1ArrayList) {
      HwBlob hwBlob1 = new HwBlob(16);
      int i = param1ArrayList.size();
      hwBlob1.putInt32(8L, i);
      hwBlob1.putBool(12L, false);
      HwBlob hwBlob2 = new HwBlob(i * 20);
      for (byte b = 0; b < i; b++)
        ((AccessTechnologySpecificInfo)param1ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 20)); 
      hwBlob1.putBlob(0L, hwBlob2);
      param1HwParcel.writeBuffer(hwBlob1);
    }
    
    public final void writeEmbeddedToBlob(HwBlob param1HwBlob, long param1Long) {
      StringBuilder stringBuilder;
      param1HwBlob.putInt8(0L + param1Long, this.hidl_d);
      byte b = this.hidl_d;
      if (b != 0) {
        if (b != 1) {
          if (b == 2) {
            eutranInfo().writeEmbeddedToBlob(param1HwBlob, 4L + param1Long);
          } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown union discriminator (value: ");
            stringBuilder.append(this.hidl_d);
            stringBuilder.append(").");
            throw new Error(stringBuilder.toString());
          } 
        } else {
          cdmaInfo().writeEmbeddedToBlob((HwBlob)stringBuilder, 4L + param1Long);
        } 
      } else {
        noinit().writeEmbeddedToBlob((HwBlob)stringBuilder, 4L + param1Long);
      } 
    }
  }
  
  public static final class Cdma2000RegistrationInfo {
    public boolean cssSupported = false;
    
    public int roamingIndicator = 0;
    
    public int systemIsInPrl = 0;
    
    public int defaultRoamingIndicator = 0;
    
    public final boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (param1Object.getClass() != Cdma2000RegistrationInfo.class)
        return false; 
      param1Object = param1Object;
      if (this.cssSupported != ((Cdma2000RegistrationInfo)param1Object).cssSupported)
        return false; 
      if (this.roamingIndicator != ((Cdma2000RegistrationInfo)param1Object).roamingIndicator)
        return false; 
      if (this.systemIsInPrl != ((Cdma2000RegistrationInfo)param1Object).systemIsInPrl)
        return false; 
      if (this.defaultRoamingIndicator != ((Cdma2000RegistrationInfo)param1Object).defaultRoamingIndicator)
        return false; 
      return true;
    }
    
    public final int hashCode() {
      boolean bool = this.cssSupported;
      int i = HidlSupport.deepHashCode(Boolean.valueOf(bool)), j = this.roamingIndicator;
      j = HidlSupport.deepHashCode(Integer.valueOf(j));
      int k = this.systemIsInPrl;
      k = HidlSupport.deepHashCode(Integer.valueOf(k));
      int m = this.defaultRoamingIndicator;
      m = HidlSupport.deepHashCode(Integer.valueOf(m));
      return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
    }
    
    public final String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      stringBuilder.append(".cssSupported = ");
      stringBuilder.append(this.cssSupported);
      stringBuilder.append(", .roamingIndicator = ");
      stringBuilder.append(this.roamingIndicator);
      stringBuilder.append(", .systemIsInPrl = ");
      stringBuilder.append(PrlIndicator.toString(this.systemIsInPrl));
      stringBuilder.append(", .defaultRoamingIndicator = ");
      stringBuilder.append(this.defaultRoamingIndicator);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public final void readFromParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = param1HwParcel.readBuffer(16L);
      readEmbeddedFromParcel(param1HwParcel, hwBlob, 0L);
    }
    
    public static final ArrayList<Cdma2000RegistrationInfo> readVectorFromParcel(HwParcel param1HwParcel) {
      ArrayList<Cdma2000RegistrationInfo> arrayList = new ArrayList();
      HwBlob hwBlob1 = param1HwParcel.readBuffer(16L);
      int i = hwBlob1.getInt32(8L);
      long l1 = (i * 16);
      long l2 = hwBlob1.handle();
      HwBlob hwBlob2 = param1HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
      arrayList.clear();
      for (byte b = 0; b < i; b++) {
        Cdma2000RegistrationInfo cdma2000RegistrationInfo = new Cdma2000RegistrationInfo();
        cdma2000RegistrationInfo.readEmbeddedFromParcel(param1HwParcel, hwBlob2, (b * 16));
        arrayList.add(cdma2000RegistrationInfo);
      } 
      return arrayList;
    }
    
    public final void readEmbeddedFromParcel(HwParcel param1HwParcel, HwBlob param1HwBlob, long param1Long) {
      this.cssSupported = param1HwBlob.getBool(0L + param1Long);
      this.roamingIndicator = param1HwBlob.getInt32(4L + param1Long);
      this.systemIsInPrl = param1HwBlob.getInt32(8L + param1Long);
      this.defaultRoamingIndicator = param1HwBlob.getInt32(12L + param1Long);
    }
    
    public final void writeToParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = new HwBlob(16);
      writeEmbeddedToBlob(hwBlob, 0L);
      param1HwParcel.writeBuffer(hwBlob);
    }
    
    public static final void writeVectorToParcel(HwParcel param1HwParcel, ArrayList<Cdma2000RegistrationInfo> param1ArrayList) {
      HwBlob hwBlob1 = new HwBlob(16);
      int i = param1ArrayList.size();
      hwBlob1.putInt32(8L, i);
      hwBlob1.putBool(12L, false);
      HwBlob hwBlob2 = new HwBlob(i * 16);
      for (byte b = 0; b < i; b++)
        ((Cdma2000RegistrationInfo)param1ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 16)); 
      hwBlob1.putBlob(0L, hwBlob2);
      param1HwParcel.writeBuffer(hwBlob1);
    }
    
    public final void writeEmbeddedToBlob(HwBlob param1HwBlob, long param1Long) {
      param1HwBlob.putBool(0L + param1Long, this.cssSupported);
      param1HwBlob.putInt32(4L + param1Long, this.roamingIndicator);
      param1HwBlob.putInt32(8L + param1Long, this.systemIsInPrl);
      param1HwBlob.putInt32(12L + param1Long, this.defaultRoamingIndicator);
    }
  }
  
  public static final class EutranRegistrationInfo {
    public LteVopsInfo lteVopsInfo;
    
    public NrIndicators nrIndicators;
    
    public EutranRegistrationInfo() {
      this.lteVopsInfo = new LteVopsInfo();
      this.nrIndicators = new NrIndicators();
    }
    
    public final boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (param1Object.getClass() != EutranRegistrationInfo.class)
        return false; 
      param1Object = param1Object;
      if (!HidlSupport.deepEquals(this.lteVopsInfo, ((EutranRegistrationInfo)param1Object).lteVopsInfo))
        return false; 
      if (!HidlSupport.deepEquals(this.nrIndicators, ((EutranRegistrationInfo)param1Object).nrIndicators))
        return false; 
      return true;
    }
    
    public final int hashCode() {
      LteVopsInfo lteVopsInfo = this.lteVopsInfo;
      int i = HidlSupport.deepHashCode(lteVopsInfo);
      NrIndicators nrIndicators = this.nrIndicators;
      int j = HidlSupport.deepHashCode(nrIndicators);
      return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
    }
    
    public final String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      stringBuilder.append(".lteVopsInfo = ");
      stringBuilder.append(this.lteVopsInfo);
      stringBuilder.append(", .nrIndicators = ");
      stringBuilder.append(this.nrIndicators);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public final void readFromParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = param1HwParcel.readBuffer(5L);
      readEmbeddedFromParcel(param1HwParcel, hwBlob, 0L);
    }
    
    public static final ArrayList<EutranRegistrationInfo> readVectorFromParcel(HwParcel param1HwParcel) {
      ArrayList<EutranRegistrationInfo> arrayList = new ArrayList();
      HwBlob hwBlob1 = param1HwParcel.readBuffer(16L);
      int i = hwBlob1.getInt32(8L);
      long l1 = (i * 5);
      long l2 = hwBlob1.handle();
      HwBlob hwBlob2 = param1HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
      arrayList.clear();
      for (byte b = 0; b < i; b++) {
        EutranRegistrationInfo eutranRegistrationInfo = new EutranRegistrationInfo();
        eutranRegistrationInfo.readEmbeddedFromParcel(param1HwParcel, hwBlob2, (b * 5));
        arrayList.add(eutranRegistrationInfo);
      } 
      return arrayList;
    }
    
    public final void readEmbeddedFromParcel(HwParcel param1HwParcel, HwBlob param1HwBlob, long param1Long) {
      this.lteVopsInfo.readEmbeddedFromParcel(param1HwParcel, param1HwBlob, 0L + param1Long);
      this.nrIndicators.readEmbeddedFromParcel(param1HwParcel, param1HwBlob, 2L + param1Long);
    }
    
    public final void writeToParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = new HwBlob(5);
      writeEmbeddedToBlob(hwBlob, 0L);
      param1HwParcel.writeBuffer(hwBlob);
    }
    
    public static final void writeVectorToParcel(HwParcel param1HwParcel, ArrayList<EutranRegistrationInfo> param1ArrayList) {
      HwBlob hwBlob1 = new HwBlob(16);
      int i = param1ArrayList.size();
      hwBlob1.putInt32(8L, i);
      hwBlob1.putBool(12L, false);
      HwBlob hwBlob2 = new HwBlob(i * 5);
      for (byte b = 0; b < i; b++)
        ((EutranRegistrationInfo)param1ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 5)); 
      hwBlob1.putBlob(0L, hwBlob2);
      param1HwParcel.writeBuffer(hwBlob1);
    }
    
    public final void writeEmbeddedToBlob(HwBlob param1HwBlob, long param1Long) {
      this.lteVopsInfo.writeEmbeddedToBlob(param1HwBlob, 0L + param1Long);
      this.nrIndicators.writeEmbeddedToBlob(param1HwBlob, 2L + param1Long);
    }
  }
  
  public static final class hidl_discriminator {
    public static final byte cdmaInfo = 1;
    
    public static final byte eutranInfo = 2;
    
    public static final byte noinit = 0;
    
    public static final String getName(byte param1Byte) {
      if (param1Byte != 0) {
        if (param1Byte != 1) {
          if (param1Byte != 2)
            return "Unknown"; 
          return "eutranInfo";
        } 
        return "cdmaInfo";
      } 
      return "noinit";
    }
  }
  
  public int regState = 0;
  
  public int rat = 0;
  
  public int reasonForDenial = 0;
  
  public CellIdentity cellIdentity = new CellIdentity();
  
  public String registeredPlmn = new String();
  
  public AccessTechnologySpecificInfo accessTechnologySpecificInfo = new AccessTechnologySpecificInfo();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != RegStateResult.class)
      return false; 
    paramObject = paramObject;
    if (this.regState != ((RegStateResult)paramObject).regState)
      return false; 
    if (this.rat != ((RegStateResult)paramObject).rat)
      return false; 
    if (this.reasonForDenial != ((RegStateResult)paramObject).reasonForDenial)
      return false; 
    if (!HidlSupport.deepEquals(this.cellIdentity, ((RegStateResult)paramObject).cellIdentity))
      return false; 
    if (!HidlSupport.deepEquals(this.registeredPlmn, ((RegStateResult)paramObject).registeredPlmn))
      return false; 
    if (!HidlSupport.deepEquals(this.accessTechnologySpecificInfo, ((RegStateResult)paramObject).accessTechnologySpecificInfo))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.regState;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.rat;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.reasonForDenial;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    CellIdentity cellIdentity = this.cellIdentity;
    int m = HidlSupport.deepHashCode(cellIdentity);
    String str = this.registeredPlmn;
    int n = HidlSupport.deepHashCode(str);
    AccessTechnologySpecificInfo accessTechnologySpecificInfo = this.accessTechnologySpecificInfo;
    int i1 = HidlSupport.deepHashCode(accessTechnologySpecificInfo);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".regState = ");
    stringBuilder.append(RegState.toString(this.regState));
    stringBuilder.append(", .rat = ");
    stringBuilder.append(RadioTechnology.toString(this.rat));
    stringBuilder.append(", .reasonForDenial = ");
    stringBuilder.append(RegistrationFailCause.toString(this.reasonForDenial));
    stringBuilder.append(", .cellIdentity = ");
    stringBuilder.append(this.cellIdentity);
    stringBuilder.append(", .registeredPlmn = ");
    stringBuilder.append(this.registeredPlmn);
    stringBuilder.append(", .accessTechnologySpecificInfo = ");
    stringBuilder.append(this.accessTechnologySpecificInfo);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(224L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<RegStateResult> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<RegStateResult> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 224);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      RegStateResult regStateResult = new RegStateResult();
      regStateResult.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 224));
      arrayList.add(regStateResult);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.regState = paramHwBlob.getInt32(paramLong + 0L);
    this.rat = paramHwBlob.getInt32(paramLong + 4L);
    this.reasonForDenial = paramHwBlob.getInt32(paramLong + 8L);
    this.cellIdentity.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 16L);
    String str = paramHwBlob.getString(paramLong + 184L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 184L + 0L, false);
    this.accessTechnologySpecificInfo.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 200L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(224);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<RegStateResult> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 224);
    for (byte b = 0; b < i; b++)
      ((RegStateResult)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 224)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.regState);
    paramHwBlob.putInt32(4L + paramLong, this.rat);
    paramHwBlob.putInt32(8L + paramLong, this.reasonForDenial);
    this.cellIdentity.writeEmbeddedToBlob(paramHwBlob, 16L + paramLong);
    paramHwBlob.putString(184L + paramLong, this.registeredPlmn);
    this.accessTechnologySpecificInfo.writeEmbeddedToBlob(paramHwBlob, 200L + paramLong);
  }
}
