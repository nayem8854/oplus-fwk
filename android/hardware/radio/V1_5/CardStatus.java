package android.hardware.radio.V1_5;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CardStatus {
  public android.hardware.radio.V1_4.CardStatus base = new android.hardware.radio.V1_4.CardStatus();
  
  public ArrayList<AppStatus> applications = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CardStatus.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.base, ((CardStatus)paramObject).base))
      return false; 
    if (!HidlSupport.deepEquals(this.applications, ((CardStatus)paramObject).applications))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    android.hardware.radio.V1_4.CardStatus cardStatus = this.base;
    int i = HidlSupport.deepHashCode(cardStatus);
    ArrayList<AppStatus> arrayList = this.applications;
    int j = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".base = ");
    stringBuilder.append(this.base);
    stringBuilder.append(", .applications = ");
    stringBuilder.append(this.applications);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(112L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CardStatus> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CardStatus> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 112);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CardStatus cardStatus = new CardStatus();
      cardStatus.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 112));
      arrayList.add(cardStatus);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.base.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 0L);
    int i = paramHwBlob.getInt32(paramLong + 96L + 8L);
    long l1 = (i * 72);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 96L + 0L, true);
    this.applications.clear();
    for (byte b = 0; b < i; b++) {
      AppStatus appStatus = new AppStatus();
      appStatus.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 72));
      this.applications.add(appStatus);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(112);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CardStatus> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 112);
    for (byte b = 0; b < i; b++)
      ((CardStatus)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 112)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.base.writeEmbeddedToBlob(paramHwBlob, paramLong + 0L);
    int i = this.applications.size();
    paramHwBlob.putInt32(paramLong + 96L + 8L, i);
    paramHwBlob.putBool(paramLong + 96L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 72);
    for (byte b = 0; b < i; b++)
      ((AppStatus)this.applications.get(b)).writeEmbeddedToBlob(hwBlob, (b * 72)); 
    paramHwBlob.putBlob(96L + paramLong + 0L, hwBlob);
  }
}
