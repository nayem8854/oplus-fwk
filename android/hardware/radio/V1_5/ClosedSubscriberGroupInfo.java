package android.hardware.radio.V1_5;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class ClosedSubscriberGroupInfo {
  public boolean csgIndication = false;
  
  public String homeNodebName = new String();
  
  public int csgIdentity = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != ClosedSubscriberGroupInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.csgIndication != ((ClosedSubscriberGroupInfo)paramObject).csgIndication)
      return false; 
    if (!HidlSupport.deepEquals(this.homeNodebName, ((ClosedSubscriberGroupInfo)paramObject).homeNodebName))
      return false; 
    if (this.csgIdentity != ((ClosedSubscriberGroupInfo)paramObject).csgIdentity)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    boolean bool = this.csgIndication;
    int i = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    String str = this.homeNodebName;
    int j = HidlSupport.deepHashCode(str), k = this.csgIdentity;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".csgIndication = ");
    stringBuilder.append(this.csgIndication);
    stringBuilder.append(", .homeNodebName = ");
    stringBuilder.append(this.homeNodebName);
    stringBuilder.append(", .csgIdentity = ");
    stringBuilder.append(this.csgIdentity);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(32L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<ClosedSubscriberGroupInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<ClosedSubscriberGroupInfo> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 32);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      ClosedSubscriberGroupInfo closedSubscriberGroupInfo = new ClosedSubscriberGroupInfo();
      closedSubscriberGroupInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 32));
      arrayList.add(closedSubscriberGroupInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.csgIndication = paramHwBlob.getBool(paramLong + 0L);
    String str = paramHwBlob.getString(paramLong + 8L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, false);
    this.csgIdentity = paramHwBlob.getInt32(paramLong + 24L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(32);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<ClosedSubscriberGroupInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 32);
    for (byte b = 0; b < i; b++)
      ((ClosedSubscriberGroupInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 32)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putBool(0L + paramLong, this.csgIndication);
    paramHwBlob.putString(8L + paramLong, this.homeNodebName);
    paramHwBlob.putInt32(24L + paramLong, this.csgIdentity);
  }
}
