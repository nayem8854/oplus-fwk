package android.hardware.radio.V1_5;

import java.util.ArrayList;

public final class RegistrationFailCause {
  public static final int CALL_CANNOT_BE_IDENTIFIED = 38;
  
  public static final int CONDITIONAL_IE_ERROR = 100;
  
  public static final int CONGESTION = 22;
  
  public static final int GPRS_AND_NON_GPRS_SERVICES_NOT_ALLOWED = 8;
  
  public static final int GPRS_SERVICES_NOT_ALLOWED = 7;
  
  public static final int GPRS_SERVICES_NOT_ALLOWED_IN_PLMN = 14;
  
  public static final int GSM_AUTHENTICATION_UNACCEPTABLE = 23;
  
  public static final int ILLEGAL_ME = 6;
  
  public static final int ILLEGAL_MS = 3;
  
  public static final int IMEI_NOT_ACCEPTED = 5;
  
  public static final int IMPLICITLY_DETACHED = 10;
  
  public static final int IMSI_UNKNOWN_IN_HLR = 2;
  
  public static final int IMSI_UNKNOWN_IN_VLR = 4;
  
  public static final int INFORMATION_ELEMENT_NON_EXISTENT_OR_NOT_IMPLEMENTED = 99;
  
  public static final int INVALID_MANDATORY_INFORMATION = 96;
  
  public static final int LOCATION_AREA_NOT_ALLOWED = 12;
  
  public static final int MAC_FAILURE = 20;
  
  public static final int MESSAGE_NOT_COMPATIBLE_WITH_PROTOCOL_STATE = 101;
  
  public static final int MESSAGE_TYPE_NON_EXISTENT_OR_NOT_IMPLEMENTED = 97;
  
  public static final int MESSAGE_TYPE_NOT_COMPATIBLE_WITH_PROTOCOL_STATE = 98;
  
  public static final int MSC_TEMPORARILY_NOT_REACHABLE = 15;
  
  public static final int MS_IDENTITY_CANNOT_BE_DERIVED_BY_NETWORK = 9;
  
  public static final int NETWORK_FAILURE = 17;
  
  public static final int NONE = 0;
  
  public static final int NOT_AUTHORIZED_FOR_THIS_CSG = 25;
  
  public static final int NO_PDP_CONTEXT_ACTIVATED = 40;
  
  public static final int NO_SUITABLE_CELLS = 15;
  
  public static final int PLMN_NOT_ALLOWED = 11;
  
  public static final int PROTOCOL_ERROR_UNSPECIFIED = 111;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_1 = 48;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_10 = 57;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_11 = 58;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_12 = 59;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_13 = 60;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_14 = 61;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_15 = 62;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_16 = 63;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_2 = 49;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_3 = 50;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_4 = 51;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_5 = 52;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_6 = 53;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_7 = 54;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_8 = 55;
  
  public static final int RETRY_UPON_ENTRY_INTO_NEW_CELL_9 = 56;
  
  public static final int ROAMING_NOT_ALLOWED = 13;
  
  public static final int SEMANTICALLY_INCORRECT_MESSAGE = 95;
  
  public static final int SERVICE_OPTION_NOT_SUBSCRIBED = 33;
  
  public static final int SERVICE_OPTION_NOT_SUPPORTED = 32;
  
  public static final int SERVICE_OPTION_TEMPORARILY_OUT_OF_ORDER = 34;
  
  public static final int SMS_PROVIDED_BY_GPRS_IN_ROUTING_AREA = 26;
  
  public static final int SYNC_FAILURE = 21;
  
  public static final String toString(int paramInt) {
    if (paramInt == 0)
      return "NONE"; 
    if (paramInt == 2)
      return "IMSI_UNKNOWN_IN_HLR"; 
    if (paramInt == 3)
      return "ILLEGAL_MS"; 
    if (paramInt == 4)
      return "IMSI_UNKNOWN_IN_VLR"; 
    if (paramInt == 5)
      return "IMEI_NOT_ACCEPTED"; 
    if (paramInt == 6)
      return "ILLEGAL_ME"; 
    if (paramInt == 7)
      return "GPRS_SERVICES_NOT_ALLOWED"; 
    if (paramInt == 8)
      return "GPRS_AND_NON_GPRS_SERVICES_NOT_ALLOWED"; 
    if (paramInt == 9)
      return "MS_IDENTITY_CANNOT_BE_DERIVED_BY_NETWORK"; 
    if (paramInt == 10)
      return "IMPLICITLY_DETACHED"; 
    if (paramInt == 11)
      return "PLMN_NOT_ALLOWED"; 
    if (paramInt == 12)
      return "LOCATION_AREA_NOT_ALLOWED"; 
    if (paramInt == 13)
      return "ROAMING_NOT_ALLOWED"; 
    if (paramInt == 14)
      return "GPRS_SERVICES_NOT_ALLOWED_IN_PLMN"; 
    if (paramInt == 15)
      return "NO_SUITABLE_CELLS"; 
    if (paramInt == 15)
      return "MSC_TEMPORARILY_NOT_REACHABLE"; 
    if (paramInt == 17)
      return "NETWORK_FAILURE"; 
    if (paramInt == 20)
      return "MAC_FAILURE"; 
    if (paramInt == 21)
      return "SYNC_FAILURE"; 
    if (paramInt == 22)
      return "CONGESTION"; 
    if (paramInt == 23)
      return "GSM_AUTHENTICATION_UNACCEPTABLE"; 
    if (paramInt == 25)
      return "NOT_AUTHORIZED_FOR_THIS_CSG"; 
    if (paramInt == 26)
      return "SMS_PROVIDED_BY_GPRS_IN_ROUTING_AREA"; 
    if (paramInt == 32)
      return "SERVICE_OPTION_NOT_SUPPORTED"; 
    if (paramInt == 33)
      return "SERVICE_OPTION_NOT_SUBSCRIBED"; 
    if (paramInt == 34)
      return "SERVICE_OPTION_TEMPORARILY_OUT_OF_ORDER"; 
    if (paramInt == 38)
      return "CALL_CANNOT_BE_IDENTIFIED"; 
    if (paramInt == 40)
      return "NO_PDP_CONTEXT_ACTIVATED"; 
    if (paramInt == 48)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_1"; 
    if (paramInt == 49)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_2"; 
    if (paramInt == 50)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_3"; 
    if (paramInt == 51)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_4"; 
    if (paramInt == 52)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_5"; 
    if (paramInt == 53)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_6"; 
    if (paramInt == 54)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_7"; 
    if (paramInt == 55)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_8"; 
    if (paramInt == 56)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_9"; 
    if (paramInt == 57)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_10"; 
    if (paramInt == 58)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_11"; 
    if (paramInt == 59)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_12"; 
    if (paramInt == 60)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_13"; 
    if (paramInt == 61)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_14"; 
    if (paramInt == 62)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_15"; 
    if (paramInt == 63)
      return "RETRY_UPON_ENTRY_INTO_NEW_CELL_16"; 
    if (paramInt == 95)
      return "SEMANTICALLY_INCORRECT_MESSAGE"; 
    if (paramInt == 96)
      return "INVALID_MANDATORY_INFORMATION"; 
    if (paramInt == 97)
      return "MESSAGE_TYPE_NON_EXISTENT_OR_NOT_IMPLEMENTED"; 
    if (paramInt == 98)
      return "MESSAGE_TYPE_NOT_COMPATIBLE_WITH_PROTOCOL_STATE"; 
    if (paramInt == 99)
      return "INFORMATION_ELEMENT_NON_EXISTENT_OR_NOT_IMPLEMENTED"; 
    if (paramInt == 100)
      return "CONDITIONAL_IE_ERROR"; 
    if (paramInt == 101)
      return "MESSAGE_NOT_COMPATIBLE_WITH_PROTOCOL_STATE"; 
    if (paramInt == 111)
      return "PROTOCOL_ERROR_UNSPECIFIED"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    arrayList.add("NONE");
    if ((paramInt & 0x2) == 2) {
      arrayList.add("IMSI_UNKNOWN_IN_HLR");
      i = 0x0 | 0x2;
    } 
    int j = i;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("ILLEGAL_MS");
      j = i | 0x3;
    } 
    i = j;
    if ((paramInt & 0x4) == 4) {
      arrayList.add("IMSI_UNKNOWN_IN_VLR");
      i = j | 0x4;
    } 
    j = i;
    if ((paramInt & 0x5) == 5) {
      arrayList.add("IMEI_NOT_ACCEPTED");
      j = i | 0x5;
    } 
    i = j;
    if ((paramInt & 0x6) == 6) {
      arrayList.add("ILLEGAL_ME");
      i = j | 0x6;
    } 
    j = i;
    if ((paramInt & 0x7) == 7) {
      arrayList.add("GPRS_SERVICES_NOT_ALLOWED");
      j = i | 0x7;
    } 
    i = j;
    if ((paramInt & 0x8) == 8) {
      arrayList.add("GPRS_AND_NON_GPRS_SERVICES_NOT_ALLOWED");
      i = j | 0x8;
    } 
    j = i;
    if ((paramInt & 0x9) == 9) {
      arrayList.add("MS_IDENTITY_CANNOT_BE_DERIVED_BY_NETWORK");
      j = i | 0x9;
    } 
    i = j;
    if ((paramInt & 0xA) == 10) {
      arrayList.add("IMPLICITLY_DETACHED");
      i = j | 0xA;
    } 
    int k = i;
    if ((paramInt & 0xB) == 11) {
      arrayList.add("PLMN_NOT_ALLOWED");
      k = i | 0xB;
    } 
    j = k;
    if ((paramInt & 0xC) == 12) {
      arrayList.add("LOCATION_AREA_NOT_ALLOWED");
      j = k | 0xC;
    } 
    i = j;
    if ((paramInt & 0xD) == 13) {
      arrayList.add("ROAMING_NOT_ALLOWED");
      i = j | 0xD;
    } 
    j = i;
    if ((paramInt & 0xE) == 14) {
      arrayList.add("GPRS_SERVICES_NOT_ALLOWED_IN_PLMN");
      j = i | 0xE;
    } 
    i = j;
    if ((paramInt & 0xF) == 15) {
      arrayList.add("NO_SUITABLE_CELLS");
      i = j | 0xF;
    } 
    k = i;
    if ((paramInt & 0xF) == 15) {
      arrayList.add("MSC_TEMPORARILY_NOT_REACHABLE");
      k = i | 0xF;
    } 
    j = k;
    if ((paramInt & 0x11) == 17) {
      arrayList.add("NETWORK_FAILURE");
      j = k | 0x11;
    } 
    i = j;
    if ((paramInt & 0x14) == 20) {
      arrayList.add("MAC_FAILURE");
      i = j | 0x14;
    } 
    j = i;
    if ((paramInt & 0x15) == 21) {
      arrayList.add("SYNC_FAILURE");
      j = i | 0x15;
    } 
    i = j;
    if ((paramInt & 0x16) == 22) {
      arrayList.add("CONGESTION");
      i = j | 0x16;
    } 
    j = i;
    if ((paramInt & 0x17) == 23) {
      arrayList.add("GSM_AUTHENTICATION_UNACCEPTABLE");
      j = i | 0x17;
    } 
    i = j;
    if ((paramInt & 0x19) == 25) {
      arrayList.add("NOT_AUTHORIZED_FOR_THIS_CSG");
      i = j | 0x19;
    } 
    j = i;
    if ((paramInt & 0x1A) == 26) {
      arrayList.add("SMS_PROVIDED_BY_GPRS_IN_ROUTING_AREA");
      j = i | 0x1A;
    } 
    k = j;
    if ((paramInt & 0x20) == 32) {
      arrayList.add("SERVICE_OPTION_NOT_SUPPORTED");
      k = j | 0x20;
    } 
    i = k;
    if ((paramInt & 0x21) == 33) {
      arrayList.add("SERVICE_OPTION_NOT_SUBSCRIBED");
      i = k | 0x21;
    } 
    j = i;
    if ((paramInt & 0x22) == 34) {
      arrayList.add("SERVICE_OPTION_TEMPORARILY_OUT_OF_ORDER");
      j = i | 0x22;
    } 
    i = j;
    if ((paramInt & 0x26) == 38) {
      arrayList.add("CALL_CANNOT_BE_IDENTIFIED");
      i = j | 0x26;
    } 
    j = i;
    if ((paramInt & 0x28) == 40) {
      arrayList.add("NO_PDP_CONTEXT_ACTIVATED");
      j = i | 0x28;
    } 
    i = j;
    if ((paramInt & 0x30) == 48) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_1");
      i = j | 0x30;
    } 
    k = i;
    if ((paramInt & 0x31) == 49) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_2");
      k = i | 0x31;
    } 
    j = k;
    if ((paramInt & 0x32) == 50) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_3");
      j = k | 0x32;
    } 
    i = j;
    if ((paramInt & 0x33) == 51) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_4");
      i = j | 0x33;
    } 
    j = i;
    if ((paramInt & 0x34) == 52) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_5");
      j = i | 0x34;
    } 
    k = j;
    if ((paramInt & 0x35) == 53) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_6");
      k = j | 0x35;
    } 
    i = k;
    if ((paramInt & 0x36) == 54) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_7");
      i = k | 0x36;
    } 
    j = i;
    if ((paramInt & 0x37) == 55) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_8");
      j = i | 0x37;
    } 
    i = j;
    if ((paramInt & 0x38) == 56) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_9");
      i = j | 0x38;
    } 
    k = i;
    if ((paramInt & 0x39) == 57) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_10");
      k = i | 0x39;
    } 
    j = k;
    if ((paramInt & 0x3A) == 58) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_11");
      j = k | 0x3A;
    } 
    i = j;
    if ((paramInt & 0x3B) == 59) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_12");
      i = j | 0x3B;
    } 
    j = i;
    if ((paramInt & 0x3C) == 60) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_13");
      j = i | 0x3C;
    } 
    i = j;
    if ((paramInt & 0x3D) == 61) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_14");
      i = j | 0x3D;
    } 
    j = i;
    if ((paramInt & 0x3E) == 62) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_15");
      j = i | 0x3E;
    } 
    i = j;
    if ((paramInt & 0x3F) == 63) {
      arrayList.add("RETRY_UPON_ENTRY_INTO_NEW_CELL_16");
      i = j | 0x3F;
    } 
    j = i;
    if ((paramInt & 0x5F) == 95) {
      arrayList.add("SEMANTICALLY_INCORRECT_MESSAGE");
      j = i | 0x5F;
    } 
    i = j;
    if ((paramInt & 0x60) == 96) {
      arrayList.add("INVALID_MANDATORY_INFORMATION");
      i = j | 0x60;
    } 
    j = i;
    if ((paramInt & 0x61) == 97) {
      arrayList.add("MESSAGE_TYPE_NON_EXISTENT_OR_NOT_IMPLEMENTED");
      j = i | 0x61;
    } 
    i = j;
    if ((paramInt & 0x62) == 98) {
      arrayList.add("MESSAGE_TYPE_NOT_COMPATIBLE_WITH_PROTOCOL_STATE");
      i = j | 0x62;
    } 
    j = i;
    if ((paramInt & 0x63) == 99) {
      arrayList.add("INFORMATION_ELEMENT_NON_EXISTENT_OR_NOT_IMPLEMENTED");
      j = i | 0x63;
    } 
    i = j;
    if ((paramInt & 0x64) == 100) {
      arrayList.add("CONDITIONAL_IE_ERROR");
      i = j | 0x64;
    } 
    j = i;
    if ((paramInt & 0x65) == 101) {
      arrayList.add("MESSAGE_NOT_COMPATIBLE_WITH_PROTOCOL_STATE");
      j = i | 0x65;
    } 
    i = j;
    if ((paramInt & 0x6F) == 111) {
      arrayList.add("PROTOCOL_ERROR_UNSPECIFIED");
      i = j | 0x6F;
    } 
    if (paramInt != i) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((i ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
