package android.hardware.radio.V1_5;

import android.hardware.radio.V1_0.GsmSignalStrength;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellInfoGsm {
  public CellIdentityGsm cellIdentityGsm = new CellIdentityGsm();
  
  public GsmSignalStrength signalStrengthGsm = new GsmSignalStrength();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellInfoGsm.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.cellIdentityGsm, ((CellInfoGsm)paramObject).cellIdentityGsm))
      return false; 
    if (!HidlSupport.deepEquals(this.signalStrengthGsm, ((CellInfoGsm)paramObject).signalStrengthGsm))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    CellIdentityGsm cellIdentityGsm = this.cellIdentityGsm;
    int i = HidlSupport.deepHashCode(cellIdentityGsm);
    GsmSignalStrength gsmSignalStrength = this.signalStrengthGsm;
    int j = HidlSupport.deepHashCode(gsmSignalStrength);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".cellIdentityGsm = ");
    stringBuilder.append(this.cellIdentityGsm);
    stringBuilder.append(", .signalStrengthGsm = ");
    stringBuilder.append(this.signalStrengthGsm);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(112L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellInfoGsm> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellInfoGsm> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 112);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellInfoGsm cellInfoGsm = new CellInfoGsm();
      cellInfoGsm.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 112));
      arrayList.add(cellInfoGsm);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.cellIdentityGsm.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 0L + paramLong);
    this.signalStrengthGsm.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 96L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(112);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellInfoGsm> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 112);
    for (byte b = 0; b < i; b++)
      ((CellInfoGsm)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 112)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.cellIdentityGsm.writeEmbeddedToBlob(paramHwBlob, 0L + paramLong);
    this.signalStrengthGsm.writeEmbeddedToBlob(paramHwBlob, 96L + paramLong);
  }
}
