package android.hardware.radio.V1_5;

import java.util.ArrayList;

public final class RadioAccessNetworks {
  public static final int CDMA2000 = 5;
  
  public static final int EUTRAN = 3;
  
  public static final int GERAN = 1;
  
  public static final int NGRAN = 4;
  
  public static final int UNKNOWN = 0;
  
  public static final int UTRAN = 2;
  
  public static final String toString(int paramInt) {
    if (paramInt == 1)
      return "GERAN"; 
    if (paramInt == 2)
      return "UTRAN"; 
    if (paramInt == 3)
      return "EUTRAN"; 
    if (paramInt == 0)
      return "UNKNOWN"; 
    if (paramInt == 4)
      return "NGRAN"; 
    if (paramInt == 5)
      return "CDMA2000"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    if ((paramInt & 0x1) == 1) {
      arrayList.add("GERAN");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("UTRAN");
      j = i | 0x2;
    } 
    i = j;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("EUTRAN");
      i = j | 0x3;
    } 
    arrayList.add("UNKNOWN");
    j = i;
    if ((paramInt & 0x4) == 4) {
      arrayList.add("NGRAN");
      j = i | 0x4;
    } 
    i = j;
    if ((paramInt & 0x5) == 5) {
      arrayList.add("CDMA2000");
      i = j | 0x5;
    } 
    if (paramInt != i) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((i ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
