package android.hardware.radio.V1_5;

import android.internal.hidl.safe_union.V1_0.Monostate;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class BarringInfo {
  public static final class ServiceType {
    public static final int CS_FALLBACK = 5;
    
    public static final int CS_SERVICE = 0;
    
    public static final int CS_VOICE = 2;
    
    public static final int EMERGENCY = 8;
    
    public static final int MMTEL_VIDEO = 7;
    
    public static final int MMTEL_VOICE = 6;
    
    public static final int MO_DATA = 4;
    
    public static final int MO_SIGNALLING = 3;
    
    public static final int OPERATOR_1 = 1001;
    
    public static final int OPERATOR_10 = 1010;
    
    public static final int OPERATOR_11 = 1011;
    
    public static final int OPERATOR_12 = 1012;
    
    public static final int OPERATOR_13 = 1013;
    
    public static final int OPERATOR_14 = 1014;
    
    public static final int OPERATOR_15 = 1015;
    
    public static final int OPERATOR_16 = 1016;
    
    public static final int OPERATOR_17 = 1017;
    
    public static final int OPERATOR_18 = 1018;
    
    public static final int OPERATOR_19 = 1019;
    
    public static final int OPERATOR_2 = 1002;
    
    public static final int OPERATOR_20 = 1020;
    
    public static final int OPERATOR_21 = 1021;
    
    public static final int OPERATOR_22 = 1022;
    
    public static final int OPERATOR_23 = 1023;
    
    public static final int OPERATOR_24 = 1024;
    
    public static final int OPERATOR_25 = 1025;
    
    public static final int OPERATOR_26 = 1026;
    
    public static final int OPERATOR_27 = 1027;
    
    public static final int OPERATOR_28 = 1028;
    
    public static final int OPERATOR_29 = 1029;
    
    public static final int OPERATOR_3 = 1003;
    
    public static final int OPERATOR_30 = 1030;
    
    public static final int OPERATOR_31 = 1031;
    
    public static final int OPERATOR_32 = 1032;
    
    public static final int OPERATOR_4 = 1004;
    
    public static final int OPERATOR_5 = 1005;
    
    public static final int OPERATOR_6 = 1006;
    
    public static final int OPERATOR_7 = 1007;
    
    public static final int OPERATOR_8 = 1008;
    
    public static final int OPERATOR_9 = 1009;
    
    public static final int PS_SERVICE = 1;
    
    public static final int SMS = 9;
    
    public static final String toString(int param1Int) {
      if (param1Int == 0)
        return "CS_SERVICE"; 
      if (param1Int == 1)
        return "PS_SERVICE"; 
      if (param1Int == 2)
        return "CS_VOICE"; 
      if (param1Int == 3)
        return "MO_SIGNALLING"; 
      if (param1Int == 4)
        return "MO_DATA"; 
      if (param1Int == 5)
        return "CS_FALLBACK"; 
      if (param1Int == 6)
        return "MMTEL_VOICE"; 
      if (param1Int == 7)
        return "MMTEL_VIDEO"; 
      if (param1Int == 8)
        return "EMERGENCY"; 
      if (param1Int == 9)
        return "SMS"; 
      if (param1Int == 1001)
        return "OPERATOR_1"; 
      if (param1Int == 1002)
        return "OPERATOR_2"; 
      if (param1Int == 1003)
        return "OPERATOR_3"; 
      if (param1Int == 1004)
        return "OPERATOR_4"; 
      if (param1Int == 1005)
        return "OPERATOR_5"; 
      if (param1Int == 1006)
        return "OPERATOR_6"; 
      if (param1Int == 1007)
        return "OPERATOR_7"; 
      if (param1Int == 1008)
        return "OPERATOR_8"; 
      if (param1Int == 1009)
        return "OPERATOR_9"; 
      if (param1Int == 1010)
        return "OPERATOR_10"; 
      if (param1Int == 1011)
        return "OPERATOR_11"; 
      if (param1Int == 1012)
        return "OPERATOR_12"; 
      if (param1Int == 1013)
        return "OPERATOR_13"; 
      if (param1Int == 1014)
        return "OPERATOR_14"; 
      if (param1Int == 1015)
        return "OPERATOR_15"; 
      if (param1Int == 1016)
        return "OPERATOR_16"; 
      if (param1Int == 1017)
        return "OPERATOR_17"; 
      if (param1Int == 1018)
        return "OPERATOR_18"; 
      if (param1Int == 1019)
        return "OPERATOR_19"; 
      if (param1Int == 1020)
        return "OPERATOR_20"; 
      if (param1Int == 1021)
        return "OPERATOR_21"; 
      if (param1Int == 1022)
        return "OPERATOR_22"; 
      if (param1Int == 1023)
        return "OPERATOR_23"; 
      if (param1Int == 1024)
        return "OPERATOR_24"; 
      if (param1Int == 1025)
        return "OPERATOR_25"; 
      if (param1Int == 1026)
        return "OPERATOR_26"; 
      if (param1Int == 1027)
        return "OPERATOR_27"; 
      if (param1Int == 1028)
        return "OPERATOR_28"; 
      if (param1Int == 1029)
        return "OPERATOR_29"; 
      if (param1Int == 1030)
        return "OPERATOR_30"; 
      if (param1Int == 1031)
        return "OPERATOR_31"; 
      if (param1Int == 1032)
        return "OPERATOR_32"; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString(param1Int));
      return stringBuilder.toString();
    }
    
    public static final String dumpBitfield(int param1Int) {
      ArrayList<String> arrayList = new ArrayList();
      int i = 0;
      arrayList.add("CS_SERVICE");
      if ((param1Int & 0x1) == 1) {
        arrayList.add("PS_SERVICE");
        i = false | true;
      } 
      int j = i;
      if ((param1Int & 0x2) == 2) {
        arrayList.add("CS_VOICE");
        j = i | 0x2;
      } 
      i = j;
      if ((param1Int & 0x3) == 3) {
        arrayList.add("MO_SIGNALLING");
        i = j | 0x3;
      } 
      int k = i;
      if ((param1Int & 0x4) == 4) {
        arrayList.add("MO_DATA");
        k = i | 0x4;
      } 
      j = k;
      if ((param1Int & 0x5) == 5) {
        arrayList.add("CS_FALLBACK");
        j = k | 0x5;
      } 
      i = j;
      if ((param1Int & 0x6) == 6) {
        arrayList.add("MMTEL_VOICE");
        i = j | 0x6;
      } 
      j = i;
      if ((param1Int & 0x7) == 7) {
        arrayList.add("MMTEL_VIDEO");
        j = i | 0x7;
      } 
      k = j;
      if ((param1Int & 0x8) == 8) {
        arrayList.add("EMERGENCY");
        k = j | 0x8;
      } 
      i = k;
      if ((param1Int & 0x9) == 9) {
        arrayList.add("SMS");
        i = k | 0x9;
      } 
      j = i;
      if ((param1Int & 0x3E9) == 1001) {
        arrayList.add("OPERATOR_1");
        j = i | 0x3E9;
      } 
      i = j;
      if ((param1Int & 0x3EA) == 1002) {
        arrayList.add("OPERATOR_2");
        i = j | 0x3EA;
      } 
      j = i;
      if ((param1Int & 0x3EB) == 1003) {
        arrayList.add("OPERATOR_3");
        j = i | 0x3EB;
      } 
      i = j;
      if ((param1Int & 0x3EC) == 1004) {
        arrayList.add("OPERATOR_4");
        i = j | 0x3EC;
      } 
      j = i;
      if ((param1Int & 0x3ED) == 1005) {
        arrayList.add("OPERATOR_5");
        j = i | 0x3ED;
      } 
      k = j;
      if ((param1Int & 0x3EE) == 1006) {
        arrayList.add("OPERATOR_6");
        k = j | 0x3EE;
      } 
      i = k;
      if ((param1Int & 0x3EF) == 1007) {
        arrayList.add("OPERATOR_7");
        i = k | 0x3EF;
      } 
      j = i;
      if ((param1Int & 0x3F0) == 1008) {
        arrayList.add("OPERATOR_8");
        j = i | 0x3F0;
      } 
      i = j;
      if ((param1Int & 0x3F1) == 1009) {
        arrayList.add("OPERATOR_9");
        i = j | 0x3F1;
      } 
      j = i;
      if ((param1Int & 0x3F2) == 1010) {
        arrayList.add("OPERATOR_10");
        j = i | 0x3F2;
      } 
      i = j;
      if ((param1Int & 0x3F3) == 1011) {
        arrayList.add("OPERATOR_11");
        i = j | 0x3F3;
      } 
      j = i;
      if ((param1Int & 0x3F4) == 1012) {
        arrayList.add("OPERATOR_12");
        j = i | 0x3F4;
      } 
      i = j;
      if ((param1Int & 0x3F5) == 1013) {
        arrayList.add("OPERATOR_13");
        i = j | 0x3F5;
      } 
      j = i;
      if ((param1Int & 0x3F6) == 1014) {
        arrayList.add("OPERATOR_14");
        j = i | 0x3F6;
      } 
      i = j;
      if ((param1Int & 0x3F7) == 1015) {
        arrayList.add("OPERATOR_15");
        i = j | 0x3F7;
      } 
      j = i;
      if ((param1Int & 0x3F8) == 1016) {
        arrayList.add("OPERATOR_16");
        j = i | 0x3F8;
      } 
      i = j;
      if ((param1Int & 0x3F9) == 1017) {
        arrayList.add("OPERATOR_17");
        i = j | 0x3F9;
      } 
      j = i;
      if ((param1Int & 0x3FA) == 1018) {
        arrayList.add("OPERATOR_18");
        j = i | 0x3FA;
      } 
      k = j;
      if ((param1Int & 0x3FB) == 1019) {
        arrayList.add("OPERATOR_19");
        k = j | 0x3FB;
      } 
      i = k;
      if ((param1Int & 0x3FC) == 1020) {
        arrayList.add("OPERATOR_20");
        i = k | 0x3FC;
      } 
      j = i;
      if ((param1Int & 0x3FD) == 1021) {
        arrayList.add("OPERATOR_21");
        j = i | 0x3FD;
      } 
      i = j;
      if ((param1Int & 0x3FE) == 1022) {
        arrayList.add("OPERATOR_22");
        i = j | 0x3FE;
      } 
      j = i;
      if ((param1Int & 0x3FF) == 1023) {
        arrayList.add("OPERATOR_23");
        j = i | 0x3FF;
      } 
      k = j;
      if ((param1Int & 0x400) == 1024) {
        arrayList.add("OPERATOR_24");
        k = j | 0x400;
      } 
      i = k;
      if ((param1Int & 0x401) == 1025) {
        arrayList.add("OPERATOR_25");
        i = k | 0x401;
      } 
      j = i;
      if ((param1Int & 0x402) == 1026) {
        arrayList.add("OPERATOR_26");
        j = i | 0x402;
      } 
      i = j;
      if ((param1Int & 0x403) == 1027) {
        arrayList.add("OPERATOR_27");
        i = j | 0x403;
      } 
      j = i;
      if ((param1Int & 0x404) == 1028) {
        arrayList.add("OPERATOR_28");
        j = i | 0x404;
      } 
      i = j;
      if ((param1Int & 0x405) == 1029) {
        arrayList.add("OPERATOR_29");
        i = j | 0x405;
      } 
      j = i;
      if ((param1Int & 0x406) == 1030) {
        arrayList.add("OPERATOR_30");
        j = i | 0x406;
      } 
      i = j;
      if ((param1Int & 0x407) == 1031) {
        arrayList.add("OPERATOR_31");
        i = j | 0x407;
      } 
      j = i;
      if ((param1Int & 0x408) == 1032) {
        arrayList.add("OPERATOR_32");
        j = i | 0x408;
      } 
      if (param1Int != j) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("0x");
        stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & param1Int));
        arrayList.add(stringBuilder.toString());
      } 
      return String.join(" | ", (Iterable)arrayList);
    }
  }
  
  public static final class BarringType {
    public static final int CONDITIONAL = 1;
    
    public static final int NONE = 0;
    
    public static final int UNCONDITIONAL = 2;
    
    public static final String toString(int param1Int) {
      if (param1Int == 0)
        return "NONE"; 
      if (param1Int == 1)
        return "CONDITIONAL"; 
      if (param1Int == 2)
        return "UNCONDITIONAL"; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString(param1Int));
      return stringBuilder.toString();
    }
    
    public static final String dumpBitfield(int param1Int) {
      ArrayList<String> arrayList = new ArrayList();
      int i = 0;
      arrayList.add("NONE");
      if ((param1Int & 0x1) == 1) {
        arrayList.add("CONDITIONAL");
        i = false | true;
      } 
      int j = i;
      if ((param1Int & 0x2) == 2) {
        arrayList.add("UNCONDITIONAL");
        j = i | 0x2;
      } 
      if (param1Int != j) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("0x");
        stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & param1Int));
        arrayList.add(stringBuilder.toString());
      } 
      return String.join(" | ", (Iterable)arrayList);
    }
  }
  
  public static final class BarringTypeSpecificInfo {
    private byte hidl_d;
    
    private Object hidl_o;
    
    public static final class Conditional {
      public int factor;
      
      public boolean isBarred;
      
      public int timeSeconds;
      
      public Conditional() {
        this.factor = 0;
        this.timeSeconds = 0;
        this.isBarred = false;
      }
      
      public final boolean equals(Object param2Object) {
        if (this == param2Object)
          return true; 
        if (param2Object == null)
          return false; 
        if (param2Object.getClass() != Conditional.class)
          return false; 
        param2Object = param2Object;
        if (this.factor != ((Conditional)param2Object).factor)
          return false; 
        if (this.timeSeconds != ((Conditional)param2Object).timeSeconds)
          return false; 
        if (this.isBarred != ((Conditional)param2Object).isBarred)
          return false; 
        return true;
      }
      
      public final int hashCode() {
        int i = this.factor;
        i = HidlSupport.deepHashCode(Integer.valueOf(i));
        int j = this.timeSeconds;
        int k = HidlSupport.deepHashCode(Integer.valueOf(j));
        boolean bool = this.isBarred;
        j = HidlSupport.deepHashCode(Boolean.valueOf(bool));
        return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(k), Integer.valueOf(j) });
      }
      
      public final String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        stringBuilder.append(".factor = ");
        stringBuilder.append(this.factor);
        stringBuilder.append(", .timeSeconds = ");
        stringBuilder.append(this.timeSeconds);
        stringBuilder.append(", .isBarred = ");
        stringBuilder.append(this.isBarred);
        stringBuilder.append("}");
        return stringBuilder.toString();
      }
      
      public final void readFromParcel(HwParcel param2HwParcel) {
        HwBlob hwBlob = param2HwParcel.readBuffer(12L);
        readEmbeddedFromParcel(param2HwParcel, hwBlob, 0L);
      }
      
      public static final ArrayList<Conditional> readVectorFromParcel(HwParcel param2HwParcel) {
        ArrayList<Conditional> arrayList = new ArrayList();
        HwBlob hwBlob = param2HwParcel.readBuffer(16L);
        int i = hwBlob.getInt32(8L);
        long l1 = (i * 12);
        long l2 = hwBlob.handle();
        hwBlob = param2HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
        arrayList.clear();
        for (byte b = 0; b < i; b++) {
          Conditional conditional = new Conditional();
          conditional.readEmbeddedFromParcel(param2HwParcel, hwBlob, (b * 12));
          arrayList.add(conditional);
        } 
        return arrayList;
      }
      
      public final void readEmbeddedFromParcel(HwParcel param2HwParcel, HwBlob param2HwBlob, long param2Long) {
        this.factor = param2HwBlob.getInt32(0L + param2Long);
        this.timeSeconds = param2HwBlob.getInt32(4L + param2Long);
        this.isBarred = param2HwBlob.getBool(8L + param2Long);
      }
      
      public final void writeToParcel(HwParcel param2HwParcel) {
        HwBlob hwBlob = new HwBlob(12);
        writeEmbeddedToBlob(hwBlob, 0L);
        param2HwParcel.writeBuffer(hwBlob);
      }
      
      public static final void writeVectorToParcel(HwParcel param2HwParcel, ArrayList<Conditional> param2ArrayList) {
        HwBlob hwBlob1 = new HwBlob(16);
        int i = param2ArrayList.size();
        hwBlob1.putInt32(8L, i);
        hwBlob1.putBool(12L, false);
        HwBlob hwBlob2 = new HwBlob(i * 12);
        for (byte b = 0; b < i; b++)
          ((Conditional)param2ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 12)); 
        hwBlob1.putBlob(0L, hwBlob2);
        param2HwParcel.writeBuffer(hwBlob1);
      }
      
      public final void writeEmbeddedToBlob(HwBlob param2HwBlob, long param2Long) {
        param2HwBlob.putInt32(0L + param2Long, this.factor);
        param2HwBlob.putInt32(4L + param2Long, this.timeSeconds);
        param2HwBlob.putBool(8L + param2Long, this.isBarred);
      }
    }
    
    public BarringTypeSpecificInfo() {
      this.hidl_d = 0;
      this.hidl_o = null;
      this.hidl_o = new Monostate();
    }
    
    public static final class hidl_discriminator {
      public static final byte conditional = 1;
      
      public static final byte noinit = 0;
      
      public static final String getName(byte param2Byte) {
        if (param2Byte != 0) {
          if (param2Byte != 1)
            return "Unknown"; 
          return "conditional";
        } 
        return "noinit";
      }
    }
    
    public void noinit(Monostate param1Monostate) {
      this.hidl_d = 0;
      this.hidl_o = param1Monostate;
    }
    
    public Monostate noinit() {
      if (this.hidl_d != 0) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || Monostate.class.isInstance(object))
        return (Monostate)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public void conditional(Conditional param1Conditional) {
      this.hidl_d = 1;
      this.hidl_o = param1Conditional;
    }
    
    public Conditional conditional() {
      if (this.hidl_d != 1) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || Conditional.class.isInstance(object))
        return (Conditional)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public byte getDiscriminator() {
      return this.hidl_d;
    }
    
    public final boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (param1Object.getClass() != BarringTypeSpecificInfo.class)
        return false; 
      param1Object = param1Object;
      if (this.hidl_d != ((BarringTypeSpecificInfo)param1Object).hidl_d)
        return false; 
      if (!HidlSupport.deepEquals(this.hidl_o, ((BarringTypeSpecificInfo)param1Object).hidl_o))
        return false; 
      return true;
    }
    
    public final int hashCode() {
      Object object = this.hidl_o;
      int i = HidlSupport.deepHashCode(object);
      byte b = this.hidl_d;
      int j = Objects.hashCode(Byte.valueOf(b));
      return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
    }
    
    public final String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      byte b = this.hidl_d;
      if (b != 0) {
        if (b == 1) {
          stringBuilder.append(".conditional = ");
          stringBuilder.append(conditional());
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown union discriminator (value: ");
          stringBuilder.append(this.hidl_d);
          stringBuilder.append(").");
          throw new Error(stringBuilder.toString());
        } 
      } else {
        stringBuilder.append(".noinit = ");
        stringBuilder.append(noinit());
      } 
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public final void readFromParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = param1HwParcel.readBuffer(16L);
      readEmbeddedFromParcel(param1HwParcel, hwBlob, 0L);
    }
    
    public static final ArrayList<BarringTypeSpecificInfo> readVectorFromParcel(HwParcel param1HwParcel) {
      ArrayList<BarringTypeSpecificInfo> arrayList = new ArrayList();
      HwBlob hwBlob = param1HwParcel.readBuffer(16L);
      int i = hwBlob.getInt32(8L);
      long l1 = (i * 16);
      long l2 = hwBlob.handle();
      hwBlob = param1HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
      arrayList.clear();
      for (byte b = 0; b < i; b++) {
        BarringTypeSpecificInfo barringTypeSpecificInfo = new BarringTypeSpecificInfo();
        barringTypeSpecificInfo.readEmbeddedFromParcel(param1HwParcel, hwBlob, (b * 16));
        arrayList.add(barringTypeSpecificInfo);
      } 
      return arrayList;
    }
    
    public final void readEmbeddedFromParcel(HwParcel param1HwParcel, HwBlob param1HwBlob, long param1Long) {
      StringBuilder stringBuilder;
      byte b = param1HwBlob.getInt8(0L + param1Long);
      if (b != 0) {
        if (b == 1) {
          Conditional conditional = new Conditional();
          conditional.readEmbeddedFromParcel(param1HwParcel, param1HwBlob, 4L + param1Long);
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown union discriminator (value: ");
          stringBuilder.append(this.hidl_d);
          stringBuilder.append(").");
          throw new IllegalStateException(stringBuilder.toString());
        } 
      } else {
        Monostate monostate = new Monostate();
        monostate.readEmbeddedFromParcel((HwParcel)stringBuilder, param1HwBlob, 4L + param1Long);
      } 
    }
    
    public final void writeToParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = new HwBlob(16);
      writeEmbeddedToBlob(hwBlob, 0L);
      param1HwParcel.writeBuffer(hwBlob);
    }
    
    public static final void writeVectorToParcel(HwParcel param1HwParcel, ArrayList<BarringTypeSpecificInfo> param1ArrayList) {
      HwBlob hwBlob1 = new HwBlob(16);
      int i = param1ArrayList.size();
      hwBlob1.putInt32(8L, i);
      hwBlob1.putBool(12L, false);
      HwBlob hwBlob2 = new HwBlob(i * 16);
      for (byte b = 0; b < i; b++)
        ((BarringTypeSpecificInfo)param1ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 16)); 
      hwBlob1.putBlob(0L, hwBlob2);
      param1HwParcel.writeBuffer(hwBlob1);
    }
    
    public final void writeEmbeddedToBlob(HwBlob param1HwBlob, long param1Long) {
      StringBuilder stringBuilder;
      param1HwBlob.putInt8(0L + param1Long, this.hidl_d);
      byte b = this.hidl_d;
      if (b != 0) {
        if (b == 1) {
          conditional().writeEmbeddedToBlob(param1HwBlob, 4L + param1Long);
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown union discriminator (value: ");
          stringBuilder.append(this.hidl_d);
          stringBuilder.append(").");
          throw new Error(stringBuilder.toString());
        } 
      } else {
        noinit().writeEmbeddedToBlob((HwBlob)stringBuilder, 4L + param1Long);
      } 
    }
  }
  
  public static final class Conditional {
    public int factor = 0;
    
    public int timeSeconds = 0;
    
    public boolean isBarred = false;
    
    public final boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (param1Object.getClass() != Conditional.class)
        return false; 
      param1Object = param1Object;
      if (this.factor != ((Conditional)param1Object).factor)
        return false; 
      if (this.timeSeconds != ((Conditional)param1Object).timeSeconds)
        return false; 
      if (this.isBarred != ((Conditional)param1Object).isBarred)
        return false; 
      return true;
    }
    
    public final int hashCode() {
      int i = this.factor;
      i = HidlSupport.deepHashCode(Integer.valueOf(i));
      int j = this.timeSeconds;
      int k = HidlSupport.deepHashCode(Integer.valueOf(j));
      boolean bool = this.isBarred;
      j = HidlSupport.deepHashCode(Boolean.valueOf(bool));
      return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(k), Integer.valueOf(j) });
    }
    
    public final String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      stringBuilder.append(".factor = ");
      stringBuilder.append(this.factor);
      stringBuilder.append(", .timeSeconds = ");
      stringBuilder.append(this.timeSeconds);
      stringBuilder.append(", .isBarred = ");
      stringBuilder.append(this.isBarred);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public final void readFromParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = param1HwParcel.readBuffer(12L);
      readEmbeddedFromParcel(param1HwParcel, hwBlob, 0L);
    }
    
    public static final ArrayList<Conditional> readVectorFromParcel(HwParcel param1HwParcel) {
      ArrayList<Conditional> arrayList = new ArrayList();
      HwBlob hwBlob = param1HwParcel.readBuffer(16L);
      int i = hwBlob.getInt32(8L);
      long l1 = (i * 12);
      long l2 = hwBlob.handle();
      hwBlob = param1HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
      arrayList.clear();
      for (byte b = 0; b < i; b++) {
        Conditional conditional = new Conditional();
        conditional.readEmbeddedFromParcel(param1HwParcel, hwBlob, (b * 12));
        arrayList.add(conditional);
      } 
      return arrayList;
    }
    
    public final void readEmbeddedFromParcel(HwParcel param1HwParcel, HwBlob param1HwBlob, long param1Long) {
      this.factor = param1HwBlob.getInt32(0L + param1Long);
      this.timeSeconds = param1HwBlob.getInt32(4L + param1Long);
      this.isBarred = param1HwBlob.getBool(8L + param1Long);
    }
    
    public final void writeToParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = new HwBlob(12);
      writeEmbeddedToBlob(hwBlob, 0L);
      param1HwParcel.writeBuffer(hwBlob);
    }
    
    public static final void writeVectorToParcel(HwParcel param1HwParcel, ArrayList<Conditional> param1ArrayList) {
      HwBlob hwBlob1 = new HwBlob(16);
      int i = param1ArrayList.size();
      hwBlob1.putInt32(8L, i);
      hwBlob1.putBool(12L, false);
      HwBlob hwBlob2 = new HwBlob(i * 12);
      for (byte b = 0; b < i; b++)
        ((Conditional)param1ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 12)); 
      hwBlob1.putBlob(0L, hwBlob2);
      param1HwParcel.writeBuffer(hwBlob1);
    }
    
    public final void writeEmbeddedToBlob(HwBlob param1HwBlob, long param1Long) {
      param1HwBlob.putInt32(0L + param1Long, this.factor);
      param1HwBlob.putInt32(4L + param1Long, this.timeSeconds);
      param1HwBlob.putBool(8L + param1Long, this.isBarred);
    }
  }
  
  public static final class hidl_discriminator {
    public static final byte conditional = 1;
    
    public static final byte noinit = 0;
    
    public static final String getName(byte param1Byte) {
      if (param1Byte != 0) {
        if (param1Byte != 1)
          return "Unknown"; 
        return "conditional";
      } 
      return "noinit";
    }
  }
  
  public int serviceType = 0;
  
  public int barringType = 0;
  
  public BarringTypeSpecificInfo barringTypeSpecificInfo = new BarringTypeSpecificInfo();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != BarringInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.serviceType != ((BarringInfo)paramObject).serviceType)
      return false; 
    if (this.barringType != ((BarringInfo)paramObject).barringType)
      return false; 
    if (!HidlSupport.deepEquals(this.barringTypeSpecificInfo, ((BarringInfo)paramObject).barringTypeSpecificInfo))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.serviceType;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.barringType;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    BarringTypeSpecificInfo barringTypeSpecificInfo = this.barringTypeSpecificInfo;
    int k = HidlSupport.deepHashCode(barringTypeSpecificInfo);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".serviceType = ");
    stringBuilder.append(ServiceType.toString(this.serviceType));
    stringBuilder.append(", .barringType = ");
    stringBuilder.append(BarringType.toString(this.barringType));
    stringBuilder.append(", .barringTypeSpecificInfo = ");
    stringBuilder.append(this.barringTypeSpecificInfo);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(24L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<BarringInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<BarringInfo> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 24);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      BarringInfo barringInfo = new BarringInfo();
      barringInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 24));
      arrayList.add(barringInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.serviceType = paramHwBlob.getInt32(0L + paramLong);
    this.barringType = paramHwBlob.getInt32(4L + paramLong);
    this.barringTypeSpecificInfo.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 8L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(24);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<BarringInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 24);
    for (byte b = 0; b < i; b++)
      ((BarringInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 24)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.serviceType);
    paramHwBlob.putInt32(4L + paramLong, this.barringType);
    this.barringTypeSpecificInfo.writeEmbeddedToBlob(paramHwBlob, 8L + paramLong);
  }
}
