package android.hardware.radio.V1_4;

import java.util.ArrayList;

public final class EmergencyServiceCategory {
  public static final int AIEC = 64;
  
  public static final int AMBULANCE = 2;
  
  public static final int FIRE_BRIGADE = 4;
  
  public static final int MARINE_GUARD = 8;
  
  public static final int MIEC = 32;
  
  public static final int MOUNTAIN_RESCUE = 16;
  
  public static final int POLICE = 1;
  
  public static final int UNSPECIFIED = 0;
  
  public static final String toString(int paramInt) {
    if (paramInt == 0)
      return "UNSPECIFIED"; 
    if (paramInt == 1)
      return "POLICE"; 
    if (paramInt == 2)
      return "AMBULANCE"; 
    if (paramInt == 4)
      return "FIRE_BRIGADE"; 
    if (paramInt == 8)
      return "MARINE_GUARD"; 
    if (paramInt == 16)
      return "MOUNTAIN_RESCUE"; 
    if (paramInt == 32)
      return "MIEC"; 
    if (paramInt == 64)
      return "AIEC"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    arrayList.add("UNSPECIFIED");
    if ((paramInt & 0x1) == 1) {
      arrayList.add("POLICE");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("AMBULANCE");
      j = i | 0x2;
    } 
    int k = j;
    if ((paramInt & 0x4) == 4) {
      arrayList.add("FIRE_BRIGADE");
      k = j | 0x4;
    } 
    i = k;
    if ((paramInt & 0x8) == 8) {
      arrayList.add("MARINE_GUARD");
      i = k | 0x8;
    } 
    j = i;
    if ((paramInt & 0x10) == 16) {
      arrayList.add("MOUNTAIN_RESCUE");
      j = i | 0x10;
    } 
    i = j;
    if ((paramInt & 0x20) == 32) {
      arrayList.add("MIEC");
      i = j | 0x20;
    } 
    j = i;
    if ((paramInt & 0x40) == 64) {
      arrayList.add("AIEC");
      j = i | 0x40;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
