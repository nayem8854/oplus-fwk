package android.hardware.radio.V1_4;

import android.hardware.radio.V1_2.CellIdentityOperatorNames;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellIdentityNr {
  public String mcc = new String();
  
  public String mnc = new String();
  
  public long nci = 0L;
  
  public int pci = 0;
  
  public int tac = 0;
  
  public int nrarfcn = 0;
  
  public CellIdentityOperatorNames operatorNames = new CellIdentityOperatorNames();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellIdentityNr.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.mcc, ((CellIdentityNr)paramObject).mcc))
      return false; 
    if (!HidlSupport.deepEquals(this.mnc, ((CellIdentityNr)paramObject).mnc))
      return false; 
    if (this.nci != ((CellIdentityNr)paramObject).nci)
      return false; 
    if (this.pci != ((CellIdentityNr)paramObject).pci)
      return false; 
    if (this.tac != ((CellIdentityNr)paramObject).tac)
      return false; 
    if (this.nrarfcn != ((CellIdentityNr)paramObject).nrarfcn)
      return false; 
    if (!HidlSupport.deepEquals(this.operatorNames, ((CellIdentityNr)paramObject).operatorNames))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    String str = this.mcc;
    int i = HidlSupport.deepHashCode(str);
    str = this.mnc;
    int j = HidlSupport.deepHashCode(str);
    long l = this.nci;
    int k = HidlSupport.deepHashCode(Long.valueOf(l)), m = this.pci;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    int n = this.tac;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    int i1 = this.nrarfcn;
    i1 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    CellIdentityOperatorNames cellIdentityOperatorNames = this.operatorNames;
    int i2 = HidlSupport.deepHashCode(cellIdentityOperatorNames);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".mcc = ");
    stringBuilder.append(this.mcc);
    stringBuilder.append(", .mnc = ");
    stringBuilder.append(this.mnc);
    stringBuilder.append(", .nci = ");
    stringBuilder.append(this.nci);
    stringBuilder.append(", .pci = ");
    stringBuilder.append(this.pci);
    stringBuilder.append(", .tac = ");
    stringBuilder.append(this.tac);
    stringBuilder.append(", .nrarfcn = ");
    stringBuilder.append(this.nrarfcn);
    stringBuilder.append(", .operatorNames = ");
    stringBuilder.append(this.operatorNames);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(88L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellIdentityNr> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellIdentityNr> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 88);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellIdentityNr cellIdentityNr = new CellIdentityNr();
      cellIdentityNr.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 88));
      arrayList.add(cellIdentityNr);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    String str = paramHwBlob.getString(paramLong + 0L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, false);
    this.mnc = str = paramHwBlob.getString(paramLong + 16L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 16L + 0L, false);
    this.nci = paramHwBlob.getInt64(paramLong + 32L);
    this.pci = paramHwBlob.getInt32(paramLong + 40L);
    this.tac = paramHwBlob.getInt32(paramLong + 44L);
    this.nrarfcn = paramHwBlob.getInt32(paramLong + 48L);
    this.operatorNames.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 56L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(88);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellIdentityNr> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 88);
    for (byte b = 0; b < i; b++)
      ((CellIdentityNr)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 88)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putString(0L + paramLong, this.mcc);
    paramHwBlob.putString(16L + paramLong, this.mnc);
    paramHwBlob.putInt64(32L + paramLong, this.nci);
    paramHwBlob.putInt32(40L + paramLong, this.pci);
    paramHwBlob.putInt32(44L + paramLong, this.tac);
    paramHwBlob.putInt32(48L + paramLong, this.nrarfcn);
    this.operatorNames.writeEmbeddedToBlob(paramHwBlob, 56L + paramLong);
  }
}
