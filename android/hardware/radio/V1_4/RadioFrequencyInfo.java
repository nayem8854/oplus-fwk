package android.hardware.radio.V1_4;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class RadioFrequencyInfo {
  private byte hidl_d;
  
  private Object hidl_o;
  
  public RadioFrequencyInfo() {
    this.hidl_d = 0;
    this.hidl_o = null;
    this.hidl_o = Integer.valueOf(0);
  }
  
  public static final class hidl_discriminator {
    public static final byte channelNumber = 1;
    
    public static final byte range = 0;
    
    public static final String getName(byte param1Byte) {
      if (param1Byte != 0) {
        if (param1Byte != 1)
          return "Unknown"; 
        return "channelNumber";
      } 
      return "range";
    }
  }
  
  public void range(int paramInt) {
    this.hidl_d = 0;
    this.hidl_o = Integer.valueOf(paramInt);
  }
  
  public int range() {
    if (this.hidl_d != 0) {
      Object object1 = this.hidl_o;
      if (object1 != null) {
        object1 = object1.getClass().getName();
      } else {
        object1 = "null";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
      stringBuilder.append(this.hidl_d);
      stringBuilder.append(" (corresponding to ");
      byte b = this.hidl_d;
      stringBuilder.append(hidl_discriminator.getName(b));
      stringBuilder.append("), and hidl_o is of type ");
      stringBuilder.append((String)object1);
      stringBuilder.append(".");
      throw new IllegalStateException(stringBuilder.toString());
    } 
    Object object = this.hidl_o;
    if (object == null || Integer.class.isInstance(object))
      return ((Integer)this.hidl_o).intValue(); 
    throw new Error("Union is in a corrupted state.");
  }
  
  public void channelNumber(int paramInt) {
    this.hidl_d = 1;
    this.hidl_o = Integer.valueOf(paramInt);
  }
  
  public int channelNumber() {
    if (this.hidl_d != 1) {
      Object object1 = this.hidl_o;
      if (object1 != null) {
        object1 = object1.getClass().getName();
      } else {
        object1 = "null";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
      stringBuilder.append(this.hidl_d);
      stringBuilder.append(" (corresponding to ");
      byte b = this.hidl_d;
      stringBuilder.append(hidl_discriminator.getName(b));
      stringBuilder.append("), and hidl_o is of type ");
      stringBuilder.append((String)object1);
      stringBuilder.append(".");
      throw new IllegalStateException(stringBuilder.toString());
    } 
    Object object = this.hidl_o;
    if (object == null || Integer.class.isInstance(object))
      return ((Integer)this.hidl_o).intValue(); 
    throw new Error("Union is in a corrupted state.");
  }
  
  public byte getDiscriminator() {
    return this.hidl_d;
  }
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != RadioFrequencyInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.hidl_d != ((RadioFrequencyInfo)paramObject).hidl_d)
      return false; 
    if (!HidlSupport.deepEquals(this.hidl_o, ((RadioFrequencyInfo)paramObject).hidl_o))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    Object object = this.hidl_o;
    int i = HidlSupport.deepHashCode(object);
    byte b = this.hidl_d;
    int j = Objects.hashCode(Byte.valueOf(b));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    byte b = this.hidl_d;
    if (b != 0) {
      if (b == 1) {
        stringBuilder.append(".channelNumber = ");
        stringBuilder.append(channelNumber());
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown union discriminator (value: ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(").");
        throw new Error(stringBuilder.toString());
      } 
    } else {
      stringBuilder.append(".range = ");
      stringBuilder.append(FrequencyRange.toString(range()));
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(8L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<RadioFrequencyInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<RadioFrequencyInfo> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 8);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      RadioFrequencyInfo radioFrequencyInfo = new RadioFrequencyInfo();
      radioFrequencyInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 8));
      arrayList.add(radioFrequencyInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    byte b = paramHwBlob.getInt8(0L + paramLong);
    if (b != 0) {
      if (b == 1) {
        this.hidl_o = Integer.valueOf(0);
        this.hidl_o = Integer.valueOf(paramHwBlob.getInt32(4L + paramLong));
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown union discriminator (value: ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(").");
        throw new IllegalStateException(stringBuilder.toString());
      } 
    } else {
      this.hidl_o = Integer.valueOf(0);
      this.hidl_o = Integer.valueOf(paramHwBlob.getInt32(4L + paramLong));
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(8);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<RadioFrequencyInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 8);
    for (byte b = 0; b < i; b++)
      ((RadioFrequencyInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 8)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    StringBuilder stringBuilder;
    paramHwBlob.putInt8(0L + paramLong, this.hidl_d);
    byte b = this.hidl_d;
    if (b != 0) {
      if (b == 1) {
        paramHwBlob.putInt32(4L + paramLong, channelNumber());
      } else {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown union discriminator (value: ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(").");
        throw new Error(stringBuilder.toString());
      } 
    } else {
      stringBuilder.putInt32(4L + paramLong, range());
    } 
  }
}
