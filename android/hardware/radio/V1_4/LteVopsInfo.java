package android.hardware.radio.V1_4;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class LteVopsInfo {
  public boolean isVopsSupported = false;
  
  public boolean isEmcBearerSupported = false;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != LteVopsInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.isVopsSupported != ((LteVopsInfo)paramObject).isVopsSupported)
      return false; 
    if (this.isEmcBearerSupported != ((LteVopsInfo)paramObject).isEmcBearerSupported)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    boolean bool = this.isVopsSupported;
    int i = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    bool = this.isEmcBearerSupported;
    int j = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".isVopsSupported = ");
    stringBuilder.append(this.isVopsSupported);
    stringBuilder.append(", .isEmcBearerSupported = ");
    stringBuilder.append(this.isEmcBearerSupported);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(2L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<LteVopsInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<LteVopsInfo> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 2);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      LteVopsInfo lteVopsInfo = new LteVopsInfo();
      lteVopsInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 2));
      arrayList.add(lteVopsInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.isVopsSupported = paramHwBlob.getBool(0L + paramLong);
    this.isEmcBearerSupported = paramHwBlob.getBool(1L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(2);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<LteVopsInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 2);
    for (byte b = 0; b < i; b++)
      ((LteVopsInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 2)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putBool(0L + paramLong, this.isVopsSupported);
    paramHwBlob.putBool(1L + paramLong, this.isEmcBearerSupported);
  }
}
