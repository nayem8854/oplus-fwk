package android.hardware.radio.V1_4;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CardStatus {
  public android.hardware.radio.V1_2.CardStatus base = new android.hardware.radio.V1_2.CardStatus();
  
  public String eid = new String();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CardStatus.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.base, ((CardStatus)paramObject).base))
      return false; 
    if (!HidlSupport.deepEquals(this.eid, ((CardStatus)paramObject).eid))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    android.hardware.radio.V1_2.CardStatus cardStatus = this.base;
    int i = HidlSupport.deepHashCode(cardStatus);
    String str = this.eid;
    int j = HidlSupport.deepHashCode(str);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".base = ");
    stringBuilder.append(this.base);
    stringBuilder.append(", .eid = ");
    stringBuilder.append(this.eid);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(96L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CardStatus> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CardStatus> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 96);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CardStatus cardStatus = new CardStatus();
      cardStatus.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 96));
      arrayList.add(cardStatus);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.base.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 0L);
    String str = paramHwBlob.getString(paramLong + 80L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 80L + 0L, false);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(96);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CardStatus> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 96);
    for (byte b = 0; b < i; b++)
      ((CardStatus)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 96)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.base.writeEmbeddedToBlob(paramHwBlob, 0L + paramLong);
    paramHwBlob.putString(80L + paramLong, this.eid);
  }
}
