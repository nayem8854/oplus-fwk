package android.hardware.radio.V1_4;

import android.hardware.radio.V1_0.CdmaSignalStrength;
import android.hardware.radio.V1_0.EvdoSignalStrength;
import android.hardware.radio.V1_0.GsmSignalStrength;
import android.hardware.radio.V1_0.LteSignalStrength;
import android.hardware.radio.V1_2.TdscdmaSignalStrength;
import android.hardware.radio.V1_2.WcdmaSignalStrength;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class SignalStrength {
  public GsmSignalStrength gsm = new GsmSignalStrength();
  
  public CdmaSignalStrength cdma = new CdmaSignalStrength();
  
  public EvdoSignalStrength evdo = new EvdoSignalStrength();
  
  public LteSignalStrength lte = new LteSignalStrength();
  
  public TdscdmaSignalStrength tdscdma = new TdscdmaSignalStrength();
  
  public WcdmaSignalStrength wcdma = new WcdmaSignalStrength();
  
  public NrSignalStrength nr = new NrSignalStrength();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != SignalStrength.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.gsm, ((SignalStrength)paramObject).gsm))
      return false; 
    if (!HidlSupport.deepEquals(this.cdma, ((SignalStrength)paramObject).cdma))
      return false; 
    if (!HidlSupport.deepEquals(this.evdo, ((SignalStrength)paramObject).evdo))
      return false; 
    if (!HidlSupport.deepEquals(this.lte, ((SignalStrength)paramObject).lte))
      return false; 
    if (!HidlSupport.deepEquals(this.tdscdma, ((SignalStrength)paramObject).tdscdma))
      return false; 
    if (!HidlSupport.deepEquals(this.wcdma, ((SignalStrength)paramObject).wcdma))
      return false; 
    if (!HidlSupport.deepEquals(this.nr, ((SignalStrength)paramObject).nr))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    GsmSignalStrength gsmSignalStrength = this.gsm;
    int i = HidlSupport.deepHashCode(gsmSignalStrength);
    CdmaSignalStrength cdmaSignalStrength = this.cdma;
    int j = HidlSupport.deepHashCode(cdmaSignalStrength);
    EvdoSignalStrength evdoSignalStrength = this.evdo;
    int k = HidlSupport.deepHashCode(evdoSignalStrength);
    LteSignalStrength lteSignalStrength = this.lte;
    int m = HidlSupport.deepHashCode(lteSignalStrength);
    TdscdmaSignalStrength tdscdmaSignalStrength = this.tdscdma;
    int n = HidlSupport.deepHashCode(tdscdmaSignalStrength);
    WcdmaSignalStrength wcdmaSignalStrength = this.wcdma;
    int i1 = HidlSupport.deepHashCode(wcdmaSignalStrength);
    NrSignalStrength nrSignalStrength = this.nr;
    int i2 = HidlSupport.deepHashCode(nrSignalStrength);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".gsm = ");
    stringBuilder.append(this.gsm);
    stringBuilder.append(", .cdma = ");
    stringBuilder.append(this.cdma);
    stringBuilder.append(", .evdo = ");
    stringBuilder.append(this.evdo);
    stringBuilder.append(", .lte = ");
    stringBuilder.append(this.lte);
    stringBuilder.append(", .tdscdma = ");
    stringBuilder.append(this.tdscdma);
    stringBuilder.append(", .wcdma = ");
    stringBuilder.append(this.wcdma);
    stringBuilder.append(", .nr = ");
    stringBuilder.append(this.nr);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(108L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<SignalStrength> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<SignalStrength> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 108);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      SignalStrength signalStrength = new SignalStrength();
      signalStrength.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 108));
      arrayList.add(signalStrength);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.gsm.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 0L + paramLong);
    this.cdma.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 12L + paramLong);
    this.evdo.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 20L + paramLong);
    this.lte.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 32L + paramLong);
    this.tdscdma.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 56L + paramLong);
    this.wcdma.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 68L + paramLong);
    this.nr.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 84L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(108);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<SignalStrength> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 108);
    for (byte b = 0; b < i; b++)
      ((SignalStrength)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 108)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.gsm.writeEmbeddedToBlob(paramHwBlob, 0L + paramLong);
    this.cdma.writeEmbeddedToBlob(paramHwBlob, 12L + paramLong);
    this.evdo.writeEmbeddedToBlob(paramHwBlob, 20L + paramLong);
    this.lte.writeEmbeddedToBlob(paramHwBlob, 32L + paramLong);
    this.tdscdma.writeEmbeddedToBlob(paramHwBlob, 56L + paramLong);
    this.wcdma.writeEmbeddedToBlob(paramHwBlob, 68L + paramLong);
    this.nr.writeEmbeddedToBlob(paramHwBlob, 84L + paramLong);
  }
}
