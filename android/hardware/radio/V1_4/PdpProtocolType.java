package android.hardware.radio.V1_4;

import java.util.ArrayList;

public final class PdpProtocolType {
  public static final int IP = 0;
  
  public static final int IPV4V6 = 2;
  
  public static final int IPV6 = 1;
  
  public static final int NON_IP = 4;
  
  public static final int PPP = 3;
  
  public static final int UNKNOWN = -1;
  
  public static final int UNSTRUCTURED = 5;
  
  public static final String toString(int paramInt) {
    if (paramInt == -1)
      return "UNKNOWN"; 
    if (paramInt == 0)
      return "IP"; 
    if (paramInt == 1)
      return "IPV6"; 
    if (paramInt == 2)
      return "IPV4V6"; 
    if (paramInt == 3)
      return "PPP"; 
    if (paramInt == 4)
      return "NON_IP"; 
    if (paramInt == 5)
      return "UNSTRUCTURED"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    if ((paramInt & 0xFFFFFFFF) == -1) {
      arrayList.add("UNKNOWN");
      i = 0x0 | 0xFFFFFFFF;
    } 
    arrayList.add("IP");
    int j = i;
    if ((paramInt & 0x1) == 1) {
      arrayList.add("IPV6");
      j = i | 0x1;
    } 
    i = j;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("IPV4V6");
      i = j | 0x2;
    } 
    j = i;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("PPP");
      j = i | 0x3;
    } 
    i = j;
    if ((paramInt & 0x4) == 4) {
      arrayList.add("NON_IP");
      i = j | 0x4;
    } 
    j = i;
    if ((paramInt & 0x5) == 5) {
      arrayList.add("UNSTRUCTURED");
      j = i | 0x5;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
