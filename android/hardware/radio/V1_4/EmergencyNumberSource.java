package android.hardware.radio.V1_4;

import java.util.ArrayList;

public final class EmergencyNumberSource {
  public static final int DEFAULT = 8;
  
  public static final int MODEM_CONFIG = 4;
  
  public static final int NETWORK_SIGNALING = 1;
  
  public static final int SIM = 2;
  
  public static final String toString(int paramInt) {
    if (paramInt == 1)
      return "NETWORK_SIGNALING"; 
    if (paramInt == 2)
      return "SIM"; 
    if (paramInt == 4)
      return "MODEM_CONFIG"; 
    if (paramInt == 8)
      return "DEFAULT"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    if ((paramInt & 0x1) == 1) {
      arrayList.add("NETWORK_SIGNALING");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("SIM");
      j = i | 0x2;
    } 
    i = j;
    if ((paramInt & 0x4) == 4) {
      arrayList.add("MODEM_CONFIG");
      i = j | 0x4;
    } 
    j = i;
    if ((paramInt & 0x8) == 8) {
      arrayList.add("DEFAULT");
      j = i | 0x8;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
