package android.hardware.radio.V1_4;

import java.util.ArrayList;

public final class SimLockMultiSimPolicy {
  public static final int NO_MULTISIM_POLICY = 0;
  
  public static final int ONE_VALID_SIM_MUST_BE_PRESENT = 1;
  
  public static final String toString(int paramInt) {
    if (paramInt == 0)
      return "NO_MULTISIM_POLICY"; 
    if (paramInt == 1)
      return "ONE_VALID_SIM_MUST_BE_PRESENT"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    arrayList.add("NO_MULTISIM_POLICY");
    if ((paramInt & 0x1) == 1) {
      arrayList.add("ONE_VALID_SIM_MUST_BE_PRESENT");
      i = false | true;
    } 
    if (paramInt != i) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((i ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
