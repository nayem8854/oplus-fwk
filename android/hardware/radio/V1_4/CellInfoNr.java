package android.hardware.radio.V1_4;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellInfoNr {
  public NrSignalStrength signalStrength = new NrSignalStrength();
  
  public CellIdentityNr cellidentity = new CellIdentityNr();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellInfoNr.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.signalStrength, ((CellInfoNr)paramObject).signalStrength))
      return false; 
    if (!HidlSupport.deepEquals(this.cellidentity, ((CellInfoNr)paramObject).cellidentity))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    NrSignalStrength nrSignalStrength = this.signalStrength;
    int i = HidlSupport.deepHashCode(nrSignalStrength);
    CellIdentityNr cellIdentityNr = this.cellidentity;
    int j = HidlSupport.deepHashCode(cellIdentityNr);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".signalStrength = ");
    stringBuilder.append(this.signalStrength);
    stringBuilder.append(", .cellidentity = ");
    stringBuilder.append(this.cellidentity);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(112L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellInfoNr> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellInfoNr> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 112);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellInfoNr cellInfoNr = new CellInfoNr();
      cellInfoNr.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 112));
      arrayList.add(cellInfoNr);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.signalStrength.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 0L + paramLong);
    this.cellidentity.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 24L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(112);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellInfoNr> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 112);
    for (byte b = 0; b < i; b++)
      ((CellInfoNr)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 112)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.signalStrength.writeEmbeddedToBlob(paramHwBlob, 0L + paramLong);
    this.cellidentity.writeEmbeddedToBlob(paramHwBlob, 24L + paramLong);
  }
}
