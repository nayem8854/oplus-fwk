package android.hardware.radio.V1_4;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class EmergencyNumber {
  public String number = new String();
  
  public String mcc = new String();
  
  public String mnc = new String();
  
  public ArrayList<String> urns = new ArrayList<>();
  
  public int categories;
  
  public int sources;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != EmergencyNumber.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.number, ((EmergencyNumber)paramObject).number))
      return false; 
    if (!HidlSupport.deepEquals(this.mcc, ((EmergencyNumber)paramObject).mcc))
      return false; 
    if (!HidlSupport.deepEquals(this.mnc, ((EmergencyNumber)paramObject).mnc))
      return false; 
    if (!HidlSupport.deepEquals(Integer.valueOf(this.categories), Integer.valueOf(((EmergencyNumber)paramObject).categories)))
      return false; 
    if (!HidlSupport.deepEquals(this.urns, ((EmergencyNumber)paramObject).urns))
      return false; 
    if (!HidlSupport.deepEquals(Integer.valueOf(this.sources), Integer.valueOf(((EmergencyNumber)paramObject).sources)))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    String str = this.number;
    int i = HidlSupport.deepHashCode(str);
    str = this.mcc;
    int j = HidlSupport.deepHashCode(str);
    str = this.mnc;
    int k = HidlSupport.deepHashCode(str), m = this.categories;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    ArrayList<String> arrayList = this.urns;
    int n = HidlSupport.deepHashCode(arrayList), i1 = this.sources;
    i1 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".number = ");
    stringBuilder.append(this.number);
    stringBuilder.append(", .mcc = ");
    stringBuilder.append(this.mcc);
    stringBuilder.append(", .mnc = ");
    stringBuilder.append(this.mnc);
    stringBuilder.append(", .categories = ");
    stringBuilder.append(EmergencyServiceCategory.dumpBitfield(this.categories));
    stringBuilder.append(", .urns = ");
    stringBuilder.append(this.urns);
    stringBuilder.append(", .sources = ");
    stringBuilder.append(EmergencyNumberSource.dumpBitfield(this.sources));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(80L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<EmergencyNumber> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<EmergencyNumber> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 80);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      EmergencyNumber emergencyNumber = new EmergencyNumber();
      emergencyNumber.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 80));
      arrayList.add(emergencyNumber);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    String str = paramHwBlob.getString(paramLong + 0L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, false);
    this.mcc = str = paramHwBlob.getString(paramLong + 16L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 16L + 0L, false);
    this.mnc = str = paramHwBlob.getString(paramLong + 32L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 32L + 0L, false);
    this.categories = paramHwBlob.getInt32(paramLong + 48L);
    int i = paramHwBlob.getInt32(paramLong + 56L + 8L);
    l2 = (i * 16);
    l1 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 56L + 0L, true);
    this.urns.clear();
    for (byte b = 0; b < i; b++) {
      new String();
      String str1 = hwBlob.getString((b * 16));
      l2 = ((str1.getBytes()).length + 1);
      l1 = hwBlob.handle();
      long l = (b * 16 + 0);
      paramHwParcel.readEmbeddedBuffer(l2, l1, l, false);
      this.urns.add(str1);
    } 
    this.sources = paramHwBlob.getInt32(paramLong + 72L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(80);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<EmergencyNumber> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 80);
    for (byte b = 0; b < i; b++)
      ((EmergencyNumber)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 80)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putString(paramLong + 0L, this.number);
    paramHwBlob.putString(16L + paramLong, this.mcc);
    paramHwBlob.putString(32L + paramLong, this.mnc);
    paramHwBlob.putInt32(48L + paramLong, this.categories);
    int i = this.urns.size();
    paramHwBlob.putInt32(paramLong + 56L + 8L, i);
    paramHwBlob.putBool(paramLong + 56L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 16);
    for (byte b = 0; b < i; b++)
      hwBlob.putString((b * 16), this.urns.get(b)); 
    paramHwBlob.putBlob(56L + paramLong + 0L, hwBlob);
    paramHwBlob.putInt32(72L + paramLong, this.sources);
  }
}
