package android.hardware.radio.V1_4;

import android.hardware.radio.V1_2.CellConnectionStatus;
import android.hardware.radio.V1_2.CellInfoCdma;
import android.hardware.radio.V1_2.CellInfoGsm;
import android.hardware.radio.V1_2.CellInfoTdscdma;
import android.hardware.radio.V1_2.CellInfoWcdma;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellInfo {
  public static final class Info {
    private byte hidl_d;
    
    private Object hidl_o;
    
    public Info() {
      this.hidl_d = 0;
      this.hidl_o = null;
      this.hidl_o = new CellInfoGsm();
    }
    
    public static final class hidl_discriminator {
      public static final byte cdma = 1;
      
      public static final byte gsm = 0;
      
      public static final byte lte = 4;
      
      public static final byte nr = 5;
      
      public static final byte tdscdma = 3;
      
      public static final byte wcdma = 2;
      
      public static final String getName(byte param2Byte) {
        if (param2Byte != 0) {
          if (param2Byte != 1) {
            if (param2Byte != 2) {
              if (param2Byte != 3) {
                if (param2Byte != 4) {
                  if (param2Byte != 5)
                    return "Unknown"; 
                  return "nr";
                } 
                return "lte";
              } 
              return "tdscdma";
            } 
            return "wcdma";
          } 
          return "cdma";
        } 
        return "gsm";
      }
    }
    
    public void gsm(CellInfoGsm param1CellInfoGsm) {
      this.hidl_d = 0;
      this.hidl_o = param1CellInfoGsm;
    }
    
    public CellInfoGsm gsm() {
      if (this.hidl_d != 0) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || CellInfoGsm.class.isInstance(object))
        return (CellInfoGsm)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public void cdma(CellInfoCdma param1CellInfoCdma) {
      this.hidl_d = 1;
      this.hidl_o = param1CellInfoCdma;
    }
    
    public CellInfoCdma cdma() {
      if (this.hidl_d != 1) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || CellInfoCdma.class.isInstance(object))
        return (CellInfoCdma)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public void wcdma(CellInfoWcdma param1CellInfoWcdma) {
      this.hidl_d = 2;
      this.hidl_o = param1CellInfoWcdma;
    }
    
    public CellInfoWcdma wcdma() {
      if (this.hidl_d != 2) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || CellInfoWcdma.class.isInstance(object))
        return (CellInfoWcdma)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public void tdscdma(CellInfoTdscdma param1CellInfoTdscdma) {
      this.hidl_d = 3;
      this.hidl_o = param1CellInfoTdscdma;
    }
    
    public CellInfoTdscdma tdscdma() {
      if (this.hidl_d != 3) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || CellInfoTdscdma.class.isInstance(object))
        return (CellInfoTdscdma)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public void lte(CellInfoLte param1CellInfoLte) {
      this.hidl_d = 4;
      this.hidl_o = param1CellInfoLte;
    }
    
    public CellInfoLte lte() {
      if (this.hidl_d != 4) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || CellInfoLte.class.isInstance(object))
        return (CellInfoLte)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public void nr(CellInfoNr param1CellInfoNr) {
      this.hidl_d = 5;
      this.hidl_o = param1CellInfoNr;
    }
    
    public CellInfoNr nr() {
      if (this.hidl_d != 5) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || CellInfoNr.class.isInstance(object))
        return (CellInfoNr)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public byte getDiscriminator() {
      return this.hidl_d;
    }
    
    public final boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (param1Object.getClass() != Info.class)
        return false; 
      param1Object = param1Object;
      if (this.hidl_d != ((Info)param1Object).hidl_d)
        return false; 
      if (!HidlSupport.deepEquals(this.hidl_o, ((Info)param1Object).hidl_o))
        return false; 
      return true;
    }
    
    public final int hashCode() {
      Object object = this.hidl_o;
      int i = HidlSupport.deepHashCode(object);
      byte b = this.hidl_d;
      int j = Objects.hashCode(Byte.valueOf(b));
      return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
    }
    
    public final String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      byte b = this.hidl_d;
      if (b != 0) {
        if (b != 1) {
          if (b != 2) {
            if (b != 3) {
              if (b != 4) {
                if (b == 5) {
                  stringBuilder.append(".nr = ");
                  stringBuilder.append(nr());
                } else {
                  stringBuilder = new StringBuilder();
                  stringBuilder.append("Unknown union discriminator (value: ");
                  stringBuilder.append(this.hidl_d);
                  stringBuilder.append(").");
                  throw new Error(stringBuilder.toString());
                } 
              } else {
                stringBuilder.append(".lte = ");
                stringBuilder.append(lte());
              } 
            } else {
              stringBuilder.append(".tdscdma = ");
              stringBuilder.append(tdscdma());
            } 
          } else {
            stringBuilder.append(".wcdma = ");
            stringBuilder.append(wcdma());
          } 
        } else {
          stringBuilder.append(".cdma = ");
          stringBuilder.append(cdma());
        } 
      } else {
        stringBuilder.append(".gsm = ");
        stringBuilder.append(gsm());
      } 
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public final void readFromParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = param1HwParcel.readBuffer(128L);
      readEmbeddedFromParcel(param1HwParcel, hwBlob, 0L);
    }
    
    public static final ArrayList<Info> readVectorFromParcel(HwParcel param1HwParcel) {
      ArrayList<Info> arrayList = new ArrayList();
      HwBlob hwBlob1 = param1HwParcel.readBuffer(16L);
      int i = hwBlob1.getInt32(8L);
      long l1 = (i * 128);
      long l2 = hwBlob1.handle();
      HwBlob hwBlob2 = param1HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
      arrayList.clear();
      for (byte b = 0; b < i; b++) {
        Info info = new Info();
        info.readEmbeddedFromParcel(param1HwParcel, hwBlob2, (b * 128));
        arrayList.add(info);
      } 
      return arrayList;
    }
    
    public final void readEmbeddedFromParcel(HwParcel param1HwParcel, HwBlob param1HwBlob, long param1Long) {
      StringBuilder stringBuilder;
      byte b = param1HwBlob.getInt8(0L + param1Long);
      if (b != 0) {
        if (b != 1) {
          if (b != 2) {
            if (b != 3) {
              if (b != 4) {
                if (b == 5) {
                  CellInfoNr cellInfoNr = new CellInfoNr();
                  cellInfoNr.readEmbeddedFromParcel(param1HwParcel, param1HwBlob, 8L + param1Long);
                } else {
                  stringBuilder = new StringBuilder();
                  stringBuilder.append("Unknown union discriminator (value: ");
                  stringBuilder.append(this.hidl_d);
                  stringBuilder.append(").");
                  throw new IllegalStateException(stringBuilder.toString());
                } 
              } else {
                CellInfoLte cellInfoLte = new CellInfoLte();
                cellInfoLte.readEmbeddedFromParcel((HwParcel)stringBuilder, param1HwBlob, 8L + param1Long);
              } 
            } else {
              CellInfoTdscdma cellInfoTdscdma = new CellInfoTdscdma();
              cellInfoTdscdma.readEmbeddedFromParcel((HwParcel)stringBuilder, param1HwBlob, 8L + param1Long);
            } 
          } else {
            CellInfoWcdma cellInfoWcdma = new CellInfoWcdma();
            cellInfoWcdma.readEmbeddedFromParcel((HwParcel)stringBuilder, param1HwBlob, 8L + param1Long);
          } 
        } else {
          CellInfoCdma cellInfoCdma = new CellInfoCdma();
          cellInfoCdma.readEmbeddedFromParcel((HwParcel)stringBuilder, param1HwBlob, 8L + param1Long);
        } 
      } else {
        CellInfoGsm cellInfoGsm = new CellInfoGsm();
        cellInfoGsm.readEmbeddedFromParcel((HwParcel)stringBuilder, param1HwBlob, 8L + param1Long);
      } 
    }
    
    public final void writeToParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = new HwBlob(128);
      writeEmbeddedToBlob(hwBlob, 0L);
      param1HwParcel.writeBuffer(hwBlob);
    }
    
    public static final void writeVectorToParcel(HwParcel param1HwParcel, ArrayList<Info> param1ArrayList) {
      HwBlob hwBlob1 = new HwBlob(16);
      int i = param1ArrayList.size();
      hwBlob1.putInt32(8L, i);
      hwBlob1.putBool(12L, false);
      HwBlob hwBlob2 = new HwBlob(i * 128);
      for (byte b = 0; b < i; b++)
        ((Info)param1ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 128)); 
      hwBlob1.putBlob(0L, hwBlob2);
      param1HwParcel.writeBuffer(hwBlob1);
    }
    
    public final void writeEmbeddedToBlob(HwBlob param1HwBlob, long param1Long) {
      StringBuilder stringBuilder;
      param1HwBlob.putInt8(0L + param1Long, this.hidl_d);
      byte b = this.hidl_d;
      if (b != 0) {
        if (b != 1) {
          if (b != 2) {
            if (b != 3) {
              if (b != 4) {
                if (b == 5) {
                  nr().writeEmbeddedToBlob(param1HwBlob, 8L + param1Long);
                } else {
                  stringBuilder = new StringBuilder();
                  stringBuilder.append("Unknown union discriminator (value: ");
                  stringBuilder.append(this.hidl_d);
                  stringBuilder.append(").");
                  throw new Error(stringBuilder.toString());
                } 
              } else {
                lte().writeEmbeddedToBlob((HwBlob)stringBuilder, 8L + param1Long);
              } 
            } else {
              tdscdma().writeEmbeddedToBlob((HwBlob)stringBuilder, 8L + param1Long);
            } 
          } else {
            wcdma().writeEmbeddedToBlob((HwBlob)stringBuilder, 8L + param1Long);
          } 
        } else {
          cdma().writeEmbeddedToBlob((HwBlob)stringBuilder, 8L + param1Long);
        } 
      } else {
        gsm().writeEmbeddedToBlob((HwBlob)stringBuilder, 8L + param1Long);
      } 
    }
  }
  
  public static final class hidl_discriminator {
    public static final byte cdma = 1;
    
    public static final byte gsm = 0;
    
    public static final byte lte = 4;
    
    public static final byte nr = 5;
    
    public static final byte tdscdma = 3;
    
    public static final byte wcdma = 2;
    
    public static final String getName(byte param1Byte) {
      if (param1Byte != 0) {
        if (param1Byte != 1) {
          if (param1Byte != 2) {
            if (param1Byte != 3) {
              if (param1Byte != 4) {
                if (param1Byte != 5)
                  return "Unknown"; 
                return "nr";
              } 
              return "lte";
            } 
            return "tdscdma";
          } 
          return "wcdma";
        } 
        return "cdma";
      } 
      return "gsm";
    }
  }
  
  public boolean isRegistered = false;
  
  public int connectionStatus = 0;
  
  public Info info = new Info();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.isRegistered != ((CellInfo)paramObject).isRegistered)
      return false; 
    if (this.connectionStatus != ((CellInfo)paramObject).connectionStatus)
      return false; 
    if (!HidlSupport.deepEquals(this.info, ((CellInfo)paramObject).info))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    boolean bool = this.isRegistered;
    int i = HidlSupport.deepHashCode(Boolean.valueOf(bool)), j = this.connectionStatus;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    Info info = this.info;
    int k = HidlSupport.deepHashCode(info);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".isRegistered = ");
    stringBuilder.append(this.isRegistered);
    stringBuilder.append(", .connectionStatus = ");
    stringBuilder.append(CellConnectionStatus.toString(this.connectionStatus));
    stringBuilder.append(", .info = ");
    stringBuilder.append(this.info);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(136L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellInfo> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 136);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellInfo cellInfo = new CellInfo();
      cellInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 136));
      arrayList.add(cellInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.isRegistered = paramHwBlob.getBool(0L + paramLong);
    this.connectionStatus = paramHwBlob.getInt32(4L + paramLong);
    this.info.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 8L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(136);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 136);
    for (byte b = 0; b < i; b++)
      ((CellInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 136)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putBool(0L + paramLong, this.isRegistered);
    paramHwBlob.putInt32(4L + paramLong, this.connectionStatus);
    this.info.writeEmbeddedToBlob(paramHwBlob, 8L + paramLong);
  }
}
