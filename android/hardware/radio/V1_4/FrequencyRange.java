package android.hardware.radio.V1_4;

import java.util.ArrayList;

public final class FrequencyRange {
  public static final int HIGH = 3;
  
  public static final int LOW = 1;
  
  public static final int MID = 2;
  
  public static final int MMWAVE = 4;
  
  public static final String toString(int paramInt) {
    if (paramInt == 1)
      return "LOW"; 
    if (paramInt == 2)
      return "MID"; 
    if (paramInt == 3)
      return "HIGH"; 
    if (paramInt == 4)
      return "MMWAVE"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    if ((paramInt & 0x1) == 1) {
      arrayList.add("LOW");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("MID");
      j = i | 0x2;
    } 
    i = j;
    if ((paramInt & 0x3) == 3) {
      arrayList.add("HIGH");
      i = j | 0x3;
    } 
    j = i;
    if ((paramInt & 0x4) == 4) {
      arrayList.add("MMWAVE");
      j = i | 0x4;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
