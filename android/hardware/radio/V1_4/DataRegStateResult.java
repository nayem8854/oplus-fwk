package android.hardware.radio.V1_4;

import android.internal.hidl.safe_union.V1_0.Monostate;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class DataRegStateResult {
  public static final class VopsInfo {
    private byte hidl_d;
    
    private Object hidl_o;
    
    public VopsInfo() {
      this.hidl_d = 0;
      this.hidl_o = null;
      this.hidl_o = new Monostate();
    }
    
    public static final class hidl_discriminator {
      public static final byte lteVopsInfo = 1;
      
      public static final byte noinit = 0;
      
      public static final String getName(byte param2Byte) {
        if (param2Byte != 0) {
          if (param2Byte != 1)
            return "Unknown"; 
          return "lteVopsInfo";
        } 
        return "noinit";
      }
    }
    
    public void noinit(Monostate param1Monostate) {
      this.hidl_d = 0;
      this.hidl_o = param1Monostate;
    }
    
    public Monostate noinit() {
      if (this.hidl_d != 0) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || Monostate.class.isInstance(object))
        return (Monostate)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public void lteVopsInfo(LteVopsInfo param1LteVopsInfo) {
      this.hidl_d = 1;
      this.hidl_o = param1LteVopsInfo;
    }
    
    public LteVopsInfo lteVopsInfo() {
      if (this.hidl_d != 1) {
        Object object1 = this.hidl_o;
        if (object1 != null) {
          object1 = object1.getClass().getName();
        } else {
          object1 = "null";
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read access to inactive union components is disallowed. Discriminator value is ");
        stringBuilder.append(this.hidl_d);
        stringBuilder.append(" (corresponding to ");
        byte b = this.hidl_d;
        stringBuilder.append(hidl_discriminator.getName(b));
        stringBuilder.append("), and hidl_o is of type ");
        stringBuilder.append((String)object1);
        stringBuilder.append(".");
        throw new IllegalStateException(stringBuilder.toString());
      } 
      Object object = this.hidl_o;
      if (object == null || LteVopsInfo.class.isInstance(object))
        return (LteVopsInfo)this.hidl_o; 
      throw new Error("Union is in a corrupted state.");
    }
    
    public byte getDiscriminator() {
      return this.hidl_d;
    }
    
    public final boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (param1Object.getClass() != VopsInfo.class)
        return false; 
      param1Object = param1Object;
      if (this.hidl_d != ((VopsInfo)param1Object).hidl_d)
        return false; 
      if (!HidlSupport.deepEquals(this.hidl_o, ((VopsInfo)param1Object).hidl_o))
        return false; 
      return true;
    }
    
    public final int hashCode() {
      Object object = this.hidl_o;
      int i = HidlSupport.deepHashCode(object);
      byte b = this.hidl_d;
      int j = Objects.hashCode(Byte.valueOf(b));
      return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
    }
    
    public final String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{");
      byte b = this.hidl_d;
      if (b != 0) {
        if (b == 1) {
          stringBuilder.append(".lteVopsInfo = ");
          stringBuilder.append(lteVopsInfo());
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown union discriminator (value: ");
          stringBuilder.append(this.hidl_d);
          stringBuilder.append(").");
          throw new Error(stringBuilder.toString());
        } 
      } else {
        stringBuilder.append(".noinit = ");
        stringBuilder.append(noinit());
      } 
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public final void readFromParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = param1HwParcel.readBuffer(3L);
      readEmbeddedFromParcel(param1HwParcel, hwBlob, 0L);
    }
    
    public static final ArrayList<VopsInfo> readVectorFromParcel(HwParcel param1HwParcel) {
      ArrayList<VopsInfo> arrayList = new ArrayList();
      HwBlob hwBlob1 = param1HwParcel.readBuffer(16L);
      int i = hwBlob1.getInt32(8L);
      long l1 = (i * 3);
      long l2 = hwBlob1.handle();
      HwBlob hwBlob2 = param1HwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
      arrayList.clear();
      for (byte b = 0; b < i; b++) {
        VopsInfo vopsInfo = new VopsInfo();
        vopsInfo.readEmbeddedFromParcel(param1HwParcel, hwBlob2, (b * 3));
        arrayList.add(vopsInfo);
      } 
      return arrayList;
    }
    
    public final void readEmbeddedFromParcel(HwParcel param1HwParcel, HwBlob param1HwBlob, long param1Long) {
      StringBuilder stringBuilder;
      byte b = param1HwBlob.getInt8(0L + param1Long);
      if (b != 0) {
        if (b == 1) {
          LteVopsInfo lteVopsInfo = new LteVopsInfo();
          lteVopsInfo.readEmbeddedFromParcel(param1HwParcel, param1HwBlob, 1L + param1Long);
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown union discriminator (value: ");
          stringBuilder.append(this.hidl_d);
          stringBuilder.append(").");
          throw new IllegalStateException(stringBuilder.toString());
        } 
      } else {
        Monostate monostate = new Monostate();
        monostate.readEmbeddedFromParcel((HwParcel)stringBuilder, param1HwBlob, 1L + param1Long);
      } 
    }
    
    public final void writeToParcel(HwParcel param1HwParcel) {
      HwBlob hwBlob = new HwBlob(3);
      writeEmbeddedToBlob(hwBlob, 0L);
      param1HwParcel.writeBuffer(hwBlob);
    }
    
    public static final void writeVectorToParcel(HwParcel param1HwParcel, ArrayList<VopsInfo> param1ArrayList) {
      HwBlob hwBlob1 = new HwBlob(16);
      int i = param1ArrayList.size();
      hwBlob1.putInt32(8L, i);
      hwBlob1.putBool(12L, false);
      HwBlob hwBlob2 = new HwBlob(i * 3);
      for (byte b = 0; b < i; b++)
        ((VopsInfo)param1ArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 3)); 
      hwBlob1.putBlob(0L, hwBlob2);
      param1HwParcel.writeBuffer(hwBlob1);
    }
    
    public final void writeEmbeddedToBlob(HwBlob param1HwBlob, long param1Long) {
      StringBuilder stringBuilder;
      param1HwBlob.putInt8(0L + param1Long, this.hidl_d);
      byte b = this.hidl_d;
      if (b != 0) {
        if (b == 1) {
          lteVopsInfo().writeEmbeddedToBlob(param1HwBlob, 1L + param1Long);
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown union discriminator (value: ");
          stringBuilder.append(this.hidl_d);
          stringBuilder.append(").");
          throw new Error(stringBuilder.toString());
        } 
      } else {
        noinit().writeEmbeddedToBlob((HwBlob)stringBuilder, 1L + param1Long);
      } 
    }
  }
  
  public static final class hidl_discriminator {
    public static final byte lteVopsInfo = 1;
    
    public static final byte noinit = 0;
    
    public static final String getName(byte param1Byte) {
      if (param1Byte != 0) {
        if (param1Byte != 1)
          return "Unknown"; 
        return "lteVopsInfo";
      } 
      return "noinit";
    }
  }
  
  public android.hardware.radio.V1_2.DataRegStateResult base = new android.hardware.radio.V1_2.DataRegStateResult();
  
  public VopsInfo vopsInfo = new VopsInfo();
  
  public NrIndicators nrIndicators = new NrIndicators();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != DataRegStateResult.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.base, ((DataRegStateResult)paramObject).base))
      return false; 
    if (!HidlSupport.deepEquals(this.vopsInfo, ((DataRegStateResult)paramObject).vopsInfo))
      return false; 
    if (!HidlSupport.deepEquals(this.nrIndicators, ((DataRegStateResult)paramObject).nrIndicators))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    android.hardware.radio.V1_2.DataRegStateResult dataRegStateResult = this.base;
    int i = HidlSupport.deepHashCode(dataRegStateResult);
    VopsInfo vopsInfo = this.vopsInfo;
    int j = HidlSupport.deepHashCode(vopsInfo);
    NrIndicators nrIndicators = this.nrIndicators;
    int k = HidlSupport.deepHashCode(nrIndicators);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".base = ");
    stringBuilder.append(this.base);
    stringBuilder.append(", .vopsInfo = ");
    stringBuilder.append(this.vopsInfo);
    stringBuilder.append(", .nrIndicators = ");
    stringBuilder.append(this.nrIndicators);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(112L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<DataRegStateResult> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<DataRegStateResult> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 112);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      DataRegStateResult dataRegStateResult = new DataRegStateResult();
      dataRegStateResult.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 112));
      arrayList.add(dataRegStateResult);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.base.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 0L + paramLong);
    this.vopsInfo.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 104L + paramLong);
    this.nrIndicators.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 107L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(112);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<DataRegStateResult> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 112);
    for (byte b = 0; b < i; b++)
      ((DataRegStateResult)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 112)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.base.writeEmbeddedToBlob(paramHwBlob, 0L + paramLong);
    this.vopsInfo.writeEmbeddedToBlob(paramHwBlob, 104L + paramLong);
    this.nrIndicators.writeEmbeddedToBlob(paramHwBlob, 107L + paramLong);
  }
}
