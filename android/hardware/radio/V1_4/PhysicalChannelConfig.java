package android.hardware.radio.V1_4;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class PhysicalChannelConfig {
  public android.hardware.radio.V1_2.PhysicalChannelConfig base = new android.hardware.radio.V1_2.PhysicalChannelConfig();
  
  public int rat = 0;
  
  public RadioFrequencyInfo rfInfo = new RadioFrequencyInfo();
  
  public ArrayList<Integer> contextIds = new ArrayList<>();
  
  public int physicalCellId = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != PhysicalChannelConfig.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.base, ((PhysicalChannelConfig)paramObject).base))
      return false; 
    if (this.rat != ((PhysicalChannelConfig)paramObject).rat)
      return false; 
    if (!HidlSupport.deepEquals(this.rfInfo, ((PhysicalChannelConfig)paramObject).rfInfo))
      return false; 
    if (!HidlSupport.deepEquals(this.contextIds, ((PhysicalChannelConfig)paramObject).contextIds))
      return false; 
    if (this.physicalCellId != ((PhysicalChannelConfig)paramObject).physicalCellId)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    android.hardware.radio.V1_2.PhysicalChannelConfig physicalChannelConfig = this.base;
    int i = HidlSupport.deepHashCode(physicalChannelConfig), j = this.rat;
    int k = HidlSupport.deepHashCode(Integer.valueOf(j));
    RadioFrequencyInfo radioFrequencyInfo = this.rfInfo;
    int m = HidlSupport.deepHashCode(radioFrequencyInfo);
    ArrayList<Integer> arrayList = this.contextIds;
    j = HidlSupport.deepHashCode(arrayList);
    int n = this.physicalCellId;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(j), Integer.valueOf(n) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".base = ");
    stringBuilder.append(this.base);
    stringBuilder.append(", .rat = ");
    stringBuilder.append(RadioTechnology.toString(this.rat));
    stringBuilder.append(", .rfInfo = ");
    stringBuilder.append(this.rfInfo);
    stringBuilder.append(", .contextIds = ");
    stringBuilder.append(this.contextIds);
    stringBuilder.append(", .physicalCellId = ");
    stringBuilder.append(this.physicalCellId);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(48L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<PhysicalChannelConfig> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<PhysicalChannelConfig> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 48);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      PhysicalChannelConfig physicalChannelConfig = new PhysicalChannelConfig();
      physicalChannelConfig.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 48));
      arrayList.add(physicalChannelConfig);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.base.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 0L);
    this.rat = paramHwBlob.getInt32(paramLong + 8L);
    this.rfInfo.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 12L);
    int i = paramHwBlob.getInt32(paramLong + 24L + 8L);
    long l1 = (i * 4);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 24L + 0L, true);
    this.contextIds.clear();
    for (byte b = 0; b < i; b++) {
      int j = hwBlob.getInt32((b * 4));
      this.contextIds.add(Integer.valueOf(j));
    } 
    this.physicalCellId = paramHwBlob.getInt32(paramLong + 40L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(48);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<PhysicalChannelConfig> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 48);
    for (byte b = 0; b < i; b++)
      ((PhysicalChannelConfig)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 48)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.base.writeEmbeddedToBlob(paramHwBlob, paramLong + 0L);
    paramHwBlob.putInt32(paramLong + 8L, this.rat);
    this.rfInfo.writeEmbeddedToBlob(paramHwBlob, paramLong + 12L);
    int i = this.contextIds.size();
    paramHwBlob.putInt32(paramLong + 24L + 8L, i);
    paramHwBlob.putBool(paramLong + 24L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 4);
    for (byte b = 0; b < i; b++)
      hwBlob.putInt32((b * 4), ((Integer)this.contextIds.get(b)).intValue()); 
    paramHwBlob.putBlob(24L + paramLong + 0L, hwBlob);
    paramHwBlob.putInt32(40L + paramLong, this.physicalCellId);
  }
}
