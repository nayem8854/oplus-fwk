package android.hardware.radio.V1_4;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class NrSignalStrength {
  public int ssRsrp = 0;
  
  public int ssRsrq = 0;
  
  public int ssSinr = 0;
  
  public int csiRsrp = 0;
  
  public int csiRsrq = 0;
  
  public int csiSinr = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != NrSignalStrength.class)
      return false; 
    paramObject = paramObject;
    if (this.ssRsrp != ((NrSignalStrength)paramObject).ssRsrp)
      return false; 
    if (this.ssRsrq != ((NrSignalStrength)paramObject).ssRsrq)
      return false; 
    if (this.ssSinr != ((NrSignalStrength)paramObject).ssSinr)
      return false; 
    if (this.csiRsrp != ((NrSignalStrength)paramObject).csiRsrp)
      return false; 
    if (this.csiRsrq != ((NrSignalStrength)paramObject).csiRsrq)
      return false; 
    if (this.csiSinr != ((NrSignalStrength)paramObject).csiSinr)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.ssRsrp;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.ssRsrq;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.ssSinr;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.csiRsrp;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    int n = this.csiRsrq;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    int i1 = this.csiSinr;
    i1 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".ssRsrp = ");
    stringBuilder.append(this.ssRsrp);
    stringBuilder.append(", .ssRsrq = ");
    stringBuilder.append(this.ssRsrq);
    stringBuilder.append(", .ssSinr = ");
    stringBuilder.append(this.ssSinr);
    stringBuilder.append(", .csiRsrp = ");
    stringBuilder.append(this.csiRsrp);
    stringBuilder.append(", .csiRsrq = ");
    stringBuilder.append(this.csiRsrq);
    stringBuilder.append(", .csiSinr = ");
    stringBuilder.append(this.csiSinr);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(24L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<NrSignalStrength> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<NrSignalStrength> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 24);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      NrSignalStrength nrSignalStrength = new NrSignalStrength();
      nrSignalStrength.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 24));
      arrayList.add(nrSignalStrength);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.ssRsrp = paramHwBlob.getInt32(0L + paramLong);
    this.ssRsrq = paramHwBlob.getInt32(4L + paramLong);
    this.ssSinr = paramHwBlob.getInt32(8L + paramLong);
    this.csiRsrp = paramHwBlob.getInt32(12L + paramLong);
    this.csiRsrq = paramHwBlob.getInt32(16L + paramLong);
    this.csiSinr = paramHwBlob.getInt32(20L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(24);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<NrSignalStrength> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 24);
    for (byte b = 0; b < i; b++)
      ((NrSignalStrength)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 24)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.ssRsrp);
    paramHwBlob.putInt32(4L + paramLong, this.ssRsrq);
    paramHwBlob.putInt32(8L + paramLong, this.ssSinr);
    paramHwBlob.putInt32(12L + paramLong, this.csiRsrp);
    paramHwBlob.putInt32(16L + paramLong, this.csiRsrq);
    paramHwBlob.putInt32(20L + paramLong, this.csiSinr);
  }
}
