package android.hardware.radio.V1_4;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class SetupDataCallResult {
  public int cause = 0;
  
  public int suggestedRetryTime = 0;
  
  public int cid = 0;
  
  public int active = 0;
  
  public int type = 0;
  
  public String ifname = new String();
  
  public ArrayList<String> addresses = new ArrayList<>();
  
  public ArrayList<String> dnses = new ArrayList<>();
  
  public ArrayList<String> gateways = new ArrayList<>();
  
  public ArrayList<String> pcscf = new ArrayList<>();
  
  public int mtu = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != SetupDataCallResult.class)
      return false; 
    paramObject = paramObject;
    if (this.cause != ((SetupDataCallResult)paramObject).cause)
      return false; 
    if (this.suggestedRetryTime != ((SetupDataCallResult)paramObject).suggestedRetryTime)
      return false; 
    if (this.cid != ((SetupDataCallResult)paramObject).cid)
      return false; 
    if (this.active != ((SetupDataCallResult)paramObject).active)
      return false; 
    if (this.type != ((SetupDataCallResult)paramObject).type)
      return false; 
    if (!HidlSupport.deepEquals(this.ifname, ((SetupDataCallResult)paramObject).ifname))
      return false; 
    if (!HidlSupport.deepEquals(this.addresses, ((SetupDataCallResult)paramObject).addresses))
      return false; 
    if (!HidlSupport.deepEquals(this.dnses, ((SetupDataCallResult)paramObject).dnses))
      return false; 
    if (!HidlSupport.deepEquals(this.gateways, ((SetupDataCallResult)paramObject).gateways))
      return false; 
    if (!HidlSupport.deepEquals(this.pcscf, ((SetupDataCallResult)paramObject).pcscf))
      return false; 
    if (this.mtu != ((SetupDataCallResult)paramObject).mtu)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.cause;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.suggestedRetryTime;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.cid;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.active;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    int n = this.type;
    int i1 = HidlSupport.deepHashCode(Integer.valueOf(n));
    String str = this.ifname;
    int i2 = HidlSupport.deepHashCode(str);
    ArrayList<String> arrayList = this.addresses;
    int i3 = HidlSupport.deepHashCode(arrayList);
    arrayList = this.dnses;
    n = HidlSupport.deepHashCode(arrayList);
    arrayList = this.gateways;
    int i4 = HidlSupport.deepHashCode(arrayList);
    arrayList = this.pcscf;
    int i5 = HidlSupport.deepHashCode(arrayList), i6 = this.mtu;
    i6 = HidlSupport.deepHashCode(Integer.valueOf(i6));
    return Objects.hash(new Object[] { 
          Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(n), Integer.valueOf(i4), Integer.valueOf(i5), 
          Integer.valueOf(i6) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".cause = ");
    stringBuilder.append(DataCallFailCause.toString(this.cause));
    stringBuilder.append(", .suggestedRetryTime = ");
    stringBuilder.append(this.suggestedRetryTime);
    stringBuilder.append(", .cid = ");
    stringBuilder.append(this.cid);
    stringBuilder.append(", .active = ");
    stringBuilder.append(DataConnActiveStatus.toString(this.active));
    stringBuilder.append(", .type = ");
    stringBuilder.append(PdpProtocolType.toString(this.type));
    stringBuilder.append(", .ifname = ");
    stringBuilder.append(this.ifname);
    stringBuilder.append(", .addresses = ");
    stringBuilder.append(this.addresses);
    stringBuilder.append(", .dnses = ");
    stringBuilder.append(this.dnses);
    stringBuilder.append(", .gateways = ");
    stringBuilder.append(this.gateways);
    stringBuilder.append(", .pcscf = ");
    stringBuilder.append(this.pcscf);
    stringBuilder.append(", .mtu = ");
    stringBuilder.append(this.mtu);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(112L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<SetupDataCallResult> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<SetupDataCallResult> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 112);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      SetupDataCallResult setupDataCallResult = new SetupDataCallResult();
      setupDataCallResult.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 112));
      arrayList.add(setupDataCallResult);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.cause = paramHwBlob.getInt32(paramLong + 0L);
    this.suggestedRetryTime = paramHwBlob.getInt32(paramLong + 4L);
    this.cid = paramHwBlob.getInt32(paramLong + 8L);
    this.active = paramHwBlob.getInt32(paramLong + 12L);
    this.type = paramHwBlob.getInt32(paramLong + 16L);
    String str = paramHwBlob.getString(paramLong + 24L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 24L + 0L, false);
    int i = paramHwBlob.getInt32(paramLong + 40L + 8L);
    l2 = (i * 16);
    l1 = paramHwBlob.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 40L + 0L, true);
    this.addresses.clear();
    byte b;
    for (b = 0; b < i; b++) {
      new String();
      str = hwBlob2.getString((b * 16));
      l1 = ((str.getBytes()).length + 1);
      l2 = hwBlob2.handle();
      long l = (b * 16 + 0);
      paramHwParcel.readEmbeddedBuffer(l1, l2, l, false);
      this.addresses.add(str);
    } 
    i = paramHwBlob.getInt32(paramLong + 56L + 8L);
    l1 = (i * 16);
    l2 = paramHwBlob.handle();
    HwBlob hwBlob1 = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 56L + 0L, true);
    this.dnses.clear();
    for (b = 0; b < i; b++) {
      new String();
      String str1 = hwBlob1.getString((b * 16));
      long l = ((str1.getBytes()).length + 1);
      l1 = hwBlob1.handle();
      l2 = (b * 16 + 0);
      paramHwParcel.readEmbeddedBuffer(l, l1, l2, false);
      this.dnses.add(str1);
    } 
    i = paramHwBlob.getInt32(paramLong + 72L + 8L);
    l2 = (i * 16);
    l1 = paramHwBlob.handle();
    hwBlob1 = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 72L + 0L, true);
    this.gateways.clear();
    for (b = 0; b < i; b++) {
      new String();
      String str1 = hwBlob1.getString((b * 16));
      l1 = ((str1.getBytes()).length + 1);
      l2 = hwBlob1.handle();
      long l = (b * 16 + 0);
      paramHwParcel.readEmbeddedBuffer(l1, l2, l, false);
      this.gateways.add(str1);
    } 
    i = paramHwBlob.getInt32(paramLong + 88L + 8L);
    l1 = (i * 16);
    l2 = paramHwBlob.handle();
    hwBlob1 = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 88L + 0L, true);
    this.pcscf.clear();
    for (b = 0; b < i; b++) {
      new String();
      String str1 = hwBlob1.getString((b * 16));
      l2 = ((str1.getBytes()).length + 1);
      l1 = hwBlob1.handle();
      long l = (b * 16 + 0);
      paramHwParcel.readEmbeddedBuffer(l2, l1, l, false);
      this.pcscf.add(str1);
    } 
    this.mtu = paramHwBlob.getInt32(paramLong + 104L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(112);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<SetupDataCallResult> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 112);
    for (byte b = 0; b < i; b++)
      ((SetupDataCallResult)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 112)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.cause);
    paramHwBlob.putInt32(paramLong + 4L, this.suggestedRetryTime);
    paramHwBlob.putInt32(paramLong + 8L, this.cid);
    paramHwBlob.putInt32(paramLong + 12L, this.active);
    paramHwBlob.putInt32(paramLong + 16L, this.type);
    paramHwBlob.putString(paramLong + 24L, this.ifname);
    int i = this.addresses.size();
    paramHwBlob.putInt32(paramLong + 40L + 8L, i);
    paramHwBlob.putBool(paramLong + 40L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 16);
    byte b;
    for (b = 0; b < i; b++)
      hwBlob.putString((b * 16), this.addresses.get(b)); 
    paramHwBlob.putBlob(paramLong + 40L + 0L, hwBlob);
    i = this.dnses.size();
    paramHwBlob.putInt32(paramLong + 56L + 8L, i);
    paramHwBlob.putBool(paramLong + 56L + 12L, false);
    hwBlob = new HwBlob(i * 16);
    for (b = 0; b < i; b++)
      hwBlob.putString((b * 16), this.dnses.get(b)); 
    paramHwBlob.putBlob(paramLong + 56L + 0L, hwBlob);
    i = this.gateways.size();
    paramHwBlob.putInt32(paramLong + 72L + 8L, i);
    paramHwBlob.putBool(paramLong + 72L + 12L, false);
    hwBlob = new HwBlob(i * 16);
    for (b = 0; b < i; b++)
      hwBlob.putString((b * 16), this.gateways.get(b)); 
    paramHwBlob.putBlob(paramLong + 72L + 0L, hwBlob);
    i = this.pcscf.size();
    paramHwBlob.putInt32(paramLong + 88L + 8L, i);
    paramHwBlob.putBool(paramLong + 88L + 12L, false);
    hwBlob = new HwBlob(i * 16);
    for (b = 0; b < i; b++)
      hwBlob.putString((b * 16), this.pcscf.get(b)); 
    paramHwBlob.putBlob(paramLong + 88L + 0L, hwBlob);
    paramHwBlob.putInt32(paramLong + 104L, this.mtu);
  }
}
