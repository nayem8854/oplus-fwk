package android.hardware.radio.V1_4;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class NrIndicators {
  public boolean isEndcAvailable = false;
  
  public boolean isDcNrRestricted = false;
  
  public boolean isNrAvailable = false;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != NrIndicators.class)
      return false; 
    paramObject = paramObject;
    if (this.isEndcAvailable != ((NrIndicators)paramObject).isEndcAvailable)
      return false; 
    if (this.isDcNrRestricted != ((NrIndicators)paramObject).isDcNrRestricted)
      return false; 
    if (this.isNrAvailable != ((NrIndicators)paramObject).isNrAvailable)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    boolean bool = this.isEndcAvailable;
    int i = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    bool = this.isDcNrRestricted;
    int j = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    bool = this.isNrAvailable;
    int k = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".isEndcAvailable = ");
    stringBuilder.append(this.isEndcAvailable);
    stringBuilder.append(", .isDcNrRestricted = ");
    stringBuilder.append(this.isDcNrRestricted);
    stringBuilder.append(", .isNrAvailable = ");
    stringBuilder.append(this.isNrAvailable);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(3L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<NrIndicators> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<NrIndicators> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 3);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      NrIndicators nrIndicators = new NrIndicators();
      nrIndicators.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 3));
      arrayList.add(nrIndicators);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.isEndcAvailable = paramHwBlob.getBool(0L + paramLong);
    this.isDcNrRestricted = paramHwBlob.getBool(1L + paramLong);
    this.isNrAvailable = paramHwBlob.getBool(2L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(3);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<NrIndicators> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 3);
    for (byte b = 0; b < i; b++)
      ((NrIndicators)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 3)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putBool(0L + paramLong, this.isEndcAvailable);
    paramHwBlob.putBool(1L + paramLong, this.isDcNrRestricted);
    paramHwBlob.putBool(2L + paramLong, this.isNrAvailable);
  }
}
