package android.hardware.radio.V1_4;

import java.util.ArrayList;

public final class DataConnActiveStatus {
  public static final int ACTIVE = 2;
  
  public static final int DORMANT = 1;
  
  public static final int INACTIVE = 0;
  
  public static final String toString(int paramInt) {
    if (paramInt == 0)
      return "INACTIVE"; 
    if (paramInt == 1)
      return "DORMANT"; 
    if (paramInt == 2)
      return "ACTIVE"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    arrayList.add("INACTIVE");
    if ((paramInt & 0x1) == 1) {
      arrayList.add("DORMANT");
      i = false | true;
    } 
    int j = i;
    if ((paramInt & 0x2) == 2) {
      arrayList.add("ACTIVE");
      j = i | 0x2;
    } 
    if (paramInt != j) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((j ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
