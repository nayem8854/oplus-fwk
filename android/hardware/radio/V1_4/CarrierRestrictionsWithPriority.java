package android.hardware.radio.V1_4;

import android.hardware.radio.V1_0.Carrier;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CarrierRestrictionsWithPriority {
  public ArrayList<Carrier> allowedCarriers = new ArrayList<>();
  
  public ArrayList<Carrier> excludedCarriers = new ArrayList<>();
  
  public boolean allowedCarriersPrioritized = false;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CarrierRestrictionsWithPriority.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.allowedCarriers, ((CarrierRestrictionsWithPriority)paramObject).allowedCarriers))
      return false; 
    if (!HidlSupport.deepEquals(this.excludedCarriers, ((CarrierRestrictionsWithPriority)paramObject).excludedCarriers))
      return false; 
    if (this.allowedCarriersPrioritized != ((CarrierRestrictionsWithPriority)paramObject).allowedCarriersPrioritized)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    ArrayList<Carrier> arrayList = this.allowedCarriers;
    int i = HidlSupport.deepHashCode(arrayList);
    arrayList = this.excludedCarriers;
    int j = HidlSupport.deepHashCode(arrayList);
    boolean bool = this.allowedCarriersPrioritized;
    int k = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".allowedCarriers = ");
    stringBuilder.append(this.allowedCarriers);
    stringBuilder.append(", .excludedCarriers = ");
    stringBuilder.append(this.excludedCarriers);
    stringBuilder.append(", .allowedCarriersPrioritized = ");
    stringBuilder.append(this.allowedCarriersPrioritized);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(40L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CarrierRestrictionsWithPriority> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CarrierRestrictionsWithPriority> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 40);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CarrierRestrictionsWithPriority carrierRestrictionsWithPriority = new CarrierRestrictionsWithPriority();
      carrierRestrictionsWithPriority.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 40));
      arrayList.add(carrierRestrictionsWithPriority);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    int i = paramHwBlob.getInt32(paramLong + 0L + 8L);
    long l1 = (i * 56);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob1 = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, true);
    this.allowedCarriers.clear();
    byte b;
    for (b = 0; b < i; b++) {
      Carrier carrier = new Carrier();
      carrier.readEmbeddedFromParcel(paramHwParcel, hwBlob1, (b * 56));
      this.allowedCarriers.add(carrier);
    } 
    i = paramHwBlob.getInt32(paramLong + 16L + 8L);
    l2 = (i * 56);
    l1 = paramHwBlob.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 16L + 0L, true);
    this.excludedCarriers.clear();
    for (b = 0; b < i; b++) {
      Carrier carrier = new Carrier();
      carrier.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 56));
      this.excludedCarriers.add(carrier);
    } 
    this.allowedCarriersPrioritized = paramHwBlob.getBool(paramLong + 32L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(40);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CarrierRestrictionsWithPriority> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 40);
    for (byte b = 0; b < i; b++)
      ((CarrierRestrictionsWithPriority)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 40)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    int i = this.allowedCarriers.size();
    paramHwBlob.putInt32(paramLong + 0L + 8L, i);
    paramHwBlob.putBool(paramLong + 0L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 56);
    byte b;
    for (b = 0; b < i; b++)
      ((Carrier)this.allowedCarriers.get(b)).writeEmbeddedToBlob(hwBlob, (b * 56)); 
    paramHwBlob.putBlob(paramLong + 0L + 0L, hwBlob);
    i = this.excludedCarriers.size();
    paramHwBlob.putInt32(paramLong + 16L + 8L, i);
    paramHwBlob.putBool(paramLong + 16L + 12L, false);
    hwBlob = new HwBlob(i * 56);
    for (b = 0; b < i; b++)
      ((Carrier)this.excludedCarriers.get(b)).writeEmbeddedToBlob(hwBlob, (b * 56)); 
    paramHwBlob.putBlob(paramLong + 16L + 0L, hwBlob);
    paramHwBlob.putBool(paramLong + 32L, this.allowedCarriersPrioritized);
  }
}
