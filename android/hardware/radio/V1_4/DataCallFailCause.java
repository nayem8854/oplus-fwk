package android.hardware.radio.V1_4;

import java.util.ArrayList;

public final class DataCallFailCause {
  public static final int ACCESS_ATTEMPT_ALREADY_IN_PROGRESS = 2219;
  
  public static final int ACCESS_BLOCK = 2087;
  
  public static final int ACCESS_BLOCK_ALL = 2088;
  
  public static final int ACCESS_CLASS_DSAC_REJECTION = 2108;
  
  public static final int ACCESS_CONTROL_LIST_CHECK_FAILURE = 2128;
  
  public static final int ACTIVATION_REJECTED_BCM_VIOLATION = 48;
  
  public static final int ACTIVATION_REJECT_GGSN = 30;
  
  public static final int ACTIVATION_REJECT_UNSPECIFIED = 31;
  
  public static final int APN_DISABLED = 2045;
  
  public static final int APN_DISALLOWED_ON_ROAMING = 2059;
  
  public static final int APN_MISMATCH = 2054;
  
  public static final int APN_PARAMETERS_CHANGED = 2060;
  
  public static final int APN_PENDING_HANDOVER = 2041;
  
  public static final int APN_TYPE_CONFLICT = 112;
  
  public static final int AUTH_FAILURE_ON_EMERGENCY_CALL = 122;
  
  public static final int BEARER_HANDLING_NOT_SUPPORTED = 60;
  
  public static final int CALL_DISALLOWED_IN_ROAMING = 2068;
  
  public static final int CALL_PREEMPT_BY_EMERGENCY_APN = 127;
  
  public static final int CANNOT_ENCODE_OTA_MESSAGE = 2159;
  
  public static final int CDMA_ALERT_STOP = 2077;
  
  public static final int CDMA_INCOMING_CALL = 2076;
  
  public static final int CDMA_INTERCEPT = 2073;
  
  public static final int CDMA_LOCK = 2072;
  
  public static final int CDMA_RELEASE_DUE_TO_SO_REJECTION = 2075;
  
  public static final int CDMA_REORDER = 2074;
  
  public static final int CDMA_RETRY_ORDER = 2086;
  
  public static final int CHANNEL_ACQUISITION_FAILURE = 2078;
  
  public static final int CLOSE_IN_PROGRESS = 2030;
  
  public static final int COLLISION_WITH_NETWORK_INITIATED_REQUEST = 56;
  
  public static final int COMPANION_IFACE_IN_USE = 118;
  
  public static final int CONCURRENT_SERVICES_INCOMPATIBLE = 2083;
  
  public static final int CONCURRENT_SERVICES_NOT_ALLOWED = 2091;
  
  public static final int CONCURRENT_SERVICE_NOT_SUPPORTED_BY_BASE_STATION = 2080;
  
  public static final int CONDITIONAL_IE_ERROR = 100;
  
  public static final int CONGESTION = 2106;
  
  public static final int CONNECTION_RELEASED = 2113;
  
  public static final int CS_DOMAIN_NOT_AVAILABLE = 2181;
  
  public static final int CS_FALLBACK_CALL_ESTABLISHMENT_NOT_ALLOWED = 2188;
  
  public static final int DATA_PLAN_EXPIRED = 2198;
  
  public static final int DATA_REGISTRATION_FAIL = -2;
  
  public static final int DATA_ROAMING_SETTINGS_DISABLED = 2064;
  
  public static final int DATA_SETTINGS_DISABLED = 2063;
  
  public static final int DBM_OR_SMS_IN_PROGRESS = 2211;
  
  public static final int DDS_SWITCHED = 2065;
  
  public static final int DDS_SWITCH_IN_PROGRESS = 2067;
  
  public static final int DRB_RELEASED_BY_RRC = 2112;
  
  public static final int DS_EXPLICIT_DEACTIVATION = 2125;
  
  public static final int DUAL_SWITCH = 2227;
  
  public static final int DUN_CALL_DISALLOWED = 2056;
  
  public static final int DUPLICATE_BEARER_ID = 2118;
  
  public static final int EHRPD_TO_HRPD_FALLBACK = 2049;
  
  public static final int EMBMS_NOT_ENABLED = 2193;
  
  public static final int EMBMS_REGULAR_DEACTIVATION = 2195;
  
  public static final int EMERGENCY_IFACE_ONLY = 116;
  
  public static final int EMERGENCY_MODE = 2221;
  
  public static final int EMM_ACCESS_BARRED = 115;
  
  public static final int EMM_ACCESS_BARRED_INFINITE_RETRY = 121;
  
  public static final int EMM_ATTACH_FAILED = 2115;
  
  public static final int EMM_ATTACH_STARTED = 2116;
  
  public static final int EMM_DETACHED = 2114;
  
  public static final int EMM_T3417_EXPIRED = 2130;
  
  public static final int EMM_T3417_EXT_EXPIRED = 2131;
  
  public static final int EPS_SERVICES_AND_NON_EPS_SERVICES_NOT_ALLOWED = 2178;
  
  public static final int EPS_SERVICES_NOT_ALLOWED_IN_PLMN = 2179;
  
  public static final int ERROR_UNSPECIFIED = 65535;
  
  public static final int ESM_BAD_OTA_MESSAGE = 2122;
  
  public static final int ESM_BEARER_DEACTIVATED_TO_SYNC_WITH_NETWORK = 2120;
  
  public static final int ESM_COLLISION_SCENARIOS = 2119;
  
  public static final int ESM_CONTEXT_TRANSFERRED_DUE_TO_IRAT = 2124;
  
  public static final int ESM_DOWNLOAD_SERVER_REJECTED_THE_CALL = 2123;
  
  public static final int ESM_FAILURE = 2182;
  
  public static final int ESM_INFO_NOT_RECEIVED = 53;
  
  public static final int ESM_LOCAL_CAUSE_NONE = 2126;
  
  public static final int ESM_NW_ACTIVATED_DED_BEARER_WITH_ID_OF_DEF_BEARER = 2121;
  
  public static final int ESM_PROCEDURE_TIME_OUT = 2155;
  
  public static final int ESM_UNKNOWN_EPS_BEARER_CONTEXT = 2111;
  
  public static final int EVDO_CONNECTION_DENY_BY_BILLING_OR_AUTHENTICATION_FAILURE = 2201;
  
  public static final int EVDO_CONNECTION_DENY_BY_GENERAL_OR_NETWORK_BUSY = 2200;
  
  public static final int EVDO_HDR_CHANGED = 2202;
  
  public static final int EVDO_HDR_CONNECTION_SETUP_TIMEOUT = 2206;
  
  public static final int EVDO_HDR_EXITED = 2203;
  
  public static final int EVDO_HDR_NO_SESSION = 2204;
  
  public static final int EVDO_USING_GPS_FIX_INSTEAD_OF_HDR_CALL = 2205;
  
  public static final int FADE = 2217;
  
  public static final int FAILED_TO_ACQUIRE_COLOCATED_HDR = 2207;
  
  public static final int FEATURE_NOT_SUPP = 40;
  
  public static final int FILTER_SEMANTIC_ERROR = 44;
  
  public static final int FILTER_SYTAX_ERROR = 45;
  
  public static final int FORBIDDEN_APN_NAME = 2066;
  
  public static final int GPRS_SERVICES_AND_NON_GPRS_SERVICES_NOT_ALLOWED = 2097;
  
  public static final int GPRS_SERVICES_NOT_ALLOWED = 2098;
  
  public static final int GPRS_SERVICES_NOT_ALLOWED_IN_THIS_PLMN = 2103;
  
  public static final int HANDOFF_PREFERENCE_CHANGED = 2251;
  
  public static final int HDR_ACCESS_FAILURE = 2213;
  
  public static final int HDR_FADE = 2212;
  
  public static final int HDR_NO_LOCK_GRANTED = 2210;
  
  public static final int IFACE_AND_POL_FAMILY_MISMATCH = 120;
  
  public static final int IFACE_MISMATCH = 117;
  
  public static final int ILLEGAL_ME = 2096;
  
  public static final int ILLEGAL_MS = 2095;
  
  public static final int IMEI_NOT_ACCEPTED = 2177;
  
  public static final int IMPLICITLY_DETACHED = 2100;
  
  public static final int IMSI_UNKNOWN_IN_HOME_SUBSCRIBER_SERVER = 2176;
  
  public static final int INCOMING_CALL_REJECTED = 2092;
  
  public static final int INSUFFICIENT_RESOURCES = 26;
  
  public static final int INTERFACE_IN_USE = 2058;
  
  public static final int INTERNAL_CALL_PREEMPT_BY_HIGH_PRIO_APN = 114;
  
  public static final int INTERNAL_EPC_NONEPC_TRANSITION = 2057;
  
  public static final int INVALID_CONNECTION_ID = 2156;
  
  public static final int INVALID_DNS_ADDR = 123;
  
  public static final int INVALID_EMM_STATE = 2190;
  
  public static final int INVALID_MANDATORY_INFO = 96;
  
  public static final int INVALID_MODE = 2223;
  
  public static final int INVALID_PCSCF_ADDR = 113;
  
  public static final int INVALID_PCSCF_OR_DNS_ADDRESS = 124;
  
  public static final int INVALID_PRIMARY_NSAPI = 2158;
  
  public static final int INVALID_SIM_STATE = 2224;
  
  public static final int INVALID_TRANSACTION_ID = 81;
  
  public static final int IPV6_ADDRESS_TRANSFER_FAILED = 2047;
  
  public static final int IPV6_PREFIX_UNAVAILABLE = 2250;
  
  public static final int IP_ADDRESS_MISMATCH = 119;
  
  public static final int IP_VERSION_MISMATCH = 2055;
  
  public static final int IRAT_HANDOVER_FAILED = 2194;
  
  public static final int IS707B_MAX_ACCESS_PROBES = 2089;
  
  public static final int LIMITED_TO_IPV4 = 2234;
  
  public static final int LIMITED_TO_IPV6 = 2235;
  
  public static final int LLC_SNDCP = 25;
  
  public static final int LOCAL_END = 2215;
  
  public static final int LOCATION_AREA_NOT_ALLOWED = 2102;
  
  public static final int LOWER_LAYER_REGISTRATION_FAILURE = 2197;
  
  public static final int LOW_POWER_MODE_OR_POWERING_DOWN = 2044;
  
  public static final int LTE_NAS_SERVICE_REQUEST_FAILED = 2117;
  
  public static final int LTE_THROTTLING_NOT_REQUIRED = 2127;
  
  public static final int MAC_FAILURE = 2183;
  
  public static final int MAXIMIUM_NSAPIS_EXCEEDED = 2157;
  
  public static final int MAXINUM_SIZE_OF_L2_MESSAGE_EXCEEDED = 2166;
  
  public static final int MAX_ACCESS_PROBE = 2079;
  
  public static final int MAX_ACTIVE_PDP_CONTEXT_REACHED = 65;
  
  public static final int MAX_IPV4_CONNECTIONS = 2052;
  
  public static final int MAX_IPV6_CONNECTIONS = 2053;
  
  public static final int MAX_PPP_INACTIVITY_TIMER_EXPIRED = 2046;
  
  public static final int MESSAGE_INCORRECT_SEMANTIC = 95;
  
  public static final int MESSAGE_TYPE_UNSUPPORTED = 97;
  
  public static final int MIP_CONFIG_FAILURE = 2050;
  
  public static final int MIP_FA_ADMIN_PROHIBITED = 2001;
  
  public static final int MIP_FA_DELIVERY_STYLE_NOT_SUPPORTED = 2012;
  
  public static final int MIP_FA_ENCAPSULATION_UNAVAILABLE = 2008;
  
  public static final int MIP_FA_HOME_AGENT_AUTHENTICATION_FAILURE = 2004;
  
  public static final int MIP_FA_INSUFFICIENT_RESOURCES = 2002;
  
  public static final int MIP_FA_MALFORMED_REPLY = 2007;
  
  public static final int MIP_FA_MALFORMED_REQUEST = 2006;
  
  public static final int MIP_FA_MISSING_CHALLENGE = 2017;
  
  public static final int MIP_FA_MISSING_HOME_ADDRESS = 2015;
  
  public static final int MIP_FA_MISSING_HOME_AGENT = 2014;
  
  public static final int MIP_FA_MISSING_NAI = 2013;
  
  public static final int MIP_FA_MOBILE_NODE_AUTHENTICATION_FAILURE = 2003;
  
  public static final int MIP_FA_REASON_UNSPECIFIED = 2000;
  
  public static final int MIP_FA_REQUESTED_LIFETIME_TOO_LONG = 2005;
  
  public static final int MIP_FA_REVERSE_TUNNEL_IS_MANDATORY = 2011;
  
  public static final int MIP_FA_REVERSE_TUNNEL_UNAVAILABLE = 2010;
  
  public static final int MIP_FA_STALE_CHALLENGE = 2018;
  
  public static final int MIP_FA_UNKNOWN_CHALLENGE = 2016;
  
  public static final int MIP_FA_VJ_HEADER_COMPRESSION_UNAVAILABLE = 2009;
  
  public static final int MIP_HA_ADMIN_PROHIBITED = 2020;
  
  public static final int MIP_HA_ENCAPSULATION_UNAVAILABLE = 2029;
  
  public static final int MIP_HA_FOREIGN_AGENT_AUTHENTICATION_FAILURE = 2023;
  
  public static final int MIP_HA_INSUFFICIENT_RESOURCES = 2021;
  
  public static final int MIP_HA_MALFORMED_REQUEST = 2025;
  
  public static final int MIP_HA_MOBILE_NODE_AUTHENTICATION_FAILURE = 2022;
  
  public static final int MIP_HA_REASON_UNSPECIFIED = 2019;
  
  public static final int MIP_HA_REGISTRATION_ID_MISMATCH = 2024;
  
  public static final int MIP_HA_REVERSE_TUNNEL_IS_MANDATORY = 2028;
  
  public static final int MIP_HA_REVERSE_TUNNEL_UNAVAILABLE = 2027;
  
  public static final int MIP_HA_UNKNOWN_HOME_AGENT_ADDRESS = 2026;
  
  public static final int MISSING_UKNOWN_APN = 27;
  
  public static final int MODEM_APP_PREEMPTED = 2032;
  
  public static final int MODEM_RESTART = 2037;
  
  public static final int MSC_TEMPORARILY_NOT_REACHABLE = 2180;
  
  public static final int MSG_AND_PROTOCOL_STATE_UNCOMPATIBLE = 101;
  
  public static final int MSG_TYPE_NONCOMPATIBLE_STATE = 98;
  
  public static final int MS_IDENTITY_CANNOT_BE_DERIVED_BY_THE_NETWORK = 2099;
  
  public static final int MULTIPLE_PDP_CALL_NOT_ALLOWED = 2192;
  
  public static final int MULTI_CONN_TO_SAME_PDN_NOT_ALLOWED = 55;
  
  public static final int NAS_LAYER_FAILURE = 2191;
  
  public static final int NAS_REQUEST_REJECTED_BY_NETWORK = 2167;
  
  public static final int NAS_SIGNALLING = 14;
  
  public static final int NETWORK_FAILURE = 38;
  
  public static final int NETWORK_INITIATED_DETACH_NO_AUTO_REATTACH = 2154;
  
  public static final int NETWORK_INITIATED_DETACH_WITH_AUTO_REATTACH = 2153;
  
  public static final int NETWORK_INITIATED_TERMINATION = 2031;
  
  public static final int NONE = 0;
  
  public static final int NON_IP_NOT_SUPPORTED = 2069;
  
  public static final int NORMAL_RELEASE = 2218;
  
  public static final int NO_CDMA_SERVICE = 2084;
  
  public static final int NO_COLLOCATED_HDR = 2225;
  
  public static final int NO_EPS_BEARER_CONTEXT_ACTIVATED = 2189;
  
  public static final int NO_GPRS_CONTEXT = 2094;
  
  public static final int NO_HYBRID_HDR_SERVICE = 2209;
  
  public static final int NO_PDP_CONTEXT_ACTIVATED = 2107;
  
  public static final int NO_RESPONSE_FROM_BASE_STATION = 2081;
  
  public static final int NO_SERVICE = 2216;
  
  public static final int NO_SERVICE_ON_GATEWAY = 2093;
  
  public static final int NSAPI_IN_USE = 35;
  
  public static final int NULL_APN_DISALLOWED = 2061;
  
  public static final int OEM_DCFAILCAUSE_1 = 4097;
  
  public static final int OEM_DCFAILCAUSE_10 = 4106;
  
  public static final int OEM_DCFAILCAUSE_11 = 4107;
  
  public static final int OEM_DCFAILCAUSE_12 = 4108;
  
  public static final int OEM_DCFAILCAUSE_13 = 4109;
  
  public static final int OEM_DCFAILCAUSE_14 = 4110;
  
  public static final int OEM_DCFAILCAUSE_15 = 4111;
  
  public static final int OEM_DCFAILCAUSE_2 = 4098;
  
  public static final int OEM_DCFAILCAUSE_3 = 4099;
  
  public static final int OEM_DCFAILCAUSE_4 = 4100;
  
  public static final int OEM_DCFAILCAUSE_5 = 4101;
  
  public static final int OEM_DCFAILCAUSE_6 = 4102;
  
  public static final int OEM_DCFAILCAUSE_7 = 4103;
  
  public static final int OEM_DCFAILCAUSE_8 = 4104;
  
  public static final int OEM_DCFAILCAUSE_9 = 4105;
  
  public static final int ONLY_IPV4V6_ALLOWED = 57;
  
  public static final int ONLY_IPV4_ALLOWED = 50;
  
  public static final int ONLY_IPV6_ALLOWED = 51;
  
  public static final int ONLY_NON_IP_ALLOWED = 58;
  
  public static final int ONLY_SINGLE_BEARER_ALLOWED = 52;
  
  public static final int OPERATOR_BARRED = 8;
  
  public static final int OTASP_COMMIT_IN_PROGRESS = 2208;
  
  public static final int PDN_CONN_DOES_NOT_EXIST = 54;
  
  public static final int PDN_INACTIVITY_TIMER_EXPIRED = 2051;
  
  public static final int PDN_IPV4_CALL_DISALLOWED = 2033;
  
  public static final int PDN_IPV4_CALL_THROTTLED = 2034;
  
  public static final int PDN_IPV6_CALL_DISALLOWED = 2035;
  
  public static final int PDN_IPV6_CALL_THROTTLED = 2036;
  
  public static final int PDN_NON_IP_CALL_DISALLOWED = 2071;
  
  public static final int PDN_NON_IP_CALL_THROTTLED = 2070;
  
  public static final int PDP_ACTIVATE_MAX_RETRY_FAILED = 2109;
  
  public static final int PDP_DUPLICATE = 2104;
  
  public static final int PDP_ESTABLISH_TIMEOUT_EXPIRED = 2161;
  
  public static final int PDP_INACTIVE_TIMEOUT_EXPIRED = 2163;
  
  public static final int PDP_LOWERLAYER_ERROR = 2164;
  
  public static final int PDP_MODIFY_COLLISION = 2165;
  
  public static final int PDP_MODIFY_TIMEOUT_EXPIRED = 2162;
  
  public static final int PDP_PPP_NOT_SUPPORTED = 2038;
  
  public static final int PDP_WITHOUT_ACTIVE_TFT = 46;
  
  public static final int PHONE_IN_USE = 2222;
  
  public static final int PHYSICAL_LINK_CLOSE_IN_PROGRESS = 2040;
  
  public static final int PLMN_NOT_ALLOWED = 2101;
  
  public static final int PPP_AUTH_FAILURE = 2229;
  
  public static final int PPP_CHAP_FAILURE = 2232;
  
  public static final int PPP_CLOSE_IN_PROGRESS = 2233;
  
  public static final int PPP_OPTION_MISMATCH = 2230;
  
  public static final int PPP_PAP_FAILURE = 2231;
  
  public static final int PPP_TIMEOUT = 2228;
  
  public static final int PREF_RADIO_TECH_CHANGED = -4;
  
  public static final int PROFILE_BEARER_INCOMPATIBLE = 2042;
  
  public static final int PROTOCOL_ERRORS = 111;
  
  public static final int QOS_NOT_ACCEPTED = 37;
  
  public static final int RADIO_ACCESS_BEARER_FAILURE = 2110;
  
  public static final int RADIO_ACCESS_BEARER_SETUP_FAILURE = 2160;
  
  public static final int RADIO_POWER_OFF = -5;
  
  public static final int REDIRECTION_OR_HANDOFF_IN_PROGRESS = 2220;
  
  public static final int REGULAR_DEACTIVATION = 36;
  
  public static final int REJECTED_BY_BASE_STATION = 2082;
  
  public static final int RRC_CONNECTION_ABORTED_AFTER_HANDOVER = 2173;
  
  public static final int RRC_CONNECTION_ABORTED_AFTER_IRAT_CELL_CHANGE = 2174;
  
  public static final int RRC_CONNECTION_ABORTED_DUE_TO_IRAT_CHANGE = 2171;
  
  public static final int RRC_CONNECTION_ABORTED_DURING_IRAT_CELL_CHANGE = 2175;
  
  public static final int RRC_CONNECTION_ABORT_REQUEST = 2151;
  
  public static final int RRC_CONNECTION_ACCESS_BARRED = 2139;
  
  public static final int RRC_CONNECTION_ACCESS_STRATUM_FAILURE = 2137;
  
  public static final int RRC_CONNECTION_ANOTHER_PROCEDURE_IN_PROGRESS = 2138;
  
  public static final int RRC_CONNECTION_CELL_NOT_CAMPED = 2144;
  
  public static final int RRC_CONNECTION_CELL_RESELECTION = 2140;
  
  public static final int RRC_CONNECTION_CONFIG_FAILURE = 2141;
  
  public static final int RRC_CONNECTION_INVALID_REQUEST = 2168;
  
  public static final int RRC_CONNECTION_LINK_FAILURE = 2143;
  
  public static final int RRC_CONNECTION_NORMAL_RELEASE = 2147;
  
  public static final int RRC_CONNECTION_OUT_OF_SERVICE_DURING_CELL_REGISTER = 2150;
  
  public static final int RRC_CONNECTION_RADIO_LINK_FAILURE = 2148;
  
  public static final int RRC_CONNECTION_REESTABLISHMENT_FAILURE = 2149;
  
  public static final int RRC_CONNECTION_REJECT_BY_NETWORK = 2146;
  
  public static final int RRC_CONNECTION_RELEASED_SECURITY_NOT_ACTIVE = 2172;
  
  public static final int RRC_CONNECTION_RF_UNAVAILABLE = 2170;
  
  public static final int RRC_CONNECTION_SYSTEM_INFORMATION_BLOCK_READ_ERROR = 2152;
  
  public static final int RRC_CONNECTION_SYSTEM_INTERVAL_FAILURE = 2145;
  
  public static final int RRC_CONNECTION_TIMER_EXPIRED = 2142;
  
  public static final int RRC_CONNECTION_TRACKING_AREA_ID_CHANGED = 2169;
  
  public static final int RRC_UPLINK_CONNECTION_RELEASE = 2134;
  
  public static final int RRC_UPLINK_DATA_TRANSMISSION_FAILURE = 2132;
  
  public static final int RRC_UPLINK_DELIVERY_FAILED_DUE_TO_HANDOVER = 2133;
  
  public static final int RRC_UPLINK_ERROR_REQUEST_FROM_NAS = 2136;
  
  public static final int RRC_UPLINK_RADIO_LINK_FAILURE = 2135;
  
  public static final int RUIM_NOT_PRESENT = 2085;
  
  public static final int SECURITY_MODE_REJECTED = 2186;
  
  public static final int SERVICE_NOT_ALLOWED_ON_PLMN = 2129;
  
  public static final int SERVICE_OPTION_NOT_SUBSCRIBED = 33;
  
  public static final int SERVICE_OPTION_NOT_SUPPORTED = 32;
  
  public static final int SERVICE_OPTION_OUT_OF_ORDER = 34;
  
  public static final int SIGNAL_LOST = -3;
  
  public static final int SIM_CARD_CHANGED = 2043;
  
  public static final int SYNCHRONIZATION_FAILURE = 2184;
  
  public static final int TEST_LOOPBACK_REGULAR_DEACTIVATION = 2196;
  
  public static final int TETHERED_CALL_ACTIVE = -6;
  
  public static final int TFT_SEMANTIC_ERROR = 41;
  
  public static final int TFT_SYTAX_ERROR = 42;
  
  public static final int THERMAL_EMERGENCY = 2090;
  
  public static final int THERMAL_MITIGATION = 2062;
  
  public static final int TRAT_SWAP_FAILED = 2048;
  
  public static final int UE_INITIATED_DETACH_OR_DISCONNECT = 128;
  
  public static final int UE_IS_ENTERING_POWERSAVE_MODE = 2226;
  
  public static final int UE_RAT_CHANGE = 2105;
  
  public static final int UE_SECURITY_CAPABILITIES_MISMATCH = 2185;
  
  public static final int UMTS_HANDOVER_TO_IWLAN = 2199;
  
  public static final int UMTS_REACTIVATION_REQ = 39;
  
  public static final int UNACCEPTABLE_NON_EPS_AUTHENTICATION = 2187;
  
  public static final int UNKNOWN_INFO_ELEMENT = 99;
  
  public static final int UNKNOWN_PDP_ADDRESS_TYPE = 28;
  
  public static final int UNKNOWN_PDP_CONTEXT = 43;
  
  public static final int UNPREFERRED_RAT = 2039;
  
  public static final int UNSUPPORTED_1X_PREV = 2214;
  
  public static final int UNSUPPORTED_APN_IN_CURRENT_PLMN = 66;
  
  public static final int UNSUPPORTED_QCI_VALUE = 59;
  
  public static final int USER_AUTHENTICATION = 29;
  
  public static final int VOICE_REGISTRATION_FAIL = -1;
  
  public static final int VSNCP_ADMINISTRATIVELY_PROHIBITED = 2245;
  
  public static final int VSNCP_APN_UNATHORIZED = 2238;
  
  public static final int VSNCP_GEN_ERROR = 2237;
  
  public static final int VSNCP_INSUFFICIENT_PARAMETERS = 2243;
  
  public static final int VSNCP_NO_PDN_GATEWAY_ADDRESS = 2240;
  
  public static final int VSNCP_PDN_EXISTS_FOR_THIS_APN = 2248;
  
  public static final int VSNCP_PDN_GATEWAY_REJECT = 2242;
  
  public static final int VSNCP_PDN_GATEWAY_UNREACHABLE = 2241;
  
  public static final int VSNCP_PDN_ID_IN_USE = 2246;
  
  public static final int VSNCP_PDN_LIMIT_EXCEEDED = 2239;
  
  public static final int VSNCP_RECONNECT_NOT_ALLOWED = 2249;
  
  public static final int VSNCP_RESOURCE_UNAVAILABLE = 2244;
  
  public static final int VSNCP_SUBSCRIBER_LIMITATION = 2247;
  
  public static final int VSNCP_TIMEOUT = 2236;
  
  public static final String toString(int paramInt) {
    if (paramInt == 0)
      return "NONE"; 
    if (paramInt == 8)
      return "OPERATOR_BARRED"; 
    if (paramInt == 14)
      return "NAS_SIGNALLING"; 
    if (paramInt == 26)
      return "INSUFFICIENT_RESOURCES"; 
    if (paramInt == 27)
      return "MISSING_UKNOWN_APN"; 
    if (paramInt == 28)
      return "UNKNOWN_PDP_ADDRESS_TYPE"; 
    if (paramInt == 29)
      return "USER_AUTHENTICATION"; 
    if (paramInt == 30)
      return "ACTIVATION_REJECT_GGSN"; 
    if (paramInt == 31)
      return "ACTIVATION_REJECT_UNSPECIFIED"; 
    if (paramInt == 32)
      return "SERVICE_OPTION_NOT_SUPPORTED"; 
    if (paramInt == 33)
      return "SERVICE_OPTION_NOT_SUBSCRIBED"; 
    if (paramInt == 34)
      return "SERVICE_OPTION_OUT_OF_ORDER"; 
    if (paramInt == 35)
      return "NSAPI_IN_USE"; 
    if (paramInt == 36)
      return "REGULAR_DEACTIVATION"; 
    if (paramInt == 37)
      return "QOS_NOT_ACCEPTED"; 
    if (paramInt == 38)
      return "NETWORK_FAILURE"; 
    if (paramInt == 39)
      return "UMTS_REACTIVATION_REQ"; 
    if (paramInt == 40)
      return "FEATURE_NOT_SUPP"; 
    if (paramInt == 41)
      return "TFT_SEMANTIC_ERROR"; 
    if (paramInt == 42)
      return "TFT_SYTAX_ERROR"; 
    if (paramInt == 43)
      return "UNKNOWN_PDP_CONTEXT"; 
    if (paramInt == 44)
      return "FILTER_SEMANTIC_ERROR"; 
    if (paramInt == 45)
      return "FILTER_SYTAX_ERROR"; 
    if (paramInt == 46)
      return "PDP_WITHOUT_ACTIVE_TFT"; 
    if (paramInt == 50)
      return "ONLY_IPV4_ALLOWED"; 
    if (paramInt == 51)
      return "ONLY_IPV6_ALLOWED"; 
    if (paramInt == 52)
      return "ONLY_SINGLE_BEARER_ALLOWED"; 
    if (paramInt == 53)
      return "ESM_INFO_NOT_RECEIVED"; 
    if (paramInt == 54)
      return "PDN_CONN_DOES_NOT_EXIST"; 
    if (paramInt == 55)
      return "MULTI_CONN_TO_SAME_PDN_NOT_ALLOWED"; 
    if (paramInt == 65)
      return "MAX_ACTIVE_PDP_CONTEXT_REACHED"; 
    if (paramInt == 66)
      return "UNSUPPORTED_APN_IN_CURRENT_PLMN"; 
    if (paramInt == 81)
      return "INVALID_TRANSACTION_ID"; 
    if (paramInt == 95)
      return "MESSAGE_INCORRECT_SEMANTIC"; 
    if (paramInt == 96)
      return "INVALID_MANDATORY_INFO"; 
    if (paramInt == 97)
      return "MESSAGE_TYPE_UNSUPPORTED"; 
    if (paramInt == 98)
      return "MSG_TYPE_NONCOMPATIBLE_STATE"; 
    if (paramInt == 99)
      return "UNKNOWN_INFO_ELEMENT"; 
    if (paramInt == 100)
      return "CONDITIONAL_IE_ERROR"; 
    if (paramInt == 101)
      return "MSG_AND_PROTOCOL_STATE_UNCOMPATIBLE"; 
    if (paramInt == 111)
      return "PROTOCOL_ERRORS"; 
    if (paramInt == 112)
      return "APN_TYPE_CONFLICT"; 
    if (paramInt == 113)
      return "INVALID_PCSCF_ADDR"; 
    if (paramInt == 114)
      return "INTERNAL_CALL_PREEMPT_BY_HIGH_PRIO_APN"; 
    if (paramInt == 115)
      return "EMM_ACCESS_BARRED"; 
    if (paramInt == 116)
      return "EMERGENCY_IFACE_ONLY"; 
    if (paramInt == 117)
      return "IFACE_MISMATCH"; 
    if (paramInt == 118)
      return "COMPANION_IFACE_IN_USE"; 
    if (paramInt == 119)
      return "IP_ADDRESS_MISMATCH"; 
    if (paramInt == 120)
      return "IFACE_AND_POL_FAMILY_MISMATCH"; 
    if (paramInt == 121)
      return "EMM_ACCESS_BARRED_INFINITE_RETRY"; 
    if (paramInt == 122)
      return "AUTH_FAILURE_ON_EMERGENCY_CALL"; 
    if (paramInt == 4097)
      return "OEM_DCFAILCAUSE_1"; 
    if (paramInt == 4098)
      return "OEM_DCFAILCAUSE_2"; 
    if (paramInt == 4099)
      return "OEM_DCFAILCAUSE_3"; 
    if (paramInt == 4100)
      return "OEM_DCFAILCAUSE_4"; 
    if (paramInt == 4101)
      return "OEM_DCFAILCAUSE_5"; 
    if (paramInt == 4102)
      return "OEM_DCFAILCAUSE_6"; 
    if (paramInt == 4103)
      return "OEM_DCFAILCAUSE_7"; 
    if (paramInt == 4104)
      return "OEM_DCFAILCAUSE_8"; 
    if (paramInt == 4105)
      return "OEM_DCFAILCAUSE_9"; 
    if (paramInt == 4106)
      return "OEM_DCFAILCAUSE_10"; 
    if (paramInt == 4107)
      return "OEM_DCFAILCAUSE_11"; 
    if (paramInt == 4108)
      return "OEM_DCFAILCAUSE_12"; 
    if (paramInt == 4109)
      return "OEM_DCFAILCAUSE_13"; 
    if (paramInt == 4110)
      return "OEM_DCFAILCAUSE_14"; 
    if (paramInt == 4111)
      return "OEM_DCFAILCAUSE_15"; 
    if (paramInt == -1)
      return "VOICE_REGISTRATION_FAIL"; 
    if (paramInt == -2)
      return "DATA_REGISTRATION_FAIL"; 
    if (paramInt == -3)
      return "SIGNAL_LOST"; 
    if (paramInt == -4)
      return "PREF_RADIO_TECH_CHANGED"; 
    if (paramInt == -5)
      return "RADIO_POWER_OFF"; 
    if (paramInt == -6)
      return "TETHERED_CALL_ACTIVE"; 
    if (paramInt == 65535)
      return "ERROR_UNSPECIFIED"; 
    if (paramInt == 25)
      return "LLC_SNDCP"; 
    if (paramInt == 48)
      return "ACTIVATION_REJECTED_BCM_VIOLATION"; 
    if (paramInt == 56)
      return "COLLISION_WITH_NETWORK_INITIATED_REQUEST"; 
    if (paramInt == 57)
      return "ONLY_IPV4V6_ALLOWED"; 
    if (paramInt == 58)
      return "ONLY_NON_IP_ALLOWED"; 
    if (paramInt == 59)
      return "UNSUPPORTED_QCI_VALUE"; 
    if (paramInt == 60)
      return "BEARER_HANDLING_NOT_SUPPORTED"; 
    if (paramInt == 123)
      return "INVALID_DNS_ADDR"; 
    if (paramInt == 124)
      return "INVALID_PCSCF_OR_DNS_ADDRESS"; 
    if (paramInt == 127)
      return "CALL_PREEMPT_BY_EMERGENCY_APN"; 
    if (paramInt == 128)
      return "UE_INITIATED_DETACH_OR_DISCONNECT"; 
    if (paramInt == 2000)
      return "MIP_FA_REASON_UNSPECIFIED"; 
    if (paramInt == 2001)
      return "MIP_FA_ADMIN_PROHIBITED"; 
    if (paramInt == 2002)
      return "MIP_FA_INSUFFICIENT_RESOURCES"; 
    if (paramInt == 2003)
      return "MIP_FA_MOBILE_NODE_AUTHENTICATION_FAILURE"; 
    if (paramInt == 2004)
      return "MIP_FA_HOME_AGENT_AUTHENTICATION_FAILURE"; 
    if (paramInt == 2005)
      return "MIP_FA_REQUESTED_LIFETIME_TOO_LONG"; 
    if (paramInt == 2006)
      return "MIP_FA_MALFORMED_REQUEST"; 
    if (paramInt == 2007)
      return "MIP_FA_MALFORMED_REPLY"; 
    if (paramInt == 2008)
      return "MIP_FA_ENCAPSULATION_UNAVAILABLE"; 
    if (paramInt == 2009)
      return "MIP_FA_VJ_HEADER_COMPRESSION_UNAVAILABLE"; 
    if (paramInt == 2010)
      return "MIP_FA_REVERSE_TUNNEL_UNAVAILABLE"; 
    if (paramInt == 2011)
      return "MIP_FA_REVERSE_TUNNEL_IS_MANDATORY"; 
    if (paramInt == 2012)
      return "MIP_FA_DELIVERY_STYLE_NOT_SUPPORTED"; 
    if (paramInt == 2013)
      return "MIP_FA_MISSING_NAI"; 
    if (paramInt == 2014)
      return "MIP_FA_MISSING_HOME_AGENT"; 
    if (paramInt == 2015)
      return "MIP_FA_MISSING_HOME_ADDRESS"; 
    if (paramInt == 2016)
      return "MIP_FA_UNKNOWN_CHALLENGE"; 
    if (paramInt == 2017)
      return "MIP_FA_MISSING_CHALLENGE"; 
    if (paramInt == 2018)
      return "MIP_FA_STALE_CHALLENGE"; 
    if (paramInt == 2019)
      return "MIP_HA_REASON_UNSPECIFIED"; 
    if (paramInt == 2020)
      return "MIP_HA_ADMIN_PROHIBITED"; 
    if (paramInt == 2021)
      return "MIP_HA_INSUFFICIENT_RESOURCES"; 
    if (paramInt == 2022)
      return "MIP_HA_MOBILE_NODE_AUTHENTICATION_FAILURE"; 
    if (paramInt == 2023)
      return "MIP_HA_FOREIGN_AGENT_AUTHENTICATION_FAILURE"; 
    if (paramInt == 2024)
      return "MIP_HA_REGISTRATION_ID_MISMATCH"; 
    if (paramInt == 2025)
      return "MIP_HA_MALFORMED_REQUEST"; 
    if (paramInt == 2026)
      return "MIP_HA_UNKNOWN_HOME_AGENT_ADDRESS"; 
    if (paramInt == 2027)
      return "MIP_HA_REVERSE_TUNNEL_UNAVAILABLE"; 
    if (paramInt == 2028)
      return "MIP_HA_REVERSE_TUNNEL_IS_MANDATORY"; 
    if (paramInt == 2029)
      return "MIP_HA_ENCAPSULATION_UNAVAILABLE"; 
    if (paramInt == 2030)
      return "CLOSE_IN_PROGRESS"; 
    if (paramInt == 2031)
      return "NETWORK_INITIATED_TERMINATION"; 
    if (paramInt == 2032)
      return "MODEM_APP_PREEMPTED"; 
    if (paramInt == 2033)
      return "PDN_IPV4_CALL_DISALLOWED"; 
    if (paramInt == 2034)
      return "PDN_IPV4_CALL_THROTTLED"; 
    if (paramInt == 2035)
      return "PDN_IPV6_CALL_DISALLOWED"; 
    if (paramInt == 2036)
      return "PDN_IPV6_CALL_THROTTLED"; 
    if (paramInt == 2037)
      return "MODEM_RESTART"; 
    if (paramInt == 2038)
      return "PDP_PPP_NOT_SUPPORTED"; 
    if (paramInt == 2039)
      return "UNPREFERRED_RAT"; 
    if (paramInt == 2040)
      return "PHYSICAL_LINK_CLOSE_IN_PROGRESS"; 
    if (paramInt == 2041)
      return "APN_PENDING_HANDOVER"; 
    if (paramInt == 2042)
      return "PROFILE_BEARER_INCOMPATIBLE"; 
    if (paramInt == 2043)
      return "SIM_CARD_CHANGED"; 
    if (paramInt == 2044)
      return "LOW_POWER_MODE_OR_POWERING_DOWN"; 
    if (paramInt == 2045)
      return "APN_DISABLED"; 
    if (paramInt == 2046)
      return "MAX_PPP_INACTIVITY_TIMER_EXPIRED"; 
    if (paramInt == 2047)
      return "IPV6_ADDRESS_TRANSFER_FAILED"; 
    if (paramInt == 2048)
      return "TRAT_SWAP_FAILED"; 
    if (paramInt == 2049)
      return "EHRPD_TO_HRPD_FALLBACK"; 
    if (paramInt == 2050)
      return "MIP_CONFIG_FAILURE"; 
    if (paramInt == 2051)
      return "PDN_INACTIVITY_TIMER_EXPIRED"; 
    if (paramInt == 2052)
      return "MAX_IPV4_CONNECTIONS"; 
    if (paramInt == 2053)
      return "MAX_IPV6_CONNECTIONS"; 
    if (paramInt == 2054)
      return "APN_MISMATCH"; 
    if (paramInt == 2055)
      return "IP_VERSION_MISMATCH"; 
    if (paramInt == 2056)
      return "DUN_CALL_DISALLOWED"; 
    if (paramInt == 2057)
      return "INTERNAL_EPC_NONEPC_TRANSITION"; 
    if (paramInt == 2058)
      return "INTERFACE_IN_USE"; 
    if (paramInt == 2059)
      return "APN_DISALLOWED_ON_ROAMING"; 
    if (paramInt == 2060)
      return "APN_PARAMETERS_CHANGED"; 
    if (paramInt == 2061)
      return "NULL_APN_DISALLOWED"; 
    if (paramInt == 2062)
      return "THERMAL_MITIGATION"; 
    if (paramInt == 2063)
      return "DATA_SETTINGS_DISABLED"; 
    if (paramInt == 2064)
      return "DATA_ROAMING_SETTINGS_DISABLED"; 
    if (paramInt == 2065)
      return "DDS_SWITCHED"; 
    if (paramInt == 2066)
      return "FORBIDDEN_APN_NAME"; 
    if (paramInt == 2067)
      return "DDS_SWITCH_IN_PROGRESS"; 
    if (paramInt == 2068)
      return "CALL_DISALLOWED_IN_ROAMING"; 
    if (paramInt == 2069)
      return "NON_IP_NOT_SUPPORTED"; 
    if (paramInt == 2070)
      return "PDN_NON_IP_CALL_THROTTLED"; 
    if (paramInt == 2071)
      return "PDN_NON_IP_CALL_DISALLOWED"; 
    if (paramInt == 2072)
      return "CDMA_LOCK"; 
    if (paramInt == 2073)
      return "CDMA_INTERCEPT"; 
    if (paramInt == 2074)
      return "CDMA_REORDER"; 
    if (paramInt == 2075)
      return "CDMA_RELEASE_DUE_TO_SO_REJECTION"; 
    if (paramInt == 2076)
      return "CDMA_INCOMING_CALL"; 
    if (paramInt == 2077)
      return "CDMA_ALERT_STOP"; 
    if (paramInt == 2078)
      return "CHANNEL_ACQUISITION_FAILURE"; 
    if (paramInt == 2079)
      return "MAX_ACCESS_PROBE"; 
    if (paramInt == 2080)
      return "CONCURRENT_SERVICE_NOT_SUPPORTED_BY_BASE_STATION"; 
    if (paramInt == 2081)
      return "NO_RESPONSE_FROM_BASE_STATION"; 
    if (paramInt == 2082)
      return "REJECTED_BY_BASE_STATION"; 
    if (paramInt == 2083)
      return "CONCURRENT_SERVICES_INCOMPATIBLE"; 
    if (paramInt == 2084)
      return "NO_CDMA_SERVICE"; 
    if (paramInt == 2085)
      return "RUIM_NOT_PRESENT"; 
    if (paramInt == 2086)
      return "CDMA_RETRY_ORDER"; 
    if (paramInt == 2087)
      return "ACCESS_BLOCK"; 
    if (paramInt == 2088)
      return "ACCESS_BLOCK_ALL"; 
    if (paramInt == 2089)
      return "IS707B_MAX_ACCESS_PROBES"; 
    if (paramInt == 2090)
      return "THERMAL_EMERGENCY"; 
    if (paramInt == 2091)
      return "CONCURRENT_SERVICES_NOT_ALLOWED"; 
    if (paramInt == 2092)
      return "INCOMING_CALL_REJECTED"; 
    if (paramInt == 2093)
      return "NO_SERVICE_ON_GATEWAY"; 
    if (paramInt == 2094)
      return "NO_GPRS_CONTEXT"; 
    if (paramInt == 2095)
      return "ILLEGAL_MS"; 
    if (paramInt == 2096)
      return "ILLEGAL_ME"; 
    if (paramInt == 2097)
      return "GPRS_SERVICES_AND_NON_GPRS_SERVICES_NOT_ALLOWED"; 
    if (paramInt == 2098)
      return "GPRS_SERVICES_NOT_ALLOWED"; 
    if (paramInt == 2099)
      return "MS_IDENTITY_CANNOT_BE_DERIVED_BY_THE_NETWORK"; 
    if (paramInt == 2100)
      return "IMPLICITLY_DETACHED"; 
    if (paramInt == 2101)
      return "PLMN_NOT_ALLOWED"; 
    if (paramInt == 2102)
      return "LOCATION_AREA_NOT_ALLOWED"; 
    if (paramInt == 2103)
      return "GPRS_SERVICES_NOT_ALLOWED_IN_THIS_PLMN"; 
    if (paramInt == 2104)
      return "PDP_DUPLICATE"; 
    if (paramInt == 2105)
      return "UE_RAT_CHANGE"; 
    if (paramInt == 2106)
      return "CONGESTION"; 
    if (paramInt == 2107)
      return "NO_PDP_CONTEXT_ACTIVATED"; 
    if (paramInt == 2108)
      return "ACCESS_CLASS_DSAC_REJECTION"; 
    if (paramInt == 2109)
      return "PDP_ACTIVATE_MAX_RETRY_FAILED"; 
    if (paramInt == 2110)
      return "RADIO_ACCESS_BEARER_FAILURE"; 
    if (paramInt == 2111)
      return "ESM_UNKNOWN_EPS_BEARER_CONTEXT"; 
    if (paramInt == 2112)
      return "DRB_RELEASED_BY_RRC"; 
    if (paramInt == 2113)
      return "CONNECTION_RELEASED"; 
    if (paramInt == 2114)
      return "EMM_DETACHED"; 
    if (paramInt == 2115)
      return "EMM_ATTACH_FAILED"; 
    if (paramInt == 2116)
      return "EMM_ATTACH_STARTED"; 
    if (paramInt == 2117)
      return "LTE_NAS_SERVICE_REQUEST_FAILED"; 
    if (paramInt == 2118)
      return "DUPLICATE_BEARER_ID"; 
    if (paramInt == 2119)
      return "ESM_COLLISION_SCENARIOS"; 
    if (paramInt == 2120)
      return "ESM_BEARER_DEACTIVATED_TO_SYNC_WITH_NETWORK"; 
    if (paramInt == 2121)
      return "ESM_NW_ACTIVATED_DED_BEARER_WITH_ID_OF_DEF_BEARER"; 
    if (paramInt == 2122)
      return "ESM_BAD_OTA_MESSAGE"; 
    if (paramInt == 2123)
      return "ESM_DOWNLOAD_SERVER_REJECTED_THE_CALL"; 
    if (paramInt == 2124)
      return "ESM_CONTEXT_TRANSFERRED_DUE_TO_IRAT"; 
    if (paramInt == 2125)
      return "DS_EXPLICIT_DEACTIVATION"; 
    if (paramInt == 2126)
      return "ESM_LOCAL_CAUSE_NONE"; 
    if (paramInt == 2127)
      return "LTE_THROTTLING_NOT_REQUIRED"; 
    if (paramInt == 2128)
      return "ACCESS_CONTROL_LIST_CHECK_FAILURE"; 
    if (paramInt == 2129)
      return "SERVICE_NOT_ALLOWED_ON_PLMN"; 
    if (paramInt == 2130)
      return "EMM_T3417_EXPIRED"; 
    if (paramInt == 2131)
      return "EMM_T3417_EXT_EXPIRED"; 
    if (paramInt == 2132)
      return "RRC_UPLINK_DATA_TRANSMISSION_FAILURE"; 
    if (paramInt == 2133)
      return "RRC_UPLINK_DELIVERY_FAILED_DUE_TO_HANDOVER"; 
    if (paramInt == 2134)
      return "RRC_UPLINK_CONNECTION_RELEASE"; 
    if (paramInt == 2135)
      return "RRC_UPLINK_RADIO_LINK_FAILURE"; 
    if (paramInt == 2136)
      return "RRC_UPLINK_ERROR_REQUEST_FROM_NAS"; 
    if (paramInt == 2137)
      return "RRC_CONNECTION_ACCESS_STRATUM_FAILURE"; 
    if (paramInt == 2138)
      return "RRC_CONNECTION_ANOTHER_PROCEDURE_IN_PROGRESS"; 
    if (paramInt == 2139)
      return "RRC_CONNECTION_ACCESS_BARRED"; 
    if (paramInt == 2140)
      return "RRC_CONNECTION_CELL_RESELECTION"; 
    if (paramInt == 2141)
      return "RRC_CONNECTION_CONFIG_FAILURE"; 
    if (paramInt == 2142)
      return "RRC_CONNECTION_TIMER_EXPIRED"; 
    if (paramInt == 2143)
      return "RRC_CONNECTION_LINK_FAILURE"; 
    if (paramInt == 2144)
      return "RRC_CONNECTION_CELL_NOT_CAMPED"; 
    if (paramInt == 2145)
      return "RRC_CONNECTION_SYSTEM_INTERVAL_FAILURE"; 
    if (paramInt == 2146)
      return "RRC_CONNECTION_REJECT_BY_NETWORK"; 
    if (paramInt == 2147)
      return "RRC_CONNECTION_NORMAL_RELEASE"; 
    if (paramInt == 2148)
      return "RRC_CONNECTION_RADIO_LINK_FAILURE"; 
    if (paramInt == 2149)
      return "RRC_CONNECTION_REESTABLISHMENT_FAILURE"; 
    if (paramInt == 2150)
      return "RRC_CONNECTION_OUT_OF_SERVICE_DURING_CELL_REGISTER"; 
    if (paramInt == 2151)
      return "RRC_CONNECTION_ABORT_REQUEST"; 
    if (paramInt == 2152)
      return "RRC_CONNECTION_SYSTEM_INFORMATION_BLOCK_READ_ERROR"; 
    if (paramInt == 2153)
      return "NETWORK_INITIATED_DETACH_WITH_AUTO_REATTACH"; 
    if (paramInt == 2154)
      return "NETWORK_INITIATED_DETACH_NO_AUTO_REATTACH"; 
    if (paramInt == 2155)
      return "ESM_PROCEDURE_TIME_OUT"; 
    if (paramInt == 2156)
      return "INVALID_CONNECTION_ID"; 
    if (paramInt == 2157)
      return "MAXIMIUM_NSAPIS_EXCEEDED"; 
    if (paramInt == 2158)
      return "INVALID_PRIMARY_NSAPI"; 
    if (paramInt == 2159)
      return "CANNOT_ENCODE_OTA_MESSAGE"; 
    if (paramInt == 2160)
      return "RADIO_ACCESS_BEARER_SETUP_FAILURE"; 
    if (paramInt == 2161)
      return "PDP_ESTABLISH_TIMEOUT_EXPIRED"; 
    if (paramInt == 2162)
      return "PDP_MODIFY_TIMEOUT_EXPIRED"; 
    if (paramInt == 2163)
      return "PDP_INACTIVE_TIMEOUT_EXPIRED"; 
    if (paramInt == 2164)
      return "PDP_LOWERLAYER_ERROR"; 
    if (paramInt == 2165)
      return "PDP_MODIFY_COLLISION"; 
    if (paramInt == 2166)
      return "MAXINUM_SIZE_OF_L2_MESSAGE_EXCEEDED"; 
    if (paramInt == 2167)
      return "NAS_REQUEST_REJECTED_BY_NETWORK"; 
    if (paramInt == 2168)
      return "RRC_CONNECTION_INVALID_REQUEST"; 
    if (paramInt == 2169)
      return "RRC_CONNECTION_TRACKING_AREA_ID_CHANGED"; 
    if (paramInt == 2170)
      return "RRC_CONNECTION_RF_UNAVAILABLE"; 
    if (paramInt == 2171)
      return "RRC_CONNECTION_ABORTED_DUE_TO_IRAT_CHANGE"; 
    if (paramInt == 2172)
      return "RRC_CONNECTION_RELEASED_SECURITY_NOT_ACTIVE"; 
    if (paramInt == 2173)
      return "RRC_CONNECTION_ABORTED_AFTER_HANDOVER"; 
    if (paramInt == 2174)
      return "RRC_CONNECTION_ABORTED_AFTER_IRAT_CELL_CHANGE"; 
    if (paramInt == 2175)
      return "RRC_CONNECTION_ABORTED_DURING_IRAT_CELL_CHANGE"; 
    if (paramInt == 2176)
      return "IMSI_UNKNOWN_IN_HOME_SUBSCRIBER_SERVER"; 
    if (paramInt == 2177)
      return "IMEI_NOT_ACCEPTED"; 
    if (paramInt == 2178)
      return "EPS_SERVICES_AND_NON_EPS_SERVICES_NOT_ALLOWED"; 
    if (paramInt == 2179)
      return "EPS_SERVICES_NOT_ALLOWED_IN_PLMN"; 
    if (paramInt == 2180)
      return "MSC_TEMPORARILY_NOT_REACHABLE"; 
    if (paramInt == 2181)
      return "CS_DOMAIN_NOT_AVAILABLE"; 
    if (paramInt == 2182)
      return "ESM_FAILURE"; 
    if (paramInt == 2183)
      return "MAC_FAILURE"; 
    if (paramInt == 2184)
      return "SYNCHRONIZATION_FAILURE"; 
    if (paramInt == 2185)
      return "UE_SECURITY_CAPABILITIES_MISMATCH"; 
    if (paramInt == 2186)
      return "SECURITY_MODE_REJECTED"; 
    if (paramInt == 2187)
      return "UNACCEPTABLE_NON_EPS_AUTHENTICATION"; 
    if (paramInt == 2188)
      return "CS_FALLBACK_CALL_ESTABLISHMENT_NOT_ALLOWED"; 
    if (paramInt == 2189)
      return "NO_EPS_BEARER_CONTEXT_ACTIVATED"; 
    if (paramInt == 2190)
      return "INVALID_EMM_STATE"; 
    if (paramInt == 2191)
      return "NAS_LAYER_FAILURE"; 
    if (paramInt == 2192)
      return "MULTIPLE_PDP_CALL_NOT_ALLOWED"; 
    if (paramInt == 2193)
      return "EMBMS_NOT_ENABLED"; 
    if (paramInt == 2194)
      return "IRAT_HANDOVER_FAILED"; 
    if (paramInt == 2195)
      return "EMBMS_REGULAR_DEACTIVATION"; 
    if (paramInt == 2196)
      return "TEST_LOOPBACK_REGULAR_DEACTIVATION"; 
    if (paramInt == 2197)
      return "LOWER_LAYER_REGISTRATION_FAILURE"; 
    if (paramInt == 2198)
      return "DATA_PLAN_EXPIRED"; 
    if (paramInt == 2199)
      return "UMTS_HANDOVER_TO_IWLAN"; 
    if (paramInt == 2200)
      return "EVDO_CONNECTION_DENY_BY_GENERAL_OR_NETWORK_BUSY"; 
    if (paramInt == 2201)
      return "EVDO_CONNECTION_DENY_BY_BILLING_OR_AUTHENTICATION_FAILURE"; 
    if (paramInt == 2202)
      return "EVDO_HDR_CHANGED"; 
    if (paramInt == 2203)
      return "EVDO_HDR_EXITED"; 
    if (paramInt == 2204)
      return "EVDO_HDR_NO_SESSION"; 
    if (paramInt == 2205)
      return "EVDO_USING_GPS_FIX_INSTEAD_OF_HDR_CALL"; 
    if (paramInt == 2206)
      return "EVDO_HDR_CONNECTION_SETUP_TIMEOUT"; 
    if (paramInt == 2207)
      return "FAILED_TO_ACQUIRE_COLOCATED_HDR"; 
    if (paramInt == 2208)
      return "OTASP_COMMIT_IN_PROGRESS"; 
    if (paramInt == 2209)
      return "NO_HYBRID_HDR_SERVICE"; 
    if (paramInt == 2210)
      return "HDR_NO_LOCK_GRANTED"; 
    if (paramInt == 2211)
      return "DBM_OR_SMS_IN_PROGRESS"; 
    if (paramInt == 2212)
      return "HDR_FADE"; 
    if (paramInt == 2213)
      return "HDR_ACCESS_FAILURE"; 
    if (paramInt == 2214)
      return "UNSUPPORTED_1X_PREV"; 
    if (paramInt == 2215)
      return "LOCAL_END"; 
    if (paramInt == 2216)
      return "NO_SERVICE"; 
    if (paramInt == 2217)
      return "FADE"; 
    if (paramInt == 2218)
      return "NORMAL_RELEASE"; 
    if (paramInt == 2219)
      return "ACCESS_ATTEMPT_ALREADY_IN_PROGRESS"; 
    if (paramInt == 2220)
      return "REDIRECTION_OR_HANDOFF_IN_PROGRESS"; 
    if (paramInt == 2221)
      return "EMERGENCY_MODE"; 
    if (paramInt == 2222)
      return "PHONE_IN_USE"; 
    if (paramInt == 2223)
      return "INVALID_MODE"; 
    if (paramInt == 2224)
      return "INVALID_SIM_STATE"; 
    if (paramInt == 2225)
      return "NO_COLLOCATED_HDR"; 
    if (paramInt == 2226)
      return "UE_IS_ENTERING_POWERSAVE_MODE"; 
    if (paramInt == 2227)
      return "DUAL_SWITCH"; 
    if (paramInt == 2228)
      return "PPP_TIMEOUT"; 
    if (paramInt == 2229)
      return "PPP_AUTH_FAILURE"; 
    if (paramInt == 2230)
      return "PPP_OPTION_MISMATCH"; 
    if (paramInt == 2231)
      return "PPP_PAP_FAILURE"; 
    if (paramInt == 2232)
      return "PPP_CHAP_FAILURE"; 
    if (paramInt == 2233)
      return "PPP_CLOSE_IN_PROGRESS"; 
    if (paramInt == 2234)
      return "LIMITED_TO_IPV4"; 
    if (paramInt == 2235)
      return "LIMITED_TO_IPV6"; 
    if (paramInt == 2236)
      return "VSNCP_TIMEOUT"; 
    if (paramInt == 2237)
      return "VSNCP_GEN_ERROR"; 
    if (paramInt == 2238)
      return "VSNCP_APN_UNATHORIZED"; 
    if (paramInt == 2239)
      return "VSNCP_PDN_LIMIT_EXCEEDED"; 
    if (paramInt == 2240)
      return "VSNCP_NO_PDN_GATEWAY_ADDRESS"; 
    if (paramInt == 2241)
      return "VSNCP_PDN_GATEWAY_UNREACHABLE"; 
    if (paramInt == 2242)
      return "VSNCP_PDN_GATEWAY_REJECT"; 
    if (paramInt == 2243)
      return "VSNCP_INSUFFICIENT_PARAMETERS"; 
    if (paramInt == 2244)
      return "VSNCP_RESOURCE_UNAVAILABLE"; 
    if (paramInt == 2245)
      return "VSNCP_ADMINISTRATIVELY_PROHIBITED"; 
    if (paramInt == 2246)
      return "VSNCP_PDN_ID_IN_USE"; 
    if (paramInt == 2247)
      return "VSNCP_SUBSCRIBER_LIMITATION"; 
    if (paramInt == 2248)
      return "VSNCP_PDN_EXISTS_FOR_THIS_APN"; 
    if (paramInt == 2249)
      return "VSNCP_RECONNECT_NOT_ALLOWED"; 
    if (paramInt == 2250)
      return "IPV6_PREFIX_UNAVAILABLE"; 
    if (paramInt == 2251)
      return "HANDOFF_PREFERENCE_CHANGED"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    arrayList.add("NONE");
    if ((paramInt & 0x8) == 8) {
      arrayList.add("OPERATOR_BARRED");
      i = 0x0 | 0x8;
    } 
    int j = i;
    if ((paramInt & 0xE) == 14) {
      arrayList.add("NAS_SIGNALLING");
      j = i | 0xE;
    } 
    i = j;
    if ((paramInt & 0x1A) == 26) {
      arrayList.add("INSUFFICIENT_RESOURCES");
      i = j | 0x1A;
    } 
    j = i;
    if ((paramInt & 0x1B) == 27) {
      arrayList.add("MISSING_UKNOWN_APN");
      j = i | 0x1B;
    } 
    i = j;
    if ((paramInt & 0x1C) == 28) {
      arrayList.add("UNKNOWN_PDP_ADDRESS_TYPE");
      i = j | 0x1C;
    } 
    int k = i;
    if ((paramInt & 0x1D) == 29) {
      arrayList.add("USER_AUTHENTICATION");
      k = i | 0x1D;
    } 
    j = k;
    if ((paramInt & 0x1E) == 30) {
      arrayList.add("ACTIVATION_REJECT_GGSN");
      j = k | 0x1E;
    } 
    i = j;
    if ((paramInt & 0x1F) == 31) {
      arrayList.add("ACTIVATION_REJECT_UNSPECIFIED");
      i = j | 0x1F;
    } 
    j = i;
    if ((paramInt & 0x20) == 32) {
      arrayList.add("SERVICE_OPTION_NOT_SUPPORTED");
      j = i | 0x20;
    } 
    i = j;
    if ((paramInt & 0x21) == 33) {
      arrayList.add("SERVICE_OPTION_NOT_SUBSCRIBED");
      i = j | 0x21;
    } 
    j = i;
    if ((paramInt & 0x22) == 34) {
      arrayList.add("SERVICE_OPTION_OUT_OF_ORDER");
      j = i | 0x22;
    } 
    k = j;
    if ((paramInt & 0x23) == 35) {
      arrayList.add("NSAPI_IN_USE");
      k = j | 0x23;
    } 
    i = k;
    if ((paramInt & 0x24) == 36) {
      arrayList.add("REGULAR_DEACTIVATION");
      i = k | 0x24;
    } 
    j = i;
    if ((paramInt & 0x25) == 37) {
      arrayList.add("QOS_NOT_ACCEPTED");
      j = i | 0x25;
    } 
    i = j;
    if ((paramInt & 0x26) == 38) {
      arrayList.add("NETWORK_FAILURE");
      i = j | 0x26;
    } 
    k = i;
    if ((paramInt & 0x27) == 39) {
      arrayList.add("UMTS_REACTIVATION_REQ");
      k = i | 0x27;
    } 
    j = k;
    if ((paramInt & 0x28) == 40) {
      arrayList.add("FEATURE_NOT_SUPP");
      j = k | 0x28;
    } 
    i = j;
    if ((paramInt & 0x29) == 41) {
      arrayList.add("TFT_SEMANTIC_ERROR");
      i = j | 0x29;
    } 
    j = i;
    if ((paramInt & 0x2A) == 42) {
      arrayList.add("TFT_SYTAX_ERROR");
      j = i | 0x2A;
    } 
    i = j;
    if ((paramInt & 0x2B) == 43) {
      arrayList.add("UNKNOWN_PDP_CONTEXT");
      i = j | 0x2B;
    } 
    j = i;
    if ((paramInt & 0x2C) == 44) {
      arrayList.add("FILTER_SEMANTIC_ERROR");
      j = i | 0x2C;
    } 
    i = j;
    if ((paramInt & 0x2D) == 45) {
      arrayList.add("FILTER_SYTAX_ERROR");
      i = j | 0x2D;
    } 
    j = i;
    if ((paramInt & 0x2E) == 46) {
      arrayList.add("PDP_WITHOUT_ACTIVE_TFT");
      j = i | 0x2E;
    } 
    i = j;
    if ((paramInt & 0x32) == 50) {
      arrayList.add("ONLY_IPV4_ALLOWED");
      i = j | 0x32;
    } 
    j = i;
    if ((paramInt & 0x33) == 51) {
      arrayList.add("ONLY_IPV6_ALLOWED");
      j = i | 0x33;
    } 
    i = j;
    if ((paramInt & 0x34) == 52) {
      arrayList.add("ONLY_SINGLE_BEARER_ALLOWED");
      i = j | 0x34;
    } 
    j = i;
    if ((paramInt & 0x35) == 53) {
      arrayList.add("ESM_INFO_NOT_RECEIVED");
      j = i | 0x35;
    } 
    k = j;
    if ((paramInt & 0x36) == 54) {
      arrayList.add("PDN_CONN_DOES_NOT_EXIST");
      k = j | 0x36;
    } 
    i = k;
    if ((paramInt & 0x37) == 55) {
      arrayList.add("MULTI_CONN_TO_SAME_PDN_NOT_ALLOWED");
      i = k | 0x37;
    } 
    j = i;
    if ((paramInt & 0x41) == 65) {
      arrayList.add("MAX_ACTIVE_PDP_CONTEXT_REACHED");
      j = i | 0x41;
    } 
    i = j;
    if ((paramInt & 0x42) == 66) {
      arrayList.add("UNSUPPORTED_APN_IN_CURRENT_PLMN");
      i = j | 0x42;
    } 
    j = i;
    if ((paramInt & 0x51) == 81) {
      arrayList.add("INVALID_TRANSACTION_ID");
      j = i | 0x51;
    } 
    i = j;
    if ((paramInt & 0x5F) == 95) {
      arrayList.add("MESSAGE_INCORRECT_SEMANTIC");
      i = j | 0x5F;
    } 
    j = i;
    if ((paramInt & 0x60) == 96) {
      arrayList.add("INVALID_MANDATORY_INFO");
      j = i | 0x60;
    } 
    i = j;
    if ((paramInt & 0x61) == 97) {
      arrayList.add("MESSAGE_TYPE_UNSUPPORTED");
      i = j | 0x61;
    } 
    j = i;
    if ((paramInt & 0x62) == 98) {
      arrayList.add("MSG_TYPE_NONCOMPATIBLE_STATE");
      j = i | 0x62;
    } 
    i = j;
    if ((paramInt & 0x63) == 99) {
      arrayList.add("UNKNOWN_INFO_ELEMENT");
      i = j | 0x63;
    } 
    j = i;
    if ((paramInt & 0x64) == 100) {
      arrayList.add("CONDITIONAL_IE_ERROR");
      j = i | 0x64;
    } 
    i = j;
    if ((paramInt & 0x65) == 101) {
      arrayList.add("MSG_AND_PROTOCOL_STATE_UNCOMPATIBLE");
      i = j | 0x65;
    } 
    j = i;
    if ((paramInt & 0x6F) == 111) {
      arrayList.add("PROTOCOL_ERRORS");
      j = i | 0x6F;
    } 
    i = j;
    if ((paramInt & 0x70) == 112) {
      arrayList.add("APN_TYPE_CONFLICT");
      i = j | 0x70;
    } 
    k = i;
    if ((paramInt & 0x71) == 113) {
      arrayList.add("INVALID_PCSCF_ADDR");
      k = i | 0x71;
    } 
    j = k;
    if ((paramInt & 0x72) == 114) {
      arrayList.add("INTERNAL_CALL_PREEMPT_BY_HIGH_PRIO_APN");
      j = k | 0x72;
    } 
    k = j;
    if ((paramInt & 0x73) == 115) {
      arrayList.add("EMM_ACCESS_BARRED");
      k = j | 0x73;
    } 
    i = k;
    if ((paramInt & 0x74) == 116) {
      arrayList.add("EMERGENCY_IFACE_ONLY");
      i = k | 0x74;
    } 
    j = i;
    if ((paramInt & 0x75) == 117) {
      arrayList.add("IFACE_MISMATCH");
      j = i | 0x75;
    } 
    i = j;
    if ((paramInt & 0x76) == 118) {
      arrayList.add("COMPANION_IFACE_IN_USE");
      i = j | 0x76;
    } 
    j = i;
    if ((paramInt & 0x77) == 119) {
      arrayList.add("IP_ADDRESS_MISMATCH");
      j = i | 0x77;
    } 
    i = j;
    if ((paramInt & 0x78) == 120) {
      arrayList.add("IFACE_AND_POL_FAMILY_MISMATCH");
      i = j | 0x78;
    } 
    j = i;
    if ((paramInt & 0x79) == 121) {
      arrayList.add("EMM_ACCESS_BARRED_INFINITE_RETRY");
      j = i | 0x79;
    } 
    i = j;
    if ((paramInt & 0x7A) == 122) {
      arrayList.add("AUTH_FAILURE_ON_EMERGENCY_CALL");
      i = j | 0x7A;
    } 
    j = i;
    if ((paramInt & 0x1001) == 4097) {
      arrayList.add("OEM_DCFAILCAUSE_1");
      j = i | 0x1001;
    } 
    i = j;
    if ((paramInt & 0x1002) == 4098) {
      arrayList.add("OEM_DCFAILCAUSE_2");
      i = j | 0x1002;
    } 
    j = i;
    if ((paramInt & 0x1003) == 4099) {
      arrayList.add("OEM_DCFAILCAUSE_3");
      j = i | 0x1003;
    } 
    i = j;
    if ((paramInt & 0x1004) == 4100) {
      arrayList.add("OEM_DCFAILCAUSE_4");
      i = j | 0x1004;
    } 
    j = i;
    if ((paramInt & 0x1005) == 4101) {
      arrayList.add("OEM_DCFAILCAUSE_5");
      j = i | 0x1005;
    } 
    i = j;
    if ((paramInt & 0x1006) == 4102) {
      arrayList.add("OEM_DCFAILCAUSE_6");
      i = j | 0x1006;
    } 
    j = i;
    if ((paramInt & 0x1007) == 4103) {
      arrayList.add("OEM_DCFAILCAUSE_7");
      j = i | 0x1007;
    } 
    i = j;
    if ((paramInt & 0x1008) == 4104) {
      arrayList.add("OEM_DCFAILCAUSE_8");
      i = j | 0x1008;
    } 
    k = i;
    if ((paramInt & 0x1009) == 4105) {
      arrayList.add("OEM_DCFAILCAUSE_9");
      k = i | 0x1009;
    } 
    j = k;
    if ((paramInt & 0x100A) == 4106) {
      arrayList.add("OEM_DCFAILCAUSE_10");
      j = k | 0x100A;
    } 
    k = j;
    if ((paramInt & 0x100B) == 4107) {
      arrayList.add("OEM_DCFAILCAUSE_11");
      k = j | 0x100B;
    } 
    i = k;
    if ((paramInt & 0x100C) == 4108) {
      arrayList.add("OEM_DCFAILCAUSE_12");
      i = k | 0x100C;
    } 
    j = i;
    if ((paramInt & 0x100D) == 4109) {
      arrayList.add("OEM_DCFAILCAUSE_13");
      j = i | 0x100D;
    } 
    i = j;
    if ((paramInt & 0x100E) == 4110) {
      arrayList.add("OEM_DCFAILCAUSE_14");
      i = j | 0x100E;
    } 
    j = i;
    if ((paramInt & 0x100F) == 4111) {
      arrayList.add("OEM_DCFAILCAUSE_15");
      j = i | 0x100F;
    } 
    i = j;
    if ((paramInt & 0xFFFFFFFF) == -1) {
      arrayList.add("VOICE_REGISTRATION_FAIL");
      i = j | 0xFFFFFFFF;
    } 
    j = i;
    if ((paramInt & 0xFFFFFFFE) == -2) {
      arrayList.add("DATA_REGISTRATION_FAIL");
      j = i | 0xFFFFFFFE;
    } 
    i = j;
    if ((paramInt & 0xFFFFFFFD) == -3) {
      arrayList.add("SIGNAL_LOST");
      i = j | 0xFFFFFFFD;
    } 
    j = i;
    if ((paramInt & 0xFFFFFFFC) == -4) {
      arrayList.add("PREF_RADIO_TECH_CHANGED");
      j = i | 0xFFFFFFFC;
    } 
    i = j;
    if ((paramInt & 0xFFFFFFFB) == -5) {
      arrayList.add("RADIO_POWER_OFF");
      i = j | 0xFFFFFFFB;
    } 
    j = i;
    if ((paramInt & 0xFFFFFFFA) == -6) {
      arrayList.add("TETHERED_CALL_ACTIVE");
      j = i | 0xFFFFFFFA;
    } 
    i = j;
    if ((0xFFFF & paramInt) == 65535) {
      arrayList.add("ERROR_UNSPECIFIED");
      i = j | 0xFFFF;
    } 
    j = i;
    if ((paramInt & 0x19) == 25) {
      arrayList.add("LLC_SNDCP");
      j = i | 0x19;
    } 
    i = j;
    if ((paramInt & 0x30) == 48) {
      arrayList.add("ACTIVATION_REJECTED_BCM_VIOLATION");
      i = j | 0x30;
    } 
    j = i;
    if ((paramInt & 0x38) == 56) {
      arrayList.add("COLLISION_WITH_NETWORK_INITIATED_REQUEST");
      j = i | 0x38;
    } 
    k = j;
    if ((paramInt & 0x39) == 57) {
      arrayList.add("ONLY_IPV4V6_ALLOWED");
      k = j | 0x39;
    } 
    i = k;
    if ((paramInt & 0x3A) == 58) {
      arrayList.add("ONLY_NON_IP_ALLOWED");
      i = k | 0x3A;
    } 
    j = i;
    if ((paramInt & 0x3B) == 59) {
      arrayList.add("UNSUPPORTED_QCI_VALUE");
      j = i | 0x3B;
    } 
    i = j;
    if ((paramInt & 0x3C) == 60) {
      arrayList.add("BEARER_HANDLING_NOT_SUPPORTED");
      i = j | 0x3C;
    } 
    j = i;
    if ((paramInt & 0x7B) == 123) {
      arrayList.add("INVALID_DNS_ADDR");
      j = i | 0x7B;
    } 
    i = j;
    if ((paramInt & 0x7C) == 124) {
      arrayList.add("INVALID_PCSCF_OR_DNS_ADDRESS");
      i = j | 0x7C;
    } 
    j = i;
    if ((paramInt & 0x7F) == 127) {
      arrayList.add("CALL_PREEMPT_BY_EMERGENCY_APN");
      j = i | 0x7F;
    } 
    i = j;
    if ((paramInt & 0x80) == 128) {
      arrayList.add("UE_INITIATED_DETACH_OR_DISCONNECT");
      i = j | 0x80;
    } 
    j = i;
    if ((paramInt & 0x7D0) == 2000) {
      arrayList.add("MIP_FA_REASON_UNSPECIFIED");
      j = i | 0x7D0;
    } 
    i = j;
    if ((paramInt & 0x7D1) == 2001) {
      arrayList.add("MIP_FA_ADMIN_PROHIBITED");
      i = j | 0x7D1;
    } 
    j = i;
    if ((paramInt & 0x7D2) == 2002) {
      arrayList.add("MIP_FA_INSUFFICIENT_RESOURCES");
      j = i | 0x7D2;
    } 
    k = j;
    if ((paramInt & 0x7D3) == 2003) {
      arrayList.add("MIP_FA_MOBILE_NODE_AUTHENTICATION_FAILURE");
      k = j | 0x7D3;
    } 
    i = k;
    if ((paramInt & 0x7D4) == 2004) {
      arrayList.add("MIP_FA_HOME_AGENT_AUTHENTICATION_FAILURE");
      i = k | 0x7D4;
    } 
    j = i;
    if ((paramInt & 0x7D5) == 2005) {
      arrayList.add("MIP_FA_REQUESTED_LIFETIME_TOO_LONG");
      j = i | 0x7D5;
    } 
    i = j;
    if ((paramInt & 0x7D6) == 2006) {
      arrayList.add("MIP_FA_MALFORMED_REQUEST");
      i = j | 0x7D6;
    } 
    j = i;
    if ((paramInt & 0x7D7) == 2007) {
      arrayList.add("MIP_FA_MALFORMED_REPLY");
      j = i | 0x7D7;
    } 
    k = j;
    if ((paramInt & 0x7D8) == 2008) {
      arrayList.add("MIP_FA_ENCAPSULATION_UNAVAILABLE");
      k = j | 0x7D8;
    } 
    i = k;
    if ((paramInt & 0x7D9) == 2009) {
      arrayList.add("MIP_FA_VJ_HEADER_COMPRESSION_UNAVAILABLE");
      i = k | 0x7D9;
    } 
    j = i;
    if ((paramInt & 0x7DA) == 2010) {
      arrayList.add("MIP_FA_REVERSE_TUNNEL_UNAVAILABLE");
      j = i | 0x7DA;
    } 
    k = j;
    if ((paramInt & 0x7DB) == 2011) {
      arrayList.add("MIP_FA_REVERSE_TUNNEL_IS_MANDATORY");
      k = j | 0x7DB;
    } 
    i = k;
    if ((paramInt & 0x7DC) == 2012) {
      arrayList.add("MIP_FA_DELIVERY_STYLE_NOT_SUPPORTED");
      i = k | 0x7DC;
    } 
    j = i;
    if ((paramInt & 0x7DD) == 2013) {
      arrayList.add("MIP_FA_MISSING_NAI");
      j = i | 0x7DD;
    } 
    i = j;
    if ((paramInt & 0x7DE) == 2014) {
      arrayList.add("MIP_FA_MISSING_HOME_AGENT");
      i = j | 0x7DE;
    } 
    j = i;
    if ((paramInt & 0x7DF) == 2015) {
      arrayList.add("MIP_FA_MISSING_HOME_ADDRESS");
      j = i | 0x7DF;
    } 
    i = j;
    if ((paramInt & 0x7E0) == 2016) {
      arrayList.add("MIP_FA_UNKNOWN_CHALLENGE");
      i = j | 0x7E0;
    } 
    j = i;
    if ((paramInt & 0x7E1) == 2017) {
      arrayList.add("MIP_FA_MISSING_CHALLENGE");
      j = i | 0x7E1;
    } 
    i = j;
    if ((paramInt & 0x7E2) == 2018) {
      arrayList.add("MIP_FA_STALE_CHALLENGE");
      i = j | 0x7E2;
    } 
    j = i;
    if ((paramInt & 0x7E3) == 2019) {
      arrayList.add("MIP_HA_REASON_UNSPECIFIED");
      j = i | 0x7E3;
    } 
    i = j;
    if ((paramInt & 0x7E4) == 2020) {
      arrayList.add("MIP_HA_ADMIN_PROHIBITED");
      i = j | 0x7E4;
    } 
    j = i;
    if ((paramInt & 0x7E5) == 2021) {
      arrayList.add("MIP_HA_INSUFFICIENT_RESOURCES");
      j = i | 0x7E5;
    } 
    i = j;
    if ((paramInt & 0x7E6) == 2022) {
      arrayList.add("MIP_HA_MOBILE_NODE_AUTHENTICATION_FAILURE");
      i = j | 0x7E6;
    } 
    j = i;
    if ((paramInt & 0x7E7) == 2023) {
      arrayList.add("MIP_HA_FOREIGN_AGENT_AUTHENTICATION_FAILURE");
      j = i | 0x7E7;
    } 
    i = j;
    if ((paramInt & 0x7E8) == 2024) {
      arrayList.add("MIP_HA_REGISTRATION_ID_MISMATCH");
      i = j | 0x7E8;
    } 
    j = i;
    if ((paramInt & 0x7E9) == 2025) {
      arrayList.add("MIP_HA_MALFORMED_REQUEST");
      j = i | 0x7E9;
    } 
    i = j;
    if ((paramInt & 0x7EA) == 2026) {
      arrayList.add("MIP_HA_UNKNOWN_HOME_AGENT_ADDRESS");
      i = j | 0x7EA;
    } 
    j = i;
    if ((paramInt & 0x7EB) == 2027) {
      arrayList.add("MIP_HA_REVERSE_TUNNEL_UNAVAILABLE");
      j = i | 0x7EB;
    } 
    i = j;
    if ((paramInt & 0x7EC) == 2028) {
      arrayList.add("MIP_HA_REVERSE_TUNNEL_IS_MANDATORY");
      i = j | 0x7EC;
    } 
    k = i;
    if ((paramInt & 0x7ED) == 2029) {
      arrayList.add("MIP_HA_ENCAPSULATION_UNAVAILABLE");
      k = i | 0x7ED;
    } 
    j = k;
    if ((paramInt & 0x7EE) == 2030) {
      arrayList.add("CLOSE_IN_PROGRESS");
      j = k | 0x7EE;
    } 
    k = j;
    if ((paramInt & 0x7EF) == 2031) {
      arrayList.add("NETWORK_INITIATED_TERMINATION");
      k = j | 0x7EF;
    } 
    i = k;
    if ((paramInt & 0x7F0) == 2032) {
      arrayList.add("MODEM_APP_PREEMPTED");
      i = k | 0x7F0;
    } 
    j = i;
    if ((paramInt & 0x7F1) == 2033) {
      arrayList.add("PDN_IPV4_CALL_DISALLOWED");
      j = i | 0x7F1;
    } 
    i = j;
    if ((paramInt & 0x7F2) == 2034) {
      arrayList.add("PDN_IPV4_CALL_THROTTLED");
      i = j | 0x7F2;
    } 
    k = i;
    if ((paramInt & 0x7F3) == 2035) {
      arrayList.add("PDN_IPV6_CALL_DISALLOWED");
      k = i | 0x7F3;
    } 
    j = k;
    if ((paramInt & 0x7F4) == 2036) {
      arrayList.add("PDN_IPV6_CALL_THROTTLED");
      j = k | 0x7F4;
    } 
    i = j;
    if ((paramInt & 0x7F5) == 2037) {
      arrayList.add("MODEM_RESTART");
      i = j | 0x7F5;
    } 
    j = i;
    if ((paramInt & 0x7F6) == 2038) {
      arrayList.add("PDP_PPP_NOT_SUPPORTED");
      j = i | 0x7F6;
    } 
    i = j;
    if ((paramInt & 0x7F7) == 2039) {
      arrayList.add("UNPREFERRED_RAT");
      i = j | 0x7F7;
    } 
    j = i;
    if ((paramInt & 0x7F8) == 2040) {
      arrayList.add("PHYSICAL_LINK_CLOSE_IN_PROGRESS");
      j = i | 0x7F8;
    } 
    i = j;
    if ((paramInt & 0x7F9) == 2041) {
      arrayList.add("APN_PENDING_HANDOVER");
      i = j | 0x7F9;
    } 
    k = i;
    if ((paramInt & 0x7FA) == 2042) {
      arrayList.add("PROFILE_BEARER_INCOMPATIBLE");
      k = i | 0x7FA;
    } 
    j = k;
    if ((paramInt & 0x7FB) == 2043) {
      arrayList.add("SIM_CARD_CHANGED");
      j = k | 0x7FB;
    } 
    i = j;
    if ((paramInt & 0x7FC) == 2044) {
      arrayList.add("LOW_POWER_MODE_OR_POWERING_DOWN");
      i = j | 0x7FC;
    } 
    j = i;
    if ((paramInt & 0x7FD) == 2045) {
      arrayList.add("APN_DISABLED");
      j = i | 0x7FD;
    } 
    i = j;
    if ((paramInt & 0x7FE) == 2046) {
      arrayList.add("MAX_PPP_INACTIVITY_TIMER_EXPIRED");
      i = j | 0x7FE;
    } 
    j = i;
    if ((paramInt & 0x7FF) == 2047) {
      arrayList.add("IPV6_ADDRESS_TRANSFER_FAILED");
      j = i | 0x7FF;
    } 
    i = j;
    if ((paramInt & 0x800) == 2048) {
      arrayList.add("TRAT_SWAP_FAILED");
      i = j | 0x800;
    } 
    j = i;
    if ((paramInt & 0x801) == 2049) {
      arrayList.add("EHRPD_TO_HRPD_FALLBACK");
      j = i | 0x801;
    } 
    i = j;
    if ((paramInt & 0x802) == 2050) {
      arrayList.add("MIP_CONFIG_FAILURE");
      i = j | 0x802;
    } 
    j = i;
    if ((paramInt & 0x803) == 2051) {
      arrayList.add("PDN_INACTIVITY_TIMER_EXPIRED");
      j = i | 0x803;
    } 
    i = j;
    if ((paramInt & 0x804) == 2052) {
      arrayList.add("MAX_IPV4_CONNECTIONS");
      i = j | 0x804;
    } 
    j = i;
    if ((paramInt & 0x805) == 2053) {
      arrayList.add("MAX_IPV6_CONNECTIONS");
      j = i | 0x805;
    } 
    i = j;
    if ((paramInt & 0x806) == 2054) {
      arrayList.add("APN_MISMATCH");
      i = j | 0x806;
    } 
    j = i;
    if ((paramInt & 0x807) == 2055) {
      arrayList.add("IP_VERSION_MISMATCH");
      j = i | 0x807;
    } 
    k = j;
    if ((paramInt & 0x808) == 2056) {
      arrayList.add("DUN_CALL_DISALLOWED");
      k = j | 0x808;
    } 
    i = k;
    if ((paramInt & 0x809) == 2057) {
      arrayList.add("INTERNAL_EPC_NONEPC_TRANSITION");
      i = k | 0x809;
    } 
    j = i;
    if ((paramInt & 0x80A) == 2058) {
      arrayList.add("INTERFACE_IN_USE");
      j = i | 0x80A;
    } 
    i = j;
    if ((paramInt & 0x80B) == 2059) {
      arrayList.add("APN_DISALLOWED_ON_ROAMING");
      i = j | 0x80B;
    } 
    j = i;
    if ((paramInt & 0x80C) == 2060) {
      arrayList.add("APN_PARAMETERS_CHANGED");
      j = i | 0x80C;
    } 
    i = j;
    if ((paramInt & 0x80D) == 2061) {
      arrayList.add("NULL_APN_DISALLOWED");
      i = j | 0x80D;
    } 
    j = i;
    if ((paramInt & 0x80E) == 2062) {
      arrayList.add("THERMAL_MITIGATION");
      j = i | 0x80E;
    } 
    k = j;
    if ((paramInt & 0x80F) == 2063) {
      arrayList.add("DATA_SETTINGS_DISABLED");
      k = j | 0x80F;
    } 
    i = k;
    if ((paramInt & 0x810) == 2064) {
      arrayList.add("DATA_ROAMING_SETTINGS_DISABLED");
      i = k | 0x810;
    } 
    j = i;
    if ((paramInt & 0x811) == 2065) {
      arrayList.add("DDS_SWITCHED");
      j = i | 0x811;
    } 
    i = j;
    if ((paramInt & 0x812) == 2066) {
      arrayList.add("FORBIDDEN_APN_NAME");
      i = j | 0x812;
    } 
    j = i;
    if ((paramInt & 0x813) == 2067) {
      arrayList.add("DDS_SWITCH_IN_PROGRESS");
      j = i | 0x813;
    } 
    i = j;
    if ((paramInt & 0x814) == 2068) {
      arrayList.add("CALL_DISALLOWED_IN_ROAMING");
      i = j | 0x814;
    } 
    j = i;
    if ((paramInt & 0x815) == 2069) {
      arrayList.add("NON_IP_NOT_SUPPORTED");
      j = i | 0x815;
    } 
    i = j;
    if ((paramInt & 0x816) == 2070) {
      arrayList.add("PDN_NON_IP_CALL_THROTTLED");
      i = j | 0x816;
    } 
    k = i;
    if ((paramInt & 0x817) == 2071) {
      arrayList.add("PDN_NON_IP_CALL_DISALLOWED");
      k = i | 0x817;
    } 
    j = k;
    if ((paramInt & 0x818) == 2072) {
      arrayList.add("CDMA_LOCK");
      j = k | 0x818;
    } 
    i = j;
    if ((paramInt & 0x819) == 2073) {
      arrayList.add("CDMA_INTERCEPT");
      i = j | 0x819;
    } 
    k = i;
    if ((paramInt & 0x81A) == 2074) {
      arrayList.add("CDMA_REORDER");
      k = i | 0x81A;
    } 
    j = k;
    if ((paramInt & 0x81B) == 2075) {
      arrayList.add("CDMA_RELEASE_DUE_TO_SO_REJECTION");
      j = k | 0x81B;
    } 
    i = j;
    if ((paramInt & 0x81C) == 2076) {
      arrayList.add("CDMA_INCOMING_CALL");
      i = j | 0x81C;
    } 
    j = i;
    if ((paramInt & 0x81D) == 2077) {
      arrayList.add("CDMA_ALERT_STOP");
      j = i | 0x81D;
    } 
    i = j;
    if ((paramInt & 0x81E) == 2078) {
      arrayList.add("CHANNEL_ACQUISITION_FAILURE");
      i = j | 0x81E;
    } 
    j = i;
    if ((paramInt & 0x81F) == 2079) {
      arrayList.add("MAX_ACCESS_PROBE");
      j = i | 0x81F;
    } 
    i = j;
    if ((paramInt & 0x820) == 2080) {
      arrayList.add("CONCURRENT_SERVICE_NOT_SUPPORTED_BY_BASE_STATION");
      i = j | 0x820;
    } 
    k = i;
    if ((paramInt & 0x821) == 2081) {
      arrayList.add("NO_RESPONSE_FROM_BASE_STATION");
      k = i | 0x821;
    } 
    j = k;
    if ((paramInt & 0x822) == 2082) {
      arrayList.add("REJECTED_BY_BASE_STATION");
      j = k | 0x822;
    } 
    i = j;
    if ((paramInt & 0x823) == 2083) {
      arrayList.add("CONCURRENT_SERVICES_INCOMPATIBLE");
      i = j | 0x823;
    } 
    j = i;
    if ((paramInt & 0x824) == 2084) {
      arrayList.add("NO_CDMA_SERVICE");
      j = i | 0x824;
    } 
    i = j;
    if ((paramInt & 0x825) == 2085) {
      arrayList.add("RUIM_NOT_PRESENT");
      i = j | 0x825;
    } 
    j = i;
    if ((paramInt & 0x826) == 2086) {
      arrayList.add("CDMA_RETRY_ORDER");
      j = i | 0x826;
    } 
    i = j;
    if ((paramInt & 0x827) == 2087) {
      arrayList.add("ACCESS_BLOCK");
      i = j | 0x827;
    } 
    j = i;
    if ((paramInt & 0x828) == 2088) {
      arrayList.add("ACCESS_BLOCK_ALL");
      j = i | 0x828;
    } 
    i = j;
    if ((paramInt & 0x829) == 2089) {
      arrayList.add("IS707B_MAX_ACCESS_PROBES");
      i = j | 0x829;
    } 
    j = i;
    if ((paramInt & 0x82A) == 2090) {
      arrayList.add("THERMAL_EMERGENCY");
      j = i | 0x82A;
    } 
    i = j;
    if ((paramInt & 0x82B) == 2091) {
      arrayList.add("CONCURRENT_SERVICES_NOT_ALLOWED");
      i = j | 0x82B;
    } 
    j = i;
    if ((paramInt & 0x82C) == 2092) {
      arrayList.add("INCOMING_CALL_REJECTED");
      j = i | 0x82C;
    } 
    i = j;
    if ((paramInt & 0x82D) == 2093) {
      arrayList.add("NO_SERVICE_ON_GATEWAY");
      i = j | 0x82D;
    } 
    j = i;
    if ((paramInt & 0x82E) == 2094) {
      arrayList.add("NO_GPRS_CONTEXT");
      j = i | 0x82E;
    } 
    i = j;
    if ((paramInt & 0x82F) == 2095) {
      arrayList.add("ILLEGAL_MS");
      i = j | 0x82F;
    } 
    j = i;
    if ((paramInt & 0x830) == 2096) {
      arrayList.add("ILLEGAL_ME");
      j = i | 0x830;
    } 
    i = j;
    if ((paramInt & 0x831) == 2097) {
      arrayList.add("GPRS_SERVICES_AND_NON_GPRS_SERVICES_NOT_ALLOWED");
      i = j | 0x831;
    } 
    j = i;
    if ((paramInt & 0x832) == 2098) {
      arrayList.add("GPRS_SERVICES_NOT_ALLOWED");
      j = i | 0x832;
    } 
    i = j;
    if ((paramInt & 0x833) == 2099) {
      arrayList.add("MS_IDENTITY_CANNOT_BE_DERIVED_BY_THE_NETWORK");
      i = j | 0x833;
    } 
    j = i;
    if ((paramInt & 0x834) == 2100) {
      arrayList.add("IMPLICITLY_DETACHED");
      j = i | 0x834;
    } 
    k = j;
    if ((paramInt & 0x835) == 2101) {
      arrayList.add("PLMN_NOT_ALLOWED");
      k = j | 0x835;
    } 
    i = k;
    if ((paramInt & 0x836) == 2102) {
      arrayList.add("LOCATION_AREA_NOT_ALLOWED");
      i = k | 0x836;
    } 
    j = i;
    if ((paramInt & 0x837) == 2103) {
      arrayList.add("GPRS_SERVICES_NOT_ALLOWED_IN_THIS_PLMN");
      j = i | 0x837;
    } 
    i = j;
    if ((paramInt & 0x838) == 2104) {
      arrayList.add("PDP_DUPLICATE");
      i = j | 0x838;
    } 
    j = i;
    if ((paramInt & 0x839) == 2105) {
      arrayList.add("UE_RAT_CHANGE");
      j = i | 0x839;
    } 
    i = j;
    if ((paramInt & 0x83A) == 2106) {
      arrayList.add("CONGESTION");
      i = j | 0x83A;
    } 
    j = i;
    if ((paramInt & 0x83B) == 2107) {
      arrayList.add("NO_PDP_CONTEXT_ACTIVATED");
      j = i | 0x83B;
    } 
    i = j;
    if ((paramInt & 0x83C) == 2108) {
      arrayList.add("ACCESS_CLASS_DSAC_REJECTION");
      i = j | 0x83C;
    } 
    k = i;
    if ((paramInt & 0x83D) == 2109) {
      arrayList.add("PDP_ACTIVATE_MAX_RETRY_FAILED");
      k = i | 0x83D;
    } 
    j = k;
    if ((paramInt & 0x83E) == 2110) {
      arrayList.add("RADIO_ACCESS_BEARER_FAILURE");
      j = k | 0x83E;
    } 
    i = j;
    if ((paramInt & 0x83F) == 2111) {
      arrayList.add("ESM_UNKNOWN_EPS_BEARER_CONTEXT");
      i = j | 0x83F;
    } 
    j = i;
    if ((paramInt & 0x840) == 2112) {
      arrayList.add("DRB_RELEASED_BY_RRC");
      j = i | 0x840;
    } 
    k = j;
    if ((paramInt & 0x841) == 2113) {
      arrayList.add("CONNECTION_RELEASED");
      k = j | 0x841;
    } 
    i = k;
    if ((paramInt & 0x842) == 2114) {
      arrayList.add("EMM_DETACHED");
      i = k | 0x842;
    } 
    j = i;
    if ((paramInt & 0x843) == 2115) {
      arrayList.add("EMM_ATTACH_FAILED");
      j = i | 0x843;
    } 
    k = j;
    if ((paramInt & 0x844) == 2116) {
      arrayList.add("EMM_ATTACH_STARTED");
      k = j | 0x844;
    } 
    i = k;
    if ((paramInt & 0x845) == 2117) {
      arrayList.add("LTE_NAS_SERVICE_REQUEST_FAILED");
      i = k | 0x845;
    } 
    j = i;
    if ((paramInt & 0x846) == 2118) {
      arrayList.add("DUPLICATE_BEARER_ID");
      j = i | 0x846;
    } 
    k = j;
    if ((paramInt & 0x847) == 2119) {
      arrayList.add("ESM_COLLISION_SCENARIOS");
      k = j | 0x847;
    } 
    i = k;
    if ((paramInt & 0x848) == 2120) {
      arrayList.add("ESM_BEARER_DEACTIVATED_TO_SYNC_WITH_NETWORK");
      i = k | 0x848;
    } 
    j = i;
    if ((paramInt & 0x849) == 2121) {
      arrayList.add("ESM_NW_ACTIVATED_DED_BEARER_WITH_ID_OF_DEF_BEARER");
      j = i | 0x849;
    } 
    i = j;
    if ((paramInt & 0x84A) == 2122) {
      arrayList.add("ESM_BAD_OTA_MESSAGE");
      i = j | 0x84A;
    } 
    j = i;
    if ((paramInt & 0x84B) == 2123) {
      arrayList.add("ESM_DOWNLOAD_SERVER_REJECTED_THE_CALL");
      j = i | 0x84B;
    } 
    i = j;
    if ((paramInt & 0x84C) == 2124) {
      arrayList.add("ESM_CONTEXT_TRANSFERRED_DUE_TO_IRAT");
      i = j | 0x84C;
    } 
    j = i;
    if ((paramInt & 0x84D) == 2125) {
      arrayList.add("DS_EXPLICIT_DEACTIVATION");
      j = i | 0x84D;
    } 
    i = j;
    if ((paramInt & 0x84E) == 2126) {
      arrayList.add("ESM_LOCAL_CAUSE_NONE");
      i = j | 0x84E;
    } 
    j = i;
    if ((paramInt & 0x84F) == 2127) {
      arrayList.add("LTE_THROTTLING_NOT_REQUIRED");
      j = i | 0x84F;
    } 
    i = j;
    if ((paramInt & 0x850) == 2128) {
      arrayList.add("ACCESS_CONTROL_LIST_CHECK_FAILURE");
      i = j | 0x850;
    } 
    k = i;
    if ((paramInt & 0x851) == 2129) {
      arrayList.add("SERVICE_NOT_ALLOWED_ON_PLMN");
      k = i | 0x851;
    } 
    j = k;
    if ((paramInt & 0x852) == 2130) {
      arrayList.add("EMM_T3417_EXPIRED");
      j = k | 0x852;
    } 
    i = j;
    if ((paramInt & 0x853) == 2131) {
      arrayList.add("EMM_T3417_EXT_EXPIRED");
      i = j | 0x853;
    } 
    j = i;
    if ((paramInt & 0x854) == 2132) {
      arrayList.add("RRC_UPLINK_DATA_TRANSMISSION_FAILURE");
      j = i | 0x854;
    } 
    k = j;
    if ((paramInt & 0x855) == 2133) {
      arrayList.add("RRC_UPLINK_DELIVERY_FAILED_DUE_TO_HANDOVER");
      k = j | 0x855;
    } 
    i = k;
    if ((paramInt & 0x856) == 2134) {
      arrayList.add("RRC_UPLINK_CONNECTION_RELEASE");
      i = k | 0x856;
    } 
    j = i;
    if ((paramInt & 0x857) == 2135) {
      arrayList.add("RRC_UPLINK_RADIO_LINK_FAILURE");
      j = i | 0x857;
    } 
    i = j;
    if ((paramInt & 0x858) == 2136) {
      arrayList.add("RRC_UPLINK_ERROR_REQUEST_FROM_NAS");
      i = j | 0x858;
    } 
    j = i;
    if ((paramInt & 0x859) == 2137) {
      arrayList.add("RRC_CONNECTION_ACCESS_STRATUM_FAILURE");
      j = i | 0x859;
    } 
    i = j;
    if ((paramInt & 0x85A) == 2138) {
      arrayList.add("RRC_CONNECTION_ANOTHER_PROCEDURE_IN_PROGRESS");
      i = j | 0x85A;
    } 
    j = i;
    if ((paramInt & 0x85B) == 2139) {
      arrayList.add("RRC_CONNECTION_ACCESS_BARRED");
      j = i | 0x85B;
    } 
    i = j;
    if ((paramInt & 0x85C) == 2140) {
      arrayList.add("RRC_CONNECTION_CELL_RESELECTION");
      i = j | 0x85C;
    } 
    j = i;
    if ((paramInt & 0x85D) == 2141) {
      arrayList.add("RRC_CONNECTION_CONFIG_FAILURE");
      j = i | 0x85D;
    } 
    i = j;
    if ((paramInt & 0x85E) == 2142) {
      arrayList.add("RRC_CONNECTION_TIMER_EXPIRED");
      i = j | 0x85E;
    } 
    j = i;
    if ((paramInt & 0x85F) == 2143) {
      arrayList.add("RRC_CONNECTION_LINK_FAILURE");
      j = i | 0x85F;
    } 
    i = j;
    if ((paramInt & 0x860) == 2144) {
      arrayList.add("RRC_CONNECTION_CELL_NOT_CAMPED");
      i = j | 0x860;
    } 
    j = i;
    if ((paramInt & 0x861) == 2145) {
      arrayList.add("RRC_CONNECTION_SYSTEM_INTERVAL_FAILURE");
      j = i | 0x861;
    } 
    i = j;
    if ((paramInt & 0x862) == 2146) {
      arrayList.add("RRC_CONNECTION_REJECT_BY_NETWORK");
      i = j | 0x862;
    } 
    j = i;
    if ((paramInt & 0x863) == 2147) {
      arrayList.add("RRC_CONNECTION_NORMAL_RELEASE");
      j = i | 0x863;
    } 
    i = j;
    if ((paramInt & 0x864) == 2148) {
      arrayList.add("RRC_CONNECTION_RADIO_LINK_FAILURE");
      i = j | 0x864;
    } 
    j = i;
    if ((paramInt & 0x865) == 2149) {
      arrayList.add("RRC_CONNECTION_REESTABLISHMENT_FAILURE");
      j = i | 0x865;
    } 
    i = j;
    if ((paramInt & 0x866) == 2150) {
      arrayList.add("RRC_CONNECTION_OUT_OF_SERVICE_DURING_CELL_REGISTER");
      i = j | 0x866;
    } 
    j = i;
    if ((paramInt & 0x867) == 2151) {
      arrayList.add("RRC_CONNECTION_ABORT_REQUEST");
      j = i | 0x867;
    } 
    i = j;
    if ((paramInt & 0x868) == 2152) {
      arrayList.add("RRC_CONNECTION_SYSTEM_INFORMATION_BLOCK_READ_ERROR");
      i = j | 0x868;
    } 
    j = i;
    if ((paramInt & 0x869) == 2153) {
      arrayList.add("NETWORK_INITIATED_DETACH_WITH_AUTO_REATTACH");
      j = i | 0x869;
    } 
    i = j;
    if ((paramInt & 0x86A) == 2154) {
      arrayList.add("NETWORK_INITIATED_DETACH_NO_AUTO_REATTACH");
      i = j | 0x86A;
    } 
    j = i;
    if ((paramInt & 0x86B) == 2155) {
      arrayList.add("ESM_PROCEDURE_TIME_OUT");
      j = i | 0x86B;
    } 
    i = j;
    if ((paramInt & 0x86C) == 2156) {
      arrayList.add("INVALID_CONNECTION_ID");
      i = j | 0x86C;
    } 
    k = i;
    if ((paramInt & 0x86D) == 2157) {
      arrayList.add("MAXIMIUM_NSAPIS_EXCEEDED");
      k = i | 0x86D;
    } 
    j = k;
    if ((paramInt & 0x86E) == 2158) {
      arrayList.add("INVALID_PRIMARY_NSAPI");
      j = k | 0x86E;
    } 
    i = j;
    if ((paramInt & 0x86F) == 2159) {
      arrayList.add("CANNOT_ENCODE_OTA_MESSAGE");
      i = j | 0x86F;
    } 
    j = i;
    if ((paramInt & 0x870) == 2160) {
      arrayList.add("RADIO_ACCESS_BEARER_SETUP_FAILURE");
      j = i | 0x870;
    } 
    i = j;
    if ((paramInt & 0x871) == 2161) {
      arrayList.add("PDP_ESTABLISH_TIMEOUT_EXPIRED");
      i = j | 0x871;
    } 
    j = i;
    if ((paramInt & 0x872) == 2162) {
      arrayList.add("PDP_MODIFY_TIMEOUT_EXPIRED");
      j = i | 0x872;
    } 
    i = j;
    if ((paramInt & 0x873) == 2163) {
      arrayList.add("PDP_INACTIVE_TIMEOUT_EXPIRED");
      i = j | 0x873;
    } 
    j = i;
    if ((paramInt & 0x874) == 2164) {
      arrayList.add("PDP_LOWERLAYER_ERROR");
      j = i | 0x874;
    } 
    k = j;
    if ((paramInt & 0x875) == 2165) {
      arrayList.add("PDP_MODIFY_COLLISION");
      k = j | 0x875;
    } 
    i = k;
    if ((paramInt & 0x876) == 2166) {
      arrayList.add("MAXINUM_SIZE_OF_L2_MESSAGE_EXCEEDED");
      i = k | 0x876;
    } 
    j = i;
    if ((paramInt & 0x877) == 2167) {
      arrayList.add("NAS_REQUEST_REJECTED_BY_NETWORK");
      j = i | 0x877;
    } 
    i = j;
    if ((paramInt & 0x878) == 2168) {
      arrayList.add("RRC_CONNECTION_INVALID_REQUEST");
      i = j | 0x878;
    } 
    j = i;
    if ((paramInt & 0x879) == 2169) {
      arrayList.add("RRC_CONNECTION_TRACKING_AREA_ID_CHANGED");
      j = i | 0x879;
    } 
    k = j;
    if ((paramInt & 0x87A) == 2170) {
      arrayList.add("RRC_CONNECTION_RF_UNAVAILABLE");
      k = j | 0x87A;
    } 
    i = k;
    if ((paramInt & 0x87B) == 2171) {
      arrayList.add("RRC_CONNECTION_ABORTED_DUE_TO_IRAT_CHANGE");
      i = k | 0x87B;
    } 
    j = i;
    if ((paramInt & 0x87C) == 2172) {
      arrayList.add("RRC_CONNECTION_RELEASED_SECURITY_NOT_ACTIVE");
      j = i | 0x87C;
    } 
    i = j;
    if ((paramInt & 0x87D) == 2173) {
      arrayList.add("RRC_CONNECTION_ABORTED_AFTER_HANDOVER");
      i = j | 0x87D;
    } 
    j = i;
    if ((paramInt & 0x87E) == 2174) {
      arrayList.add("RRC_CONNECTION_ABORTED_AFTER_IRAT_CELL_CHANGE");
      j = i | 0x87E;
    } 
    i = j;
    if ((paramInt & 0x87F) == 2175) {
      arrayList.add("RRC_CONNECTION_ABORTED_DURING_IRAT_CELL_CHANGE");
      i = j | 0x87F;
    } 
    j = i;
    if ((paramInt & 0x880) == 2176) {
      arrayList.add("IMSI_UNKNOWN_IN_HOME_SUBSCRIBER_SERVER");
      j = i | 0x880;
    } 
    i = j;
    if ((paramInt & 0x881) == 2177) {
      arrayList.add("IMEI_NOT_ACCEPTED");
      i = j | 0x881;
    } 
    j = i;
    if ((paramInt & 0x882) == 2178) {
      arrayList.add("EPS_SERVICES_AND_NON_EPS_SERVICES_NOT_ALLOWED");
      j = i | 0x882;
    } 
    i = j;
    if ((paramInt & 0x883) == 2179) {
      arrayList.add("EPS_SERVICES_NOT_ALLOWED_IN_PLMN");
      i = j | 0x883;
    } 
    j = i;
    if ((paramInt & 0x884) == 2180) {
      arrayList.add("MSC_TEMPORARILY_NOT_REACHABLE");
      j = i | 0x884;
    } 
    i = j;
    if ((paramInt & 0x885) == 2181) {
      arrayList.add("CS_DOMAIN_NOT_AVAILABLE");
      i = j | 0x885;
    } 
    j = i;
    if ((paramInt & 0x886) == 2182) {
      arrayList.add("ESM_FAILURE");
      j = i | 0x886;
    } 
    i = j;
    if ((paramInt & 0x887) == 2183) {
      arrayList.add("MAC_FAILURE");
      i = j | 0x887;
    } 
    j = i;
    if ((paramInt & 0x888) == 2184) {
      arrayList.add("SYNCHRONIZATION_FAILURE");
      j = i | 0x888;
    } 
    i = j;
    if ((paramInt & 0x889) == 2185) {
      arrayList.add("UE_SECURITY_CAPABILITIES_MISMATCH");
      i = j | 0x889;
    } 
    j = i;
    if ((paramInt & 0x88A) == 2186) {
      arrayList.add("SECURITY_MODE_REJECTED");
      j = i | 0x88A;
    } 
    k = j;
    if ((paramInt & 0x88B) == 2187) {
      arrayList.add("UNACCEPTABLE_NON_EPS_AUTHENTICATION");
      k = j | 0x88B;
    } 
    i = k;
    if ((paramInt & 0x88C) == 2188) {
      arrayList.add("CS_FALLBACK_CALL_ESTABLISHMENT_NOT_ALLOWED");
      i = k | 0x88C;
    } 
    j = i;
    if ((paramInt & 0x88D) == 2189) {
      arrayList.add("NO_EPS_BEARER_CONTEXT_ACTIVATED");
      j = i | 0x88D;
    } 
    i = j;
    if ((paramInt & 0x88E) == 2190) {
      arrayList.add("INVALID_EMM_STATE");
      i = j | 0x88E;
    } 
    j = i;
    if ((paramInt & 0x88F) == 2191) {
      arrayList.add("NAS_LAYER_FAILURE");
      j = i | 0x88F;
    } 
    i = j;
    if ((paramInt & 0x890) == 2192) {
      arrayList.add("MULTIPLE_PDP_CALL_NOT_ALLOWED");
      i = j | 0x890;
    } 
    j = i;
    if ((paramInt & 0x891) == 2193) {
      arrayList.add("EMBMS_NOT_ENABLED");
      j = i | 0x891;
    } 
    i = j;
    if ((paramInt & 0x892) == 2194) {
      arrayList.add("IRAT_HANDOVER_FAILED");
      i = j | 0x892;
    } 
    j = i;
    if ((paramInt & 0x893) == 2195) {
      arrayList.add("EMBMS_REGULAR_DEACTIVATION");
      j = i | 0x893;
    } 
    i = j;
    if ((paramInt & 0x894) == 2196) {
      arrayList.add("TEST_LOOPBACK_REGULAR_DEACTIVATION");
      i = j | 0x894;
    } 
    j = i;
    if ((paramInt & 0x895) == 2197) {
      arrayList.add("LOWER_LAYER_REGISTRATION_FAILURE");
      j = i | 0x895;
    } 
    i = j;
    if ((paramInt & 0x896) == 2198) {
      arrayList.add("DATA_PLAN_EXPIRED");
      i = j | 0x896;
    } 
    j = i;
    if ((paramInt & 0x897) == 2199) {
      arrayList.add("UMTS_HANDOVER_TO_IWLAN");
      j = i | 0x897;
    } 
    i = j;
    if ((paramInt & 0x898) == 2200) {
      arrayList.add("EVDO_CONNECTION_DENY_BY_GENERAL_OR_NETWORK_BUSY");
      i = j | 0x898;
    } 
    j = i;
    if ((paramInt & 0x899) == 2201) {
      arrayList.add("EVDO_CONNECTION_DENY_BY_BILLING_OR_AUTHENTICATION_FAILURE");
      j = i | 0x899;
    } 
    i = j;
    if ((paramInt & 0x89A) == 2202) {
      arrayList.add("EVDO_HDR_CHANGED");
      i = j | 0x89A;
    } 
    j = i;
    if ((paramInt & 0x89B) == 2203) {
      arrayList.add("EVDO_HDR_EXITED");
      j = i | 0x89B;
    } 
    i = j;
    if ((paramInt & 0x89C) == 2204) {
      arrayList.add("EVDO_HDR_NO_SESSION");
      i = j | 0x89C;
    } 
    j = i;
    if ((paramInt & 0x89D) == 2205) {
      arrayList.add("EVDO_USING_GPS_FIX_INSTEAD_OF_HDR_CALL");
      j = i | 0x89D;
    } 
    i = j;
    if ((paramInt & 0x89E) == 2206) {
      arrayList.add("EVDO_HDR_CONNECTION_SETUP_TIMEOUT");
      i = j | 0x89E;
    } 
    j = i;
    if ((paramInt & 0x89F) == 2207) {
      arrayList.add("FAILED_TO_ACQUIRE_COLOCATED_HDR");
      j = i | 0x89F;
    } 
    i = j;
    if ((paramInt & 0x8A0) == 2208) {
      arrayList.add("OTASP_COMMIT_IN_PROGRESS");
      i = j | 0x8A0;
    } 
    j = i;
    if ((paramInt & 0x8A1) == 2209) {
      arrayList.add("NO_HYBRID_HDR_SERVICE");
      j = i | 0x8A1;
    } 
    k = j;
    if ((paramInt & 0x8A2) == 2210) {
      arrayList.add("HDR_NO_LOCK_GRANTED");
      k = j | 0x8A2;
    } 
    i = k;
    if ((paramInt & 0x8A3) == 2211) {
      arrayList.add("DBM_OR_SMS_IN_PROGRESS");
      i = k | 0x8A3;
    } 
    j = i;
    if ((paramInt & 0x8A4) == 2212) {
      arrayList.add("HDR_FADE");
      j = i | 0x8A4;
    } 
    i = j;
    if ((paramInt & 0x8A5) == 2213) {
      arrayList.add("HDR_ACCESS_FAILURE");
      i = j | 0x8A5;
    } 
    j = i;
    if ((paramInt & 0x8A6) == 2214) {
      arrayList.add("UNSUPPORTED_1X_PREV");
      j = i | 0x8A6;
    } 
    i = j;
    if ((paramInt & 0x8A7) == 2215) {
      arrayList.add("LOCAL_END");
      i = j | 0x8A7;
    } 
    j = i;
    if ((paramInt & 0x8A8) == 2216) {
      arrayList.add("NO_SERVICE");
      j = i | 0x8A8;
    } 
    k = j;
    if ((paramInt & 0x8A9) == 2217) {
      arrayList.add("FADE");
      k = j | 0x8A9;
    } 
    i = k;
    if ((paramInt & 0x8AA) == 2218) {
      arrayList.add("NORMAL_RELEASE");
      i = k | 0x8AA;
    } 
    j = i;
    if ((paramInt & 0x8AB) == 2219) {
      arrayList.add("ACCESS_ATTEMPT_ALREADY_IN_PROGRESS");
      j = i | 0x8AB;
    } 
    i = j;
    if ((paramInt & 0x8AC) == 2220) {
      arrayList.add("REDIRECTION_OR_HANDOFF_IN_PROGRESS");
      i = j | 0x8AC;
    } 
    j = i;
    if ((paramInt & 0x8AD) == 2221) {
      arrayList.add("EMERGENCY_MODE");
      j = i | 0x8AD;
    } 
    i = j;
    if ((paramInt & 0x8AE) == 2222) {
      arrayList.add("PHONE_IN_USE");
      i = j | 0x8AE;
    } 
    j = i;
    if ((paramInt & 0x8AF) == 2223) {
      arrayList.add("INVALID_MODE");
      j = i | 0x8AF;
    } 
    i = j;
    if ((paramInt & 0x8B0) == 2224) {
      arrayList.add("INVALID_SIM_STATE");
      i = j | 0x8B0;
    } 
    j = i;
    if ((paramInt & 0x8B1) == 2225) {
      arrayList.add("NO_COLLOCATED_HDR");
      j = i | 0x8B1;
    } 
    i = j;
    if ((paramInt & 0x8B2) == 2226) {
      arrayList.add("UE_IS_ENTERING_POWERSAVE_MODE");
      i = j | 0x8B2;
    } 
    j = i;
    if ((paramInt & 0x8B3) == 2227) {
      arrayList.add("DUAL_SWITCH");
      j = i | 0x8B3;
    } 
    i = j;
    if ((paramInt & 0x8B4) == 2228) {
      arrayList.add("PPP_TIMEOUT");
      i = j | 0x8B4;
    } 
    j = i;
    if ((paramInt & 0x8B5) == 2229) {
      arrayList.add("PPP_AUTH_FAILURE");
      j = i | 0x8B5;
    } 
    i = j;
    if ((paramInt & 0x8B6) == 2230) {
      arrayList.add("PPP_OPTION_MISMATCH");
      i = j | 0x8B6;
    } 
    j = i;
    if ((paramInt & 0x8B7) == 2231) {
      arrayList.add("PPP_PAP_FAILURE");
      j = i | 0x8B7;
    } 
    i = j;
    if ((paramInt & 0x8B8) == 2232) {
      arrayList.add("PPP_CHAP_FAILURE");
      i = j | 0x8B8;
    } 
    j = i;
    if ((paramInt & 0x8B9) == 2233) {
      arrayList.add("PPP_CLOSE_IN_PROGRESS");
      j = i | 0x8B9;
    } 
    k = j;
    if ((paramInt & 0x8BA) == 2234) {
      arrayList.add("LIMITED_TO_IPV4");
      k = j | 0x8BA;
    } 
    i = k;
    if ((paramInt & 0x8BB) == 2235) {
      arrayList.add("LIMITED_TO_IPV6");
      i = k | 0x8BB;
    } 
    j = i;
    if ((paramInt & 0x8BC) == 2236) {
      arrayList.add("VSNCP_TIMEOUT");
      j = i | 0x8BC;
    } 
    k = j;
    if ((paramInt & 0x8BD) == 2237) {
      arrayList.add("VSNCP_GEN_ERROR");
      k = j | 0x8BD;
    } 
    i = k;
    if ((paramInt & 0x8BE) == 2238) {
      arrayList.add("VSNCP_APN_UNATHORIZED");
      i = k | 0x8BE;
    } 
    j = i;
    if ((paramInt & 0x8BF) == 2239) {
      arrayList.add("VSNCP_PDN_LIMIT_EXCEEDED");
      j = i | 0x8BF;
    } 
    i = j;
    if ((paramInt & 0x8C0) == 2240) {
      arrayList.add("VSNCP_NO_PDN_GATEWAY_ADDRESS");
      i = j | 0x8C0;
    } 
    j = i;
    if ((paramInt & 0x8C1) == 2241) {
      arrayList.add("VSNCP_PDN_GATEWAY_UNREACHABLE");
      j = i | 0x8C1;
    } 
    i = j;
    if ((paramInt & 0x8C2) == 2242) {
      arrayList.add("VSNCP_PDN_GATEWAY_REJECT");
      i = j | 0x8C2;
    } 
    j = i;
    if ((paramInt & 0x8C3) == 2243) {
      arrayList.add("VSNCP_INSUFFICIENT_PARAMETERS");
      j = i | 0x8C3;
    } 
    i = j;
    if ((paramInt & 0x8C4) == 2244) {
      arrayList.add("VSNCP_RESOURCE_UNAVAILABLE");
      i = j | 0x8C4;
    } 
    j = i;
    if ((paramInt & 0x8C5) == 2245) {
      arrayList.add("VSNCP_ADMINISTRATIVELY_PROHIBITED");
      j = i | 0x8C5;
    } 
    i = j;
    if ((paramInt & 0x8C6) == 2246) {
      arrayList.add("VSNCP_PDN_ID_IN_USE");
      i = j | 0x8C6;
    } 
    j = i;
    if ((paramInt & 0x8C7) == 2247) {
      arrayList.add("VSNCP_SUBSCRIBER_LIMITATION");
      j = i | 0x8C7;
    } 
    i = j;
    if ((paramInt & 0x8C8) == 2248) {
      arrayList.add("VSNCP_PDN_EXISTS_FOR_THIS_APN");
      i = j | 0x8C8;
    } 
    k = i;
    if ((paramInt & 0x8C9) == 2249) {
      arrayList.add("VSNCP_RECONNECT_NOT_ALLOWED");
      k = i | 0x8C9;
    } 
    j = k;
    if ((paramInt & 0x8CA) == 2250) {
      arrayList.add("IPV6_PREFIX_UNAVAILABLE");
      j = k | 0x8CA;
    } 
    i = j;
    if ((paramInt & 0x8CB) == 2251) {
      arrayList.add("HANDOFF_PREFERENCE_CHANGED");
      i = j | 0x8CB;
    } 
    if (paramInt != i) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((i ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
