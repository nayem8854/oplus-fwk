package android.hardware.radio;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ICloseHandle extends IInterface {
  void close() throws RemoteException;
  
  class Default implements ICloseHandle {
    public void close() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ICloseHandle {
    private static final String DESCRIPTOR = "android.hardware.radio.ICloseHandle";
    
    static final int TRANSACTION_close = 1;
    
    public Stub() {
      attachInterface(this, "android.hardware.radio.ICloseHandle");
    }
    
    public static ICloseHandle asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.radio.ICloseHandle");
      if (iInterface != null && iInterface instanceof ICloseHandle)
        return (ICloseHandle)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "close";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.hardware.radio.ICloseHandle");
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.radio.ICloseHandle");
      close();
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements ICloseHandle {
      public static ICloseHandle sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.radio.ICloseHandle";
      }
      
      public void close() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.radio.ICloseHandle");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ICloseHandle.Stub.getDefaultImpl() != null) {
            ICloseHandle.Stub.getDefaultImpl().close();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ICloseHandle param1ICloseHandle) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ICloseHandle != null) {
          Proxy.sDefaultImpl = param1ICloseHandle;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ICloseHandle getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
