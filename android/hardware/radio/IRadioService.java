package android.hardware.radio;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IRadioService extends IInterface {
  ICloseHandle addAnnouncementListener(int[] paramArrayOfint, IAnnouncementListener paramIAnnouncementListener) throws RemoteException;
  
  List<RadioManager.ModuleProperties> listModules() throws RemoteException;
  
  ITuner openTuner(int paramInt, RadioManager.BandConfig paramBandConfig, boolean paramBoolean, ITunerCallback paramITunerCallback) throws RemoteException;
  
  class Default implements IRadioService {
    public List<RadioManager.ModuleProperties> listModules() throws RemoteException {
      return null;
    }
    
    public ITuner openTuner(int param1Int, RadioManager.BandConfig param1BandConfig, boolean param1Boolean, ITunerCallback param1ITunerCallback) throws RemoteException {
      return null;
    }
    
    public ICloseHandle addAnnouncementListener(int[] param1ArrayOfint, IAnnouncementListener param1IAnnouncementListener) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRadioService {
    private static final String DESCRIPTOR = "android.hardware.radio.IRadioService";
    
    static final int TRANSACTION_addAnnouncementListener = 3;
    
    static final int TRANSACTION_listModules = 1;
    
    static final int TRANSACTION_openTuner = 2;
    
    public Stub() {
      attachInterface(this, "android.hardware.radio.IRadioService");
    }
    
    public static IRadioService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.radio.IRadioService");
      if (iInterface != null && iInterface instanceof IRadioService)
        return (IRadioService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "addAnnouncementListener";
        } 
        return "openTuner";
      } 
      return "listModules";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder;
      if (param1Int1 != 1) {
        IBinder iBinder1;
        ICloseHandle iCloseHandle2;
        boolean bool;
        int[] arrayOfInt = null;
        IAnnouncementListener iAnnouncementListener = null;
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.hardware.radio.IRadioService");
            return true;
          } 
          param1Parcel1.enforceInterface("android.hardware.radio.IRadioService");
          arrayOfInt = param1Parcel1.createIntArray();
          IAnnouncementListener iAnnouncementListener1 = IAnnouncementListener.Stub.asInterface(param1Parcel1.readStrongBinder());
          iCloseHandle2 = addAnnouncementListener(arrayOfInt, iAnnouncementListener1);
          param1Parcel2.writeNoException();
          iAnnouncementListener1 = iAnnouncementListener;
          if (iCloseHandle2 != null)
            iBinder1 = iCloseHandle2.asBinder(); 
          param1Parcel2.writeStrongBinder(iBinder1);
          return true;
        } 
        iBinder1.enforceInterface("android.hardware.radio.IRadioService");
        param1Int1 = iBinder1.readInt();
        if (iBinder1.readInt() != 0) {
          RadioManager.BandConfig bandConfig = (RadioManager.BandConfig)RadioManager.BandConfig.CREATOR.createFromParcel((Parcel)iBinder1);
        } else {
          iAnnouncementListener = null;
        } 
        if (iBinder1.readInt() != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        ITunerCallback iTunerCallback = ITunerCallback.Stub.asInterface(iBinder1.readStrongBinder());
        ITuner iTuner = openTuner(param1Int1, (RadioManager.BandConfig)iAnnouncementListener, bool, iTunerCallback);
        param1Parcel2.writeNoException();
        ICloseHandle iCloseHandle1 = iCloseHandle2;
        if (iTuner != null)
          iBinder = iTuner.asBinder(); 
        param1Parcel2.writeStrongBinder(iBinder);
        return true;
      } 
      iBinder.enforceInterface("android.hardware.radio.IRadioService");
      List<RadioManager.ModuleProperties> list = listModules();
      param1Parcel2.writeNoException();
      param1Parcel2.writeTypedList(list);
      return true;
    }
    
    private static class Proxy implements IRadioService {
      public static IRadioService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.radio.IRadioService";
      }
      
      public List<RadioManager.ModuleProperties> listModules() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.radio.IRadioService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IRadioService.Stub.getDefaultImpl() != null)
            return IRadioService.Stub.getDefaultImpl().listModules(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(RadioManager.ModuleProperties.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ITuner openTuner(int param2Int, RadioManager.BandConfig param2BandConfig, boolean param2Boolean, ITunerCallback param2ITunerCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.radio.IRadioService");
          parcel1.writeInt(param2Int);
          boolean bool = true;
          if (param2BandConfig != null) {
            parcel1.writeInt(1);
            param2BandConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          if (param2ITunerCallback != null) {
            iBinder = param2ITunerCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IRadioService.Stub.getDefaultImpl() != null)
            return IRadioService.Stub.getDefaultImpl().openTuner(param2Int, param2BandConfig, param2Boolean, param2ITunerCallback); 
          parcel2.readException();
          return ITuner.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ICloseHandle addAnnouncementListener(int[] param2ArrayOfint, IAnnouncementListener param2IAnnouncementListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.radio.IRadioService");
          parcel1.writeIntArray(param2ArrayOfint);
          if (param2IAnnouncementListener != null) {
            iBinder = param2IAnnouncementListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IRadioService.Stub.getDefaultImpl() != null)
            return IRadioService.Stub.getDefaultImpl().addAnnouncementListener(param2ArrayOfint, param2IAnnouncementListener); 
          parcel2.readException();
          return ICloseHandle.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRadioService param1IRadioService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRadioService != null) {
          Proxy.sDefaultImpl = param1IRadioService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRadioService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
