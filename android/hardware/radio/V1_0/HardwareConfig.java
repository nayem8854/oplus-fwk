package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class HardwareConfig {
  public int type = 0;
  
  public String uuid = new String();
  
  public int state = 0;
  
  public ArrayList<HardwareConfigModem> modem = new ArrayList<>();
  
  public ArrayList<HardwareConfigSim> sim = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != HardwareConfig.class)
      return false; 
    paramObject = paramObject;
    if (this.type != ((HardwareConfig)paramObject).type)
      return false; 
    if (!HidlSupport.deepEquals(this.uuid, ((HardwareConfig)paramObject).uuid))
      return false; 
    if (this.state != ((HardwareConfig)paramObject).state)
      return false; 
    if (!HidlSupport.deepEquals(this.modem, ((HardwareConfig)paramObject).modem))
      return false; 
    if (!HidlSupport.deepEquals(this.sim, ((HardwareConfig)paramObject).sim))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.type;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    String str = this.uuid;
    i = HidlSupport.deepHashCode(str);
    int k = this.state;
    int m = HidlSupport.deepHashCode(Integer.valueOf(k));
    ArrayList<HardwareConfigModem> arrayList1 = this.modem;
    int n = HidlSupport.deepHashCode(arrayList1);
    ArrayList<HardwareConfigSim> arrayList = this.sim;
    k = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(i), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".type = ");
    stringBuilder.append(HardwareConfigType.toString(this.type));
    stringBuilder.append(", .uuid = ");
    stringBuilder.append(this.uuid);
    stringBuilder.append(", .state = ");
    stringBuilder.append(HardwareConfigState.toString(this.state));
    stringBuilder.append(", .modem = ");
    stringBuilder.append(this.modem);
    stringBuilder.append(", .sim = ");
    stringBuilder.append(this.sim);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(64L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<HardwareConfig> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<HardwareConfig> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 64);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      HardwareConfig hardwareConfig = new HardwareConfig();
      hardwareConfig.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 64));
      arrayList.add(hardwareConfig);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.type = paramHwBlob.getInt32(paramLong + 0L);
    String str = paramHwBlob.getString(paramLong + 8L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, false);
    this.state = paramHwBlob.getInt32(paramLong + 24L);
    int i = paramHwBlob.getInt32(paramLong + 32L + 8L);
    l1 = (i * 20);
    l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 32L + 0L, true);
    this.modem.clear();
    byte b;
    for (b = 0; b < i; b++) {
      HardwareConfigModem hardwareConfigModem = new HardwareConfigModem();
      hardwareConfigModem.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 20));
      this.modem.add(hardwareConfigModem);
    } 
    i = paramHwBlob.getInt32(paramLong + 48L + 8L);
    l2 = (i * 16);
    l1 = paramHwBlob.handle();
    paramHwBlob = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 48L + 0L, true);
    this.sim.clear();
    for (b = 0; b < i; b++) {
      HardwareConfigSim hardwareConfigSim = new HardwareConfigSim();
      hardwareConfigSim.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, (b * 16));
      this.sim.add(hardwareConfigSim);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(64);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<HardwareConfig> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 64);
    for (byte b = 0; b < i; b++)
      ((HardwareConfig)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 64)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.type);
    paramHwBlob.putString(paramLong + 8L, this.uuid);
    paramHwBlob.putInt32(paramLong + 24L, this.state);
    int i = this.modem.size();
    paramHwBlob.putInt32(paramLong + 32L + 8L, i);
    paramHwBlob.putBool(paramLong + 32L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 20);
    byte b;
    for (b = 0; b < i; b++)
      ((HardwareConfigModem)this.modem.get(b)).writeEmbeddedToBlob(hwBlob, (b * 20)); 
    paramHwBlob.putBlob(paramLong + 32L + 0L, hwBlob);
    i = this.sim.size();
    paramHwBlob.putInt32(paramLong + 48L + 8L, i);
    paramHwBlob.putBool(paramLong + 48L + 12L, false);
    hwBlob = new HwBlob(i * 16);
    for (b = 0; b < i; b++)
      ((HardwareConfigSim)this.sim.get(b)).writeEmbeddedToBlob(hwBlob, (b * 16)); 
    paramHwBlob.putBlob(paramLong + 48L + 0L, hwBlob);
  }
}
