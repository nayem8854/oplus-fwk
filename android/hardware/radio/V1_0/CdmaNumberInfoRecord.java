package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CdmaNumberInfoRecord {
  public String number = new String();
  
  public byte numberType = 0;
  
  public byte numberPlan = 0;
  
  public byte pi = 0;
  
  public byte si = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CdmaNumberInfoRecord.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.number, ((CdmaNumberInfoRecord)paramObject).number))
      return false; 
    if (this.numberType != ((CdmaNumberInfoRecord)paramObject).numberType)
      return false; 
    if (this.numberPlan != ((CdmaNumberInfoRecord)paramObject).numberPlan)
      return false; 
    if (this.pi != ((CdmaNumberInfoRecord)paramObject).pi)
      return false; 
    if (this.si != ((CdmaNumberInfoRecord)paramObject).si)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    String str = this.number;
    int i = HidlSupport.deepHashCode(str);
    byte b = this.numberType;
    int j = HidlSupport.deepHashCode(Byte.valueOf(b));
    b = this.numberPlan;
    int k = HidlSupport.deepHashCode(Byte.valueOf(b));
    b = this.pi;
    int m = HidlSupport.deepHashCode(Byte.valueOf(b));
    b = this.si;
    int n = HidlSupport.deepHashCode(Byte.valueOf(b));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".number = ");
    stringBuilder.append(this.number);
    stringBuilder.append(", .numberType = ");
    stringBuilder.append(this.numberType);
    stringBuilder.append(", .numberPlan = ");
    stringBuilder.append(this.numberPlan);
    stringBuilder.append(", .pi = ");
    stringBuilder.append(this.pi);
    stringBuilder.append(", .si = ");
    stringBuilder.append(this.si);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(24L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CdmaNumberInfoRecord> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CdmaNumberInfoRecord> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 24);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CdmaNumberInfoRecord cdmaNumberInfoRecord = new CdmaNumberInfoRecord();
      cdmaNumberInfoRecord.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 24));
      arrayList.add(cdmaNumberInfoRecord);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    String str = paramHwBlob.getString(paramLong + 0L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, false);
    this.numberType = paramHwBlob.getInt8(16L + paramLong);
    this.numberPlan = paramHwBlob.getInt8(17L + paramLong);
    this.pi = paramHwBlob.getInt8(18L + paramLong);
    this.si = paramHwBlob.getInt8(19L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(24);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CdmaNumberInfoRecord> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 24);
    for (byte b = 0; b < i; b++)
      ((CdmaNumberInfoRecord)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 24)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putString(0L + paramLong, this.number);
    paramHwBlob.putInt8(16L + paramLong, this.numberType);
    paramHwBlob.putInt8(17L + paramLong, this.numberPlan);
    paramHwBlob.putInt8(18L + paramLong, this.pi);
    paramHwBlob.putInt8(19L + paramLong, this.si);
  }
}
