package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CdmaLineControlInfoRecord {
  public byte lineCtrlPolarityIncluded = 0;
  
  public byte lineCtrlToggle = 0;
  
  public byte lineCtrlReverse = 0;
  
  public byte lineCtrlPowerDenial = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CdmaLineControlInfoRecord.class)
      return false; 
    paramObject = paramObject;
    if (this.lineCtrlPolarityIncluded != ((CdmaLineControlInfoRecord)paramObject).lineCtrlPolarityIncluded)
      return false; 
    if (this.lineCtrlToggle != ((CdmaLineControlInfoRecord)paramObject).lineCtrlToggle)
      return false; 
    if (this.lineCtrlReverse != ((CdmaLineControlInfoRecord)paramObject).lineCtrlReverse)
      return false; 
    if (this.lineCtrlPowerDenial != ((CdmaLineControlInfoRecord)paramObject).lineCtrlPowerDenial)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    byte b = this.lineCtrlPolarityIncluded;
    int i = HidlSupport.deepHashCode(Byte.valueOf(b));
    b = this.lineCtrlToggle;
    int j = HidlSupport.deepHashCode(Byte.valueOf(b));
    b = this.lineCtrlReverse;
    int k = HidlSupport.deepHashCode(Byte.valueOf(b));
    b = this.lineCtrlPowerDenial;
    int m = HidlSupport.deepHashCode(Byte.valueOf(b));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".lineCtrlPolarityIncluded = ");
    stringBuilder.append(this.lineCtrlPolarityIncluded);
    stringBuilder.append(", .lineCtrlToggle = ");
    stringBuilder.append(this.lineCtrlToggle);
    stringBuilder.append(", .lineCtrlReverse = ");
    stringBuilder.append(this.lineCtrlReverse);
    stringBuilder.append(", .lineCtrlPowerDenial = ");
    stringBuilder.append(this.lineCtrlPowerDenial);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(4L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CdmaLineControlInfoRecord> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CdmaLineControlInfoRecord> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 4);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CdmaLineControlInfoRecord cdmaLineControlInfoRecord = new CdmaLineControlInfoRecord();
      cdmaLineControlInfoRecord.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 4));
      arrayList.add(cdmaLineControlInfoRecord);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.lineCtrlPolarityIncluded = paramHwBlob.getInt8(0L + paramLong);
    this.lineCtrlToggle = paramHwBlob.getInt8(1L + paramLong);
    this.lineCtrlReverse = paramHwBlob.getInt8(2L + paramLong);
    this.lineCtrlPowerDenial = paramHwBlob.getInt8(3L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(4);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CdmaLineControlInfoRecord> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 4);
    for (byte b = 0; b < i; b++)
      ((CdmaLineControlInfoRecord)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 4)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt8(0L + paramLong, this.lineCtrlPolarityIncluded);
    paramHwBlob.putInt8(1L + paramLong, this.lineCtrlToggle);
    paramHwBlob.putInt8(2L + paramLong, this.lineCtrlReverse);
    paramHwBlob.putInt8(3L + paramLong, this.lineCtrlPowerDenial);
  }
}
