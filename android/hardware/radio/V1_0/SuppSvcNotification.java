package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class SuppSvcNotification {
  public boolean isMT = false;
  
  public int code = 0;
  
  public int index = 0;
  
  public int type = 0;
  
  public String number = new String();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != SuppSvcNotification.class)
      return false; 
    paramObject = paramObject;
    if (this.isMT != ((SuppSvcNotification)paramObject).isMT)
      return false; 
    if (this.code != ((SuppSvcNotification)paramObject).code)
      return false; 
    if (this.index != ((SuppSvcNotification)paramObject).index)
      return false; 
    if (this.type != ((SuppSvcNotification)paramObject).type)
      return false; 
    if (!HidlSupport.deepEquals(this.number, ((SuppSvcNotification)paramObject).number))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    boolean bool = this.isMT;
    int i = HidlSupport.deepHashCode(Boolean.valueOf(bool)), j = this.code;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.index;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.type;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    String str = this.number;
    int n = HidlSupport.deepHashCode(str);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".isMT = ");
    stringBuilder.append(this.isMT);
    stringBuilder.append(", .code = ");
    stringBuilder.append(this.code);
    stringBuilder.append(", .index = ");
    stringBuilder.append(this.index);
    stringBuilder.append(", .type = ");
    stringBuilder.append(this.type);
    stringBuilder.append(", .number = ");
    stringBuilder.append(this.number);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(32L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<SuppSvcNotification> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<SuppSvcNotification> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 32);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      SuppSvcNotification suppSvcNotification = new SuppSvcNotification();
      suppSvcNotification.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 32));
      arrayList.add(suppSvcNotification);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.isMT = paramHwBlob.getBool(paramLong + 0L);
    this.code = paramHwBlob.getInt32(paramLong + 4L);
    this.index = paramHwBlob.getInt32(paramLong + 8L);
    this.type = paramHwBlob.getInt32(paramLong + 12L);
    String str = paramHwBlob.getString(paramLong + 16L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 16L + 0L, false);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(32);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<SuppSvcNotification> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 32);
    for (byte b = 0; b < i; b++)
      ((SuppSvcNotification)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 32)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putBool(0L + paramLong, this.isMT);
    paramHwBlob.putInt32(4L + paramLong, this.code);
    paramHwBlob.putInt32(8L + paramLong, this.index);
    paramHwBlob.putInt32(12L + paramLong, this.type);
    paramHwBlob.putString(16L + paramLong, this.number);
  }
}
