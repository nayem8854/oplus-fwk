package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CdmaT53AudioControlInfoRecord {
  public byte upLink = 0;
  
  public byte downLink = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CdmaT53AudioControlInfoRecord.class)
      return false; 
    paramObject = paramObject;
    if (this.upLink != ((CdmaT53AudioControlInfoRecord)paramObject).upLink)
      return false; 
    if (this.downLink != ((CdmaT53AudioControlInfoRecord)paramObject).downLink)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    byte b = this.upLink;
    int i = HidlSupport.deepHashCode(Byte.valueOf(b));
    b = this.downLink;
    int j = HidlSupport.deepHashCode(Byte.valueOf(b));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".upLink = ");
    stringBuilder.append(this.upLink);
    stringBuilder.append(", .downLink = ");
    stringBuilder.append(this.downLink);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(2L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CdmaT53AudioControlInfoRecord> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CdmaT53AudioControlInfoRecord> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 2);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CdmaT53AudioControlInfoRecord cdmaT53AudioControlInfoRecord = new CdmaT53AudioControlInfoRecord();
      cdmaT53AudioControlInfoRecord.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 2));
      arrayList.add(cdmaT53AudioControlInfoRecord);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.upLink = paramHwBlob.getInt8(0L + paramLong);
    this.downLink = paramHwBlob.getInt8(1L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(2);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CdmaT53AudioControlInfoRecord> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 2);
    for (byte b = 0; b < i; b++)
      ((CdmaT53AudioControlInfoRecord)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 2)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt8(0L + paramLong, this.upLink);
    paramHwBlob.putInt8(1L + paramLong, this.downLink);
  }
}
