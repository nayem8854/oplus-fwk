package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class SelectUiccSub {
  public int slot = 0;
  
  public int appIndex = 0;
  
  public int subType = 0;
  
  public int actStatus = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != SelectUiccSub.class)
      return false; 
    paramObject = paramObject;
    if (this.slot != ((SelectUiccSub)paramObject).slot)
      return false; 
    if (this.appIndex != ((SelectUiccSub)paramObject).appIndex)
      return false; 
    if (this.subType != ((SelectUiccSub)paramObject).subType)
      return false; 
    if (this.actStatus != ((SelectUiccSub)paramObject).actStatus)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.slot;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.appIndex;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.subType;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.actStatus;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".slot = ");
    stringBuilder.append(this.slot);
    stringBuilder.append(", .appIndex = ");
    stringBuilder.append(this.appIndex);
    stringBuilder.append(", .subType = ");
    stringBuilder.append(SubscriptionType.toString(this.subType));
    stringBuilder.append(", .actStatus = ");
    stringBuilder.append(UiccSubActStatus.toString(this.actStatus));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<SelectUiccSub> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<SelectUiccSub> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 16);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      SelectUiccSub selectUiccSub = new SelectUiccSub();
      selectUiccSub.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 16));
      arrayList.add(selectUiccSub);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.slot = paramHwBlob.getInt32(0L + paramLong);
    this.appIndex = paramHwBlob.getInt32(4L + paramLong);
    this.subType = paramHwBlob.getInt32(8L + paramLong);
    this.actStatus = paramHwBlob.getInt32(12L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(16);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<SelectUiccSub> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 16);
    for (byte b = 0; b < i; b++)
      ((SelectUiccSub)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 16)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.slot);
    paramHwBlob.putInt32(4L + paramLong, this.appIndex);
    paramHwBlob.putInt32(8L + paramLong, this.subType);
    paramHwBlob.putInt32(12L + paramLong, this.actStatus);
  }
}
