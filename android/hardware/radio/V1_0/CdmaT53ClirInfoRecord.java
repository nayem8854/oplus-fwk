package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CdmaT53ClirInfoRecord {
  public byte cause = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CdmaT53ClirInfoRecord.class)
      return false; 
    paramObject = paramObject;
    if (this.cause != ((CdmaT53ClirInfoRecord)paramObject).cause)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    byte b = this.cause;
    int i = HidlSupport.deepHashCode(Byte.valueOf(b));
    return Objects.hash(new Object[] { Integer.valueOf(i) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".cause = ");
    stringBuilder.append(this.cause);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(1L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CdmaT53ClirInfoRecord> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CdmaT53ClirInfoRecord> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 1);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CdmaT53ClirInfoRecord cdmaT53ClirInfoRecord = new CdmaT53ClirInfoRecord();
      cdmaT53ClirInfoRecord.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 1));
      arrayList.add(cdmaT53ClirInfoRecord);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.cause = paramHwBlob.getInt8(0L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(1);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CdmaT53ClirInfoRecord> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 1);
    for (byte b = 0; b < i; b++)
      ((CdmaT53ClirInfoRecord)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 1)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt8(0L + paramLong, this.cause);
  }
}
