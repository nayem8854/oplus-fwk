package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class SsInfoData {
  public ArrayList<Integer> ssInfo = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != SsInfoData.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.ssInfo, ((SsInfoData)paramObject).ssInfo))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    ArrayList<Integer> arrayList = this.ssInfo;
    int i = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(i) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".ssInfo = ");
    stringBuilder.append(this.ssInfo);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<SsInfoData> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<SsInfoData> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 16);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      SsInfoData ssInfoData = new SsInfoData();
      ssInfoData.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 16));
      arrayList.add(ssInfoData);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    int i = paramHwBlob.getInt32(paramLong + 0L + 8L);
    long l1 = (i * 4);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, true);
    this.ssInfo.clear();
    for (byte b = 0; b < i; b++) {
      int j = hwBlob.getInt32((b * 4));
      this.ssInfo.add(Integer.valueOf(j));
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(16);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<SsInfoData> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 16);
    for (byte b = 0; b < i; b++)
      ((SsInfoData)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 16)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    int i = this.ssInfo.size();
    paramHwBlob.putInt32(paramLong + 0L + 8L, i);
    paramHwBlob.putBool(paramLong + 0L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 4);
    for (byte b = 0; b < i; b++)
      hwBlob.putInt32((b * 4), ((Integer)this.ssInfo.get(b)).intValue()); 
    paramHwBlob.putBlob(paramLong + 0L + 0L, hwBlob);
  }
}
