package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class EvdoSignalStrength {
  public int dbm = 0;
  
  public int ecio = 0;
  
  public int signalNoiseRatio = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != EvdoSignalStrength.class)
      return false; 
    paramObject = paramObject;
    if (this.dbm != ((EvdoSignalStrength)paramObject).dbm)
      return false; 
    if (this.ecio != ((EvdoSignalStrength)paramObject).ecio)
      return false; 
    if (this.signalNoiseRatio != ((EvdoSignalStrength)paramObject).signalNoiseRatio)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.dbm;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.ecio;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.signalNoiseRatio;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".dbm = ");
    stringBuilder.append(this.dbm);
    stringBuilder.append(", .ecio = ");
    stringBuilder.append(this.ecio);
    stringBuilder.append(", .signalNoiseRatio = ");
    stringBuilder.append(this.signalNoiseRatio);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(12L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<EvdoSignalStrength> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<EvdoSignalStrength> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 12);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      EvdoSignalStrength evdoSignalStrength = new EvdoSignalStrength();
      evdoSignalStrength.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 12));
      arrayList.add(evdoSignalStrength);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.dbm = paramHwBlob.getInt32(0L + paramLong);
    this.ecio = paramHwBlob.getInt32(4L + paramLong);
    this.signalNoiseRatio = paramHwBlob.getInt32(8L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(12);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<EvdoSignalStrength> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 12);
    for (byte b = 0; b < i; b++)
      ((EvdoSignalStrength)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 12)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.dbm);
    paramHwBlob.putInt32(4L + paramLong, this.ecio);
    paramHwBlob.putInt32(8L + paramLong, this.signalNoiseRatio);
  }
}
