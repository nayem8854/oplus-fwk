package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CdmaSmsAck {
  public int errorClass = 0;
  
  public int smsCauseCode = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CdmaSmsAck.class)
      return false; 
    paramObject = paramObject;
    if (this.errorClass != ((CdmaSmsAck)paramObject).errorClass)
      return false; 
    if (this.smsCauseCode != ((CdmaSmsAck)paramObject).smsCauseCode)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.errorClass;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.smsCauseCode;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".errorClass = ");
    stringBuilder.append(CdmaSmsErrorClass.toString(this.errorClass));
    stringBuilder.append(", .smsCauseCode = ");
    stringBuilder.append(this.smsCauseCode);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(8L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CdmaSmsAck> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CdmaSmsAck> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 8);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CdmaSmsAck cdmaSmsAck = new CdmaSmsAck();
      cdmaSmsAck.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 8));
      arrayList.add(cdmaSmsAck);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.errorClass = paramHwBlob.getInt32(0L + paramLong);
    this.smsCauseCode = paramHwBlob.getInt32(4L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(8);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CdmaSmsAck> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 8);
    for (byte b = 0; b < i; b++)
      ((CdmaSmsAck)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 8)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.errorClass);
    paramHwBlob.putInt32(4L + paramLong, this.smsCauseCode);
  }
}
