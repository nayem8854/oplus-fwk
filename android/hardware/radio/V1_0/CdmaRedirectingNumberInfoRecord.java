package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CdmaRedirectingNumberInfoRecord {
  public CdmaNumberInfoRecord redirectingNumber = new CdmaNumberInfoRecord();
  
  public int redirectingReason = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CdmaRedirectingNumberInfoRecord.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.redirectingNumber, ((CdmaRedirectingNumberInfoRecord)paramObject).redirectingNumber))
      return false; 
    if (this.redirectingReason != ((CdmaRedirectingNumberInfoRecord)paramObject).redirectingReason)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    CdmaNumberInfoRecord cdmaNumberInfoRecord = this.redirectingNumber;
    int i = HidlSupport.deepHashCode(cdmaNumberInfoRecord), j = this.redirectingReason;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".redirectingNumber = ");
    stringBuilder.append(this.redirectingNumber);
    stringBuilder.append(", .redirectingReason = ");
    stringBuilder.append(CdmaRedirectingReason.toString(this.redirectingReason));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(32L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CdmaRedirectingNumberInfoRecord> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CdmaRedirectingNumberInfoRecord> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 32);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CdmaRedirectingNumberInfoRecord cdmaRedirectingNumberInfoRecord = new CdmaRedirectingNumberInfoRecord();
      cdmaRedirectingNumberInfoRecord.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 32));
      arrayList.add(cdmaRedirectingNumberInfoRecord);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.redirectingNumber.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 0L + paramLong);
    this.redirectingReason = paramHwBlob.getInt32(24L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(32);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CdmaRedirectingNumberInfoRecord> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 32);
    for (byte b = 0; b < i; b++)
      ((CdmaRedirectingNumberInfoRecord)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 32)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.redirectingNumber.writeEmbeddedToBlob(paramHwBlob, 0L + paramLong);
    paramHwBlob.putInt32(24L + paramLong, this.redirectingReason);
  }
}
