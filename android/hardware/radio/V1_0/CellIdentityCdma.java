package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellIdentityCdma {
  public int networkId = 0;
  
  public int systemId = 0;
  
  public int baseStationId = 0;
  
  public int longitude = 0;
  
  public int latitude = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellIdentityCdma.class)
      return false; 
    paramObject = paramObject;
    if (this.networkId != ((CellIdentityCdma)paramObject).networkId)
      return false; 
    if (this.systemId != ((CellIdentityCdma)paramObject).systemId)
      return false; 
    if (this.baseStationId != ((CellIdentityCdma)paramObject).baseStationId)
      return false; 
    if (this.longitude != ((CellIdentityCdma)paramObject).longitude)
      return false; 
    if (this.latitude != ((CellIdentityCdma)paramObject).latitude)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.networkId;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.systemId;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.baseStationId;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.longitude;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    int n = this.latitude;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".networkId = ");
    stringBuilder.append(this.networkId);
    stringBuilder.append(", .systemId = ");
    stringBuilder.append(this.systemId);
    stringBuilder.append(", .baseStationId = ");
    stringBuilder.append(this.baseStationId);
    stringBuilder.append(", .longitude = ");
    stringBuilder.append(this.longitude);
    stringBuilder.append(", .latitude = ");
    stringBuilder.append(this.latitude);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(20L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellIdentityCdma> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellIdentityCdma> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 20);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellIdentityCdma cellIdentityCdma = new CellIdentityCdma();
      cellIdentityCdma.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 20));
      arrayList.add(cellIdentityCdma);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.networkId = paramHwBlob.getInt32(0L + paramLong);
    this.systemId = paramHwBlob.getInt32(4L + paramLong);
    this.baseStationId = paramHwBlob.getInt32(8L + paramLong);
    this.longitude = paramHwBlob.getInt32(12L + paramLong);
    this.latitude = paramHwBlob.getInt32(16L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(20);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellIdentityCdma> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 20);
    for (byte b = 0; b < i; b++)
      ((CellIdentityCdma)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 20)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.networkId);
    paramHwBlob.putInt32(4L + paramLong, this.systemId);
    paramHwBlob.putInt32(8L + paramLong, this.baseStationId);
    paramHwBlob.putInt32(12L + paramLong, this.longitude);
    paramHwBlob.putInt32(16L + paramLong, this.latitude);
  }
}
