package android.hardware.radio.V1_0;

import java.util.ArrayList;

public final class P2Constant {
  public static final int NO_P2 = -1;
  
  public static final String toString(int paramInt) {
    if (paramInt == -1)
      return "NO_P2"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    return stringBuilder.toString();
  }
  
  public static final String dumpBitfield(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    int i = 0;
    if ((paramInt & 0xFFFFFFFF) == -1) {
      arrayList.add("NO_P2");
      i = 0x0 | 0xFFFFFFFF;
    } 
    if (paramInt != i) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("0x");
      stringBuilder.append(Integer.toHexString((i ^ 0xFFFFFFFF) & paramInt));
      arrayList.add(stringBuilder.toString());
    } 
    return String.join(" | ", (Iterable)arrayList);
  }
}
