package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class PcoDataInfo {
  public int cid = 0;
  
  public String bearerProto = new String();
  
  public int pcoId = 0;
  
  public ArrayList<Byte> contents = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != PcoDataInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.cid != ((PcoDataInfo)paramObject).cid)
      return false; 
    if (!HidlSupport.deepEquals(this.bearerProto, ((PcoDataInfo)paramObject).bearerProto))
      return false; 
    if (this.pcoId != ((PcoDataInfo)paramObject).pcoId)
      return false; 
    if (!HidlSupport.deepEquals(this.contents, ((PcoDataInfo)paramObject).contents))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.cid;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    String str = this.bearerProto;
    int j = HidlSupport.deepHashCode(str), k = this.pcoId;
    int m = HidlSupport.deepHashCode(Integer.valueOf(k));
    ArrayList<Byte> arrayList = this.contents;
    k = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(m), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".cid = ");
    stringBuilder.append(this.cid);
    stringBuilder.append(", .bearerProto = ");
    stringBuilder.append(this.bearerProto);
    stringBuilder.append(", .pcoId = ");
    stringBuilder.append(this.pcoId);
    stringBuilder.append(", .contents = ");
    stringBuilder.append(this.contents);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(48L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<PcoDataInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<PcoDataInfo> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 48);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      PcoDataInfo pcoDataInfo = new PcoDataInfo();
      pcoDataInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 48));
      arrayList.add(pcoDataInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.cid = paramHwBlob.getInt32(paramLong + 0L);
    String str = paramHwBlob.getString(paramLong + 8L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, false);
    this.pcoId = paramHwBlob.getInt32(paramLong + 24L);
    int i = paramHwBlob.getInt32(paramLong + 32L + 8L);
    l2 = (i * 1);
    l1 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 32L + 0L, true);
    this.contents.clear();
    for (byte b = 0; b < i; b++) {
      byte b1 = hwBlob.getInt8((b * 1));
      this.contents.add(Byte.valueOf(b1));
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(48);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<PcoDataInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 48);
    for (byte b = 0; b < i; b++)
      ((PcoDataInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 48)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.cid);
    paramHwBlob.putString(paramLong + 8L, this.bearerProto);
    paramHwBlob.putInt32(24L + paramLong, this.pcoId);
    int i = this.contents.size();
    paramHwBlob.putInt32(paramLong + 32L + 8L, i);
    paramHwBlob.putBool(paramLong + 32L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 1);
    for (byte b = 0; b < i; b++)
      hwBlob.putInt8((b * 1), ((Byte)this.contents.get(b)).byteValue()); 
    paramHwBlob.putBlob(32L + paramLong + 0L, hwBlob);
  }
}
