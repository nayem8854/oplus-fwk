package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class LceDataInfo {
  public int lastHopCapacityKbps = 0;
  
  public byte confidenceLevel = 0;
  
  public boolean lceSuspended = false;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != LceDataInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.lastHopCapacityKbps != ((LceDataInfo)paramObject).lastHopCapacityKbps)
      return false; 
    if (this.confidenceLevel != ((LceDataInfo)paramObject).confidenceLevel)
      return false; 
    if (this.lceSuspended != ((LceDataInfo)paramObject).lceSuspended)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.lastHopCapacityKbps;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    byte b = this.confidenceLevel;
    int j = HidlSupport.deepHashCode(Byte.valueOf(b));
    boolean bool = this.lceSuspended;
    int k = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".lastHopCapacityKbps = ");
    stringBuilder.append(this.lastHopCapacityKbps);
    stringBuilder.append(", .confidenceLevel = ");
    stringBuilder.append(this.confidenceLevel);
    stringBuilder.append(", .lceSuspended = ");
    stringBuilder.append(this.lceSuspended);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(8L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<LceDataInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<LceDataInfo> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 8);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      LceDataInfo lceDataInfo = new LceDataInfo();
      lceDataInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 8));
      arrayList.add(lceDataInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.lastHopCapacityKbps = paramHwBlob.getInt32(0L + paramLong);
    this.confidenceLevel = paramHwBlob.getInt8(4L + paramLong);
    this.lceSuspended = paramHwBlob.getBool(5L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(8);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<LceDataInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 8);
    for (byte b = 0; b < i; b++)
      ((LceDataInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 8)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.lastHopCapacityKbps);
    paramHwBlob.putInt8(4L + paramLong, this.confidenceLevel);
    paramHwBlob.putBool(5L + paramLong, this.lceSuspended);
  }
}
