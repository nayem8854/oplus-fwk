package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class SendSmsResult {
  public int messageRef = 0;
  
  public String ackPDU = new String();
  
  public int errorCode = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != SendSmsResult.class)
      return false; 
    paramObject = paramObject;
    if (this.messageRef != ((SendSmsResult)paramObject).messageRef)
      return false; 
    if (!HidlSupport.deepEquals(this.ackPDU, ((SendSmsResult)paramObject).ackPDU))
      return false; 
    if (this.errorCode != ((SendSmsResult)paramObject).errorCode)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.messageRef;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    String str = this.ackPDU;
    i = HidlSupport.deepHashCode(str);
    int k = this.errorCode;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(i), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".messageRef = ");
    stringBuilder.append(this.messageRef);
    stringBuilder.append(", .ackPDU = ");
    stringBuilder.append(this.ackPDU);
    stringBuilder.append(", .errorCode = ");
    stringBuilder.append(this.errorCode);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(32L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<SendSmsResult> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<SendSmsResult> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 32);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      SendSmsResult sendSmsResult = new SendSmsResult();
      sendSmsResult.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 32));
      arrayList.add(sendSmsResult);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.messageRef = paramHwBlob.getInt32(paramLong + 0L);
    String str = paramHwBlob.getString(paramLong + 8L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, false);
    this.errorCode = paramHwBlob.getInt32(paramLong + 24L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(32);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<SendSmsResult> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 32);
    for (byte b = 0; b < i; b++)
      ((SendSmsResult)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 32)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.messageRef);
    paramHwBlob.putString(8L + paramLong, this.ackPDU);
    paramHwBlob.putInt32(24L + paramLong, this.errorCode);
  }
}
