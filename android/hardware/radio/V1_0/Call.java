package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class Call {
  public int state = 0;
  
  public int index = 0;
  
  public int toa = 0;
  
  public boolean isMpty = false;
  
  public boolean isMT = false;
  
  public byte als = 0;
  
  public boolean isVoice = false;
  
  public boolean isVoicePrivacy = false;
  
  public String number = new String();
  
  public int numberPresentation = 0;
  
  public String name = new String();
  
  public int namePresentation = 0;
  
  public ArrayList<UusInfo> uusInfo = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != Call.class)
      return false; 
    paramObject = paramObject;
    if (this.state != ((Call)paramObject).state)
      return false; 
    if (this.index != ((Call)paramObject).index)
      return false; 
    if (this.toa != ((Call)paramObject).toa)
      return false; 
    if (this.isMpty != ((Call)paramObject).isMpty)
      return false; 
    if (this.isMT != ((Call)paramObject).isMT)
      return false; 
    if (this.als != ((Call)paramObject).als)
      return false; 
    if (this.isVoice != ((Call)paramObject).isVoice)
      return false; 
    if (this.isVoicePrivacy != ((Call)paramObject).isVoicePrivacy)
      return false; 
    if (!HidlSupport.deepEquals(this.number, ((Call)paramObject).number))
      return false; 
    if (this.numberPresentation != ((Call)paramObject).numberPresentation)
      return false; 
    if (!HidlSupport.deepEquals(this.name, ((Call)paramObject).name))
      return false; 
    if (this.namePresentation != ((Call)paramObject).namePresentation)
      return false; 
    if (!HidlSupport.deepEquals(this.uusInfo, ((Call)paramObject).uusInfo))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.state;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.index;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.toa;
    int m = HidlSupport.deepHashCode(Integer.valueOf(k));
    boolean bool = this.isMpty;
    int n = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    bool = this.isMT;
    int i1 = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    byte b = this.als;
    int i2 = HidlSupport.deepHashCode(Byte.valueOf(b));
    bool = this.isVoice;
    int i3 = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    bool = this.isVoicePrivacy;
    int i4 = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    String str = this.number;
    k = HidlSupport.deepHashCode(str);
    int i5 = this.numberPresentation;
    int i6 = HidlSupport.deepHashCode(Integer.valueOf(i5));
    str = this.name;
    i5 = HidlSupport.deepHashCode(str);
    int i7 = this.namePresentation;
    i7 = HidlSupport.deepHashCode(Integer.valueOf(i7));
    ArrayList<UusInfo> arrayList = this.uusInfo;
    int i8 = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { 
          Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(k), Integer.valueOf(i6), 
          Integer.valueOf(i5), Integer.valueOf(i7), Integer.valueOf(i8) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".state = ");
    stringBuilder.append(CallState.toString(this.state));
    stringBuilder.append(", .index = ");
    stringBuilder.append(this.index);
    stringBuilder.append(", .toa = ");
    stringBuilder.append(this.toa);
    stringBuilder.append(", .isMpty = ");
    stringBuilder.append(this.isMpty);
    stringBuilder.append(", .isMT = ");
    stringBuilder.append(this.isMT);
    stringBuilder.append(", .als = ");
    stringBuilder.append(this.als);
    stringBuilder.append(", .isVoice = ");
    stringBuilder.append(this.isVoice);
    stringBuilder.append(", .isVoicePrivacy = ");
    stringBuilder.append(this.isVoicePrivacy);
    stringBuilder.append(", .number = ");
    stringBuilder.append(this.number);
    stringBuilder.append(", .numberPresentation = ");
    stringBuilder.append(CallPresentation.toString(this.numberPresentation));
    stringBuilder.append(", .name = ");
    stringBuilder.append(this.name);
    stringBuilder.append(", .namePresentation = ");
    stringBuilder.append(CallPresentation.toString(this.namePresentation));
    stringBuilder.append(", .uusInfo = ");
    stringBuilder.append(this.uusInfo);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(88L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<Call> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<Call> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 88);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      Call call = new Call();
      call.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 88));
      arrayList.add(call);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.state = paramHwBlob.getInt32(paramLong + 0L);
    this.index = paramHwBlob.getInt32(paramLong + 4L);
    this.toa = paramHwBlob.getInt32(paramLong + 8L);
    this.isMpty = paramHwBlob.getBool(paramLong + 12L);
    this.isMT = paramHwBlob.getBool(paramLong + 13L);
    this.als = paramHwBlob.getInt8(paramLong + 14L);
    this.isVoice = paramHwBlob.getBool(paramLong + 15L);
    this.isVoicePrivacy = paramHwBlob.getBool(paramLong + 16L);
    String str = paramHwBlob.getString(paramLong + 24L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 24L + 0L, false);
    this.numberPresentation = paramHwBlob.getInt32(paramLong + 40L);
    this.name = str = paramHwBlob.getString(paramLong + 48L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 48L + 0L, false);
    this.namePresentation = paramHwBlob.getInt32(paramLong + 64L);
    int i = paramHwBlob.getInt32(paramLong + 72L + 8L);
    l1 = (i * 24);
    l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 72L + 0L, true);
    this.uusInfo.clear();
    for (byte b = 0; b < i; b++) {
      UusInfo uusInfo = new UusInfo();
      uusInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 24));
      this.uusInfo.add(uusInfo);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(88);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<Call> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 88);
    for (byte b = 0; b < i; b++)
      ((Call)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 88)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.state);
    paramHwBlob.putInt32(4L + paramLong, this.index);
    paramHwBlob.putInt32(paramLong + 8L, this.toa);
    paramHwBlob.putBool(paramLong + 12L, this.isMpty);
    paramHwBlob.putBool(13L + paramLong, this.isMT);
    paramHwBlob.putInt8(14L + paramLong, this.als);
    paramHwBlob.putBool(15L + paramLong, this.isVoice);
    paramHwBlob.putBool(16L + paramLong, this.isVoicePrivacy);
    paramHwBlob.putString(24L + paramLong, this.number);
    paramHwBlob.putInt32(40L + paramLong, this.numberPresentation);
    paramHwBlob.putString(48L + paramLong, this.name);
    paramHwBlob.putInt32(64L + paramLong, this.namePresentation);
    int i = this.uusInfo.size();
    paramHwBlob.putInt32(paramLong + 72L + 8L, i);
    paramHwBlob.putBool(paramLong + 72L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 24);
    for (byte b = 0; b < i; b++)
      ((UusInfo)this.uusInfo.get(b)).writeEmbeddedToBlob(hwBlob, (b * 24)); 
    paramHwBlob.putBlob(72L + paramLong + 0L, hwBlob);
  }
}
