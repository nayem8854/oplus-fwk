package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class LteSignalStrength {
  public int signalStrength = 0;
  
  public int rsrp = 0;
  
  public int rsrq = 0;
  
  public int rssnr = 0;
  
  public int cqi = 0;
  
  public int timingAdvance = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != LteSignalStrength.class)
      return false; 
    paramObject = paramObject;
    if (this.signalStrength != ((LteSignalStrength)paramObject).signalStrength)
      return false; 
    if (this.rsrp != ((LteSignalStrength)paramObject).rsrp)
      return false; 
    if (this.rsrq != ((LteSignalStrength)paramObject).rsrq)
      return false; 
    if (this.rssnr != ((LteSignalStrength)paramObject).rssnr)
      return false; 
    if (this.cqi != ((LteSignalStrength)paramObject).cqi)
      return false; 
    if (this.timingAdvance != ((LteSignalStrength)paramObject).timingAdvance)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.signalStrength;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.rsrp;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.rsrq;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.rssnr;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    int n = this.cqi;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    int i1 = this.timingAdvance;
    i1 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".signalStrength = ");
    stringBuilder.append(this.signalStrength);
    stringBuilder.append(", .rsrp = ");
    stringBuilder.append(this.rsrp);
    stringBuilder.append(", .rsrq = ");
    stringBuilder.append(this.rsrq);
    stringBuilder.append(", .rssnr = ");
    stringBuilder.append(this.rssnr);
    stringBuilder.append(", .cqi = ");
    stringBuilder.append(this.cqi);
    stringBuilder.append(", .timingAdvance = ");
    stringBuilder.append(this.timingAdvance);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(24L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<LteSignalStrength> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<LteSignalStrength> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 24);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      LteSignalStrength lteSignalStrength = new LteSignalStrength();
      lteSignalStrength.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 24));
      arrayList.add(lteSignalStrength);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.signalStrength = paramHwBlob.getInt32(0L + paramLong);
    this.rsrp = paramHwBlob.getInt32(4L + paramLong);
    this.rsrq = paramHwBlob.getInt32(8L + paramLong);
    this.rssnr = paramHwBlob.getInt32(12L + paramLong);
    this.cqi = paramHwBlob.getInt32(16L + paramLong);
    this.timingAdvance = paramHwBlob.getInt32(20L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(24);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<LteSignalStrength> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 24);
    for (byte b = 0; b < i; b++)
      ((LteSignalStrength)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 24)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.signalStrength);
    paramHwBlob.putInt32(4L + paramLong, this.rsrp);
    paramHwBlob.putInt32(8L + paramLong, this.rsrq);
    paramHwBlob.putInt32(12L + paramLong, this.rssnr);
    paramHwBlob.putInt32(16L + paramLong, this.cqi);
    paramHwBlob.putInt32(20L + paramLong, this.timingAdvance);
  }
}
