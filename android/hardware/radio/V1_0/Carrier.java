package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class Carrier {
  public String mcc = new String();
  
  public String mnc = new String();
  
  public int matchType = 0;
  
  public String matchData = new String();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != Carrier.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.mcc, ((Carrier)paramObject).mcc))
      return false; 
    if (!HidlSupport.deepEquals(this.mnc, ((Carrier)paramObject).mnc))
      return false; 
    if (this.matchType != ((Carrier)paramObject).matchType)
      return false; 
    if (!HidlSupport.deepEquals(this.matchData, ((Carrier)paramObject).matchData))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    String str = this.mcc;
    int i = HidlSupport.deepHashCode(str);
    str = this.mnc;
    int j = HidlSupport.deepHashCode(str), k = this.matchType;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    str = this.matchData;
    int m = HidlSupport.deepHashCode(str);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".mcc = ");
    stringBuilder.append(this.mcc);
    stringBuilder.append(", .mnc = ");
    stringBuilder.append(this.mnc);
    stringBuilder.append(", .matchType = ");
    stringBuilder.append(CarrierMatchType.toString(this.matchType));
    stringBuilder.append(", .matchData = ");
    stringBuilder.append(this.matchData);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(56L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<Carrier> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<Carrier> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 56);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      Carrier carrier = new Carrier();
      carrier.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 56));
      arrayList.add(carrier);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    String str = paramHwBlob.getString(paramLong + 0L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, false);
    this.mnc = str = paramHwBlob.getString(paramLong + 16L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 16L + 0L, false);
    this.matchType = paramHwBlob.getInt32(paramLong + 32L);
    this.matchData = str = paramHwBlob.getString(paramLong + 40L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 40L + 0L, false);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(56);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<Carrier> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 56);
    for (byte b = 0; b < i; b++)
      ((Carrier)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 56)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putString(0L + paramLong, this.mcc);
    paramHwBlob.putString(16L + paramLong, this.mnc);
    paramHwBlob.putInt32(32L + paramLong, this.matchType);
    paramHwBlob.putString(40L + paramLong, this.matchData);
  }
}
