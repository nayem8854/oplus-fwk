package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class RadioCapability {
  public int session = 0;
  
  public int phase = 0;
  
  public String logicalModemUuid = new String();
  
  public int status = 0;
  
  public int raf;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != RadioCapability.class)
      return false; 
    paramObject = paramObject;
    if (this.session != ((RadioCapability)paramObject).session)
      return false; 
    if (this.phase != ((RadioCapability)paramObject).phase)
      return false; 
    if (!HidlSupport.deepEquals(Integer.valueOf(this.raf), Integer.valueOf(((RadioCapability)paramObject).raf)))
      return false; 
    if (!HidlSupport.deepEquals(this.logicalModemUuid, ((RadioCapability)paramObject).logicalModemUuid))
      return false; 
    if (this.status != ((RadioCapability)paramObject).status)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.session;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.phase;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.raf;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    String str = this.logicalModemUuid;
    int m = HidlSupport.deepHashCode(str), n = this.status;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".session = ");
    stringBuilder.append(this.session);
    stringBuilder.append(", .phase = ");
    stringBuilder.append(RadioCapabilityPhase.toString(this.phase));
    stringBuilder.append(", .raf = ");
    stringBuilder.append(RadioAccessFamily.dumpBitfield(this.raf));
    stringBuilder.append(", .logicalModemUuid = ");
    stringBuilder.append(this.logicalModemUuid);
    stringBuilder.append(", .status = ");
    stringBuilder.append(RadioCapabilityStatus.toString(this.status));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(40L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<RadioCapability> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<RadioCapability> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 40);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      RadioCapability radioCapability = new RadioCapability();
      radioCapability.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 40));
      arrayList.add(radioCapability);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.session = paramHwBlob.getInt32(paramLong + 0L);
    this.phase = paramHwBlob.getInt32(paramLong + 4L);
    this.raf = paramHwBlob.getInt32(paramLong + 8L);
    String str = paramHwBlob.getString(paramLong + 16L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 16L + 0L, false);
    this.status = paramHwBlob.getInt32(paramLong + 32L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(40);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<RadioCapability> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 40);
    for (byte b = 0; b < i; b++)
      ((RadioCapability)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 40)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.session);
    paramHwBlob.putInt32(4L + paramLong, this.phase);
    paramHwBlob.putInt32(8L + paramLong, this.raf);
    paramHwBlob.putString(16L + paramLong, this.logicalModemUuid);
    paramHwBlob.putInt32(32L + paramLong, this.status);
  }
}
