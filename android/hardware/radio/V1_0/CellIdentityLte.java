package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellIdentityLte {
  public String mcc = new String();
  
  public String mnc = new String();
  
  public int ci = 0;
  
  public int pci = 0;
  
  public int tac = 0;
  
  public int earfcn = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellIdentityLte.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.mcc, ((CellIdentityLte)paramObject).mcc))
      return false; 
    if (!HidlSupport.deepEquals(this.mnc, ((CellIdentityLte)paramObject).mnc))
      return false; 
    if (this.ci != ((CellIdentityLte)paramObject).ci)
      return false; 
    if (this.pci != ((CellIdentityLte)paramObject).pci)
      return false; 
    if (this.tac != ((CellIdentityLte)paramObject).tac)
      return false; 
    if (this.earfcn != ((CellIdentityLte)paramObject).earfcn)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    String str = this.mcc;
    int i = HidlSupport.deepHashCode(str);
    str = this.mnc;
    int j = HidlSupport.deepHashCode(str), k = this.ci;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.pci;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    int n = this.tac;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    int i1 = this.earfcn;
    i1 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".mcc = ");
    stringBuilder.append(this.mcc);
    stringBuilder.append(", .mnc = ");
    stringBuilder.append(this.mnc);
    stringBuilder.append(", .ci = ");
    stringBuilder.append(this.ci);
    stringBuilder.append(", .pci = ");
    stringBuilder.append(this.pci);
    stringBuilder.append(", .tac = ");
    stringBuilder.append(this.tac);
    stringBuilder.append(", .earfcn = ");
    stringBuilder.append(this.earfcn);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(48L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellIdentityLte> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellIdentityLte> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 48);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellIdentityLte cellIdentityLte = new CellIdentityLte();
      cellIdentityLte.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 48));
      arrayList.add(cellIdentityLte);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    String str = paramHwBlob.getString(paramLong + 0L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, false);
    this.mnc = str = paramHwBlob.getString(paramLong + 16L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 16L + 0L, false);
    this.ci = paramHwBlob.getInt32(paramLong + 32L);
    this.pci = paramHwBlob.getInt32(paramLong + 36L);
    this.tac = paramHwBlob.getInt32(paramLong + 40L);
    this.earfcn = paramHwBlob.getInt32(paramLong + 44L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(48);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellIdentityLte> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 48);
    for (byte b = 0; b < i; b++)
      ((CellIdentityLte)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 48)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putString(0L + paramLong, this.mcc);
    paramHwBlob.putString(16L + paramLong, this.mnc);
    paramHwBlob.putInt32(32L + paramLong, this.ci);
    paramHwBlob.putInt32(36L + paramLong, this.pci);
    paramHwBlob.putInt32(40L + paramLong, this.tac);
    paramHwBlob.putInt32(44L + paramLong, this.earfcn);
  }
}
