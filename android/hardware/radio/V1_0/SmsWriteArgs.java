package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class SmsWriteArgs {
  public int status = 0;
  
  public String pdu = new String();
  
  public String smsc = new String();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != SmsWriteArgs.class)
      return false; 
    paramObject = paramObject;
    if (this.status != ((SmsWriteArgs)paramObject).status)
      return false; 
    if (!HidlSupport.deepEquals(this.pdu, ((SmsWriteArgs)paramObject).pdu))
      return false; 
    if (!HidlSupport.deepEquals(this.smsc, ((SmsWriteArgs)paramObject).smsc))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.status;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    String str = this.pdu;
    int k = HidlSupport.deepHashCode(str);
    str = this.smsc;
    i = HidlSupport.deepHashCode(str);
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(i) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".status = ");
    stringBuilder.append(SmsWriteArgsStatus.toString(this.status));
    stringBuilder.append(", .pdu = ");
    stringBuilder.append(this.pdu);
    stringBuilder.append(", .smsc = ");
    stringBuilder.append(this.smsc);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(40L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<SmsWriteArgs> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<SmsWriteArgs> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 40);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      SmsWriteArgs smsWriteArgs = new SmsWriteArgs();
      smsWriteArgs.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 40));
      arrayList.add(smsWriteArgs);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.status = paramHwBlob.getInt32(paramLong + 0L);
    String str = paramHwBlob.getString(paramLong + 8L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, false);
    this.smsc = str = paramHwBlob.getString(paramLong + 24L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 24L + 0L, false);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(40);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<SmsWriteArgs> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 40);
    for (byte b = 0; b < i; b++)
      ((SmsWriteArgs)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 40)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.status);
    paramHwBlob.putString(8L + paramLong, this.pdu);
    paramHwBlob.putString(24L + paramLong, this.smsc);
  }
}
