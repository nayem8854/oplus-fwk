package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellInfoTdscdma {
  public CellIdentityTdscdma cellIdentityTdscdma = new CellIdentityTdscdma();
  
  public TdScdmaSignalStrength signalStrengthTdscdma = new TdScdmaSignalStrength();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellInfoTdscdma.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.cellIdentityTdscdma, ((CellInfoTdscdma)paramObject).cellIdentityTdscdma))
      return false; 
    if (!HidlSupport.deepEquals(this.signalStrengthTdscdma, ((CellInfoTdscdma)paramObject).signalStrengthTdscdma))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    CellIdentityTdscdma cellIdentityTdscdma = this.cellIdentityTdscdma;
    int i = HidlSupport.deepHashCode(cellIdentityTdscdma);
    TdScdmaSignalStrength tdScdmaSignalStrength = this.signalStrengthTdscdma;
    int j = HidlSupport.deepHashCode(tdScdmaSignalStrength);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".cellIdentityTdscdma = ");
    stringBuilder.append(this.cellIdentityTdscdma);
    stringBuilder.append(", .signalStrengthTdscdma = ");
    stringBuilder.append(this.signalStrengthTdscdma);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(56L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellInfoTdscdma> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellInfoTdscdma> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 56);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellInfoTdscdma cellInfoTdscdma = new CellInfoTdscdma();
      cellInfoTdscdma.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 56));
      arrayList.add(cellInfoTdscdma);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.cellIdentityTdscdma.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 0L + paramLong);
    this.signalStrengthTdscdma.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 48L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(56);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellInfoTdscdma> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 56);
    for (byte b = 0; b < i; b++)
      ((CellInfoTdscdma)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 56)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.cellIdentityTdscdma.writeEmbeddedToBlob(paramHwBlob, 0L + paramLong);
    this.signalStrengthTdscdma.writeEmbeddedToBlob(paramHwBlob, 48L + paramLong);
  }
}
