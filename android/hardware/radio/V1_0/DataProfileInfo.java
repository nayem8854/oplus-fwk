package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class DataProfileInfo {
  public int profileId = 0;
  
  public String apn = new String();
  
  public String protocol = new String();
  
  public String roamingProtocol = new String();
  
  public int authType = 0;
  
  public String user = new String();
  
  public String password = new String();
  
  public int type = 0;
  
  public int maxConnsTime = 0;
  
  public int maxConns = 0;
  
  public int waitTime = 0;
  
  public boolean enabled = false;
  
  public int mtu = 0;
  
  public int mvnoType = 0;
  
  public String mvnoMatchData = new String();
  
  public int bearerBitmap;
  
  public int supportedApnTypesBitmap;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != DataProfileInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.profileId != ((DataProfileInfo)paramObject).profileId)
      return false; 
    if (!HidlSupport.deepEquals(this.apn, ((DataProfileInfo)paramObject).apn))
      return false; 
    if (!HidlSupport.deepEquals(this.protocol, ((DataProfileInfo)paramObject).protocol))
      return false; 
    if (!HidlSupport.deepEquals(this.roamingProtocol, ((DataProfileInfo)paramObject).roamingProtocol))
      return false; 
    if (this.authType != ((DataProfileInfo)paramObject).authType)
      return false; 
    if (!HidlSupport.deepEquals(this.user, ((DataProfileInfo)paramObject).user))
      return false; 
    if (!HidlSupport.deepEquals(this.password, ((DataProfileInfo)paramObject).password))
      return false; 
    if (this.type != ((DataProfileInfo)paramObject).type)
      return false; 
    if (this.maxConnsTime != ((DataProfileInfo)paramObject).maxConnsTime)
      return false; 
    if (this.maxConns != ((DataProfileInfo)paramObject).maxConns)
      return false; 
    if (this.waitTime != ((DataProfileInfo)paramObject).waitTime)
      return false; 
    if (this.enabled != ((DataProfileInfo)paramObject).enabled)
      return false; 
    if (!HidlSupport.deepEquals(Integer.valueOf(this.supportedApnTypesBitmap), Integer.valueOf(((DataProfileInfo)paramObject).supportedApnTypesBitmap)))
      return false; 
    if (!HidlSupport.deepEquals(Integer.valueOf(this.bearerBitmap), Integer.valueOf(((DataProfileInfo)paramObject).bearerBitmap)))
      return false; 
    if (this.mtu != ((DataProfileInfo)paramObject).mtu)
      return false; 
    if (this.mvnoType != ((DataProfileInfo)paramObject).mvnoType)
      return false; 
    if (!HidlSupport.deepEquals(this.mvnoMatchData, ((DataProfileInfo)paramObject).mvnoMatchData))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.profileId;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    String str = this.apn;
    int j = HidlSupport.deepHashCode(str);
    str = this.protocol;
    int k = HidlSupport.deepHashCode(str);
    str = this.roamingProtocol;
    int m = HidlSupport.deepHashCode(str), n = this.authType;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    str = this.user;
    int i1 = HidlSupport.deepHashCode(str);
    str = this.password;
    int i2 = HidlSupport.deepHashCode(str), i3 = this.type;
    i3 = HidlSupport.deepHashCode(Integer.valueOf(i3));
    int i4 = this.maxConnsTime;
    i4 = HidlSupport.deepHashCode(Integer.valueOf(i4));
    int i5 = this.maxConns;
    i5 = HidlSupport.deepHashCode(Integer.valueOf(i5));
    int i6 = this.waitTime;
    int i7 = HidlSupport.deepHashCode(Integer.valueOf(i6));
    boolean bool = this.enabled;
    i6 = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    int i8 = this.supportedApnTypesBitmap;
    i8 = HidlSupport.deepHashCode(Integer.valueOf(i8));
    int i9 = this.bearerBitmap;
    i9 = HidlSupport.deepHashCode(Integer.valueOf(i9));
    int i10 = this.mtu;
    i10 = HidlSupport.deepHashCode(Integer.valueOf(i10));
    int i11 = this.mvnoType;
    int i12 = HidlSupport.deepHashCode(Integer.valueOf(i11));
    str = this.mvnoMatchData;
    i11 = HidlSupport.deepHashCode(str);
    return Objects.hash(new Object[] { 
          Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), 
          Integer.valueOf(i7), Integer.valueOf(i6), Integer.valueOf(i8), Integer.valueOf(i9), Integer.valueOf(i10), Integer.valueOf(i12), Integer.valueOf(i11) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".profileId = ");
    stringBuilder.append(DataProfileId.toString(this.profileId));
    stringBuilder.append(", .apn = ");
    stringBuilder.append(this.apn);
    stringBuilder.append(", .protocol = ");
    stringBuilder.append(this.protocol);
    stringBuilder.append(", .roamingProtocol = ");
    stringBuilder.append(this.roamingProtocol);
    stringBuilder.append(", .authType = ");
    stringBuilder.append(ApnAuthType.toString(this.authType));
    stringBuilder.append(", .user = ");
    stringBuilder.append(this.user);
    stringBuilder.append(", .password = ");
    stringBuilder.append(this.password);
    stringBuilder.append(", .type = ");
    stringBuilder.append(DataProfileInfoType.toString(this.type));
    stringBuilder.append(", .maxConnsTime = ");
    stringBuilder.append(this.maxConnsTime);
    stringBuilder.append(", .maxConns = ");
    stringBuilder.append(this.maxConns);
    stringBuilder.append(", .waitTime = ");
    stringBuilder.append(this.waitTime);
    stringBuilder.append(", .enabled = ");
    stringBuilder.append(this.enabled);
    stringBuilder.append(", .supportedApnTypesBitmap = ");
    stringBuilder.append(ApnTypes.dumpBitfield(this.supportedApnTypesBitmap));
    stringBuilder.append(", .bearerBitmap = ");
    stringBuilder.append(RadioAccessFamily.dumpBitfield(this.bearerBitmap));
    stringBuilder.append(", .mtu = ");
    stringBuilder.append(this.mtu);
    stringBuilder.append(", .mvnoType = ");
    stringBuilder.append(MvnoType.toString(this.mvnoType));
    stringBuilder.append(", .mvnoMatchData = ");
    stringBuilder.append(this.mvnoMatchData);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(152L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<DataProfileInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<DataProfileInfo> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 152);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      DataProfileInfo dataProfileInfo = new DataProfileInfo();
      dataProfileInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 152));
      arrayList.add(dataProfileInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.profileId = paramHwBlob.getInt32(paramLong + 0L);
    String str = paramHwBlob.getString(paramLong + 8L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, false);
    this.protocol = str = paramHwBlob.getString(paramLong + 24L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 24L + 0L, false);
    this.roamingProtocol = str = paramHwBlob.getString(paramLong + 40L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 40L + 0L, false);
    this.authType = paramHwBlob.getInt32(paramLong + 56L);
    this.user = str = paramHwBlob.getString(paramLong + 64L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 64L + 0L, false);
    this.password = str = paramHwBlob.getString(paramLong + 80L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 80L + 0L, false);
    this.type = paramHwBlob.getInt32(paramLong + 96L);
    this.maxConnsTime = paramHwBlob.getInt32(paramLong + 100L);
    this.maxConns = paramHwBlob.getInt32(paramLong + 104L);
    this.waitTime = paramHwBlob.getInt32(paramLong + 108L);
    this.enabled = paramHwBlob.getBool(paramLong + 112L);
    this.supportedApnTypesBitmap = paramHwBlob.getInt32(paramLong + 116L);
    this.bearerBitmap = paramHwBlob.getInt32(paramLong + 120L);
    this.mtu = paramHwBlob.getInt32(paramLong + 124L);
    this.mvnoType = paramHwBlob.getInt32(paramLong + 128L);
    this.mvnoMatchData = str = paramHwBlob.getString(paramLong + 136L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 136L + 0L, false);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(152);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<DataProfileInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 152);
    for (byte b = 0; b < i; b++)
      ((DataProfileInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 152)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.profileId);
    paramHwBlob.putString(8L + paramLong, this.apn);
    paramHwBlob.putString(24L + paramLong, this.protocol);
    paramHwBlob.putString(40L + paramLong, this.roamingProtocol);
    paramHwBlob.putInt32(56L + paramLong, this.authType);
    paramHwBlob.putString(64L + paramLong, this.user);
    paramHwBlob.putString(80L + paramLong, this.password);
    paramHwBlob.putInt32(96L + paramLong, this.type);
    paramHwBlob.putInt32(100L + paramLong, this.maxConnsTime);
    paramHwBlob.putInt32(104L + paramLong, this.maxConns);
    paramHwBlob.putInt32(108L + paramLong, this.waitTime);
    paramHwBlob.putBool(112L + paramLong, this.enabled);
    paramHwBlob.putInt32(116L + paramLong, this.supportedApnTypesBitmap);
    paramHwBlob.putInt32(120L + paramLong, this.bearerBitmap);
    paramHwBlob.putInt32(124L + paramLong, this.mtu);
    paramHwBlob.putInt32(128L + paramLong, this.mvnoType);
    paramHwBlob.putString(136L + paramLong, this.mvnoMatchData);
  }
}
