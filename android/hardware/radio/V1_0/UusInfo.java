package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class UusInfo {
  public int uusType = 0;
  
  public int uusDcs = 0;
  
  public String uusData = new String();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != UusInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.uusType != ((UusInfo)paramObject).uusType)
      return false; 
    if (this.uusDcs != ((UusInfo)paramObject).uusDcs)
      return false; 
    if (!HidlSupport.deepEquals(this.uusData, ((UusInfo)paramObject).uusData))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.uusType;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.uusDcs;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    String str = this.uusData;
    int k = HidlSupport.deepHashCode(str);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".uusType = ");
    stringBuilder.append(UusType.toString(this.uusType));
    stringBuilder.append(", .uusDcs = ");
    stringBuilder.append(UusDcs.toString(this.uusDcs));
    stringBuilder.append(", .uusData = ");
    stringBuilder.append(this.uusData);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(24L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<UusInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<UusInfo> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 24);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      UusInfo uusInfo = new UusInfo();
      uusInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 24));
      arrayList.add(uusInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.uusType = paramHwBlob.getInt32(paramLong + 0L);
    this.uusDcs = paramHwBlob.getInt32(paramLong + 4L);
    String str = paramHwBlob.getString(paramLong + 8L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, false);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(24);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<UusInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 24);
    for (byte b = 0; b < i; b++)
      ((UusInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 24)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.uusType);
    paramHwBlob.putInt32(4L + paramLong, this.uusDcs);
    paramHwBlob.putString(8L + paramLong, this.uusData);
  }
}
