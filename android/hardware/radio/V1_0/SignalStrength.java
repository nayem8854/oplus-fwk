package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class SignalStrength {
  public GsmSignalStrength gw = new GsmSignalStrength();
  
  public CdmaSignalStrength cdma = new CdmaSignalStrength();
  
  public EvdoSignalStrength evdo = new EvdoSignalStrength();
  
  public LteSignalStrength lte = new LteSignalStrength();
  
  public TdScdmaSignalStrength tdScdma = new TdScdmaSignalStrength();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != SignalStrength.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.gw, ((SignalStrength)paramObject).gw))
      return false; 
    if (!HidlSupport.deepEquals(this.cdma, ((SignalStrength)paramObject).cdma))
      return false; 
    if (!HidlSupport.deepEquals(this.evdo, ((SignalStrength)paramObject).evdo))
      return false; 
    if (!HidlSupport.deepEquals(this.lte, ((SignalStrength)paramObject).lte))
      return false; 
    if (!HidlSupport.deepEquals(this.tdScdma, ((SignalStrength)paramObject).tdScdma))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    GsmSignalStrength gsmSignalStrength = this.gw;
    int i = HidlSupport.deepHashCode(gsmSignalStrength);
    CdmaSignalStrength cdmaSignalStrength = this.cdma;
    int j = HidlSupport.deepHashCode(cdmaSignalStrength);
    EvdoSignalStrength evdoSignalStrength = this.evdo;
    int k = HidlSupport.deepHashCode(evdoSignalStrength);
    LteSignalStrength lteSignalStrength = this.lte;
    int m = HidlSupport.deepHashCode(lteSignalStrength);
    TdScdmaSignalStrength tdScdmaSignalStrength = this.tdScdma;
    int n = HidlSupport.deepHashCode(tdScdmaSignalStrength);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".gw = ");
    stringBuilder.append(this.gw);
    stringBuilder.append(", .cdma = ");
    stringBuilder.append(this.cdma);
    stringBuilder.append(", .evdo = ");
    stringBuilder.append(this.evdo);
    stringBuilder.append(", .lte = ");
    stringBuilder.append(this.lte);
    stringBuilder.append(", .tdScdma = ");
    stringBuilder.append(this.tdScdma);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(60L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<SignalStrength> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<SignalStrength> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 60);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      SignalStrength signalStrength = new SignalStrength();
      signalStrength.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 60));
      arrayList.add(signalStrength);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.gw.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 0L + paramLong);
    this.cdma.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 12L + paramLong);
    this.evdo.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 20L + paramLong);
    this.lte.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 32L + paramLong);
    this.tdScdma.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 56L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(60);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<SignalStrength> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 60);
    for (byte b = 0; b < i; b++)
      ((SignalStrength)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 60)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.gw.writeEmbeddedToBlob(paramHwBlob, 0L + paramLong);
    this.cdma.writeEmbeddedToBlob(paramHwBlob, 12L + paramLong);
    this.evdo.writeEmbeddedToBlob(paramHwBlob, 20L + paramLong);
    this.lte.writeEmbeddedToBlob(paramHwBlob, 32L + paramLong);
    this.tdScdma.writeEmbeddedToBlob(paramHwBlob, 56L + paramLong);
  }
}
