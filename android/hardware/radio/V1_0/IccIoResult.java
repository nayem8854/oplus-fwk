package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class IccIoResult {
  public int sw1 = 0;
  
  public int sw2 = 0;
  
  public String simResponse = new String();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != IccIoResult.class)
      return false; 
    paramObject = paramObject;
    if (this.sw1 != ((IccIoResult)paramObject).sw1)
      return false; 
    if (this.sw2 != ((IccIoResult)paramObject).sw2)
      return false; 
    if (!HidlSupport.deepEquals(this.simResponse, ((IccIoResult)paramObject).simResponse))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.sw1;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.sw2;
    int k = HidlSupport.deepHashCode(Integer.valueOf(j));
    String str = this.simResponse;
    j = HidlSupport.deepHashCode(str);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(k), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".sw1 = ");
    stringBuilder.append(this.sw1);
    stringBuilder.append(", .sw2 = ");
    stringBuilder.append(this.sw2);
    stringBuilder.append(", .simResponse = ");
    stringBuilder.append(this.simResponse);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(24L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<IccIoResult> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<IccIoResult> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 24);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      IccIoResult iccIoResult = new IccIoResult();
      iccIoResult.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 24));
      arrayList.add(iccIoResult);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.sw1 = paramHwBlob.getInt32(paramLong + 0L);
    this.sw2 = paramHwBlob.getInt32(paramLong + 4L);
    String str = paramHwBlob.getString(paramLong + 8L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, false);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(24);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<IccIoResult> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 24);
    for (byte b = 0; b < i; b++)
      ((IccIoResult)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 24)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.sw1);
    paramHwBlob.putInt32(4L + paramLong, this.sw2);
    paramHwBlob.putString(8L + paramLong, this.simResponse);
  }
}
