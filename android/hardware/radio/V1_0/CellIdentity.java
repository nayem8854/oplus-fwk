package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellIdentity {
  public int cellInfoType = 0;
  
  public ArrayList<CellIdentityGsm> cellIdentityGsm = new ArrayList<>();
  
  public ArrayList<CellIdentityWcdma> cellIdentityWcdma = new ArrayList<>();
  
  public ArrayList<CellIdentityCdma> cellIdentityCdma = new ArrayList<>();
  
  public ArrayList<CellIdentityLte> cellIdentityLte = new ArrayList<>();
  
  public ArrayList<CellIdentityTdscdma> cellIdentityTdscdma = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellIdentity.class)
      return false; 
    paramObject = paramObject;
    if (this.cellInfoType != ((CellIdentity)paramObject).cellInfoType)
      return false; 
    if (!HidlSupport.deepEquals(this.cellIdentityGsm, ((CellIdentity)paramObject).cellIdentityGsm))
      return false; 
    if (!HidlSupport.deepEquals(this.cellIdentityWcdma, ((CellIdentity)paramObject).cellIdentityWcdma))
      return false; 
    if (!HidlSupport.deepEquals(this.cellIdentityCdma, ((CellIdentity)paramObject).cellIdentityCdma))
      return false; 
    if (!HidlSupport.deepEquals(this.cellIdentityLte, ((CellIdentity)paramObject).cellIdentityLte))
      return false; 
    if (!HidlSupport.deepEquals(this.cellIdentityTdscdma, ((CellIdentity)paramObject).cellIdentityTdscdma))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.cellInfoType;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    ArrayList<CellIdentityGsm> arrayList4 = this.cellIdentityGsm;
    int k = HidlSupport.deepHashCode(arrayList4);
    ArrayList<CellIdentityWcdma> arrayList3 = this.cellIdentityWcdma;
    int m = HidlSupport.deepHashCode(arrayList3);
    ArrayList<CellIdentityCdma> arrayList2 = this.cellIdentityCdma;
    int n = HidlSupport.deepHashCode(arrayList2);
    ArrayList<CellIdentityLte> arrayList1 = this.cellIdentityLte;
    int i1 = HidlSupport.deepHashCode(arrayList1);
    ArrayList<CellIdentityTdscdma> arrayList = this.cellIdentityTdscdma;
    i = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".cellInfoType = ");
    stringBuilder.append(CellInfoType.toString(this.cellInfoType));
    stringBuilder.append(", .cellIdentityGsm = ");
    stringBuilder.append(this.cellIdentityGsm);
    stringBuilder.append(", .cellIdentityWcdma = ");
    stringBuilder.append(this.cellIdentityWcdma);
    stringBuilder.append(", .cellIdentityCdma = ");
    stringBuilder.append(this.cellIdentityCdma);
    stringBuilder.append(", .cellIdentityLte = ");
    stringBuilder.append(this.cellIdentityLte);
    stringBuilder.append(", .cellIdentityTdscdma = ");
    stringBuilder.append(this.cellIdentityTdscdma);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(88L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellIdentity> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellIdentity> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 88);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellIdentity cellIdentity = new CellIdentity();
      cellIdentity.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 88));
      arrayList.add(cellIdentity);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.cellInfoType = paramHwBlob.getInt32(paramLong + 0L);
    int i = paramHwBlob.getInt32(paramLong + 8L + 8L);
    long l1 = (i * 48);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob1 = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, true);
    this.cellIdentityGsm.clear();
    byte b;
    for (b = 0; b < i; b++) {
      CellIdentityGsm cellIdentityGsm = new CellIdentityGsm();
      cellIdentityGsm.readEmbeddedFromParcel(paramHwParcel, hwBlob1, (b * 48));
      this.cellIdentityGsm.add(cellIdentityGsm);
    } 
    i = paramHwBlob.getInt32(paramLong + 24L + 8L);
    l1 = (i * 48);
    l2 = paramHwBlob.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 24L + 0L, true);
    this.cellIdentityWcdma.clear();
    for (b = 0; b < i; b++) {
      CellIdentityWcdma cellIdentityWcdma = new CellIdentityWcdma();
      cellIdentityWcdma.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 48));
      this.cellIdentityWcdma.add(cellIdentityWcdma);
    } 
    i = paramHwBlob.getInt32(paramLong + 40L + 8L);
    l2 = (i * 20);
    l1 = paramHwBlob.handle();
    hwBlob2 = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 40L + 0L, true);
    this.cellIdentityCdma.clear();
    for (b = 0; b < i; b++) {
      CellIdentityCdma cellIdentityCdma = new CellIdentityCdma();
      cellIdentityCdma.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 20));
      this.cellIdentityCdma.add(cellIdentityCdma);
    } 
    i = paramHwBlob.getInt32(paramLong + 56L + 8L);
    l1 = (i * 48);
    l2 = paramHwBlob.handle();
    hwBlob1 = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 56L + 0L, true);
    this.cellIdentityLte.clear();
    for (b = 0; b < i; b++) {
      CellIdentityLte cellIdentityLte = new CellIdentityLte();
      cellIdentityLte.readEmbeddedFromParcel(paramHwParcel, hwBlob1, (b * 48));
      this.cellIdentityLte.add(cellIdentityLte);
    } 
    i = paramHwBlob.getInt32(paramLong + 72L + 8L);
    l2 = (i * 48);
    l1 = paramHwBlob.handle();
    paramHwBlob = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 72L + 0L, true);
    this.cellIdentityTdscdma.clear();
    for (b = 0; b < i; b++) {
      CellIdentityTdscdma cellIdentityTdscdma = new CellIdentityTdscdma();
      cellIdentityTdscdma.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, (b * 48));
      this.cellIdentityTdscdma.add(cellIdentityTdscdma);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(88);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellIdentity> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 88);
    for (byte b = 0; b < i; b++)
      ((CellIdentity)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 88)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.cellInfoType);
    int i = this.cellIdentityGsm.size();
    paramHwBlob.putInt32(paramLong + 8L + 8L, i);
    paramHwBlob.putBool(paramLong + 8L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 48);
    byte b;
    for (b = 0; b < i; b++)
      ((CellIdentityGsm)this.cellIdentityGsm.get(b)).writeEmbeddedToBlob(hwBlob, (b * 48)); 
    paramHwBlob.putBlob(paramLong + 8L + 0L, hwBlob);
    i = this.cellIdentityWcdma.size();
    paramHwBlob.putInt32(paramLong + 24L + 8L, i);
    paramHwBlob.putBool(paramLong + 24L + 12L, false);
    hwBlob = new HwBlob(i * 48);
    for (b = 0; b < i; b++)
      ((CellIdentityWcdma)this.cellIdentityWcdma.get(b)).writeEmbeddedToBlob(hwBlob, (b * 48)); 
    paramHwBlob.putBlob(paramLong + 24L + 0L, hwBlob);
    i = this.cellIdentityCdma.size();
    paramHwBlob.putInt32(paramLong + 40L + 8L, i);
    paramHwBlob.putBool(paramLong + 40L + 12L, false);
    hwBlob = new HwBlob(i * 20);
    for (b = 0; b < i; b++)
      ((CellIdentityCdma)this.cellIdentityCdma.get(b)).writeEmbeddedToBlob(hwBlob, (b * 20)); 
    paramHwBlob.putBlob(paramLong + 40L + 0L, hwBlob);
    i = this.cellIdentityLte.size();
    paramHwBlob.putInt32(paramLong + 56L + 8L, i);
    paramHwBlob.putBool(paramLong + 56L + 12L, false);
    hwBlob = new HwBlob(i * 48);
    for (b = 0; b < i; b++)
      ((CellIdentityLte)this.cellIdentityLte.get(b)).writeEmbeddedToBlob(hwBlob, (b * 48)); 
    paramHwBlob.putBlob(paramLong + 56L + 0L, hwBlob);
    i = this.cellIdentityTdscdma.size();
    paramHwBlob.putInt32(paramLong + 72L + 8L, i);
    paramHwBlob.putBool(paramLong + 72L + 12L, false);
    hwBlob = new HwBlob(i * 48);
    for (b = 0; b < i; b++)
      ((CellIdentityTdscdma)this.cellIdentityTdscdma.get(b)).writeEmbeddedToBlob(hwBlob, (b * 48)); 
    paramHwBlob.putBlob(paramLong + 72L + 0L, hwBlob);
  }
}
