package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CdmaBroadcastSmsConfigInfo {
  public int serviceCategory = 0;
  
  public int language = 0;
  
  public boolean selected = false;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CdmaBroadcastSmsConfigInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.serviceCategory != ((CdmaBroadcastSmsConfigInfo)paramObject).serviceCategory)
      return false; 
    if (this.language != ((CdmaBroadcastSmsConfigInfo)paramObject).language)
      return false; 
    if (this.selected != ((CdmaBroadcastSmsConfigInfo)paramObject).selected)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.serviceCategory;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.language;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    boolean bool = this.selected;
    int k = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".serviceCategory = ");
    stringBuilder.append(this.serviceCategory);
    stringBuilder.append(", .language = ");
    stringBuilder.append(this.language);
    stringBuilder.append(", .selected = ");
    stringBuilder.append(this.selected);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(12L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CdmaBroadcastSmsConfigInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CdmaBroadcastSmsConfigInfo> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 12);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CdmaBroadcastSmsConfigInfo cdmaBroadcastSmsConfigInfo = new CdmaBroadcastSmsConfigInfo();
      cdmaBroadcastSmsConfigInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 12));
      arrayList.add(cdmaBroadcastSmsConfigInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.serviceCategory = paramHwBlob.getInt32(0L + paramLong);
    this.language = paramHwBlob.getInt32(4L + paramLong);
    this.selected = paramHwBlob.getBool(8L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(12);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CdmaBroadcastSmsConfigInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 12);
    for (byte b = 0; b < i; b++)
      ((CdmaBroadcastSmsConfigInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 12)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.serviceCategory);
    paramHwBlob.putInt32(4L + paramLong, this.language);
    paramHwBlob.putBool(8L + paramLong, this.selected);
  }
}
