package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public final class ActivityStatsInfo {
  public int sleepModeTimeMs = 0;
  
  public int idleModeTimeMs = 0;
  
  public int[] txmModetimeMs = new int[5];
  
  public int rxModeTimeMs = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != ActivityStatsInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.sleepModeTimeMs != ((ActivityStatsInfo)paramObject).sleepModeTimeMs)
      return false; 
    if (this.idleModeTimeMs != ((ActivityStatsInfo)paramObject).idleModeTimeMs)
      return false; 
    if (!HidlSupport.deepEquals(this.txmModetimeMs, ((ActivityStatsInfo)paramObject).txmModetimeMs))
      return false; 
    if (this.rxModeTimeMs != ((ActivityStatsInfo)paramObject).rxModeTimeMs)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.sleepModeTimeMs;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.idleModeTimeMs;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int[] arrayOfInt = this.txmModetimeMs;
    int k = HidlSupport.deepHashCode(arrayOfInt), m = this.rxModeTimeMs;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".sleepModeTimeMs = ");
    stringBuilder.append(this.sleepModeTimeMs);
    stringBuilder.append(", .idleModeTimeMs = ");
    stringBuilder.append(this.idleModeTimeMs);
    stringBuilder.append(", .txmModetimeMs = ");
    stringBuilder.append(Arrays.toString(this.txmModetimeMs));
    stringBuilder.append(", .rxModeTimeMs = ");
    stringBuilder.append(this.rxModeTimeMs);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(32L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<ActivityStatsInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<ActivityStatsInfo> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 32);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      ActivityStatsInfo activityStatsInfo = new ActivityStatsInfo();
      activityStatsInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 32));
      arrayList.add(activityStatsInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.sleepModeTimeMs = paramHwBlob.getInt32(0L + paramLong);
    this.idleModeTimeMs = paramHwBlob.getInt32(4L + paramLong);
    paramHwBlob.copyToInt32Array(8L + paramLong, this.txmModetimeMs, 5);
    this.rxModeTimeMs = paramHwBlob.getInt32(28L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(32);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<ActivityStatsInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 32);
    for (byte b = 0; b < i; b++)
      ((ActivityStatsInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 32)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.sleepModeTimeMs);
    paramHwBlob.putInt32(4L + paramLong, this.idleModeTimeMs);
    int[] arrayOfInt = this.txmModetimeMs;
    if (arrayOfInt != null && arrayOfInt.length == 5) {
      paramHwBlob.putInt32Array(8L + paramLong, arrayOfInt);
      paramHwBlob.putInt32(28L + paramLong, this.rxModeTimeMs);
      return;
    } 
    throw new IllegalArgumentException("Array element is not of the expected length");
  }
}
