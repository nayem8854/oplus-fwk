package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class SetupDataCallResult {
  public int status = 0;
  
  public int suggestedRetryTime = 0;
  
  public int cid = 0;
  
  public int active = 0;
  
  public String type = new String();
  
  public String ifname = new String();
  
  public String addresses = new String();
  
  public String dnses = new String();
  
  public String gateways = new String();
  
  public String pcscf = new String();
  
  public int mtu = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != SetupDataCallResult.class)
      return false; 
    paramObject = paramObject;
    if (this.status != ((SetupDataCallResult)paramObject).status)
      return false; 
    if (this.suggestedRetryTime != ((SetupDataCallResult)paramObject).suggestedRetryTime)
      return false; 
    if (this.cid != ((SetupDataCallResult)paramObject).cid)
      return false; 
    if (this.active != ((SetupDataCallResult)paramObject).active)
      return false; 
    if (!HidlSupport.deepEquals(this.type, ((SetupDataCallResult)paramObject).type))
      return false; 
    if (!HidlSupport.deepEquals(this.ifname, ((SetupDataCallResult)paramObject).ifname))
      return false; 
    if (!HidlSupport.deepEquals(this.addresses, ((SetupDataCallResult)paramObject).addresses))
      return false; 
    if (!HidlSupport.deepEquals(this.dnses, ((SetupDataCallResult)paramObject).dnses))
      return false; 
    if (!HidlSupport.deepEquals(this.gateways, ((SetupDataCallResult)paramObject).gateways))
      return false; 
    if (!HidlSupport.deepEquals(this.pcscf, ((SetupDataCallResult)paramObject).pcscf))
      return false; 
    if (this.mtu != ((SetupDataCallResult)paramObject).mtu)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.status;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.suggestedRetryTime;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.cid;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.active;
    int n = HidlSupport.deepHashCode(Integer.valueOf(m));
    String str = this.type;
    int i1 = HidlSupport.deepHashCode(str);
    str = this.ifname;
    int i2 = HidlSupport.deepHashCode(str);
    str = this.addresses;
    int i3 = HidlSupport.deepHashCode(str);
    str = this.dnses;
    int i4 = HidlSupport.deepHashCode(str);
    str = this.gateways;
    int i5 = HidlSupport.deepHashCode(str);
    str = this.pcscf;
    m = HidlSupport.deepHashCode(str);
    int i6 = this.mtu;
    i6 = HidlSupport.deepHashCode(Integer.valueOf(i6));
    return Objects.hash(new Object[] { 
          Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), Integer.valueOf(m), 
          Integer.valueOf(i6) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".status = ");
    stringBuilder.append(DataCallFailCause.toString(this.status));
    stringBuilder.append(", .suggestedRetryTime = ");
    stringBuilder.append(this.suggestedRetryTime);
    stringBuilder.append(", .cid = ");
    stringBuilder.append(this.cid);
    stringBuilder.append(", .active = ");
    stringBuilder.append(this.active);
    stringBuilder.append(", .type = ");
    stringBuilder.append(this.type);
    stringBuilder.append(", .ifname = ");
    stringBuilder.append(this.ifname);
    stringBuilder.append(", .addresses = ");
    stringBuilder.append(this.addresses);
    stringBuilder.append(", .dnses = ");
    stringBuilder.append(this.dnses);
    stringBuilder.append(", .gateways = ");
    stringBuilder.append(this.gateways);
    stringBuilder.append(", .pcscf = ");
    stringBuilder.append(this.pcscf);
    stringBuilder.append(", .mtu = ");
    stringBuilder.append(this.mtu);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(120L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<SetupDataCallResult> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<SetupDataCallResult> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 120);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      SetupDataCallResult setupDataCallResult = new SetupDataCallResult();
      setupDataCallResult.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 120));
      arrayList.add(setupDataCallResult);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.status = paramHwBlob.getInt32(paramLong + 0L);
    this.suggestedRetryTime = paramHwBlob.getInt32(paramLong + 4L);
    this.cid = paramHwBlob.getInt32(paramLong + 8L);
    this.active = paramHwBlob.getInt32(paramLong + 12L);
    String str = paramHwBlob.getString(paramLong + 16L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 16L + 0L, false);
    this.ifname = str = paramHwBlob.getString(paramLong + 32L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 32L + 0L, false);
    this.addresses = str = paramHwBlob.getString(paramLong + 48L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 48L + 0L, false);
    this.dnses = str = paramHwBlob.getString(paramLong + 64L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 64L + 0L, false);
    this.gateways = str = paramHwBlob.getString(paramLong + 80L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 80L + 0L, false);
    this.pcscf = str = paramHwBlob.getString(paramLong + 96L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 96L + 0L, false);
    this.mtu = paramHwBlob.getInt32(paramLong + 112L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(120);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<SetupDataCallResult> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 120);
    for (byte b = 0; b < i; b++)
      ((SetupDataCallResult)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 120)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.status);
    paramHwBlob.putInt32(4L + paramLong, this.suggestedRetryTime);
    paramHwBlob.putInt32(8L + paramLong, this.cid);
    paramHwBlob.putInt32(12L + paramLong, this.active);
    paramHwBlob.putString(16L + paramLong, this.type);
    paramHwBlob.putString(32L + paramLong, this.ifname);
    paramHwBlob.putString(48L + paramLong, this.addresses);
    paramHwBlob.putString(64L + paramLong, this.dnses);
    paramHwBlob.putString(80L + paramLong, this.gateways);
    paramHwBlob.putString(96L + paramLong, this.pcscf);
    paramHwBlob.putInt32(112L + paramLong, this.mtu);
  }
}
