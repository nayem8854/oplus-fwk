package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CdmaSmsAddress {
  public int digitMode = 0;
  
  public int numberMode = 0;
  
  public int numberType = 0;
  
  public int numberPlan = 0;
  
  public ArrayList<Byte> digits = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CdmaSmsAddress.class)
      return false; 
    paramObject = paramObject;
    if (this.digitMode != ((CdmaSmsAddress)paramObject).digitMode)
      return false; 
    if (this.numberMode != ((CdmaSmsAddress)paramObject).numberMode)
      return false; 
    if (this.numberType != ((CdmaSmsAddress)paramObject).numberType)
      return false; 
    if (this.numberPlan != ((CdmaSmsAddress)paramObject).numberPlan)
      return false; 
    if (!HidlSupport.deepEquals(this.digits, ((CdmaSmsAddress)paramObject).digits))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.digitMode;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.numberMode;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.numberType;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.numberPlan;
    int n = HidlSupport.deepHashCode(Integer.valueOf(m));
    ArrayList<Byte> arrayList = this.digits;
    m = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(n), Integer.valueOf(m) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".digitMode = ");
    stringBuilder.append(CdmaSmsDigitMode.toString(this.digitMode));
    stringBuilder.append(", .numberMode = ");
    stringBuilder.append(CdmaSmsNumberMode.toString(this.numberMode));
    stringBuilder.append(", .numberType = ");
    stringBuilder.append(CdmaSmsNumberType.toString(this.numberType));
    stringBuilder.append(", .numberPlan = ");
    stringBuilder.append(CdmaSmsNumberPlan.toString(this.numberPlan));
    stringBuilder.append(", .digits = ");
    stringBuilder.append(this.digits);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(32L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CdmaSmsAddress> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CdmaSmsAddress> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 32);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CdmaSmsAddress cdmaSmsAddress = new CdmaSmsAddress();
      cdmaSmsAddress.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 32));
      arrayList.add(cdmaSmsAddress);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.digitMode = paramHwBlob.getInt32(paramLong + 0L);
    this.numberMode = paramHwBlob.getInt32(paramLong + 4L);
    this.numberType = paramHwBlob.getInt32(paramLong + 8L);
    this.numberPlan = paramHwBlob.getInt32(paramLong + 12L);
    int i = paramHwBlob.getInt32(paramLong + 16L + 8L);
    long l1 = (i * 1);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 16L + 0L, true);
    this.digits.clear();
    for (byte b = 0; b < i; b++) {
      byte b1 = hwBlob.getInt8((b * 1));
      this.digits.add(Byte.valueOf(b1));
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(32);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CdmaSmsAddress> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 32);
    for (byte b = 0; b < i; b++)
      ((CdmaSmsAddress)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 32)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.digitMode);
    paramHwBlob.putInt32(4L + paramLong, this.numberMode);
    paramHwBlob.putInt32(paramLong + 8L, this.numberType);
    paramHwBlob.putInt32(paramLong + 12L, this.numberPlan);
    int i = this.digits.size();
    paramHwBlob.putInt32(paramLong + 16L + 8L, i);
    paramHwBlob.putBool(paramLong + 16L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 1);
    for (byte b = 0; b < i; b++)
      hwBlob.putInt8((b * 1), ((Byte)this.digits.get(b)).byteValue()); 
    paramHwBlob.putBlob(16L + paramLong + 0L, hwBlob);
  }
}
