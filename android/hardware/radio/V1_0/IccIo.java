package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class IccIo {
  public int command = 0;
  
  public int fileId = 0;
  
  public String path = new String();
  
  public int p1 = 0;
  
  public int p2 = 0;
  
  public int p3 = 0;
  
  public String data = new String();
  
  public String pin2 = new String();
  
  public String aid = new String();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != IccIo.class)
      return false; 
    paramObject = paramObject;
    if (this.command != ((IccIo)paramObject).command)
      return false; 
    if (this.fileId != ((IccIo)paramObject).fileId)
      return false; 
    if (!HidlSupport.deepEquals(this.path, ((IccIo)paramObject).path))
      return false; 
    if (this.p1 != ((IccIo)paramObject).p1)
      return false; 
    if (this.p2 != ((IccIo)paramObject).p2)
      return false; 
    if (this.p3 != ((IccIo)paramObject).p3)
      return false; 
    if (!HidlSupport.deepEquals(this.data, ((IccIo)paramObject).data))
      return false; 
    if (!HidlSupport.deepEquals(this.pin2, ((IccIo)paramObject).pin2))
      return false; 
    if (!HidlSupport.deepEquals(this.aid, ((IccIo)paramObject).aid))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.command;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.fileId;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    String str = this.path;
    int k = HidlSupport.deepHashCode(str), m = this.p1;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    int n = this.p2;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    int i1 = this.p3;
    i1 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    str = this.data;
    int i2 = HidlSupport.deepHashCode(str);
    str = this.pin2;
    int i3 = HidlSupport.deepHashCode(str);
    str = this.aid;
    int i4 = HidlSupport.deepHashCode(str);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".command = ");
    stringBuilder.append(this.command);
    stringBuilder.append(", .fileId = ");
    stringBuilder.append(this.fileId);
    stringBuilder.append(", .path = ");
    stringBuilder.append(this.path);
    stringBuilder.append(", .p1 = ");
    stringBuilder.append(this.p1);
    stringBuilder.append(", .p2 = ");
    stringBuilder.append(this.p2);
    stringBuilder.append(", .p3 = ");
    stringBuilder.append(this.p3);
    stringBuilder.append(", .data = ");
    stringBuilder.append(this.data);
    stringBuilder.append(", .pin2 = ");
    stringBuilder.append(this.pin2);
    stringBuilder.append(", .aid = ");
    stringBuilder.append(this.aid);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(88L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<IccIo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<IccIo> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 88);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      IccIo iccIo = new IccIo();
      iccIo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 88));
      arrayList.add(iccIo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.command = paramHwBlob.getInt32(paramLong + 0L);
    this.fileId = paramHwBlob.getInt32(paramLong + 4L);
    String str = paramHwBlob.getString(paramLong + 8L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, false);
    this.p1 = paramHwBlob.getInt32(paramLong + 24L);
    this.p2 = paramHwBlob.getInt32(paramLong + 28L);
    this.p3 = paramHwBlob.getInt32(paramLong + 32L);
    this.data = str = paramHwBlob.getString(paramLong + 40L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 40L + 0L, false);
    this.pin2 = str = paramHwBlob.getString(paramLong + 56L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 56L + 0L, false);
    this.aid = str = paramHwBlob.getString(paramLong + 72L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 72L + 0L, false);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(88);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<IccIo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 88);
    for (byte b = 0; b < i; b++)
      ((IccIo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 88)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.command);
    paramHwBlob.putInt32(4L + paramLong, this.fileId);
    paramHwBlob.putString(8L + paramLong, this.path);
    paramHwBlob.putInt32(24L + paramLong, this.p1);
    paramHwBlob.putInt32(28L + paramLong, this.p2);
    paramHwBlob.putInt32(32L + paramLong, this.p3);
    paramHwBlob.putString(40L + paramLong, this.data);
    paramHwBlob.putString(56L + paramLong, this.pin2);
    paramHwBlob.putString(72L + paramLong, this.aid);
  }
}
