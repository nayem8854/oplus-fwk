package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class SimApdu {
  public int sessionId = 0;
  
  public int cla = 0;
  
  public int instruction = 0;
  
  public int p1 = 0;
  
  public int p2 = 0;
  
  public int p3 = 0;
  
  public String data = new String();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != SimApdu.class)
      return false; 
    paramObject = paramObject;
    if (this.sessionId != ((SimApdu)paramObject).sessionId)
      return false; 
    if (this.cla != ((SimApdu)paramObject).cla)
      return false; 
    if (this.instruction != ((SimApdu)paramObject).instruction)
      return false; 
    if (this.p1 != ((SimApdu)paramObject).p1)
      return false; 
    if (this.p2 != ((SimApdu)paramObject).p2)
      return false; 
    if (this.p3 != ((SimApdu)paramObject).p3)
      return false; 
    if (!HidlSupport.deepEquals(this.data, ((SimApdu)paramObject).data))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.sessionId;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.cla;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.instruction;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.p1;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    int n = this.p2;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    int i1 = this.p3;
    int i2 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    String str = this.data;
    i1 = HidlSupport.deepHashCode(str);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i2), Integer.valueOf(i1) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".sessionId = ");
    stringBuilder.append(this.sessionId);
    stringBuilder.append(", .cla = ");
    stringBuilder.append(this.cla);
    stringBuilder.append(", .instruction = ");
    stringBuilder.append(this.instruction);
    stringBuilder.append(", .p1 = ");
    stringBuilder.append(this.p1);
    stringBuilder.append(", .p2 = ");
    stringBuilder.append(this.p2);
    stringBuilder.append(", .p3 = ");
    stringBuilder.append(this.p3);
    stringBuilder.append(", .data = ");
    stringBuilder.append(this.data);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(40L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<SimApdu> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<SimApdu> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 40);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      SimApdu simApdu = new SimApdu();
      simApdu.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 40));
      arrayList.add(simApdu);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.sessionId = paramHwBlob.getInt32(paramLong + 0L);
    this.cla = paramHwBlob.getInt32(paramLong + 4L);
    this.instruction = paramHwBlob.getInt32(paramLong + 8L);
    this.p1 = paramHwBlob.getInt32(paramLong + 12L);
    this.p2 = paramHwBlob.getInt32(paramLong + 16L);
    this.p3 = paramHwBlob.getInt32(paramLong + 20L);
    String str = paramHwBlob.getString(paramLong + 24L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 24L + 0L, false);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(40);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<SimApdu> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 40);
    for (byte b = 0; b < i; b++)
      ((SimApdu)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 40)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.sessionId);
    paramHwBlob.putInt32(4L + paramLong, this.cla);
    paramHwBlob.putInt32(8L + paramLong, this.instruction);
    paramHwBlob.putInt32(12L + paramLong, this.p1);
    paramHwBlob.putInt32(16L + paramLong, this.p2);
    paramHwBlob.putInt32(20L + paramLong, this.p3);
    paramHwBlob.putString(24L + paramLong, this.data);
  }
}
