package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CdmaSmsSubaddress {
  public int subaddressType = 0;
  
  public boolean odd = false;
  
  public ArrayList<Byte> digits = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CdmaSmsSubaddress.class)
      return false; 
    paramObject = paramObject;
    if (this.subaddressType != ((CdmaSmsSubaddress)paramObject).subaddressType)
      return false; 
    if (this.odd != ((CdmaSmsSubaddress)paramObject).odd)
      return false; 
    if (!HidlSupport.deepEquals(this.digits, ((CdmaSmsSubaddress)paramObject).digits))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.subaddressType;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    boolean bool = this.odd;
    int k = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    ArrayList<Byte> arrayList = this.digits;
    i = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(i) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".subaddressType = ");
    stringBuilder.append(CdmaSmsSubaddressType.toString(this.subaddressType));
    stringBuilder.append(", .odd = ");
    stringBuilder.append(this.odd);
    stringBuilder.append(", .digits = ");
    stringBuilder.append(this.digits);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(24L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CdmaSmsSubaddress> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CdmaSmsSubaddress> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 24);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CdmaSmsSubaddress cdmaSmsSubaddress = new CdmaSmsSubaddress();
      cdmaSmsSubaddress.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 24));
      arrayList.add(cdmaSmsSubaddress);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.subaddressType = paramHwBlob.getInt32(paramLong + 0L);
    this.odd = paramHwBlob.getBool(paramLong + 4L);
    int i = paramHwBlob.getInt32(paramLong + 8L + 8L);
    long l1 = (i * 1);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, true);
    this.digits.clear();
    for (byte b = 0; b < i; b++) {
      byte b1 = hwBlob.getInt8((b * 1));
      this.digits.add(Byte.valueOf(b1));
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(24);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CdmaSmsSubaddress> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 24);
    for (byte b = 0; b < i; b++)
      ((CdmaSmsSubaddress)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 24)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.subaddressType);
    paramHwBlob.putBool(4L + paramLong, this.odd);
    int i = this.digits.size();
    paramHwBlob.putInt32(paramLong + 8L + 8L, i);
    paramHwBlob.putBool(paramLong + 8L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 1);
    for (byte b = 0; b < i; b++)
      hwBlob.putInt8((b * 1), ((Byte)this.digits.get(b)).byteValue()); 
    paramHwBlob.putBlob(8L + paramLong + 0L, hwBlob);
  }
}
