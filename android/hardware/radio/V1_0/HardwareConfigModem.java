package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class HardwareConfigModem {
  public int rilModel = 0;
  
  public int rat = 0;
  
  public int maxVoice = 0;
  
  public int maxData = 0;
  
  public int maxStandby = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != HardwareConfigModem.class)
      return false; 
    paramObject = paramObject;
    if (this.rilModel != ((HardwareConfigModem)paramObject).rilModel)
      return false; 
    if (this.rat != ((HardwareConfigModem)paramObject).rat)
      return false; 
    if (this.maxVoice != ((HardwareConfigModem)paramObject).maxVoice)
      return false; 
    if (this.maxData != ((HardwareConfigModem)paramObject).maxData)
      return false; 
    if (this.maxStandby != ((HardwareConfigModem)paramObject).maxStandby)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.rilModel;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.rat;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.maxVoice;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.maxData;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    int n = this.maxStandby;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".rilModel = ");
    stringBuilder.append(this.rilModel);
    stringBuilder.append(", .rat = ");
    stringBuilder.append(this.rat);
    stringBuilder.append(", .maxVoice = ");
    stringBuilder.append(this.maxVoice);
    stringBuilder.append(", .maxData = ");
    stringBuilder.append(this.maxData);
    stringBuilder.append(", .maxStandby = ");
    stringBuilder.append(this.maxStandby);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(20L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<HardwareConfigModem> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<HardwareConfigModem> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 20);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      HardwareConfigModem hardwareConfigModem = new HardwareConfigModem();
      hardwareConfigModem.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 20));
      arrayList.add(hardwareConfigModem);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.rilModel = paramHwBlob.getInt32(0L + paramLong);
    this.rat = paramHwBlob.getInt32(4L + paramLong);
    this.maxVoice = paramHwBlob.getInt32(8L + paramLong);
    this.maxData = paramHwBlob.getInt32(12L + paramLong);
    this.maxStandby = paramHwBlob.getInt32(16L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(20);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<HardwareConfigModem> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 20);
    for (byte b = 0; b < i; b++)
      ((HardwareConfigModem)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 20)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.rilModel);
    paramHwBlob.putInt32(4L + paramLong, this.rat);
    paramHwBlob.putInt32(8L + paramLong, this.maxVoice);
    paramHwBlob.putInt32(12L + paramLong, this.maxData);
    paramHwBlob.putInt32(16L + paramLong, this.maxStandby);
  }
}
