package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CdmaInformationRecord {
  public int name = 0;
  
  public ArrayList<CdmaDisplayInfoRecord> display = new ArrayList<>();
  
  public ArrayList<CdmaNumberInfoRecord> number = new ArrayList<>();
  
  public ArrayList<CdmaSignalInfoRecord> signal = new ArrayList<>();
  
  public ArrayList<CdmaRedirectingNumberInfoRecord> redir = new ArrayList<>();
  
  public ArrayList<CdmaLineControlInfoRecord> lineCtrl = new ArrayList<>();
  
  public ArrayList<CdmaT53ClirInfoRecord> clir = new ArrayList<>();
  
  public ArrayList<CdmaT53AudioControlInfoRecord> audioCtrl = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CdmaInformationRecord.class)
      return false; 
    paramObject = paramObject;
    if (this.name != ((CdmaInformationRecord)paramObject).name)
      return false; 
    if (!HidlSupport.deepEquals(this.display, ((CdmaInformationRecord)paramObject).display))
      return false; 
    if (!HidlSupport.deepEquals(this.number, ((CdmaInformationRecord)paramObject).number))
      return false; 
    if (!HidlSupport.deepEquals(this.signal, ((CdmaInformationRecord)paramObject).signal))
      return false; 
    if (!HidlSupport.deepEquals(this.redir, ((CdmaInformationRecord)paramObject).redir))
      return false; 
    if (!HidlSupport.deepEquals(this.lineCtrl, ((CdmaInformationRecord)paramObject).lineCtrl))
      return false; 
    if (!HidlSupport.deepEquals(this.clir, ((CdmaInformationRecord)paramObject).clir))
      return false; 
    if (!HidlSupport.deepEquals(this.audioCtrl, ((CdmaInformationRecord)paramObject).audioCtrl))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.name;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    ArrayList<CdmaDisplayInfoRecord> arrayList6 = this.display;
    i = HidlSupport.deepHashCode(arrayList6);
    ArrayList<CdmaNumberInfoRecord> arrayList5 = this.number;
    int k = HidlSupport.deepHashCode(arrayList5);
    ArrayList<CdmaSignalInfoRecord> arrayList4 = this.signal;
    int m = HidlSupport.deepHashCode(arrayList4);
    ArrayList<CdmaRedirectingNumberInfoRecord> arrayList3 = this.redir;
    int n = HidlSupport.deepHashCode(arrayList3);
    ArrayList<CdmaLineControlInfoRecord> arrayList2 = this.lineCtrl;
    int i1 = HidlSupport.deepHashCode(arrayList2);
    ArrayList<CdmaT53ClirInfoRecord> arrayList1 = this.clir;
    int i2 = HidlSupport.deepHashCode(arrayList1);
    ArrayList<CdmaT53AudioControlInfoRecord> arrayList = this.audioCtrl;
    int i3 = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(i), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".name = ");
    stringBuilder.append(CdmaInfoRecName.toString(this.name));
    stringBuilder.append(", .display = ");
    stringBuilder.append(this.display);
    stringBuilder.append(", .number = ");
    stringBuilder.append(this.number);
    stringBuilder.append(", .signal = ");
    stringBuilder.append(this.signal);
    stringBuilder.append(", .redir = ");
    stringBuilder.append(this.redir);
    stringBuilder.append(", .lineCtrl = ");
    stringBuilder.append(this.lineCtrl);
    stringBuilder.append(", .clir = ");
    stringBuilder.append(this.clir);
    stringBuilder.append(", .audioCtrl = ");
    stringBuilder.append(this.audioCtrl);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(120L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CdmaInformationRecord> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CdmaInformationRecord> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 120);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CdmaInformationRecord cdmaInformationRecord = new CdmaInformationRecord();
      cdmaInformationRecord.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 120));
      arrayList.add(cdmaInformationRecord);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.name = paramHwBlob.getInt32(paramLong + 0L);
    int i = paramHwBlob.getInt32(paramLong + 8L + 8L);
    long l1 = (i * 16);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob1 = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, true);
    this.display.clear();
    byte b;
    for (b = 0; b < i; b++) {
      CdmaDisplayInfoRecord cdmaDisplayInfoRecord = new CdmaDisplayInfoRecord();
      cdmaDisplayInfoRecord.readEmbeddedFromParcel(paramHwParcel, hwBlob1, (b * 16));
      this.display.add(cdmaDisplayInfoRecord);
    } 
    i = paramHwBlob.getInt32(paramLong + 24L + 8L);
    l2 = (i * 24);
    l1 = paramHwBlob.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 24L + 0L, true);
    this.number.clear();
    for (b = 0; b < i; b++) {
      CdmaNumberInfoRecord cdmaNumberInfoRecord = new CdmaNumberInfoRecord();
      cdmaNumberInfoRecord.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 24));
      this.number.add(cdmaNumberInfoRecord);
    } 
    i = paramHwBlob.getInt32(paramLong + 40L + 8L);
    l2 = (i * 4);
    l1 = paramHwBlob.handle();
    hwBlob1 = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 40L + 0L, true);
    this.signal.clear();
    for (b = 0; b < i; b++) {
      CdmaSignalInfoRecord cdmaSignalInfoRecord = new CdmaSignalInfoRecord();
      cdmaSignalInfoRecord.readEmbeddedFromParcel(paramHwParcel, hwBlob1, (b * 4));
      this.signal.add(cdmaSignalInfoRecord);
    } 
    i = paramHwBlob.getInt32(paramLong + 56L + 8L);
    l1 = (i * 32);
    l2 = paramHwBlob.handle();
    hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 56L + 0L, true);
    this.redir.clear();
    for (b = 0; b < i; b++) {
      CdmaRedirectingNumberInfoRecord cdmaRedirectingNumberInfoRecord = new CdmaRedirectingNumberInfoRecord();
      cdmaRedirectingNumberInfoRecord.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 32));
      this.redir.add(cdmaRedirectingNumberInfoRecord);
    } 
    i = paramHwBlob.getInt32(paramLong + 72L + 8L);
    l1 = (i * 4);
    l2 = paramHwBlob.handle();
    hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 72L + 0L, true);
    this.lineCtrl.clear();
    for (b = 0; b < i; b++) {
      CdmaLineControlInfoRecord cdmaLineControlInfoRecord = new CdmaLineControlInfoRecord();
      cdmaLineControlInfoRecord.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 4));
      this.lineCtrl.add(cdmaLineControlInfoRecord);
    } 
    i = paramHwBlob.getInt32(paramLong + 88L + 8L);
    l2 = (i * 1);
    l1 = paramHwBlob.handle();
    hwBlob1 = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 88L + 0L, true);
    this.clir.clear();
    for (b = 0; b < i; b++) {
      CdmaT53ClirInfoRecord cdmaT53ClirInfoRecord = new CdmaT53ClirInfoRecord();
      cdmaT53ClirInfoRecord.readEmbeddedFromParcel(paramHwParcel, hwBlob1, (b * 1));
      this.clir.add(cdmaT53ClirInfoRecord);
    } 
    i = paramHwBlob.getInt32(paramLong + 104L + 8L);
    l2 = (i * 2);
    l1 = paramHwBlob.handle();
    paramHwBlob = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 104L + 0L, true);
    this.audioCtrl.clear();
    for (b = 0; b < i; b++) {
      CdmaT53AudioControlInfoRecord cdmaT53AudioControlInfoRecord = new CdmaT53AudioControlInfoRecord();
      cdmaT53AudioControlInfoRecord.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, (b * 2));
      this.audioCtrl.add(cdmaT53AudioControlInfoRecord);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(120);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CdmaInformationRecord> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 120);
    for (byte b = 0; b < i; b++)
      ((CdmaInformationRecord)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 120)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.name);
    int i = this.display.size();
    paramHwBlob.putInt32(paramLong + 8L + 8L, i);
    paramHwBlob.putBool(paramLong + 8L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 16);
    byte b;
    for (b = 0; b < i; b++)
      ((CdmaDisplayInfoRecord)this.display.get(b)).writeEmbeddedToBlob(hwBlob, (b * 16)); 
    paramHwBlob.putBlob(paramLong + 8L + 0L, hwBlob);
    i = this.number.size();
    paramHwBlob.putInt32(paramLong + 24L + 8L, i);
    paramHwBlob.putBool(paramLong + 24L + 12L, false);
    hwBlob = new HwBlob(i * 24);
    for (b = 0; b < i; b++)
      ((CdmaNumberInfoRecord)this.number.get(b)).writeEmbeddedToBlob(hwBlob, (b * 24)); 
    paramHwBlob.putBlob(paramLong + 24L + 0L, hwBlob);
    i = this.signal.size();
    paramHwBlob.putInt32(paramLong + 40L + 8L, i);
    paramHwBlob.putBool(paramLong + 40L + 12L, false);
    hwBlob = new HwBlob(i * 4);
    for (b = 0; b < i; b++)
      ((CdmaSignalInfoRecord)this.signal.get(b)).writeEmbeddedToBlob(hwBlob, (b * 4)); 
    paramHwBlob.putBlob(paramLong + 40L + 0L, hwBlob);
    i = this.redir.size();
    paramHwBlob.putInt32(paramLong + 56L + 8L, i);
    paramHwBlob.putBool(paramLong + 56L + 12L, false);
    hwBlob = new HwBlob(i * 32);
    for (b = 0; b < i; b++)
      ((CdmaRedirectingNumberInfoRecord)this.redir.get(b)).writeEmbeddedToBlob(hwBlob, (b * 32)); 
    paramHwBlob.putBlob(paramLong + 56L + 0L, hwBlob);
    i = this.lineCtrl.size();
    paramHwBlob.putInt32(paramLong + 72L + 8L, i);
    paramHwBlob.putBool(paramLong + 72L + 12L, false);
    hwBlob = new HwBlob(i * 4);
    for (b = 0; b < i; b++)
      ((CdmaLineControlInfoRecord)this.lineCtrl.get(b)).writeEmbeddedToBlob(hwBlob, (b * 4)); 
    paramHwBlob.putBlob(paramLong + 72L + 0L, hwBlob);
    i = this.clir.size();
    paramHwBlob.putInt32(paramLong + 88L + 8L, i);
    paramHwBlob.putBool(paramLong + 88L + 12L, false);
    hwBlob = new HwBlob(i * 1);
    for (b = 0; b < i; b++)
      ((CdmaT53ClirInfoRecord)this.clir.get(b)).writeEmbeddedToBlob(hwBlob, (b * 1)); 
    paramHwBlob.putBlob(paramLong + 88L + 0L, hwBlob);
    i = this.audioCtrl.size();
    paramHwBlob.putInt32(paramLong + 104L + 8L, i);
    paramHwBlob.putBool(paramLong + 104L + 12L, false);
    hwBlob = new HwBlob(i * 2);
    for (b = 0; b < i; b++)
      ((CdmaT53AudioControlInfoRecord)this.audioCtrl.get(b)).writeEmbeddedToBlob(hwBlob, (b * 2)); 
    paramHwBlob.putBlob(paramLong + 104L + 0L, hwBlob);
  }
}
