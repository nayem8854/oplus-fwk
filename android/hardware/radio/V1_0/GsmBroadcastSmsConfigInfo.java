package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class GsmBroadcastSmsConfigInfo {
  public int fromServiceId = 0;
  
  public int toServiceId = 0;
  
  public int fromCodeScheme = 0;
  
  public int toCodeScheme = 0;
  
  public boolean selected = false;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != GsmBroadcastSmsConfigInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.fromServiceId != ((GsmBroadcastSmsConfigInfo)paramObject).fromServiceId)
      return false; 
    if (this.toServiceId != ((GsmBroadcastSmsConfigInfo)paramObject).toServiceId)
      return false; 
    if (this.fromCodeScheme != ((GsmBroadcastSmsConfigInfo)paramObject).fromCodeScheme)
      return false; 
    if (this.toCodeScheme != ((GsmBroadcastSmsConfigInfo)paramObject).toCodeScheme)
      return false; 
    if (this.selected != ((GsmBroadcastSmsConfigInfo)paramObject).selected)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.fromServiceId;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.toServiceId;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.fromCodeScheme;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.toCodeScheme;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    boolean bool = this.selected;
    int n = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".fromServiceId = ");
    stringBuilder.append(this.fromServiceId);
    stringBuilder.append(", .toServiceId = ");
    stringBuilder.append(this.toServiceId);
    stringBuilder.append(", .fromCodeScheme = ");
    stringBuilder.append(this.fromCodeScheme);
    stringBuilder.append(", .toCodeScheme = ");
    stringBuilder.append(this.toCodeScheme);
    stringBuilder.append(", .selected = ");
    stringBuilder.append(this.selected);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(20L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<GsmBroadcastSmsConfigInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<GsmBroadcastSmsConfigInfo> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 20);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      GsmBroadcastSmsConfigInfo gsmBroadcastSmsConfigInfo = new GsmBroadcastSmsConfigInfo();
      gsmBroadcastSmsConfigInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 20));
      arrayList.add(gsmBroadcastSmsConfigInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.fromServiceId = paramHwBlob.getInt32(0L + paramLong);
    this.toServiceId = paramHwBlob.getInt32(4L + paramLong);
    this.fromCodeScheme = paramHwBlob.getInt32(8L + paramLong);
    this.toCodeScheme = paramHwBlob.getInt32(12L + paramLong);
    this.selected = paramHwBlob.getBool(16L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(20);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<GsmBroadcastSmsConfigInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 20);
    for (byte b = 0; b < i; b++)
      ((GsmBroadcastSmsConfigInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 20)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.fromServiceId);
    paramHwBlob.putInt32(4L + paramLong, this.toServiceId);
    paramHwBlob.putInt32(8L + paramLong, this.fromCodeScheme);
    paramHwBlob.putInt32(12L + paramLong, this.toCodeScheme);
    paramHwBlob.putBool(16L + paramLong, this.selected);
  }
}
