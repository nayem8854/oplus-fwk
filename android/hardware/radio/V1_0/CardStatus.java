package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CardStatus {
  public int cardState = 0;
  
  public int universalPinState = 0;
  
  public int gsmUmtsSubscriptionAppIndex = 0;
  
  public int cdmaSubscriptionAppIndex = 0;
  
  public int imsSubscriptionAppIndex = 0;
  
  public ArrayList<AppStatus> applications = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CardStatus.class)
      return false; 
    paramObject = paramObject;
    if (this.cardState != ((CardStatus)paramObject).cardState)
      return false; 
    if (this.universalPinState != ((CardStatus)paramObject).universalPinState)
      return false; 
    if (this.gsmUmtsSubscriptionAppIndex != ((CardStatus)paramObject).gsmUmtsSubscriptionAppIndex)
      return false; 
    if (this.cdmaSubscriptionAppIndex != ((CardStatus)paramObject).cdmaSubscriptionAppIndex)
      return false; 
    if (this.imsSubscriptionAppIndex != ((CardStatus)paramObject).imsSubscriptionAppIndex)
      return false; 
    if (!HidlSupport.deepEquals(this.applications, ((CardStatus)paramObject).applications))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.cardState;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.universalPinState;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.gsmUmtsSubscriptionAppIndex;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.cdmaSubscriptionAppIndex;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    int n = this.imsSubscriptionAppIndex;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    ArrayList<AppStatus> arrayList = this.applications;
    int i1 = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".cardState = ");
    stringBuilder.append(CardState.toString(this.cardState));
    stringBuilder.append(", .universalPinState = ");
    stringBuilder.append(PinState.toString(this.universalPinState));
    stringBuilder.append(", .gsmUmtsSubscriptionAppIndex = ");
    stringBuilder.append(this.gsmUmtsSubscriptionAppIndex);
    stringBuilder.append(", .cdmaSubscriptionAppIndex = ");
    stringBuilder.append(this.cdmaSubscriptionAppIndex);
    stringBuilder.append(", .imsSubscriptionAppIndex = ");
    stringBuilder.append(this.imsSubscriptionAppIndex);
    stringBuilder.append(", .applications = ");
    stringBuilder.append(this.applications);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(40L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CardStatus> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CardStatus> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 40);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CardStatus cardStatus = new CardStatus();
      cardStatus.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 40));
      arrayList.add(cardStatus);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.cardState = paramHwBlob.getInt32(paramLong + 0L);
    this.universalPinState = paramHwBlob.getInt32(paramLong + 4L);
    this.gsmUmtsSubscriptionAppIndex = paramHwBlob.getInt32(paramLong + 8L);
    this.cdmaSubscriptionAppIndex = paramHwBlob.getInt32(paramLong + 12L);
    this.imsSubscriptionAppIndex = paramHwBlob.getInt32(paramLong + 16L);
    int i = paramHwBlob.getInt32(paramLong + 24L + 8L);
    long l1 = (i * 64);
    long l2 = paramHwBlob.handle();
    paramHwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 24L + 0L, true);
    this.applications.clear();
    for (byte b = 0; b < i; b++) {
      AppStatus appStatus = new AppStatus();
      appStatus.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, (b * 64));
      this.applications.add(appStatus);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(40);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CardStatus> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 40);
    for (byte b = 0; b < i; b++)
      ((CardStatus)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 40)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.cardState);
    paramHwBlob.putInt32(4L + paramLong, this.universalPinState);
    paramHwBlob.putInt32(paramLong + 8L, this.gsmUmtsSubscriptionAppIndex);
    paramHwBlob.putInt32(paramLong + 12L, this.cdmaSubscriptionAppIndex);
    paramHwBlob.putInt32(16L + paramLong, this.imsSubscriptionAppIndex);
    int i = this.applications.size();
    paramHwBlob.putInt32(paramLong + 24L + 8L, i);
    paramHwBlob.putBool(paramLong + 24L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 64);
    for (byte b = 0; b < i; b++)
      ((AppStatus)this.applications.get(b)).writeEmbeddedToBlob(hwBlob, (b * 64)); 
    paramHwBlob.putBlob(24L + paramLong + 0L, hwBlob);
  }
}
