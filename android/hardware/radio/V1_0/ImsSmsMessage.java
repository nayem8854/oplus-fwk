package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class ImsSmsMessage {
  public int tech = 0;
  
  public boolean retry = false;
  
  public int messageRef = 0;
  
  public ArrayList<CdmaSmsMessage> cdmaMessage = new ArrayList<>();
  
  public ArrayList<GsmSmsMessage> gsmMessage = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != ImsSmsMessage.class)
      return false; 
    paramObject = paramObject;
    if (this.tech != ((ImsSmsMessage)paramObject).tech)
      return false; 
    if (this.retry != ((ImsSmsMessage)paramObject).retry)
      return false; 
    if (this.messageRef != ((ImsSmsMessage)paramObject).messageRef)
      return false; 
    if (!HidlSupport.deepEquals(this.cdmaMessage, ((ImsSmsMessage)paramObject).cdmaMessage))
      return false; 
    if (!HidlSupport.deepEquals(this.gsmMessage, ((ImsSmsMessage)paramObject).gsmMessage))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.tech;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    boolean bool = this.retry;
    i = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    int k = this.messageRef;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    ArrayList<CdmaSmsMessage> arrayList1 = this.cdmaMessage;
    int m = HidlSupport.deepHashCode(arrayList1);
    ArrayList<GsmSmsMessage> arrayList = this.gsmMessage;
    int n = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(i), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".tech = ");
    stringBuilder.append(RadioTechnologyFamily.toString(this.tech));
    stringBuilder.append(", .retry = ");
    stringBuilder.append(this.retry);
    stringBuilder.append(", .messageRef = ");
    stringBuilder.append(this.messageRef);
    stringBuilder.append(", .cdmaMessage = ");
    stringBuilder.append(this.cdmaMessage);
    stringBuilder.append(", .gsmMessage = ");
    stringBuilder.append(this.gsmMessage);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(48L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<ImsSmsMessage> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<ImsSmsMessage> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 48);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      ImsSmsMessage imsSmsMessage = new ImsSmsMessage();
      imsSmsMessage.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 48));
      arrayList.add(imsSmsMessage);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.tech = paramHwBlob.getInt32(paramLong + 0L);
    this.retry = paramHwBlob.getBool(paramLong + 4L);
    this.messageRef = paramHwBlob.getInt32(paramLong + 8L);
    int i = paramHwBlob.getInt32(paramLong + 16L + 8L);
    long l1 = (i * 88);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 16L + 0L, true);
    this.cdmaMessage.clear();
    byte b;
    for (b = 0; b < i; b++) {
      CdmaSmsMessage cdmaSmsMessage = new CdmaSmsMessage();
      cdmaSmsMessage.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 88));
      this.cdmaMessage.add(cdmaSmsMessage);
    } 
    i = paramHwBlob.getInt32(paramLong + 32L + 8L);
    l2 = (i * 32);
    l1 = paramHwBlob.handle();
    paramHwBlob = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 32L + 0L, true);
    this.gsmMessage.clear();
    for (b = 0; b < i; b++) {
      GsmSmsMessage gsmSmsMessage = new GsmSmsMessage();
      gsmSmsMessage.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, (b * 32));
      this.gsmMessage.add(gsmSmsMessage);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(48);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<ImsSmsMessage> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 48);
    for (byte b = 0; b < i; b++)
      ((ImsSmsMessage)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 48)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.tech);
    paramHwBlob.putBool(paramLong + 4L, this.retry);
    paramHwBlob.putInt32(paramLong + 8L, this.messageRef);
    int i = this.cdmaMessage.size();
    paramHwBlob.putInt32(paramLong + 16L + 8L, i);
    paramHwBlob.putBool(paramLong + 16L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 88);
    byte b;
    for (b = 0; b < i; b++)
      ((CdmaSmsMessage)this.cdmaMessage.get(b)).writeEmbeddedToBlob(hwBlob, (b * 88)); 
    paramHwBlob.putBlob(paramLong + 16L + 0L, hwBlob);
    i = this.gsmMessage.size();
    paramHwBlob.putInt32(paramLong + 32L + 8L, i);
    paramHwBlob.putBool(paramLong + 32L + 12L, false);
    hwBlob = new HwBlob(i * 32);
    for (b = 0; b < i; b++)
      ((GsmSmsMessage)this.gsmMessage.get(b)).writeEmbeddedToBlob(hwBlob, (b * 32)); 
    paramHwBlob.putBlob(paramLong + 32L + 0L, hwBlob);
  }
}
