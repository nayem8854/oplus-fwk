package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class LceStatusInfo {
  public int lceStatus = 0;
  
  public byte actualIntervalMs = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != LceStatusInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.lceStatus != ((LceStatusInfo)paramObject).lceStatus)
      return false; 
    if (this.actualIntervalMs != ((LceStatusInfo)paramObject).actualIntervalMs)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.lceStatus;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    byte b = this.actualIntervalMs;
    i = HidlSupport.deepHashCode(Byte.valueOf(b));
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(i) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".lceStatus = ");
    stringBuilder.append(LceStatus.toString(this.lceStatus));
    stringBuilder.append(", .actualIntervalMs = ");
    stringBuilder.append(this.actualIntervalMs);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(8L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<LceStatusInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<LceStatusInfo> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 8);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      LceStatusInfo lceStatusInfo = new LceStatusInfo();
      lceStatusInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 8));
      arrayList.add(lceStatusInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.lceStatus = paramHwBlob.getInt32(0L + paramLong);
    this.actualIntervalMs = paramHwBlob.getInt8(4L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(8);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<LceStatusInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 8);
    for (byte b = 0; b < i; b++)
      ((LceStatusInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 8)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.lceStatus);
    paramHwBlob.putInt8(4L + paramLong, this.actualIntervalMs);
  }
}
