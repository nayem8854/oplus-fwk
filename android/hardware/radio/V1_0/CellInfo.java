package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellInfo {
  public int cellInfoType = 0;
  
  public boolean registered = false;
  
  public int timeStampType = 0;
  
  public long timeStamp = 0L;
  
  public ArrayList<CellInfoGsm> gsm = new ArrayList<>();
  
  public ArrayList<CellInfoCdma> cdma = new ArrayList<>();
  
  public ArrayList<CellInfoLte> lte = new ArrayList<>();
  
  public ArrayList<CellInfoWcdma> wcdma = new ArrayList<>();
  
  public ArrayList<CellInfoTdscdma> tdscdma = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.cellInfoType != ((CellInfo)paramObject).cellInfoType)
      return false; 
    if (this.registered != ((CellInfo)paramObject).registered)
      return false; 
    if (this.timeStampType != ((CellInfo)paramObject).timeStampType)
      return false; 
    if (this.timeStamp != ((CellInfo)paramObject).timeStamp)
      return false; 
    if (!HidlSupport.deepEquals(this.gsm, ((CellInfo)paramObject).gsm))
      return false; 
    if (!HidlSupport.deepEquals(this.cdma, ((CellInfo)paramObject).cdma))
      return false; 
    if (!HidlSupport.deepEquals(this.lte, ((CellInfo)paramObject).lte))
      return false; 
    if (!HidlSupport.deepEquals(this.wcdma, ((CellInfo)paramObject).wcdma))
      return false; 
    if (!HidlSupport.deepEquals(this.tdscdma, ((CellInfo)paramObject).tdscdma))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.cellInfoType;
    int j = HidlSupport.deepHashCode(Integer.valueOf(i));
    boolean bool = this.registered;
    i = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    int k = this.timeStampType;
    int m = HidlSupport.deepHashCode(Integer.valueOf(k));
    long l = this.timeStamp;
    int n = HidlSupport.deepHashCode(Long.valueOf(l));
    ArrayList<CellInfoGsm> arrayList4 = this.gsm;
    k = HidlSupport.deepHashCode(arrayList4);
    ArrayList<CellInfoCdma> arrayList3 = this.cdma;
    int i1 = HidlSupport.deepHashCode(arrayList3);
    ArrayList<CellInfoLte> arrayList2 = this.lte;
    int i2 = HidlSupport.deepHashCode(arrayList2);
    ArrayList<CellInfoWcdma> arrayList1 = this.wcdma;
    int i3 = HidlSupport.deepHashCode(arrayList1);
    ArrayList<CellInfoTdscdma> arrayList = this.tdscdma;
    int i4 = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(j), Integer.valueOf(i), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(k), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".cellInfoType = ");
    stringBuilder.append(CellInfoType.toString(this.cellInfoType));
    stringBuilder.append(", .registered = ");
    stringBuilder.append(this.registered);
    stringBuilder.append(", .timeStampType = ");
    stringBuilder.append(TimeStampType.toString(this.timeStampType));
    stringBuilder.append(", .timeStamp = ");
    stringBuilder.append(this.timeStamp);
    stringBuilder.append(", .gsm = ");
    stringBuilder.append(this.gsm);
    stringBuilder.append(", .cdma = ");
    stringBuilder.append(this.cdma);
    stringBuilder.append(", .lte = ");
    stringBuilder.append(this.lte);
    stringBuilder.append(", .wcdma = ");
    stringBuilder.append(this.wcdma);
    stringBuilder.append(", .tdscdma = ");
    stringBuilder.append(this.tdscdma);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(104L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellInfo> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 104);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellInfo cellInfo = new CellInfo();
      cellInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 104));
      arrayList.add(cellInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.cellInfoType = paramHwBlob.getInt32(paramLong + 0L);
    this.registered = paramHwBlob.getBool(paramLong + 4L);
    this.timeStampType = paramHwBlob.getInt32(paramLong + 8L);
    this.timeStamp = paramHwBlob.getInt64(paramLong + 16L);
    int i = paramHwBlob.getInt32(paramLong + 24L + 8L);
    long l1 = (i * 64);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob1 = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 24L + 0L, true);
    this.gsm.clear();
    byte b;
    for (b = 0; b < i; b++) {
      CellInfoGsm cellInfoGsm = new CellInfoGsm();
      cellInfoGsm.readEmbeddedFromParcel(paramHwParcel, hwBlob1, (b * 64));
      this.gsm.add(cellInfoGsm);
    } 
    i = paramHwBlob.getInt32(paramLong + 40L + 8L);
    l2 = (i * 40);
    l1 = paramHwBlob.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 40L + 0L, true);
    this.cdma.clear();
    for (b = 0; b < i; b++) {
      CellInfoCdma cellInfoCdma = new CellInfoCdma();
      cellInfoCdma.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 40));
      this.cdma.add(cellInfoCdma);
    } 
    i = paramHwBlob.getInt32(paramLong + 56L + 8L);
    l1 = (i * 72);
    l2 = paramHwBlob.handle();
    hwBlob1 = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 56L + 0L, true);
    this.lte.clear();
    for (b = 0; b < i; b++) {
      CellInfoLte cellInfoLte = new CellInfoLte();
      cellInfoLte.readEmbeddedFromParcel(paramHwParcel, hwBlob1, (b * 72));
      this.lte.add(cellInfoLte);
    } 
    i = paramHwBlob.getInt32(paramLong + 72L + 8L);
    l2 = (i * 56);
    l1 = paramHwBlob.handle();
    hwBlob1 = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 72L + 0L, true);
    this.wcdma.clear();
    for (b = 0; b < i; b++) {
      CellInfoWcdma cellInfoWcdma = new CellInfoWcdma();
      cellInfoWcdma.readEmbeddedFromParcel(paramHwParcel, hwBlob1, (b * 56));
      this.wcdma.add(cellInfoWcdma);
    } 
    i = paramHwBlob.getInt32(paramLong + 88L + 8L);
    l2 = (i * 56);
    l1 = paramHwBlob.handle();
    paramHwBlob = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 88L + 0L, true);
    this.tdscdma.clear();
    for (b = 0; b < i; b++) {
      CellInfoTdscdma cellInfoTdscdma = new CellInfoTdscdma();
      cellInfoTdscdma.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, (b * 56));
      this.tdscdma.add(cellInfoTdscdma);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(104);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 104);
    for (byte b = 0; b < i; b++)
      ((CellInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 104)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.cellInfoType);
    paramHwBlob.putBool(paramLong + 4L, this.registered);
    paramHwBlob.putInt32(paramLong + 8L, this.timeStampType);
    paramHwBlob.putInt64(paramLong + 16L, this.timeStamp);
    int i = this.gsm.size();
    paramHwBlob.putInt32(paramLong + 24L + 8L, i);
    paramHwBlob.putBool(paramLong + 24L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 64);
    byte b;
    for (b = 0; b < i; b++)
      ((CellInfoGsm)this.gsm.get(b)).writeEmbeddedToBlob(hwBlob, (b * 64)); 
    paramHwBlob.putBlob(paramLong + 24L + 0L, hwBlob);
    i = this.cdma.size();
    paramHwBlob.putInt32(paramLong + 40L + 8L, i);
    paramHwBlob.putBool(paramLong + 40L + 12L, false);
    hwBlob = new HwBlob(i * 40);
    for (b = 0; b < i; b++)
      ((CellInfoCdma)this.cdma.get(b)).writeEmbeddedToBlob(hwBlob, (b * 40)); 
    paramHwBlob.putBlob(paramLong + 40L + 0L, hwBlob);
    i = this.lte.size();
    paramHwBlob.putInt32(paramLong + 56L + 8L, i);
    paramHwBlob.putBool(paramLong + 56L + 12L, false);
    hwBlob = new HwBlob(i * 72);
    for (b = 0; b < i; b++)
      ((CellInfoLte)this.lte.get(b)).writeEmbeddedToBlob(hwBlob, (b * 72)); 
    paramHwBlob.putBlob(paramLong + 56L + 0L, hwBlob);
    i = this.wcdma.size();
    paramHwBlob.putInt32(paramLong + 72L + 8L, i);
    paramHwBlob.putBool(paramLong + 72L + 12L, false);
    hwBlob = new HwBlob(i * 56);
    for (b = 0; b < i; b++)
      ((CellInfoWcdma)this.wcdma.get(b)).writeEmbeddedToBlob(hwBlob, (b * 56)); 
    paramHwBlob.putBlob(paramLong + 72L + 0L, hwBlob);
    i = this.tdscdma.size();
    paramHwBlob.putInt32(paramLong + 88L + 8L, i);
    paramHwBlob.putBool(paramLong + 88L + 12L, false);
    hwBlob = new HwBlob(i * 56);
    for (b = 0; b < i; b++)
      ((CellInfoTdscdma)this.tdscdma.get(b)).writeEmbeddedToBlob(hwBlob, (b * 56)); 
    paramHwBlob.putBlob(paramLong + 88L + 0L, hwBlob);
  }
}
