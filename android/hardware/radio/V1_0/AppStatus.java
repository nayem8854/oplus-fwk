package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class AppStatus {
  public int appType = 0;
  
  public int appState = 0;
  
  public int persoSubstate = 0;
  
  public String aidPtr = new String();
  
  public String appLabelPtr = new String();
  
  public int pin1Replaced = 0;
  
  public int pin1 = 0;
  
  public int pin2 = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != AppStatus.class)
      return false; 
    paramObject = paramObject;
    if (this.appType != ((AppStatus)paramObject).appType)
      return false; 
    if (this.appState != ((AppStatus)paramObject).appState)
      return false; 
    if (this.persoSubstate != ((AppStatus)paramObject).persoSubstate)
      return false; 
    if (!HidlSupport.deepEquals(this.aidPtr, ((AppStatus)paramObject).aidPtr))
      return false; 
    if (!HidlSupport.deepEquals(this.appLabelPtr, ((AppStatus)paramObject).appLabelPtr))
      return false; 
    if (this.pin1Replaced != ((AppStatus)paramObject).pin1Replaced)
      return false; 
    if (this.pin1 != ((AppStatus)paramObject).pin1)
      return false; 
    if (this.pin2 != ((AppStatus)paramObject).pin2)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.appType;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.appState;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.persoSubstate;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    String str = this.aidPtr;
    int m = HidlSupport.deepHashCode(str);
    str = this.appLabelPtr;
    int n = HidlSupport.deepHashCode(str), i1 = this.pin1Replaced;
    i1 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    int i2 = this.pin1;
    i2 = HidlSupport.deepHashCode(Integer.valueOf(i2));
    int i3 = this.pin2;
    i3 = HidlSupport.deepHashCode(Integer.valueOf(i3));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".appType = ");
    stringBuilder.append(AppType.toString(this.appType));
    stringBuilder.append(", .appState = ");
    stringBuilder.append(AppState.toString(this.appState));
    stringBuilder.append(", .persoSubstate = ");
    stringBuilder.append(PersoSubstate.toString(this.persoSubstate));
    stringBuilder.append(", .aidPtr = ");
    stringBuilder.append(this.aidPtr);
    stringBuilder.append(", .appLabelPtr = ");
    stringBuilder.append(this.appLabelPtr);
    stringBuilder.append(", .pin1Replaced = ");
    stringBuilder.append(this.pin1Replaced);
    stringBuilder.append(", .pin1 = ");
    stringBuilder.append(PinState.toString(this.pin1));
    stringBuilder.append(", .pin2 = ");
    stringBuilder.append(PinState.toString(this.pin2));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(64L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<AppStatus> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<AppStatus> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 64);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      AppStatus appStatus = new AppStatus();
      appStatus.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 64));
      arrayList.add(appStatus);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.appType = paramHwBlob.getInt32(paramLong + 0L);
    this.appState = paramHwBlob.getInt32(paramLong + 4L);
    this.persoSubstate = paramHwBlob.getInt32(paramLong + 8L);
    String str = paramHwBlob.getString(paramLong + 16L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 16L + 0L, false);
    this.appLabelPtr = str = paramHwBlob.getString(paramLong + 32L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 32L + 0L, false);
    this.pin1Replaced = paramHwBlob.getInt32(paramLong + 48L);
    this.pin1 = paramHwBlob.getInt32(paramLong + 52L);
    this.pin2 = paramHwBlob.getInt32(paramLong + 56L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(64);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<AppStatus> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 64);
    for (byte b = 0; b < i; b++)
      ((AppStatus)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 64)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.appType);
    paramHwBlob.putInt32(4L + paramLong, this.appState);
    paramHwBlob.putInt32(8L + paramLong, this.persoSubstate);
    paramHwBlob.putString(16L + paramLong, this.aidPtr);
    paramHwBlob.putString(32L + paramLong, this.appLabelPtr);
    paramHwBlob.putInt32(48L + paramLong, this.pin1Replaced);
    paramHwBlob.putInt32(52L + paramLong, this.pin1);
    paramHwBlob.putInt32(56L + paramLong, this.pin2);
  }
}
