package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class DataRegStateResult {
  public int regState = 0;
  
  public int rat = 0;
  
  public int reasonDataDenied = 0;
  
  public int maxDataCalls = 0;
  
  public CellIdentity cellIdentity = new CellIdentity();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != DataRegStateResult.class)
      return false; 
    paramObject = paramObject;
    if (this.regState != ((DataRegStateResult)paramObject).regState)
      return false; 
    if (this.rat != ((DataRegStateResult)paramObject).rat)
      return false; 
    if (this.reasonDataDenied != ((DataRegStateResult)paramObject).reasonDataDenied)
      return false; 
    if (this.maxDataCalls != ((DataRegStateResult)paramObject).maxDataCalls)
      return false; 
    if (!HidlSupport.deepEquals(this.cellIdentity, ((DataRegStateResult)paramObject).cellIdentity))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.regState;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.rat;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.reasonDataDenied;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.maxDataCalls;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    CellIdentity cellIdentity = this.cellIdentity;
    int n = HidlSupport.deepHashCode(cellIdentity);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".regState = ");
    stringBuilder.append(RegState.toString(this.regState));
    stringBuilder.append(", .rat = ");
    stringBuilder.append(this.rat);
    stringBuilder.append(", .reasonDataDenied = ");
    stringBuilder.append(this.reasonDataDenied);
    stringBuilder.append(", .maxDataCalls = ");
    stringBuilder.append(this.maxDataCalls);
    stringBuilder.append(", .cellIdentity = ");
    stringBuilder.append(this.cellIdentity);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(104L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<DataRegStateResult> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<DataRegStateResult> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 104);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      DataRegStateResult dataRegStateResult = new DataRegStateResult();
      dataRegStateResult.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 104));
      arrayList.add(dataRegStateResult);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.regState = paramHwBlob.getInt32(0L + paramLong);
    this.rat = paramHwBlob.getInt32(4L + paramLong);
    this.reasonDataDenied = paramHwBlob.getInt32(8L + paramLong);
    this.maxDataCalls = paramHwBlob.getInt32(12L + paramLong);
    this.cellIdentity.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 16L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(104);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<DataRegStateResult> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 104);
    for (byte b = 0; b < i; b++)
      ((DataRegStateResult)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 104)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.regState);
    paramHwBlob.putInt32(4L + paramLong, this.rat);
    paramHwBlob.putInt32(8L + paramLong, this.reasonDataDenied);
    paramHwBlob.putInt32(12L + paramLong, this.maxDataCalls);
    this.cellIdentity.writeEmbeddedToBlob(paramHwBlob, 16L + paramLong);
  }
}
