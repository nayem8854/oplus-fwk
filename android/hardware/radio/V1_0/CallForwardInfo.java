package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CallForwardInfo {
  public int status = 0;
  
  public int reason = 0;
  
  public int serviceClass = 0;
  
  public int toa = 0;
  
  public String number = new String();
  
  public int timeSeconds = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CallForwardInfo.class)
      return false; 
    paramObject = paramObject;
    if (this.status != ((CallForwardInfo)paramObject).status)
      return false; 
    if (this.reason != ((CallForwardInfo)paramObject).reason)
      return false; 
    if (this.serviceClass != ((CallForwardInfo)paramObject).serviceClass)
      return false; 
    if (this.toa != ((CallForwardInfo)paramObject).toa)
      return false; 
    if (!HidlSupport.deepEquals(this.number, ((CallForwardInfo)paramObject).number))
      return false; 
    if (this.timeSeconds != ((CallForwardInfo)paramObject).timeSeconds)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.status;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.reason;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.serviceClass;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.toa;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    String str = this.number;
    int n = HidlSupport.deepHashCode(str), i1 = this.timeSeconds;
    i1 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".status = ");
    stringBuilder.append(CallForwardInfoStatus.toString(this.status));
    stringBuilder.append(", .reason = ");
    stringBuilder.append(this.reason);
    stringBuilder.append(", .serviceClass = ");
    stringBuilder.append(this.serviceClass);
    stringBuilder.append(", .toa = ");
    stringBuilder.append(this.toa);
    stringBuilder.append(", .number = ");
    stringBuilder.append(this.number);
    stringBuilder.append(", .timeSeconds = ");
    stringBuilder.append(this.timeSeconds);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(40L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CallForwardInfo> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CallForwardInfo> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 40);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CallForwardInfo callForwardInfo = new CallForwardInfo();
      callForwardInfo.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 40));
      arrayList.add(callForwardInfo);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.status = paramHwBlob.getInt32(paramLong + 0L);
    this.reason = paramHwBlob.getInt32(paramLong + 4L);
    this.serviceClass = paramHwBlob.getInt32(paramLong + 8L);
    this.toa = paramHwBlob.getInt32(paramLong + 12L);
    String str = paramHwBlob.getString(paramLong + 16L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 16L + 0L, false);
    this.timeSeconds = paramHwBlob.getInt32(paramLong + 32L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(40);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CallForwardInfo> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 40);
    for (byte b = 0; b < i; b++)
      ((CallForwardInfo)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 40)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.status);
    paramHwBlob.putInt32(4L + paramLong, this.reason);
    paramHwBlob.putInt32(8L + paramLong, this.serviceClass);
    paramHwBlob.putInt32(12L + paramLong, this.toa);
    paramHwBlob.putString(16L + paramLong, this.number);
    paramHwBlob.putInt32(32L + paramLong, this.timeSeconds);
  }
}
