package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class StkCcUnsolSsResult {
  public int serviceType = 0;
  
  public int requestType = 0;
  
  public int teleserviceType = 0;
  
  public int result = 0;
  
  public ArrayList<SsInfoData> ssInfo = new ArrayList<>();
  
  public ArrayList<CfData> cfData = new ArrayList<>();
  
  public int serviceClass;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != StkCcUnsolSsResult.class)
      return false; 
    paramObject = paramObject;
    if (this.serviceType != ((StkCcUnsolSsResult)paramObject).serviceType)
      return false; 
    if (this.requestType != ((StkCcUnsolSsResult)paramObject).requestType)
      return false; 
    if (this.teleserviceType != ((StkCcUnsolSsResult)paramObject).teleserviceType)
      return false; 
    if (!HidlSupport.deepEquals(Integer.valueOf(this.serviceClass), Integer.valueOf(((StkCcUnsolSsResult)paramObject).serviceClass)))
      return false; 
    if (this.result != ((StkCcUnsolSsResult)paramObject).result)
      return false; 
    if (!HidlSupport.deepEquals(this.ssInfo, ((StkCcUnsolSsResult)paramObject).ssInfo))
      return false; 
    if (!HidlSupport.deepEquals(this.cfData, ((StkCcUnsolSsResult)paramObject).cfData))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.serviceType;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.requestType;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.teleserviceType;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    int m = this.serviceClass;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    int n = this.result;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    ArrayList<SsInfoData> arrayList1 = this.ssInfo;
    int i1 = HidlSupport.deepHashCode(arrayList1);
    ArrayList<CfData> arrayList = this.cfData;
    int i2 = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".serviceType = ");
    stringBuilder.append(SsServiceType.toString(this.serviceType));
    stringBuilder.append(", .requestType = ");
    stringBuilder.append(SsRequestType.toString(this.requestType));
    stringBuilder.append(", .teleserviceType = ");
    stringBuilder.append(SsTeleserviceType.toString(this.teleserviceType));
    stringBuilder.append(", .serviceClass = ");
    stringBuilder.append(SuppServiceClass.dumpBitfield(this.serviceClass));
    stringBuilder.append(", .result = ");
    stringBuilder.append(RadioError.toString(this.result));
    stringBuilder.append(", .ssInfo = ");
    stringBuilder.append(this.ssInfo);
    stringBuilder.append(", .cfData = ");
    stringBuilder.append(this.cfData);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(56L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<StkCcUnsolSsResult> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<StkCcUnsolSsResult> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 56);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      StkCcUnsolSsResult stkCcUnsolSsResult = new StkCcUnsolSsResult();
      stkCcUnsolSsResult.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 56));
      arrayList.add(stkCcUnsolSsResult);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.serviceType = paramHwBlob.getInt32(paramLong + 0L);
    this.requestType = paramHwBlob.getInt32(paramLong + 4L);
    this.teleserviceType = paramHwBlob.getInt32(paramLong + 8L);
    this.serviceClass = paramHwBlob.getInt32(paramLong + 12L);
    this.result = paramHwBlob.getInt32(paramLong + 16L);
    int i = paramHwBlob.getInt32(paramLong + 24L + 8L);
    long l1 = (i * 16);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob1 = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 24L + 0L, true);
    this.ssInfo.clear();
    byte b;
    for (b = 0; b < i; b++) {
      SsInfoData ssInfoData = new SsInfoData();
      ssInfoData.readEmbeddedFromParcel(paramHwParcel, hwBlob1, (b * 16));
      this.ssInfo.add(ssInfoData);
    } 
    i = paramHwBlob.getInt32(paramLong + 40L + 8L);
    l2 = (i * 16);
    l1 = paramHwBlob.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 40L + 0L, true);
    this.cfData.clear();
    for (b = 0; b < i; b++) {
      CfData cfData = new CfData();
      cfData.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 16));
      this.cfData.add(cfData);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(56);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<StkCcUnsolSsResult> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 56);
    for (byte b = 0; b < i; b++)
      ((StkCcUnsolSsResult)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 56)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.serviceType);
    paramHwBlob.putInt32(paramLong + 4L, this.requestType);
    paramHwBlob.putInt32(paramLong + 8L, this.teleserviceType);
    paramHwBlob.putInt32(paramLong + 12L, this.serviceClass);
    paramHwBlob.putInt32(paramLong + 16L, this.result);
    int i = this.ssInfo.size();
    paramHwBlob.putInt32(paramLong + 24L + 8L, i);
    paramHwBlob.putBool(paramLong + 24L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 16);
    byte b;
    for (b = 0; b < i; b++)
      ((SsInfoData)this.ssInfo.get(b)).writeEmbeddedToBlob(hwBlob, (b * 16)); 
    paramHwBlob.putBlob(paramLong + 24L + 0L, hwBlob);
    i = this.cfData.size();
    paramHwBlob.putInt32(paramLong + 40L + 8L, i);
    paramHwBlob.putBool(paramLong + 40L + 12L, false);
    hwBlob = new HwBlob(i * 16);
    for (b = 0; b < i; b++)
      ((CfData)this.cfData.get(b)).writeEmbeddedToBlob(hwBlob, (b * 16)); 
    paramHwBlob.putBlob(paramLong + 40L + 0L, hwBlob);
  }
}
