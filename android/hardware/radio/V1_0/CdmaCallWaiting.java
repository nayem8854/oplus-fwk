package android.hardware.radio.V1_0;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CdmaCallWaiting {
  public String number = new String();
  
  public int numberPresentation = 0;
  
  public String name = new String();
  
  public CdmaSignalInfoRecord signalInfoRecord = new CdmaSignalInfoRecord();
  
  public int numberType = 0;
  
  public int numberPlan = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CdmaCallWaiting.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.number, ((CdmaCallWaiting)paramObject).number))
      return false; 
    if (this.numberPresentation != ((CdmaCallWaiting)paramObject).numberPresentation)
      return false; 
    if (!HidlSupport.deepEquals(this.name, ((CdmaCallWaiting)paramObject).name))
      return false; 
    if (!HidlSupport.deepEquals(this.signalInfoRecord, ((CdmaCallWaiting)paramObject).signalInfoRecord))
      return false; 
    if (this.numberType != ((CdmaCallWaiting)paramObject).numberType)
      return false; 
    if (this.numberPlan != ((CdmaCallWaiting)paramObject).numberPlan)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    String str = this.number;
    int i = HidlSupport.deepHashCode(str), j = this.numberPresentation;
    int k = HidlSupport.deepHashCode(Integer.valueOf(j));
    str = this.name;
    int m = HidlSupport.deepHashCode(str);
    CdmaSignalInfoRecord cdmaSignalInfoRecord = this.signalInfoRecord;
    j = HidlSupport.deepHashCode(cdmaSignalInfoRecord);
    int n = this.numberType;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    int i1 = this.numberPlan;
    i1 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(j), Integer.valueOf(n), Integer.valueOf(i1) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".number = ");
    stringBuilder.append(this.number);
    stringBuilder.append(", .numberPresentation = ");
    stringBuilder.append(CdmaCallWaitingNumberPresentation.toString(this.numberPresentation));
    stringBuilder.append(", .name = ");
    stringBuilder.append(this.name);
    stringBuilder.append(", .signalInfoRecord = ");
    stringBuilder.append(this.signalInfoRecord);
    stringBuilder.append(", .numberType = ");
    stringBuilder.append(CdmaCallWaitingNumberType.toString(this.numberType));
    stringBuilder.append(", .numberPlan = ");
    stringBuilder.append(CdmaCallWaitingNumberPlan.toString(this.numberPlan));
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(56L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CdmaCallWaiting> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CdmaCallWaiting> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 56);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CdmaCallWaiting cdmaCallWaiting = new CdmaCallWaiting();
      cdmaCallWaiting.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 56));
      arrayList.add(cdmaCallWaiting);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    String str = paramHwBlob.getString(paramLong + 0L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, false);
    this.numberPresentation = paramHwBlob.getInt32(paramLong + 16L);
    this.name = str = paramHwBlob.getString(paramLong + 24L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 24L + 0L, false);
    this.signalInfoRecord.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 40L);
    this.numberType = paramHwBlob.getInt32(paramLong + 44L);
    this.numberPlan = paramHwBlob.getInt32(paramLong + 48L);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(56);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CdmaCallWaiting> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 56);
    for (byte b = 0; b < i; b++)
      ((CdmaCallWaiting)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 56)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putString(0L + paramLong, this.number);
    paramHwBlob.putInt32(16L + paramLong, this.numberPresentation);
    paramHwBlob.putString(24L + paramLong, this.name);
    this.signalInfoRecord.writeEmbeddedToBlob(paramHwBlob, 40L + paramLong);
    paramHwBlob.putInt32(44L + paramLong, this.numberType);
    paramHwBlob.putInt32(48L + paramLong, this.numberPlan);
  }
}
