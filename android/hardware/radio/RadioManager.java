package android.hardware.radio;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.function.ToIntFunction;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@SystemApi
public class RadioManager {
  public static final int BAND_AM = 0;
  
  public static final int BAND_AM_HD = 3;
  
  public static final int BAND_FM = 1;
  
  public static final int BAND_FM_HD = 2;
  
  public static final int BAND_INVALID = -1;
  
  public static final int CLASS_AM_FM = 0;
  
  public static final int CLASS_DT = 2;
  
  public static final int CLASS_SAT = 1;
  
  public static final int CONFIG_DAB_DAB_LINKING = 6;
  
  public static final int CONFIG_DAB_DAB_SOFT_LINKING = 8;
  
  public static final int CONFIG_DAB_FM_LINKING = 7;
  
  public static final int CONFIG_DAB_FM_SOFT_LINKING = 9;
  
  public static final int CONFIG_FORCE_ANALOG = 2;
  
  public static final int CONFIG_FORCE_DIGITAL = 3;
  
  public static final int CONFIG_FORCE_MONO = 1;
  
  public static final int CONFIG_RDS_AF = 4;
  
  public static final int CONFIG_RDS_REG = 5;
  
  public static final int REGION_ITU_1 = 0;
  
  public static final int REGION_ITU_2 = 1;
  
  public static final int REGION_JAPAN = 3;
  
  public static final int REGION_KOREA = 4;
  
  public static final int REGION_OIRT = 2;
  
  public static final int STATUS_BAD_VALUE = -22;
  
  public static final int STATUS_DEAD_OBJECT = -32;
  
  public static final int STATUS_ERROR = -2147483648;
  
  public static final int STATUS_INVALID_OPERATION = -38;
  
  public static final int STATUS_NO_INIT = -19;
  
  public static final int STATUS_OK = 0;
  
  public static final int STATUS_PERMISSION_DENIED = -1;
  
  public static final int STATUS_TIMED_OUT = -110;
  
  private static final String TAG = "BroadcastRadio.manager";
  
  class ModuleProperties implements Parcelable {
    public ModuleProperties(RadioManager this$0, String param1String1, int param1Int1, String param1String2, String param1String3, String param1String4, String param1String5, int param1Int2, int param1Int3, boolean param1Boolean1, boolean param1Boolean2, RadioManager.BandDescriptor[] param1ArrayOfBandDescriptor, boolean param1Boolean3, int[] param1ArrayOfint1, int[] param1ArrayOfint2, Map<String, Integer> param1Map, Map<String, String> param1Map1) {
      this.mId = this$0;
      if (TextUtils.isEmpty(param1String1))
        param1String1 = "default"; 
      this.mServiceName = param1String1;
      this.mClassId = param1Int1;
      this.mImplementor = param1String2;
      this.mProduct = param1String3;
      this.mVersion = param1String4;
      this.mSerial = param1String5;
      this.mNumTuners = param1Int2;
      this.mNumAudioSources = param1Int3;
      this.mIsInitializationRequired = param1Boolean1;
      this.mIsCaptureSupported = param1Boolean2;
      this.mBands = param1ArrayOfBandDescriptor;
      this.mIsBgScanSupported = param1Boolean3;
      this.mSupportedProgramTypes = arrayToSet(param1ArrayOfint1);
      this.mSupportedIdentifierTypes = arrayToSet(param1ArrayOfint2);
      if (param1Map != null)
        for (Map.Entry<String, Integer> entry : param1Map.entrySet()) {
          Objects.requireNonNull((String)entry.getKey());
          Objects.requireNonNull((Integer)entry.getValue());
        }  
      this.mDabFrequencyTable = param1Map;
      if (param1Map1 == null)
        param1Map1 = new HashMap<>(); 
      this.mVendorInfo = param1Map1;
    }
    
    private static Set<Integer> arrayToSet(int[] param1ArrayOfint) {
      return Arrays.stream(param1ArrayOfint).boxed().collect((Collector)Collectors.toSet());
    }
    
    private static int[] setToArray(Set<Integer> param1Set) {
      return param1Set.stream().mapToInt((ToIntFunction)_$$Lambda$UV1wDVoVlbcxpr8zevj_aMFtUGw.INSTANCE).toArray();
    }
    
    public int getId() {
      return this.mId;
    }
    
    public String getServiceName() {
      return this.mServiceName;
    }
    
    public int getClassId() {
      return this.mClassId;
    }
    
    public String getImplementor() {
      return this.mImplementor;
    }
    
    public String getProduct() {
      return this.mProduct;
    }
    
    public String getVersion() {
      return this.mVersion;
    }
    
    public String getSerial() {
      return this.mSerial;
    }
    
    public int getNumTuners() {
      return this.mNumTuners;
    }
    
    public int getNumAudioSources() {
      return this.mNumAudioSources;
    }
    
    public boolean isInitializationRequired() {
      return this.mIsInitializationRequired;
    }
    
    public boolean isCaptureSupported() {
      return this.mIsCaptureSupported;
    }
    
    public boolean isBackgroundScanningSupported() {
      return this.mIsBgScanSupported;
    }
    
    public boolean isProgramTypeSupported(int param1Int) {
      return this.mSupportedProgramTypes.contains(Integer.valueOf(param1Int));
    }
    
    public boolean isProgramIdentifierSupported(int param1Int) {
      return this.mSupportedIdentifierTypes.contains(Integer.valueOf(param1Int));
    }
    
    public Map<String, Integer> getDabFrequencyTable() {
      return this.mDabFrequencyTable;
    }
    
    public Map<String, String> getVendorInfo() {
      return this.mVendorInfo;
    }
    
    public RadioManager.BandDescriptor[] getBands() {
      return this.mBands;
    }
    
    private ModuleProperties(RadioManager this$0) {
      this.mId = this$0.readInt();
      String str = this$0.readString();
      if (TextUtils.isEmpty(str))
        str = "default"; 
      this.mServiceName = str;
      this.mClassId = this$0.readInt();
      this.mImplementor = this$0.readString();
      this.mProduct = this$0.readString();
      this.mVersion = this$0.readString();
      this.mSerial = this$0.readString();
      this.mNumTuners = this$0.readInt();
      this.mNumAudioSources = this$0.readInt();
      int i = this$0.readInt();
      boolean bool1 = false;
      if (i == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mIsInitializationRequired = bool2;
      if (this$0.readInt() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mIsCaptureSupported = bool2;
      Parcelable[] arrayOfParcelable = this$0.readParcelableArray(RadioManager.BandDescriptor.class.getClassLoader());
      this.mBands = new RadioManager.BandDescriptor[arrayOfParcelable.length];
      for (i = 0; i < arrayOfParcelable.length; i++)
        this.mBands[i] = (RadioManager.BandDescriptor)arrayOfParcelable[i]; 
      boolean bool2 = bool1;
      if (this$0.readInt() == 1)
        bool2 = true; 
      this.mIsBgScanSupported = bool2;
      this.mSupportedProgramTypes = arrayToSet(this$0.createIntArray());
      this.mSupportedIdentifierTypes = arrayToSet(this$0.createIntArray());
      this.mDabFrequencyTable = Utils.readStringIntMap((Parcel)this$0);
      this.mVendorInfo = Utils.readStringMap((Parcel)this$0);
    }
    
    public static final Parcelable.Creator<ModuleProperties> CREATOR = new Parcelable.Creator<ModuleProperties>() {
        public RadioManager.ModuleProperties createFromParcel(Parcel param2Parcel) {
          return new RadioManager.ModuleProperties();
        }
        
        public RadioManager.ModuleProperties[] newArray(int param2Int) {
          return new RadioManager.ModuleProperties[param2Int];
        }
      };
    
    private final RadioManager.BandDescriptor[] mBands;
    
    private final int mClassId;
    
    private final Map<String, Integer> mDabFrequencyTable;
    
    private final int mId;
    
    private final String mImplementor;
    
    private final boolean mIsBgScanSupported;
    
    private final boolean mIsCaptureSupported;
    
    private final boolean mIsInitializationRequired;
    
    private final int mNumAudioSources;
    
    private final int mNumTuners;
    
    private final String mProduct;
    
    private final String mSerial;
    
    private final String mServiceName;
    
    private final Set<Integer> mSupportedIdentifierTypes;
    
    private final Set<Integer> mSupportedProgramTypes;
    
    private final Map<String, String> mVendorInfo;
    
    private final String mVersion;
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mId);
      param1Parcel.writeString(this.mServiceName);
      param1Parcel.writeInt(this.mClassId);
      param1Parcel.writeString(this.mImplementor);
      param1Parcel.writeString(this.mProduct);
      param1Parcel.writeString(this.mVersion);
      param1Parcel.writeString(this.mSerial);
      param1Parcel.writeInt(this.mNumTuners);
      param1Parcel.writeInt(this.mNumAudioSources);
      param1Parcel.writeInt(this.mIsInitializationRequired);
      param1Parcel.writeInt(this.mIsCaptureSupported);
      param1Parcel.writeParcelableArray((Parcelable[])this.mBands, param1Int);
      param1Parcel.writeInt(this.mIsBgScanSupported);
      param1Parcel.writeIntArray(setToArray(this.mSupportedProgramTypes));
      param1Parcel.writeIntArray(setToArray(this.mSupportedIdentifierTypes));
      Utils.writeStringIntMap(param1Parcel, this.mDabFrequencyTable);
      Utils.writeStringMap(param1Parcel, this.mVendorInfo);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ModuleProperties [mId=");
      stringBuilder.append(this.mId);
      stringBuilder.append(", mServiceName=");
      stringBuilder.append(this.mServiceName);
      stringBuilder.append(", mClassId=");
      stringBuilder.append(this.mClassId);
      stringBuilder.append(", mImplementor=");
      stringBuilder.append(this.mImplementor);
      stringBuilder.append(", mProduct=");
      stringBuilder.append(this.mProduct);
      stringBuilder.append(", mVersion=");
      stringBuilder.append(this.mVersion);
      stringBuilder.append(", mSerial=");
      stringBuilder.append(this.mSerial);
      stringBuilder.append(", mNumTuners=");
      stringBuilder.append(this.mNumTuners);
      stringBuilder.append(", mNumAudioSources=");
      stringBuilder.append(this.mNumAudioSources);
      stringBuilder.append(", mIsInitializationRequired=");
      stringBuilder.append(this.mIsInitializationRequired);
      stringBuilder.append(", mIsCaptureSupported=");
      stringBuilder.append(this.mIsCaptureSupported);
      stringBuilder.append(", mIsBgScanSupported=");
      stringBuilder.append(this.mIsBgScanSupported);
      stringBuilder.append(", mBands=");
      RadioManager.BandDescriptor[] arrayOfBandDescriptor = this.mBands;
      stringBuilder.append(Arrays.toString((Object[])arrayOfBandDescriptor));
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public int hashCode() {
      int i = this.mId;
      String str1 = this.mServiceName;
      int j = this.mClassId;
      String str2 = this.mImplementor, str3 = this.mProduct, str4 = this.mVersion, str5 = this.mSerial;
      int k = this.mNumTuners;
      int m = this.mNumAudioSources;
      boolean bool1 = this.mIsInitializationRequired, bool2 = this.mIsCaptureSupported;
      RadioManager.BandDescriptor[] arrayOfBandDescriptor = this.mBands;
      boolean bool3 = this.mIsBgScanSupported;
      Map<String, Integer> map = this.mDabFrequencyTable;
      Map<String, String> map1 = this.mVendorInfo;
      return Objects.hash(new Object[] { 
            Integer.valueOf(i), str1, Integer.valueOf(j), str2, str3, str4, str5, Integer.valueOf(k), Integer.valueOf(m), Boolean.valueOf(bool1), 
            Boolean.valueOf(bool2), arrayOfBandDescriptor, Boolean.valueOf(bool3), map, map1 });
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof ModuleProperties))
        return false; 
      param1Object = param1Object;
      if (this.mId != param1Object.getId())
        return false; 
      if (!TextUtils.equals(this.mServiceName, ((ModuleProperties)param1Object).mServiceName))
        return false; 
      if (this.mClassId != ((ModuleProperties)param1Object).mClassId)
        return false; 
      if (!Objects.equals(this.mImplementor, ((ModuleProperties)param1Object).mImplementor))
        return false; 
      if (!Objects.equals(this.mProduct, ((ModuleProperties)param1Object).mProduct))
        return false; 
      if (!Objects.equals(this.mVersion, ((ModuleProperties)param1Object).mVersion))
        return false; 
      if (!Objects.equals(this.mSerial, ((ModuleProperties)param1Object).mSerial))
        return false; 
      if (this.mNumTuners != ((ModuleProperties)param1Object).mNumTuners)
        return false; 
      if (this.mNumAudioSources != ((ModuleProperties)param1Object).mNumAudioSources)
        return false; 
      if (this.mIsInitializationRequired != ((ModuleProperties)param1Object).mIsInitializationRequired)
        return false; 
      if (this.mIsCaptureSupported != ((ModuleProperties)param1Object).mIsCaptureSupported)
        return false; 
      if (!Objects.equals(this.mBands, ((ModuleProperties)param1Object).mBands))
        return false; 
      if (this.mIsBgScanSupported != ((ModuleProperties)param1Object).mIsBgScanSupported)
        return false; 
      if (!Objects.equals(this.mDabFrequencyTable, ((ModuleProperties)param1Object).mDabFrequencyTable))
        return false; 
      if (!Objects.equals(this.mVendorInfo, ((ModuleProperties)param1Object).mVendorInfo))
        return false; 
      return true;
    }
  }
  
  class BandDescriptor implements Parcelable {
    BandDescriptor(RadioManager this$0, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      if (param1Int1 == 0 || param1Int1 == 1 || param1Int1 == 2 || param1Int1 == 3) {
        this.mRegion = this$0;
        this.mType = param1Int1;
        this.mLowerLimit = param1Int2;
        this.mUpperLimit = param1Int3;
        this.mSpacing = param1Int4;
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported band: ");
      stringBuilder.append(param1Int1);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public int getRegion() {
      return this.mRegion;
    }
    
    public int getType() {
      return this.mType;
    }
    
    public boolean isAmBand() {
      int i = this.mType;
      return (i == 0 || i == 3);
    }
    
    public boolean isFmBand() {
      int i = this.mType;
      boolean bool1 = true, bool2 = bool1;
      if (i != 1)
        if (i == 2) {
          bool2 = bool1;
        } else {
          bool2 = false;
        }  
      return bool2;
    }
    
    public int getLowerLimit() {
      return this.mLowerLimit;
    }
    
    public int getUpperLimit() {
      return this.mUpperLimit;
    }
    
    public int getSpacing() {
      return this.mSpacing;
    }
    
    private BandDescriptor(RadioManager this$0) {
      this.mRegion = this$0.readInt();
      this.mType = this$0.readInt();
      this.mLowerLimit = this$0.readInt();
      this.mUpperLimit = this$0.readInt();
      this.mSpacing = this$0.readInt();
    }
    
    private static int lookupTypeFromParcel(Parcel param1Parcel) {
      int i = param1Parcel.dataPosition();
      param1Parcel.readInt();
      int j = param1Parcel.readInt();
      param1Parcel.setDataPosition(i);
      return j;
    }
    
    public static final Parcelable.Creator<BandDescriptor> CREATOR = new Parcelable.Creator<BandDescriptor>() {
        public RadioManager.BandDescriptor createFromParcel(Parcel param2Parcel) {
          StringBuilder stringBuilder;
          int i = RadioManager.BandDescriptor.lookupTypeFromParcel(param2Parcel);
          if (i != 0)
            if (i != 1 && i != 2) {
              if (i != 3) {
                stringBuilder = new StringBuilder();
                stringBuilder.append("Unsupported band: ");
                stringBuilder.append(i);
                throw new IllegalArgumentException(stringBuilder.toString());
              } 
            } else {
              return new RadioManager.FmBandDescriptor();
            }  
          return new RadioManager.AmBandDescriptor();
        }
        
        public RadioManager.BandDescriptor[] newArray(int param2Int) {
          return new RadioManager.BandDescriptor[param2Int];
        }
      };
    
    private final int mLowerLimit;
    
    private final int mRegion;
    
    private final int mSpacing;
    
    private final int mType;
    
    private final int mUpperLimit;
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mRegion);
      param1Parcel.writeInt(this.mType);
      param1Parcel.writeInt(this.mLowerLimit);
      param1Parcel.writeInt(this.mUpperLimit);
      param1Parcel.writeInt(this.mSpacing);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("BandDescriptor [mRegion=");
      stringBuilder.append(this.mRegion);
      stringBuilder.append(", mType=");
      stringBuilder.append(this.mType);
      stringBuilder.append(", mLowerLimit=");
      stringBuilder.append(this.mLowerLimit);
      stringBuilder.append(", mUpperLimit=");
      stringBuilder.append(this.mUpperLimit);
      stringBuilder.append(", mSpacing=");
      stringBuilder.append(this.mSpacing);
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public int hashCode() {
      int i = this.mRegion;
      int j = this.mType;
      int k = this.mLowerLimit;
      int m = this.mUpperLimit;
      int n = this.mSpacing;
      return ((((1 * 31 + i) * 31 + j) * 31 + k) * 31 + m) * 31 + n;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof BandDescriptor))
        return false; 
      param1Object = param1Object;
      if (this.mRegion != param1Object.getRegion())
        return false; 
      if (this.mType != param1Object.getType())
        return false; 
      if (this.mLowerLimit != param1Object.getLowerLimit())
        return false; 
      if (this.mUpperLimit != param1Object.getUpperLimit())
        return false; 
      if (this.mSpacing != param1Object.getSpacing())
        return false; 
      return true;
    }
  }
  
  class FmBandDescriptor extends BandDescriptor {
    public FmBandDescriptor(RadioManager this$0, int param1Int1, int param1Int2, int param1Int3, int param1Int4, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, boolean param1Boolean5) {
      super(param1Int1, param1Int2, param1Int3, param1Int4);
      this.mStereo = param1Boolean1;
      this.mRds = param1Boolean2;
      this.mTa = param1Boolean3;
      this.mAf = param1Boolean4;
      this.mEa = param1Boolean5;
    }
    
    public boolean isStereoSupported() {
      return this.mStereo;
    }
    
    public boolean isRdsSupported() {
      return this.mRds;
    }
    
    public boolean isTaSupported() {
      return this.mTa;
    }
    
    public boolean isAfSupported() {
      return this.mAf;
    }
    
    public boolean isEaSupported() {
      return this.mEa;
    }
    
    private FmBandDescriptor(RadioManager this$0) {
      byte b = this$0.readByte();
      boolean bool1 = false;
      if (b == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mStereo = bool2;
      if (this$0.readByte() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mRds = bool2;
      if (this$0.readByte() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mTa = bool2;
      if (this$0.readByte() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mAf = bool2;
      boolean bool2 = bool1;
      if (this$0.readByte() == 1)
        bool2 = true; 
      this.mEa = bool2;
    }
    
    public static final Parcelable.Creator<FmBandDescriptor> CREATOR = (Parcelable.Creator<FmBandDescriptor>)new Object();
    
    private final boolean mAf;
    
    private final boolean mEa;
    
    private final boolean mRds;
    
    private final boolean mStereo;
    
    private final boolean mTa;
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeByte((byte)this.mStereo);
      param1Parcel.writeByte((byte)this.mRds);
      param1Parcel.writeByte((byte)this.mTa);
      param1Parcel.writeByte((byte)this.mAf);
      param1Parcel.writeByte((byte)this.mEa);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("FmBandDescriptor [ ");
      stringBuilder.append(super.toString());
      stringBuilder.append(" mStereo=");
      stringBuilder.append(this.mStereo);
      stringBuilder.append(", mRds=");
      stringBuilder.append(this.mRds);
      stringBuilder.append(", mTa=");
      stringBuilder.append(this.mTa);
      stringBuilder.append(", mAf=");
      stringBuilder.append(this.mAf);
      stringBuilder.append(", mEa =");
      stringBuilder.append(this.mEa);
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public int hashCode() {
      int i = super.hashCode();
      boolean bool1 = this.mStereo;
      boolean bool2 = this.mRds;
      boolean bool3 = this.mTa;
      boolean bool4 = this.mAf;
      boolean bool5 = this.mEa;
      return ((((i * 31 + bool1) * 31 + bool2) * 31 + bool3) * 31 + bool4) * 31 + bool5;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (!super.equals(param1Object))
        return false; 
      if (!(param1Object instanceof FmBandDescriptor))
        return false; 
      param1Object = param1Object;
      if (this.mStereo != param1Object.isStereoSupported())
        return false; 
      if (this.mRds != param1Object.isRdsSupported())
        return false; 
      if (this.mTa != param1Object.isTaSupported())
        return false; 
      if (this.mAf != param1Object.isAfSupported())
        return false; 
      if (this.mEa != param1Object.isEaSupported())
        return false; 
      return true;
    }
  }
  
  class AmBandDescriptor extends BandDescriptor {
    public AmBandDescriptor(RadioManager this$0, int param1Int1, int param1Int2, int param1Int3, int param1Int4, boolean param1Boolean) {
      super(param1Int1, param1Int2, param1Int3, param1Int4);
      this.mStereo = param1Boolean;
    }
    
    public boolean isStereoSupported() {
      return this.mStereo;
    }
    
    private AmBandDescriptor(RadioManager this$0) {
      byte b = this$0.readByte();
      boolean bool = true;
      if (b != 1)
        bool = false; 
      this.mStereo = bool;
    }
    
    public static final Parcelable.Creator<AmBandDescriptor> CREATOR = (Parcelable.Creator<AmBandDescriptor>)new Object();
    
    private final boolean mStereo;
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeByte((byte)this.mStereo);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AmBandDescriptor [ ");
      stringBuilder.append(super.toString());
      stringBuilder.append(" mStereo=");
      stringBuilder.append(this.mStereo);
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public int hashCode() {
      int i = super.hashCode();
      boolean bool = this.mStereo;
      return i * 31 + bool;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (!super.equals(param1Object))
        return false; 
      if (!(param1Object instanceof AmBandDescriptor))
        return false; 
      param1Object = param1Object;
      if (this.mStereo != param1Object.isStereoSupported())
        return false; 
      return true;
    }
  }
  
  class BandConfig implements Parcelable {
    BandConfig(RadioManager this$0) {
      Objects.requireNonNull(this$0);
      this.mDescriptor = (RadioManager.BandDescriptor)this$0;
    }
    
    BandConfig(RadioManager this$0, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this.mDescriptor = new RadioManager.BandDescriptor(param1Int1, param1Int2, param1Int3, param1Int4);
    }
    
    private BandConfig(RadioManager this$0) {
      this.mDescriptor = new RadioManager.BandDescriptor();
    }
    
    RadioManager.BandDescriptor getDescriptor() {
      return this.mDescriptor;
    }
    
    public int getRegion() {
      return this.mDescriptor.getRegion();
    }
    
    public int getType() {
      return this.mDescriptor.getType();
    }
    
    public int getLowerLimit() {
      return this.mDescriptor.getLowerLimit();
    }
    
    public int getUpperLimit() {
      return this.mDescriptor.getUpperLimit();
    }
    
    public int getSpacing() {
      return this.mDescriptor.getSpacing();
    }
    
    public static final Parcelable.Creator<BandConfig> CREATOR = new Parcelable.Creator<BandConfig>() {
        public RadioManager.BandConfig createFromParcel(Parcel param2Parcel) {
          StringBuilder stringBuilder;
          int i = RadioManager.BandDescriptor.lookupTypeFromParcel(param2Parcel);
          if (i != 0)
            if (i != 1 && i != 2) {
              if (i != 3) {
                stringBuilder = new StringBuilder();
                stringBuilder.append("Unsupported band: ");
                stringBuilder.append(i);
                throw new IllegalArgumentException(stringBuilder.toString());
              } 
            } else {
              return new RadioManager.FmBandConfig();
            }  
          return new RadioManager.AmBandConfig();
        }
        
        public RadioManager.BandConfig[] newArray(int param2Int) {
          return new RadioManager.BandConfig[param2Int];
        }
      };
    
    final RadioManager.BandDescriptor mDescriptor;
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      this.mDescriptor.writeToParcel(param1Parcel, param1Int);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("BandConfig [ ");
      stringBuilder.append(this.mDescriptor.toString());
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public int hashCode() {
      int i = this.mDescriptor.hashCode();
      return 1 * 31 + i;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool1, bool2;
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof BandConfig))
        return false; 
      param1Object = param1Object;
      RadioManager.BandDescriptor bandDescriptor = param1Object.getDescriptor();
      if (this.mDescriptor == null) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if (bandDescriptor == null) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (bool1 != bool2)
        return false; 
      param1Object = this.mDescriptor;
      if (param1Object != null && !param1Object.equals(bandDescriptor))
        return false; 
      return true;
    }
  }
  
  class FmBandConfig extends BandConfig {
    public FmBandConfig(RadioManager this$0) {
      this.mStereo = this$0.isStereoSupported();
      this.mRds = this$0.isRdsSupported();
      this.mTa = this$0.isTaSupported();
      this.mAf = this$0.isAfSupported();
      this.mEa = this$0.isEaSupported();
    }
    
    FmBandConfig(RadioManager this$0, int param1Int1, int param1Int2, int param1Int3, int param1Int4, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, boolean param1Boolean5) {
      super(param1Int1, param1Int2, param1Int3, param1Int4);
      this.mStereo = param1Boolean1;
      this.mRds = param1Boolean2;
      this.mTa = param1Boolean3;
      this.mAf = param1Boolean4;
      this.mEa = param1Boolean5;
    }
    
    public boolean getStereo() {
      return this.mStereo;
    }
    
    public boolean getRds() {
      return this.mRds;
    }
    
    public boolean getTa() {
      return this.mTa;
    }
    
    public boolean getAf() {
      return this.mAf;
    }
    
    public boolean getEa() {
      return this.mEa;
    }
    
    private FmBandConfig(RadioManager this$0) {
      byte b = this$0.readByte();
      boolean bool1 = false;
      if (b == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mStereo = bool2;
      if (this$0.readByte() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mRds = bool2;
      if (this$0.readByte() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mTa = bool2;
      if (this$0.readByte() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mAf = bool2;
      boolean bool2 = bool1;
      if (this$0.readByte() == 1)
        bool2 = true; 
      this.mEa = bool2;
    }
    
    public static final Parcelable.Creator<FmBandConfig> CREATOR = (Parcelable.Creator<FmBandConfig>)new Object();
    
    private final boolean mAf;
    
    private final boolean mEa;
    
    private final boolean mRds;
    
    private final boolean mStereo;
    
    private final boolean mTa;
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeByte((byte)this.mStereo);
      param1Parcel.writeByte((byte)this.mRds);
      param1Parcel.writeByte((byte)this.mTa);
      param1Parcel.writeByte((byte)this.mAf);
      param1Parcel.writeByte((byte)this.mEa);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("FmBandConfig [");
      stringBuilder.append(super.toString());
      stringBuilder.append(", mStereo=");
      stringBuilder.append(this.mStereo);
      stringBuilder.append(", mRds=");
      stringBuilder.append(this.mRds);
      stringBuilder.append(", mTa=");
      stringBuilder.append(this.mTa);
      stringBuilder.append(", mAf=");
      stringBuilder.append(this.mAf);
      stringBuilder.append(", mEa =");
      stringBuilder.append(this.mEa);
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public int hashCode() {
      int i = super.hashCode();
      boolean bool1 = this.mStereo;
      boolean bool2 = this.mRds;
      boolean bool3 = this.mTa;
      boolean bool4 = this.mAf;
      boolean bool5 = this.mEa;
      return ((((i * 31 + bool1) * 31 + bool2) * 31 + bool3) * 31 + bool4) * 31 + bool5;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (!super.equals(param1Object))
        return false; 
      if (!(param1Object instanceof FmBandConfig))
        return false; 
      param1Object = param1Object;
      if (this.mStereo != ((FmBandConfig)param1Object).mStereo)
        return false; 
      if (this.mRds != ((FmBandConfig)param1Object).mRds)
        return false; 
      if (this.mTa != ((FmBandConfig)param1Object).mTa)
        return false; 
      if (this.mAf != ((FmBandConfig)param1Object).mAf)
        return false; 
      if (this.mEa != ((FmBandConfig)param1Object).mEa)
        return false; 
      return true;
    }
    
    class Builder {
      private boolean mAf;
      
      private final RadioManager.BandDescriptor mDescriptor;
      
      private boolean mEa;
      
      private boolean mRds;
      
      private boolean mStereo;
      
      private boolean mTa;
      
      public Builder(RadioManager.FmBandConfig this$0) {
        int i = this$0.getRegion(), j = this$0.getType();
        int k = this$0.getLowerLimit(), m = this$0.getUpperLimit();
        this.mDescriptor = new RadioManager.BandDescriptor(j, k, m, this$0.getSpacing());
        this.mStereo = this$0.isStereoSupported();
        this.mRds = this$0.isRdsSupported();
        this.mTa = this$0.isTaSupported();
        this.mAf = this$0.isAfSupported();
        this.mEa = this$0.isEaSupported();
      }
      
      public Builder(RadioManager.FmBandConfig this$0) {
        int i = this$0.getRegion(), j = this$0.getType();
        this.mDescriptor = new RadioManager.BandDescriptor(j, this$0.getLowerLimit(), this$0.getUpperLimit(), this$0.getSpacing());
        this.mStereo = this$0.getStereo();
        this.mRds = this$0.getRds();
        this.mTa = this$0.getTa();
        this.mAf = this$0.getAf();
        this.mEa = this$0.getEa();
      }
      
      public RadioManager.FmBandConfig build() {
        int i = this.mDescriptor.getRegion();
        RadioManager.BandDescriptor bandDescriptor = this.mDescriptor;
        int j = bandDescriptor.getType(), k = this.mDescriptor.getLowerLimit();
        bandDescriptor = this.mDescriptor;
        return new RadioManager.FmBandConfig(j, k, bandDescriptor.getUpperLimit(), this.mDescriptor.getSpacing(), this.mStereo, this.mRds, this.mTa, this.mAf, this.mEa);
      }
      
      public Builder setStereo(boolean param2Boolean) {
        this.mStereo = param2Boolean;
        return this;
      }
      
      public Builder setRds(boolean param2Boolean) {
        this.mRds = param2Boolean;
        return this;
      }
      
      public Builder setTa(boolean param2Boolean) {
        this.mTa = param2Boolean;
        return this;
      }
      
      public Builder setAf(boolean param2Boolean) {
        this.mAf = param2Boolean;
        return this;
      }
      
      public Builder setEa(boolean param2Boolean) {
        this.mEa = param2Boolean;
        return this;
      }
    }
  }
  
  public static class Builder {
    private boolean mAf;
    
    private final RadioManager.BandDescriptor mDescriptor;
    
    private boolean mEa;
    
    private boolean mRds;
    
    private boolean mStereo;
    
    private boolean mTa;
    
    public Builder(RadioManager.FmBandDescriptor param1FmBandDescriptor) {
      int i = param1FmBandDescriptor.getRegion(), j = param1FmBandDescriptor.getType();
      int k = param1FmBandDescriptor.getLowerLimit(), m = param1FmBandDescriptor.getUpperLimit();
      this.mDescriptor = new RadioManager.BandDescriptor(j, k, m, param1FmBandDescriptor.getSpacing());
      this.mStereo = param1FmBandDescriptor.isStereoSupported();
      this.mRds = param1FmBandDescriptor.isRdsSupported();
      this.mTa = param1FmBandDescriptor.isTaSupported();
      this.mAf = param1FmBandDescriptor.isAfSupported();
      this.mEa = param1FmBandDescriptor.isEaSupported();
    }
    
    public Builder(RadioManager.FmBandConfig param1FmBandConfig) {
      int i = param1FmBandConfig.getRegion(), j = param1FmBandConfig.getType();
      this.mDescriptor = new RadioManager.BandDescriptor(j, param1FmBandConfig.getLowerLimit(), param1FmBandConfig.getUpperLimit(), param1FmBandConfig.getSpacing());
      this.mStereo = param1FmBandConfig.getStereo();
      this.mRds = param1FmBandConfig.getRds();
      this.mTa = param1FmBandConfig.getTa();
      this.mAf = param1FmBandConfig.getAf();
      this.mEa = param1FmBandConfig.getEa();
    }
    
    public RadioManager.FmBandConfig build() {
      int i = this.mDescriptor.getRegion();
      RadioManager.BandDescriptor bandDescriptor = this.mDescriptor;
      int j = bandDescriptor.getType(), k = this.mDescriptor.getLowerLimit();
      bandDescriptor = this.mDescriptor;
      return new RadioManager.FmBandConfig(j, k, bandDescriptor.getUpperLimit(), this.mDescriptor.getSpacing(), this.mStereo, this.mRds, this.mTa, this.mAf, this.mEa);
    }
    
    public Builder setStereo(boolean param1Boolean) {
      this.mStereo = param1Boolean;
      return this;
    }
    
    public Builder setRds(boolean param1Boolean) {
      this.mRds = param1Boolean;
      return this;
    }
    
    public Builder setTa(boolean param1Boolean) {
      this.mTa = param1Boolean;
      return this;
    }
    
    public Builder setAf(boolean param1Boolean) {
      this.mAf = param1Boolean;
      return this;
    }
    
    public Builder setEa(boolean param1Boolean) {
      this.mEa = param1Boolean;
      return this;
    }
  }
  
  class AmBandConfig extends BandConfig {
    public AmBandConfig(RadioManager this$0) {
      this.mStereo = this$0.isStereoSupported();
    }
    
    AmBandConfig(RadioManager this$0, int param1Int1, int param1Int2, int param1Int3, int param1Int4, boolean param1Boolean) {
      super(param1Int1, param1Int2, param1Int3, param1Int4);
      this.mStereo = param1Boolean;
    }
    
    public boolean getStereo() {
      return this.mStereo;
    }
    
    private AmBandConfig(RadioManager this$0) {
      byte b = this$0.readByte();
      boolean bool = true;
      if (b != 1)
        bool = false; 
      this.mStereo = bool;
    }
    
    public static final Parcelable.Creator<AmBandConfig> CREATOR = (Parcelable.Creator<AmBandConfig>)new Object();
    
    private final boolean mStereo;
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeByte((byte)this.mStereo);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AmBandConfig [");
      stringBuilder.append(super.toString());
      stringBuilder.append(", mStereo=");
      stringBuilder.append(this.mStereo);
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public int hashCode() {
      int i = super.hashCode();
      boolean bool = this.mStereo;
      return i * 31 + bool;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (!super.equals(param1Object))
        return false; 
      if (!(param1Object instanceof AmBandConfig))
        return false; 
      param1Object = param1Object;
      if (this.mStereo != param1Object.getStereo())
        return false; 
      return true;
    }
    
    class Builder {
      private final RadioManager.BandDescriptor mDescriptor;
      
      private boolean mStereo;
      
      public Builder(RadioManager.AmBandConfig this$0) {
        int i = this$0.getRegion(), j = this$0.getType();
        int k = this$0.getLowerLimit(), m = this$0.getUpperLimit();
        this.mDescriptor = new RadioManager.BandDescriptor(j, k, m, this$0.getSpacing());
        this.mStereo = this$0.isStereoSupported();
      }
      
      public Builder(RadioManager.AmBandConfig this$0) {
        int i = this$0.getRegion(), j = this$0.getType();
        this.mDescriptor = new RadioManager.BandDescriptor(j, this$0.getLowerLimit(), this$0.getUpperLimit(), this$0.getSpacing());
        this.mStereo = this$0.getStereo();
      }
      
      public RadioManager.AmBandConfig build() {
        int i = this.mDescriptor.getRegion();
        RadioManager.BandDescriptor bandDescriptor = this.mDescriptor;
        int j = bandDescriptor.getType(), k = this.mDescriptor.getLowerLimit();
        bandDescriptor = this.mDescriptor;
        return new RadioManager.AmBandConfig(j, k, bandDescriptor.getUpperLimit(), this.mDescriptor.getSpacing(), this.mStereo);
      }
      
      public Builder setStereo(boolean param2Boolean) {
        this.mStereo = param2Boolean;
        return this;
      }
    }
  }
  
  public static class Builder {
    private final RadioManager.BandDescriptor mDescriptor;
    
    private boolean mStereo;
    
    public Builder(RadioManager.AmBandDescriptor param1AmBandDescriptor) {
      int i = param1AmBandDescriptor.getRegion(), j = param1AmBandDescriptor.getType();
      int k = param1AmBandDescriptor.getLowerLimit(), m = param1AmBandDescriptor.getUpperLimit();
      this.mDescriptor = new RadioManager.BandDescriptor(j, k, m, param1AmBandDescriptor.getSpacing());
      this.mStereo = param1AmBandDescriptor.isStereoSupported();
    }
    
    public Builder(RadioManager.AmBandConfig param1AmBandConfig) {
      int i = param1AmBandConfig.getRegion(), j = param1AmBandConfig.getType();
      this.mDescriptor = new RadioManager.BandDescriptor(j, param1AmBandConfig.getLowerLimit(), param1AmBandConfig.getUpperLimit(), param1AmBandConfig.getSpacing());
      this.mStereo = param1AmBandConfig.getStereo();
    }
    
    public RadioManager.AmBandConfig build() {
      int i = this.mDescriptor.getRegion();
      RadioManager.BandDescriptor bandDescriptor = this.mDescriptor;
      int j = bandDescriptor.getType(), k = this.mDescriptor.getLowerLimit();
      bandDescriptor = this.mDescriptor;
      return new RadioManager.AmBandConfig(j, k, bandDescriptor.getUpperLimit(), this.mDescriptor.getSpacing(), this.mStereo);
    }
    
    public Builder setStereo(boolean param1Boolean) {
      this.mStereo = param1Boolean;
      return this;
    }
  }
  
  class ProgramInfo implements Parcelable {
    public ProgramInfo(RadioManager this$0, ProgramSelector.Identifier param1Identifier1, ProgramSelector.Identifier param1Identifier2, Collection<ProgramSelector.Identifier> param1Collection, int param1Int1, int param1Int2, RadioMetadata param1RadioMetadata, Map<String, String> param1Map) {
      Map<String, String> map;
      Objects.requireNonNull(this$0);
      this.mSelector = (ProgramSelector)this$0;
      this.mLogicallyTunedTo = param1Identifier1;
      this.mPhysicallyTunedTo = param1Identifier2;
      if (param1Collection == null) {
        this.mRelatedContent = Collections.emptyList();
      } else {
        Preconditions.checkCollectionElementsNotNull(param1Collection, "relatedContent");
        this.mRelatedContent = param1Collection;
      } 
      this.mInfoFlags = param1Int1;
      this.mSignalQuality = param1Int2;
      this.mMetadata = param1RadioMetadata;
      if (param1Map == null) {
        HashMap<Object, Object> hashMap = new HashMap<>();
      } else {
        map = param1Map;
      } 
      this.mVendorInfo = map;
    }
    
    public ProgramSelector getSelector() {
      return this.mSelector;
    }
    
    public ProgramSelector.Identifier getLogicallyTunedTo() {
      return this.mLogicallyTunedTo;
    }
    
    public ProgramSelector.Identifier getPhysicallyTunedTo() {
      return this.mPhysicallyTunedTo;
    }
    
    public Collection<ProgramSelector.Identifier> getRelatedContent() {
      return this.mRelatedContent;
    }
    
    @Deprecated
    public int getChannel() {
      try {
        long l = this.mSelector.getFirstId(1);
        return (int)l;
      } catch (IllegalArgumentException illegalArgumentException) {
        Log.w("BroadcastRadio.manager", "Not an AM/FM program");
        return 0;
      } 
    }
    
    @Deprecated
    public int getSubChannel() {
      try {
        long l = this.mSelector.getFirstId(4);
        return (int)l + 1;
      } catch (IllegalArgumentException illegalArgumentException) {
        return 0;
      } 
    }
    
    public boolean isTuned() {
      boolean bool;
      if ((this.mInfoFlags & 0x10) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isStereo() {
      boolean bool;
      if ((this.mInfoFlags & 0x20) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    @Deprecated
    public boolean isDigital() {
      ProgramSelector.Identifier identifier1 = this.mLogicallyTunedTo;
      ProgramSelector.Identifier identifier2 = identifier1;
      if (identifier1 == null)
        identifier2 = this.mSelector.getPrimaryId(); 
      int i = identifier2.getType();
      boolean bool = true;
      if (i == 1 || i == 2)
        bool = false; 
      return bool;
    }
    
    public boolean isLive() {
      int i = this.mInfoFlags;
      boolean bool = true;
      if ((i & 0x1) == 0)
        bool = false; 
      return bool;
    }
    
    public boolean isMuted() {
      boolean bool;
      if ((this.mInfoFlags & 0x2) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isTrafficProgram() {
      boolean bool;
      if ((this.mInfoFlags & 0x4) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isTrafficAnnouncementActive() {
      boolean bool;
      if ((this.mInfoFlags & 0x8) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getSignalStrength() {
      return this.mSignalQuality;
    }
    
    public RadioMetadata getMetadata() {
      return this.mMetadata;
    }
    
    public Map<String, String> getVendorInfo() {
      return this.mVendorInfo;
    }
    
    private ProgramInfo(RadioManager this$0) {
      ProgramSelector programSelector = (ProgramSelector)this$0.readTypedObject(ProgramSelector.CREATOR);
      Objects.requireNonNull(programSelector);
      this.mSelector = programSelector;
      this.mLogicallyTunedTo = (ProgramSelector.Identifier)this$0.readTypedObject(ProgramSelector.Identifier.CREATOR);
      this.mPhysicallyTunedTo = (ProgramSelector.Identifier)this$0.readTypedObject(ProgramSelector.Identifier.CREATOR);
      this.mRelatedContent = this$0.createTypedArrayList(ProgramSelector.Identifier.CREATOR);
      this.mInfoFlags = this$0.readInt();
      this.mSignalQuality = this$0.readInt();
      this.mMetadata = (RadioMetadata)this$0.readTypedObject(RadioMetadata.CREATOR);
      this.mVendorInfo = Utils.readStringMap((Parcel)this$0);
    }
    
    public static final Parcelable.Creator<ProgramInfo> CREATOR = new Parcelable.Creator<ProgramInfo>() {
        public RadioManager.ProgramInfo createFromParcel(Parcel param2Parcel) {
          return new RadioManager.ProgramInfo();
        }
        
        public RadioManager.ProgramInfo[] newArray(int param2Int) {
          return new RadioManager.ProgramInfo[param2Int];
        }
      };
    
    private static final int FLAG_LIVE = 1;
    
    private static final int FLAG_MUTED = 2;
    
    private static final int FLAG_STEREO = 32;
    
    private static final int FLAG_TRAFFIC_ANNOUNCEMENT = 8;
    
    private static final int FLAG_TRAFFIC_PROGRAM = 4;
    
    private static final int FLAG_TUNED = 16;
    
    private final int mInfoFlags;
    
    private final ProgramSelector.Identifier mLogicallyTunedTo;
    
    private final RadioMetadata mMetadata;
    
    private final ProgramSelector.Identifier mPhysicallyTunedTo;
    
    private final Collection<ProgramSelector.Identifier> mRelatedContent;
    
    private final ProgramSelector mSelector;
    
    private final int mSignalQuality;
    
    private final Map<String, String> mVendorInfo;
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeTypedObject(this.mSelector, param1Int);
      param1Parcel.writeTypedObject(this.mLogicallyTunedTo, param1Int);
      param1Parcel.writeTypedObject(this.mPhysicallyTunedTo, param1Int);
      Utils.writeTypedCollection(param1Parcel, this.mRelatedContent);
      param1Parcel.writeInt(this.mInfoFlags);
      param1Parcel.writeInt(this.mSignalQuality);
      param1Parcel.writeTypedObject(this.mMetadata, param1Int);
      Utils.writeStringMap(param1Parcel, this.mVendorInfo);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ProgramInfo [selector=");
      stringBuilder.append(this.mSelector);
      stringBuilder.append(", logicallyTunedTo=");
      ProgramSelector.Identifier identifier = this.mLogicallyTunedTo;
      stringBuilder.append(Objects.toString(identifier));
      stringBuilder.append(", physicallyTunedTo=");
      identifier = this.mPhysicallyTunedTo;
      stringBuilder.append(Objects.toString(identifier));
      stringBuilder.append(", relatedContent=");
      Collection<ProgramSelector.Identifier> collection = this.mRelatedContent;
      stringBuilder.append(collection.size());
      stringBuilder.append(", infoFlags=");
      stringBuilder.append(this.mInfoFlags);
      stringBuilder.append(", mSignalQuality=");
      stringBuilder.append(this.mSignalQuality);
      stringBuilder.append(", mMetadata=");
      RadioMetadata radioMetadata = this.mMetadata;
      stringBuilder.append(Objects.toString(radioMetadata));
      stringBuilder.append("]");
      return stringBuilder.toString();
    }
    
    public int hashCode() {
      ProgramSelector programSelector = this.mSelector;
      ProgramSelector.Identifier identifier1 = this.mLogicallyTunedTo, identifier2 = this.mPhysicallyTunedTo;
      Collection<ProgramSelector.Identifier> collection = this.mRelatedContent;
      int i = this.mInfoFlags;
      int j = this.mSignalQuality;
      RadioMetadata radioMetadata = this.mMetadata;
      Map<String, String> map = this.mVendorInfo;
      return Objects.hash(new Object[] { programSelector, identifier1, identifier2, collection, Integer.valueOf(i), Integer.valueOf(j), radioMetadata, map });
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof ProgramInfo))
        return false; 
      param1Object = param1Object;
      if (!Objects.equals(this.mSelector, ((ProgramInfo)param1Object).mSelector))
        return false; 
      if (!Objects.equals(this.mLogicallyTunedTo, ((ProgramInfo)param1Object).mLogicallyTunedTo))
        return false; 
      if (!Objects.equals(this.mPhysicallyTunedTo, ((ProgramInfo)param1Object).mPhysicallyTunedTo))
        return false; 
      if (!Objects.equals(this.mRelatedContent, ((ProgramInfo)param1Object).mRelatedContent))
        return false; 
      if (this.mInfoFlags != ((ProgramInfo)param1Object).mInfoFlags)
        return false; 
      if (this.mSignalQuality != ((ProgramInfo)param1Object).mSignalQuality)
        return false; 
      if (!Objects.equals(this.mMetadata, ((ProgramInfo)param1Object).mMetadata))
        return false; 
      if (!Objects.equals(this.mVendorInfo, ((ProgramInfo)param1Object).mVendorInfo))
        return false; 
      return true;
    }
  }
  
  public int listModules(List<ModuleProperties> paramList) {
    if (paramList == null) {
      Log.e("BroadcastRadio.manager", "the output list must not be empty");
      return -22;
    } 
    Log.d("BroadcastRadio.manager", "Listing available tuners...");
    try {
      List<ModuleProperties> list = this.mService.listModules();
      if (list == null) {
        Log.e("BroadcastRadio.manager", "Returned list was a null");
        return Integer.MIN_VALUE;
      } 
      paramList.addAll(list);
      return 0;
    } catch (RemoteException remoteException) {
      Log.e("BroadcastRadio.manager", "Failed listing available tuners", (Throwable)remoteException);
      return -32;
    } 
  }
  
  public RadioTuner openTuner(int paramInt, BandConfig paramBandConfig, boolean paramBoolean, RadioTuner.Callback paramCallback, Handler paramHandler) {
    if (paramCallback != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Opening tuner ");
      stringBuilder.append(paramInt);
      stringBuilder.append("...");
      Log.d("BroadcastRadio.manager", stringBuilder.toString());
      TunerCallbackAdapter tunerCallbackAdapter = new TunerCallbackAdapter(paramCallback, paramHandler);
      try {
        ITuner iTuner = this.mService.openTuner(paramInt, paramBandConfig, paramBoolean, tunerCallbackAdapter);
        if (iTuner == null) {
          Log.e("BroadcastRadio.manager", "Failed to open tuner");
          return null;
        } 
        if (paramBandConfig != null) {
          paramInt = paramBandConfig.getType();
        } else {
          paramInt = -1;
        } 
        return new TunerAdapter(iTuner, tunerCallbackAdapter, paramInt);
      } catch (RemoteException|IllegalArgumentException|IllegalStateException remoteException) {
        Log.e("BroadcastRadio.manager", "Failed to open tuner", (Throwable)remoteException);
        return null;
      } 
    } 
    throw new IllegalArgumentException("callback must not be empty");
  }
  
  private final Map<Announcement.OnListUpdatedListener, ICloseHandle> mAnnouncementListeners = new HashMap<>();
  
  private final Context mContext;
  
  private final IRadioService mService;
  
  public void addAnnouncementListener(Set<Integer> paramSet, Announcement.OnListUpdatedListener paramOnListUpdatedListener) {
    addAnnouncementListener((Executor)_$$Lambda$RadioManager$cfMLnpQqL72UMrjmCGbrhAOHHgg.INSTANCE, paramSet, paramOnListUpdatedListener);
  }
  
  public void addAnnouncementListener(Executor paramExecutor, Set<Integer> paramSet, Announcement.OnListUpdatedListener paramOnListUpdatedListener) {
    Objects.requireNonNull(paramExecutor);
    Objects.requireNonNull(paramOnListUpdatedListener);
    int[] arrayOfInt = paramSet.stream().mapToInt((ToIntFunction)_$$Lambda$UV1wDVoVlbcxpr8zevj_aMFtUGw.INSTANCE).toArray();
    Object object = new Object(this, paramExecutor, paramOnListUpdatedListener);
    Map<Announcement.OnListUpdatedListener, ICloseHandle> map = this.mAnnouncementListeners;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/util/Map<[InnerObjectType{ObjectType{android/hardware/radio/Announcement}.Landroid/hardware/radio/Announcement$OnListUpdatedListener;}, ObjectType{android/hardware/radio/ICloseHandle}]>}, name=null} */
    paramExecutor = null;
    try {
      ICloseHandle iCloseHandle2 = this.mService.addAnnouncementListener(arrayOfInt, (IAnnouncementListener)object), iCloseHandle1 = iCloseHandle2;
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } finally {}
    Objects.requireNonNull(paramExecutor);
    ICloseHandle iCloseHandle = (ICloseHandle)this.mAnnouncementListeners.put(paramOnListUpdatedListener, paramExecutor);
    if (iCloseHandle != null)
      Utils.close(iCloseHandle); 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/util/Map<[InnerObjectType{ObjectType{android/hardware/radio/Announcement}.Landroid/hardware/radio/Announcement$OnListUpdatedListener;}, ObjectType{android/hardware/radio/ICloseHandle}]>}, name=null} */
  }
  
  public void removeAnnouncementListener(Announcement.OnListUpdatedListener paramOnListUpdatedListener) {
    Objects.requireNonNull(paramOnListUpdatedListener);
    synchronized (this.mAnnouncementListeners) {
      ICloseHandle iCloseHandle = this.mAnnouncementListeners.remove(paramOnListUpdatedListener);
      if (iCloseHandle != null)
        Utils.close(iCloseHandle); 
      return;
    } 
  }
  
  public RadioManager(Context paramContext) throws ServiceManager.ServiceNotFoundException {
    this.mContext = paramContext;
    IBinder iBinder = ServiceManager.getServiceOrThrow("broadcastradio");
    this.mService = IRadioService.Stub.asInterface(iBinder);
  }
  
  private native int nativeListModules(List<ModuleProperties> paramList);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Band {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ConfigFlag {}
}
