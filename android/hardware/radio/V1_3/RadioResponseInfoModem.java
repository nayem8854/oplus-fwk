package android.hardware.radio.V1_3;

import android.hardware.radio.V1_0.RadioError;
import android.hardware.radio.V1_0.RadioResponseType;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class RadioResponseInfoModem {
  public int type = 0;
  
  public int serial = 0;
  
  public int error = 0;
  
  public boolean isEnabled = false;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != RadioResponseInfoModem.class)
      return false; 
    paramObject = paramObject;
    if (this.type != ((RadioResponseInfoModem)paramObject).type)
      return false; 
    if (this.serial != ((RadioResponseInfoModem)paramObject).serial)
      return false; 
    if (this.error != ((RadioResponseInfoModem)paramObject).error)
      return false; 
    if (this.isEnabled != ((RadioResponseInfoModem)paramObject).isEnabled)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.type;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.serial;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.error;
    int m = HidlSupport.deepHashCode(Integer.valueOf(k));
    boolean bool = this.isEnabled;
    k = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(m), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".type = ");
    stringBuilder.append(RadioResponseType.toString(this.type));
    stringBuilder.append(", .serial = ");
    stringBuilder.append(this.serial);
    stringBuilder.append(", .error = ");
    stringBuilder.append(RadioError.toString(this.error));
    stringBuilder.append(", .isEnabled = ");
    stringBuilder.append(this.isEnabled);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<RadioResponseInfoModem> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<RadioResponseInfoModem> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 16);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      RadioResponseInfoModem radioResponseInfoModem = new RadioResponseInfoModem();
      radioResponseInfoModem.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 16));
      arrayList.add(radioResponseInfoModem);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.type = paramHwBlob.getInt32(0L + paramLong);
    this.serial = paramHwBlob.getInt32(4L + paramLong);
    this.error = paramHwBlob.getInt32(8L + paramLong);
    this.isEnabled = paramHwBlob.getBool(12L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(16);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<RadioResponseInfoModem> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 16);
    for (byte b = 0; b < i; b++)
      ((RadioResponseInfoModem)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 16)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.type);
    paramHwBlob.putInt32(4L + paramLong, this.serial);
    paramHwBlob.putInt32(8L + paramLong, this.error);
    paramHwBlob.putBool(12L + paramLong, this.isEnabled);
  }
}
