package android.hardware.radio.V1_2;

import android.hardware.radio.V1_0.CallForwardInfo;
import android.hardware.radio.V1_0.CarrierRestrictions;
import android.hardware.radio.V1_0.CdmaBroadcastSmsConfigInfo;
import android.hardware.radio.V1_0.CdmaSmsAck;
import android.hardware.radio.V1_0.CdmaSmsMessage;
import android.hardware.radio.V1_0.CdmaSmsWriteArgs;
import android.hardware.radio.V1_0.DataProfileInfo;
import android.hardware.radio.V1_0.Dial;
import android.hardware.radio.V1_0.GsmBroadcastSmsConfigInfo;
import android.hardware.radio.V1_0.GsmSmsMessage;
import android.hardware.radio.V1_0.IRadioIndication;
import android.hardware.radio.V1_0.IRadioResponse;
import android.hardware.radio.V1_0.IccIo;
import android.hardware.radio.V1_0.ImsSmsMessage;
import android.hardware.radio.V1_0.NvWriteItem;
import android.hardware.radio.V1_0.RadioCapability;
import android.hardware.radio.V1_0.SelectUiccSub;
import android.hardware.radio.V1_0.SimApdu;
import android.hardware.radio.V1_0.SmsWriteArgs;
import android.hardware.radio.V1_1.IRadio;
import android.hardware.radio.V1_1.ImsiEncryptionInfo;
import android.hardware.radio.V1_1.KeepaliveRequest;
import android.hardware.radio.V1_1.NetworkScanRequest;
import android.internal.hidl.base.V1_0.DebugInfo;
import android.os.HidlSupport;
import android.os.HwBinder;
import android.os.HwBlob;
import android.os.HwParcel;
import android.os.IHwBinder;
import android.os.IHwInterface;
import android.os.NativeHandle;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public interface IRadio extends IRadio {
  public static final String kInterfaceName = "android.hardware.radio@1.2::IRadio";
  
  static IRadio asInterface(IHwBinder paramIHwBinder) {
    if (paramIHwBinder == null)
      return null; 
    IHwInterface iHwInterface = paramIHwBinder.queryLocalInterface("android.hardware.radio@1.2::IRadio");
    if (iHwInterface != null && iHwInterface instanceof IRadio)
      return (IRadio)iHwInterface; 
    iHwInterface = new Proxy(paramIHwBinder);
    try {
      for (String str : iHwInterface.interfaceChain()) {
        boolean bool = str.equals("android.hardware.radio@1.2::IRadio");
        if (bool)
          return (IRadio)iHwInterface; 
      } 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  static IRadio castFrom(IHwInterface paramIHwInterface) {
    if (paramIHwInterface == null) {
      paramIHwInterface = null;
    } else {
      paramIHwInterface = asInterface(paramIHwInterface.asBinder());
    } 
    return (IRadio)paramIHwInterface;
  }
  
  static IRadio getService(String paramString, boolean paramBoolean) throws RemoteException {
    return asInterface(HwBinder.getService("android.hardware.radio@1.2::IRadio", paramString, paramBoolean));
  }
  
  static IRadio getService(boolean paramBoolean) throws RemoteException {
    return getService("default", paramBoolean);
  }
  
  static IRadio getService(String paramString) throws RemoteException {
    return asInterface(HwBinder.getService("android.hardware.radio@1.2::IRadio", paramString));
  }
  
  static IRadio getService() throws RemoteException {
    return getService("default");
  }
  
  IHwBinder asBinder();
  
  void deactivateDataCall_1_2(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void debug(NativeHandle paramNativeHandle, ArrayList<String> paramArrayList) throws RemoteException;
  
  DebugInfo getDebugInfo() throws RemoteException;
  
  ArrayList<byte[]> getHashChain() throws RemoteException;
  
  ArrayList<String> interfaceChain() throws RemoteException;
  
  String interfaceDescriptor() throws RemoteException;
  
  boolean linkToDeath(IHwBinder.DeathRecipient paramDeathRecipient, long paramLong) throws RemoteException;
  
  void notifySyspropsChanged() throws RemoteException;
  
  void ping() throws RemoteException;
  
  void setHALInstrumentation() throws RemoteException;
  
  void setIndicationFilter_1_2(int paramInt1, int paramInt2) throws RemoteException;
  
  void setLinkCapacityReportingCriteria(int paramInt1, int paramInt2, int paramInt3, int paramInt4, ArrayList<Integer> paramArrayList1, ArrayList<Integer> paramArrayList2, int paramInt5) throws RemoteException;
  
  void setSignalStrengthReportingCriteria(int paramInt1, int paramInt2, int paramInt3, ArrayList<Integer> paramArrayList, int paramInt4) throws RemoteException;
  
  void setupDataCall_1_2(int paramInt1, int paramInt2, DataProfileInfo paramDataProfileInfo, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, int paramInt3, ArrayList<String> paramArrayList1, ArrayList<String> paramArrayList2) throws RemoteException;
  
  void startNetworkScan_1_2(int paramInt, NetworkScanRequest paramNetworkScanRequest) throws RemoteException;
  
  boolean unlinkToDeath(IHwBinder.DeathRecipient paramDeathRecipient) throws RemoteException;
  
  public static final class Proxy implements IRadio {
    private IHwBinder mRemote;
    
    public Proxy(IHwBinder param1IHwBinder) {
      Objects.requireNonNull(param1IHwBinder);
      this.mRemote = param1IHwBinder;
    }
    
    public IHwBinder asBinder() {
      return this.mRemote;
    }
    
    public String toString() {
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append(interfaceDescriptor());
        stringBuilder.append("@Proxy");
        return stringBuilder.toString();
      } catch (RemoteException remoteException) {
        return "[class or subclass of android.hardware.radio@1.2::IRadio]@Proxy";
      } 
    }
    
    public final boolean equals(Object param1Object) {
      return HidlSupport.interfacesEqual(this, param1Object);
    }
    
    public final int hashCode() {
      return asBinder().hashCode();
    }
    
    public void setResponseFunctions(IRadioResponse param1IRadioResponse, IRadioIndication param1IRadioIndication) throws RemoteException {
      IHwBinder iHwBinder1;
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      IHwBinder iHwBinder2 = null;
      if (param1IRadioResponse == null) {
        param1IRadioResponse = null;
      } else {
        iHwBinder1 = param1IRadioResponse.asBinder();
      } 
      hwParcel2.writeStrongBinder(iHwBinder1);
      if (param1IRadioIndication == null) {
        iHwBinder1 = iHwBinder2;
      } else {
        iHwBinder1 = param1IRadioIndication.asBinder();
      } 
      hwParcel2.writeStrongBinder(iHwBinder1);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(1, hwParcel2, hwParcel1, 0);
        hwParcel1.verifySuccess();
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void getIccCardStatus(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(2, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void supplyIccPinForApp(int param1Int, String param1String1, String param1String2) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      hwParcel2.writeInt32(param1Int);
      hwParcel2.writeString(param1String1);
      hwParcel2.writeString(param1String2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(3, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void supplyIccPukForApp(int param1Int, String param1String1, String param1String2, String param1String3) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      hwParcel2.writeInt32(param1Int);
      hwParcel2.writeString(param1String1);
      hwParcel2.writeString(param1String2);
      hwParcel2.writeString(param1String3);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(4, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void supplyIccPin2ForApp(int param1Int, String param1String1, String param1String2) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      hwParcel2.writeInt32(param1Int);
      hwParcel2.writeString(param1String1);
      hwParcel2.writeString(param1String2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(5, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void supplyIccPuk2ForApp(int param1Int, String param1String1, String param1String2, String param1String3) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      hwParcel2.writeInt32(param1Int);
      hwParcel2.writeString(param1String1);
      hwParcel2.writeString(param1String2);
      hwParcel2.writeString(param1String3);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(6, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void changeIccPinForApp(int param1Int, String param1String1, String param1String2, String param1String3) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      hwParcel2.writeInt32(param1Int);
      hwParcel2.writeString(param1String1);
      hwParcel2.writeString(param1String2);
      hwParcel2.writeString(param1String3);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(7, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void changeIccPin2ForApp(int param1Int, String param1String1, String param1String2, String param1String3) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      hwParcel2.writeInt32(param1Int);
      hwParcel2.writeString(param1String1);
      hwParcel2.writeString(param1String2);
      hwParcel2.writeString(param1String3);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(8, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void supplyNetworkDepersonalization(int param1Int, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(9, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCurrentCalls(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(10, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void dial(int param1Int, Dial param1Dial) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1Dial.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(11, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getImsiForApp(int param1Int, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(12, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void hangup(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(13, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void hangupWaitingOrBackground(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(14, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void hangupForegroundResumeBackground(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(15, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void switchWaitingOrHoldingAndActive(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(16, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void conference(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(17, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void rejectCall(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(18, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getLastCallFailCause(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(19, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getSignalStrength(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(20, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getVoiceRegistrationState(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(21, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getDataRegistrationState(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(22, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getOperator(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(23, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setRadioPower(int param1Int, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(24, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendDtmf(int param1Int, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(25, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendSms(int param1Int, GsmSmsMessage param1GsmSmsMessage) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1GsmSmsMessage.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(26, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendSMSExpectMore(int param1Int, GsmSmsMessage param1GsmSmsMessage) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1GsmSmsMessage.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(27, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setupDataCall(int param1Int1, int param1Int2, DataProfileInfo param1DataProfileInfo, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      param1DataProfileInfo.writeToParcel(null);
      null.writeBool(param1Boolean1);
      null.writeBool(param1Boolean2);
      null.writeBool(param1Boolean3);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(28, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void iccIOForApp(int param1Int, IccIo param1IccIo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1IccIo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(29, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendUssd(int param1Int, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(30, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void cancelPendingUssd(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(31, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getClir(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(32, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setClir(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(33, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCallForwardStatus(int param1Int, CallForwardInfo param1CallForwardInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1CallForwardInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(34, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setCallForward(int param1Int, CallForwardInfo param1CallForwardInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1CallForwardInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(35, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCallWaiting(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(36, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setCallWaiting(int param1Int1, boolean param1Boolean, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeBool(param1Boolean);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(37, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void acknowledgeLastIncomingGsmSms(int param1Int1, boolean param1Boolean, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeBool(param1Boolean);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(38, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void acceptCall(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(39, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void deactivateDataCall(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(40, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getFacilityLockForApp(int param1Int1, String param1String1, String param1String2, int param1Int2, String param1String3) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      hwParcel2.writeInt32(param1Int1);
      hwParcel2.writeString(param1String1);
      hwParcel2.writeString(param1String2);
      hwParcel2.writeInt32(param1Int2);
      hwParcel2.writeString(param1String3);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(41, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setFacilityLockForApp(int param1Int1, String param1String1, boolean param1Boolean, String param1String2, int param1Int2, String param1String3) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      hwParcel2.writeInt32(param1Int1);
      hwParcel2.writeString(param1String1);
      hwParcel2.writeBool(param1Boolean);
      hwParcel2.writeString(param1String2);
      hwParcel2.writeInt32(param1Int2);
      hwParcel2.writeString(param1String3);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(42, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setBarringPassword(int param1Int, String param1String1, String param1String2, String param1String3) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      hwParcel2.writeInt32(param1Int);
      hwParcel2.writeString(param1String1);
      hwParcel2.writeString(param1String2);
      hwParcel2.writeString(param1String3);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(43, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void getNetworkSelectionMode(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(44, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setNetworkSelectionModeAutomatic(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(45, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setNetworkSelectionModeManual(int param1Int, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(46, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getAvailableNetworks(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(47, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void startDtmf(int param1Int, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(48, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void stopDtmf(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(49, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getBasebandVersion(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(50, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void separateConnection(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(51, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setMute(int param1Int, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(52, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getMute(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(53, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getClip(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(54, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getDataCallList(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(55, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setSuppServiceNotifications(int param1Int, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(56, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void writeSmsToSim(int param1Int, SmsWriteArgs param1SmsWriteArgs) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1SmsWriteArgs.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(57, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void deleteSmsOnSim(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(58, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setBandMode(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(59, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getAvailableBandModes(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(60, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendEnvelope(int param1Int, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(61, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendTerminalResponseToSim(int param1Int, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(62, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void handleStkCallSetupRequestFromSim(int param1Int, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(63, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void explicitCallTransfer(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(64, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setPreferredNetworkType(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(65, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getPreferredNetworkType(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(66, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getNeighboringCids(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(67, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setLocationUpdates(int param1Int, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(68, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setCdmaSubscriptionSource(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(69, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setCdmaRoamingPreference(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(70, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCdmaRoamingPreference(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(71, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setTTYMode(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(72, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getTTYMode(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(73, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setPreferredVoicePrivacy(int param1Int, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(74, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getPreferredVoicePrivacy(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(75, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendCDMAFeatureCode(int param1Int, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(76, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendBurstDtmf(int param1Int1, String param1String, int param1Int2, int param1Int3) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeString(param1String);
      null.writeInt32(param1Int2);
      null.writeInt32(param1Int3);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(77, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendCdmaSms(int param1Int, CdmaSmsMessage param1CdmaSmsMessage) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1CdmaSmsMessage.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(78, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void acknowledgeLastIncomingCdmaSms(int param1Int, CdmaSmsAck param1CdmaSmsAck) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1CdmaSmsAck.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(79, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getGsmBroadcastConfig(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(80, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setGsmBroadcastConfig(int param1Int, ArrayList<GsmBroadcastSmsConfigInfo> param1ArrayList) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      GsmBroadcastSmsConfigInfo.writeVectorToParcel(null, param1ArrayList);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(81, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setGsmBroadcastActivation(int param1Int, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(82, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCdmaBroadcastConfig(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(83, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setCdmaBroadcastConfig(int param1Int, ArrayList<CdmaBroadcastSmsConfigInfo> param1ArrayList) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      CdmaBroadcastSmsConfigInfo.writeVectorToParcel(null, param1ArrayList);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(84, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setCdmaBroadcastActivation(int param1Int, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(85, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCDMASubscription(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(86, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void writeSmsToRuim(int param1Int, CdmaSmsWriteArgs param1CdmaSmsWriteArgs) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1CdmaSmsWriteArgs.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(87, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void deleteSmsOnRuim(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(88, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getDeviceIdentity(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(89, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void exitEmergencyCallbackMode(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(90, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getSmscAddress(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(91, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setSmscAddress(int param1Int, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(92, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void reportSmsMemoryStatus(int param1Int, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(93, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void reportStkServiceIsRunning(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(94, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCdmaSubscriptionSource(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(95, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void requestIsimAuthentication(int param1Int, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(96, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void acknowledgeIncomingGsmSmsWithPdu(int param1Int, boolean param1Boolean, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeBool(param1Boolean);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(97, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendEnvelopeWithStatus(int param1Int, String param1String) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeString(param1String);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(98, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getVoiceRadioTechnology(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(99, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getCellInfoList(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(100, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setCellInfoListRate(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(101, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setInitialAttachApn(int param1Int, DataProfileInfo param1DataProfileInfo, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1DataProfileInfo.writeToParcel(null);
      null.writeBool(param1Boolean1);
      null.writeBool(param1Boolean2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(102, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getImsRegistrationState(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(103, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendImsSms(int param1Int, ImsSmsMessage param1ImsSmsMessage) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1ImsSmsMessage.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(104, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void iccTransmitApduBasicChannel(int param1Int, SimApdu param1SimApdu) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1SimApdu.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(105, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void iccOpenLogicalChannel(int param1Int1, String param1String, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeString(param1String);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(106, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void iccCloseLogicalChannel(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(107, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void iccTransmitApduLogicalChannel(int param1Int, SimApdu param1SimApdu) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1SimApdu.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(108, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void nvReadItem(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(109, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void nvWriteItem(int param1Int, NvWriteItem param1NvWriteItem) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1NvWriteItem.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(110, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void nvWriteCdmaPrl(int param1Int, ArrayList<Byte> param1ArrayList) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeInt8Vector(param1ArrayList);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(111, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void nvResetConfig(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(112, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setUiccSubscription(int param1Int, SelectUiccSub param1SelectUiccSub) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1SelectUiccSub.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(113, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setDataAllowed(int param1Int, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(114, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getHardwareConfig(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(115, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void requestIccSimAuthentication(int param1Int1, int param1Int2, String param1String1, String param1String2) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      hwParcel2.writeInt32(param1Int1);
      hwParcel2.writeInt32(param1Int2);
      hwParcel2.writeString(param1String1);
      hwParcel2.writeString(param1String2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(116, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setDataProfile(int param1Int, ArrayList<DataProfileInfo> param1ArrayList, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      DataProfileInfo.writeVectorToParcel(null, param1ArrayList);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(117, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void requestShutdown(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(118, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getRadioCapability(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(119, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setRadioCapability(int param1Int, RadioCapability param1RadioCapability) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      param1RadioCapability.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(120, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void startLceService(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(121, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void stopLceService(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(122, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void pullLceData(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(123, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getModemActivityInfo(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(124, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setAllowedCarriers(int param1Int, boolean param1Boolean, CarrierRestrictions param1CarrierRestrictions) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeBool(param1Boolean);
      param1CarrierRestrictions.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(125, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void getAllowedCarriers(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(126, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void sendDeviceState(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(127, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setIndicationFilter(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(128, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setSimCardPower(int param1Int, boolean param1Boolean) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      null.writeInt32(param1Int);
      null.writeBool(param1Boolean);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(129, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void responseAcknowledgement() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.0::IRadio");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(130, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setCarrierInfoForImsiEncryption(int param1Int, ImsiEncryptionInfo param1ImsiEncryptionInfo) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.1::IRadio");
      null.writeInt32(param1Int);
      param1ImsiEncryptionInfo.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(131, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setSimCardPower_1_1(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.1::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(132, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void startNetworkScan(int param1Int, NetworkScanRequest param1NetworkScanRequest) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.1::IRadio");
      null.writeInt32(param1Int);
      param1NetworkScanRequest.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(133, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void stopNetworkScan(int param1Int) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.1::IRadio");
      null.writeInt32(param1Int);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(134, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void startKeepalive(int param1Int, KeepaliveRequest param1KeepaliveRequest) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.1::IRadio");
      null.writeInt32(param1Int);
      param1KeepaliveRequest.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(135, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void stopKeepalive(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.1::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(136, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void startNetworkScan_1_2(int param1Int, NetworkScanRequest param1NetworkScanRequest) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.2::IRadio");
      null.writeInt32(param1Int);
      param1NetworkScanRequest.writeToParcel(null);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(137, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setIndicationFilter_1_2(int param1Int1, int param1Int2) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.2::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(138, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setSignalStrengthReportingCriteria(int param1Int1, int param1Int2, int param1Int3, ArrayList<Integer> param1ArrayList, int param1Int4) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.2::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      null.writeInt32(param1Int3);
      null.writeInt32Vector(param1ArrayList);
      null.writeInt32(param1Int4);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(139, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setLinkCapacityReportingCriteria(int param1Int1, int param1Int2, int param1Int3, int param1Int4, ArrayList<Integer> param1ArrayList1, ArrayList<Integer> param1ArrayList2, int param1Int5) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.2::IRadio");
      hwParcel2.writeInt32(param1Int1);
      hwParcel2.writeInt32(param1Int2);
      hwParcel2.writeInt32(param1Int3);
      hwParcel2.writeInt32(param1Int4);
      hwParcel2.writeInt32Vector(param1ArrayList1);
      hwParcel2.writeInt32Vector(param1ArrayList2);
      hwParcel2.writeInt32(param1Int5);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(140, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void setupDataCall_1_2(int param1Int1, int param1Int2, DataProfileInfo param1DataProfileInfo, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, int param1Int3, ArrayList<String> param1ArrayList1, ArrayList<String> param1ArrayList2) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hardware.radio@1.2::IRadio");
      hwParcel2.writeInt32(param1Int1);
      hwParcel2.writeInt32(param1Int2);
      param1DataProfileInfo.writeToParcel(hwParcel2);
      hwParcel2.writeBool(param1Boolean1);
      hwParcel2.writeBool(param1Boolean2);
      hwParcel2.writeBool(param1Boolean3);
      hwParcel2.writeInt32(param1Int3);
      hwParcel2.writeStringVector(param1ArrayList1);
      hwParcel2.writeStringVector(param1ArrayList2);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(141, hwParcel2, hwParcel1, 1);
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public void deactivateDataCall_1_2(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hardware.radio@1.2::IRadio");
      null.writeInt32(param1Int1);
      null.writeInt32(param1Int2);
      null.writeInt32(param1Int3);
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(142, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<String> interfaceChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256067662, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readStringVector();
      } finally {
        hwParcel.release();
      } 
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) throws RemoteException {
      HwParcel hwParcel2 = new HwParcel();
      hwParcel2.writeInterfaceToken("android.hidl.base@1.0::IBase");
      hwParcel2.writeNativeHandle(param1NativeHandle);
      hwParcel2.writeStringVector(param1ArrayList);
      HwParcel hwParcel1 = new HwParcel();
      try {
        this.mRemote.transact(256131655, hwParcel2, hwParcel1, 0);
        hwParcel1.verifySuccess();
        hwParcel2.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel1.release();
      } 
    }
    
    public String interfaceDescriptor() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256136003, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return hwParcel.readString();
      } finally {
        hwParcel.release();
      } 
    }
    
    public ArrayList<byte[]> getHashChain() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256398152, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        ArrayList<byte[]> arrayList = new ArrayList();
        this();
        HwBlob hwBlob = hwParcel.readBuffer(16L);
        int i = hwBlob.getInt32(8L);
        long l1 = (i * 32);
        long l2 = hwBlob.handle();
        hwBlob = hwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
        arrayList.clear();
        for (byte b = 0; b < i; b++) {
          byte[] arrayOfByte = new byte[32];
          l2 = (b * 32);
          hwBlob.copyToInt8Array(l2, arrayOfByte, 32);
          arrayList.add(arrayOfByte);
        } 
        return arrayList;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void setHALInstrumentation() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256462420, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) throws RemoteException {
      return this.mRemote.linkToDeath(param1DeathRecipient, param1Long);
    }
    
    public void ping() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(256921159, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public DebugInfo getDebugInfo() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257049926, null, hwParcel, 0);
        hwParcel.verifySuccess();
        null.releaseTemporaryStorage();
        DebugInfo debugInfo = new DebugInfo();
        this();
        debugInfo.readFromParcel(hwParcel);
        return debugInfo;
      } finally {
        hwParcel.release();
      } 
    }
    
    public void notifySyspropsChanged() throws RemoteException {
      null = new HwParcel();
      null.writeInterfaceToken("android.hidl.base@1.0::IBase");
      HwParcel hwParcel = new HwParcel();
      try {
        this.mRemote.transact(257120595, null, hwParcel, 1);
        null.releaseTemporaryStorage();
        return;
      } finally {
        hwParcel.release();
      } 
    }
    
    public boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) throws RemoteException {
      return this.mRemote.unlinkToDeath(param1DeathRecipient);
    }
  }
  
  public static abstract class Stub extends HwBinder implements IRadio {
    public IHwBinder asBinder() {
      return this;
    }
    
    public final ArrayList<String> interfaceChain() {
      return new ArrayList<>(Arrays.asList(new String[] { "android.hardware.radio@1.2::IRadio", "android.hardware.radio@1.1::IRadio", "android.hardware.radio@1.0::IRadio", "android.hidl.base@1.0::IBase" }));
    }
    
    public void debug(NativeHandle param1NativeHandle, ArrayList<String> param1ArrayList) {}
    
    public final String interfaceDescriptor() {
      return "android.hardware.radio@1.2::IRadio";
    }
    
    public final ArrayList<byte[]> getHashChain() {
      return (ArrayList)new ArrayList<>(Arrays.asList((byte[])new byte[][] { { 
                29, 25, 114, 13, 79, -45, -117, 16, -107, -16, 
                -11, 85, -92, -67, -110, -77, -79, 44, -101, 29, 
                15, 86, 11, 14, -102, 71, 76, -42, -36, -62, 
                13, -74 }, { 
                -9, -98, -33, 80, -93, 120, -87, -55, -69, 115, 
                Byte.MAX_VALUE, -109, -14, 5, -38, -71, 27, 76, 99, -22, 
                73, 114, 58, -4, 111, -123, 108, 19, -126, 3, 
                -22, -127 }, { 
                -101, 90, -92, -103, -20, 59, 66, 38, -15, 95, 
                72, -11, -19, 8, -119, 110, 47, -64, 103, 111, 
                -105, -116, -98, 25, -100, 29, -94, 29, -86, -16, 
                2, -90 }, { 
                -20, Byte.MAX_VALUE, -41, -98, -48, 45, -6, -123, -68, 73, 
                -108, 38, -83, -82, 62, -66, 35, -17, 5, 36, 
                -13, -51, 105, 87, 19, -109, 36, -72, 59, 24, 
                -54, 76 } }));
    }
    
    public final void setHALInstrumentation() {}
    
    public final boolean linkToDeath(IHwBinder.DeathRecipient param1DeathRecipient, long param1Long) {
      return true;
    }
    
    public final void ping() {}
    
    public final DebugInfo getDebugInfo() {
      DebugInfo debugInfo = new DebugInfo();
      debugInfo.pid = HidlSupport.getPidIfSharable();
      debugInfo.ptr = 0L;
      debugInfo.arch = 0;
      return debugInfo;
    }
    
    public final void notifySyspropsChanged() {
      HwBinder.enableInstrumentation();
    }
    
    public final boolean unlinkToDeath(IHwBinder.DeathRecipient param1DeathRecipient) {
      return true;
    }
    
    public IHwInterface queryLocalInterface(String param1String) {
      if ("android.hardware.radio@1.2::IRadio".equals(param1String))
        return this; 
      return null;
    }
    
    public void registerAsService(String param1String) throws RemoteException {
      registerService(param1String);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(interfaceDescriptor());
      stringBuilder.append("@Stub");
      return stringBuilder.toString();
    }
    
    public void onTransact(int param1Int1, HwParcel param1HwParcel1, HwParcel param1HwParcel2, int param1Int2) throws RemoteException {
      DebugInfo debugInfo;
      HwBlob hwBlob1;
      String str4;
      ArrayList<String> arrayList2;
      String str3;
      ArrayList<Byte> arrayList1;
      String str2;
      ArrayList<CdmaBroadcastSmsConfigInfo> arrayList;
      String str1;
      ArrayList<String> arrayList4;
      NetworkScanRequest networkScanRequest1;
      KeepaliveRequest keepaliveRequest;
      NetworkScanRequest networkScanRequest;
      ImsiEncryptionInfo imsiEncryptionInfo;
      CarrierRestrictions carrierRestrictions;
      RadioCapability radioCapability;
      ArrayList<DataProfileInfo> arrayList3;
      String str9;
      SelectUiccSub selectUiccSub;
      NvWriteItem nvWriteItem;
      SimApdu simApdu2;
      String str8;
      SimApdu simApdu1;
      ImsSmsMessage imsSmsMessage;
      DataProfileInfo dataProfileInfo2;
      CdmaSmsWriteArgs cdmaSmsWriteArgs;
      CdmaSmsAck cdmaSmsAck;
      CdmaSmsMessage cdmaSmsMessage;
      String str7;
      SmsWriteArgs smsWriteArgs;
      String str6;
      CallForwardInfo callForwardInfo;
      IccIo iccIo;
      DataProfileInfo dataProfileInfo1;
      GsmSmsMessage gsmSmsMessage;
      Dial dial;
      String str5;
      ArrayList<byte[]> arrayList5;
      HwBlob hwBlob2;
      NativeHandle nativeHandle;
      DataProfileInfo dataProfileInfo3;
      ArrayList<Integer> arrayList6;
      String str10;
      int i;
      boolean bool1, bool2, bool3;
      int j, k;
      switch (param1Int1) {
        default:
          switch (param1Int1) {
            default:
              return;
            case 257120595:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              notifySyspropsChanged();
            case 257049926:
              param1HwParcel1.enforceInterface("android.hidl.base@1.0::IBase");
              debugInfo = getDebugInfo();
              param1HwParcel2.writeStatus(0);
              debugInfo.writeToParcel(param1HwParcel2);
              param1HwParcel2.send();
            case 256921159:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              ping();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256462420:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              setHALInstrumentation();
            case 256398152:
              debugInfo.enforceInterface("android.hidl.base@1.0::IBase");
              arrayList5 = getHashChain();
              param1HwParcel2.writeStatus(0);
              hwBlob2 = new HwBlob(16);
              param1Int2 = arrayList5.size();
              hwBlob2.putInt32(8L, param1Int2);
              hwBlob2.putBool(12L, false);
              hwBlob1 = new HwBlob(param1Int2 * 32);
              for (param1Int1 = 0; param1Int1 < param1Int2; ) {
                long l = (param1Int1 * 32);
                byte[] arrayOfByte = arrayList5.get(param1Int1);
                if (arrayOfByte != null && arrayOfByte.length == 32) {
                  hwBlob1.putInt8Array(l, arrayOfByte);
                  param1Int1++;
                } 
                throw new IllegalArgumentException("Array element is not of the expected length");
              } 
              hwBlob2.putBlob(0L, hwBlob1);
              param1HwParcel2.writeBuffer(hwBlob2);
              param1HwParcel2.send();
            case 256136003:
              hwBlob1.enforceInterface("android.hidl.base@1.0::IBase");
              str4 = interfaceDescriptor();
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.writeString(str4);
              param1HwParcel2.send();
            case 256131655:
              str4.enforceInterface("android.hidl.base@1.0::IBase");
              nativeHandle = str4.readNativeHandle();
              arrayList2 = str4.readStringVector();
              debug(nativeHandle, arrayList2);
              param1HwParcel2.writeStatus(0);
              param1HwParcel2.send();
            case 256067662:
              break;
          } 
          arrayList2.enforceInterface("android.hidl.base@1.0::IBase");
          arrayList2 = interfaceChain();
          param1HwParcel2.writeStatus(0);
          param1HwParcel2.writeStringVector(arrayList2);
          param1HwParcel2.send();
        case 142:
          arrayList2.enforceInterface("android.hardware.radio@1.2::IRadio");
          i = arrayList2.readInt32();
          param1Int2 = arrayList2.readInt32();
          param1Int1 = arrayList2.readInt32();
          deactivateDataCall_1_2(i, param1Int2, param1Int1);
        case 141:
          arrayList2.enforceInterface("android.hardware.radio@1.2::IRadio");
          param1Int1 = arrayList2.readInt32();
          i = arrayList2.readInt32();
          dataProfileInfo3 = new DataProfileInfo();
          dataProfileInfo3.readFromParcel((HwParcel)arrayList2);
          bool1 = arrayList2.readBool();
          bool2 = arrayList2.readBool();
          bool3 = arrayList2.readBool();
          param1Int2 = arrayList2.readInt32();
          arrayList4 = arrayList2.readStringVector();
          arrayList2 = arrayList2.readStringVector();
          setupDataCall_1_2(param1Int1, i, dataProfileInfo3, bool1, bool2, bool3, param1Int2, arrayList4, arrayList2);
        case 140:
          arrayList2.enforceInterface("android.hardware.radio@1.2::IRadio");
          i = arrayList2.readInt32();
          param1Int1 = arrayList2.readInt32();
          param1Int2 = arrayList2.readInt32();
          j = arrayList2.readInt32();
          arrayList4 = (ArrayList)arrayList2.readInt32Vector();
          arrayList6 = arrayList2.readInt32Vector();
          k = arrayList2.readInt32();
          setLinkCapacityReportingCriteria(i, param1Int1, param1Int2, j, (ArrayList)arrayList4, arrayList6, k);
        case 139:
          arrayList2.enforceInterface("android.hardware.radio@1.2::IRadio");
          param1Int1 = arrayList2.readInt32();
          param1Int2 = arrayList2.readInt32();
          j = arrayList2.readInt32();
          arrayList4 = (ArrayList)arrayList2.readInt32Vector();
          i = arrayList2.readInt32();
          setSignalStrengthReportingCriteria(param1Int1, param1Int2, j, (ArrayList)arrayList4, i);
        case 138:
          arrayList2.enforceInterface("android.hardware.radio@1.2::IRadio");
          param1Int1 = arrayList2.readInt32();
          param1Int2 = arrayList2.readInt32();
          setIndicationFilter_1_2(param1Int1, param1Int2);
        case 137:
          arrayList2.enforceInterface("android.hardware.radio@1.2::IRadio");
          param1Int1 = arrayList2.readInt32();
          networkScanRequest1 = new NetworkScanRequest();
          networkScanRequest1.readFromParcel((HwParcel)arrayList2);
          startNetworkScan_1_2(param1Int1, networkScanRequest1);
        case 136:
          arrayList2.enforceInterface("android.hardware.radio@1.1::IRadio");
          param1Int2 = arrayList2.readInt32();
          param1Int1 = arrayList2.readInt32();
          stopKeepalive(param1Int2, param1Int1);
        case 135:
          arrayList2.enforceInterface("android.hardware.radio@1.1::IRadio");
          param1Int1 = arrayList2.readInt32();
          keepaliveRequest = new KeepaliveRequest();
          keepaliveRequest.readFromParcel((HwParcel)arrayList2);
          startKeepalive(param1Int1, keepaliveRequest);
        case 134:
          arrayList2.enforceInterface("android.hardware.radio@1.1::IRadio");
          param1Int1 = arrayList2.readInt32();
          stopNetworkScan(param1Int1);
        case 133:
          arrayList2.enforceInterface("android.hardware.radio@1.1::IRadio");
          param1Int1 = arrayList2.readInt32();
          networkScanRequest = new NetworkScanRequest();
          networkScanRequest.readFromParcel((HwParcel)arrayList2);
          startNetworkScan(param1Int1, networkScanRequest);
        case 132:
          arrayList2.enforceInterface("android.hardware.radio@1.1::IRadio");
          param1Int2 = arrayList2.readInt32();
          param1Int1 = arrayList2.readInt32();
          setSimCardPower_1_1(param1Int2, param1Int1);
        case 131:
          arrayList2.enforceInterface("android.hardware.radio@1.1::IRadio");
          param1Int1 = arrayList2.readInt32();
          imsiEncryptionInfo = new ImsiEncryptionInfo();
          imsiEncryptionInfo.readFromParcel((HwParcel)arrayList2);
          setCarrierInfoForImsiEncryption(param1Int1, imsiEncryptionInfo);
        case 130:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          responseAcknowledgement();
        case 129:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList2.readInt32();
          bool3 = arrayList2.readBool();
          setSimCardPower(param1Int1, bool3);
        case 128:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = arrayList2.readInt32();
          param1Int1 = arrayList2.readInt32();
          setIndicationFilter(param1Int2, param1Int1);
        case 127:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList2.readInt32();
          param1Int2 = arrayList2.readInt32();
          bool3 = arrayList2.readBool();
          sendDeviceState(param1Int1, param1Int2, bool3);
        case 126:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList2.readInt32();
          getAllowedCarriers(param1Int1);
        case 125:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList2.readInt32();
          bool3 = arrayList2.readBool();
          carrierRestrictions = new CarrierRestrictions();
          carrierRestrictions.readFromParcel((HwParcel)arrayList2);
          setAllowedCarriers(param1Int1, bool3, carrierRestrictions);
        case 124:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList2.readInt32();
          getModemActivityInfo(param1Int1);
        case 123:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList2.readInt32();
          pullLceData(param1Int1);
        case 122:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList2.readInt32();
          stopLceService(param1Int1);
        case 121:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList2.readInt32();
          param1Int2 = arrayList2.readInt32();
          bool3 = arrayList2.readBool();
          startLceService(param1Int1, param1Int2, bool3);
        case 120:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList2.readInt32();
          radioCapability = new RadioCapability();
          radioCapability.readFromParcel((HwParcel)arrayList2);
          setRadioCapability(param1Int1, radioCapability);
        case 119:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList2.readInt32();
          getRadioCapability(param1Int1);
        case 118:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList2.readInt32();
          requestShutdown(param1Int1);
        case 117:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList2.readInt32();
          arrayList3 = DataProfileInfo.readVectorFromParcel((HwParcel)arrayList2);
          bool3 = arrayList2.readBool();
          setDataProfile(param1Int1, arrayList3, bool3);
        case 116:
          arrayList2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList2.readInt32();
          param1Int2 = arrayList2.readInt32();
          str9 = arrayList2.readString();
          str3 = arrayList2.readString();
          requestIccSimAuthentication(param1Int1, param1Int2, str9, str3);
        case 115:
          str3.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str3.readInt32();
          getHardwareConfig(param1Int1);
        case 114:
          str3.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str3.readInt32();
          bool3 = str3.readBool();
          setDataAllowed(param1Int1, bool3);
        case 113:
          str3.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str3.readInt32();
          selectUiccSub = new SelectUiccSub();
          selectUiccSub.readFromParcel((HwParcel)str3);
          setUiccSubscription(param1Int1, selectUiccSub);
        case 112:
          str3.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str3.readInt32();
          param1Int2 = str3.readInt32();
          nvResetConfig(param1Int1, param1Int2);
        case 111:
          str3.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str3.readInt32();
          arrayList1 = str3.readInt8Vector();
          nvWriteCdmaPrl(param1Int1, arrayList1);
        case 110:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList1.readInt32();
          nvWriteItem = new NvWriteItem();
          nvWriteItem.readFromParcel((HwParcel)arrayList1);
          nvWriteItem(param1Int1, nvWriteItem);
        case 109:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = arrayList1.readInt32();
          param1Int1 = arrayList1.readInt32();
          nvReadItem(param1Int2, param1Int1);
        case 108:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList1.readInt32();
          simApdu2 = new SimApdu();
          simApdu2.readFromParcel((HwParcel)arrayList1);
          iccTransmitApduLogicalChannel(param1Int1, simApdu2);
        case 107:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList1.readInt32();
          param1Int2 = arrayList1.readInt32();
          iccCloseLogicalChannel(param1Int1, param1Int2);
        case 106:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = arrayList1.readInt32();
          str8 = arrayList1.readString();
          param1Int1 = arrayList1.readInt32();
          iccOpenLogicalChannel(param1Int2, str8, param1Int1);
        case 105:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList1.readInt32();
          simApdu1 = new SimApdu();
          simApdu1.readFromParcel((HwParcel)arrayList1);
          iccTransmitApduBasicChannel(param1Int1, simApdu1);
        case 104:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList1.readInt32();
          imsSmsMessage = new ImsSmsMessage();
          imsSmsMessage.readFromParcel((HwParcel)arrayList1);
          sendImsSms(param1Int1, imsSmsMessage);
        case 103:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList1.readInt32();
          getImsRegistrationState(param1Int1);
        case 102:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList1.readInt32();
          dataProfileInfo2 = new DataProfileInfo();
          dataProfileInfo2.readFromParcel((HwParcel)arrayList1);
          bool2 = arrayList1.readBool();
          bool3 = arrayList1.readBool();
          setInitialAttachApn(param1Int1, dataProfileInfo2, bool2, bool3);
        case 101:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = arrayList1.readInt32();
          param1Int1 = arrayList1.readInt32();
          setCellInfoListRate(param1Int2, param1Int1);
        case 100:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList1.readInt32();
          getCellInfoList(param1Int1);
        case 99:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList1.readInt32();
          getVoiceRadioTechnology(param1Int1);
        case 98:
          arrayList1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList1.readInt32();
          str2 = arrayList1.readString();
          sendEnvelopeWithStatus(param1Int1, str2);
        case 97:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str2.readInt32();
          bool3 = str2.readBool();
          str2 = str2.readString();
          acknowledgeIncomingGsmSmsWithPdu(param1Int1, bool3, str2);
        case 96:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str2.readInt32();
          str2 = str2.readString();
          requestIsimAuthentication(param1Int1, str2);
        case 95:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str2.readInt32();
          getCdmaSubscriptionSource(param1Int1);
        case 94:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str2.readInt32();
          reportStkServiceIsRunning(param1Int1);
        case 93:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str2.readInt32();
          bool3 = str2.readBool();
          reportSmsMemoryStatus(param1Int1, bool3);
        case 92:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str2.readInt32();
          str2 = str2.readString();
          setSmscAddress(param1Int1, str2);
        case 91:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str2.readInt32();
          getSmscAddress(param1Int1);
        case 90:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str2.readInt32();
          exitEmergencyCallbackMode(param1Int1);
        case 89:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str2.readInt32();
          getDeviceIdentity(param1Int1);
        case 88:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = str2.readInt32();
          param1Int1 = str2.readInt32();
          deleteSmsOnRuim(param1Int2, param1Int1);
        case 87:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str2.readInt32();
          cdmaSmsWriteArgs = new CdmaSmsWriteArgs();
          cdmaSmsWriteArgs.readFromParcel((HwParcel)str2);
          writeSmsToRuim(param1Int1, cdmaSmsWriteArgs);
        case 86:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str2.readInt32();
          getCDMASubscription(param1Int1);
        case 85:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str2.readInt32();
          bool3 = str2.readBool();
          setCdmaBroadcastActivation(param1Int1, bool3);
        case 84:
          str2.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str2.readInt32();
          arrayList = CdmaBroadcastSmsConfigInfo.readVectorFromParcel((HwParcel)str2);
          setCdmaBroadcastConfig(param1Int1, arrayList);
        case 83:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList.readInt32();
          getCdmaBroadcastConfig(param1Int1);
        case 82:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList.readInt32();
          bool3 = arrayList.readBool();
          setGsmBroadcastActivation(param1Int1, bool3);
        case 81:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList.readInt32();
          arrayList = (ArrayList)GsmBroadcastSmsConfigInfo.readVectorFromParcel((HwParcel)arrayList);
          setGsmBroadcastConfig(param1Int1, (ArrayList)arrayList);
        case 80:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList.readInt32();
          getGsmBroadcastConfig(param1Int1);
        case 79:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList.readInt32();
          cdmaSmsAck = new CdmaSmsAck();
          cdmaSmsAck.readFromParcel((HwParcel)arrayList);
          acknowledgeLastIncomingCdmaSms(param1Int1, cdmaSmsAck);
        case 78:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList.readInt32();
          cdmaSmsMessage = new CdmaSmsMessage();
          cdmaSmsMessage.readFromParcel((HwParcel)arrayList);
          sendCdmaSms(param1Int1, cdmaSmsMessage);
        case 77:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = arrayList.readInt32();
          str7 = arrayList.readString();
          param1Int1 = arrayList.readInt32();
          i = arrayList.readInt32();
          sendBurstDtmf(param1Int2, str7, param1Int1, i);
        case 76:
          arrayList.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = arrayList.readInt32();
          str1 = arrayList.readString();
          sendCDMAFeatureCode(param1Int1, str1);
        case 75:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getPreferredVoicePrivacy(param1Int1);
        case 74:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          bool3 = str1.readBool();
          setPreferredVoicePrivacy(param1Int1, bool3);
        case 73:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getTTYMode(param1Int1);
        case 72:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = str1.readInt32();
          param1Int1 = str1.readInt32();
          setTTYMode(param1Int2, param1Int1);
        case 71:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getCdmaRoamingPreference(param1Int1);
        case 70:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = str1.readInt32();
          param1Int1 = str1.readInt32();
          setCdmaRoamingPreference(param1Int2, param1Int1);
        case 69:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          param1Int2 = str1.readInt32();
          setCdmaSubscriptionSource(param1Int1, param1Int2);
        case 68:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          bool3 = str1.readBool();
          setLocationUpdates(param1Int1, bool3);
        case 67:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getNeighboringCids(param1Int1);
        case 66:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getPreferredNetworkType(param1Int1);
        case 65:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = str1.readInt32();
          param1Int1 = str1.readInt32();
          setPreferredNetworkType(param1Int2, param1Int1);
        case 64:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          explicitCallTransfer(param1Int1);
        case 63:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          bool3 = str1.readBool();
          handleStkCallSetupRequestFromSim(param1Int1, bool3);
        case 62:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str1 = str1.readString();
          sendTerminalResponseToSim(param1Int1, str1);
        case 61:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str1 = str1.readString();
          sendEnvelope(param1Int1, str1);
        case 60:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getAvailableBandModes(param1Int1);
        case 59:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          param1Int2 = str1.readInt32();
          setBandMode(param1Int1, param1Int2);
        case 58:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          param1Int2 = str1.readInt32();
          deleteSmsOnSim(param1Int1, param1Int2);
        case 57:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          smsWriteArgs = new SmsWriteArgs();
          smsWriteArgs.readFromParcel((HwParcel)str1);
          writeSmsToSim(param1Int1, smsWriteArgs);
        case 56:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          bool3 = str1.readBool();
          setSuppServiceNotifications(param1Int1, bool3);
        case 55:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getDataCallList(param1Int1);
        case 54:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getClip(param1Int1);
        case 53:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getMute(param1Int1);
        case 52:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          bool3 = str1.readBool();
          setMute(param1Int1, bool3);
        case 51:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = str1.readInt32();
          param1Int1 = str1.readInt32();
          separateConnection(param1Int2, param1Int1);
        case 50:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getBasebandVersion(param1Int1);
        case 49:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          stopDtmf(param1Int1);
        case 48:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str1 = str1.readString();
          startDtmf(param1Int1, str1);
        case 47:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getAvailableNetworks(param1Int1);
        case 46:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str1 = str1.readString();
          setNetworkSelectionModeManual(param1Int1, str1);
        case 45:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          setNetworkSelectionModeAutomatic(param1Int1);
        case 44:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getNetworkSelectionMode(param1Int1);
        case 43:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str6 = str1.readString();
          str10 = str1.readString();
          str1 = str1.readString();
          setBarringPassword(param1Int1, str6, str10, str1);
        case 42:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = str1.readInt32();
          str10 = str1.readString();
          bool3 = str1.readBool();
          str6 = str1.readString();
          param1Int1 = str1.readInt32();
          str1 = str1.readString();
          setFacilityLockForApp(param1Int2, str10, bool3, str6, param1Int1, str1);
        case 41:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = str1.readInt32();
          str10 = str1.readString();
          str6 = str1.readString();
          param1Int1 = str1.readInt32();
          str1 = str1.readString();
          getFacilityLockForApp(param1Int2, str10, str6, param1Int1, str1);
        case 40:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = str1.readInt32();
          param1Int1 = str1.readInt32();
          bool3 = str1.readBool();
          deactivateDataCall(param1Int2, param1Int1, bool3);
        case 39:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          acceptCall(param1Int1);
        case 38:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          bool3 = str1.readBool();
          param1Int2 = str1.readInt32();
          acknowledgeLastIncomingGsmSms(param1Int1, bool3, param1Int2);
        case 37:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          bool3 = str1.readBool();
          param1Int2 = str1.readInt32();
          setCallWaiting(param1Int1, bool3, param1Int2);
        case 36:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = str1.readInt32();
          param1Int1 = str1.readInt32();
          getCallWaiting(param1Int2, param1Int1);
        case 35:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          callForwardInfo = new CallForwardInfo();
          callForwardInfo.readFromParcel((HwParcel)str1);
          setCallForward(param1Int1, callForwardInfo);
        case 34:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          callForwardInfo = new CallForwardInfo();
          callForwardInfo.readFromParcel((HwParcel)str1);
          getCallForwardStatus(param1Int1, callForwardInfo);
        case 33:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = str1.readInt32();
          param1Int1 = str1.readInt32();
          setClir(param1Int2, param1Int1);
        case 32:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getClir(param1Int1);
        case 31:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          cancelPendingUssd(param1Int1);
        case 30:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str1 = str1.readString();
          sendUssd(param1Int1, str1);
        case 29:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          iccIo = new IccIo();
          iccIo.readFromParcel((HwParcel)str1);
          iccIOForApp(param1Int1, iccIo);
        case 28:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          param1Int2 = str1.readInt32();
          dataProfileInfo1 = new DataProfileInfo();
          dataProfileInfo1.readFromParcel((HwParcel)str1);
          bool1 = str1.readBool();
          bool3 = str1.readBool();
          bool2 = str1.readBool();
          setupDataCall(param1Int1, param1Int2, dataProfileInfo1, bool1, bool3, bool2);
        case 27:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          gsmSmsMessage = new GsmSmsMessage();
          gsmSmsMessage.readFromParcel((HwParcel)str1);
          sendSMSExpectMore(param1Int1, gsmSmsMessage);
        case 26:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          gsmSmsMessage = new GsmSmsMessage();
          gsmSmsMessage.readFromParcel((HwParcel)str1);
          sendSms(param1Int1, gsmSmsMessage);
        case 25:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str1 = str1.readString();
          sendDtmf(param1Int1, str1);
        case 24:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          bool3 = str1.readBool();
          setRadioPower(param1Int1, bool3);
        case 23:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getOperator(param1Int1);
        case 22:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getDataRegistrationState(param1Int1);
        case 21:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getVoiceRegistrationState(param1Int1);
        case 20:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getSignalStrength(param1Int1);
        case 19:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getLastCallFailCause(param1Int1);
        case 18:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          rejectCall(param1Int1);
        case 17:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          conference(param1Int1);
        case 16:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          switchWaitingOrHoldingAndActive(param1Int1);
        case 15:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          hangupForegroundResumeBackground(param1Int1);
        case 14:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          hangupWaitingOrBackground(param1Int1);
        case 13:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int2 = str1.readInt32();
          param1Int1 = str1.readInt32();
          hangup(param1Int2, param1Int1);
        case 12:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str1 = str1.readString();
          getImsiForApp(param1Int1, str1);
        case 11:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          dial = new Dial();
          dial.readFromParcel((HwParcel)str1);
          dial(param1Int1, dial);
        case 10:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getCurrentCalls(param1Int1);
        case 9:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str1 = str1.readString();
          supplyNetworkDepersonalization(param1Int1, str1);
        case 8:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str5 = str1.readString();
          str10 = str1.readString();
          str1 = str1.readString();
          changeIccPin2ForApp(param1Int1, str5, str10, str1);
        case 7:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str5 = str1.readString();
          str10 = str1.readString();
          str1 = str1.readString();
          changeIccPinForApp(param1Int1, str5, str10, str1);
        case 6:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str10 = str1.readString();
          str5 = str1.readString();
          str1 = str1.readString();
          supplyIccPuk2ForApp(param1Int1, str10, str5, str1);
        case 5:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str5 = str1.readString();
          str1 = str1.readString();
          supplyIccPin2ForApp(param1Int1, str5, str1);
        case 4:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str5 = str1.readString();
          str10 = str1.readString();
          str1 = str1.readString();
          supplyIccPukForApp(param1Int1, str5, str10, str1);
        case 3:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          str5 = str1.readString();
          str1 = str1.readString();
          supplyIccPinForApp(param1Int1, str5, str1);
        case 2:
          str1.enforceInterface("android.hardware.radio@1.0::IRadio");
          param1Int1 = str1.readInt32();
          getIccCardStatus(param1Int1);
        case 1:
          break;
      } 
      str1.enforceInterface("android.hardware.radio@1.0::IRadio");
      IRadioResponse iRadioResponse = IRadioResponse.asInterface(str1.readStrongBinder());
      IRadioIndication iRadioIndication = IRadioIndication.asInterface(str1.readStrongBinder());
      setResponseFunctions(iRadioResponse, iRadioIndication);
      str5.writeStatus(0);
      str5.send();
    }
  }
}
