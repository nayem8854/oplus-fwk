package android.hardware.radio.V1_2;

import android.hardware.radio.V1_0.RegState;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class VoiceRegStateResult {
  public int regState = 0;
  
  public int rat = 0;
  
  public boolean cssSupported = false;
  
  public int roamingIndicator = 0;
  
  public int systemIsInPrl = 0;
  
  public int defaultRoamingIndicator = 0;
  
  public int reasonForDenial = 0;
  
  public CellIdentity cellIdentity = new CellIdentity();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != VoiceRegStateResult.class)
      return false; 
    paramObject = paramObject;
    if (this.regState != ((VoiceRegStateResult)paramObject).regState)
      return false; 
    if (this.rat != ((VoiceRegStateResult)paramObject).rat)
      return false; 
    if (this.cssSupported != ((VoiceRegStateResult)paramObject).cssSupported)
      return false; 
    if (this.roamingIndicator != ((VoiceRegStateResult)paramObject).roamingIndicator)
      return false; 
    if (this.systemIsInPrl != ((VoiceRegStateResult)paramObject).systemIsInPrl)
      return false; 
    if (this.defaultRoamingIndicator != ((VoiceRegStateResult)paramObject).defaultRoamingIndicator)
      return false; 
    if (this.reasonForDenial != ((VoiceRegStateResult)paramObject).reasonForDenial)
      return false; 
    if (!HidlSupport.deepEquals(this.cellIdentity, ((VoiceRegStateResult)paramObject).cellIdentity))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.regState;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.rat;
    int k = HidlSupport.deepHashCode(Integer.valueOf(j));
    boolean bool = this.cssSupported;
    j = HidlSupport.deepHashCode(Boolean.valueOf(bool));
    int m = this.roamingIndicator;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    int n = this.systemIsInPrl;
    n = HidlSupport.deepHashCode(Integer.valueOf(n));
    int i1 = this.defaultRoamingIndicator;
    i1 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    int i2 = this.reasonForDenial;
    i2 = HidlSupport.deepHashCode(Integer.valueOf(i2));
    CellIdentity cellIdentity = this.cellIdentity;
    int i3 = HidlSupport.deepHashCode(cellIdentity);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(k), Integer.valueOf(j), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".regState = ");
    stringBuilder.append(RegState.toString(this.regState));
    stringBuilder.append(", .rat = ");
    stringBuilder.append(this.rat);
    stringBuilder.append(", .cssSupported = ");
    stringBuilder.append(this.cssSupported);
    stringBuilder.append(", .roamingIndicator = ");
    stringBuilder.append(this.roamingIndicator);
    stringBuilder.append(", .systemIsInPrl = ");
    stringBuilder.append(this.systemIsInPrl);
    stringBuilder.append(", .defaultRoamingIndicator = ");
    stringBuilder.append(this.defaultRoamingIndicator);
    stringBuilder.append(", .reasonForDenial = ");
    stringBuilder.append(this.reasonForDenial);
    stringBuilder.append(", .cellIdentity = ");
    stringBuilder.append(this.cellIdentity);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(120L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<VoiceRegStateResult> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<VoiceRegStateResult> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 120);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      VoiceRegStateResult voiceRegStateResult = new VoiceRegStateResult();
      voiceRegStateResult.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 120));
      arrayList.add(voiceRegStateResult);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.regState = paramHwBlob.getInt32(0L + paramLong);
    this.rat = paramHwBlob.getInt32(4L + paramLong);
    this.cssSupported = paramHwBlob.getBool(8L + paramLong);
    this.roamingIndicator = paramHwBlob.getInt32(12L + paramLong);
    this.systemIsInPrl = paramHwBlob.getInt32(16L + paramLong);
    this.defaultRoamingIndicator = paramHwBlob.getInt32(20L + paramLong);
    this.reasonForDenial = paramHwBlob.getInt32(24L + paramLong);
    this.cellIdentity.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 32L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(120);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<VoiceRegStateResult> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 120);
    for (byte b = 0; b < i; b++)
      ((VoiceRegStateResult)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 120)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.regState);
    paramHwBlob.putInt32(4L + paramLong, this.rat);
    paramHwBlob.putBool(8L + paramLong, this.cssSupported);
    paramHwBlob.putInt32(12L + paramLong, this.roamingIndicator);
    paramHwBlob.putInt32(16L + paramLong, this.systemIsInPrl);
    paramHwBlob.putInt32(20L + paramLong, this.defaultRoamingIndicator);
    paramHwBlob.putInt32(24L + paramLong, this.reasonForDenial);
    this.cellIdentity.writeEmbeddedToBlob(paramHwBlob, 32L + paramLong);
  }
}
