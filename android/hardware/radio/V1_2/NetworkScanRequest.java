package android.hardware.radio.V1_2;

import android.hardware.radio.V1_1.RadioAccessSpecifier;
import android.hardware.radio.V1_1.ScanType;
import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class NetworkScanRequest {
  public int type = 0;
  
  public int interval = 0;
  
  public ArrayList<RadioAccessSpecifier> specifiers = new ArrayList<>();
  
  public int maxSearchTime = 0;
  
  public boolean incrementalResults = false;
  
  public int incrementalResultsPeriodicity = 0;
  
  public ArrayList<String> mccMncs = new ArrayList<>();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != NetworkScanRequest.class)
      return false; 
    paramObject = paramObject;
    if (this.type != ((NetworkScanRequest)paramObject).type)
      return false; 
    if (this.interval != ((NetworkScanRequest)paramObject).interval)
      return false; 
    if (!HidlSupport.deepEquals(this.specifiers, ((NetworkScanRequest)paramObject).specifiers))
      return false; 
    if (this.maxSearchTime != ((NetworkScanRequest)paramObject).maxSearchTime)
      return false; 
    if (this.incrementalResults != ((NetworkScanRequest)paramObject).incrementalResults)
      return false; 
    if (this.incrementalResultsPeriodicity != ((NetworkScanRequest)paramObject).incrementalResultsPeriodicity)
      return false; 
    if (!HidlSupport.deepEquals(this.mccMncs, ((NetworkScanRequest)paramObject).mccMncs))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.type;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.interval;
    int k = HidlSupport.deepHashCode(Integer.valueOf(j));
    ArrayList<RadioAccessSpecifier> arrayList1 = this.specifiers;
    j = HidlSupport.deepHashCode(arrayList1);
    int m = this.maxSearchTime;
    m = HidlSupport.deepHashCode(Integer.valueOf(m));
    boolean bool = this.incrementalResults;
    int n = HidlSupport.deepHashCode(Boolean.valueOf(bool)), i1 = this.incrementalResultsPeriodicity;
    i1 = HidlSupport.deepHashCode(Integer.valueOf(i1));
    ArrayList<String> arrayList = this.mccMncs;
    int i2 = HidlSupport.deepHashCode(arrayList);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(k), Integer.valueOf(j), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".type = ");
    stringBuilder.append(ScanType.toString(this.type));
    stringBuilder.append(", .interval = ");
    stringBuilder.append(this.interval);
    stringBuilder.append(", .specifiers = ");
    stringBuilder.append(this.specifiers);
    stringBuilder.append(", .maxSearchTime = ");
    stringBuilder.append(this.maxSearchTime);
    stringBuilder.append(", .incrementalResults = ");
    stringBuilder.append(this.incrementalResults);
    stringBuilder.append(", .incrementalResultsPeriodicity = ");
    stringBuilder.append(this.incrementalResultsPeriodicity);
    stringBuilder.append(", .mccMncs = ");
    stringBuilder.append(this.mccMncs);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(56L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<NetworkScanRequest> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<NetworkScanRequest> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 56);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      NetworkScanRequest networkScanRequest = new NetworkScanRequest();
      networkScanRequest.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 56));
      arrayList.add(networkScanRequest);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.type = paramHwBlob.getInt32(paramLong + 0L);
    this.interval = paramHwBlob.getInt32(paramLong + 4L);
    int i = paramHwBlob.getInt32(paramLong + 8L + 8L);
    long l1 = (i * 72);
    long l2 = paramHwBlob.handle();
    HwBlob hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 8L + 0L, true);
    this.specifiers.clear();
    byte b;
    for (b = 0; b < i; b++) {
      RadioAccessSpecifier radioAccessSpecifier = new RadioAccessSpecifier();
      radioAccessSpecifier.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 72));
      this.specifiers.add(radioAccessSpecifier);
    } 
    this.maxSearchTime = paramHwBlob.getInt32(paramLong + 24L);
    this.incrementalResults = paramHwBlob.getBool(paramLong + 28L);
    this.incrementalResultsPeriodicity = paramHwBlob.getInt32(paramLong + 32L);
    i = paramHwBlob.getInt32(paramLong + 40L + 8L);
    l1 = (i * 16);
    l2 = paramHwBlob.handle();
    paramHwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 40L + 0L, true);
    this.mccMncs.clear();
    for (b = 0; b < i; b++) {
      new String();
      String str = paramHwBlob.getString((b * 16));
      l2 = ((str.getBytes()).length + 1);
      paramLong = paramHwBlob.handle();
      l1 = (b * 16 + 0);
      paramHwParcel.readEmbeddedBuffer(l2, paramLong, l1, false);
      this.mccMncs.add(str);
    } 
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(56);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<NetworkScanRequest> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 56);
    for (byte b = 0; b < i; b++)
      ((NetworkScanRequest)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 56)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(paramLong + 0L, this.type);
    paramHwBlob.putInt32(paramLong + 4L, this.interval);
    int i = this.specifiers.size();
    paramHwBlob.putInt32(paramLong + 8L + 8L, i);
    paramHwBlob.putBool(paramLong + 8L + 12L, false);
    HwBlob hwBlob = new HwBlob(i * 72);
    byte b;
    for (b = 0; b < i; b++)
      ((RadioAccessSpecifier)this.specifiers.get(b)).writeEmbeddedToBlob(hwBlob, (b * 72)); 
    paramHwBlob.putBlob(paramLong + 8L + 0L, hwBlob);
    paramHwBlob.putInt32(paramLong + 24L, this.maxSearchTime);
    paramHwBlob.putBool(paramLong + 28L, this.incrementalResults);
    paramHwBlob.putInt32(paramLong + 32L, this.incrementalResultsPeriodicity);
    i = this.mccMncs.size();
    paramHwBlob.putInt32(paramLong + 40L + 8L, i);
    paramHwBlob.putBool(paramLong + 40L + 12L, false);
    hwBlob = new HwBlob(i * 16);
    for (b = 0; b < i; b++)
      hwBlob.putString((b * 16), this.mccMncs.get(b)); 
    paramHwBlob.putBlob(paramLong + 40L + 0L, hwBlob);
  }
}
