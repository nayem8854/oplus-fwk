package android.hardware.radio.V1_2;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CellIdentityOperatorNames {
  public String alphaLong = new String();
  
  public String alphaShort = new String();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CellIdentityOperatorNames.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.alphaLong, ((CellIdentityOperatorNames)paramObject).alphaLong))
      return false; 
    if (!HidlSupport.deepEquals(this.alphaShort, ((CellIdentityOperatorNames)paramObject).alphaShort))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    String str = this.alphaLong;
    int i = HidlSupport.deepHashCode(str);
    str = this.alphaShort;
    int j = HidlSupport.deepHashCode(str);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".alphaLong = ");
    stringBuilder.append(this.alphaLong);
    stringBuilder.append(", .alphaShort = ");
    stringBuilder.append(this.alphaShort);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(32L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CellIdentityOperatorNames> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CellIdentityOperatorNames> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 32);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CellIdentityOperatorNames cellIdentityOperatorNames = new CellIdentityOperatorNames();
      cellIdentityOperatorNames.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 32));
      arrayList.add(cellIdentityOperatorNames);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    String str = paramHwBlob.getString(paramLong + 0L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 0L + 0L, false);
    this.alphaShort = str = paramHwBlob.getString(paramLong + 16L);
    l2 = ((str.getBytes()).length + 1);
    l1 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l2, l1, paramLong + 16L + 0L, false);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(32);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CellIdentityOperatorNames> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 32);
    for (byte b = 0; b < i; b++)
      ((CellIdentityOperatorNames)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 32)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putString(0L + paramLong, this.alphaLong);
    paramHwBlob.putString(16L + paramLong, this.alphaShort);
  }
}
