package android.hardware.radio.V1_2;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class LinkCapacityEstimate {
  public int downlinkCapacityKbps = 0;
  
  public int uplinkCapacityKbps = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != LinkCapacityEstimate.class)
      return false; 
    paramObject = paramObject;
    if (this.downlinkCapacityKbps != ((LinkCapacityEstimate)paramObject).downlinkCapacityKbps)
      return false; 
    if (this.uplinkCapacityKbps != ((LinkCapacityEstimate)paramObject).uplinkCapacityKbps)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    int i = this.downlinkCapacityKbps;
    i = HidlSupport.deepHashCode(Integer.valueOf(i));
    int j = this.uplinkCapacityKbps;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".downlinkCapacityKbps = ");
    stringBuilder.append(this.downlinkCapacityKbps);
    stringBuilder.append(", .uplinkCapacityKbps = ");
    stringBuilder.append(this.uplinkCapacityKbps);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(8L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<LinkCapacityEstimate> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<LinkCapacityEstimate> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 8);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      LinkCapacityEstimate linkCapacityEstimate = new LinkCapacityEstimate();
      linkCapacityEstimate.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 8));
      arrayList.add(linkCapacityEstimate);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.downlinkCapacityKbps = paramHwBlob.getInt32(0L + paramLong);
    this.uplinkCapacityKbps = paramHwBlob.getInt32(4L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(8);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<LinkCapacityEstimate> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 8);
    for (byte b = 0; b < i; b++)
      ((LinkCapacityEstimate)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 8)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    paramHwBlob.putInt32(0L + paramLong, this.downlinkCapacityKbps);
    paramHwBlob.putInt32(4L + paramLong, this.uplinkCapacityKbps);
  }
}
