package android.hardware.radio.V1_2;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class CardStatus {
  public android.hardware.radio.V1_0.CardStatus base = new android.hardware.radio.V1_0.CardStatus();
  
  public int physicalSlotId = 0;
  
  public String atr = new String();
  
  public String iccid = new String();
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != CardStatus.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.base, ((CardStatus)paramObject).base))
      return false; 
    if (this.physicalSlotId != ((CardStatus)paramObject).physicalSlotId)
      return false; 
    if (!HidlSupport.deepEquals(this.atr, ((CardStatus)paramObject).atr))
      return false; 
    if (!HidlSupport.deepEquals(this.iccid, ((CardStatus)paramObject).iccid))
      return false; 
    return true;
  }
  
  public final int hashCode() {
    android.hardware.radio.V1_0.CardStatus cardStatus = this.base;
    int i = HidlSupport.deepHashCode(cardStatus), j = this.physicalSlotId;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    String str = this.atr;
    int k = HidlSupport.deepHashCode(str);
    str = this.iccid;
    int m = HidlSupport.deepHashCode(str);
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".base = ");
    stringBuilder.append(this.base);
    stringBuilder.append(", .physicalSlotId = ");
    stringBuilder.append(this.physicalSlotId);
    stringBuilder.append(", .atr = ");
    stringBuilder.append(this.atr);
    stringBuilder.append(", .iccid = ");
    stringBuilder.append(this.iccid);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(80L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<CardStatus> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<CardStatus> arrayList = new ArrayList();
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    int i = hwBlob.getInt32(8L);
    long l1 = (i * 80);
    long l2 = hwBlob.handle();
    hwBlob = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      CardStatus cardStatus = new CardStatus();
      cardStatus.readEmbeddedFromParcel(paramHwParcel, hwBlob, (b * 80));
      arrayList.add(cardStatus);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.base.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, paramLong + 0L);
    this.physicalSlotId = paramHwBlob.getInt32(paramLong + 40L);
    String str = paramHwBlob.getString(paramLong + 48L);
    long l1 = ((str.getBytes()).length + 1);
    long l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 48L + 0L, false);
    this.iccid = str = paramHwBlob.getString(paramLong + 64L);
    l1 = ((str.getBytes()).length + 1);
    l2 = paramHwBlob.handle();
    paramHwParcel.readEmbeddedBuffer(l1, l2, paramLong + 64L + 0L, false);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(80);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<CardStatus> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 80);
    for (byte b = 0; b < i; b++)
      ((CardStatus)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 80)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.base.writeEmbeddedToBlob(paramHwBlob, 0L + paramLong);
    paramHwBlob.putInt32(40L + paramLong, this.physicalSlotId);
    paramHwBlob.putString(48L + paramLong, this.atr);
    paramHwBlob.putString(64L + paramLong, this.iccid);
  }
}
