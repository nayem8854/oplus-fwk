package android.hardware.radio.V1_2;

import android.os.HidlSupport;
import android.os.HwBlob;
import android.os.HwParcel;
import java.util.ArrayList;
import java.util.Objects;

public final class WcdmaSignalStrength {
  public android.hardware.radio.V1_0.WcdmaSignalStrength base = new android.hardware.radio.V1_0.WcdmaSignalStrength();
  
  public int rscp = 0;
  
  public int ecno = 0;
  
  public final boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (paramObject.getClass() != WcdmaSignalStrength.class)
      return false; 
    paramObject = paramObject;
    if (!HidlSupport.deepEquals(this.base, ((WcdmaSignalStrength)paramObject).base))
      return false; 
    if (this.rscp != ((WcdmaSignalStrength)paramObject).rscp)
      return false; 
    if (this.ecno != ((WcdmaSignalStrength)paramObject).ecno)
      return false; 
    return true;
  }
  
  public final int hashCode() {
    android.hardware.radio.V1_0.WcdmaSignalStrength wcdmaSignalStrength = this.base;
    int i = HidlSupport.deepHashCode(wcdmaSignalStrength), j = this.rscp;
    j = HidlSupport.deepHashCode(Integer.valueOf(j));
    int k = this.ecno;
    k = HidlSupport.deepHashCode(Integer.valueOf(k));
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k) });
  }
  
  public final String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(".base = ");
    stringBuilder.append(this.base);
    stringBuilder.append(", .rscp = ");
    stringBuilder.append(this.rscp);
    stringBuilder.append(", .ecno = ");
    stringBuilder.append(this.ecno);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public final void readFromParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = paramHwParcel.readBuffer(16L);
    readEmbeddedFromParcel(paramHwParcel, hwBlob, 0L);
  }
  
  public static final ArrayList<WcdmaSignalStrength> readVectorFromParcel(HwParcel paramHwParcel) {
    ArrayList<WcdmaSignalStrength> arrayList = new ArrayList();
    HwBlob hwBlob1 = paramHwParcel.readBuffer(16L);
    int i = hwBlob1.getInt32(8L);
    long l1 = (i * 16);
    long l2 = hwBlob1.handle();
    HwBlob hwBlob2 = paramHwParcel.readEmbeddedBuffer(l1, l2, 0L, true);
    arrayList.clear();
    for (byte b = 0; b < i; b++) {
      WcdmaSignalStrength wcdmaSignalStrength = new WcdmaSignalStrength();
      wcdmaSignalStrength.readEmbeddedFromParcel(paramHwParcel, hwBlob2, (b * 16));
      arrayList.add(wcdmaSignalStrength);
    } 
    return arrayList;
  }
  
  public final void readEmbeddedFromParcel(HwParcel paramHwParcel, HwBlob paramHwBlob, long paramLong) {
    this.base.readEmbeddedFromParcel(paramHwParcel, paramHwBlob, 0L + paramLong);
    this.rscp = paramHwBlob.getInt32(8L + paramLong);
    this.ecno = paramHwBlob.getInt32(12L + paramLong);
  }
  
  public final void writeToParcel(HwParcel paramHwParcel) {
    HwBlob hwBlob = new HwBlob(16);
    writeEmbeddedToBlob(hwBlob, 0L);
    paramHwParcel.writeBuffer(hwBlob);
  }
  
  public static final void writeVectorToParcel(HwParcel paramHwParcel, ArrayList<WcdmaSignalStrength> paramArrayList) {
    HwBlob hwBlob1 = new HwBlob(16);
    int i = paramArrayList.size();
    hwBlob1.putInt32(8L, i);
    hwBlob1.putBool(12L, false);
    HwBlob hwBlob2 = new HwBlob(i * 16);
    for (byte b = 0; b < i; b++)
      ((WcdmaSignalStrength)paramArrayList.get(b)).writeEmbeddedToBlob(hwBlob2, (b * 16)); 
    hwBlob1.putBlob(0L, hwBlob2);
    paramHwParcel.writeBuffer(hwBlob1);
  }
  
  public final void writeEmbeddedToBlob(HwBlob paramHwBlob, long paramLong) {
    this.base.writeEmbeddedToBlob(paramHwBlob, 0L + paramLong);
    paramHwBlob.putInt32(8L + paramLong, this.rscp);
    paramHwBlob.putInt32(12L + paramLong, this.ecno);
  }
}
