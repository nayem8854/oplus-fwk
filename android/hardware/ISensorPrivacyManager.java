package android.hardware;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISensorPrivacyManager extends IInterface {
  void addSensorPrivacyListener(ISensorPrivacyListener paramISensorPrivacyListener) throws RemoteException;
  
  boolean isSensorPrivacyEnabled() throws RemoteException;
  
  void removeSensorPrivacyListener(ISensorPrivacyListener paramISensorPrivacyListener) throws RemoteException;
  
  void setSensorPrivacy(boolean paramBoolean) throws RemoteException;
  
  class Default implements ISensorPrivacyManager {
    public void addSensorPrivacyListener(ISensorPrivacyListener param1ISensorPrivacyListener) throws RemoteException {}
    
    public void removeSensorPrivacyListener(ISensorPrivacyListener param1ISensorPrivacyListener) throws RemoteException {}
    
    public boolean isSensorPrivacyEnabled() throws RemoteException {
      return false;
    }
    
    public void setSensorPrivacy(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISensorPrivacyManager {
    private static final String DESCRIPTOR = "android.hardware.ISensorPrivacyManager";
    
    static final int TRANSACTION_addSensorPrivacyListener = 1;
    
    static final int TRANSACTION_isSensorPrivacyEnabled = 3;
    
    static final int TRANSACTION_removeSensorPrivacyListener = 2;
    
    static final int TRANSACTION_setSensorPrivacy = 4;
    
    public Stub() {
      attachInterface(this, "android.hardware.ISensorPrivacyManager");
    }
    
    public static ISensorPrivacyManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.ISensorPrivacyManager");
      if (iInterface != null && iInterface instanceof ISensorPrivacyManager)
        return (ISensorPrivacyManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4)
              return null; 
            return "setSensorPrivacy";
          } 
          return "isSensorPrivacyEnabled";
        } 
        return "removeSensorPrivacyListener";
      } 
      return "addSensorPrivacyListener";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            boolean bool1;
            if (param1Int1 != 4) {
              if (param1Int1 != 1598968902)
                return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
              param1Parcel2.writeString("android.hardware.ISensorPrivacyManager");
              return true;
            } 
            param1Parcel1.enforceInterface("android.hardware.ISensorPrivacyManager");
            if (param1Parcel1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            setSensorPrivacy(bool1);
            param1Parcel2.writeNoException();
            return true;
          } 
          param1Parcel1.enforceInterface("android.hardware.ISensorPrivacyManager");
          boolean bool = isSensorPrivacyEnabled();
          param1Parcel2.writeNoException();
          param1Parcel2.writeInt(bool);
          return true;
        } 
        param1Parcel1.enforceInterface("android.hardware.ISensorPrivacyManager");
        iSensorPrivacyListener = ISensorPrivacyListener.Stub.asInterface(param1Parcel1.readStrongBinder());
        removeSensorPrivacyListener(iSensorPrivacyListener);
        param1Parcel2.writeNoException();
        return true;
      } 
      iSensorPrivacyListener.enforceInterface("android.hardware.ISensorPrivacyManager");
      ISensorPrivacyListener iSensorPrivacyListener = ISensorPrivacyListener.Stub.asInterface(iSensorPrivacyListener.readStrongBinder());
      addSensorPrivacyListener(iSensorPrivacyListener);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements ISensorPrivacyManager {
      public static ISensorPrivacyManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.ISensorPrivacyManager";
      }
      
      public void addSensorPrivacyListener(ISensorPrivacyListener param2ISensorPrivacyListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.ISensorPrivacyManager");
          if (param2ISensorPrivacyListener != null) {
            iBinder = param2ISensorPrivacyListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISensorPrivacyManager.Stub.getDefaultImpl() != null) {
            ISensorPrivacyManager.Stub.getDefaultImpl().addSensorPrivacyListener(param2ISensorPrivacyListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeSensorPrivacyListener(ISensorPrivacyListener param2ISensorPrivacyListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.hardware.ISensorPrivacyManager");
          if (param2ISensorPrivacyListener != null) {
            iBinder = param2ISensorPrivacyListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISensorPrivacyManager.Stub.getDefaultImpl() != null) {
            ISensorPrivacyManager.Stub.getDefaultImpl().removeSensorPrivacyListener(param2ISensorPrivacyListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSensorPrivacyEnabled() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.hardware.ISensorPrivacyManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && ISensorPrivacyManager.Stub.getDefaultImpl() != null) {
            bool1 = ISensorPrivacyManager.Stub.getDefaultImpl().isSensorPrivacyEnabled();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSensorPrivacy(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.hardware.ISensorPrivacyManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool1 && ISensorPrivacyManager.Stub.getDefaultImpl() != null) {
            ISensorPrivacyManager.Stub.getDefaultImpl().setSensorPrivacy(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISensorPrivacyManager param1ISensorPrivacyManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISensorPrivacyManager != null) {
          Proxy.sDefaultImpl = param1ISensorPrivacyManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISensorPrivacyManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
