package android.hardware;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.os.MemoryFile;
import android.os.MessageQueue;
import android.os.SystemProperties;
import android.util.Log;
import android.util.SeempLog;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import com.aiunit.aon.AonSmartRotation;
import dalvik.system.CloseGuard;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SystemSensorManager extends SensorManager {
  private static final Object sLock = new Object();
  
  private static boolean sNativeClassInited = false;
  
  static {
    sInjectEventQueue = null;
    mAonSmartRotation = null;
    haveAonSmartRotation = false;
    hardwareValue = 0.0F;
    mLastSmartRotationStatus = 0;
    mCurrentSmartRotationStatus = 0;
  }
  
  private final ArrayList<Sensor> mFullSensorsList = new ArrayList<>();
  
  private List<Sensor> mFullDynamicSensorsList = new ArrayList<>();
  
  private boolean mDynamicSensorListDirty = true;
  
  private final HashMap<Integer, Sensor> mHandleToSensor = new HashMap<>();
  
  private final HashMap<SensorEventListener, SensorEventQueue> mSensorListeners = new HashMap<>();
  
  private final HashMap<TriggerEventListener, TriggerEventQueue> mTriggerListeners = new HashMap<>();
  
  private HashMap<SensorManager.DynamicSensorCallback, Handler> mDynamicSensorCallbacks = new HashMap<>();
  
  private static final boolean DEBUG_DYNAMIC_SENSOR = true;
  
  private static final int MAX_LISTENER_COUNT = 128;
  
  private static final int MIN_DIRECT_CHANNEL_BUFFER_SIZE = 104;
  
  private static float hardwareValue;
  
  private static boolean haveAonSmartRotation;
  
  private static AonSmartRotation mAonSmartRotation;
  
  private static int mCurrentSmartRotationStatus;
  
  private static int mLastSmartRotationStatus;
  
  private static InjectEventQueue sInjectEventQueue;
  
  private final Context mContext;
  
  private BroadcastReceiver mDynamicSensorBroadcastReceiver;
  
  private final Looper mMainLooper;
  
  private final long mNativeInstance;
  
  private final int mTargetSdkLevel;
  
  public SystemSensorManager(Context paramContext, Looper paramLooper) {
    synchronized (sLock) {
      if (!sNativeClassInited) {
        sNativeClassInited = true;
        nativeClassInit();
      } 
      this.mMainLooper = paramLooper;
      this.mTargetSdkLevel = (paramContext.getApplicationInfo()).targetSdkVersion;
      this.mContext = paramContext;
      this.mNativeInstance = nativeCreate(paramContext.getOpPackageName());
      for (byte b = 0;; b++) {
        Sensor sensor = new Sensor();
        if (!nativeGetSensorAtIndex(this.mNativeInstance, sensor, b))
          return; 
        this.mFullSensorsList.add(sensor);
        this.mHandleToSensor.put(Integer.valueOf(sensor.getHandle()), sensor);
      } 
    } 
  }
  
  protected List<Sensor> getFullSensorList() {
    return this.mFullSensorsList;
  }
  
  protected List<Sensor> getFullDynamicSensorList() {
    setupDynamicSensorBroadcastReceiver();
    updateDynamicSensorList();
    return this.mFullDynamicSensorsList;
  }
  
  protected boolean registerListenerImpl(SensorEventListener paramSensorEventListener, Sensor paramSensor, int paramInt1, Handler paramHandler, int paramInt2, int paramInt3) {
    SeempLog.record_sensor_rate(381, paramSensor, paramInt1);
    if (paramSensorEventListener == null || paramSensor == null) {
      Log.e("SensorManager", "sensor or listener is null");
      return false;
    } 
    if (paramSensor.getReportingMode() == 2) {
      Log.e("SensorManager", "Trigger Sensors should use the requestTriggerSensor.");
      return false;
    } 
    if (paramInt2 < 0 || paramInt1 < 0) {
      Log.e("SensorManager", "maxBatchReportLatencyUs and delayUs should be non-negative");
      return false;
    } 
    if (paramSensorEventListener.getClass().getName() != null && paramSensorEventListener.getClass().getName().indexOf("com.tencent.beacon") != -1) {
      Log.w("SensorManager", "Block tencent beacon for using sensor.");
      return false;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("RegisterListener ");
    stringBuilder.append(paramSensor.getName());
    stringBuilder.append(" type:");
    stringBuilder.append(paramSensor.getType());
    stringBuilder.append(" delay:");
    stringBuilder.append(paramInt1);
    stringBuilder.append("us by ");
    stringBuilder.append(paramSensorEventListener.getClass().getName());
    String str = stringBuilder.toString();
    Log.v("SensorManager", str);
    if (paramSensor != null)
      try {
        if (paramSensor.getType() == 27 && mAonSmartRotation == null) {
          boolean bool = SystemProperties.get("persist.sys.oplus.smartrotation", "false").equals("true");
          if (bool) {
            AonSmartRotation aonSmartRotation = new AonSmartRotation();
            this(this.mContext);
            mAonSmartRotation = aonSmartRotation;
            StringBuilder stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append("SmartRotationDebug, Init AonSmartRotation by ");
            stringBuilder1.append(paramSensorEventListener.getClass().getName());
            Log.d("SensorManager", stringBuilder1.toString());
          } 
        } 
      } catch (Exception exception) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("SmartRotation got exception, e = ");
        stringBuilder1.append(exception.toString());
        Log.e("SensorManager", stringBuilder1.toString());
      }  
    if (this.mSensorListeners.size() < 128)
      synchronized (this.mSensorListeners) {
        String str1;
        SensorEventQueue sensorEventQueue = this.mSensorListeners.get(paramSensorEventListener);
        if (sensorEventQueue == null) {
          Looper looper;
          if (paramHandler != null) {
            looper = paramHandler.getLooper();
          } else {
            looper = this.mMainLooper;
          } 
          if (paramSensorEventListener.getClass().getEnclosingClass() != null) {
            str1 = paramSensorEventListener.getClass().getEnclosingClass().getName();
          } else {
            str1 = paramSensorEventListener.getClass().getName();
          } 
          SensorEventQueue sensorEventQueue1 = new SensorEventQueue();
          this(paramSensorEventListener, looper, this, str1);
          if (!sensorEventQueue1.addSensor(paramSensor, paramInt1, paramInt2)) {
            sensorEventQueue1.dispose();
            return false;
          } 
          this.mSensorListeners.put(paramSensorEventListener, sensorEventQueue1);
          return true;
        } 
        return str1.addSensor(paramSensor, paramInt1, paramInt2);
      }  
    throw new IllegalStateException("register failed, the sensor listeners size has exceeded the maximum limit 128");
  }
  
  protected void unregisterListenerImpl(SensorEventListener paramSensorEventListener, Sensor paramSensor) {
    SeempLog.record_sensor(382, paramSensor);
    if (paramSensorEventListener != null)
      if (paramSensor != null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("unRegisterListener by ");
        stringBuilder.append(paramSensorEventListener.getClass().getName());
        stringBuilder.append(" and its name is ");
        stringBuilder.append(paramSensor.getName());
        Log.v("SensorManager", stringBuilder.toString());
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("unRegisterListener by ");
        stringBuilder.append(paramSensorEventListener.getClass().getName());
        Log.v("SensorManager", stringBuilder.toString());
      }  
    if (paramSensor != null)
      try {
        if (paramSensor.getType() == 27 && mAonSmartRotation != null) {
          mAonSmartRotation = null;
          if (paramSensorEventListener != null) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("SmartRotationDebug, Destroy AonSmartRotation by ");
            stringBuilder.append(paramSensorEventListener.getClass().getName());
            Log.d("SensorManager", stringBuilder.toString());
          } 
        } 
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SmartRotation got exception, e = ");
        stringBuilder.append(exception.toString());
        Log.e("SensorManager", stringBuilder.toString());
      }  
    if (paramSensor != null && paramSensor.getReportingMode() == 2)
      return; 
    synchronized (this.mSensorListeners) {
      SensorEventQueue sensorEventQueue = this.mSensorListeners.get(paramSensorEventListener);
      if (sensorEventQueue != null) {
        boolean bool;
        if (paramSensor == null) {
          bool = sensorEventQueue.removeAllSensors();
        } else {
          bool = sensorEventQueue.removeSensor(paramSensor, true);
        } 
        if (bool && !sensorEventQueue.hasSensors()) {
          this.mSensorListeners.remove(paramSensorEventListener);
          sensorEventQueue.dispose();
        } 
      } 
      return;
    } 
  }
  
  protected boolean requestTriggerSensorImpl(TriggerEventListener paramTriggerEventListener, Sensor paramSensor) {
    if (paramSensor != null) {
      if (paramTriggerEventListener != null) {
        if (paramSensor.getReportingMode() != 2)
          return false; 
        if (this.mTriggerListeners.size() < 128)
          synchronized (this.mTriggerListeners) {
            String str;
            TriggerEventQueue triggerEventQueue = this.mTriggerListeners.get(paramTriggerEventListener);
            if (triggerEventQueue == null) {
              if (paramTriggerEventListener.getClass().getEnclosingClass() != null) {
                str = paramTriggerEventListener.getClass().getEnclosingClass().getName();
              } else {
                str = paramTriggerEventListener.getClass().getName();
              } 
              TriggerEventQueue triggerEventQueue1 = new TriggerEventQueue();
              this(paramTriggerEventListener, this.mMainLooper, this, str);
              if (!triggerEventQueue1.addSensor(paramSensor, 0, 0)) {
                triggerEventQueue1.dispose();
                return false;
              } 
              this.mTriggerListeners.put(paramTriggerEventListener, triggerEventQueue1);
              return true;
            } 
            return str.addSensor(paramSensor, 0, 0);
          }  
        throw new IllegalStateException("request failed, the trigger listeners size has exceeded the maximum limit 128");
      } 
      throw new IllegalArgumentException("listener cannot be null");
    } 
    throw new IllegalArgumentException("sensor cannot be null");
  }
  
  protected boolean cancelTriggerSensorImpl(TriggerEventListener paramTriggerEventListener, Sensor paramSensor, boolean paramBoolean) {
    if (paramSensor != null && paramSensor.getReportingMode() != 2)
      return false; 
    synchronized (this.mTriggerListeners) {
      TriggerEventQueue triggerEventQueue = this.mTriggerListeners.get(paramTriggerEventListener);
      if (triggerEventQueue != null) {
        if (paramSensor == null) {
          paramBoolean = triggerEventQueue.removeAllSensors();
        } else {
          paramBoolean = triggerEventQueue.removeSensor(paramSensor, paramBoolean);
        } 
        if (paramBoolean && !triggerEventQueue.hasSensors()) {
          this.mTriggerListeners.remove(paramTriggerEventListener);
          triggerEventQueue.dispose();
        } 
        return paramBoolean;
      } 
      return false;
    } 
  }
  
  protected boolean flushImpl(SensorEventListener paramSensorEventListener) {
    if (paramSensorEventListener != null)
      synchronized (this.mSensorListeners) {
        SensorEventQueue sensorEventQueue = this.mSensorListeners.get(paramSensorEventListener);
        boolean bool = false;
        if (sensorEventQueue == null)
          return false; 
        if (sensorEventQueue.flush() == 0)
          bool = true; 
        return bool;
      }  
    throw new IllegalArgumentException("listener cannot be null");
  }
  
  protected boolean initDataInjectionImpl(boolean paramBoolean) {
    // Byte code:
    //   0: getstatic android/hardware/SystemSensorManager.sLock : Ljava/lang/Object;
    //   3: astore_2
    //   4: aload_2
    //   5: monitorenter
    //   6: iconst_1
    //   7: istore_3
    //   8: iload_1
    //   9: ifeq -> 143
    //   12: aload_0
    //   13: getfield mNativeInstance : J
    //   16: invokestatic nativeIsDataInjectionEnabled : (J)Z
    //   19: istore_1
    //   20: iload_1
    //   21: ifne -> 37
    //   24: ldc 'SensorManager'
    //   26: ldc_w 'Data Injection mode not enabled'
    //   29: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   32: pop
    //   33: aload_2
    //   34: monitorexit
    //   35: iconst_0
    //   36: ireturn
    //   37: getstatic android/hardware/SystemSensorManager.sInjectEventQueue : Landroid/hardware/SystemSensorManager$InjectEventQueue;
    //   40: astore #4
    //   42: aload #4
    //   44: ifnonnull -> 126
    //   47: new android/hardware/SystemSensorManager$InjectEventQueue
    //   50: astore #5
    //   52: aload_0
    //   53: getfield mMainLooper : Landroid/os/Looper;
    //   56: astore #4
    //   58: aload_0
    //   59: getfield mContext : Landroid/content/Context;
    //   62: astore #6
    //   64: aload #5
    //   66: aload_0
    //   67: aload #4
    //   69: aload_0
    //   70: aload #6
    //   72: invokevirtual getPackageName : ()Ljava/lang/String;
    //   75: invokespecial <init> : (Landroid/hardware/SystemSensorManager;Landroid/os/Looper;Landroid/hardware/SystemSensorManager;Ljava/lang/String;)V
    //   78: aload #5
    //   80: putstatic android/hardware/SystemSensorManager.sInjectEventQueue : Landroid/hardware/SystemSensorManager$InjectEventQueue;
    //   83: goto -> 126
    //   86: astore #4
    //   88: new java/lang/StringBuilder
    //   91: astore #6
    //   93: aload #6
    //   95: invokespecial <init> : ()V
    //   98: aload #6
    //   100: ldc_w 'Cannot create InjectEventQueue: '
    //   103: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   106: pop
    //   107: aload #6
    //   109: aload #4
    //   111: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   114: pop
    //   115: ldc 'SensorManager'
    //   117: aload #6
    //   119: invokevirtual toString : ()Ljava/lang/String;
    //   122: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   125: pop
    //   126: getstatic android/hardware/SystemSensorManager.sInjectEventQueue : Landroid/hardware/SystemSensorManager$InjectEventQueue;
    //   129: ifnull -> 137
    //   132: iload_3
    //   133: istore_1
    //   134: goto -> 139
    //   137: iconst_0
    //   138: istore_1
    //   139: aload_2
    //   140: monitorexit
    //   141: iload_1
    //   142: ireturn
    //   143: getstatic android/hardware/SystemSensorManager.sInjectEventQueue : Landroid/hardware/SystemSensorManager$InjectEventQueue;
    //   146: ifnull -> 159
    //   149: getstatic android/hardware/SystemSensorManager.sInjectEventQueue : Landroid/hardware/SystemSensorManager$InjectEventQueue;
    //   152: invokevirtual dispose : ()V
    //   155: aconst_null
    //   156: putstatic android/hardware/SystemSensorManager.sInjectEventQueue : Landroid/hardware/SystemSensorManager$InjectEventQueue;
    //   159: aload_2
    //   160: monitorexit
    //   161: iconst_1
    //   162: ireturn
    //   163: astore #4
    //   165: aload_2
    //   166: monitorexit
    //   167: aload #4
    //   169: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #357	-> 0
    //   #358	-> 6
    //   #359	-> 12
    //   #361	-> 20
    //   #362	-> 24
    //   #363	-> 33
    //   #366	-> 37
    //   #368	-> 47
    //   #369	-> 64
    //   #372	-> 83
    //   #370	-> 86
    //   #371	-> 88
    //   #374	-> 126
    //   #377	-> 143
    //   #378	-> 149
    //   #379	-> 155
    //   #381	-> 159
    //   #383	-> 163
    // Exception table:
    //   from	to	target	type
    //   12	20	163	finally
    //   24	33	163	finally
    //   33	35	163	finally
    //   37	42	163	finally
    //   47	64	86	java/lang/RuntimeException
    //   47	64	163	finally
    //   64	83	86	java/lang/RuntimeException
    //   64	83	163	finally
    //   88	126	163	finally
    //   126	132	163	finally
    //   139	141	163	finally
    //   143	149	163	finally
    //   149	155	163	finally
    //   155	159	163	finally
    //   159	161	163	finally
    //   165	167	163	finally
  }
  
  protected boolean injectSensorDataImpl(Sensor paramSensor, float[] paramArrayOffloat, int paramInt, long paramLong) {
    synchronized (sLock) {
      InjectEventQueue injectEventQueue = sInjectEventQueue;
      boolean bool = false;
      if (injectEventQueue == null) {
        Log.e("SensorManager", "Data injection mode not activated before calling injectSensorData");
        return false;
      } 
      paramInt = sInjectEventQueue.injectSensorData(paramSensor.getHandle(), paramArrayOffloat, paramInt, paramLong);
      if (paramInt != 0) {
        sInjectEventQueue.dispose();
        sInjectEventQueue = null;
      } 
      if (paramInt == 0)
        bool = true; 
      return bool;
    } 
  }
  
  private void cleanupSensorConnection(Sensor paramSensor) {
    this.mHandleToSensor.remove(Integer.valueOf(paramSensor.getHandle()));
    if (paramSensor.getReportingMode() == 2) {
      synchronized (this.mTriggerListeners) {
        HashMap<Object, Object> hashMap = new HashMap<>();
        this((Map)this.mTriggerListeners);
        for (TriggerEventListener triggerEventListener : hashMap.keySet()) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("removed trigger listener");
          stringBuilder.append(triggerEventListener.toString());
          stringBuilder.append(" due to sensor disconnection");
          Log.i("SensorManager", stringBuilder.toString());
          cancelTriggerSensorImpl(triggerEventListener, paramSensor, true);
        } 
      } 
    } else {
      synchronized (this.mSensorListeners) {
        HashMap<Object, Object> hashMap = new HashMap<>();
        this((Map)this.mSensorListeners);
        for (SensorEventListener sensorEventListener : hashMap.keySet()) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("removed event listener");
          stringBuilder.append(sensorEventListener.toString());
          stringBuilder.append(" due to sensor disconnection");
          Log.i("SensorManager", stringBuilder.toString());
          unregisterListenerImpl(sensorEventListener, paramSensor);
        } 
        return;
      } 
    } 
  }
  
  private void updateDynamicSensorList() {
    synchronized (this.mFullDynamicSensorsList) {
      if (this.mDynamicSensorListDirty) {
        ArrayList<Sensor> arrayList1 = new ArrayList();
        this();
        nativeGetDynamicSensors(this.mNativeInstance, arrayList1);
        ArrayList<Sensor> arrayList2 = new ArrayList();
        this();
        ArrayList<Sensor> arrayList3 = new ArrayList();
        this();
        ArrayList<Sensor> arrayList4 = new ArrayList();
        this();
        boolean bool = diffSortedSensorList(this.mFullDynamicSensorsList, arrayList1, arrayList2, arrayList3, arrayList4);
        if (bool) {
          Log.i("SensorManager", "DYNS dynamic sensor list cached should be updated");
          this.mFullDynamicSensorsList = arrayList2;
          for (Sensor sensor : arrayList3)
            this.mHandleToSensor.put(Integer.valueOf(sensor.getHandle()), sensor); 
          Handler handler = new Handler();
          this(this.mContext.getMainLooper());
          for (Map.Entry<SensorManager.DynamicSensorCallback, Handler> entry : this.mDynamicSensorCallbacks.entrySet()) {
            Handler handler1;
            SensorManager.DynamicSensorCallback dynamicSensorCallback = (SensorManager.DynamicSensorCallback)entry.getKey();
            if (entry.getValue() == null) {
              handler1 = handler;
            } else {
              handler1 = (Handler)handler1.getValue();
            } 
            Object object = new Object();
            super(this, arrayList3, dynamicSensorCallback, arrayList4);
            handler1.post((Runnable)object);
          } 
          for (Sensor sensor : arrayList4)
            cleanupSensorConnection(sensor); 
        } 
        this.mDynamicSensorListDirty = false;
      } 
      return;
    } 
  }
  
  private void setupDynamicSensorBroadcastReceiver() {
    if (this.mDynamicSensorBroadcastReceiver == null) {
      this.mDynamicSensorBroadcastReceiver = new BroadcastReceiver() {
          final SystemSensorManager this$0;
          
          public void onReceive(Context param1Context, Intent param1Intent) {
            if (param1Intent.getAction() == "android.intent.action.DYNAMIC_SENSOR_CHANGED") {
              Log.i("SensorManager", "DYNS received DYNAMIC_SENSOR_CHANED broadcast");
              SystemSensorManager.access$002(SystemSensorManager.this, true);
              SystemSensorManager.this.updateDynamicSensorList();
            } 
          }
        };
      IntentFilter intentFilter = new IntentFilter("dynamic_sensor_change");
      intentFilter.addAction("android.intent.action.DYNAMIC_SENSOR_CHANGED");
      this.mContext.registerReceiver(this.mDynamicSensorBroadcastReceiver, intentFilter);
    } 
  }
  
  private void teardownDynamicSensorBroadcastReceiver() {
    this.mDynamicSensorCallbacks.clear();
    this.mContext.unregisterReceiver(this.mDynamicSensorBroadcastReceiver);
    this.mDynamicSensorBroadcastReceiver = null;
  }
  
  protected void registerDynamicSensorCallbackImpl(SensorManager.DynamicSensorCallback paramDynamicSensorCallback, Handler paramHandler) {
    Log.i("SensorManager", "DYNS Register dynamic sensor callback");
    if (paramDynamicSensorCallback != null) {
      if (this.mDynamicSensorCallbacks.containsKey(paramDynamicSensorCallback))
        return; 
      setupDynamicSensorBroadcastReceiver();
      this.mDynamicSensorCallbacks.put(paramDynamicSensorCallback, paramHandler);
      return;
    } 
    throw new IllegalArgumentException("callback cannot be null");
  }
  
  protected void unregisterDynamicSensorCallbackImpl(SensorManager.DynamicSensorCallback paramDynamicSensorCallback) {
    Log.i("SensorManager", "Removing dynamic sensor listerner");
    this.mDynamicSensorCallbacks.remove(paramDynamicSensorCallback);
  }
  
  private static boolean diffSortedSensorList(List<Sensor> paramList1, List<Sensor> paramList2, List<Sensor> paramList3, List<Sensor> paramList4, List<Sensor> paramList5) {
    boolean bool = false;
    byte b1 = 0, b2 = 0;
    while (true) {
      if (b2 < paramList1.size() && (b1 >= paramList2.size() || (
        (Sensor)paramList2.get(b1)).getHandle() > ((Sensor)paramList1.get(b2)).getHandle())) {
        bool = true;
        if (paramList5 != null)
          paramList5.add(paramList1.get(b2)); 
        b2++;
        continue;
      } 
      if (b1 < paramList2.size() && (b2 >= paramList1.size() || (
        (Sensor)paramList2.get(b1)).getHandle() < ((Sensor)paramList1.get(b2)).getHandle())) {
        bool = true;
        if (paramList4 != null)
          paramList4.add(paramList2.get(b1)); 
        if (paramList3 != null)
          paramList3.add(paramList2.get(b1)); 
        b1++;
        continue;
      } 
      if (b1 < paramList2.size() && b2 < paramList1.size() && (
        (Sensor)paramList2.get(b1)).getHandle() == ((Sensor)paramList1.get(b2)).getHandle()) {
        if (paramList3 != null)
          paramList3.add(paramList1.get(b2)); 
        b1++;
        b2++;
        continue;
      } 
      break;
    } 
    return bool;
  }
  
  protected int configureDirectChannelImpl(SensorDirectChannel paramSensorDirectChannel, Sensor paramSensor, int paramInt) {
    if (paramSensorDirectChannel.isOpen()) {
      if (paramInt >= 0 && paramInt <= 3) {
        if (paramSensor != null || paramInt == 0) {
          if (paramSensor == null) {
            i = -1;
          } else {
            i = paramSensor.getHandle();
          } 
          long l = this.mNativeInstance;
          int j = paramSensorDirectChannel.getNativeHandle();
          int i = nativeConfigDirectChannel(l, j, i, paramInt);
          j = 0;
          boolean bool = false;
          if (paramInt == 0) {
            paramInt = bool;
            if (i == 0)
              paramInt = 1; 
            return paramInt;
          } 
          paramInt = j;
          if (i > 0)
            paramInt = i; 
          return paramInt;
        } 
        throw new IllegalArgumentException("when sensor is null, rate can only be DIRECT_RATE_STOP");
      } 
      throw new IllegalArgumentException("rate parameter invalid");
    } 
    throw new IllegalStateException("channel is closed");
  }
  
  protected SensorDirectChannel createDirectChannelImpl(MemoryFile paramMemoryFile, HardwareBuffer paramHardwareBuffer) {
    int i;
    long l;
    byte b;
    if (paramMemoryFile != null) {
      try {
        i = paramMemoryFile.getFileDescriptor().getInt$();
        if (paramMemoryFile.length() >= 104) {
          l = paramMemoryFile.length();
          i = nativeCreateDirectChannel(this.mNativeInstance, l, 1, i, null);
          if (i > 0) {
            b = 1;
          } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("create MemoryFile direct channel failed ");
            stringBuilder.append(i);
            throw new UncheckedIOException(new IOException(stringBuilder.toString()));
          } 
        } else {
          throw new IllegalArgumentException("Size of MemoryFile has to be greater than 104");
        } 
      } catch (IOException iOException) {
        throw new IllegalArgumentException("MemoryFile object is not valid");
      } 
    } else {
      if (paramHardwareBuffer != null) {
        if (paramHardwareBuffer.getFormat() == 33) {
          if (paramHardwareBuffer.getHeight() == 1) {
            if (paramHardwareBuffer.getWidth() >= 104) {
              if ((paramHardwareBuffer.getUsage() & 0x800000L) != 0L) {
                l = paramHardwareBuffer.getWidth();
                i = nativeCreateDirectChannel(this.mNativeInstance, l, 2, -1, paramHardwareBuffer);
                if (i > 0) {
                  b = 2;
                  return new SensorDirectChannel(this, i, b, l);
                } 
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("create HardwareBuffer direct channel failed ");
                stringBuilder.append(i);
                throw new UncheckedIOException(new IOException(stringBuilder.toString()));
              } 
              throw new IllegalArgumentException("HardwareBuffer must set usage flag USAGE_SENSOR_DIRECT_DATA");
            } 
            throw new IllegalArgumentException("Width if HaradwareBuffer must be greater than 104");
          } 
          throw new IllegalArgumentException("Height of HardwareBuffer must be 1");
        } 
        throw new IllegalArgumentException("Format of HardwareBuffer must be BLOB");
      } 
      throw new NullPointerException("shared memory object cannot be null");
    } 
    return new SensorDirectChannel(this, i, b, l);
  }
  
  protected void destroyDirectChannelImpl(SensorDirectChannel paramSensorDirectChannel) {
    if (paramSensorDirectChannel != null)
      nativeDestroyDirectChannel(this.mNativeInstance, paramSensorDirectChannel.getNativeHandle()); 
  }
  
  class BaseEventQueue {
    private final SparseBooleanArray mActiveSensors = new SparseBooleanArray();
    
    protected final SparseIntArray mSensorAccuracies = new SparseIntArray();
    
    private final CloseGuard mCloseGuard = CloseGuard.get();
    
    protected static final int OPERATING_MODE_DATA_INJECTION = 1;
    
    protected static final int OPERATING_MODE_NORMAL = 0;
    
    protected final SystemSensorManager mManager;
    
    private long mNativeSensorEventQueue;
    
    BaseEventQueue(SystemSensorManager this$0, SystemSensorManager param1SystemSensorManager, int param1Int, String param1String) {
      String str1 = param1String;
      if (param1String == null)
        str1 = ""; 
      long l = param1SystemSensorManager.mNativeInstance;
      WeakReference<BaseEventQueue> weakReference = new WeakReference<>(this);
      MessageQueue messageQueue = this$0.getQueue();
      String str2 = param1SystemSensorManager.mContext.getOpPackageName();
      this.mNativeSensorEventQueue = nativeInitBaseEventQueue(l, weakReference, messageQueue, str1, param1Int, str2);
      this.mCloseGuard.open("dispose");
      this.mManager = param1SystemSensorManager;
    }
    
    public void dispose() {
      dispose(false);
    }
    
    public boolean addSensor(Sensor param1Sensor, int param1Int1, int param1Int2) {
      int i = param1Sensor.getHandle();
      if (param1Sensor.getType() == 2 || 
        param1Sensor.getType() == 3)
        this.mSensorAccuracies.put(param1Sensor.getHandle(), -1); 
      if (this.mActiveSensors.get(i))
        return false; 
      this.mActiveSensors.put(i, true);
      addSensorEvent(param1Sensor);
      if (enableSensor(param1Sensor, param1Int1, param1Int2) != 0)
        if (param1Int2 == 0 || (param1Int2 > 0 && 
          enableSensor(param1Sensor, param1Int1, 0) != 0)) {
          removeSensor(param1Sensor, false);
          return false;
        }  
      return true;
    }
    
    public boolean removeAllSensors() {
      for (byte b = 0; b < this.mActiveSensors.size(); b++) {
        if (this.mActiveSensors.valueAt(b) == true) {
          int i = this.mActiveSensors.keyAt(b);
          Sensor sensor = (Sensor)this.mManager.mHandleToSensor.get(Integer.valueOf(i));
          if (sensor != null) {
            disableSensor(sensor);
            this.mActiveSensors.put(i, false);
            removeSensorEvent(sensor);
          } 
        } 
      } 
      return true;
    }
    
    public boolean removeSensor(Sensor param1Sensor, boolean param1Boolean) {
      int i = param1Sensor.getHandle();
      if (this.mActiveSensors.get(i)) {
        if (param1Boolean)
          disableSensor(param1Sensor); 
        this.mActiveSensors.put(param1Sensor.getHandle(), false);
        removeSensorEvent(param1Sensor);
        return true;
      } 
      return false;
    }
    
    public int flush() {
      long l = this.mNativeSensorEventQueue;
      if (l != 0L)
        return nativeFlushSensor(l); 
      throw null;
    }
    
    public boolean hasSensors() {
      SparseBooleanArray sparseBooleanArray = this.mActiveSensors;
      boolean bool = true;
      if (sparseBooleanArray.indexOfValue(true) < 0)
        bool = false; 
      return bool;
    }
    
    protected void finalize() throws Throwable {
      try {
        dispose(true);
        return;
      } finally {
        super.finalize();
      } 
    }
    
    private void dispose(boolean param1Boolean) {
      CloseGuard closeGuard = this.mCloseGuard;
      if (closeGuard != null) {
        if (param1Boolean)
          closeGuard.warnIfOpen(); 
        this.mCloseGuard.close();
      } 
      long l = this.mNativeSensorEventQueue;
      if (l != 0L) {
        nativeDestroySensorEventQueue(l);
        this.mNativeSensorEventQueue = 0L;
      } 
    }
    
    private int enableSensor(Sensor param1Sensor, int param1Int1, int param1Int2) {
      long l = this.mNativeSensorEventQueue;
      if (l != 0L) {
        if (param1Sensor != null)
          return nativeEnableSensor(l, param1Sensor.getHandle(), param1Int1, param1Int2); 
        throw null;
      } 
      throw null;
    }
    
    protected int injectSensorDataBase(int param1Int1, float[] param1ArrayOffloat, int param1Int2, long param1Long) {
      return nativeInjectSensorData(this.mNativeSensorEventQueue, param1Int1, param1ArrayOffloat, param1Int2, param1Long);
    }
    
    private int disableSensor(Sensor param1Sensor) {
      long l = this.mNativeSensorEventQueue;
      if (l != 0L) {
        if (param1Sensor != null)
          return nativeDisableSensor(l, param1Sensor.getHandle()); 
        throw null;
      } 
      throw null;
    }
    
    protected void dispatchAdditionalInfoEvent(int param1Int1, int param1Int2, int param1Int3, float[] param1ArrayOffloat, int[] param1ArrayOfint) {}
    
    private static native void nativeDestroySensorEventQueue(long param1Long);
    
    private static native int nativeDisableSensor(long param1Long, int param1Int);
    
    private static native int nativeEnableSensor(long param1Long, int param1Int1, int param1Int2, int param1Int3);
    
    private static native int nativeFlushSensor(long param1Long);
    
    private static native long nativeInitBaseEventQueue(long param1Long, WeakReference<BaseEventQueue> param1WeakReference, MessageQueue param1MessageQueue, String param1String1, int param1Int, String param1String2);
    
    private static native int nativeInjectSensorData(long param1Long1, int param1Int1, float[] param1ArrayOffloat, int param1Int2, long param1Long2);
    
    protected abstract void addSensorEvent(Sensor param1Sensor);
    
    protected abstract void dispatchFlushCompleteEvent(int param1Int);
    
    protected abstract void dispatchSensorEvent(int param1Int1, float[] param1ArrayOffloat, int param1Int2, long param1Long);
    
    protected abstract void removeSensorEvent(Sensor param1Sensor);
  }
  
  static final class SensorEventQueue extends BaseEventQueue {
    private final SparseArray<SensorEvent> mSensorsEvents = new SparseArray();
    
    float mLastStep = 0.0F;
    
    float mPedoLastStep = 0.0F;
    
    int MAX_STEP_INTERVAL = 100;
    
    private final SensorEventListener mListener;
    
    public SensorEventQueue(SensorEventListener param1SensorEventListener, Looper param1Looper, SystemSensorManager param1SystemSensorManager, String param1String) {
      super(param1Looper, param1SystemSensorManager, 0, param1String);
      this.mListener = param1SensorEventListener;
    }
    
    public void addSensorEvent(Sensor param1Sensor) {
      SystemSensorManager systemSensorManager = this.mManager;
      int i = systemSensorManager.mTargetSdkLevel;
      SensorEvent sensorEvent = new SensorEvent(Sensor.getMaxLengthValuesArray(param1Sensor, i));
      synchronized (this.mSensorsEvents) {
        this.mSensorsEvents.put(param1Sensor.getHandle(), sensorEvent);
        return;
      } 
    }
    
    public void removeSensorEvent(Sensor param1Sensor) {
      synchronized (this.mSensorsEvents) {
        this.mSensorsEvents.delete(param1Sensor.getHandle());
        return;
      } 
    }
    
    protected void dispatchSensorEvent(int param1Int1, float[] param1ArrayOffloat, int param1Int2, long param1Long) {
      SparseArray<SensorEvent> sparseArray;
      Sensor sensor = (Sensor)this.mManager.mHandleToSensor.get(Integer.valueOf(param1Int1));
      if (sensor == null)
        return; 
      synchronized (this.mSensorsEvents) {
        SensorEvent sensorEvent = (SensorEvent)this.mSensorsEvents.get(param1Int1);
        if (sensorEvent == null)
          return; 
        System.arraycopy(param1ArrayOffloat, 0, sensorEvent.values, 0, sensorEvent.values.length);
        sensorEvent.timestamp = param1Long;
        sensorEvent.accuracy = param1Int2;
        sensorEvent.sensor = sensor;
        param1Int2 = this.mSensorAccuracies.get(param1Int1);
        if (sensorEvent.accuracy >= 0 && param1Int2 != sensorEvent.accuracy) {
          this.mSensorAccuracies.put(param1Int1, sensorEvent.accuracy);
          this.mListener.onAccuracyChanged(sensorEvent.sensor, sensorEvent.accuracy);
        } 
        try {
          if (sensor.getType() == 19) {
            if (this.mLastStep == 0.0F || Math.abs(param1ArrayOffloat[0] - this.mLastStep) >= this.MAX_STEP_INTERVAL) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("step counter dispatchSensorEvent step ");
              stringBuilder.append(param1ArrayOffloat[0]);
              stringBuilder.append(" to + ");
              stringBuilder.append(this.mListener);
              Log.v("SensorManager", stringBuilder.toString());
              this.mLastStep = param1ArrayOffloat[0];
            } 
          } else if (sensor.getType() == 33171034 && (
            this.mPedoLastStep == 0.0F || Math.abs(param1ArrayOffloat[0] - this.mPedoLastStep) >= this.MAX_STEP_INTERVAL)) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Pedometer minute step counter dispatchSensorEvent step ");
            stringBuilder.append(param1ArrayOffloat[0]);
            stringBuilder.append(" to + ");
            stringBuilder.append(this.mListener);
            Log.v("SensorManager", stringBuilder.toString());
            this.mPedoLastStep = param1ArrayOffloat[0];
          } 
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("step counter error e = ");
          stringBuilder.append(exception.toString());
          Log.e("SensorManager", stringBuilder.toString());
        } 
        if (SensorManager.mInMirage)
          return; 
        if (sensor != null)
          try {
            if (sensor.getType() == 27) {
              if (SystemSensorManager.haveAonSmartRotation && SystemSensorManager
                .mAonSmartRotation != null) {
                SystemSensorManager.access$802(SystemSensorManager.mAonSmartRotation.getStatus());
                if (SystemSensorManager.mCurrentSmartRotationStatus != SystemSensorManager.mLastSmartRotationStatus)
                  if (SystemSensorManager.mCurrentSmartRotationStatus == 1) {
                    Log.d("SensorManager", "SmartRotationDebug, SmartRotation switch On.");
                    SystemSensorManager.mAonSmartRotation.createSmartRotationConnection();
                  } else if (SystemSensorManager.mCurrentSmartRotationStatus == 0) {
                    Log.d("SensorManager", "SmartRotationDebug, SmartRotation switch Off.");
                    SystemSensorManager.mAonSmartRotation.destroySmartRotationConnection();
                  } else {
                    Log.w("SensorManager", "SmartRotationDebug, SmartRotation switch Invalid, reset Status value.");
                    SystemSensorManager.access$802(0);
                    SystemSensorManager.access$902(0);
                  }  
                SystemSensorManager.access$902(SystemSensorManager.mCurrentSmartRotationStatus);
                SystemSensorManager.access$1002(param1ArrayOffloat[0]);
                param1ArrayOffloat[0] = SystemSensorManager.mAonSmartRotation.makeDecisionBySmartRotation(param1ArrayOffloat);
                if (SystemSensorManager.hardwareValue != param1ArrayOffloat[0]) {
                  sensorEvent.values[0] = param1ArrayOffloat[0];
                  StringBuilder stringBuilder = new StringBuilder();
                  this();
                  stringBuilder.append("SmartRotationDebug, Device Orientation changed by SmartRotation, t.value[0] is ");
                  stringBuilder.append(sensorEvent.values[0]);
                  stringBuilder.append(", and Hardware Value is ");
                  stringBuilder.append(SystemSensorManager.hardwareValue);
                  stringBuilder.append("to: ");
                  stringBuilder.append(this.mListener);
                  Log.d("SensorManager", stringBuilder.toString());
                } 
              } 
              float f = sensorEvent.values[0];
              if (f > 4.0D)
                return; 
            } 
          } catch (Exception exception1) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("SmartRotation got exception, e = ");
            stringBuilder.append(exception1.toString());
            Log.e("SensorManager", stringBuilder.toString());
          }  
        this.mListener.onSensorChanged(sensorEvent);
        return;
      } 
    }
    
    protected void dispatchFlushCompleteEvent(int param1Int) {
      if (this.mListener instanceof SensorEventListener2) {
        Sensor sensor = (Sensor)this.mManager.mHandleToSensor.get(Integer.valueOf(param1Int));
        if (sensor == null)
          return; 
        ((SensorEventListener2)this.mListener).onFlushCompleted(sensor);
      } 
    }
    
    protected void dispatchAdditionalInfoEvent(int param1Int1, int param1Int2, int param1Int3, float[] param1ArrayOffloat, int[] param1ArrayOfint) {
      if (this.mListener instanceof SensorEventCallback) {
        Sensor sensor = (Sensor)this.mManager.mHandleToSensor.get(Integer.valueOf(param1Int1));
        if (sensor == null)
          return; 
        SensorAdditionalInfo sensorAdditionalInfo = new SensorAdditionalInfo(sensor, param1Int2, param1Int3, param1ArrayOfint, param1ArrayOffloat);
        ((SensorEventCallback)this.mListener).onSensorAdditionalInfo(sensorAdditionalInfo);
      } 
    }
  }
  
  static final class TriggerEventQueue extends BaseEventQueue {
    private final TriggerEventListener mListener;
    
    private final SparseArray<TriggerEvent> mTriggerEvents = new SparseArray();
    
    public TriggerEventQueue(TriggerEventListener param1TriggerEventListener, Looper param1Looper, SystemSensorManager param1SystemSensorManager, String param1String) {
      super(param1Looper, param1SystemSensorManager, 0, param1String);
      this.mListener = param1TriggerEventListener;
    }
    
    public void addSensorEvent(Sensor param1Sensor) {
      SystemSensorManager systemSensorManager = this.mManager;
      int i = systemSensorManager.mTargetSdkLevel;
      TriggerEvent triggerEvent = new TriggerEvent(Sensor.getMaxLengthValuesArray(param1Sensor, i));
      synchronized (this.mTriggerEvents) {
        this.mTriggerEvents.put(param1Sensor.getHandle(), triggerEvent);
        return;
      } 
    }
    
    public void removeSensorEvent(Sensor param1Sensor) {
      synchronized (this.mTriggerEvents) {
        this.mTriggerEvents.delete(param1Sensor.getHandle());
        return;
      } 
    }
    
    protected void dispatchSensorEvent(int param1Int1, float[] param1ArrayOffloat, int param1Int2, long param1Long) {
      Sensor sensor = (Sensor)this.mManager.mHandleToSensor.get(Integer.valueOf(param1Int1));
      if (sensor == null)
        return; 
      synchronized (this.mTriggerEvents) {
        StringBuilder stringBuilder;
        TriggerEvent triggerEvent = (TriggerEvent)this.mTriggerEvents.get(param1Int1);
        if (triggerEvent == null) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Error: Trigger Event is null for Sensor: ");
          stringBuilder.append(sensor);
          Log.e("SensorManager", stringBuilder.toString());
          return;
        } 
        System.arraycopy(stringBuilder, 0, triggerEvent.values, 0, triggerEvent.values.length);
        triggerEvent.timestamp = param1Long;
        triggerEvent.sensor = sensor;
        this.mManager.cancelTriggerSensorImpl(this.mListener, sensor, false);
        this.mListener.onTrigger(triggerEvent);
        return;
      } 
    }
    
    protected void dispatchFlushCompleteEvent(int param1Int) {}
  }
  
  final class InjectEventQueue extends BaseEventQueue {
    final SystemSensorManager this$0;
    
    public InjectEventQueue(Looper param1Looper, SystemSensorManager param1SystemSensorManager1, String param1String) {
      super(param1Looper, param1SystemSensorManager1, 1, param1String);
    }
    
    int injectSensorData(int param1Int1, float[] param1ArrayOffloat, int param1Int2, long param1Long) {
      return injectSensorDataBase(param1Int1, param1ArrayOffloat, param1Int2, param1Long);
    }
    
    protected void dispatchSensorEvent(int param1Int1, float[] param1ArrayOffloat, int param1Int2, long param1Long) {}
    
    protected void dispatchFlushCompleteEvent(int param1Int) {}
    
    protected void addSensorEvent(Sensor param1Sensor) {}
    
    protected void removeSensorEvent(Sensor param1Sensor) {}
  }
  
  protected boolean setOperationParameterImpl(SensorAdditionalInfo paramSensorAdditionalInfo) {
    boolean bool;
    int i = -1;
    if (paramSensorAdditionalInfo.sensor != null)
      i = paramSensorAdditionalInfo.sensor.getHandle(); 
    if (nativeSetOperationParameter(this.mNativeInstance, i, paramSensorAdditionalInfo.type, paramSensorAdditionalInfo.floatValues, paramSensorAdditionalInfo.intValues) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  protected void updateSensorConfingImpl() {
    Log.v("SensorManager", "updateSensorConfingImpl");
    nativeUpdateSensorConfing(this.mNativeInstance);
  }
  
  protected void setScreenShotDisableImpl() {
    Log.v("SensorManager", "setScreenShotDisableImpl");
    nativeSetScreenShotDisable(this.mNativeInstance);
  }
  
  private static native void nativeClassInit();
  
  private static native int nativeConfigDirectChannel(long paramLong, int paramInt1, int paramInt2, int paramInt3);
  
  private static native long nativeCreate(String paramString);
  
  private static native int nativeCreateDirectChannel(long paramLong1, long paramLong2, int paramInt1, int paramInt2, HardwareBuffer paramHardwareBuffer);
  
  private static native void nativeDestroyDirectChannel(long paramLong, int paramInt);
  
  private static native void nativeGetDynamicSensors(long paramLong, List<Sensor> paramList);
  
  private static native boolean nativeGetSensorAtIndex(long paramLong, Sensor paramSensor, int paramInt);
  
  private static native boolean nativeIsDataInjectionEnabled(long paramLong);
  
  private static native int nativeSetOperationParameter(long paramLong, int paramInt1, int paramInt2, float[] paramArrayOffloat, int[] paramArrayOfint);
  
  private static native void nativeSetScreenShotDisable(long paramLong);
  
  private static native void nativeUpdateSensorConfing(long paramLong);
}
