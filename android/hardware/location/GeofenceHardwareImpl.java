package android.hardware.location;

import android.content.Context;
import android.location.IFusedGeofenceHardware;
import android.location.IGpsGeofenceHardware;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import java.util.ArrayList;

public final class GeofenceHardwareImpl {
  private static final boolean DEBUG = Log.isLoggable("GeofenceHardwareImpl", 3);
  
  private final SparseArray<IGeofenceHardwareCallback> mGeofences = new SparseArray();
  
  private final ArrayList<IGeofenceHardwareMonitorCallback>[] mCallbacks = (ArrayList<IGeofenceHardwareMonitorCallback>[])new ArrayList[2];
  
  private final ArrayList<Reaper> mReapers = new ArrayList<>();
  
  private int mVersion = 1;
  
  private int[] mSupportedMonitorTypes = new int[2];
  
  private static final int ADD_GEOFENCE_CALLBACK = 2;
  
  private static final int CALLBACK_ADD = 2;
  
  private static final int CALLBACK_REMOVE = 3;
  
  private static final int CAPABILITY_GNSS = 1;
  
  private static final int FIRST_VERSION_WITH_CAPABILITIES = 2;
  
  private static final int GEOFENCE_CALLBACK_BINDER_DIED = 6;
  
  private static final int GEOFENCE_STATUS = 1;
  
  private static final int GEOFENCE_TRANSITION_CALLBACK = 1;
  
  private static final int LOCATION_HAS_ACCURACY = 16;
  
  private static final int LOCATION_HAS_ALTITUDE = 2;
  
  private static final int LOCATION_HAS_BEARING = 8;
  
  private static final int LOCATION_HAS_LAT_LONG = 1;
  
  private static final int LOCATION_HAS_SPEED = 4;
  
  private static final int LOCATION_INVALID = 0;
  
  private static final int MONITOR_CALLBACK_BINDER_DIED = 4;
  
  private static final int PAUSE_GEOFENCE_CALLBACK = 4;
  
  private static final int REAPER_GEOFENCE_ADDED = 1;
  
  private static final int REAPER_MONITOR_CALLBACK_ADDED = 2;
  
  private static final int REAPER_REMOVED = 3;
  
  private static final int REMOVE_GEOFENCE_CALLBACK = 3;
  
  private static final int RESOLUTION_LEVEL_COARSE = 2;
  
  private static final int RESOLUTION_LEVEL_FINE = 3;
  
  private static final int RESOLUTION_LEVEL_NONE = 1;
  
  private static final int RESUME_GEOFENCE_CALLBACK = 5;
  
  private static final String TAG = "GeofenceHardwareImpl";
  
  private static GeofenceHardwareImpl sInstance;
  
  private Handler mCallbacksHandler;
  
  private int mCapabilities;
  
  private final Context mContext;
  
  private IFusedGeofenceHardware mFusedService;
  
  private Handler mGeofenceHandler;
  
  private IGpsGeofenceHardware mGpsService;
  
  private Handler mReaperHandler;
  
  private PowerManager.WakeLock mWakeLock;
  
  public static GeofenceHardwareImpl getInstance(Context paramContext) {
    // Byte code:
    //   0: ldc android/hardware/location/GeofenceHardwareImpl
    //   2: monitorenter
    //   3: getstatic android/hardware/location/GeofenceHardwareImpl.sInstance : Landroid/hardware/location/GeofenceHardwareImpl;
    //   6: ifnonnull -> 22
    //   9: new android/hardware/location/GeofenceHardwareImpl
    //   12: astore_1
    //   13: aload_1
    //   14: aload_0
    //   15: invokespecial <init> : (Landroid/content/Context;)V
    //   18: aload_1
    //   19: putstatic android/hardware/location/GeofenceHardwareImpl.sInstance : Landroid/hardware/location/GeofenceHardwareImpl;
    //   22: getstatic android/hardware/location/GeofenceHardwareImpl.sInstance : Landroid/hardware/location/GeofenceHardwareImpl;
    //   25: astore_0
    //   26: ldc android/hardware/location/GeofenceHardwareImpl
    //   28: monitorexit
    //   29: aload_0
    //   30: areturn
    //   31: astore_0
    //   32: ldc android/hardware/location/GeofenceHardwareImpl
    //   34: monitorexit
    //   35: aload_0
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #99	-> 3
    //   #100	-> 9
    //   #102	-> 22
    //   #98	-> 31
    // Exception table:
    //   from	to	target	type
    //   3	9	31	finally
    //   9	22	31	finally
    //   22	26	31	finally
  }
  
  private void acquireWakeLock() {
    if (this.mWakeLock == null) {
      Context context = this.mContext;
      PowerManager powerManager = (PowerManager)context.getSystemService("power");
      this.mWakeLock = powerManager.newWakeLock(1, "GeofenceHardwareImpl");
    } 
    this.mWakeLock.acquire();
  }
  
  private void releaseWakeLock() {
    if (this.mWakeLock.isHeld())
      this.mWakeLock.release(); 
  }
  
  private void updateGpsHardwareAvailability() {
    boolean bool;
    try {
      bool = this.mGpsService.isHardwareGeofenceSupported();
    } catch (RemoteException remoteException) {
      Log.e("GeofenceHardwareImpl", "Remote Exception calling LocationManagerService");
      bool = false;
    } 
    if (bool)
      setMonitorAvailability(0, 0); 
  }
  
  private void updateFusedHardwareAvailability() {
    boolean bool;
    try {
      if (this.mVersion < 2 || (this.mCapabilities & 0x1) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      if (this.mFusedService != null) {
        boolean bool1 = this.mFusedService.isSupported();
        if (bool1 && bool) {
          bool = true;
        } else {
          bool = false;
        } 
      } else {
        bool = false;
      } 
    } catch (RemoteException remoteException) {
      Log.e("GeofenceHardwareImpl", "RemoteException calling LocationManagerService");
      bool = false;
    } 
    if (bool)
      setMonitorAvailability(1, 0); 
  }
  
  public void setGpsHardwareGeofence(IGpsGeofenceHardware paramIGpsGeofenceHardware) {
    if (this.mGpsService == null) {
      this.mGpsService = paramIGpsGeofenceHardware;
      updateGpsHardwareAvailability();
    } else if (paramIGpsGeofenceHardware == null) {
      this.mGpsService = null;
      Log.w("GeofenceHardwareImpl", "GPS Geofence Hardware service seems to have crashed");
    } else {
      Log.e("GeofenceHardwareImpl", "Error: GpsService being set again.");
    } 
  }
  
  public void onCapabilities(int paramInt) {
    this.mCapabilities = paramInt;
    updateFusedHardwareAvailability();
  }
  
  public void setVersion(int paramInt) {
    this.mVersion = paramInt;
    updateFusedHardwareAvailability();
  }
  
  public void setFusedGeofenceHardware(IFusedGeofenceHardware paramIFusedGeofenceHardware) {
    if (this.mFusedService == null) {
      this.mFusedService = paramIFusedGeofenceHardware;
      updateFusedHardwareAvailability();
    } else if (paramIFusedGeofenceHardware == null) {
      this.mFusedService = null;
      Log.w("GeofenceHardwareImpl", "Fused Geofence Hardware service seems to have crashed");
    } else {
      Log.e("GeofenceHardwareImpl", "Error: FusedService being set again");
    } 
  }
  
  public int[] getMonitoringTypes() {
    synchronized (this.mSupportedMonitorTypes) {
      boolean bool1;
      boolean bool2;
      if (this.mSupportedMonitorTypes[0] != 2) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if (this.mSupportedMonitorTypes[1] != 2) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if (bool1) {
        if (bool2)
          return new int[] { 0, 1 }; 
        return new int[] { 0 };
      } 
      if (bool2)
        return new int[] { 1 }; 
      return new int[0];
    } 
  }
  
  public int getStatusOfMonitoringType(int paramInt) {
    synchronized (this.mSupportedMonitorTypes) {
      if (paramInt < this.mSupportedMonitorTypes.length && paramInt >= 0) {
        paramInt = this.mSupportedMonitorTypes[paramInt];
        return paramInt;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      this("Unknown monitoring type");
      throw illegalArgumentException;
    } 
  }
  
  public int getCapabilitiesForMonitoringType(int paramInt) {
    if (this.mSupportedMonitorTypes[paramInt] == 0) {
      if (paramInt != 0) {
        if (paramInt == 1) {
          if (this.mVersion >= 2)
            return this.mCapabilities; 
          return 1;
        } 
        return 0;
      } 
      return 1;
    } 
    return 0;
  }
  
  public boolean addCircularFence(int paramInt, GeofenceHardwareRequestParcelable paramGeofenceHardwareRequestParcelable, IGeofenceHardwareCallback paramIGeofenceHardwareCallback) {
    SparseArray<IGeofenceHardwareCallback> sparseArray;
    IGpsGeofenceHardware iGpsGeofenceHardware;
    int i = paramGeofenceHardwareRequestParcelable.getId();
    if (DEBUG) {
      String str = String.format("addCircularFence: monitoringType=%d, %s", new Object[] { Integer.valueOf(paramInt), paramGeofenceHardwareRequestParcelable });
      Log.d("GeofenceHardwareImpl", str);
    } 
    synchronized (this.mGeofences) {
      boolean bool;
      this.mGeofences.put(i, paramIGeofenceHardwareCallback);
      if (paramInt != 0) {
        if (paramInt != 1) {
          bool = false;
        } else {
          IFusedGeofenceHardware iFusedGeofenceHardware = this.mFusedService;
          if (iFusedGeofenceHardware == null)
            return false; 
          try {
            iFusedGeofenceHardware.addGeofences(new GeofenceHardwareRequestParcelable[] { paramGeofenceHardwareRequestParcelable });
            bool = true;
          } catch (RemoteException remoteException) {
            Log.e("GeofenceHardwareImpl", "AddGeofence: RemoteException calling LocationManagerService");
            bool = false;
          } 
        } 
      } else {
        iGpsGeofenceHardware = this.mGpsService;
        if (iGpsGeofenceHardware == null)
          return false; 
        try {
          int j = remoteException.getId();
          double d1 = remoteException.getLatitude();
          double d2 = remoteException.getLongitude();
          double d3 = remoteException.getRadius();
          int k = remoteException.getLastTransition();
          int m = remoteException.getMonitorTransitions();
          int n = remoteException.getNotificationResponsiveness();
          int i1 = remoteException.getUnknownTimer();
          bool = iGpsGeofenceHardware.addCircularHardwareGeofence(j, d1, d2, d3, k, m, n, i1);
        } catch (RemoteException remoteException1) {
          Log.e("GeofenceHardwareImpl", "AddGeofence: Remote Exception calling LocationManagerService");
          bool = false;
        } 
      } 
      if (bool) {
        Message message = this.mReaperHandler.obtainMessage(1, paramIGeofenceHardwareCallback);
        message.arg1 = paramInt;
        this.mReaperHandler.sendMessage(message);
      } else {
        synchronized (this.mGeofences) {
          this.mGeofences.remove(i);
          if (DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("addCircularFence: Result is: ");
            stringBuilder.append(bool);
            Log.d("GeofenceHardwareImpl", stringBuilder.toString());
          } 
          return bool;
        } 
      } 
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("addCircularFence: Result is: ");
        stringBuilder.append(bool);
        Log.d("GeofenceHardwareImpl", stringBuilder.toString());
      } 
      return bool;
    } 
  }
  
  public boolean removeGeofence(int paramInt1, int paramInt2) {
    SparseArray<IGeofenceHardwareCallback> sparseArray;
    StringBuilder stringBuilder;
    if (DEBUG) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Remove Geofence: GeofenceId: ");
      stringBuilder.append(paramInt1);
      Log.d("GeofenceHardwareImpl", stringBuilder.toString());
    } 
    synchronized (this.mGeofences) {
      if (this.mGeofences.get(paramInt1) != null) {
        boolean bool;
        if (paramInt2 != 0) {
          if (paramInt2 != 1) {
            bool = false;
          } else {
            IFusedGeofenceHardware iFusedGeofenceHardware = this.mFusedService;
            if (iFusedGeofenceHardware == null)
              return false; 
            try {
              iFusedGeofenceHardware.removeGeofences(new int[] { paramInt1 });
              bool = true;
            } catch (RemoteException remoteException) {
              Log.e("GeofenceHardwareImpl", "RemoveGeofence: RemoteException calling LocationManagerService");
              bool = false;
            } 
          } 
        } else {
          IGpsGeofenceHardware iGpsGeofenceHardware = this.mGpsService;
          if (iGpsGeofenceHardware == null)
            return false; 
          try {
            bool = iGpsGeofenceHardware.removeHardwareGeofence(paramInt1);
          } catch (RemoteException remoteException) {
            Log.e("GeofenceHardwareImpl", "RemoveGeofence: Remote Exception calling LocationManagerService");
            bool = false;
          } 
        } 
        if (DEBUG) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("removeGeofence: Result is: ");
          stringBuilder.append(bool);
          Log.d("GeofenceHardwareImpl", stringBuilder.toString());
        } 
        return bool;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append("Geofence ");
      stringBuilder1.append(paramInt1);
      stringBuilder1.append(" not registered.");
      this(stringBuilder1.toString());
      throw illegalArgumentException;
    } 
  }
  
  public boolean pauseGeofence(int paramInt1, int paramInt2) {
    SparseArray<IGeofenceHardwareCallback> sparseArray;
    StringBuilder stringBuilder;
    if (DEBUG) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Pause Geofence: GeofenceId: ");
      stringBuilder.append(paramInt1);
      Log.d("GeofenceHardwareImpl", stringBuilder.toString());
    } 
    synchronized (this.mGeofences) {
      if (this.mGeofences.get(paramInt1) != null) {
        boolean bool;
        if (paramInt2 != 0) {
          if (paramInt2 != 1) {
            bool = false;
          } else {
            IFusedGeofenceHardware iFusedGeofenceHardware = this.mFusedService;
            if (iFusedGeofenceHardware == null)
              return false; 
            try {
              iFusedGeofenceHardware.pauseMonitoringGeofence(paramInt1);
              bool = true;
            } catch (RemoteException remoteException) {
              Log.e("GeofenceHardwareImpl", "PauseGeofence: RemoteException calling LocationManagerService");
              bool = false;
            } 
          } 
        } else {
          IGpsGeofenceHardware iGpsGeofenceHardware = this.mGpsService;
          if (iGpsGeofenceHardware == null)
            return false; 
          try {
            bool = iGpsGeofenceHardware.pauseHardwareGeofence(paramInt1);
          } catch (RemoteException remoteException) {
            Log.e("GeofenceHardwareImpl", "PauseGeofence: Remote Exception calling LocationManagerService");
            bool = false;
          } 
        } 
        if (DEBUG) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("pauseGeofence: Result is: ");
          stringBuilder.append(bool);
          Log.d("GeofenceHardwareImpl", stringBuilder.toString());
        } 
        return bool;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append("Geofence ");
      stringBuilder1.append(paramInt1);
      stringBuilder1.append(" not registered.");
      this(stringBuilder1.toString());
      throw illegalArgumentException;
    } 
  }
  
  public boolean resumeGeofence(int paramInt1, int paramInt2, int paramInt3) {
    SparseArray<IGeofenceHardwareCallback> sparseArray;
    StringBuilder stringBuilder;
    if (DEBUG) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Resume Geofence: GeofenceId: ");
      stringBuilder.append(paramInt1);
      Log.d("GeofenceHardwareImpl", stringBuilder.toString());
    } 
    synchronized (this.mGeofences) {
      if (this.mGeofences.get(paramInt1) != null) {
        boolean bool;
        if (paramInt2 != 0) {
          if (paramInt2 != 1) {
            bool = false;
          } else {
            IFusedGeofenceHardware iFusedGeofenceHardware = this.mFusedService;
            if (iFusedGeofenceHardware == null)
              return false; 
            try {
              iFusedGeofenceHardware.resumeMonitoringGeofence(paramInt1, paramInt3);
              bool = true;
            } catch (RemoteException remoteException) {
              Log.e("GeofenceHardwareImpl", "ResumeGeofence: RemoteException calling LocationManagerService");
              bool = false;
            } 
          } 
        } else {
          IGpsGeofenceHardware iGpsGeofenceHardware = this.mGpsService;
          if (iGpsGeofenceHardware == null)
            return false; 
          try {
            bool = iGpsGeofenceHardware.resumeHardwareGeofence(paramInt1, paramInt3);
          } catch (RemoteException remoteException) {
            Log.e("GeofenceHardwareImpl", "ResumeGeofence: Remote Exception calling LocationManagerService");
            bool = false;
          } 
        } 
        if (DEBUG) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("resumeGeofence: Result is: ");
          stringBuilder.append(bool);
          Log.d("GeofenceHardwareImpl", stringBuilder.toString());
        } 
        return bool;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      StringBuilder stringBuilder1 = new StringBuilder();
      this();
      stringBuilder1.append("Geofence ");
      stringBuilder1.append(paramInt1);
      stringBuilder1.append(" not registered.");
      this(stringBuilder1.toString());
      throw illegalArgumentException;
    } 
  }
  
  public boolean registerForMonitorStateChangeCallback(int paramInt, IGeofenceHardwareMonitorCallback paramIGeofenceHardwareMonitorCallback) {
    Handler handler = this.mReaperHandler;
    Message message2 = handler.obtainMessage(2, paramIGeofenceHardwareMonitorCallback);
    message2.arg1 = paramInt;
    this.mReaperHandler.sendMessage(message2);
    Message message1 = this.mCallbacksHandler.obtainMessage(2, paramIGeofenceHardwareMonitorCallback);
    message1.arg1 = paramInt;
    this.mCallbacksHandler.sendMessage(message1);
    return true;
  }
  
  public boolean unregisterForMonitorStateChangeCallback(int paramInt, IGeofenceHardwareMonitorCallback paramIGeofenceHardwareMonitorCallback) {
    Message message = this.mCallbacksHandler.obtainMessage(3, paramIGeofenceHardwareMonitorCallback);
    message.arg1 = paramInt;
    this.mCallbacksHandler.sendMessage(message);
    return true;
  }
  
  public void reportGeofenceTransition(int paramInt1, Location paramLocation, int paramInt2, long paramLong, int paramInt3, int paramInt4) {
    if (paramLocation == null) {
      Log.e("GeofenceHardwareImpl", String.format("Invalid Geofence Transition: location=null", new Object[0]));
      return;
    } 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("GeofenceTransition| ");
      stringBuilder.append(paramLocation);
      stringBuilder.append(", transition:");
      stringBuilder.append(paramInt2);
      stringBuilder.append(", transitionTimestamp:");
      stringBuilder.append(paramLong);
      stringBuilder.append(", monitoringType:");
      stringBuilder.append(paramInt3);
      stringBuilder.append(", sourcesUsed:");
      stringBuilder.append(paramInt4);
      Log.d("GeofenceHardwareImpl", stringBuilder.toString());
    } 
    GeofenceTransition geofenceTransition = new GeofenceTransition(paramInt1, paramInt2, paramLong, paramLocation, paramInt3, paramInt4);
    acquireWakeLock();
    Message message = this.mGeofenceHandler.obtainMessage(1, geofenceTransition);
    message.sendToTarget();
  }
  
  public void reportGeofenceMonitorStatus(int paramInt1, int paramInt2, Location paramLocation, int paramInt3) {
    setMonitorAvailability(paramInt1, paramInt2);
    acquireWakeLock();
    GeofenceHardwareMonitorEvent geofenceHardwareMonitorEvent = new GeofenceHardwareMonitorEvent(paramInt1, paramInt2, paramInt3, paramLocation);
    Message message = this.mCallbacksHandler.obtainMessage(1, geofenceHardwareMonitorEvent);
    message.sendToTarget();
  }
  
  private void reportGeofenceOperationStatus(int paramInt1, int paramInt2, int paramInt3) {
    acquireWakeLock();
    Message message = this.mGeofenceHandler.obtainMessage(paramInt1);
    message.arg1 = paramInt2;
    message.arg2 = paramInt3;
    message.sendToTarget();
  }
  
  public void reportGeofenceAddStatus(int paramInt1, int paramInt2) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AddCallback| id:");
      stringBuilder.append(paramInt1);
      stringBuilder.append(", status:");
      stringBuilder.append(paramInt2);
      Log.d("GeofenceHardwareImpl", stringBuilder.toString());
    } 
    reportGeofenceOperationStatus(2, paramInt1, paramInt2);
  }
  
  public void reportGeofenceRemoveStatus(int paramInt1, int paramInt2) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("RemoveCallback| id:");
      stringBuilder.append(paramInt1);
      stringBuilder.append(", status:");
      stringBuilder.append(paramInt2);
      Log.d("GeofenceHardwareImpl", stringBuilder.toString());
    } 
    reportGeofenceOperationStatus(3, paramInt1, paramInt2);
  }
  
  public void reportGeofencePauseStatus(int paramInt1, int paramInt2) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("PauseCallbac| id:");
      stringBuilder.append(paramInt1);
      stringBuilder.append(", status");
      stringBuilder.append(paramInt2);
      Log.d("GeofenceHardwareImpl", stringBuilder.toString());
    } 
    reportGeofenceOperationStatus(4, paramInt1, paramInt2);
  }
  
  public void reportGeofenceResumeStatus(int paramInt1, int paramInt2) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ResumeCallback| id:");
      stringBuilder.append(paramInt1);
      stringBuilder.append(", status:");
      stringBuilder.append(paramInt2);
      Log.d("GeofenceHardwareImpl", stringBuilder.toString());
    } 
    reportGeofenceOperationStatus(5, paramInt1, paramInt2);
  }
  
  private GeofenceHardwareImpl(Context paramContext) {
    this.mGeofenceHandler = (Handler)new Object(this);
    this.mCallbacksHandler = (Handler)new Object(this);
    this.mReaperHandler = (Handler)new Object(this);
    this.mContext = paramContext;
    setMonitorAvailability(0, 2);
    setMonitorAvailability(1, 2);
  }
  
  private class GeofenceTransition {
    private int mGeofenceId;
    
    private Location mLocation;
    
    private int mMonitoringType;
    
    private int mSourcesUsed;
    
    private long mTimestamp;
    
    private int mTransition;
    
    final GeofenceHardwareImpl this$0;
    
    GeofenceTransition(int param1Int1, int param1Int2, long param1Long, Location param1Location, int param1Int3, int param1Int4) {
      this.mGeofenceId = param1Int1;
      this.mTransition = param1Int2;
      this.mTimestamp = param1Long;
      this.mLocation = param1Location;
      this.mMonitoringType = param1Int3;
      this.mSourcesUsed = param1Int4;
    }
  }
  
  private void setMonitorAvailability(int paramInt1, int paramInt2) {
    synchronized (this.mSupportedMonitorTypes) {
      this.mSupportedMonitorTypes[paramInt1] = paramInt2;
      return;
    } 
  }
  
  int getMonitoringResolutionLevel(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1)
        return 1; 
      return 3;
    } 
    return 3;
  }
  
  class Reaper implements IBinder.DeathRecipient {
    private IGeofenceHardwareCallback mCallback;
    
    private IGeofenceHardwareMonitorCallback mMonitorCallback;
    
    private int mMonitoringType;
    
    final GeofenceHardwareImpl this$0;
    
    Reaper(IGeofenceHardwareCallback param1IGeofenceHardwareCallback, int param1Int) {
      this.mCallback = param1IGeofenceHardwareCallback;
      this.mMonitoringType = param1Int;
    }
    
    Reaper(IGeofenceHardwareMonitorCallback param1IGeofenceHardwareMonitorCallback, int param1Int) {
      this.mMonitorCallback = param1IGeofenceHardwareMonitorCallback;
      this.mMonitoringType = param1Int;
    }
    
    public void binderDied() {
      if (this.mCallback != null) {
        Message message1 = GeofenceHardwareImpl.this.mGeofenceHandler.obtainMessage(6, this.mCallback);
        message1.arg1 = this.mMonitoringType;
        GeofenceHardwareImpl.this.mGeofenceHandler.sendMessage(message1);
      } else if (this.mMonitorCallback != null) {
        Message message1 = GeofenceHardwareImpl.this.mCallbacksHandler.obtainMessage(4, this.mMonitorCallback);
        message1.arg1 = this.mMonitoringType;
        GeofenceHardwareImpl.this.mCallbacksHandler.sendMessage(message1);
      } 
      Message message = GeofenceHardwareImpl.this.mReaperHandler.obtainMessage(3, this);
      GeofenceHardwareImpl.this.mReaperHandler.sendMessage(message);
    }
    
    public int hashCode() {
      byte b;
      IGeofenceHardwareCallback iGeofenceHardwareCallback = this.mCallback;
      int i = 0;
      if (iGeofenceHardwareCallback != null) {
        b = iGeofenceHardwareCallback.asBinder().hashCode();
      } else {
        b = 0;
      } 
      IGeofenceHardwareMonitorCallback iGeofenceHardwareMonitorCallback = this.mMonitorCallback;
      if (iGeofenceHardwareMonitorCallback != null)
        i = iGeofenceHardwareMonitorCallback.asBinder().hashCode(); 
      int j = this.mMonitoringType;
      return ((17 * 31 + b) * 31 + i) * 31 + j;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = false;
      if (param1Object == null)
        return false; 
      if (param1Object == this)
        return true; 
      Reaper reaper = (Reaper)param1Object;
      if (binderEquals(reaper.mCallback, this.mCallback)) {
        param1Object = reaper.mMonitorCallback;
        IGeofenceHardwareMonitorCallback iGeofenceHardwareMonitorCallback = this.mMonitorCallback;
        if (binderEquals((IInterface)param1Object, iGeofenceHardwareMonitorCallback) && reaper.mMonitoringType == this.mMonitoringType)
          bool = true; 
      } 
      return bool;
    }
    
    private boolean binderEquals(IInterface param1IInterface1, IInterface param1IInterface2) {
      boolean bool = true;
      null = true;
      if (param1IInterface1 == null) {
        if (param1IInterface2 != null)
          null = false; 
        return null;
      } 
      return (param1IInterface2 != null && param1IInterface1.asBinder() == param1IInterface2.asBinder()) ? bool : false;
    }
    
    private boolean unlinkToDeath() {
      IGeofenceHardwareMonitorCallback iGeofenceHardwareMonitorCallback = this.mMonitorCallback;
      if (iGeofenceHardwareMonitorCallback != null)
        return iGeofenceHardwareMonitorCallback.asBinder().unlinkToDeath(this, 0); 
      IGeofenceHardwareCallback iGeofenceHardwareCallback = this.mCallback;
      if (iGeofenceHardwareCallback != null)
        return iGeofenceHardwareCallback.asBinder().unlinkToDeath(this, 0); 
      return true;
    }
    
    private boolean callbackEquals(IGeofenceHardwareCallback param1IGeofenceHardwareCallback) {
      boolean bool;
      IGeofenceHardwareCallback iGeofenceHardwareCallback = this.mCallback;
      if (iGeofenceHardwareCallback != null && iGeofenceHardwareCallback.asBinder() == param1IGeofenceHardwareCallback.asBinder()) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  int getAllowedResolutionLevel(int paramInt1, int paramInt2) {
    if (this.mContext.checkPermission("android.permission.ACCESS_FINE_LOCATION", paramInt1, paramInt2) == 0)
      return 3; 
    if (this.mContext.checkPermission("android.permission.ACCESS_COARSE_LOCATION", paramInt1, paramInt2) == 0)
      return 2; 
    return 1;
  }
}
