package android.hardware.location;

import android.annotation.SystemApi;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public class GeofenceHardwareMonitorEvent implements Parcelable {
  public GeofenceHardwareMonitorEvent(int paramInt1, int paramInt2, int paramInt3, Location paramLocation) {
    this.mMonitoringType = paramInt1;
    this.mMonitoringStatus = paramInt2;
    this.mSourceTechnologies = paramInt3;
    this.mLocation = paramLocation;
  }
  
  public int getMonitoringType() {
    return this.mMonitoringType;
  }
  
  public int getMonitoringStatus() {
    return this.mMonitoringStatus;
  }
  
  public int getSourceTechnologies() {
    return this.mSourceTechnologies;
  }
  
  public Location getLocation() {
    return this.mLocation;
  }
  
  public static final Parcelable.Creator<GeofenceHardwareMonitorEvent> CREATOR = new Parcelable.Creator<GeofenceHardwareMonitorEvent>() {
      public GeofenceHardwareMonitorEvent createFromParcel(Parcel param1Parcel) {
        ClassLoader classLoader = GeofenceHardwareMonitorEvent.class.getClassLoader();
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        Location location = (Location)param1Parcel.readParcelable(classLoader);
        return new GeofenceHardwareMonitorEvent(i, j, k, location);
      }
      
      public GeofenceHardwareMonitorEvent[] newArray(int param1Int) {
        return new GeofenceHardwareMonitorEvent[param1Int];
      }
    };
  
  private final Location mLocation;
  
  private final int mMonitoringStatus;
  
  private final int mMonitoringType;
  
  private final int mSourceTechnologies;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mMonitoringType);
    paramParcel.writeInt(this.mMonitoringStatus);
    paramParcel.writeInt(this.mSourceTechnologies);
    paramParcel.writeParcelable((Parcelable)this.mLocation, paramInt);
  }
  
  public String toString() {
    int i = this.mMonitoringType;
    int j = this.mMonitoringStatus;
    int k = this.mSourceTechnologies;
    Location location = this.mLocation;
    return String.format("GeofenceHardwareMonitorEvent: type=%d, status=%d, sources=%d, location=%s", new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), location });
  }
}
