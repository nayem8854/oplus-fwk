package android.hardware.location;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IContextHubTransactionCallback extends IInterface {
  void onQueryResponse(int paramInt, List<NanoAppState> paramList) throws RemoteException;
  
  void onTransactionComplete(int paramInt) throws RemoteException;
  
  class Default implements IContextHubTransactionCallback {
    public void onQueryResponse(int param1Int, List<NanoAppState> param1List) throws RemoteException {}
    
    public void onTransactionComplete(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IContextHubTransactionCallback {
    private static final String DESCRIPTOR = "android.hardware.location.IContextHubTransactionCallback";
    
    static final int TRANSACTION_onQueryResponse = 1;
    
    static final int TRANSACTION_onTransactionComplete = 2;
    
    public Stub() {
      attachInterface(this, "android.hardware.location.IContextHubTransactionCallback");
    }
    
    public static IContextHubTransactionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.hardware.location.IContextHubTransactionCallback");
      if (iInterface != null && iInterface instanceof IContextHubTransactionCallback)
        return (IContextHubTransactionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onTransactionComplete";
      } 
      return "onQueryResponse";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.hardware.location.IContextHubTransactionCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.hardware.location.IContextHubTransactionCallback");
        param1Int1 = param1Parcel1.readInt();
        onTransactionComplete(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.hardware.location.IContextHubTransactionCallback");
      param1Int1 = param1Parcel1.readInt();
      ArrayList<NanoAppState> arrayList = param1Parcel1.createTypedArrayList(NanoAppState.CREATOR);
      onQueryResponse(param1Int1, arrayList);
      return true;
    }
    
    private static class Proxy implements IContextHubTransactionCallback {
      public static IContextHubTransactionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.hardware.location.IContextHubTransactionCallback";
      }
      
      public void onQueryResponse(int param2Int, List<NanoAppState> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.location.IContextHubTransactionCallback");
          parcel.writeInt(param2Int);
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IContextHubTransactionCallback.Stub.getDefaultImpl() != null) {
            IContextHubTransactionCallback.Stub.getDefaultImpl().onQueryResponse(param2Int, param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTransactionComplete(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.hardware.location.IContextHubTransactionCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IContextHubTransactionCallback.Stub.getDefaultImpl() != null) {
            IContextHubTransactionCallback.Stub.getDefaultImpl().onTransactionComplete(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IContextHubTransactionCallback param1IContextHubTransactionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IContextHubTransactionCallback != null) {
          Proxy.sDefaultImpl = param1IContextHubTransactionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IContextHubTransactionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
