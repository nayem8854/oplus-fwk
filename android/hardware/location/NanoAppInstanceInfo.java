package android.hardware.location;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import libcore.util.EmptyArray;

@SystemApi
@Deprecated
public class NanoAppInstanceInfo implements Parcelable {
  private String mPublisher = "Unknown";
  
  private String mName = "Unknown";
  
  private int mNeededReadMemBytes = 0;
  
  private int mNeededWriteMemBytes = 0;
  
  private int mNeededExecMemBytes = 0;
  
  private int[] mNeededSensors = EmptyArray.INT;
  
  private int[] mOutputEvents = EmptyArray.INT;
  
  public NanoAppInstanceInfo(int paramInt1, long paramLong, int paramInt2, int paramInt3) {
    this.mHandle = paramInt1;
    this.mAppId = paramLong;
    this.mAppVersion = paramInt2;
    this.mContexthubId = paramInt3;
  }
  
  public String getPublisher() {
    return this.mPublisher;
  }
  
  public String getName() {
    return this.mName;
  }
  
  public long getAppId() {
    return this.mAppId;
  }
  
  public int getAppVersion() {
    return this.mAppVersion;
  }
  
  public int getNeededReadMemBytes() {
    return this.mNeededReadMemBytes;
  }
  
  public int getNeededWriteMemBytes() {
    return this.mNeededWriteMemBytes;
  }
  
  public int getNeededExecMemBytes() {
    return this.mNeededExecMemBytes;
  }
  
  public int[] getNeededSensors() {
    return this.mNeededSensors;
  }
  
  public int[] getOutputEvents() {
    return this.mOutputEvents;
  }
  
  public int getContexthubId() {
    return this.mContexthubId;
  }
  
  public int getHandle() {
    return this.mHandle;
  }
  
  private NanoAppInstanceInfo(Parcel paramParcel) {
    this.mPublisher = paramParcel.readString();
    this.mName = paramParcel.readString();
    this.mHandle = paramParcel.readInt();
    this.mAppId = paramParcel.readLong();
    this.mAppVersion = paramParcel.readInt();
    this.mContexthubId = paramParcel.readInt();
    this.mNeededReadMemBytes = paramParcel.readInt();
    this.mNeededWriteMemBytes = paramParcel.readInt();
    this.mNeededExecMemBytes = paramParcel.readInt();
    int i = paramParcel.readInt();
    int[] arrayOfInt = new int[i];
    paramParcel.readIntArray(arrayOfInt);
    i = paramParcel.readInt();
    this.mOutputEvents = arrayOfInt = new int[i];
    paramParcel.readIntArray(arrayOfInt);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mPublisher);
    paramParcel.writeString(this.mName);
    paramParcel.writeInt(this.mHandle);
    paramParcel.writeLong(this.mAppId);
    paramParcel.writeInt(this.mAppVersion);
    paramParcel.writeInt(this.mContexthubId);
    paramParcel.writeInt(this.mNeededReadMemBytes);
    paramParcel.writeInt(this.mNeededWriteMemBytes);
    paramParcel.writeInt(this.mNeededExecMemBytes);
    paramParcel.writeInt(this.mNeededSensors.length);
    paramParcel.writeIntArray(this.mNeededSensors);
    paramParcel.writeInt(this.mOutputEvents.length);
    paramParcel.writeIntArray(this.mOutputEvents);
  }
  
  public static final Parcelable.Creator<NanoAppInstanceInfo> CREATOR = new Parcelable.Creator<NanoAppInstanceInfo>() {
      public NanoAppInstanceInfo createFromParcel(Parcel param1Parcel) {
        return new NanoAppInstanceInfo(param1Parcel);
      }
      
      public NanoAppInstanceInfo[] newArray(int param1Int) {
        return new NanoAppInstanceInfo[param1Int];
      }
    };
  
  private long mAppId;
  
  private int mAppVersion;
  
  private int mContexthubId;
  
  private int mHandle;
  
  public String toString() {
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("handle : ");
    stringBuilder2.append(this.mHandle);
    String str1 = stringBuilder2.toString();
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append(str1);
    stringBuilder3.append(", Id : 0x");
    stringBuilder3.append(Long.toHexString(this.mAppId));
    String str2 = stringBuilder3.toString();
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(str2);
    stringBuilder1.append(", Version : 0x");
    stringBuilder1.append(Integer.toHexString(this.mAppVersion));
    return stringBuilder1.toString();
  }
  
  public NanoAppInstanceInfo() {}
}
