package android.hardware.location;

import android.annotation.SystemApi;

@SystemApi
public class ContextHubClientCallback {
  public void onMessageFromNanoApp(ContextHubClient paramContextHubClient, NanoAppMessage paramNanoAppMessage) {}
  
  public void onHubReset(ContextHubClient paramContextHubClient) {}
  
  public void onNanoAppAborted(ContextHubClient paramContextHubClient, long paramLong, int paramInt) {}
  
  public void onNanoAppLoaded(ContextHubClient paramContextHubClient, long paramLong) {}
  
  public void onNanoAppUnloaded(ContextHubClient paramContextHubClient, long paramLong) {}
  
  public void onNanoAppEnabled(ContextHubClient paramContextHubClient, long paramLong) {}
  
  public void onNanoAppDisabled(ContextHubClient paramContextHubClient, long paramLong) {}
}
