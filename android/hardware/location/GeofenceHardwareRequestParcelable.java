package android.hardware.location;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public final class GeofenceHardwareRequestParcelable implements Parcelable {
  public GeofenceHardwareRequestParcelable(int paramInt, GeofenceHardwareRequest paramGeofenceHardwareRequest) {
    this.mId = paramInt;
    this.mRequest = paramGeofenceHardwareRequest;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public double getLatitude() {
    return this.mRequest.getLatitude();
  }
  
  public double getLongitude() {
    return this.mRequest.getLongitude();
  }
  
  public double getRadius() {
    return this.mRequest.getRadius();
  }
  
  public int getMonitorTransitions() {
    return this.mRequest.getMonitorTransitions();
  }
  
  public int getUnknownTimer() {
    return this.mRequest.getUnknownTimer();
  }
  
  public int getNotificationResponsiveness() {
    return this.mRequest.getNotificationResponsiveness();
  }
  
  public int getLastTransition() {
    return this.mRequest.getLastTransition();
  }
  
  int getType() {
    return this.mRequest.getType();
  }
  
  int getSourceTechnologies() {
    return this.mRequest.getSourceTechnologies();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("id=");
    stringBuilder.append(this.mId);
    stringBuilder.append(", type=");
    stringBuilder.append(this.mRequest.getType());
    stringBuilder.append(", latitude=");
    stringBuilder.append(this.mRequest.getLatitude());
    stringBuilder.append(", longitude=");
    stringBuilder.append(this.mRequest.getLongitude());
    stringBuilder.append(", radius=");
    stringBuilder.append(this.mRequest.getRadius());
    stringBuilder.append(", lastTransition=");
    stringBuilder.append(this.mRequest.getLastTransition());
    stringBuilder.append(", unknownTimer=");
    stringBuilder.append(this.mRequest.getUnknownTimer());
    stringBuilder.append(", monitorTransitions=");
    stringBuilder.append(this.mRequest.getMonitorTransitions());
    stringBuilder.append(", notificationResponsiveness=");
    stringBuilder.append(this.mRequest.getNotificationResponsiveness());
    stringBuilder.append(", sourceTechnologies=");
    stringBuilder.append(this.mRequest.getSourceTechnologies());
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<GeofenceHardwareRequestParcelable> CREATOR = new Parcelable.Creator<GeofenceHardwareRequestParcelable>() {
      public GeofenceHardwareRequestParcelable createFromParcel(Parcel param1Parcel) {
        String str;
        int i = param1Parcel.readInt();
        if (i != 0) {
          str = String.format("Invalid Geofence type: %d", new Object[] { Integer.valueOf(i) });
          Log.e("GeofenceHardwareRequest", str);
          return null;
        } 
        double d1 = str.readDouble();
        double d2 = str.readDouble();
        double d3 = str.readDouble();
        GeofenceHardwareRequest geofenceHardwareRequest = GeofenceHardwareRequest.createCircularGeofence(d1, d2, d3);
        geofenceHardwareRequest.setLastTransition(str.readInt());
        geofenceHardwareRequest.setMonitorTransitions(str.readInt());
        geofenceHardwareRequest.setUnknownTimer(str.readInt());
        geofenceHardwareRequest.setNotificationResponsiveness(str.readInt());
        geofenceHardwareRequest.setSourceTechnologies(str.readInt());
        i = str.readInt();
        return new GeofenceHardwareRequestParcelable(i, geofenceHardwareRequest);
      }
      
      public GeofenceHardwareRequestParcelable[] newArray(int param1Int) {
        return new GeofenceHardwareRequestParcelable[param1Int];
      }
    };
  
  private int mId;
  
  private GeofenceHardwareRequest mRequest;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(getType());
    paramParcel.writeDouble(getLatitude());
    paramParcel.writeDouble(getLongitude());
    paramParcel.writeDouble(getRadius());
    paramParcel.writeInt(getLastTransition());
    paramParcel.writeInt(getMonitorTransitions());
    paramParcel.writeInt(getUnknownTimer());
    paramParcel.writeInt(getNotificationResponsiveness());
    paramParcel.writeInt(getSourceTechnologies());
    paramParcel.writeInt(getId());
  }
}
