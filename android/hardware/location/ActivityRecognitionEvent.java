package android.hardware.location;

import android.os.Parcel;
import android.os.Parcelable;

public class ActivityRecognitionEvent implements Parcelable {
  public ActivityRecognitionEvent(String paramString, int paramInt, long paramLong) {
    this.mActivity = paramString;
    this.mEventType = paramInt;
    this.mTimestampNs = paramLong;
  }
  
  public String getActivity() {
    return this.mActivity;
  }
  
  public int getEventType() {
    return this.mEventType;
  }
  
  public long getTimestampNs() {
    return this.mTimestampNs;
  }
  
  public static final Parcelable.Creator<ActivityRecognitionEvent> CREATOR = new Parcelable.Creator<ActivityRecognitionEvent>() {
      public ActivityRecognitionEvent createFromParcel(Parcel param1Parcel) {
        String str = param1Parcel.readString();
        int i = param1Parcel.readInt();
        long l = param1Parcel.readLong();
        return new ActivityRecognitionEvent(str, i, l);
      }
      
      public ActivityRecognitionEvent[] newArray(int param1Int) {
        return new ActivityRecognitionEvent[param1Int];
      }
    };
  
  private final String mActivity;
  
  private final int mEventType;
  
  private final long mTimestampNs;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mActivity);
    paramParcel.writeInt(this.mEventType);
    paramParcel.writeLong(this.mTimestampNs);
  }
  
  public String toString() {
    String str = this.mActivity;
    int i = this.mEventType;
    long l = this.mTimestampNs;
    return String.format("Activity='%s', EventType=%s, TimestampNs=%s", new Object[] { str, Integer.valueOf(i), Long.valueOf(l) });
  }
}
