package android.hardware.location;

import android.annotation.SystemApi;

@SystemApi
public final class GeofenceHardwareRequest {
  private int mLastTransition = 4;
  
  private int mUnknownTimer = 30000;
  
  private int mMonitorTransitions = 7;
  
  private int mNotificationResponsiveness = 5000;
  
  private int mSourceTechnologies = 1;
  
  static final int GEOFENCE_TYPE_CIRCLE = 0;
  
  private double mLatitude;
  
  private double mLongitude;
  
  private double mRadius;
  
  private int mType;
  
  private void setCircularGeofence(double paramDouble1, double paramDouble2, double paramDouble3) {
    this.mLatitude = paramDouble1;
    this.mLongitude = paramDouble2;
    this.mRadius = paramDouble3;
    this.mType = 0;
  }
  
  public static GeofenceHardwareRequest createCircularGeofence(double paramDouble1, double paramDouble2, double paramDouble3) {
    GeofenceHardwareRequest geofenceHardwareRequest = new GeofenceHardwareRequest();
    geofenceHardwareRequest.setCircularGeofence(paramDouble1, paramDouble2, paramDouble3);
    return geofenceHardwareRequest;
  }
  
  public void setLastTransition(int paramInt) {
    this.mLastTransition = paramInt;
  }
  
  public void setUnknownTimer(int paramInt) {
    this.mUnknownTimer = paramInt;
  }
  
  public void setMonitorTransitions(int paramInt) {
    this.mMonitorTransitions = paramInt;
  }
  
  public void setNotificationResponsiveness(int paramInt) {
    this.mNotificationResponsiveness = paramInt;
  }
  
  public void setSourceTechnologies(int paramInt) {
    paramInt &= 0x1F;
    if (paramInt != 0) {
      this.mSourceTechnologies = paramInt;
      return;
    } 
    throw new IllegalArgumentException("At least one valid source technology must be set.");
  }
  
  public double getLatitude() {
    return this.mLatitude;
  }
  
  public double getLongitude() {
    return this.mLongitude;
  }
  
  public double getRadius() {
    return this.mRadius;
  }
  
  public int getMonitorTransitions() {
    return this.mMonitorTransitions;
  }
  
  public int getUnknownTimer() {
    return this.mUnknownTimer;
  }
  
  public int getNotificationResponsiveness() {
    return this.mNotificationResponsiveness;
  }
  
  public int getLastTransition() {
    return this.mLastTransition;
  }
  
  public int getSourceTechnologies() {
    return this.mSourceTechnologies;
  }
  
  int getType() {
    return this.mType;
  }
}
