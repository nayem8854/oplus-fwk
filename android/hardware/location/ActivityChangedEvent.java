package android.hardware.location;

import android.os.Parcel;
import android.os.Parcelable;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.List;

public class ActivityChangedEvent implements Parcelable {
  public ActivityChangedEvent(ActivityRecognitionEvent[] paramArrayOfActivityRecognitionEvent) {
    if (paramArrayOfActivityRecognitionEvent != null) {
      this.mActivityRecognitionEvents = Arrays.asList(paramArrayOfActivityRecognitionEvent);
      return;
    } 
    throw new InvalidParameterException("Parameter 'activityRecognitionEvents' must not be null.");
  }
  
  public Iterable<ActivityRecognitionEvent> getActivityRecognitionEvents() {
    return this.mActivityRecognitionEvents;
  }
  
  public static final Parcelable.Creator<ActivityChangedEvent> CREATOR = new Parcelable.Creator<ActivityChangedEvent>() {
      public ActivityChangedEvent createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        ActivityRecognitionEvent[] arrayOfActivityRecognitionEvent = new ActivityRecognitionEvent[i];
        param1Parcel.readTypedArray((Object[])arrayOfActivityRecognitionEvent, ActivityRecognitionEvent.CREATOR);
        return new ActivityChangedEvent(arrayOfActivityRecognitionEvent);
      }
      
      public ActivityChangedEvent[] newArray(int param1Int) {
        return new ActivityChangedEvent[param1Int];
      }
    };
  
  private final List<ActivityRecognitionEvent> mActivityRecognitionEvents;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    List<ActivityRecognitionEvent> list = this.mActivityRecognitionEvents;
    ActivityRecognitionEvent[] arrayOfActivityRecognitionEvent = list.<ActivityRecognitionEvent>toArray(new ActivityRecognitionEvent[0]);
    paramParcel.writeInt(arrayOfActivityRecognitionEvent.length);
    paramParcel.writeTypedArray((Parcelable[])arrayOfActivityRecognitionEvent, paramInt);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("[ ActivityChangedEvent:");
    for (ActivityRecognitionEvent activityRecognitionEvent : this.mActivityRecognitionEvents) {
      stringBuilder.append("\n    ");
      stringBuilder.append(activityRecognitionEvent.toString());
    } 
    stringBuilder.append("\n]");
    return stringBuilder.toString();
  }
}
